<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'database_name_here');

/** MySQL database username */
define('DB_USER', 'username_here');

/** MySQL database password */
define('DB_PASSWORD', 'password_here');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/** Define enviroment */
define('WP_ENV', 'dev');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'put your unique phrase here');
define('SECURE_AUTH_KEY',  'put your unique phrase here');
define('LOGGED_IN_KEY',    'put your unique phrase here');
define('NONCE_KEY',        'put your unique phrase here');
define('AUTH_SALT',        'put your unique phrase here');
define('SECURE_AUTH_SALT', 'put your unique phrase here');
define('LOGGED_IN_SALT',   'put your unique phrase here');
define('NONCE_SALT',       'put your unique phrase here');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/**
 * Solr configuration
 * */
define('APL_SOLR_DATA_DIR', '/solr_source/server/solr'); // Require for the network admin whenever you enable solr
define('APL_SOLR_HOST', '127.0.0.1'); // Not require, if this parameter does not exist the default value is 127.0.0.2
define('APL_SOLR_PORT', '8983'); // Not require, if this parameter does not exist the default value is 8983

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** config variables for get users on reference server. Define above the line <require_once(ABSPATH . 'wp-settings.php');>*/
//define('APL_USER_API_BASE_URL', 'http://local1.apollo-theme.elidev.info/api/v1/users/apollo-users-api');
//define('APL_USER_API_GET_FROM_USER_ID', 0);
//define('APL_USER_API_TOKEN', 'HaXEFxCcWfW3hgNnd20IkxH0B7wdIcBpfYKqL26Q');
//define('APL_USER_API_REFERENCE_SERVER_NAME', 'Web 4');
//define('APL_USER_API_HIDDEN', true);
/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/**
 * Set up MongoDB for tracking event clicks
 *
 * MongoDB does not enable access control by default.
 * So, if you are not configure username and password, just leave it empty.
 * Ref: https://docs.mongodb.com/v3.4/core/authorization/#enable-access-control
 *
 * @ticket 11252
 */
define('MONGODB_HOST'    , 'localhost');
define('MONGODB_PORT'    , 27017);
define('MONGODB_USERNAME', '');
define('MONGODB_PASSWORD', '');


// Define a default Google API key server
define('APL_GOOGLE_API_KEY_SERVER', 'AIzaSyB-IfveaF3vLc2aRXy-t5O94NctKgaofvA');


define('APL_USER_API_BASE_URL', 'http://network1.artsopolis.com/api/v1/users/apollo-users-api');
define('APL_USER_API_GET_FROM_USER_ID', 28966);
define('APL_USER_API_GET_FROM_USER_ID', 30000);
define('APL_USER_API_TOKEN', 'HaXEFxCcWfW3hgNnd20IkxH0B7wdIcBpfYKqL26Q');
define('APL_USER_API_REFERENCE_SERVER_NAME', 'BlueHost');
define('APOLLO_MIGRATED_SITES', array());