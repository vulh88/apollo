<?php 

require_once( 'wp-load.php' );

$url = $_SERVER['QUERY_STRING'];
$vars = explode('&',$url);
$keys = array();
	foreach ($vars as $var){
		list($key,$val) = explode('=',$var);
		$keys[$key] = $val;
	}

//print_r($keys);

global $wpdb;

if ($keys['post'] != '') {

	// Get the duplicate post data
	$querystr = "SELECT * from ".$wpdb->prefix."apollo_duplicates where post_id_1 = ".$keys['post']." limit 1";
	$dupe = $wpdb->get_results($querystr, OBJECT);

	// Newer dupe
	$querystr = "SELECT * from ".$wpdb->prefix."posts where ID=". $dupe[0]->post_id_1." limit 1";
	$event1 = $wpdb->get_results($querystr, OBJECT);
	$querystr = "SELECT * from ".$wpdb->prefix."users where ID = ". $event1[0]->post_author." limit 1";
	$user1 = $wpdb->get_results($querystr, OBJECT);

	// Older dupe
	$querystr = "SELECT * from ".$wpdb->prefix."posts where ID=". $dupe[0]->post_id_2." limit 1";
	$event2 = $wpdb->get_results($querystr, OBJECT);
	$querystr = "SELECT * from ".$wpdb->prefix."users where ID = ". $event2[0]->post_author." limit 1";
	$user2 = $wpdb->get_results($querystr, OBJECT);


	// Are we doing an event?
	if ($event1[0]->post_type == 'event'){
	
       $start_date1 = $wpdb->get_results("SELECT meta_value FROM $wpdb->apollo_eventmeta WHERE meta_key = '_apollo_event_start_date' AND apollo_event_id = " . $dupe[0]->post_id_1);
	$start_date1 = date("m-d-Y", strtotime($start_date1[0]->meta_value));
       $end_date1 = $wpdb->get_results("SELECT meta_value FROM $wpdb->apollo_eventmeta WHERE meta_key = '_apollo_event_end_date' AND apollo_event_id = " .  $dupe[0]->post_id_1);
	$end_date1 = date("m-d-Y", strtotime($end_date1[0]->meta_value));

       $start_date2 = $wpdb->get_results("SELECT meta_value FROM $wpdb->apollo_eventmeta WHERE meta_key = '_apollo_event_start_date' AND apollo_event_id = " . $dupe[0]->post_id_2);
	$start_date2 = date("m-d-Y", strtotime($start_date2[0]->meta_value));
       $end_date2 = $wpdb->get_results("SELECT meta_value FROM $wpdb->apollo_eventmeta WHERE meta_key = '_apollo_event_end_date' AND apollo_event_id = " . $dupe[0]->post_id_2);
	$end_date2 = date("m-d-Y", strtotime($end_date2[0]->meta_value));

	// get venue info
	$venue1 = $wpdb->get_results("select post_title from $wpdb->posts where ID = (SELECT meta_value FROM $wpdb->apollo_eventmeta WHERE meta_key = '_apollo_event_venue' AND apollo_event_id = ".$dupe[0]->post_id_1.")");
	$venue1 = $venue1[0]->post_title;
	$venue2 = $wpdb->get_results("select post_title from $wpdb->posts where ID = (SELECT meta_value FROM $wpdb->apollo_eventmeta WHERE meta_key = '_apollo_event_venue' AND apollo_event_id = ".$dupe[0]->post_id_2.")");
	$venue2 = $venue2[0]->post_title;

	// Get org info
	$org_name = $wpdb->get_results("select post_title from $wpdb->posts where ID = (select org_id from $wpdb->apollo_event_org where is_main = 'yes' and post_id = ".$dupe[0]->post_id_1.")");
	$org_name1 = $org_name[0]->post_title;
	$org_name = $wpdb->get_results("select post_title from $wpdb->posts where ID = (select org_id from $wpdb->apollo_event_org where is_main = 'yes' and post_id = " .$dupe[0]->post_id_2.")");
	$org_name2 = $org_name[0]->post_title;


		echo "<SCRIPT language=JavaScript>	function win(){ window.opener.location.href=\"dupe_window.php?postid=".$dupe[0]->post_id_1."\"; self.close(); }</SCRIPT>";

		echo "<style>td, th {font-family:arial; font-size:12px;}</style>";
		echo "<table border=1><th>Existing Event (status:".$event2[0]->post_status.")</th><th>Possible Duplicate Event (status:".$event1[0]->post_status.")</th>";
		echo "<tr><td width=50%>TITLE : ".$event2[0]->post_title."</td><td width=50%>TITLE : ".$event1[0]->post_title."</td></tr>";
		echo "<tr><td>DESC : ".substr($event2[0]->post_content, 0, 100)."</td><td>DESC : ".substr($event1[0]->post_content, 0, 100)."</td></tr>";
		echo "<tr><td>START DATE : $start_date2</td><td>START DATE : $start_date1</td></tr>";
		echo "<tr><td>END DATE : $end_date2</td><td>END DATE : $end_date1</td></tr>";
		echo "<tr><td>ORG : $org_name2</td><td>ORG : $org_name1</td></tr>";
		echo "<tr><td>VENUE : ".$venue2."</td><td>VENUE : ".$venue1."</td></tr>";
		echo "<tr><td>EVENT ID : ".$event2[0]->ID."</td><td>EVENT ID : ".$event1[0]->ID."</td></tr>";
		echo "<tr><td>USER ID : ".$event2[0]->post_author." (".$user2[0]->display_name.")</td><td>USER ID : ".$event1[0]->post_author." (".$user1[0]->display_name.")</td></tr>";
		echo "<tr><td>&nbsp;</td><td>&nbsp;</td></tr>";
		echo "<tr><td>&nbsp;</td><td>DUPLICATE EVENT CONFIDENCE : <span style=\"color:red; font-weight:bold;\">".$dupe[0]->confidence."</span></td></tr>";
		//echo "<tr><td><button type=\"button\" onclick=\"javascript:window.close()\">CLOSE THIS WINDOW</button></td><td><form action=\"/admin/dupes/delete_dupe/$dupeventID\"><input type=\"submit\" value=\"DELETE DUPLICATE EVENT\"></form></td></tr>";
		echo "<tr><td><button type=\"button\" onclick=\"javascript:window.close()\">CLOSE THIS WINDOW</button></td><td><center><input type=button onClick=\"win();\" value=\"DELETE DUPLICATE\"></center></td></tr>";

		echo "</table>";
		exit;
	}


	// Are we doing an org?
	if ($event1[0]->post_type == 'organization'){

	// Get org info
	$org_name = $wpdb->get_results("select post_title from $wpdb->posts where ID = (select org_id from $wpdb->apollo_event_org where is_main = 'yes' and post_id = ".$dupe[0]->post_id_1.")");
	$org_name1 = $org_name[0]->post_title;
	$org_name = $wpdb->get_results("select post_title from $wpdb->posts where ID = (select org_id from $wpdb->apollo_event_org where is_main = 'yes' and post_id = " .$dupe[0]->post_id_2.")");
	$org_name2 = $org_name[0]->post_title;

	$org =  $wpdb->get_results("select meta_key, meta_value from $wpdb->apollo_organizationmeta where meta_key ='_apl_org_address' and apollo_organization_id = ".$dupe[0]->post_id_1);
	$org1 = unserialize(unserialize($org[0]->meta_value));
	$org1addr = $org1['_org_address1'].", ".$org1['_org_city']." ".$org1['_org_zip'];

	$org =  $wpdb->get_results("select meta_key, meta_value from $wpdb->apollo_organizationmeta where meta_key ='_apl_org_address' and apollo_organization_id = ".$dupe[0]->post_id_2);
	$org2 = unserialize(unserialize($org[0]->meta_value));
	$org2addr = $org2['_org_address1'].", ".$org2['_org_city']." ".$org2['_org_zip'];	


		echo "<SCRIPT language=JavaScript>	function win(){ window.opener.location.href=\"dupe_window.php?postid=".$dupe[0]->post_id_1."\"; self.close(); }</SCRIPT>";

		echo "<style>td, th {font-family:arial; font-size:12px;}</style>";
		echo "<table border=1><th>Existing Org (status:".$event2[0]->post_status.")</th><th>Possible Duplicate Org (status:".$event1[0]->post_status.")</th>";
		echo "<tr><td width=50%>ORG : ".$event2[0]->post_title."</td><td width=50%>ORG : ".$event1[0]->post_title."</td></tr>";
		echo "<tr><td>DESC : ".substr($event2[0]->post_content, 0, 100)."</td><td>DESC : ".substr($event1[0]->post_content, 0, 100)."</td></tr>";
		echo "<tr><td>ADDRESS : ".$org2addr."</td><td>ADDRESS : ".$org1addr."</td></tr>";
		echo "<tr><td>ORG ID : ".$event2[0]->ID."</td><td>ORG ID : ".$event1[0]->ID."</td></tr>";
		echo "<tr><td>USER ID : ".$event2[0]->post_author." (".$user2[0]->display_name.")</td><td>USER ID : ".$event1[0]->post_author." (".$user1[0]->display_name.")</td></tr>";
		echo "<tr><td>&nbsp;</td><td>&nbsp;</td></tr>";
		echo "<tr><td>&nbsp;</td><td>DUPLICATE ORG CONFIDENCE : <span style=\"color:red; font-weight:bold;\">".$dupe[0]->confidence."</span></td></tr>";
		//echo "<tr><td><button type=\"button\" onclick=\"javascript:window.close()\">CLOSE THIS WINDOW</button></td><td><form action=\"/admin/dupes/delete_dupe/$dupeventID\"><input type=\"submit\" value=\"DELETE DUPLICATE ORG\"></form></td></tr>";
		echo "<tr><td><button type=\"button\" onclick=\"javascript:window.close()\">CLOSE THIS WINDOW</button></td><td><center><input type=button onClick=\"win();\" value=\"DELETE DUPLICATE\"></center></td></tr>";

		echo "</table>";
		exit;


	}


	// Are we doing an venue?
	if ($event1[0]->post_type == 'venue'){

	// get venue info
	$venue1 = $wpdb->get_results("select post_title from $wpdb->posts where ID = (SELECT meta_value FROM $wpdb->apollo_eventmeta WHERE meta_key = '_apollo_event_venue' AND apollo_event_id = ".$dupe[0]->post_id_1.")");
	$venue1 = $venue1[0]->post_title;
	$venue2 = $wpdb->get_results("select post_title from $wpdb->posts where ID = (SELECT meta_value FROM $wpdb->apollo_eventmeta WHERE meta_key = '_apollo_event_venue' AND apollo_event_id = ".$dupe[0]->post_id_2.")");
	$venue2 = $venue2[0]->post_title;

	$ven =  $wpdb->get_results("select meta_key, meta_value from $wpdb->apollo_venuemeta where meta_key ='_apl_venue_address' and apollo_venue_id = ".$dupe[0]->post_id_1);
	$ven1 = unserialize(unserialize($ven[0]->meta_value));
	$ven1addr = $ven1['_venue_address1'].", ".$ven1['_venue_city']." ".$ven1['_venue_zip'];

	$ven =  $wpdb->get_results("select meta_key, meta_value from $wpdb->apollo_venuemeta where meta_key ='_apl_venue_address' and apollo_venue_id = ".$dupe[0]->post_id_1);
	$ven2 = unserialize(unserialize($ven[0]->meta_value));
	$ven2addr = $ven2['_venue_address1'].", ".$ven2['_venue_city']." ".$ven2['_venue_zip'];

		echo "<SCRIPT language=JavaScript>	function win(){ window.opener.location.href=\"dupe_window.php?postid=".$dupe[0]->post_id_1."\"; self.close(); }</SCRIPT>";

		echo "<style>td, th {font-family:arial; font-size:12px;}</style>";
		echo "<table border=1><th>Existing Venue (status:".$event2[0]->post_status.")</th><th>Possible Duplicate Venue (status:".$event1[0]->post_status.")</th>";
		echo "<tr><td width=50%>VENUE : ".$event2[0]->post_title."</td><td width=50%>VENUE : ".$event1[0]->post_title."</td></tr>";
		echo "<tr><td>DESC : ".substr($event2[0]->post_content, 0, 100)."</td><td>DESC : ".substr($event1[0]->post_content, 0, 100)."</td></tr>";
		echo "<tr><td>ADDRESS : ".$ven2addr."</td><td>ADDRESS : ".$ven1addr."</td></tr>";
		echo "<tr><td>VENUE ID : ".$event2[0]->ID."</td><td>VENUE ID : ".$event1[0]->ID."</td></tr>";
		echo "<tr><td>USER ID : ".$event2[0]->post_author." (".$user2[0]->display_name.")</td><td>USER ID : ".$event1[0]->post_author." (".$user1[0]->display_name.")</td></tr>";
		echo "<tr><td>&nbsp;</td><td>&nbsp;</td></tr>";
		echo "<tr><td>&nbsp;</td><td>DUPLICATE VENUE CONFIDENCE : <span style=\"color:red; font-weight:bold;\">".$dupe[0]->confidence."</span></td></tr>";
		//echo "<tr><td><button type=\"button\" onclick=\"javascript:window.close()\">CLOSE THIS WINDOW</button></td><td><form action=\"/admin/dupes/delete_dupe/$dupeventID\"><input type=\"submit\" value=\"DELETE DUPLICATE ORG\"></form></td></tr>";
		echo "<tr><td><button type=\"button\" onclick=\"javascript:window.close()\">CLOSE THIS WINDOW</button></td><td><center><input type=button onClick=\"win();\" value=\"DELETE DUPLICATE\"></center></td></tr>";

		echo "</table>";
		exit;


	}


} 

if ($keys['postid'] != '') { 

	// Allow admin to send post to trash

	if (current_user_can( 'manage_options' )) {
       	wp_trash_post( $keys['postid']);
	}

}


?>