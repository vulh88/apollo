module.exports = function (grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jade: {
      compile: {
        options: {
          pretty: true
        },
        files: [{
            expand: true,
            cwd: 'source/views/fullscreen/',
            src: ['full-screen-*.jade'],
            dest: 'public/page',
            ext: '.html'
          },
          {
            expand: true,
            cwd: 'source/views/sitemap/',
            src: ['*.jade'],
            dest: 'public/sitemap',
            ext: '.html'
          },
          {
            expand: true,
            cwd: 'source/views/blocks',
            src: ['*.jade'],
            dest: 'public/blocks',
            ext: '.html'
          },
          {
            expand: true,
            cwd: 'source/views/index',
            src: ['*.jade'],
            dest: 'public/index',
            ext: '.html'
          },
          {
            expand: true,
            cwd: 'source/views/acc',
            src: ['*.jade'],
            dest: 'public/acc',
            ext: '.html'
          },
          {
            expand: true,
            cwd: 'source/views/artist',
            src: ['*.jade'],
            dest: 'public/artist',
            ext: '.html'
          },
          {
            expand: true,
            cwd: 'source/views/blog',
            src: ['*.jade'],
            dest: 'public/blog',
            ext: '.html'
          },
          {
            expand: true,
            cwd: 'source/views/business',
            src: ['*.jade'],
            dest: 'public/business',
            ext: '.html'
          },
          {
            expand: true,
            cwd: 'source/views/category',
            src: ['*.jade'],
            dest: 'public/category',
            ext: '.html'
          },
          {
            expand: true,
            cwd: 'source/views/classified',
            src: ['*.jade'],
            dest: 'public/classified',
            ext: '.html'
          },
          {
            expand: true,
            cwd: 'source/views/education',
            src: ['*.jade'],
            dest: 'public/education',
            ext: '.html'
          },
          {
            expand: true,
            cwd: 'source/views/event',
            src: ['*.jade'],
            dest: 'public/event',
            ext: '.html'
          },
          {
            expand: true,
            cwd: 'source/views/featured-event',
            src: ['*.jade'],
            dest: 'public/featured-event',
            ext: '.html'
          },
          {
            expand: true,
            cwd: 'source/views/home',
            src: ['*.jade'],
            dest: 'public/home',
            ext: '.html'
          },
          {
            expand: true,
            cwd: 'source/views/organization',
            src: ['*.jade'],
            dest: 'public/organization',
            ext: '.html'
          },
          {
            expand: true,
            cwd: 'source/views/venue',
            src: ['*.jade'],
            dest: 'public/venue',
            ext: '.html'
          },
          {
            expand: true,
            cwd: 'source/views/others',
            src: ['*.jade'],
            dest: 'public/others',
            ext: '.html'
          }
        ]
      }
    },
    less: {
      build: {
        options: {
          compress: false,
          sourceMap: true
        },
        files: [{
          'public/assets/css/style.css': 'source/assets/css/style.less',
          'public/assets/css/global-style.css': 'source/assets/css/global/global-style.less',
          'public/assets/css/octave.css': 'source/assets/css/octave.less',
          'public/assets/css/print.css': 'source/assets/css/print.less',
          'public/assets/css/business-spotlight.css': 'source/assets/css/business-spotlight.less',
          'public/assets/css/_style-new-template.css': 'source/assets/css/_style-new-template.less',
        }
        ]
      }
    },
    concat: {
      libs: {
        src: 'source/assets/js/libs/jquery-1.11.1.js',
        dest: 'public/assets/js/libs.js'
      },
      plugins: {
        src: 'source/assets/js/plugins/*.js',
        dest: 'public/assets/js/plugins.js'
      },
      start: {
        src: 'source/assets/js/start.js',
        dest: 'public/assets/js/start.js'
      },
      start_v1: {
        src: 'source/assets/js/start_v1.js',
        dest: 'public/assets/js/start_v1.js'
      }
    },
    copy: {
      ajax: {
        files: [{
          expand: true,
          cwd: 'source/views/ajax/',
          src: '*',
          dest: 'public/ajax/'
        }
        ]
      },
      robots: {
        files: [{
          expand: true,
          cwd: 'source/assets/robots/',
          src: '*',
          dest: 'public'
        }
        ]
      },
      scripts: {
        files: [{
          expand: true,
          cwd: 'source/assets/js/',
          src: '**/*',
          dest: 'public/assets/js/'
        }
        ]
      },
      images: {
        files: [{
          expand: true,
          cwd: 'source/assets/images/',
          src: '**/*',
          dest: 'public/assets/images/'
        }
        ]
      },
      uploads: {
        files: [{
          expand: true,
          cwd: 'source/assets/uploads/',
          src: '**/*',
          dest: 'public/assets/uploads/'
        }
        ]
      },
      fonts: {
        files: [{
          expand: true,
          cwd: 'source/assets/fonts/',
          src: '**/*',
          dest: 'public/assets/fonts/'
        }
        ]
      }
    },
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      files: ['source/assets/js/plugins/plugin-*.js', 'source/assets/js/*.js']
    },
    watch: {
      options: {
        livereload: {
          host: 'localhost',
          port: 3011
        }
      },
      js: {
        files: ['<%= jshint.files %>'],
        tasks: ['concat']
      },
      jade: {
        files: ['source/views/**/*.jade'],
        tasks: ['jade']
      },
      less: {
        files: ['source/assets/css/**/*.less'],
        tasks: ['less:build']
      },
      ajax: {
        files: ['source/views/ajax/**/*.*'],
        tasks: ['copy:ajax']
      },
      fonts: {
        files: ['source/assets/fonts/**/*'],
        tasks: ['copy:fonts']
      },
      images: {
        files: ['source/assets/images/**/*'],
        tasks: ['copy:images']
      },
      uploads: {
        files: ['source/assets/uploads/**/*'],
        tasks: ['copy:uploads']
      }
    },
    cssmin: {
      compress: {
        files: [{
          'public/assets/css/style.css': 'public/assets/css/style.css',
          'public/assets/css/octave.css': 'public/assets/css/octave.css',
          'public/assets/css/_style-new-template.css': 'public/assets/css/_style-new-template.css',
          'public/assets/css/business-spotlight.css': 'public/assets/css/business-spotlight.less'
        }
        ]
      }
    },
    usemin: {
      // html: ['public/**/*.html'],
      css: ['public/css/**/*.css']
    },
    uglify: {
      options: {
        compress: true,
        beautify: false,
        preserveComments: false
      },
      libs: {
        files: [{
          'public/js/libs.js': 'source/assets/js/libs/jquery-1.11.1.js'
        }
        ]
      },
      plugins: {
        files: [{
          'public/js/plugins.js': 'public/js/plugins.js'
        }
        ]
      }
    },
    clean: {
      build: ['public']
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-usemin');

  grunt.registerTask('default', ['clean:build', 'concat', 'less:build', 'jade', 'copy']);
  grunt.registerTask('build', 'default');
  // grunt.registerTask('release', ['build', 'uglify', 'cssmin', 'usemin']);
  grunt.registerTask('deploy', ['build', 'uglify']);

};