function initOctave($) {
  var $body = $('body');
  var isDefaultHeader = $body.hasClass('apl-default-header');

  //Helper
  function runOnWindowResize(callback, time) {
    var resizeTimer;
    $(window).on('resize', function (e) {
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(function () {
        callback();
      }, time || 250);
    });
  }

  function getFixedItems() {
    var fixedItems = $('*:visible:not(#cboxOverlay)').filter(function () {
      return $(this).css('position') === 'fixed' && $(this).outerWidth() >= $(document.body).outerWidth() * 0.75 && $(this).outerHeight() < $(document.body).outerHeight();
    });
    return fixedItems;
  }

  function isDesktop() {
    return $(window).width() > 768;
  }


  function isFixed($el) {
    return $el.css("position") === 'fixed';
  }

  function isAbsolute($el) {
    return $el.css("position") === 'absolute';
  }

  // End helpers

  /**
   * [toggleClasses use for toggle class onclick]
   */
  function toggleClasses() {
    $(document).on('click', '[data-toggle]', function (e) {
      var $target = $($(this).data('toggle-target') || this);
      var classToggle = $(this).data('toggle');
      $target.toggleClass(classToggle);
    });

    $('*').on("click", function (e) {
      if ($(e.target).is('.overplay,.overplay *, .search-tool .search-form,.search-tool .search-form *')) {
        e.stopPropagation();
        return;
      }
      if ($('[data-toggle].show').length) {
        if(!$(e.target).is('[data-toggle]'))
          $('[data-toggle].show').removeClass('show');
      }
    });
  }

  function addToggleToSearchButton() {
    $('.top-blk.search-box').attr('data-toggle', 'show');
    $('.top-blk.search-box form').addClass('overplay');
  }


  /**
   * [Fixed header when scrolls]
   */
  function initHeader() {
    var $body         = $('body');
    var $nav          = $body.find('.header');
    var $topbar = $body.find('#topbar');
    var fixedClass = 'oct-active--scroll--fixed';
    var triggerPosition = 0;

    function calculatePaddingTop() {
      return (!isDesktop() || $body.hasClass('oct-transparent--bg') || $('body.apl-default-header').get(0) ? 0 : $nav.height()) + ($topbar ? $topbar.innerHeight() : 0);
    }
    function createStyle() { return $('<style>body.'+fixedClass+' { padding-top: '+calculatePaddingTop()+'px; }</style>')}

    if ($nav.length && ($topbar.hasClass('scroll-with-page') || $body.hasClass('header-scroll-with-page'))) {
      var $style = createStyle();
      $('html > head').append($style);

      $(window).scroll(function () {
        var self = $(this);
        if (self.scrollTop() > triggerPosition) {
          $nav.addClass("header--fixed");
          $topbar.addClass("fixed-topBar");
          $body.addClass(fixedClass);
        }
        else {
          $topbar.removeClass("fixed-topBar");
          $nav.removeClass("header--fixed");
          $body.removeClass(fixedClass);
        }
      });

      runOnWindowResize(function () {
        $style.get(0).innerHTML = (createStyle()).get(0).innerHTML;
      }, 10);
    }
  }

  function mainMenuScrollWithPage() {
    var $mainMenu = $('header .main-menu.pc-show.scroll-with-page');
    var $topbarFixed = $('#topbar.scroll-with-page');
    var $adminBar = $('#wpadminbar');
    if (isDesktop() && $mainMenu.get(0) && isDefaultHeader) {
      var topbarOffsetFallback = ($topbarFixed.get(0) ?  $topbarFixed.innerHeight() : 0) + ($adminBar.get(0) ? $adminBar.innerHeight() : 0);
      var startPosition = $mainMenu.offset().top - topbarOffsetFallback;
      console.log('topbarOffsetFallback',topbarOffsetFallback);
      // Create placeholder
      var $placeHolder = $('<div class="fixed-placeholder"></div>');
      $placeHolder.css({height: $mainMenu.innerHeight() + 'px'});

      // Fix style position
      $mainMenu.css({top: topbarOffsetFallback});
      $mainMenu.after($placeHolder);

      $(window).scroll(function () {
        var self = $(this);
        if (self.scrollTop() > startPosition) {
          $mainMenu.addClass("main-menu--fixed enable-placeholder");
        }
        else {
          $mainMenu.removeClass("main-menu--fixed enable-placeholder");
        }
      });

    }
  }

  function featuredSliderItem() {
    var $mainFeatureItem = $('.featured-event-blk .item-cat');
    $mainFeatureItem.each( function() {
      var self = $(this),
          $actionBtnRow    = self.find('.wrapper-scroll--action'),
          $prevAct         = $actionBtnRow.find('.fea-act-prev'),
          $nextAct         = $actionBtnRow.find('.fea-act-next'),
          $listContent     = self.find('.fea-evt-row.fea-evt-list--scroll'),
          $itemFeature     = $listContent.find('.fea-evt-item');
      var feaItmWidth      = $itemFeature.outerWidth(true);
      var totalItems       = $itemFeature.length;
      var currentScoll = 0;

      $listContent.on('scroll', function () {
        var scrollLeftPost = $listContent[0].scrollLeft;
        currentScoll = scrollLeftPost;
      });
      // Prev Click
      $prevAct.on('click', function (event) {
        event.preventDefault();
        var prevScroll = currentScoll - feaItmWidth;
        if (prevScroll <= 0) {
          currentScoll = 0;
        } else {
          currentScoll = currentScoll - feaItmWidth;
        }
        $listContent.animate({
          scrollLeft: currentScoll
        }, 250);
      });

      // Next Click
      $nextAct.on('click', function (event) {
        event.preventDefault();
        var nextScroll = currentScoll + feaItmWidth;
        var lastScroll = feaItmWidth * totalItems - $listContent[0].offsetWidth - 15

        if (nextScroll >= lastScroll) {
          currentScoll = lastScroll;
        } else {
          currentScoll = currentScoll + feaItmWidth;
        }

        $listContent.animate({
          scrollLeft: currentScoll
        }, 250);
      });
    });
  }

  function featuredSlickSlider() {
    var $mainFeatureItem = $('.item-cat__rotate-title .fea-evt-row');

    $mainFeatureItem.each( function() {
      $(this).slick({
        arrows: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: true,

        centerMode: false,
        infinite: false,
        centerPadding: '0',
        responsive: [
          {
            breakpoint: 769,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 561,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      });

      var $wrapperDot = $(this).find(".slick-dots");
      ($wrapperDot).after($('<div class="wrapper-action-slick"></div>'));
      var $buttonPrev = $(this).find("button.slick-prev");
      var $buttonNext = $(this).find("button.slick-next");
      var $actionSlick = $(this).find(".wrapper-action-slick");
      if ($actionSlick.length) {
        $actionSlick.append($buttonPrev);
        $actionSlick.append($wrapperDot);
        $actionSlick.append($buttonNext);
      }
    });
  }

  addToggleToSearchButton();
  mainMenuScrollWithPage();
  featuredSliderItem();
  featuredSlickSlider();
  toggleClasses();
  initHeader();
}

(function ($) {
  $(document).ready(function () {
    initOctave($);
  });
})(window.$ || window.jQuery);

window.$ = window.jQuery;
