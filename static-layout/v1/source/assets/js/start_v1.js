/**
 * START - ONLOAD - JS
 */
/* ----------------------------------------------- */
/* ------------- FrontEnd Functions -------------- */
/* ----------------------------------------------- */
(function($) {
    function collapseSubMenuChild() {
        var $menuCollapseBtn     = $('.main-menu.tablet-show .mn-menu.has-toogle__menu-child .nav > li.has-child > a');
        $menuCollapseBtn.each(function(e) {
            $(this).on('click', function(e){
                var $menuCollapseContent = $(this).siblings('ul.sub-menu.level-1');
                
                e.preventDefault();
                e.stopPropagation();
                
                $(this).toggleClass('active-show--child');
                $menuCollapseContent.toggleClass('show-full-sub__child');
            });
        });
    }

    jQuery(document).ready(function($){
        collapseSubMenuChild();
    });

    var init = function () {
    };

    $(window).on('load', init);    
})(jQuery);