/**
 * START - ONLOAD - JS
 */
/* ----------------------------------------------- */
/* ------------- FrontEnd Functions -------------- */
/* ----------------------------------------------- */

(function($) {
  /**
   * runMainSlider: init MAIN SLIDER in HOME
   * @param  {[type]} mslider [description]
   * @return {[type]}         [description]
   */
  function runMainSlider(mslider) {
      if (!$(mslider).length) {return;}
      $(mslider).flexslider({
        slideshowSpeed: 3000,
        animationSpeed: 2000
      });
  }

  /**
   * [runCalendarIpt description]
   * @param  {[type]} tgt [description]
   * @return {[type]}     [description]
   */
  function runCalendarIpt (tgt) {
      'use strict';
      if (!$('#' + tgt).length) {return;}
      /*$('#' + tgt).datepicker({
          dateFormat: 'yy-mm-dd',
          nextText: '&#187;',
          prevText: '&#171;'
      });*/
      if((typeof APL) === undefined){
          var currentLang = APL.currentLang != null ? APL.currentLang : '';
      }else{
          var currentLang="";
      }

      if (!$('#' + tgt).length) {return;}

      var options = $.extend({}, // empty object
          $.datepicker.regional[currentLang], {
              dateFormat: 'yy-mm-dd',
              nextText: '&#187;',
              prevText: '&#171;',
          }
      );

      $( '#' + tgt ).datepicker(options);


  }



  /**
   * [showSubMenu description]
   * @param  {[type]} mmenu [description]
   * @return {[type]}       [description]
   */
  function showSubMenu (mmenu) {
      if (!$(mmenu).length) {return;}
      if (!$(mmenu).find('.has-child').length) {return;}

      $(mmenu).find('.has-child').each(function(e) {
          $(this).on('mouseover', function (i) {
              var offsetLeft = $(this).offset().left;
              if(($(window).width() - offsetLeft) <230){

                  $('.sub-menu.level-1').css('left',-180);
                  $('.sub-menu.level-1').css('width',220);
              }else{
                  $('.sub-menu.level-1').css('left',0);
                  $('.sub-menu.level-1').css('width',220);
              }
              var offsetRight = $(this).offset().left + 220;
              var _distance = $(window).width() - offsetRight;

              if( _distance < 230){
                  $('.sub-menu.level-2.show').css('left',-220);
              }else{
                  $('.sub-menu.level-2.show').css('left',220);
              }
              $(this).find('.sub-menu' + '.' + $(this).data('lvl')).addClass('show');
          }).on('mouseout', function (i) {
              $(this).find('.sub-menu' + '.' + $(this).data('lvl')).removeClass('show');
          });
      });
  }

  /**
   * [showMBMenu description]
   * @param  {[type]} mb_menu [description]
   */
  function showMBMenu (mb_menu) {
      if (!$(mb_menu).length) {return;}
      // body...
      $(mb_menu).on('click', function (e) {
          $this = $(this);
          if (!$($this.data('tgr')).length) {return;}

          $mb = $($this.data('tgr'));

          if ($mb.hasClass('show')) {
              $this.removeClass('active');
              $mb.removeClass('show');
          } else {
              $this.addClass('active');
              $mb.addClass('show');
          }
      });

      // CLICK OUT
      $( $(mb_menu).data('tgr') ).on( 'clickoutside', function(event){
          if ($(event.target).hasClass('mb-menu')) {
              return;
          }
          if ($($(mb_menu).data('tgr')).hasClass('show')) {
              $($(mb_menu).data('tgr')).removeClass('show');
              $(mb_menu).removeClass('active');
          }
      });

      /*$('.mobile-menu .mn-menu').find('.has-child').each(function(e) {
       $(this).on('click', function (i) {
       $(this).find('.sub-menu' + '.' + $(this).data('lvl')).toggleClass('show');
       });
       });*/
      $('.mobile-menu .mn-menu').find('li').each(function(e) {
          $(this).find('.has-child').each(function(e) {
              $(this).on('click', function (i) {
                  console.log('message');
                  $(this).parent().find('.sub-menu' + '.' + $(this).data('lvl')).toggleClass('show');
              });
          });
      });
  }

  /**
   * expBussList
   * @param  {[type]} mb_menu [description]
   */
  function expBussList () {
      if (!$('.nofi-exp').length) {return;}
      _self = $('.nofi-exp');
      _exp  = $('.' + _self.data('target'));
      var _id = _self.attr('id');

      // ON CLICK EXP LINK
      _self.on('click', function () {
          console.log(_id);
          if (_id) {
              if ($('.' + _id + ' li:gt(9)').css('display') != 'none') {
                  // _exp.hide();
                  $('.' + _id + ' li:gt(9)').hide();
                  _self.text($('#data-view-more').attr('data-viewmore'));
              } else {
                  //  _exp.show();
                  $('.' + _id + ' li:gt(9)').show();
                  _self.text($('#data-view-less').attr('data-viewless'));
              }
          } else {
              if ($('.nofi-more li:gt(9)').css('display') != 'none') {
                  // _exp.hide();
                  $('.nofi-more li:gt(9)').hide();
                  _self.text($('#data-view-more').attr('data-viewmore'));
              } else {
                  //  _exp.show();
                  $('.nofi-more li:gt(9)').show();
                  _self.text($('#data-view-less').attr('data-viewless'));
              }
          }
      });
  }

  /**
   * [expDSBMenu description]
   * @param  {[type]} ahaschild [description]
   * @return {[type]}           [description]
   */
  function expDSBMenu (ahaschild) {
      if (!$(ahaschild).length) {return;}
      var is_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;

      $a_hasChild = $(ahaschild);

      $('.dsb-main-nav .sub-nav').each(function (i) {
          $(this).data('heig', $(this).find('li').not('.hidden').length * $(this).find('li').eq(0).outerHeight());
      });

      $a_hasChild.on('click', function (e) {
          e.preventDefault();
          $this     = $(this);
          // force redirect to serve for special purpose of take user to page which we want to take them to
          if( !$this.hasClass('is-loading-page')
              && !_.isEmpty($this.attr('data-force-redirected'))
              && $this.attr('data-force-redirected') == 'true'){
              window.location.href = $this.attr('href');
              return false;
          } else {
              $this.removeClass('is-loading-page');
          }

          $sub_menu = $this.next('.sub-nav');

          if ( $sub_menu.hasClass('show')) {
              $sub_menu.css('height', 0).removeClass('show');
              if (is_firefox) { $sub_menu.find('ul').hide(); }
              $this.prev('i.arr.fa').removeClass('fa-angle-down').delay(10).addClass('fa-angle-right');
          } else {
              $('.dsb-main-nav .sub-nav').css('height', 0).removeClass('show');
              $sub_menu.css('height', $sub_menu.data('heig')).addClass('show');
              if (is_firefox) {
                  $('.dsb-main-nav .sub-nav').find('ul').hide();
                  $sub_menu.find('ul').delay(300).fadeIn(150);
              }
              $('.dsb-main-nav i.arr.fa').removeClass('fa-angle-down').delay(5).addClass('fa-angle-right');
              $this.prev('i.arr.fa').removeClass('fa-angle-right').delay(10).addClass('fa-angle-down');
          }
      });

      if (is_firefox) {
          $(function() {
              $('.dsb-main-nav .sub-nav').find('ul').hide();
          });
      }
  }
  /**
   * menu@Tiers
   */
  function menuTwoTiers(){
      if($('#main_nav')){
          $('#main_nav > li').click(function(){
              $('#main_nav > li').removeClass('selected');
              $(this).addClass('selected');
              if($(this).find('ul').length){
                  $('.menu-background').css('display','block');
              }else{
                  $('.menu-background').css('display','none');
              }
          })
      }
  }
  function changeTab(){
      if($('.nav-tab').length!==""){
          $('.tab-list li').click(function(e){
              e.preventDefault();
              $('.tab-list li').removeClass('selected');
              $(this).addClass('selected');
              var _curTab = $(this).find('a').attr('data-id');
              $('.blog-blk').css('display','none');
              $('.blog-blk[data-target='+_curTab+']').fadeIn(1000);
          });
          $('.tab-list li').first().trigger('click');
      }
      $('.tab-image-list li').click(function(e){
          e.preventDefault();
          $('.tab-image-list li').removeClass('selected');
          $(this).addClass('selected');
          var _curBkl = $(this).find('a').attr('data-id');
          $('.gallery-bkl').css('display','none');
          $('.gallery-bkl[data-target='+_curBkl+']').fadeIn(1000);
      });
      $('.tab-image-list li').first().trigger('click');

      // ARTIST DETAIL PAGE
      if($('.blog-bkl .nav-tab').length!==""){
          $('.blog-bkl .tab-list li').click(function(e){
              e.preventDefault();

              $('blog-bkl .tab-list li').removeClass('selected');
              $(this).addClass('selected');
              var _curTab = $(this).find('a').attr('data-id');
              $('.tab').css('display','none');
              $('.tab[data-target='+_curTab+']').fadeIn(1000);
          });
          $('.blog-bkl .tab-list li').first().trigger('click');
      }
  }

  function fixTab(){
      if($('.wc-f').length){
          var _columnW = $('.wc-f').width();
      }else{
          var _columnW = $('.wc-l').width();
      }
      var _navW =0;
      $('.nav-tab li').each(function(){
          _navW = _navW + $(this).width();
      });
      if( _columnW <360){
          $('.nav-tab').width('100%');
          $('.tab-list li').css('width','49%');
      }
      if(360 <=_columnW && _columnW < 420){
          $('.nav-tab').width('100%');
          if(_columnW -  _navW <= 30){
              $('.tab-list li').css('width','49%');
          }else{
              $('.tab-list li').css('width','auto');
          }
      }
      if( 421 < _columnW && _columnW <600){
          if(_columnW -  _navW <= 30){
              $('.tab-list li').css('width','33.1%');
          }else{
              $('.tab-list li').css('width','auto');
          }
      }
      if( 600 <= _columnW ){
          $('.tab-list li').css('width','auto');
      }
      // SEARCH
      $('.nav-tab-search li').each(function(){
          _navW = _navW + $(this).width();
      });
      if( _columnW <360){
          $('.tab-list-search li').css('width','49%');
      }
      if(360 <=_columnW && _columnW < 420){
          if(_columnW -  _navW <= 30){
              $('.tab-list-search li').css('width','49%');
          }else{
              $('.tab-list-search li').css('width','auto');
          }
      }
      if( 421 < _columnW && _columnW <600){
          if(_columnW -  _navW <= 30){
              $('.tab-list-search li').css('width','33.1%');
          }else{
              $('.tab-list-search li').css('width','auto');
          }
      }
      if( 600 <= _columnW ){
          $('.tab-list-search li').css('width','auto');
      }

  }
  function fixDSBMenu (ahaschild) {
      if (!$(ahaschild).length) {return;}
      $a_hasChild = $(ahaschild);

      if ($(window).width() > 580) {
          $('.dsb-main-nav > ul > li').show();
      } else {
          if ($a_hasChild.eq(2).is(":visible")) {
              $('.dsb-main-nav > ul > li').hide().filter($('a.h-menu').parent('li')).show();
          }
      }
  }

  function uploadFile(){
      $('#uploadBtn').change(function(e){
          e.preventDefault();
          $('.crop-img').css('display','block');
          $('.img-crop img').remove();
          $('.img-crop').append('<img src="uploads/crop-img.jpg">');
          $('.upload-img').css('display','none');
          $('.crop-img .upl-tt').click(function(){
              $('.crop-tool').css('display','block');
          });
          $('#save-crop, #cancel-crop').click(function(e){
              e.preventDefault();
              $('.crop-img').css('display','none');
              $('.crop-tool').css('display','none');
              $('.upload-img').css('display','block');
          });
      });

  }
  function radioChange(){
      $('#rad-audio').each(function(){
          $('input:radio').click(function(){
              if($(this).is(':checked')){
                  $(this).parent().parent().css('background','#ebebeb');
                  $('.media-list li').css('background','none');
                  $(this).parent().parent().css('background','#ebebeb');
              }
          });
      });
  }
  function secondSliderFix(){
      if($(window).width() <768 ){
          $('.slider-pic').find('img').each(function(){
              if($(this).height()<300){
                  $(this).css('top',150-($(this).height()/2));
              }else{
                  $(this).css('top',150-($(this).height()/2));
                  $(this).parent().css('overflow','hidden');
              }
          });
      }else{
          $('.slider-pic').find('img').each(function(){
              $('.slider-pic').height(300);
              if($(this).height()<300){
                  $(this).css('top',150-($(this).height()/2));
              }else{
                  $(this).css('top',105-($(this).height()/2));
                  $(this).parent().css('overflow','hidden');
              }
          });
      }


  }
  function sliderFix(){
    if ($('body.responsive-slider').length) {
      return;
    }
    if($('.main-slider').width()<=360){
          $('.main-slider').height(160);
          $('.slides:eq(0)').find('img').each(function(){
              $(this).height(160);
              $(this).width('auto');
              $(this).css('top',0);
              $(this).css('position','absolute');
              $(this).parent().parent().css('height',160);
              $(this).parent().parent().css('overflow','hidden');
              $('.i-caption').css('bottom',20);
              $('.flex-control-paging').css('bottom',10);
          });
      }
      if(360 < $('.main-slider').width() && $('.main-slider').width()<=480){
          $('.main-slider').height(250);
          $('.slides:eq(0)').find('img').each(function(){
              $(this).height(250);
              $(this).width('auto');
              $(this).css('top',0);
              $(this).css('position','absolute');
              $(this).parent().parent().css('height',250);
              $(this).parent().parent().css('overflow','hidden');
              $('.i-caption').css('bottom',30);
              $('.flex-control-paging').css('bottom',10);
          });
      }
      if(480 < $('.main-slider').width() && $('.main-slider').width()<=640){
          $('.main-slider').height(300);
          $('.slides:eq(0)').find('img').each(function(){
              $(this).height(300);
              $(this).width('auto');
              $(this).css('top',0);
              $(this).css('position','absolute');
              $(this).parent().parent().css('height',300);
              $(this).parent().parent().css('overflow','hidden');
              $('.i-caption').css('bottom',30);
              $('.flex-control-paging').css('bottom',10);
          });
      }
      if(640 < $('.main-slider').width() && $('.main-slider').width()<=768){
          $('.main-slider').height(350);
          $('.slides:eq(0)').find('img').each(function(){
              $(this).height(350);
              $(this).width('auto');
              $(this).css('top',0);
              $(this).css('position','absolute');
              $(this).parent().parent().css('height',350);
              $(this).parent().parent().css('overflow','hidden');
              $('.i-caption').css('bottom',30);
              $('.flex-control-paging').css('bottom',10);
          });
      }
      if(768 < $('.main-slider').width()){
          $('.main-slider').height(400);
          $('.slides:eq(0)').find('img').each(function(){
              $(this).width('100%');
              $(this).height('auto');
              if($(this).height()<400){
                  $(this).css('top',0);
                  $('.i-caption').css('bottom',200-($(this).height()/2)+30);
                  $('.flex-control-paging').css('bottom',200-($(this).height()/2)+10);
              }else{
                  $(this).css('top',0);
                  $('.i-caption').css('bottom',30);
                  $('.flex-control-paging').css('bottom',10);
              }
              $(this).css('position','absolute');
              $(this).parent().parent().css('height',400);
              $(this).parent().parent().css('overflow','hidden');
          });
      }
  }
  function tellAfriend(){
      $('#tellafriend').click(function(){
          var _tellAF = '<div class="tellafriend"></div';
          $('body,html').scrollTop(0);
          $('body').append(_tellAF);
          $('.tellAF-popup').css('display','block');
          $('.tellAF-popup').fadeIn(1000, 'linear');
          $('.close').click(function(){
              $('.tellAF-popup').css('display','none');
              $('.tellafriend').remove();
          });

      });
  }
  function pagingSelect(){
      if($('.rad-pg').length){
          $(".rad-pg").click (function(){
              var _sl = $("input:checked").val();
              if(_sl==="op"){
                  $('.search-bkl .blk-paging').css('display','none');
                  $('#load-more').css('display','block');
              }else{
                  $('.search-bkl .blk-paging').css('display','block');
                  $('#load-more').css('display','none');
              }
          });
      }
  }
  function checkAll(){
      $("#event-ckall").click (function(){
          if ($(this).is(':checked') ) {
              $('.check-event').each(function() { //loop through each checkbox
                  this.checked = true;  //select all checkboxes with class "checkbox1"
              });
          }else{
              $('.check-event').each(function() { //loop through each checkbox
                  this.checked = false;  //select all checkboxes with class "checkbox1"
              });
          }

      });

      $(".check-event").click (function(){
          if (this.checked === false) {
              if($("#event-ckall").is(':checked')){
                  $("#event-ckall").removeAttr('checked');
              }
          }else{
              if($(".check-event").length === $(".check-event:checked").length){
                  $("#event-ckall").prop('checked','true');
              }

          }
      });

      $("#org-ckall").click (function(){
          if ($(this).is(':checked') ) {
              $('.check-org').each(function() { //loop through each checkbox
                  this.checked = true;  //select all checkboxes with class "checkbox1"
              });
          }else{
              $('.check-org').each(function() { //loop through each checkbox
                  this.checked = false;  //select all checkboxes with class "checkbox1"
              });
          }

      });

      $(".check-org").click (function(){
          if (this.checked === false) {
              if($("#org-ckall").is(':checked')){
                  $("#org-ckall").removeAttr('checked');
              }
          }else{
              if($(".check-org").length === $(".check-org:checked").length){
                  $("#org-ckall").prop('checked','true');
              }

          }
      });

      $("#venue-ckall").click (function(){
          if ($(this).is(':checked') ) {
              $('.check-venue').each(function() { //loop through each checkbox
                  this.checked = true;  //select all checkboxes with class "checkbox1"
              });
          }else{
              $('.check-venue').each(function() { //loop through each checkbox
                  this.checked = false;  //select all checkboxes with class "checkbox1"
              });
          }

      });

      $(".check-venue").click (function(){
          if (this.checked === false) {
              if($("#venue-ckall").is(':checked')){
                  $("#venue-ckall").removeAttr('checked');
              }
          }else{
              if($(".check-venue").length === $(".check-venue:checked").length){
                  $("#venue-ckall").prop('checked','true');
              }

          }
      });

  }
  function ytVidId(url) {
      var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
      return (url.match(p)) ? RegExp.$1 : false;
  }
  function viewVideo(){
      $('#video-preview').click(function(e){
          e.preventDefault();

          var embeds = $( 'input[name="video_embed[]"]' ),
              is_empty = true;
          $.each( embeds, function( i, v) {
              if ( $(v).val() && ytVidId( $(v).val() ) ) {
                  is_empty = false;
              }
          });
          if ( is_empty ){return;}

          var _lB = '<div class="light-box"></div';
          $('body,html').scrollTop(0);
          $('body').append(_lB);
          $('.video-box').css('display','block');
          $('.video-box').fadeIn(1000, 'linear');
          $('.video-box').width($(window).width());
          $('.video-box').height($(window).height());
          $('.video').height($(window).height()-150);
          $('.closevideo').click(function(){
              $('.video-box').css('display','none');
              $('.light-box').remove();
              $('.wrapper-top-thumb' ).html('');
          });

      });
  }
  function viewFullPhoto(){
      $('.fullscreen').click(function(e){

          e.preventDefault();
          var _lB = '<div class="light-box"></div';
          $('body').append(_lB);
          $('.photo-box').css('display','block');
          $('.photo-box').fadeIn(1000, 'linear');
          $('.photo-box').width($(window).width());
          $('.photo-box').height($(window).height());
          $('.photo-box .photo').height($(window).height()-90);
          $('.photo-box .photo' ).css('line-height', $(window).height()-90+ 'px');
          $('.photo-box .photo img' ).css('max-height', $(window).height()-130+ 'px');
          if(!$('.photo-box .photo.selected').length){
              $('.photo-box .photo:first').addClass('selected');
          }

          $('.closevideo').click(function(){
              $('.photo-box').css('display','none');
              $('.light-box').remove();
              $('#slider1prev').remove();
              $('#slider1next').remove();
              $('.wrapper-top-thumb' ).html('');
          });
          // View Photo
          if($(".photo-box").length){
              $(".thumb-slider").easySlider({
                  nextId: "slider1next",
                  prevId: "slider1prev"
              });
              $('.thumb-slider').width(($(window).width()*80)/100);
          }
          if(typeof APL == undefined){
              if(!$('.thumb-slider li a.current').length){
                  $('.thumb-slider li:first a' ).addClass('current');
              }
          }
          $( '.thumb-slider' ).on( 'click', 'a', function(){
              var _currentId = $(this).attr('data-id');
              $('.thumb-slider li a' ).removeClass('current');
              $(this).addClass('current');
              $('.photo-box .photo').removeClass('selected');

              $('.photo-box .photo[data-target='+_currentId+']').addClass('selected');
              $('.photo-box .photo img').css('max-height', $(window).height()-140+ 'px');

          });

          $('#slider1next a').click(function(){
              $('.thumb-slider li a.current').parent().next().find('a').trigger('click');
          });
          $('#slider1prev a').click(function(){
              $('.thumb-slider li a.current').parent().prev().find('a').trigger('click');
          });

          /*var big_img = $( '.photo-box .photo' ),
           big_img_str = '',

           thumb_img = $( '.photo-box .thumb-slider ul' ),
           thumb_img_str = '';

           $.each(big_img, function(k, v) {
           $(v).html('<img src="'+$(v).data().src+'" />');
           });*/
      });
  }
  function addMoreVideo(){
      var _videoNum = $('.video-item').length;
      $('#video-addmore').click(function(e){
          e.preventDefault();

          var _videoItemOb =$('.video-item#video-group').clone();
          var _img = $('#video-addmore').attr('data-img');
          var str = '<div class="delVideo"><i class="fa fa-times"></i><input type="hidden" name="video_deleted[]" value="0" /></div>';

          _videoItemOb.find('.delVideo').remove();

          _videoItemOb.attr('id','video-group'+_videoNum);
          _videoItemOb.append(str);

          if ( $('.delVideo').length > 1 ) {
              _videoItemOb.prepend('<div class="divider"></div>');
          } else {
              _videoItemOb.find('.delVideo').css('top', 0);
          }

          $('.des-list').append( _videoItemOb);
          _videoItemOb.children('.el-blk.full').children('textarea').val('');
          _videoItemOb.children('.el-blk.full').children('input').val('');
          _videoItemOb.children('.el-blk.full').children('input').removeClass( 'inp-error' );
          _videoItemOb.children('.el-blk.full').children('.error').hide();
          _videoItemOb.show();
          _videoItemOb.removeClass('is_hidden');
          _videoNum++;

      });
  }

  function eventClick(){

      // Add OrG
      $('#add-org').click(function(){
          $(this).parent().parent().css('display','none');
          $('.evt-blk.add-co').css('display','none');
          $('.add-organization').css('display','block');
      });
      $('#back-selec-list').click(function(){
          $(this).parent().parent().css('display','none');
          $('.evt-blk.org').css('display','block');
          $('.evt-blk.add-co').css('display','block');
      });
      $('.expend').click(function(){
          $('.cat-list').toggle("swing");
          setTimeout(function(){
              if($('.cat-list').css('display') === 'block'){
                  $('.expend i').removeClass('fa-chevron-circle-down');
                  $('.expend i').addClass('fa-chevron-circle-up');
              }else{
                  $('.expend i').removeClass('fa-chevron-circle-up');
                  $('.expend i').addClass('fa-chevron-circle-down');
              }
          }, 500);
      });
      $('.expend-access').click(function(){
          $('.access-list').toggle("swing");
          setTimeout(function(){
              if($('.access-list').css('display') === 'block'){
                  $('.expend-access i ').removeClass('fa-chevron-circle-down');
                  $('.expend-access i ').addClass('fa-chevron-circle-up');
              }else{
                  $('.expend-access i ').removeClass('fa-chevron-circle-up');
                  $('.expend-access i ').addClass('fa-chevron-circle-down');
              }
          }, 500);
      });
      $('.expend-video').click(function(){
          $('.video-area').toggle("swing");
          setTimeout(function(){
              if($('.video-area').css('display') !== 'none'){
                  $('.expend-video i ').removeClass('fa-chevron-circle-down');
                  $('.expend-video i ').addClass('fa-chevron-circle-up');
              }else{
                  $('.expend-video i ').removeClass('fa-chevron-circle-up');
                  $('.expend-video i ').addClass('fa-chevron-circle-down');
              }
          }, 500);
      });

      // Add Venue
      $('#add-venue').click(function(e){
          e.preventDefault();
          $(this).parent().parent().css('display','none');
          $('.evt-blk.newvenue').css('display','block');
          $('.evt-blk.newvenue').css('margin-top',-25);
      });
      $('#back-venue-list').click(function(e){
          e.preventDefault();
          $(this).parent().parent().css('display','none');
          $('.venue').css('display','block');
      });
      var _num = 1;
      $('#add-co').click(function(e){
          e.preventDefault();
          var _corNum = $('.more-org').length;

          if(_corNum === 4){
              $('#add-co').parent().css('display','none');
          }else{
              $('#add-co').parent().css('display','block');
          }
          if(_corNum<5){
              // $('#add-co').css('margin-bottom',15);
              var _img = $('#add-co').attr('data-img');
              var _selectTitle = $('#add-co').attr('data-title');
              var str = '<div class="more-org"><div class="event-list no-mrn more-orga'+ (_corNum + 1) +'" style="float:left"></div>';
              str = str + '<div class="del"><i class="fa fa-times"></i></div></div>';
              $('.evt-blk.add-co').prepend(str);
              var _selectOb =$('#org-select').clone();
              _selectOb.attr('id','org-select'+ (_corNum + 1) );
              $('.event-list.no-mrn.more-orga'+ (_corNum + 1) ).append(_selectOb);
              _selectOb.val("");
//                $('.event-list.no-mrn.more-orga'+_num).append('<div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>');
              $('#org-select'+ (_corNum + 1) ).find('option:first').text(_selectTitle);

              $('body').trigger('add-more-co', {
                  id: '#org-select'+ (_corNum + 1)
              });
          }
          $('.del').click(function(){
              $(this).parent().remove();
              if($('.more-org').length ===5){
                  $('#add-co').parent().css('display','none');
              }else{
                  $('#add-co').parent().css('display','block');
              }
          });
      });
      if($('.cm').length){
          $('.cm').click(function(){
              var target = $( '#comment' )
              if ( ! $('#comment').length ) {
                  target = $( '.comments-area' )
              }

              $('html,body').animate({ scrollTop: target.offset().top }, 1000);
              return false;
          });
      }
      if($('a.qualification').length){
          $('a.qualification').click(function(e){
              e.preventDefault();
              if($(this).hasClass('active')){
                  $(this).removeClass('active');
              }else{
                  $(this).addClass('active');
              }
          })
      }
      // Delete pdf file
      if($('.pdf-list').length){
          $('.fa-times').click(function(){
              $(this).parent().remove();
          });
      }
  }

  function playVideo(){
      $('.slider li a').click(function(){
          var _currentId = $(this).attr('data-id');
          $('.video').removeClass('selected');
          $('.video[data-target='+_currentId+']').addClass('selected');
      });
  }
  function subTrimMonth(){
      if($(window).width() <= 480){
          var _m = $('.m').text().substring(0,3);
          $('.m').text(_m);
      }
  }
  resizeWindow = function(){
      $('.line-bar').width($('.evt-blk').width()-240);
      if($(".video-box").length){
          $('.thumb-slider').width(($(window).width()*80)/100);
          $('.video-box').width($(window).width());
          $('.video-box').height($(window).height());
          $('.video').height($(window).height()-150);
          $('.thumb-slider').width(($(window).width()*80)/100);

      }
      if($(".photo-box").length){
          $('.thumb-slider').width(($(window).width()*80)/100);
          $('.photo-box').width($(window).width());
          $('.photo-box').height($(window).height());
          $('.photo-box .photo').height($(window).height()-90);
          $('.photo-box .photo' ).css('line-height', $(window).height()-90+ 'px');
          $('.photo-box .photo img' ).css('max-height', $(window).height()-130+ 'px');
          $('.thumb-slider').width(($(window).width()*80)/100);

      }

      if($(window).width() <= 768){
          if($('.search-bkl .blk-paging').length){
              $('.search-bkl .blk-paging').css('display','none');
              $('#load-more').css('display','block');
          }
      }else{
          var _slect = $("input[type='radio']:checked").val();
          if(_slect==="op"){
              $('.search-bkl .blk-paging').css('display','none');
              $('#load-more').css('display','block');
          }else{
              $('.search-bkl .blk-paging').css('display','block');
              $('#load-more').css('display','none');
          }
      }
      if((typeof APL) === undefined){
          if($(window).width() <1024){
              $('.home').removeClass('grid');
          } else if ( APL.config.layout.front_page == 'right_sidebar_two_columns' ){
              $('.home').addClass('grid');
          }
      }
  };

  function closeModal(){
      $('.light-box').click(function(e){
          if($('.box').length){
              $('.closepopup').trigger('click');
          }
          if($('.viewmore-box').length){
              $('.closeviewmore').trigger('click');
          }
          if($('.boxEdit').length){
              $('.closepopup').trigger('click');
          }
      });
  }
  function currentDate(){
      if($('.eventfrom').length){
          var d = new Date();
          var month = d.getMonth()+1;
          var day = d.getDate();
          var output = d.getFullYear() + "-" + ((''+month).length<2 ? '0' : '') + month + "-" + ((''+day).length<2 ? '0' : '') + day;
          $('#c-s').val(output);
          $('#c-e').val(output);
      }
  }
  function checkAccesssibility(){
      if($('.access-list').length){
          $('.ACLlist li img, .ACRlist li img, .ACLlist li label, .ACRlist li label').click(function(){
              if(!$(this).parent().find('>input').is(':checked') ){
                  $(this).parent().find('input').prop('checked','true');
              }else{
                  $(this).parent().find('>input').removeAttr('checked');
              }
          });
      }
      if($('.cat-listing').length){
          $('.Llist li > label, .Rlist li > label').click(function(e){
              if(!$(this).parent().find('>input').is(':checked') ){
                  $(this).parent().find('>input').checked = true;
                  $(this).parent().find('>input').prop('checked','true');
              }else{
                  $(this).parent().find('>input').checked = false;
                  $(this).parent().find('>input').removeAttr('checked');
              }
          });
      }
      $('.free-evt').click(function(){
          if(!$('.free-event').is(':checked') ){
              $('.free-event').prop('checked','true');
          }else{
              $('.free-event').removeAttr('checked');
          }
      });
  }

  function editorCreate(){
      if($('#editor1').length){
          CKEDITOR.replace( 'editor1' );
      }
      if($('#resume').length){
          CKEDITOR.replace( 'resume' );
      }
      if($('#program-des').length){
          CKEDITOR.replace( 'program-des' );
      }
      if($('#available-date').length){
          CKEDITOR.replace( 'available-date' );
      }
      if($('#time').length){
          CKEDITOR.replace( 'time' );
      }
      if($('#space').length){
          CKEDITOR.replace( 'space' );
      }
      if($('#location').length){
          CKEDITOR.replace( 'location' );
      }
      if($('#fee').length){
          CKEDITOR.replace( 'fee' );
      }
      if($('#core').length){
          CKEDITOR.replace( 'core' );
      }
      if($('#program-essen').length){
          CKEDITOR.replace( 'program-essen' );
      }
      if($('#cancel').length){
          CKEDITOR.replace( 'cancel' );
      }
      if($('#refer').length){
          CKEDITOR.replace( 'refer' );
      }
      if($('#venue-des').length){
          CKEDITOR.replace( 'venue-des' );
      }
      if($('#venue-parking').length){
          CKEDITOR.replace( 'venue-parking' );
      }
      if($('#venue-publics').length){
          CKEDITOR.replace( 'venue-public' );
      }
  }
  function arrowClick(){
      $(document).keydown(function(e){
          if($(".photo-box").length){
              // left arrow
              if ((e.keyCode) == 37)
              {
                  $('#slider1prev a').trigger('click');
              }
              // right arrow
              if ((e.keyCode) == 39)
              {
                  $('#slider1next a').trigger('click');
              }
              if ((e.keyCode) == 27)
              {
                  $('.closevideo').trigger('click');
              }
          }
      });
  }
  function showTip(){
      if($('.icons-list').length){
          $('.icons-list span').mouseover(function(e){
              var _parent_top = $(this).parent().offset().top;
              var _child_top = $(this).offset().top;
              var _child_left = $(this).offset().left - $(this).parent().offset().left;
              $(this).find('.ttip').css('top', (_child_top - _parent_top -11));
              $(this).find('.ttip').css('left', (_child_left+13));
              var tipH = $(this).find('.show-tip').height();
              $(this).find('.show-tip').css('top', (_child_top - _parent_top - tipH - 20));
          });
      }
  }
  function backToTop(){
      if ($('#back-to-top').length) {
          $(window).on("scroll", function() {
              var scrollTrigger = 300; // px
              var scrollTop = $(window).scrollTop();
              if (scrollTop > scrollTrigger) {
                  $('#back-to-top').fadeIn(500);
              } else {
                  $('#back-to-top').fadeOut(500);
              }
          });
          $('#back-to-top').on('click', function (e) {
              e.preventDefault();
              $('html,body').animate({
                  scrollTop: 0
              }, 700);
          });
      }
  }
  function featuredEvent(wrapper, moreBtn) {

      var numItem = 0;

      // Get max number of items on top
      var numItemftCol1 = $(wrapper+ ' .item-cat.vertical').first().find('.fea-evt-item.vertical').length,
          numItemftCol2 = $(wrapper+ ' .item-cat.vertical').last().find('.fea-evt-item.vertical').length;
      numItem = Math.max(numItemftCol1, numItemftCol2);

      // Init height
      var heightBetweenTwoItem = parseInt($(wrapper+ ' .item-cat.vertical').first().find('.fea-evt-item.vertical').css('margin-bottom'));
      var numDefaultItem = numItem <= 3 ? numItem :  3;
      var topWrapperHeight = 90;

      // Get height of each item
      var outerHItem = $(wrapper+ ' .fea-evt-item').first().outerHeight();

      // Get the default height
      var defaultHeight = (outerHItem + heightBetweenTwoItem) * numDefaultItem + topWrapperHeight -20;

      var resizeFullVerticleFeaturedEvent = function(height) {
          $(wrapper).css('height', height+ 'px');
      };

      // Count the maximum height of wrapper
      var fullHTop = numItem * (outerHItem + heightBetweenTwoItem) + topWrapperHeight;

      // Hide button if not have more items
      if(fullHTop <= defaultHeight ) { // Three items
          $(moreBtn).hide();
          defaultHeight -= 30; // Remove the space of more btn
      }

      // Init height for wrapper
      if ( $(window).width() <= 768 ) {
          resizeFullVerticleFeaturedEvent(defaultHeight);
      } else {
          $(wrapper).css('height', 'auto');
      }

      // Init resize window
      $( window ).resize(function() {
          // Get new height
          var outerHItem = $(wrapper+ ' .fea-evt-item').first().outerHeight();
          var defaultHeight = (outerHItem + heightBetweenTwoItem) * numDefaultItem + topWrapperHeight -20 ;
          if ( $(window).width() <= 768 ) {
              resizeFullVerticleFeaturedEvent(defaultHeight);
          } else {
              $(wrapper).css('height', 'auto');
          }
      });

      // Init click on view more
      $('.featured-event-blk').on('click', moreBtn, function(e) {
          e.preventDefault();
          if( $(this).attr('is-more') == undefined || $(this).attr('is-more') == "" ){

              $(this).attr('is-more', 1);

              $(this).text($(this).data().viewless);
              var wH = window.innerHeight;

              $(wrapper).css('height', fullHTop + 30 );

              $('html,body').animate({
                  scrollTop: -(wH - $(moreBtn).offset().top ) + 50
              }, 700);

          }else{
              $(this).attr('is-more', "");
              $(this).text($(this).data().viewmore);
              $('html,body').animate({
                  scrollTop: $(wrapper).offset().top
              }, 700);
              $(wrapper).css('height', defaultHeight+ 'px');
          }
      });

  }

  function showTopSearch(){
      if($(window).width() <= 480){

          /* call function to handle show/hide search widget on mobile from global.js */
          try {
              if(typeof $.apl.handleDisplayingSearchWidgetOnMobileVersion !== "undefined"){
                  $.apl.handleDisplayingSearchWidgetOnMobileVersion();
              }
          } catch(e) {
              console.log('init search');
          }

          $('.top-search-row .btn.btn-l.s').click(function(){
              $('.top-search-row .el-blk').toggleClass('displayed');
              $('.top-search-row .s-rw').toggleClass('displayed');
              $('.top-search-row .sub-blk').toggleClass('displayed');
          });
          $('.top-search-row .event-search-custom').on('input propertychange paste', function(){
              if ($(this).val().length != 0){
                  $('.top-search-row .el-blk').addClass('displayed');
                  $('.top-search-row .s-rw').addClass('displayed');
                  $('.top-search-row .sub-blk').addClass('displayed');
              }

          });
      }

  }
  function hasTopBar(){
      if($('#topbar').length){
          $(window).on("scroll", function() {
              var scrollTop = $(window).scrollTop();
              if (scrollTop > 0) {
                  if($('#wpadminbar').length){
                      $('#topbar').css('position','relative');
                  }
                  else {
                      $('#topbar').css('position','fixed');
                  }
              } else {
                  $('#topbar').css('position','relative');
              }
          });
      }
  }

  function initBusinessDetailSlider(){
      if(!$('#business-slide-big').length) return;


      $('#business-slide-nav').flexslider({
          animation: "slide",
          controlNav: false,
          animationLoop: true,
          slideshow: false,
          itemWidth: 140,
          itemMargin: 15,
          asNavFor: '#business-slide-big'
      });

      $('#business-slide-big').flexslider({
          animation: "slide",
          controlNav: false,
          animationLoop: true,
          slideshow: false,
          sync: "#business-slide-nav"
      });

  }
  /* ----------------------------------------------- */
  /* ----------------------------------------------- */
  //TriLM: Add loading img slider
  jQuery(window).load(function(){
      $('.main-slider-overlay').hide();
  });
  /* OnLoad Page */
  jQuery(document).ready(function($){
      var hamburgerMenu = $('.hamburger-menu');
      hamburgerMenu.find('.hamburger-btn').on('click', function () {
          $('body').toggleClass('toggleSideBar');
      });
      hamburgerMenu.find('.container ul.nav > .has-child').on('click', function () {
          $(this).toggleClass('expand');
      });

      hamburgerMenu.find('.backdrop').on('click', function () {
          $('body').removeClass('toggleSideBar');
      });

      showSubMenu('.main-menu.pc-show .mn-menu');
      // 1. DETAIL PAGE
      expBussList ();
      // 3. Dashboard
      expDSBMenu('.dsb-main-nav a.has-child');
      menuTwoTiers();
      changeTab();
      fixTab();
      tellAfriend();
      checkAll();
      checkAccesssibility();
      showTip();
      showTopSearch();
      $('.line-bar').width($('.evt-blk').width()-250);
      //TriLM: modify code for loading img slider
      if($('.main-slider').length){
          $('.main-slider').css('opacity',1);
          setTimeout(function(){
              //$('.loading').css('top', $('.main-slider').offset().top + $('.main-slider').height()/2);
              $('.loading').css('display','block');
          },100);
      }
  });
  eventClick();
  viewVideo();
  uploadFile();
  addMoreVideo();
  subTrimMonth();
  closeModal();
  pagingSelect();
  viewFullPhoto();
  backToTop();
  hasTopBar();

  $('.thumb-slider' ).on( 'click', 'a', function(){
      var _currentId = $(this).attr('data-id');
      $('.thumb-slider li').removeClass('current');
      $(this).parent().addClass('current');
      $('.video').removeClass('selected');
      var currentVideo = $('.wrapper-top-thumb div[data-target="'+_currentId+'"]'),
          src = currentVideo.length ? currentVideo.data().src : '';
      $('.wrapper-top-thumb iframe' ).attr('src', '');
      currentVideo.children('iframe').attr('src', src );
      $('.video[data-target='+_currentId+']').addClass('selected');
  });
  // The slider being synced must be initialized first
  if(typeof APL ==undefined){
      if($(".video-box").length){
          $(".thumb-slider").easySlider({
              continuous: false,
              nextId: "slider1next",
              prevId: "slider1prev"
          });
          $('.thumb-slider').width(($(window).width()*80)/100);
      }
  }
  if($('.second-slider').length){
      $('.second-slider .flexslider').css('opacity',0);
      $('.loader').css('display','block');
      $('.loader').css('position','absolute');
      $('.loader').css('top','40%');
      $('.loader').css('left','46%');
      setTimeout(function(){
          $('.loader').fadeOut(function(){
              $('.second-slider .flexslider').animate({opacity:1});
          });
      }, 1000);
  }
  if($('.a-block-ct-video').length){
      $('.video-inner').css('opacity',0);
      $('.vd-loader').css('position','absolute');
      $('.vd-loader').css('top','34%');
      $('.vd-loader').css('left','46%');
      $('.vd-loader').css('display','block');
      $('.vd-loader').animate({opacity:1});
      setTimeout(function(){
          $('.vd-loader').fadeOut(function(){
              $('.video-inner').animate({opacity:1});
          });
      }, 2000);
  }
  if($('.a-block-ct-photo').length){
      $('.photo-inner').css('opacity',0);
      $('.loader').css('position','absolute');
      $('.loader').css('top','46%');
      $('.loader').css('left','46%');
      $('.loader').css('display','block');
      $('.loader').animate({opacity:1});
      setTimeout(function(){
          $('.loader').fadeOut(function(){
              $('.bxslider li').css('display','none');
              $('.bxslider li:first').css('display','block');
              $('.photo-inner').animate({opacity:1});
          });
      }, 3000);
  }

  var slider = $('.bxslider').bxSlider({
      pagerCustom: '#bx-pager',
      controls:'false',
      mode: 'fade'
  });

  $('#bx-pager').bxSlider({
      slideWidth: 80,
      minSlides: 1,
      maxSlides: 8,
      slideMargin:5,
      wrapperClass: 'pbx-wrapper'
  });

  $('.video-block').bxSlider({
      adaptiveHeight: true,
      pagerCustom:'#video-pager',
      controls:'false',
      mode: 'fade'

  });
  $('#video-pager').bxSlider({
      slideWidth: 80,
      minSlides: 1,
      maxSlides: 8,
      slideMargin:5,
      wrapperClass: 'pbx-wrapper',
  });
  /* OnLoad Window */
  var init = function () {
      runMainSlider('.main-slider .flexslider');
      runMainSlider('.wg-slider .flexslider');
      runMainSlider('.second-slider .flexslider');
      runCalendarIpt('c-s');
      runCalendarIpt('c-e');

      runCalendarIpt('top-c-s');
      runCalendarIpt('top-c-e');

      showMBMenu('.mb-menu');
      if($('.main-slider').length){
          sliderFix();
      }
      editorCreate();
      arrowClick();
      resizeWindow();

      featuredEvent('.wrapper-l', '#evt-top-blk');
      featuredEvent('.wrapper-r', '#evt-bottom-blk');

  };
  $(window).on('load', init);
  /* RESIZE Window */
  $(window).resize($.debounce(300,
      function(){
          fixTab();
          if($('.main-slider').length){
              sliderFix();
          }
          subTrimMonth();
          resizeWindow();
      }
  ));

  initBusinessDetailSlider();


})(jQuery);

