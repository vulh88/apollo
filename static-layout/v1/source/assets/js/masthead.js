function initMastHeadMobile($) {
  /** Update new header style custom on moble */
  function duplicateTopHeadOnMobile() {
    if ($("body.oct__hamburger-mobile").length) {
      $("body.oct__hamburger-mobile").find(".header .top-head").addClass("top-head__oct-mobile");
      if ($(".top-head.top-head__oct-mobile").length) {
        $(".top-head.top-head__oct-mobile").ready(function() {
          var $coreTopHead  = $(".header .top-head.top-head__oct-mobile");
          var $logoHead     = $coreTopHead.find(".logo");
          var $topHeadClone = $coreTopHead
            .clone()
            .removeClass("top-head__oct-mobile");
          var $newTopHeadClone = $topHeadClone.addClass("top-head__custom-mobile");
          // Add bars and search icon
          $logoHead.append(
            '<a href="#" class="btn-bars__oct"><i class="fa fa-bars"></i></a>'
          );
          $logoHead.append(
            '<a href="#" class="btn-search__oct"><i class="fa fa-search"></i></a>'
          );
          $newTopHeadClone.append(
            '<a href="#" class="btn-closed-bars__oct">X</a>'
          );
          $coreTopHead.closest("body").append('<div class="overlay__oct-mobile"></div>');
          if ($newTopHeadClone.length) {
            $topHeadClone.appendTo("header.header>.inner");
          }
        });
      }
    }
  }

  function topHeadActionMobile() {
    if ($(".top-head.top-head__oct-mobile").length) {
      $(".top-head.top-head__oct-mobile").ready(function() {
        var $wrapperHeader    = $(".header .top-head.top-head__oct-mobile"),
            $wrapperLogo      = $wrapperHeader.find(".logo"),
            $btnSidebar       = $wrapperLogo.find(".btn-bars__oct"),
            $btnSearch        = $wrapperLogo.find(".btn-search__oct"),
            $wrapperSearchBox = $wrapperHeader.find(".top-blk.search-box"),
            $wrapperSidebar   = $wrapperHeader.siblings(".top-head.top-head__custom-mobile"),
            $btnCloseSidebar  = $wrapperSidebar.find(".btn-closed-bars__oct"),
            $bodyOverlay      = $("body").find(".overlay__oct-mobile"),
            $menuItemSidebar  = $wrapperSidebar.find(".mn-menu.has-toogle__menu-child .nav > li.has-child > a");

        // Append icon carret
        $btnSidebar.on("click", function(event) {
          event.stopPropagation();
          event.preventDefault();
          $wrapperSidebar.toggleClass("show-oct__custom-sidebar");
          $bodyOverlay.toggleClass("active-overlay");
          if ($wrapperSearchBox.hasClass("show-search__box-mobile")) {
            $wrapperSearchBox.removeClass("show-search__box-mobile");
          }
          if ($wrapperHeader.find(".mb-menu.tablet-show").hasClass("active")) {
            $wrapperHeader.find(".mb-menu.tablet-show").removeClass("active");
          }
          if ($wrapperHeader.find(".main-menu.tablet-show").hasClass("show")) {
            $wrapperHeader.find(".main-menu.tablet-show").removeClass("show");
          }
          if ($("body.oct__show-menu-item").length) {
            $("body.oct__show-menu-item.on-mobile .header .top-head.top-head__oct-mobile .mn-menu ul.nav li.has-child").find(".active-show--child").removeClass("active-show--child");
            $("body.oct__show-menu-item.on-mobile .header .top-head.top-head__oct-mobile .mn-menu ul.nav li.has-child").find(".show-full-sub__child").removeClass("show-full-sub__child");
          }
        });

        $btnCloseSidebar.on("click", function(event) {
          event.stopPropagation();
          event.preventDefault();
          $wrapperSidebar.removeClass("show-oct__custom-sidebar");
          $bodyOverlay.removeClass("active-overlay");
          $menuItemSidebar.closest("li").find(".active__icon").removeClass("active__icon");
          $menuItemSidebar.closest("li").find(".active__sub-menu").removeClass("active__sub-menu");
        });

        $btnSearch.on("click", function(event) {
          event.stopPropagation();
          event.preventDefault();
          $wrapperSearchBox.toggleClass("show-search__box-mobile");
          if ($wrapperHeader.find(".mb-menu.tablet-show").hasClass("active")) {
            $wrapperHeader.find(".mb-menu.tablet-show").removeClass("active");
          }
          if ($wrapperHeader.find(".main-menu.tablet-show").hasClass("show")) {
            $wrapperHeader.find(".main-menu.tablet-show").removeClass("show");
          }
          if ($("body.oct__show-menu-item").length) {
            $("body.oct__show-menu-item.on-mobile .header .top-head.top-head__oct-mobile .mn-menu ul.nav li.has-child").find(".active-show--child").removeClass("active-show--child");
            $("body.oct__show-menu-item.on-mobile .header .top-head.top-head__oct-mobile .mn-menu ul.nav li.has-child").find(".show-full-sub__child").removeClass("show-full-sub__child");
          }
        });

        // Click overlay_body
        $bodyOverlay.on("click", function() {
          $bodyOverlay.removeClass("active-overlay");
          $wrapperSidebar.removeClass("show-oct__custom-sidebar");
        });

        // Click out search box
        $(document).on("click", function(e) {
          if (
            $(e.target).is(".top-blk.search-box.show-search__box-mobile") ||
            $(e.target).is(".top-blk.search-box.show-search__box-mobile *") ||
            $(e.target).is(".top-head.top-head__custom-mobile .mn-menu li.has-child>a") ||
            $(e.target).is(".top-head.top-head__custom-mobile .mn-menu li.has-child>a * ")) { return; }
          $wrapperSearchBox.removeClass("show-search__box-mobile");
          $menuItemSidebar.closest("li").find(".active__icon").removeClass("active__icon");
          $menuItemSidebar.closest("li").find(".active__sub-menu").removeClass("active__sub-menu");
        });

        var $menuActiveItemChild = $("body.oct__show-menu-item.on-mobile .header .top-head.top-head__oct-mobile .mn-menu ul.nav > li.has-child > a");
        if ($menuActiveItemChild.length > 0) {
          $menuActiveItemChild.off("click").on("click", function(event) {
            event.stopPropagation();
            event.preventDefault();
            var $currentItem = $(event.currentTarget);
            var $parentUl    = $currentItem.closest("ul.nav");
            var $parentLi    = $currentItem.closest("li");
            if ($currentItem.hasClass("active-show--child")) {
              $currentItem.removeClass("active-show--child");
              $parentLi.find(".show-full-sub__child").removeClass("show-full-sub__child");
            } else {
              $parentUl.find(".active-show--child").removeClass("active-show--child");
              $parentUl.find(".show-full-sub__child").removeClass("show-full-sub__child");
              $currentItem.addClass("active-show--child");
              $parentLi.find("ul").addClass("show-full-sub__child");
            }
          });
        }

        // Click out
        $(document).on("click", function(e) {
          if (
            $(e.target).is("body.oct__show-menu-item.on-mobile .header .top-head.top-head__oct-mobile .mn-menu li.has-child .active-show--child")) { return; }
            $("body.oct__show-menu-item.on-mobile .header .top-head.top-head__oct-mobile .mn-menu ul.nav li.has-child").find(".active-show--child").removeClass("active-show--child");
            $("body.oct__show-menu-item.on-mobile .header .top-head.top-head__oct-mobile .mn-menu ul.nav li.has-child").find(".show-full-sub__child").removeClass("show-full-sub__child");
          });

        // Click menuItem child
        if ($menuItemSidebar.length > 0) {
          $menuItemSidebar.on("click", function(event) {
            event.stopPropagation();
            event.preventDefault();
            var $closestLiItem = this.closest("li");
            $closestLiItem.children[0].classList.toggle("active__icon");
            $closestLiItem.children[1].classList.toggle("active__sub-menu");
          });
        }
      });
    }
  }

  // Add title hover icon
  function addAttrOctTitle() {
    if($("body.header-bar--right .header .top-head").length) {
      var $bodyHead = $("body.header-bar--right .header .top-head");
      var $itemHasData = $bodyHead.find('[data-oct-title]');

      if($itemHasData.length) {
        $itemHasData.each( function() {
          var self = $(this);
          var selfDataValue = self.data("oct-title");
          self.attr('title', selfDataValue);
        })
      }
    }
  }

  duplicateTopHeadOnMobile();
  topHeadActionMobile();
  addAttrOctTitle();
}
(function($) {
  $(document).ready(function() {
    initMastHeadMobile($);
  });
})(jQuery);
