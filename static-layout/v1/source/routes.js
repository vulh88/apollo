var find = require('find');
var path = require('path');
var viewDir = path.join(__dirname, './views');

exports.index = function(req, res) {
  res.render('index');
};

exports.contact = function(req, res) {
  res.render('contact');
};

exports.cate = function(req, res) {
  res.render('cate');
};

exports.html = function(req, res) {
  var pageName = req.params.pageName;
  find.file(pageName + '.jade', viewDir, function(files) {
    files = files.filter(function(e) {
      return e.indexOf('/layouts/') === -1;
    });
    if (files && files[0]) {
      res.render(files[0], {
        isCategory: undefined,
        isClient: undefined,
        isPageNotFound: undefined,
      });
    } else {
      res.send(404);
    }
  });
};
