// Default modules
var express = require('express');
var path = require('path');
var routes = require('./routes');
var app = express();

// Default configuration
app.set('port', process.env.PORT || 3010);
app.set('views', path.join(__dirname, '/views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('frontend'));

//app.use(express.static(path.join(__dirname, 'assets')));
app.use(express.static(path.join(__dirname, '../public')));
app.use(express.static(path.join(__dirname, '../public/assets')));
app.locals.pretty = false;

app.use(app.router);

// Development configuration
if ('development' === app.get('env')) {
  app.use(express.errorHandler({
    dumpExceptions: true,
    showStack: true
  }));
  app.locals.pretty = true;
}

// Routes
app.get('/', routes.index);
app.get('/index.html', routes.index);
app.get('/:pageName.html', routes.html);


// Initialization
app.listen(app.get('port'), function() {
  console.log('Frontend template is running on port ' + app.get('port'));
});