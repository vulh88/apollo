'use strict';

/**
 * ALL PAGE
 * 1. Run slider
 */

var allpage_js = {};
(function ($) {

    /*1. Run slider */
    allpage_js = {
        runSlider: function runSlider(main_slider, controlClass, startCb, afterCb) {
            if (!$(main_slider).length) {
                return;
            }
            $(main_slider).flexslider({
                animation: 'slide',
                controlNav: true,
                directionNav: true,
                manualControls: controlClass,
                customDirectionNav: $(".custom-navigation a"),
                useCSS: false,
                start: function start(slide) {
                    if (typeof startCb === 'function') {
                        startCb($(slide).find('.flex-active-slide >div'));
                    }
                },
                after: function after(slide) {
                    if (typeof afterCb === 'function') {
                        afterCb($(slide).find('.flex-active-slide >div'));
                    }
                }
            });
        },
        bottomSlider: function bottomSlider(itemName) {
            $(itemName).flexslider({
                animation: "slide",
                animationLoop: false,
                controlNav: true,
                directionNav: true,
                customDirectionNav: $(".cust-nav a"),
                itemWidth: 315,
                minItems: 4,
                maxItems: 4
            });
        }
    };

    $(document).ready(function ($) {
        // Run slider
        function changeColor(activeBg) {
            var prvBtn = $('.custom-navigation .sli-icon.icon-prev');
            var nextBtn = $('.custom-navigation .sli-icon.icon-next');
            var nextColor = activeBg.attr('data-color-next-button');
            var prvColor = activeBg.attr('data-color-prev-button');
            prvBtn.css({ 'background-color': prvColor });
            nextBtn.css({ 'background-color': nextColor });
        };
        allpage_js.runSlider('.main-slider', '.flex-control-nav li', function (activeBg) {
            changeColor(activeBg);
        }, function (activeBg) {
            changeColor(activeBg);
        });
        // Run bottom slider
        $('.bottom-slider').flexslider({
            animation: "slide",
            animationLoop: false,
            controlNav: false,
            directionNav: true,
            customDirectionNav: $(".cust-nav a"),
            itemWidth: 315,
            itemMargin: 10,
            minItems: 1,
            maxItems: 4
        });;
    });
    /* OnLoad Window */
    var init = function init() {};
    $(document).on('click', '.hamburger-menu', function () {
        $('body').toggleClass('menu-open');
    });
    window.onload = init;
})(jQuery);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFjdGlvbnMvYWxscGFnZXMuanMiXSwibmFtZXMiOlsiYWxscGFnZV9qcyIsIiQiLCJydW5TbGlkZXIiLCJtYWluX3NsaWRlciIsImNvbnRyb2xDbGFzcyIsInN0YXJ0Q2IiLCJhZnRlckNiIiwibGVuZ3RoIiwiZmxleHNsaWRlciIsImFuaW1hdGlvbiIsImNvbnRyb2xOYXYiLCJkaXJlY3Rpb25OYXYiLCJtYW51YWxDb250cm9scyIsImN1c3RvbURpcmVjdGlvbk5hdiIsInVzZUNTUyIsInN0YXJ0Iiwic2xpZGUiLCJmaW5kIiwiYWZ0ZXIiLCJib3R0b21TbGlkZXIiLCJpdGVtTmFtZSIsImFuaW1hdGlvbkxvb3AiLCJpdGVtV2lkdGgiLCJtaW5JdGVtcyIsIm1heEl0ZW1zIiwiZG9jdW1lbnQiLCJyZWFkeSIsImNoYW5nZUNvbG9yIiwiYWN0aXZlQmciLCJwcnZCdG4iLCJuZXh0QnRuIiwibmV4dENvbG9yIiwiYXR0ciIsInBydkNvbG9yIiwiY3NzIiwiaXRlbU1hcmdpbiIsImluaXQiLCJvbiIsInRvZ2dsZUNsYXNzIiwid2luZG93Iiwib25sb2FkIiwialF1ZXJ5Il0sIm1hcHBpbmdzIjoiOztBQUFBOzs7OztBQUtBLElBQUlBLGFBQWEsRUFBakI7QUFDQSxDQUFDLFVBQVVDLENBQVYsRUFBYzs7QUFFWDtBQUNBRCxpQkFBYTtBQUNURSxtQkFBVyxtQkFBU0MsV0FBVCxFQUFzQkMsWUFBdEIsRUFBb0NDLE9BQXBDLEVBQTZDQyxPQUE3QyxFQUFzRDtBQUM3RCxnQkFBRyxDQUFDTCxFQUFFRSxXQUFGLEVBQWVJLE1BQW5CLEVBQTJCO0FBQUU7QUFBUztBQUN0Q04sY0FBRUUsV0FBRixFQUFlSyxVQUFmLENBQTBCO0FBQ3RCQywyQkFBVyxPQURXO0FBRXRCQyw0QkFBWSxJQUZVO0FBR3RCQyw4QkFBYyxJQUhRO0FBSXRCQyxnQ0FBZ0JSLFlBSk07QUFLdEJTLG9DQUFvQlosRUFBRSxzQkFBRixDQUxFO0FBTXRCYSx3QkFBUSxLQU5jO0FBT3RCQyx1QkFBTyxlQUFTQyxLQUFULEVBQWdCO0FBQ25CLHdCQUFHLE9BQU9YLE9BQVAsS0FBbUIsVUFBdEIsRUFBa0M7QUFDOUJBLGdDQUFRSixFQUFFZSxLQUFGLEVBQVNDLElBQVQsQ0FBYyx5QkFBZCxDQUFSO0FBQ0g7QUFDSixpQkFYcUI7QUFZdEJDLHVCQUFPLGVBQVNGLEtBQVQsRUFBZ0I7QUFDbkIsd0JBQUcsT0FBT1YsT0FBUCxLQUFtQixVQUF0QixFQUFrQztBQUM5QkEsZ0NBQVFMLEVBQUVlLEtBQUYsRUFBU0MsSUFBVCxDQUFjLHlCQUFkLENBQVI7QUFDSDtBQUNKO0FBaEJxQixhQUExQjtBQWtCSCxTQXJCUTtBQXNCVEUsc0JBQWMsc0JBQVNDLFFBQVQsRUFBa0I7QUFDNUJuQixjQUFFbUIsUUFBRixFQUFZWixVQUFaLENBQXVCO0FBQ25CQywyQkFBVyxPQURRO0FBRW5CWSwrQkFBZSxLQUZJO0FBR25CWCw0QkFBWSxJQUhPO0FBSW5CQyw4QkFBYyxJQUpLO0FBS25CRSxvQ0FBb0JaLEVBQUUsYUFBRixDQUxEO0FBTW5CcUIsMkJBQVcsR0FOUTtBQU9uQkMsMEJBQVUsQ0FQUztBQVFuQkMsMEJBQVU7QUFSUyxhQUF2QjtBQVVIO0FBakNRLEtBQWI7O0FBcUNBdkIsTUFBRXdCLFFBQUYsRUFBWUMsS0FBWixDQUFrQixVQUFTekIsQ0FBVCxFQUFXO0FBQ3pCO0FBQ0EsaUJBQVMwQixXQUFULENBQXFCQyxRQUFyQixFQUErQjtBQUMzQixnQkFBSUMsU0FBUzVCLEVBQUUsd0NBQUYsQ0FBYjtBQUNBLGdCQUFJNkIsVUFBVTdCLEVBQUUsd0NBQUYsQ0FBZDtBQUNBLGdCQUFJOEIsWUFBWUgsU0FBU0ksSUFBVCxDQUFjLHdCQUFkLENBQWhCO0FBQ0EsZ0JBQUlDLFdBQVdMLFNBQVNJLElBQVQsQ0FBYyx3QkFBZCxDQUFmO0FBQ0FILG1CQUFPSyxHQUFQLENBQVcsRUFBQyxvQkFBb0JELFFBQXJCLEVBQVg7QUFDQUgsb0JBQVFJLEdBQVIsQ0FBWSxFQUFDLG9CQUFvQkgsU0FBckIsRUFBWjtBQUNIO0FBQ0QvQixtQkFBV0UsU0FBWCxDQUFxQixjQUFyQixFQUFxQyxzQkFBckMsRUFDQSxVQUFTMEIsUUFBVCxFQUFtQjtBQUFDRCx3QkFBWUMsUUFBWjtBQUF1QixTQUQzQyxFQUVBLFVBQVNBLFFBQVQsRUFBbUI7QUFBQ0Qsd0JBQVlDLFFBQVo7QUFBdUIsU0FGM0M7QUFJQTtBQUNDM0IsVUFBRSxnQkFBRixFQUFvQk8sVUFBcEIsQ0FBK0I7QUFDeEJDLHVCQUFXLE9BRGE7QUFFeEJZLDJCQUFlLEtBRlM7QUFHeEJYLHdCQUFZLEtBSFk7QUFJeEJDLDBCQUFjLElBSlU7QUFLeEJFLGdDQUFvQlosRUFBRSxhQUFGLENBTEk7QUFNeEJxQix1QkFBVyxHQU5hO0FBT3hCYSx3QkFBWSxFQVBZO0FBUXhCWixzQkFBVSxDQVJjO0FBU3hCQyxzQkFBVTtBQVRjLFNBQS9CLEVBVU07QUFFVixLQTNCRDtBQTRCQTtBQUNBLFFBQUlZLE9BQU8sU0FBUEEsSUFBTyxHQUFZLENBRXRCLENBRkQ7QUFHQW5DLE1BQUV3QixRQUFGLEVBQVlZLEVBQVosQ0FBZSxPQUFmLEVBQXdCLGlCQUF4QixFQUEyQyxZQUFZO0FBQ25EcEMsVUFBRSxNQUFGLEVBQVVxQyxXQUFWLENBQXNCLFdBQXRCO0FBQ0gsS0FGRDtBQUdBQyxXQUFPQyxNQUFQLEdBQWdCSixJQUFoQjtBQUNILENBNUVELEVBNEVHSyxNQTVFSCIsImZpbGUiOiJhY3Rpb25zL2FsbHBhZ2VzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXHJcbiAqIEFMTCBQQUdFXHJcbiAqIDEuIFJ1biBzbGlkZXJcclxuICovXHJcblxyXG52YXIgYWxscGFnZV9qcyA9IHt9O1xyXG4oZnVuY3Rpb24oICQgKSB7IFxyXG4gIFxyXG4gICAgLyoxLiBSdW4gc2xpZGVyICovXHJcbiAgICBhbGxwYWdlX2pzID0ge1xyXG4gICAgICAgIHJ1blNsaWRlcjogZnVuY3Rpb24obWFpbl9zbGlkZXIsIGNvbnRyb2xDbGFzcywgc3RhcnRDYiwgYWZ0ZXJDYikge1xyXG4gICAgICAgICAgICBpZighJChtYWluX3NsaWRlcikubGVuZ3RoKSB7IHJldHVybjsgfVxyXG4gICAgICAgICAgICAkKG1haW5fc2xpZGVyKS5mbGV4c2xpZGVyKHtcclxuICAgICAgICAgICAgICAgIGFuaW1hdGlvbjogJ3NsaWRlJyxcclxuICAgICAgICAgICAgICAgIGNvbnRyb2xOYXY6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBkaXJlY3Rpb25OYXY6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBtYW51YWxDb250cm9sczogY29udHJvbENsYXNzLFxyXG4gICAgICAgICAgICAgICAgY3VzdG9tRGlyZWN0aW9uTmF2OiAkKFwiLmN1c3RvbS1uYXZpZ2F0aW9uIGFcIiksXHJcbiAgICAgICAgICAgICAgICB1c2VDU1M6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgc3RhcnQ6IGZ1bmN0aW9uKHNsaWRlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYodHlwZW9mIHN0YXJ0Q2IgPT09ICdmdW5jdGlvbicpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc3RhcnRDYigkKHNsaWRlKS5maW5kKCcuZmxleC1hY3RpdmUtc2xpZGUgPmRpdicpKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgYWZ0ZXI6IGZ1bmN0aW9uKHNsaWRlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYodHlwZW9mIGFmdGVyQ2IgPT09ICdmdW5jdGlvbicpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWZ0ZXJDYigkKHNsaWRlKS5maW5kKCcuZmxleC1hY3RpdmUtc2xpZGUgPmRpdicpKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pIFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgYm90dG9tU2xpZGVyOiBmdW5jdGlvbihpdGVtTmFtZSl7XHJcbiAgICAgICAgICAgICQoaXRlbU5hbWUpLmZsZXhzbGlkZXIoe1xyXG4gICAgICAgICAgICAgICAgYW5pbWF0aW9uOiBcInNsaWRlXCIsXHJcbiAgICAgICAgICAgICAgICBhbmltYXRpb25Mb29wOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIGNvbnRyb2xOYXY6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBkaXJlY3Rpb25OYXY6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBjdXN0b21EaXJlY3Rpb25OYXY6ICQoXCIuY3VzdC1uYXYgYVwiKSxcclxuICAgICAgICAgICAgICAgIGl0ZW1XaWR0aDogMzE1LFxyXG4gICAgICAgICAgICAgICAgbWluSXRlbXM6IDQsXHJcbiAgICAgICAgICAgICAgICBtYXhJdGVtczogNFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuXHJcbiAgICAkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigkKXtcclxuICAgICAgICAvLyBSdW4gc2xpZGVyXHJcbiAgICAgICAgZnVuY3Rpb24gY2hhbmdlQ29sb3IoYWN0aXZlQmcpIHtcclxuICAgICAgICAgICAgdmFyIHBydkJ0biA9ICQoJy5jdXN0b20tbmF2aWdhdGlvbiAuc2xpLWljb24uaWNvbi1wcmV2Jyk7XHJcbiAgICAgICAgICAgIHZhciBuZXh0QnRuID0gJCgnLmN1c3RvbS1uYXZpZ2F0aW9uIC5zbGktaWNvbi5pY29uLW5leHQnKTtcclxuICAgICAgICAgICAgdmFyIG5leHRDb2xvciA9IGFjdGl2ZUJnLmF0dHIoJ2RhdGEtY29sb3ItbmV4dC1idXR0b24nKTtcclxuICAgICAgICAgICAgdmFyIHBydkNvbG9yID0gYWN0aXZlQmcuYXR0cignZGF0YS1jb2xvci1wcmV2LWJ1dHRvbicpO1xyXG4gICAgICAgICAgICBwcnZCdG4uY3NzKHsnYmFja2dyb3VuZC1jb2xvcic6IHBydkNvbG9yfSk7XHJcbiAgICAgICAgICAgIG5leHRCdG4uY3NzKHsnYmFja2dyb3VuZC1jb2xvcic6IG5leHRDb2xvcn0pO1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgYWxscGFnZV9qcy5ydW5TbGlkZXIoJy5tYWluLXNsaWRlcicsICcuZmxleC1jb250cm9sLW5hdiBsaScsIFxyXG4gICAgICAgIGZ1bmN0aW9uKGFjdGl2ZUJnKSB7Y2hhbmdlQ29sb3IoYWN0aXZlQmcpO30sXHJcbiAgICAgICAgZnVuY3Rpb24oYWN0aXZlQmcpIHtjaGFuZ2VDb2xvcihhY3RpdmVCZyk7fVxyXG4gICAgICAgICk7XHJcbiAgICAgICAgLy8gUnVuIGJvdHRvbSBzbGlkZXJcclxuICAgICAgICAgJCgnLmJvdHRvbS1zbGlkZXInKS5mbGV4c2xpZGVyKHtcclxuICAgICAgICAgICAgICAgIGFuaW1hdGlvbjogXCJzbGlkZVwiLFxyXG4gICAgICAgICAgICAgICAgYW5pbWF0aW9uTG9vcDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBjb250cm9sTmF2OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIGRpcmVjdGlvbk5hdjogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGN1c3RvbURpcmVjdGlvbk5hdjogJChcIi5jdXN0LW5hdiBhXCIpLFxyXG4gICAgICAgICAgICAgICAgaXRlbVdpZHRoOiAzMTUsXHJcbiAgICAgICAgICAgICAgICBpdGVtTWFyZ2luOiAxMCxcclxuICAgICAgICAgICAgICAgIG1pbkl0ZW1zOiAxLFxyXG4gICAgICAgICAgICAgICAgbWF4SXRlbXM6IDRcclxuICAgICAgICAgICAgfSk7O1xyXG4gICAgICAgICBcclxuICAgIH0pO1xyXG4gICAgLyogT25Mb2FkIFdpbmRvdyAqL1xyXG4gICAgdmFyIGluaXQgPSBmdW5jdGlvbiAoKSB7ICAgXHJcbiAgIFxyXG4gICAgfTtcclxuICAgICQoZG9jdW1lbnQpLm9uKCdjbGljaycsICcuaGFtYnVyZ2VyLW1lbnUnLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgJCgnYm9keScpLnRvZ2dsZUNsYXNzKCdtZW51LW9wZW4nKTtcclxuICAgIH0pO1xyXG4gICAgd2luZG93Lm9ubG9hZCA9IGluaXQ7XHJcbn0pKGpRdWVyeSk7Il19
