/**
 * ALL PAGE
 * 1. Run slider
 */

var allpage_js = {};
(function( $ ) { 
  
    /*1. Run slider */
    allpage_js = {
        runSlider: function(main_slider, beforeCb, afterCb) {
            if(!$(main_slider).length) { return; }
            $(main_slider).flexslider({
                animation: 'slide',
                controlNav: false,
                directionNav: true,
                customDirectionNav: $(".custom-navigation a"),
                before: function(slide) {
                    if(typeof beforeCb === 'function') {
                        beforeCb($(slide).find('.flex-active-slide >div').attr('class'));
                    }
                },
                after: function(slide) {
                    if(typeof afterCb === 'function') {
                        afterCb($(slide).find('.flex-active-slide >div').attr('class'));
                    }
                }
            }) 
        },
        bottomSlider: function(itemName){
            $(itemName).flexslider({
                animation: "slide",
                animationLoop: false,
                controlNav: false,
                directionNav: true,
                customDirectionNav: $(".cust-nav a"),
                itemWidth: 315,
                minItems: 4,
                maxItems: 4
            });
        }
    };


    $(document).ready(function($){
        // Run slider
        allpage_js.runSlider('.main-slider', null , function(activeBg) {
            $current_active = activeBg;
            var prvBtn = $('.custom-navigation .sli-icon.icon-prev');
            var nextBtn = $('.custom-navigation .sli-icon.icon-next');
            var prvClass = 'sli-icon icon-prev';
            var nextClass = 'sli-icon icon-next';
            switch(activeBg) {
                case 'slider-item bg-green' :
                    prvClass += ' bg-icon-red';
                    nextClass += ' bg-icon-blue';
                    break; 
                case 'slider-item bg-blue':
                    prvClass += ' bg-icon-red';
                    nextClass += ' bg-icon-green';
                    break;
                case 'slider-item bg-red':
                    prvClass += ' bg-icon-slive';
                    nextClass += ' bg-icon-blue';
                    break;
                default: 
                    prvClass += ' bg-icon-gray';
                    nextClass += ' bg-icon-gray';
            }
            prvBtn.attr({class: prvClass});
            nextBtn.attr({class: nextClass});
        });
        // Run bottom slider
         $('.bottom-slider').flexslider({
                animation: "slide",
                animationLoop: false,
                controlNav: false,
                directionNav: true,
                customDirectionNav: $(".cust-nav a"),
                itemWidth: 315,
                itemMargin: 10,
                minItems: 4,
                maxItems: 4
            });;
         
    });
    /* OnLoad Window */
    var init = function () {   

    };
    window.onload = init;
})(jQuery);