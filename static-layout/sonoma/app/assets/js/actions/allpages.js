/**
 * ALL PAGE
 * 1. Run slider
 */

var allpage_js = {};
(function( $ ) { 
  
    /*1. Run slider */
    allpage_js = {
        runSlider: function(main_slider, controlClass, startCb, afterCb) {
            if(!$(main_slider).length) { return; }
            $(main_slider).flexslider({
                animation: 'slide',
                controlNav: true,
                directionNav: true,
                manualControls: controlClass,
                customDirectionNav: $(".custom-navigation a"),
                useCSS: false,
                start: function(slide) {
                    if(typeof startCb === 'function') {
                        startCb($(slide).find('.flex-active-slide >div'));
                    }
                },
                after: function(slide) {
                    if(typeof afterCb === 'function') {
                        afterCb($(slide).find('.flex-active-slide >div'));
                    }
                }
            }) 
        },
        bottomSlider: function(itemName){
            $(itemName).flexslider({
                animation: "slide",
                animationLoop: false,
                controlNav: true,
                directionNav: true,
                customDirectionNav: $(".cust-nav a"),
                itemWidth: 315,
                minItems: 4,
                maxItems: 4
            });
        }
    };


    $(document).ready(function($){
        // Run slider
        function changeColor(activeBg) {
            var prvBtn = $('.custom-navigation .sli-icon.icon-prev');
            var nextBtn = $('.custom-navigation .sli-icon.icon-next');
            var nextColor = activeBg.attr('data-color-next-button');
            var prvColor = activeBg.attr('data-color-prev-button');
            prvBtn.css({'background-color': prvColor});
            nextBtn.css({'background-color': nextColor});
        };
        allpage_js.runSlider('.main-slider', '.flex-control-nav li', 
        function(activeBg) {changeColor(activeBg);},
        function(activeBg) {changeColor(activeBg);}
        );
        // Run bottom slider
         $('.bottom-slider').flexslider({
                animation: "slide",
                animationLoop: false,
                controlNav: false,
                directionNav: true,
                customDirectionNav: $(".cust-nav a"),
                itemWidth: 315,
                itemMargin: 10,
                minItems: 1,
                maxItems: 4
            });;
         
    });
    /* OnLoad Window */
    var init = function () {   
   
    };
    $(document).on('click', '.hamburger-menu', function () {
        $('body').toggleClass('menu-open');
    });
    window.onload = init;
})(jQuery);