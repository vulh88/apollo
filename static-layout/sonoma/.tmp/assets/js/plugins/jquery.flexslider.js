"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*
 * jQuery FlexSlider v2.6.2
 * Copyright 2012 WooThemes
 * Contributing Author: Tyler Smith
 */
;
(function ($) {

  var focused = true;

  //FlexSlider: Object Instance
  $.flexslider = function (el, options) {
    var slider = $(el);

    // making variables public
    slider.vars = $.extend({}, $.flexslider.defaults, options);

    var namespace = slider.vars.namespace,
        msGesture = window.navigator && window.navigator.msPointerEnabled && window.MSGesture,
        touch = ("ontouchstart" in window || msGesture || window.DocumentTouch && document instanceof DocumentTouch) && slider.vars.touch,

    // depricating this idea, as devices are being released with both of these events
    eventType = "click touchend MSPointerUp keyup",
        watchedEvent = "",
        watchedEventClearTimer,
        vertical = slider.vars.direction === "vertical",
        reverse = slider.vars.reverse,
        carousel = slider.vars.itemWidth > 0,
        fade = slider.vars.animation === "fade",
        asNav = slider.vars.asNavFor !== "",
        methods = {};

    // Store a reference to the slider object
    $.data(el, "flexslider", slider);

    // Private slider methods
    methods = {
      init: function init() {
        slider.animating = false;
        // Get current slide and make sure it is a number
        slider.currentSlide = parseInt(slider.vars.startAt ? slider.vars.startAt : 0, 10);
        if (isNaN(slider.currentSlide)) {
          slider.currentSlide = 0;
        }
        slider.animatingTo = slider.currentSlide;
        slider.atEnd = slider.currentSlide === 0 || slider.currentSlide === slider.last;
        slider.containerSelector = slider.vars.selector.substr(0, slider.vars.selector.search(' '));
        slider.slides = $(slider.vars.selector, slider);
        slider.container = $(slider.containerSelector, slider);
        slider.count = slider.slides.length;
        // SYNC:
        slider.syncExists = $(slider.vars.sync).length > 0;
        // SLIDE:
        if (slider.vars.animation === "slide") {
          slider.vars.animation = "swing";
        }
        slider.prop = vertical ? "top" : "marginLeft";
        slider.args = {};
        // SLIDESHOW:
        slider.manualPause = false;
        slider.stopped = false;
        //PAUSE WHEN INVISIBLE
        slider.started = false;
        slider.startTimeout = null;
        // TOUCH/USECSS:
        slider.transitions = !slider.vars.video && !fade && slider.vars.useCSS && function () {
          var obj = document.createElement('div'),
              props = ['perspectiveProperty', 'WebkitPerspective', 'MozPerspective', 'OPerspective', 'msPerspective'];
          for (var i in props) {
            if (obj.style[props[i]] !== undefined) {
              slider.pfx = props[i].replace('Perspective', '').toLowerCase();
              slider.prop = "-" + slider.pfx + "-transform";
              return true;
            }
          }
          return false;
        }();
        slider.ensureAnimationEnd = '';
        // CONTROLSCONTAINER:
        if (slider.vars.controlsContainer !== "") slider.controlsContainer = $(slider.vars.controlsContainer).length > 0 && $(slider.vars.controlsContainer);
        // MANUAL:
        if (slider.vars.manualControls !== "") slider.manualControls = $(slider.vars.manualControls).length > 0 && $(slider.vars.manualControls);

        // CUSTOM DIRECTION NAV:
        if (slider.vars.customDirectionNav !== "") slider.customDirectionNav = $(slider.vars.customDirectionNav).length === 2 && $(slider.vars.customDirectionNav);

        // RANDOMIZE:
        if (slider.vars.randomize) {
          slider.slides.sort(function () {
            return Math.round(Math.random()) - 0.5;
          });
          slider.container.empty().append(slider.slides);
        }

        slider.doMath();

        // INIT
        slider.setup("init");

        // CONTROLNAV:
        if (slider.vars.controlNav) {
          methods.controlNav.setup();
        }

        // DIRECTIONNAV:
        if (slider.vars.directionNav) {
          methods.directionNav.setup();
        }

        // KEYBOARD:
        if (slider.vars.keyboard && ($(slider.containerSelector).length === 1 || slider.vars.multipleKeyboard)) {
          $(document).bind('keyup', function (event) {
            var keycode = event.keyCode;
            if (!slider.animating && (keycode === 39 || keycode === 37)) {
              var target = keycode === 39 ? slider.getTarget('next') : keycode === 37 ? slider.getTarget('prev') : false;
              slider.flexAnimate(target, slider.vars.pauseOnAction);
            }
          });
        }
        // MOUSEWHEEL:
        if (slider.vars.mousewheel) {
          slider.bind('mousewheel', function (event, delta, deltaX, deltaY) {
            event.preventDefault();
            var target = delta < 0 ? slider.getTarget('next') : slider.getTarget('prev');
            slider.flexAnimate(target, slider.vars.pauseOnAction);
          });
        }

        // PAUSEPLAY
        if (slider.vars.pausePlay) {
          methods.pausePlay.setup();
        }

        //PAUSE WHEN INVISIBLE
        if (slider.vars.slideshow && slider.vars.pauseInvisible) {
          methods.pauseInvisible.init();
        }

        // SLIDSESHOW
        if (slider.vars.slideshow) {
          if (slider.vars.pauseOnHover) {
            slider.hover(function () {
              if (!slider.manualPlay && !slider.manualPause) {
                slider.pause();
              }
            }, function () {
              if (!slider.manualPause && !slider.manualPlay && !slider.stopped) {
                slider.play();
              }
            });
          }
          // initialize animation
          //If we're visible, or we don't use PageVisibility API
          if (!slider.vars.pauseInvisible || !methods.pauseInvisible.isHidden()) {
            slider.vars.initDelay > 0 ? slider.startTimeout = setTimeout(slider.play, slider.vars.initDelay) : slider.play();
          }
        }

        // ASNAV:
        if (asNav) {
          methods.asNav.setup();
        }

        // TOUCH
        if (touch && slider.vars.touch) {
          methods.touch();
        }

        // FADE&&SMOOTHHEIGHT || SLIDE:
        if (!fade || fade && slider.vars.smoothHeight) {
          $(window).bind("resize orientationchange focus", methods.resize);
        }

        slider.find("img").attr("draggable", "false");

        // API: start() Callback
        setTimeout(function () {
          slider.vars.start(slider);
        }, 200);
      },
      asNav: {
        setup: function setup() {
          slider.asNav = true;
          slider.animatingTo = Math.floor(slider.currentSlide / slider.move);
          slider.currentItem = slider.currentSlide;
          slider.slides.removeClass(namespace + "active-slide").eq(slider.currentItem).addClass(namespace + "active-slide");
          if (!msGesture) {
            slider.slides.on(eventType, function (e) {
              e.preventDefault();
              var $slide = $(this),
                  target = $slide.index();
              var posFromLeft = $slide.offset().left - $(slider).scrollLeft(); // Find position of slide relative to left of slider container
              if (posFromLeft <= 0 && $slide.hasClass(namespace + 'active-slide')) {
                slider.flexAnimate(slider.getTarget("prev"), true);
              } else if (!$(slider.vars.asNavFor).data('flexslider').animating && !$slide.hasClass(namespace + "active-slide")) {
                slider.direction = slider.currentItem < target ? "next" : "prev";
                slider.flexAnimate(target, slider.vars.pauseOnAction, false, true, true);
              }
            });
          } else {
            el._slider = slider;
            slider.slides.each(function () {
              var that = this;
              that._gesture = new MSGesture();
              that._gesture.target = that;
              that.addEventListener("MSPointerDown", function (e) {
                e.preventDefault();
                if (e.currentTarget._gesture) {
                  e.currentTarget._gesture.addPointer(e.pointerId);
                }
              }, false);
              that.addEventListener("MSGestureTap", function (e) {
                e.preventDefault();
                var $slide = $(this),
                    target = $slide.index();
                if (!$(slider.vars.asNavFor).data('flexslider').animating && !$slide.hasClass('active')) {
                  slider.direction = slider.currentItem < target ? "next" : "prev";
                  slider.flexAnimate(target, slider.vars.pauseOnAction, false, true, true);
                }
              });
            });
          }
        }
      },
      controlNav: {
        setup: function setup() {
          if (!slider.manualControls) {
            methods.controlNav.setupPaging();
          } else {
            // MANUALCONTROLS:
            methods.controlNav.setupManual();
          }
        },
        setupPaging: function setupPaging() {
          var type = slider.vars.controlNav === "thumbnails" ? 'control-thumbs' : 'control-paging',
              j = 1,
              item,
              slide;

          slider.controlNavScaffold = $('<ol class="' + namespace + 'control-nav ' + namespace + type + '"></ol>');

          if (slider.pagingCount > 1) {
            for (var i = 0; i < slider.pagingCount; i++) {
              slide = slider.slides.eq(i);
              if (undefined === slide.attr('data-thumb-alt')) {
                slide.attr('data-thumb-alt', '');
              }
              var altText = '' !== slide.attr('data-thumb-alt') ? altText = ' alt="' + slide.attr('data-thumb-alt') + '"' : '';
              item = slider.vars.controlNav === "thumbnails" ? '<img src="' + slide.attr('data-thumb') + '"' + altText + '/>' : '<a href="#">' + j + '</a>';
              if ('thumbnails' === slider.vars.controlNav && true === slider.vars.thumbCaptions) {
                var captn = slide.attr('data-thumbcaption');
                if ('' !== captn && undefined !== captn) {
                  item += '<span class="' + namespace + 'caption">' + captn + '</span>';
                }
              }
              slider.controlNavScaffold.append('<li>' + item + '</li>');
              j++;
            }
          }

          // CONTROLSCONTAINER:
          slider.controlsContainer ? $(slider.controlsContainer).append(slider.controlNavScaffold) : slider.append(slider.controlNavScaffold);
          methods.controlNav.set();

          methods.controlNav.active();

          slider.controlNavScaffold.delegate('a, img', eventType, function (event) {
            event.preventDefault();

            if (watchedEvent === "" || watchedEvent === event.type) {
              var $this = $(this),
                  target = slider.controlNav.index($this);

              if (!$this.hasClass(namespace + 'active')) {
                slider.direction = target > slider.currentSlide ? "next" : "prev";
                slider.flexAnimate(target, slider.vars.pauseOnAction);
              }
            }

            // setup flags to prevent event duplication
            if (watchedEvent === "") {
              watchedEvent = event.type;
            }
            methods.setToClearWatchedEvent();
          });
        },
        setupManual: function setupManual() {
          slider.controlNav = slider.manualControls;
          methods.controlNav.active();

          slider.controlNav.bind(eventType, function (event) {
            event.preventDefault();

            if (watchedEvent === "" || watchedEvent === event.type) {
              var $this = $(this),
                  target = slider.controlNav.index($this);

              if (!$this.hasClass(namespace + 'active')) {
                target > slider.currentSlide ? slider.direction = "next" : slider.direction = "prev";
                slider.flexAnimate(target, slider.vars.pauseOnAction);
              }
            }

            // setup flags to prevent event duplication
            if (watchedEvent === "") {
              watchedEvent = event.type;
            }
            methods.setToClearWatchedEvent();
          });
        },
        set: function set() {
          var selector = slider.vars.controlNav === "thumbnails" ? 'img' : 'a';
          slider.controlNav = $('.' + namespace + 'control-nav li ' + selector, slider.controlsContainer ? slider.controlsContainer : slider);
        },
        active: function active() {
          slider.controlNav.removeClass(namespace + "active").eq(slider.animatingTo).addClass(namespace + "active");
        },
        update: function update(action, pos) {
          if (slider.pagingCount > 1 && action === "add") {
            slider.controlNavScaffold.append($('<li><a href="#">' + slider.count + '</a></li>'));
          } else if (slider.pagingCount === 1) {
            slider.controlNavScaffold.find('li').remove();
          } else {
            slider.controlNav.eq(pos).closest('li').remove();
          }
          methods.controlNav.set();
          slider.pagingCount > 1 && slider.pagingCount !== slider.controlNav.length ? slider.update(pos, action) : methods.controlNav.active();
        }
      },
      directionNav: {
        setup: function setup() {
          var directionNavScaffold = $('<ul class="' + namespace + 'direction-nav"><li class="' + namespace + 'nav-prev"><a class="' + namespace + 'prev" href="#">' + slider.vars.prevText + '</a></li><li class="' + namespace + 'nav-next"><a class="' + namespace + 'next" href="#">' + slider.vars.nextText + '</a></li></ul>');

          // CUSTOM DIRECTION NAV:
          if (slider.customDirectionNav) {
            slider.directionNav = slider.customDirectionNav;
            // CONTROLSCONTAINER:
          } else if (slider.controlsContainer) {
            $(slider.controlsContainer).append(directionNavScaffold);
            slider.directionNav = $('.' + namespace + 'direction-nav li a', slider.controlsContainer);
          } else {
            slider.append(directionNavScaffold);
            slider.directionNav = $('.' + namespace + 'direction-nav li a', slider);
          }

          methods.directionNav.update();

          slider.directionNav.bind(eventType, function (event) {
            event.preventDefault();
            var target;

            if (watchedEvent === "" || watchedEvent === event.type) {
              target = $(this).hasClass(namespace + 'next') ? slider.getTarget('next') : slider.getTarget('prev');
              slider.flexAnimate(target, slider.vars.pauseOnAction);
            }

            // setup flags to prevent event duplication
            if (watchedEvent === "") {
              watchedEvent = event.type;
            }
            methods.setToClearWatchedEvent();
          });
        },
        update: function update() {
          var disabledClass = namespace + 'disabled';
          if (slider.pagingCount === 1) {
            slider.directionNav.addClass(disabledClass).attr('tabindex', '-1');
          } else if (!slider.vars.animationLoop) {
            if (slider.animatingTo === 0) {
              slider.directionNav.removeClass(disabledClass).filter('.' + namespace + "prev").addClass(disabledClass).attr('tabindex', '-1');
            } else if (slider.animatingTo === slider.last) {
              slider.directionNav.removeClass(disabledClass).filter('.' + namespace + "next").addClass(disabledClass).attr('tabindex', '-1');
            } else {
              slider.directionNav.removeClass(disabledClass).removeAttr('tabindex');
            }
          } else {
            slider.directionNav.removeClass(disabledClass).removeAttr('tabindex');
          }
        }
      },
      pausePlay: {
        setup: function setup() {
          var pausePlayScaffold = $('<div class="' + namespace + 'pauseplay"><a href="#"></a></div>');

          // CONTROLSCONTAINER:
          if (slider.controlsContainer) {
            slider.controlsContainer.append(pausePlayScaffold);
            slider.pausePlay = $('.' + namespace + 'pauseplay a', slider.controlsContainer);
          } else {
            slider.append(pausePlayScaffold);
            slider.pausePlay = $('.' + namespace + 'pauseplay a', slider);
          }

          methods.pausePlay.update(slider.vars.slideshow ? namespace + 'pause' : namespace + 'play');

          slider.pausePlay.bind(eventType, function (event) {
            event.preventDefault();

            if (watchedEvent === "" || watchedEvent === event.type) {
              if ($(this).hasClass(namespace + 'pause')) {
                slider.manualPause = true;
                slider.manualPlay = false;
                slider.pause();
              } else {
                slider.manualPause = false;
                slider.manualPlay = true;
                slider.play();
              }
            }

            // setup flags to prevent event duplication
            if (watchedEvent === "") {
              watchedEvent = event.type;
            }
            methods.setToClearWatchedEvent();
          });
        },
        update: function update(state) {
          state === "play" ? slider.pausePlay.removeClass(namespace + 'pause').addClass(namespace + 'play').html(slider.vars.playText) : slider.pausePlay.removeClass(namespace + 'play').addClass(namespace + 'pause').html(slider.vars.pauseText);
        }
      },
      touch: function touch() {
        var startX,
            startY,
            offset,
            cwidth,
            dx,
            startT,
            onTouchStart,
            onTouchMove,
            _onTouchEnd,
            scrolling = false,
            localX = 0,
            localY = 0,
            accDx = 0;

        if (!msGesture) {
          onTouchStart = function onTouchStart(e) {
            if (slider.animating) {
              e.preventDefault();
            } else if (window.navigator.msPointerEnabled || e.touches.length === 1) {
              slider.pause();
              // CAROUSEL:
              cwidth = vertical ? slider.h : slider.w;
              startT = Number(new Date());
              // CAROUSEL:

              // Local vars for X and Y points.
              localX = e.touches[0].pageX;
              localY = e.touches[0].pageY;

              offset = carousel && reverse && slider.animatingTo === slider.last ? 0 : carousel && reverse ? slider.limit - (slider.itemW + slider.vars.itemMargin) * slider.move * slider.animatingTo : carousel && slider.currentSlide === slider.last ? slider.limit : carousel ? (slider.itemW + slider.vars.itemMargin) * slider.move * slider.currentSlide : reverse ? (slider.last - slider.currentSlide + slider.cloneOffset) * cwidth : (slider.currentSlide + slider.cloneOffset) * cwidth;
              startX = vertical ? localY : localX;
              startY = vertical ? localX : localY;

              el.addEventListener('touchmove', onTouchMove, false);
              el.addEventListener('touchend', _onTouchEnd, false);
            }
          };

          onTouchMove = function onTouchMove(e) {
            // Local vars for X and Y points.

            localX = e.touches[0].pageX;
            localY = e.touches[0].pageY;

            dx = vertical ? startX - localY : startX - localX;
            scrolling = vertical ? Math.abs(dx) < Math.abs(localX - startY) : Math.abs(dx) < Math.abs(localY - startY);

            var fxms = 500;

            if (!scrolling || Number(new Date()) - startT > fxms) {
              e.preventDefault();
              if (!fade && slider.transitions) {
                if (!slider.vars.animationLoop) {
                  dx = dx / (slider.currentSlide === 0 && dx < 0 || slider.currentSlide === slider.last && dx > 0 ? Math.abs(dx) / cwidth + 2 : 1);
                }
                slider.setProps(offset + dx, "setTouch");
              }
            }
          };

          _onTouchEnd = function onTouchEnd(e) {
            // finish the touch by undoing the touch session
            el.removeEventListener('touchmove', onTouchMove, false);

            if (slider.animatingTo === slider.currentSlide && !scrolling && !(dx === null)) {
              var updateDx = reverse ? -dx : dx,
                  target = updateDx > 0 ? slider.getTarget('next') : slider.getTarget('prev');

              if (slider.canAdvance(target) && (Number(new Date()) - startT < 550 && Math.abs(updateDx) > 50 || Math.abs(updateDx) > cwidth / 2)) {
                slider.flexAnimate(target, slider.vars.pauseOnAction);
              } else {
                if (!fade) {
                  slider.flexAnimate(slider.currentSlide, slider.vars.pauseOnAction, true);
                }
              }
            }
            el.removeEventListener('touchend', _onTouchEnd, false);

            startX = null;
            startY = null;
            dx = null;
            offset = null;
          };

          el.addEventListener('touchstart', onTouchStart, false);
        } else {
          var onMSPointerDown = function onMSPointerDown(e) {
            e.stopPropagation();
            if (slider.animating) {
              e.preventDefault();
            } else {
              slider.pause();
              el._gesture.addPointer(e.pointerId);
              accDx = 0;
              cwidth = vertical ? slider.h : slider.w;
              startT = Number(new Date());
              // CAROUSEL:

              offset = carousel && reverse && slider.animatingTo === slider.last ? 0 : carousel && reverse ? slider.limit - (slider.itemW + slider.vars.itemMargin) * slider.move * slider.animatingTo : carousel && slider.currentSlide === slider.last ? slider.limit : carousel ? (slider.itemW + slider.vars.itemMargin) * slider.move * slider.currentSlide : reverse ? (slider.last - slider.currentSlide + slider.cloneOffset) * cwidth : (slider.currentSlide + slider.cloneOffset) * cwidth;
            }
          };

          var onMSGestureChange = function onMSGestureChange(e) {
            e.stopPropagation();
            var slider = e.target._slider;
            if (!slider) {
              return;
            }
            var transX = -e.translationX,
                transY = -e.translationY;

            //Accumulate translations.
            accDx = accDx + (vertical ? transY : transX);
            dx = accDx;
            scrolling = vertical ? Math.abs(accDx) < Math.abs(-transX) : Math.abs(accDx) < Math.abs(-transY);

            if (e.detail === e.MSGESTURE_FLAG_INERTIA) {
              setImmediate(function () {
                el._gesture.stop();
              });

              return;
            }

            if (!scrolling || Number(new Date()) - startT > 500) {
              e.preventDefault();
              if (!fade && slider.transitions) {
                if (!slider.vars.animationLoop) {
                  dx = accDx / (slider.currentSlide === 0 && accDx < 0 || slider.currentSlide === slider.last && accDx > 0 ? Math.abs(accDx) / cwidth + 2 : 1);
                }
                slider.setProps(offset + dx, "setTouch");
              }
            }
          };

          var onMSGestureEnd = function onMSGestureEnd(e) {
            e.stopPropagation();
            var slider = e.target._slider;
            if (!slider) {
              return;
            }
            if (slider.animatingTo === slider.currentSlide && !scrolling && !(dx === null)) {
              var updateDx = reverse ? -dx : dx,
                  target = updateDx > 0 ? slider.getTarget('next') : slider.getTarget('prev');

              if (slider.canAdvance(target) && (Number(new Date()) - startT < 550 && Math.abs(updateDx) > 50 || Math.abs(updateDx) > cwidth / 2)) {
                slider.flexAnimate(target, slider.vars.pauseOnAction);
              } else {
                if (!fade) {
                  slider.flexAnimate(slider.currentSlide, slider.vars.pauseOnAction, true);
                }
              }
            }

            startX = null;
            startY = null;
            dx = null;
            offset = null;
            accDx = 0;
          };

          el.style.msTouchAction = "none";
          el._gesture = new MSGesture();
          el._gesture.target = el;
          el.addEventListener("MSPointerDown", onMSPointerDown, false);
          el._slider = slider;
          el.addEventListener("MSGestureChange", onMSGestureChange, false);
          el.addEventListener("MSGestureEnd", onMSGestureEnd, false);
        }
      },
      resize: function resize() {
        if (!slider.animating && slider.is(':visible')) {
          if (!carousel) {
            slider.doMath();
          }

          if (fade) {
            // SMOOTH HEIGHT:
            methods.smoothHeight();
          } else if (carousel) {
            //CAROUSEL:
            slider.slides.width(slider.computedW);
            slider.update(slider.pagingCount);
            slider.setProps();
          } else if (vertical) {
            //VERTICAL:
            slider.viewport.height(slider.h);
            slider.setProps(slider.h, "setTotal");
          } else {
            // SMOOTH HEIGHT:
            if (slider.vars.smoothHeight) {
              methods.smoothHeight();
            }
            slider.newSlides.width(slider.computedW);
            slider.setProps(slider.computedW, "setTotal");
          }
        }
      },
      smoothHeight: function smoothHeight(dur) {
        if (!vertical || fade) {
          var $obj = fade ? slider : slider.viewport;
          dur ? $obj.animate({ "height": slider.slides.eq(slider.animatingTo).innerHeight() }, dur).css('overflow', 'visible') : $obj.innerHeight(slider.slides.eq(slider.animatingTo).innerHeight());
        }
      },
      sync: function sync(action) {
        var $obj = $(slider.vars.sync).data("flexslider"),
            target = slider.animatingTo;

        switch (action) {
          case "animate":
            $obj.flexAnimate(target, slider.vars.pauseOnAction, false, true);break;
          case "play":
            if (!$obj.playing && !$obj.asNav) {
              $obj.play();
            }break;
          case "pause":
            $obj.pause();break;
        }
      },
      uniqueID: function uniqueID($clone) {
        // Append _clone to current level and children elements with id attributes
        $clone.filter('[id]').add($clone.find('[id]')).each(function () {
          var $this = $(this);
          $this.attr('id', $this.attr('id') + '_clone');
        });
        return $clone;
      },
      pauseInvisible: {
        visProp: null,
        init: function init() {
          var visProp = methods.pauseInvisible.getHiddenProp();
          if (visProp) {
            var evtname = visProp.replace(/[H|h]idden/, '') + 'visibilitychange';
            document.addEventListener(evtname, function () {
              if (methods.pauseInvisible.isHidden()) {
                if (slider.startTimeout) {
                  clearTimeout(slider.startTimeout); //If clock is ticking, stop timer and prevent from starting while invisible
                } else {
                  slider.pause(); //Or just pause
                }
              } else {
                if (slider.started) {
                  slider.play(); //Initiated before, just play
                } else {
                  if (slider.vars.initDelay > 0) {
                    setTimeout(slider.play, slider.vars.initDelay);
                  } else {
                    slider.play(); //Didn't init before: simply init or wait for it
                  }
                }
              }
            });
          }
        },
        isHidden: function isHidden() {
          var prop = methods.pauseInvisible.getHiddenProp();
          if (!prop) {
            return false;
          }
          return document[prop];
        },
        getHiddenProp: function getHiddenProp() {
          var prefixes = ['webkit', 'moz', 'ms', 'o'];
          // if 'hidden' is natively supported just return it
          if ('hidden' in document) {
            return 'hidden';
          }
          // otherwise loop over all the known prefixes until we find one
          for (var i = 0; i < prefixes.length; i++) {
            if (prefixes[i] + 'Hidden' in document) {
              return prefixes[i] + 'Hidden';
            }
          }
          // otherwise it's not supported
          return null;
        }
      },
      setToClearWatchedEvent: function setToClearWatchedEvent() {
        clearTimeout(watchedEventClearTimer);
        watchedEventClearTimer = setTimeout(function () {
          watchedEvent = "";
        }, 3000);
      }
    };

    // public methods
    slider.flexAnimate = function (target, pause, override, withSync, fromNav) {
      if (!slider.vars.animationLoop && target !== slider.currentSlide) {
        slider.direction = target > slider.currentSlide ? "next" : "prev";
      }

      if (asNav && slider.pagingCount === 1) slider.direction = slider.currentItem < target ? "next" : "prev";

      if (!slider.animating && (slider.canAdvance(target, fromNav) || override) && slider.is(":visible")) {
        if (asNav && withSync) {
          var master = $(slider.vars.asNavFor).data('flexslider');
          slider.atEnd = target === 0 || target === slider.count - 1;
          master.flexAnimate(target, true, false, true, fromNav);
          slider.direction = slider.currentItem < target ? "next" : "prev";
          master.direction = slider.direction;

          if (Math.ceil((target + 1) / slider.visible) - 1 !== slider.currentSlide && target !== 0) {
            slider.currentItem = target;
            slider.slides.removeClass(namespace + "active-slide").eq(target).addClass(namespace + "active-slide");
            target = Math.floor(target / slider.visible);
          } else {
            slider.currentItem = target;
            slider.slides.removeClass(namespace + "active-slide").eq(target).addClass(namespace + "active-slide");
            return false;
          }
        }

        slider.animating = true;
        slider.animatingTo = target;

        // SLIDESHOW:
        if (pause) {
          slider.pause();
        }

        // API: before() animation Callback
        slider.vars.before(slider);

        // SYNC:
        if (slider.syncExists && !fromNav) {
          methods.sync("animate");
        }

        // CONTROLNAV
        if (slider.vars.controlNav) {
          methods.controlNav.active();
        }

        // !CAROUSEL:
        // CANDIDATE: slide active class (for add/remove slide)
        if (!carousel) {
          slider.slides.removeClass(namespace + 'active-slide').eq(target).addClass(namespace + 'active-slide');
        }

        // INFINITE LOOP:
        // CANDIDATE: atEnd
        slider.atEnd = target === 0 || target === slider.last;

        // DIRECTIONNAV:
        if (slider.vars.directionNav) {
          methods.directionNav.update();
        }

        if (target === slider.last) {
          // API: end() of cycle Callback
          slider.vars.end(slider);
          // SLIDESHOW && !INFINITE LOOP:
          if (!slider.vars.animationLoop) {
            slider.pause();
          }
        }

        // SLIDE:
        if (!fade) {
          var dimension = vertical ? slider.slides.filter(':first').height() : slider.computedW,
              margin,
              slideString,
              calcNext;

          // INFINITE LOOP / REVERSE:
          if (carousel) {
            margin = slider.vars.itemMargin;
            calcNext = (slider.itemW + margin) * slider.move * slider.animatingTo;
            slideString = calcNext > slider.limit && slider.visible !== 1 ? slider.limit : calcNext;
          } else if (slider.currentSlide === 0 && target === slider.count - 1 && slider.vars.animationLoop && slider.direction !== "next") {
            slideString = reverse ? (slider.count + slider.cloneOffset) * dimension : 0;
          } else if (slider.currentSlide === slider.last && target === 0 && slider.vars.animationLoop && slider.direction !== "prev") {
            slideString = reverse ? 0 : (slider.count + 1) * dimension;
          } else {
            slideString = reverse ? (slider.count - 1 - target + slider.cloneOffset) * dimension : (target + slider.cloneOffset) * dimension;
          }
          slider.setProps(slideString, "", slider.vars.animationSpeed);
          if (slider.transitions) {
            if (!slider.vars.animationLoop || !slider.atEnd) {
              slider.animating = false;
              slider.currentSlide = slider.animatingTo;
            }

            // Unbind previous transitionEnd events and re-bind new transitionEnd event
            slider.container.unbind("webkitTransitionEnd transitionend");
            slider.container.bind("webkitTransitionEnd transitionend", function () {
              clearTimeout(slider.ensureAnimationEnd);
              slider.wrapup(dimension);
            });

            // Insurance for the ever-so-fickle transitionEnd event
            clearTimeout(slider.ensureAnimationEnd);
            slider.ensureAnimationEnd = setTimeout(function () {
              slider.wrapup(dimension);
            }, slider.vars.animationSpeed + 100);
          } else {
            slider.container.animate(slider.args, slider.vars.animationSpeed, slider.vars.easing, function () {
              slider.wrapup(dimension);
            });
          }
        } else {
          // FADE:
          if (!touch) {
            //slider.slides.eq(slider.currentSlide).fadeOut(slider.vars.animationSpeed, slider.vars.easing);
            //slider.slides.eq(target).fadeIn(slider.vars.animationSpeed, slider.vars.easing, slider.wrapup);

            slider.slides.eq(slider.currentSlide).css({ "zIndex": 1, "display": "none" }).animate({ "opacity": 0 }, slider.vars.animationSpeed, slider.vars.easing);
            slider.slides.eq(target).css({ "zIndex": 2, "display": "block" }).animate({ "opacity": 1 }, slider.vars.animationSpeed, slider.vars.easing, slider.wrapup);
          } else {
            slider.slides.eq(slider.currentSlide).css({ "opacity": 0, "zIndex": 1, "display": "none" });
            slider.slides.eq(target).css({ "opacity": 1, "zIndex": 2, "display": "block" });
            slider.wrapup(dimension);
          }
        }
        // SMOOTH HEIGHT:
        if (slider.vars.smoothHeight) {
          methods.smoothHeight(slider.vars.animationSpeed);
        }
      }
    };
    slider.wrapup = function (dimension) {
      // SLIDE:
      if (!fade && !carousel) {
        if (slider.currentSlide === 0 && slider.animatingTo === slider.last && slider.vars.animationLoop) {
          slider.setProps(dimension, "jumpEnd");
        } else if (slider.currentSlide === slider.last && slider.animatingTo === 0 && slider.vars.animationLoop) {
          slider.setProps(dimension, "jumpStart");
        }
      }
      slider.animating = false;
      slider.currentSlide = slider.animatingTo;
      // API: after() animation Callback
      slider.vars.after(slider);
    };

    // SLIDESHOW:
    slider.animateSlides = function () {
      if (!slider.animating && focused) {
        slider.flexAnimate(slider.getTarget("next"));
      }
    };
    // SLIDESHOW:
    slider.pause = function () {
      clearInterval(slider.animatedSlides);
      slider.animatedSlides = null;
      slider.playing = false;
      // PAUSEPLAY:
      if (slider.vars.pausePlay) {
        methods.pausePlay.update("play");
      }
      // SYNC:
      if (slider.syncExists) {
        methods.sync("pause");
      }
    };
    // SLIDESHOW:
    slider.play = function () {
      if (slider.playing) {
        clearInterval(slider.animatedSlides);
      }
      slider.animatedSlides = slider.animatedSlides || setInterval(slider.animateSlides, slider.vars.slideshowSpeed);
      slider.started = slider.playing = true;
      // PAUSEPLAY:
      if (slider.vars.pausePlay) {
        methods.pausePlay.update("pause");
      }
      // SYNC:
      if (slider.syncExists) {
        methods.sync("play");
      }
    };
    // STOP:
    slider.stop = function () {
      slider.pause();
      slider.stopped = true;
    };
    slider.canAdvance = function (target, fromNav) {
      // ASNAV:
      var last = asNav ? slider.pagingCount - 1 : slider.last;
      return fromNav ? true : asNav && slider.currentItem === slider.count - 1 && target === 0 && slider.direction === "prev" ? true : asNav && slider.currentItem === 0 && target === slider.pagingCount - 1 && slider.direction !== "next" ? false : target === slider.currentSlide && !asNav ? false : slider.vars.animationLoop ? true : slider.atEnd && slider.currentSlide === 0 && target === last && slider.direction !== "next" ? false : slider.atEnd && slider.currentSlide === last && target === 0 && slider.direction === "next" ? false : true;
    };
    slider.getTarget = function (dir) {
      slider.direction = dir;
      if (dir === "next") {
        return slider.currentSlide === slider.last ? 0 : slider.currentSlide + 1;
      } else {
        return slider.currentSlide === 0 ? slider.last : slider.currentSlide - 1;
      }
    };

    // SLIDE:
    slider.setProps = function (pos, special, dur) {
      var target = function () {
        var posCheck = pos ? pos : (slider.itemW + slider.vars.itemMargin) * slider.move * slider.animatingTo,
            posCalc = function () {
          if (carousel) {
            return special === "setTouch" ? pos : reverse && slider.animatingTo === slider.last ? 0 : reverse ? slider.limit - (slider.itemW + slider.vars.itemMargin) * slider.move * slider.animatingTo : slider.animatingTo === slider.last ? slider.limit : posCheck;
          } else {
            switch (special) {
              case "setTotal":
                return reverse ? (slider.count - 1 - slider.currentSlide + slider.cloneOffset) * pos : (slider.currentSlide + slider.cloneOffset) * pos;
              case "setTouch":
                return reverse ? pos : pos;
              case "jumpEnd":
                return reverse ? pos : slider.count * pos;
              case "jumpStart":
                return reverse ? slider.count * pos : pos;
              default:
                return pos;
            }
          }
        }();

        return posCalc * -1 + "px";
      }();

      if (slider.transitions) {
        target = vertical ? "translate3d(0," + target + ",0)" : "translate3d(" + target + ",0,0)";
        dur = dur !== undefined ? dur / 1000 + "s" : "0s";
        slider.container.css("-" + slider.pfx + "-transition-duration", dur);
        slider.container.css("transition-duration", dur);
      }

      slider.args[slider.prop] = target;
      if (slider.transitions || dur === undefined) {
        slider.container.css(slider.args);
      }

      slider.container.css('transform', target);
    };

    slider.setup = function (type) {
      // SLIDE:
      if (!fade) {
        var sliderOffset, arr;

        if (type === "init") {
          slider.viewport = $('<div class="' + namespace + 'viewport"></div>').css({ "overflow": "hidden", "position": "relative" }).appendTo(slider).append(slider.container);
          // INFINITE LOOP:
          slider.cloneCount = 0;
          slider.cloneOffset = 0;
          // REVERSE:
          if (reverse) {
            arr = $.makeArray(slider.slides).reverse();
            slider.slides = $(arr);
            slider.container.empty().append(slider.slides);
          }
        }
        // INFINITE LOOP && !CAROUSEL:
        if (slider.vars.animationLoop && !carousel) {
          slider.cloneCount = 2;
          slider.cloneOffset = 1;
          // clear out old clones
          if (type !== "init") {
            slider.container.find('.clone').remove();
          }
          slider.container.append(methods.uniqueID(slider.slides.first().clone().addClass('clone')).attr('aria-hidden', 'true')).prepend(methods.uniqueID(slider.slides.last().clone().addClass('clone')).attr('aria-hidden', 'true'));
        }
        slider.newSlides = $(slider.vars.selector, slider);

        sliderOffset = reverse ? slider.count - 1 - slider.currentSlide + slider.cloneOffset : slider.currentSlide + slider.cloneOffset;
        // VERTICAL:
        if (vertical && !carousel) {
          slider.container.height((slider.count + slider.cloneCount) * 200 + "%").css("position", "absolute").width("100%");
          setTimeout(function () {
            slider.newSlides.css({ "display": "block" });
            slider.doMath();
            slider.viewport.height(slider.h);
            slider.setProps(sliderOffset * slider.h, "init");
          }, type === "init" ? 100 : 0);
        } else {
          slider.container.width((slider.count + slider.cloneCount) * 200 + "%");
          slider.setProps(sliderOffset * slider.computedW, "init");
          setTimeout(function () {
            slider.doMath();
            slider.newSlides.css({ "width": slider.computedW, "marginRight": slider.computedM, "float": "left", "display": "block" });
            // SMOOTH HEIGHT:
            if (slider.vars.smoothHeight) {
              methods.smoothHeight();
            }
          }, type === "init" ? 100 : 0);
        }
      } else {
        // FADE:
        slider.slides.css({ "width": "100%", "float": "left", "marginRight": "-100%", "position": "relative" });
        if (type === "init") {
          if (!touch) {
            //slider.slides.eq(slider.currentSlide).fadeIn(slider.vars.animationSpeed, slider.vars.easing);
            if (slider.vars.fadeFirstSlide == false) {
              slider.slides.css({ "opacity": 0, "display": "none", "zIndex": 1 }).eq(slider.currentSlide).css({ "zIndex": 2, "display": "block" }).css({ "opacity": 1 });
            } else {
              slider.slides.css({ "opacity": 0, "display": "none", "zIndex": 1 }).eq(slider.currentSlide).css({ "zIndex": 2, "display": "block" }).animate({ "opacity": 1 }, slider.vars.animationSpeed, slider.vars.easing);
            }
          } else {
            slider.slides.css({ "opacity": 0, "display": "none", "webkitTransition": "opacity " + slider.vars.animationSpeed / 1000 + "s ease", "zIndex": 1 }).eq(slider.currentSlide).css({ "opacity": 1, "zIndex": 2, "display": "block" });
          }
        }
        // SMOOTH HEIGHT:
        if (slider.vars.smoothHeight) {
          methods.smoothHeight();
        }
      }
      // !CAROUSEL:
      // CANDIDATE: active slide
      if (!carousel) {
        slider.slides.removeClass(namespace + "active-slide").eq(slider.currentSlide).addClass(namespace + "active-slide");
      }

      //FlexSlider: init() Callback
      slider.vars.init(slider);
    };

    slider.doMath = function () {
      var slide = slider.slides.first(),
          slideMargin = slider.vars.itemMargin,
          minItems = slider.vars.minItems,
          maxItems = slider.vars.maxItems;

      slider.w = slider.viewport === undefined ? slider.width() : slider.viewport.width();
      slider.h = slide.height();
      slider.boxPadding = slide.outerWidth() - slide.width();

      // CAROUSEL:
      if (carousel) {
        slider.itemT = slider.vars.itemWidth + slideMargin;
        slider.itemM = slideMargin;
        slider.minW = minItems ? minItems * slider.itemT : slider.w;
        slider.maxW = maxItems ? maxItems * slider.itemT - slideMargin : slider.w;
        slider.itemW = slider.minW > slider.w ? (slider.w - slideMargin * (minItems - 1)) / minItems : slider.maxW < slider.w ? (slider.w - slideMargin * (maxItems - 1)) / maxItems : slider.vars.itemWidth > slider.w ? slider.w : slider.vars.itemWidth;

        slider.visible = Math.floor(slider.w / slider.itemW);
        slider.move = slider.vars.move > 0 && slider.vars.move < slider.visible ? slider.vars.move : slider.visible;
        slider.pagingCount = Math.ceil((slider.count - slider.visible) / slider.move + 1);
        slider.last = slider.pagingCount - 1;
        slider.limit = slider.pagingCount === 1 ? 0 : slider.vars.itemWidth > slider.w ? slider.itemW * (slider.count - 1) + slideMargin * (slider.count - 1) : (slider.itemW + slideMargin) * slider.count - slider.w - slideMargin;
      } else {
        slider.itemW = slider.w;
        slider.itemM = slideMargin;
        slider.pagingCount = slider.count;
        slider.last = slider.count - 1;
      }
      slider.computedW = slider.itemW - slider.boxPadding;
      slider.computedM = slider.itemM;
    };

    slider.update = function (pos, action) {
      slider.doMath();

      // update currentSlide and slider.animatingTo if necessary
      if (!carousel) {
        if (pos < slider.currentSlide) {
          slider.currentSlide += 1;
        } else if (pos <= slider.currentSlide && pos !== 0) {
          slider.currentSlide -= 1;
        }
        slider.animatingTo = slider.currentSlide;
      }

      // update controlNav
      if (slider.vars.controlNav && !slider.manualControls) {
        if (action === "add" && !carousel || slider.pagingCount > slider.controlNav.length) {
          methods.controlNav.update("add");
        } else if (action === "remove" && !carousel || slider.pagingCount < slider.controlNav.length) {
          if (carousel && slider.currentSlide > slider.last) {
            slider.currentSlide -= 1;
            slider.animatingTo -= 1;
          }
          methods.controlNav.update("remove", slider.last);
        }
      }
      // update directionNav
      if (slider.vars.directionNav) {
        methods.directionNav.update();
      }
    };

    slider.addSlide = function (obj, pos) {
      var $obj = $(obj);

      slider.count += 1;
      slider.last = slider.count - 1;

      // append new slide
      if (vertical && reverse) {
        pos !== undefined ? slider.slides.eq(slider.count - pos).after($obj) : slider.container.prepend($obj);
      } else {
        pos !== undefined ? slider.slides.eq(pos).before($obj) : slider.container.append($obj);
      }

      // update currentSlide, animatingTo, controlNav, and directionNav
      slider.update(pos, "add");

      // update slider.slides
      slider.slides = $(slider.vars.selector + ':not(.clone)', slider);
      // re-setup the slider to accomdate new slide
      slider.setup();

      //FlexSlider: added() Callback
      slider.vars.added(slider);
    };
    slider.removeSlide = function (obj) {
      var pos = isNaN(obj) ? slider.slides.index($(obj)) : obj;

      // update count
      slider.count -= 1;
      slider.last = slider.count - 1;

      // remove slide
      if (isNaN(obj)) {
        $(obj, slider.slides).remove();
      } else {
        vertical && reverse ? slider.slides.eq(slider.last).remove() : slider.slides.eq(obj).remove();
      }

      // update currentSlide, animatingTo, controlNav, and directionNav
      slider.doMath();
      slider.update(pos, "remove");

      // update slider.slides
      slider.slides = $(slider.vars.selector + ':not(.clone)', slider);
      // re-setup the slider to accomdate new slide
      slider.setup();

      // FlexSlider: removed() Callback
      slider.vars.removed(slider);
    };

    //FlexSlider: Initialize
    methods.init();
  };

  // Ensure the slider isn't focussed if the window loses focus.
  $(window).blur(function (e) {
    focused = false;
  }).focus(function (e) {
    focused = true;
  });

  //FlexSlider: Default Settings
  $.flexslider.defaults = {
    namespace: "flex-", //{NEW} String: Prefix string attached to the class of every element generated by the plugin
    selector: ".slides > li", //{NEW} Selector: Must match a simple pattern. '{container} > {slide}' -- Ignore pattern at your own peril
    animation: "fade", //String: Select your animation type, "fade" or "slide"
    easing: "swing", //{NEW} String: Determines the easing method used in jQuery transitions. jQuery easing plugin is supported!
    direction: "horizontal", //String: Select the sliding direction, "horizontal" or "vertical"
    reverse: false, //{NEW} Boolean: Reverse the animation direction
    animationLoop: true, //Boolean: Should the animation loop? If false, directionNav will received "disable" classes at either end
    smoothHeight: false, //{NEW} Boolean: Allow height of the slider to animate smoothly in horizontal mode
    startAt: 0, //Integer: The slide that the slider should start on. Array notation (0 = first slide)
    slideshow: true, //Boolean: Animate slider automatically
    slideshowSpeed: 7000, //Integer: Set the speed of the slideshow cycling, in milliseconds
    animationSpeed: 600, //Integer: Set the speed of animations, in milliseconds
    initDelay: 0, //{NEW} Integer: Set an initialization delay, in milliseconds
    randomize: false, //Boolean: Randomize slide order
    fadeFirstSlide: true, //Boolean: Fade in the first slide when animation type is "fade"
    thumbCaptions: false, //Boolean: Whether or not to put captions on thumbnails when using the "thumbnails" controlNav.

    // Usability features
    pauseOnAction: true, //Boolean: Pause the slideshow when interacting with control elements, highly recommended.
    pauseOnHover: false, //Boolean: Pause the slideshow when hovering over slider, then resume when no longer hovering
    pauseInvisible: true, //{NEW} Boolean: Pause the slideshow when tab is invisible, resume when visible. Provides better UX, lower CPU usage.
    useCSS: true, //{NEW} Boolean: Slider will use CSS3 transitions if available
    touch: true, //{NEW} Boolean: Allow touch swipe navigation of the slider on touch-enabled devices
    video: false, //{NEW} Boolean: If using video in the slider, will prevent CSS3 3D Transforms to avoid graphical glitches

    // Primary Controls
    controlNav: true, //Boolean: Create navigation for paging control of each slide? Note: Leave true for manualControls usage
    directionNav: true, //Boolean: Create navigation for previous/next navigation? (true/false)
    prevText: "Previous", //String: Set the text for the "previous" directionNav item
    nextText: "Next", //String: Set the text for the "next" directionNav item

    // Secondary Navigation
    keyboard: true, //Boolean: Allow slider navigating via keyboard left/right keys
    multipleKeyboard: false, //{NEW} Boolean: Allow keyboard navigation to affect multiple sliders. Default behavior cuts out keyboard navigation with more than one slider present.
    mousewheel: false, //{UPDATED} Boolean: Requires jquery.mousewheel.js (https://github.com/brandonaaron/jquery-mousewheel) - Allows slider navigating via mousewheel
    pausePlay: false, //Boolean: Create pause/play dynamic element
    pauseText: "Pause", //String: Set the text for the "pause" pausePlay item
    playText: "Play", //String: Set the text for the "play" pausePlay item

    // Special properties
    controlsContainer: "", //{UPDATED} jQuery Object/Selector: Declare which container the navigation elements should be appended too. Default container is the FlexSlider element. Example use would be $(".flexslider-container"). Property is ignored if given element is not found.
    manualControls: "", //{UPDATED} jQuery Object/Selector: Declare custom control navigation. Examples would be $(".flex-control-nav li") or "#tabs-nav li img", etc. The number of elements in your controlNav should match the number of slides/tabs.
    customDirectionNav: "", //{NEW} jQuery Object/Selector: Custom prev / next button. Must be two jQuery elements. In order to make the events work they have to have the classes "prev" and "next" (plus namespace)
    sync: "", //{NEW} Selector: Mirror the actions performed on this slider with another slider. Use with care.
    asNavFor: "", //{NEW} Selector: Internal property exposed for turning the slider into a thumbnail navigation for another slider

    // Carousel Options
    itemWidth: 0, //{NEW} Integer: Box-model width of individual carousel items, including horizontal borders and padding.
    itemMargin: 0, //{NEW} Integer: Margin between carousel items.
    minItems: 1, //{NEW} Integer: Minimum number of carousel items that should be visible. Items will resize fluidly when below this.
    maxItems: 0, //{NEW} Integer: Maxmimum number of carousel items that should be visible. Items will resize fluidly when above this limit.
    move: 0, //{NEW} Integer: Number of carousel items that should move on animation. If 0, slider will move all visible items.
    allowOneSlide: true, //{NEW} Boolean: Whether or not to allow a slider comprised of a single slide

    // Callback API
    start: function start() {}, //Callback: function(slider) - Fires when the slider loads the first slide
    before: function before() {}, //Callback: function(slider) - Fires asynchronously with each slider animation
    after: function after() {}, //Callback: function(slider) - Fires after each slider animation completes
    end: function end() {}, //Callback: function(slider) - Fires when the slider reaches the last slide (asynchronous)
    added: function added() {}, //{NEW} Callback: function(slider) - Fires after a slide is added
    removed: function removed() {}, //{NEW} Callback: function(slider) - Fires after a slide is removed
    init: function init() {} //{NEW} Callback: function(slider) - Fires after the slider is initially setup
  };

  //FlexSlider: Plugin Function
  $.fn.flexslider = function (options) {
    if (options === undefined) {
      options = {};
    }

    if ((typeof options === "undefined" ? "undefined" : _typeof(options)) === "object") {
      return this.each(function () {
        var $this = $(this),
            selector = options.selector ? options.selector : ".slides > li",
            $slides = $this.find(selector);

        if ($slides.length === 1 && options.allowOneSlide === false || $slides.length === 0) {
          $slides.fadeIn(400);
          if (options.start) {
            options.start($this);
          }
        } else if ($this.data('flexslider') === undefined) {
          new $.flexslider(this, options);
        }
      });
    } else {
      // Helper strings to quickly perform functions on the slider
      var $slider = $(this).data('flexslider');
      switch (options) {
        case "play":
          $slider.play();break;
        case "pause":
          $slider.pause();break;
        case "stop":
          $slider.stop();break;
        case "next":
          $slider.flexAnimate($slider.getTarget("next"), true);break;
        case "prev":
        case "previous":
          $slider.flexAnimate($slider.getTarget("prev"), true);break;
        default:
          if (typeof options === "number") {
            $slider.flexAnimate(options, true);
          }
      }
    }
  };
})(jQuery);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBsdWdpbnMvanF1ZXJ5LmZsZXhzbGlkZXIuanMiXSwibmFtZXMiOlsiJCIsImZvY3VzZWQiLCJmbGV4c2xpZGVyIiwiZWwiLCJvcHRpb25zIiwic2xpZGVyIiwidmFycyIsImV4dGVuZCIsImRlZmF1bHRzIiwibmFtZXNwYWNlIiwibXNHZXN0dXJlIiwid2luZG93IiwibmF2aWdhdG9yIiwibXNQb2ludGVyRW5hYmxlZCIsIk1TR2VzdHVyZSIsInRvdWNoIiwiRG9jdW1lbnRUb3VjaCIsImRvY3VtZW50IiwiZXZlbnRUeXBlIiwid2F0Y2hlZEV2ZW50Iiwid2F0Y2hlZEV2ZW50Q2xlYXJUaW1lciIsInZlcnRpY2FsIiwiZGlyZWN0aW9uIiwicmV2ZXJzZSIsImNhcm91c2VsIiwiaXRlbVdpZHRoIiwiZmFkZSIsImFuaW1hdGlvbiIsImFzTmF2IiwiYXNOYXZGb3IiLCJtZXRob2RzIiwiZGF0YSIsImluaXQiLCJhbmltYXRpbmciLCJjdXJyZW50U2xpZGUiLCJwYXJzZUludCIsInN0YXJ0QXQiLCJpc05hTiIsImFuaW1hdGluZ1RvIiwiYXRFbmQiLCJsYXN0IiwiY29udGFpbmVyU2VsZWN0b3IiLCJzZWxlY3RvciIsInN1YnN0ciIsInNlYXJjaCIsInNsaWRlcyIsImNvbnRhaW5lciIsImNvdW50IiwibGVuZ3RoIiwic3luY0V4aXN0cyIsInN5bmMiLCJwcm9wIiwiYXJncyIsIm1hbnVhbFBhdXNlIiwic3RvcHBlZCIsInN0YXJ0ZWQiLCJzdGFydFRpbWVvdXQiLCJ0cmFuc2l0aW9ucyIsInZpZGVvIiwidXNlQ1NTIiwib2JqIiwiY3JlYXRlRWxlbWVudCIsInByb3BzIiwiaSIsInN0eWxlIiwidW5kZWZpbmVkIiwicGZ4IiwicmVwbGFjZSIsInRvTG93ZXJDYXNlIiwiZW5zdXJlQW5pbWF0aW9uRW5kIiwiY29udHJvbHNDb250YWluZXIiLCJtYW51YWxDb250cm9scyIsImN1c3RvbURpcmVjdGlvbk5hdiIsInJhbmRvbWl6ZSIsInNvcnQiLCJNYXRoIiwicm91bmQiLCJyYW5kb20iLCJlbXB0eSIsImFwcGVuZCIsImRvTWF0aCIsInNldHVwIiwiY29udHJvbE5hdiIsImRpcmVjdGlvbk5hdiIsImtleWJvYXJkIiwibXVsdGlwbGVLZXlib2FyZCIsImJpbmQiLCJldmVudCIsImtleWNvZGUiLCJrZXlDb2RlIiwidGFyZ2V0IiwiZ2V0VGFyZ2V0IiwiZmxleEFuaW1hdGUiLCJwYXVzZU9uQWN0aW9uIiwibW91c2V3aGVlbCIsImRlbHRhIiwiZGVsdGFYIiwiZGVsdGFZIiwicHJldmVudERlZmF1bHQiLCJwYXVzZVBsYXkiLCJzbGlkZXNob3ciLCJwYXVzZUludmlzaWJsZSIsInBhdXNlT25Ib3ZlciIsImhvdmVyIiwibWFudWFsUGxheSIsInBhdXNlIiwicGxheSIsImlzSGlkZGVuIiwiaW5pdERlbGF5Iiwic2V0VGltZW91dCIsInNtb290aEhlaWdodCIsInJlc2l6ZSIsImZpbmQiLCJhdHRyIiwic3RhcnQiLCJmbG9vciIsIm1vdmUiLCJjdXJyZW50SXRlbSIsInJlbW92ZUNsYXNzIiwiZXEiLCJhZGRDbGFzcyIsIm9uIiwiZSIsIiRzbGlkZSIsImluZGV4IiwicG9zRnJvbUxlZnQiLCJvZmZzZXQiLCJsZWZ0Iiwic2Nyb2xsTGVmdCIsImhhc0NsYXNzIiwiX3NsaWRlciIsImVhY2giLCJ0aGF0IiwiX2dlc3R1cmUiLCJhZGRFdmVudExpc3RlbmVyIiwiY3VycmVudFRhcmdldCIsImFkZFBvaW50ZXIiLCJwb2ludGVySWQiLCJzZXR1cFBhZ2luZyIsInNldHVwTWFudWFsIiwidHlwZSIsImoiLCJpdGVtIiwic2xpZGUiLCJjb250cm9sTmF2U2NhZmZvbGQiLCJwYWdpbmdDb3VudCIsImFsdFRleHQiLCJ0aHVtYkNhcHRpb25zIiwiY2FwdG4iLCJzZXQiLCJhY3RpdmUiLCJkZWxlZ2F0ZSIsIiR0aGlzIiwic2V0VG9DbGVhcldhdGNoZWRFdmVudCIsInVwZGF0ZSIsImFjdGlvbiIsInBvcyIsInJlbW92ZSIsImNsb3Nlc3QiLCJkaXJlY3Rpb25OYXZTY2FmZm9sZCIsInByZXZUZXh0IiwibmV4dFRleHQiLCJkaXNhYmxlZENsYXNzIiwiYW5pbWF0aW9uTG9vcCIsImZpbHRlciIsInJlbW92ZUF0dHIiLCJwYXVzZVBsYXlTY2FmZm9sZCIsInN0YXRlIiwiaHRtbCIsInBsYXlUZXh0IiwicGF1c2VUZXh0Iiwic3RhcnRYIiwic3RhcnRZIiwiY3dpZHRoIiwiZHgiLCJzdGFydFQiLCJvblRvdWNoU3RhcnQiLCJvblRvdWNoTW92ZSIsIm9uVG91Y2hFbmQiLCJzY3JvbGxpbmciLCJsb2NhbFgiLCJsb2NhbFkiLCJhY2NEeCIsInRvdWNoZXMiLCJoIiwidyIsIk51bWJlciIsIkRhdGUiLCJwYWdlWCIsInBhZ2VZIiwibGltaXQiLCJpdGVtVyIsIml0ZW1NYXJnaW4iLCJjbG9uZU9mZnNldCIsImFicyIsImZ4bXMiLCJzZXRQcm9wcyIsInJlbW92ZUV2ZW50TGlzdGVuZXIiLCJ1cGRhdGVEeCIsImNhbkFkdmFuY2UiLCJvbk1TUG9pbnRlckRvd24iLCJzdG9wUHJvcGFnYXRpb24iLCJvbk1TR2VzdHVyZUNoYW5nZSIsInRyYW5zWCIsInRyYW5zbGF0aW9uWCIsInRyYW5zWSIsInRyYW5zbGF0aW9uWSIsImRldGFpbCIsIk1TR0VTVFVSRV9GTEFHX0lORVJUSUEiLCJzZXRJbW1lZGlhdGUiLCJzdG9wIiwib25NU0dlc3R1cmVFbmQiLCJtc1RvdWNoQWN0aW9uIiwiaXMiLCJ3aWR0aCIsImNvbXB1dGVkVyIsInZpZXdwb3J0IiwiaGVpZ2h0IiwibmV3U2xpZGVzIiwiZHVyIiwiJG9iaiIsImFuaW1hdGUiLCJpbm5lckhlaWdodCIsImNzcyIsInBsYXlpbmciLCJ1bmlxdWVJRCIsIiRjbG9uZSIsImFkZCIsInZpc1Byb3AiLCJnZXRIaWRkZW5Qcm9wIiwiZXZ0bmFtZSIsImNsZWFyVGltZW91dCIsInByZWZpeGVzIiwib3ZlcnJpZGUiLCJ3aXRoU3luYyIsImZyb21OYXYiLCJtYXN0ZXIiLCJjZWlsIiwidmlzaWJsZSIsImJlZm9yZSIsImVuZCIsImRpbWVuc2lvbiIsIm1hcmdpbiIsInNsaWRlU3RyaW5nIiwiY2FsY05leHQiLCJhbmltYXRpb25TcGVlZCIsInVuYmluZCIsIndyYXB1cCIsImVhc2luZyIsImFmdGVyIiwiYW5pbWF0ZVNsaWRlcyIsImNsZWFySW50ZXJ2YWwiLCJhbmltYXRlZFNsaWRlcyIsInNldEludGVydmFsIiwic2xpZGVzaG93U3BlZWQiLCJkaXIiLCJzcGVjaWFsIiwicG9zQ2hlY2siLCJwb3NDYWxjIiwic2xpZGVyT2Zmc2V0IiwiYXJyIiwiYXBwZW5kVG8iLCJjbG9uZUNvdW50IiwibWFrZUFycmF5IiwiZmlyc3QiLCJjbG9uZSIsInByZXBlbmQiLCJjb21wdXRlZE0iLCJmYWRlRmlyc3RTbGlkZSIsInNsaWRlTWFyZ2luIiwibWluSXRlbXMiLCJtYXhJdGVtcyIsImJveFBhZGRpbmciLCJvdXRlcldpZHRoIiwiaXRlbVQiLCJpdGVtTSIsIm1pblciLCJtYXhXIiwiYWRkU2xpZGUiLCJhZGRlZCIsInJlbW92ZVNsaWRlIiwicmVtb3ZlZCIsImJsdXIiLCJmb2N1cyIsImFsbG93T25lU2xpZGUiLCJmbiIsIiRzbGlkZXMiLCJmYWRlSW4iLCIkc2xpZGVyIiwialF1ZXJ5Il0sIm1hcHBpbmdzIjoiOzs7O0FBQUE7Ozs7O0FBS0E7QUFDQSxDQUFDLFVBQVVBLENBQVYsRUFBYTs7QUFFWixNQUFJQyxVQUFVLElBQWQ7O0FBRUE7QUFDQUQsSUFBRUUsVUFBRixHQUFlLFVBQVNDLEVBQVQsRUFBYUMsT0FBYixFQUFzQjtBQUNuQyxRQUFJQyxTQUFTTCxFQUFFRyxFQUFGLENBQWI7O0FBRUE7QUFDQUUsV0FBT0MsSUFBUCxHQUFjTixFQUFFTyxNQUFGLENBQVMsRUFBVCxFQUFhUCxFQUFFRSxVQUFGLENBQWFNLFFBQTFCLEVBQW9DSixPQUFwQyxDQUFkOztBQUVBLFFBQUlLLFlBQVlKLE9BQU9DLElBQVAsQ0FBWUcsU0FBNUI7QUFBQSxRQUNJQyxZQUFZQyxPQUFPQyxTQUFQLElBQW9CRCxPQUFPQyxTQUFQLENBQWlCQyxnQkFBckMsSUFBeURGLE9BQU9HLFNBRGhGO0FBQUEsUUFFSUMsUUFBUSxDQUFHLGtCQUFrQkosTUFBcEIsSUFBZ0NELFNBQWhDLElBQTZDQyxPQUFPSyxhQUFQLElBQXdCQyxvQkFBb0JELGFBQTFGLEtBQTRHWCxPQUFPQyxJQUFQLENBQVlTLEtBRnBJOztBQUdJO0FBQ0FHLGdCQUFZLGtDQUpoQjtBQUFBLFFBS0lDLGVBQWUsRUFMbkI7QUFBQSxRQU1JQyxzQkFOSjtBQUFBLFFBT0lDLFdBQVdoQixPQUFPQyxJQUFQLENBQVlnQixTQUFaLEtBQTBCLFVBUHpDO0FBQUEsUUFRSUMsVUFBVWxCLE9BQU9DLElBQVAsQ0FBWWlCLE9BUjFCO0FBQUEsUUFTSUMsV0FBWW5CLE9BQU9DLElBQVAsQ0FBWW1CLFNBQVosR0FBd0IsQ0FUeEM7QUFBQSxRQVVJQyxPQUFPckIsT0FBT0MsSUFBUCxDQUFZcUIsU0FBWixLQUEwQixNQVZyQztBQUFBLFFBV0lDLFFBQVF2QixPQUFPQyxJQUFQLENBQVl1QixRQUFaLEtBQXlCLEVBWHJDO0FBQUEsUUFZSUMsVUFBVSxFQVpkOztBQWNBO0FBQ0E5QixNQUFFK0IsSUFBRixDQUFPNUIsRUFBUCxFQUFXLFlBQVgsRUFBeUJFLE1BQXpCOztBQUVBO0FBQ0F5QixjQUFVO0FBQ1JFLFlBQU0sZ0JBQVc7QUFDZjNCLGVBQU80QixTQUFQLEdBQW1CLEtBQW5CO0FBQ0E7QUFDQTVCLGVBQU82QixZQUFQLEdBQXNCQyxTQUFZOUIsT0FBT0MsSUFBUCxDQUFZOEIsT0FBWixHQUFzQi9CLE9BQU9DLElBQVAsQ0FBWThCLE9BQWxDLEdBQTRDLENBQXhELEVBQTRELEVBQTVELENBQXRCO0FBQ0EsWUFBS0MsTUFBT2hDLE9BQU82QixZQUFkLENBQUwsRUFBb0M7QUFBRTdCLGlCQUFPNkIsWUFBUCxHQUFzQixDQUF0QjtBQUEwQjtBQUNoRTdCLGVBQU9pQyxXQUFQLEdBQXFCakMsT0FBTzZCLFlBQTVCO0FBQ0E3QixlQUFPa0MsS0FBUCxHQUFnQmxDLE9BQU82QixZQUFQLEtBQXdCLENBQXhCLElBQTZCN0IsT0FBTzZCLFlBQVAsS0FBd0I3QixPQUFPbUMsSUFBNUU7QUFDQW5DLGVBQU9vQyxpQkFBUCxHQUEyQnBDLE9BQU9DLElBQVAsQ0FBWW9DLFFBQVosQ0FBcUJDLE1BQXJCLENBQTRCLENBQTVCLEVBQThCdEMsT0FBT0MsSUFBUCxDQUFZb0MsUUFBWixDQUFxQkUsTUFBckIsQ0FBNEIsR0FBNUIsQ0FBOUIsQ0FBM0I7QUFDQXZDLGVBQU93QyxNQUFQLEdBQWdCN0MsRUFBRUssT0FBT0MsSUFBUCxDQUFZb0MsUUFBZCxFQUF3QnJDLE1BQXhCLENBQWhCO0FBQ0FBLGVBQU95QyxTQUFQLEdBQW1COUMsRUFBRUssT0FBT29DLGlCQUFULEVBQTRCcEMsTUFBNUIsQ0FBbkI7QUFDQUEsZUFBTzBDLEtBQVAsR0FBZTFDLE9BQU93QyxNQUFQLENBQWNHLE1BQTdCO0FBQ0E7QUFDQTNDLGVBQU80QyxVQUFQLEdBQW9CakQsRUFBRUssT0FBT0MsSUFBUCxDQUFZNEMsSUFBZCxFQUFvQkYsTUFBcEIsR0FBNkIsQ0FBakQ7QUFDQTtBQUNBLFlBQUkzQyxPQUFPQyxJQUFQLENBQVlxQixTQUFaLEtBQTBCLE9BQTlCLEVBQXVDO0FBQUV0QixpQkFBT0MsSUFBUCxDQUFZcUIsU0FBWixHQUF3QixPQUF4QjtBQUFrQztBQUMzRXRCLGVBQU84QyxJQUFQLEdBQWU5QixRQUFELEdBQWEsS0FBYixHQUFxQixZQUFuQztBQUNBaEIsZUFBTytDLElBQVAsR0FBYyxFQUFkO0FBQ0E7QUFDQS9DLGVBQU9nRCxXQUFQLEdBQXFCLEtBQXJCO0FBQ0FoRCxlQUFPaUQsT0FBUCxHQUFpQixLQUFqQjtBQUNBO0FBQ0FqRCxlQUFPa0QsT0FBUCxHQUFpQixLQUFqQjtBQUNBbEQsZUFBT21ELFlBQVAsR0FBc0IsSUFBdEI7QUFDQTtBQUNBbkQsZUFBT29ELFdBQVAsR0FBcUIsQ0FBQ3BELE9BQU9DLElBQVAsQ0FBWW9ELEtBQWIsSUFBc0IsQ0FBQ2hDLElBQXZCLElBQStCckIsT0FBT0MsSUFBUCxDQUFZcUQsTUFBM0MsSUFBc0QsWUFBVztBQUNwRixjQUFJQyxNQUFNM0MsU0FBUzRDLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBVjtBQUFBLGNBQ0lDLFFBQVEsQ0FBQyxxQkFBRCxFQUF3QixtQkFBeEIsRUFBNkMsZ0JBQTdDLEVBQStELGNBQS9ELEVBQStFLGVBQS9FLENBRFo7QUFFQSxlQUFLLElBQUlDLENBQVQsSUFBY0QsS0FBZCxFQUFxQjtBQUNuQixnQkFBS0YsSUFBSUksS0FBSixDQUFXRixNQUFNQyxDQUFOLENBQVgsTUFBMEJFLFNBQS9CLEVBQTJDO0FBQ3pDNUQscUJBQU82RCxHQUFQLEdBQWFKLE1BQU1DLENBQU4sRUFBU0ksT0FBVCxDQUFpQixhQUFqQixFQUErQixFQUEvQixFQUFtQ0MsV0FBbkMsRUFBYjtBQUNBL0QscUJBQU84QyxJQUFQLEdBQWMsTUFBTTlDLE9BQU82RCxHQUFiLEdBQW1CLFlBQWpDO0FBQ0EscUJBQU8sSUFBUDtBQUNEO0FBQ0Y7QUFDRCxpQkFBTyxLQUFQO0FBQ0QsU0FYMEUsRUFBM0U7QUFZQTdELGVBQU9nRSxrQkFBUCxHQUE0QixFQUE1QjtBQUNBO0FBQ0EsWUFBSWhFLE9BQU9DLElBQVAsQ0FBWWdFLGlCQUFaLEtBQWtDLEVBQXRDLEVBQTBDakUsT0FBT2lFLGlCQUFQLEdBQTJCdEUsRUFBRUssT0FBT0MsSUFBUCxDQUFZZ0UsaUJBQWQsRUFBaUN0QixNQUFqQyxHQUEwQyxDQUExQyxJQUErQ2hELEVBQUVLLE9BQU9DLElBQVAsQ0FBWWdFLGlCQUFkLENBQTFFO0FBQzFDO0FBQ0EsWUFBSWpFLE9BQU9DLElBQVAsQ0FBWWlFLGNBQVosS0FBK0IsRUFBbkMsRUFBdUNsRSxPQUFPa0UsY0FBUCxHQUF3QnZFLEVBQUVLLE9BQU9DLElBQVAsQ0FBWWlFLGNBQWQsRUFBOEJ2QixNQUE5QixHQUF1QyxDQUF2QyxJQUE0Q2hELEVBQUVLLE9BQU9DLElBQVAsQ0FBWWlFLGNBQWQsQ0FBcEU7O0FBRXZDO0FBQ0EsWUFBSWxFLE9BQU9DLElBQVAsQ0FBWWtFLGtCQUFaLEtBQW1DLEVBQXZDLEVBQTJDbkUsT0FBT21FLGtCQUFQLEdBQTRCeEUsRUFBRUssT0FBT0MsSUFBUCxDQUFZa0Usa0JBQWQsRUFBa0N4QixNQUFsQyxLQUE2QyxDQUE3QyxJQUFrRGhELEVBQUVLLE9BQU9DLElBQVAsQ0FBWWtFLGtCQUFkLENBQTlFOztBQUUzQztBQUNBLFlBQUluRSxPQUFPQyxJQUFQLENBQVltRSxTQUFoQixFQUEyQjtBQUN6QnBFLGlCQUFPd0MsTUFBUCxDQUFjNkIsSUFBZCxDQUFtQixZQUFXO0FBQUUsbUJBQVFDLEtBQUtDLEtBQUwsQ0FBV0QsS0FBS0UsTUFBTCxFQUFYLElBQTBCLEdBQWxDO0FBQXlDLFdBQXpFO0FBQ0F4RSxpQkFBT3lDLFNBQVAsQ0FBaUJnQyxLQUFqQixHQUF5QkMsTUFBekIsQ0FBZ0MxRSxPQUFPd0MsTUFBdkM7QUFDRDs7QUFFRHhDLGVBQU8yRSxNQUFQOztBQUVBO0FBQ0EzRSxlQUFPNEUsS0FBUCxDQUFhLE1BQWI7O0FBRUE7QUFDQSxZQUFJNUUsT0FBT0MsSUFBUCxDQUFZNEUsVUFBaEIsRUFBNEI7QUFBRXBELGtCQUFRb0QsVUFBUixDQUFtQkQsS0FBbkI7QUFBNkI7O0FBRTNEO0FBQ0EsWUFBSTVFLE9BQU9DLElBQVAsQ0FBWTZFLFlBQWhCLEVBQThCO0FBQUVyRCxrQkFBUXFELFlBQVIsQ0FBcUJGLEtBQXJCO0FBQStCOztBQUUvRDtBQUNBLFlBQUk1RSxPQUFPQyxJQUFQLENBQVk4RSxRQUFaLEtBQXlCcEYsRUFBRUssT0FBT29DLGlCQUFULEVBQTRCTyxNQUE1QixLQUF1QyxDQUF2QyxJQUE0QzNDLE9BQU9DLElBQVAsQ0FBWStFLGdCQUFqRixDQUFKLEVBQXdHO0FBQ3RHckYsWUFBRWlCLFFBQUYsRUFBWXFFLElBQVosQ0FBaUIsT0FBakIsRUFBMEIsVUFBU0MsS0FBVCxFQUFnQjtBQUN4QyxnQkFBSUMsVUFBVUQsTUFBTUUsT0FBcEI7QUFDQSxnQkFBSSxDQUFDcEYsT0FBTzRCLFNBQVIsS0FBc0J1RCxZQUFZLEVBQVosSUFBa0JBLFlBQVksRUFBcEQsQ0FBSixFQUE2RDtBQUMzRCxrQkFBSUUsU0FBVUYsWUFBWSxFQUFiLEdBQW1CbkYsT0FBT3NGLFNBQVAsQ0FBaUIsTUFBakIsQ0FBbkIsR0FDQ0gsWUFBWSxFQUFiLEdBQW1CbkYsT0FBT3NGLFNBQVAsQ0FBaUIsTUFBakIsQ0FBbkIsR0FBOEMsS0FEM0Q7QUFFQXRGLHFCQUFPdUYsV0FBUCxDQUFtQkYsTUFBbkIsRUFBMkJyRixPQUFPQyxJQUFQLENBQVl1RixhQUF2QztBQUNEO0FBQ0YsV0FQRDtBQVFEO0FBQ0Q7QUFDQSxZQUFJeEYsT0FBT0MsSUFBUCxDQUFZd0YsVUFBaEIsRUFBNEI7QUFDMUJ6RixpQkFBT2lGLElBQVAsQ0FBWSxZQUFaLEVBQTBCLFVBQVNDLEtBQVQsRUFBZ0JRLEtBQWhCLEVBQXVCQyxNQUF2QixFQUErQkMsTUFBL0IsRUFBdUM7QUFDL0RWLGtCQUFNVyxjQUFOO0FBQ0EsZ0JBQUlSLFNBQVVLLFFBQVEsQ0FBVCxHQUFjMUYsT0FBT3NGLFNBQVAsQ0FBaUIsTUFBakIsQ0FBZCxHQUF5Q3RGLE9BQU9zRixTQUFQLENBQWlCLE1BQWpCLENBQXREO0FBQ0F0RixtQkFBT3VGLFdBQVAsQ0FBbUJGLE1BQW5CLEVBQTJCckYsT0FBT0MsSUFBUCxDQUFZdUYsYUFBdkM7QUFDRCxXQUpEO0FBS0Q7O0FBRUQ7QUFDQSxZQUFJeEYsT0FBT0MsSUFBUCxDQUFZNkYsU0FBaEIsRUFBMkI7QUFBRXJFLGtCQUFRcUUsU0FBUixDQUFrQmxCLEtBQWxCO0FBQTRCOztBQUV6RDtBQUNBLFlBQUk1RSxPQUFPQyxJQUFQLENBQVk4RixTQUFaLElBQXlCL0YsT0FBT0MsSUFBUCxDQUFZK0YsY0FBekMsRUFBeUQ7QUFBRXZFLGtCQUFRdUUsY0FBUixDQUF1QnJFLElBQXZCO0FBQWdDOztBQUUzRjtBQUNBLFlBQUkzQixPQUFPQyxJQUFQLENBQVk4RixTQUFoQixFQUEyQjtBQUN6QixjQUFJL0YsT0FBT0MsSUFBUCxDQUFZZ0csWUFBaEIsRUFBOEI7QUFDNUJqRyxtQkFBT2tHLEtBQVAsQ0FBYSxZQUFXO0FBQ3RCLGtCQUFJLENBQUNsRyxPQUFPbUcsVUFBUixJQUFzQixDQUFDbkcsT0FBT2dELFdBQWxDLEVBQStDO0FBQUVoRCx1QkFBT29HLEtBQVA7QUFBaUI7QUFDbkUsYUFGRCxFQUVHLFlBQVc7QUFDWixrQkFBSSxDQUFDcEcsT0FBT2dELFdBQVIsSUFBdUIsQ0FBQ2hELE9BQU9tRyxVQUEvQixJQUE2QyxDQUFDbkcsT0FBT2lELE9BQXpELEVBQWtFO0FBQUVqRCx1QkFBT3FHLElBQVA7QUFBZ0I7QUFDckYsYUFKRDtBQUtEO0FBQ0Q7QUFDQTtBQUNBLGNBQUcsQ0FBQ3JHLE9BQU9DLElBQVAsQ0FBWStGLGNBQWIsSUFBK0IsQ0FBQ3ZFLFFBQVF1RSxjQUFSLENBQXVCTSxRQUF2QixFQUFuQyxFQUFzRTtBQUNuRXRHLG1CQUFPQyxJQUFQLENBQVlzRyxTQUFaLEdBQXdCLENBQXpCLEdBQThCdkcsT0FBT21ELFlBQVAsR0FBc0JxRCxXQUFXeEcsT0FBT3FHLElBQWxCLEVBQXdCckcsT0FBT0MsSUFBUCxDQUFZc0csU0FBcEMsQ0FBcEQsR0FBcUd2RyxPQUFPcUcsSUFBUCxFQUFyRztBQUNEO0FBQ0Y7O0FBRUQ7QUFDQSxZQUFJOUUsS0FBSixFQUFXO0FBQUVFLGtCQUFRRixLQUFSLENBQWNxRCxLQUFkO0FBQXdCOztBQUVyQztBQUNBLFlBQUlsRSxTQUFTVixPQUFPQyxJQUFQLENBQVlTLEtBQXpCLEVBQWdDO0FBQUVlLGtCQUFRZixLQUFSO0FBQWtCOztBQUVwRDtBQUNBLFlBQUksQ0FBQ1csSUFBRCxJQUFVQSxRQUFRckIsT0FBT0MsSUFBUCxDQUFZd0csWUFBbEMsRUFBaUQ7QUFBRTlHLFlBQUVXLE1BQUYsRUFBVTJFLElBQVYsQ0FBZSxnQ0FBZixFQUFpRHhELFFBQVFpRixNQUF6RDtBQUFtRTs7QUFFdEgxRyxlQUFPMkcsSUFBUCxDQUFZLEtBQVosRUFBbUJDLElBQW5CLENBQXdCLFdBQXhCLEVBQXFDLE9BQXJDOztBQUVBO0FBQ0FKLG1CQUFXLFlBQVU7QUFDbkJ4RyxpQkFBT0MsSUFBUCxDQUFZNEcsS0FBWixDQUFrQjdHLE1BQWxCO0FBQ0QsU0FGRCxFQUVHLEdBRkg7QUFHRCxPQXhITztBQXlIUnVCLGFBQU87QUFDTHFELGVBQU8saUJBQVc7QUFDaEI1RSxpQkFBT3VCLEtBQVAsR0FBZSxJQUFmO0FBQ0F2QixpQkFBT2lDLFdBQVAsR0FBcUJxQyxLQUFLd0MsS0FBTCxDQUFXOUcsT0FBTzZCLFlBQVAsR0FBb0I3QixPQUFPK0csSUFBdEMsQ0FBckI7QUFDQS9HLGlCQUFPZ0gsV0FBUCxHQUFxQmhILE9BQU82QixZQUE1QjtBQUNBN0IsaUJBQU93QyxNQUFQLENBQWN5RSxXQUFkLENBQTBCN0csWUFBWSxjQUF0QyxFQUFzRDhHLEVBQXRELENBQXlEbEgsT0FBT2dILFdBQWhFLEVBQTZFRyxRQUE3RSxDQUFzRi9HLFlBQVksY0FBbEc7QUFDQSxjQUFHLENBQUNDLFNBQUosRUFBYztBQUNWTCxtQkFBT3dDLE1BQVAsQ0FBYzRFLEVBQWQsQ0FBaUJ2RyxTQUFqQixFQUE0QixVQUFTd0csQ0FBVCxFQUFXO0FBQ3JDQSxnQkFBRXhCLGNBQUY7QUFDQSxrQkFBSXlCLFNBQVMzSCxFQUFFLElBQUYsQ0FBYjtBQUFBLGtCQUNJMEYsU0FBU2lDLE9BQU9DLEtBQVAsRUFEYjtBQUVBLGtCQUFJQyxjQUFjRixPQUFPRyxNQUFQLEdBQWdCQyxJQUFoQixHQUF1Qi9ILEVBQUVLLE1BQUYsRUFBVTJILFVBQVYsRUFBekMsQ0FKcUMsQ0FJNEI7QUFDakUsa0JBQUlILGVBQWUsQ0FBZixJQUFvQkYsT0FBT00sUUFBUCxDQUFpQnhILFlBQVksY0FBN0IsQ0FBeEIsRUFBd0U7QUFDdEVKLHVCQUFPdUYsV0FBUCxDQUFtQnZGLE9BQU9zRixTQUFQLENBQWlCLE1BQWpCLENBQW5CLEVBQTZDLElBQTdDO0FBQ0QsZUFGRCxNQUVPLElBQUksQ0FBQzNGLEVBQUVLLE9BQU9DLElBQVAsQ0FBWXVCLFFBQWQsRUFBd0JFLElBQXhCLENBQTZCLFlBQTdCLEVBQTJDRSxTQUE1QyxJQUF5RCxDQUFDMEYsT0FBT00sUUFBUCxDQUFnQnhILFlBQVksY0FBNUIsQ0FBOUQsRUFBMkc7QUFDaEhKLHVCQUFPaUIsU0FBUCxHQUFvQmpCLE9BQU9nSCxXQUFQLEdBQXFCM0IsTUFBdEIsR0FBZ0MsTUFBaEMsR0FBeUMsTUFBNUQ7QUFDQXJGLHVCQUFPdUYsV0FBUCxDQUFtQkYsTUFBbkIsRUFBMkJyRixPQUFPQyxJQUFQLENBQVl1RixhQUF2QyxFQUFzRCxLQUF0RCxFQUE2RCxJQUE3RCxFQUFtRSxJQUFuRTtBQUNEO0FBQ0YsYUFYRDtBQVlILFdBYkQsTUFhSztBQUNEMUYsZUFBRytILE9BQUgsR0FBYTdILE1BQWI7QUFDQUEsbUJBQU93QyxNQUFQLENBQWNzRixJQUFkLENBQW1CLFlBQVc7QUFDMUIsa0JBQUlDLE9BQU8sSUFBWDtBQUNBQSxtQkFBS0MsUUFBTCxHQUFnQixJQUFJdkgsU0FBSixFQUFoQjtBQUNBc0gsbUJBQUtDLFFBQUwsQ0FBYzNDLE1BQWQsR0FBdUIwQyxJQUF2QjtBQUNBQSxtQkFBS0UsZ0JBQUwsQ0FBc0IsZUFBdEIsRUFBdUMsVUFBVVosQ0FBVixFQUFZO0FBQy9DQSxrQkFBRXhCLGNBQUY7QUFDQSxvQkFBR3dCLEVBQUVhLGFBQUYsQ0FBZ0JGLFFBQW5CLEVBQTZCO0FBQzNCWCxvQkFBRWEsYUFBRixDQUFnQkYsUUFBaEIsQ0FBeUJHLFVBQXpCLENBQW9DZCxFQUFFZSxTQUF0QztBQUNEO0FBQ0osZUFMRCxFQUtHLEtBTEg7QUFNQUwsbUJBQUtFLGdCQUFMLENBQXNCLGNBQXRCLEVBQXNDLFVBQVVaLENBQVYsRUFBWTtBQUM5Q0Esa0JBQUV4QixjQUFGO0FBQ0Esb0JBQUl5QixTQUFTM0gsRUFBRSxJQUFGLENBQWI7QUFBQSxvQkFDSTBGLFNBQVNpQyxPQUFPQyxLQUFQLEVBRGI7QUFFQSxvQkFBSSxDQUFDNUgsRUFBRUssT0FBT0MsSUFBUCxDQUFZdUIsUUFBZCxFQUF3QkUsSUFBeEIsQ0FBNkIsWUFBN0IsRUFBMkNFLFNBQTVDLElBQXlELENBQUMwRixPQUFPTSxRQUFQLENBQWdCLFFBQWhCLENBQTlELEVBQXlGO0FBQ3JGNUgseUJBQU9pQixTQUFQLEdBQW9CakIsT0FBT2dILFdBQVAsR0FBcUIzQixNQUF0QixHQUFnQyxNQUFoQyxHQUF5QyxNQUE1RDtBQUNBckYseUJBQU91RixXQUFQLENBQW1CRixNQUFuQixFQUEyQnJGLE9BQU9DLElBQVAsQ0FBWXVGLGFBQXZDLEVBQXNELEtBQXRELEVBQTZELElBQTdELEVBQW1FLElBQW5FO0FBQ0g7QUFDSixlQVJEO0FBU0gsYUFuQkQ7QUFvQkg7QUFDRjtBQTFDSSxPQXpIQztBQXFLUlgsa0JBQVk7QUFDVkQsZUFBTyxpQkFBVztBQUNoQixjQUFJLENBQUM1RSxPQUFPa0UsY0FBWixFQUE0QjtBQUMxQnpDLG9CQUFRb0QsVUFBUixDQUFtQndELFdBQW5CO0FBQ0QsV0FGRCxNQUVPO0FBQUU7QUFDUDVHLG9CQUFRb0QsVUFBUixDQUFtQnlELFdBQW5CO0FBQ0Q7QUFDRixTQVBTO0FBUVZELHFCQUFhLHVCQUFXO0FBQ3RCLGNBQUlFLE9BQVF2SSxPQUFPQyxJQUFQLENBQVk0RSxVQUFaLEtBQTJCLFlBQTVCLEdBQTRDLGdCQUE1QyxHQUErRCxnQkFBMUU7QUFBQSxjQUNJMkQsSUFBSSxDQURSO0FBQUEsY0FFSUMsSUFGSjtBQUFBLGNBR0lDLEtBSEo7O0FBS0ExSSxpQkFBTzJJLGtCQUFQLEdBQTRCaEosRUFBRSxnQkFBZVMsU0FBZixHQUEyQixjQUEzQixHQUE0Q0EsU0FBNUMsR0FBd0RtSSxJQUF4RCxHQUErRCxTQUFqRSxDQUE1Qjs7QUFFQSxjQUFJdkksT0FBTzRJLFdBQVAsR0FBcUIsQ0FBekIsRUFBNEI7QUFDMUIsaUJBQUssSUFBSWxGLElBQUksQ0FBYixFQUFnQkEsSUFBSTFELE9BQU80SSxXQUEzQixFQUF3Q2xGLEdBQXhDLEVBQTZDO0FBQzNDZ0Ysc0JBQVExSSxPQUFPd0MsTUFBUCxDQUFjMEUsRUFBZCxDQUFpQnhELENBQWpCLENBQVI7QUFDQSxrQkFBS0UsY0FBYzhFLE1BQU05QixJQUFOLENBQVksZ0JBQVosQ0FBbkIsRUFBb0Q7QUFBRThCLHNCQUFNOUIsSUFBTixDQUFZLGdCQUFaLEVBQThCLEVBQTlCO0FBQXFDO0FBQzNGLGtCQUFJaUMsVUFBWSxPQUFPSCxNQUFNOUIsSUFBTixDQUFZLGdCQUFaLENBQVQsR0FBNENpQyxVQUFVLFdBQVdILE1BQU05QixJQUFOLENBQVksZ0JBQVosQ0FBWCxHQUE0QyxHQUFsRyxHQUF3RyxFQUF0SDtBQUNBNkIscUJBQVF6SSxPQUFPQyxJQUFQLENBQVk0RSxVQUFaLEtBQTJCLFlBQTVCLEdBQTRDLGVBQWU2RCxNQUFNOUIsSUFBTixDQUFZLFlBQVosQ0FBZixHQUE0QyxHQUE1QyxHQUFrRGlDLE9BQWxELEdBQTRELElBQXhHLEdBQStHLGlCQUFpQkwsQ0FBakIsR0FBcUIsTUFBM0k7QUFDQSxrQkFBSyxpQkFBaUJ4SSxPQUFPQyxJQUFQLENBQVk0RSxVQUE3QixJQUEyQyxTQUFTN0UsT0FBT0MsSUFBUCxDQUFZNkksYUFBckUsRUFBcUY7QUFDbkYsb0JBQUlDLFFBQVFMLE1BQU05QixJQUFOLENBQVksbUJBQVosQ0FBWjtBQUNBLG9CQUFLLE9BQU9tQyxLQUFQLElBQWdCbkYsY0FBY21GLEtBQW5DLEVBQTJDO0FBQUVOLDBCQUFRLGtCQUFrQnJJLFNBQWxCLEdBQThCLFdBQTlCLEdBQTRDMkksS0FBNUMsR0FBb0QsU0FBNUQ7QUFBd0U7QUFDdEg7QUFDRC9JLHFCQUFPMkksa0JBQVAsQ0FBMEJqRSxNQUExQixDQUFpQyxTQUFTK0QsSUFBVCxHQUFnQixPQUFqRDtBQUNBRDtBQUNEO0FBQ0Y7O0FBRUQ7QUFDQ3hJLGlCQUFPaUUsaUJBQVIsR0FBNkJ0RSxFQUFFSyxPQUFPaUUsaUJBQVQsRUFBNEJTLE1BQTVCLENBQW1DMUUsT0FBTzJJLGtCQUExQyxDQUE3QixHQUE2RjNJLE9BQU8wRSxNQUFQLENBQWMxRSxPQUFPMkksa0JBQXJCLENBQTdGO0FBQ0FsSCxrQkFBUW9ELFVBQVIsQ0FBbUJtRSxHQUFuQjs7QUFFQXZILGtCQUFRb0QsVUFBUixDQUFtQm9FLE1BQW5COztBQUVBakosaUJBQU8ySSxrQkFBUCxDQUEwQk8sUUFBMUIsQ0FBbUMsUUFBbkMsRUFBNkNySSxTQUE3QyxFQUF3RCxVQUFTcUUsS0FBVCxFQUFnQjtBQUN0RUEsa0JBQU1XLGNBQU47O0FBRUEsZ0JBQUkvRSxpQkFBaUIsRUFBakIsSUFBdUJBLGlCQUFpQm9FLE1BQU1xRCxJQUFsRCxFQUF3RDtBQUN0RCxrQkFBSVksUUFBUXhKLEVBQUUsSUFBRixDQUFaO0FBQUEsa0JBQ0kwRixTQUFTckYsT0FBTzZFLFVBQVAsQ0FBa0IwQyxLQUFsQixDQUF3QjRCLEtBQXhCLENBRGI7O0FBR0Esa0JBQUksQ0FBQ0EsTUFBTXZCLFFBQU4sQ0FBZXhILFlBQVksUUFBM0IsQ0FBTCxFQUEyQztBQUN6Q0osdUJBQU9pQixTQUFQLEdBQW9Cb0UsU0FBU3JGLE9BQU82QixZQUFqQixHQUFpQyxNQUFqQyxHQUEwQyxNQUE3RDtBQUNBN0IsdUJBQU91RixXQUFQLENBQW1CRixNQUFuQixFQUEyQnJGLE9BQU9DLElBQVAsQ0FBWXVGLGFBQXZDO0FBQ0Q7QUFDRjs7QUFFRDtBQUNBLGdCQUFJMUUsaUJBQWlCLEVBQXJCLEVBQXlCO0FBQ3ZCQSw2QkFBZW9FLE1BQU1xRCxJQUFyQjtBQUNEO0FBQ0Q5RyxvQkFBUTJILHNCQUFSO0FBRUQsV0FuQkQ7QUFvQkQsU0F6RFM7QUEwRFZkLHFCQUFhLHVCQUFXO0FBQ3RCdEksaUJBQU82RSxVQUFQLEdBQW9CN0UsT0FBT2tFLGNBQTNCO0FBQ0F6QyxrQkFBUW9ELFVBQVIsQ0FBbUJvRSxNQUFuQjs7QUFFQWpKLGlCQUFPNkUsVUFBUCxDQUFrQkksSUFBbEIsQ0FBdUJwRSxTQUF2QixFQUFrQyxVQUFTcUUsS0FBVCxFQUFnQjtBQUNoREEsa0JBQU1XLGNBQU47O0FBRUEsZ0JBQUkvRSxpQkFBaUIsRUFBakIsSUFBdUJBLGlCQUFpQm9FLE1BQU1xRCxJQUFsRCxFQUF3RDtBQUN0RCxrQkFBSVksUUFBUXhKLEVBQUUsSUFBRixDQUFaO0FBQUEsa0JBQ0kwRixTQUFTckYsT0FBTzZFLFVBQVAsQ0FBa0IwQyxLQUFsQixDQUF3QjRCLEtBQXhCLENBRGI7O0FBR0Esa0JBQUksQ0FBQ0EsTUFBTXZCLFFBQU4sQ0FBZXhILFlBQVksUUFBM0IsQ0FBTCxFQUEyQztBQUN4Q2lGLHlCQUFTckYsT0FBTzZCLFlBQWpCLEdBQWlDN0IsT0FBT2lCLFNBQVAsR0FBbUIsTUFBcEQsR0FBNkRqQixPQUFPaUIsU0FBUCxHQUFtQixNQUFoRjtBQUNBakIsdUJBQU91RixXQUFQLENBQW1CRixNQUFuQixFQUEyQnJGLE9BQU9DLElBQVAsQ0FBWXVGLGFBQXZDO0FBQ0Q7QUFDRjs7QUFFRDtBQUNBLGdCQUFJMUUsaUJBQWlCLEVBQXJCLEVBQXlCO0FBQ3ZCQSw2QkFBZW9FLE1BQU1xRCxJQUFyQjtBQUNEO0FBQ0Q5RyxvQkFBUTJILHNCQUFSO0FBQ0QsV0FsQkQ7QUFtQkQsU0FqRlM7QUFrRlZKLGFBQUssZUFBVztBQUNkLGNBQUkzRyxXQUFZckMsT0FBT0MsSUFBUCxDQUFZNEUsVUFBWixLQUEyQixZQUE1QixHQUE0QyxLQUE1QyxHQUFvRCxHQUFuRTtBQUNBN0UsaUJBQU82RSxVQUFQLEdBQW9CbEYsRUFBRSxNQUFNUyxTQUFOLEdBQWtCLGlCQUFsQixHQUFzQ2lDLFFBQXhDLEVBQW1EckMsT0FBT2lFLGlCQUFSLEdBQTZCakUsT0FBT2lFLGlCQUFwQyxHQUF3RGpFLE1BQTFHLENBQXBCO0FBQ0QsU0FyRlM7QUFzRlZpSixnQkFBUSxrQkFBVztBQUNqQmpKLGlCQUFPNkUsVUFBUCxDQUFrQm9DLFdBQWxCLENBQThCN0csWUFBWSxRQUExQyxFQUFvRDhHLEVBQXBELENBQXVEbEgsT0FBT2lDLFdBQTlELEVBQTJFa0YsUUFBM0UsQ0FBb0YvRyxZQUFZLFFBQWhHO0FBQ0QsU0F4RlM7QUF5RlZpSixnQkFBUSxnQkFBU0MsTUFBVCxFQUFpQkMsR0FBakIsRUFBc0I7QUFDNUIsY0FBSXZKLE9BQU80SSxXQUFQLEdBQXFCLENBQXJCLElBQTBCVSxXQUFXLEtBQXpDLEVBQWdEO0FBQzlDdEosbUJBQU8ySSxrQkFBUCxDQUEwQmpFLE1BQTFCLENBQWlDL0UsRUFBRSxxQkFBcUJLLE9BQU8wQyxLQUE1QixHQUFvQyxXQUF0QyxDQUFqQztBQUNELFdBRkQsTUFFTyxJQUFJMUMsT0FBTzRJLFdBQVAsS0FBdUIsQ0FBM0IsRUFBOEI7QUFDbkM1SSxtQkFBTzJJLGtCQUFQLENBQTBCaEMsSUFBMUIsQ0FBK0IsSUFBL0IsRUFBcUM2QyxNQUFyQztBQUNELFdBRk0sTUFFQTtBQUNMeEosbUJBQU82RSxVQUFQLENBQWtCcUMsRUFBbEIsQ0FBcUJxQyxHQUFyQixFQUEwQkUsT0FBMUIsQ0FBa0MsSUFBbEMsRUFBd0NELE1BQXhDO0FBQ0Q7QUFDRC9ILGtCQUFRb0QsVUFBUixDQUFtQm1FLEdBQW5CO0FBQ0NoSixpQkFBTzRJLFdBQVAsR0FBcUIsQ0FBckIsSUFBMEI1SSxPQUFPNEksV0FBUCxLQUF1QjVJLE9BQU82RSxVQUFQLENBQWtCbEMsTUFBcEUsR0FBOEUzQyxPQUFPcUosTUFBUCxDQUFjRSxHQUFkLEVBQW1CRCxNQUFuQixDQUE5RSxHQUEyRzdILFFBQVFvRCxVQUFSLENBQW1Cb0UsTUFBbkIsRUFBM0c7QUFDRDtBQW5HUyxPQXJLSjtBQTBRUm5FLG9CQUFjO0FBQ1pGLGVBQU8saUJBQVc7QUFDaEIsY0FBSThFLHVCQUF1Qi9KLEVBQUUsZ0JBQWdCUyxTQUFoQixHQUE0Qiw0QkFBNUIsR0FBMkRBLFNBQTNELEdBQXVFLHNCQUF2RSxHQUFnR0EsU0FBaEcsR0FBNEcsaUJBQTVHLEdBQWdJSixPQUFPQyxJQUFQLENBQVkwSixRQUE1SSxHQUF1SixzQkFBdkosR0FBZ0x2SixTQUFoTCxHQUE0TCxzQkFBNUwsR0FBcU5BLFNBQXJOLEdBQWlPLGlCQUFqTyxHQUFxUEosT0FBT0MsSUFBUCxDQUFZMkosUUFBalEsR0FBNFEsZ0JBQTlRLENBQTNCOztBQUVBO0FBQ0EsY0FBSTVKLE9BQU9tRSxrQkFBWCxFQUErQjtBQUM3Qm5FLG1CQUFPOEUsWUFBUCxHQUFzQjlFLE9BQU9tRSxrQkFBN0I7QUFDRjtBQUNDLFdBSEQsTUFHTyxJQUFJbkUsT0FBT2lFLGlCQUFYLEVBQThCO0FBQ25DdEUsY0FBRUssT0FBT2lFLGlCQUFULEVBQTRCUyxNQUE1QixDQUFtQ2dGLG9CQUFuQztBQUNBMUosbUJBQU84RSxZQUFQLEdBQXNCbkYsRUFBRSxNQUFNUyxTQUFOLEdBQWtCLG9CQUFwQixFQUEwQ0osT0FBT2lFLGlCQUFqRCxDQUF0QjtBQUNELFdBSE0sTUFHQTtBQUNMakUsbUJBQU8wRSxNQUFQLENBQWNnRixvQkFBZDtBQUNBMUosbUJBQU84RSxZQUFQLEdBQXNCbkYsRUFBRSxNQUFNUyxTQUFOLEdBQWtCLG9CQUFwQixFQUEwQ0osTUFBMUMsQ0FBdEI7QUFDRDs7QUFFRHlCLGtCQUFRcUQsWUFBUixDQUFxQnVFLE1BQXJCOztBQUVBckosaUJBQU84RSxZQUFQLENBQW9CRyxJQUFwQixDQUF5QnBFLFNBQXpCLEVBQW9DLFVBQVNxRSxLQUFULEVBQWdCO0FBQ2xEQSxrQkFBTVcsY0FBTjtBQUNBLGdCQUFJUixNQUFKOztBQUVBLGdCQUFJdkUsaUJBQWlCLEVBQWpCLElBQXVCQSxpQkFBaUJvRSxNQUFNcUQsSUFBbEQsRUFBd0Q7QUFDdERsRCx1QkFBVTFGLEVBQUUsSUFBRixFQUFRaUksUUFBUixDQUFpQnhILFlBQVksTUFBN0IsQ0FBRCxHQUF5Q0osT0FBT3NGLFNBQVAsQ0FBaUIsTUFBakIsQ0FBekMsR0FBb0V0RixPQUFPc0YsU0FBUCxDQUFpQixNQUFqQixDQUE3RTtBQUNBdEYscUJBQU91RixXQUFQLENBQW1CRixNQUFuQixFQUEyQnJGLE9BQU9DLElBQVAsQ0FBWXVGLGFBQXZDO0FBQ0Q7O0FBRUQ7QUFDQSxnQkFBSTFFLGlCQUFpQixFQUFyQixFQUF5QjtBQUN2QkEsNkJBQWVvRSxNQUFNcUQsSUFBckI7QUFDRDtBQUNEOUcsb0JBQVEySCxzQkFBUjtBQUNELFdBZEQ7QUFlRCxTQWpDVztBQWtDWkMsZ0JBQVEsa0JBQVc7QUFDakIsY0FBSVEsZ0JBQWdCekosWUFBWSxVQUFoQztBQUNBLGNBQUlKLE9BQU80SSxXQUFQLEtBQXVCLENBQTNCLEVBQThCO0FBQzVCNUksbUJBQU84RSxZQUFQLENBQW9CcUMsUUFBcEIsQ0FBNkIwQyxhQUE3QixFQUE0Q2pELElBQTVDLENBQWlELFVBQWpELEVBQTZELElBQTdEO0FBQ0QsV0FGRCxNQUVPLElBQUksQ0FBQzVHLE9BQU9DLElBQVAsQ0FBWTZKLGFBQWpCLEVBQWdDO0FBQ3JDLGdCQUFJOUosT0FBT2lDLFdBQVAsS0FBdUIsQ0FBM0IsRUFBOEI7QUFDNUJqQyxxQkFBTzhFLFlBQVAsQ0FBb0JtQyxXQUFwQixDQUFnQzRDLGFBQWhDLEVBQStDRSxNQUEvQyxDQUFzRCxNQUFNM0osU0FBTixHQUFrQixNQUF4RSxFQUFnRitHLFFBQWhGLENBQXlGMEMsYUFBekYsRUFBd0dqRCxJQUF4RyxDQUE2RyxVQUE3RyxFQUF5SCxJQUF6SDtBQUNELGFBRkQsTUFFTyxJQUFJNUcsT0FBT2lDLFdBQVAsS0FBdUJqQyxPQUFPbUMsSUFBbEMsRUFBd0M7QUFDN0NuQyxxQkFBTzhFLFlBQVAsQ0FBb0JtQyxXQUFwQixDQUFnQzRDLGFBQWhDLEVBQStDRSxNQUEvQyxDQUFzRCxNQUFNM0osU0FBTixHQUFrQixNQUF4RSxFQUFnRitHLFFBQWhGLENBQXlGMEMsYUFBekYsRUFBd0dqRCxJQUF4RyxDQUE2RyxVQUE3RyxFQUF5SCxJQUF6SDtBQUNELGFBRk0sTUFFQTtBQUNMNUcscUJBQU84RSxZQUFQLENBQW9CbUMsV0FBcEIsQ0FBZ0M0QyxhQUFoQyxFQUErQ0csVUFBL0MsQ0FBMEQsVUFBMUQ7QUFDRDtBQUNGLFdBUk0sTUFRQTtBQUNMaEssbUJBQU84RSxZQUFQLENBQW9CbUMsV0FBcEIsQ0FBZ0M0QyxhQUFoQyxFQUErQ0csVUFBL0MsQ0FBMEQsVUFBMUQ7QUFDRDtBQUNGO0FBakRXLE9BMVFOO0FBNlRSbEUsaUJBQVc7QUFDVGxCLGVBQU8saUJBQVc7QUFDaEIsY0FBSXFGLG9CQUFvQnRLLEVBQUUsaUJBQWlCUyxTQUFqQixHQUE2QixtQ0FBL0IsQ0FBeEI7O0FBRUE7QUFDQSxjQUFJSixPQUFPaUUsaUJBQVgsRUFBOEI7QUFDNUJqRSxtQkFBT2lFLGlCQUFQLENBQXlCUyxNQUF6QixDQUFnQ3VGLGlCQUFoQztBQUNBakssbUJBQU84RixTQUFQLEdBQW1CbkcsRUFBRSxNQUFNUyxTQUFOLEdBQWtCLGFBQXBCLEVBQW1DSixPQUFPaUUsaUJBQTFDLENBQW5CO0FBQ0QsV0FIRCxNQUdPO0FBQ0xqRSxtQkFBTzBFLE1BQVAsQ0FBY3VGLGlCQUFkO0FBQ0FqSyxtQkFBTzhGLFNBQVAsR0FBbUJuRyxFQUFFLE1BQU1TLFNBQU4sR0FBa0IsYUFBcEIsRUFBbUNKLE1BQW5DLENBQW5CO0FBQ0Q7O0FBRUR5QixrQkFBUXFFLFNBQVIsQ0FBa0J1RCxNQUFsQixDQUEwQnJKLE9BQU9DLElBQVAsQ0FBWThGLFNBQWIsR0FBMEIzRixZQUFZLE9BQXRDLEdBQWdEQSxZQUFZLE1BQXJGOztBQUVBSixpQkFBTzhGLFNBQVAsQ0FBaUJiLElBQWpCLENBQXNCcEUsU0FBdEIsRUFBaUMsVUFBU3FFLEtBQVQsRUFBZ0I7QUFDL0NBLGtCQUFNVyxjQUFOOztBQUVBLGdCQUFJL0UsaUJBQWlCLEVBQWpCLElBQXVCQSxpQkFBaUJvRSxNQUFNcUQsSUFBbEQsRUFBd0Q7QUFDdEQsa0JBQUk1SSxFQUFFLElBQUYsRUFBUWlJLFFBQVIsQ0FBaUJ4SCxZQUFZLE9BQTdCLENBQUosRUFBMkM7QUFDekNKLHVCQUFPZ0QsV0FBUCxHQUFxQixJQUFyQjtBQUNBaEQsdUJBQU9tRyxVQUFQLEdBQW9CLEtBQXBCO0FBQ0FuRyx1QkFBT29HLEtBQVA7QUFDRCxlQUpELE1BSU87QUFDTHBHLHVCQUFPZ0QsV0FBUCxHQUFxQixLQUFyQjtBQUNBaEQsdUJBQU9tRyxVQUFQLEdBQW9CLElBQXBCO0FBQ0FuRyx1QkFBT3FHLElBQVA7QUFDRDtBQUNGOztBQUVEO0FBQ0EsZ0JBQUl2RixpQkFBaUIsRUFBckIsRUFBeUI7QUFDdkJBLDZCQUFlb0UsTUFBTXFELElBQXJCO0FBQ0Q7QUFDRDlHLG9CQUFRMkgsc0JBQVI7QUFDRCxXQXBCRDtBQXFCRCxTQXBDUTtBQXFDVEMsZ0JBQVEsZ0JBQVNhLEtBQVQsRUFBZ0I7QUFDckJBLG9CQUFVLE1BQVgsR0FBcUJsSyxPQUFPOEYsU0FBUCxDQUFpQm1CLFdBQWpCLENBQTZCN0csWUFBWSxPQUF6QyxFQUFrRCtHLFFBQWxELENBQTJEL0csWUFBWSxNQUF2RSxFQUErRStKLElBQS9FLENBQW9GbkssT0FBT0MsSUFBUCxDQUFZbUssUUFBaEcsQ0FBckIsR0FBaUlwSyxPQUFPOEYsU0FBUCxDQUFpQm1CLFdBQWpCLENBQTZCN0csWUFBWSxNQUF6QyxFQUFpRCtHLFFBQWpELENBQTBEL0csWUFBWSxPQUF0RSxFQUErRStKLElBQS9FLENBQW9GbkssT0FBT0MsSUFBUCxDQUFZb0ssU0FBaEcsQ0FBakk7QUFDRDtBQXZDUSxPQTdUSDtBQXNXUjNKLGFBQU8saUJBQVc7QUFDaEIsWUFBSTRKLE1BQUo7QUFBQSxZQUNFQyxNQURGO0FBQUEsWUFFRTlDLE1BRkY7QUFBQSxZQUdFK0MsTUFIRjtBQUFBLFlBSUVDLEVBSkY7QUFBQSxZQUtFQyxNQUxGO0FBQUEsWUFNRUMsWUFORjtBQUFBLFlBT0VDLFdBUEY7QUFBQSxZQVFFQyxXQVJGO0FBQUEsWUFTRUMsWUFBWSxLQVRkO0FBQUEsWUFVRUMsU0FBUyxDQVZYO0FBQUEsWUFXRUMsU0FBUyxDQVhYO0FBQUEsWUFZRUMsUUFBUSxDQVpWOztBQWNBLFlBQUcsQ0FBQzVLLFNBQUosRUFBYztBQUNWc0sseUJBQWUsc0JBQVN0RCxDQUFULEVBQVk7QUFDekIsZ0JBQUlySCxPQUFPNEIsU0FBWCxFQUFzQjtBQUNwQnlGLGdCQUFFeEIsY0FBRjtBQUNELGFBRkQsTUFFTyxJQUFPdkYsT0FBT0MsU0FBUCxDQUFpQkMsZ0JBQW5CLElBQXlDNkcsRUFBRTZELE9BQUYsQ0FBVXZJLE1BQVYsS0FBcUIsQ0FBbkUsRUFBdUU7QUFDNUUzQyxxQkFBT29HLEtBQVA7QUFDQTtBQUNBb0UsdUJBQVV4SixRQUFELEdBQWFoQixPQUFPbUwsQ0FBcEIsR0FBd0JuTCxPQUFRb0wsQ0FBekM7QUFDQVYsdUJBQVNXLE9BQU8sSUFBSUMsSUFBSixFQUFQLENBQVQ7QUFDQTs7QUFFQTtBQUNBUCx1QkFBUzFELEVBQUU2RCxPQUFGLENBQVUsQ0FBVixFQUFhSyxLQUF0QjtBQUNBUCx1QkFBUzNELEVBQUU2RCxPQUFGLENBQVUsQ0FBVixFQUFhTSxLQUF0Qjs7QUFFQS9ELHVCQUFVdEcsWUFBWUQsT0FBWixJQUF1QmxCLE9BQU9pQyxXQUFQLEtBQXVCakMsT0FBT21DLElBQXRELEdBQThELENBQTlELEdBQ0NoQixZQUFZRCxPQUFiLEdBQXdCbEIsT0FBT3lMLEtBQVAsR0FBaUIsQ0FBQ3pMLE9BQU8wTCxLQUFQLEdBQWUxTCxPQUFPQyxJQUFQLENBQVkwTCxVQUE1QixJQUEwQzNMLE9BQU8rRyxJQUFsRCxHQUEwRC9HLE9BQU9pQyxXQUF6RyxHQUNDZCxZQUFZbkIsT0FBTzZCLFlBQVAsS0FBd0I3QixPQUFPbUMsSUFBNUMsR0FBb0RuQyxPQUFPeUwsS0FBM0QsR0FDQ3RLLFFBQUQsR0FBYyxDQUFDbkIsT0FBTzBMLEtBQVAsR0FBZTFMLE9BQU9DLElBQVAsQ0FBWTBMLFVBQTVCLElBQTBDM0wsT0FBTytHLElBQWxELEdBQTBEL0csT0FBTzZCLFlBQTlFLEdBQ0NYLE9BQUQsR0FBWSxDQUFDbEIsT0FBT21DLElBQVAsR0FBY25DLE9BQU82QixZQUFyQixHQUFvQzdCLE9BQU80TCxXQUE1QyxJQUEyRHBCLE1BQXZFLEdBQWdGLENBQUN4SyxPQUFPNkIsWUFBUCxHQUFzQjdCLE9BQU80TCxXQUE5QixJQUE2Q3BCLE1BSnRJO0FBS0FGLHVCQUFVdEosUUFBRCxHQUFhZ0ssTUFBYixHQUFzQkQsTUFBL0I7QUFDQVIsdUJBQVV2SixRQUFELEdBQWErSixNQUFiLEdBQXNCQyxNQUEvQjs7QUFFQWxMLGlCQUFHbUksZ0JBQUgsQ0FBb0IsV0FBcEIsRUFBaUMyQyxXQUFqQyxFQUE4QyxLQUE5QztBQUNBOUssaUJBQUdtSSxnQkFBSCxDQUFvQixVQUFwQixFQUFnQzRDLFdBQWhDLEVBQTRDLEtBQTVDO0FBQ0Q7QUFDRixXQXpCRDs7QUEyQkFELHdCQUFjLHFCQUFTdkQsQ0FBVCxFQUFZO0FBQ3hCOztBQUVBMEQscUJBQVMxRCxFQUFFNkQsT0FBRixDQUFVLENBQVYsRUFBYUssS0FBdEI7QUFDQVAscUJBQVMzRCxFQUFFNkQsT0FBRixDQUFVLENBQVYsRUFBYU0sS0FBdEI7O0FBRUFmLGlCQUFNekosUUFBRCxHQUFhc0osU0FBU1UsTUFBdEIsR0FBK0JWLFNBQVNTLE1BQTdDO0FBQ0FELHdCQUFhOUosUUFBRCxHQUFjc0QsS0FBS3VILEdBQUwsQ0FBU3BCLEVBQVQsSUFBZW5HLEtBQUt1SCxHQUFMLENBQVNkLFNBQVNSLE1BQWxCLENBQTdCLEdBQTJEakcsS0FBS3VILEdBQUwsQ0FBU3BCLEVBQVQsSUFBZW5HLEtBQUt1SCxHQUFMLENBQVNiLFNBQVNULE1BQWxCLENBQXRGOztBQUVBLGdCQUFJdUIsT0FBTyxHQUFYOztBQUVBLGdCQUFLLENBQUVoQixTQUFGLElBQWVPLE9BQVEsSUFBSUMsSUFBSixFQUFSLElBQXVCWixNQUF2QixHQUFnQ29CLElBQXBELEVBQTJEO0FBQ3pEekUsZ0JBQUV4QixjQUFGO0FBQ0Esa0JBQUksQ0FBQ3hFLElBQUQsSUFBU3JCLE9BQU9vRCxXQUFwQixFQUFpQztBQUMvQixvQkFBSSxDQUFDcEQsT0FBT0MsSUFBUCxDQUFZNkosYUFBakIsRUFBZ0M7QUFDOUJXLHVCQUFLQSxNQUFLekssT0FBTzZCLFlBQVAsS0FBd0IsQ0FBeEIsSUFBNkI0SSxLQUFLLENBQWxDLElBQXVDekssT0FBTzZCLFlBQVAsS0FBd0I3QixPQUFPbUMsSUFBL0IsSUFBdUNzSSxLQUFLLENBQXBGLEdBQTBGbkcsS0FBS3VILEdBQUwsQ0FBU3BCLEVBQVQsSUFBYUQsTUFBYixHQUFvQixDQUE5RyxHQUFtSCxDQUF2SCxDQUFMO0FBQ0Q7QUFDRHhLLHVCQUFPK0wsUUFBUCxDQUFnQnRFLFNBQVNnRCxFQUF6QixFQUE2QixVQUE3QjtBQUNEO0FBQ0Y7QUFDRixXQXBCRDs7QUFzQkFJLHdCQUFhLG9CQUFTeEQsQ0FBVCxFQUFZO0FBQ3ZCO0FBQ0F2SCxlQUFHa00sbUJBQUgsQ0FBdUIsV0FBdkIsRUFBb0NwQixXQUFwQyxFQUFpRCxLQUFqRDs7QUFFQSxnQkFBSTVLLE9BQU9pQyxXQUFQLEtBQXVCakMsT0FBTzZCLFlBQTlCLElBQThDLENBQUNpSixTQUEvQyxJQUE0RCxFQUFFTCxPQUFPLElBQVQsQ0FBaEUsRUFBZ0Y7QUFDOUUsa0JBQUl3QixXQUFZL0ssT0FBRCxHQUFZLENBQUN1SixFQUFiLEdBQWtCQSxFQUFqQztBQUFBLGtCQUNJcEYsU0FBVTRHLFdBQVcsQ0FBWixHQUFpQmpNLE9BQU9zRixTQUFQLENBQWlCLE1BQWpCLENBQWpCLEdBQTRDdEYsT0FBT3NGLFNBQVAsQ0FBaUIsTUFBakIsQ0FEekQ7O0FBR0Esa0JBQUl0RixPQUFPa00sVUFBUCxDQUFrQjdHLE1BQWxCLE1BQThCZ0csT0FBTyxJQUFJQyxJQUFKLEVBQVAsSUFBcUJaLE1BQXJCLEdBQThCLEdBQTlCLElBQXFDcEcsS0FBS3VILEdBQUwsQ0FBU0ksUUFBVCxJQUFxQixFQUExRCxJQUFnRTNILEtBQUt1SCxHQUFMLENBQVNJLFFBQVQsSUFBcUJ6QixTQUFPLENBQTFILENBQUosRUFBa0k7QUFDaEl4Syx1QkFBT3VGLFdBQVAsQ0FBbUJGLE1BQW5CLEVBQTJCckYsT0FBT0MsSUFBUCxDQUFZdUYsYUFBdkM7QUFDRCxlQUZELE1BRU87QUFDTCxvQkFBSSxDQUFDbkUsSUFBTCxFQUFXO0FBQUVyQix5QkFBT3VGLFdBQVAsQ0FBbUJ2RixPQUFPNkIsWUFBMUIsRUFBd0M3QixPQUFPQyxJQUFQLENBQVl1RixhQUFwRCxFQUFtRSxJQUFuRTtBQUEyRTtBQUN6RjtBQUNGO0FBQ0QxRixlQUFHa00sbUJBQUgsQ0FBdUIsVUFBdkIsRUFBbUNuQixXQUFuQyxFQUErQyxLQUEvQzs7QUFFQVAscUJBQVMsSUFBVDtBQUNBQyxxQkFBUyxJQUFUO0FBQ0FFLGlCQUFLLElBQUw7QUFDQWhELHFCQUFTLElBQVQ7QUFDRCxXQXBCRDs7QUFzQkEzSCxhQUFHbUksZ0JBQUgsQ0FBb0IsWUFBcEIsRUFBa0MwQyxZQUFsQyxFQUFnRCxLQUFoRDtBQUNILFNBekVELE1BeUVLO0FBQUEsY0FTUXdCLGVBVFIsR0FTRCxTQUFTQSxlQUFULENBQXlCOUUsQ0FBekIsRUFBMkI7QUFDdkJBLGNBQUUrRSxlQUFGO0FBQ0EsZ0JBQUlwTSxPQUFPNEIsU0FBWCxFQUFzQjtBQUNsQnlGLGdCQUFFeEIsY0FBRjtBQUNILGFBRkQsTUFFSztBQUNEN0YscUJBQU9vRyxLQUFQO0FBQ0F0RyxpQkFBR2tJLFFBQUgsQ0FBWUcsVUFBWixDQUF1QmQsRUFBRWUsU0FBekI7QUFDQTZDLHNCQUFRLENBQVI7QUFDQVQsdUJBQVV4SixRQUFELEdBQWFoQixPQUFPbUwsQ0FBcEIsR0FBd0JuTCxPQUFRb0wsQ0FBekM7QUFDQVYsdUJBQVNXLE9BQU8sSUFBSUMsSUFBSixFQUFQLENBQVQ7QUFDQTs7QUFFQTdELHVCQUFVdEcsWUFBWUQsT0FBWixJQUF1QmxCLE9BQU9pQyxXQUFQLEtBQXVCakMsT0FBT21DLElBQXRELEdBQThELENBQTlELEdBQ0poQixZQUFZRCxPQUFiLEdBQXdCbEIsT0FBT3lMLEtBQVAsR0FBaUIsQ0FBQ3pMLE9BQU8wTCxLQUFQLEdBQWUxTCxPQUFPQyxJQUFQLENBQVkwTCxVQUE1QixJQUEwQzNMLE9BQU8rRyxJQUFsRCxHQUEwRC9HLE9BQU9pQyxXQUF6RyxHQUNLZCxZQUFZbkIsT0FBTzZCLFlBQVAsS0FBd0I3QixPQUFPbUMsSUFBNUMsR0FBb0RuQyxPQUFPeUwsS0FBM0QsR0FDS3RLLFFBQUQsR0FBYyxDQUFDbkIsT0FBTzBMLEtBQVAsR0FBZTFMLE9BQU9DLElBQVAsQ0FBWTBMLFVBQTVCLElBQTBDM0wsT0FBTytHLElBQWxELEdBQTBEL0csT0FBTzZCLFlBQTlFLEdBQ0tYLE9BQUQsR0FBWSxDQUFDbEIsT0FBT21DLElBQVAsR0FBY25DLE9BQU82QixZQUFyQixHQUFvQzdCLE9BQU80TCxXQUE1QyxJQUEyRHBCLE1BQXZFLEdBQWdGLENBQUN4SyxPQUFPNkIsWUFBUCxHQUFzQjdCLE9BQU80TCxXQUE5QixJQUE2Q3BCLE1BSjdJO0FBS0g7QUFDSixXQTNCQTs7QUFBQSxjQTZCUTZCLGlCQTdCUixHQTZCRCxTQUFTQSxpQkFBVCxDQUEyQmhGLENBQTNCLEVBQThCO0FBQzFCQSxjQUFFK0UsZUFBRjtBQUNBLGdCQUFJcE0sU0FBU3FILEVBQUVoQyxNQUFGLENBQVN3QyxPQUF0QjtBQUNBLGdCQUFHLENBQUM3SCxNQUFKLEVBQVc7QUFDUDtBQUNIO0FBQ0QsZ0JBQUlzTSxTQUFTLENBQUNqRixFQUFFa0YsWUFBaEI7QUFBQSxnQkFDSUMsU0FBUyxDQUFDbkYsRUFBRW9GLFlBRGhCOztBQUdBO0FBQ0F4QixvQkFBUUEsU0FBVWpLLFFBQUQsR0FBYXdMLE1BQWIsR0FBc0JGLE1BQS9CLENBQVI7QUFDQTdCLGlCQUFLUSxLQUFMO0FBQ0FILHdCQUFhOUosUUFBRCxHQUFjc0QsS0FBS3VILEdBQUwsQ0FBU1osS0FBVCxJQUFrQjNHLEtBQUt1SCxHQUFMLENBQVMsQ0FBQ1MsTUFBVixDQUFoQyxHQUFzRGhJLEtBQUt1SCxHQUFMLENBQVNaLEtBQVQsSUFBa0IzRyxLQUFLdUgsR0FBTCxDQUFTLENBQUNXLE1BQVYsQ0FBcEY7O0FBRUEsZ0JBQUduRixFQUFFcUYsTUFBRixLQUFhckYsRUFBRXNGLHNCQUFsQixFQUF5QztBQUNyQ0MsMkJBQWEsWUFBVztBQUNwQjlNLG1CQUFHa0ksUUFBSCxDQUFZNkUsSUFBWjtBQUNILGVBRkQ7O0FBSUE7QUFDSDs7QUFFRCxnQkFBSSxDQUFDL0IsU0FBRCxJQUFjTyxPQUFPLElBQUlDLElBQUosRUFBUCxJQUFxQlosTUFBckIsR0FBOEIsR0FBaEQsRUFBcUQ7QUFDakRyRCxnQkFBRXhCLGNBQUY7QUFDQSxrQkFBSSxDQUFDeEUsSUFBRCxJQUFTckIsT0FBT29ELFdBQXBCLEVBQWlDO0FBQzdCLG9CQUFJLENBQUNwRCxPQUFPQyxJQUFQLENBQVk2SixhQUFqQixFQUFnQztBQUM1QlcsdUJBQUtRLFNBQVVqTCxPQUFPNkIsWUFBUCxLQUF3QixDQUF4QixJQUE2Qm9KLFFBQVEsQ0FBckMsSUFBMENqTCxPQUFPNkIsWUFBUCxLQUF3QjdCLE9BQU9tQyxJQUEvQixJQUF1QzhJLFFBQVEsQ0FBMUYsR0FBZ0czRyxLQUFLdUgsR0FBTCxDQUFTWixLQUFULElBQWtCVCxNQUFsQixHQUEyQixDQUEzSCxHQUFnSSxDQUF6SSxDQUFMO0FBQ0g7QUFDRHhLLHVCQUFPK0wsUUFBUCxDQUFnQnRFLFNBQVNnRCxFQUF6QixFQUE2QixVQUE3QjtBQUNIO0FBQ0o7QUFDSixXQTVEQTs7QUFBQSxjQThEUXFDLGNBOURSLEdBOERELFNBQVNBLGNBQVQsQ0FBd0J6RixDQUF4QixFQUEyQjtBQUN2QkEsY0FBRStFLGVBQUY7QUFDQSxnQkFBSXBNLFNBQVNxSCxFQUFFaEMsTUFBRixDQUFTd0MsT0FBdEI7QUFDQSxnQkFBRyxDQUFDN0gsTUFBSixFQUFXO0FBQ1A7QUFDSDtBQUNELGdCQUFJQSxPQUFPaUMsV0FBUCxLQUF1QmpDLE9BQU82QixZQUE5QixJQUE4QyxDQUFDaUosU0FBL0MsSUFBNEQsRUFBRUwsT0FBTyxJQUFULENBQWhFLEVBQWdGO0FBQzVFLGtCQUFJd0IsV0FBWS9LLE9BQUQsR0FBWSxDQUFDdUosRUFBYixHQUFrQkEsRUFBakM7QUFBQSxrQkFDSXBGLFNBQVU0RyxXQUFXLENBQVosR0FBaUJqTSxPQUFPc0YsU0FBUCxDQUFpQixNQUFqQixDQUFqQixHQUE0Q3RGLE9BQU9zRixTQUFQLENBQWlCLE1BQWpCLENBRHpEOztBQUdBLGtCQUFJdEYsT0FBT2tNLFVBQVAsQ0FBa0I3RyxNQUFsQixNQUE4QmdHLE9BQU8sSUFBSUMsSUFBSixFQUFQLElBQXFCWixNQUFyQixHQUE4QixHQUE5QixJQUFxQ3BHLEtBQUt1SCxHQUFMLENBQVNJLFFBQVQsSUFBcUIsRUFBMUQsSUFBZ0UzSCxLQUFLdUgsR0FBTCxDQUFTSSxRQUFULElBQXFCekIsU0FBTyxDQUExSCxDQUFKLEVBQWtJO0FBQzlIeEssdUJBQU91RixXQUFQLENBQW1CRixNQUFuQixFQUEyQnJGLE9BQU9DLElBQVAsQ0FBWXVGLGFBQXZDO0FBQ0gsZUFGRCxNQUVPO0FBQ0gsb0JBQUksQ0FBQ25FLElBQUwsRUFBVztBQUFFckIseUJBQU91RixXQUFQLENBQW1CdkYsT0FBTzZCLFlBQTFCLEVBQXdDN0IsT0FBT0MsSUFBUCxDQUFZdUYsYUFBcEQsRUFBbUUsSUFBbkU7QUFBMkU7QUFDM0Y7QUFDSjs7QUFFRDhFLHFCQUFTLElBQVQ7QUFDQUMscUJBQVMsSUFBVDtBQUNBRSxpQkFBSyxJQUFMO0FBQ0FoRCxxQkFBUyxJQUFUO0FBQ0F3RCxvQkFBUSxDQUFSO0FBQ0gsV0FwRkE7O0FBQ0RuTCxhQUFHNkQsS0FBSCxDQUFTb0osYUFBVCxHQUF5QixNQUF6QjtBQUNBak4sYUFBR2tJLFFBQUgsR0FBYyxJQUFJdkgsU0FBSixFQUFkO0FBQ0FYLGFBQUdrSSxRQUFILENBQVkzQyxNQUFaLEdBQXFCdkYsRUFBckI7QUFDQUEsYUFBR21JLGdCQUFILENBQW9CLGVBQXBCLEVBQXFDa0UsZUFBckMsRUFBc0QsS0FBdEQ7QUFDQXJNLGFBQUcrSCxPQUFILEdBQWE3SCxNQUFiO0FBQ0FGLGFBQUdtSSxnQkFBSCxDQUFvQixpQkFBcEIsRUFBdUNvRSxpQkFBdkMsRUFBMEQsS0FBMUQ7QUFDQXZNLGFBQUdtSSxnQkFBSCxDQUFvQixjQUFwQixFQUFvQzZFLGNBQXBDLEVBQW9ELEtBQXBEO0FBOEVIO0FBQ0YsT0FwaEJPO0FBcWhCUnBHLGNBQVEsa0JBQVc7QUFDakIsWUFBSSxDQUFDMUcsT0FBTzRCLFNBQVIsSUFBcUI1QixPQUFPZ04sRUFBUCxDQUFVLFVBQVYsQ0FBekIsRUFBZ0Q7QUFDOUMsY0FBSSxDQUFDN0wsUUFBTCxFQUFlO0FBQUVuQixtQkFBTzJFLE1BQVA7QUFBa0I7O0FBRW5DLGNBQUl0RCxJQUFKLEVBQVU7QUFDUjtBQUNBSSxvQkFBUWdGLFlBQVI7QUFDRCxXQUhELE1BR08sSUFBSXRGLFFBQUosRUFBYztBQUFFO0FBQ3JCbkIsbUJBQU93QyxNQUFQLENBQWN5SyxLQUFkLENBQW9Cak4sT0FBT2tOLFNBQTNCO0FBQ0FsTixtQkFBT3FKLE1BQVAsQ0FBY3JKLE9BQU80SSxXQUFyQjtBQUNBNUksbUJBQU8rTCxRQUFQO0FBQ0QsV0FKTSxNQUtGLElBQUkvSyxRQUFKLEVBQWM7QUFBRTtBQUNuQmhCLG1CQUFPbU4sUUFBUCxDQUFnQkMsTUFBaEIsQ0FBdUJwTixPQUFPbUwsQ0FBOUI7QUFDQW5MLG1CQUFPK0wsUUFBUCxDQUFnQi9MLE9BQU9tTCxDQUF2QixFQUEwQixVQUExQjtBQUNELFdBSEksTUFHRTtBQUNMO0FBQ0EsZ0JBQUluTCxPQUFPQyxJQUFQLENBQVl3RyxZQUFoQixFQUE4QjtBQUFFaEYsc0JBQVFnRixZQUFSO0FBQXlCO0FBQ3pEekcsbUJBQU9xTixTQUFQLENBQWlCSixLQUFqQixDQUF1QmpOLE9BQU9rTixTQUE5QjtBQUNBbE4sbUJBQU8rTCxRQUFQLENBQWdCL0wsT0FBT2tOLFNBQXZCLEVBQWtDLFVBQWxDO0FBQ0Q7QUFDRjtBQUNGLE9BM2lCTztBQTRpQlJ6RyxvQkFBYyxzQkFBUzZHLEdBQVQsRUFBYztBQUMxQixZQUFJLENBQUN0TSxRQUFELElBQWFLLElBQWpCLEVBQXVCO0FBQ3JCLGNBQUlrTSxPQUFRbE0sSUFBRCxHQUFTckIsTUFBVCxHQUFrQkEsT0FBT21OLFFBQXBDO0FBQ0NHLGFBQUQsR0FBUUMsS0FBS0MsT0FBTCxDQUFhLEVBQUMsVUFBVXhOLE9BQU93QyxNQUFQLENBQWMwRSxFQUFkLENBQWlCbEgsT0FBT2lDLFdBQXhCLEVBQXFDd0wsV0FBckMsRUFBWCxFQUFiLEVBQTZFSCxHQUE3RSxFQUFrRkksR0FBbEYsQ0FBc0YsVUFBdEYsRUFBa0csU0FBbEcsQ0FBUixHQUF1SEgsS0FBS0UsV0FBTCxDQUFpQnpOLE9BQU93QyxNQUFQLENBQWMwRSxFQUFkLENBQWlCbEgsT0FBT2lDLFdBQXhCLEVBQXFDd0wsV0FBckMsRUFBakIsQ0FBdkg7QUFDRDtBQUNGLE9BampCTztBQWtqQlI1SyxZQUFNLGNBQVN5RyxNQUFULEVBQWlCO0FBQ3JCLFlBQUlpRSxPQUFPNU4sRUFBRUssT0FBT0MsSUFBUCxDQUFZNEMsSUFBZCxFQUFvQm5CLElBQXBCLENBQXlCLFlBQXpCLENBQVg7QUFBQSxZQUNJMkQsU0FBU3JGLE9BQU9pQyxXQURwQjs7QUFHQSxnQkFBUXFILE1BQVI7QUFDRSxlQUFLLFNBQUw7QUFBZ0JpRSxpQkFBS2hJLFdBQUwsQ0FBaUJGLE1BQWpCLEVBQXlCckYsT0FBT0MsSUFBUCxDQUFZdUYsYUFBckMsRUFBb0QsS0FBcEQsRUFBMkQsSUFBM0QsRUFBa0U7QUFDbEYsZUFBSyxNQUFMO0FBQWEsZ0JBQUksQ0FBQytILEtBQUtJLE9BQU4sSUFBaUIsQ0FBQ0osS0FBS2hNLEtBQTNCLEVBQWtDO0FBQUVnTSxtQkFBS2xILElBQUw7QUFBYyxhQUFDO0FBQ2hFLGVBQUssT0FBTDtBQUFja0gsaUJBQUtuSCxLQUFMLEdBQWM7QUFIOUI7QUFLRCxPQTNqQk87QUE0akJSd0gsZ0JBQVUsa0JBQVNDLE1BQVQsRUFBaUI7QUFDekI7QUFDQUEsZUFBTzlELE1BQVAsQ0FBZSxNQUFmLEVBQXdCK0QsR0FBeEIsQ0FBNEJELE9BQU9sSCxJQUFQLENBQWEsTUFBYixDQUE1QixFQUFtRG1CLElBQW5ELENBQXdELFlBQVc7QUFDakUsY0FBSXFCLFFBQVF4SixFQUFFLElBQUYsQ0FBWjtBQUNBd0osZ0JBQU12QyxJQUFOLENBQVksSUFBWixFQUFrQnVDLE1BQU12QyxJQUFOLENBQVksSUFBWixJQUFxQixRQUF2QztBQUNELFNBSEQ7QUFJQSxlQUFPaUgsTUFBUDtBQUNELE9BbmtCTztBQW9rQlI3SCxzQkFBZ0I7QUFDZCtILGlCQUFTLElBREs7QUFFZHBNLGNBQU0sZ0JBQVc7QUFDZixjQUFJb00sVUFBVXRNLFFBQVF1RSxjQUFSLENBQXVCZ0ksYUFBdkIsRUFBZDtBQUNBLGNBQUlELE9BQUosRUFBYTtBQUNYLGdCQUFJRSxVQUFVRixRQUFRakssT0FBUixDQUFnQixZQUFoQixFQUE2QixFQUE3QixJQUFtQyxrQkFBakQ7QUFDQWxELHFCQUFTcUgsZ0JBQVQsQ0FBMEJnRyxPQUExQixFQUFtQyxZQUFXO0FBQzVDLGtCQUFJeE0sUUFBUXVFLGNBQVIsQ0FBdUJNLFFBQXZCLEVBQUosRUFBdUM7QUFDckMsb0JBQUd0RyxPQUFPbUQsWUFBVixFQUF3QjtBQUN0QitLLCtCQUFhbE8sT0FBT21ELFlBQXBCLEVBRHNCLENBQ2E7QUFDcEMsaUJBRkQsTUFFTztBQUNMbkQseUJBQU9vRyxLQUFQLEdBREssQ0FDVztBQUNqQjtBQUNGLGVBTkQsTUFPSztBQUNILG9CQUFHcEcsT0FBT2tELE9BQVYsRUFBbUI7QUFDakJsRCx5QkFBT3FHLElBQVAsR0FEaUIsQ0FDRjtBQUNoQixpQkFGRCxNQUVPO0FBQ0wsc0JBQUlyRyxPQUFPQyxJQUFQLENBQVlzRyxTQUFaLEdBQXdCLENBQTVCLEVBQStCO0FBQzdCQywrQkFBV3hHLE9BQU9xRyxJQUFsQixFQUF3QnJHLE9BQU9DLElBQVAsQ0FBWXNHLFNBQXBDO0FBQ0QsbUJBRkQsTUFFTztBQUNMdkcsMkJBQU9xRyxJQUFQLEdBREssQ0FDVTtBQUNoQjtBQUNGO0FBQ0Y7QUFDRixhQW5CRDtBQW9CRDtBQUNGLFNBM0JhO0FBNEJkQyxrQkFBVSxvQkFBVztBQUNuQixjQUFJeEQsT0FBT3JCLFFBQVF1RSxjQUFSLENBQXVCZ0ksYUFBdkIsRUFBWDtBQUNBLGNBQUksQ0FBQ2xMLElBQUwsRUFBVztBQUNULG1CQUFPLEtBQVA7QUFDRDtBQUNELGlCQUFPbEMsU0FBU2tDLElBQVQsQ0FBUDtBQUNELFNBbENhO0FBbUNka0wsdUJBQWUseUJBQVc7QUFDeEIsY0FBSUcsV0FBVyxDQUFDLFFBQUQsRUFBVSxLQUFWLEVBQWdCLElBQWhCLEVBQXFCLEdBQXJCLENBQWY7QUFDQTtBQUNBLGNBQUksWUFBWXZOLFFBQWhCLEVBQTBCO0FBQ3hCLG1CQUFPLFFBQVA7QUFDRDtBQUNEO0FBQ0EsZUFBTSxJQUFJOEMsSUFBSSxDQUFkLEVBQWlCQSxJQUFJeUssU0FBU3hMLE1BQTlCLEVBQXNDZSxHQUF0QyxFQUE0QztBQUN4QyxnQkFBS3lLLFNBQVN6SyxDQUFULElBQWMsUUFBZixJQUE0QjlDLFFBQWhDLEVBQTBDO0FBQ3hDLHFCQUFPdU4sU0FBU3pLLENBQVQsSUFBYyxRQUFyQjtBQUNEO0FBQ0o7QUFDRDtBQUNBLGlCQUFPLElBQVA7QUFDRDtBQWpEYSxPQXBrQlI7QUF1bkJSMEYsOEJBQXdCLGtDQUFXO0FBQ2pDOEUscUJBQWFuTixzQkFBYjtBQUNBQSxpQ0FBeUJ5RixXQUFXLFlBQVc7QUFDN0MxRix5QkFBZSxFQUFmO0FBQ0QsU0FGd0IsRUFFdEIsSUFGc0IsQ0FBekI7QUFHRDtBQTVuQk8sS0FBVjs7QUErbkJBO0FBQ0FkLFdBQU91RixXQUFQLEdBQXFCLFVBQVNGLE1BQVQsRUFBaUJlLEtBQWpCLEVBQXdCZ0ksUUFBeEIsRUFBa0NDLFFBQWxDLEVBQTRDQyxPQUE1QyxFQUFxRDtBQUN4RSxVQUFJLENBQUN0TyxPQUFPQyxJQUFQLENBQVk2SixhQUFiLElBQThCekUsV0FBV3JGLE9BQU82QixZQUFwRCxFQUFrRTtBQUNoRTdCLGVBQU9pQixTQUFQLEdBQW9Cb0UsU0FBU3JGLE9BQU82QixZQUFqQixHQUFpQyxNQUFqQyxHQUEwQyxNQUE3RDtBQUNEOztBQUVELFVBQUlOLFNBQVN2QixPQUFPNEksV0FBUCxLQUF1QixDQUFwQyxFQUF1QzVJLE9BQU9pQixTQUFQLEdBQW9CakIsT0FBT2dILFdBQVAsR0FBcUIzQixNQUF0QixHQUFnQyxNQUFoQyxHQUF5QyxNQUE1RDs7QUFFdkMsVUFBSSxDQUFDckYsT0FBTzRCLFNBQVIsS0FBc0I1QixPQUFPa00sVUFBUCxDQUFrQjdHLE1BQWxCLEVBQTBCaUosT0FBMUIsS0FBc0NGLFFBQTVELEtBQXlFcE8sT0FBT2dOLEVBQVAsQ0FBVSxVQUFWLENBQTdFLEVBQW9HO0FBQ2xHLFlBQUl6TCxTQUFTOE0sUUFBYixFQUF1QjtBQUNyQixjQUFJRSxTQUFTNU8sRUFBRUssT0FBT0MsSUFBUCxDQUFZdUIsUUFBZCxFQUF3QkUsSUFBeEIsQ0FBNkIsWUFBN0IsQ0FBYjtBQUNBMUIsaUJBQU9rQyxLQUFQLEdBQWVtRCxXQUFXLENBQVgsSUFBZ0JBLFdBQVdyRixPQUFPMEMsS0FBUCxHQUFlLENBQXpEO0FBQ0E2TCxpQkFBT2hKLFdBQVAsQ0FBbUJGLE1BQW5CLEVBQTJCLElBQTNCLEVBQWlDLEtBQWpDLEVBQXdDLElBQXhDLEVBQThDaUosT0FBOUM7QUFDQXRPLGlCQUFPaUIsU0FBUCxHQUFvQmpCLE9BQU9nSCxXQUFQLEdBQXFCM0IsTUFBdEIsR0FBZ0MsTUFBaEMsR0FBeUMsTUFBNUQ7QUFDQWtKLGlCQUFPdE4sU0FBUCxHQUFtQmpCLE9BQU9pQixTQUExQjs7QUFFQSxjQUFJcUQsS0FBS2tLLElBQUwsQ0FBVSxDQUFDbkosU0FBUyxDQUFWLElBQWFyRixPQUFPeU8sT0FBOUIsSUFBeUMsQ0FBekMsS0FBK0N6TyxPQUFPNkIsWUFBdEQsSUFBc0V3RCxXQUFXLENBQXJGLEVBQXdGO0FBQ3RGckYsbUJBQU9nSCxXQUFQLEdBQXFCM0IsTUFBckI7QUFDQXJGLG1CQUFPd0MsTUFBUCxDQUFjeUUsV0FBZCxDQUEwQjdHLFlBQVksY0FBdEMsRUFBc0Q4RyxFQUF0RCxDQUF5RDdCLE1BQXpELEVBQWlFOEIsUUFBakUsQ0FBMEUvRyxZQUFZLGNBQXRGO0FBQ0FpRixxQkFBU2YsS0FBS3dDLEtBQUwsQ0FBV3pCLFNBQU9yRixPQUFPeU8sT0FBekIsQ0FBVDtBQUNELFdBSkQsTUFJTztBQUNMek8sbUJBQU9nSCxXQUFQLEdBQXFCM0IsTUFBckI7QUFDQXJGLG1CQUFPd0MsTUFBUCxDQUFjeUUsV0FBZCxDQUEwQjdHLFlBQVksY0FBdEMsRUFBc0Q4RyxFQUF0RCxDQUF5RDdCLE1BQXpELEVBQWlFOEIsUUFBakUsQ0FBMEUvRyxZQUFZLGNBQXRGO0FBQ0EsbUJBQU8sS0FBUDtBQUNEO0FBQ0Y7O0FBRURKLGVBQU80QixTQUFQLEdBQW1CLElBQW5CO0FBQ0E1QixlQUFPaUMsV0FBUCxHQUFxQm9ELE1BQXJCOztBQUVBO0FBQ0EsWUFBSWUsS0FBSixFQUFXO0FBQUVwRyxpQkFBT29HLEtBQVA7QUFBaUI7O0FBRTlCO0FBQ0FwRyxlQUFPQyxJQUFQLENBQVl5TyxNQUFaLENBQW1CMU8sTUFBbkI7O0FBRUE7QUFDQSxZQUFJQSxPQUFPNEMsVUFBUCxJQUFxQixDQUFDMEwsT0FBMUIsRUFBbUM7QUFBRTdNLGtCQUFRb0IsSUFBUixDQUFhLFNBQWI7QUFBMEI7O0FBRS9EO0FBQ0EsWUFBSTdDLE9BQU9DLElBQVAsQ0FBWTRFLFVBQWhCLEVBQTRCO0FBQUVwRCxrQkFBUW9ELFVBQVIsQ0FBbUJvRSxNQUFuQjtBQUE4Qjs7QUFFNUQ7QUFDQTtBQUNBLFlBQUksQ0FBQzlILFFBQUwsRUFBZTtBQUFFbkIsaUJBQU93QyxNQUFQLENBQWN5RSxXQUFkLENBQTBCN0csWUFBWSxjQUF0QyxFQUFzRDhHLEVBQXRELENBQXlEN0IsTUFBekQsRUFBaUU4QixRQUFqRSxDQUEwRS9HLFlBQVksY0FBdEY7QUFBd0c7O0FBRXpIO0FBQ0E7QUFDQUosZUFBT2tDLEtBQVAsR0FBZW1ELFdBQVcsQ0FBWCxJQUFnQkEsV0FBV3JGLE9BQU9tQyxJQUFqRDs7QUFFQTtBQUNBLFlBQUluQyxPQUFPQyxJQUFQLENBQVk2RSxZQUFoQixFQUE4QjtBQUFFckQsa0JBQVFxRCxZQUFSLENBQXFCdUUsTUFBckI7QUFBZ0M7O0FBRWhFLFlBQUloRSxXQUFXckYsT0FBT21DLElBQXRCLEVBQTRCO0FBQzFCO0FBQ0FuQyxpQkFBT0MsSUFBUCxDQUFZME8sR0FBWixDQUFnQjNPLE1BQWhCO0FBQ0E7QUFDQSxjQUFJLENBQUNBLE9BQU9DLElBQVAsQ0FBWTZKLGFBQWpCLEVBQWdDO0FBQUU5SixtQkFBT29HLEtBQVA7QUFBaUI7QUFDcEQ7O0FBRUQ7QUFDQSxZQUFJLENBQUMvRSxJQUFMLEVBQVc7QUFDVCxjQUFJdU4sWUFBYTVOLFFBQUQsR0FBYWhCLE9BQU93QyxNQUFQLENBQWN1SCxNQUFkLENBQXFCLFFBQXJCLEVBQStCcUQsTUFBL0IsRUFBYixHQUF1RHBOLE9BQU9rTixTQUE5RTtBQUFBLGNBQ0kyQixNQURKO0FBQUEsY0FDWUMsV0FEWjtBQUFBLGNBQ3lCQyxRQUR6Qjs7QUFHQTtBQUNBLGNBQUk1TixRQUFKLEVBQWM7QUFDWjBOLHFCQUFTN08sT0FBT0MsSUFBUCxDQUFZMEwsVUFBckI7QUFDQW9ELHVCQUFZLENBQUMvTyxPQUFPMEwsS0FBUCxHQUFlbUQsTUFBaEIsSUFBMEI3TyxPQUFPK0csSUFBbEMsR0FBMEMvRyxPQUFPaUMsV0FBNUQ7QUFDQTZNLDBCQUFlQyxXQUFXL08sT0FBT3lMLEtBQWxCLElBQTJCekwsT0FBT3lPLE9BQVAsS0FBbUIsQ0FBL0MsR0FBb0R6TyxPQUFPeUwsS0FBM0QsR0FBbUVzRCxRQUFqRjtBQUNELFdBSkQsTUFJTyxJQUFJL08sT0FBTzZCLFlBQVAsS0FBd0IsQ0FBeEIsSUFBNkJ3RCxXQUFXckYsT0FBTzBDLEtBQVAsR0FBZSxDQUF2RCxJQUE0RDFDLE9BQU9DLElBQVAsQ0FBWTZKLGFBQXhFLElBQXlGOUosT0FBT2lCLFNBQVAsS0FBcUIsTUFBbEgsRUFBMEg7QUFDL0g2TiwwQkFBZTVOLE9BQUQsR0FBWSxDQUFDbEIsT0FBTzBDLEtBQVAsR0FBZTFDLE9BQU80TCxXQUF2QixJQUFzQ2dELFNBQWxELEdBQThELENBQTVFO0FBQ0QsV0FGTSxNQUVBLElBQUk1TyxPQUFPNkIsWUFBUCxLQUF3QjdCLE9BQU9tQyxJQUEvQixJQUF1Q2tELFdBQVcsQ0FBbEQsSUFBdURyRixPQUFPQyxJQUFQLENBQVk2SixhQUFuRSxJQUFvRjlKLE9BQU9pQixTQUFQLEtBQXFCLE1BQTdHLEVBQXFIO0FBQzFINk4sMEJBQWU1TixPQUFELEdBQVksQ0FBWixHQUFnQixDQUFDbEIsT0FBTzBDLEtBQVAsR0FBZSxDQUFoQixJQUFxQmtNLFNBQW5EO0FBQ0QsV0FGTSxNQUVBO0FBQ0xFLDBCQUFlNU4sT0FBRCxHQUFZLENBQUVsQixPQUFPMEMsS0FBUCxHQUFlLENBQWhCLEdBQXFCMkMsTUFBckIsR0FBOEJyRixPQUFPNEwsV0FBdEMsSUFBcURnRCxTQUFqRSxHQUE2RSxDQUFDdkosU0FBU3JGLE9BQU80TCxXQUFqQixJQUFnQ2dELFNBQTNIO0FBQ0Q7QUFDRDVPLGlCQUFPK0wsUUFBUCxDQUFnQitDLFdBQWhCLEVBQTZCLEVBQTdCLEVBQWlDOU8sT0FBT0MsSUFBUCxDQUFZK08sY0FBN0M7QUFDQSxjQUFJaFAsT0FBT29ELFdBQVgsRUFBd0I7QUFDdEIsZ0JBQUksQ0FBQ3BELE9BQU9DLElBQVAsQ0FBWTZKLGFBQWIsSUFBOEIsQ0FBQzlKLE9BQU9rQyxLQUExQyxFQUFpRDtBQUMvQ2xDLHFCQUFPNEIsU0FBUCxHQUFtQixLQUFuQjtBQUNBNUIscUJBQU82QixZQUFQLEdBQXNCN0IsT0FBT2lDLFdBQTdCO0FBQ0Q7O0FBRUQ7QUFDQWpDLG1CQUFPeUMsU0FBUCxDQUFpQndNLE1BQWpCLENBQXdCLG1DQUF4QjtBQUNBalAsbUJBQU95QyxTQUFQLENBQWlCd0MsSUFBakIsQ0FBc0IsbUNBQXRCLEVBQTJELFlBQVc7QUFDcEVpSiwyQkFBYWxPLE9BQU9nRSxrQkFBcEI7QUFDQWhFLHFCQUFPa1AsTUFBUCxDQUFjTixTQUFkO0FBQ0QsYUFIRDs7QUFLQTtBQUNBVix5QkFBYWxPLE9BQU9nRSxrQkFBcEI7QUFDQWhFLG1CQUFPZ0Usa0JBQVAsR0FBNEJ3QyxXQUFXLFlBQVc7QUFDaER4RyxxQkFBT2tQLE1BQVAsQ0FBY04sU0FBZDtBQUNELGFBRjJCLEVBRXpCNU8sT0FBT0MsSUFBUCxDQUFZK08sY0FBWixHQUE2QixHQUZKLENBQTVCO0FBSUQsV0FuQkQsTUFtQk87QUFDTGhQLG1CQUFPeUMsU0FBUCxDQUFpQitLLE9BQWpCLENBQXlCeE4sT0FBTytDLElBQWhDLEVBQXNDL0MsT0FBT0MsSUFBUCxDQUFZK08sY0FBbEQsRUFBa0VoUCxPQUFPQyxJQUFQLENBQVlrUCxNQUE5RSxFQUFzRixZQUFVO0FBQzlGblAscUJBQU9rUCxNQUFQLENBQWNOLFNBQWQ7QUFDRCxhQUZEO0FBR0Q7QUFDRixTQXpDRCxNQXlDTztBQUFFO0FBQ1AsY0FBSSxDQUFDbE8sS0FBTCxFQUFZO0FBQ1Y7QUFDQTs7QUFFQVYsbUJBQU93QyxNQUFQLENBQWMwRSxFQUFkLENBQWlCbEgsT0FBTzZCLFlBQXhCLEVBQXNDNkwsR0FBdEMsQ0FBMEMsRUFBQyxVQUFVLENBQVgsRUFBYyxXQUFXLE1BQXpCLEVBQTFDLEVBQTRFRixPQUE1RSxDQUFvRixFQUFDLFdBQVcsQ0FBWixFQUFwRixFQUFvR3hOLE9BQU9DLElBQVAsQ0FBWStPLGNBQWhILEVBQWdJaFAsT0FBT0MsSUFBUCxDQUFZa1AsTUFBNUk7QUFDQW5QLG1CQUFPd0MsTUFBUCxDQUFjMEUsRUFBZCxDQUFpQjdCLE1BQWpCLEVBQXlCcUksR0FBekIsQ0FBNkIsRUFBQyxVQUFVLENBQVgsRUFBYyxXQUFXLE9BQXpCLEVBQTdCLEVBQWdFRixPQUFoRSxDQUF3RSxFQUFDLFdBQVcsQ0FBWixFQUF4RSxFQUF3RnhOLE9BQU9DLElBQVAsQ0FBWStPLGNBQXBHLEVBQW9IaFAsT0FBT0MsSUFBUCxDQUFZa1AsTUFBaEksRUFBd0luUCxPQUFPa1AsTUFBL0k7QUFFRCxXQVBELE1BT087QUFDTGxQLG1CQUFPd0MsTUFBUCxDQUFjMEUsRUFBZCxDQUFpQmxILE9BQU82QixZQUF4QixFQUFzQzZMLEdBQXRDLENBQTBDLEVBQUUsV0FBVyxDQUFiLEVBQWdCLFVBQVUsQ0FBMUIsRUFBNkIsV0FBVyxNQUF4QyxFQUExQztBQUNBMU4sbUJBQU93QyxNQUFQLENBQWMwRSxFQUFkLENBQWlCN0IsTUFBakIsRUFBeUJxSSxHQUF6QixDQUE2QixFQUFFLFdBQVcsQ0FBYixFQUFnQixVQUFVLENBQTFCLEVBQTZCLFdBQVcsT0FBeEMsRUFBN0I7QUFDQTFOLG1CQUFPa1AsTUFBUCxDQUFjTixTQUFkO0FBQ0Q7QUFDRjtBQUNEO0FBQ0EsWUFBSTVPLE9BQU9DLElBQVAsQ0FBWXdHLFlBQWhCLEVBQThCO0FBQUVoRixrQkFBUWdGLFlBQVIsQ0FBcUJ6RyxPQUFPQyxJQUFQLENBQVkrTyxjQUFqQztBQUFtRDtBQUNwRjtBQUNGLEtBdEhEO0FBdUhBaFAsV0FBT2tQLE1BQVAsR0FBZ0IsVUFBU04sU0FBVCxFQUFvQjtBQUNsQztBQUNBLFVBQUksQ0FBQ3ZOLElBQUQsSUFBUyxDQUFDRixRQUFkLEVBQXdCO0FBQ3RCLFlBQUluQixPQUFPNkIsWUFBUCxLQUF3QixDQUF4QixJQUE2QjdCLE9BQU9pQyxXQUFQLEtBQXVCakMsT0FBT21DLElBQTNELElBQW1FbkMsT0FBT0MsSUFBUCxDQUFZNkosYUFBbkYsRUFBa0c7QUFDaEc5SixpQkFBTytMLFFBQVAsQ0FBZ0I2QyxTQUFoQixFQUEyQixTQUEzQjtBQUNELFNBRkQsTUFFTyxJQUFJNU8sT0FBTzZCLFlBQVAsS0FBd0I3QixPQUFPbUMsSUFBL0IsSUFBdUNuQyxPQUFPaUMsV0FBUCxLQUF1QixDQUE5RCxJQUFtRWpDLE9BQU9DLElBQVAsQ0FBWTZKLGFBQW5GLEVBQWtHO0FBQ3ZHOUosaUJBQU8rTCxRQUFQLENBQWdCNkMsU0FBaEIsRUFBMkIsV0FBM0I7QUFDRDtBQUNGO0FBQ0Q1TyxhQUFPNEIsU0FBUCxHQUFtQixLQUFuQjtBQUNBNUIsYUFBTzZCLFlBQVAsR0FBc0I3QixPQUFPaUMsV0FBN0I7QUFDQTtBQUNBakMsYUFBT0MsSUFBUCxDQUFZbVAsS0FBWixDQUFrQnBQLE1BQWxCO0FBQ0QsS0FiRDs7QUFlQTtBQUNBQSxXQUFPcVAsYUFBUCxHQUF1QixZQUFXO0FBQ2hDLFVBQUksQ0FBQ3JQLE9BQU80QixTQUFSLElBQXFCaEMsT0FBekIsRUFBbUM7QUFBRUksZUFBT3VGLFdBQVAsQ0FBbUJ2RixPQUFPc0YsU0FBUCxDQUFpQixNQUFqQixDQUFuQjtBQUErQztBQUNyRixLQUZEO0FBR0E7QUFDQXRGLFdBQU9vRyxLQUFQLEdBQWUsWUFBVztBQUN4QmtKLG9CQUFjdFAsT0FBT3VQLGNBQXJCO0FBQ0F2UCxhQUFPdVAsY0FBUCxHQUF3QixJQUF4QjtBQUNBdlAsYUFBTzJOLE9BQVAsR0FBaUIsS0FBakI7QUFDQTtBQUNBLFVBQUkzTixPQUFPQyxJQUFQLENBQVk2RixTQUFoQixFQUEyQjtBQUFFckUsZ0JBQVFxRSxTQUFSLENBQWtCdUQsTUFBbEIsQ0FBeUIsTUFBekI7QUFBbUM7QUFDaEU7QUFDQSxVQUFJckosT0FBTzRDLFVBQVgsRUFBdUI7QUFBRW5CLGdCQUFRb0IsSUFBUixDQUFhLE9BQWI7QUFBd0I7QUFDbEQsS0FSRDtBQVNBO0FBQ0E3QyxXQUFPcUcsSUFBUCxHQUFjLFlBQVc7QUFDdkIsVUFBSXJHLE9BQU8yTixPQUFYLEVBQW9CO0FBQUUyQixzQkFBY3RQLE9BQU91UCxjQUFyQjtBQUF1QztBQUM3RHZQLGFBQU91UCxjQUFQLEdBQXdCdlAsT0FBT3VQLGNBQVAsSUFBeUJDLFlBQVl4UCxPQUFPcVAsYUFBbkIsRUFBa0NyUCxPQUFPQyxJQUFQLENBQVl3UCxjQUE5QyxDQUFqRDtBQUNBelAsYUFBT2tELE9BQVAsR0FBaUJsRCxPQUFPMk4sT0FBUCxHQUFpQixJQUFsQztBQUNBO0FBQ0EsVUFBSTNOLE9BQU9DLElBQVAsQ0FBWTZGLFNBQWhCLEVBQTJCO0FBQUVyRSxnQkFBUXFFLFNBQVIsQ0FBa0J1RCxNQUFsQixDQUF5QixPQUF6QjtBQUFvQztBQUNqRTtBQUNBLFVBQUlySixPQUFPNEMsVUFBWCxFQUF1QjtBQUFFbkIsZ0JBQVFvQixJQUFSLENBQWEsTUFBYjtBQUF1QjtBQUNqRCxLQVJEO0FBU0E7QUFDQTdDLFdBQU82TSxJQUFQLEdBQWMsWUFBWTtBQUN4QjdNLGFBQU9vRyxLQUFQO0FBQ0FwRyxhQUFPaUQsT0FBUCxHQUFpQixJQUFqQjtBQUNELEtBSEQ7QUFJQWpELFdBQU9rTSxVQUFQLEdBQW9CLFVBQVM3RyxNQUFULEVBQWlCaUosT0FBakIsRUFBMEI7QUFDNUM7QUFDQSxVQUFJbk0sT0FBUVosS0FBRCxHQUFVdkIsT0FBTzRJLFdBQVAsR0FBcUIsQ0FBL0IsR0FBbUM1SSxPQUFPbUMsSUFBckQ7QUFDQSxhQUFRbU0sT0FBRCxHQUFZLElBQVosR0FDQy9NLFNBQVN2QixPQUFPZ0gsV0FBUCxLQUF1QmhILE9BQU8wQyxLQUFQLEdBQWUsQ0FBL0MsSUFBb0QyQyxXQUFXLENBQS9ELElBQW9FckYsT0FBT2lCLFNBQVAsS0FBcUIsTUFBMUYsR0FBb0csSUFBcEcsR0FDQ00sU0FBU3ZCLE9BQU9nSCxXQUFQLEtBQXVCLENBQWhDLElBQXFDM0IsV0FBV3JGLE9BQU80SSxXQUFQLEdBQXFCLENBQXJFLElBQTBFNUksT0FBT2lCLFNBQVAsS0FBcUIsTUFBaEcsR0FBMEcsS0FBMUcsR0FDQ29FLFdBQVdyRixPQUFPNkIsWUFBbEIsSUFBa0MsQ0FBQ04sS0FBcEMsR0FBNkMsS0FBN0MsR0FDQ3ZCLE9BQU9DLElBQVAsQ0FBWTZKLGFBQWIsR0FBOEIsSUFBOUIsR0FDQzlKLE9BQU9rQyxLQUFQLElBQWdCbEMsT0FBTzZCLFlBQVAsS0FBd0IsQ0FBeEMsSUFBNkN3RCxXQUFXbEQsSUFBeEQsSUFBZ0VuQyxPQUFPaUIsU0FBUCxLQUFxQixNQUF0RixHQUFnRyxLQUFoRyxHQUNDakIsT0FBT2tDLEtBQVAsSUFBZ0JsQyxPQUFPNkIsWUFBUCxLQUF3Qk0sSUFBeEMsSUFBZ0RrRCxXQUFXLENBQTNELElBQWdFckYsT0FBT2lCLFNBQVAsS0FBcUIsTUFBdEYsR0FBZ0csS0FBaEcsR0FDQSxJQVBQO0FBUUQsS0FYRDtBQVlBakIsV0FBT3NGLFNBQVAsR0FBbUIsVUFBU29LLEdBQVQsRUFBYztBQUMvQjFQLGFBQU9pQixTQUFQLEdBQW1CeU8sR0FBbkI7QUFDQSxVQUFJQSxRQUFRLE1BQVosRUFBb0I7QUFDbEIsZUFBUTFQLE9BQU82QixZQUFQLEtBQXdCN0IsT0FBT21DLElBQWhDLEdBQXdDLENBQXhDLEdBQTRDbkMsT0FBTzZCLFlBQVAsR0FBc0IsQ0FBekU7QUFDRCxPQUZELE1BRU87QUFDTCxlQUFRN0IsT0FBTzZCLFlBQVAsS0FBd0IsQ0FBekIsR0FBOEI3QixPQUFPbUMsSUFBckMsR0FBNENuQyxPQUFPNkIsWUFBUCxHQUFzQixDQUF6RTtBQUNEO0FBQ0YsS0FQRDs7QUFTQTtBQUNBN0IsV0FBTytMLFFBQVAsR0FBa0IsVUFBU3hDLEdBQVQsRUFBY29HLE9BQWQsRUFBdUJyQyxHQUF2QixFQUE0QjtBQUM1QyxVQUFJakksU0FBVSxZQUFXO0FBQ3ZCLFlBQUl1SyxXQUFZckcsR0FBRCxHQUFRQSxHQUFSLEdBQWUsQ0FBQ3ZKLE9BQU8wTCxLQUFQLEdBQWUxTCxPQUFPQyxJQUFQLENBQVkwTCxVQUE1QixJQUEwQzNMLE9BQU8rRyxJQUFsRCxHQUEwRC9HLE9BQU9pQyxXQUE5RjtBQUFBLFlBQ0k0TixVQUFXLFlBQVc7QUFDcEIsY0FBSTFPLFFBQUosRUFBYztBQUNaLG1CQUFRd08sWUFBWSxVQUFiLEdBQTJCcEcsR0FBM0IsR0FDQ3JJLFdBQVdsQixPQUFPaUMsV0FBUCxLQUF1QmpDLE9BQU9tQyxJQUExQyxHQUFrRCxDQUFsRCxHQUNDakIsT0FBRCxHQUFZbEIsT0FBT3lMLEtBQVAsR0FBaUIsQ0FBQ3pMLE9BQU8wTCxLQUFQLEdBQWUxTCxPQUFPQyxJQUFQLENBQVkwTCxVQUE1QixJQUEwQzNMLE9BQU8rRyxJQUFsRCxHQUEwRC9HLE9BQU9pQyxXQUE3RixHQUNDakMsT0FBT2lDLFdBQVAsS0FBdUJqQyxPQUFPbUMsSUFBL0IsR0FBdUNuQyxPQUFPeUwsS0FBOUMsR0FBc0RtRSxRQUg3RDtBQUlELFdBTEQsTUFLTztBQUNMLG9CQUFRRCxPQUFSO0FBQ0UsbUJBQUssVUFBTDtBQUFpQix1QkFBUXpPLE9BQUQsR0FBWSxDQUFFbEIsT0FBTzBDLEtBQVAsR0FBZSxDQUFoQixHQUFxQjFDLE9BQU82QixZQUE1QixHQUEyQzdCLE9BQU80TCxXQUFuRCxJQUFrRXJDLEdBQTlFLEdBQW9GLENBQUN2SixPQUFPNkIsWUFBUCxHQUFzQjdCLE9BQU80TCxXQUE5QixJQUE2Q3JDLEdBQXhJO0FBQ2pCLG1CQUFLLFVBQUw7QUFBaUIsdUJBQVFySSxPQUFELEdBQVlxSSxHQUFaLEdBQWtCQSxHQUF6QjtBQUNqQixtQkFBSyxTQUFMO0FBQWdCLHVCQUFRckksT0FBRCxHQUFZcUksR0FBWixHQUFrQnZKLE9BQU8wQyxLQUFQLEdBQWU2RyxHQUF4QztBQUNoQixtQkFBSyxXQUFMO0FBQWtCLHVCQUFRckksT0FBRCxHQUFZbEIsT0FBTzBDLEtBQVAsR0FBZTZHLEdBQTNCLEdBQWlDQSxHQUF4QztBQUNsQjtBQUFTLHVCQUFPQSxHQUFQO0FBTFg7QUFPRDtBQUNGLFNBZlUsRUFEZjs7QUFrQkksZUFBUXNHLFVBQVUsQ0FBQyxDQUFaLEdBQWlCLElBQXhCO0FBQ0QsT0FwQlMsRUFBZDs7QUFzQkEsVUFBSTdQLE9BQU9vRCxXQUFYLEVBQXdCO0FBQ3RCaUMsaUJBQVVyRSxRQUFELEdBQWEsbUJBQW1CcUUsTUFBbkIsR0FBNEIsS0FBekMsR0FBaUQsaUJBQWlCQSxNQUFqQixHQUEwQixPQUFwRjtBQUNBaUksY0FBT0EsUUFBUTFKLFNBQVQsR0FBdUIwSixNQUFJLElBQUwsR0FBYSxHQUFuQyxHQUF5QyxJQUEvQztBQUNBdE4sZUFBT3lDLFNBQVAsQ0FBaUJpTCxHQUFqQixDQUFxQixNQUFNMU4sT0FBTzZELEdBQWIsR0FBbUIsc0JBQXhDLEVBQWdFeUosR0FBaEU7QUFDQ3ROLGVBQU95QyxTQUFQLENBQWlCaUwsR0FBakIsQ0FBcUIscUJBQXJCLEVBQTRDSixHQUE1QztBQUNGOztBQUVEdE4sYUFBTytDLElBQVAsQ0FBWS9DLE9BQU84QyxJQUFuQixJQUEyQnVDLE1BQTNCO0FBQ0EsVUFBSXJGLE9BQU9vRCxXQUFQLElBQXNCa0ssUUFBUTFKLFNBQWxDLEVBQTZDO0FBQUU1RCxlQUFPeUMsU0FBUCxDQUFpQmlMLEdBQWpCLENBQXFCMU4sT0FBTytDLElBQTVCO0FBQW9DOztBQUVuRi9DLGFBQU95QyxTQUFQLENBQWlCaUwsR0FBakIsQ0FBcUIsV0FBckIsRUFBaUNySSxNQUFqQztBQUNELEtBbENEOztBQW9DQXJGLFdBQU80RSxLQUFQLEdBQWUsVUFBUzJELElBQVQsRUFBZTtBQUM1QjtBQUNBLFVBQUksQ0FBQ2xILElBQUwsRUFBVztBQUNULFlBQUl5TyxZQUFKLEVBQWtCQyxHQUFsQjs7QUFFQSxZQUFJeEgsU0FBUyxNQUFiLEVBQXFCO0FBQ25CdkksaUJBQU9tTixRQUFQLEdBQWtCeE4sRUFBRSxpQkFBaUJTLFNBQWpCLEdBQTZCLGtCQUEvQixFQUFtRHNOLEdBQW5ELENBQXVELEVBQUMsWUFBWSxRQUFiLEVBQXVCLFlBQVksVUFBbkMsRUFBdkQsRUFBdUdzQyxRQUF2RyxDQUFnSGhRLE1BQWhILEVBQXdIMEUsTUFBeEgsQ0FBK0gxRSxPQUFPeUMsU0FBdEksQ0FBbEI7QUFDQTtBQUNBekMsaUJBQU9pUSxVQUFQLEdBQW9CLENBQXBCO0FBQ0FqUSxpQkFBTzRMLFdBQVAsR0FBcUIsQ0FBckI7QUFDQTtBQUNBLGNBQUkxSyxPQUFKLEVBQWE7QUFDWDZPLGtCQUFNcFEsRUFBRXVRLFNBQUYsQ0FBWWxRLE9BQU93QyxNQUFuQixFQUEyQnRCLE9BQTNCLEVBQU47QUFDQWxCLG1CQUFPd0MsTUFBUCxHQUFnQjdDLEVBQUVvUSxHQUFGLENBQWhCO0FBQ0EvUCxtQkFBT3lDLFNBQVAsQ0FBaUJnQyxLQUFqQixHQUF5QkMsTUFBekIsQ0FBZ0MxRSxPQUFPd0MsTUFBdkM7QUFDRDtBQUNGO0FBQ0Q7QUFDQSxZQUFJeEMsT0FBT0MsSUFBUCxDQUFZNkosYUFBWixJQUE2QixDQUFDM0ksUUFBbEMsRUFBNEM7QUFDMUNuQixpQkFBT2lRLFVBQVAsR0FBb0IsQ0FBcEI7QUFDQWpRLGlCQUFPNEwsV0FBUCxHQUFxQixDQUFyQjtBQUNBO0FBQ0EsY0FBSXJELFNBQVMsTUFBYixFQUFxQjtBQUFFdkksbUJBQU95QyxTQUFQLENBQWlCa0UsSUFBakIsQ0FBc0IsUUFBdEIsRUFBZ0M2QyxNQUFoQztBQUEyQztBQUNsRXhKLGlCQUFPeUMsU0FBUCxDQUFpQmlDLE1BQWpCLENBQXdCakQsUUFBUW1NLFFBQVIsQ0FBaUI1TixPQUFPd0MsTUFBUCxDQUFjMk4sS0FBZCxHQUFzQkMsS0FBdEIsR0FBOEJqSixRQUE5QixDQUF1QyxPQUF2QyxDQUFqQixFQUFrRVAsSUFBbEUsQ0FBdUUsYUFBdkUsRUFBc0YsTUFBdEYsQ0FBeEIsRUFDaUJ5SixPQURqQixDQUN5QjVPLFFBQVFtTSxRQUFSLENBQWlCNU4sT0FBT3dDLE1BQVAsQ0FBY0wsSUFBZCxHQUFxQmlPLEtBQXJCLEdBQTZCakosUUFBN0IsQ0FBc0MsT0FBdEMsQ0FBakIsRUFBaUVQLElBQWpFLENBQXNFLGFBQXRFLEVBQXFGLE1BQXJGLENBRHpCO0FBRUQ7QUFDRDVHLGVBQU9xTixTQUFQLEdBQW1CMU4sRUFBRUssT0FBT0MsSUFBUCxDQUFZb0MsUUFBZCxFQUF3QnJDLE1BQXhCLENBQW5COztBQUVBOFAsdUJBQWdCNU8sT0FBRCxHQUFZbEIsT0FBTzBDLEtBQVAsR0FBZSxDQUFmLEdBQW1CMUMsT0FBTzZCLFlBQTFCLEdBQXlDN0IsT0FBTzRMLFdBQTVELEdBQTBFNUwsT0FBTzZCLFlBQVAsR0FBc0I3QixPQUFPNEwsV0FBdEg7QUFDQTtBQUNBLFlBQUk1SyxZQUFZLENBQUNHLFFBQWpCLEVBQTJCO0FBQ3pCbkIsaUJBQU95QyxTQUFQLENBQWlCMkssTUFBakIsQ0FBd0IsQ0FBQ3BOLE9BQU8wQyxLQUFQLEdBQWUxQyxPQUFPaVEsVUFBdkIsSUFBcUMsR0FBckMsR0FBMkMsR0FBbkUsRUFBd0V2QyxHQUF4RSxDQUE0RSxVQUE1RSxFQUF3RixVQUF4RixFQUFvR1QsS0FBcEcsQ0FBMEcsTUFBMUc7QUFDQXpHLHFCQUFXLFlBQVU7QUFDbkJ4RyxtQkFBT3FOLFNBQVAsQ0FBaUJLLEdBQWpCLENBQXFCLEVBQUMsV0FBVyxPQUFaLEVBQXJCO0FBQ0ExTixtQkFBTzJFLE1BQVA7QUFDQTNFLG1CQUFPbU4sUUFBUCxDQUFnQkMsTUFBaEIsQ0FBdUJwTixPQUFPbUwsQ0FBOUI7QUFDQW5MLG1CQUFPK0wsUUFBUCxDQUFnQitELGVBQWU5UCxPQUFPbUwsQ0FBdEMsRUFBeUMsTUFBekM7QUFDRCxXQUxELEVBS0k1QyxTQUFTLE1BQVYsR0FBb0IsR0FBcEIsR0FBMEIsQ0FMN0I7QUFNRCxTQVJELE1BUU87QUFDTHZJLGlCQUFPeUMsU0FBUCxDQUFpQndLLEtBQWpCLENBQXVCLENBQUNqTixPQUFPMEMsS0FBUCxHQUFlMUMsT0FBT2lRLFVBQXZCLElBQXFDLEdBQXJDLEdBQTJDLEdBQWxFO0FBQ0FqUSxpQkFBTytMLFFBQVAsQ0FBZ0IrRCxlQUFlOVAsT0FBT2tOLFNBQXRDLEVBQWlELE1BQWpEO0FBQ0ExRyxxQkFBVyxZQUFVO0FBQ25CeEcsbUJBQU8yRSxNQUFQO0FBQ0EzRSxtQkFBT3FOLFNBQVAsQ0FBaUJLLEdBQWpCLENBQXFCLEVBQUMsU0FBUzFOLE9BQU9rTixTQUFqQixFQUE0QixlQUFnQmxOLE9BQU9zUSxTQUFuRCxFQUE4RCxTQUFTLE1BQXZFLEVBQStFLFdBQVcsT0FBMUYsRUFBckI7QUFDQTtBQUNBLGdCQUFJdFEsT0FBT0MsSUFBUCxDQUFZd0csWUFBaEIsRUFBOEI7QUFBRWhGLHNCQUFRZ0YsWUFBUjtBQUF5QjtBQUMxRCxXQUxELEVBS0k4QixTQUFTLE1BQVYsR0FBb0IsR0FBcEIsR0FBMEIsQ0FMN0I7QUFNRDtBQUNGLE9BOUNELE1BOENPO0FBQUU7QUFDUHZJLGVBQU93QyxNQUFQLENBQWNrTCxHQUFkLENBQWtCLEVBQUMsU0FBUyxNQUFWLEVBQWtCLFNBQVMsTUFBM0IsRUFBbUMsZUFBZSxPQUFsRCxFQUEyRCxZQUFZLFVBQXZFLEVBQWxCO0FBQ0EsWUFBSW5GLFNBQVMsTUFBYixFQUFxQjtBQUNuQixjQUFJLENBQUM3SCxLQUFMLEVBQVk7QUFDVjtBQUNBLGdCQUFJVixPQUFPQyxJQUFQLENBQVlzUSxjQUFaLElBQThCLEtBQWxDLEVBQXlDO0FBQ3ZDdlEscUJBQU93QyxNQUFQLENBQWNrTCxHQUFkLENBQWtCLEVBQUUsV0FBVyxDQUFiLEVBQWdCLFdBQVcsTUFBM0IsRUFBbUMsVUFBVSxDQUE3QyxFQUFsQixFQUFvRXhHLEVBQXBFLENBQXVFbEgsT0FBTzZCLFlBQTlFLEVBQTRGNkwsR0FBNUYsQ0FBZ0csRUFBQyxVQUFVLENBQVgsRUFBYyxXQUFXLE9BQXpCLEVBQWhHLEVBQW1JQSxHQUFuSSxDQUF1SSxFQUFDLFdBQVcsQ0FBWixFQUF2STtBQUNELGFBRkQsTUFFTztBQUNMMU4scUJBQU93QyxNQUFQLENBQWNrTCxHQUFkLENBQWtCLEVBQUUsV0FBVyxDQUFiLEVBQWdCLFdBQVcsTUFBM0IsRUFBbUMsVUFBVSxDQUE3QyxFQUFsQixFQUFvRXhHLEVBQXBFLENBQXVFbEgsT0FBTzZCLFlBQTlFLEVBQTRGNkwsR0FBNUYsQ0FBZ0csRUFBQyxVQUFVLENBQVgsRUFBYyxXQUFXLE9BQXpCLEVBQWhHLEVBQW1JRixPQUFuSSxDQUEySSxFQUFDLFdBQVcsQ0FBWixFQUEzSSxFQUEwSnhOLE9BQU9DLElBQVAsQ0FBWStPLGNBQXRLLEVBQXFMaFAsT0FBT0MsSUFBUCxDQUFZa1AsTUFBak07QUFDRDtBQUNGLFdBUEQsTUFPTztBQUNMblAsbUJBQU93QyxNQUFQLENBQWNrTCxHQUFkLENBQWtCLEVBQUUsV0FBVyxDQUFiLEVBQWdCLFdBQVcsTUFBM0IsRUFBbUMsb0JBQW9CLGFBQWExTixPQUFPQyxJQUFQLENBQVkrTyxjQUFaLEdBQTZCLElBQTFDLEdBQWlELFFBQXhHLEVBQWtILFVBQVUsQ0FBNUgsRUFBbEIsRUFBbUo5SCxFQUFuSixDQUFzSmxILE9BQU82QixZQUE3SixFQUEySzZMLEdBQTNLLENBQStLLEVBQUUsV0FBVyxDQUFiLEVBQWdCLFVBQVUsQ0FBMUIsRUFBNkIsV0FBVyxPQUF4QyxFQUEvSztBQUNEO0FBQ0Y7QUFDRDtBQUNBLFlBQUkxTixPQUFPQyxJQUFQLENBQVl3RyxZQUFoQixFQUE4QjtBQUFFaEYsa0JBQVFnRixZQUFSO0FBQXlCO0FBQzFEO0FBQ0Q7QUFDQTtBQUNBLFVBQUksQ0FBQ3RGLFFBQUwsRUFBZTtBQUFFbkIsZUFBT3dDLE1BQVAsQ0FBY3lFLFdBQWQsQ0FBMEI3RyxZQUFZLGNBQXRDLEVBQXNEOEcsRUFBdEQsQ0FBeURsSCxPQUFPNkIsWUFBaEUsRUFBOEVzRixRQUE5RSxDQUF1Ri9HLFlBQVksY0FBbkc7QUFBcUg7O0FBRXRJO0FBQ0FKLGFBQU9DLElBQVAsQ0FBWTBCLElBQVosQ0FBaUIzQixNQUFqQjtBQUNELEtBdkVEOztBQXlFQUEsV0FBTzJFLE1BQVAsR0FBZ0IsWUFBVztBQUN6QixVQUFJK0QsUUFBUTFJLE9BQU93QyxNQUFQLENBQWMyTixLQUFkLEVBQVo7QUFBQSxVQUNJSyxjQUFjeFEsT0FBT0MsSUFBUCxDQUFZMEwsVUFEOUI7QUFBQSxVQUVJOEUsV0FBV3pRLE9BQU9DLElBQVAsQ0FBWXdRLFFBRjNCO0FBQUEsVUFHSUMsV0FBVzFRLE9BQU9DLElBQVAsQ0FBWXlRLFFBSDNCOztBQUtBMVEsYUFBT29MLENBQVAsR0FBWXBMLE9BQU9tTixRQUFQLEtBQWtCdkosU0FBbkIsR0FBZ0M1RCxPQUFPaU4sS0FBUCxFQUFoQyxHQUFpRGpOLE9BQU9tTixRQUFQLENBQWdCRixLQUFoQixFQUE1RDtBQUNBak4sYUFBT21MLENBQVAsR0FBV3pDLE1BQU0wRSxNQUFOLEVBQVg7QUFDQXBOLGFBQU8yUSxVQUFQLEdBQW9CakksTUFBTWtJLFVBQU4sS0FBcUJsSSxNQUFNdUUsS0FBTixFQUF6Qzs7QUFFQTtBQUNBLFVBQUk5TCxRQUFKLEVBQWM7QUFDWm5CLGVBQU82USxLQUFQLEdBQWU3USxPQUFPQyxJQUFQLENBQVltQixTQUFaLEdBQXdCb1AsV0FBdkM7QUFDQXhRLGVBQU84USxLQUFQLEdBQWVOLFdBQWY7QUFDQXhRLGVBQU8rUSxJQUFQLEdBQWVOLFFBQUQsR0FBYUEsV0FBV3pRLE9BQU82USxLQUEvQixHQUF1QzdRLE9BQU9vTCxDQUE1RDtBQUNBcEwsZUFBT2dSLElBQVAsR0FBZU4sUUFBRCxHQUFjQSxXQUFXMVEsT0FBTzZRLEtBQW5CLEdBQTRCTCxXQUF6QyxHQUF1RHhRLE9BQU9vTCxDQUE1RTtBQUNBcEwsZUFBTzBMLEtBQVAsR0FBZ0IxTCxPQUFPK1EsSUFBUCxHQUFjL1EsT0FBT29MLENBQXRCLEdBQTJCLENBQUNwTCxPQUFPb0wsQ0FBUCxHQUFZb0YsZUFBZUMsV0FBVyxDQUExQixDQUFiLElBQTRDQSxRQUF2RSxHQUNDelEsT0FBT2dSLElBQVAsR0FBY2hSLE9BQU9vTCxDQUF0QixHQUEyQixDQUFDcEwsT0FBT29MLENBQVAsR0FBWW9GLGVBQWVFLFdBQVcsQ0FBMUIsQ0FBYixJQUE0Q0EsUUFBdkUsR0FDQzFRLE9BQU9DLElBQVAsQ0FBWW1CLFNBQVosR0FBd0JwQixPQUFPb0wsQ0FBaEMsR0FBcUNwTCxPQUFPb0wsQ0FBNUMsR0FBZ0RwTCxPQUFPQyxJQUFQLENBQVltQixTQUYzRTs7QUFJQXBCLGVBQU95TyxPQUFQLEdBQWlCbkssS0FBS3dDLEtBQUwsQ0FBVzlHLE9BQU9vTCxDQUFQLEdBQVVwTCxPQUFPMEwsS0FBNUIsQ0FBakI7QUFDQTFMLGVBQU8rRyxJQUFQLEdBQWUvRyxPQUFPQyxJQUFQLENBQVk4RyxJQUFaLEdBQW1CLENBQW5CLElBQXdCL0csT0FBT0MsSUFBUCxDQUFZOEcsSUFBWixHQUFtQi9HLE9BQU95TyxPQUFuRCxHQUErRHpPLE9BQU9DLElBQVAsQ0FBWThHLElBQTNFLEdBQWtGL0csT0FBT3lPLE9BQXZHO0FBQ0F6TyxlQUFPNEksV0FBUCxHQUFxQnRFLEtBQUtrSyxJQUFMLENBQVcsQ0FBQ3hPLE9BQU8wQyxLQUFQLEdBQWUxQyxPQUFPeU8sT0FBdkIsSUFBZ0N6TyxPQUFPK0csSUFBeEMsR0FBZ0QsQ0FBMUQsQ0FBckI7QUFDQS9HLGVBQU9tQyxJQUFQLEdBQWVuQyxPQUFPNEksV0FBUCxHQUFxQixDQUFwQztBQUNBNUksZUFBT3lMLEtBQVAsR0FBZ0J6TCxPQUFPNEksV0FBUCxLQUF1QixDQUF4QixHQUE2QixDQUE3QixHQUNDNUksT0FBT0MsSUFBUCxDQUFZbUIsU0FBWixHQUF3QnBCLE9BQU9vTCxDQUFoQyxHQUFzQ3BMLE9BQU8wTCxLQUFQLElBQWdCMUwsT0FBTzBDLEtBQVAsR0FBZSxDQUEvQixDQUFELEdBQXVDOE4sZUFBZXhRLE9BQU8wQyxLQUFQLEdBQWUsQ0FBOUIsQ0FBNUUsR0FBaUgsQ0FBQzFDLE9BQU8wTCxLQUFQLEdBQWU4RSxXQUFoQixJQUErQnhRLE9BQU8wQyxLQUF2QyxHQUFnRDFDLE9BQU9vTCxDQUF2RCxHQUEyRG9GLFdBRDFMO0FBRUQsT0FmRCxNQWVPO0FBQ0x4USxlQUFPMEwsS0FBUCxHQUFlMUwsT0FBT29MLENBQXRCO0FBQ0FwTCxlQUFPOFEsS0FBUCxHQUFlTixXQUFmO0FBQ0F4USxlQUFPNEksV0FBUCxHQUFxQjVJLE9BQU8wQyxLQUE1QjtBQUNBMUMsZUFBT21DLElBQVAsR0FBY25DLE9BQU8wQyxLQUFQLEdBQWUsQ0FBN0I7QUFDRDtBQUNEMUMsYUFBT2tOLFNBQVAsR0FBbUJsTixPQUFPMEwsS0FBUCxHQUFlMUwsT0FBTzJRLFVBQXpDO0FBQ0EzUSxhQUFPc1EsU0FBUCxHQUFtQnRRLE9BQU84USxLQUExQjtBQUNELEtBbENEOztBQW9DQTlRLFdBQU9xSixNQUFQLEdBQWdCLFVBQVNFLEdBQVQsRUFBY0QsTUFBZCxFQUFzQjtBQUNwQ3RKLGFBQU8yRSxNQUFQOztBQUVBO0FBQ0EsVUFBSSxDQUFDeEQsUUFBTCxFQUFlO0FBQ2IsWUFBSW9JLE1BQU12SixPQUFPNkIsWUFBakIsRUFBK0I7QUFDN0I3QixpQkFBTzZCLFlBQVAsSUFBdUIsQ0FBdkI7QUFDRCxTQUZELE1BRU8sSUFBSTBILE9BQU92SixPQUFPNkIsWUFBZCxJQUE4QjBILFFBQVEsQ0FBMUMsRUFBNkM7QUFDbER2SixpQkFBTzZCLFlBQVAsSUFBdUIsQ0FBdkI7QUFDRDtBQUNEN0IsZUFBT2lDLFdBQVAsR0FBcUJqQyxPQUFPNkIsWUFBNUI7QUFDRDs7QUFFRDtBQUNBLFVBQUk3QixPQUFPQyxJQUFQLENBQVk0RSxVQUFaLElBQTBCLENBQUM3RSxPQUFPa0UsY0FBdEMsRUFBc0Q7QUFDcEQsWUFBS29GLFdBQVcsS0FBWCxJQUFvQixDQUFDbkksUUFBdEIsSUFBbUNuQixPQUFPNEksV0FBUCxHQUFxQjVJLE9BQU82RSxVQUFQLENBQWtCbEMsTUFBOUUsRUFBc0Y7QUFDcEZsQixrQkFBUW9ELFVBQVIsQ0FBbUJ3RSxNQUFuQixDQUEwQixLQUExQjtBQUNELFNBRkQsTUFFTyxJQUFLQyxXQUFXLFFBQVgsSUFBdUIsQ0FBQ25JLFFBQXpCLElBQXNDbkIsT0FBTzRJLFdBQVAsR0FBcUI1SSxPQUFPNkUsVUFBUCxDQUFrQmxDLE1BQWpGLEVBQXlGO0FBQzlGLGNBQUl4QixZQUFZbkIsT0FBTzZCLFlBQVAsR0FBc0I3QixPQUFPbUMsSUFBN0MsRUFBbUQ7QUFDakRuQyxtQkFBTzZCLFlBQVAsSUFBdUIsQ0FBdkI7QUFDQTdCLG1CQUFPaUMsV0FBUCxJQUFzQixDQUF0QjtBQUNEO0FBQ0RSLGtCQUFRb0QsVUFBUixDQUFtQndFLE1BQW5CLENBQTBCLFFBQTFCLEVBQW9DckosT0FBT21DLElBQTNDO0FBQ0Q7QUFDRjtBQUNEO0FBQ0EsVUFBSW5DLE9BQU9DLElBQVAsQ0FBWTZFLFlBQWhCLEVBQThCO0FBQUVyRCxnQkFBUXFELFlBQVIsQ0FBcUJ1RSxNQUFyQjtBQUFnQztBQUVqRSxLQTVCRDs7QUE4QkFySixXQUFPaVIsUUFBUCxHQUFrQixVQUFTMU4sR0FBVCxFQUFjZ0csR0FBZCxFQUFtQjtBQUNuQyxVQUFJZ0UsT0FBTzVOLEVBQUU0RCxHQUFGLENBQVg7O0FBRUF2RCxhQUFPMEMsS0FBUCxJQUFnQixDQUFoQjtBQUNBMUMsYUFBT21DLElBQVAsR0FBY25DLE9BQU8wQyxLQUFQLEdBQWUsQ0FBN0I7O0FBRUE7QUFDQSxVQUFJMUIsWUFBWUUsT0FBaEIsRUFBeUI7QUFDdEJxSSxnQkFBUTNGLFNBQVQsR0FBc0I1RCxPQUFPd0MsTUFBUCxDQUFjMEUsRUFBZCxDQUFpQmxILE9BQU8wQyxLQUFQLEdBQWU2RyxHQUFoQyxFQUFxQzZGLEtBQXJDLENBQTJDN0IsSUFBM0MsQ0FBdEIsR0FBeUV2TixPQUFPeUMsU0FBUCxDQUFpQjROLE9BQWpCLENBQXlCOUMsSUFBekIsQ0FBekU7QUFDRCxPQUZELE1BRU87QUFDSmhFLGdCQUFRM0YsU0FBVCxHQUFzQjVELE9BQU93QyxNQUFQLENBQWMwRSxFQUFkLENBQWlCcUMsR0FBakIsRUFBc0JtRixNQUF0QixDQUE2Qm5CLElBQTdCLENBQXRCLEdBQTJEdk4sT0FBT3lDLFNBQVAsQ0FBaUJpQyxNQUFqQixDQUF3QjZJLElBQXhCLENBQTNEO0FBQ0Q7O0FBRUQ7QUFDQXZOLGFBQU9xSixNQUFQLENBQWNFLEdBQWQsRUFBbUIsS0FBbkI7O0FBRUE7QUFDQXZKLGFBQU93QyxNQUFQLEdBQWdCN0MsRUFBRUssT0FBT0MsSUFBUCxDQUFZb0MsUUFBWixHQUF1QixjQUF6QixFQUF5Q3JDLE1BQXpDLENBQWhCO0FBQ0E7QUFDQUEsYUFBTzRFLEtBQVA7O0FBRUE7QUFDQTVFLGFBQU9DLElBQVAsQ0FBWWlSLEtBQVosQ0FBa0JsUixNQUFsQjtBQUNELEtBdkJEO0FBd0JBQSxXQUFPbVIsV0FBUCxHQUFxQixVQUFTNU4sR0FBVCxFQUFjO0FBQ2pDLFVBQUlnRyxNQUFPdkgsTUFBTXVCLEdBQU4sQ0FBRCxHQUFldkQsT0FBT3dDLE1BQVAsQ0FBYytFLEtBQWQsQ0FBb0I1SCxFQUFFNEQsR0FBRixDQUFwQixDQUFmLEdBQTZDQSxHQUF2RDs7QUFFQTtBQUNBdkQsYUFBTzBDLEtBQVAsSUFBZ0IsQ0FBaEI7QUFDQTFDLGFBQU9tQyxJQUFQLEdBQWNuQyxPQUFPMEMsS0FBUCxHQUFlLENBQTdCOztBQUVBO0FBQ0EsVUFBSVYsTUFBTXVCLEdBQU4sQ0FBSixFQUFnQjtBQUNkNUQsVUFBRTRELEdBQUYsRUFBT3ZELE9BQU93QyxNQUFkLEVBQXNCZ0gsTUFBdEI7QUFDRCxPQUZELE1BRU87QUFDSnhJLG9CQUFZRSxPQUFiLEdBQXdCbEIsT0FBT3dDLE1BQVAsQ0FBYzBFLEVBQWQsQ0FBaUJsSCxPQUFPbUMsSUFBeEIsRUFBOEJxSCxNQUE5QixFQUF4QixHQUFpRXhKLE9BQU93QyxNQUFQLENBQWMwRSxFQUFkLENBQWlCM0QsR0FBakIsRUFBc0JpRyxNQUF0QixFQUFqRTtBQUNEOztBQUVEO0FBQ0F4SixhQUFPMkUsTUFBUDtBQUNBM0UsYUFBT3FKLE1BQVAsQ0FBY0UsR0FBZCxFQUFtQixRQUFuQjs7QUFFQTtBQUNBdkosYUFBT3dDLE1BQVAsR0FBZ0I3QyxFQUFFSyxPQUFPQyxJQUFQLENBQVlvQyxRQUFaLEdBQXVCLGNBQXpCLEVBQXlDckMsTUFBekMsQ0FBaEI7QUFDQTtBQUNBQSxhQUFPNEUsS0FBUDs7QUFFQTtBQUNBNUUsYUFBT0MsSUFBUCxDQUFZbVIsT0FBWixDQUFvQnBSLE1BQXBCO0FBQ0QsS0F6QkQ7O0FBMkJBO0FBQ0F5QixZQUFRRSxJQUFSO0FBQ0QsR0FyakNEOztBQXVqQ0E7QUFDQWhDLElBQUdXLE1BQUgsRUFBWStRLElBQVosQ0FBa0IsVUFBV2hLLENBQVgsRUFBZTtBQUMvQnpILGNBQVUsS0FBVjtBQUNELEdBRkQsRUFFRzBSLEtBRkgsQ0FFVSxVQUFXakssQ0FBWCxFQUFlO0FBQ3ZCekgsY0FBVSxJQUFWO0FBQ0QsR0FKRDs7QUFNQTtBQUNBRCxJQUFFRSxVQUFGLENBQWFNLFFBQWIsR0FBd0I7QUFDdEJDLGVBQVcsT0FEVyxFQUNVO0FBQ2hDaUMsY0FBVSxjQUZZLEVBRVU7QUFDaENmLGVBQVcsTUFIVyxFQUdVO0FBQ2hDNk4sWUFBUSxPQUpjLEVBSVU7QUFDaENsTyxlQUFXLFlBTFcsRUFLVTtBQUNoQ0MsYUFBUyxLQU5hLEVBTVU7QUFDaEM0SSxtQkFBZSxJQVBPLEVBT1U7QUFDaENyRCxrQkFBYyxLQVJRLEVBUVU7QUFDaEMxRSxhQUFTLENBVGEsRUFTVTtBQUNoQ2dFLGVBQVcsSUFWVyxFQVVVO0FBQ2hDMEosb0JBQWdCLElBWE0sRUFXVTtBQUNoQ1Qsb0JBQWdCLEdBWk0sRUFZVTtBQUNoQ3pJLGVBQVcsQ0FiVyxFQWFVO0FBQ2hDbkMsZUFBVyxLQWRXLEVBY1U7QUFDaENtTSxvQkFBZ0IsSUFmTSxFQWVVO0FBQ2hDekgsbUJBQWUsS0FoQk8sRUFnQlU7O0FBRWhDO0FBQ0F0RCxtQkFBZSxJQW5CTyxFQW1CVTtBQUNoQ1Msa0JBQWMsS0FwQlEsRUFvQlU7QUFDaENELG9CQUFnQixJQXJCTSxFQXFCSTtBQUMxQjFDLFlBQVEsSUF0QmMsRUFzQlU7QUFDaEM1QyxXQUFPLElBdkJlLEVBdUJVO0FBQ2hDMkMsV0FBTyxLQXhCZSxFQXdCVTs7QUFFaEM7QUFDQXdCLGdCQUFZLElBM0JVLEVBMkJVO0FBQ2hDQyxrQkFBYyxJQTVCUSxFQTRCVTtBQUNoQzZFLGNBQVUsVUE3QlksRUE2QlU7QUFDaENDLGNBQVUsTUE5QlksRUE4QlU7O0FBRWhDO0FBQ0E3RSxjQUFVLElBakNZLEVBaUNVO0FBQ2hDQyxzQkFBa0IsS0FsQ0ksRUFrQ1U7QUFDaENTLGdCQUFZLEtBbkNVLEVBbUNVO0FBQ2hDSyxlQUFXLEtBcENXLEVBb0NVO0FBQ2hDdUUsZUFBVyxPQXJDVyxFQXFDVTtBQUNoQ0QsY0FBVSxNQXRDWSxFQXNDVTs7QUFFaEM7QUFDQW5HLHVCQUFtQixFQXpDRyxFQXlDVTtBQUNoQ0Msb0JBQWdCLEVBMUNNLEVBMENVO0FBQ2hDQyx3QkFBb0IsRUEzQ0UsRUEyQ1U7QUFDaEN0QixVQUFNLEVBNUNnQixFQTRDVTtBQUNoQ3JCLGNBQVUsRUE3Q1ksRUE2Q1U7O0FBRWhDO0FBQ0FKLGVBQVcsQ0FoRFcsRUFnRFU7QUFDaEN1SyxnQkFBWSxDQWpEVSxFQWlEVTtBQUNoQzhFLGNBQVUsQ0FsRFksRUFrRFU7QUFDaENDLGNBQVUsQ0FuRFksRUFtRFU7QUFDaEMzSixVQUFNLENBcERnQixFQW9EVTtBQUNoQ3dLLG1CQUFlLElBckRPLEVBcURTOztBQUUvQjtBQUNBMUssV0FBTyxpQkFBVSxDQUFFLENBeERHLEVBd0RVO0FBQ2hDNkgsWUFBUSxrQkFBVSxDQUFFLENBekRFLEVBeURVO0FBQ2hDVSxXQUFPLGlCQUFVLENBQUUsQ0ExREcsRUEwRFU7QUFDaENULFNBQUssZUFBVSxDQUFFLENBM0RLLEVBMkRVO0FBQ2hDdUMsV0FBTyxpQkFBVSxDQUFFLENBNURHLEVBNERVO0FBQ2hDRSxhQUFTLG1CQUFVLENBQUUsQ0E3REMsRUE2RFc7QUFDakN6UCxVQUFNLGdCQUFXLENBQUUsQ0E5REcsQ0E4RFU7QUE5RFYsR0FBeEI7O0FBaUVBO0FBQ0FoQyxJQUFFNlIsRUFBRixDQUFLM1IsVUFBTCxHQUFrQixVQUFTRSxPQUFULEVBQWtCO0FBQ2xDLFFBQUlBLFlBQVk2RCxTQUFoQixFQUEyQjtBQUFFN0QsZ0JBQVUsRUFBVjtBQUFlOztBQUU1QyxRQUFJLFFBQU9BLE9BQVAseUNBQU9BLE9BQVAsT0FBbUIsUUFBdkIsRUFBaUM7QUFDL0IsYUFBTyxLQUFLK0gsSUFBTCxDQUFVLFlBQVc7QUFDMUIsWUFBSXFCLFFBQVF4SixFQUFFLElBQUYsQ0FBWjtBQUFBLFlBQ0kwQyxXQUFZdEMsUUFBUXNDLFFBQVQsR0FBcUJ0QyxRQUFRc0MsUUFBN0IsR0FBd0MsY0FEdkQ7QUFBQSxZQUVJb1AsVUFBVXRJLE1BQU14QyxJQUFOLENBQVd0RSxRQUFYLENBRmQ7O0FBSUYsWUFBT29QLFFBQVE5TyxNQUFSLEtBQW1CLENBQW5CLElBQXdCNUMsUUFBUXdSLGFBQVIsS0FBMEIsS0FBcEQsSUFBK0RFLFFBQVE5TyxNQUFSLEtBQW1CLENBQXZGLEVBQTJGO0FBQ3ZGOE8sa0JBQVFDLE1BQVIsQ0FBZSxHQUFmO0FBQ0EsY0FBSTNSLFFBQVE4RyxLQUFaLEVBQW1CO0FBQUU5RyxvQkFBUThHLEtBQVIsQ0FBY3NDLEtBQWQ7QUFBdUI7QUFDN0MsU0FISCxNQUdTLElBQUlBLE1BQU16SCxJQUFOLENBQVcsWUFBWCxNQUE2QmtDLFNBQWpDLEVBQTRDO0FBQ2pELGNBQUlqRSxFQUFFRSxVQUFOLENBQWlCLElBQWpCLEVBQXVCRSxPQUF2QjtBQUNEO0FBQ0YsT0FYTSxDQUFQO0FBWUQsS0FiRCxNQWFPO0FBQ0w7QUFDQSxVQUFJNFIsVUFBVWhTLEVBQUUsSUFBRixFQUFRK0IsSUFBUixDQUFhLFlBQWIsQ0FBZDtBQUNBLGNBQVEzQixPQUFSO0FBQ0UsYUFBSyxNQUFMO0FBQWE0UixrQkFBUXRMLElBQVIsR0FBZ0I7QUFDN0IsYUFBSyxPQUFMO0FBQWNzTCxrQkFBUXZMLEtBQVIsR0FBaUI7QUFDL0IsYUFBSyxNQUFMO0FBQWF1TCxrQkFBUTlFLElBQVIsR0FBZ0I7QUFDN0IsYUFBSyxNQUFMO0FBQWE4RSxrQkFBUXBNLFdBQVIsQ0FBb0JvTSxRQUFRck0sU0FBUixDQUFrQixNQUFsQixDQUFwQixFQUErQyxJQUEvQyxFQUFzRDtBQUNuRSxhQUFLLE1BQUw7QUFDQSxhQUFLLFVBQUw7QUFBaUJxTSxrQkFBUXBNLFdBQVIsQ0FBb0JvTSxRQUFRck0sU0FBUixDQUFrQixNQUFsQixDQUFwQixFQUErQyxJQUEvQyxFQUFzRDtBQUN2RTtBQUFTLGNBQUksT0FBT3ZGLE9BQVAsS0FBbUIsUUFBdkIsRUFBaUM7QUFBRTRSLG9CQUFRcE0sV0FBUixDQUFvQnhGLE9BQXBCLEVBQTZCLElBQTdCO0FBQXFDO0FBUG5GO0FBU0Q7QUFDRixHQTdCRDtBQThCRCxDQXBxQ0QsRUFvcUNHNlIsTUFwcUNIIiwiZmlsZSI6InBsdWdpbnMvanF1ZXJ5LmZsZXhzbGlkZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxyXG4gKiBqUXVlcnkgRmxleFNsaWRlciB2Mi42LjJcclxuICogQ29weXJpZ2h0IDIwMTIgV29vVGhlbWVzXHJcbiAqIENvbnRyaWJ1dGluZyBBdXRob3I6IFR5bGVyIFNtaXRoXHJcbiAqL1xyXG47XHJcbihmdW5jdGlvbiAoJCkge1xyXG5cclxuICB2YXIgZm9jdXNlZCA9IHRydWU7XHJcblxyXG4gIC8vRmxleFNsaWRlcjogT2JqZWN0IEluc3RhbmNlXHJcbiAgJC5mbGV4c2xpZGVyID0gZnVuY3Rpb24oZWwsIG9wdGlvbnMpIHtcclxuICAgIHZhciBzbGlkZXIgPSAkKGVsKTtcclxuXHJcbiAgICAvLyBtYWtpbmcgdmFyaWFibGVzIHB1YmxpY1xyXG4gICAgc2xpZGVyLnZhcnMgPSAkLmV4dGVuZCh7fSwgJC5mbGV4c2xpZGVyLmRlZmF1bHRzLCBvcHRpb25zKTtcclxuXHJcbiAgICB2YXIgbmFtZXNwYWNlID0gc2xpZGVyLnZhcnMubmFtZXNwYWNlLFxyXG4gICAgICAgIG1zR2VzdHVyZSA9IHdpbmRvdy5uYXZpZ2F0b3IgJiYgd2luZG93Lm5hdmlnYXRvci5tc1BvaW50ZXJFbmFibGVkICYmIHdpbmRvdy5NU0dlc3R1cmUsXHJcbiAgICAgICAgdG91Y2ggPSAoKCBcIm9udG91Y2hzdGFydFwiIGluIHdpbmRvdyApIHx8IG1zR2VzdHVyZSB8fCB3aW5kb3cuRG9jdW1lbnRUb3VjaCAmJiBkb2N1bWVudCBpbnN0YW5jZW9mIERvY3VtZW50VG91Y2gpICYmIHNsaWRlci52YXJzLnRvdWNoLFxyXG4gICAgICAgIC8vIGRlcHJpY2F0aW5nIHRoaXMgaWRlYSwgYXMgZGV2aWNlcyBhcmUgYmVpbmcgcmVsZWFzZWQgd2l0aCBib3RoIG9mIHRoZXNlIGV2ZW50c1xyXG4gICAgICAgIGV2ZW50VHlwZSA9IFwiY2xpY2sgdG91Y2hlbmQgTVNQb2ludGVyVXAga2V5dXBcIixcclxuICAgICAgICB3YXRjaGVkRXZlbnQgPSBcIlwiLFxyXG4gICAgICAgIHdhdGNoZWRFdmVudENsZWFyVGltZXIsXHJcbiAgICAgICAgdmVydGljYWwgPSBzbGlkZXIudmFycy5kaXJlY3Rpb24gPT09IFwidmVydGljYWxcIixcclxuICAgICAgICByZXZlcnNlID0gc2xpZGVyLnZhcnMucmV2ZXJzZSxcclxuICAgICAgICBjYXJvdXNlbCA9IChzbGlkZXIudmFycy5pdGVtV2lkdGggPiAwKSxcclxuICAgICAgICBmYWRlID0gc2xpZGVyLnZhcnMuYW5pbWF0aW9uID09PSBcImZhZGVcIixcclxuICAgICAgICBhc05hdiA9IHNsaWRlci52YXJzLmFzTmF2Rm9yICE9PSBcIlwiLFxyXG4gICAgICAgIG1ldGhvZHMgPSB7fTtcclxuXHJcbiAgICAvLyBTdG9yZSBhIHJlZmVyZW5jZSB0byB0aGUgc2xpZGVyIG9iamVjdFxyXG4gICAgJC5kYXRhKGVsLCBcImZsZXhzbGlkZXJcIiwgc2xpZGVyKTtcclxuXHJcbiAgICAvLyBQcml2YXRlIHNsaWRlciBtZXRob2RzXHJcbiAgICBtZXRob2RzID0ge1xyXG4gICAgICBpbml0OiBmdW5jdGlvbigpIHtcclxuICAgICAgICBzbGlkZXIuYW5pbWF0aW5nID0gZmFsc2U7XHJcbiAgICAgICAgLy8gR2V0IGN1cnJlbnQgc2xpZGUgYW5kIG1ha2Ugc3VyZSBpdCBpcyBhIG51bWJlclxyXG4gICAgICAgIHNsaWRlci5jdXJyZW50U2xpZGUgPSBwYXJzZUludCggKCBzbGlkZXIudmFycy5zdGFydEF0ID8gc2xpZGVyLnZhcnMuc3RhcnRBdCA6IDApLCAxMCApO1xyXG4gICAgICAgIGlmICggaXNOYU4oIHNsaWRlci5jdXJyZW50U2xpZGUgKSApIHsgc2xpZGVyLmN1cnJlbnRTbGlkZSA9IDA7IH1cclxuICAgICAgICBzbGlkZXIuYW5pbWF0aW5nVG8gPSBzbGlkZXIuY3VycmVudFNsaWRlO1xyXG4gICAgICAgIHNsaWRlci5hdEVuZCA9IChzbGlkZXIuY3VycmVudFNsaWRlID09PSAwIHx8IHNsaWRlci5jdXJyZW50U2xpZGUgPT09IHNsaWRlci5sYXN0KTtcclxuICAgICAgICBzbGlkZXIuY29udGFpbmVyU2VsZWN0b3IgPSBzbGlkZXIudmFycy5zZWxlY3Rvci5zdWJzdHIoMCxzbGlkZXIudmFycy5zZWxlY3Rvci5zZWFyY2goJyAnKSk7XHJcbiAgICAgICAgc2xpZGVyLnNsaWRlcyA9ICQoc2xpZGVyLnZhcnMuc2VsZWN0b3IsIHNsaWRlcik7XHJcbiAgICAgICAgc2xpZGVyLmNvbnRhaW5lciA9ICQoc2xpZGVyLmNvbnRhaW5lclNlbGVjdG9yLCBzbGlkZXIpO1xyXG4gICAgICAgIHNsaWRlci5jb3VudCA9IHNsaWRlci5zbGlkZXMubGVuZ3RoO1xyXG4gICAgICAgIC8vIFNZTkM6XHJcbiAgICAgICAgc2xpZGVyLnN5bmNFeGlzdHMgPSAkKHNsaWRlci52YXJzLnN5bmMpLmxlbmd0aCA+IDA7XHJcbiAgICAgICAgLy8gU0xJREU6XHJcbiAgICAgICAgaWYgKHNsaWRlci52YXJzLmFuaW1hdGlvbiA9PT0gXCJzbGlkZVwiKSB7IHNsaWRlci52YXJzLmFuaW1hdGlvbiA9IFwic3dpbmdcIjsgfVxyXG4gICAgICAgIHNsaWRlci5wcm9wID0gKHZlcnRpY2FsKSA/IFwidG9wXCIgOiBcIm1hcmdpbkxlZnRcIjtcclxuICAgICAgICBzbGlkZXIuYXJncyA9IHt9O1xyXG4gICAgICAgIC8vIFNMSURFU0hPVzpcclxuICAgICAgICBzbGlkZXIubWFudWFsUGF1c2UgPSBmYWxzZTtcclxuICAgICAgICBzbGlkZXIuc3RvcHBlZCA9IGZhbHNlO1xyXG4gICAgICAgIC8vUEFVU0UgV0hFTiBJTlZJU0lCTEVcclxuICAgICAgICBzbGlkZXIuc3RhcnRlZCA9IGZhbHNlO1xyXG4gICAgICAgIHNsaWRlci5zdGFydFRpbWVvdXQgPSBudWxsO1xyXG4gICAgICAgIC8vIFRPVUNIL1VTRUNTUzpcclxuICAgICAgICBzbGlkZXIudHJhbnNpdGlvbnMgPSAhc2xpZGVyLnZhcnMudmlkZW8gJiYgIWZhZGUgJiYgc2xpZGVyLnZhcnMudXNlQ1NTICYmIChmdW5jdGlvbigpIHtcclxuICAgICAgICAgIHZhciBvYmogPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKSxcclxuICAgICAgICAgICAgICBwcm9wcyA9IFsncGVyc3BlY3RpdmVQcm9wZXJ0eScsICdXZWJraXRQZXJzcGVjdGl2ZScsICdNb3pQZXJzcGVjdGl2ZScsICdPUGVyc3BlY3RpdmUnLCAnbXNQZXJzcGVjdGl2ZSddO1xyXG4gICAgICAgICAgZm9yICh2YXIgaSBpbiBwcm9wcykge1xyXG4gICAgICAgICAgICBpZiAoIG9iai5zdHlsZVsgcHJvcHNbaV0gXSAhPT0gdW5kZWZpbmVkICkge1xyXG4gICAgICAgICAgICAgIHNsaWRlci5wZnggPSBwcm9wc1tpXS5yZXBsYWNlKCdQZXJzcGVjdGl2ZScsJycpLnRvTG93ZXJDYXNlKCk7XHJcbiAgICAgICAgICAgICAgc2xpZGVyLnByb3AgPSBcIi1cIiArIHNsaWRlci5wZnggKyBcIi10cmFuc2Zvcm1cIjtcclxuICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH0oKSk7XHJcbiAgICAgICAgc2xpZGVyLmVuc3VyZUFuaW1hdGlvbkVuZCA9ICcnO1xyXG4gICAgICAgIC8vIENPTlRST0xTQ09OVEFJTkVSOlxyXG4gICAgICAgIGlmIChzbGlkZXIudmFycy5jb250cm9sc0NvbnRhaW5lciAhPT0gXCJcIikgc2xpZGVyLmNvbnRyb2xzQ29udGFpbmVyID0gJChzbGlkZXIudmFycy5jb250cm9sc0NvbnRhaW5lcikubGVuZ3RoID4gMCAmJiAkKHNsaWRlci52YXJzLmNvbnRyb2xzQ29udGFpbmVyKTtcclxuICAgICAgICAvLyBNQU5VQUw6XHJcbiAgICAgICAgaWYgKHNsaWRlci52YXJzLm1hbnVhbENvbnRyb2xzICE9PSBcIlwiKSBzbGlkZXIubWFudWFsQ29udHJvbHMgPSAkKHNsaWRlci52YXJzLm1hbnVhbENvbnRyb2xzKS5sZW5ndGggPiAwICYmICQoc2xpZGVyLnZhcnMubWFudWFsQ29udHJvbHMpO1xyXG5cclxuICAgICAgICAvLyBDVVNUT00gRElSRUNUSU9OIE5BVjpcclxuICAgICAgICBpZiAoc2xpZGVyLnZhcnMuY3VzdG9tRGlyZWN0aW9uTmF2ICE9PSBcIlwiKSBzbGlkZXIuY3VzdG9tRGlyZWN0aW9uTmF2ID0gJChzbGlkZXIudmFycy5jdXN0b21EaXJlY3Rpb25OYXYpLmxlbmd0aCA9PT0gMiAmJiAkKHNsaWRlci52YXJzLmN1c3RvbURpcmVjdGlvbk5hdik7XHJcblxyXG4gICAgICAgIC8vIFJBTkRPTUlaRTpcclxuICAgICAgICBpZiAoc2xpZGVyLnZhcnMucmFuZG9taXplKSB7XHJcbiAgICAgICAgICBzbGlkZXIuc2xpZGVzLnNvcnQoZnVuY3Rpb24oKSB7IHJldHVybiAoTWF0aC5yb3VuZChNYXRoLnJhbmRvbSgpKS0wLjUpOyB9KTtcclxuICAgICAgICAgIHNsaWRlci5jb250YWluZXIuZW1wdHkoKS5hcHBlbmQoc2xpZGVyLnNsaWRlcyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBzbGlkZXIuZG9NYXRoKCk7XHJcblxyXG4gICAgICAgIC8vIElOSVRcclxuICAgICAgICBzbGlkZXIuc2V0dXAoXCJpbml0XCIpO1xyXG5cclxuICAgICAgICAvLyBDT05UUk9MTkFWOlxyXG4gICAgICAgIGlmIChzbGlkZXIudmFycy5jb250cm9sTmF2KSB7IG1ldGhvZHMuY29udHJvbE5hdi5zZXR1cCgpOyB9XHJcblxyXG4gICAgICAgIC8vIERJUkVDVElPTk5BVjpcclxuICAgICAgICBpZiAoc2xpZGVyLnZhcnMuZGlyZWN0aW9uTmF2KSB7IG1ldGhvZHMuZGlyZWN0aW9uTmF2LnNldHVwKCk7IH1cclxuXHJcbiAgICAgICAgLy8gS0VZQk9BUkQ6XHJcbiAgICAgICAgaWYgKHNsaWRlci52YXJzLmtleWJvYXJkICYmICgkKHNsaWRlci5jb250YWluZXJTZWxlY3RvcikubGVuZ3RoID09PSAxIHx8IHNsaWRlci52YXJzLm11bHRpcGxlS2V5Ym9hcmQpKSB7XHJcbiAgICAgICAgICAkKGRvY3VtZW50KS5iaW5kKCdrZXl1cCcsIGZ1bmN0aW9uKGV2ZW50KSB7XHJcbiAgICAgICAgICAgIHZhciBrZXljb2RlID0gZXZlbnQua2V5Q29kZTtcclxuICAgICAgICAgICAgaWYgKCFzbGlkZXIuYW5pbWF0aW5nICYmIChrZXljb2RlID09PSAzOSB8fCBrZXljb2RlID09PSAzNykpIHtcclxuICAgICAgICAgICAgICB2YXIgdGFyZ2V0ID0gKGtleWNvZGUgPT09IDM5KSA/IHNsaWRlci5nZXRUYXJnZXQoJ25leHQnKSA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIChrZXljb2RlID09PSAzNykgPyBzbGlkZXIuZ2V0VGFyZ2V0KCdwcmV2JykgOiBmYWxzZTtcclxuICAgICAgICAgICAgICBzbGlkZXIuZmxleEFuaW1hdGUodGFyZ2V0LCBzbGlkZXIudmFycy5wYXVzZU9uQWN0aW9uKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIE1PVVNFV0hFRUw6XHJcbiAgICAgICAgaWYgKHNsaWRlci52YXJzLm1vdXNld2hlZWwpIHtcclxuICAgICAgICAgIHNsaWRlci5iaW5kKCdtb3VzZXdoZWVsJywgZnVuY3Rpb24oZXZlbnQsIGRlbHRhLCBkZWx0YVgsIGRlbHRhWSkge1xyXG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICB2YXIgdGFyZ2V0ID0gKGRlbHRhIDwgMCkgPyBzbGlkZXIuZ2V0VGFyZ2V0KCduZXh0JykgOiBzbGlkZXIuZ2V0VGFyZ2V0KCdwcmV2Jyk7XHJcbiAgICAgICAgICAgIHNsaWRlci5mbGV4QW5pbWF0ZSh0YXJnZXQsIHNsaWRlci52YXJzLnBhdXNlT25BY3Rpb24pO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBQQVVTRVBMQVlcclxuICAgICAgICBpZiAoc2xpZGVyLnZhcnMucGF1c2VQbGF5KSB7IG1ldGhvZHMucGF1c2VQbGF5LnNldHVwKCk7IH1cclxuXHJcbiAgICAgICAgLy9QQVVTRSBXSEVOIElOVklTSUJMRVxyXG4gICAgICAgIGlmIChzbGlkZXIudmFycy5zbGlkZXNob3cgJiYgc2xpZGVyLnZhcnMucGF1c2VJbnZpc2libGUpIHsgbWV0aG9kcy5wYXVzZUludmlzaWJsZS5pbml0KCk7IH1cclxuXHJcbiAgICAgICAgLy8gU0xJRFNFU0hPV1xyXG4gICAgICAgIGlmIChzbGlkZXIudmFycy5zbGlkZXNob3cpIHtcclxuICAgICAgICAgIGlmIChzbGlkZXIudmFycy5wYXVzZU9uSG92ZXIpIHtcclxuICAgICAgICAgICAgc2xpZGVyLmhvdmVyKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgIGlmICghc2xpZGVyLm1hbnVhbFBsYXkgJiYgIXNsaWRlci5tYW51YWxQYXVzZSkgeyBzbGlkZXIucGF1c2UoKTsgfVxyXG4gICAgICAgICAgICB9LCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICBpZiAoIXNsaWRlci5tYW51YWxQYXVzZSAmJiAhc2xpZGVyLm1hbnVhbFBsYXkgJiYgIXNsaWRlci5zdG9wcGVkKSB7IHNsaWRlci5wbGF5KCk7IH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICAvLyBpbml0aWFsaXplIGFuaW1hdGlvblxyXG4gICAgICAgICAgLy9JZiB3ZSdyZSB2aXNpYmxlLCBvciB3ZSBkb24ndCB1c2UgUGFnZVZpc2liaWxpdHkgQVBJXHJcbiAgICAgICAgICBpZighc2xpZGVyLnZhcnMucGF1c2VJbnZpc2libGUgfHwgIW1ldGhvZHMucGF1c2VJbnZpc2libGUuaXNIaWRkZW4oKSkge1xyXG4gICAgICAgICAgICAoc2xpZGVyLnZhcnMuaW5pdERlbGF5ID4gMCkgPyBzbGlkZXIuc3RhcnRUaW1lb3V0ID0gc2V0VGltZW91dChzbGlkZXIucGxheSwgc2xpZGVyLnZhcnMuaW5pdERlbGF5KSA6IHNsaWRlci5wbGF5KCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBBU05BVjpcclxuICAgICAgICBpZiAoYXNOYXYpIHsgbWV0aG9kcy5hc05hdi5zZXR1cCgpOyB9XHJcblxyXG4gICAgICAgIC8vIFRPVUNIXHJcbiAgICAgICAgaWYgKHRvdWNoICYmIHNsaWRlci52YXJzLnRvdWNoKSB7IG1ldGhvZHMudG91Y2goKTsgfVxyXG5cclxuICAgICAgICAvLyBGQURFJiZTTU9PVEhIRUlHSFQgfHwgU0xJREU6XHJcbiAgICAgICAgaWYgKCFmYWRlIHx8IChmYWRlICYmIHNsaWRlci52YXJzLnNtb290aEhlaWdodCkpIHsgJCh3aW5kb3cpLmJpbmQoXCJyZXNpemUgb3JpZW50YXRpb25jaGFuZ2UgZm9jdXNcIiwgbWV0aG9kcy5yZXNpemUpOyB9XHJcblxyXG4gICAgICAgIHNsaWRlci5maW5kKFwiaW1nXCIpLmF0dHIoXCJkcmFnZ2FibGVcIiwgXCJmYWxzZVwiKTtcclxuXHJcbiAgICAgICAgLy8gQVBJOiBzdGFydCgpIENhbGxiYWNrXHJcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpe1xyXG4gICAgICAgICAgc2xpZGVyLnZhcnMuc3RhcnQoc2xpZGVyKTtcclxuICAgICAgICB9LCAyMDApO1xyXG4gICAgICB9LFxyXG4gICAgICBhc05hdjoge1xyXG4gICAgICAgIHNldHVwOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgIHNsaWRlci5hc05hdiA9IHRydWU7XHJcbiAgICAgICAgICBzbGlkZXIuYW5pbWF0aW5nVG8gPSBNYXRoLmZsb29yKHNsaWRlci5jdXJyZW50U2xpZGUvc2xpZGVyLm1vdmUpO1xyXG4gICAgICAgICAgc2xpZGVyLmN1cnJlbnRJdGVtID0gc2xpZGVyLmN1cnJlbnRTbGlkZTtcclxuICAgICAgICAgIHNsaWRlci5zbGlkZXMucmVtb3ZlQ2xhc3MobmFtZXNwYWNlICsgXCJhY3RpdmUtc2xpZGVcIikuZXEoc2xpZGVyLmN1cnJlbnRJdGVtKS5hZGRDbGFzcyhuYW1lc3BhY2UgKyBcImFjdGl2ZS1zbGlkZVwiKTtcclxuICAgICAgICAgIGlmKCFtc0dlc3R1cmUpe1xyXG4gICAgICAgICAgICAgIHNsaWRlci5zbGlkZXMub24oZXZlbnRUeXBlLCBmdW5jdGlvbihlKXtcclxuICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgICAgIHZhciAkc2xpZGUgPSAkKHRoaXMpLFxyXG4gICAgICAgICAgICAgICAgICAgIHRhcmdldCA9ICRzbGlkZS5pbmRleCgpO1xyXG4gICAgICAgICAgICAgICAgdmFyIHBvc0Zyb21MZWZ0ID0gJHNsaWRlLm9mZnNldCgpLmxlZnQgLSAkKHNsaWRlcikuc2Nyb2xsTGVmdCgpOyAvLyBGaW5kIHBvc2l0aW9uIG9mIHNsaWRlIHJlbGF0aXZlIHRvIGxlZnQgb2Ygc2xpZGVyIGNvbnRhaW5lclxyXG4gICAgICAgICAgICAgICAgaWYoIHBvc0Zyb21MZWZ0IDw9IDAgJiYgJHNsaWRlLmhhc0NsYXNzKCBuYW1lc3BhY2UgKyAnYWN0aXZlLXNsaWRlJyApICkge1xyXG4gICAgICAgICAgICAgICAgICBzbGlkZXIuZmxleEFuaW1hdGUoc2xpZGVyLmdldFRhcmdldChcInByZXZcIiksIHRydWUpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICghJChzbGlkZXIudmFycy5hc05hdkZvcikuZGF0YSgnZmxleHNsaWRlcicpLmFuaW1hdGluZyAmJiAhJHNsaWRlLmhhc0NsYXNzKG5hbWVzcGFjZSArIFwiYWN0aXZlLXNsaWRlXCIpKSB7XHJcbiAgICAgICAgICAgICAgICAgIHNsaWRlci5kaXJlY3Rpb24gPSAoc2xpZGVyLmN1cnJlbnRJdGVtIDwgdGFyZ2V0KSA/IFwibmV4dFwiIDogXCJwcmV2XCI7XHJcbiAgICAgICAgICAgICAgICAgIHNsaWRlci5mbGV4QW5pbWF0ZSh0YXJnZXQsIHNsaWRlci52YXJzLnBhdXNlT25BY3Rpb24sIGZhbHNlLCB0cnVlLCB0cnVlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAgIGVsLl9zbGlkZXIgPSBzbGlkZXI7XHJcbiAgICAgICAgICAgICAgc2xpZGVyLnNsaWRlcy5lYWNoKGZ1bmN0aW9uICgpe1xyXG4gICAgICAgICAgICAgICAgICB2YXIgdGhhdCA9IHRoaXM7XHJcbiAgICAgICAgICAgICAgICAgIHRoYXQuX2dlc3R1cmUgPSBuZXcgTVNHZXN0dXJlKCk7XHJcbiAgICAgICAgICAgICAgICAgIHRoYXQuX2dlc3R1cmUudGFyZ2V0ID0gdGhhdDtcclxuICAgICAgICAgICAgICAgICAgdGhhdC5hZGRFdmVudExpc3RlbmVyKFwiTVNQb2ludGVyRG93blwiLCBmdW5jdGlvbiAoZSl7XHJcbiAgICAgICAgICAgICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICBpZihlLmN1cnJlbnRUYXJnZXQuX2dlc3R1cmUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZS5jdXJyZW50VGFyZ2V0Ll9nZXN0dXJlLmFkZFBvaW50ZXIoZS5wb2ludGVySWQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICB9LCBmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgIHRoYXQuYWRkRXZlbnRMaXN0ZW5lcihcIk1TR2VzdHVyZVRhcFwiLCBmdW5jdGlvbiAoZSl7XHJcbiAgICAgICAgICAgICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICB2YXIgJHNsaWRlID0gJCh0aGlzKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICB0YXJnZXQgPSAkc2xpZGUuaW5kZXgoKTtcclxuICAgICAgICAgICAgICAgICAgICAgIGlmICghJChzbGlkZXIudmFycy5hc05hdkZvcikuZGF0YSgnZmxleHNsaWRlcicpLmFuaW1hdGluZyAmJiAhJHNsaWRlLmhhc0NsYXNzKCdhY3RpdmUnKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHNsaWRlci5kaXJlY3Rpb24gPSAoc2xpZGVyLmN1cnJlbnRJdGVtIDwgdGFyZ2V0KSA/IFwibmV4dFwiIDogXCJwcmV2XCI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgc2xpZGVyLmZsZXhBbmltYXRlKHRhcmdldCwgc2xpZGVyLnZhcnMucGF1c2VPbkFjdGlvbiwgZmFsc2UsIHRydWUsIHRydWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH0sXHJcbiAgICAgIGNvbnRyb2xOYXY6IHtcclxuICAgICAgICBzZXR1cDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICBpZiAoIXNsaWRlci5tYW51YWxDb250cm9scykge1xyXG4gICAgICAgICAgICBtZXRob2RzLmNvbnRyb2xOYXYuc2V0dXBQYWdpbmcoKTtcclxuICAgICAgICAgIH0gZWxzZSB7IC8vIE1BTlVBTENPTlRST0xTOlxyXG4gICAgICAgICAgICBtZXRob2RzLmNvbnRyb2xOYXYuc2V0dXBNYW51YWwoKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHNldHVwUGFnaW5nOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgIHZhciB0eXBlID0gKHNsaWRlci52YXJzLmNvbnRyb2xOYXYgPT09IFwidGh1bWJuYWlsc1wiKSA/ICdjb250cm9sLXRodW1icycgOiAnY29udHJvbC1wYWdpbmcnLFxyXG4gICAgICAgICAgICAgIGogPSAxLFxyXG4gICAgICAgICAgICAgIGl0ZW0sXHJcbiAgICAgICAgICAgICAgc2xpZGU7XHJcblxyXG4gICAgICAgICAgc2xpZGVyLmNvbnRyb2xOYXZTY2FmZm9sZCA9ICQoJzxvbCBjbGFzcz1cIicrIG5hbWVzcGFjZSArICdjb250cm9sLW5hdiAnICsgbmFtZXNwYWNlICsgdHlwZSArICdcIj48L29sPicpO1xyXG5cclxuICAgICAgICAgIGlmIChzbGlkZXIucGFnaW5nQ291bnQgPiAxKSB7XHJcbiAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgc2xpZGVyLnBhZ2luZ0NvdW50OyBpKyspIHtcclxuICAgICAgICAgICAgICBzbGlkZSA9IHNsaWRlci5zbGlkZXMuZXEoaSk7XHJcbiAgICAgICAgICAgICAgaWYgKCB1bmRlZmluZWQgPT09IHNsaWRlLmF0dHIoICdkYXRhLXRodW1iLWFsdCcgKSApIHsgc2xpZGUuYXR0ciggJ2RhdGEtdGh1bWItYWx0JywgJycgKTsgfVxyXG4gICAgICAgICAgICAgIHZhciBhbHRUZXh0ID0gKCAnJyAhPT0gc2xpZGUuYXR0ciggJ2RhdGEtdGh1bWItYWx0JyApICkgPyBhbHRUZXh0ID0gJyBhbHQ9XCInICsgc2xpZGUuYXR0ciggJ2RhdGEtdGh1bWItYWx0JyApICsgJ1wiJyA6ICcnO1xyXG4gICAgICAgICAgICAgIGl0ZW0gPSAoc2xpZGVyLnZhcnMuY29udHJvbE5hdiA9PT0gXCJ0aHVtYm5haWxzXCIpID8gJzxpbWcgc3JjPVwiJyArIHNsaWRlLmF0dHIoICdkYXRhLXRodW1iJyApICsgJ1wiJyArIGFsdFRleHQgKyAnLz4nIDogJzxhIGhyZWY9XCIjXCI+JyArIGogKyAnPC9hPic7XHJcbiAgICAgICAgICAgICAgaWYgKCAndGh1bWJuYWlscycgPT09IHNsaWRlci52YXJzLmNvbnRyb2xOYXYgJiYgdHJ1ZSA9PT0gc2xpZGVyLnZhcnMudGh1bWJDYXB0aW9ucyApIHtcclxuICAgICAgICAgICAgICAgIHZhciBjYXB0biA9IHNsaWRlLmF0dHIoICdkYXRhLXRodW1iY2FwdGlvbicgKTtcclxuICAgICAgICAgICAgICAgIGlmICggJycgIT09IGNhcHRuICYmIHVuZGVmaW5lZCAhPT0gY2FwdG4gKSB7IGl0ZW0gKz0gJzxzcGFuIGNsYXNzPVwiJyArIG5hbWVzcGFjZSArICdjYXB0aW9uXCI+JyArIGNhcHRuICsgJzwvc3Bhbj4nOyB9XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIHNsaWRlci5jb250cm9sTmF2U2NhZmZvbGQuYXBwZW5kKCc8bGk+JyArIGl0ZW0gKyAnPC9saT4nKTtcclxuICAgICAgICAgICAgICBqKys7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAvLyBDT05UUk9MU0NPTlRBSU5FUjpcclxuICAgICAgICAgIChzbGlkZXIuY29udHJvbHNDb250YWluZXIpID8gJChzbGlkZXIuY29udHJvbHNDb250YWluZXIpLmFwcGVuZChzbGlkZXIuY29udHJvbE5hdlNjYWZmb2xkKSA6IHNsaWRlci5hcHBlbmQoc2xpZGVyLmNvbnRyb2xOYXZTY2FmZm9sZCk7XHJcbiAgICAgICAgICBtZXRob2RzLmNvbnRyb2xOYXYuc2V0KCk7XHJcblxyXG4gICAgICAgICAgbWV0aG9kcy5jb250cm9sTmF2LmFjdGl2ZSgpO1xyXG5cclxuICAgICAgICAgIHNsaWRlci5jb250cm9sTmF2U2NhZmZvbGQuZGVsZWdhdGUoJ2EsIGltZycsIGV2ZW50VHlwZSwgZnVuY3Rpb24oZXZlbnQpIHtcclxuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICAgICAgICAgIGlmICh3YXRjaGVkRXZlbnQgPT09IFwiXCIgfHwgd2F0Y2hlZEV2ZW50ID09PSBldmVudC50eXBlKSB7XHJcbiAgICAgICAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKSxcclxuICAgICAgICAgICAgICAgICAgdGFyZ2V0ID0gc2xpZGVyLmNvbnRyb2xOYXYuaW5kZXgoJHRoaXMpO1xyXG5cclxuICAgICAgICAgICAgICBpZiAoISR0aGlzLmhhc0NsYXNzKG5hbWVzcGFjZSArICdhY3RpdmUnKSkge1xyXG4gICAgICAgICAgICAgICAgc2xpZGVyLmRpcmVjdGlvbiA9ICh0YXJnZXQgPiBzbGlkZXIuY3VycmVudFNsaWRlKSA/IFwibmV4dFwiIDogXCJwcmV2XCI7XHJcbiAgICAgICAgICAgICAgICBzbGlkZXIuZmxleEFuaW1hdGUodGFyZ2V0LCBzbGlkZXIudmFycy5wYXVzZU9uQWN0aW9uKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIHNldHVwIGZsYWdzIHRvIHByZXZlbnQgZXZlbnQgZHVwbGljYXRpb25cclxuICAgICAgICAgICAgaWYgKHdhdGNoZWRFdmVudCA9PT0gXCJcIikge1xyXG4gICAgICAgICAgICAgIHdhdGNoZWRFdmVudCA9IGV2ZW50LnR5cGU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgbWV0aG9kcy5zZXRUb0NsZWFyV2F0Y2hlZEV2ZW50KCk7XHJcblxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBzZXR1cE1hbnVhbDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICBzbGlkZXIuY29udHJvbE5hdiA9IHNsaWRlci5tYW51YWxDb250cm9scztcclxuICAgICAgICAgIG1ldGhvZHMuY29udHJvbE5hdi5hY3RpdmUoKTtcclxuXHJcbiAgICAgICAgICBzbGlkZXIuY29udHJvbE5hdi5iaW5kKGV2ZW50VHlwZSwgZnVuY3Rpb24oZXZlbnQpIHtcclxuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICAgICAgICAgIGlmICh3YXRjaGVkRXZlbnQgPT09IFwiXCIgfHwgd2F0Y2hlZEV2ZW50ID09PSBldmVudC50eXBlKSB7XHJcbiAgICAgICAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKSxcclxuICAgICAgICAgICAgICAgICAgdGFyZ2V0ID0gc2xpZGVyLmNvbnRyb2xOYXYuaW5kZXgoJHRoaXMpO1xyXG5cclxuICAgICAgICAgICAgICBpZiAoISR0aGlzLmhhc0NsYXNzKG5hbWVzcGFjZSArICdhY3RpdmUnKSkge1xyXG4gICAgICAgICAgICAgICAgKHRhcmdldCA+IHNsaWRlci5jdXJyZW50U2xpZGUpID8gc2xpZGVyLmRpcmVjdGlvbiA9IFwibmV4dFwiIDogc2xpZGVyLmRpcmVjdGlvbiA9IFwicHJldlwiO1xyXG4gICAgICAgICAgICAgICAgc2xpZGVyLmZsZXhBbmltYXRlKHRhcmdldCwgc2xpZGVyLnZhcnMucGF1c2VPbkFjdGlvbik7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvLyBzZXR1cCBmbGFncyB0byBwcmV2ZW50IGV2ZW50IGR1cGxpY2F0aW9uXHJcbiAgICAgICAgICAgIGlmICh3YXRjaGVkRXZlbnQgPT09IFwiXCIpIHtcclxuICAgICAgICAgICAgICB3YXRjaGVkRXZlbnQgPSBldmVudC50eXBlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIG1ldGhvZHMuc2V0VG9DbGVhcldhdGNoZWRFdmVudCgpO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBzZXQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgdmFyIHNlbGVjdG9yID0gKHNsaWRlci52YXJzLmNvbnRyb2xOYXYgPT09IFwidGh1bWJuYWlsc1wiKSA/ICdpbWcnIDogJ2EnO1xyXG4gICAgICAgICAgc2xpZGVyLmNvbnRyb2xOYXYgPSAkKCcuJyArIG5hbWVzcGFjZSArICdjb250cm9sLW5hdiBsaSAnICsgc2VsZWN0b3IsIChzbGlkZXIuY29udHJvbHNDb250YWluZXIpID8gc2xpZGVyLmNvbnRyb2xzQ29udGFpbmVyIDogc2xpZGVyKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGFjdGl2ZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICBzbGlkZXIuY29udHJvbE5hdi5yZW1vdmVDbGFzcyhuYW1lc3BhY2UgKyBcImFjdGl2ZVwiKS5lcShzbGlkZXIuYW5pbWF0aW5nVG8pLmFkZENsYXNzKG5hbWVzcGFjZSArIFwiYWN0aXZlXCIpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgdXBkYXRlOiBmdW5jdGlvbihhY3Rpb24sIHBvcykge1xyXG4gICAgICAgICAgaWYgKHNsaWRlci5wYWdpbmdDb3VudCA+IDEgJiYgYWN0aW9uID09PSBcImFkZFwiKSB7XHJcbiAgICAgICAgICAgIHNsaWRlci5jb250cm9sTmF2U2NhZmZvbGQuYXBwZW5kKCQoJzxsaT48YSBocmVmPVwiI1wiPicgKyBzbGlkZXIuY291bnQgKyAnPC9hPjwvbGk+JykpO1xyXG4gICAgICAgICAgfSBlbHNlIGlmIChzbGlkZXIucGFnaW5nQ291bnQgPT09IDEpIHtcclxuICAgICAgICAgICAgc2xpZGVyLmNvbnRyb2xOYXZTY2FmZm9sZC5maW5kKCdsaScpLnJlbW92ZSgpO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgc2xpZGVyLmNvbnRyb2xOYXYuZXEocG9zKS5jbG9zZXN0KCdsaScpLnJlbW92ZSgpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgbWV0aG9kcy5jb250cm9sTmF2LnNldCgpO1xyXG4gICAgICAgICAgKHNsaWRlci5wYWdpbmdDb3VudCA+IDEgJiYgc2xpZGVyLnBhZ2luZ0NvdW50ICE9PSBzbGlkZXIuY29udHJvbE5hdi5sZW5ndGgpID8gc2xpZGVyLnVwZGF0ZShwb3MsIGFjdGlvbikgOiBtZXRob2RzLmNvbnRyb2xOYXYuYWN0aXZlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9LFxyXG4gICAgICBkaXJlY3Rpb25OYXY6IHtcclxuICAgICAgICBzZXR1cDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICB2YXIgZGlyZWN0aW9uTmF2U2NhZmZvbGQgPSAkKCc8dWwgY2xhc3M9XCInICsgbmFtZXNwYWNlICsgJ2RpcmVjdGlvbi1uYXZcIj48bGkgY2xhc3M9XCInICsgbmFtZXNwYWNlICsgJ25hdi1wcmV2XCI+PGEgY2xhc3M9XCInICsgbmFtZXNwYWNlICsgJ3ByZXZcIiBocmVmPVwiI1wiPicgKyBzbGlkZXIudmFycy5wcmV2VGV4dCArICc8L2E+PC9saT48bGkgY2xhc3M9XCInICsgbmFtZXNwYWNlICsgJ25hdi1uZXh0XCI+PGEgY2xhc3M9XCInICsgbmFtZXNwYWNlICsgJ25leHRcIiBocmVmPVwiI1wiPicgKyBzbGlkZXIudmFycy5uZXh0VGV4dCArICc8L2E+PC9saT48L3VsPicpO1xyXG5cclxuICAgICAgICAgIC8vIENVU1RPTSBESVJFQ1RJT04gTkFWOlxyXG4gICAgICAgICAgaWYgKHNsaWRlci5jdXN0b21EaXJlY3Rpb25OYXYpIHtcclxuICAgICAgICAgICAgc2xpZGVyLmRpcmVjdGlvbk5hdiA9IHNsaWRlci5jdXN0b21EaXJlY3Rpb25OYXY7XHJcbiAgICAgICAgICAvLyBDT05UUk9MU0NPTlRBSU5FUjpcclxuICAgICAgICAgIH0gZWxzZSBpZiAoc2xpZGVyLmNvbnRyb2xzQ29udGFpbmVyKSB7XHJcbiAgICAgICAgICAgICQoc2xpZGVyLmNvbnRyb2xzQ29udGFpbmVyKS5hcHBlbmQoZGlyZWN0aW9uTmF2U2NhZmZvbGQpO1xyXG4gICAgICAgICAgICBzbGlkZXIuZGlyZWN0aW9uTmF2ID0gJCgnLicgKyBuYW1lc3BhY2UgKyAnZGlyZWN0aW9uLW5hdiBsaSBhJywgc2xpZGVyLmNvbnRyb2xzQ29udGFpbmVyKTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHNsaWRlci5hcHBlbmQoZGlyZWN0aW9uTmF2U2NhZmZvbGQpO1xyXG4gICAgICAgICAgICBzbGlkZXIuZGlyZWN0aW9uTmF2ID0gJCgnLicgKyBuYW1lc3BhY2UgKyAnZGlyZWN0aW9uLW5hdiBsaSBhJywgc2xpZGVyKTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBtZXRob2RzLmRpcmVjdGlvbk5hdi51cGRhdGUoKTtcclxuXHJcbiAgICAgICAgICBzbGlkZXIuZGlyZWN0aW9uTmF2LmJpbmQoZXZlbnRUeXBlLCBmdW5jdGlvbihldmVudCkge1xyXG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICB2YXIgdGFyZ2V0O1xyXG5cclxuICAgICAgICAgICAgaWYgKHdhdGNoZWRFdmVudCA9PT0gXCJcIiB8fCB3YXRjaGVkRXZlbnQgPT09IGV2ZW50LnR5cGUpIHtcclxuICAgICAgICAgICAgICB0YXJnZXQgPSAoJCh0aGlzKS5oYXNDbGFzcyhuYW1lc3BhY2UgKyAnbmV4dCcpKSA/IHNsaWRlci5nZXRUYXJnZXQoJ25leHQnKSA6IHNsaWRlci5nZXRUYXJnZXQoJ3ByZXYnKTtcclxuICAgICAgICAgICAgICBzbGlkZXIuZmxleEFuaW1hdGUodGFyZ2V0LCBzbGlkZXIudmFycy5wYXVzZU9uQWN0aW9uKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gc2V0dXAgZmxhZ3MgdG8gcHJldmVudCBldmVudCBkdXBsaWNhdGlvblxyXG4gICAgICAgICAgICBpZiAod2F0Y2hlZEV2ZW50ID09PSBcIlwiKSB7XHJcbiAgICAgICAgICAgICAgd2F0Y2hlZEV2ZW50ID0gZXZlbnQudHlwZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBtZXRob2RzLnNldFRvQ2xlYXJXYXRjaGVkRXZlbnQoKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgdXBkYXRlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgIHZhciBkaXNhYmxlZENsYXNzID0gbmFtZXNwYWNlICsgJ2Rpc2FibGVkJztcclxuICAgICAgICAgIGlmIChzbGlkZXIucGFnaW5nQ291bnQgPT09IDEpIHtcclxuICAgICAgICAgICAgc2xpZGVyLmRpcmVjdGlvbk5hdi5hZGRDbGFzcyhkaXNhYmxlZENsYXNzKS5hdHRyKCd0YWJpbmRleCcsICctMScpO1xyXG4gICAgICAgICAgfSBlbHNlIGlmICghc2xpZGVyLnZhcnMuYW5pbWF0aW9uTG9vcCkge1xyXG4gICAgICAgICAgICBpZiAoc2xpZGVyLmFuaW1hdGluZ1RvID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgc2xpZGVyLmRpcmVjdGlvbk5hdi5yZW1vdmVDbGFzcyhkaXNhYmxlZENsYXNzKS5maWx0ZXIoJy4nICsgbmFtZXNwYWNlICsgXCJwcmV2XCIpLmFkZENsYXNzKGRpc2FibGVkQ2xhc3MpLmF0dHIoJ3RhYmluZGV4JywgJy0xJyk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoc2xpZGVyLmFuaW1hdGluZ1RvID09PSBzbGlkZXIubGFzdCkge1xyXG4gICAgICAgICAgICAgIHNsaWRlci5kaXJlY3Rpb25OYXYucmVtb3ZlQ2xhc3MoZGlzYWJsZWRDbGFzcykuZmlsdGVyKCcuJyArIG5hbWVzcGFjZSArIFwibmV4dFwiKS5hZGRDbGFzcyhkaXNhYmxlZENsYXNzKS5hdHRyKCd0YWJpbmRleCcsICctMScpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIHNsaWRlci5kaXJlY3Rpb25OYXYucmVtb3ZlQ2xhc3MoZGlzYWJsZWRDbGFzcykucmVtb3ZlQXR0cigndGFiaW5kZXgnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgc2xpZGVyLmRpcmVjdGlvbk5hdi5yZW1vdmVDbGFzcyhkaXNhYmxlZENsYXNzKS5yZW1vdmVBdHRyKCd0YWJpbmRleCcpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfSxcclxuICAgICAgcGF1c2VQbGF5OiB7XHJcbiAgICAgICAgc2V0dXA6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgdmFyIHBhdXNlUGxheVNjYWZmb2xkID0gJCgnPGRpdiBjbGFzcz1cIicgKyBuYW1lc3BhY2UgKyAncGF1c2VwbGF5XCI+PGEgaHJlZj1cIiNcIj48L2E+PC9kaXY+Jyk7XHJcblxyXG4gICAgICAgICAgLy8gQ09OVFJPTFNDT05UQUlORVI6XHJcbiAgICAgICAgICBpZiAoc2xpZGVyLmNvbnRyb2xzQ29udGFpbmVyKSB7XHJcbiAgICAgICAgICAgIHNsaWRlci5jb250cm9sc0NvbnRhaW5lci5hcHBlbmQocGF1c2VQbGF5U2NhZmZvbGQpO1xyXG4gICAgICAgICAgICBzbGlkZXIucGF1c2VQbGF5ID0gJCgnLicgKyBuYW1lc3BhY2UgKyAncGF1c2VwbGF5IGEnLCBzbGlkZXIuY29udHJvbHNDb250YWluZXIpO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgc2xpZGVyLmFwcGVuZChwYXVzZVBsYXlTY2FmZm9sZCk7XHJcbiAgICAgICAgICAgIHNsaWRlci5wYXVzZVBsYXkgPSAkKCcuJyArIG5hbWVzcGFjZSArICdwYXVzZXBsYXkgYScsIHNsaWRlcik7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgbWV0aG9kcy5wYXVzZVBsYXkudXBkYXRlKChzbGlkZXIudmFycy5zbGlkZXNob3cpID8gbmFtZXNwYWNlICsgJ3BhdXNlJyA6IG5hbWVzcGFjZSArICdwbGF5Jyk7XHJcblxyXG4gICAgICAgICAgc2xpZGVyLnBhdXNlUGxheS5iaW5kKGV2ZW50VHlwZSwgZnVuY3Rpb24oZXZlbnQpIHtcclxuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICAgICAgICAgIGlmICh3YXRjaGVkRXZlbnQgPT09IFwiXCIgfHwgd2F0Y2hlZEV2ZW50ID09PSBldmVudC50eXBlKSB7XHJcbiAgICAgICAgICAgICAgaWYgKCQodGhpcykuaGFzQ2xhc3MobmFtZXNwYWNlICsgJ3BhdXNlJykpIHtcclxuICAgICAgICAgICAgICAgIHNsaWRlci5tYW51YWxQYXVzZSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICBzbGlkZXIubWFudWFsUGxheSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgc2xpZGVyLnBhdXNlKCk7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHNsaWRlci5tYW51YWxQYXVzZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgc2xpZGVyLm1hbnVhbFBsYXkgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgc2xpZGVyLnBsYXkoKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIHNldHVwIGZsYWdzIHRvIHByZXZlbnQgZXZlbnQgZHVwbGljYXRpb25cclxuICAgICAgICAgICAgaWYgKHdhdGNoZWRFdmVudCA9PT0gXCJcIikge1xyXG4gICAgICAgICAgICAgIHdhdGNoZWRFdmVudCA9IGV2ZW50LnR5cGU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgbWV0aG9kcy5zZXRUb0NsZWFyV2F0Y2hlZEV2ZW50KCk7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIHVwZGF0ZTogZnVuY3Rpb24oc3RhdGUpIHtcclxuICAgICAgICAgIChzdGF0ZSA9PT0gXCJwbGF5XCIpID8gc2xpZGVyLnBhdXNlUGxheS5yZW1vdmVDbGFzcyhuYW1lc3BhY2UgKyAncGF1c2UnKS5hZGRDbGFzcyhuYW1lc3BhY2UgKyAncGxheScpLmh0bWwoc2xpZGVyLnZhcnMucGxheVRleHQpIDogc2xpZGVyLnBhdXNlUGxheS5yZW1vdmVDbGFzcyhuYW1lc3BhY2UgKyAncGxheScpLmFkZENsYXNzKG5hbWVzcGFjZSArICdwYXVzZScpLmh0bWwoc2xpZGVyLnZhcnMucGF1c2VUZXh0KTtcclxuICAgICAgICB9XHJcbiAgICAgIH0sXHJcbiAgICAgIHRvdWNoOiBmdW5jdGlvbigpIHtcclxuICAgICAgICB2YXIgc3RhcnRYLFxyXG4gICAgICAgICAgc3RhcnRZLFxyXG4gICAgICAgICAgb2Zmc2V0LFxyXG4gICAgICAgICAgY3dpZHRoLFxyXG4gICAgICAgICAgZHgsXHJcbiAgICAgICAgICBzdGFydFQsXHJcbiAgICAgICAgICBvblRvdWNoU3RhcnQsXHJcbiAgICAgICAgICBvblRvdWNoTW92ZSxcclxuICAgICAgICAgIG9uVG91Y2hFbmQsXHJcbiAgICAgICAgICBzY3JvbGxpbmcgPSBmYWxzZSxcclxuICAgICAgICAgIGxvY2FsWCA9IDAsXHJcbiAgICAgICAgICBsb2NhbFkgPSAwLFxyXG4gICAgICAgICAgYWNjRHggPSAwO1xyXG5cclxuICAgICAgICBpZighbXNHZXN0dXJlKXtcclxuICAgICAgICAgICAgb25Ub3VjaFN0YXJ0ID0gZnVuY3Rpb24oZSkge1xyXG4gICAgICAgICAgICAgIGlmIChzbGlkZXIuYW5pbWF0aW5nKSB7XHJcbiAgICAgICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIGlmICggKCB3aW5kb3cubmF2aWdhdG9yLm1zUG9pbnRlckVuYWJsZWQgKSB8fCBlLnRvdWNoZXMubGVuZ3RoID09PSAxICkge1xyXG4gICAgICAgICAgICAgICAgc2xpZGVyLnBhdXNlKCk7XHJcbiAgICAgICAgICAgICAgICAvLyBDQVJPVVNFTDpcclxuICAgICAgICAgICAgICAgIGN3aWR0aCA9ICh2ZXJ0aWNhbCkgPyBzbGlkZXIuaCA6IHNsaWRlci4gdztcclxuICAgICAgICAgICAgICAgIHN0YXJ0VCA9IE51bWJlcihuZXcgRGF0ZSgpKTtcclxuICAgICAgICAgICAgICAgIC8vIENBUk9VU0VMOlxyXG5cclxuICAgICAgICAgICAgICAgIC8vIExvY2FsIHZhcnMgZm9yIFggYW5kIFkgcG9pbnRzLlxyXG4gICAgICAgICAgICAgICAgbG9jYWxYID0gZS50b3VjaGVzWzBdLnBhZ2VYO1xyXG4gICAgICAgICAgICAgICAgbG9jYWxZID0gZS50b3VjaGVzWzBdLnBhZ2VZO1xyXG5cclxuICAgICAgICAgICAgICAgIG9mZnNldCA9IChjYXJvdXNlbCAmJiByZXZlcnNlICYmIHNsaWRlci5hbmltYXRpbmdUbyA9PT0gc2xpZGVyLmxhc3QpID8gMCA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAoY2Fyb3VzZWwgJiYgcmV2ZXJzZSkgPyBzbGlkZXIubGltaXQgLSAoKChzbGlkZXIuaXRlbVcgKyBzbGlkZXIudmFycy5pdGVtTWFyZ2luKSAqIHNsaWRlci5tb3ZlKSAqIHNsaWRlci5hbmltYXRpbmdUbykgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgKGNhcm91c2VsICYmIHNsaWRlci5jdXJyZW50U2xpZGUgPT09IHNsaWRlci5sYXN0KSA/IHNsaWRlci5saW1pdCA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAoY2Fyb3VzZWwpID8gKChzbGlkZXIuaXRlbVcgKyBzbGlkZXIudmFycy5pdGVtTWFyZ2luKSAqIHNsaWRlci5tb3ZlKSAqIHNsaWRlci5jdXJyZW50U2xpZGUgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgKHJldmVyc2UpID8gKHNsaWRlci5sYXN0IC0gc2xpZGVyLmN1cnJlbnRTbGlkZSArIHNsaWRlci5jbG9uZU9mZnNldCkgKiBjd2lkdGggOiAoc2xpZGVyLmN1cnJlbnRTbGlkZSArIHNsaWRlci5jbG9uZU9mZnNldCkgKiBjd2lkdGg7XHJcbiAgICAgICAgICAgICAgICBzdGFydFggPSAodmVydGljYWwpID8gbG9jYWxZIDogbG9jYWxYO1xyXG4gICAgICAgICAgICAgICAgc3RhcnRZID0gKHZlcnRpY2FsKSA/IGxvY2FsWCA6IGxvY2FsWTtcclxuXHJcbiAgICAgICAgICAgICAgICBlbC5hZGRFdmVudExpc3RlbmVyKCd0b3VjaG1vdmUnLCBvblRvdWNoTW92ZSwgZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgZWwuYWRkRXZlbnRMaXN0ZW5lcigndG91Y2hlbmQnLCBvblRvdWNoRW5kLCBmYWxzZSk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgb25Ub3VjaE1vdmUgPSBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgICAgICAgLy8gTG9jYWwgdmFycyBmb3IgWCBhbmQgWSBwb2ludHMuXHJcblxyXG4gICAgICAgICAgICAgIGxvY2FsWCA9IGUudG91Y2hlc1swXS5wYWdlWDtcclxuICAgICAgICAgICAgICBsb2NhbFkgPSBlLnRvdWNoZXNbMF0ucGFnZVk7XHJcblxyXG4gICAgICAgICAgICAgIGR4ID0gKHZlcnRpY2FsKSA/IHN0YXJ0WCAtIGxvY2FsWSA6IHN0YXJ0WCAtIGxvY2FsWDtcclxuICAgICAgICAgICAgICBzY3JvbGxpbmcgPSAodmVydGljYWwpID8gKE1hdGguYWJzKGR4KSA8IE1hdGguYWJzKGxvY2FsWCAtIHN0YXJ0WSkpIDogKE1hdGguYWJzKGR4KSA8IE1hdGguYWJzKGxvY2FsWSAtIHN0YXJ0WSkpO1xyXG5cclxuICAgICAgICAgICAgICB2YXIgZnhtcyA9IDUwMDtcclxuXHJcbiAgICAgICAgICAgICAgaWYgKCAhIHNjcm9sbGluZyB8fCBOdW1iZXIoIG5ldyBEYXRlKCkgKSAtIHN0YXJ0VCA+IGZ4bXMgKSB7XHJcbiAgICAgICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgICAgICBpZiAoIWZhZGUgJiYgc2xpZGVyLnRyYW5zaXRpb25zKSB7XHJcbiAgICAgICAgICAgICAgICAgIGlmICghc2xpZGVyLnZhcnMuYW5pbWF0aW9uTG9vcCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGR4ID0gZHgvKChzbGlkZXIuY3VycmVudFNsaWRlID09PSAwICYmIGR4IDwgMCB8fCBzbGlkZXIuY3VycmVudFNsaWRlID09PSBzbGlkZXIubGFzdCAmJiBkeCA+IDApID8gKE1hdGguYWJzKGR4KS9jd2lkdGgrMikgOiAxKTtcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICBzbGlkZXIuc2V0UHJvcHMob2Zmc2V0ICsgZHgsIFwic2V0VG91Y2hcIik7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgb25Ub3VjaEVuZCA9IGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICAgICAgICAvLyBmaW5pc2ggdGhlIHRvdWNoIGJ5IHVuZG9pbmcgdGhlIHRvdWNoIHNlc3Npb25cclxuICAgICAgICAgICAgICBlbC5yZW1vdmVFdmVudExpc3RlbmVyKCd0b3VjaG1vdmUnLCBvblRvdWNoTW92ZSwgZmFsc2UpO1xyXG5cclxuICAgICAgICAgICAgICBpZiAoc2xpZGVyLmFuaW1hdGluZ1RvID09PSBzbGlkZXIuY3VycmVudFNsaWRlICYmICFzY3JvbGxpbmcgJiYgIShkeCA9PT0gbnVsbCkpIHtcclxuICAgICAgICAgICAgICAgIHZhciB1cGRhdGVEeCA9IChyZXZlcnNlKSA/IC1keCA6IGR4LFxyXG4gICAgICAgICAgICAgICAgICAgIHRhcmdldCA9ICh1cGRhdGVEeCA+IDApID8gc2xpZGVyLmdldFRhcmdldCgnbmV4dCcpIDogc2xpZGVyLmdldFRhcmdldCgncHJldicpO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmIChzbGlkZXIuY2FuQWR2YW5jZSh0YXJnZXQpICYmIChOdW1iZXIobmV3IERhdGUoKSkgLSBzdGFydFQgPCA1NTAgJiYgTWF0aC5hYnModXBkYXRlRHgpID4gNTAgfHwgTWF0aC5hYnModXBkYXRlRHgpID4gY3dpZHRoLzIpKSB7XHJcbiAgICAgICAgICAgICAgICAgIHNsaWRlci5mbGV4QW5pbWF0ZSh0YXJnZXQsIHNsaWRlci52YXJzLnBhdXNlT25BY3Rpb24pO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgaWYgKCFmYWRlKSB7IHNsaWRlci5mbGV4QW5pbWF0ZShzbGlkZXIuY3VycmVudFNsaWRlLCBzbGlkZXIudmFycy5wYXVzZU9uQWN0aW9uLCB0cnVlKTsgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICBlbC5yZW1vdmVFdmVudExpc3RlbmVyKCd0b3VjaGVuZCcsIG9uVG91Y2hFbmQsIGZhbHNlKTtcclxuXHJcbiAgICAgICAgICAgICAgc3RhcnRYID0gbnVsbDtcclxuICAgICAgICAgICAgICBzdGFydFkgPSBudWxsO1xyXG4gICAgICAgICAgICAgIGR4ID0gbnVsbDtcclxuICAgICAgICAgICAgICBvZmZzZXQgPSBudWxsO1xyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgZWwuYWRkRXZlbnRMaXN0ZW5lcigndG91Y2hzdGFydCcsIG9uVG91Y2hTdGFydCwgZmFsc2UpO1xyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICBlbC5zdHlsZS5tc1RvdWNoQWN0aW9uID0gXCJub25lXCI7XHJcbiAgICAgICAgICAgIGVsLl9nZXN0dXJlID0gbmV3IE1TR2VzdHVyZSgpO1xyXG4gICAgICAgICAgICBlbC5fZ2VzdHVyZS50YXJnZXQgPSBlbDtcclxuICAgICAgICAgICAgZWwuYWRkRXZlbnRMaXN0ZW5lcihcIk1TUG9pbnRlckRvd25cIiwgb25NU1BvaW50ZXJEb3duLCBmYWxzZSk7XHJcbiAgICAgICAgICAgIGVsLl9zbGlkZXIgPSBzbGlkZXI7XHJcbiAgICAgICAgICAgIGVsLmFkZEV2ZW50TGlzdGVuZXIoXCJNU0dlc3R1cmVDaGFuZ2VcIiwgb25NU0dlc3R1cmVDaGFuZ2UsIGZhbHNlKTtcclxuICAgICAgICAgICAgZWwuYWRkRXZlbnRMaXN0ZW5lcihcIk1TR2VzdHVyZUVuZFwiLCBvbk1TR2VzdHVyZUVuZCwgZmFsc2UpO1xyXG5cclxuICAgICAgICAgICAgZnVuY3Rpb24gb25NU1BvaW50ZXJEb3duKGUpe1xyXG4gICAgICAgICAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICAgICAgICAgIGlmIChzbGlkZXIuYW5pbWF0aW5nKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICAgICAgc2xpZGVyLnBhdXNlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgZWwuX2dlc3R1cmUuYWRkUG9pbnRlcihlLnBvaW50ZXJJZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgYWNjRHggPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgIGN3aWR0aCA9ICh2ZXJ0aWNhbCkgPyBzbGlkZXIuaCA6IHNsaWRlci4gdztcclxuICAgICAgICAgICAgICAgICAgICBzdGFydFQgPSBOdW1iZXIobmV3IERhdGUoKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gQ0FST1VTRUw6XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIG9mZnNldCA9IChjYXJvdXNlbCAmJiByZXZlcnNlICYmIHNsaWRlci5hbmltYXRpbmdUbyA9PT0gc2xpZGVyLmxhc3QpID8gMCA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIChjYXJvdXNlbCAmJiByZXZlcnNlKSA/IHNsaWRlci5saW1pdCAtICgoKHNsaWRlci5pdGVtVyArIHNsaWRlci52YXJzLml0ZW1NYXJnaW4pICogc2xpZGVyLm1vdmUpICogc2xpZGVyLmFuaW1hdGluZ1RvKSA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAoY2Fyb3VzZWwgJiYgc2xpZGVyLmN1cnJlbnRTbGlkZSA9PT0gc2xpZGVyLmxhc3QpID8gc2xpZGVyLmxpbWl0IDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAoY2Fyb3VzZWwpID8gKChzbGlkZXIuaXRlbVcgKyBzbGlkZXIudmFycy5pdGVtTWFyZ2luKSAqIHNsaWRlci5tb3ZlKSAqIHNsaWRlci5jdXJyZW50U2xpZGUgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAocmV2ZXJzZSkgPyAoc2xpZGVyLmxhc3QgLSBzbGlkZXIuY3VycmVudFNsaWRlICsgc2xpZGVyLmNsb25lT2Zmc2V0KSAqIGN3aWR0aCA6IChzbGlkZXIuY3VycmVudFNsaWRlICsgc2xpZGVyLmNsb25lT2Zmc2V0KSAqIGN3aWR0aDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgZnVuY3Rpb24gb25NU0dlc3R1cmVDaGFuZ2UoZSkge1xyXG4gICAgICAgICAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICAgICAgICAgIHZhciBzbGlkZXIgPSBlLnRhcmdldC5fc2xpZGVyO1xyXG4gICAgICAgICAgICAgICAgaWYoIXNsaWRlcil7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdmFyIHRyYW5zWCA9IC1lLnRyYW5zbGF0aW9uWCxcclxuICAgICAgICAgICAgICAgICAgICB0cmFuc1kgPSAtZS50cmFuc2xhdGlvblk7XHJcblxyXG4gICAgICAgICAgICAgICAgLy9BY2N1bXVsYXRlIHRyYW5zbGF0aW9ucy5cclxuICAgICAgICAgICAgICAgIGFjY0R4ID0gYWNjRHggKyAoKHZlcnRpY2FsKSA/IHRyYW5zWSA6IHRyYW5zWCk7XHJcbiAgICAgICAgICAgICAgICBkeCA9IGFjY0R4O1xyXG4gICAgICAgICAgICAgICAgc2Nyb2xsaW5nID0gKHZlcnRpY2FsKSA/IChNYXRoLmFicyhhY2NEeCkgPCBNYXRoLmFicygtdHJhbnNYKSkgOiAoTWF0aC5hYnMoYWNjRHgpIDwgTWF0aC5hYnMoLXRyYW5zWSkpO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmKGUuZGV0YWlsID09PSBlLk1TR0VTVFVSRV9GTEFHX0lORVJUSUEpe1xyXG4gICAgICAgICAgICAgICAgICAgIHNldEltbWVkaWF0ZShmdW5jdGlvbiAoKXtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZWwuX2dlc3R1cmUuc3RvcCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKCFzY3JvbGxpbmcgfHwgTnVtYmVyKG5ldyBEYXRlKCkpIC0gc3RhcnRUID4gNTAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghZmFkZSAmJiBzbGlkZXIudHJhbnNpdGlvbnMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFzbGlkZXIudmFycy5hbmltYXRpb25Mb29wKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkeCA9IGFjY0R4IC8gKChzbGlkZXIuY3VycmVudFNsaWRlID09PSAwICYmIGFjY0R4IDwgMCB8fCBzbGlkZXIuY3VycmVudFNsaWRlID09PSBzbGlkZXIubGFzdCAmJiBhY2NEeCA+IDApID8gKE1hdGguYWJzKGFjY0R4KSAvIGN3aWR0aCArIDIpIDogMSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgc2xpZGVyLnNldFByb3BzKG9mZnNldCArIGR4LCBcInNldFRvdWNoXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgZnVuY3Rpb24gb25NU0dlc3R1cmVFbmQoZSkge1xyXG4gICAgICAgICAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICAgICAgICAgIHZhciBzbGlkZXIgPSBlLnRhcmdldC5fc2xpZGVyO1xyXG4gICAgICAgICAgICAgICAgaWYoIXNsaWRlcil7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKHNsaWRlci5hbmltYXRpbmdUbyA9PT0gc2xpZGVyLmN1cnJlbnRTbGlkZSAmJiAhc2Nyb2xsaW5nICYmICEoZHggPT09IG51bGwpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIHVwZGF0ZUR4ID0gKHJldmVyc2UpID8gLWR4IDogZHgsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRhcmdldCA9ICh1cGRhdGVEeCA+IDApID8gc2xpZGVyLmdldFRhcmdldCgnbmV4dCcpIDogc2xpZGVyLmdldFRhcmdldCgncHJldicpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAoc2xpZGVyLmNhbkFkdmFuY2UodGFyZ2V0KSAmJiAoTnVtYmVyKG5ldyBEYXRlKCkpIC0gc3RhcnRUIDwgNTUwICYmIE1hdGguYWJzKHVwZGF0ZUR4KSA+IDUwIHx8IE1hdGguYWJzKHVwZGF0ZUR4KSA+IGN3aWR0aC8yKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzbGlkZXIuZmxleEFuaW1hdGUodGFyZ2V0LCBzbGlkZXIudmFycy5wYXVzZU9uQWN0aW9uKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoIWZhZGUpIHsgc2xpZGVyLmZsZXhBbmltYXRlKHNsaWRlci5jdXJyZW50U2xpZGUsIHNsaWRlci52YXJzLnBhdXNlT25BY3Rpb24sIHRydWUpOyB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHN0YXJ0WCA9IG51bGw7XHJcbiAgICAgICAgICAgICAgICBzdGFydFkgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgZHggPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgb2Zmc2V0ID0gbnVsbDtcclxuICAgICAgICAgICAgICAgIGFjY0R4ID0gMDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfSxcclxuICAgICAgcmVzaXplOiBmdW5jdGlvbigpIHtcclxuICAgICAgICBpZiAoIXNsaWRlci5hbmltYXRpbmcgJiYgc2xpZGVyLmlzKCc6dmlzaWJsZScpKSB7XHJcbiAgICAgICAgICBpZiAoIWNhcm91c2VsKSB7IHNsaWRlci5kb01hdGgoKTsgfVxyXG5cclxuICAgICAgICAgIGlmIChmYWRlKSB7XHJcbiAgICAgICAgICAgIC8vIFNNT09USCBIRUlHSFQ6XHJcbiAgICAgICAgICAgIG1ldGhvZHMuc21vb3RoSGVpZ2h0KCk7XHJcbiAgICAgICAgICB9IGVsc2UgaWYgKGNhcm91c2VsKSB7IC8vQ0FST1VTRUw6XHJcbiAgICAgICAgICAgIHNsaWRlci5zbGlkZXMud2lkdGgoc2xpZGVyLmNvbXB1dGVkVyk7XHJcbiAgICAgICAgICAgIHNsaWRlci51cGRhdGUoc2xpZGVyLnBhZ2luZ0NvdW50KTtcclxuICAgICAgICAgICAgc2xpZGVyLnNldFByb3BzKCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBlbHNlIGlmICh2ZXJ0aWNhbCkgeyAvL1ZFUlRJQ0FMOlxyXG4gICAgICAgICAgICBzbGlkZXIudmlld3BvcnQuaGVpZ2h0KHNsaWRlci5oKTtcclxuICAgICAgICAgICAgc2xpZGVyLnNldFByb3BzKHNsaWRlci5oLCBcInNldFRvdGFsXCIpO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgLy8gU01PT1RIIEhFSUdIVDpcclxuICAgICAgICAgICAgaWYgKHNsaWRlci52YXJzLnNtb290aEhlaWdodCkgeyBtZXRob2RzLnNtb290aEhlaWdodCgpOyB9XHJcbiAgICAgICAgICAgIHNsaWRlci5uZXdTbGlkZXMud2lkdGgoc2xpZGVyLmNvbXB1dGVkVyk7XHJcbiAgICAgICAgICAgIHNsaWRlci5zZXRQcm9wcyhzbGlkZXIuY29tcHV0ZWRXLCBcInNldFRvdGFsXCIpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfSxcclxuICAgICAgc21vb3RoSGVpZ2h0OiBmdW5jdGlvbihkdXIpIHtcclxuICAgICAgICBpZiAoIXZlcnRpY2FsIHx8IGZhZGUpIHtcclxuICAgICAgICAgIHZhciAkb2JqID0gKGZhZGUpID8gc2xpZGVyIDogc2xpZGVyLnZpZXdwb3J0O1xyXG4gICAgICAgICAgKGR1cikgPyAkb2JqLmFuaW1hdGUoe1wiaGVpZ2h0XCI6IHNsaWRlci5zbGlkZXMuZXEoc2xpZGVyLmFuaW1hdGluZ1RvKS5pbm5lckhlaWdodCgpfSwgZHVyKS5jc3MoJ292ZXJmbG93JywgJ3Zpc2libGUnKSA6ICRvYmouaW5uZXJIZWlnaHQoc2xpZGVyLnNsaWRlcy5lcShzbGlkZXIuYW5pbWF0aW5nVG8pLmlubmVySGVpZ2h0KCkpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSxcclxuICAgICAgc3luYzogZnVuY3Rpb24oYWN0aW9uKSB7XHJcbiAgICAgICAgdmFyICRvYmogPSAkKHNsaWRlci52YXJzLnN5bmMpLmRhdGEoXCJmbGV4c2xpZGVyXCIpLFxyXG4gICAgICAgICAgICB0YXJnZXQgPSBzbGlkZXIuYW5pbWF0aW5nVG87XHJcblxyXG4gICAgICAgIHN3aXRjaCAoYWN0aW9uKSB7XHJcbiAgICAgICAgICBjYXNlIFwiYW5pbWF0ZVwiOiAkb2JqLmZsZXhBbmltYXRlKHRhcmdldCwgc2xpZGVyLnZhcnMucGF1c2VPbkFjdGlvbiwgZmFsc2UsIHRydWUpOyBicmVhaztcclxuICAgICAgICAgIGNhc2UgXCJwbGF5XCI6IGlmICghJG9iai5wbGF5aW5nICYmICEkb2JqLmFzTmF2KSB7ICRvYmoucGxheSgpOyB9IGJyZWFrO1xyXG4gICAgICAgICAgY2FzZSBcInBhdXNlXCI6ICRvYmoucGF1c2UoKTsgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgICB9LFxyXG4gICAgICB1bmlxdWVJRDogZnVuY3Rpb24oJGNsb25lKSB7XHJcbiAgICAgICAgLy8gQXBwZW5kIF9jbG9uZSB0byBjdXJyZW50IGxldmVsIGFuZCBjaGlsZHJlbiBlbGVtZW50cyB3aXRoIGlkIGF0dHJpYnV0ZXNcclxuICAgICAgICAkY2xvbmUuZmlsdGVyKCAnW2lkXScgKS5hZGQoJGNsb25lLmZpbmQoICdbaWRdJyApKS5lYWNoKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKTtcclxuICAgICAgICAgICR0aGlzLmF0dHIoICdpZCcsICR0aGlzLmF0dHIoICdpZCcgKSArICdfY2xvbmUnICk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuICRjbG9uZTtcclxuICAgICAgfSxcclxuICAgICAgcGF1c2VJbnZpc2libGU6IHtcclxuICAgICAgICB2aXNQcm9wOiBudWxsLFxyXG4gICAgICAgIGluaXQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgdmFyIHZpc1Byb3AgPSBtZXRob2RzLnBhdXNlSW52aXNpYmxlLmdldEhpZGRlblByb3AoKTtcclxuICAgICAgICAgIGlmICh2aXNQcm9wKSB7XHJcbiAgICAgICAgICAgIHZhciBldnRuYW1lID0gdmlzUHJvcC5yZXBsYWNlKC9bSHxoXWlkZGVuLywnJykgKyAndmlzaWJpbGl0eWNoYW5nZSc7XHJcbiAgICAgICAgICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoZXZ0bmFtZSwgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgaWYgKG1ldGhvZHMucGF1c2VJbnZpc2libGUuaXNIaWRkZW4oKSkge1xyXG4gICAgICAgICAgICAgICAgaWYoc2xpZGVyLnN0YXJ0VGltZW91dCkge1xyXG4gICAgICAgICAgICAgICAgICBjbGVhclRpbWVvdXQoc2xpZGVyLnN0YXJ0VGltZW91dCk7IC8vSWYgY2xvY2sgaXMgdGlja2luZywgc3RvcCB0aW1lciBhbmQgcHJldmVudCBmcm9tIHN0YXJ0aW5nIHdoaWxlIGludmlzaWJsZVxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgc2xpZGVyLnBhdXNlKCk7IC8vT3IganVzdCBwYXVzZVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGlmKHNsaWRlci5zdGFydGVkKSB7XHJcbiAgICAgICAgICAgICAgICAgIHNsaWRlci5wbGF5KCk7IC8vSW5pdGlhdGVkIGJlZm9yZSwganVzdCBwbGF5XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICBpZiAoc2xpZGVyLnZhcnMuaW5pdERlbGF5ID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoc2xpZGVyLnBsYXksIHNsaWRlci52YXJzLmluaXREZWxheSk7XHJcbiAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2xpZGVyLnBsYXkoKTsgLy9EaWRuJ3QgaW5pdCBiZWZvcmU6IHNpbXBseSBpbml0IG9yIHdhaXQgZm9yIGl0XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgaXNIaWRkZW46IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgdmFyIHByb3AgPSBtZXRob2RzLnBhdXNlSW52aXNpYmxlLmdldEhpZGRlblByb3AoKTtcclxuICAgICAgICAgIGlmICghcHJvcCkge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICByZXR1cm4gZG9jdW1lbnRbcHJvcF07XHJcbiAgICAgICAgfSxcclxuICAgICAgICBnZXRIaWRkZW5Qcm9wOiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgIHZhciBwcmVmaXhlcyA9IFsnd2Via2l0JywnbW96JywnbXMnLCdvJ107XHJcbiAgICAgICAgICAvLyBpZiAnaGlkZGVuJyBpcyBuYXRpdmVseSBzdXBwb3J0ZWQganVzdCByZXR1cm4gaXRcclxuICAgICAgICAgIGlmICgnaGlkZGVuJyBpbiBkb2N1bWVudCkge1xyXG4gICAgICAgICAgICByZXR1cm4gJ2hpZGRlbic7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICAvLyBvdGhlcndpc2UgbG9vcCBvdmVyIGFsbCB0aGUga25vd24gcHJlZml4ZXMgdW50aWwgd2UgZmluZCBvbmVcclxuICAgICAgICAgIGZvciAoIHZhciBpID0gMDsgaSA8IHByZWZpeGVzLmxlbmd0aDsgaSsrICkge1xyXG4gICAgICAgICAgICAgIGlmICgocHJlZml4ZXNbaV0gKyAnSGlkZGVuJykgaW4gZG9jdW1lbnQpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBwcmVmaXhlc1tpXSArICdIaWRkZW4nO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICAgIC8vIG90aGVyd2lzZSBpdCdzIG5vdCBzdXBwb3J0ZWRcclxuICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgfSxcclxuICAgICAgc2V0VG9DbGVhcldhdGNoZWRFdmVudDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgY2xlYXJUaW1lb3V0KHdhdGNoZWRFdmVudENsZWFyVGltZXIpO1xyXG4gICAgICAgIHdhdGNoZWRFdmVudENsZWFyVGltZXIgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgd2F0Y2hlZEV2ZW50ID0gXCJcIjtcclxuICAgICAgICB9LCAzMDAwKTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICAvLyBwdWJsaWMgbWV0aG9kc1xyXG4gICAgc2xpZGVyLmZsZXhBbmltYXRlID0gZnVuY3Rpb24odGFyZ2V0LCBwYXVzZSwgb3ZlcnJpZGUsIHdpdGhTeW5jLCBmcm9tTmF2KSB7XHJcbiAgICAgIGlmICghc2xpZGVyLnZhcnMuYW5pbWF0aW9uTG9vcCAmJiB0YXJnZXQgIT09IHNsaWRlci5jdXJyZW50U2xpZGUpIHtcclxuICAgICAgICBzbGlkZXIuZGlyZWN0aW9uID0gKHRhcmdldCA+IHNsaWRlci5jdXJyZW50U2xpZGUpID8gXCJuZXh0XCIgOiBcInByZXZcIjtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKGFzTmF2ICYmIHNsaWRlci5wYWdpbmdDb3VudCA9PT0gMSkgc2xpZGVyLmRpcmVjdGlvbiA9IChzbGlkZXIuY3VycmVudEl0ZW0gPCB0YXJnZXQpID8gXCJuZXh0XCIgOiBcInByZXZcIjtcclxuXHJcbiAgICAgIGlmICghc2xpZGVyLmFuaW1hdGluZyAmJiAoc2xpZGVyLmNhbkFkdmFuY2UodGFyZ2V0LCBmcm9tTmF2KSB8fCBvdmVycmlkZSkgJiYgc2xpZGVyLmlzKFwiOnZpc2libGVcIikpIHtcclxuICAgICAgICBpZiAoYXNOYXYgJiYgd2l0aFN5bmMpIHtcclxuICAgICAgICAgIHZhciBtYXN0ZXIgPSAkKHNsaWRlci52YXJzLmFzTmF2Rm9yKS5kYXRhKCdmbGV4c2xpZGVyJyk7XHJcbiAgICAgICAgICBzbGlkZXIuYXRFbmQgPSB0YXJnZXQgPT09IDAgfHwgdGFyZ2V0ID09PSBzbGlkZXIuY291bnQgLSAxO1xyXG4gICAgICAgICAgbWFzdGVyLmZsZXhBbmltYXRlKHRhcmdldCwgdHJ1ZSwgZmFsc2UsIHRydWUsIGZyb21OYXYpO1xyXG4gICAgICAgICAgc2xpZGVyLmRpcmVjdGlvbiA9IChzbGlkZXIuY3VycmVudEl0ZW0gPCB0YXJnZXQpID8gXCJuZXh0XCIgOiBcInByZXZcIjtcclxuICAgICAgICAgIG1hc3Rlci5kaXJlY3Rpb24gPSBzbGlkZXIuZGlyZWN0aW9uO1xyXG5cclxuICAgICAgICAgIGlmIChNYXRoLmNlaWwoKHRhcmdldCArIDEpL3NsaWRlci52aXNpYmxlKSAtIDEgIT09IHNsaWRlci5jdXJyZW50U2xpZGUgJiYgdGFyZ2V0ICE9PSAwKSB7XHJcbiAgICAgICAgICAgIHNsaWRlci5jdXJyZW50SXRlbSA9IHRhcmdldDtcclxuICAgICAgICAgICAgc2xpZGVyLnNsaWRlcy5yZW1vdmVDbGFzcyhuYW1lc3BhY2UgKyBcImFjdGl2ZS1zbGlkZVwiKS5lcSh0YXJnZXQpLmFkZENsYXNzKG5hbWVzcGFjZSArIFwiYWN0aXZlLXNsaWRlXCIpO1xyXG4gICAgICAgICAgICB0YXJnZXQgPSBNYXRoLmZsb29yKHRhcmdldC9zbGlkZXIudmlzaWJsZSk7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBzbGlkZXIuY3VycmVudEl0ZW0gPSB0YXJnZXQ7XHJcbiAgICAgICAgICAgIHNsaWRlci5zbGlkZXMucmVtb3ZlQ2xhc3MobmFtZXNwYWNlICsgXCJhY3RpdmUtc2xpZGVcIikuZXEodGFyZ2V0KS5hZGRDbGFzcyhuYW1lc3BhY2UgKyBcImFjdGl2ZS1zbGlkZVwiKTtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgc2xpZGVyLmFuaW1hdGluZyA9IHRydWU7XHJcbiAgICAgICAgc2xpZGVyLmFuaW1hdGluZ1RvID0gdGFyZ2V0O1xyXG5cclxuICAgICAgICAvLyBTTElERVNIT1c6XHJcbiAgICAgICAgaWYgKHBhdXNlKSB7IHNsaWRlci5wYXVzZSgpOyB9XHJcblxyXG4gICAgICAgIC8vIEFQSTogYmVmb3JlKCkgYW5pbWF0aW9uIENhbGxiYWNrXHJcbiAgICAgICAgc2xpZGVyLnZhcnMuYmVmb3JlKHNsaWRlcik7XHJcblxyXG4gICAgICAgIC8vIFNZTkM6XHJcbiAgICAgICAgaWYgKHNsaWRlci5zeW5jRXhpc3RzICYmICFmcm9tTmF2KSB7IG1ldGhvZHMuc3luYyhcImFuaW1hdGVcIik7IH1cclxuXHJcbiAgICAgICAgLy8gQ09OVFJPTE5BVlxyXG4gICAgICAgIGlmIChzbGlkZXIudmFycy5jb250cm9sTmF2KSB7IG1ldGhvZHMuY29udHJvbE5hdi5hY3RpdmUoKTsgfVxyXG5cclxuICAgICAgICAvLyAhQ0FST1VTRUw6XHJcbiAgICAgICAgLy8gQ0FORElEQVRFOiBzbGlkZSBhY3RpdmUgY2xhc3MgKGZvciBhZGQvcmVtb3ZlIHNsaWRlKVxyXG4gICAgICAgIGlmICghY2Fyb3VzZWwpIHsgc2xpZGVyLnNsaWRlcy5yZW1vdmVDbGFzcyhuYW1lc3BhY2UgKyAnYWN0aXZlLXNsaWRlJykuZXEodGFyZ2V0KS5hZGRDbGFzcyhuYW1lc3BhY2UgKyAnYWN0aXZlLXNsaWRlJyk7IH1cclxuXHJcbiAgICAgICAgLy8gSU5GSU5JVEUgTE9PUDpcclxuICAgICAgICAvLyBDQU5ESURBVEU6IGF0RW5kXHJcbiAgICAgICAgc2xpZGVyLmF0RW5kID0gdGFyZ2V0ID09PSAwIHx8IHRhcmdldCA9PT0gc2xpZGVyLmxhc3Q7XHJcblxyXG4gICAgICAgIC8vIERJUkVDVElPTk5BVjpcclxuICAgICAgICBpZiAoc2xpZGVyLnZhcnMuZGlyZWN0aW9uTmF2KSB7IG1ldGhvZHMuZGlyZWN0aW9uTmF2LnVwZGF0ZSgpOyB9XHJcblxyXG4gICAgICAgIGlmICh0YXJnZXQgPT09IHNsaWRlci5sYXN0KSB7XHJcbiAgICAgICAgICAvLyBBUEk6IGVuZCgpIG9mIGN5Y2xlIENhbGxiYWNrXHJcbiAgICAgICAgICBzbGlkZXIudmFycy5lbmQoc2xpZGVyKTtcclxuICAgICAgICAgIC8vIFNMSURFU0hPVyAmJiAhSU5GSU5JVEUgTE9PUDpcclxuICAgICAgICAgIGlmICghc2xpZGVyLnZhcnMuYW5pbWF0aW9uTG9vcCkgeyBzbGlkZXIucGF1c2UoKTsgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gU0xJREU6XHJcbiAgICAgICAgaWYgKCFmYWRlKSB7XHJcbiAgICAgICAgICB2YXIgZGltZW5zaW9uID0gKHZlcnRpY2FsKSA/IHNsaWRlci5zbGlkZXMuZmlsdGVyKCc6Zmlyc3QnKS5oZWlnaHQoKSA6IHNsaWRlci5jb21wdXRlZFcsXHJcbiAgICAgICAgICAgICAgbWFyZ2luLCBzbGlkZVN0cmluZywgY2FsY05leHQ7XHJcblxyXG4gICAgICAgICAgLy8gSU5GSU5JVEUgTE9PUCAvIFJFVkVSU0U6XHJcbiAgICAgICAgICBpZiAoY2Fyb3VzZWwpIHtcclxuICAgICAgICAgICAgbWFyZ2luID0gc2xpZGVyLnZhcnMuaXRlbU1hcmdpbjtcclxuICAgICAgICAgICAgY2FsY05leHQgPSAoKHNsaWRlci5pdGVtVyArIG1hcmdpbikgKiBzbGlkZXIubW92ZSkgKiBzbGlkZXIuYW5pbWF0aW5nVG87XHJcbiAgICAgICAgICAgIHNsaWRlU3RyaW5nID0gKGNhbGNOZXh0ID4gc2xpZGVyLmxpbWl0ICYmIHNsaWRlci52aXNpYmxlICE9PSAxKSA/IHNsaWRlci5saW1pdCA6IGNhbGNOZXh0O1xyXG4gICAgICAgICAgfSBlbHNlIGlmIChzbGlkZXIuY3VycmVudFNsaWRlID09PSAwICYmIHRhcmdldCA9PT0gc2xpZGVyLmNvdW50IC0gMSAmJiBzbGlkZXIudmFycy5hbmltYXRpb25Mb29wICYmIHNsaWRlci5kaXJlY3Rpb24gIT09IFwibmV4dFwiKSB7XHJcbiAgICAgICAgICAgIHNsaWRlU3RyaW5nID0gKHJldmVyc2UpID8gKHNsaWRlci5jb3VudCArIHNsaWRlci5jbG9uZU9mZnNldCkgKiBkaW1lbnNpb24gOiAwO1xyXG4gICAgICAgICAgfSBlbHNlIGlmIChzbGlkZXIuY3VycmVudFNsaWRlID09PSBzbGlkZXIubGFzdCAmJiB0YXJnZXQgPT09IDAgJiYgc2xpZGVyLnZhcnMuYW5pbWF0aW9uTG9vcCAmJiBzbGlkZXIuZGlyZWN0aW9uICE9PSBcInByZXZcIikge1xyXG4gICAgICAgICAgICBzbGlkZVN0cmluZyA9IChyZXZlcnNlKSA/IDAgOiAoc2xpZGVyLmNvdW50ICsgMSkgKiBkaW1lbnNpb247XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBzbGlkZVN0cmluZyA9IChyZXZlcnNlKSA/ICgoc2xpZGVyLmNvdW50IC0gMSkgLSB0YXJnZXQgKyBzbGlkZXIuY2xvbmVPZmZzZXQpICogZGltZW5zaW9uIDogKHRhcmdldCArIHNsaWRlci5jbG9uZU9mZnNldCkgKiBkaW1lbnNpb247XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBzbGlkZXIuc2V0UHJvcHMoc2xpZGVTdHJpbmcsIFwiXCIsIHNsaWRlci52YXJzLmFuaW1hdGlvblNwZWVkKTtcclxuICAgICAgICAgIGlmIChzbGlkZXIudHJhbnNpdGlvbnMpIHtcclxuICAgICAgICAgICAgaWYgKCFzbGlkZXIudmFycy5hbmltYXRpb25Mb29wIHx8ICFzbGlkZXIuYXRFbmQpIHtcclxuICAgICAgICAgICAgICBzbGlkZXIuYW5pbWF0aW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgc2xpZGVyLmN1cnJlbnRTbGlkZSA9IHNsaWRlci5hbmltYXRpbmdUbztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gVW5iaW5kIHByZXZpb3VzIHRyYW5zaXRpb25FbmQgZXZlbnRzIGFuZCByZS1iaW5kIG5ldyB0cmFuc2l0aW9uRW5kIGV2ZW50XHJcbiAgICAgICAgICAgIHNsaWRlci5jb250YWluZXIudW5iaW5kKFwid2Via2l0VHJhbnNpdGlvbkVuZCB0cmFuc2l0aW9uZW5kXCIpO1xyXG4gICAgICAgICAgICBzbGlkZXIuY29udGFpbmVyLmJpbmQoXCJ3ZWJraXRUcmFuc2l0aW9uRW5kIHRyYW5zaXRpb25lbmRcIiwgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgY2xlYXJUaW1lb3V0KHNsaWRlci5lbnN1cmVBbmltYXRpb25FbmQpO1xyXG4gICAgICAgICAgICAgIHNsaWRlci53cmFwdXAoZGltZW5zaW9uKTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBJbnN1cmFuY2UgZm9yIHRoZSBldmVyLXNvLWZpY2tsZSB0cmFuc2l0aW9uRW5kIGV2ZW50XHJcbiAgICAgICAgICAgIGNsZWFyVGltZW91dChzbGlkZXIuZW5zdXJlQW5pbWF0aW9uRW5kKTtcclxuICAgICAgICAgICAgc2xpZGVyLmVuc3VyZUFuaW1hdGlvbkVuZCA9IHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgc2xpZGVyLndyYXB1cChkaW1lbnNpb24pO1xyXG4gICAgICAgICAgICB9LCBzbGlkZXIudmFycy5hbmltYXRpb25TcGVlZCArIDEwMCk7XHJcblxyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgc2xpZGVyLmNvbnRhaW5lci5hbmltYXRlKHNsaWRlci5hcmdzLCBzbGlkZXIudmFycy5hbmltYXRpb25TcGVlZCwgc2xpZGVyLnZhcnMuZWFzaW5nLCBmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAgIHNsaWRlci53cmFwdXAoZGltZW5zaW9uKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHsgLy8gRkFERTpcclxuICAgICAgICAgIGlmICghdG91Y2gpIHtcclxuICAgICAgICAgICAgLy9zbGlkZXIuc2xpZGVzLmVxKHNsaWRlci5jdXJyZW50U2xpZGUpLmZhZGVPdXQoc2xpZGVyLnZhcnMuYW5pbWF0aW9uU3BlZWQsIHNsaWRlci52YXJzLmVhc2luZyk7XHJcbiAgICAgICAgICAgIC8vc2xpZGVyLnNsaWRlcy5lcSh0YXJnZXQpLmZhZGVJbihzbGlkZXIudmFycy5hbmltYXRpb25TcGVlZCwgc2xpZGVyLnZhcnMuZWFzaW5nLCBzbGlkZXIud3JhcHVwKTtcclxuXHJcbiAgICAgICAgICAgIHNsaWRlci5zbGlkZXMuZXEoc2xpZGVyLmN1cnJlbnRTbGlkZSkuY3NzKHtcInpJbmRleFwiOiAxLCBcImRpc3BsYXlcIjogXCJub25lXCJ9KS5hbmltYXRlKHtcIm9wYWNpdHlcIjogMH0sIHNsaWRlci52YXJzLmFuaW1hdGlvblNwZWVkLCBzbGlkZXIudmFycy5lYXNpbmcpO1xyXG4gICAgICAgICAgICBzbGlkZXIuc2xpZGVzLmVxKHRhcmdldCkuY3NzKHtcInpJbmRleFwiOiAyLCBcImRpc3BsYXlcIjogXCJibG9ja1wifSkuYW5pbWF0ZSh7XCJvcGFjaXR5XCI6IDF9LCBzbGlkZXIudmFycy5hbmltYXRpb25TcGVlZCwgc2xpZGVyLnZhcnMuZWFzaW5nLCBzbGlkZXIud3JhcHVwKTtcclxuXHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBzbGlkZXIuc2xpZGVzLmVxKHNsaWRlci5jdXJyZW50U2xpZGUpLmNzcyh7IFwib3BhY2l0eVwiOiAwLCBcInpJbmRleFwiOiAxLCBcImRpc3BsYXlcIjogXCJub25lXCIgfSk7XHJcbiAgICAgICAgICAgIHNsaWRlci5zbGlkZXMuZXEodGFyZ2V0KS5jc3MoeyBcIm9wYWNpdHlcIjogMSwgXCJ6SW5kZXhcIjogMiwgXCJkaXNwbGF5XCI6IFwiYmxvY2tcIiB9KTtcclxuICAgICAgICAgICAgc2xpZGVyLndyYXB1cChkaW1lbnNpb24pO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAvLyBTTU9PVEggSEVJR0hUOlxyXG4gICAgICAgIGlmIChzbGlkZXIudmFycy5zbW9vdGhIZWlnaHQpIHsgbWV0aG9kcy5zbW9vdGhIZWlnaHQoc2xpZGVyLnZhcnMuYW5pbWF0aW9uU3BlZWQpOyB9XHJcbiAgICAgIH1cclxuICAgIH07XHJcbiAgICBzbGlkZXIud3JhcHVwID0gZnVuY3Rpb24oZGltZW5zaW9uKSB7XHJcbiAgICAgIC8vIFNMSURFOlxyXG4gICAgICBpZiAoIWZhZGUgJiYgIWNhcm91c2VsKSB7XHJcbiAgICAgICAgaWYgKHNsaWRlci5jdXJyZW50U2xpZGUgPT09IDAgJiYgc2xpZGVyLmFuaW1hdGluZ1RvID09PSBzbGlkZXIubGFzdCAmJiBzbGlkZXIudmFycy5hbmltYXRpb25Mb29wKSB7XHJcbiAgICAgICAgICBzbGlkZXIuc2V0UHJvcHMoZGltZW5zaW9uLCBcImp1bXBFbmRcIik7XHJcbiAgICAgICAgfSBlbHNlIGlmIChzbGlkZXIuY3VycmVudFNsaWRlID09PSBzbGlkZXIubGFzdCAmJiBzbGlkZXIuYW5pbWF0aW5nVG8gPT09IDAgJiYgc2xpZGVyLnZhcnMuYW5pbWF0aW9uTG9vcCkge1xyXG4gICAgICAgICAgc2xpZGVyLnNldFByb3BzKGRpbWVuc2lvbiwgXCJqdW1wU3RhcnRcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIHNsaWRlci5hbmltYXRpbmcgPSBmYWxzZTtcclxuICAgICAgc2xpZGVyLmN1cnJlbnRTbGlkZSA9IHNsaWRlci5hbmltYXRpbmdUbztcclxuICAgICAgLy8gQVBJOiBhZnRlcigpIGFuaW1hdGlvbiBDYWxsYmFja1xyXG4gICAgICBzbGlkZXIudmFycy5hZnRlcihzbGlkZXIpO1xyXG4gICAgfTtcclxuXHJcbiAgICAvLyBTTElERVNIT1c6XHJcbiAgICBzbGlkZXIuYW5pbWF0ZVNsaWRlcyA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICBpZiAoIXNsaWRlci5hbmltYXRpbmcgJiYgZm9jdXNlZCApIHsgc2xpZGVyLmZsZXhBbmltYXRlKHNsaWRlci5nZXRUYXJnZXQoXCJuZXh0XCIpKTsgfVxyXG4gICAgfTtcclxuICAgIC8vIFNMSURFU0hPVzpcclxuICAgIHNsaWRlci5wYXVzZSA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICBjbGVhckludGVydmFsKHNsaWRlci5hbmltYXRlZFNsaWRlcyk7XHJcbiAgICAgIHNsaWRlci5hbmltYXRlZFNsaWRlcyA9IG51bGw7XHJcbiAgICAgIHNsaWRlci5wbGF5aW5nID0gZmFsc2U7XHJcbiAgICAgIC8vIFBBVVNFUExBWTpcclxuICAgICAgaWYgKHNsaWRlci52YXJzLnBhdXNlUGxheSkgeyBtZXRob2RzLnBhdXNlUGxheS51cGRhdGUoXCJwbGF5XCIpOyB9XHJcbiAgICAgIC8vIFNZTkM6XHJcbiAgICAgIGlmIChzbGlkZXIuc3luY0V4aXN0cykgeyBtZXRob2RzLnN5bmMoXCJwYXVzZVwiKTsgfVxyXG4gICAgfTtcclxuICAgIC8vIFNMSURFU0hPVzpcclxuICAgIHNsaWRlci5wbGF5ID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgIGlmIChzbGlkZXIucGxheWluZykgeyBjbGVhckludGVydmFsKHNsaWRlci5hbmltYXRlZFNsaWRlcyk7IH1cclxuICAgICAgc2xpZGVyLmFuaW1hdGVkU2xpZGVzID0gc2xpZGVyLmFuaW1hdGVkU2xpZGVzIHx8IHNldEludGVydmFsKHNsaWRlci5hbmltYXRlU2xpZGVzLCBzbGlkZXIudmFycy5zbGlkZXNob3dTcGVlZCk7XHJcbiAgICAgIHNsaWRlci5zdGFydGVkID0gc2xpZGVyLnBsYXlpbmcgPSB0cnVlO1xyXG4gICAgICAvLyBQQVVTRVBMQVk6XHJcbiAgICAgIGlmIChzbGlkZXIudmFycy5wYXVzZVBsYXkpIHsgbWV0aG9kcy5wYXVzZVBsYXkudXBkYXRlKFwicGF1c2VcIik7IH1cclxuICAgICAgLy8gU1lOQzpcclxuICAgICAgaWYgKHNsaWRlci5zeW5jRXhpc3RzKSB7IG1ldGhvZHMuc3luYyhcInBsYXlcIik7IH1cclxuICAgIH07XHJcbiAgICAvLyBTVE9QOlxyXG4gICAgc2xpZGVyLnN0b3AgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIHNsaWRlci5wYXVzZSgpO1xyXG4gICAgICBzbGlkZXIuc3RvcHBlZCA9IHRydWU7XHJcbiAgICB9O1xyXG4gICAgc2xpZGVyLmNhbkFkdmFuY2UgPSBmdW5jdGlvbih0YXJnZXQsIGZyb21OYXYpIHtcclxuICAgICAgLy8gQVNOQVY6XHJcbiAgICAgIHZhciBsYXN0ID0gKGFzTmF2KSA/IHNsaWRlci5wYWdpbmdDb3VudCAtIDEgOiBzbGlkZXIubGFzdDtcclxuICAgICAgcmV0dXJuIChmcm9tTmF2KSA/IHRydWUgOlxyXG4gICAgICAgICAgICAgKGFzTmF2ICYmIHNsaWRlci5jdXJyZW50SXRlbSA9PT0gc2xpZGVyLmNvdW50IC0gMSAmJiB0YXJnZXQgPT09IDAgJiYgc2xpZGVyLmRpcmVjdGlvbiA9PT0gXCJwcmV2XCIpID8gdHJ1ZSA6XHJcbiAgICAgICAgICAgICAoYXNOYXYgJiYgc2xpZGVyLmN1cnJlbnRJdGVtID09PSAwICYmIHRhcmdldCA9PT0gc2xpZGVyLnBhZ2luZ0NvdW50IC0gMSAmJiBzbGlkZXIuZGlyZWN0aW9uICE9PSBcIm5leHRcIikgPyBmYWxzZSA6XHJcbiAgICAgICAgICAgICAodGFyZ2V0ID09PSBzbGlkZXIuY3VycmVudFNsaWRlICYmICFhc05hdikgPyBmYWxzZSA6XHJcbiAgICAgICAgICAgICAoc2xpZGVyLnZhcnMuYW5pbWF0aW9uTG9vcCkgPyB0cnVlIDpcclxuICAgICAgICAgICAgIChzbGlkZXIuYXRFbmQgJiYgc2xpZGVyLmN1cnJlbnRTbGlkZSA9PT0gMCAmJiB0YXJnZXQgPT09IGxhc3QgJiYgc2xpZGVyLmRpcmVjdGlvbiAhPT0gXCJuZXh0XCIpID8gZmFsc2UgOlxyXG4gICAgICAgICAgICAgKHNsaWRlci5hdEVuZCAmJiBzbGlkZXIuY3VycmVudFNsaWRlID09PSBsYXN0ICYmIHRhcmdldCA9PT0gMCAmJiBzbGlkZXIuZGlyZWN0aW9uID09PSBcIm5leHRcIikgPyBmYWxzZSA6XHJcbiAgICAgICAgICAgICB0cnVlO1xyXG4gICAgfTtcclxuICAgIHNsaWRlci5nZXRUYXJnZXQgPSBmdW5jdGlvbihkaXIpIHtcclxuICAgICAgc2xpZGVyLmRpcmVjdGlvbiA9IGRpcjtcclxuICAgICAgaWYgKGRpciA9PT0gXCJuZXh0XCIpIHtcclxuICAgICAgICByZXR1cm4gKHNsaWRlci5jdXJyZW50U2xpZGUgPT09IHNsaWRlci5sYXN0KSA/IDAgOiBzbGlkZXIuY3VycmVudFNsaWRlICsgMTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICByZXR1cm4gKHNsaWRlci5jdXJyZW50U2xpZGUgPT09IDApID8gc2xpZGVyLmxhc3QgOiBzbGlkZXIuY3VycmVudFNsaWRlIC0gMTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICAvLyBTTElERTpcclxuICAgIHNsaWRlci5zZXRQcm9wcyA9IGZ1bmN0aW9uKHBvcywgc3BlY2lhbCwgZHVyKSB7XHJcbiAgICAgIHZhciB0YXJnZXQgPSAoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdmFyIHBvc0NoZWNrID0gKHBvcykgPyBwb3MgOiAoKHNsaWRlci5pdGVtVyArIHNsaWRlci52YXJzLml0ZW1NYXJnaW4pICogc2xpZGVyLm1vdmUpICogc2xpZGVyLmFuaW1hdGluZ1RvLFxyXG4gICAgICAgICAgICBwb3NDYWxjID0gKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgIGlmIChjYXJvdXNlbCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIChzcGVjaWFsID09PSBcInNldFRvdWNoXCIpID8gcG9zIDpcclxuICAgICAgICAgICAgICAgICAgICAgICAocmV2ZXJzZSAmJiBzbGlkZXIuYW5pbWF0aW5nVG8gPT09IHNsaWRlci5sYXN0KSA/IDAgOlxyXG4gICAgICAgICAgICAgICAgICAgICAgIChyZXZlcnNlKSA/IHNsaWRlci5saW1pdCAtICgoKHNsaWRlci5pdGVtVyArIHNsaWRlci52YXJzLml0ZW1NYXJnaW4pICogc2xpZGVyLm1vdmUpICogc2xpZGVyLmFuaW1hdGluZ1RvKSA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgKHNsaWRlci5hbmltYXRpbmdUbyA9PT0gc2xpZGVyLmxhc3QpID8gc2xpZGVyLmxpbWl0IDogcG9zQ2hlY2s7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHN3aXRjaCAoc3BlY2lhbCkge1xyXG4gICAgICAgICAgICAgICAgICBjYXNlIFwic2V0VG90YWxcIjogcmV0dXJuIChyZXZlcnNlKSA/ICgoc2xpZGVyLmNvdW50IC0gMSkgLSBzbGlkZXIuY3VycmVudFNsaWRlICsgc2xpZGVyLmNsb25lT2Zmc2V0KSAqIHBvcyA6IChzbGlkZXIuY3VycmVudFNsaWRlICsgc2xpZGVyLmNsb25lT2Zmc2V0KSAqIHBvcztcclxuICAgICAgICAgICAgICAgICAgY2FzZSBcInNldFRvdWNoXCI6IHJldHVybiAocmV2ZXJzZSkgPyBwb3MgOiBwb3M7XHJcbiAgICAgICAgICAgICAgICAgIGNhc2UgXCJqdW1wRW5kXCI6IHJldHVybiAocmV2ZXJzZSkgPyBwb3MgOiBzbGlkZXIuY291bnQgKiBwb3M7XHJcbiAgICAgICAgICAgICAgICAgIGNhc2UgXCJqdW1wU3RhcnRcIjogcmV0dXJuIChyZXZlcnNlKSA/IHNsaWRlci5jb3VudCAqIHBvcyA6IHBvcztcclxuICAgICAgICAgICAgICAgICAgZGVmYXVsdDogcmV0dXJuIHBvcztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0oKSk7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gKHBvc0NhbGMgKiAtMSkgKyBcInB4XCI7XHJcbiAgICAgICAgICB9KCkpO1xyXG5cclxuICAgICAgaWYgKHNsaWRlci50cmFuc2l0aW9ucykge1xyXG4gICAgICAgIHRhcmdldCA9ICh2ZXJ0aWNhbCkgPyBcInRyYW5zbGF0ZTNkKDAsXCIgKyB0YXJnZXQgKyBcIiwwKVwiIDogXCJ0cmFuc2xhdGUzZChcIiArIHRhcmdldCArIFwiLDAsMClcIjtcclxuICAgICAgICBkdXIgPSAoZHVyICE9PSB1bmRlZmluZWQpID8gKGR1ci8xMDAwKSArIFwic1wiIDogXCIwc1wiO1xyXG4gICAgICAgIHNsaWRlci5jb250YWluZXIuY3NzKFwiLVwiICsgc2xpZGVyLnBmeCArIFwiLXRyYW5zaXRpb24tZHVyYXRpb25cIiwgZHVyKTtcclxuICAgICAgICAgc2xpZGVyLmNvbnRhaW5lci5jc3MoXCJ0cmFuc2l0aW9uLWR1cmF0aW9uXCIsIGR1cik7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHNsaWRlci5hcmdzW3NsaWRlci5wcm9wXSA9IHRhcmdldDtcclxuICAgICAgaWYgKHNsaWRlci50cmFuc2l0aW9ucyB8fCBkdXIgPT09IHVuZGVmaW5lZCkgeyBzbGlkZXIuY29udGFpbmVyLmNzcyhzbGlkZXIuYXJncyk7IH1cclxuXHJcbiAgICAgIHNsaWRlci5jb250YWluZXIuY3NzKCd0cmFuc2Zvcm0nLHRhcmdldCk7XHJcbiAgICB9O1xyXG5cclxuICAgIHNsaWRlci5zZXR1cCA9IGZ1bmN0aW9uKHR5cGUpIHtcclxuICAgICAgLy8gU0xJREU6XHJcbiAgICAgIGlmICghZmFkZSkge1xyXG4gICAgICAgIHZhciBzbGlkZXJPZmZzZXQsIGFycjtcclxuXHJcbiAgICAgICAgaWYgKHR5cGUgPT09IFwiaW5pdFwiKSB7XHJcbiAgICAgICAgICBzbGlkZXIudmlld3BvcnQgPSAkKCc8ZGl2IGNsYXNzPVwiJyArIG5hbWVzcGFjZSArICd2aWV3cG9ydFwiPjwvZGl2PicpLmNzcyh7XCJvdmVyZmxvd1wiOiBcImhpZGRlblwiLCBcInBvc2l0aW9uXCI6IFwicmVsYXRpdmVcIn0pLmFwcGVuZFRvKHNsaWRlcikuYXBwZW5kKHNsaWRlci5jb250YWluZXIpO1xyXG4gICAgICAgICAgLy8gSU5GSU5JVEUgTE9PUDpcclxuICAgICAgICAgIHNsaWRlci5jbG9uZUNvdW50ID0gMDtcclxuICAgICAgICAgIHNsaWRlci5jbG9uZU9mZnNldCA9IDA7XHJcbiAgICAgICAgICAvLyBSRVZFUlNFOlxyXG4gICAgICAgICAgaWYgKHJldmVyc2UpIHtcclxuICAgICAgICAgICAgYXJyID0gJC5tYWtlQXJyYXkoc2xpZGVyLnNsaWRlcykucmV2ZXJzZSgpO1xyXG4gICAgICAgICAgICBzbGlkZXIuc2xpZGVzID0gJChhcnIpO1xyXG4gICAgICAgICAgICBzbGlkZXIuY29udGFpbmVyLmVtcHR5KCkuYXBwZW5kKHNsaWRlci5zbGlkZXMpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAvLyBJTkZJTklURSBMT09QICYmICFDQVJPVVNFTDpcclxuICAgICAgICBpZiAoc2xpZGVyLnZhcnMuYW5pbWF0aW9uTG9vcCAmJiAhY2Fyb3VzZWwpIHtcclxuICAgICAgICAgIHNsaWRlci5jbG9uZUNvdW50ID0gMjtcclxuICAgICAgICAgIHNsaWRlci5jbG9uZU9mZnNldCA9IDE7XHJcbiAgICAgICAgICAvLyBjbGVhciBvdXQgb2xkIGNsb25lc1xyXG4gICAgICAgICAgaWYgKHR5cGUgIT09IFwiaW5pdFwiKSB7IHNsaWRlci5jb250YWluZXIuZmluZCgnLmNsb25lJykucmVtb3ZlKCk7IH1cclxuICAgICAgICAgIHNsaWRlci5jb250YWluZXIuYXBwZW5kKG1ldGhvZHMudW5pcXVlSUQoc2xpZGVyLnNsaWRlcy5maXJzdCgpLmNsb25lKCkuYWRkQ2xhc3MoJ2Nsb25lJykpLmF0dHIoJ2FyaWEtaGlkZGVuJywgJ3RydWUnKSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAucHJlcGVuZChtZXRob2RzLnVuaXF1ZUlEKHNsaWRlci5zbGlkZXMubGFzdCgpLmNsb25lKCkuYWRkQ2xhc3MoJ2Nsb25lJykpLmF0dHIoJ2FyaWEtaGlkZGVuJywgJ3RydWUnKSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHNsaWRlci5uZXdTbGlkZXMgPSAkKHNsaWRlci52YXJzLnNlbGVjdG9yLCBzbGlkZXIpO1xyXG5cclxuICAgICAgICBzbGlkZXJPZmZzZXQgPSAocmV2ZXJzZSkgPyBzbGlkZXIuY291bnQgLSAxIC0gc2xpZGVyLmN1cnJlbnRTbGlkZSArIHNsaWRlci5jbG9uZU9mZnNldCA6IHNsaWRlci5jdXJyZW50U2xpZGUgKyBzbGlkZXIuY2xvbmVPZmZzZXQ7XHJcbiAgICAgICAgLy8gVkVSVElDQUw6XHJcbiAgICAgICAgaWYgKHZlcnRpY2FsICYmICFjYXJvdXNlbCkge1xyXG4gICAgICAgICAgc2xpZGVyLmNvbnRhaW5lci5oZWlnaHQoKHNsaWRlci5jb3VudCArIHNsaWRlci5jbG9uZUNvdW50KSAqIDIwMCArIFwiJVwiKS5jc3MoXCJwb3NpdGlvblwiLCBcImFic29sdXRlXCIpLndpZHRoKFwiMTAwJVwiKTtcclxuICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgc2xpZGVyLm5ld1NsaWRlcy5jc3Moe1wiZGlzcGxheVwiOiBcImJsb2NrXCJ9KTtcclxuICAgICAgICAgICAgc2xpZGVyLmRvTWF0aCgpO1xyXG4gICAgICAgICAgICBzbGlkZXIudmlld3BvcnQuaGVpZ2h0KHNsaWRlci5oKTtcclxuICAgICAgICAgICAgc2xpZGVyLnNldFByb3BzKHNsaWRlck9mZnNldCAqIHNsaWRlci5oLCBcImluaXRcIik7XHJcbiAgICAgICAgICB9LCAodHlwZSA9PT0gXCJpbml0XCIpID8gMTAwIDogMCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHNsaWRlci5jb250YWluZXIud2lkdGgoKHNsaWRlci5jb3VudCArIHNsaWRlci5jbG9uZUNvdW50KSAqIDIwMCArIFwiJVwiKTtcclxuICAgICAgICAgIHNsaWRlci5zZXRQcm9wcyhzbGlkZXJPZmZzZXQgKiBzbGlkZXIuY29tcHV0ZWRXLCBcImluaXRcIik7XHJcbiAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgIHNsaWRlci5kb01hdGgoKTtcclxuICAgICAgICAgICAgc2xpZGVyLm5ld1NsaWRlcy5jc3Moe1wid2lkdGhcIjogc2xpZGVyLmNvbXB1dGVkVywgXCJtYXJnaW5SaWdodFwiIDogc2xpZGVyLmNvbXB1dGVkTSwgXCJmbG9hdFwiOiBcImxlZnRcIiwgXCJkaXNwbGF5XCI6IFwiYmxvY2tcIn0pO1xyXG4gICAgICAgICAgICAvLyBTTU9PVEggSEVJR0hUOlxyXG4gICAgICAgICAgICBpZiAoc2xpZGVyLnZhcnMuc21vb3RoSGVpZ2h0KSB7IG1ldGhvZHMuc21vb3RoSGVpZ2h0KCk7IH1cclxuICAgICAgICAgIH0sICh0eXBlID09PSBcImluaXRcIikgPyAxMDAgOiAwKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7IC8vIEZBREU6XHJcbiAgICAgICAgc2xpZGVyLnNsaWRlcy5jc3Moe1wid2lkdGhcIjogXCIxMDAlXCIsIFwiZmxvYXRcIjogXCJsZWZ0XCIsIFwibWFyZ2luUmlnaHRcIjogXCItMTAwJVwiLCBcInBvc2l0aW9uXCI6IFwicmVsYXRpdmVcIn0pO1xyXG4gICAgICAgIGlmICh0eXBlID09PSBcImluaXRcIikge1xyXG4gICAgICAgICAgaWYgKCF0b3VjaCkge1xyXG4gICAgICAgICAgICAvL3NsaWRlci5zbGlkZXMuZXEoc2xpZGVyLmN1cnJlbnRTbGlkZSkuZmFkZUluKHNsaWRlci52YXJzLmFuaW1hdGlvblNwZWVkLCBzbGlkZXIudmFycy5lYXNpbmcpO1xyXG4gICAgICAgICAgICBpZiAoc2xpZGVyLnZhcnMuZmFkZUZpcnN0U2xpZGUgPT0gZmFsc2UpIHtcclxuICAgICAgICAgICAgICBzbGlkZXIuc2xpZGVzLmNzcyh7IFwib3BhY2l0eVwiOiAwLCBcImRpc3BsYXlcIjogXCJub25lXCIsIFwiekluZGV4XCI6IDEgfSkuZXEoc2xpZGVyLmN1cnJlbnRTbGlkZSkuY3NzKHtcInpJbmRleFwiOiAyLCBcImRpc3BsYXlcIjogXCJibG9ja1wifSkuY3NzKHtcIm9wYWNpdHlcIjogMX0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIHNsaWRlci5zbGlkZXMuY3NzKHsgXCJvcGFjaXR5XCI6IDAsIFwiZGlzcGxheVwiOiBcIm5vbmVcIiwgXCJ6SW5kZXhcIjogMSB9KS5lcShzbGlkZXIuY3VycmVudFNsaWRlKS5jc3Moe1wiekluZGV4XCI6IDIsIFwiZGlzcGxheVwiOiBcImJsb2NrXCJ9KS5hbmltYXRlKHtcIm9wYWNpdHlcIjogMX0sc2xpZGVyLnZhcnMuYW5pbWF0aW9uU3BlZWQsc2xpZGVyLnZhcnMuZWFzaW5nKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgc2xpZGVyLnNsaWRlcy5jc3MoeyBcIm9wYWNpdHlcIjogMCwgXCJkaXNwbGF5XCI6IFwibm9uZVwiLCBcIndlYmtpdFRyYW5zaXRpb25cIjogXCJvcGFjaXR5IFwiICsgc2xpZGVyLnZhcnMuYW5pbWF0aW9uU3BlZWQgLyAxMDAwICsgXCJzIGVhc2VcIiwgXCJ6SW5kZXhcIjogMSB9KS5lcShzbGlkZXIuY3VycmVudFNsaWRlKS5jc3MoeyBcIm9wYWNpdHlcIjogMSwgXCJ6SW5kZXhcIjogMiwgXCJkaXNwbGF5XCI6IFwiYmxvY2tcIn0pO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAvLyBTTU9PVEggSEVJR0hUOlxyXG4gICAgICAgIGlmIChzbGlkZXIudmFycy5zbW9vdGhIZWlnaHQpIHsgbWV0aG9kcy5zbW9vdGhIZWlnaHQoKTsgfVxyXG4gICAgICB9XHJcbiAgICAgIC8vICFDQVJPVVNFTDpcclxuICAgICAgLy8gQ0FORElEQVRFOiBhY3RpdmUgc2xpZGVcclxuICAgICAgaWYgKCFjYXJvdXNlbCkgeyBzbGlkZXIuc2xpZGVzLnJlbW92ZUNsYXNzKG5hbWVzcGFjZSArIFwiYWN0aXZlLXNsaWRlXCIpLmVxKHNsaWRlci5jdXJyZW50U2xpZGUpLmFkZENsYXNzKG5hbWVzcGFjZSArIFwiYWN0aXZlLXNsaWRlXCIpOyB9XHJcblxyXG4gICAgICAvL0ZsZXhTbGlkZXI6IGluaXQoKSBDYWxsYmFja1xyXG4gICAgICBzbGlkZXIudmFycy5pbml0KHNsaWRlcik7XHJcbiAgICB9O1xyXG5cclxuICAgIHNsaWRlci5kb01hdGggPSBmdW5jdGlvbigpIHtcclxuICAgICAgdmFyIHNsaWRlID0gc2xpZGVyLnNsaWRlcy5maXJzdCgpLFxyXG4gICAgICAgICAgc2xpZGVNYXJnaW4gPSBzbGlkZXIudmFycy5pdGVtTWFyZ2luLFxyXG4gICAgICAgICAgbWluSXRlbXMgPSBzbGlkZXIudmFycy5taW5JdGVtcyxcclxuICAgICAgICAgIG1heEl0ZW1zID0gc2xpZGVyLnZhcnMubWF4SXRlbXM7XHJcblxyXG4gICAgICBzbGlkZXIudyA9IChzbGlkZXIudmlld3BvcnQ9PT11bmRlZmluZWQpID8gc2xpZGVyLndpZHRoKCkgOiBzbGlkZXIudmlld3BvcnQud2lkdGgoKTtcclxuICAgICAgc2xpZGVyLmggPSBzbGlkZS5oZWlnaHQoKTtcclxuICAgICAgc2xpZGVyLmJveFBhZGRpbmcgPSBzbGlkZS5vdXRlcldpZHRoKCkgLSBzbGlkZS53aWR0aCgpO1xyXG5cclxuICAgICAgLy8gQ0FST1VTRUw6XHJcbiAgICAgIGlmIChjYXJvdXNlbCkge1xyXG4gICAgICAgIHNsaWRlci5pdGVtVCA9IHNsaWRlci52YXJzLml0ZW1XaWR0aCArIHNsaWRlTWFyZ2luO1xyXG4gICAgICAgIHNsaWRlci5pdGVtTSA9IHNsaWRlTWFyZ2luO1xyXG4gICAgICAgIHNsaWRlci5taW5XID0gKG1pbkl0ZW1zKSA/IG1pbkl0ZW1zICogc2xpZGVyLml0ZW1UIDogc2xpZGVyLnc7XHJcbiAgICAgICAgc2xpZGVyLm1heFcgPSAobWF4SXRlbXMpID8gKG1heEl0ZW1zICogc2xpZGVyLml0ZW1UKSAtIHNsaWRlTWFyZ2luIDogc2xpZGVyLnc7XHJcbiAgICAgICAgc2xpZGVyLml0ZW1XID0gKHNsaWRlci5taW5XID4gc2xpZGVyLncpID8gKHNsaWRlci53IC0gKHNsaWRlTWFyZ2luICogKG1pbkl0ZW1zIC0gMSkpKS9taW5JdGVtcyA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgKHNsaWRlci5tYXhXIDwgc2xpZGVyLncpID8gKHNsaWRlci53IC0gKHNsaWRlTWFyZ2luICogKG1heEl0ZW1zIC0gMSkpKS9tYXhJdGVtcyA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgKHNsaWRlci52YXJzLml0ZW1XaWR0aCA+IHNsaWRlci53KSA/IHNsaWRlci53IDogc2xpZGVyLnZhcnMuaXRlbVdpZHRoO1xyXG5cclxuICAgICAgICBzbGlkZXIudmlzaWJsZSA9IE1hdGguZmxvb3Ioc2xpZGVyLncvKHNsaWRlci5pdGVtVykpO1xyXG4gICAgICAgIHNsaWRlci5tb3ZlID0gKHNsaWRlci52YXJzLm1vdmUgPiAwICYmIHNsaWRlci52YXJzLm1vdmUgPCBzbGlkZXIudmlzaWJsZSApID8gc2xpZGVyLnZhcnMubW92ZSA6IHNsaWRlci52aXNpYmxlO1xyXG4gICAgICAgIHNsaWRlci5wYWdpbmdDb3VudCA9IE1hdGguY2VpbCgoKHNsaWRlci5jb3VudCAtIHNsaWRlci52aXNpYmxlKS9zbGlkZXIubW92ZSkgKyAxKTtcclxuICAgICAgICBzbGlkZXIubGFzdCA9ICBzbGlkZXIucGFnaW5nQ291bnQgLSAxO1xyXG4gICAgICAgIHNsaWRlci5saW1pdCA9IChzbGlkZXIucGFnaW5nQ291bnQgPT09IDEpID8gMCA6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgKHNsaWRlci52YXJzLml0ZW1XaWR0aCA+IHNsaWRlci53KSA/IChzbGlkZXIuaXRlbVcgKiAoc2xpZGVyLmNvdW50IC0gMSkpICsgKHNsaWRlTWFyZ2luICogKHNsaWRlci5jb3VudCAtIDEpKSA6ICgoc2xpZGVyLml0ZW1XICsgc2xpZGVNYXJnaW4pICogc2xpZGVyLmNvdW50KSAtIHNsaWRlci53IC0gc2xpZGVNYXJnaW47XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgc2xpZGVyLml0ZW1XID0gc2xpZGVyLnc7XHJcbiAgICAgICAgc2xpZGVyLml0ZW1NID0gc2xpZGVNYXJnaW47XHJcbiAgICAgICAgc2xpZGVyLnBhZ2luZ0NvdW50ID0gc2xpZGVyLmNvdW50O1xyXG4gICAgICAgIHNsaWRlci5sYXN0ID0gc2xpZGVyLmNvdW50IC0gMTtcclxuICAgICAgfVxyXG4gICAgICBzbGlkZXIuY29tcHV0ZWRXID0gc2xpZGVyLml0ZW1XIC0gc2xpZGVyLmJveFBhZGRpbmc7XHJcbiAgICAgIHNsaWRlci5jb21wdXRlZE0gPSBzbGlkZXIuaXRlbU07XHJcbiAgICB9O1xyXG5cclxuICAgIHNsaWRlci51cGRhdGUgPSBmdW5jdGlvbihwb3MsIGFjdGlvbikge1xyXG4gICAgICBzbGlkZXIuZG9NYXRoKCk7XHJcblxyXG4gICAgICAvLyB1cGRhdGUgY3VycmVudFNsaWRlIGFuZCBzbGlkZXIuYW5pbWF0aW5nVG8gaWYgbmVjZXNzYXJ5XHJcbiAgICAgIGlmICghY2Fyb3VzZWwpIHtcclxuICAgICAgICBpZiAocG9zIDwgc2xpZGVyLmN1cnJlbnRTbGlkZSkge1xyXG4gICAgICAgICAgc2xpZGVyLmN1cnJlbnRTbGlkZSArPSAxO1xyXG4gICAgICAgIH0gZWxzZSBpZiAocG9zIDw9IHNsaWRlci5jdXJyZW50U2xpZGUgJiYgcG9zICE9PSAwKSB7XHJcbiAgICAgICAgICBzbGlkZXIuY3VycmVudFNsaWRlIC09IDE7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHNsaWRlci5hbmltYXRpbmdUbyA9IHNsaWRlci5jdXJyZW50U2xpZGU7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIHVwZGF0ZSBjb250cm9sTmF2XHJcbiAgICAgIGlmIChzbGlkZXIudmFycy5jb250cm9sTmF2ICYmICFzbGlkZXIubWFudWFsQ29udHJvbHMpIHtcclxuICAgICAgICBpZiAoKGFjdGlvbiA9PT0gXCJhZGRcIiAmJiAhY2Fyb3VzZWwpIHx8IHNsaWRlci5wYWdpbmdDb3VudCA+IHNsaWRlci5jb250cm9sTmF2Lmxlbmd0aCkge1xyXG4gICAgICAgICAgbWV0aG9kcy5jb250cm9sTmF2LnVwZGF0ZShcImFkZFwiKTtcclxuICAgICAgICB9IGVsc2UgaWYgKChhY3Rpb24gPT09IFwicmVtb3ZlXCIgJiYgIWNhcm91c2VsKSB8fCBzbGlkZXIucGFnaW5nQ291bnQgPCBzbGlkZXIuY29udHJvbE5hdi5sZW5ndGgpIHtcclxuICAgICAgICAgIGlmIChjYXJvdXNlbCAmJiBzbGlkZXIuY3VycmVudFNsaWRlID4gc2xpZGVyLmxhc3QpIHtcclxuICAgICAgICAgICAgc2xpZGVyLmN1cnJlbnRTbGlkZSAtPSAxO1xyXG4gICAgICAgICAgICBzbGlkZXIuYW5pbWF0aW5nVG8gLT0gMTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIG1ldGhvZHMuY29udHJvbE5hdi51cGRhdGUoXCJyZW1vdmVcIiwgc2xpZGVyLmxhc3QpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAvLyB1cGRhdGUgZGlyZWN0aW9uTmF2XHJcbiAgICAgIGlmIChzbGlkZXIudmFycy5kaXJlY3Rpb25OYXYpIHsgbWV0aG9kcy5kaXJlY3Rpb25OYXYudXBkYXRlKCk7IH1cclxuXHJcbiAgICB9O1xyXG5cclxuICAgIHNsaWRlci5hZGRTbGlkZSA9IGZ1bmN0aW9uKG9iaiwgcG9zKSB7XHJcbiAgICAgIHZhciAkb2JqID0gJChvYmopO1xyXG5cclxuICAgICAgc2xpZGVyLmNvdW50ICs9IDE7XHJcbiAgICAgIHNsaWRlci5sYXN0ID0gc2xpZGVyLmNvdW50IC0gMTtcclxuXHJcbiAgICAgIC8vIGFwcGVuZCBuZXcgc2xpZGVcclxuICAgICAgaWYgKHZlcnRpY2FsICYmIHJldmVyc2UpIHtcclxuICAgICAgICAocG9zICE9PSB1bmRlZmluZWQpID8gc2xpZGVyLnNsaWRlcy5lcShzbGlkZXIuY291bnQgLSBwb3MpLmFmdGVyKCRvYmopIDogc2xpZGVyLmNvbnRhaW5lci5wcmVwZW5kKCRvYmopO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIChwb3MgIT09IHVuZGVmaW5lZCkgPyBzbGlkZXIuc2xpZGVzLmVxKHBvcykuYmVmb3JlKCRvYmopIDogc2xpZGVyLmNvbnRhaW5lci5hcHBlbmQoJG9iaik7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIHVwZGF0ZSBjdXJyZW50U2xpZGUsIGFuaW1hdGluZ1RvLCBjb250cm9sTmF2LCBhbmQgZGlyZWN0aW9uTmF2XHJcbiAgICAgIHNsaWRlci51cGRhdGUocG9zLCBcImFkZFwiKTtcclxuXHJcbiAgICAgIC8vIHVwZGF0ZSBzbGlkZXIuc2xpZGVzXHJcbiAgICAgIHNsaWRlci5zbGlkZXMgPSAkKHNsaWRlci52YXJzLnNlbGVjdG9yICsgJzpub3QoLmNsb25lKScsIHNsaWRlcik7XHJcbiAgICAgIC8vIHJlLXNldHVwIHRoZSBzbGlkZXIgdG8gYWNjb21kYXRlIG5ldyBzbGlkZVxyXG4gICAgICBzbGlkZXIuc2V0dXAoKTtcclxuXHJcbiAgICAgIC8vRmxleFNsaWRlcjogYWRkZWQoKSBDYWxsYmFja1xyXG4gICAgICBzbGlkZXIudmFycy5hZGRlZChzbGlkZXIpO1xyXG4gICAgfTtcclxuICAgIHNsaWRlci5yZW1vdmVTbGlkZSA9IGZ1bmN0aW9uKG9iaikge1xyXG4gICAgICB2YXIgcG9zID0gKGlzTmFOKG9iaikpID8gc2xpZGVyLnNsaWRlcy5pbmRleCgkKG9iaikpIDogb2JqO1xyXG5cclxuICAgICAgLy8gdXBkYXRlIGNvdW50XHJcbiAgICAgIHNsaWRlci5jb3VudCAtPSAxO1xyXG4gICAgICBzbGlkZXIubGFzdCA9IHNsaWRlci5jb3VudCAtIDE7XHJcblxyXG4gICAgICAvLyByZW1vdmUgc2xpZGVcclxuICAgICAgaWYgKGlzTmFOKG9iaikpIHtcclxuICAgICAgICAkKG9iaiwgc2xpZGVyLnNsaWRlcykucmVtb3ZlKCk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgKHZlcnRpY2FsICYmIHJldmVyc2UpID8gc2xpZGVyLnNsaWRlcy5lcShzbGlkZXIubGFzdCkucmVtb3ZlKCkgOiBzbGlkZXIuc2xpZGVzLmVxKG9iaikucmVtb3ZlKCk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIHVwZGF0ZSBjdXJyZW50U2xpZGUsIGFuaW1hdGluZ1RvLCBjb250cm9sTmF2LCBhbmQgZGlyZWN0aW9uTmF2XHJcbiAgICAgIHNsaWRlci5kb01hdGgoKTtcclxuICAgICAgc2xpZGVyLnVwZGF0ZShwb3MsIFwicmVtb3ZlXCIpO1xyXG5cclxuICAgICAgLy8gdXBkYXRlIHNsaWRlci5zbGlkZXNcclxuICAgICAgc2xpZGVyLnNsaWRlcyA9ICQoc2xpZGVyLnZhcnMuc2VsZWN0b3IgKyAnOm5vdCguY2xvbmUpJywgc2xpZGVyKTtcclxuICAgICAgLy8gcmUtc2V0dXAgdGhlIHNsaWRlciB0byBhY2NvbWRhdGUgbmV3IHNsaWRlXHJcbiAgICAgIHNsaWRlci5zZXR1cCgpO1xyXG5cclxuICAgICAgLy8gRmxleFNsaWRlcjogcmVtb3ZlZCgpIENhbGxiYWNrXHJcbiAgICAgIHNsaWRlci52YXJzLnJlbW92ZWQoc2xpZGVyKTtcclxuICAgIH07XHJcblxyXG4gICAgLy9GbGV4U2xpZGVyOiBJbml0aWFsaXplXHJcbiAgICBtZXRob2RzLmluaXQoKTtcclxuICB9O1xyXG5cclxuICAvLyBFbnN1cmUgdGhlIHNsaWRlciBpc24ndCBmb2N1c3NlZCBpZiB0aGUgd2luZG93IGxvc2VzIGZvY3VzLlxyXG4gICQoIHdpbmRvdyApLmJsdXIoIGZ1bmN0aW9uICggZSApIHtcclxuICAgIGZvY3VzZWQgPSBmYWxzZTtcclxuICB9KS5mb2N1cyggZnVuY3Rpb24gKCBlICkge1xyXG4gICAgZm9jdXNlZCA9IHRydWU7XHJcbiAgfSk7XHJcblxyXG4gIC8vRmxleFNsaWRlcjogRGVmYXVsdCBTZXR0aW5nc1xyXG4gICQuZmxleHNsaWRlci5kZWZhdWx0cyA9IHtcclxuICAgIG5hbWVzcGFjZTogXCJmbGV4LVwiLCAgICAgICAgICAgICAvL3tORVd9IFN0cmluZzogUHJlZml4IHN0cmluZyBhdHRhY2hlZCB0byB0aGUgY2xhc3Mgb2YgZXZlcnkgZWxlbWVudCBnZW5lcmF0ZWQgYnkgdGhlIHBsdWdpblxyXG4gICAgc2VsZWN0b3I6IFwiLnNsaWRlcyA+IGxpXCIsICAgICAgIC8ve05FV30gU2VsZWN0b3I6IE11c3QgbWF0Y2ggYSBzaW1wbGUgcGF0dGVybi4gJ3tjb250YWluZXJ9ID4ge3NsaWRlfScgLS0gSWdub3JlIHBhdHRlcm4gYXQgeW91ciBvd24gcGVyaWxcclxuICAgIGFuaW1hdGlvbjogXCJmYWRlXCIsICAgICAgICAgICAgICAvL1N0cmluZzogU2VsZWN0IHlvdXIgYW5pbWF0aW9uIHR5cGUsIFwiZmFkZVwiIG9yIFwic2xpZGVcIlxyXG4gICAgZWFzaW5nOiBcInN3aW5nXCIsICAgICAgICAgICAgICAgIC8ve05FV30gU3RyaW5nOiBEZXRlcm1pbmVzIHRoZSBlYXNpbmcgbWV0aG9kIHVzZWQgaW4galF1ZXJ5IHRyYW5zaXRpb25zLiBqUXVlcnkgZWFzaW5nIHBsdWdpbiBpcyBzdXBwb3J0ZWQhXHJcbiAgICBkaXJlY3Rpb246IFwiaG9yaXpvbnRhbFwiLCAgICAgICAgLy9TdHJpbmc6IFNlbGVjdCB0aGUgc2xpZGluZyBkaXJlY3Rpb24sIFwiaG9yaXpvbnRhbFwiIG9yIFwidmVydGljYWxcIlxyXG4gICAgcmV2ZXJzZTogZmFsc2UsICAgICAgICAgICAgICAgICAvL3tORVd9IEJvb2xlYW46IFJldmVyc2UgdGhlIGFuaW1hdGlvbiBkaXJlY3Rpb25cclxuICAgIGFuaW1hdGlvbkxvb3A6IHRydWUsICAgICAgICAgICAgLy9Cb29sZWFuOiBTaG91bGQgdGhlIGFuaW1hdGlvbiBsb29wPyBJZiBmYWxzZSwgZGlyZWN0aW9uTmF2IHdpbGwgcmVjZWl2ZWQgXCJkaXNhYmxlXCIgY2xhc3NlcyBhdCBlaXRoZXIgZW5kXHJcbiAgICBzbW9vdGhIZWlnaHQ6IGZhbHNlLCAgICAgICAgICAgIC8ve05FV30gQm9vbGVhbjogQWxsb3cgaGVpZ2h0IG9mIHRoZSBzbGlkZXIgdG8gYW5pbWF0ZSBzbW9vdGhseSBpbiBob3Jpem9udGFsIG1vZGVcclxuICAgIHN0YXJ0QXQ6IDAsICAgICAgICAgICAgICAgICAgICAgLy9JbnRlZ2VyOiBUaGUgc2xpZGUgdGhhdCB0aGUgc2xpZGVyIHNob3VsZCBzdGFydCBvbi4gQXJyYXkgbm90YXRpb24gKDAgPSBmaXJzdCBzbGlkZSlcclxuICAgIHNsaWRlc2hvdzogdHJ1ZSwgICAgICAgICAgICAgICAgLy9Cb29sZWFuOiBBbmltYXRlIHNsaWRlciBhdXRvbWF0aWNhbGx5XHJcbiAgICBzbGlkZXNob3dTcGVlZDogNzAwMCwgICAgICAgICAgIC8vSW50ZWdlcjogU2V0IHRoZSBzcGVlZCBvZiB0aGUgc2xpZGVzaG93IGN5Y2xpbmcsIGluIG1pbGxpc2Vjb25kc1xyXG4gICAgYW5pbWF0aW9uU3BlZWQ6IDYwMCwgICAgICAgICAgICAvL0ludGVnZXI6IFNldCB0aGUgc3BlZWQgb2YgYW5pbWF0aW9ucywgaW4gbWlsbGlzZWNvbmRzXHJcbiAgICBpbml0RGVsYXk6IDAsICAgICAgICAgICAgICAgICAgIC8ve05FV30gSW50ZWdlcjogU2V0IGFuIGluaXRpYWxpemF0aW9uIGRlbGF5LCBpbiBtaWxsaXNlY29uZHNcclxuICAgIHJhbmRvbWl6ZTogZmFsc2UsICAgICAgICAgICAgICAgLy9Cb29sZWFuOiBSYW5kb21pemUgc2xpZGUgb3JkZXJcclxuICAgIGZhZGVGaXJzdFNsaWRlOiB0cnVlLCAgICAgICAgICAgLy9Cb29sZWFuOiBGYWRlIGluIHRoZSBmaXJzdCBzbGlkZSB3aGVuIGFuaW1hdGlvbiB0eXBlIGlzIFwiZmFkZVwiXHJcbiAgICB0aHVtYkNhcHRpb25zOiBmYWxzZSwgICAgICAgICAgIC8vQm9vbGVhbjogV2hldGhlciBvciBub3QgdG8gcHV0IGNhcHRpb25zIG9uIHRodW1ibmFpbHMgd2hlbiB1c2luZyB0aGUgXCJ0aHVtYm5haWxzXCIgY29udHJvbE5hdi5cclxuXHJcbiAgICAvLyBVc2FiaWxpdHkgZmVhdHVyZXNcclxuICAgIHBhdXNlT25BY3Rpb246IHRydWUsICAgICAgICAgICAgLy9Cb29sZWFuOiBQYXVzZSB0aGUgc2xpZGVzaG93IHdoZW4gaW50ZXJhY3Rpbmcgd2l0aCBjb250cm9sIGVsZW1lbnRzLCBoaWdobHkgcmVjb21tZW5kZWQuXHJcbiAgICBwYXVzZU9uSG92ZXI6IGZhbHNlLCAgICAgICAgICAgIC8vQm9vbGVhbjogUGF1c2UgdGhlIHNsaWRlc2hvdyB3aGVuIGhvdmVyaW5nIG92ZXIgc2xpZGVyLCB0aGVuIHJlc3VtZSB3aGVuIG5vIGxvbmdlciBob3ZlcmluZ1xyXG4gICAgcGF1c2VJbnZpc2libGU6IHRydWUsICAgXHRcdC8ve05FV30gQm9vbGVhbjogUGF1c2UgdGhlIHNsaWRlc2hvdyB3aGVuIHRhYiBpcyBpbnZpc2libGUsIHJlc3VtZSB3aGVuIHZpc2libGUuIFByb3ZpZGVzIGJldHRlciBVWCwgbG93ZXIgQ1BVIHVzYWdlLlxyXG4gICAgdXNlQ1NTOiB0cnVlLCAgICAgICAgICAgICAgICAgICAvL3tORVd9IEJvb2xlYW46IFNsaWRlciB3aWxsIHVzZSBDU1MzIHRyYW5zaXRpb25zIGlmIGF2YWlsYWJsZVxyXG4gICAgdG91Y2g6IHRydWUsICAgICAgICAgICAgICAgICAgICAvL3tORVd9IEJvb2xlYW46IEFsbG93IHRvdWNoIHN3aXBlIG5hdmlnYXRpb24gb2YgdGhlIHNsaWRlciBvbiB0b3VjaC1lbmFibGVkIGRldmljZXNcclxuICAgIHZpZGVvOiBmYWxzZSwgICAgICAgICAgICAgICAgICAgLy97TkVXfSBCb29sZWFuOiBJZiB1c2luZyB2aWRlbyBpbiB0aGUgc2xpZGVyLCB3aWxsIHByZXZlbnQgQ1NTMyAzRCBUcmFuc2Zvcm1zIHRvIGF2b2lkIGdyYXBoaWNhbCBnbGl0Y2hlc1xyXG5cclxuICAgIC8vIFByaW1hcnkgQ29udHJvbHNcclxuICAgIGNvbnRyb2xOYXY6IHRydWUsICAgICAgICAgICAgICAgLy9Cb29sZWFuOiBDcmVhdGUgbmF2aWdhdGlvbiBmb3IgcGFnaW5nIGNvbnRyb2wgb2YgZWFjaCBzbGlkZT8gTm90ZTogTGVhdmUgdHJ1ZSBmb3IgbWFudWFsQ29udHJvbHMgdXNhZ2VcclxuICAgIGRpcmVjdGlvbk5hdjogdHJ1ZSwgICAgICAgICAgICAgLy9Cb29sZWFuOiBDcmVhdGUgbmF2aWdhdGlvbiBmb3IgcHJldmlvdXMvbmV4dCBuYXZpZ2F0aW9uPyAodHJ1ZS9mYWxzZSlcclxuICAgIHByZXZUZXh0OiBcIlByZXZpb3VzXCIsICAgICAgICAgICAvL1N0cmluZzogU2V0IHRoZSB0ZXh0IGZvciB0aGUgXCJwcmV2aW91c1wiIGRpcmVjdGlvbk5hdiBpdGVtXHJcbiAgICBuZXh0VGV4dDogXCJOZXh0XCIsICAgICAgICAgICAgICAgLy9TdHJpbmc6IFNldCB0aGUgdGV4dCBmb3IgdGhlIFwibmV4dFwiIGRpcmVjdGlvbk5hdiBpdGVtXHJcblxyXG4gICAgLy8gU2Vjb25kYXJ5IE5hdmlnYXRpb25cclxuICAgIGtleWJvYXJkOiB0cnVlLCAgICAgICAgICAgICAgICAgLy9Cb29sZWFuOiBBbGxvdyBzbGlkZXIgbmF2aWdhdGluZyB2aWEga2V5Ym9hcmQgbGVmdC9yaWdodCBrZXlzXHJcbiAgICBtdWx0aXBsZUtleWJvYXJkOiBmYWxzZSwgICAgICAgIC8ve05FV30gQm9vbGVhbjogQWxsb3cga2V5Ym9hcmQgbmF2aWdhdGlvbiB0byBhZmZlY3QgbXVsdGlwbGUgc2xpZGVycy4gRGVmYXVsdCBiZWhhdmlvciBjdXRzIG91dCBrZXlib2FyZCBuYXZpZ2F0aW9uIHdpdGggbW9yZSB0aGFuIG9uZSBzbGlkZXIgcHJlc2VudC5cclxuICAgIG1vdXNld2hlZWw6IGZhbHNlLCAgICAgICAgICAgICAgLy97VVBEQVRFRH0gQm9vbGVhbjogUmVxdWlyZXMganF1ZXJ5Lm1vdXNld2hlZWwuanMgKGh0dHBzOi8vZ2l0aHViLmNvbS9icmFuZG9uYWFyb24vanF1ZXJ5LW1vdXNld2hlZWwpIC0gQWxsb3dzIHNsaWRlciBuYXZpZ2F0aW5nIHZpYSBtb3VzZXdoZWVsXHJcbiAgICBwYXVzZVBsYXk6IGZhbHNlLCAgICAgICAgICAgICAgIC8vQm9vbGVhbjogQ3JlYXRlIHBhdXNlL3BsYXkgZHluYW1pYyBlbGVtZW50XHJcbiAgICBwYXVzZVRleHQ6IFwiUGF1c2VcIiwgICAgICAgICAgICAgLy9TdHJpbmc6IFNldCB0aGUgdGV4dCBmb3IgdGhlIFwicGF1c2VcIiBwYXVzZVBsYXkgaXRlbVxyXG4gICAgcGxheVRleHQ6IFwiUGxheVwiLCAgICAgICAgICAgICAgIC8vU3RyaW5nOiBTZXQgdGhlIHRleHQgZm9yIHRoZSBcInBsYXlcIiBwYXVzZVBsYXkgaXRlbVxyXG5cclxuICAgIC8vIFNwZWNpYWwgcHJvcGVydGllc1xyXG4gICAgY29udHJvbHNDb250YWluZXI6IFwiXCIsICAgICAgICAgIC8ve1VQREFURUR9IGpRdWVyeSBPYmplY3QvU2VsZWN0b3I6IERlY2xhcmUgd2hpY2ggY29udGFpbmVyIHRoZSBuYXZpZ2F0aW9uIGVsZW1lbnRzIHNob3VsZCBiZSBhcHBlbmRlZCB0b28uIERlZmF1bHQgY29udGFpbmVyIGlzIHRoZSBGbGV4U2xpZGVyIGVsZW1lbnQuIEV4YW1wbGUgdXNlIHdvdWxkIGJlICQoXCIuZmxleHNsaWRlci1jb250YWluZXJcIikuIFByb3BlcnR5IGlzIGlnbm9yZWQgaWYgZ2l2ZW4gZWxlbWVudCBpcyBub3QgZm91bmQuXHJcbiAgICBtYW51YWxDb250cm9sczogXCJcIiwgICAgICAgICAgICAgLy97VVBEQVRFRH0galF1ZXJ5IE9iamVjdC9TZWxlY3RvcjogRGVjbGFyZSBjdXN0b20gY29udHJvbCBuYXZpZ2F0aW9uLiBFeGFtcGxlcyB3b3VsZCBiZSAkKFwiLmZsZXgtY29udHJvbC1uYXYgbGlcIikgb3IgXCIjdGFicy1uYXYgbGkgaW1nXCIsIGV0Yy4gVGhlIG51bWJlciBvZiBlbGVtZW50cyBpbiB5b3VyIGNvbnRyb2xOYXYgc2hvdWxkIG1hdGNoIHRoZSBudW1iZXIgb2Ygc2xpZGVzL3RhYnMuXHJcbiAgICBjdXN0b21EaXJlY3Rpb25OYXY6IFwiXCIsICAgICAgICAgLy97TkVXfSBqUXVlcnkgT2JqZWN0L1NlbGVjdG9yOiBDdXN0b20gcHJldiAvIG5leHQgYnV0dG9uLiBNdXN0IGJlIHR3byBqUXVlcnkgZWxlbWVudHMuIEluIG9yZGVyIHRvIG1ha2UgdGhlIGV2ZW50cyB3b3JrIHRoZXkgaGF2ZSB0byBoYXZlIHRoZSBjbGFzc2VzIFwicHJldlwiIGFuZCBcIm5leHRcIiAocGx1cyBuYW1lc3BhY2UpXHJcbiAgICBzeW5jOiBcIlwiLCAgICAgICAgICAgICAgICAgICAgICAgLy97TkVXfSBTZWxlY3RvcjogTWlycm9yIHRoZSBhY3Rpb25zIHBlcmZvcm1lZCBvbiB0aGlzIHNsaWRlciB3aXRoIGFub3RoZXIgc2xpZGVyLiBVc2Ugd2l0aCBjYXJlLlxyXG4gICAgYXNOYXZGb3I6IFwiXCIsICAgICAgICAgICAgICAgICAgIC8ve05FV30gU2VsZWN0b3I6IEludGVybmFsIHByb3BlcnR5IGV4cG9zZWQgZm9yIHR1cm5pbmcgdGhlIHNsaWRlciBpbnRvIGEgdGh1bWJuYWlsIG5hdmlnYXRpb24gZm9yIGFub3RoZXIgc2xpZGVyXHJcblxyXG4gICAgLy8gQ2Fyb3VzZWwgT3B0aW9uc1xyXG4gICAgaXRlbVdpZHRoOiAwLCAgICAgICAgICAgICAgICAgICAvL3tORVd9IEludGVnZXI6IEJveC1tb2RlbCB3aWR0aCBvZiBpbmRpdmlkdWFsIGNhcm91c2VsIGl0ZW1zLCBpbmNsdWRpbmcgaG9yaXpvbnRhbCBib3JkZXJzIGFuZCBwYWRkaW5nLlxyXG4gICAgaXRlbU1hcmdpbjogMCwgICAgICAgICAgICAgICAgICAvL3tORVd9IEludGVnZXI6IE1hcmdpbiBiZXR3ZWVuIGNhcm91c2VsIGl0ZW1zLlxyXG4gICAgbWluSXRlbXM6IDEsICAgICAgICAgICAgICAgICAgICAvL3tORVd9IEludGVnZXI6IE1pbmltdW0gbnVtYmVyIG9mIGNhcm91c2VsIGl0ZW1zIHRoYXQgc2hvdWxkIGJlIHZpc2libGUuIEl0ZW1zIHdpbGwgcmVzaXplIGZsdWlkbHkgd2hlbiBiZWxvdyB0aGlzLlxyXG4gICAgbWF4SXRlbXM6IDAsICAgICAgICAgICAgICAgICAgICAvL3tORVd9IEludGVnZXI6IE1heG1pbXVtIG51bWJlciBvZiBjYXJvdXNlbCBpdGVtcyB0aGF0IHNob3VsZCBiZSB2aXNpYmxlLiBJdGVtcyB3aWxsIHJlc2l6ZSBmbHVpZGx5IHdoZW4gYWJvdmUgdGhpcyBsaW1pdC5cclxuICAgIG1vdmU6IDAsICAgICAgICAgICAgICAgICAgICAgICAgLy97TkVXfSBJbnRlZ2VyOiBOdW1iZXIgb2YgY2Fyb3VzZWwgaXRlbXMgdGhhdCBzaG91bGQgbW92ZSBvbiBhbmltYXRpb24uIElmIDAsIHNsaWRlciB3aWxsIG1vdmUgYWxsIHZpc2libGUgaXRlbXMuXHJcbiAgICBhbGxvd09uZVNsaWRlOiB0cnVlLCAgICAgICAgICAgLy97TkVXfSBCb29sZWFuOiBXaGV0aGVyIG9yIG5vdCB0byBhbGxvdyBhIHNsaWRlciBjb21wcmlzZWQgb2YgYSBzaW5nbGUgc2xpZGVcclxuXHJcbiAgICAvLyBDYWxsYmFjayBBUElcclxuICAgIHN0YXJ0OiBmdW5jdGlvbigpe30sICAgICAgICAgICAgLy9DYWxsYmFjazogZnVuY3Rpb24oc2xpZGVyKSAtIEZpcmVzIHdoZW4gdGhlIHNsaWRlciBsb2FkcyB0aGUgZmlyc3Qgc2xpZGVcclxuICAgIGJlZm9yZTogZnVuY3Rpb24oKXt9LCAgICAgICAgICAgLy9DYWxsYmFjazogZnVuY3Rpb24oc2xpZGVyKSAtIEZpcmVzIGFzeW5jaHJvbm91c2x5IHdpdGggZWFjaCBzbGlkZXIgYW5pbWF0aW9uXHJcbiAgICBhZnRlcjogZnVuY3Rpb24oKXt9LCAgICAgICAgICAgIC8vQ2FsbGJhY2s6IGZ1bmN0aW9uKHNsaWRlcikgLSBGaXJlcyBhZnRlciBlYWNoIHNsaWRlciBhbmltYXRpb24gY29tcGxldGVzXHJcbiAgICBlbmQ6IGZ1bmN0aW9uKCl7fSwgICAgICAgICAgICAgIC8vQ2FsbGJhY2s6IGZ1bmN0aW9uKHNsaWRlcikgLSBGaXJlcyB3aGVuIHRoZSBzbGlkZXIgcmVhY2hlcyB0aGUgbGFzdCBzbGlkZSAoYXN5bmNocm9ub3VzKVxyXG4gICAgYWRkZWQ6IGZ1bmN0aW9uKCl7fSwgICAgICAgICAgICAvL3tORVd9IENhbGxiYWNrOiBmdW5jdGlvbihzbGlkZXIpIC0gRmlyZXMgYWZ0ZXIgYSBzbGlkZSBpcyBhZGRlZFxyXG4gICAgcmVtb3ZlZDogZnVuY3Rpb24oKXt9LCAgICAgICAgICAgLy97TkVXfSBDYWxsYmFjazogZnVuY3Rpb24oc2xpZGVyKSAtIEZpcmVzIGFmdGVyIGEgc2xpZGUgaXMgcmVtb3ZlZFxyXG4gICAgaW5pdDogZnVuY3Rpb24oKSB7fSAgICAgICAgICAgICAvL3tORVd9IENhbGxiYWNrOiBmdW5jdGlvbihzbGlkZXIpIC0gRmlyZXMgYWZ0ZXIgdGhlIHNsaWRlciBpcyBpbml0aWFsbHkgc2V0dXBcclxuICB9O1xyXG5cclxuICAvL0ZsZXhTbGlkZXI6IFBsdWdpbiBGdW5jdGlvblxyXG4gICQuZm4uZmxleHNsaWRlciA9IGZ1bmN0aW9uKG9wdGlvbnMpIHtcclxuICAgIGlmIChvcHRpb25zID09PSB1bmRlZmluZWQpIHsgb3B0aW9ucyA9IHt9OyB9XHJcblxyXG4gICAgaWYgKHR5cGVvZiBvcHRpb25zID09PSBcIm9iamVjdFwiKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKSxcclxuICAgICAgICAgICAgc2VsZWN0b3IgPSAob3B0aW9ucy5zZWxlY3RvcikgPyBvcHRpb25zLnNlbGVjdG9yIDogXCIuc2xpZGVzID4gbGlcIixcclxuICAgICAgICAgICAgJHNsaWRlcyA9ICR0aGlzLmZpbmQoc2VsZWN0b3IpO1xyXG5cclxuICAgICAgaWYgKCAoICRzbGlkZXMubGVuZ3RoID09PSAxICYmIG9wdGlvbnMuYWxsb3dPbmVTbGlkZSA9PT0gZmFsc2UgKSB8fCAkc2xpZGVzLmxlbmd0aCA9PT0gMCApIHtcclxuICAgICAgICAgICRzbGlkZXMuZmFkZUluKDQwMCk7XHJcbiAgICAgICAgICBpZiAob3B0aW9ucy5zdGFydCkgeyBvcHRpb25zLnN0YXJ0KCR0aGlzKTsgfVxyXG4gICAgICAgIH0gZWxzZSBpZiAoJHRoaXMuZGF0YSgnZmxleHNsaWRlcicpID09PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgIG5ldyAkLmZsZXhzbGlkZXIodGhpcywgb3B0aW9ucyk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIC8vIEhlbHBlciBzdHJpbmdzIHRvIHF1aWNrbHkgcGVyZm9ybSBmdW5jdGlvbnMgb24gdGhlIHNsaWRlclxyXG4gICAgICB2YXIgJHNsaWRlciA9ICQodGhpcykuZGF0YSgnZmxleHNsaWRlcicpO1xyXG4gICAgICBzd2l0Y2ggKG9wdGlvbnMpIHtcclxuICAgICAgICBjYXNlIFwicGxheVwiOiAkc2xpZGVyLnBsYXkoKTsgYnJlYWs7XHJcbiAgICAgICAgY2FzZSBcInBhdXNlXCI6ICRzbGlkZXIucGF1c2UoKTsgYnJlYWs7XHJcbiAgICAgICAgY2FzZSBcInN0b3BcIjogJHNsaWRlci5zdG9wKCk7IGJyZWFrO1xyXG4gICAgICAgIGNhc2UgXCJuZXh0XCI6ICRzbGlkZXIuZmxleEFuaW1hdGUoJHNsaWRlci5nZXRUYXJnZXQoXCJuZXh0XCIpLCB0cnVlKTsgYnJlYWs7XHJcbiAgICAgICAgY2FzZSBcInByZXZcIjpcclxuICAgICAgICBjYXNlIFwicHJldmlvdXNcIjogJHNsaWRlci5mbGV4QW5pbWF0ZSgkc2xpZGVyLmdldFRhcmdldChcInByZXZcIiksIHRydWUpOyBicmVhaztcclxuICAgICAgICBkZWZhdWx0OiBpZiAodHlwZW9mIG9wdGlvbnMgPT09IFwibnVtYmVyXCIpIHsgJHNsaWRlci5mbGV4QW5pbWF0ZShvcHRpb25zLCB0cnVlKTsgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfTtcclxufSkoalF1ZXJ5KTtcclxuIl19
