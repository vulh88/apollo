# Table of Contents

* [Installation](#installation)
    * [General](#general)
    * [Wordpress](#wordpress)
    * [gulp command](#gulp-command)
    * [htaccess](#htaccess)
* [Development](#development)
    * [Solr Search](#solr-search)
    * [Cron Job](#cron-job)
    * [Report Module](#report-module)
        * [Overview](#overview)
        * [Enable authentication for MongoDB (optional)](#enable-authentication-for-mongodb-optional)
    * [Widget](#widget)
    * [Shortcode](#shortcode)    
        * [User] (#user)
        * [Taxonomy](#taxonomy)
        * [Content](#content)
        * [Apollo custom widget location](#apollo-custom-widget-location)
        * [Others](#others)
    * [Browser caching](#browser-caching)
* [Upgrade WP to the latest version](#upgrade-wp-to-the-latest-version)
* [Testing](#testing)
* [Deployment](#deployment)
    * [On local machine](#on-local-machine)
    * [On production server](#on-production-server)
* [Migration sites](#migration-sites)  
* [Launch a new site](#launch-a-new-site)
    * [Google Cloud](#google-cloud)  
* [Apollo server](#apollo-server)    
        

# Installation

## General

* Double check *curl* library

## Wordpress

- Clone source from GIT http://git.elidev.info/nhanlt/apollo-theme
- Rename wp-content/mu-plugins to wp-content/_mu-plugins
- Install single Wordpress
- Make sure you have written permission for wp-content/uploads folder
- Import the attached table (wp-content/themes/apollo/databases/wp_apollo_blog_user.sql) to the database
- Enable Wordpress multisite (https://codex.wordpress.org/Create_A_Network)
- Refresh the page, Wordpress will ask you to login again to the Admin panel
- Enable whatever theme (apollo or apollo-child or milwaukee365-child) that you need to adjust from the network theme setting page (/wp-admin/network/themes.php)
- Rename wp-content/_mu-plugins to wp-content/mu-plugins
- Activate the specific theme from the site theme setting (/wp-admin/themes.php)
- Logout and login to admin. If you can not login admin site, please remove the browser cache (include cookies), the try it again.
- Done!!!

## Webpack command

- Require yarn or npm for developing css, js

## htaccess

Configuring max upload file size to increase the maximum upload file size for admin panel by adding the following code below to the end of the .htaccess file

```
...
php_value upload_max_filesize 20M
php_value post_max_size 20M
```



# Development

## Solr Search

- Add new configuration to: ```wp-config.php```

```
define('APL_SOLR_DATA_DIR', '/Users/vulh/Documents/apollo/solr-6.3.0/server/solr'); // Require for the network admin whenever you enable solr
define('APL_SOLR_HOST', '127.0.0.1'); // Not require, if this parameter does not exist the default value is 127.0.0.1
define('APL_SOLR_PORT', '8983'); // Not require, if this parameter does not exist the default value is 8983
```

- Copy the **conf** directory from **solr_source/server/solr/configsets/basic_configs/conf** to **apollo/inc/admin/network/solr**. 

- Update **apollo/inc/admin/network/solr/managed-schema** file by adding 2 fields and a copy field after the "id" field:

```
<field name="title" type="text_general" indexed="true" stored="true"/>
<field name="category" type="strings"/>
<copyField source="title" dest="_text_"/>
```

- Enable and re-saving network configuration of current site. Eg http://apollo-theme.elidev.info/wp-admin/network/site-settings.php?id=1 . At that time, SOLR has been enabled successfully!


## Cron Job

- Generate top ten event

```
0 */8 * * * wget -q -O - /cron/LEVhUHmdGeZWXcYZv6IsQpsxaHNszov2FUnQl9DW/
```

## Event PDF Export

From the apollo theme:

```
    tar -zxvf inc/tools/pdflib.tar.gz
```

## Report Module

#### Overview

* Admin UI https://robomongo.org/download
* Report is a big module using MongoDB for storing data as the number of clicks of Buy Ticket, Discount, Official Website buttons.
* Ticket for referring: http://redmine.elidev.info/issues/11227, http://212.98.167.242/mantisbt/view.php?id=1703#c9759
* Setting up:
    * Setting up Mongo DB server https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/
    * Setting up PHP mongo DB driver https://docs.mongodb.com/php-library/master/tutorial/install-php-library
    * Basic PHP Mongo DB https://docs.mongodb.com/php-library/master/tutorial/install-php-library/
* Setting up for MongoDB authentication (optional)
* Admin default sort: last updated, DESC
* Code files
    * Admin
        * inc/admin/report/APL_Admin_Report.php
        * inc/admin/report/views/APL_Report_Table.php
    * Front end
        * inc/class-apollo-ajax.php (handle AJAX call, function: log_click_activity())
        * inc/apollo-activity-system.php
        * inc/apollo-log-system.php (Register store click event callback)
        * inc/Lib/Report/Abstracts/ReportAbstract.php
        * inc/Lib/Report/Contracts/ReportInterface.php
        * inc/Lib/Report/Event.php (insert/update clicks to MongoDB)

#### Enable authentication for MongoDB (optional)

* This is MongoDB configuration on my machine
    * MongoDB version: v3.4.2
    * MongoDB host : 127.0.0.1
    * MongoDB port : 27017
* This guide is based on these official MongoDB documents
    * https://docs.mongodb.com/manual/tutorial/enable-authentication/
    * https://docs.mongodb.com/manual/reference/configuration-options/#security-options
    

**Step 1** : Login to mongo shell, use "admin" database to create a new admin user. This admin user will have full permissions.
    
    ```
    $ mongo
    $ show dbs
    $ use admin
    $ db.createUser({user: "admin", pwd: "123456", roles:[{role: "userAdminAnyDatabase", db: "admin"}, {role: "dbAdminAnyDatabase", db: "admin"}, {role: "readWriteAnyDatabase", db: "admin"}]})
    ```    

    Now check the user was created
    
    ```
    $ show collections
    $ db.system.users.find()
    $ exit
    ```    


**Step 2**: Change mongodb config file to enable authentication

    ```
    $ sudo vim /etc/mongod.conf
    ```

    Add the following lines and save

    ```
    security:
      authorization: enabled    
    ```
    
    Then, restart MongoDB daemon service

    ```
    $ sudo service mongod restart
    $ ps aux | grep mongo
    ```
    
**Step 3**: run mongod with authentication mode and use mongo shell to login "admin" database to execute test queries

    ```
    $ mongod --auth --port 27017
    $ mongo 127.0.0.1:27017/admin -u admin -p '123456'
    ```    
    
**Step 4**: now, you must add account and password to wp-config.php file of Apollo project
  
    ```
    define('MONGODB_HOST'    , '127.0.0.1');
    define('MONGODB_PORT'    , 27017);
    define('MONGODB_USERNAME', 'admin');
    define('MONGODB_PASSWORD', '123456');
    ```    
      
    Finally, please re-check in Admin panel: http://apollo-theme.elidev.info/wp-admin/admin.php?page=apollo-report
          
          
## Widget
Ticket [#3162](http://redmine.elidev.info/issues/3162)
           
* apollo search
* apollo custom text
* apollo subscribe
* apollo top ten     
     
## Shortcode
Ticket [#3162](http://redmine.elidev.info/issues/3163)
     
#### User <a id="sc-user"></a>

```
[apollo_registration]
[apollo_login]
[apollo_login_fb]
[apollo_logout]
```


#### Taxonomy

```
[apollo_taxonomy taxonomy='{taxonomy_name}' per_page='{number}']
```


* {taxonomy_name} default organization
    * event-type
    * organization-type
    * venue-type
    * artist-type
    * classified-type
    * public_art-type
    * education-type
    * business-type
    
* {number} default 10


#### Content

```
[apollo_content type={content_type} id={type_id}]
```

* {content_type} - default page
* {type_id} - id of content type (example, id of page/post)

#### Apollo custom widget location

```
[apollo_widgets location='std_cus_pos' key={widget_location_key}]
```

* Locations (location)
    * std: Standard (right column on desktop; below body content on mobile)
    * std_cus_pos: Standard custom position 1 (right column on desktop; between spotlight and featured blocks on mobile
    * desktop_only: Desktop only
    * mobile_only: Mobile only
    * mobile_only_cus_pos: Mobile only custom position 1 (between spotlight and featured blocks

`Use inside the Theme Core`


#### Others

```
[apollo_slideshow ids="{id1,id2,..., idn}" height={width} width={height} caption={true/false}]
```

* ids: ID of images

```
[apollo_topten_viewed]
```

* Add the is_widget parameter which is equal to 1 if it is displayed in a custom widget     
     
## Browser caching

**Enable Apache modules**

Firstly, to use ```.htaccess``` file for browser caching, you should enable 2 modules in Apache server: ```mod_expires``` & ```mod_headers```.
To check which modules are enabled in Apache, please use this command:

```
$ apache2ctl -M
```

Enable ```mod_expires``` & ```mod_headers``` module

```
$ sudo a2enmod expires
$ sudo a2enmod headers
```

Restart Apache server

```
$ sudo /etc/init.d/apache2 restart
```

**Config .htaccess file to enable browser caching**

File to config: ```apollo-theme/.htaccess```

```
<IfModule mod_expires.c>
    ExpiresActive   On
    ExpiresDefault  "access plus 2 days"

    # CSS
    ExpiresByType text/css "access plus 1 month"

    # Javascript
    ExpiresByType text/javascript           "access plus 1 month"
    ExpiresByType application/javascript    "access plus 1 month"
    ExpiresByType application/x-javascript  "access plus 1 month"

    # Images
    ExpiresByType image/gif     "access plus 1 month"
    ExpiresByType image/bmp     "access plus 1 month"
    ExpiresByType image/jpeg    "access plus 1 month"
    ExpiresByType image/jpg     "access plus 1 month"
    ExpiresByType image/png     "access plus 1 month"
    ExpiresByType image/x-icon  "access plus 1 month"
</IfModule>

<IfModule mod_headers.c>
    Header unset ETag
</IfModule>
```

**Notes:**

* You can change the duration of caching. For example: 2 years, 2 months, 2 weeks...
* ```Ctrl + F5``` to reload the page, ```F5``` to see which files are cached (file cached with 304 HTTP response)


**References**

* https://github.com/h5bp/html5-boilerplate/blob/master/dist/.htaccess
* http://httpd.apache.org/docs/2.4/mod/mod_expires.html
* http://httpd.apache.org/docs/2.4/mod/mod_headers.html 
          

# Testing

* From the terminal on the DEV server type:

```
$ nano ~/.bash_profile
```

* Add script to your bash_profile in dev server:

```
git_build_apl() {
      cd /var/www/apollo-theme.elidev.info/web;
      git reset --hard HEAD;
      git pull;
      git checkout master;
      git checkout -B $1 origin/$1;
}
  
git_rebuild_apl() {
      cd /var/www/apollo-theme.elidev.info/web;
      git reset --hard HEAD;
      git checkout $1;
      git pull origin $1;
}
``` 

* Usage
    * Setting up a new branch: **git_build_apl {branch}**
    * Setting up  an existed branch: **git_rebuild_apl {branch}**  
  


# Deployment

## On local machine

**Step 1:** (Optional) if you do not update any JS or CSS file, please ignore this step

```
    ./_sh/cb_apollo.sh
    ./_sh/cb_octave.sh
```

## On production server 

**Step 1:** From the terminal of the live server run command

```
./_sh/deploy.sh
```

**Step 2:** (Optional) If we have already modified or inserted a new REWRITE rule, run the below command to flush rewrite rules functionality for all sites

* http://wp1.artsopolis.com/script/?action=flush_rewrite_rules_all_sites&site_ids=ALL
* http://wpsrv2.artsopolis.com/script/?action=flush_rewrite_rules_all_sites&site_ids=ALL


# Upgrade WP to the latest version
     
* Backup the database
* Backup the modify query raw(from line 108 to line 123) on the apollo-theme/wp-admin/network/site-settings.php.
* Upgrade Wordpress step by step https://codex.wordpress.org/Updating_WordPress. 
* Upgrade network: the system will display a link requires us to upgrade the network of all sites in this page http://wpdev1.artsopolis.com/wp-admin/network/
* We modified this form for some specific purpose eg 'User Association' so should double check admin user form http://wpdev1.artsopolis.com/wp-admin/user-new.php again to make sure it works correctly with the latest version
* Double check most of modules both on the front-end and admin site eg: event, venue, organization etc


# Migration sites

https://goo.gl/tBbuCP

# Launch a new site

### Google cloud

https://goo.gl/nLA3Zs

# Apollo server

https://goo.gl/tHhKV3

### Migrate users between two servers:

* Source server

```
    define('APL_USER_API_BASE_URL', 'http://{Destination_domain}/api/v1/users/apollo-users-api');
    define('APL_USER_API_GET_FROM_USER_ID', 28966);
    define('APL_USER_API_TOKEN', 'HaXEFxCcWfW3hgNnd20IkxH0B7wdIcBpfYKqL26Q');
    define('APL_USER_API_REFERENCE_SERVER_NAME', 'BlueHost');
    define('APOLLO_MIGRATED_SITES', array()); // Avoid get users of migrated site 
    
```

* Destination server

```
    define('APL_USER_API_BASE_URL', 'http://{Source_domain}/api/v1/users/apollo-users-api');
    define('APL_USER_API_GET_FROM_USER_ID', 28966);
    define('APL_USER_API_TOKEN', 'HaXEFxCcWfW3hgNnd20IkxH0B7wdIcBpfYKqL26Q');
    define('APL_USER_API_REFERENCE_SERVER_NAME', 'Web 3');
    
```

* Enable maintenance mode for Admin panel

```
   define('APOLLO_ADMIN_NOTICE', true);
   define('APOLLO_ADMIN_NOTICE_BLOG_ID', 68); // Active only for specific site 
    
```

* Allow update plugins

wp-config.php

```
    define('FS_METHOD', 'direct');
```

From *wp-content* directory, create: *upgrade* directory with writable permission for www-data



# Changes log

== 2018-12-27 ==

- Remove gulp, versioning css, js files by webpack instead of defining any version

== 2019-02-11 ==

- Event > Syndication > Add new outputs: Expanded HTML - English, Expanded HTML 2 - English   