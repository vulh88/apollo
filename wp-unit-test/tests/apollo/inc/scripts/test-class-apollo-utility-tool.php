<?php

class ApolloUtilityToolTest extends WP_UnitTestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    public function tearDown()
    {
        parent::tearDown();
    }

    static function setUpBeforeClass()
    {

    }

    static function tearDownAfterClass()
    {
        parent::tearDownAfterClass();
    }

    function providerMigrateBlogUserTableDataToUserModulesTable()
    {
        return array(
            // blogId, userId, artistId, orgId, venueId, agencyId, educatorId
            array(1, 9998, 9998, 9998, 9998, 9998, 9998),
            array(1, 9999, 9999, 9999, 9999, 9999, 9999),
        );
    }

    /**
     * Test migrate associated data of multiple selection module
     *
     * @param int $blogId
     * @param int $userId
     * @param int $artistId
     * @param int $orgId
     * @param int $venueId
     * @param int $agencyId
     * @param int $educatorId
     *
     * @dataProvider providerMigrateBlogUserTableDataToUserModulesTable
     * @ticket #15324 - CF] 20180321 - [Unit test] Clone associated data of multiple selection module
     */
    function testMigrateBlogUserTableDataToUserModuleTable($blogId, $userId, $artistId, $orgId, $venueId, $agencyId, $educatorId)
    {
        global $wpdb;
        $wpdb->blogs = $wpdb->prefix . 'blogs';

        // prepare data
        wp_set_current_user($userId);
        $aplQuery = new Apl_query(Apollo_App::getBlogUserTable());
        $where = sprintf(
            'blog_id = %d AND user_id = %d AND artist_id = %d AND org_id = %d AND venue_id = %d AND agency_id = %d AND educator_id = %d',
            $blogId, $userId, $artistId, $orgId, $venueId, $agencyId, $educatorId
        );
        if (!$aplQuery->get_total($where)) {
            $aplQuery->insert(array(
                'blog_id'     => $blogId,
                'user_id'     => $userId,
                'artist_id'   => $artistId,
                'org_id'      => $orgId,
                'venue_id'    => $venueId,
                'agency_id'   => $agencyId,
                'educator_id' => $educatorId,
            ));
        }

        $utility = new Apollo_Utility_Tool();
        $utility->setSiteIds(array($blogId));
        $utility->migrateBlogUserTableDataToUserModulesTable();

        $table          = $wpdb->prefix . Apollo_Tables::_APL_USER_MODULES;
        $totalArtists   = $wpdb->get_var("SELECT count(*) FROM $table WHERE user_id = $userId AND post_id = $artistId   AND post_type = '" . Apollo_DB_Schema::_ARTIST_PT . "'");
        $totalOrgs      = $wpdb->get_var("SELECT count(*) FROM $table WHERE user_id = $userId AND post_id = $orgId      AND post_type = '" . Apollo_DB_Schema::_ORGANIZATION_PT . "'");
        $totalVenues    = $wpdb->get_var("SELECT count(*) FROM $table WHERE user_id = $userId AND post_id = $venueId    AND post_type = '" . Apollo_DB_Schema::_VENUE_PT . "'");
        $totalAgencies  = $wpdb->get_var("SELECT count(*) FROM $table WHERE user_id = $userId AND post_id = $agencyId   AND post_type = '" . Apollo_DB_Schema::_AGENCY_PT . "'");
        $totalEducators = $wpdb->get_var("SELECT count(*) FROM $table WHERE user_id = $userId AND post_id = $educatorId AND post_type = '" . Apollo_DB_Schema::_EDUCATOR_PT . "'");

        $this->assertEquals(1, $totalArtists);
        $this->assertEquals(1, $totalOrgs);
        $this->assertEquals(1, $totalVenues);
        $this->assertEquals(1, $totalAgencies);
        $this->assertEquals(1, $totalEducators);
    }
}
