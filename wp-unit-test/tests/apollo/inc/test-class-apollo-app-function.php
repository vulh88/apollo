<?php

class ApolloAppTest extends WP_UnitTestCase {

    public function setUp() {
        // This code will run before each test
        parent::setUp();
    }
    public function tearDown() {
        // This code will run after each test
        parent::tearDown();
    }

    static function setUpBeforeClass(){

    }

    static function tearDownAfterClass(){
        parent::tearDownAfterClass();
    }

    function testThemeActive() {
        fwrite(STDOUT, "\n\t\t Theme active:" . wp_get_theme() . "\n");
        $this->assertTrue(  wp_get_theme() == 'Apollo Theme');
    }

    function testByPassEventPendingApproveLoggedinUser() {
        $value_compare = Apollo_App::bypassEventPendingApproveLoggedinUser();

        $value_test_1 = of_get_option(Apollo_DB_Schema::_ENABLE_EVENT_BYPASS_APPROVAL_PROCESS, false);
        $value_test_2 = of_get_option(Apollo_DB_Schema::_ENABLE_EVENT_BYPASS_APPROVAL_METHOD, 1);

        if (!$value_test_1) {
            $this->assertFalse($value_compare);
        } else if($value_test_2) {
            $this->assertTrue($value_compare);
        } else {
            $listOfUserCan = get_site_option(Apollo_Tables::_APL_BYPASS_PENDING_APPROVAL, array());
            $currentUserId = get_current_user_id() ? get_current_user_id() : -1;

            if (in_array($currentUserId, $listOfUserCan)) {
                $this->assertTrue($value_compare);
            } else {
                $this->assertFalse($value_compare);
            }
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Start test getNumberImagePulledFromModule
    |--------------------------------------------------------------------------
    */
    public function providerTestGetNumberImagePulledFromModuleReturnEmpty(){
        return array(
            array('', ''),
            array('', 'a'),
            array('', 123),
            array('', 0),
            array('', NULL),
            array('', false),
            array('1', ''),
            array('a', ''),
            array(1, ''),
            array(0, ''),
            array(-1, ''),
            array(NULL, 1),
            array(0, 1),
            array(false, 1),
            array(-1, ''),
            array(1, NULL),
            array(1, 0),
            array(1, false),
        );
    }

    /**
     * @param $module
     * @param $post_id
     * @dataProvider providerTestGetNumberImagePulledFromModuleReturnEmpty
     */
    public function testGetNumberImagePulledFromModuleReturnEmpty($post_id, $module)
    {
        $this->assertEmpty(Apollo_App::getNumberImagePulledFromModule($post_id, $module));
    }


    public function providerTestGetNumberImagePulledFromModuleReturnNumber(){
        return array(
            array('123', '456'),
            array('123', 123),
            array('abcabc', 123),
            array(2, 3),
            array(2, -1),
            array(-1, 3),
            array(2, 'artists'),
        );
    }

    /**
     * @dataProvider providerTestGetNumberImagePulledFromModuleReturnNumber
     * @param $post_id
     * @param $module
     */
    public function testGetNumberImagePulledFromModuleReturnNumber($post_id, $module)
    {
        $value_result = Apollo_App::getNumberImagePulledFromModule($post_id, $module);
        $isEnable = Apollo_App::apollo_get_meta_data($post_id, 'enable_images_'.$module ) == 'ON';
        $numberImages = Apollo_App::apollo_get_meta_data($post_id, 'number_images_'.$module );
        if ($isEnable) {
            $this->assertInternalType("int", $value_result);
            $this->assertEquals($numberImages, $value_result);
        } else {
            $this->assertEmpty($value_result);
        }
    }
    /*
    |--------------------------------------------------------------------------
    | End testing getNumberImagePulledFromModule
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | Start test getCustomUrlByModuleName
    |--------------------------------------------------------------------------
    */
    public function providerGetCustomUrlByModuleName(){
        $exp_classified = of_get_option(Apollo_DB_Schema::_CLASSIFIED_CUSTOM_SLUG);
        $exp_blog = of_get_option(Apollo_DB_Schema::_BLOG_CUSTOM_SLUG);
        $exp_artist = of_get_option(Apollo_DB_Schema::_ARTIST_CUSTOM_SLUG);

        return array(
            array('post', $exp_blog ? home_url('/') : home_url() . '/blog', $exp_blog ? 0 : 1),
            array('classified', $exp_classified ? home_url('/') : home_url() . '/classified', $exp_classified ? 0 : 1),
            array('artist', $exp_artist ? home_url('/') : home_url() . '/artist', $exp_artist ? 0 : 1),
            array('', home_url(), 1),
            array('test', home_url(), 1),
        );
    }

    /**
     * @dataProvider providerGetCustomUrlByModuleName
     * @param $module
     * @param $expectedResult
     * @param $exactly
     */
    public function testGetCustomUrlByModuleName($module, $expectedResult, $exactly){
        if($exactly) {
            $this->assertEquals($expectedResult, Apollo_App::getCustomUrlByModuleName($module));
        } else {
            $this->assertStringStartsWith($expectedResult, Apollo_App::getCustomUrlByModuleName($module));
        }
    }
    /*
    |--------------------------------------------------------------------------
    | End testing getCustomUrlByModuleName
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | Start test isUserOwnPost
    |--------------------------------------------------------------------------
    */
    public function providerIsUserOwnPost(){

        $data = [];
        $data_post = [
            array('venue', 'venue', 'publish', true, true), // post_type, post_type test, post_status, is author, expected result
            array('venue', 'educator', 'pending', true, false),
            array('venue', 'venue', 'pending', true, true),
            array('venue', 'venue', 'pending', false, false),
            array('agency', 'agency', 'draft', true, false),
            array('agency', 'venue', 'pending', true, false),
            array('artist', 'artist', 'pending', true, true),
            array('artist', 'artist', 'pending', false, false),
            array('educator', 'educator', 'trash', true, false),
        ];
        for($i= 0 ; $i< count($data_post); $i++) {
            $userId = $this->factory->user->create([
                'user_login' => 'User' . time() . $i . rand(0,1000),
                'user_email' => 'User' . time() . $i . rand(0,1000) . "@example.org"
            ]);
            $postId = $this->factory->post->create([
                'post_author' => $userId,
                'post_type' => $data_post[$i][0],
                'post_status' => $data_post[$i][2],
            ]);
            $data[] = [$data_post[$i][3] ? $userId : ($userId + 1), $postId, $data_post[$i][1], $data_post[$i][4]];
        }
        return $data;
    }

    /**
     * @dataProvider providerIsUserOwnPost
     * @param $userId
     * @param $postId
     * @param $postType
     * @param $expectedResult
     */
    public function testIsUserOwnPost($userId, $postId, $postType, $expectedResult){
        //set login for user
        wp_set_current_user($userId);
        $result = Apollo_App::isUserOwnPost($postId, $postType);

        if($expectedResult) {
            $this->assertTrue($result);
        } else {
            $this->assertFalse($result);
        }
    }
    /*
    |--------------------------------------------------------------------------
    | End testing isUserOwnPost
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | Start test isFullTemplatePage
    |--------------------------------------------------------------------------
    */
    public function providerIsFullTemplatePage()
    {
        $data = [
            ['template-full-width.php', false, true],
            ['template-full-width.php', true, false],
            ['template-full-width-test.php', false, false],
            ['template-full-width-test.php', true, false]
        ];

        return $data;
    }

    /**
     * @dataProvider providerIsFullTemplatePage
     * @param $pageTemplate
     * @param $expectedResult
     */
    public function testIsFullTemplatePage($pageTemplate, $haveParam, $expectedResult){
        if($haveParam) {
            $_GET['s'] = 1;
        }
        $result = Apollo_App::isFullTemplatePage($pageTemplate);

        if($expectedResult) {
            $this->assertTrue($result);
        } else {
            $this->assertFalse($result);
        }
    }
    /*
    |--------------------------------------------------------------------------
    | End testing isFullTemplatePage
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | Start test my_cut_excerpt
    |--------------------------------------------------------------------------
    */
    public function providerMyCutExcerpt()
    {
        $data = [
            ['Unit Testing WordPress Plugins with PHPUnit', 10, '...', 'Unit Testi...'],
            ['Unit Testing WordPress Plugins with PHPUnit', false, '...', '...'],
            ['Unit Testing WordPress Plugins with PHPUnit', 0, '...', '...'],
            //['Unit Testing WordPress Plugins with PHPUnit', -5, '...', 'Unit Testing WordPress Plugins with PH...'],
            ['Việt nam', 5, '...', 'Việt ...'],
            ['Việt nam', 3, '...', 'Việ...'],
            ['Việt nam', 4, '...', 'Việt...'],
            ['Việt \n nam', 40, '...', 'Việt \n nam'],
            ['This&nbsp;is&nbsp;a&nbsp;string', 7, '...', 'This is...'],
            ['This&nbsp;is&nbsp;a&nbsp;string', 100, '...', 'This is a string'],
            ['This&nbsp;is&nbsp;a&nbsp;string', 20, '...', 'This is a string'],
            ['This&nbsp;is&nbsp;a&nbsp;string', 10, '...', 'This is a ...'],
            ['This&nbsp;is&nbsp;a&nbsp;string', 12, '...', 'This is a st...'],
        ];

        return $data;
    }

    /**
     * @dataProvider providerMyCutExcerpt
     * @param $pageTemplate
     * @param $expectedResult
     */
    public function testMyCutExcerpt($str, $character, $more, $expectedResult){
        $result = Apollo_App::my_cut_excerpt($str, $character, $more);
        $this->assertEquals($expectedResult, $result);
    }
    /*
    |--------------------------------------------------------------------------
    | End testing my_cut_excerpt
    |--------------------------------------------------------------------------
    */



    /*
    |--------------------------------------------------------------------------
    | Start test getListTerritory
    |--------------------------------------------------------------------------
    */
    public function testGetListTerritory(){
        $aplQuery = new Apl_Query('wp_apollo_state_zip');

        $aplQuery->insert([
            'zipcode' => 'VN001',
            'zipsel' => 'N',
            'city' => 'Ho Chi Minh',
            'citysel' => 'N',
            'state_prefix' => 'VN',
            'state_name' => 'Viet Nam',
        ]);
        $arrKey = ['VN'];
        $result = Apollo_App::getListTerritory($arrKey);
        $this->assertCount(1, $result);

        //----
        $aplQuery->insert([
            'zipcode' => 'VN002',
            'zipsel' => 'Y',
            'city' => 'Ha Noi',
            'citysel' => 'N',
            'state_prefix' => 'VN',
            'state_name' => 'Viet Nam',
        ]);
        $arrKey = ['VN'];
        $result = Apollo_App::getListTerritory($arrKey);
        $this->assertCount(1, $result);

        //----
        $aplQuery->insert([
            'zipcode' => 'CN001',
            'zipsel' => 'Y',
            'city' => 'city',
            'citysel' => 'N',
            'state_prefix' => 'CN',
            'state_name' => 'China',
        ]);
        $arrKey = ['VN', 'CN'];
        $result = Apollo_App::getListTerritory($arrKey);
        $this->assertCount(2, $result);

        $arrKey = ['VN_CN'];
        $result = Apollo_App::getListTerritory($arrKey);
        $this->assertEmpty($result);
    }
    /*
    |--------------------------------------------------------------------------
    | End testing getListTerritory
    |--------------------------------------------------------------------------
    */



    /*
     |--------------------------------------------------------------------------
     | Start test getStateByTerritory
     |--------------------------------------------------------------------------
     */
    public function providerGetStateByTerritory()
    {
        return [
            array(3, false, false, 3),//no required, no has label
            array(1, false, true, 2),//no required, has label
            array(4, true, false, 4),//has required, no label
            array(4, true, true, 5),//has required, has label
        ];
    }

    public function randomString(){
        return base64_encode(random_bytes(10));
    }

    /**
     * @dataProvider providerGetStateByTerritory
     * @param $number_state
     * @param $required
     * @param $hasLabel
     * @param $expectedResult
     */
    public function testGetStateByTerritory($number_state, $required, $hasLabel, $expectedResult){
        $oldThemeOpt = get_option('_apollo_theme_options');
        $oldThemeOpt[Apollo_DB_Schema::_TERR_STATES] = [];
        for($i = 0; $i < $number_state; $i++) {
            $oldThemeOpt[Apollo_DB_Schema::_TERR_STATES][] = [$this->randomString() => 1];
        }
        update_option('_apollo_theme_options', $oldThemeOpt);

        $result = Apollo_App::getStateByTerritory($required, $hasLabel);
        $this->assertInternalType('array', $result);
        $this->assertCount($expectedResult, $result);

        if($hasLabel) {
            $this->assertContains("Select State", $result[0]);
            if ($required) {
                $this->assertContains("(*)", $result[0]);
            }
        }
    }
    /*
    |--------------------------------------------------------------------------
    | End testing getStateByTerritory
    |--------------------------------------------------------------------------
    */



    /*
    |--------------------------------------------------------------------------
    | Start test getCityByTerritory
    |--------------------------------------------------------------------------
    */
    public function providerGetCityByTerritory()
    {
        $state1 = 'state-1';
        $state2 = 'state-2';
        $state3 = 'state-3';

        $oldThemeOpt = get_option('_apollo_theme_options');

        $oldThemeOpt[Apollo_DB_Schema::_TERR_STATES] = [
            $state1 => 1,
            $state2 => 1,
        ];
        $oldThemeOpt[Apollo_DB_Schema::_TERR_DATA] = [
            $state1 => [
                'city-1' => 'city-1 label',
                'city-2' => 'city-2 label',
                'city-3' => 'city-3 label',
            ],
            $state2 => [
                'city-4' => 'city-4 label',
                'city-5' => 'city-5 label',
                'city-6' => 'city-6 label',
            ]
        ];
        update_option('_apollo_theme_options', $oldThemeOpt);
        return [
            array(false, $state1, false, '', 3),//no required, no has label
            array(false, $state1, true, '', 4),//no required, has label
            array(false, $state3, false, '', 6),//state invalid, no label
            array(false, $state3, true, '', 7),//state invalid, has label
            array(true, $state1, true, '', 4),//has required, has label
            array(false, $state1, false, '', 3),//no required, no label
            array(true, $state1, false, '', 3),//has required, has label
            array(true, $state1, true, 'first label', 4),//has required, has label, has first label
            array(true, $state1, false, 'first 2 label', 3),//has required, no label, has first label
        ];
    }

    /**
     * @dataProvider providerGetCityByTerritory
     * @param $required
     * @param $state
     * @param $hasLabel
     * @param $firstLabel
     * @param $expectedResult
     */
    public function testGetCityByTerritory($required, $state, $hasLabel, $firstLabel, $expectedResult){
        $result = Apollo_App::getCityByTerritory($required, $state, $hasLabel, $firstLabel);
        $this->assertInternalType('array', $result);
        $this->assertCount($expectedResult, $result);
        if($hasLabel) {
            if(!$firstLabel) {
                $this->assertContains("Select City", $result[0]);
            } else {
                $this->assertContains($firstLabel, $result[0]);
            }
        }
        if($hasLabel && $required) {
            if($firstLabel) {
                $this->assertContains($firstLabel, $result[0]);
            } else {
                $this->assertContains("Select City (*)", $result[0]);
            }
        }
    }
    /*
    |--------------------------------------------------------------------------
    | End testing getCityByTerritory
    |--------------------------------------------------------------------------
    */
}