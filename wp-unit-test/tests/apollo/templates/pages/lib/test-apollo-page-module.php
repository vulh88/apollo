<?php

class Apollo_Page_Module_Test extends WP_UnitTestCase {

    public function setUp() {
        // This code will run before each test
        parent::setUp();
        $_SERVER['QUERY_STRING'] = '';
    }
    public function tearDown() {
        // This code will run after each test
        //parent::tearDown();
    }

    static function setUpBeforeClass(){

    }

    static function tearDownAfterClass(){
        //parent::tearDownAfterClass();
    }


    /*
    |--------------------------------------------------------------------------
    | Start test is_onepage
    |--------------------------------------------------------------------------
    */
    public function providerTestIsOnePage(){
        return array(
            array('1', true),
            array(1, false),
            array(0, false),
            array(-1, false),
        );
    }

    /**
     * @dataProvider providerTestIsOnePage
     * @param $value
     * @param $expectedResult
     */
    public function testIsOnePage($value, $expectedResult){
        if($value >= 0) {
            $_REQUEST['onepage'] = $value;
        }
        $apollo_page_module = new Apollo_Page_Module();

        $result = $apollo_page_module->is_onepage();
        if($expectedResult) {
            $this->assertTrue($result);
        } else {
            $this->assertFalse($result);
        }
    }
    /*
    |--------------------------------------------------------------------------
    | End testing is_onepage
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | Start test have_more_on_viewmore
    |--------------------------------------------------------------------------
    */
    static function getPrivate($object, $property) {
        $reflector = new ReflectionProperty(get_class($object), $property);
        $reflector->setAccessible(true);
        return $reflector->getValue($object);
    }

    public function providerTestHaveMoreOnViewMore(){
        $_SERVER['QUERY_STRING'] = '';
        $apollo_page_module = new Apollo_Event_Page();
        $numItemFirstPage = of_get_option(Apollo_DB_Schema::_APL_NUMBER_ITEMS_FIRST_PAGE, Apollo_Display_Config::APL_DEFAULT_NUMBER_ITEMS_FIRST_PAGE);
        $numViewMore = of_get_option(Apollo_DB_Schema::_APL_NUMBER_ITEMS_VIEW_MORE, Apollo_Display_Config::APL_DEFAULT_NUMBER_ITEMS_VIEW_MORE);

        $dataProvider = [
            array(1, ''),
            array(10, ''),
            array(0, ''),
            array(3, ''),
            array(-1, ''),
        ];
        for($i = 0; $i < count($dataProvider); $i++) {
            $_GET['page'] = $dataProvider[$i][0];
            $apollo_page_module->search();
            $total = self::getPrivate($apollo_page_module, 'total');
            $page = self::getPrivate($apollo_page_module, 'page');
            $dataProvider[$i][1] = ($numItemFirstPage + ($numViewMore*($page - 1))) < intval($total);
        }
        return $dataProvider;
    }

    /**
     * @dataProvider providerTestHaveMoreOnViewMore
     * @param $page
     * @param $expectedResult
     */
    public function testHaveMoreOnViewMore($page, $expectedResult){
        $apollo_page_module = new Apollo_Event_Page();
        $_GET['page'] = $page;
        $apollo_page_module->search();
        $have_more = $apollo_page_module->have_more_on_viewmore();
        $this->assertEquals($expectedResult, $have_more);
    }
    /*
    |--------------------------------------------------------------------------
    | End testing have_more_on_viewmore
    |--------------------------------------------------------------------------
    */
}