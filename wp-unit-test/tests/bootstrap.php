<?php
/**
 * PHPUnit bootstrap file
 *
 * @package Hieu
 */

ob_start();

$_tests_dir = dirname( dirname( __FILE__ ) ) . '/wordpress-tests-lib';

if ( ! $_tests_dir ) {
	$_tests_dir = rtrim( sys_get_temp_dir(), '/\\' ) . '/wordpress-tests-lib';
}

if ( ! file_exists( $_tests_dir . '/includes/functions.php' ) ) {
	throw new Exception( "Could not find $_tests_dir/includes/functions.php, have you run bin/install-wp-tests.sh ?" );
}

require dirname( dirname( __FILE__ ) ) . '/wp-unit-test.php';

// Give access to tests_add_filter() function.
require_once $_tests_dir . '/includes/functions.php';

/**
 * Manually load the plugin being tested.
 */
function _manually_load_plugin() {
//    // Add your theme …
//    switch_theme('apollo');
//
//    // Update array with plugins to include ...
//    $plugins_to_active = array(
//        'wp-unitest/wp-unitest.php'
//    );
//
//    update_option( 'active_plugins', $plugins_to_active );

}
tests_add_filter( 'muplugins_loaded', '_manually_load_plugin' );

// Start up the WP testing environment.
require $_tests_dir . '/includes/bootstrap.php';

require_once dirname(dirname($_tests_dir)) . '/wp-includes/ms-blogs.php';
require_once dirname(dirname($_tests_dir)) . '/wp-includes/class-wp-site.php';
require_once dirname(dirname($_tests_dir)) . '/wp-content/themes/apollo/inc/src/event/inc/class-event-search.php';
require_once dirname(dirname($_tests_dir)) . '/wp-content/themes/apollo/inc/scripts/class-apollo-utility-tool.php';
