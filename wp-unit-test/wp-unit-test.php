<?php
//echo "123";
//echo WP_DEBUG;
//print_r(wp_get_theme());
//die;

/*
Plugin Name: Hieu
Description: phpunit test
Version:     1.0
Author:      Hieu Luong
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Domain Path: /languages
*/

if ( !defined('WP_UNIT_TEST_DIR') ) {
    define('WP_UNIT_TEST_DIR', dirname( __FILE__ ));
}