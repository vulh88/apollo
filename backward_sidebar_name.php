<?php
define('DOING_AJAX', true);
define('WP_USE_THEMES', false);
//define('SHORTINIT', true);
$_SERVER = array(
    "HTTP_HOST" => "WPCLI",
    "SERVER_NAME" => "WPCLI",
    "REQUEST_URI" => "/",
    "REQUEST_METHOD" => "GET"
);

/* Fix error internal in wordpress */
global $current_blog, $current_site;

$current_blog = new stdClass();
$current_blog->blog_id = 'WPCLI';
$current_blog->site_id = 'WPCLI';

$current_site = new stdClass();
$current_site->domain = 'WPCLI';
$current_site->blog_id = 'WPCLI';
$current_site->path = 'WPCLI';

require __DIR__.'/wp-load.php';


class Apollo_Command_Util {
    public static function getAllPublicBlog()
    {
        global $wpdb;
        $sql = "select blog_id from $wpdb->blogs where public = '1'";

        $data = $wpdb->get_results($sql);

        return $data;
    }
}


global $wpdb;
$arr_blog = Apollo_Command_Util::getAllPublicBlog();

if(empty($arr_blog)) die('empty');

$old_sidebar_prefix = 'sidebar';
$new_sidebar_prefix = Apollo_DB_Schema::_SIDEBAR_PREFIX;

if($old_sidebar_prefix === $new_sidebar_prefix) die('yes');

foreach($arr_blog as $obj_info) {
    $tbl_o = $wpdb->base_prefix . $obj_info->blog_id . '_options';
    if($obj_info->blog_id === '1') {
        $tbl_o = $wpdb->base_prefix . 'options';
    }

    $oname = 'sidebars_widgets';
    $sql = 'select * from '. $tbl_o .' where option_name = "'. $oname .'" limit 1;';
    $arr_results = $wpdb->get_results($sql);

    if(empty($arr_results)) continue;

    $svalues = $arr_results[0]->option_value;

    $current_sidebar_widgets = maybe_unserialize($svalues);

    if(empty($current_sidebar_widgets)) {
        continue;
    }


    $arrNeedCopy = array();
    $listNameNeedRemove = array();
    foreach($current_sidebar_widgets as $sidebar_name => $arrValue) {
        if(!is_array($arrValue)) continue;
        if(strpos($sidebar_name, $old_sidebar_prefix ) !== 0) continue;

        // Convert name
        $_sidebar_name = substr_replace($sidebar_name, $new_sidebar_prefix, 0, strlen($old_sidebar_prefix));
        $arrNeedCopy[$_sidebar_name] = $arrValue;
        $listNameNeedRemove[] = $sidebar_name;
    }

    if(empty($arrNeedCopy)) continue;

    //BACKUP BEFORE DO ANYTHING
    $sql = 'insert into '. $tbl_o .'(option_name, option_value, autoload) value (%s, %s, "no") ;';
    $wpdb->query($wpdb->prepare($sql, array(
                '_bk_sidebars_widgets',
                maybe_serialize($current_sidebar_widgets),
            )));

    // REMOVE
    foreach($listNameNeedRemove as $sidebar_name) {
        unset($current_sidebar_widgets[$sidebar_name]);
    }

    // MERGE
    $current_sidebar_widgets = array_merge($current_sidebar_widgets, $arrNeedCopy);


    $sql = 'update '. $tbl_o .' set option_value = "%s" where option_name = "'. $oname .'" ;';
    $result = $wpdb->query($wpdb->prepare($sql, array(
                maybe_serialize($current_sidebar_widgets),
            )));

    if($result === false || $result === null) {
        break;
    }
}

