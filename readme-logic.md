# Table of Contents

* [System workflow](#system-workflow)
* [General](General)
    * [Post statuses](#post-statuses)
    * [Image rule](image-rule)
* [Network site settings](#network-site-settings)
* [User settings](#user-settings)
* [Home page](#home-page)
* [Event](#event)
    * [Dashboard form](#dashboard-form)
    * [Admin form](admin-form)
    * [Admin display management](admin-display-management)
    * [Category page](#category-page)
    * [Listing page](#listing-page)
    * [Detail page](#detail-page)
    * [Print page](#print-page)
* [Additional features](#additional-features)    
    * [Report](#report)
    * [Syndication](#syndication)
    * [Event import tool](#event-import-tool)
    * [SOLR search](#solr-search)
    * [Ticketing](#ticketing)
    * [Copy to mini sites](copy-to-mini-sites)
    * [Offer date time widget](#offer-date-time-widget)
    * [Additional fields](#additional-fields)


# System workflow

@todo


# General

### Post Statuses

* draft
* pending (be default when creating in FE)
* approval (allow to show in list or detail pages FE)

### Image rule

* Image types
    * Thumbnail
    * Medium - featured images
    * Large - sportlight images
    * Large Medium (768 x height, Wordpress Default)
    * 100x100 for the recent post widget (fixed in the code)
    * 150x150 for the print event listing page (fixed in the code)
* Uploaded image should follow proportion
    * 4:3 (450*3/4 ~ 340 - use 350)
    * 16:9 (1024*9/16 ~ 580 - use 700)
* Naming uploaded image
    * “{post-type}-featured-{post-title}-{timestampt}”
    * “{post-type}-gallery-1-{post-title}-{timestampt}”
    * image for event don’t need {post-type}
    * image for artist - {post-title} will be their username


# Network site settings

* **Network admin > Edit a site > Settings tab**
* Enable/disable any module (Event, Artist, Blog etc)

##### Enable customization States/Cities

* Can manage states/ cities/ zips in each site. If this config is disabled, this site will use common state/city/zip data
* **Site admin > Settings > States Cities**
* To use states/cities/zips on a form or a widget they should be selected via **Theme Options > Territory**


##### Enable Event Import Tool

* Allow user can import multiples events from the user dashboard


##### Enable Apollo Cache

* File caching, transient cache

##### Enable Apollo Solr Search

* Allow runtime searching for events, venues, orgs etc
* Syn data manually via **Theme Options > Site config > Search**
* When this config is enabled, a SOLR instance will be created for storing data for searching if it does not exist yet

##### Enable Apollo Photo Slider ShortCode

* For Sonoma site

##### Enable Apollo iFrame Search Widget

* Generate an event Iframe search widget

##### Enable Reports Module

* Storing number of clicking of Official Website, Buy Ticket, Discounts buttons on event detail page
* Using MongoDB
* When this config is enabled, system creates a database for the current site if it does not exist yet
* Admin page: **wp-admin/admin.php?page=apollo-report**

##### Disabled Plugins

* Disallow plugins for the current site


# User settings

#### User Association tab

* Assign an artist, educator, ORG, venue for a logged in user


#### Apollo Config tab

*  Enable/Disable Import Event Tool:
    * Allow enable/disable Event Import tool from the user dashboard for the current user.
    * This config is only available when the parent config (**Theme Options > Site Config > Event > FE Events User Dashboard > Enable Import** tab for all users) is “NO”,
     meaning this site opens this feature for some specific users. By contrast, ALL users can use the Import tool

* Bypass Pending Approval
    * Currently only apply for event module
    * Enable/Disable bypass pending approve for a user from the EVENT dashboard form and EVENT Import Tool
    * This config is only available when the parent config **Theme Options > Site config > Event:  > Enable Bypass Approval** is “YES”
    and Bypass Approval Method is “Individual users only"


# Home page

Feature name | Cache Type | Notes
-------- | -------- | -------
Default Spotlight | Apollo file cache | This cache will expire in 2 days. When a user add/edit/delete a spotlight post, he/she need to clean the Apollo Cache to pull new data
Event Spotlight | Apollo file cache | The expired time is the latest end date of an event plus one day
Top Ten | Apollo file cache | - Cache is cleared every 8 hours by a cron job <br/>- 10 events is pulled from the **apollo_top_ten** table, priority popular events (count)<br> - Remove all events in the TOP TEN TABLE are not visited from the past 24-48 hours, this is configed from admin site *Theme Options > Home Content > How many hours that events will be expired in the Top ten tab?*
Fearture events | Apollo file cache | - Cache is cleared at the latest expired time of an event plus one day
Feature articles (blog) |  | - Can control position (top, bottom)

* Default spotlight rules:
    * If content manager ignores date fields and does NOT select any dates then slide should display immediately and NOT expire (as it does currently).
    * If content manager selects a start date, but NOT an end date, then slide should become active on the start date but never expire.
    * If content manager does NOT select a start date, but DOES select an end date then slide should become active immediately and then expire on the end date.


# Event

### Dashboard form

#### Send Email

* When user submit an event, send an email to the owner and administrator to alert he/she has just created a new event.
* There are 3 configs handle this sending email logic:
    * (1) Theme Options > Site config > Event > General > Enable Bypass Approve
    * (2) Theme Options > Site config > Event > General > Bypass Approval Method
    * (3) User profile > Bypass Pending Approval

 &nbsp; | Confirm created new post to admin | Confirm created new post to admin owner | Confirmed approved email to the owner
-------- | -------- | -------- | -------
(1) no  | Immediately | Immediately | After Admin approved
(1): yes <br> (2): Apply to all users  | Immediately | Immediately | Immediately
(1): yes <br> (2): Individual users only <br/> (3): yes  | Immediately | Immediately | Immediately
1): yes <br>(2): Individual users only <br> (3): no  | Immediately | Immediately | After Admin approved

* When user edit an event, sending the email to admin to alert that user has edit this event.

#### Calendar

* Original rule http://redmine.elidev.info/issues/3452
* http://redmine.elidev.info/issues/3726


### Admin form

* In the admin site, we allow to update an expired event and do not alert that this event is expired
* Preferred checkbox: When selected the event should appear at the very top/beginning of all event search results. However, it should only appear at the top of any results that it would normally be included in (#9675)
* Private checkbox: When selected the event does NOT display anywhere on the front-end. However, the approved event is still available to the CONTENT SYNDICATION feeds (#8749)


### Admin display management

* Data is stored in the wp_{blog_id}_apollo_post_term
* Events will not display for the Home page filter option until they are selected in the category filter
* When an event is un-checked from category filter, it is still display on home page filter for handling whether display or not on the front-end

### Category page:

* URL: /categories/{category_name}
* Display a slider (Event Spotlight) and events of an category, is manuall selected from Display Management tool **from wp-admin/edit.php?post_type=event&dm=true**
* Settings page: Theme Options > Site configs > Event > Category Page
* If a category is set as a Theme Tool **wp-admin/edit.php?post_type=event&page=event_themes**, its logic will be override by new conditions unless the “Leave as normal” option is not selected from a Theme Tool setting page.
* If admin does not select manually any event for spotlight, featured events yet, system will fill up automatically appropriate events for them
* Each event is only displayed one time in a category page, e.g. an event is selected for both spotlight and feature location, it must be displayed on one of them, not for both


Feature name | Cache Type | Notes
-------- | -------- | -------
Top Spotlight (slider) |  | - Display selected spotlights for current category
Spotlight (single event) | Transient cache | - 2 types (image on the left, image on the left) <br> - Selected from the Display management page or filled up automatically <br> Time-put at the latest end date of an event
Feature events (listing) | Transient cache | - Selected from the Display management page or filled up automatically <br> Time-put at the latest end date of an event
Other events (listing) |  | - Remaining events


### Landing page

Feature name | Cache Type | Notes
-------- | -------- | -------
Spotlight (slider) |  | Display all event spotlights not selected for any category
Events listing |  |


### Detail page:

* Basic info
* Individual date times or Date/time widget
* The “What near by” map is enabled from Theme Option > Site config > Business
* Venue map is suppressed when the “What near by” map is activated

### Print pages:

* Print landing page (/event/?print_event_search_result=1)
* Print an event


# Additional Features:

@Todo

## Report

@Todo

## Syndication

@Todo

## Event import tool

@Todo

## SOLR search

@Todo

## Ticketing

@Todo

## Copy to mini sites

@Todo

## Offer date time widget

@Todo

## Icon

@Todo

## Additional fields

Applying to Artist, Educator, Program, Evaluation, Grant Education, Organization, Venue, Classified, Public Art, Business

#### Event 

@Ticket #14075
There is a specific logic for the event module is "Alternate value" field for the choice fields (select, multiple). 
This field will not be displayed in the Syndication XML feed unless this field is not empty or has at least mapped value with the parent value field.    

