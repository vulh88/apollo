<?php

/**
 * @ticket #19271: Include report data, list all presenting orgs
 * @param $post
 * @return array
 */
function includeReportData($post){

        require_once APOLLO_INCLUDES_VERDOR . '/autoload.php'; // include Composer's autoloader
        // set up connection
        $host = defined('MONGODB_HOST') ? MONGODB_HOST : '127.0.0.1';
        $port = defined('MONGODB_PORT') ? MONGODB_PORT : 27017;
        $connection = sprintf('mongodb://%s:%d', $host, $port);

        /**
         * MongoDB does not enable access control by default.
         * So, if you are not configure username and password, just leave it empty.
         * Ref: https://docs.mongodb.com/v3.4/core/authorization/#enable-access-control
         */
        $uriOptions = array();
        $username = defined('MONGODB_USERNAME') ? MONGODB_USERNAME : '';
        $password = defined('MONGODB_PASSWORD') ? MONGODB_PASSWORD : '';
        if (!empty($username) && !empty($password)) {
            $uriOptions = array(
                'username' => $username,
                'password' => $password,
            );
        }

        try {
            $blogInfo = get_blog_details();
            $domain = str_replace(['.', '-'], '_', $blogInfo->domain);

            // db name pattern: apollo_mongo_report_{BlogID}_{BlogDomain}
            $dbName = 'apollo_mongo_report_' . get_current_blog_id() . '_' . $domain;
            $collection = (new MongoDB\Client($connection, $uriOptions))->{$dbName}->{'eventReport'};
        } catch (Exception $ex) {
            $connectionErrorMessage = $ex->getMessage();
        }

        $event = $collection->findOne(array('item_id' => intval($post->ID)));

        return array(
            'official_web_clicks' => isset($event->official_web_clicks) ? $event->official_web_clicks : '',
            'buy_ticket_clicks' => isset($event->buy_ticket_clicks) ? $event->buy_ticket_clicks : '',
            'discount_ticket_clicks' => isset($event->discount_ticket_clicks) ? $event->discount_ticket_clicks : ''
        );
}

function termList($post_id,$taxonomy)
{
$terms = get_the_terms( $post_id, $taxonomy );

if ( $terms && ! is_wp_error( $terms ) ){

    $term_list = array();

    foreach ( $terms as $term ) {

        $term_list[] = $term->name;
    }

    $term_list = implode( ", ", $term_list );

return str_replace('&amp;', '&', $term_list);
}
else{
return '';
}
}
/////////////////////////////////////////////////////////
// This code powers the CSV download of post_types
// selected in the CSV export admin panel.
/////////////////////////////////////////////////////////

require_once('wp-load.php');

$post_id = $_GET['apid'];

$csv_stored_meta = get_post_meta($post_id);

$csv_type = $csv_stored_meta['csv_type'][0];
$csv_filter = (isset($csv_stored_meta['csv_filter']) && isset($csv_stored_meta['csv_filter'][0])) ? $csv_stored_meta['csv_filter'][0] : '';
$csv_drange = (isset($csv_stored_meta['csv_drange']) && isset($csv_stored_meta['csv_drange'][0])) ? $csv_stored_meta['csv_drange'][0] : '';
$csv_dateStart = $csv_stored_meta['csv_dateStart'][0];
$csv_dateEnd = $csv_stored_meta['csv_dateEnd'][0];
$incind = isset($csv_stored_meta['csv_incind']) ? $csv_stored_meta['csv_incind'][0] : '';
$csv_report = $csv_stored_meta['csv_report'][0] && Apollo_App::get_network_enable_reports( get_current_blog_id());
$term_primary = (isset($csv_stored_meta['term_primary']) && isset($csv_stored_meta['term_primary'][0])) ? $csv_stored_meta['term_primary'][0] : '';
$term_primary_organization = (isset($csv_stored_meta['term_primary_organization']) && isset($csv_stored_meta['term_primary_organization'][0])) ? $csv_stored_meta['term_primary_organization'][0] : '';
$add_cat = (isset($csv_stored_meta['add_cat']) && isset($csv_stored_meta['add_cat'][0])) ? $csv_stored_meta['add_cat'][0] : '';
$add_cat_organization = (isset($csv_stored_meta['add_cat_organization']) && isset($csv_stored_meta['add_cat_organization'][0])) ? $csv_stored_meta['add_cat_organization'][0] : '';
$add_cat_venue = (isset($csv_stored_meta['add_cat_venue']) && isset($csv_stored_meta['add_cat_venue'][0])) ? $csv_stored_meta['add_cat_venue'][0] : '';
$add_cat_artist = (isset($csv_stored_meta['add_cat_artist']) && isset($csv_stored_meta['add_cat_artist'][0])) ? $csv_stored_meta['add_cat_artist'][0] : '';
$add_cat_business = (isset($csv_stored_meta['add_cat_business']) && isset($csv_stored_meta['add_cat_business'][0])) ? $csv_stored_meta['add_cat_business'][0] : '';
$add_cat_public_art = (isset($csv_stored_meta['add_cat_public_art']) && isset($csv_stored_meta['add_cat_public_art'][0])) ? $csv_stored_meta['add_cat_public_art'][0] : '';
$add_location_public_art = (isset($csv_stored_meta['add_location_public_art']) && isset($csv_stored_meta['add_location_public_art'][0])) ? $csv_stored_meta['add_location_public_art'][0] : '';
$add_medium_public_art = (isset($csv_stored_meta['add_medium_public_art']) && isset($csv_stored_meta['add_medium_public_art'][0])) ? $csv_stored_meta['add_medium_public_art'][0] : '';
$add_collection_public_art = (isset($csv_stored_meta['add_collection_public_art']) && isset($csv_stored_meta['add_collection_public_art'][0])) ? $csv_stored_meta['add_collection_public_art'][0] : '';
$public_artist = (isset($csv_stored_meta['public_artist']) && isset($csv_stored_meta['public_artist'][0])) ? $csv_stored_meta['public_artist'][0] : '';

$add_cat_classified = (isset($csv_stored_meta['add_cat_classified']) && isset($csv_stored_meta['add_cat_classified'][0])) ? $csv_stored_meta['add_cat_classified'][0] : '';


/*@ticket #16640  Allow select multiple individual organizations, venues, cities, and zip codes with type is the event.*/
if (isset($csv_stored_meta['csv-export-event-meta-zip']) && isset($csv_stored_meta['csv-export-event-meta-zip'][0])) {
    if (is_serialized($csv_stored_meta['csv-export-event-meta-zip'][0])) {
        $eventMetaZip = maybe_unserialize($csv_stored_meta['csv-export-event-meta-zip'][0]);
    } else {
        $eventMetaZip = $csv_stored_meta['csv-export-event-meta-zip'][0];
    }
}

if (isset($csv_stored_meta['csv-export-event-meta-cities']) && isset($csv_stored_meta['csv-export-event-meta-cities'][0])) {
    if (is_serialized($csv_stored_meta['csv-export-event-meta-cities'][0])) {
        $eventMetaCities = maybe_unserialize($csv_stored_meta['csv-export-event-meta-cities'][0]);
    } else {
        $eventMetaCities = $csv_stored_meta['csv-export-event-meta-cities'][0];
    }
}

if (isset($csv_stored_meta['csv-export-event-meta-org']) && isset($csv_stored_meta['csv-export-event-meta-org'][0])) {
    if (is_serialized($csv_stored_meta['csv-export-event-meta-org'][0])) {
        $eventMetaOrg = maybe_unserialize($csv_stored_meta['csv-export-event-meta-org'][0]);
    } else {
        $eventMetaOrg = $csv_stored_meta['csv-export-event-meta-org'][0];
    }
}

if (isset($csv_stored_meta['csv-export-event-meta-venue']) && isset($csv_stored_meta['csv-export-event-meta-venue'][0])) {
    if (is_serialized($csv_stored_meta['csv-export-event-meta-venue'][0])) {
        $eventMetaVenue = maybe_unserialize($csv_stored_meta['csv-export-event-meta-venue'][0]);
    } else {
        $eventMetaVenue = $csv_stored_meta['csv-export-event-meta-venue'][0];
    }
}
/*end*/

if ($csv_dateEnd == '') $csv_dateEnd = '2037-01-01';

$date = date('m-d-Y');
$filename = $csv_type . '_' . $date . '.csv';

// Output headers.
header("Cache-Control: private");
header("Content-Type: application/stream");
header("Content-Disposition: attachment; filename=" . $filename);

$url = get_site_url();
$domain = substr($url, 7);

global $wpdb, $blog_id;;

$post_status = '';
$post_list = '';
$publish = isset($csv_stored_meta['csv_publish']) ? $csv_stored_meta['csv_publish'][0] : '';
$pending = isset($csv_stored_meta['csv_pending']) ? $csv_stored_meta['csv_pending'][0] : '';
$draft = isset($csv_stored_meta['csv_draft']) ? $csv_stored_meta['csv_draft'][0] : '';

if ($publish != '') $post_list .= "'publish',";
if ($pending != '') $post_list .= "'pending',";
if ($draft != '') $post_list .= "'draft',";

$post_list = substr($post_list, 0, -1);

if ($post_list != '') $post_status = " and post_status in ($post_list) ";

/////////////////////////////////////////
// type 'event' has additional date information
// that may be set in the admin - if so, then
// we need to switch SQL strings accordingly
/////////////////////////////////////////

$distinctPostStr = "SELECT DISTINCT wp.ID,wp.post_title,wp.post_content,wp.post_date, wp.post_modified, wp.post_author FROM $wpdb->posts wp ";
switch ($csv_type) {
    case 'event':

    $event_sql = "SELECT DISTINCT 	wp.ID,wp.post_title,wp.post_content,wp.post_date, wp.post_modified, wp.post_author from $wpdb->posts wp";

    $event_sql .= " LEFT JOIN $wpdb->apollo_eventmeta emv on emv.apollo_event_id = wp.ID AND emv.meta_key = '_apollo_event_venue'";

    $event_sql .= " LEFT JOIN $wpdb->apollo_venuemeta vm on vm.apollo_venue_id = emv.meta_value AND vm.meta_key = '_venue_zip'";

	$event_sql .= " LEFT JOIN $wpdb->apollo_eventmeta tv on tv.apollo_event_id = wp.ID and tv.meta_key ='_apl_event_tmp_venue'";

    $event_sql .= " LEFT JOIN $wpdb->apollo_venuemeta vmc on vmc.apollo_venue_id = emv.meta_value AND vmc.meta_key = '_venue_city'";

    //if (!empty($organization)) $event_sql .= " JOIN $wpdb->apollo_event_org eo ON eo.org_id = $organization AND wp.ID = eo.post_id";
    /*@ticket #16640  Allow select multiple individual organizations, venues, cities, and zip codes with type is the event.*/
    if (!empty($eventMetaOrg)) $event_sql .= " JOIN $wpdb->apollo_event_org eo ON wp.ID = eo.post_id";

    if ($csv_filter == 0) {
        if (($csv_drange == '') AND ($csv_dateStart == '')) {
            $event_sql .= " join $wpdb->apollo_eventmeta em2 on em2.apollo_event_id = wp.ID and em2.meta_key = '_apollo_event_end_date' and em2.meta_value >= date(now())";
        } else {
            if ($csv_drange != '') {
                $csv_dateStart = date('Y-m-d');
                $days = $csv_drange;
                $csv_dateEnd = date('Y-m-d', (time() + (86400 * $days)));
            }
            if ($incind == 'Y') {

                //$event_sql .= " join $wpdb->apollo_event_calendar on event_id = wp.ID and (date_event between '$csv_dateStart' and '$csv_dateEnd')";
                $event_sql .= " join $wpdb->apollo_eventmeta em2 on em2.apollo_event_id = wp.ID and em2.meta_key = '_apollo_event_end_date' and em2.meta_value >= '$csv_dateStart'";
                $event_sql .= " join $wpdb->apollo_eventmeta em3 on em3.apollo_event_id = wp.ID and em3.meta_key = '_apollo_event_start_date' and em3.meta_value <= '$csv_dateEnd'    ";
            } else {
                $event_sql .= " join $wpdb->apollo_eventmeta em2 on em2.apollo_event_id = wp.ID and em2.meta_key = '_apollo_event_end_date' and em2.meta_value >= '$csv_dateStart'";
                $event_sql .= " join $wpdb->apollo_eventmeta em3 on em3.apollo_event_id = wp.ID and em3.meta_key = '_apollo_event_start_date' and em3.meta_value <= '$csv_dateEnd'    ";
            }
        }
    }

    if (!empty($term_primary)) $event_sql .= " JOIN $wpdb->apollo_eventmeta emp on emp.apollo_event_id = wp.ID and emp.meta_key = '_apl_event_term_primary_id' AND emp.meta_value = $term_primary";

    if (!empty($add_cat)) $event_sql .= " JOIN $wpdb->term_relationships tr on wp.ID = tr.object_id AND tr.term_taxonomy_id in ($add_cat)";

    $event_sql .= " where 1+1";

    $event_sql .= " and post_type = '$csv_type' $post_status ";

    if ($csv_filter == 1) {

        //date posted
        if (($csv_drange == '') AND ($csv_dateStart == '')) {
            $event_sql .= " join $wpdb->apollo_eventmeta em2 on em2.apollo_event_id = wp.ID and em2.meta_key = '_apollo_event_end_date' and em2.meta_value >= date(now())";
        } else {

            $stop = '>=';
            $edop = '<=';
            if ($csv_drange != '') {
                $csv_dateStart = date('Y-m-d');
                $days = $csv_drange;
                $csv_dateEnd = date('Y-m-d', (time() - (86400 * $days)));
                $stop = '<=';
                $edop = '>=';
            }


            if ($incind == 'Y') {
                $event_sql .= " AND DATE(post_date) $stop '$csv_dateStart' AND DATE(post_date) $edop '$csv_dateEnd'";
            } else {
                $event_sql .= " AND DATE(post_date) $stop '$csv_dateStart' AND DATE(post_date) $edop '$csv_dateEnd'";
            }

        }

    }

    /*@ticket #16640  Allow select multiple individual organizations, venues, cities, and zip codes with type is the event.*/
    if (!empty($eventMetaZip)) {
        $eventMetaZipStr = implode("','", $eventMetaZip);
        $event_sql .= " AND (vm.meta_value in ('$eventMetaZipStr') OR(";
        $event_sql .= "tv.meta_value like '%" . implode("%' OR tv.meta_value like '%", $eventMetaZip) . "%'";
        $event_sql .= '))';
    }

    if (!empty($eventMetaCities)) {
        $eventMetaCitiesStr = implode("','", $eventMetaCities);
        $event_sql .= " AND (vmc.meta_value in ('$eventMetaCitiesStr') OR(";
        $event_sql .= "tv.meta_value like '%" . implode("%' OR tv.meta_value like '%", $eventMetaCities) . "%'";
        $event_sql .= '))';
    }

    if (!empty($eventMetaVenue)) {
        $eventMetaVenue = implode("','", $eventMetaVenue);
        $event_sql .= " AND emv.meta_value in ('$eventMetaVenue')";
    }

    if (!empty($eventMetaOrg)) {
        $eventMetaOrg = implode("','", $eventMetaOrg);
        $event_sql .= " AND eo.org_id in ('$eventMetaOrg')";
    }
    /*end */


//$event_sql .= " GROUP BY wp.ID";

//echo "$event_sql";
    $posts = $wpdb->get_results($event_sql);

        break;
    case 'organization':

	$event_sql = $distinctPostStr;

	if (!empty($add_cat_organization)) $event_sql .= " JOIN $wpdb->term_relationships tr on wp.ID = tr.object_id AND tr.term_taxonomy_id in ($add_cat_organization)";

	$event_sql .= " where post_type = '$csv_type' $post_status";
	//echo $event_sql;
	$posts = $wpdb->get_results($event_sql);


        break;
    case 'venue':
	$event_sql = $distinctPostStr;

	if (!empty($add_cat_venue)) $event_sql .= " JOIN $wpdb->term_relationships tr on wp.ID = tr.object_id AND tr.term_taxonomy_id in ($add_cat_venue)";

	$event_sql .= " where post_type = '$csv_type' $post_status";
	//echo $event_sql;
	$posts = $wpdb->get_results($event_sql);

        break;

    case 'artist':
	$event_sql = $distinctPostStr;

	if (!empty($add_cat_artist)) $event_sql .= " JOIN $wpdb->term_relationships tr on wp.ID = tr.object_id AND tr.term_taxonomy_id in ($add_cat_artist)";

	$event_sql .= " where post_type = '$csv_type' $post_status";
	//echo $event_sql;
	$posts = $wpdb->get_results($event_sql);

        break;

    case 'business':
	$event_sql = $distinctPostStr;

	if (!empty($add_cat_business)) $event_sql .= " JOIN $wpdb->term_relationships tr on wp.ID = tr.object_id AND tr.term_taxonomy_id in ($add_cat_business)";

	$event_sql .= " where post_type = '$csv_type' $post_status";
	//echo $event_sql;
	$posts = $wpdb->get_results($event_sql);

        break;

    case 'public-art':
	$event_sql = $distinctPostStr;

	if (!empty($public_artist))$event_sql .= " JOIN $wpdb->apollo_public_artmeta pm ON pm.meta_key = '_apl_public_art_artist' and pm.meta_value LIKE '%$public_artist%' and pm.apollo_public_art_id = wp.ID";


	if (!empty($add_cat_public_art)) $event_sql .= " JOIN $wpdb->term_relationships tr on wp.ID = tr.object_id AND tr.term_taxonomy_id in ($add_cat_public_art)";

	if (!empty($add_location_public_art)) $event_sql .= " JOIN $wpdb->term_relationships trl on wp.ID = trl.object_id AND trl.term_taxonomy_id in ($add_location_public_art)";

	if (!empty($add_medium_public_art)) $event_sql .= " JOIN $wpdb->term_relationships trm on wp.ID = trm.object_id AND trm.term_taxonomy_id in ($add_medium_public_art)";

	if (!empty($add_collection_public_art)) $event_sql .= " JOIN $wpdb->term_relationships trc on wp.ID = trc.object_id AND trc.term_taxonomy_id in ($add_collection_public_art)";



	$event_sql .= " where post_type = 'public-art' $post_status";
	//print_r($event_sql);
	$posts = $wpdb->get_results($event_sql);

        break;

    case 'classified':
	$event_sql = $distinctPostStr;

	if (!empty($add_cat_classified)) $event_sql .= " JOIN $wpdb->term_relationships tr on wp.ID = tr.object_id AND tr.term_taxonomy_id in ($add_cat_classified)";

	$event_sql .= " where post_type = '$csv_type' $post_status";
	//echo $event_sql;
	$posts = $wpdb->get_results($event_sql);

        break;

    case 'program':
	$posts = $wpdb->get_results($distinctPostStr . " where post_type = '$csv_type' $post_status");
        break;

    case 'educator':
	$posts = $wpdb->get_results($distinctPostStr ." where post_type = '$csv_type' $post_status");
        break;
}


// This USER section added as it does not conform with other custom post types - it is not a post type at all
// USERS are global and have to be handled in a different way to the custom posts by defining the blog associated with them first

if ($csv_type == 'user') {

    $posts = $wpdb->get_results("select * from wp_usermeta where meta_key = 'primary_blog' and meta_value = $blog_id;");

}


// Begin Loop.
$idx = 0;
$noposts = true;
foreach ($posts as $post) {

    if (isset($post->post_title ) && $post->post_title == 'Auto Draft') continue;

    $noposts = false;

    if (isset($post->ID)){
        $image_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
    }
    else{
        $image_url = '';
    }


// Get unique data from custom tables
/////////////////////////////////////
// Organization details
/////////////////////////////////////

    if ($csv_type == 'organization') {

        $org = $wpdb->get_results("select meta_key, meta_value from $wpdb->apollo_organizationmeta where meta_key ='_apl_org_data' and apollo_organization_id = " . $post->ID);
        $od = isset($org[0]) ? unserialize(unserialize($org[0]->meta_value)) : array();
        $org = $wpdb->get_results("select meta_key, meta_value from $wpdb->apollo_organizationmeta where meta_key ='_apl_org_address' and apollo_organization_id = " . $post->ID);
        $oa =  isset($org[0]) ? unserialize(unserialize($org[0]->meta_value)) : array();

        // Set up column header
        $csvt = array('id', 'title', 'content', 'org phone', 'org email', 'org fax', 'org address1', 'org address2', 'org city', 'org state', 'org zip', 'contact name', 'contact email', 'contact phone', 'website url', 'post status', 'date published', 'image url', 'userid', 'organization categories', 'donate');

        $csv[$idx][0] = $post->ID;

        $csv[$idx][1] = $post->post_title;
        $csv[$idx][2] = $post->post_content;
        $csv[$idx][1] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $csv[$idx][1]);
        $csv[$idx][2] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $csv[$idx][2]);
        $find = array('&rsquo;', '&#039;', ':', '�', '&nbsp;');
        $repl = array("'", "'", " ", '-', ' ');
        $csv[$idx][1] = str_ireplace($find, $repl, $csv[$idx][1]);
        $csv[$idx][2] = str_ireplace($find, $repl, $csv[$idx][2]);
        $csv[$idx][1] = strip_tags($csv[$idx][1]);
        $csv[$idx][2] = strip_tags($csv[$idx][2]);
        $csv[$idx][1] = html_entity_decode($csv[$idx][1], ENT_QUOTES, "UTF-8");
        $csv[$idx][2] = html_entity_decode($csv[$idx][2], ENT_QUOTES, "UTF-8");

        $csv[$idx][3] = isset($od['_org_phone']) ? $od['_org_phone'] : '';
        $csv[$idx][4] = isset($od['_org_email']) ? $od['_org_email'] : '';
        $csv[$idx][5] = isset($od['_org_fax']) ? $od['_org_fax'] : '';

        $csv[$idx][6] = isset($oa['_org_address1']) ? $oa['_org_address1'] : '';
        $csv[$idx][7] = isset($oa['_org_address2']) ? $oa['_org_address2'] : '';
        $csv[$idx][8] = isset($oa['_org_city']) ? $oa['_org_city'] : '';
        $csv[$idx][9] = isset($oa['_org_state']) ? $oa['_org_state'] : '';
        $csv[$idx][10] =isset($oa['_org_zip']) ? $oa['_org_zip'] : '';

        $csv[$idx][11] = isset($od['_org_contact_name']) ? $od['_org_contact_name'] : '';
        $csv[$idx][12] = isset($od['_org_contact_email']) ? $od['_org_contact_email'] : '';
        $csv[$idx][13] = isset($od['_org_contact_phone']) ? $od['_org_contact_phone'] : '';
        $csv[$idx][14] = isset($od['_org_website_url']) ? $od['_org_website_url'] : '';
        $csv[$idx][15] = $post->post_status;
        $csv[$idx][16] = substr($post->post_date, 0, 10);
        $csv[$idx][17] = $image_url;
        $csv[$idx][18] = $post->post_author;

        $classifiedcats = '';

        $terms = $wpdb->get_results("select * from $wpdb->term_relationships where object_id = $post->ID");

        foreach ($terms as $term) {
            $classifiedcats .= $term->term_taxonomy_id . ",";
        }

        $classifiedcats = substr($classifiedcats, 0, -1);

        $tnames = $wpdb->get_results("select * from $wpdb->terms where term_id in ($classifiedcats)");

        $classifiedcats = '';

        foreach ($tnames as $tname) {
            $classifiedcats .= $tname->name . ", ";

        }

        $classifiedcats = substr($classifiedcats, 0, -2);
        $classifiedcats = str_replace('&amp;', '&', $classifiedcats);

        $csv[$idx][19] = $classifiedcats;
        $csv[$idx][20] = isset($od['_org_donate_url']) ? $od['_org_donate_url'] : '';


    }


// Get unique data from custom tables
/////////////////////////////////////
// Venue details
/////////////////////////////////////

    if ($csv_type == 'venue') {

        $lat = '';
        $lng = '';

        $org = $wpdb->get_results("select meta_key, meta_value from $wpdb->apollo_venuemeta where meta_key ='_apl_venue_data' and apollo_venue_id = " . $post->ID);
        $vd = isset($org[0]) ? Apollo_App::unserialize($org[0]->meta_value) : array();
        $org = $wpdb->get_results("select meta_key, meta_value from $wpdb->apollo_venuemeta where meta_key ='_apl_venue_address' and apollo_venue_id = " . $post->ID);
        $va = isset($org[0]) ? Apollo_App::unserialize($org[0]->meta_value) : array();

        $latlng = $wpdb->get_row("SELECT ANY_VALUE(m1.meta_value) as '_venue_lat', ANY_VALUE(m2.meta_value) as '_venue_lng' from $wpdb->apollo_venuemeta m1	JOIN $wpdb->apollo_venuemeta m2 ON m2.apollo_venue_id = m1.apollo_venue_id AND m2.meta_key = '_venue_lng' WHERE m1.apollo_venue_id = " . $post->ID . " AND m1.meta_key = '_venue_lat' GROUP BY m1.apollo_venue_id");
        if (!empty($latlng)) {
            $lat = $latlng->_venue_lat;
            $lng = $latlng->_venue_lng;
        }

        // Set up column header
        $csvt = array('id', 'title', 'content', 'venue phone', 'venue email', 'venue fax', 'venue address1', 'venue address2', 'venue city', 'venue state', 'venue zip', 'latitude', 'longitude', 'website url', 'post status', 'date published', 'image url', 'userid', 'venue categories', 'parking', 'accessibility');

        $csv[$idx][0] = $post->ID;

        $csv[$idx][1] = $post->post_title;
        $csv[$idx][2] = $post->post_content;
        $csv[$idx][1] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $csv[$idx][1]);
        $csv[$idx][2] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $csv[$idx][2]);
        $find = array('&rsquo;', '&#039;', ':', '�', '&nbsp;');
        $repl = array("'", "'", " ", '-', ' ');
        $csv[$idx][1] = str_ireplace($find, $repl, $csv[$idx][1]);
        $csv[$idx][2] = str_ireplace($find, $repl, $csv[$idx][2]);
        $csv[$idx][1] = strip_tags($csv[$idx][1]);
        $csv[$idx][2] = strip_tags($csv[$idx][2]);
        $csv[$idx][1] = html_entity_decode($csv[$idx][1], ENT_QUOTES, "UTF-8");
        $csv[$idx][2] = html_entity_decode($csv[$idx][2], ENT_QUOTES, "UTF-8");

        $csv[$idx][3] = isset($vd['_venue_phone']) ? $vd['_venue_phone'] : '';
        $csv[$idx][4] = isset($vd['_venue_email']) ? $vd['_venue_email'] : '';
        $csv[$idx][5] = isset($vd['_venue_fax']) ? $vd['_venue_fax'] : '';

        $csv[$idx][6] = isset($va['_venue_address1']) ? $va['_venue_address1'] : '';
        $csv[$idx][7] = isset($va['_venue_address2']) ? $va['_venue_address2'] : '';
        $csv[$idx][8] = isset($va['_venue_city']) ? $va['_venue_city'] : '';
        $csv[$idx][9] = isset($va['_venue_state']) ? $va['_venue_state'] : '';
        $csv[$idx][10] = isset($va['_venue_zip']) ? $va['_venue_zip'] : '';

        $csv[$idx][11] = $lat;
        $csv[$idx][12] = $lng;

        $csv[$idx][13] = isset($vd['_venue_website_url']) ? $vd['_venue_website_url'] : '';
        $csv[$idx][14] = $post->post_status;
        $csv[$idx][15] = substr($post->post_date, 0, 10);
        $csv[$idx][16] = $image_url;
        $csv[$idx][17] = $post->post_author;

        $classifiedcats = '';

        $terms = $wpdb->get_results("select * from $wpdb->term_relationships where object_id = $post->ID");

        foreach ($terms as $term) {
            $classifiedcats .= $term->term_taxonomy_id . ",";
        }

        $classifiedcats = substr($classifiedcats, 0, -1);

        $tnames = $wpdb->get_results("select * from $wpdb->terms where term_id in ($classifiedcats)");

        $classifiedcats = '';

        foreach ($tnames as $tname) {
            $classifiedcats .= $tname->name . ", ";

        }

        $classifiedcats = substr($classifiedcats, 0, -2);
        $classifiedcats = str_replace('&amp;', '&', $classifiedcats);

        $csv[$idx][18] = $classifiedcats;

        /*@ticket #17449 [CF] 20180905 - [CSV Export] Add 'parking' and 'accessiblity' text fields to Venue CSV export file*/
        $csv[$idx][19] = isset($vd['_venue_parking_info']) ? $vd['_venue_parking_info'] : '';
        $csv[$idx][20] = isset($vd['_accessibility_info']) ? $vd['_accessibility_info'] : '';


    }

// Get unique data from custom tables
/////////////////////////////////////
// Artist details
/////////////////////////////////////

    if ($csv_type == 'artist') {

        $org = $wpdb->get_results("select meta_key, meta_value from $wpdb->apollo_artistmeta where meta_key ='_apl_artist_data' and apollo_artist_id = " . $post->ID);
        //$ad = unserialize(unserialize($org[0]->meta_value));
        $ad = Apollo_App::unserialize($org[0]->meta_value);

        $org = $wpdb->get_results("select meta_key, meta_value from $wpdb->apollo_artistmeta where meta_key ='_apl_artist_address' and apollo_artist_id = " . $post->ID);
        $aa = unserialize(unserialize($org[0]->meta_value));

		$artist_cats = termList($post->ID,'artist-type');
		$artist_style = termList($post->ID,'artist-style');
		$artist_medium = termList($post->ID,'artist-medium');

        // Set up column header for artist
        $csvt = array('id', 'title', 'content', 'artist phone', 'artist email', 'street address', 'artist city', 'artist state', 'artist zip', 'artist county', 'post status', 'date published', 'image url', 'userid', 'artist categories', 'artist mediums', 'artist styles');

        $csv[$idx][0] = $post->ID;

        $csv[$idx][1] = $post->post_title;
        $csv[$idx][2] = $post->post_content;
        $csv[$idx][1] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $csv[$idx][1]);
        $csv[$idx][2] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $csv[$idx][2]);
        $find = array('&rsquo;', '&#039;', ':', '�', '&nbsp;');
        $repl = array("'", "'", " ", '-', ' ');
        $csv[$idx][1] = str_ireplace($find, $repl, $csv[$idx][1]);
        $csv[$idx][2] = str_ireplace($find, $repl, $csv[$idx][2]);
        $csv[$idx][1] = strip_tags($csv[$idx][1]);
        $csv[$idx][2] = strip_tags($csv[$idx][2]);
        $csv[$idx][1] = html_entity_decode($csv[$idx][1], ENT_QUOTES, "UTF-8");
        $csv[$idx][2] = html_entity_decode($csv[$idx][2], ENT_QUOTES, "UTF-8");

        $csv[$idx][3] = $ad['_apl_artist_phone'];
        $csv[$idx][4] = $ad['_apl_artist_email'];

        $csv[$idx][5] = $aa['_apl_artist_street'];
        $csv[$idx][6] = $aa['_apl_artist_city'];
        $csv[$idx][7] = $aa['_apl_artist_state'];
        $csv[$idx][8] = $aa['_apl_artist_zip'];
        $csv[$idx][9] = $aa['_apl_artist_country'];
        $csv[$idx][10] = $post->post_status;
        $csv[$idx][11] = substr($post->post_date, 0, 10);
        $csv[$idx][12] = $image_url;
        $csv[$idx][13] = $post->post_author;

        $csv[$idx][14] = $artist_cats;
        $csv[$idx][15] = $artist_medium;
        $csv[$idx][16] = $artist_style;



    }


// Get unique data from custom tables
/////////////////////////////////////
// Event details
/////////////////////////////////////

    if ($csv_type == 'event') {

        $event = $wpdb->get_results("select meta_key, meta_value from $wpdb->apollo_eventmeta where meta_key ='_apollo_event_data' and apollo_event_id = " . $post->ID);
        $ed = maybe_unserialize(unserialize($event[0]->meta_value));

        $start_date = $wpdb->get_results("select meta_value from $wpdb->apollo_eventmeta where meta_key ='_apollo_event_start_date' and apollo_event_id = " . $post->ID);
        $end_date = $wpdb->get_results("select meta_value from $wpdb->apollo_eventmeta where meta_key ='_apollo_event_end_date' and apollo_event_id = " . $post->ID);
        $primary = $wpdb->get_results("select meta_value from $wpdb->apollo_eventmeta where meta_key ='_apl_event_term_primary_id' and apollo_event_id = " . $post->ID);
        $secondary = $wpdb->get_results("SELECT t.name FROM $wpdb->term_relationships tr JOIN $wpdb->terms t on t.term_id = tr.term_taxonomy_id JOIN $wpdb->term_taxonomy tt on tt.term_id = tr.term_taxonomy_id AND taxonomy = 'event-type' WHERE object_id = " . $post->ID);

        $start_date = $start_date[0]->meta_value;
        $end_date = $end_date[0]->meta_value;
        $primary = isset($primary[0]) ? $primary[0]->meta_value : array();
        //	$venue = (!empty($venue) && isset($venue[0])) ? $venue[0]->meta_value : array();

        $cat_array = array();
        foreach ($secondary as $sec) {
            //echo "<LI>";print_r($sec->name);
            $cat_array[] = $sec->name;
        }
        if (!empty($cat_array)) sort($cat_array);
        $secondary_cat = '';
        $secondary_cat = implode(",", $cat_array);

        ////////////////////////////////////
        // Get org data temp or registered
        ////////////////////////////////////

        /**
         * @ticket #19271: Include report data, list all presenting orgs
         */
        $org_name = $wpdb->get_results("SELECT post_title FROM $wpdb->apollo_event_org o JOIN $wpdb->posts p on p.ID = o.org_id WHERE post_id = $post->ID ORDER BY o.org_ordering", ARRAY_A);
        if (!empty($org_name)) {
            $org_array = array();
            foreach ($org_name as $org) {
                $org_array[] = $org['post_title'];
            }
            $orgname = implode(", ", $org_array);
        } else {
            $orgname = $wpdb->get_results("select meta_value from $wpdb->apollo_eventmeta where meta_key ='_apollo_event_tmp_org' and apollo_event_id = " . $post->ID);
            $orgname = isset($orgname[0]) ? $orgname[0]->meta_value : '';
        }

        ////////////////////////////////////
        // Get venue data temp or registered
        ////////////////////////////////////

        $venue = $wpdb->get_results("select meta_value from $wpdb->apollo_eventmeta where meta_key ='_apollo_event_venue' and apollo_event_id = " . $post->ID);
        $venue_id = $venue[0]->meta_value;

        ///////////////////////
        // registered org and venue work
        ///////////////////////

        if ($venue_id != 0) {
            $venueres = $wpdb->get_results("select meta_value from $wpdb->apollo_venuemeta where meta_key ='_apl_venue_address' and apollo_venue_id = " . $venue_id);
            if (!empty($venueres)) {
                $venue = maybe_unserialize(unserialize($venueres[0]->meta_value));
            } else {
                $venue = array();
            }


            if (empty($venue)) {
                $venue['_venue_name'] = "Venue does not exist";
            } else {
                $venue_name = $wpdb->get_results("select post_title, ID from $wpdb->posts where post_type = 'venue' and ID = (SELECT meta_value FROM $wpdb->apollo_eventmeta WHERE meta_key = '_apollo_event_venue' AND apollo_event_id = $post->ID limit 1)");
                $venue['_venue_name'] = $venue_name[0]->post_title;
            }

        } else {
            $venue = $wpdb->get_results("select meta_value from $wpdb->apollo_eventmeta where meta_key ='_apl_event_tmp_venue' and apollo_event_id = " . $post->ID);
            if (!empty($venue)) {
                $venue = unserialize(unserialize($venue[0]->meta_value));
            } else {
                $venue = array();
            }
        }

        $primary = $wpdb->get_results("select name from $wpdb->terms where term_id = $primary");
        $primary = isset($primary[0]) ? $primary[0]->name : '';


        if($csv_report)
        {
            $reportData = includeReportData($post);
        }

        // Set up column header for event
        if ($incind != 'Y') {
            $csvt = array('id', 'title', 'content', 'start date', 'end date', 'website url', 'admission details', 'admission phone', 'ticket url', 'ticket email', 'discount url', 'contact name', 'contact email', 'contact phone', 'post status', 'category', 'secondary category', 'organization', 'venue', 'venue address', 'venue city', 'venue state', 'venue zip','official_web_clicks', 'buy_ticket_clicks', 'discount ticket clicks', 'date published', 'image url', 'userid');
        } else {
            $csvt = array('id', 'title', 'content', 'start date', 'end date', 'website url', 'admission details', 'admission phone', 'ticket url', 'ticket email', 'discount url', 'contact name', 'contact email', 'contact phone', 'post status', 'category', 'secondary category', 'organization', 'venue', 'venue address', 'venue city', 'venue state', 'venue zip', 'official_web_clicks', 'buy_ticket_clicks', 'discount ticket clicks', 'date published', 'image url', 'userid', 'individual date', 'individual time');
        }

        $csv[$idx][0] = $post->ID;

        // decode htmlentities
        $csv[$idx][1] = $post->post_title;
        $csv[$idx][2] = $post->post_content;
        $csv[$idx][1] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $csv[$idx][1]);
        $csv[$idx][2] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $csv[$idx][2]);
        $find = array('&rsquo;', '&#039;', ':', '�', '&nbsp;');
        $repl = array("'", "'", " ", '-', ' ');
        $csv[$idx][1] = str_ireplace($find, $repl, $csv[$idx][1]);
        $csv[$idx][2] = str_ireplace($find, $repl, $csv[$idx][2]);
        $csv[$idx][1] = strip_tags($csv[$idx][1]);
        $csv[$idx][2] = strip_tags($csv[$idx][2]);
        $csv[$idx][1] = html_entity_decode($csv[$idx][1], ENT_QUOTES, "UTF-8");
        $csv[$idx][2] = html_entity_decode($csv[$idx][2], ENT_QUOTES, "UTF-8");

        $csv[$idx][3] = $start_date;
        $csv[$idx][4] = $end_date;

        $csv[$idx][5] = $ed['_website_url'];

        $csv[$idx][6] = isset($ed['_admission_detail']) ? strip_tags($ed['_admission_detail']) : '';

        $csv[$idx][7] = $ed['_admission_phone'];
        $csv[$idx][8] = $ed['_admission_ticket_url'];
        $csv[$idx][9] = $ed['_admission_ticket_email'];
        $csv[$idx][10] = isset($ed['_admission_discount_url']) ? $ed['_admission_discount_url'] : '';
        $csv[$idx][11] = $ed['_contact_name'];
        $csv[$idx][12] = $ed['_contact_email'];
        $csv[$idx][13] = $ed['_contact_phone'];
        $csv[$idx][14] = isset($post->post_status) ? $post->post_status : '';
        $csv[$idx][15] = str_replace('&amp;', '&', $primary);
        $csv[$idx][16] = $secondary_cat;
        $csv[$idx][17] = $orgname;

        $csv[$idx][18] = isset($venue['_venue_name']) ? $venue['_venue_name'] : '';
        if (!isset($venue['_venue_address1'])) {
            $venue['_venue_address1'] = '';
        }
        if (!isset($venue['_venue_address2'])) {
            $venue['_venue_address2'] = '';
        }
        $csv[$idx][19] = $venue['_venue_address1'] . " " . $venue['_venue_address2'];
        $csv[$idx][20] = isset($venue['_venue_city']) ? $venue['_venue_city'] : '';
        $csv[$idx][21] = isset($venue['_venue_state']) ? $venue['_venue_state'] : '';
        $csv[$idx][22] = isset($venue['_venue_zip']) ? $venue['_venue_zip'] : '';

		$csv[$idx][23] = !empty($reportData['official_web_clicks']) ? $reportData['official_web_clicks'] : '';
        $csv[$idx][24] = !empty($reportData['buy_ticket_clicks']) ? $reportData['buy_ticket_clicks'] : '';
        $csv[$idx][25] = !empty($reportData['discount_ticket_clicks']) ? $reportData['discount_ticket_clicks'] : '';
        $csv[$idx][26] = substr($post->post_date, 0, 10);
        $csv[$idx][27] = $image_url;
        $csv[$idx][28] = isset($post->post_author) ? $post->post_author : '';

    }

// Get unique data from custom tables
/////////////////////////////////////
// Educator details
/////////////////////////////////////

    if ($csv_type == 'educator') {

        $org = $wpdb->get_results("select meta_key, meta_value from $wpdb->apollo_educatormeta where meta_key ='_apl_educator_data' and apollo_educator_id = " . $post->ID);
        $ad = unserialize(unserialize($org[0]->meta_value));
        $terms = $wpdb->get_results("SELECT * from $wpdb->term_relationships tr JOIN $wpdb->term_taxonomy tx ON tr.term_taxonomy_id = tx.term_id where object_id = " . $post->ID);
        $ed = $wpdb->get_row("select edu_id from $wpdb->apollo_program_educator where prog_id =" . $post->ID);
        $user = $wpdb->get_row("select user_id from wp_apollo_blog_user where educator_id =" . $post->ID);


        $at = array();
        $an = array();
        foreach ($terms as $term) {

            $at[$term->taxonomy][] = $term->term_taxonomy_id;
            $an[$term->taxonomy][] = get_term_by('id', $term->term_taxonomy_id, $term->taxonomy)->name;

        }
//print_r($an);

        // Set up column header for educator
        $csvt = array('id', 'title', 'content', 'add1', 'add2', 'state', 'city', 'zip', 'region', 'county', 'phone1', 'fax', 'email', 'url', 'blog', 'educator-type', 'educator-type id', 'post status', 'post date', 'image url', 'post author', 'user id');

        $csv[$idx][0] = $post->ID;


        $csv[$idx][1] = $post->post_title;
        $csv[$idx][2] = $post->post_content;
        $csv[$idx][1] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $csv[$idx][1]);
        $csv[$idx][2] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $csv[$idx][2]);
        $find = array('&rsquo;', '&#039;', ':', '�', '&nbsp;');
        $repl = array("'", "'", " ", '-', ' ');
        $csv[$idx][1] = str_ireplace($find, $repl, $csv[$idx][1]);
        $csv[$idx][2] = str_ireplace($find, $repl, $csv[$idx][2]);
        $csv[$idx][1] = strip_tags($csv[$idx][1]);
        $csv[$idx][2] = strip_tags($csv[$idx][2]);
        $csv[$idx][1] = html_entity_decode($csv[$idx][1], ENT_QUOTES, "UTF-8");
        $csv[$idx][2] = html_entity_decode($csv[$idx][2], ENT_QUOTES, "UTF-8");

        $csv[$idx][3] = $ad['_apl_educator_add1'];
        $csv[$idx][4] = $ad['_apl_educator_add2'];

        $csv[$idx][5] = $ad['_apl_educator_state'];
        $csv[$idx][6] = $ad['_apl_educator_city'];
        $csv[$idx][7] = $ad['_apl_educator_zip'];
        $csv[$idx][8] = $ad['_apl_educator_region'];

        $csv[$idx][9] = $ad['_apl_educator_county'];
        $csv[$idx][10] = $ad['_apl_educator_phone1'];
        $csv[$idx][11] = $ad['_apl_educator_fax'];
        $csv[$idx][12] = $ad['_apl_educator_email'];
        $csv[$idx][13] = $ad['_apl_educator_url'];
        $csv[$idx][14] = $ad['_apl_educator_blog'];

        $csv[$idx][15] = isset($at['educator-type']) ? implode(",", $at['educator-type']) : '';
        $csv[$idx][16] = isset($an['educator-type']) ? implode(",", $an['educator-type']) : '';

        $csv[$idx][17] = $post->post_status;
        $csv[$idx][18] = substr($post->post_date, 0, 10);
        $csv[$idx][19] = $image_url;
        $csv[$idx][20] = $post->post_author;

        $csv[$idx][21] = isset($user->user_id) ? $user->user_id : '';
    }

// Get unique data from custom tables
/////////////////////////////////////
// Program details
/////////////////////////////////////
//echo "SELECT * from $wpdb->apollo_term_relationships tr JOIN $wpdb->apollo_term_taxonomy tx ON tr.term_taxonomy_id = tx.term_id where object_id = ".$post->ID;
    if ($csv_type == 'program') {

        $org = $wpdb->get_results("select meta_key, meta_value from $wpdb->apollo_programmeta where meta_key ='_apl_program_data' and apollo_program_id = " . $post->ID);
        $ad = Apollo_App::unserialize($org[0]->meta_value);
        $org = $wpdb->get_results("select meta_key, meta_value from $wpdb->apollo_programmeta where meta_key ='_apollo_program_end_date' and apollo_program_id = " . $post->ID);
        $aa = isset($org[0]) ? unserialize($org[0]->meta_value) : '';
        $terms = $wpdb->get_results("SELECT * from $wpdb->term_relationships tr JOIN $wpdb->term_taxonomy tx ON tr.term_taxonomy_id = tx.term_id where object_id = " . $post->ID);
        $ed = $wpdb->get_row("select pe.edu_id, p.post_title from $wpdb->apollo_program_educator pe join $wpdb->posts p on p.id = pe.edu_id where prog_id =" . $post->ID);


        $at = array();
        $an = array();
        foreach ($terms as $term) {

            $at[$term->taxonomy][] = $term->term_taxonomy_id;
            $an[$term->taxonomy][] = get_term_by('id', $term->term_taxonomy_id, $term->taxonomy)->name;

        }


        // Set up column header
        $csvt = array('id', 'title', 'content', 'phone', 'email', 'url', 'max students', 'program length', 'program video', 'available date', '_apl_program_available_time',
            'space technical', 'program fee', 'program program core', 'program program essential', 'program cancel policy', 'program references', '_prog_edu_id', 'population-served id', 'subject id', 'program-type id', 'artistic-discipline id', 'cultural-origin id', 'population-served', 'subject', 'program-type', 'artistic-discipline', 'cultural-origin', 'educator id', 'educator name', 'post status', 'date published', 'image url', 'userid');

        $csv[$idx][0] = $post->ID;

        $csv[$idx][1] = $post->post_title;
        $csv[$idx][2] = $post->post_content;
        $csv[$idx][1] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $csv[$idx][1]);
        $csv[$idx][2] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $csv[$idx][2]);
        $find = array('&rsquo;', '&#039;', ':', '�', '&nbsp;');
        $repl = array("'", "'", " ", '-', ' ');
        $csv[$idx][1] = str_ireplace($find, $repl, $csv[$idx][1]);
        $csv[$idx][2] = str_ireplace($find, $repl, $csv[$idx][2]);
        $csv[$idx][1] = strip_tags($csv[$idx][1]);
        $csv[$idx][2] = strip_tags($csv[$idx][2]);
        $csv[$idx][1] = html_entity_decode($csv[$idx][1], ENT_QUOTES, "UTF-8");
        $csv[$idx][2] = html_entity_decode($csv[$idx][2], ENT_QUOTES, "UTF-8");

        $csv[$idx][3] = isset($ad['_apl_program_phone']) ? $ad['_apl_program_phone'] : '';
        $csv[$idx][4] = isset($ad['_apl_program_email']) ? $ad['_apl_program_email'] : '';

        $csv[$idx][5] = isset($ad['_apl_program_url']) ? $ad['_apl_program_url'] : '' ;
        $csv[$idx][6] = isset($ad['_apl_program_max_students']) ? $ad['_apl_program_max_students'] : '' ;
        $csv[$idx][7] = isset($ad['_apl_program_length_program']) ? $ad['_apl_program_length_program'] : '';
        $csv[$idx][8] = isset($ad['_apl_program_video'] ) ? $ad['_apl_program_video'] : '';
        $csv[$idx][9] = isset($ad['_apl_program_available_date'] ) ? $ad['_apl_program_available_date'] : '';

        $csv[$idx][10] = isset($ad['_apl_program_available_time']) ? $ad['_apl_program_available_time'] : '' ;
        $csv[$idx][11] = isset($ad['_apl_program_space_technical']) ? $ad['_apl_program_space_technical'] : '';
        $csv[$idx][12] = isset($ad['_apl_program_fee']) ? $ad['_apl_program_fee'] : '';
        $csv[$idx][13] = isset($ad['_apl_program_program_core']) ? $ad['_apl_program_program_core'] : '';
        $csv[$idx][14] = isset($ad['_apl_program_program_essential']) ? $ad['_apl_program_program_essential'] : '';
        $csv[$idx][15] = isset($ad['_apl_program_cancel_policy']) ? $ad['_apl_program_cancel_policy'] : '';
        $csv[$idx][16] = isset($ad['_apl_program_references']) ? $ad['_apl_program_references'] : '';
        $csv[$idx][17] = isset($ad['_prog_edu_id'] ) ? $ad['_prog_edu_id'] : '';

        $csv[$idx][18] = isset($at['population-served']) ? implode(",", $at['population-served']) : '';
        $csv[$idx][19] = isset($at['subject']) ? implode(",", $at['subject']) : '';
        $csv[$idx][20] = isset($at['program-type']) ? implode(",", $at['program-type']) : '';
        $csv[$idx][21] = isset($at['artistic-discipline']) ? implode(",", $at['artistic-discipline']) : '';
        $csv[$idx][22] = isset($at['cultural-origin']) ? implode(",", $at['cultural-origin']) : '';

        $csv[$idx][23] = isset($an['population-served']) ? implode(",", $an['population-served']) : '';
        $csv[$idx][24] = isset($an['subject']) ? implode(",", $an['subject']) : '';
        $csv[$idx][25] = isset($an['program-type']) ? implode(",", $an['program-type']) : '';
        $csv[$idx][26] = isset($an['artistic-discipline']) ? implode(",", $an['artistic-discipline']) : '';
        $csv[$idx][27] = isset($an['cultural-origin']) ? implode(",", $an['cultural-origin']) : '';

        $csv[$idx][28] = $ed->edu_id;
        $csv[$idx][29] = $ed->post_title;


//print_r($an);exit;
//echo implode(",",$an['population-served']);exit;


        $csv[$idx][30] = $post->post_status;
        $csv[$idx][31] = substr($post->post_date, 0, 10);
        $csv[$idx][32] = $image_url;
        $csv[$idx][33] = $post->post_author;


    }


// Get unique data from custom tables
/////////////////////////////////////
// Public Art details
/////////////////////////////////////

    if ($csv_type == 'public-art') {

        $pub = $wpdb->get_results("select meta_key, meta_value from $wpdb->apollo_public_artmeta where meta_key ='_apl_public_art_data' and apollo_public_art_id = " . $post->ID);
        $ad = unserialize(unserialize($pub[0]->meta_value));

        $pub = $wpdb->get_results("select meta_key, meta_value from $wpdb->apollo_public_artmeta where meta_key ='_apl_public_art_address' and apollo_public_art_id = " . $post->ID);
        $aa = unserialize(unserialize($pub[0]->meta_value));

        // Set up column header
        $csvt = array('id', 'title', 'content', 'contact name', 'contact phone', 'contact email', 'website url', 'latitude', 'longitude', 'dimension', 'created date', 'street address', 'city', 'state', 'zip', 'region', 'post status', 'date published', 'image url', 'userid');

        $csv[$idx][0] = $post->ID;

        $csv[$idx][1] = $post->post_title;
        $csv[$idx][2] = $post->post_content;
        $csv[$idx][1] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $csv[$idx][1]);
        $csv[$idx][2] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $csv[$idx][2]);
        $find = array('&rsquo;', '&#039;', ':', '�', '&nbsp;');
        $repl = array("'", "'", " ", '-', ' ');
        $csv[$idx][1] = str_ireplace($find, $repl, $csv[$idx][1]);
        $csv[$idx][2] = str_ireplace($find, $repl, $csv[$idx][2]);
        $csv[$idx][1] = strip_tags($csv[$idx][1]);
        $csv[$idx][2] = strip_tags($csv[$idx][2]);
        $csv[$idx][1] = html_entity_decode($csv[$idx][1], ENT_QUOTES, "UTF-8");
        $csv[$idx][2] = html_entity_decode($csv[$idx][2], ENT_QUOTES, "UTF-8");

        $csv[$idx][3] = $ad['_public_art_contact_name'];
        $csv[$idx][4] = $ad['_public_art_contact_phone'];
        $csv[$idx][5] = $ad['_public_art_contact_email'];
        $csv[$idx][6] = $ad['_public_art_website_url'];

        $csv[$idx][7] = isset($ad['_public_art_latitude']) ? $ad['_public_art_latitude'] : '';
        $csv[$idx][8] = isset($ad['_public_art_longitude']) ? $ad['_public_art_longitude'] : '';

        $csv[$idx][9] = $ad['_public_art_dimension'];
        $csv[$idx][10] = $ad['_public_art_created_date'];

        $csv[$idx][11] = $aa['_public_art_address'];
        $csv[$idx][12] = $aa['_public_art_city'];
        $csv[$idx][13] = $aa['_public_art_state'];
        $csv[$idx][14] = $aa['_public_art_zip'];
        $csv[$idx][15] = $aa['_public_art_region'];
        $csv[$idx][16] = $post->post_status;
        $csv[$idx][19] = substr($post->post_date, 0, 10);
        $csv[$idx][20] = $image_url;
        $csv[$idx][21] = $post->post_author;


    }

// Get unique data from custom tables
/////////////////////////////////////
// Business details
/////////////////////////////////////

    if ($csv_type == 'business') {

        // Set up column header
        $csvt = array('id', 'title', 'content', 'business categories', 'post status', 'userid');

        $csv[$idx][0] = $post->ID;

        $csv[$idx][1] = $post->post_title;
        $csv[$idx][2] = $post->post_content;
        $csv[$idx][1] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $csv[$idx][1]);
        $csv[$idx][2] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $csv[$idx][2]);
        $find = array('&rsquo;', '&#039;', ':', '�', '&nbsp;');
        $repl = array("'", "'", " ", '-', ' ');
        $csv[$idx][1] = str_ireplace($find, $repl, $csv[$idx][1]);
        $csv[$idx][2] = str_ireplace($find, $repl, $csv[$idx][2]);
        $csv[$idx][1] = strip_tags($csv[$idx][1]);
        $csv[$idx][2] = strip_tags($csv[$idx][2]);
        $csv[$idx][1] = html_entity_decode($csv[$idx][1], ENT_QUOTES, "UTF-8");
        $csv[$idx][2] = html_entity_decode($csv[$idx][2], ENT_QUOTES, "UTF-8");

        $classifiedcats = '';

        $terms = $wpdb->get_results("select * from $wpdb->term_relationships where object_id = $post->ID");

        foreach ($terms as $term) {
            $classifiedcats .= $term->term_taxonomy_id . ",";
        }

        $classifiedcats = substr($classifiedcats, 0, -1);

        $tnames = $wpdb->get_results("select * from $wpdb->terms where term_id in ($classifiedcats)");

        $classifiedcats = '';

        foreach ($tnames as $tname) {
            $classifiedcats .= $tname->name . ", ";

        }

        $classifiedcats = substr($classifiedcats, 0, -2);
        $classifiedcats = str_replace('&amp;', '&', $classifiedcats);

        $csv[$idx][3] = $classifiedcats;

        $csv[$idx][4] = $post->post_status;
        $csv[$idx][5] = $post->post_author;


    }


// Get unique data from custom tables
/////////////////////////////////////
// USER details
/////////////////////////////////////

    if ($csv_type == 'user') {


        // Set up column header
        $csvt = array('id', 'first name', 'last name', 'email', 'account created', 'role(s)', 'org id', 'venue id', 'artist id');

        $csv[$idx][0] = $post->user_id;

        $capkey = 'wp_' . $blog_id . '_capabilities';

        // GET DATA
        $data1 = $wpdb->get_results("select * from $wpdb->usermeta where meta_key = 'first_name' and user_id = $post->user_id");
        $firstname = $data1[0]->meta_value;
        $data2 = $wpdb->get_results("select * from $wpdb->usermeta where meta_key = 'last_name' and user_id = $post->user_id");
        $lastname = $data2[0]->meta_value;
        $data3 = $wpdb->get_results("select * from $wpdb->usermeta where meta_key = '$capkey' and user_id = $post->user_id");
        $capes = isset($data3[0]) ? unserialize($data3[0]->meta_value) : array();
        $data4 = $wpdb->get_results("select * from $wpdb->users where ID = $post->user_id");
        $data5 = $wpdb->get_results("select * from wp_apollo_blog_user where user_id = $post->user_id and blog_id = $blog_id");

        if ($data5[0]->org_id == 0) $data5[0]->org_id = '';
        if ($data5[0]->venue_id == 0) $data5[0]->venue_id = '';
        if ($data5[0]->artist_id == 0) $data5[0]->artist_id = '';

        $capa = '';
        foreach ($capes as $key => $val) {
            $capa .= $key . ", ";
        }
        $capa = substr($capa, 0, -2);

        $csv[$idx][1] = $firstname;
        $csv[$idx][2] = $lastname;
        $csv[$idx][3] = $data4[0]->user_email;
        $csv[$idx][4] = substr($data4[0]->user_registered, 0, 10);
        $csv[$idx][5] = $capa;
        $csv[$idx][6] = $data5[0]->org_id;
        $csv[$idx][7] = $data5[0]->venue_id;
        $csv[$idx][8] = $data5[0]->artist_id;


    }


// Get unique data from custom tables
/////////////////////////////////////
// Classified details
/////////////////////////////////////

    if ($csv_type == 'classified') {


        // Set up column header
        $csvt = array('id', 'title', 'content', 'organization', 'url', 'address', 'city', 'state', 'zip', 'other city', 'other state', 'phone', 'fax', 'email', 'expiration date', 'deadline date', 'contact name', 'contact phone', 'contact email', 'classified categories', 'post status', 'userid');

        $csv[$idx][0] = $post->ID;

        $csv[$idx][1] = $post->post_title;
        $csv[$idx][2] = $post->post_content;
        $csv[$idx][1] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $csv[$idx][1]);
        $csv[$idx][2] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", $csv[$idx][2]);
        $find = array('&rsquo;', '&#039;', ':', '�', '&nbsp;');
        $repl = array("'", "'", " ", '-', ' ');
        $csv[$idx][1] = str_ireplace($find, $repl, $csv[$idx][1]);
        $csv[$idx][2] = str_ireplace($find, $repl, $csv[$idx][2]);
        $csv[$idx][1] = strip_tags($csv[$idx][1]);
        $csv[$idx][2] = strip_tags($csv[$idx][2]);
        $csv[$idx][1] = html_entity_decode($csv[$idx][1], ENT_QUOTES, "UTF-8");
        $csv[$idx][2] = html_entity_decode($csv[$idx][2], ENT_QUOTES, "UTF-8");

        // GET DATA
        $data = $wpdb->get_results("select * from $wpdb->apollo_classifiedmeta where meta_key = '_apl_classified_data' and apollo_classified_id = $post->ID");
        $data = unserialize($data[0]->meta_value);

        // GET ADDRESS
        $data1 = $wpdb->get_results("select * from $wpdb->apollo_classifiedmeta where meta_key = '_apl_classified_address' and apollo_classified_id = $post->ID");
        $data1 = unserialize($data1[0]->meta_value);

        // Get org data
        $org = $wpdb->get_results("select * from $wpdb->apollo_classifiedmeta where meta_key = '_apl_classified_organization' and apollo_classified_id = $post->ID");
        $orgid = $org[0]->meta_value;

        if ($orgid != 0) {
            $orgn = $wpdb->get_results("select * from $wpdb->posts where post_type = 'organization' and ID = $orgid");
            $orgname = $orgn[0]->post_title;
        } else {
            $orgn = $wpdb->get_results("select * from $wpdb->apollo_classifiedmeta where meta_key = '_apl_classified_tmp_org' and apollo_classified_id = $post->ID");
            $orgname = $orgn[0]->meta_value;
        }

        $csv[$idx][3] = $orgname;
        $csv[$idx][4] = isset($data['_classified_website_url']) ? $data['_classified_website_url'] : '';
        $csv[$idx][5] = isset($data1['_classified_address']) ? $data1['_classified_address'] : '';
        $csv[$idx][6] = isset($data1['_classified_city']) ? $data1['_classified_city'] : '';
        $csv[$idx][7] = isset($data1['_classified_state']) ? $data1['_classified_state'] : '';
        $csv[$idx][8] = ' '; // zip
        $csv[$idx][9] = isset($data1['_classified_tmp_city']) ? $data1['_classified_tmp_city'] : '';
        $csv[$idx][10] = isset($data1['_classified_tmp_state']) ? $data1['_classified_tmp_state'] : '';

        $csv[$idx][11] = isset($data['_classified_phone']) ? $data['_classified_phone'] : '';
        $csv[$idx][12] = isset($data['_classified_fax']) ? $data['_classified_fax'] : '';
        $csv[$idx][13] = isset($data['_classified_email']) ? $data['_classified_email'] : '';
        $csv[$idx][14] = isset($data['_classified_exp_date']) ? $data['_classified_exp_date'] : '';
        $csv[$idx][15] = isset($data['_classified_deadline_date']) ? $data['_classified_deadline_date'] : '';
        $csv[$idx][16] = isset($data['_classified_contact_name']) ? $data['_classified_contact_name'] : '';
        $csv[$idx][17] = isset($data['_classified_contact_phone']) ? $data['_classified_contact_phone'] : '';
        $csv[$idx][18] = isset($data['_classified_contact_email']) ? $data['_classified_contact_email'] : '';

        $classifiedcats = '';

        $terms = $wpdb->get_results("select * from $wpdb->term_relationships where object_id = $post->ID");

        foreach ($terms as $term) {
            $classifiedcats .= $term->term_taxonomy_id . ",";
        }

        $classifiedcats = substr($classifiedcats, 0, -1);

        $tnames = $wpdb->get_results("select * from $wpdb->terms where term_id in ($classifiedcats)");

        $classifiedcats = '';

        foreach ($tnames as $tname) {
            $classifiedcats .= $tname->name . ", ";

        }

        $classifiedcats = substr($classifiedcats, 0, -2);
        $classifiedcats = str_replace('&amp;', '&', $classifiedcats);

        $csv[$idx][19] = $classifiedcats;

        $csv[$idx][20] = $post->post_status;
        $csv[$idx][21] = $post->post_author;

    }


    $idx++;


}
//die;

if ($noposts) {
    echo "No data found for your selection";
    die;
}

$header = '';

foreach ($csvt as $val) {
    $header .= $val . ",";
}

$header = substr($header, 0, -1);
echo $header . "\n";

$fp1 = fopen('php://output', 'w');

if (($incind == 'Y') AND ($csv_type == 'event')) {

    foreach ($csv as $row) {

//if($_SERVER['REMOTE_ADDR'] == '98.150.102.99' ) { echo "<PRE>"; print_r($row);print("select * from $wpdb->apollo_event_calendar where event_id = $row[0]"); }die();

        $datetime = $wpdb->get_results("select * from $wpdb->apollo_event_calendar where event_id = $row[0]");
        foreach ($datetime as $dt) {

            if ($dt->date_event < $csv_dateStart) continue;
            if ($dt->date_event > $csv_dateEnd) continue;

            $row[29] = $dt->date_event;
            $row[30] = date('g:ia', strtotime($dt->time_from));
            fputcsv($fp1, $row);
        }
    }

} else {

    foreach ($csv as $row) {
        fputcsv($fp1, $row);
    }
}

fclose($fp1);
die;


?>