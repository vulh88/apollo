<?php

/**
 * ThienLD : MUST put 2 hooks "stylesheet_directory_uri" and "stylesheet_directory" at the top of apollo-child functions.php in order custom stylesheet uri
 * and directory path to be always going to parent theme apollo
*/

add_filter('stylesheet_directory_uri','sonoma_custom_stylesheet_directory_uri',99,3);
function sonoma_custom_stylesheet_directory_uri($stylesheet_dir_uri, $stylesheet, $theme_root_uri ){
    return $theme_root_uri . '/' . 'apollo';
}
add_filter('stylesheet_directory','sonoma_custom_stylesheet_directory',99,3);
function sonoma_custom_stylesheet_directory($stylesheet_dir, $stylesheet, $theme_root){
    return $theme_root . '/' . 'apollo';
}


/**
 * sonoma includes
 *
 * The $sonoma_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 */
$sonoma_includes = array(

    // Parent includes
    'lib/config.php',          // Configuration
    'lib/utils.php',           // Utility functions
    'lib/init.php',            // Initial theme setup and constants
    'lib/wrapper.php',         // Theme wrapper class
    'lib/sidebar.php',         // Sidebar class
    'lib/activation.php',      // Theme activation
    'lib/titles.php',          // Page titles
    'lib/nav.php',             // Custom nav modifications
    'lib/gallery.php',         // Custom [gallery] modifications
    'lib/comments.php',        // Custom comments modifications
    'lib/scripts.php',         // Scripts and stylesheets
    'lib/extras.php',          // Custom functions
    'lib/languages.php',       // Language
    'lib/retina.php',          // Support Retina

    // Children  includes
    'libraries/config.php',          // Configuration
    'libraries/init.php',            // Initial theme setup and constants
    'libraries/scripts.php',         // Scripts and stylesheets
    'libraries/extras.php',          // Custom functions
    'libraries/multi-post-thumbnails.php',          // Multi thumbnails
    'libraries/const.php',          // Common const
    'inc/modules/init.php',          // Sonoma module init
    'inc/modules/admin-init.php',          // Sonoma module admin init
    'inc/admin/assets.php',

);

foreach ($sonoma_includes as $file) {
    if (!$filepath = locate_template($file)) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'sonoma'), $file), E_USER_ERROR);
    }

    require_once $filepath;
}
unset($file, $filepath);




/**
 * Retrieve stylesheet directory URI.
 *
 * @return string
 */
function sm_get_stylesheet_directory_uri() {
    $stylesheet = str_replace( '%2F', '/', rawurlencode( get_stylesheet() ) );
    $theme_root_uri = get_theme_root_uri( $stylesheet );
    $stylesheet_dir_uri = "$theme_root_uri/$stylesheet";

    /**
     * Filter the stylesheet directory URI.
     *
     * @param string $stylesheet_dir_uri Stylesheet directory URI.
     * @param string $stylesheet         Name of the activated theme's directory.
     * @param string $theme_root_uri     Themes root URI.
     */
    return apply_filters( 'sm_stylesheet_directory_uri', $stylesheet_dir_uri, $stylesheet, $theme_root_uri );
}


/**
 * Retrieve stylesheet directory path for current theme.
 *
 * @return string Path to current theme directory.
 */
function sm_get_stylesheet_directory() {
    $stylesheet = get_stylesheet();
    $theme_root = get_theme_root( $stylesheet );
    $stylesheet_dir = "$theme_root/$stylesheet";

    /**
     * Filter the stylesheet directory path for current theme.
     *
     * @since 1.5.0
     *
     * @param string $stylesheet_dir Absolute path to the current them.
     * @param string $stylesheet     Directory name of the current theme.
     * @param string $theme_root     Absolute path to themes directory.
     */
    return apply_filters( 'sm_stylesheet_directory', $stylesheet_dir, $stylesheet, $theme_root );
}

function sm_get_network_options($key){
    $option = get_option($key);
    if ( empty($option) || $option == 1 ){
        update_option($key, 0);
    }
}

add_filter('_apl_hide_tabs_unnecessary','_apl_hide_tabs_unnecessary',99,1);
function _apl_hide_tabs_unnecessary($tab = ''){
    $tabArray = array();
    if ( in_array($tab,$tabArray)) {
        return 'hidden';
    }
    return '';
}

add_filter(APL_SM_HOOK_FILTER_CURRENT_ACTIVE_THEME,'_apl_get_current_active_theme_handler',99);
function _apl_get_current_active_theme_handler( $name =''){
    $currentTheme = wp_get_theme();
    return !empty($currentTheme) ? $currentTheme->stylesheet : '';
}

add_action('_apl_add_additionfields_for_page','_apl_add_additionfields_for_page',99,2);
function _apl_add_additionfields_for_page( $mod, $function){
    add_submenu_page("edit.php?post_type={$mod}", __( 'Additional Fields', 'apollo' ), __( 'Additional Fields', 'apollo' ), 'manage_options', "".$mod."_custom_fields", $function);

}

add_filter( 'get_child_theme', 'return_get_child_theme' );
function return_get_child_theme( $arg = '' ) {
    $currentTheme = wp_get_theme();
    return 'wp-content/themes/'. $currentTheme->stylesheet;

}

add_filter('_apl_taxonomy_custom_category_spotlight_size', 'custom_category_spotlight_image_size');
function custom_category_spotlight_image_size () {
    return 'large';
}

add_filter('apl_event_listing_page_custom_title', 'octave_custom_event_listing_page_title' , 10);
function octave_custom_event_listing_page_title() {
    return true;
}

/**
 * The left and right style of the navigation are using the tablet menu on the desktop version, so we need to
 * add a class to handle UI style on this theme
 */
add_filter('apl_tablet_menu_class', function() {
    return 'pc-show';
});


/**
 * The left and right style of the navigation are using the tablet menu on the desktop version, so we need to
 * add a class to handle UI style on this theme
 */
add_filter('apl_tablet_menu_class', function() {
    return 'pc-show';
});
/*@ticket #16738: Octave Theme - [Home blog feature] Increase the max words of the description each blog item*/
add_filter('apl_increase_max_word_for_description_of_home_blog_feature', function(){
    return 175;
});




add_filter('oc_body_class', function() {

    $bodyClass = array();

    $bodyClass[] = get_query_var('_apollo_artist_timeline') === 'true' ? 'body-css' : 'body-css full-theme';

    /*@ticket #16236: Apply full-screen template with only column for the blog detail page*/
    if (is_single()& get_post_type() == 'post' && of_get_option(Apollo_DB_Schema::_BLOG_DISPLAY_STYLE,'default') == 'one-column'){
        $bodyClass[] = "single-post-solo";
    }

    if (!of_get_option(Apollo_DB_Schema::_ENABLE_TOP_HEADER)) {
        $bodyClass[] = 'no-topbar';

        // Apply not scroll with page to header if the top bar is disabled
        if (!of_get_option(Apollo_DB_Schema::_SCROLL_WITH_PAGE)) {
            $bodyClass[] = 'header-scroll-with-page';
        }
    }

    //header-scroll-with-page

    $fullScreenMenu = of_get_option(OC_Common_Const::_OC_TOP_HEADER_NAVIGATION_STYLE, '');
    $bodyClass[] = $fullScreenMenu;

    /** @Ticket #19446 */
    if ($fullScreenMenu != 'header-bar--right') {
        $mastheadButtonStyle = of_get_option(Apollo_DB_Schema::_MASTHEAD_GROUP_BUTTONS_STYLE, '');
        if (!empty($mastheadButtonStyle)) {
            $bodyClass[] = $mastheadButtonStyle;
        }
    }

    $spotlightType = of_get_option(Apollo_DB_Schema::_SPOT_TYPE);
    if ($spotlightType == 'large'
        && of_get_option(OC_Common_Const::_OC_TRANSPARENT_MASTHEAD_NAV, 0) == 1
        && Apollo_App::is_homepage()) {
        // responsive-slider: avoid hard fix the height of the slider
        $bodyClass[] = 'oct-transparent--bg';
    }

    if ($spotlightType == 'large' || $spotlightType == 'small') {
        $bodyClass[] = 'responsive-slider';
    }

    /**
     * @ticket #18509: 0002522: wpdev54 Customization - Display full width and height of the images on the slider.
     */
    if(Apollo_App::is_homepage()
        && of_get_option(Apollo_DB_Schema::_APL_HOME_SLIDER_DISPLAY_FULL, 0)
        && $spotlightType == 'large'){
        $bodyClass[] = 'apl-home-slider-display-full';
    }

    /** @Ticket #18570 */
    $mobileMenuStyle = of_get_option(Apollo_DB_Schema::_MOBILE_MENU_STYLE, '');
    if (!empty($mobileMenuStyle)) {
        $mobileNavStyle = of_get_option(Apollo_DB_Schema::_MOBILE_NAVIGATION_STYLE, 'oct__show-menu-item');
        $bodyClass[] = $mobileMenuStyle . ' ' . $mobileNavStyle;
    }

    return implode(' ', $bodyClass);
});

add_filter('octave_render_bookmark_icon', function ($bkIcon, $event){
    global $apl_current_taxonomy_type, $post;
    if ($apl_current_taxonomy_type && strpos($apl_current_taxonomy_type, 'event-type') !== false
        || ($post && $post->post_type == Apollo_DB_Schema::_EVENT_PT)
        || isset($_GET['action']) && $_GET['action'] == 'apollo_category_show_more_other_event') {
        return $event->renderBookmarkBtn(array(), true);
    }
    return $bkIcon;
}, 10, 2);

add_filter('octave_custom_bookmark_btn', function ($bookmarkIcon){
    global $apl_current_taxonomy_type, $post;
    if ($apl_current_taxonomy_type && strpos($apl_current_taxonomy_type, 'event-type') !== false
        || ($post && $post->post_type == Apollo_DB_Schema::_EVENT_PT)
        || isset($_GET['action']) && $_GET['action'] == 'apollo_category_show_more_other_event'
        || Apollo_App::is_homepage()) {
        return '';
    }
    return $bookmarkIcon;
});

add_filter('apl_add_bookmark_icon_to_social_btn', 'addBookmarkIconToSocialBtn', 10, 2);
function addBookmarkIconToSocialBtn($bookmarkIcon, $eventId = '')
{
    if (Apollo_App::is_homepage()) {
        if (empty($eventId)) {
            return '';
        }
        $event = get_event($eventId);
        return $event->renderBookmarkBtn(array('class' => 'btn btn-category add-it-btn'), true);
    }
    return '';
}

add_filter('octave_filter_by_my_location', 'octave_filter_by_my_location', 99, 3);
function octave_filter_by_my_location($html, $searchData, $position) {
    $locationService = of_get_option(OC_Common_Const::_OC_ENABLE_GEO_LOCATION, false);
    $locationFilter = of_get_option(OC_Common_Const::_OC_SEARCH_WIDGET_ENABLE_FILTER_BY_MY_LOCATION, false);
    $positionShow = $position ? 'by_my_location_top' : 'by_my_location';
    if ($locationService && $locationFilter) {
        $checked = !empty($searchData['by_my_location']) ? 'checked' : '';
        $latitude = !empty($searchData['apl-lat']) ? $searchData['apl-lat'] : '';
        $longitude = !empty($searchData['apl-lng']) ? $searchData['apl-lng'] : '';
        $html .= '<div class="s-rw apl-filter-location el-blk hidden " />
                    <input class="by_my_location has-map-icon "'. $checked .' name="by_my_location" value="1" id="'.$positionShow.'" type="checkbox">
                    <label for="'.$positionShow.'" class="map-icon-ipt">'.__( 'Filter by my location', 'apollo' ).'</label>
                    <input type="hidden" name="apl-lat" class="apl-lat" value="'.$latitude.'" data-old-value="'.$latitude.'"/>
                    <input type="hidden" name="apl-lng" class="apl-lng" value="'.$longitude.'" data-old-value="'.$longitude.'"/>
                </div>';
    }
    return $html;
}

add_filter('octave_render_map_view', 'renderMapView', 99, 1);
function renderMapView($search_obj) {
    require_once APOLLO_TEMPLATES_DIR . '/events/listing-page/map.php';
}

add_filter('apl_add_options_filtering', 'addOptionsFiltering', 10, 2);
function addOptionsFiltering($mapView, $searchObj) {
    $url = $searchObj->get_template_btn_url('map');
    $filterIconOption = of_get_option(OC_Common_Const::_OC_EVENT_LISTING_FILTER_ICON, 'only_icon');
    $textFilterMap = $filterIconOption == 'icon_and_text' ? __('Map', 'apollo') : '';
    $textFilterMap = !empty($textFilterMap) ? '<span class="event-filter-text">' . $textFilterMap . '</span>' : '';
    $iconAndTextClass = $filterIconOption == 'icon_and_text' ? 'event-filter-icon-text' : '';

    if (strpos($url, '&by_my_location=1') && strpos($url, '&apl-lat=') && strpos($url, '&apl-lng=')) {
        $mapView = '<li class="'.$iconAndTextClass.'" id="octave-map-option-filtering">
                <a href="'. $searchObj->get_template_btn_url('map') .'">
                    <i class="fa fa-map-marker fa-2x"></i>
                    '.$textFilterMap.'
                </a>
            </li>';
    }
    return $mapView;
}

add_filter('apl_custom_default_options_filtering', 'customDefaultOptionFiltering', 10, 2);
function customDefaultOptionFiltering($customFiltering, $searchObj) {
    $enableTile = of_get_option(OC_Common_Const::_OC_ENABLE_LISTING_PAGE_TILE_VIEW, false);
    $filterIconOption = of_get_option(OC_Common_Const::_OC_EVENT_LISTING_FILTER_ICON, 'only_icon');
    if ($filterIconOption == 'icon_and_text') {
        $tileText = '<span class="event-filter-text">' . __('Tile', 'apollo') . '</span>';
        $listText = '<span class="event-filter-text">' . __('List', 'apollo') . '</span>';
        $iconAndTextClass = 'event-filter-icon-text';
    } else {
        $tileText = '';
        $listText = '';
        $iconAndTextClass = '';
    }
    if ($enableTile) {
        $customFiltering = '<li class="'.$iconAndTextClass.' '
                            . (!$searchObj->is_list_page(of_get_option(Apollo_DB_Schema::_EVENT_DEFAULT_VIEW_TYPE), 2) ? 'current' : '') .'">
                            <a href="'. $searchObj->get_template_btn_url() .'">
                                <i class="fa fa-th fa-2x"></i>
                                '.$tileText.'
                            </a>
                        </li>';
    }
    $customFiltering .= '<li class="'.$iconAndTextClass . ' '
                            . ($searchObj->is_list_page(of_get_option(Apollo_DB_Schema::_EVENT_DEFAULT_VIEW_TYPE), 2) ? 'current' : '') .'">
                            <a href="'. $searchObj->get_template_btn_url( 'list' ) .'">
                                <i class="fa fa-bars fa-2x"></i>
                                '.$listText.'
                            </a>
                        </li>';
    return $customFiltering;
}

add_filter('apl_render_more_socials', 'renderMoreSocials', 10, 2);
function renderMoreSocials($instagram, $share_datas) {
    if (of_get_option(OC_Common_Const::_OC_SOCIAL_ICON_STYLE, 0)) {
        $iconStyle = 'bg_social';
    } else {
        $iconStyle = '';
    }
    $instagram = '<a href="'. SocialFactory::getInstagramSharerLink( $share_datas['info'] ) .'" target="_blank" class="sc inst '.$iconStyle.'"
            data-ride="ap-logclick"
            data-action="apollo_log_share_activity"
            data-activity="'. Apollo_Activity_System::SHARE_INSTA .'"
            data-item_id="'. $share_datas['id'] .'"
             ><span><i class="fa fa-instagram"></i></span><span class="hidden">'. __( 'Instagram', 'apollo' ).'</span></a>';
    return $instagram;
}

add_filter('apl_social_icon_style', 'socialIconStyle', 10, 1);
function socialIconStyle($socialIconClass) {
    if (of_get_option(OC_Common_Const::_OC_SOCIAL_ICON_STYLE, 0)) {
        return 'bg_social';
    }
    return $socialIconClass;
}

add_filter('apl_render_cta_button', 'renderCallToActionButton', 10, 4);
function renderCallToActionButton($value, $postId, $target, $link) {
    /*@ticket 17249 Octave Theme - [Spotlight] Add new button to the homepage spotlight - item 2*/
    $actionButton = get_post_meta($postId , Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_ACTION_BUTTON ,true);

    if(!$actionButton)
        return '';

    $semiTransparent = get_post_meta($postId , Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_SEMI_TRANSPARENT ,true);
    $displaySetting =  get_post_meta($postId , Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_SETTING ,true);

    $text = isset($displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_TEXT]) ? $displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_TEXT] : _('Call to action', 'apollo');
    $transparentLevel = isset($displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_TRANSPARENT_LEVEL]) ? $displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_TRANSPARENT_LEVEL] : 75;
    $transparentLevel = $transparentLevel/100;
    $background = isset($displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_BACKGROUND]) ? $displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_BACKGROUND] : '#429ad4';
    $backgroundHover = adjust_brightness( $background, check_is_bright_color( $background ) ? -15 : 50 );
    $width = isset($displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_WIDTH]) ? $displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_WIDTH] : 200;
    $height = isset($displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_HEIGHT]) ? $displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_HEIGHT] : 50;

    if($semiTransparent === 'on' || !$semiTransparent){
        list($r, $g, $b) = sscanf($background, "#%02x%02x%02x");
        $background = "rgba($r, $g, $b, $transparentLevel)";
    }

    $buttonStyle = "style='height: $height"."px; width: $width"."px; background: $background; line-height: calc($height"."px - 20px)'";
    $mouseOver = "this.style.background='$backgroundHover'";
    $mouseOut = "this.style.background='$background'";

    return sprintf('<div class="blk-button-call"><div class="inner">
                            <a class="btn btn-callToAction" %s href="%s" %s onMouseOver="%s" onMouseOut="%s">%s</a>
                            </div></div>',
                    $target, $link, $buttonStyle, $mouseOver, $mouseOut, $text);
}

/*@ticket #17592: [TF] 20180917 – Octave theme - Webpack*/
if ( ! function_exists('oc_mix')) {
    function oc_mix( $path, $manifest_directory = '' ) {
        static $manifest;

        $manifest_path = SONOMA_PARENT_DIR . '/mix-manifest.json' ;

        if ( ! file_exists( $manifest_path ) ) {
            return  '/' . $path ;
        }
        if ( ! $manifest ) {
            $manifest = json_decode( file_get_contents( $manifest_path ), true );
        }

        $path = str_replace( $manifest_directory, '', $path );

        if ( ! array_key_exists( $path, $manifest ) ) {
            return  '/' .$path ;
        }

        $path = $manifest[ $path ];
        $path = ltrim( $path, '/' );
        return trailingslashit( $manifest_directory ) . $path ;
    }
}

/*@ticket #17681 */
add_filter('apl_add_custom_position_for_widget_location', 'apl_add_custom_position_for_widget_location', 99, 1);
function apl_add_custom_position_for_widget_location($defaulValue) {
    return array(
        'blog_detail_footer'  => __("Blog Detail Footer", "apollo")
    );
}

add_filter('apl_render_class_item_per_row', 'apl_render_class_item_per_row', 99);
function apl_render_class_item_per_row () {
    $itemPerRow = of_get_option(Apollo_DB_Schema::_HOME_FEATURE_EVENT_ITEM_PER_PAGE, 2);
    if (of_get_option(Apollo_DB_Schema::_DEFAULT_HOME_LAYOUT, 'right_sidebar_one_column') !== 'right_sidebar_two_columns') {
        return '';
    }
    switch ($itemPerRow) {
        case 2:
            $classItemPerRow = 'col-6';
            break;
        case 3:
            $classItemPerRow = 'col-4';
            break;
        case 4:
            $classItemPerRow = 'col-3';
            break;
        default:
            $classItemPerRow = 'col-6';
            break;
    }
    return $classItemPerRow;
}

/**
 * @ticket #18340: 0002410: wpdev55 - Add the discount text and icon is the category page featured and and lower listing areas
 */
add_filter('oc_get_discount_config', function(){
    return  array(
        "enable-discount-description" => of_get_option(Apollo_DB_Schema::_EVENT_ENABLE_DISCOUNT_DESCRIPTION_FE, false),
        "include_discount_url" => of_get_option(Apollo_DB_Schema::_EVENT_INCLUDE_DISCOUNT_URL, false),
        "display_all_icon" => of_get_option(APL_Theme_Option_Site_Config_SubTab::_EVENT_DETAIL_DISPLAY_ALL_ICON, false),
    );
});

/**
 * @ticket #19131: Octave Theme - Change all detail page section labels same the section labels on the homepage - item 2
 */
add_filter('oc_get_opt_section_labels', function(){
    return of_get_option(OC_Common_Const::_OC_SECTION_LABELS, 'default');
});