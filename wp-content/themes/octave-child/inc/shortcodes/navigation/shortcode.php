<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class OC_Navigation_Shortcode {

    /**
     * OC_Navigation_Shortcode constructor.
     */
    public function __construct() {
        add_shortcode('ac_navigation_sc', array($this, 'handle'));
        add_shortcode('oc_recent_bookmarks', array($this, 'get_recent_bookmarks'));
    }

    /**
     * @return string
     */
    public function handle() {
        ob_start();
        include OC_SHORTCODE_DIR . '/navigation/templates/main-nav.php';
        return ob_get_clean();
    }

    /**
     * @return string
     */
    public function get_recent_bookmarks(){
        ob_start();
        include OC_SHORTCODE_DIR . '/navigation/templates/bookmark-list.php';
        return ob_get_clean();
    }
}
new OC_Navigation_Shortcode();