<?php
require_once APOLLO_TEMPLATES_DIR . '/pages/dashboard/inc/apollo-account-dashboard-dataprovider.php';

$recentBookmarks = Apollo_Account_Dashboard_Provider::getBookmark(array(
    'pagesize' => 10,
    'start' => 0,
    'post_type' => [],
    'user_id' => get_current_user_id(),
));

$bookmarkItems = '';
foreach ($recentBookmarks["datas"] as $bookmark) {

    if (get_permalink($bookmark->post_id) && $bookmark->post_title) {
        $bookmarkItems .= '<a href="' . get_permalink($bookmark->post_id) . '" class="bookmark-item">' . $bookmark->post_title . '</a>';

    }
}

if ($bookmarkItems === '') {
    echo "<p>" . __('There are no bookmarks recently', 'apollo') . "</p>";
} else {
    echo '<div class="list-bookmarks">' . $bookmarkItems . '</div>
          <a href="' . home_url() . '/user/my-list" class="view-all">' . __('View all bookmarks') . '</a>';
}
