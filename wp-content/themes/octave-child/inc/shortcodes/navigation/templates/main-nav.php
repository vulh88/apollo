<?php
$navStyle = of_get_option(Apollo_DB_Schema::_NAVIGATION_BAR_STYLE);
if (has_nav_menu('primary_navigation')) {
    $menu_str = wp_nav_menu(array(
        'theme_location' => 'primary_navigation',
        'menu_class' => 'nav',
        'echo' => false,
    ));

} else {
    $menu_str = wp_page_menu(array(
        'echo' => false
    ));
}

echo $menu_str;