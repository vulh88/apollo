<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class OC_Blog_Detail_Shortcode {

    /**
     * OC_Navigation_Shortcode constructor.
     */
    public function __construct() {
        add_shortcode('ac_parallax_sc', array($this, 'handle'));
        add_shortcode('ac_animation_iframe_sc', array($this, 'handle_animation'));
    }

    /**
     * @param array $atts
     * @return string
     */
    public function handle($atts = array()) {
        ob_start();
        include OC_SHORTCODE_DIR . '/blog/templates/parallax.php';
        return ob_get_clean();
    }

    /**
     * @param array $atts
     * @return string
     */
    public function handle_animation($atts = array()) {
        ob_start();
        include OC_SHORTCODE_DIR . '/blog/templates/animation.php';
        return ob_get_clean();
    }

}
new OC_Blog_Detail_Shortcode();