<?php
$src = !empty($atts['src']) ? $atts['src'] : '';
$width = !empty($atts['width']) ? $atts['width'] : '';
$height = !empty($atts['height']) ? $atts['height'] : '';
if ($src):
?>
    <div class="responsive-block">
        <iframe <?php echo $width ? 'width="'.$width.'"' : '' ?> <?php echo $height ? 'height="'.$height.'"' : '' ?> src="<?php echo $src ?>" class="responsive-item"></iframe>
    </div>
<?php endif; ?>
