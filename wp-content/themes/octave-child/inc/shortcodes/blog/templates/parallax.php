<?php
$bg = !empty($atts['image_url']) ? $atts['image_url'] : '';
$title = !empty($atts['title']) ? $atts['title'] : '';
$url = !empty($atts['link']) ? $atts['link'] : '';
$targetLink = !empty($atts['target_link']) ? $atts['target_link'] : '';
$isImage = !empty($atts['is_image']) ? $atts['is_image'] : false;
$attachmentId = !empty($atts['attachment_id']) ? intval($atts['attachment_id']) : '';
$creditText = !empty($atts['credit_text']) ? $atts['credit_text'] : '';
if ($bg):
    ob_start();
        if ($url): ?><a <?php echo $targetLink ? 'target="'.$targetLink.'"' : '' ?> href=""><?php endif;

        if ($isImage) : ?>
            <div class="ov-parallax full-width-block parallax-title"><img src="<?php echo $bg; ?>"></div>
            <?php
        else : ?>
            <div class="ov-parallax full-width-block parallax-title"
                 style="background-image: url('<?php echo $bg ?>');"><?php echo $title ?></div>
            <?php
        endif;
        ?>
        <?php if ($url): ?></a><?php endif ;
    $parallaxHtml = ob_get_clean();
    if ($attachmentId) :
        $caption = wp_get_attachment_caption($attachmentId); ?>
            <figure  class="wp-caption apl-parallax-sc">
                <?php echo $parallaxHtml; ?>
                <figcaption class="wp-caption-text">
                    <p class="apl-caption-text"><?php echo $caption; ?></p>
                    <?php
                    if (empty($creditText) && function_exists('the_media_credit_html')) {
                        $creditText = get_media_credit_html($attachmentId);
                    }
                    if (!empty($creditText)) : ?>
                        <p class="media-credit"><?php echo $creditText; ?></p>
                    <?php endif; ?>
                </figcaption>
            </figure>
    <?php
    else :
        echo $parallaxHtml;
    endif;
?>
<?php endif; ?>