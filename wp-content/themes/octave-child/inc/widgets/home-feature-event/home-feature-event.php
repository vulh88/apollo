<?php

require_once APOLLO_INCLUDES_DIR. '/widgets/Apollo_Widget.php';

class apollo_home_feature_event_widget extends WP_Widget {
    private $adminTemplatePath = '';
    public function __construct() {
        $this->adminTemplatePath = dirname(__FILE__). '/admin.php';
        parent::__construct(
            
            // ID of widget
            'apollo_home_feature_event_widget',

            // Widget name 
            __('Apollo Home Feature Event Widget', 'apollo'),

            // Widget description
            array( 'description' => __( 'Apollo Home Feature Event Widget', 'apollo' ), )
        );
    }

    public function form($instance)
    {
        if (!file_exists($this->adminTemplatePath)) {
            return '';
        }

        ob_start();
        include $this->adminTemplatePath;
        $html = ob_get_contents();
        ob_end_clean();
        echo $html;
    }

    public function widget($args, $instance)
    {
        $active_top_ten = of_get_option( Apollo_DB_Schema::_ACTIVATE_TOPTEN_TAB );
        $features_category = of_get_option( Apollo_DB_Schema::_HOME_CAT_FEATURES, false, true );
        include_once APOLLO_TEMPLATES_DIR.'/events/list-events.php';
        $main_content = ( $active_top_ten || $features_category ) && Apollo_App::is_avaiable_module(Apollo_DB_Schema::_EVENT_PT );
        $top_sub_contain =  Apollo_App::get_static_html_content( Apollo_DB_Schema::_TOP_SUB_CONTAIN);

        if ( get_option( 'show_on_front' ) == 'posts' ) {
            include APOLLO_TEMPLATES_DIR. '/home/latest-posts.php';
        } else {
            include APOLLO_TEMPLATES_DIR. '/home/featured-events.php';
        }
    }

    public function update($new_instance, $old_instance)
    {
        return parent::update($new_instance, $old_instance);
    }
}  