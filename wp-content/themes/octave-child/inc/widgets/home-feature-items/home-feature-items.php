<?php

require_once APOLLO_INCLUDES_DIR. '/widgets/Apollo_Widget.php';

class apollo_home_feature_items_widget extends WP_Widget {
    private $adminTemplatePath = '';
    public function __construct() {
        $this->adminTemplatePath = dirname(__FILE__). '/admin.php';
        parent::__construct(
            
            // ID of widget
            'apollo_home_feature_items_widget',

            // Widget name 
            __('Apollo Home Feature Items Widget', 'apollo'),

            // Widget description
            array( 'description' => __( 'Apollo Home Feature Items Widget', 'apollo' ), )
        );
    }

    public function form($instance)
    {
        if (!file_exists($this->adminTemplatePath)) {
            return '';
        }

        ob_start();
        include $this->adminTemplatePath;
        $html = ob_get_contents();
        ob_end_clean();
        echo $html;
    }

    public function widget($args, $instance)
    {
        $fileName = dirname(__FILE__) . '/frontend-templates/featured-item.php';
        if (!file_exists($fileName)) {
            return '';
        }
        ob_start();
        include $fileName;
        $html = ob_get_contents();
        ob_end_clean();
        echo $html;
    }

    public function update($new_instance, $old_instance)
    {
        return parent::update($new_instance, $old_instance);
    }
}
/*@ticket #17853:  Modify the "Content" field of the widget to include the editing tool bar and access to Media Library*/
if ( ! class_exists( 'Apollo_Custom_Editor_Widget' ) && is_admin()) {
    include APOLLO_WIDGETS_DIR .  '/custom/apollo-custom-editor-widget.php';
}
