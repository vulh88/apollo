<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'apollo' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" 
           name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" 
           value="<?php echo esc_attr( isset( $instance['title'] ) ? $instance['title'] : '' ); ?>" />
</p>
<p>
    <input type="checkbox" id="<?php echo $this->get_field_id( 'has-underline' ); ?>" name="<?php echo $this->get_field_name( 'has-underline' ); ?>"
    <?php echo isset($instance['has-underline']) ? 'checked' : ''; ?>>
    <label for="<?php echo $this->get_field_id( 'has-underline' ); ?>"><?php _e( 'Has underline', 'apollo' ); ?></label>
</p>
<div class="list-items-wrap">
    <?php
    $listItem = isset($instance['item-title']) ? $instance['item-title'] : array();
    $itemTitleName = $this->get_field_name('item-title');
    $itemTitleUrl = $this->get_field_name('item-url');
    $itemImageUrl = $this->get_field_name('item-image-url');
    $itemContent = $this->get_field_name('item-content');
    $hideItemTitle = $this->get_field_name('hide-item-title');
    $removeItem = '<div data-confirm="'.__('Are you sure to remove this item ?', 'apollo').'" class="widget-del" title="Remove Item"><i class="fa fa-times"></i></div>';
    if (!empty($listItem)) :
        foreach ($listItem as $key => $value) : {
            $itemUrlValue = isset($instance['item-url'][$key]) ? $instance['item-url'][$key] : '';
            $itemImageUrlValue = isset($instance['item-image-url'][$key]) ? $instance['item-image-url'][$key] : '';
            $itemContentValue = isset($instance['item-content'][$key]) ? $instance['item-content'][$key] : '';
            $hideItemTitleValue = isset($instance['hide-item-title'][$key]) ? $instance['hide-item-title'][$key] : 'off';
            $checked = $hideItemTitleValue === 'on' ? 'checked' : '';
            /*@ticket #17853:  Modify the "Content" field of the widget to include the editing tool bar and access to Media Library*/
            $itemContentId = "home-featured-item-content-$this->number-$key"
            ?>
            <details class="apl-item-inner" open>
                <summary></summary>
                <div>
                    <?php echo $removeItem; ?>
                    <div>
                        <label><?php _e('Hide item title: ', 'apollo'); ?></label>
                        <input class="hide-item-title widefat"  type="checkbox" <?php echo $checked; ?> />
                        <input type="hidden" class="hide-item-title-value" name="<?php echo $hideItemTitle; ?>[]" value="<?php echo $hideItemTitleValue ?>" />
                    </div>
                    <div>
                        <label><?php _e('Item title: ', 'apollo'); ?></label>
                        <input name="<?php echo $itemTitleName; ?>[]" class="item-title widefat"
                               value="<?php echo $value; ?>"/>
                    </div>
                    <div>
                        <label><?php _e('Item title url: ', 'apollo'); ?></label>
                        <input name="<?php echo $itemTitleUrl; ?>[]" class="item-title-url widefat of-input apollo_input_url"
                               value="<?php echo $itemUrlValue; ?>"/>
                    </div>
                    <div>
                        <label><?php _e('Image', 'apollo'); ?> <a class="apl-widget-set-item-image"><?php _e("Browse an image","apollo") ?></a></label>
                        <div class="apl-widget-img-wrapper <?php echo $itemImageUrlValue ? '' : 'hidden'?>">
                            <input type="hidden" name="<?php echo $itemImageUrl; ?>[]" class="item-image-url widefat"
                                   value="<?php echo $itemImageUrlValue; ?>"/>
                            <span class="apl-widget-delete-item-image"><i class="fa fa-times"></i></span>
                            <img class="apl-widget-item-image widefat" src="<?php echo $itemImageUrlValue; ?>">
                        </div>
                    </div>
                    <div>
                        <input type="hidden" name="<?php echo $itemContent; ?>[]" id="<?php echo $itemContentId?>" value="<?php echo esc_attr($itemContentValue); ?>"/>
                        <a href="javascript:APLEditorWidget.showEditor('<?php echo $itemContentId?>');" class="button home-featured-item-show-editor"><?php _e( 'Edit content', 'apollo' ) ?></a>
                    </div>
                </div>
            </details>

            <?php
        } endforeach;
    else :
        ?>
        <details class="apl-item-inner" open>
            <summary></summary>
            <div>
                <?php echo $removeItem; ?>
                <div>
                    <label><?php _e('Hide item title: ', 'apollo'); ?></label>
                    <input class="hide-item-title widefat" type="checkbox">
                    <input class="hidden" name="<?php echo $hideItemTitle; ?>[]" class="hide-item-title-value" ?>
                </div>
                <div>
                    <label><?php _e('Item title: ', 'apollo'); ?></label>
                    <input name="<?php echo $itemTitleName; ?>[]" class="item-title widefat" />
                </div>
                <div>
                    <label><?php _e('Item title url: ', 'apollo'); ?></label>
                    <input name="<?php echo $itemTitleUrl; ?>[]" class="item-title-url widefat" />
                </div>
                <div>
                    <label><?php _e('Image:', 'apollo'); ?> <a class="apl-widget-set-item-image"><?php _e("Browse an image","apollo") ?></a></a></label>
                    <div class="apl-widget-img-wrapper hidden">
                        <input type="hidden" name="<?php echo $itemImageUrl; ?>[]" class="item-image-url widefat" />
                        <span class="apl-widget-delete-item-image"><i class="fa fa-times"></i></span>
                        <img class="apl-widget-item-image widefat" />
                    </div>
                </div>
                <div>
                   <input type="hidden" name="<?php echo $itemContent; ?>[]" id="<?php echo "home-featured-item-content-$this->number-0" ?>"/>
                    <a href="javascript:APLEditorWidget.showEditor('<?php echo "home-featured-item-content-$this->number-0" ?>');" class="button home-featured-item-show-editor"><?php _e( 'Edit content', 'apollo' ) ?></a>
                </div>
            </div>
        </details>
    <?php
    endif;
    ?>
</div>
<p>
    <a class="widget-add-more-item button"><?php _e('Add more', 'apollo'); ?></a>
    <span><?php echo __("Maximum is 4 items") ?></span>
</p>
