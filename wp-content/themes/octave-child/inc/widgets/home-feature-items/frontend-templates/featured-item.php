<?php
$totalItems = count($instance['item-title']);
$blockName = "block-$totalItems-item";
$classBlockTitle = of_get_option(Apollo_DB_Schema::_FEATURE_BLOCK_TITLE_STYLE, '');
?>

<div class="section-block <?php echo isset($instance['has-underline']) ? 'has-underline ' : ''; echo $blockName?>">
    <?php if(!empty($instance['title'])) : ?>
        <h3 class="block__title <?php echo $classBlockTitle?>"><span class="fea-ttl__text"><?php echo $instance['title']; ?></span></h3>
    <?php endif; ?>
    <?php
    if (!empty($instance['item-title'])) : ?>
        <div class="block__content flex-row  <?php echo $totalItems > 1 ? 'equal-md-up' : '' ?>" >
            <?php
            foreach ($instance['item-title'] as $key => $value) : ?>
                <div class="equal">
                    <div class="featured-item">
                        <?php
                        $itemUrl = isset($instance['item-url'][$key]) ? $instance['item-url'][$key] : '';

                        if($itemUrl === ''){
                            $itemUrl  = 'javascript:void(0);';
                        }
                        else{
                            if(!preg_match("@^http://@i",$itemUrl) && !preg_match("@^https://@i",$itemUrl)){
                                $itemUrl = 'http://'.$itemUrl;
                            }
                        }

                        $imageUrl = isset($instance['item-image-url'][$key]) ? $instance['item-image-url'][$key] : '';
                        $itemContent = isset($instance['item-content'][$key]) ? $instance['item-content'][$key] : '';
                        $hidItemTitle = (isset($instance['hide-item-title'][$key]) && $instance['hide-item-title'][$key] === 'on') ? 'on' : 'off'
                        ?>
                        <?php if(!empty($imageUrl)){ ?>
                        <a  class="featured__thumbnail" href="<?php echo $itemUrl; ?>">
                            <img src="<?php echo $imageUrl; ?>">
                        </a>
                        <?php }?>
                        <div class="featured__description">
                            <?php if(!empty($value) && $hidItemTitle === 'off') : ?>
                                <a href="<?php echo $itemUrl; ?>" title="<?php echo $value; ?>" class="featured__title"><?php echo $value?></a>
                            <?php endif; ?>
                            <?php if (!empty($itemContent)) : ?>
                                <?php echo do_shortcode($itemContent); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?php
            endforeach;
            ?>
        </div>
    <?php endif; ?>
</div>