<?php

/*@ticket #16782: 0002308: Calendar Month search widget - Month view layout */

class Oc_Search_Event_Widget {
    /**
     * Oc_Search_Event_Widget constructor.
     */
    public function __construct() {
        add_filter('apl_add_search_widget_month_view', array($this, 'apl_add_search_widget_month_view'));
    }

    public function apl_add_search_widget_month_view(){

        if (of_get_option('apl_search_by_month_view', false)) {?>
        <div data-date-format="mm-dd-yy" class="s-rw el-blk" id="apl-search-widget-month-view"></div>
        <!--@ticket #19211: Artsopolis "Octave" Widescreen Theme - Load calendar grid at the same time as the rest of the widget -->
        <div class="apl-loading-calendar">
            <span><i class="fa fa-spinner fa-spin fa-3x"></i></span>
       </div>

        <?php };
    }
}

new Oc_Search_Event_Widget();