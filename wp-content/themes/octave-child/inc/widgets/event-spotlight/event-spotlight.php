<?php

require_once APOLLO_INCLUDES_DIR. '/widgets/Apollo_Widget.php';

class apollo_event_spotlight_widget extends WP_Widget {
    private $adminTemplatePath = '';
    public function __construct() {
        $this->adminTemplatePath = dirname(__FILE__). '/admin.php';
        parent::__construct(
            
            // ID of widget
            'apollo_event_spotlight_widget',

            // Widget name 
            __('Event Spotlight Widget', 'apollo'),

            // Widget description
            array( 'description' => __( 'Event Spotlight Widget', 'apollo' ), )
        );
    }

    public function form($instance)
    {
        if (!file_exists($this->adminTemplatePath)) {
            return '';
        }

        ob_start();
        include $this->adminTemplatePath;
        $html = ob_get_contents();
        ob_end_clean();
        echo $html;
    }

    public function widget($args, $instance)
    {
        if ( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EVENT_PT )
            && in_array(of_get_option( Apollo_DB_Schema::_SPOT_TYPE ), array('medium','slider'))
            && is_front_page() ) {
            // Count again the expired time
            Apollo_App::getTemplatePartCustom(APOLLO_TEMPLATES_DIR. '/medium-slider.php',array(),false,true);
        }
        else{
            echo '';
        }
    }

    public function update($new_instance, $old_instance)
    {
        return parent::update($new_instance, $old_instance);
    }
}  