<?php

class OC_Admin {
    public function __construct()
    {
        $this->init();
    }

    private function init() {
        require_once SONOMA_ADMIN_DIR. '/theme-options/theme-options.php';
    }
}

new OC_Admin();