<?php
/**
 * Load assets.
 *
 * @author        vulh
 * @category    Admin
 * @package    inc/admin
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

if (!class_exists('SMAdminAssets')) :

    /**
     * SMAdminAssets Class
     */
    class SMAdminAssets
    {

        public function __construct()
        {

            add_action('admin_enqueue_scripts', array($this, 'admin_styles'));
            add_action('admin_enqueue_scripts', array($this, 'admin_scripts'));
        }

        public function admin_scripts()
        {
            global $typenow;

            $screen = get_current_screen();


            wp_register_script('admin-js', SONOMA_ADMIN_JS_URL . '/admin.js', array('jquery'));

            wp_enqueue_script('admin-js');
            $w = 2 * get_option('circle_image_size_w');
            $h = 2 * get_option('circle_image_size_h');
            wp_localize_script('admin-js', 'SM_ADMIN', array(
                    'error' => sprintf(__('Image must be equal or larger than %sx%s', 'apollo'), $w, $h),
                    'gallery_error' => sprintf(__('The selected images must be equal or larger than %sx%s. ' . "\n" .' The images in red below  do NOT meet the corrected dimensions in this gallery.', 'apollo'), $w, $h),
                    'circle_image_size_w' => $w,
                    'circle_image_size_h' => $h
                )
            );

        }

        public function admin_styles()
        {
            wp_enqueue_style('admin-css', SONOMA_ADMIN_CSS_URL . '/admin.css');
        }

    }
endif;

return new SMAdminAssets();