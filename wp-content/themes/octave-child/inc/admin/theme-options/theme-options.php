<?php

class OC_Theme_Options
{

    protected $_oc_array_fields_hidden;
    const DEFAULT_TOP_BAR_HEIGTH = 70;
    const _BLOG_FEATURED_IMAGE_DEFAULT_MAX_HEIGHT = 800;

    /**
     * OC_Theme_Options constructor.
     */
    public function __construct()
    {

        $this->_oc_array_fields_hidden = array(
            __('Navigation', 'apollo'),
            Apollo_DB_Schema::_NAVIGATION_BAR_STYLE,
        );

        add_filter('_apl_hide_fields_unnecessary', array($this, 'hide_fields_unnecessary'), 99, 1);
        add_filter('_add_blog_display_style', array($this, 'add_blog_display_style'), 99, 1);
        add_filter('_apl_custom_primary_css',  array($this, 'custom_primary_css'), 99, 2);
        add_filter('_load_primary_color',  array($this, '_load_primary_color'), 99, 1);
        add_filter('apl_custom_top_header_options',  array($this, 'custom_top_header_options'), 99, 1);
        add_filter('apl_custom_masthead_options',  array($this, 'custom_masthead_options'), 99, 1);
        add_filter('apl_custom_navigation_options',  array($this, 'custom_navigation_options'), 99, 1);
        add_filter('apl_custom_home_setting_options',  array($this, 'custom_home_setting_options'), 99, 1);
        add_filter('apl_add_geo_location_option', array($this, 'add_geo_location_option'), 99, 1);
        add_filter('apl_filter_by_my_location_option', array($this, 'apl_filter_by_my_location_option'), 99, 1);
        add_filter('apl_add_checkbox_my_location', array($this, 'add_checkbox_my_location'), 99, 1);
//        add_filter('apl_custom_header_options',  array($this, 'custom_header_options'), 99, 1);
        add_filter('apl_custom_event_options',  array($this, 'apl_custom_event_options'), 99, 1);

        /*@ticket #16782: 0002308: Calendar Month search widget - Month view layout */
        add_filter('apl_add_custom_options_for_search_event',  array($this, 'apl_add_custom_options_for_search_event'), 99, 1);
        add_filter('apl_add_google_map_selected_marker_option',  array($this, 'addSelectedMarkerOption'), 99, 1);
        add_filter('apl_add_event_listing_tile_view_option', array($this, 'addListingTileViewOption'), 99, 1);
        add_filter('apl_event_listing_custom_default_view_type', array($this, 'customListingDefaultViewType'), 99, 1);
        add_filter('apl_event_listing_filter_icon_display', array($this, 'customDisplayListingIcon'), 99, 1);

        /*@ticket #16983: Octave Theme - [CF] 20180730 - Apply horizontal style for the home feature event block on the home page*/
        add_filter('apl_add_max_total_event',  array($this, 'apl_add_max_total_event'), 99, 1);

        /*@ticket #16886: Octave Theme - [CF] 20180730 - Three columns options of feature events block*/
        add_filter('apl_featured_event_display_option', array($this, 'apl_featured_event_display_option'), 99, 1);

        /*@ticket #17250: Octave Theme - [Feature block] New feature block title - item 3, 4, 5, 6, 7*/
        add_filter('apl_add_option_feature_block_title_style', array($this, 'apl_add_option_feature_block_title_style'), 99, 1);
        add_filter('apl_display_icon_with_discount_url_option', array($this, 'displayIconWithDiscountUrlOption'), 99, 1);
        add_filter('apl_add_social_icon_style', array($this, 'addSocialIconStyle'), 99, 1);

        /*@ticket 17346 */
        add_filter('apl_enable_discount_text_for_event_form', array($this, 'apl_enable_discount_text_for_event_form'));

        /*@ticket #17304: 0002410: wpdev55 - Requirements part1 - [Page 5] Change order of buttons under summary text to*/
        add_filter('apl_add_option_map_it_button', array($this, 'apl_add_option_map_it_button'), 99, 1);
        add_filter('apl_add_option_add_to_calendar_button', array($this, 'apl_add_option_add_to_calendar_button'), 99, 1);

        /*@ticket #17347 */
        add_filter('apl_add_option_event_discount', array($this, 'apl_add_option_event_discount'), 99, 1);

        /*@ticket #17348 */
        add_filter('apl_add_option_each_event_discount_description', array($this, 'apl_add_option_each_event_discount_description'), 99, 1);

        /* @ticket: #17303 0002410: wpdev55 - Requirements part1 - [Page 5] Move Bookmark (save) icon after other share icons*/
        add_filter('apl_add_option_bookmark_button_location', array($this, 'apl_add_option_bookmark_button_location'), 99, 1);
        add_filter('apl_add_option_bookmark_button_type', array($this, 'apl_add_option_bookmark_button_type'), 99, 1);

        /*@ticket: #17349: 0002410: wpdev55 - Requirements part2 - [Event] Clock icon option on the listing */
        add_filter('apl_add_option_enable_clock_icon', array($this, 'apl_add_option_enable_clock_icon'), 99, 1);

        /*@ticket #17252: Octave Theme - [Event feature] New title style for the horizontal feature block - item 8, 9, 10 */
        add_filter('apl_add_option_row_title_style', array($this, 'apl_add_option_row_title_style'), 99, 1);
        add_filter('apl_add_option_home_featured_date_option', array($this, 'apl_add_option_home_featured_date_option'), 99, 1);

        /*@ticket #17821 0002466: Promo video homepage - New option for the home spotlight*/
        add_filter('apl_add_custom_spotlight_type', array($this, 'apl_add_custom_spotlight_type'), 99, 1);
        add_filter('apl_add_custom_spotlight_contain', array($this, 'apl_add_custom_spotlight_contain'), 99, 1);
        add_filter('apl_custom_item_per_row', array($this, 'apl_custom_item_per_row'), 99, 1);
        /*@ticket $18181: 0002410: wpdev55 - Modify the position of the 'discount' icon*/
        add_filter('apl_add_event_icons_position', array($this, 'apl_add_event_icons_position'), 99, 1);

        /**
         * @ticket #18509: 0002522: wpdev54 Customization - Display full width and height of the images on the slider.
         */
        add_filter('apl_home_slider_display_full', array($this, 'apl_home_slider_display_full'));

        /**
         * @ticket #18657: Change the layout structure for Artist, Organization, Venue
         */
        add_filter('apl_add_number_of_character_description', array($this, 'apl_add_number_of_character_description'), 99, 2);

        /** #Ticket #19050 */
        add_filter('apl_add_custom_display_options_type', array($this, 'apl_add_custom_display_options_type'), 99, 1);

        /**
         * @ticket #19131: Octave Theme - Change all detail page section labels same the section labels on the homepage - item 2
         */
        add_filter('oc_add_options_section_labels', array($this, 'oc_add_options_section_labels'), 99, 1);

        /** @Ticket #19723 */
        add_filter('apl_blog_featured_image_max_height', array($this, 'apl_blog_featured_image_max_height'), 99, 1);
    }

    public function apl_blog_featured_image_max_height($options) {
        $options[] = array(
            'name'     => __( 'Featured image max height', 'apollo' ),
            'desc'     => __("Default max height is 800px. Only apply for the 'One Column' style", "apollo"),
            'id'   => Apollo_DB_Schema::_BLOG_FEATURED_IMAGE_MAX_HEIGHT,
            'std'  => self::_BLOG_FEATURED_IMAGE_DEFAULT_MAX_HEIGHT,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );


        return $options;
    }


    /**
     * @param $displayOptions
     * @return array
     */
    public function apl_add_custom_display_options_type($displayOptions){
        $displayOptions['plain-text'] = __( 'Plain Text - Under the Presented by ...', 'apollo' );
        return $displayOptions;
    }


    /**
     * @param string $fieldName
     * @return string
     */
    public function hide_fields_unnecessary($fieldName = '')
    {
        if (in_array($fieldName, $this->_oc_array_fields_hidden)) {
            return 'hidden';
        }
        return '';
    }

    /**
     * @return array
     * @ticket #16236: Apply full-screen template with only column for the blog detail page
     */
    public function add_blog_display_style(){
        return array(
            'default'    => __( 'Default', 'apollo' ),
            'header-new'      => __( 'Date on the top', 'apollo' ),
            'one-column'    => __('One Column', 'apollo')
        );
    }

    /**
     * @return string
     * @ticket #16224: Octave Theme - Review the main color feature
     */
    public function _load_primary_color(){
        return SONOMA_ADMIN_DIR . '/theme-options/default-template/primary-color.less';
    }

    /**
     * @Ticket #16467 - Custom top header options
     * @param $octaveTopHeader
     * @return array
     */
    public function custom_top_header_options($octaveTopHeader) {

        $octaveTopHeader[] = array(
            'name'     => __( 'Top header height', 'apollo' ),
            'desc'     => __('Default height is 70px', 'apollo'),
            'id'   => OC_Common_Const::_OC_TOP_HEADER_HEIGHT,
            'std'  => self::DEFAULT_TOP_BAR_HEIGTH,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );


        return $octaveTopHeader;
    }

    function custom_masthead_options ($options) {
        $options[] = array(
            'name'        => __( 'Masthead Setting', 'apollo' ),
            'type'        => 'header_block'
        );

        /** @Ticket #18570 Menu mobile */
        $options[] = array(
            'name'     => __( 'Mobile masthead style', 'apollo' ),
            'id'   => Apollo_DB_Schema::_MOBILE_MENU_STYLE,
            'std'  => '',
            'type'      => 'select',
            'options'   => array(
                ''           => __( 'Default', 'apollo' ),
                'oct__hamburger-mobile'  => __( 'Hamburger', 'apollo' ),
            ),
            'preview' => true,
            'class'     => 'no-border inline',
        );

        return $options;
    }

    function custom_navigation_options ($options) {

        $options[] = array(
            'name'     => __( 'Desktop navigation style', 'apollo' ),
            'id'   => OC_Common_Const::_OC_TOP_HEADER_NAVIGATION_STYLE,
            'std'  => '',
            'type'      => 'select',
            'options'   => array(
                ''           => __( 'On the left sidebar (default)', 'apollo' ),
                'header-bar--right'           => __( 'On the right sidebar', 'apollo' ),
                'apl-default-header'           => __( 'Full-screen', 'apollo')
            ),
            'preview'   => true,
            'class'     => 'no-border inline',
        );

        $options[] = array(
            'name'     => __( 'Masthead group buttons style', 'apollo' ),
            'id'   => Apollo_DB_Schema::_MASTHEAD_GROUP_BUTTONS_STYLE,
            'std'  => '',
            'desc'  => __('Do not apply for the right sidebar', 'apollo'),
            'type'      => 'select',
            'options'   => array(
                ''                          => __( 'Default', 'apollo' ),
                'oc-masthead-group-icons'   => __( 'Icons', 'apollo' ),
            ),
            'preview'   => true,
            'class'     => 'no-border inline',
        );

        $options[] = array(
            'name'     => __( 'Mobile navigation style', 'apollo' ),
            'id'   => Apollo_DB_Schema::_MOBILE_NAVIGATION_STYLE,
            'std'  => 'oct__show-menu-item',
            'desc'  => __('Only apply for the "Hamburger" masthead style', 'apollo'),
            'type'      => 'select',
            'options'   => array(
                'oct__show-menu-box'   => __( 'Default (Display as a drop-down list)', 'apollo' ),
                'oct__show-menu-item'  => __( 'Display all parent menus', 'apollo' ),
                'oct__hide-menu-item'  => __( 'Suppress the menu', 'apollo' ),
            ),
            'preview' => true,
            'class'     => 'no-border inline',
        );

        return $options;
    }

    /**
     * @param $options
     * @return array
     */
    function custom_home_setting_options($options) {

        $options[] = array(
            'name'     => __( 'Spotlight height', 'apollo' ),
            'id'   => Apollo_DB_Schema::_SPOT_HEIGHT,
            'std'  => 768,
            'desc' => __("This config is only available if the Spotlight type is the Alternate Spotlight or Small Alternate Spotlight", 'apollo'),
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );

        $options[] = array(
            'name'     => __( 'Transparent masthead', 'apollo' ),
            'id'   => OC_Common_Const::_OC_TRANSPARENT_MASTHEAD_NAV,
            'std'  => 0,
            'desc' => __("This config is only available if the Spotlight type is the Alternate Spotlight", 'apollo'),
            'type'     => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        /*@ticket 17212 */
        $options[] = array(
            'name'       => __( 'Masthead transparent background', 'apollo' ),
            'id'        => OC_Common_Const::_OC_MASTHEAD_BACKGROUND,
            'type'      => 'color',
            'std'       => '#FFFFFF',
        );

        $options[] = array(
            'name'     => __( 'Transparent masthead level', 'apollo' ),
            'id'   => OC_Common_Const::_OC_TRANSPARENT_MASTHEAD_NAV_LEVEL,
            'std'  => OC_Common_Const::_OC_TRANSPARENT_MASTHEAD_NAV_LEVEL_DEFAULT,
            'desc' => sprintf(__("Default %d percent (min = 1, max = 100)", 'apollo'), OC_Common_Const::_OC_TRANSPARENT_MASTHEAD_NAV_LEVEL_DEFAULT),
            'type'     => 'number',
            'class' => 'apollo_input_number no-border inline',
            'attr'    => "min = 1 max=100",
        );

        return $options;
    }

    /**
     * @Ticket #16467 - Custom header options
     * @param $octaveHeader
     * @return array
     */
    public function custom_header_options($octaveHeader) {
        $octaveHeader[] = array(
            'name'      => __( 'Scroll with page', 'apollo' ),
            'id'        => OC_Common_Const::_OC_HEADER_SCROLL_WITH_PAGE,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );
        return $octaveHeader;
    }

    /**
     * Geo-Location option
     * @param $geoLocation
     * @return array
     */
    public function add_geo_location_option($geoLocation) {
        // Enable GEO-LOCATION Service
        $geoLocation[] = array(
            'name'      => __( 'Enable Geo-Location Service', 'apollo' ),
            'id'        => OC_Common_Const::_OC_ENABLE_GEO_LOCATION,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        $geoLocation[] = array(
            'name'     => __( 'Google map zooming', 'apollo' ),
            'desc'     => __( 'default 12', 'apollo' ),
            'id'   => OC_Common_Const::_OC_GOOGLE_MAP_ZOOM,
            'std'  => 12,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );

        return $geoLocation;
    }

    /**
     * Search widget filter by my location option
     * @param $option
     * @return array
     */
    public function apl_filter_by_my_location_option ($option)
    {
        $option[] = array(
            'name'      => __( 'Enable Filter Events By My Location', 'apollo' ),
            'id'        => OC_Common_Const::_OC_SEARCH_WIDGET_ENABLE_FILTER_BY_MY_LOCATION,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        return $option;
    }

    /**
     * @Ticket #16467
     * @param $css_content
     * @param $input
     * @return mixed
     */
    public function custom_primary_css($css_content, $input) {
        $ocTopHeaderHeight = $input[OC_Common_Const::_OC_TOP_HEADER_HEIGHT];
        $ocTopHeaderHeight = $ocTopHeaderHeight . 'px';
        $ocTopAdminBarHeaderHeight = $ocTopHeaderHeight + 32 . 'px';
        $css_content = str_replace( '$topHeaderHeight', $ocTopHeaderHeight, $css_content );
        $css_content = str_replace( '$ocTopAdminBarHeaderHeight', $ocTopAdminBarHeaderHeight, $css_content );

        $spotHeight = $input[Apollo_DB_Schema::_SPOT_HEIGHT];
        $css_content = str_replace( '$spotHeight', $spotHeight. 'px', $css_content );

        /** @Ticket #19723 */
        $ocBlogFeaturedImageMaxHeight = $input[Apollo_DB_Schema::_BLOG_FEATURED_IMAGE_MAX_HEIGHT];
        $ocBlogFeaturedImageMaxHeight = $ocBlogFeaturedImageMaxHeight . 'px';
        $css_content = str_replace('$ocBlogFeaturedImageMaxHeight', $ocBlogFeaturedImageMaxHeight, $css_content);

        /**
         * Override transparent level
         * @ticket #17186
         */
        $navTransparentLevel = $input[OC_Common_Const::_OC_TRANSPARENT_MASTHEAD_NAV_LEVEL]/100;

        /*@ticket #17212 */
        $mastheadBackgound = $input[OC_Common_Const::_OC_MASTHEAD_BACKGROUND];

        list($r, $g, $b) = sscanf($mastheadBackgound, "#%02x%02x%02x");

        $mastheadTransparentBackground = "rgba($r, $g, $b, $navTransparentLevel)";

        $css_content = str_replace( '$mastheadTransparentBackground', $mastheadTransparentBackground, $css_content );

        /*@ticket #17348 */
        $discountStyleOptions = $input[Apollo_DB_Schema::_EVENT_DISCOUNT_DESCRIPTION_FE_STYLE];
        $discountStyle = '';

        if (isset($discountStyleOptions['italic']) && $discountStyleOptions['italic'] === 'on'){
            $discountStyle .= " font-style: italic;";
        }
        if (isset($discountStyleOptions['bold']) && $discountStyleOptions['bold'] === 'on'){
            $discountStyle .= " font-weight: bold;";
        }

        $css_content = str_replace( '$discountDescription', $discountStyle, $css_content );

        return $css_content;
    }


    /**
     * @ticket #16609:  Merge the 'Add to Outlook' and 'Add to Google Calendar' icons into one popup window.
     * @return array
     */
    public function  apl_custom_event_options(){
        return array(
            'name'       => __( 'Outlook and Google Calendar icons style', 'apollo' ),
            'desc'       => '',
            'id'      => 'separation_group',
            'type'       => 'select',
            'class'   => 'no-border inline',
            'std'     => 'two_icon',
            'options' => array(
                'two_icon'   => __( 'Separation', 'apollo' ),
                'one_icon'      => __( 'In a group', 'apollo' ),
            )
        );
    }

    /**
     * @param $option
     * @return array
     */
    public function addSelectedMarkerOption($option) {
        $imagePath = get_template_directory_uri();
        $option[] = array(
            'name'      => __( 'Default google map selected maker icon', 'apollo' ),
            'desc'      => '',
            'id'        => OC_Common_Const::_OC_GOOGLE_MAP_SELECTED_MARKER_ICON,
            'std'       => '',
            'type'      => 'text',
            'class'     => 'no-border inline',
            'desc_img'  => '/wp-content/themes/apollo/assets/images/icon-location-selected.png',
            'icon-src' =>  $imagePath . '/assets/images/icon-location-selected.png',
        );
        return $option;
    }
    /**
     * @ticket #16782: 0002308: Calendar Month search widget - Month view layout
     * @return array
     */
    public function apl_add_custom_options_for_search_event()
    {
        $optionSearchEvent[] = array(
            'name'      => __( 'Enable search by month view ', 'apollo' ),
            'id'        => 'apl_search_by_month_view',
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        $optionSearchEvent[] = array(
            'name'      => __( 'Enable search by date range', 'apollo' ),
            'id'        => 'apl_search_by_date_range',
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        return $optionSearchEvent;
    }



    /**
     * @param $option
     * @return array
     */
    public function addListingTileViewOption($option) {
        $option[] = array(
            'name'      => __( 'Enable Tile view', 'apollo' ),
            'id'        => OC_Common_Const::_OC_ENABLE_LISTING_PAGE_TILE_VIEW,
            'std'       => 0,
            'desc'      => '',
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border octave-tile-view-option',
            'attr' => array(
                'test-radio-attribute' => 'test'
            ),
        );
        return $option;
    }

    /**
     * @param $event_default_view_type
     * @return array
     */
    public function customListingDefaultViewType($event_default_view_type) {
        return array(
            '1'    => __( 'Tile', 'apollo' ),
            '2'      => __( 'List (default)', 'apollo' ),
        );
    }

    public function customDisplayListingIcon($option) {
        $option[] =array(
            'name'       => __( 'Default filter icon', 'apollo' ),
            'desc'       => '',
            'id'      => OC_Common_Const::_OC_EVENT_LISTING_FILTER_ICON,
            'std'    => 1,
            'type'       => 'select',
            'preview'   => true,
            'class'   => 'apollo_input_number no-border inline',
            'options' => array(
                'only_icon'     => __('Only icon'),
                'icon_and_text' => __('Icon and text')
            )
        );
        return $option;
    }

    /**
     * @ticket #16983: Octave Theme - [CF] 20180730 - Apply horizontal style for the home feature event block on the home page
     * @return array
     */
    public function apl_add_max_total_event() {
        $maxTotalEvents[] = array(
            'name'     => __( 'Max total events', 'apollo' ),
            'desc'     => __('Only apply to the horizontal style','apollo'),
            'id'   => 'max_total_events',
            'std'  => 4,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );
        return $maxTotalEvents;
    }


    /**
     * @param $eventDisplayOption
     * @ticket #16886: Octave Theme - [CF] 20180730 - Three columns options of feature events block
     * @return array
     */
    public function apl_featured_event_display_option($eventDisplayOption){
        $newOption = array('three_columns' => __('Three Columns - The option requires the second option from the Default Layout Homepage ', 'apollo'));

        return array_merge($eventDisplayOption, $newOption);
    }


    /**
     * @ticket #17250: Octave Theme - [Feature block] New feature block title - item 3, 4, 5, 6, 7
     * @return array
     */
    public function apl_add_option_feature_block_title_style(){
        $option[] = array(
            'name'       => __( 'Feature block title style', 'apollo' ),
            'id'        => Apollo_DB_Schema::_FEATURE_BLOCK_TITLE_STYLE,
            'std'       => '',
            'type'      => 'select',
            'options'   => array(
                ''     => __( 'Align on the left', 'apollo' ),
                'fea-ttl__centered'  => __( 'Centered', 'apollo' ),
            ),
            'class'     => 'no-border inline',
            'preview'   => true
        );
        return $option;
    }

    /**
     * @param $option
     * @return array
     */
    public function addSocialIconStyle($option) {
        $option[] = array(
            'name'      => __( 'Icon style', 'apollo' ),
            'id'        => OC_Common_Const::_OC_SOCIAL_ICON_STYLE,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                0           => __( 'Dark color', 'apollo' ),
                1           => __( 'Native color', 'apollo' ),
            ),
            'class'     => 'no-border apollo_input_url',
        );
        return $option;
    }

    public function apl_enable_discount_text_for_event_form(){
        $option[] = array(
            'name'     => __( 'Enable discount text', 'apollo' ),
            'id'   => Apollo_DB_Schema::_EVENT_FORM_ENABLE_DISCOUNT_TEXT,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class' => 'no-border inline'
        );
        return $option;
    }

    /**
     * @ticket #17347
     * @param $option
     * @return array
     */
    public function apl_add_option_event_discount($option) {
        $options[] = array(
            'name'      => __( '-- Discount description', 'apollo' ),
            'type'      => 'header_block'
        );

        $options[] = array(
            'name'      => __( 'Enable Discount description', 'apollo' ),
            'id'        => Apollo_DB_Schema::_EVENT_ENABLE_DISCOUNT_DESCRIPTION,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'      => __( 'Discount text', 'apollo' ),
            'id'        => Apollo_DB_Schema::_EVENT_DISCOUNT_DESCRIPTION_TEXT,
            'std'       => '',
            'type'      => 'text',
            'class'     => 'no-border inline',
        );

        $options[] = array(
            'name'      => __( 'Discount image', 'apollo' ),
            'id'        => Apollo_DB_Schema::_EVENT_DISCOUNT_DESCRIPTION_IMAGE,
            'std'       => '',
            'type'      => 'text',
            'class'     => 'no-border inline apollo_input_url',
        );

        return $options;
    }

    public function displayIconWithDiscountUrlOption($option) {
        $option[] = array(
            'name'      => __('Displays all icons once an event has a discount URL', 'apollo'),
            'id'        => APL_Theme_Option_Site_Config_SubTab::_EVENT_DETAIL_DISPLAY_ALL_ICON,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );
        return $option;
    }

    /**
     * @ticket #17304: 0002410: wpdev55 - Requirements part1 - [Page 5] Change order of buttons under summary text to
     * @return array
     */
    public function apl_add_option_map_it_button(){
        $options[] = array(
            'name'      => __( 'Enable MAP IT button', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_MAP_IT_BUTTON,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        return $options;
    }

    /**
     * @ticket #17304: 0002410: wpdev55 - Requirements part1 - [Page 5] Change order of buttons under summary text to
     * @return array
     */
    public function apl_add_option_add_to_calendar_button(){
        $options[] = array(
            'name'      => __( 'Enable ADD TO CALENDAR button', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_ADD_TO_CALENDAR_BUTTON,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        return $options;
    }

    public function apl_add_option_each_event_discount_description(){
        $options[] = array(
            'name'      => __( '-- Each event discount description', 'apollo' ),
            'type'      => 'header_block'
        );

        $options[] = array(
            'name'      => __( 'Enable on the FE', 'apollo' ),
            'id'        => Apollo_DB_Schema::_EVENT_ENABLE_DISCOUNT_DESCRIPTION_FE ,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );
        $options[] = array(
            'name'      => __( 'Include discount URL', 'apollo' ),
            'id'        => Apollo_DB_Schema::_EVENT_INCLUDE_DISCOUNT_URL,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );
        $options[] = array(
            'name'      => __( 'Style', 'apollo' ),
            'id'        => Apollo_DB_Schema::_EVENT_DISCOUNT_DESCRIPTION_FE_STYLE,
            'type'      => 'multicheck',
            'wrapper_class' => 'bs-type-multi-cb apl-event-discount-description-fe-style',
            'options'   => array(
                'bold'           => __( 'Bold', 'apollo' ),
                'italic'           => __( 'Italic', 'apollo' ),
            ),
            'class'     => 'no-border inline',
        );

        return $options;
    }

    /**
     * @ticket: #17303 0002410: wpdev55 - Requirements part1 - [Page 5] Move Bookmark (save) icon after other share icons
     * @return array
     */
    public function apl_add_option_bookmark_button_location(){
        $option[] = array(
            'name'      => __( 'Bookmark button location', 'apollo' ),
            'id'        => Apollo_DB_Schema::_APOLLO_BOOKMARK_BUTTON_LOCATION,
            'std'       => 'after_comment',
            'type'      => 'select',
            'options'   => array(
                'after_comment'          => __( 'After the comment icon', 'apollo' ),
                'after_social'           => __( 'After the social icons', 'apollo' ),
            ),
            'class'     => 'no-border inline'
        );
        return $option;
    }

    /**
     * @ticket: #17303 0002410: wpdev55 - Requirements part1 - [Page 5] Move Bookmark (save) icon after other share icons
     * @return array
     */
    public function apl_add_option_bookmark_button_type(){
        $option[] = array(
            'name'      => __( 'ADD IT button type', 'apollo' ),
            'id'        => Apollo_DB_Schema::_APOLLO_BOOKMARK_BUTTON_TYPE,
            'std'       => 'icon_text',
            'type'      => 'radio',
            'options'   => array(
                'icon_text'          => __( 'Icon & Save text', 'apollo' ),
                'icon'           => __( 'Only Icon', 'apollo' ),
            ),
            'class'     => 'no-border'
        );
        return $option;
    }

    public function apl_add_option_enable_clock_icon(){
        $option[] = array(
            'name'      => __( 'Enable clock icon', 'apollo' ),
            'id'        => Apollo_DB_Schema::_APOLLO_EVENT_ENABLE_CLOCK_ICON,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1          => __( 'Yes', 'apollo' ),
                0           => __( 'No - Display discount icon instead', 'apollo' ),
            ),
            'class'     => 'no-border'
        );
        return $option;
    }

    /**
     * @ticket #17252: Octave Theme - [Event feature] New title style for the horizontal feature block - item 8, 9, 10
     * @return array
     */
    public function apl_add_option_row_title_style(){
        $option[] = array(
            'name'       => __( 'Row title style', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ROW_TITLE_STYLE,
            'std'       => '',
            'type'      => 'select',
            'options'   => array(
                ''     => __( 'Default' ),
                'item-cat__rotate-title'  => __( 'Rotate 90 degrees - This option is only available when Featured Event Display Option is horizontal', 'apollo' ),
            ),
            'class'     => 'no-border inline',
            'preview'   => true
        );
        return $option;
    }

    /**
     * @ticket #17252: Octave Theme - [Event feature] New title style for the horizontal feature block - item 8, 9, 10
     * @return array
     */
    public function apl_add_option_home_featured_date_option(){
        return array('below_tile' => __('Date below tile - This option is only available when Featured Event Display Option is horizontal', 'apollo'));
    }

    /**
     *  @ticket #17821 0002466: Promo video homepage - New option for the home spotlight
     * @return array
     */
    public function apl_add_custom_spotlight_type(){
        return array('customize_html' => __('Customize HTML', 'apollo'));
    }

    /**
     * @ticket #17821 0002466: Promo video homepage - New option for the home spotlight
     * @return array
     */
    public function apl_add_custom_spotlight_contain(){
        $option[] =  array(
            'name'     => '',
            'id'   => Apollo_DB_Schema::_HOME_SPOTLIGHT_CUSTOM_CONTAIN,
            'std'  => '',
            'type'     => 'editor',
            'class'     => 'clearfix hidden apl-custom-spotlight-contain',
        );

        return $option;
    }

    public function apl_custom_item_per_row() {

        $option[] = array(
            'name'     => __('Number items per row', 'apollo'),
            'id'   => Apollo_DB_Schema::_HOME_FEATURE_EVENT_ITEM_PER_PAGE,
            'std'  => 2,
            'type'      => 'select',
            'options'   => array(
                2       => __('2 items'),
                3       => __('3 items'),
                4       => __('4 items'),
            ),
            'class' => 'no-border inline',
            'desc'  => __('Only apply to the standard style')
        );
        return $option;
    }

    /**
     * @ticket #18181: 0002410: wpdev55 - Modify the position of the 'discount' icon
     * @return array
     */
    public function apl_add_event_icons_position(){
        return array('before_discount_text' => __('Before discount text', 'apollo'));
    }

    /**
     * @ticket #18509: 0002522: wpdev54 Customization - Display full width and height of the images on the slider.
     * @return array
     */
    public function apl_home_slider_display_full(){
        $option[] = array(
            'name'      => __( 'Enable display full width and height of the images on the slider', 'apollo' ),
            'id'        => Apollo_DB_Schema::_APL_HOME_SLIDER_DISPLAY_FULL,
            'std'       => 0,
            'type'      => 'radio',
            'desc'      => __('This config is only available if the Spotlight type is the Alternate Spotlight', 'apollo'),
            'options'   => array(
                1          => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border'
        );
        return $option;
    }

    /**
     * @param $defaultValue
     * @param $module
     * @return array
     */
    public function apl_add_number_of_character_description($option, $module){
        $option[] = array(
            'name'      => __( 'Number of characters description truncation', 'apollo' ),
            'id'        => OC_Common_Const::_OC_NUMBER_OF_CHARACTERS_DESCRIPTION_TRUNCATION . '_' . $module,
            'std'       => OC_Common_Const::_OC_NUMBER_OF_CHARACTERS_DESCRIPTION_TRUNCATION_DEFAULT,
            'type'      => 'text',
            'desc'     => sprintf( __( '%s characters by default', 'apollo' ), OC_Common_Const::_OC_NUMBER_OF_CHARACTERS_DESCRIPTION_TRUNCATION_DEFAULT),
            'class'     => 'apollo_input_number no-border inline'
        );

        return $option;
    }

    /**
     * @ticket #19131: Octave Theme - Change all detail page section labels same the section labels on the homepage - item 2
     * @return array
     */
    public function oc_add_options_section_labels(){
        $sectionLabels = array(
            'default'  => __( 'Default', 'apollo' ),
            'line'     => __( 'Title and line', 'apollo' )
        );

        // Enable Facebook Enable
        $options[] = array(
            'name'       => __('Section label on the detail pages', 'apollo' ),
            'id'        => OC_Common_Const::_OC_SECTION_LABELS,
            'std'       => 'default',
            'type'      => 'select',
            'options'   => $sectionLabels,
            'class'     => 'no-border inline',
            'preview'   => true
        );

        return $options;
    }
}

new OC_Theme_Options();