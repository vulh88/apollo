/**
 * Created by truong on 26/09/2016.
 */

(function ($) {

    var SonomaGallery = {
        handleAddSportLightGallery: function (e, $el) {
            e.preventDefault();

            var $image_gallery_ids = $('#event_image_gallery');
            var $event_images = $('#event_images_container ul.event_images');
            var attachment_ids = $image_gallery_ids.val();

            var event_gallery_frame = SonomaGallery.openMediaFrame($el);

            // When an image is selected, run a callback.
            event_gallery_frame.on('select', function () {
                var selection = event_gallery_frame.state().get('selection');
                var error = false;

                selection.map(function (attachment) {
                    attachment = attachment.toJSON();
                    if (attachment.height < SM_ADMIN.circle_image_size_h || attachment.width < SM_ADMIN.circle_image_size_w) {
                        $('.media-frame-content li[data-id = ' + attachment.id + ']').addClass('attachment-error');
                        error = true;
                    } else if (attachment.id) {
                        attachment_ids = attachment_ids ? attachment_ids + "," + attachment.id : attachment.id;

                        $event_images.append('\
                        <li class="image" data-attachment_id="' + attachment.id + '">\
                                <img src="' + attachment.url + '" />\
                                <ul class="actions">\
                                        <li><a href="#" class="delete" title="' + $el.data('delete') + '">' + $el.data('text') + '</a></li>\
                                </ul>\
                        </li>');
                    }
                });

                if (error) {
                    alert(SM_ADMIN.gallery_error);
                    event_gallery_frame.open();
                }

                $image_gallery_ids.val(attachment_ids);
            });
        },
        openMediaFrame: function (el) {
            // Create the media frame.
            var media_frame = wp.media.frames.event_gallery = wp.media({
                // Set the title of the modal.
                title: el.data('choose'),
                button: {
                    text: el.data('update')
                },
                library: {
                    type: 'image'
                },
                multiple: true
            });

            // Finally, open the modal.
            media_frame.open();

            return media_frame;
        },
        bindEventToElement: function () {
            $('.add_spotlight_images').on('click', 'a', function (event) {
                SonomaGallery.handleAddSportLightGallery(event, $(this));
            });
        }
    };

    /*@ticket #17821 0002466: Promo video homepage - New option for the home spotlight */
    var handleCustomHomeSpotlight = function () {
        var spotlight = $('.apl-spot-type input[type="radio"]:checked');
        if(spotlight.length){
            if (spotlight.val() == 'customize_html'){
                $('.apl-spot-type .explain').addClass("hidden");
                $('.apl-custom-spotlight-contain').removeClass("hidden");
            }
            else{
                $('.apl-spot-type .explain').removeClass("hidden");
                $('.apl-custom-spotlight-contain').addClass("hidden");
            }
        }
    };

    $(document).ready(function () {
        SonomaGallery.bindEventToElement();

        $('.wp-admin').off('click', '.widget-add-more-item').on('click', '.widget-add-more-item', function (e) {
            var current = $(e.currentTarget);
            var listItem = current.closest('.widget-content').find('.list-items-wrap');
            var itemInner = listItem.find('.apl-item-inner');
            if (itemInner.length > 0 && itemInner.length < 4) {
                var itemHtml = $(itemInner[itemInner.length - 1]).clone();
                $(itemHtml).find('input').val('');
                $(itemHtml).find('textarea').val('');
                $(itemHtml).find('img').attr("src", '');
                $(itemHtml).find('.apl-widget-img-wrapper').addClass('hidden');
                $(itemHtml).find(".hide-item-title").prop('checked', false);

                /*@ticket #17853*/
                /*generate item content id for editor (id = lastEditor++) */
                var itemtContent = $(itemHtml).find("[id^='home-featured-item-content']");
                if(itemtContent.attr('id')){
                    var itemtContentId = itemtContent.attr('id').split("-")
                    var newId = parseInt(itemtContentId.pop()) + 1;
                    itemtContentId = itemtContentId.join('-');
                    itemtContentId = itemtContentId + '-' + newId;
                    itemtContent.attr('id',   itemtContentId);

                    var showEditor = $(itemHtml).find(".home-featured-item-show-editor");
                    var editorUrl = "javascript:APLEditorWidget.showEditor('" + itemtContentId + "')";
                    showEditor.attr('href', editorUrl);
                }

                listItem.append(itemHtml);
            }
            else{
                alert("Maximum is 4 items");
            }
        });

        $('.wp-admin').off('click', '.list-items-wrap .widget-del').on('click', '.list-items-wrap .widget-del', function (e) {
            var current = $(e.currentTarget);
            var messageConfirm = current.attr('data-confirm');
            if (confirm(messageConfirm)) {
                var currentItem = current.closest('.apl-item-inner');
                if (currentItem.length > 0) {
                    currentItem.remove();
                }
            }
        });

        $('.wp-admin').off('click', '.apl-widget-set-item-image').on('click', '.apl-widget-set-item-image', function (e) {
            e.preventDefault();
            var parent = $(this).parent().parent();
            var img = parent.find('.apl-widget-item-image');
            var input = parent.find('.item-image-url');
            var imgWrapper = parent.find('.apl-widget-img-wrapper');
            wp.media.editor.send.attachment = function(props, attachment) {

                if(attachment.sizes && attachment.sizes.thumbnail && attachment.sizes.thumbnail.url){
                    img.attr("src",attachment.sizes.thumbnail.url);
                }
                else{
                    img.attr("src",attachment.url);
                }

                input.val(attachment.url);
                imgWrapper.removeClass('hidden');
            };
            wp.media.editor.open($(this));
            return false;
        });

        $('.wp-admin').off('click', '.apl-widget-delete-item-image').on('click', '.apl-widget-delete-item-image', function (e) {
            e.preventDefault();
            var self = $(this);
            var parent = $(this).parent(); // class img-wrapper
            var img = parent.find('.apl-widget-item-image');
            var input = parent.find('.item-image-url');
            img.attr("src", '');
            input.val('');
            parent.addClass('hidden');
        });

        var tileOption = $('.octave-tile-view-option');
        if (tileOption.length > 0) {
            var listDefaultView = tileOption.find('#_apollo_theme_options-oc_enable_listing_page_tile_view-0');
            var viewTypeOptions = $('#_event_default_view_type');
            if (viewTypeOptions.length > 0) {
                var html = viewTypeOptions.find('option[value="1"]');
                var attrData = '<option value="1">'+ html.html() +'</option>';
                tileOption.attr('data-default-list-view', attrData);
                if (listDefaultView.is(":checked")) {
                    html.remove();
                }
            }
        }

        $('.octave-tile-view-option').off('click', 'input[type="radio"]').on('click', 'input[type="radio"]', function (e) {
            var current = $(e.currentTarget);
            var viewTypeOptions = $('#_event_default_view_type');
            if (viewTypeOptions.length > 0) {
                var parent = current.closest('.octave-tile-view-option');
                var htmlOption = parent.attr('data-default-list-view');
                if (current.val() == 1) {
                    viewTypeOptions.prepend(htmlOption);
                } else {
                    viewTypeOptions.find('option[value="1"]').remove();
                }
            }
        });

        handleCustomHomeSpotlight();

        $('.wp-admin').off('click', '.apl-spot-type input[type="radio"]').on('click', '.apl-spot-type input[type="radio"]', function (e) {
            handleCustomHomeSpotlight();
        });

        $('.wp-admin').off('click', '.hide-item-title').on('click', '.hide-item-title', function (e) {
            var value = $(this).is( ':checked' ) ? 'on': 'off';
            $(this).parent().find('.hide-item-title-value').val(value);
        });

    });
})(jQuery);