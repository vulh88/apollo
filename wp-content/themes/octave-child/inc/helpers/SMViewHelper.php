<?php

class SMViewHelper
{
    public static function smGetTemplatePartCustom( $file, $model, $return = false ) {

        $fileBasename = basename($file);

        if ( !file_exists($file) && file_exists( get_stylesheet_directory() . '/' . $fileBasename . '.php' ) ) {
            $file = get_stylesheet_directory() . '/' . $fileBasename . '.php';
        }elseif ( !file_exists($file) && file_exists( get_template_directory() . '/' . $fileBasename . '.php' ) ){
            $file = get_template_directory() . '/' . $fileBasename . '.php';
        }
        ob_start();
        require( $file );
        $data = ob_get_clean();

        if ( $return ){
            return $data;
        }
        echo $data;
    }

    public static function renderTemplate($type,$data ){

        $file = SONOMA_MODULES_DIR.'/'.$type.'/templates/'.$type.'.php';
        self::smGetTemplatePartCustom($file,$data);
    }
    
    public static function smGetTemplatePart() {
        if (is_front_page()) {
            get_template_part('partials/content-home');
        } elseif(Apollo_App::isBlogPostsPage() ) {
            if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_BLOG_POST_PT ) ) {
                wp_safe_redirect( '/' );
            }
            get_template_part('single');
        } elseif(is_page() && !is_front_page()) {
            get_template_part('page');
        } elseif(get_query_var('_apollo_public_art_map') === 'true') {
            set_query_var('_apollo_public_art_search','true');
            get_template_part('custom-post-type');
        } else if (is_tax()) {
            get_template_part('taxonomy');
        }
        else {
            set_query_var('_sm_custom_post_type', true);
            get_template_part('custom-post-type');
        }
    }
}