<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

require_once SONOMA_MODULES_DIR.'/abstract-single-model.php';
require_once SONOMA_MODULES_DIR.'/abstract-list-model.php';
require_once SONOMA_MODULES_DIR.'/admin-media-setting.php';
require_once SONOMA_HELPER_DIR.'/view.php';

class OC_Module_Init
{

    public function __construct($type){

        $isAvailableModule = Apollo_App::is_avaiable_module($type);

        if ($type == OC_Common_Const::_OC_SINGLE_CONTENT_POST_TYPE) {
            global $post;

            if ($post->post_type === Apollo_DB_Schema::_EDUCATOR_PT || $post->post_type === Apollo_DB_Schema::_PROGRAM_PT){
                $isAvailableModule = Apollo_App::is_avaiable_module(Apollo_DB_Schema::_EDUCATION);
            }
            else{
                $isAvailableModule = Apollo_App::is_avaiable_module($post->post_type);
            }

            $type = $post->post_type;
        }
        $file = sprintf(SONOMA_MODULES_DIR. '/'. $type . '/'. 'init.php');

        if (!file_exists($file) ||  ( is_single() && !$isAvailableModule ) ) {
            _e('Page not available.', 'apollo');
        }
        else {
            require_once $file;
        }

    }

}
