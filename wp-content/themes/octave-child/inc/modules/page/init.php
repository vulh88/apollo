<?php

class OC_Page_Module
{
    public function __construct(){

        $this->renderPageTemplate();
    }

    public function renderPageTemplate(){
        if(Apollo_App::isDashboardPage()) {
            $file = SONOMA_MODULES_DIR . '/page/templates/dashboard.php';
        }
        else if ( get_query_var('_apollo_page_blog' ) == 'true' ) {
            if ( ! Apollo_App::is_avaiable_module( 'post' ) ) {
                wp_safe_redirect( '/' );
            }
            $file = APOLLO_TEMPLATES_DIR.'/pages/blog.php';
        }
        else {
            $file = APOLLO_TEMPLATES_DIR . '/pages/normal.php';
        }
        if (file_exists($file)) {
            OC_View_Helper::smGetTemplatePartCustom($file, array());
        }
    }

}

new OC_Page_Module();