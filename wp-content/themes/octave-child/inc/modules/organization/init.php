<?php

/**
 * Class OC_Organization_Module
 */
class OC_Organization_Module
{
    public function __construct()
    {
        $this->renderPageTemplate();
    }

    public function renderPageTemplate()
    {
        if (is_single()) {
            require_once __DIR__ . '/inc/single-model.php';
            $service = new OC_Organization_Model();
            $file = SONOMA_MODULES_DIR . '/organization/templates/single.php';
        }
        else { // Listing or search page
           /* require_once __DIR__ . '/inc/list-model.php';
            $service = new OC_Organizations_Model();*/
            $file = SONOMA_MODULES_DIR . '/organization/templates/list.php';
        }

        if (file_exists($file)) {
            OC_View_Helper::smGetTemplatePartCustom($file, true);
        }
    }
}

new OC_Organization_Module();