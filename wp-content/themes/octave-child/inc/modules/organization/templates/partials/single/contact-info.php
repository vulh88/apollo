<?php

/**
 * @Ticket #18657
 * Only available on the Octave theme
 */


if (!empty($data['email']) || !empty($data['web']) || !empty($data['blog']) || !empty($data['facebook'])
    || !empty($data['twitter']) || !empty($data['inst']) || !empty($data['linked']) || !empty($data['pinterest'])
    || !empty($data['donate']) || !empty($data['address']) || !empty($secondAddress)
    || !empty($data['phone']) || !empty($data['fax'])
    || !empty($data[Apollo_DB_Schema::_ORG_DISCOUNT_URL]) || $addItButton) : ?>

    <div class="blog-bkl">
        <div class="a-block">
            <?php echo Apollo_App::renderSectionLabels(__('Contact information', 'apollo')); ?>
            <?php include(APOLLO_TEMPLATES_DIR . '/content-single/org/social.php'); ?>
        </div>
    </div>
<?php endif ?>