<?php
/** @Ticket #13137 */
$image = $artist->get_image_with_option_placeholder('medium', array(),
    array(
        'aw' => true,
        'ah' => true,
    ),
    'normal', '');

?>
<div class="el-blk">
    <div class="art-pic <?php echo !$image ? 'no-place-holder-cate' : ''; ?>"><?php echo $image; ?></div>
    <div class="art-desc apl-internal-content">
        <?php
        $numberCharacter = of_get_option(OC_Common_Const::_OC_NUMBER_OF_CHARACTERS_DESCRIPTION_TRUNCATION . '_' . Apollo_DB_Schema::_ARTIST_PT, OC_Common_Const::_OC_NUMBER_OF_CHARACTERS_DESCRIPTION_TRUNCATION_DEFAULT);
        $fullContent = $artist->get_full_content();
        $textTruncation = Apollo_App::getStringByLength( $fullContent, $numberCharacter, true, true);
        $artist->the_short_desc($textTruncation, $fullContent, '_ed_sum_short', '_ed_sum_full');
        ?>
    </div>
</div>