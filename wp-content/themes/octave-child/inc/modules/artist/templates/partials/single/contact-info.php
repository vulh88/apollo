<?php
/**
 * @Ticket #18657
 * Only available on the Octave theme
 */
$addItBtn = $artist->renderBookmarkBtn(array('class' => 'btn-bm btn btn-b'));

if ($email || $website || $blog || $fb || $tw || $ins || $lk || $pr || $contact_info || $addItBtn): ?>
    <div class="blog-bkl">
        <div class="a-block">
            <?php echo Apollo_App::renderSectionLabels(__('Contact information', 'apollo')); ?>
            <?php include_once (APOLLO_TEMPLATES_DIR . '/content-single/artists/social.php'); ?>
        </div>
    </div>
<?php endif ?>