<?php

/**
 * Class OC_Artist_Module
 */
class OC_Artist_Module
{
    public function __construct()
    {
        $this->renderPageTemplate();
    }

    public function renderPageTemplate()
    {
        if (is_single()) {
            require_once __DIR__ . '/inc/sing-model.php';
            $service = new OC_Artist_Model();
            $file = SONOMA_MODULES_DIR . '/artist/templates/single.php';
        }
        else if (get_query_var('_apollo_artist_timeline') === 'true'){
            $file = SONOMA_MODULES_DIR . '/artist/templates/timeline.php';
        }
        else { // Listing or search page
           /* require_once __DIR__ . '/inc/list-model.php';
            $service = new OC_Artists_Model();*/
            $file = SONOMA_MODULES_DIR . '/artist/templates/list.php';
        }

        if (file_exists($file)) {
            OC_View_Helper::smGetTemplatePartCustom($file, true);
        }
    }
}

new OC_Artist_Module();