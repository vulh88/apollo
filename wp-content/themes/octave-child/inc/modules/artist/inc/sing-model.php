<?php


class OC_Artist_Model extends Abstract_OC_Single_Model
{
    public function __construct()
    {
        /**
         * @ticket #18657: Change the layout structure for Artist, Organization, Venue
         */
        add_filter('oc_artist_change_general_detail_template', array($this, 'generalDetailTemplatePath'));
        add_filter('oc_artist_add_contact_info', array($this, 'contactInfoTemplatePath'));
    }

    /**
     * @return string
     */
    public function generalDetailTemplatePath(){
        return SONOMA_MODULES_DIR . '/artist/templates/partials/single/general-detail.php';
    }

    /**
     * @return string
     */
    public function contactInfoTemplatePath(){
        return SONOMA_MODULES_DIR . '/artist/templates/partials/single/contact-info.php';
    }
}