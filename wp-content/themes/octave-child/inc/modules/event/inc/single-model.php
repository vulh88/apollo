<?php


class OC_Event_Model
{

    private $generalHTMLContent;

    public function __construct()
    {
        /*@ticket: #17303 0002410: wpdev55 - Requirements part1 - [Page 5] Move Bookmark (save) icon after other share icons*/
        add_filter('apl_add_event_bookmark_icon_after_social_btn', array($this,'add_event_book_mark_icon_after_social_btn'), 10, 2);
    }


    /**
     * @param $bookmarkIcon
     * @param string $eventId
     * @return string
     */
    public function add_event_book_mark_icon_after_social_btn($bookmarkIcon, $eventId = '')
    {
        if (get_post_type() === Apollo_DB_Schema::_EVENT_PT
            && of_get_option(Apollo_DB_Schema::_APOLLO_BOOKMARK_BUTTON_LOCATION, 'after_comment') === 'after_social') {
            if (empty($eventId)) {
                return '';
            }
            $event = get_event($eventId);
            return '<span class="event-bookmark-icon">' . $event->renderBookmarkBtn(array('class' => 'btn btn-category add-it-btn'), true) . '</span>';
        }
        return '';
    }

}