<?php

/**
 * Class OC_Event_Module
 */
class OC_Event_Module
{
    public function __construct()
    {
        $this->renderPageTemplate();
    }

    public function renderPageTemplate()
    {
        if (is_single()) {
            require_once __DIR__ . '/inc/single-model.php';
            $service = new OC_Event_Model();
            $file = SONOMA_MODULES_DIR . '/event/templates/single.php';
        }
        else if (is_tax()) {
            /*require_once __DIR__ . '/inc/taxonomy-model.php';
            $service = new OC_Event_Taxonomy_Model();*/
            $file = SONOMA_MODULES_DIR . '/event/templates/taxonomy.php';
        }
        else { // Listing or search page
           /* require_once __DIR__ . '/inc/list-model.php';
            $service = new OC_Events_Model();*/
            $file = SONOMA_MODULES_DIR . '/event/templates/list.php';
        }

        if (file_exists($file)) {
            OC_View_Helper::smGetTemplatePartCustom($file, true);
        }
    }
}

new OC_Event_Module();