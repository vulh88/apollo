<?php

/**
 * Class OC_Blog_Module
 */
class OC_Blog_Module
{
    public function __construct()
    {
        $this->renderPageTemplate();
    }

    public function renderPageTemplate()
    {
        //@ticket #16236: Apply full-screen template with only column for the blog detail page
        $blogDisplayType = of_get_option(Apollo_DB_Schema::_BLOG_DISPLAY_STYLE,'default');

        if(is_single() && $blogDisplayType == 'one-column'){
            require_once __DIR__ . '/inc/sing-model.php';
            $model = new OC_Post_Model();
            $file = SONOMA_MODULES_DIR . '/post/templates/single-one-column.php';
        }
        elseif (is_single()) {
            require_once __DIR__ . '/inc/sing-model.php';
            $model = new OC_Post_Model();
            $file = SONOMA_MODULES_DIR . '/post/templates/single.php';
        }
        elseif (is_archive()) {
            /*require_once __DIR__ . '/inc/archive-model.php';
            $service = new OC_Archive_Model();*/
            $file = SONOMA_MODULES_DIR . '/post/templates/archive.php';
        }
        elseif (get_query_var( '_apollo_author_search' ) === 'true'){
            $file = SONOMA_MODULES_DIR . '/post/templates/authors.php';
        }
        else { // Listing or search page
           /* require_once __DIR__ . '/inc/list-model.php';
            $service = new OC_Posts_Model();*/
            $file = SONOMA_MODULES_DIR . '/post/templates/list.php';
        }

        if (file_exists($file)) {

            $model = isset($model) ?$model : true;

            OC_View_Helper::smGetTemplatePartCustom($file, $model);
        }
    }
}

new OC_Blog_Module();