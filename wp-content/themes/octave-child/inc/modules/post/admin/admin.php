<?php

/**
 * Class SM_Post_Admin
 */
class SM_Post_Admin
{
    public function __construct()
    {
        /*@todo: optimize code*/
        /* @ticket #16768: 0002309: BLOG - Sponsored Content of the blog */
        require_once APOLLO_ADMIN_DIR . '/post-types/meta-boxes/common/class-apollo-meta-box-post-sponsored-content.php';
        require_once APOLLO_ADMIN_DIR . '/post-types/meta-boxes/common/class-apollo-meta-box-common.php';
        add_action('add_meta_boxes', array($this, 'add_sponsored_content_meta_box'), 10);
        add_action('save_post', "Apollo_Meta_Box_Post_Sponsored_Content::save", 10, 2);
        if (defined('APOLLO_BLOG_NEWS_SITE_ENABLE') && APOLLO_BLOG_NEWS_SITE_ENABLE == get_current_blog_id()) {
            add_action('save_post', "Apollo_Meta_Box_Common::updateCopyToAnotherSite", 99, 2);
        }

    }
    /* @ticket #16768: 0002309: BLOG - Sponsored Content of the blog */
    public function add_sponsored_content_meta_box($post_type)
    {
        if ( in_array( $post_type, array( 'post' ) ) ) {
            add_meta_box('apollo-sponsored-content', __('Sponsored Content', 'apollo'), 'Apollo_Meta_Box_Post_Sponsored_Content::output', $post_type, 'normal', 'high');

            /** @Ticket #19481 */
            if (defined('APOLLO_BLOG_NEWS_SITE_ENABLE') && APOLLO_BLOG_NEWS_SITE_ENABLE == get_current_blog_id()) {
                add_meta_box('apollo-blog-data-to-another-site', __('Copy Data', 'apollo'), 'Apollo_Meta_Box_Common::outputCopyToAnotherSite', 'post', 'normal', 'high');
            }
        }
    }
}

new SM_Post_Admin();