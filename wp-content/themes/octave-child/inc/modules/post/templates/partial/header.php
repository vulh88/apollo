<?php
$arraySocial = array(
    'info' => $arrShareInfo,
    'id' => $post_id,
    'data_btns' =>
        array(
            'print',
            'sendmail',
        )
);
ob_start();
SocialFactory::social_btns($arraySocial); //
$shareBtnsOnDefault = ob_get_contents();
ob_clean();
$src = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'full');
$thumbnailCaption = get_the_post_thumbnail_caption($post_id);
?>

<div class="b-share-cat">
    <input name="eventid" value="<?php echo $post_id; ?>" type="hidden"/>
</div>
<?php if ($src && isset($src[0])): ?>
    <div class="blog-pic-detail allow-max-height">
        <div class="full-width-block blog-header-image <?php echo empty($thumbnailCaption) ? 'apl-only-credit' : ''?>">
            <div class="single-post-sponsored-content">
                <?php
                /*@ticket #7547 */
                $sponsoredContent = get_post_meta($post_id,'post-sponsored-content',true);
                ob_start();
                if ($sponsoredContent && $sponsoredContent === 'on'){
                    echo "<div  class='sponsored-content-text-single'>" .  __('SPONSORED CONTENT', 'apollo') . "</div>";
                }
                $sponsoredHtml = ob_get_clean();
                if (!empty($thumbnailCaption)) : ?>
                    <figure class="wp-caption apl-custom-figure">
                        <div class="blog-header-image--content">
                            <?php echo $sponsoredHtml; ?>
                            <img src="<?php echo $src[0]; ?>">
                        </div>
                        <figcaption class="wp-caption-text">
                            <p class="apl-caption-text"><?php echo esc_html($thumbnailCaption); ?> </p>
                            <?php if (function_exists('get_media_credit_html')) :
                                $creditText = get_media_credit_html(get_post_thumbnail_id($post_id));
                                if (!empty($creditText)) :
                                ?>
                                <p class="media-credit"><?php echo $creditText; ?></p>
                                <?php endif; ?>
                            <?php endif; ?>
                        </figcaption>
                    </figure>
                <?php
                else :
                    echo $sponsoredHtml;
                ?>
                    <div class="blog-header-image--content">
                        <img src="<?php echo $src[0]; ?>">
                    </div>
                    <?php if (function_exists('the_media_credit_html')) { ?>
                        <p class="media-credit"><?php the_media_credit_html(get_post_thumbnail_id($post_id)); ?></p>
                    <?php }
                endif;
                ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="bg-wrapper full-width-block">
    <div class="inner">
        <div class="blog-tt">
            <div class="blog-name">
                <h1 class="namedetail-txt"><?php echo get_the_title(); ?></h1>
                <?php
                /**
                 * @ticket #18403: Arts Education Customizations - [Blog detail page] - Add the option suppress the 'Excerpt' field - Item 5
                 */
                $enableExcerpt = of_get_option(APL_Theme_Option_Site_Config_SubTab::_BLOG_ENABLE_EXCERPT, 1);
                $excerpt = get_the_excerpt();
                if ($excerpt && $enableExcerpt) : ?>
                    <p><?php echo $excerpt ?></p>
                <?php endif; ?>
            </div>
        </div>
        <div class="b-share-cat">
            <?php echo $shareBtnsOnDefault; ?>
        </div>
        <div class="blog-category">
            <?php
            //if ($enableDate): ?>
            <div class="cate-blk parent-not-underline-hover apl-blog-date">
                <i class="fa fa-calendar fa-lg">&nbsp;&nbsp;&nbsp;</i><span style="  float: left;
                        padding: 5px 0px;"><?php echo Apollo_App::apl_date('M d, Y', get_post_time()) ?></span><a
                        class="slash">/</a>
            </div>
            <?php //endif; ?>

            <div class="cate-blk parent-not-underline-hover apl-blog-author">
                <i class="fa fa-user fa-lg">&nbsp;&nbsp;&nbsp;</i><?php echo the_author_posts_link() ?><a
                        class="slash">/</a>
            </div>

            <?php
            //if ($enableThumbUp || $enableCommentNumber):
            ?>
            <div class="cate-blk  parent-not-underline-hover apl-blog-thumbup">

                <?php
                /**
                 * @ticket #18768
                 */
                $enableThumbUp = of_get_option(APL_Theme_Option_Site_Config_SubTab::_BLOG_ENABLE_THUMBS_UP, 1);
                if ( $enableThumbUp ): ?>
                <div class="like parent-not-underline-hover"
                     id="_event_rating_<?php echo $post_id . '_' . 'post' ?>"
                     data-ride="ap-event_rating" data-i="<?php echo $post_id ?>"
                     data-t="post"><a href="javascript:void(0);" style="padding-top: 1px">
                        <span class="_count">&nbsp;</span></a>
                </div>
                <?php endif; ?>

                <?php //if ( $enableCommentNumber && comments_open($post_id) ): ?>
                <?php if (comments_open($post_id)) : ?>
                <div class="cate-blk  cmb apl-blog-cmb">
                    <i class="fa fa-comment fa-lg">&nbsp;&nbsp;</i>
                    <?php
                    $cn = get_comments_number(get_the_ID());
                    $cn = $cn && $cn < 10 ? '0' . $cn : $cn;
                    ?>
                    <a class="">
                        <?php
                        $default = sprintf('%s comment', '<span class="cm-num">(1)</span>');
                        echo sprintf(_n($default, '%s comments', $cn, 'apollo'), '<span class="cm-num">(' . number_format_i18n($cn) . ')</span>');
                        ?>
                    </a>
                </div>
                <?php  endif; ?>

            </div>
            <?php //endif; ?>
        </div>
    </div>
</div>

