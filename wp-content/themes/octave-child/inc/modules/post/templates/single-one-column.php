<?php
$blogCustomLabel = Apollo_App::getCustomLabelByModuleName(Apollo_DB_Schema::_BLOG_POST_PT);
$post_id = get_the_ID();
/**
 * @Ticket #18768
 */
$enableThumbUp = of_get_option(APL_Theme_Option_Site_Config_SubTab::_BLOG_ENABLE_THUMBS_UP, 1);

?>
<div class="breadcrumbs breadcrumbs-blogdetail">
    <ul class="nav">
        <li><a href="/"><?php _e('Home', 'apollo') ?></a></li>
        <li>
            <span><a href="<?php echo Apollo_App::getCustomUrlByModuleName(Apollo_DB_Schema::_BLOG_POST_PT); ?>"><?php echo $blogCustomLabel; ?></a></span>
        </li>
        <li> <?php the_category(', ') ?></li>
        <span style="float: left; margin-top: 3px; ;"><?php echo get_the_title(); ?></span>
    </ul>
</div>
<div class="blog-bkl apl-internal-content">
    <nav class="blog-list">
        <?php
        if (have_posts()): the_post();

            add_filter('excerpt_length', function () {
                return Apollo_Display_Config::MAX_SHORT_DESC;
            }, 999);

            add_filter('excerpt_more', function () {
                return '';
            });
            $title = html_entity_decode(get_the_title());
            $arrShareInfo = array(
                'url' => get_permalink(),
                'summary' => get_the_excerpt(),
                'title' => $title,
                'media' => wp_get_attachment_thumb_url(get_post_thumbnail_id($post_id))
            );

            ?>
            <li>
                <?php include(SONOMA_MODULES_DIR . '/post/templates/partial/header.php'); ?>

                <div class="blog-content">
                    <div class="blog-text-detail"><p><?php the_content() ?></p></div>
                </div>

                <?php
                $tags = get_the_tag_list('', ', ');
                if ($tags):
                    ?>
                    <div class="blog-tags">
                        <label><?php _e('Tags', 'apollo') ?>:</label>
                        <?php echo $tags ?>
                    </div>
                <?php endif; ?>

            </li>
        <?php endif; ?>

        <?php if (isset($shareBtnsOnDefault)): ?>
            <div class="b-share-cat"><?php echo $shareBtnsOnDefault; ?></div>
        <?php elseif (isset($shareButtons)): ?>
            <div class="b-share-cat blog-category social-blog">
                <?php if (isset($shareButtons)) echo $shareButtons ?>
            </div>
        <?php endif; ?>
    </nav>
</div>

<?php
/** @Ticket #13250 */
include APOLLO_TEMPLATES_DIR . '/content-single/common/jetpack-related-posts.php';
?>

<!-- @ticket #17546 0002398 - Recommendation Widgets - Display widget on the full-screen template -->
<div class="apl-recommend-post-bottom"><?php $model->generateRecommentPost(); ?></div>

<?php
/*@ticket #17681 */
$model->apl_render_blog_detail_footer();
?>

<div class="blog-bkl astro-featr">
    <?php
    // If comments are open or we have at least one comment, load up the comment template.
    if (comments_open() || get_comments_number()) {
        comments_template('/templates/comments.php');
    }
    ?>
</div>