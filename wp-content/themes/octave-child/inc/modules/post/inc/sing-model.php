<?php


class OC_Post_Model extends Abstract_OC_Single_Model
{
    public function __construct()
    {
        add_filter('apl_render_blog_detail_footer', array($this, 'apl_render_blog_detail_footer'), 10, 1);
    }

    /**
     * @ticket #17546 0002398 - Recommendation Widgets - Display widget on the full-screen template
     */
    public function generateRecommentPost(){
        $blogAreaData = get_option('widget_apollo_recommend_post_widget');
        $widgetIDs = Apollo_App::getWidgetId('apl-sidebar-post', 'apollo_recommend_post_widget');
        if(isset($widgetIDs[0]) && isset($blogAreaData[$widgetIDs[0]])){
            the_widget( 'apollo_recommend_post_widget', $blogAreaData[$widgetIDs[0]]);
        }
    }

    /**
     * Render custom widget in blog detail footer
     * @ticket #17681
     */
    public function apl_render_blog_detail_footer(){
        $customWidgetData = get_option('widget_apollo_custom_widget');
        $widgetIDs = Apollo_App::getWidgetId('apl-sidebar-post', 'apollo_custom_widget');
        foreach ($widgetIDs as $id) {
            if (isset($customWidgetData[$id]) && $customWidgetData[$id]['location'] === "blog_detail_footer") {
                the_widget('apollo_custom_widget', $customWidgetData[$id], array('templateLocation' => 'blog_detail_footer'));
            }
        }
    }
}