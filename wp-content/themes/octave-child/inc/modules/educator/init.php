<?php

/**
 * Class OC_Educator_Module
 */
class OC_Educator_Module
{
    public function __construct()
    {
        $this->renderPageTemplate();
    }

    public function renderPageTemplate()
    {
        if (is_single()) {
           /* require_once __DIR__ . '/inc/sing-model.php';
            $service = new OC_Educator_Model();*/
            $file = SONOMA_MODULES_DIR . '/educator/templates/single.php';
        }
        else { // Listing or search page
            /*require_once __DIR__ . '/inc/list-model.php';
            $service = new OC_Educators_Model();*/
            $file = SONOMA_MODULES_DIR . '/educator/templates/list.php';
        }

        if (file_exists($file)) {
            OC_View_Helper::smGetTemplatePartCustom($file, true);
        }
    }
}

new OC_Educator_Module();