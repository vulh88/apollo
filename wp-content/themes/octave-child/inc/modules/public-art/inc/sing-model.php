<?php


class OC_Public_Art_Model extends Abstract_OC_Single_Model
{
    public function __construct()
    {
        /**
         * @ticket #19025: wpdev54 Customization - Change layout the public art on the octave theme
         */
        add_filter('oc_public_art_change_general_detail_template', array($this, 'generalDetailTemplatePath'));
        add_filter('oc_public_art_add_contact_info', array($this, 'contactInfoTemplatePath'));
    }


    /**
     * @return string
     */
    public function generalDetailTemplatePath(){
        return SONOMA_MODULES_DIR . '/public-art/templates/partials/single/general-detail.php';
    }

    /**
     * @return string
     */
    public function contactInfoTemplatePath(){
        return SONOMA_MODULES_DIR . '/public-art/templates/partials/single/contact-info.php';
    }
}