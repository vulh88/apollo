<?php
/**
 * @ticket #19025: wpdev54 Customization - Change layout the public art on the octave theme
 */
?>
<div class="el-blk">
    <div class="art-pic">
        <?php echo $public_art->get_image('medium'); ?>
    </div>
    <div class="art-desc apl-internal-content">
        <?php
        $numberCharacter = of_get_option(OC_Common_Const::_OC_NUMBER_OF_CHARACTERS_DESCRIPTION_TRUNCATION . '_' . Apollo_DB_Schema::_PUBLIC_ART_PT, OC_Common_Const::_OC_NUMBER_OF_CHARACTERS_DESCRIPTION_TRUNCATION_DEFAULT);
        $fullContent = $public_art->get_full_content();
        $textTruncation = Apollo_App::getStringByLength($fullContent, $numberCharacter, true, true);
        $public_art->the_short_desc($textTruncation, $fullContent, '_ed_sum_short', '_ed_sum_full');
        ?>
    </div>
</div>