<?php

/**
 * @ticket #19025: wpdev54 Customization - Change layout the public art on the octave theme
 */
if (!empty($data['email']) || !empty($data['web'])
    || !empty($data['address']) || !empty($data['phone']) || $addItButton
    || !empty($medium_type) || !empty($data['date_created']) || !empty($data['dimension'])) : ?>

    <div class="blog-bkl">
        <div class="a-block">
            <?php echo Apollo_App::renderSectionLabels(__('Contact information', 'apollo')); ?>

            <?php include(APOLLO_TEMPLATES_DIR . '/content-single/public-art/social.php'); ?>

            <div class="el-blk">
                <?php if (!empty($medium_type)) { ?>
                    <p class="org-type medium"><?php _e('Medium type: ', 'apollo'); ?><?php echo $medium_type; ?></p>
                <?php } ?>

                <?php if (!empty($data['date_created'])) { ?>
                    <p><?php _e('Date created: ', 'apollo'); ?><?php echo $data['date_created'] ?></p>
                <?php } ?>

                <?php if (!empty($data['dimension'])) { ?>
                    <p><?php _e('Dimensions: ', 'apollo'); ?><?php echo $data['dimension'] ?></p>
                <?php } ?>
            </div>
        </div>
    </div>
<?php endif; ?>