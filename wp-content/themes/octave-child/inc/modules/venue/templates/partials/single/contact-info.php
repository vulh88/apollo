<?php

/**
 * @Ticket #18657
 * Only available on the Octave theme
 */

if (!empty($data['email']) || !empty($data['web']) || !empty($data['blog']) || !empty($data['facebook'])
    || !empty($data['twitter']) || !empty($data['inst']) || !empty($data['linked']) || !empty($data['pinterest'])
    || !empty($data['address']) || !empty($data['phone']) || !empty($data['fax'])
    || $addItButton) : ?>

    <div class="blog-bkl">
        <div class="a-block">
            <?php echo Apollo_App::renderSectionLabels(__('Contact information', 'apollo')); ?>
            <?php include(APOLLO_TEMPLATES_DIR . '/content-single/venue/social.php'); ?>
        </div>
    </div>
<?php endif ?>