<?php

/**
 * Class OC_Venue_Module
 */
class OC_Venue_Module
{
    public function __construct()
    {
        $this->renderPageTemplate();
    }

    public function renderPageTemplate()
    {
        if (is_single()) {
            require_once __DIR__ . '/inc/sing-model.php';
            $service = new OC_Venue_Model();
            $file = SONOMA_MODULES_DIR . '/venue/templates/single.php';
        }
        else { // Listing or search page
         /*   require_once __DIR__ . '/inc/list-model.php';
            $service = new OC_Venues_Model();*/
            $file = SONOMA_MODULES_DIR . '/venue/templates/list.php';
        }

        if (file_exists($file)) {
            OC_View_Helper::smGetTemplatePartCustom($file, true);
        }
    }
}

new OC_Venue_Module();