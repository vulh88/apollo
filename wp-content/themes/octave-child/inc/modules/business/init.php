<?php

/**
 * Class OC_Business_Module
 */
class OC_Business_Module
{
    public function __construct()
    {
        $this->renderPageTemplate();
    }

    public function renderPageTemplate()
    {
        if (is_single()) {
            /*require_once __DIR__ . '/inc/sing-model.php';
            $service = new OC_Business_Model();*/
            $file = SONOMA_MODULES_DIR . '/business/templates/single.php';
        }
        else if (is_tax()) {
            /*@ticket #17839: show business taxonomy*/
            $file = SONOMA_MODULES_DIR . '/business/templates/taxonomy.php';
        }
        else { // Listing or search page
           /* require_once __DIR__ . '/inc/list-model.php';
            $service = new OC_Businesses_Model();*/
            $file = SONOMA_MODULES_DIR . '/business/templates/list.php';
        }

        if (file_exists($file)) {
            OC_View_Helper::smGetTemplatePartCustom($file, true);
        }
    }
}

new OC_Business_Module();