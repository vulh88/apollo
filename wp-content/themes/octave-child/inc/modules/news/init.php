<?php

/**
 * Class OC_News_Module
 */
class OC_News_Module
{
    public function __construct()
    {
        $this->renderPageTemplate();
    }

    public function renderPageTemplate()
    {
        if (is_single()) {
            $file = SONOMA_MODULES_DIR . '/news/templates/single.php';
        }
        else {
            $file = SONOMA_MODULES_DIR . '/news/templates/list.php';
        }

        if (file_exists($file)) {

            $model = isset($model) ?$model : true;
            OC_View_Helper::smGetTemplatePartCustom($file, $model);
        }
    }
}

new OC_News_Module();