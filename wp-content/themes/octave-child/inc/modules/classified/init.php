<?php

/**
 * Class OC_Classified_Module
 */
class OC_Classified_Module
{
    public function __construct()
    {
        $this->renderPageTemplate();
    }

    public function renderPageTemplate()
    {
        if (is_single()) {
           /* require_once __DIR__ . '/inc/sing-model.php';
            $service = new OC_Classified_Model();*/
            $file = SONOMA_MODULES_DIR . '/classified/templates/single.php';
        }
        else { // Listing or search page
            /*require_once __DIR__ . '/inc/list-model.php';
            $service = new OC_Classifieds_Model();*/
            $file = SONOMA_MODULES_DIR . '/classified/templates/list.php';
        }

        if (file_exists($file)) {
            OC_View_Helper::smGetTemplatePartCustom($file, true);
        }
    }
}

new OC_Classified_Module();