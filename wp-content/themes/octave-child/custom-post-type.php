<?php

    $currentTerm = get_query_var('term');

    if (get_query_var('_apollo_org_search') === 'true') {
        new OC_Module_Init(Apollo_DB_Schema::_ORGANIZATION_PT);
    }
    elseif (get_query_var('_apollo_artist_search') === 'true' || get_query_var('_apollo_artist_timeline') === 'true') {
        new OC_Module_Init(Apollo_DB_Schema::_ARTIST_PT);
    }
    elseif (get_query_var('_apollo_venue_search') === 'true') {
        new OC_Module_Init(Apollo_DB_Schema::_VENUE_PT);
    }
    elseif (get_query_var('_apollo_org_search') === 'true') {
        new OC_Module_Init(Apollo_DB_Schema::_ORGANIZATION_PT);
    }
    elseif (get_query_var('_apollo_event_search') === 'true') {
        new OC_Module_Init(Apollo_DB_Schema::_EVENT_PT);
    }
    elseif (get_query_var('_apollo_classified_search') === 'true') {
        new OC_Module_Init(Apollo_DB_Schema::_CLASSIFIED);
    }
    elseif (get_query_var('_apollo_public_art_search') === 'true') {
        new OC_Module_Init(Apollo_DB_Schema::_PUBLIC_ART_PT);
    }
    elseif (get_query_var('_apollo_educator_search') === 'true'){
        new OC_Module_Init(Apollo_DB_Schema::_EDUCATOR_PT);
    }
    elseif (get_query_var('_apollo_program_search') === 'true'){
        new OC_Module_Init(Apollo_DB_Schema::_PROGRAM_PT);
    }
    elseif (get_query_var('_apollo_business_search') === 'true'){
        new OC_Module_Init(Apollo_DB_Schema::_BUSINESS_PT);
    }
    elseif (get_query_var( '_apollo_author_search' ) === 'true'){
        new OC_Module_Init(Apollo_DB_Schema::_BLOG_POST_PT);
    }
    elseif (get_post_type() == Apollo_DB_Schema::_SPOT_PT) {
        get_template_part('partials/comming-soon');
    }
    elseif (get_query_var('_apollo_news_search') === 'true') {
        new OC_Module_Init(Apollo_DB_Schema::_NEWS_PT);
    }
    elseif ( !empty($currentTerm) ) {
        global $sonoma_array_taxonomy;
        $currentTerm = get_term_by('slug', $currentTerm, get_query_var( 'taxonomy' ));
        $curPage = isset($_GET['page']) ? $_GET['page'] : 1;
        $_GET['paged'] = $curPage;
        $_GET['term'] = !empty($currentTerm) ? $currentTerm->term_id : '';
        $type = !empty($currentTerm) && isset($sonoma_array_taxonomy) && !empty($sonoma_array_taxonomy) ? $sonoma_array_taxonomy[$currentTerm->taxonomy] : get_post_type();
        new OC_Module_Init($type);
    }
    else {
        if ( is_single()){
            new OC_Module_Init(OC_Common_Const::_OC_SINGLE_CONTENT_POST_TYPE);
        }else if (is_search()){
            get_template_part('search');
        }
        /**
         * @ticket #18888: Missing the right bar widget
         * For the post categories are empty item
         */
        else if(is_archive()){
            new OC_Module_Init(Apollo_DB_Schema::_BLOG_POST_PT);
        }
        else{
            get_template_part('404');
        }

    }


