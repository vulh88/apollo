<?php

function octave_scripts() {

    $themeUri = sm_get_stylesheet_directory_uri();

    wp_enqueue_style('custom_css', $themeUri . oc_mix('/dist/css/octave.css'), false);

    wp_dequeue_style('apollo_primary_color_css');

    $overrideCssVersion = of_get_option(Apollo_DB_Schema::_OVERRIDE_CSS_VERSION, '1.0');
    wp_enqueue_style('octave_primary_color_css', get_primary_color_css_url(), false, $overrideCssVersion);

    if ( of_get_option( Apollo_DB_Schema::_ACTIVATE_OVERRIDE_CSS ) && file_exists(get_override_css_abs_path() ) ) {
        $overrideCssVersion = of_get_option(Apollo_DB_Schema::_OVERRIDE_CSS_VERSION, '1.0');
        wp_dequeue_style('apollo_override_css');
        wp_enqueue_style('apollo_override_css', get_override_css_url(), false, $overrideCssVersion);
    }

    /*@ticket #17252*/
    if(of_get_option(Apollo_DB_Schema::_ROW_TITLE_STYLE, '') || of_get_option(Apollo_DB_Schema::_BLOG_DISPLAY_STYLE,'default') == 'one-column') {
        wp_enqueue_script('slick_js', $themeUri . oc_mix('/dist/js/slick.js'), array('jquery'), false, true);
    }

    wp_enqueue_script('octave_start_js', $themeUri . oc_mix('/dist/js/oc-combine.js'), array('jquery'), false, true);
    wp_enqueue_script('flexslider_js', $themeUri . oc_mix('/dist/js/flexslider.js'), array('jquery'), false, true);

    if (of_get_option(OC_Common_Const::_OC_ENABLE_GEO_LOCATION, false)) {
        wp_enqueue_script('geo-location', $themeUri . oc_mix('/dist/js/geo-location.js'),  array('jquery'), false, true);
        $octaveLocalizeScript = array(
            'google_marker_selected' => of_get_option(OC_Common_Const::_OC_GOOGLE_MAP_SELECTED_MARKER_ICON, '/wp-content/themes/apollo/assets/images/icon-location-selected.png')
        );
        of_get_option(OC_Common_Const::_OC_GOOGLE_MAP_SELECTED_MARKER_ICON, '/wp-content/themes/apollo/assets/images/icon-location-selected.png');
        wp_localize_script( 'geo-location', 'octave_admin_data', $octaveLocalizeScript );
    }

}
add_action('wp_enqueue_scripts', 'octave_scripts', 105);