<?php
/**
 * sonoma initial setup and constants
 */
define( 'SM_CIRCLE_IMAGE_SIZE', 'circle_image_size' );
function sonoma_after_setup_theme() {
    // put anything logic once the theme is activated in Appearance > Themes
}
add_action( 'after_setup_theme', 'sonoma_after_setup_theme' );

function sonoma_init_theme() {
    $arrayOptionDisable = array();
    if (function_exists('sm_get_network_options')){
        foreach ( $arrayOptionDisable as $option){
            sm_get_network_options($option);
        }
    }
    $height = get_option( SM_CIRCLE_IMAGE_SIZE.'_h' );
    $width = get_option( SM_CIRCLE_IMAGE_SIZE.'_w' );

    if ( !empty( $height ) && !empty( $width ) ) {
        // Add the images sizes
        add_image_size( 'circle_image_size', $height, $width , true );
    }
}
add_action( 'init', 'sonoma_init_theme' );

// Include shortcodes
require_once OC_SHORTCODE_DIR. '/navigation/shortcode.php';
require_once OC_SHORTCODE_DIR. '/blog/shortcode.php';

if (is_admin()) {
    require_once SONOMA_ADMIN_DIR. '/admin.php';
}

/**
 * Register sidebars
 */
function octave_widgets_init() {
    register_sidebar( array (
        'name'          => __( 'Left Sidebar Homepage', 'apollo' ),
        'id'            => 'sidebar-left-homepage',
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
    ) );
}

add_action( 'widgets_init', 'octave_widgets_init' );

/** @Ticket #16187 - Register widget */
require_once SONOMA_INCLUDES_DIR . '/widgets/home-feature-items/home-feature-items.php';
require_once SONOMA_INCLUDES_DIR . '/widgets/home-feature-event/home-feature-event.php';
require_once SONOMA_INCLUDES_DIR . '/widgets/event-spotlight/event-spotlight.php';
/*@ticket #16782: 0002308: Calendar Month search widget - Month view layout */
require_once SONOMA_INCLUDES_DIR . '/widgets/class-oc-search-event-widget.php';

register_widget('apollo_home_feature_items_widget');
register_widget('apollo_home_feature_event_widget');
register_widget('apollo_event_spotlight_widget');
