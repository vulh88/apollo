<?php

class OC_Common_Const
{
    const _OC_ITEMS_NUM_LISTING_PAGE = 25;
    const _OC_SINGLE_CONTENT_POST_TYPE = '_oc_content_post_type';
    const _OC_LISTING_DESC_MAX_TRIM_WORDS = 220;
    const _OC_TOP_HEADER_HEIGHT = 'oc_top_header_height';
    const _OC_TRANSPARENT_MASTHEAD_NAV = 'oc_transparent_masthead_nav';

    const _OC_TRANSPARENT_MASTHEAD_NAV_LEVEL = 'oc_transparent_masthead_nav_level';
    const _OC_TRANSPARENT_MASTHEAD_NAV_LEVEL_DEFAULT = 15;

    const _OC_TOP_HEADER_NAVIGATION_STYLE= 'oc_top_header_navigation_style';
    const _OC_HEADER_SCROLL_WITH_PAGE = 'oc_header_scroll_width_page';
    const _OC_ENABLE_GEO_LOCATION = 'oc_enable_geo_location';
    const _OC_SEARCH_WIDGET_ENABLE_FILTER_BY_MY_LOCATION = 'oc_search_widget_enable_filter_by_my_location';
    const _OC_GOOGLE_MAP_SELECTED_MARKER_ICON = 'oc_google_map_selected_marker_icon';
    const _OC_ENABLE_LISTING_PAGE_TILE_VIEW = 'oc_enable_listing_page_tile_view';
    const _OC_GOOGLE_MAP_ZOOM = 'oc_google_map_zoom';

    const _OC_MASTHEAD_BACKGROUND = 'oc_masthead_background';
    const _OC_EVENT_LISTING_FILTER_ICON = 'oc_event_listing_filter_icon';
    const _OC_SOCIAL_ICON_STYLE = '_oc_social_icon_style';

    /**
     * @ticket #18657: Change the layout structure for Artist, Organization, Venue
     */
    const _OC_NUMBER_OF_CHARACTERS_DESCRIPTION_TRUNCATION = '_oc_number_of_characters_description_truncation';
    const _OC_NUMBER_OF_CHARACTERS_DESCRIPTION_TRUNCATION_DEFAULT = 1500;

    /**
     * @ticket #19131: Octave Theme - Change all detail page section labels same the section labels on the homepage - item 2
     */
    const _OC_SECTION_LABELS = 'oc_section_labels';
}