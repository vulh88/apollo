<?php
define('SONOMA_THEME_NAME', 'apollo-child');
define('SONOMA_FRONTEND_ASSETS_URI', sm_get_stylesheet_directory_uri().'/assets' );

/**
 * Define Directory Location Constants
 */
define( 'SONOMA_PARENT_DIR', sm_get_stylesheet_directory() );

define( 'SONOMA_INCLUDES_DIR', SONOMA_PARENT_DIR. '/inc' );
define( 'OC_SHORTCODE_DIR', SONOMA_PARENT_DIR. '/inc/shortcodes' );
define( 'SONOMA_ADMIN_DIR', SONOMA_INCLUDES_DIR . '/admin' );
define( 'SONOMA_MODULES_DIR', SONOMA_INCLUDES_DIR . '/modules' );
define( 'SONOMA_HELPER_DIR', SONOMA_INCLUDES_DIR . '/helpers' );

define( 'SONOMA_EVENT_INCLUDES_DIR', SONOMA_INCLUDES_DIR. '/modules/event' );
define( 'SONOMA_EVENT_TEMPLATE_DIR', SONOMA_EVENT_INCLUDES_DIR. '/templates' );
define( 'SONOMA_ORG_INCLUDES_DIR', SONOMA_INCLUDES_DIR. '/modules/organization' );
define( 'SONOMA_ORG_TEMPLATE_DIR', SONOMA_ORG_INCLUDES_DIR. '/templates' );

$GLOBALS['sonoma_array_taxonomy'] = array(
    'venue-type' => Apollo_DB_Schema::_VENUE_PT,
);

/**
 * Define URL Location Constants
 */
define( 'SONOMA_PARENT_URL', sm_get_stylesheet_directory_uri() );

define( 'SONOMA_ASSETS', SONOMA_PARENT_URL. '/assets' );

define( 'SONOMA_CSS_URL', SONOMA_ASSETS . '/css' );
define( 'SONOMA_JS_URL', SONOMA_ASSETS . '/js' );
define( 'SONOMA_IMAGES_URL', SONOMA_ASSETS . '/images' );

define( 'SONOMA_INCLUDES_URL', SONOMA_PARENT_URL. '/inc' );
define( 'SONOMA_ADMIN_URL', SONOMA_INCLUDES_URL. '/admin' );

define( 'SONOMA_ADMIN_ASSETS', SONOMA_ADMIN_URL. '/assets' );

define( 'SONOMA_ADMIN_CSS_URL', SONOMA_ADMIN_ASSETS . '/css' );
define( 'SONOMA_ADMIN_JS_URL', SONOMA_ADMIN_ASSETS . '/js' );
define( 'SONOMA_ADMIN_IMAGES_URL', SONOMA_ADMIN_ASSETS . '/images' );

define( 'SONOMA_DIRECTORY_LISTING_DEFAULT_VIEW_TYPE', 1 ); // default view type = 1 : 'thumbs' : 2 : 'lists'

define('OC_ADD_DEFAULT_DATA_TYPE', 1);

/* ThienLD : define circle image size width & height */
define ( 'SONOMA_CIRCLE_IMAGE_WIDTH', 250);
define ( 'SONOMA_CIRCLE_IMAGE_HEIGHT', 250);


define( 'OC_CSS_VERSION', '1.6.32' );
define( 'OC_JS_VERSION', '1.1.20' );


$GLOBALS['sonoma_available_modules'] = array(
    Apollo_DB_Schema::_VENUE_PT
);