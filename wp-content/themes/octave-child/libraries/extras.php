<?php
class OC_Extras {

    public static function isTheFirstAccessToPage(){
        if (is_front_page() && !isset($_SESSION['home_page']) ) {
            return true;
        }
        return false;
    }
}