<?php  if(Apollo_App::isDashboardPage()) : ?>
    <?php get_template_part('base-userdashboard'); ?>
<?php else : ?>
<!doctype html>
<!--[if IE 8]><html lang="en" class="no-js ie ie8"><![endif]-->
<!--[if IE 9]><html lang="en" class="no-js ie ie9"><![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>>
<!--<![endif]-->
<?php
get_template_part('partials/head');
$bodyClass = apply_filters('oc_body_class', '');
?>
<body <?php body_class($bodyClass); ?>>
<?php
echo Apollo_App::renderScriptInBodyTag();
if (OC_Extras::isTheFirstAccessToPage()) {
    get_template_part('partials/content-animation-homepage');
}

$is_one_column = false;
if(get_query_var('_rw_is_one_column') === 'true') {
    $is_one_column = true;
}

global $post;
$pageTemplate = get_page_template_slug($post);

?>
<div class="layout layout-fluid">

    <?php get_template_part('partials/header') ?>

    <?php echo do_shortcode('[apollo_home_spotlight]'); ?>
    <!--        --><?php //OC_View_Helper::smGetTemplatePartCustom(SONOMA_MODULES_DIR . '/event/templates/taxonomy/slider.php', array()); ?>

    <?php
    /*@ticket #17839: include business spotlight*/
    @include_once APOLLO_TEMPLATES_DIR . '/business/slider.php';

     /*@ticket #17932: include event spotlight*/
     @include_once APOLLO_TEMPLATES_DIR . '/events/slider.php'
    ?>

    <!-- @ticket: 16045 - missing public art map page -->
    <?php if (get_query_var('_apollo_public_art_map')) {
        include APOLLO_TEMPLATES_DIR . '/public-art/map-search.php';
    } else {
        ?>
        <section class="main">
            <div class="inner">

                <?php
                Apollo_App::displaySearchVenueWidgetOnMobile();
                Apollo_App::displaySearchArtistWidgetOnMobile();
                Apollo_App::displaySearchBusinessWidgetOnMobile();
                Apollo_App::displaySearchClassifiedWidgetOnMobile();
                Apollo_App::displaySearchEducationWidgetOnMobile();
                Apollo_App::displaySearchEventWidgetOnMobile();
                Apollo_App::displaySearchOrganizationWidgetOnMobile();
                Apollo_App::displaySearchPublicArtWidgetOnMobile();
                ?>

                    <?php if(!is_404()) :
                        if (Apollo_App::is_homepage()) :
                            the_widget('Apollo_Home_Blog_Feature', array(), array(
                                'position' => 'top',
                                'type'  => 'full',
                            ));
                        endif;
                    ?>
                    <div class="row two-col ">
                        <?php
                        if ($is_one_column) :
                            OC_View_Helper::smGetTemplatePart();
                        else :
                            $homeLayOut = of_get_option(Apollo_DB_Schema::_DEFAULT_HOME_LAYOUT, 'right_sidebar_one_column');
                            // Only apply fullwidth 960 for home page
                            if (Apollo_App::is_homepage() && $homeLayOut == 'full_960') :
                                ?>
                                <!-- start left side -->
                                <div class="wc-f">
                                    <?php OC_View_Helper::smGetTemplatePart(); ?>
                                </div> <!-- left side -->
                            <?php
                            else :
                                if (!Apollo_App::isFullTemplatePage($pageTemplate)) :
                                    ?>
                                    <div class="v-line"></div>
                                <?php endif; ?>

                                <!-- start left side -->
                                <div class="wc-l <?php echo $homeLayOut == 'right_sidebar_two_columns' ? 'grid' : ''; ?>">
                                    <?php
                                    if (Apollo_App::is_homepage()) :
                                        the_widget('Apollo_Home_Blog_Feature', array(), array(
                                            'position' => 'top',
                                            'type'  => 'left',
                                        ));
                                    endif;
                                    ?>
                                    <?php OC_View_Helper::smGetTemplatePart(); ?>
                                    <?php
                                    if (Apollo_App::is_homepage()) :
                                        the_widget('Apollo_Home_Blog_Feature', array(), array(
                                            'position' => 'bottom',
                                            'type'  => 'left',
                                        ));
                                    endif;
                                    ?>
                                </div> <!-- left side -->

                                <!-- start right side -->
                                <?php
                                if (!Apollo_App::isFullTemplatePage($pageTemplate)) :
                                    ?>
                                    <div class="wc-r">
                                        <?php
                                        include apollo_sidebar_path();
                                        ?>
                                    </div> <!-- right side -->
                                <?php
                                endif;
                            endif;
                        endif; ?>
                        </div>
                    <?php
                    else :
                        get_template_part('404');
                    endif; ?>
                    <?php

                    if (!isset($_SESSION)) {
                        session_start();
                    }
                    if(!isset($_SESSION['home_page'])) {
                        $_SESSION['home_page'] = true;
                    }
                    ?>
            </div>
        </section>

        <?php if (Apollo_App::is_homepage() && !is_404()): ?>

            <?php
            if ( $featuredBlogTopContent =  Apollo_App::get_static_html_content( Apollo_DB_Schema::_FEATURED_BLOG_TOP_CONTENT) ):
                echo $featuredBlogTopContent;
            endif;
            ?>

            <section class="main <?php echo Apollo_App::isFullTemplatePage($pageTemplate) ? 'apl-full-width-page' : '' ?> apl_bottom_home_blog_feature_widget">
                <div class="inner">
                    <?php
                    the_widget('Apollo_Home_Blog_Feature', array(), array(
                        'position' => 'bottom',
                        'type'  => 'full',
                    ));
                    ?>
                </div>
            </section>

        <?php endif; ?>

    <?php } ?>

</div>
<footer>
    <?php get_template_part('partials/footer') ?>
</footer>

</body>
</html>
<?php endif; ?>
