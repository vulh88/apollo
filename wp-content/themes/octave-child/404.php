<?php
$routing = of_get_option(Apollo_DB_Schema::_ROUTING_FOR_404_ERROR, '404');

if ($routing == 'home_page') {
    header("HTTP/1.1 301 Moved Permanently");
    header("Location: " . get_bloginfo('url'));
    exit();
} else {
    $content = Apollo_App::get_static_html_content(Apollo_DB_Schema::_PAGE_404);
    ?>
    <div class="apl-internal-content page-not-found">
        <div class="content-wrapper">
            <div class="block block-events">
                <?php if (!empty($content)) {
                    echo $content;
                } else { ?>
                        <div class="alert alert-warning">
                            <h1><?php _e('404 PAGE NOT FOUND!', 'sage'); ?></h1>
                            <small><?php _e('There\'s some reasons why this page is 404.', 'sage'); ?></small>
                        </div>
                <?php } ?>
            </div>
        </div>
    </div>

<?php } ?>
