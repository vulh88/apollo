const { mix } = require('laravel-mix');

var jsPath      = 'assets/js/',
    cssPath     = 'assets/css/',
    destinationJs = 'dist/js',
    destinationCss = 'dist/css',
    staticPath = '../../../static-layout/v1/public/assets';

mix.setPublicPath('/');

mix.scripts([
    staticPath + '/js/octave.js',
    staticPath + '/js/masthead.js',
    jsPath + '/octave.js'
], destinationJs + '/oc-combine.js');

mix.scripts( jsPath + '/geo-location.js', destinationJs + '/geo-location.js');
mix.scripts( jsPath + '/slick.js', destinationJs + '/slick.js');
mix.scripts( jsPath + '/flexslider.js', destinationJs + '/flexslider.js');

mix.styles([
    'style.css',
    staticPath + '/css/octave.css',
    cssPath + '/dev.css'
], destinationCss + '/octave.css');

if (mix.config.production) {
    mix.version();
}