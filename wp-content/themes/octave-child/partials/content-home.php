<?php

if (of_get_option(Apollo_DB_Schema::_HOME_FEATURED_TYPE, 'sta') === 'hor'){
    add_filter('apl_get_featured_event_horizontal_style', function(){
        return array(
            'wrapper_scroll_action' => '<div class="wrapper-scroll--action"><a href="#" class="fea-act-prev"><i class="fa fa-arrow-left"></i></a>
                <a href="#" class="fea-act-next"><i class="fa fa-arrow-right"></i></a></div>',
            'fea_evt_list_scroll' => 'fea-evt-list--scroll'
        );
    });

    /*@ticket #17604 Octave Theme - [CF] 20180917 - [2222#c15450] New customization of the horizontal feature*/
    add_filter('custome_style_horizontal_feature_ev_tt', function($defaultValue, $event){
        $permarlink = $event->get_permalink();
        $title = $event->get_title(true);
        $orgHtml = $event->renderOrgVenueHtml(false, true) ;
          return "<div class='ev-tt'><a href='$permarlink' >$title </a>
                        <div class='apl-event-org-name'>$orgHtml</div>
                   </div>";
    }, 10 , 2);

    add_filter('custome_style_horizontal_feature_back_title', function($defaultValue, $event){
        $permarlink = $event->get_permalink();
        $title = $event->get_title(true);
        return "<div class='back-title'><a href='$permarlink'>$title </a></div>";
    }, 10 , 2);

    /*@ticket #17724 Octave Theme - [CF] 20180927 - [Homepage] Insert a view more in tile at the last of each row of the feature event block - item 1, 2, 4, 6*/
   if(of_get_option(Apollo_DB_Schema::_ROW_TITLE_STYLE, '')){
       add_filter('apl_add_view_more_event_category', function($defaultValue, $linkCategory, $categoryName){
           return "<div class='fea-evt-item last-item'><div class='view-more'><a href='$linkCategory'>View more $categoryName </a></div></div>";
       }, 10, 3);
   }
}

if (of_get_option(Apollo_DB_Schema::_HOME_FEATURED_TYPE, 'sta') === 'three_columns'){
    add_filter('apl_get_class_three_columns_for_featured_event', function(){
        return __('featured-blog-list', 'apollo');
});
}

if (of_get_option('is_default_data_type', 1)) {
    include APOLLO_TEMPLATES_DIR. '/content-home-page.php';
}
else {
    /** @Ticket #16187 - Render Left home content widget */
    if (is_active_sidebar('sidebar-left-homepage')) {
        dynamic_sidebar('sidebar-left-homepage');
    }
}