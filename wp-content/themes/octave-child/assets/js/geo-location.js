(function ($) {
    var myMap;
    var iconWidth = parseInt(APL.google_map_settings.google_map_icon_width);
    var iconHeight = parseInt(APL.google_map_settings.google_map_icon_height);
    var iconSrc = APL.google_map_settings.google_map_icon;
    var selectedMarkerIcon = octave_admin_data.google_marker_selected;
    var octaveMarkers = [];
    var octaveGlobalEvents = [];
    var currentSearchForm = null;
    $(document).ready(function () {
        if (navigator.geolocation) {
            if(location.protocol == 'https:') {
                navigator.geolocation.getCurrentPosition(function(position){
                    enableLocationFilter(position.coords.longitude, position.coords.latitude);
                });
            } else {
                tryAPIGeolocation();
            }
        } else {
            var $locationCb = $('.apl-filter-location');
            if ($locationCb.length) {
                $locationCb.remove();
            }

        }

        $('#search-event').off('click', '.by_my_location').on('click', '.by_my_location', function(e){
            updateViewName(e);
        });
        $('#search-event-m-t').off('click', '.by_my_location').on('click', '.by_my_location', function(e){
            updateViewName(e);
        });

        /*@ticket 17496 */
        $('#search-event').off('click', '.apl-event-submit').on('click', '.apl-event-submit', function(e){
            reCalculateMapForm(e);
        });

        /*@ticket 17496 */
        $('#search-event-m-t').off('click', '.apl-event-submit').on('click', '.apl-event-submit', function(e){
            reCalculateMapForm(e);
        });

        $('.maps-events-filter--view').off('click', '.btn-prev').on('click', '.btn-prev', function (e) {
            loadMoreEvent(e);
        });

        $('.maps-events-filter--view').off('click', '.btn-next').on('click', '.btn-next', function (e) {
            loadMoreEvent(e);
        });

    });

    var reCalculateMapForm = function (e) {
        var current = $(e.currentTarget);
        var form = current.closest('form');

        var filterByMyLocation = form.find('.by_my_location'),
            minLat = form.find('input[name="min_lat"]').val(),
            minLng = form.find('input[name="min_lng"]').val(),
            maxLat = form.find('input[name="max_lat"]').val(),
            maxLng = form.find('input[name="max_lng"]').val(),
            currentView = form.find('input[name="view"]').val();

        if (filterByMyLocation.length &&
            filterByMyLocation.is(':checked') &&
            !minLat && !minLng && !maxLat && !maxLng &&
            currentView !== 'map') {
            e.preventDefault();
            $('.map-event-list').removeClass("hidden");
            currentSearchForm = $(form);
            console.log('Loading map');
            initMap();
        }
    };


    var updateViewName = function (e) {
        var current = $(e.currentTarget);
        var inputView = current.closest('form').find('input[name="view"]');
        if (inputView.length > 0) {
            var oldValue = inputView.attr('data-current-view');
            oldValue = (typeof oldValue == 'undefined') ? '' : oldValue;
            if (current.is(":checked")) {
                inputView.val('list');
            } else {
                inputView.val(oldValue == 'map' ? '' : oldValue);
            }
        }

        var latLngData = $('#map-Event');
        var filterLocation = current.closest('form').find('.apl-filter-location');
        var lat = filterLocation.find('.apl-lat');
        var lng = filterLocation.find('.apl-lng');
        if (filterLocation.length > 0 ) {
            if (lat.val() == '') {
                if (latLngData.length > 0) {
                    lat.val(latLngData.attr('data-apl-lat'));
                } else {
                    lat.val(lat.attr('data-old-value'))
                }
            }
            if (lng.val() == '') {
                if (latLngData.length > 0) {
                    lng.val(latLngData.attr('data-apl-lng'));
                } else {
                    lng.val(lng.attr('data-old-value'));
                }
            }
        }

    };

    var loadMoreEvent = function (e) {
        e.preventDefault();
        var current = $(e.currentTarget);
        if (current.hasClass('octave-disabled')) {
            return;
        }
        var parent = current.closest('.maps-events-filter--view');
        var currentPage = parseInt(current.attr('data-page'));
        var minLat = parent.attr('data-min-lat');
        var maxLat = parent.attr('data-max-lat');
        var minLng = parent.attr('data-min-lng');
        var maxLng = parent.attr('data-max-lng');
        var eventMapView = $('.search-map--event #map-Event');
        var dataRequest = getDataRequest(eventMapView, minLat, maxLat, minLng, maxLng);
        dataRequest.page = currentPage;

        var wrapContent = $('.search-bkl.wrap-search-map-event');
        $.ajax({
            url: APL.ajax_url,
            data: dataRequest,
            beforeSend: function () {
                wrapContent.block($.apl.blockUI);
            },
            success: function (res) {
                clearMarkers();
                setMarkers(res.data, res.iconSrc);
                var btnControl = $('.maps-events-filter--view');
                var filterResults = $('.maps-events-filter--result').find('p');
                if (filterResults.length > 0) {
                    filterResults.html(res.textPagination);
                }
                if (btnControl.length > 0) {
                    var btnNext = btnControl.find('.btn-next');
                    var btnPrev = btnControl.find('.btn-prev');
                    if (!res.have_more) {
                        btnNext.addClass('octave-disabled');
                    } else {
                        btnNext.removeClass('octave-disabled');
                    }
                    if (res.page > 2) {
                        btnPrev.removeClass('octave-disabled');
                    } else {
                        btnPrev.addClass('octave-disabled');
                    }

                    btnNext.attr('data-page', res.page);
                    btnPrev.attr('data-page', currentPage - 1 > 0 ? currentPage - 1 : 1);
                }
                $(wrapContent).unblock();
            }
        });
    };

    var clearMarkers = function () {
        if (octaveMarkers.length > 0) {
            for(var i = 0; i < octaveMarkers.length; i++) {
                octaveMarkers[i].setMap(null);
            }
        }
        octaveMarkers = [];
    };

    var initMap = function(){
        if(typeof google === 'undefined' ) {
            $.getScript('https://maps.googleapis.com/maps/api/js?signed_in=false&key=' + APL.google.apikey.browser + '&callback=jQuery.hasLoadGoogleMapJs');
        } else {
            $.hasLoadGoogleMapJs();
        }
    };

    window.onload  = function () {
        if ($('input[name="view"]').val() === 'map'){
            initMap();
        }
    };

    $.hasLoadGoogleMapJs = function () {
        $.apl.google_map.has_load = true;
        var eventMapView = $('.search-map--event #map-Event');

        if ($('input[name="view"]').val() !== 'map'){
            eventMapView = $(currentSearchForm).find('.search-map--event #map-Event-list');
        }

        if (eventMapView.length > 0) {
            var mapViews = $('#octave-map-option-filtering');
            if (mapViews.length > 0) {
                mapViews.closest('ul').addClass('octave-disabled');
            }
            var lng = eventMapView.attr('data-apl-lng');
            var lat = eventMapView.attr('data-apl-lat');

            /*@ticket 17496 */
            var filterLocation = $('.apl-filter-location');

            if(!lng){
                lng = filterLocation.find('.apl-lng').val();
            }

            if(!lat){
                lat = filterLocation.find('.apl-lat').val();
            }

            var mapZoom = parseInt(eventMapView.attr('data-map-zoom'));
            if (typeof lng != 'undefined' && lng != '' && typeof lat != 'undefined' && lat != '') {
                var mapOptions = {
                    center: new google.maps.LatLng(parseFloat(lat), parseFloat(lng)),
                    zoom: mapZoom
                };
                myMap =  new google.maps.Map(eventMapView[0], mapOptions);
                myMap.addListener('bounds_changed', performSearch);
            }
        }
    };

    var performSearch = function () {
        var bounds  = myMap.getBounds(),
        ne      = bounds.getNorthEast(), // top-left
        sw      = bounds.getSouthWest(); // bottom-right

        /** Get data from server */
        var eventMapView = $('.search-map--event #map-Event');
        var dataRequest = getDataRequest(eventMapView, sw.lat(), ne.lat(), sw.lng(), ne.lng());
        var wrapContent = $('.search-bkl.wrap-search-map-event');
        setBounds(sw.lat(), ne.lat(), sw.lng(), ne.lng());

        var currentView = $(currentSearchForm).find('input[name="view"]');
        if (currentView.length && currentView.val() !== 'map') {
            currentSearchForm = $(currentSearchForm);
            currentSearchForm.find('input[name="min_lat"]').val(sw.lat());
            currentSearchForm.find('input[name="max_lat"]').val(ne.lat());
            currentSearchForm.find('input[name="min_lng"]').val(sw.lng());
            currentSearchForm.find('input[name="max_lng"]').val(ne.lng());
            $(currentSearchForm).submit();
            return;
        }

        clearMarkers();
        $.ajax({
            url: APL.ajax_url,
            data: dataRequest,
            beforeSend: function () {
                wrapContent.block($.apl.blockUI);
            },
            success: function (res) {
                setMarkers(res.data, res.iconSrc);
                var filterResults = $('.maps-events-filter--result').find('p');
                if (filterResults.length > 0) {
                    filterResults.html(res.textPagination);
                }
                var btnControl = $('.maps-events-filter--view');
                if (btnControl.length > 0) {
                    var btnNext = btnControl.find('.btn-next');
                    var btnPrev = btnControl.find('.btn-prev');
                    if (!res.have_more) {
                        btnNext.addClass('octave-disabled');
                    } else {
                        btnNext.removeClass('octave-disabled');
                    }
                    btnPrev.addClass('octave-disabled');
                    btnNext.attr('data-page', res.page);
                    btnControl.attr('data-min-lat', sw.lat());
                    btnControl.attr('data-max-lat', ne.lat());
                    btnControl.attr('data-min-lng', sw.lng());
                    btnControl.attr('data-max-lng', ne.lng());
                }
                $(wrapContent).unblock();
                $('.map-event-list').addClass("hidden");
            }
        });
    };

    var setBounds = function (minLat, maxLat, minLng, maxLng) {
        /** @Ticket #16931 display events by location with Tile and List views */
        var mapViews = $('#octave-map-option-filtering');
        if (mapViews.length > 0) {
            var filteringOptions = mapViews.closest('ul').find('li');
            if (filteringOptions.length > 0) {
                $.each(filteringOptions, function (index, item) {
                    item = $(item).find('a');
                    var url = item.attr('href');
                    if (url.length > 0) {
                        var args = url.split('&');
                        var urlReplace = [];
                        var minLatRequest = 'min_lat=' + minLat;
                        var maxLatRequest = 'max_lat=' + maxLat;
                        var minLngRequest = 'min_lng=' + minLng;
                        var maxLngRequest = 'max_lng=' + maxLng;
                        $.each(args, function (urlIndex, urlItem) {
                            if (urlItem.indexOf('min_lat') > -1) {
                                return true;
                            }
                            if (urlItem.indexOf('max_lat') > -1) {
                                return true;
                            }
                            if (urlItem.indexOf('min_lng') > -1) {
                                return true;
                            }
                            if (urlItem.indexOf('max_lng') > -1) {
                                return true;
                            }
                            urlReplace.push(urlItem);
                        });
                        urlReplace.push(minLatRequest);
                        urlReplace.push(maxLatRequest);
                        urlReplace.push(minLngRequest);
                        urlReplace.push(maxLngRequest);
                        item.attr('href', urlReplace.join('&'));
                    }
                });
            }
            mapViews.closest('ul').removeClass('octave-disabled');
        }
    };

    var displayListEvents = function (lat, lng) {
        var html = '';
        if (octaveGlobalEvents.length > 0) {
            $.each(octaveGlobalEvents, function (index, item) {
               if (item.venueLat == lat && item.venueLng == lng) {
                   html += item.eventHtml;
               }
            });
        }
        return html;
    };

    var getDataRequest = function (wrapMapView, min_lat, max_lat, min_lng, max_lng) {
        var dataRequest = {
            action: 'apollo_get_event_for_map_view',
            by_my_location: 1,
            min_lat: min_lat,
            max_lat: max_lat,
            min_lng: min_lng,
            max_lng: max_lng
        };
        var attribute = ['keyword', 'start_date', 'end_date', 'term', 'event_location',
            'event_org', 'view', 'save_lst_list', 'city', 'region', 'accessibility',
            'is_discount', 'neighborhood', 'by_my_location', 'apl-lat', 'apl-lng', 'date_format', 'free_event'
        ];
        $.each(attribute, function(index, item){
            var attrValue = $(wrapMapView).attr('data-' + item);
            if (typeof attrValue != 'undefined' && (attrValue != '' || attrValue != 0)) {
                if (item == 'city') {
                    dataRequest['event_city'] = attrValue;
                } else {
                    dataRequest[item] = attrValue;
                }
            }
        });
        return dataRequest;
    };

    var setMarkers = function(data, icon) {
        if (data.length > 0) {
            octaveGlobalEvents = data;
            for (var i = 0; i < data.length; i++) {
                var location = data[i];
                if (!checkAlreadyLocation(location.venueLat, location.venueLng)) {
                    var defaultIcon = makeIcon(icon);
                    var marker = new google.maps.Marker({
                        position: {lat: parseFloat(location.venueLat), lng: parseFloat(location.venueLng)},
                        map: myMap,
                        icon: defaultIcon,
                        title: location.venueName
                    });

                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        return function() {
                            var eventDetail = $('.blk-event-map-item');
                            if (eventDetail.length > 0 ) {
                                eventDetail.html('');
                                eventDetail.removeClass('hidden');
                                eventDetail.html(displayListEvents(octaveGlobalEvents[i].venueLat, octaveGlobalEvents[i].venueLng));
                            }
                            setMarkersDefault(marker);
                        };
                    })(marker, i));
                    octaveMarkers.push(marker);
                }
            }
        }else {
            octaveGlobalEvents = [];
        }
        displayAllEventItem(data);

    };

    var setMarkersDefault = function (currentMarker) {
        if (octaveMarkers.length > 0) {
            $.each(octaveMarkers, function (index, marker) {
                if(marker.position.lat() == currentMarker.position.lat()
                    && marker.position.lng() == currentMarker.position.lng()) {
                    marker.setIcon(makeIcon(selectedMarkerIcon));
                } else {
                    marker.setIcon(makeIcon());
                }
            });
        }
    };

    var checkAlreadyLocation = function (lat, lng) {
        var result = false;
        if (octaveMarkers.length > 0) {
            $.each(octaveMarkers, function (index, item) {
               if (item.position.lat() == lat && item.position.lng() == lng) {
                   result = true;
                   return true;
               }
            });
        }
        return result;
    };

    var makeIcon = function(src) {
        var _src = src && src != undefined ? src : iconSrc;
        return new google.maps.MarkerImage(_src,
            null, null, null, new google.maps.Size(iconWidth, iconHeight));
    };


    var tryAPIGeolocation = function() {
        jQuery.post( 'https://www.googleapis.com/geolocation/v1/geolocate?key=' + APL.google.apikey.browser, function(success) {
            enableLocationFilter(success.location.lng, success.location.lat);
        })
        .fail(function(err) {
            console.log("API Geo-Location error! ", err);
        });
    };
    var enableLocationFilter = function(longitude, latitude) {
        var filterLocation = $('.apl-filter-location');
        if (filterLocation.length > 0) {
            if (typeof longitude != 'undefined' && longitude != null && longitude != ''
                && typeof latitude != 'undefined' && latitude != null && latitude != '') {
                /** Enable filter by my location */
                if (filterLocation.hasClass('hidden')) {
                    filterLocation.removeClass('hidden');
                }
                var lat = filterLocation.find('.apl-lat');
                var lng = filterLocation.find('.apl-lng');
                if (lat.length > 0) {
                    lat.val(latitude);
                }
                if (lng.length > 0) {
                    lng.val(longitude);
                }

            } else {
                /** Hidden filter by my location and clean location data */
                if (!filterLocation.hasClass('hidden')) {
                    filterLocation.addClass('hidden');
                }
            }
        }
    };

    var displayAllEventItem = function (data) {
        var eventDetail = $('.blk-event-map-item');
        if (eventDetail.length > 0) {
            eventDetail.html('');
            if (data.length > 0) {
                eventDetail.removeClass('hidden');
                $.each(data, function (index, item) {
                    eventDetail.append(item.eventHtml);
                });
            } else {
                eventDetail.addClass('hidden');
            }
        }
    };

})(jQuery);