$(document).ready(function () {

    if($('.apl-recommend-post-hor').length){
        $('.apl-recommend-post-hor').slick({
                arrows: false,
                slidesToShow: 4,
                slidesToScroll: 1,
                dots: true,
                centerMode: false,
                infinite: false,
                centerPadding: '0',
                responsive: [
                    {
                        breakpoint: 769,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 681,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 361,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            }
        );
    }

    var groupButtons = $('.single-event .apl-detail-event-button-group');
    if (groupButtons.length > 0 && groupButtons.children().length == 0) {
        groupButtons.addClass('hidden');
    }
});