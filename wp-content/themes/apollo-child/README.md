# APOLLO SONOMA THEME STRUCTURES

## I Introduction
Sonoma Theme is new skin of Apollo Theme and is built under kind of Wordpress Child Theme. The folder contains the whole implementations is 'apollo-child'. By using child-theme, we can inherit all features and functions from Apollo Parent Theme and feel free to implement any customizations based on the requirements in Sonoma without any conflicts or influences to its parent. 

## II. Folder structures
    assets : FrontEnd Resources. It's only synchronized from /apollo-theme/static-layout/sonoma/dist/assets once have any modification from FE team. 
    | css
    | fonts
    | images
    | js
    libraries : the specific libraries which need to use and define in Sonoma. 
    | class-sonoma-const.php : define common constant
    | config.php : Define Directory Location Constants,  URL Location Constants
    | init.php : place for adding the hooks when initializing the theme
    | multi-post-thumbnails.php : an additional library which allows for uploading/choosing multiple thumbnail/featured image in each editing post screen. 
    | scripts.php : place for enqueueing css/js scripts which need to used for Sonoma Theme.
    partials : contains the large template part which are based on the master Sonoma template (apollo-child/base.php)
    inc : managing modules and admin 
    | admin : contains hooks/customization when child theme Sonoma is activated
    | | assets : Sonoma WP Admin Resources 
    | | | styles 
    | | | js
    | | theme-options : contains additional/customized logic in ThemeOption. 
    | | | default-template : contains default templates which are used for auto-generating when Sonoma Theme is active/re-active in any cases of these files are not existing. 
    | | class-sonoma-admin-assets.php : place for enqueueing css/js scripts which need to used for Sonoma Theme Admin Area.
    | modules : contains the available modules which are implemented only in Sonoma Theme
    | | common-templates : contains common templates which can be used any places in all of Sonoma's modules
    | | {module_name} : e.g (inc/modules/artist)
    | | | admin : custom logic and template of module in WP Admin area. 
    | | | inc : contains model of the module
    | | | | sm_{module_name}_model.php : the model file is used for interacting between querying data and determining the specific template for rendering.
    | | | templates : contains all template-parts on the module
    | | | {module_name}.php : e.g(inc/modules/artist/artist.php) : work as controller to manage routes in the module.  
    | | sm_mod_abstract.php : the abstract class contains all of common functions and properties of child-modules
    | | sm_mod_admin_init.php : initializing all module in admin side.
    | | sm_mod_init.php : initializing all module in client side.
    | | sm_mod_media_setting.php : initializing all module in admin side but relating to media settings. 
    | | sm_mod_model : the abstract class for all model in each child-module.
    
## II. Installations
    1. Place the Sonoma Skin under folder 'apollo-child' within '/wp-content/themes/'. 
    2. Go to Network Admin and ENABLE Sonoma Theme as in this image http://prntscr.com/d1keqt . 
    3. Back to the site which need to apply the Sonoma Skin and ACTIVE it in Appearance > Themes > Sonoma Theme.
    4. Visit the site and enjoy the Sonoma Theme. 

## III. Media Settings 
    A. Image Sizes Information
                Image Sizes                 Image Names use in code         |        Enabled Retina mode 
        1. THUMBNAIL - 150x150        |           thumbnail                 |              300x300             
        2. MEDIUM    - 350x450        |            medium                   |              700x900 
        3. LARGE     - 1024x700       |            large                    |             2048x1400 
        4. CIRCLE    - 250x250        |       circle_image_size             |              500x500 
        5. ORIGINAL - image real size |            full                     |       real-width*2xreal-height*2
    B. Using Image Sizes in client site
        1. THUMBNAIL 
            - The images are rendered in Directory Detail pages at Gallery Photos block in small view area http://prntscr.com/d1lepw .
            - The images are rendered in the associated list posts in Directory Detail page http://prntscr.com/d1lnc0 . 
        2. MEDIUM    
            - The images are rendered in Blog Listing page http://prntscr.com/d1lj4z .  
            - The image is rendered in Directory Detail pages http://prntscr.com/d1ljpg . 
        3. LARGE     
            - The images are rendered in Directory Detail pages at Gallery Photos block in large view area http://prntscr.com/d1ldz2 .   
            - The image is rendered in Blog Author page http://prntscr.com/d1lgh2 .    
        4. CIRCLE    
            - The circle images in each Spotlight in Homepage - Main Slider http://prntscr.com/d1ksr9
            - The circle images in Directory listing pages (view type: 'Thumbs' - http://prntscr.com/d1l9g6 & 'List' - http://prntscr.com/d1lau8). Currently, it's applied for 5 available modules in Sonoma Theme (organization,artist,venue,public-art,classified)
            - The circle image in the Gallery Images which is rendered in page custom template http://prntscr.com/d1lbbh .   
        5. ORIGINAL  
            - The background image for Spotlight in Homepage - Main Slider http://prntscr.com/d1krjq . 
            - The background image for Top & Middle positions http://prntscr.com/d1lkzf in page - custom template. 
            - The background iamge for Feature image in Blog post detail page http://prntscr.com/d1llzt . 

## IV. HOW TO CREATE A NEW CHILD THEME
    1. Copy apollo-child-base to apoll-child-1
    2. Go to Network Admin and ENABLE YOUR-NEW-CHILD Theme like as in this image http://prntscr.com/d1keqt . 
    3. Back to the site which need to apply the Sonoma Skin and ACTIVE it in Appearance > Themes > YOUR-NEW-CHILD Theme.
    4. Visit the site and enjoy the YOUR-NEW-CHILD Theme.

## V. CODING STYLE GUILDS
    - Call apollo-child-base shortly [ACB]
    1. First of all, take a look (II.) for meaning of Folder Structure of a [ACB]
    2. To custom base template for MAIN SITE in FE, do it in [ACB]/base.php
    3. To custom base template for User Dashboard in FE, do it in [ACB]/base-userdashboard.php
    4. [ACB] build post types in FE as modules separately. Currently, there are the following modules:
    4.1. Post ([ACB]/inc/modules/post)
    4.2. Page ([ACB]/inc/modules/page)
    4.3. artist ([ACB]/inc/modules/artist)
    => We can follow this skeleton for a module as in Folder Structure to build the new ones, such as Classified, Organization, etc.
    5. Keep all the additional hooks/implementation/customization via Admin into [ACB]/inc/admin
    6. Keep all the additional hooks/implementation/customization via FrontPage into [ACB]/inc/modules
    7. Keep all of the additional common constants which are used only in the current child theme into [ACB]/libraries/class-child-const.php
    8. Keep all of defining by using php function define('') into [ACB]/libraries/config.php
    9. Keep all of either global hooks or static function in [ACB]/functions.php or [ACB]/libraries/extras.php
    10. Keep all enqueue or dequeue scripts in FrontPage into [ACB]/libraries/scripts.php
    11. Keep all hooks of initializing the current child theme into [ACB]/libraries/init.php
    12. Shouldn't add more any codes to [ACB]/page.php & [ACB]/single.php. The whole customization of them are implemented into post and page modules. See (V. CODING STYLE GUILDS > 4.) 
    13. Follow the way in [ACB]/custom-post-type.php to add a new module of a custom post type. 
    14. To make a page which is bound with a selected custom template value from WPAdmin > Page
    - Don't add more any codes into [ACB]/page_custom.php 
    - Follow the way in [ACB]/inc/modules/page/page.php for implementation the new one.

## VI. NOTES
    - Our theme structure is building adherent to https://roots.io/sage/

## LOGS

##### - Release sonoma theme version 1.0.0  19/10/2016