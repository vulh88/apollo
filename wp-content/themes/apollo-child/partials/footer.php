
<div class="footer clearfix" data-uk-scrollspy="{cls:'uk-animation-fade'}">
    <?php  echo Apollo_App::get_static_html_content( Apollo_DB_Schema::_FOOTER1 );?>
    <?php  echo Apollo_App::get_static_html_content( Apollo_DB_Schema::_FOOTER3 );?>
    <?php  echo Apollo_App::get_static_html_content( Apollo_DB_Schema::_FOOTER4 );?>
</div>

<?php
wp_footer();
?>