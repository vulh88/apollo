<head>
  <meta charset=utf-8>
  <meta http-equiv=X-UA-Compatible content="IE=edge">
  <?php  if (get_query_var('_apollo_public_art_map')):
  $pagename = get_query_var('pagename');
  ?>
  <title><?php echo sprintf(__("Public Art Map | %s "), get_bloginfo('name')); ?></title>
  <?php else : ?>
  <title><?php wp_title('|', true, 'right'); ?></title>
  <?php endif; ?>
  <meta name="description"
        content="<?php echo sanitize_text_field(of_get_option(Apollo_DB_Schema::_SITE_SEO_DESCRIPTION, '')) ?>">
  <meta name="keywords"
        content="<?php echo sanitize_text_field(of_get_option(Apollo_DB_Schema::_SITE_SEO_KEYWORDS, '')) ?>">
  <meta name=viewport content="width=device-width,initial-scale=1,user-scalable=0,minimum-scale=1,maximum-scale=1">
  <title></title>
  <meta name=msapplication-tap-highlight content=no>
  <link rel=manifest href=<?php echo SONOMA_FRONTEND_ASSETS_URI; ?>/manifest.json>
  <meta name=mobile-web-app-capable content=yes>
  <meta name=application-name content="Web Starter Kit">
  <link rel=icon sizes=192x192 href=<?php echo SONOMA_FRONTEND_ASSETS_URI; ?>/images/touch/chrome-touch-icon-192x192.png>
  <meta name=apple-mobile-web-app-capable content=yes>
  <meta name=apple-mobile-web-app-status-bar-style content=black>
  <meta name=apple-mobile-web-app-title content="Web Starter Kit">
  <link rel=apple-touch-icon href=<?php echo SONOMA_FRONTEND_ASSETS_URI; ?>/images/touch/apple-touch-icon.png>
  <meta name=msapplication-TileImage content=<?php echo SONOMA_FRONTEND_ASSETS_URI; ?>/images/touch/ms-touch-icon-144x144-precomposed.png>
  <meta name=msapplication-TileColor content=#2F3BA2>
  <meta name=theme-color content=#2F3BA2>
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <?php wp_head(); ?>
</head>
