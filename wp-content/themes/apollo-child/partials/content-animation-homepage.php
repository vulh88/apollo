<?php
/**
 * Created by PhpStorm.
 * User: truong
 * Date: 16/11/2016
 * Time: 14:10
 */
?>
<!-- // BEGIN ANIMATION CODE SNIPPET -->
<div id="page_wrapper">
    <div id="wrapper" class="active">
        <div class="ani-wrapper">
            <div class="toprule">
                <div id="red"></div>
            </div>
            <div class="container r">
                <div class="red">
                    <div id="halfclip">
                        <div class="halfcircle" id="clipped"></div>
                    </div>
                    <div class="halfcircle" id="fixed"></div>
                </div>
                <div class="green"></div>
            </div>
            <div class="container o">
                <div class="blue"></div>
                <div class="orange">
                    <div id="halfclip">
                        <div class="halfcircle" id="clipped"></div>
                    </div>
                    <div class="halfcircle" id="fixed"></div>
                </div>
            </div>
            <div class="container cs">
                <div class="creative">Creative
                    <div class="sonoma">Sonoma</div>
                </div>
                <div class="rings"></div>
                <div class="seg-green"></div>
                <div class="seg-blue"></div>
                <div class="seg-red"></div>
                <div class="seg-orange"></div>
            </div>
            <div class="watermark">
                <!-- // REFERENCE TO IMAGE FILE -->
                <img src="<?php echo SONOMA_FRONTEND_ASSETS_URI;?>/images/animation/CreativeSonoma_watermark.png" alt="" title="" />
            </div>
            <div class="bottomrule">
                <div id="orange"></div>
            </div>
        </div>

        <div class="ghost-ring one">
            <div class="one"></div>
        </div>
        <div class="ghost-ring two">
            <div class="two"></div>
        </div>
        <div class="ghost-ring three">
            <div class="three"></div>
        </div>
        <div class="ghost-ring four">
            <div class="four"></div>
        </div>

    </div>
</div>

<!-- // END ANIMATION CODE SNIPPET -->
