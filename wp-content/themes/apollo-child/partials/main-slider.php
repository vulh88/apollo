<div class=sm-main-slider>
    <ul class="slides">
        <?php
        $spots = get_posts( array(
            'post_type' => Apollo_DB_Schema::_SPOT_PT,
            'posts_per_page' => -1,
            'orderby'   => 'menu_order',
            'order'          => 'ASC',
        ));

        $number_spot_display = of_get_option(Apollo_DB_Schema::_APL_SPOT_LIGHT_NUM_DISPLAY_SLIDER);
        $number_spot_display = intval($number_spot_display) === 0 ? Apollo_Display_Config::NUM_HOME_SPOTLIGHT : $number_spot_display;
        $spotIndex = 0;
        foreach ( $spots as $spt ):
            if( $spotIndex == $number_spot_display ){
                break;
            }
            $spt = get_spot( $spt );

            $l = $spt->get_post_meta_data( Apollo_DB_Schema::_APL_SPOT_LINK );
            $color_left_arrow = $spt->get_post_meta_data( Apollo_DB_Schema::_APL_SPOT_LIGHT_COLOR_LEFT_ARROW );
            $color_right_arrow = $spt->get_post_meta_data( Apollo_DB_Schema::_APL_SPOT_LIGHT_COLOR_RIGHT_ARROW );

            $l = trim($l);
            $_l = !empty($l) ? $l : 'javascript:void(0);';

            $open_style = $spt->get_post_meta_data( Apollo_DB_Schema::_APL_SPOT_LINK_OPEN_STYLE );
            $target = $open_style ? 'target = "blank"' : '';

            $spot_image_gallery = $spt->get_post_meta_data(Apollo_DB_Schema::_APL_SPOT_LIGHT_GALLERY );
            $attachments = array_filter( explode( ',', $spot_image_gallery ) );

            $img_id = $spt->get_image_id();
            $src_feature_img = wp_get_attachment_image_src( $img_id, 'full');

            $_o_summary = $spt->get_summary(Apollo_Display_Config::HOME_SPOTLIGHT_EVENT_OVERVIEW_NUMBER_CHAR); ?>
            <li>
                <div class="slider-item" data-color-prev-button="<?php echo $color_left_arrow ?>"  data-color-next-button="<?php echo $color_right_arrow ?>"
                     style="background-repeat:no-repeat;background-image:url(<?php echo isset( $src_feature_img[0] ) ? $src_feature_img[0] : '';  ?>)">
                    <div class=blk-sli-img >
                        <?php if($attachments) :
                            $circleImgIndex = 0;
                            foreach ( $attachments as $attachment_id ) :
                                if( $circleImgIndex == 3 ){
                                    break;
                                }
                                $src_img = wp_get_attachment_image_src( $attachment_id, SM_CIRCLE_IMAGE_SIZE);
                            ?>
                            <div class=itm-circle>
                                <div class=art-title><img src="<?php  echo isset( $src_img[0] ) ? $src_img[0] : ''; ?>" class="teaser-bg" alt=""></div>
                            </div>
                            <?php
                                $circleImgIndex++;
                            endforeach;
                        endif; ?>
                    </div>
                    <div class=blk-sli-ct>
                        <h3 class=ttl-art><?php echo $spt->get_title(); ?></h3>
                        <div class="desc"><?php echo $spt->get_full_content(); ?></div>
                        <a <?php echo $target ?> href="<?php echo $_l; ?>" class="btn btn-explore"><?php _e('READ MORE', 'apollo') ?> ></a></div>
                </div>
            </li>
        <?php
            $spotIndex++;
        endforeach; ?>
    </ul>
</div>
<?php if($number_spot_display > 1) : ?>
    <div class=custom-navigation><a href=# class="sli-icon flex-prev icon-prev d"><i
                class="icon ico-prev"></i> </a><a href=# class="sli-icon flex-next icon-next "><i
                class="icon ico-next"></i></a></div>
<?php endif; ?>
<div class="blk-sli-menu flexslider-controls clearfix">
    <ol class="flex-control-nav">
        <?php $clIndex = 0;
        foreach ( $spots as $spt ):
        if( $clIndex == $number_spot_display ){
            break;
        }
        $spt = get_spot( $spt ); ?>
        <li><?php  echo  strtoupper($spt->get_title()); ?>
        <?php  $clIndex++; endforeach; ?>
    </ol>
</div>
<div class="next-section"><a href="#feature-block" class="let-down" data-uk-smooth-scroll="{offset: 89}"><i
            class="icon ico-down"></i></a></div>


