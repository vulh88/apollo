<?php

/**
 * ThienLD : MUST put 2 hooks "stylesheet_directory_uri" and "stylesheet_directory" at the top of apollo-child functions.php in order custom stylesheet uri
 * and directory path to be always going to parent theme apollo
*/

add_filter('stylesheet_directory_uri','sonoma_custom_stylesheet_directory_uri',99,3);
function sonoma_custom_stylesheet_directory_uri($stylesheet_dir_uri, $stylesheet, $theme_root_uri ){
    return $theme_root_uri . '/' . 'apollo';
}
add_filter('stylesheet_directory','sonoma_custom_stylesheet_directory',99,3);
function sonoma_custom_stylesheet_directory($stylesheet_dir, $stylesheet, $theme_root){
    return $theme_root . '/' . 'apollo';
}

/**
 * sonoma includes
 *
 * The $sonoma_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 */
$sonoma_includes = array(

    // Parent includes
    'lib/config.php',          // Configuration
    'lib/utils.php',           // Utility functions
    'lib/init.php',            // Initial theme setup and constants
    'lib/wrapper.php',         // Theme wrapper class
    'lib/sidebar.php',         // Sidebar class
    'lib/activation.php',      // Theme activation
    'lib/titles.php',          // Page titles
    'lib/nav.php',             // Custom nav modifications
    'lib/gallery.php',         // Custom [gallery] modifications
    'lib/comments.php',        // Custom comments modifications
    'lib/scripts.php',         // Scripts and stylesheets
    'lib/extras.php',          // Custom functions
    'lib/languages.php',       // Language
    'lib/retina.php',          // Support Retina

    // Children  includes
    'libraries/config.php',          // Configuration
    'libraries/init.php',            // Initial theme setup and constants
    'libraries/scripts.php',         // Scripts and stylesheets
    'libraries/extras.php',          // Custom functions
    'libraries/multi-post-thumbnails.php',          // Multi thumbnails
    'libraries/class-sonoma-const.php',          // Common const
    'inc/modules/sm_mod_init.php',          // Sonoma module init
    'inc/modules/sm_mod_admin_init.php',          // Sonoma module admin init
    'inc/admin/class-sonoma-admin-assets.php',

);

foreach ($sonoma_includes as $file) {
    if (!$filepath = locate_template($file)) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'sonoma'), $file), E_USER_ERROR);
    }

    require_once $filepath;
}
unset($file, $filepath);




/**
 * Retrieve stylesheet directory URI.
 *
 * @return string
 */
function sm_get_stylesheet_directory_uri() {
    $stylesheet = str_replace( '%2F', '/', rawurlencode( get_stylesheet() ) );
    $theme_root_uri = get_theme_root_uri( $stylesheet );
    $stylesheet_dir_uri = "$theme_root_uri/$stylesheet";

    /**
     * Filter the stylesheet directory URI.
     *
     * @param string $stylesheet_dir_uri Stylesheet directory URI.
     * @param string $stylesheet         Name of the activated theme's directory.
     * @param string $theme_root_uri     Themes root URI.
     */
    return apply_filters( 'sm_stylesheet_directory_uri', $stylesheet_dir_uri, $stylesheet, $theme_root_uri );
}


/**
 * Retrieve stylesheet directory path for current theme.
 *
 * @return string Path to current theme directory.
 */
function sm_get_stylesheet_directory() {
    $stylesheet = get_stylesheet();
    $theme_root = get_theme_root( $stylesheet );
    $stylesheet_dir = "$theme_root/$stylesheet";

    /**
     * Filter the stylesheet directory path for current theme.
     *
     * @since 1.5.0
     *
     * @param string $stylesheet_dir Absolute path to the current them.
     * @param string $stylesheet     Directory name of the current theme.
     * @param string $theme_root     Absolute path to themes directory.
     */
    return apply_filters( 'sm_stylesheet_directory', $stylesheet_dir, $stylesheet, $theme_root );
}

function sm_get_network_options($key){
    $option = get_option($key);
    if ( empty($option) || $option == 1 ){
        update_option($key, 0);
    }
}


add_filter('_apl_custom_theme_options_default_templates','_apl_custom_theme_options_default_templates_handler',99,1);
function _apl_custom_theme_options_default_templates_handler($default_templates = array()){
    global $sonoma_theme_options_default_templates;
    if(empty($default_templates)){
        return $sonoma_theme_options_default_templates;
    }
    return array_replace($default_templates,$sonoma_theme_options_default_templates);
}

add_filter('_apl_custom_theme_options_file_path_handler','_apl_custom_theme_options_file_path_handler',99,1);
function _apl_custom_theme_options_file_path_handler($default_template_key =''){
    global $sonoma_theme_options_default_templates;
    if( ! empty($default_template_key) && in_array($default_template_key, array_keys($sonoma_theme_options_default_templates) )){
        return  $sonoma_theme_options_default_templates[$default_template_key];
    }
    return '';
}


add_filter('_apl_hide_tabs_unnecessary','_apl_hide_tabs_unnecessary',99,1);
function _apl_hide_tabs_unnecessary($tab = ''){
    $tabArray = array();
    if ( in_array($tab,$tabArray)) {
        return 'hidden';
    }
    return '';
}


add_filter('_apl_hide_fields_unnecessary','_apl_hide_fields_unnecessary',99,1);
function _apl_hide_fields_unnecessary($fieldName = ''){
    global $sonoma_array_fields_hidden;
    if ( in_array($fieldName,$sonoma_array_fields_hidden)) {
        return 'hidden';
    }
    return '';
}



add_filter(APL_SM_HOOK_FILTER_CURRENT_ACTIVE_THEME,'_apl_get_current_active_theme_handler',99);
function _apl_get_current_active_theme_handler( $name =''){
    $currentTheme = wp_get_theme();
    return !empty($currentTheme) ? $currentTheme->stylesheet : '';
}

add_action('_apl_add_additionfields_for_page','_apl_add_additionfields_for_page',99,2);
function _apl_add_additionfields_for_page( $mod, $function){
    add_submenu_page("edit.php?post_type={$mod}", __( 'Additional Fields', 'apollo' ), __( 'Additional Fields', 'apollo' ), 'manage_options', "".$mod."_custom_fields", $function);

}