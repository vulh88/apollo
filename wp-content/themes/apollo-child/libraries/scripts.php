<?php
/**
 * Scripts and stylesheets
 */
function sonoma_scripts() {
    global $post;
    $themeUri = sm_get_stylesheet_directory_uri();
    $url_slug = isset($post) && !empty($post) ? $post->post_name : '';
    $url_slug = stripslashes($url_slug);
    $assetCss = array(
        'sonoma-fontface' => $themeUri . '/assets/css/font-proxima.css',
        'sonoma-fontface-cond' => $themeUri . '/assets/css/font-proxima-cond.css',
        'sonoma-uikit-css' => $themeUri . '/assets/css/uikit.min.css',
        'sonoma-style-css' => $themeUri . '/assets/css/style.css',
        'sonoma-style-dev-css' => $themeUri . '/style.css',
        'sonoma-introduction-css' => $themeUri . '/assets/css/introduction.css',
    );

    if(Apollo_App::isDashboardPage() || Apollo_App::isLoginPage() || Apollo_App::isRegisterPage() || Apollo_App::isRegistrationSucceedPage()) {
        $assetCss['sonoma-style-dev-user-dashboard-css'] = $themeUri . '/style-user-dashboard.css';
    }

    if(isset($post->ID) && get_page_template_slug($post->ID) == "page_sonoma_fire_2017.php"){
        $assetCss['sonoma-wildfire-css'] = $themeUri . '/assets/css/sonoma-wildfire.css';
    }


    $assetJS = array(
        'sonoma-uikit-min-js'   => '/assets/js/vendor/ukit/uikit.min.js',
        'sonoma-ofi-browser-js'   => '/assets/js/vendor/ofi.browser/ofi.browser.js',
        'sonoma-flexslider-js'   => '/assets/js/plugins/jquery.flexslider.js',
        'sonoma-main-js'   => '/assets/js/main.js',
        'sonoma-allpages-js'   => '/assets/js/actions/allpages.js',
        'sonoma-functions-js'   => '/assets/js/functions.js',
        'sonoma-introducion-js' => '/assets/js/introduction.js',
        'sonoma-event-js' => '/assets/js/event.js',
    );

    wp_dequeue_style('apollo_primary_color_css');

    foreach ( $assetCss as $key => $css ) {
        wp_enqueue_style($key, $css, false, null);
    }

    wp_enqueue_style('sonoma_primary_color_css', get_primary_color_css_url(), false, null);

    /*wp_register_script('sonoma-jquery-min-js', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', false, null, true);
    wp_enqueue_script('sonoma-jquery-min-js');*/
    wp_enqueue_script('sonoma-uiket-min-js', $themeUri. $assetJS['sonoma-uikit-min-js'], array('jquery','underscore'), null,  true);
    wp_enqueue_script('sonoma-ofi-browser-js', $themeUri. $assetJS['sonoma-ofi-browser-js'], array('jquery'), null,  true);
    wp_enqueue_script('sonoma-flexslider-js', $themeUri. $assetJS['sonoma-flexslider-js'], array('jquery'), null,  true);
    wp_enqueue_script('sonoma-main-js', $themeUri. $assetJS['sonoma-main-js'], array('jquery'), null,  true);
    wp_enqueue_script('sonoma-allpages-js', $themeUri. $assetJS['sonoma-allpages-js'], array('jquery'), null,  true);
    wp_enqueue_script('sonoma-functions-js', $themeUri. $assetJS['sonoma-functions-js'], array('jquery'), null,  true);
    wp_enqueue_script('sonoma-introducion-js', $themeUri. $assetJS['sonoma-introducion-js'], array('jquery'), null,  true);
    wp_localize_script( 'sonoma-allpages-js' , 'SM_FE', array(
            'url_slug' =>  $url_slug,
            'email_error' => __('Email is invalid', 'apollo'))
    );

    // Add additional js
    $addiJsFile = get_html_file_url(Apollo_DB_Schema::_ADDITIONAL_JS, 'js');
    if ($addiJsFile) {
        wp_enqueue_script('sonoma-additional-js', $addiJsFile, array('jquery'), null,  true);
    }
    if( !Apollo_App::isDashboardPage()){
        wp_dequeue_style('apl-dev-style');
        wp_dequeue_style('apl-css');
    }
    if ( !SonomaExtras::isTheFirstAccessToPage()){
        wp_dequeue_script('sonoma-introducion-js');
        wp_dequeue_style('sonoma-introducion-css');
    }


    if ( of_get_option( Apollo_DB_Schema::_ACTIVATE_OVERRIDE_CSS ) && file_exists(get_override_css_abs_path() ) ) {
        wp_dequeue_style('apollo_override_css');
        wp_enqueue_style('apollo_override_css', get_override_css_url(), false, null);
    }


}
add_action('wp_enqueue_scripts', 'sonoma_scripts', 105);