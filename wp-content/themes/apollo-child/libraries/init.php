<?php
/**
 * sonoma initial setup and constants
 */
define( 'SM_CIRCLE_IMAGE_SIZE', 'circle_image_size' );
function sonoma_after_setup_theme() {
    // put anything logic once the theme is activated in Appearance > Themes
}
add_action( 'after_setup_theme', 'sonoma_after_setup_theme' );

function sonoma_init_theme() {
    $arrayOptionDisable = array('_apollo_manage_states_cities','_apollo_enable_event_import_tool', '_apollo_enable_local_cache' );
    if (function_exists('sm_get_network_options')){
        foreach ( $arrayOptionDisable as $option){
            sm_get_network_options($option);
        }
    }
    $height = get_option( SM_CIRCLE_IMAGE_SIZE.'_h' );
    $width = get_option( SM_CIRCLE_IMAGE_SIZE.'_w' );

    if ( !empty( $height ) && !empty( $width ) ) {
        // Add the images sizes
        add_image_size( 'circle_image_size', $height, $width , true );
    }
}
add_action( 'init', 'sonoma_init_theme' );

