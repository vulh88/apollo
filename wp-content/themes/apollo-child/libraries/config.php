<?php
define('SONOMA_THEME_NAME', 'apollo-child');
define('SONOMA_FRONTEND_ASSETS_URI', sm_get_stylesheet_directory_uri().'/assets' );

/**
 * Define Directory Location Constants
 */
define( 'SONOMA_PARENT_DIR', sm_get_stylesheet_directory() );

define( 'SONOMA_INCLUDES_DIR', SONOMA_PARENT_DIR. '/inc' );
define( 'SONOMA_ADMIN_DIR', SONOMA_INCLUDES_DIR . '/admin' );
define( 'SONOMA_MODULES_DIR', SONOMA_INCLUDES_DIR . '/modules' );

/**
 * Default template
 */


define( 'SONOMA_THEME_HEADER', SONOMA_ADMIN_DIR. '/theme-options/default-template/header.php' );
define( 'SONOMA_THEME_TOP_HEADER', SONOMA_ADMIN_DIR. '/theme-options/default-template/top-header.php' );
define( 'SONOMA_THEME_FOOTER1', SONOMA_ADMIN_DIR. '/theme-options/default-template/footer1.php' );
define( 'SONOMA_THEME_FOOTER2', SONOMA_ADMIN_DIR. '/theme-options/default-template/footer2.php' );
define( 'SONOMA_THEME_FOOTER3', SONOMA_ADMIN_DIR. '/theme-options/default-template/siteinfo.php' );
define( 'SONOMA_THEME_FOOTER4', SONOMA_ADMIN_DIR. '/theme-options/default-template/footer.php' );
define( 'SONOMA_THEME_FOOTER5', SONOMA_ADMIN_DIR. '/theme-options/default-template/footer5.php' );
define( 'SONOMA_THEME_SUB_CONTAIN_TOP', SONOMA_ADMIN_DIR. '/theme-options/default-template/sub-contain-top.php' );
define( 'SONOMA_THEME_SUB_CONTAIN_BOTTOM', SONOMA_ADMIN_DIR. '/theme-options/default-template/sub-contain-bottom.php' );
define( 'SONOMA_THEME_OVERRIDE_CSS', SONOMA_ADMIN_DIR. '/theme-options/default-template/override.less' );



$GLOBALS['sonoma_theme_options_default_templates'] = array(
    Apollo_DB_Schema::_HEADER => SONOMA_THEME_HEADER ,
    Apollo_DB_Schema::_FOOTER3 => SONOMA_THEME_FOOTER3,
    Apollo_DB_Schema::_FOOTER4 => SONOMA_THEME_FOOTER4,
    Apollo_DB_Schema::_BOTTOM_SUB_CONTAIN => SONOMA_THEME_SUB_CONTAIN_BOTTOM,
    Apollo_DB_Schema::_TOP_SUB_CONTAIN => SONOMA_THEME_SUB_CONTAIN_TOP,
    Apollo_DB_Schema::_FOOTER1 => SONOMA_THEME_FOOTER1,
);

$GLOBALS['sonoma_array_taxonomy'] = array(
    'classified-type' => Apollo_DB_Schema::_CLASSIFIED,
    'artist-type' => Apollo_DB_Schema::_ARTIST_PT,
    'artist-medium' => Apollo_DB_Schema::_ARTIST_PT,
    'artist-style' => Apollo_DB_Schema::_ARTIST_PT,
    'organization-type' => Apollo_DB_Schema::_ORGANIZATION_PT,
    'public-art-location' => Apollo_DB_Schema::_PUBLIC_ART_PT,
    'public-art-type' => Apollo_DB_Schema::_PUBLIC_ART_PT,
    'public-art-collection' => Apollo_DB_Schema::_PUBLIC_ART_PT,
    'public-art-medium' => Apollo_DB_Schema::_PUBLIC_ART_PT,
    'venue-type' => Apollo_DB_Schema::_VENUE_PT,
    'event-type' => Apollo_DB_Schema::_EVENT_PT

);

$GLOBALS['sonoma_array_fields_hidden'] = array(
    __('Navigation', 'apollo'),
    Apollo_DB_Schema::_NAVIGATION_BAR_STYLE,
    __('Top Header', 'apollo'),
    Apollo_DB_Schema::_ENABLE_TOP_HEADER,
    Apollo_DB_Schema::_SCROLL_WITH_PAGE,
    Apollo_DB_Schema::_TOP_HEADER,
    Apollo_DB_Schema::_FOOTER2,
    Apollo_DB_Schema::_FOOTER5,
    __('Footer 2 - Bottom Navigation (columns links)', 'apollo'),
    __('Footer 5 - Artsopolis network member city list', 'apollo'),
    __('Default Layout Homepage', 'apollo'),
    Apollo_DB_Schema::_DEFAULT_HOME_LAYOUT,
    __('Homepage Settings', 'apollo'),
    Apollo_DB_Schema::_SPOT_TYPE,
    Apollo_DB_Schema::_COLOR_SELECTION_FONT_TITLE,
    Apollo_DB_Schema::_COLOR_SELECTION_FONT,
    __('Home Featured Articles', 'apollo'),
    Apollo_DB_Schema::_HOME_FEATURED_ARTICLES_TITLE,
    Apollo_DB_Schema::_ACTIVE_HOME_FEATURED_ARTICLES,
    Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_LARGE_LEFT,
    Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_LARGE_RIGHT,
    Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_BOTTOM,
    Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_MIDDLE,
    Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_TOP,
    Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_POSTION,
    __('Social Settings', 'apollo'),
    Apollo_DB_Schema::_FACEBOOK,
    Apollo_DB_Schema::_LINKEDIN,
    Apollo_DB_Schema::_TWITTER,
    Apollo_DB_Schema::_YOUTUBE,
    Apollo_DB_Schema::_VIMEO,
    __('Enable Facebook Features', 'apollo'),
    Apollo_DB_Schema::_FB_ENABLE,
    Apollo_DB_Schema::_FB_APP_ID,
    Apollo_DB_Schema::_FB_API_KEY,
    Apollo_DB_Schema::_FB_SECRET_KEY,
    __('Google map', 'apollo'),
    Apollo_DB_Schema::_GOOGLE_MAP_MY_POSITION_MARKER_ICON,
    Apollo_DB_Schema::_GOOGLE_MAP_MARKER_ICON,
    Apollo_DB_Schema::_GOOGLE_MAP_MARKER_ICON_WIDTH,
    Apollo_DB_Schema::_GOOGLE_MAP_MARKER_ICON_HEIGHT,

    __('Hovering Color on Listing', 'apollo'),
    Apollo_DB_Schema::_HOVERING_SEARCH_CONTENT_COLOR,
    __('Menu Color Setting', 'apollo'),
    Apollo_DB_Schema::_NAV_TWO_TIER_MAIN_HOVER_BG_COLOR,
    Apollo_DB_Schema::_NAV_SUB_L1_COLOR,
    Apollo_DB_Schema::_NAV_SUB_L1_BG_COLOR,
    Apollo_DB_Schema::_NAV_SUB_L1_HOVER_COLOR,
    Apollo_DB_Schema::_NAV_SUB_L1_HOVER_BG_COLOR,
    Apollo_DB_Schema::_NAV_SUB_L2_COLOR,
    Apollo_DB_Schema::_NAV_SUB_L2_HOVER_COLOR,
    Apollo_DB_Schema::_NAV_SUB_L2_HOVER_BG_COLOR,
    Apollo_DB_Schema::_NAV_SUB_L2_BG_COLOR,
    Apollo_DB_Schema::_BLOG_NUM_OF_CHAR,
    Apollo_DB_Schema::_BLOG_NUM_OF_HEADLINE,
    Apollo_DB_Schema::_BLOG_DISPLAY_STYLE,
    Apollo_DB_Schema::_ENABLE_ORG_BYPASS_APPROVAL_PROCESS,
    Apollo_DB_Schema::_ENABLE_VENUE_BYPASS_APPROVAL_PROCESS,
    Apollo_DB_Schema::_ENABLE_ARTIST_BYPASS_APPROVAL_PROCESS,
    Apollo_DB_Schema:: _ENABLE_CLASSIFIED_BYPASS_APPROVAL_PROCESS,
    Apollo_DB_Schema::_BLOG_LISTING_TYPE,
    Apollo_DB_Schema::_ARTIST_ACTIVE_MEMBER_FIELD,
    Apollo_DB_Schema::_EVENT_SEARCH_TYPE,
    Apollo_DB_Schema::_CATEGORY_PAGE_DATE_DISPLAY_OPTIONS
);
/**
 * Define URL Location Constants
 */
define( 'SONOMA_PARENT_URL', sm_get_stylesheet_directory_uri() );

define( 'SONOMA_ASSETS', SONOMA_PARENT_URL. '/assets' );

define( 'SONOMA_CSS_URL', SONOMA_ASSETS . '/css' );
define( 'SONOMA_JS_URL', SONOMA_ASSETS . '/js' );
define( 'SONOMA_IMAGES_URL', SONOMA_ASSETS . '/images' );

define( 'SONOMA_INCLUDES_URL', SONOMA_PARENT_URL. '/inc' );
define( 'SONOMA_ADMIN_URL', SONOMA_INCLUDES_URL. '/admin' );

define( 'SONOMA_ADMIN_ASSETS', SONOMA_ADMIN_URL. '/assets' );

define( 'SONOMA_ADMIN_CSS_URL', SONOMA_ADMIN_ASSETS . '/css' );
define( 'SONOMA_ADMIN_JS_URL', SONOMA_ADMIN_ASSETS . '/js' );
define( 'SONOMA_ADMIN_IMAGES_URL', SONOMA_ADMIN_ASSETS . '/images' );

define( 'SONOMA_DIRECTORY_LISTING_DEFAULT_VIEW_TYPE', 1 ); // default view type = 1 : 'thumbs' : 2 : 'lists'

/* ThienLD : define circle image size width & height */
define ( 'SONOMA_CIRCLE_IMAGE_WIDTH', 250);
define ( 'SONOMA_CIRCLE_IMAGE_HEIGHT', 250);



$GLOBALS['sonoma_available_modules'] = array(
    Apollo_DB_Schema::_ORGANIZATION_PT,
    Apollo_DB_Schema::_ARTIST_PT,
    Apollo_DB_Schema::_PUBLIC_ART_PT,
    Apollo_DB_Schema::_CLASSIFIED,
    Apollo_DB_Schema::_VENUE_PT,
    Apollo_DB_Schema::_EVENT_PT,
);