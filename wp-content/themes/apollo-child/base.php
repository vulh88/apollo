<?php  if(Apollo_App::isDashboardPage() || get_query_var('_apollo_artist_timeline') === 'true' ) : ?>
    <?php get_template_part('base-userdashboard'); ?>
<?php else : ?>
    <!doctype html>
    <!--[if IE 8]><html lang="en" class="no-js ie ie8"><![endif]-->
    <!--[if IE 9]><html lang="en" class="no-js ie ie9"><![endif]-->
    <!--[if gt IE 9]><!-->
    <html class="no-js" <?php language_attributes(); ?>>
    <!--<![endif]-->
    <?php get_template_part('partials/head'); ?>
    <body <?php body_class(); ?>>
    <?php if ( SonomaExtras::isTheFirstAccessToPage()) {
		get_template_part('partials/content-animation-homepage');
	 }?>
    <div class="layout layout-fluid">
        <header>
            <?php get_template_part('partials/header') ?>
        </header>
        <main>
            <?php if(!is_404()) : ?>
                <?php if (is_front_page()) { ?>
                    <div class="content-wrapper content-wrapper--master-layout clearfix ">
                        <div class="block block-carousel clearfix">
                            <?php get_template_part('partials/main-slider') ?>
                        </div>
                        <?php get_template_part('partials/content-home') ?>
                    </div>
                <?php } elseif(Apollo_App::isBlogPostsPage() ) {  ?>
                    <?php
                    if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_BLOG_POST_PT ) ) {
                        wp_safe_redirect( '/' );
                    }
                    get_template_part('single'); ?>
                <?php } elseif(is_page() && !is_front_page()) {  ?>
                    <?php get_template_part('page'); ?>
                <?php } elseif(get_query_var('_apollo_public_art_map') === 'true') {  ?>
                    <?php
                    set_query_var('_apollo_public_art_search','true');
                    get_template_part('custom-post-type');
                    ?>
                <?php } else { ?>
                    <?php get_template_part('custom-post-type'); ?>
                <?php } ?>
            <?php else : ?>
                <?php get_template_part('404'); ?>
            <?php endif;
            
			if(session_id() == '' || !isset($_SESSION)) {
                // session isn't started
                session_start();
            }
            if( !isset($_SESSION['home_page'])) {
                $_SESSION['home_page'] = true;
            }
			?>
        </main>
    </div>
    <footer>
        <?php get_template_part('partials/footer') ?>
    </footer>

    <style>
        html,body {
            margin: 0 !important;
        }
    </style>
    <script>(function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-XXXXX-X', 'auto');
        ga('send', 'pageview');

        (function () {
            'use strict';
            console.log(objectFitImages());
        })();

    </script>

    </body>
    </html>
<?php endif; ?>
