'use strict';

/**
 * ALL PAGE
 * 1. Run slider
 */

var allpage_js = {};
(function ($) {

    /*1. Run slider */
    allpage_js = {
        runSlider: function runSlider(main_slider, controlClass, startCb, afterCb) {
            if (!$(main_slider).length) {
                return;
            }
            $(main_slider).flexslider({
                initDelay: 9300,
                animation: 'slide',
                controlNav: true,
                directionNav: true,
                manualControls: controlClass,
                slideshowSpeed: 7000,
                customDirectionNav: $(".custom-navigation a"),
                useCSS: false,
                start: function start(slide) {
                    if (typeof startCb === 'function') {
                        startCb($(slide).find('.flex-active-slide >div'));
                    }
                },
                after: function after(slide) {
                    if (typeof afterCb === 'function') {
                        afterCb($(slide).find('.flex-active-slide >div'));
                    }
                }
            });
        },
        bottomSlider: function bottomSlider(itemName) {
            $(itemName).flexslider({
                animation: "slide",
                animationLoop: false,
                controlNav: true,
                directionNav: true,
                customDirectionNav: $(".cust-nav a"),
                itemWidth: 315,
                minItems: 4,
                maxItems: 4
            });
        },
        processSetUrl: function(page, url) {
            var obj = { Page: page, Url: url };
            history.pushState(obj, obj.Page, obj.Url);
        },
        stopScrollItem: function stopScrollItem() {
            
            if (!$('.rich-txt').length) { return; }

            var $itemClick = $('.rich-txt').find('a[href^="#"]');
            var headHeigh = $('header').outerHeight() + $('#wpadminbar').outerHeight() + 15;
            var url      = window.location.href;

            $itemClick.each(function(index, value) {
                $(value).attr('data-uk-smooth-scroll', '{offset: ' + headHeigh + '}');
                $(this).on('click', function(){
                    var $linkItem = $(this).attr('href');
                    var currentLnk = window.location.pathname + $linkItem;
                    allpage_js.processSetUrl('',currentLnk);
                });
            });
        }
    };

    $(document).ready(function () {
        // Run slider
        function changeColor(activeBg) {
            var prvBtn = $('.custom-navigation .sli-icon.icon-prev');
            var nextBtn = $('.custom-navigation .sli-icon.icon-next');
            var nextColor = activeBg.attr('data-color-next-button');
            var prvColor = activeBg.attr('data-color-prev-button');
            prvBtn.css({ 'background-color': prvColor });
            nextBtn.css({ 'background-color': nextColor });
        };
        allpage_js.runSlider('.sm-main-slider', '.flex-control-nav li', function (activeBg) {
            changeColor(activeBg);
        }, function (activeBg) {
            changeColor(activeBg);
        });


        // Scroll to item
        allpage_js.stopScrollItem();
    });

    $(window).load(function(){
        if(window.location.hash) {
            var hashTag = window.location.hash;
            $('.rich-txt').find('a[href="'+hashTag+'"]').trigger('click');
        }
    });
    /* OnLoad Window */
    var init = function init() {};
    $(document).on('click', '.hamburger-menu', function () {
        $('body').toggleClass('menu-open');

    });
    window.onload = init;
})(jQuery);

