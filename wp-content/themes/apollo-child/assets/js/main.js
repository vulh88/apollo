'use strict';

/*!
 *
 *  Web Starter Kit
 *  Copyright 2015 Google Inc. All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    https://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 *
 */
/* eslint-env browser */

(function ($) {
  'use strict';

  // Check to make sure service workers are supported in the current browser,
  // and that the current page is accessed from a secure origin. Using a
  // service worker from an insecure origin will trigger JS console errors. See
  // http://www.chromium.org/Home/chromium-security/prefer-secure-origins-for-powerful-new-features


  console.log(objectFitImages());
  $(document).on('click', '.cm', function(event) {
    event.stopPropagation();
  });
  $(document).ready(function () {
    // $('.top-search .el-blk select').select2();
    $("iframe").closest("p").css({"margin-top":"25px","padding-bottom": "0","position" : "relative", "width" : "100%", "height" :"360px", "text-align" :"center"});
    var thum_w = 80,
        total_gallerys  = $( 'input[name="total-gallerys"]' ).val();
    $(".photo-inner .pbx-wrapper").css({"max-width":thum_w * total_gallerys + (total_gallerys -1 )*5});

    // $('.cm').click(function(){
    //     var target = $( '#comment' )
    //     if ( ! $('#comment').length ) {
    //         target = $( '.comments-area' )
    //     }

    //     $('html,body').animate({ scrollTop: target.position().top }, 1000);
    //     return false;
    // });
    $("iframe").closest(".audio-list li p").css({"margin-top":"25px","padding-bottom": "0","position" : "relative", "width" : "100%", "max-height" :"400px", "text-align" :"center"});
  });
  // Your custom JavaScript goes here
})(jQuery);