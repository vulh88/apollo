jQuery(document).ready(function () {

    //function show popup
    function showPopupCalendar($this){
        var directSubmit = $('#calendar_action_button').attr('data-type');
        if(directSubmit == 0){
            $( '#_popup_choose_event .modal-content' ).css( 'z-index', 20 );
            console.log($this.attr('target'));
            var _pid = $this.attr('target');
            var $_popup = $(_pid);
            var _height_popup = $_popup.outerHeight();

            $_popup.addClass($_popup.data('active')).css({
                'margin-top': -1*(_height_popup/2)
            });
            //e.stopPropagation();
        }else{
            $('#calendar_action_button').trigger('click');
        }

    }

    $('.calendar_btn').on('click',function(e) {
        $('#export_ics_frm').attr('action','/export_calendar/event/');
        $('#export_ics_frm').attr('method','POST');
        $('#calendar_action_button').text($(this).attr('title'));

        var $this = $(this);
        showPopupCalendar($this);
    });
    $('.share-calendar').on('click',function(e){
        $('#export_ics_frm').attr('action','http://www.google.com/calendar/event');
        $('#export_ics_frm').attr('method','GET');
        $('#export_ics_frm').attr('target','_blank');
        $('#calendar_action_button').text($(this).attr('title'));
        var $this = $(this);
        showPopupCalendar($this);

    });
    $('#calendar_action_button').on('click',function(){
        $('#export_ics_frm').submit();
    });

    return {
        init: function () {

        }
    };
});