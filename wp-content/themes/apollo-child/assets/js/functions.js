(function ($) {
    $(document).on('click', '.top-search-row .btn-l.s', function() {
        $('.top-search').toggleClass('expand');
    });

    $(document).on('keyup', '.top-search .event-search', function() {
        $('.top-search').toggleClass('expand');
    });
    $('.frm-sign-up ').on('click', '.button', function() {
        var email = $('input[name="ea"]').val();
        if ( isEmail(email)){
            this.closest('form').submit();
        } else {
            alert(SM_FE.email_error);
        }
    });
})

(jQuery);

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}