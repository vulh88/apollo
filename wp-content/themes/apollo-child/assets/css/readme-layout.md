======HTML STRUCTURE======
<div class="content-wrapper content-wrapper--master-layout clearfix [style-wrapper-class]">

    <!--Start Sample custom block-->
    <div class="custom-block-class [width-style]">
    </div>
    <!--End Sample custom block-->

    <!--Start SIDEBAR LEFT-->
    <div class="sidebar-left clearfix">
        <h2>Sidebar left</h2>
    </div>
    <!--END SIDEBAR LEFT-->

    <!--Start MAIN CONTAIN-->
    <div class="main-contain-wrapper clearfix">
    </div>
    <!--Start MAIN CONTAIN-->

    <!--Start SIDEBAR RIGHT-->
    <div class="sidebar-right clearfix">
        <h2>Sidebar right</h2>
    </div>
    <!--Start SIDEBAR RIGHT-->
</div>

======STYLE APPLY======
[style-wrapper-class] are defined with pre-config width, and sidebar left, sidebar right as follow
    'content-width-fluid' : all the container will have width 100% (DO NOT add 'content-width-1000') 
    'content-width-1000' : all the container will have width 1000px (DO NOT add 'content-width-fluid')
    'has-sidebar-left': content-wrapper will have a child with class name 'sidebar-left' as sample structure abow 
    'has-sidebar-right': content-wrapper will have a child with class name 'sidebar-right' as sample structure abow 

[width-style]
    'content-width-1000'
    'content-width-fluid'


In Fluid mode the width will be measure in percentage (%)
    'sidebar-left' and 'sidebar-right' will ocupied 20% for each one
    'main-contain-wrapper' will ocupied 60% or 80% in case of 2 side bars or 1 side bar respectively

In 1000px mode the width will be measure in pixels (px)
    'sidebar-left' and 'sidebar-right' will ocupied 200px for each one
    'main-contain-wrapper' will ocupied 600px or 800px in case of 2 side bars or 1 side bar respectively

We shuold have 'clearfix' class on 'sidebar-left', 'sidebar-right' and 'main-contain-wrapper'

====== RICH TEXT EDITOR RESET HTML======
you add main style and modifier style to wrapper
STYLE syntax : 'rich-txt rich-txt--[modifier]'
MODIFIER: 
    'rich-txt--hight-first-child'


modify and update layout using this source below. 
online transpile http://www.sassmeister.com/ to compile this SASS to CSS layout
=================== LAYOUT SCSS ===================
/**
* style for child element of .content-wrapper 
*/ 

.content-wrapper--master-layout {
    width: 100%;
    &:not(.has-sidebar-right):not(.has-sidebar-left) {
      >.sidebar-left, >.sidebar-right {
        display: none;
      }
    }
    &.content-width-fluid {
      > * {
        max-width: 100%;
      }
      &.has-sidebar-left:not(.has-sidebar-right) {
        .main-contain-wrapper {
          width: 80%;
          float: left;
        }
        .sidebar-right {
          display: none;
        }
      }
      &.has-sidebar-right:not(.has-sidebar-left) {
        .main-contain-wrapper {
          width: 80%;
          float: left;
        }
        .sidebar-left {
          display: none;
        }
      }
      &.has-sidebar-right.has-sidebar-left {
        .main-contain-wrapper {
          width: 60%;
          float: left;
        }
      }
      >.sidebar-left, >.sidebar-right {
        min-height: 1px;
        width: 20%;
        float: left;
      }
    }
    &.content-width-1000 {
      > * {
          max-width: 1000px;
          margin-left: auto;
          margin-right: auto;
      }
      &.has-sidebar-left:not(.has-sidebar-right) {
        .main-contain-wrapper {
          width: 800px;
          float: left;
        }
        >.sidebar-left {
          margin-left: calc(50vw - 500px);
        }
        .sidebar-right {
          display: none;
        }
      }
      &.has-sidebar-right:not(.has-sidebar-left) {
        .main-contain-wrapper {
          width: 800px;
          float: left;
          margin-left: calc(50vw - 500px);
        }
        .sidebar-left {
          display: none;
        }
      }
      &.has-sidebar-right.has-sidebar-left {
        .main-contain-wrapper {
          width: 600px;
          float: left;
        }
        >.sidebar-left {
          margin-left: calc(50vw - 500px);
        }
      }
      >.sidebar-left, >.sidebar-right {
        min-height: 1px;
        width: 200px;
        float: left;
      }
    }
    .force-width-1000 {
      max-width: 1000px !important;
      margin-left: auto;
      margin-right: auto;
    }
    
    >.force-width-fluid {
      max-width: 100% !important;
    }
}

.rich-txt {
  * {
    font-family: 'ProximaNova-Regular', sans-serif;
  }
  p, span, strong, em, i, b, strong, li {
    font-size: 1.4rem;
  }
  
  p {
    margin: 0 0 10px;
    clear: both;
  }
  
  h1 {
    font-size: 3.6rem;
    font-weight: bold;
  }
  h2 {
    font-size: 3rem;
    font-weight: bold;
  }
  
  h3 {
    font-size: 2.4rem;
    font-weight: bold;
  }
  
  h4 {
    font-size: 1.8rem;
    font-weight: bold;
  }
  
  h5 {
    font-size: 1.4rem;
    font-weight: bold;
  }
  
  h6 {
    font-size: 1.2rem;
    font-weight: bold;
  }
  
  pre {
    font-family: monospace, monospace;
    font-size: 1em;
  }
  
  ul {
    line-height: 1.7em;
    list-style-type: disc;
    margin-left: 30px;
    li {
      list-style-type: disc;
    }
  }
  
  blockquote {
      padding: 10px 20px;
      margin: 0 0 20px;
      font-size: 1.75rem;
      border-left: 5px solid #eeeeee;
  }
  
  hr {
      margin-top: 20px;
      margin-bottom: 20px;
      border: 0;
      border-top: 1px solid #eeeeee;
  }
  
  &.rich-txt--hight-first-child {
    >p:first-child {
      font-size: 1.4em;
      line-height: 1.4em;
      color: #f95f22;
    }
  }
}