<?php

/**
 * Class SM_Artist_Mod
 */
require_once __DIR__.'/inc/sm_blog_model.php';
class SM_Blog_Mod extends SM_Mod_Abstract {


	public function __construct() {
		$this->renderPostTemplate();
	}

    public function renderPostTemplate(){
        $smPostModel = new SM_Blog_Model();
        if ( is_single() ){
            $file = SONOMA_MODULES_DIR.'/post/templates/post-details.php';
        } else{
            $file = SONOMA_MODULES_DIR.'/post/templates/post.php';
        }
        $this->smGetTemplatePartCustom( $file, $smPostModel );
    }

}

new SM_Blog_Mod();