<div class="content-wrapper content-wrapper--master-layout clearfix">
    <?php if (!empty($model) ):?>
        <div class="block block-details">
            <div class="detail-banner"
                 style="background: url(<?php echo $model->getSrc() ?>) no-repeat top;  position: relative; height: 606px; background-size: cover;">
                <div class="detail-ttl"><h3><?php echo $model->getTitle(); ?></h3></div>
            </div>
            <div class="detail-body" style="background-image: url(<?php echo $model->getBackgroundImage() ?>)">
                <div class="desc-detail rich-txt">
                    <?php echo $model->getSummary(); ?>
                </div>
                <div class="specify-detail rich-txt">
                    <?php echo $model->getContent(); ?>
                </div>
            </div>
        </div>

        <div class="block block-share">
            <input name="eventid" value="<?php echo $model->getPostId() ?>" type="hidden">
            <?php echo  $model->getSharingInfo(array( 'print', 'sendmail' ), $model->getPostId());?>
        </div>
    <?php  endif; ?>
</div>