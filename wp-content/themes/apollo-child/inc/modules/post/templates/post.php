<div class="content-wrapper">
        <div class="block block-events block-events--post clearfix">
            <div class="breadcrumbs blog">
                <ul class="nav">
                    <li><a href="/" ><?php _e( 'Home' ) ?></a></li>

                    <?php if ( get_query_var( '_apollo_page_blog' ) ) { ?>
                        <li><span><?php echo $model->getCustomLabel(); ?></span></li>
                    <?php } else { ?>
                        <li><span><a href="<?php echo $model->getCustomUrlBySlug(); ?>"><?php echo $model->getCustomLabel(); ?></a></span></li>
                        <li> <span><?php if (is_author() ) {
                                    echo get_the_author();
                                } else {
                                    single_cat_title();
                                } ?></span></li>
                    <?php } ?>

                </ul>
            </div>
            <?php echo $model->renderListingPage(); ?>
        </div>
</div>