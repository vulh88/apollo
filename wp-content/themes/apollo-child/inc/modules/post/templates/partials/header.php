<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/15/2015
 * Time: 10:41 AM
 */
?>
<div class="b-share-cat">
    <?php
    SocialFactory::social_btns( array( 'info' => $arrShareInfo, 'id' => $post_id ) );
    ?>
</div>

<div class="blog-tt">
    <div class="blog-name">
        <?php
            if(is_single()){
                ?>
                <div class="namedetail-txt blog-date parent-not-underline-hover"><a><?php echo get_the_title();  ?></a></div>
                <?php if ($excerpt = get_the_excerpt()): ?>
                <p><?php echo $excerpt ?></p>
                <?php endif; ?>
                <?php
            }else{
                ?>
                <div class="namedetail-txt blog-date parent-not-underline-hover">
                    <a href="<?php echo get_the_permalink(); ?>">
                        <?php echo get_the_title();  ?>
                    </a>
                </div>
                <?php
            }
        ?>

    </div>
</div>

<?php
//if ( (($enableDate || $enableAuthor || $enableThumbUp || $enableCommentNumber) && is_single()) || ! is_single()): ?>
<div class="blog-category">

    <?php
    //if ($enableDate): ?>
    <div class="cate-blk parent-not-underline-hover apl-blog-date">
        <i class="fa fa-calendar fa-lg">&nbsp;&nbsp;&nbsp;</i><span style="  float: left;
                        padding: 5px 0px;"><?php echo Apollo_App::apl_date( 'M d, Y', get_post_time() ) ?></span><a class="slash">/</a>
    </div>
    <?php //endif; ?>

    <?php  if( !get_query_var('author') ){ ?>
    <div class="cate-blk parent-not-underline-hover apl-blog-author">
        <i class="fa fa-user fa-lg">&nbsp;&nbsp;&nbsp;</i><?php echo the_author_posts_link() ?><a class="slash">/</a>
    </div>
    <?php } ?>

    <?php
        if(!is_single()){
            ?>
            <div class="cate-blk apl-blog-cat"><i class="fa fa-folder-open fa-lg">&nbsp;&nbsp;</i>
                <?php $blogType = of_get_option( Apollo_DB_Schema::_BLOG_CATEGORY_TYPES, 'parent-only');
                Apollo_App::apl_get_blog_categories($post_id,$blogType)?>
                <a class="slash">/</a>
            </div>
            <?php
        }
    ?>

    <?php
    //if ($enableThumbUp || $enableCommentNumber):
    ?>
        <div class="cate-blk  parent-not-underline-hover apl-blog-thumbup">

            <?php //if ( $enableThumbUp ): ?>
            <div  class="like parent-not-underline-hover" id="_event_rating_<?php echo $post_id. '_' . 'post' ?>"
                  data-ride="ap-event_rating" data-i="<?php echo $post_id ?>"
                  data-t="post"> <a href="javascript:void(0);" style="padding-top: 1px">
                    <span class="_count">&nbsp;</span></a>
            </div>
            <?php //endif; ?>

            <?php //if ( $enableCommentNumber && comments_open($post_id) ): ?>
            <div class="cate-blk  cmb apl-blog-cmb">
            <i class="fa fa-comment fa-lg">&nbsp;&nbsp;</i>
            <?php
            $cn = get_comments_number( get_the_ID() );
            $cn = $cn && $cn < 10 ? '0'. $cn :  $cn;
            ?>
            <a class="">
                <?php
                $default = sprintf( '%s comment', '<span class="cm-num">(1)</span>' );
                echo  sprintf( _n( $default, '%s comments', $cn, 'apollo' ), '<span class="cm-num">('.number_format_i18n( $cn ). ')</span>' );
                ?>
            </a>
            </div>
            <?php // endif; ?>

        </div>
    <?php //endif; ?>

</div>
<?php
    //endif; // End !$enableDate || !$enableAuthor || !$enableThumbUp || !$enableCommentNumber
?>

