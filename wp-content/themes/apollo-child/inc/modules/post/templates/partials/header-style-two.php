
<div class="blog-category dt-blog">

        <div class="cate-blk apl-blog-cat"><i class="fa fa-folder-open fa-lg">&nbsp;&nbsp;</i>
            <?php $blogType = of_get_option( Apollo_DB_Schema::_BLOG_CATEGORY_TYPES, 'parent-only');
            Apollo_App::apl_get_blog_categories($post_id,$blogType)?>
            <a >&nbsp; | </a>
        </div>


    <?php
    //if ($enableDate): ?>
    <div class="cate-blk parent-not-underline-hover apl-blog-date">
        <i class="fa fa-calendar fa-lg">&nbsp;&nbsp;&nbsp;</i><span style="  float: left;
                        padding: 5px 0px;"><?php echo Apollo_App::apl_date( 'M d, Y', get_post_time() ) ?>
    </div>
    <?php //endif; ?>
</div>
<div class="blog-tt">
    <div class="blog-name">
        <?php
        if(is_single()){
            ?>
            <div class="namedetail-txt blog-date parent-not-underline-hover"><a><?php echo get_the_title();  ?></a></div>
            <?php if ($excerpt = get_the_excerpt()): ?>
                <p><?php echo $excerpt ?></p>
            <?php endif; ?>
            <?php
        }else{
            ?>
            <div class="namedetail-txt blog-date parent-not-underline-hover">
                <a href="<?php echo get_the_permalink(); ?>">
                    <?php echo get_the_title();  ?>
                </a>
            </div>
            <?php
        }
        ?>

    </div>
</div>
<div class="blog-category author-blog">

    <div class=" apl-blog-author">
         <i class="fa fa-user fa-lg">&nbsp;&nbsp;&nbsp;</i><?php echo _e('BY ', 'apollo').the_author_posts_link() ?>
    </div>

</div>
<div class="b-share-cat blog-category social-blog">
    <?php
    SocialFactory::social_btns( array( 'info' => $arrShareInfo, 'id' => $post_id ) );
    ?>
</div>


