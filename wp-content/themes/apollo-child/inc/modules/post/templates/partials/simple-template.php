
<?php

$args = $model->getArgsForQueryPosts();

query_posts($args);

?>
<div class="blog-bkl" style="margin-top: 20px;">
    <?php
    $blogPageContent = Apollo_App::get_post_info(Apollo_Page_Creator::ID_BLOG,'page');
    if($blogPageContent && !is_author() ){?>
        <div class="art-desc apl-internal-content">
            <?php   echo Apollo_App::convertContentEditorToHtml($blogPageContent->post_content, true); ?>
        </div>
        <?php
    }
    ?>
    <nav class="blog-list">
        <?php if ( have_posts() ):
            $character = of_get_option(Apollo_DB_Schema::_BLOG_NUM_OF_CHAR,Apollo_Display_Config::_BLOG_NUM_OF_CHAR) ;

            add_filter( 'excerpt_length', function() {
                return of_get_option(Apollo_DB_Schema::_BLOG_NUM_OF_CHAR,Apollo_Display_Config::_BLOG_NUM_OF_CHAR);
            }, 999 );

            while (have_posts() ) : the_post();
                $post_id  = get_the_ID();
               ?>

                <li>
                    <div class="blog-content no-margin">
                        <div class="blog-pic-detail allow-max-height">
                            <a href="<?php the_permalink() ?>">
                                <?php echo has_post_thumbnail() ? get_the_post_thumbnail( $post_id, 'medium' ) : Apollo_App::defaultBlogImage('medium', false); ?>
                            </a>
                            <?php if (  function_exists( 'the_media_credit_html' ) ) { ?>
                                <span class="media-credit not-detail"><?php the_media_credit_html( get_post_thumbnail_id( $post_id ) );?></span>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="blog-category-list">
                        <?php
                        if(!is_single()){
                            ?>
                            <div class="cate-blk apl-blog-cat">
                                <?php $blogType = of_get_option( Apollo_DB_Schema::_BLOG_CATEGORY_TYPES, 'parent-only');
                                Apollo_App::apl_get_blog_categories($post_id,$blogType)?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="blog-tt blog-category-list">
                        <div class="blog-name">
                            <?php
                            if(is_single()){
                                ?>
                                <div class="namedetail-txt blog-date parent-not-underline-hover"><a><?php echo get_the_title();  ?></a></div>
                                <?php
                            }else{
                                ?>
                                <div class="namedetail-txt blog-date parent-not-underline-hover">
                                    <a href="<?php echo get_the_permalink(); ?>">
                                        <?php echo get_the_title();  ?>
                                    </a>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                  <?php  if( !get_query_var('author') ){ ?>
                    <div class=" blog-category-list author-blog-listing">
                        <div class="cate-blk parent-not-underline-hover apl-blog-author listing-post-author">
                          <?php echo _e('by ', 'apollo').the_author_posts_link() ?>
                        </div>
                    </div>
                    <?php }?>

                    <div class="blog-text center">
                        <?php $more =  '&nbsp;<a class="vmore" href="'. get_permalink( get_the_ID() ) . '">' . __('View more', 'apollo') . '</a>';?>
                        <p><?php
                            $summary = get_post_meta($post_id,'post-summary',true);
                            echo Apollo_App::my_cut_excerpt($summary,$character,$more); ?></p>
                    </div>

                </li>
            <?php endwhile; ?>
        <?php else: ?>
            <div><?php _e( "Don't have any post !", 'apollo' ); ?></div>
        <?php endif; ?>
    </nav>
</div>
<?php
$next_link = get_next_posts_link();
$prev_link = get_previous_posts_link();

if ( $next_link || $prev_link ):
    ?>
    <div class="blog-bkl">
        <div class="blk-paging">
        <span class="pg-tt"><?php _e( 'Pages', 'apollo' ) ?>:
            <?php apollo_blog_paging_nav() ?>
        </span>
        </div>
    </div>

<?php endif; ?>