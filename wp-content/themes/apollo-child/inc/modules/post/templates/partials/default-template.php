
<?php

$args = $model->getArgsForQueryPosts();

query_posts($args);
?>
<div class="blog-bkl">
    <?php
    $blogPageContent = Apollo_App::get_post_info(Apollo_Page_Creator::ID_BLOG,'page');
    if($blogPageContent && !is_author() ){ ?>
        <div class="art-desc apl-internal-content">
            <?php   echo Apollo_App::convertContentEditorToHtml($blogPageContent->post_content, true); ?>
        </div>
        <?php
    }
    ?>
    <nav class="blog-list">
        <?php if ( have_posts() ):
            $character = of_get_option(Apollo_DB_Schema::_BLOG_NUM_OF_CHAR,Apollo_Display_Config::_BLOG_NUM_OF_CHAR) ;

            add_filter( 'excerpt_length', function() {
                return of_get_option(Apollo_DB_Schema::_BLOG_NUM_OF_CHAR,Apollo_Display_Config::_BLOG_NUM_OF_CHAR);
            }, 999 );

//            $enableDate   = of_get_option( Apollo_DB_Schema::_BLOG_ENDABLE_DATE, 1 );
//            $enableAuthor = of_get_option( Apollo_DB_Schema::_BLOG_ENDABLE_AUTHOR, 1 );
//            $enableThumbUp = of_get_option( Apollo_DB_Schema::_BLOG_ENDABLE_THUMB_UP, 1 );
//            $enableCommentNumber = of_get_option( Apollo_DB_Schema::_BLOG_ENDABLE_COMMENT, 1 );
//
            while (have_posts() ) : the_post();

                add_filter( 'excerpt_more', function () {return '';});
                $post_id = get_the_ID();
                $arrShareInfo = array(
                    'url' => get_permalink(),
                    'summary' => Apollo_App::my_cut_excerpt(get_the_excerpt(),$character),
                    'media' => wp_get_attachment_thumb_url( get_post_thumbnail_id( $post_id ) )
                );

		if ( isset($_GET['debug']) ) {

			$more =  '&nbsp;<a class="vmore" href="'. get_permalink( get_the_ID() ) . '">' . __('View more', 'apollo') . '</a>';
		$a = Apollo_App::my_cut_excerpt(get_the_excerpt(),$character,$more);

		}
            ?>

            <li>
                <?php
                    include(SONOMA_MODULES_DIR.'/post/templates/partials/header.php');
                ?>
                <div class="blog-content">
                    <div class="blog-pic">
                        <a href="<?php the_permalink() ?>"><?php echo has_post_thumbnail() ? get_the_post_thumbnail( $post_id, 'medium' ) : Apollo_App::defaultBlogImage('medium', false, 'auto'); ?></a>
                        <?php if (  function_exists( 'the_media_credit_html' ) ) { ?>
                            <span class="media-credit "><?php the_media_credit_html( get_post_thumbnail_id( $post_id ) );?></span>
                        <?php } ?>
                    </div>
                    <div class="blog-text">
                        <?php $more =  '&nbsp;<a class="vmore" href="'. get_permalink( get_the_ID() ) . '">' . __('View more', 'apollo') . '</a>';?>
                        <p><?php
                            $summary = get_post_meta($post_id,'post-summary',true);
                            echo Apollo_App::my_cut_excerpt($summary,$character,$more); ?></p>
                    </div>
                </div>

                <?php
                    $tags = get_the_tag_list( '', ', ' );

                    if ( $tags ):
                ?>
                <div class="blog-tags">
                    <label><?php _e( 'Tags', 'apollo' ) ?>:</label>
                    <?php echo $tags ?>
                </div>
                <?php endif; ?>

            </li>
        <?php endwhile; ?>
        <?php else: ?>
            <div><?php _e( "Don't have any post !", 'apollo' ); ?></div>
        <?php endif; ?>
    </nav>
</div>
<?php
    $next_link = get_next_posts_link();
    $prev_link = get_previous_posts_link();

    if ( $next_link || $prev_link ):
?>
<div class="blog-bkl">
    <div class="blk-paging">
        <span class="pg-tt"><?php _e( 'Pages', 'apollo' ) ?>:
        <?php apollo_blog_paging_nav() ?>
        </span>
    </div>
</div>

<?php endif; ?>
