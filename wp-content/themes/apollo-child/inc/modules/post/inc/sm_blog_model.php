<?php

/**
 * Created by PhpStorm.
 * User: truong
 * Date: 23/09/2016
 * Time: 16:17
 */
class SM_Blog_Model extends SM_Mod_Model {
	private $post_id;
	private $summary;
	private $src;
	private $title;
	private $content;
    private $backgroundImage;

    /**
     * @return mixed
     */
    public function getBackgroundImage()
    {
        return $this->backgroundImage;
    }

    /**
     * @param mixed $backgroundImage
     */
    public function setBackgroundImage($backgroundImage)
    {
        $this->backgroundImage = $backgroundImage;
    }



	public function __construct() {
	    parent::__construct();
		$this->getDataBlog();
	}

	protected function getDataBlog(){
		global $post;
		$blog = get_apl_blog($post);
        $this->setPostId(get_the_ID());
        $bgImage  = get_post_meta( $this->post_id, 'post_background-image_thumbnail_id', true );
        $attachmentBgImage    = wp_get_attachment_image_src( $bgImage, 'full' );
        $bgImageUrl = $attachmentBgImage && isset( $attachmentBgImage[0] ) ? $attachmentBgImage[0] : "";
		$summary = $blog->get_summary_blog();
		$summary = !empty($summary) ? $summary['text'] : '';
		$this->setPostId(get_the_ID());
		$this->setSummary($summary);
		$this->setSrc($blog->get_article_image_url());
		$this->setTitle($blog->get_full_title());
		$this->setContent($blog->get_full_content());
        $this->setBackgroundImage($bgImageUrl);


	}

	/**
	 * @return mixed
	 */
	public function getContent() {
		return $this->content;
	}

	/**
	 * @param mixed $content
	 */
	public function setContent( $content ) {
		$this->content = $content;
	}

	/**
	 * @return mixed
	 */
	public function getPostId() {
		return $this->post_id;
	}

	/**
	 * @param mixed $post_id
	 */
	public function setPostId( $post_id ) {
		$this->post_id = $post_id;
	}

	/**
	 * @return mixed
	 */
	public function getSummary() {
		return $this->summary;
	}

	/**
	 * @param mixed $summary
	 */
	public function setSummary( $summary ) {
		$this->summary = $summary;
	}

	/**
	 * @return mixed
	 */
	public function getSrc() {
		return $this->src;
	}

	/**
	 * @param mixed $src
	 */
	public function setSrc( $src ) {
		$this->src = $src;
	}

	/**
	 * @return mixed
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @param mixed $title
	 */
	public function setTitle( $title ) {
		$this->title = $title;
	}

	public function renderSimpleTemplate(){
        $file =  SONOMA_MODULES_DIR. '/post/templates/partials/simple-template.php';
        $data = $this->smGetTemplatePartCustom($file, $this ,true);
        return $data;
    }

    public function renderDefaultTemplate(){
        $file =  SONOMA_MODULES_DIR. '/post/templates/partials/default-template.php';
        $data = $this->smGetTemplatePartCustom($file, $this ,true);
        return $data;
    }
    public function renderDefaultHeaderTemplate(){
        $file =  SONOMA_MODULES_DIR. '/post/templates/partials/header.php';
        $data = $this->smGetTemplatePartCustom($file, $this ,true);
        return $data;
    }
	public function renderListingPage(){
        if ( have_posts() ): the_post();
            $content = get_the_content();

            /**
             * Have the blog shortcode then render default template
             * Do that to avoid some page before updating not have shortcode
             */

            if (strpos($content, '[apollo_blog') !== false) {
                echo Apollo_App::convertContentEditorToHtml($content);
            } else {
                $file = '';
                if( is_author()){
                    $file =  SONOMA_MODULES_DIR. '/post/templates/partials/author.php';
                } else {
                    $file =  SONOMA_MODULES_DIR. '/post/templates/partials/simple-template.php';
                }
                $data = $this->smGetTemplatePartCustom($file, $this ,true);
                return $data;
            }
        endif;
    }

    public function getArgsForQueryPosts(){

        $postsOrder = of_get_option(Apollo_DB_Schema::_BLOG_CATEGORY_PAGE_ORDER,'title-asc');
        switch($postsOrder){
            case ('title-asc'):
                $orderBy = 'post_title';
                $order = 'ASC';
                break;
            case ('title-desc'):
                $orderBy = 'post_title';
                $order = 'DESC';
                break;
            case ('date-asc'):
                $orderBy = 'post_date';
                $order = 'ASC';
                break;
            case ('date-desc'):
                $orderBy = 'post_date';
                $order = 'DESC';
                break;
            case ('order-asc'):
                $orderBy = 'menu_order';
                $order = 'ASC';
                break;
            case ('order-desc'):
                $orderBy = 'menu_order';
                $order = 'DESC';
                break;
            default:
                $orderBy = 'post_title';
                $order = 'ASC';
                break;
        }

        $args = array(
            'post_type' => array('post'),
            'post_status' => 'publish',
            'showposts' => get_option('posts_per_page'),
            'paged' => get_query_var('paged'),

        );

        $args['orderby'] = $orderBy;
        $args['order']    = $order;

        if ($catID = get_query_var( 'cat' )) {
            $args['category__in'] = $catID;
        }

        if( $author = get_query_var('author')){
            $args['author'] = $author;
        }

        $tag =  get_query_var('tag');
        if($tag != ''){
            $args['tag_slug__in'] = array($tag);
        }

       return $args;
    }

    public function getSharingInfo( $additionalFields = array(), $id = 0 )
    {
        $arrShareInfo = $this->getShareInfo();
        $this->social_btns_post( array( 'info' => $arrShareInfo, 'id' => $id ,
            'data_btns' =>  $additionalFields  ) );
    }

    public  function social_btns_post( $share_datas  ) {
        do_action( 'apollo_social_factory' );
        ?>

        <li class="bg-fb"><a href="<?php echo SocialFactory::getFacebookSharerLink( $share_datas['info'] ) ?>" target="_blank"
                             data-ride="ap-logclick"
                             data-action="apollo_log_share_activity"
                             data-activity="<?php echo Apollo_Activity_System::SHARE_FB ?>"
                             data-item_id="<?php echo $share_datas['id'] ?>"><i class="uk-icon-justify uk-icon-facebook"></i></a></li>

        <li class="bg-twt"><a href="<?php echo SocialFactory::getTwitterSharerLink( $share_datas['info'] ) ?>" target="_blank"
                              data-ride="ap-logclick"
                              data-action="apollo_log_share_activity"
                              data-activity="<?php echo Apollo_Activity_System::TWEET ?>"
                              data-item_id="<?php echo $share_datas['id'] ?>"
            ><i class="uk-icon-justify uk-icon-twitter"></i></a></li>

        <?php if ( isset( $share_datas['data_btns'] ) && in_array( 'print' , $share_datas['data_btns'] ) ):

            if ( isset( $share_datas['prog_id'] ) ) {
                $prog_url = $share_datas['prog_id'] ? 'program/'. $share_datas['prog_id'] : 'program/0';
            } else {
                $prog_url = '';
            }

            ?>
            <li class="bg-print">
                <a  title="<?php _e('Print') ?>" target="_blank" href="<?php echo home_url() ?>/print/<?php echo get_post_type() ?>/<?php echo $share_datas['id']. '/'. $prog_url ?>">
                    <i class="uk-icon-justify uk-icon-print"></i>
                </a>
            </li>
        <?php endif; ?>

        <?php if ( isset( $share_datas['data_btns'] ) && in_array( 'event-print' , $share_datas['data_btns'] ) ):

            if ( isset( $share_datas['event_print_url'] ) ) {
                $event_ptr_url = $share_datas['event_print_url'];
            } else {
                $event_ptr_url = '';
            }
            ?>
            <li class="bg-print">
                <a  title="<?php _e('Print') ?>" target="_blank" href="<?php echo $event_ptr_url; ?>">
                    <i class="uk-icon-justify uk-icon-print"></i>
                </a>
            </li>
        <?php endif; ?>

        <li class="bg-mail"><?php if ( isset( $share_datas['data_btns'] ) &&  in_array( 'sendmail' , $share_datas['data_btns'] ) ): ?>
            <a data-ride="ap-tellfriendbylink"
                data-action_after="open:popup"
                data-single="true"
                data-url="<?php echo admin_url('admin-ajax.php?action=apollo_set_bookmark_for_myfriend') ?>"
                data-data_source_selector="[name^='eventid']"
                data-data="ids:value"
                title="<?php _e('Email') ?>"
            ><i class="uk-icon-justify uk-icon-envelope-o"></i></a> </li>
            <?php
        endif;

    }

    /**
     * Get custom label which set into ThemeOption Classified tab
     * @return string
     */
    public function getCustomLabel(){
        return Apollo_App::getCustomLabelByModuleName(Apollo_DB_Schema::_BLOG_POST_PT);
    }

    /**
     * Get custom url by custom slug which set into ThemeOption Classified tab
     * @return string
     */
    public function getCustomUrlBySlug(){
        return Apollo_App::getCustomUrlByModuleName(Apollo_DB_Schema::_BLOG_POST_PT);
    }

}