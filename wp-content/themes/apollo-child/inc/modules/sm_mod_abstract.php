<?php

/**
 *
 * Class SM_Mod_Abstract
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}


abstract class SM_Mod_Abstract
{
    public function __construct($type){

    }

    public function renderTemplate($type,$data ){

        $file = SONOMA_MODULES_DIR.'/'.$type.'/templates/'.$type.'.php';
        $this->smGetTemplatePartCustom($file,$data);
    }

	public  function smGetTemplatePartCustom( $file, $model, $return = false ) {

		$fileBasename = basename($file);

		if ( !file_exists($file) && file_exists( get_stylesheet_directory() . '/' . $fileBasename . '.php' ) ) {
			$file = get_stylesheet_directory() . '/' . $fileBasename . '.php';
		}elseif ( !file_exists($file) && file_exists( get_template_directory() . '/' . $fileBasename . '.php' ) ){
			$file = get_template_directory() . '/' . $fileBasename . '.php';
		}
		ob_start();
		require( $file );
		$data = ob_get_clean();

        if ( $return ){
            return $data;
        }
		echo $data;
	}
}