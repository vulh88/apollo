<?php

/**
 * Created by PhpStorm.
 * User: truong
 * Date: 23/09/2016
 * Time: 16:51
 */
class SM_Public_Art_Model extends SM_Mod_Model
{

    private $public_art;
    private $id;

    /**
     * @return mixed
     */
    public function getPublicArt()
    {
        return $this->public_art;
    }

    /**
     * @param mixed $public_art
     */
    public function setPublicArt($public_art)
    {
        $this->public_art = $public_art;
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function __construct()
    {
        parent::__construct();
        $this->setDataPublicArt();
    }

    public function setDataPublicArt(){
        $this->pagesize = Apollo_App::aplGetPageSize( SM_Common_Const::_SM_ITEMS_NUM_LISTING_PAGE, Apollo_DB_Schema::_PUBLIC_ART_NUM_ITEMS_LISTING_PAGE );
        global $post;
        $this->setPublicArt( get_public_art($post) );
        $this->setId( $post->ID );
    }

    public function isAllowComment(){
        return of_get_option(Apollo_DB_Schema::_ENABLE_COMMENT,1) && comments_open();
    }

    public function getDataPublicArt(){
        $data = $this->getPublicArt()->getDataDetail();
        $data['str_address_all'] = $this->getPublicArt()->get_full_address();
        return $data;
    }

    public function getListArtist(){
        $data = $this->getPublicArt()->getListArtists();
        return $data;
    }

    public function renderPublicArtType()
    {
        $publicArtType = $this->getPublicArt()->generate_categories();
        if ($publicArtType) {
            echo '<div class="org-type">' .__('Category: ','apollo').''. $publicArtType . ' </div>';
        }
    }

    public function renderPublicArtCollections()
    {
        $publicArtCollections = $this->getPublicArt()->generate_public_art_collections();
        if ($publicArtCollections) {
            echo '<div class="org-type">' .__('Collection: ','apollo').''. $publicArtCollections . ' </div>';
        }
    }
    public function renderPublicArtLocations()
    {
        $publicArtLocations = $this->getPublicArt()->generate_public_art_locations();
        if ($publicArtLocations) {
            echo '<div class="org-type">' .__('Location: ','apollo').''. $publicArtLocations . ' </div>';
        }
    }

    public function renderPublicArtMediums()
    {

        $medium_type = $this->getPublicArt()->generate_public_art_mediums();
        if (isset($medium_type) && $medium_type) {

            echo '<p class="org-type medium">' . __('Medium type: ', 'apollo') . '' . $medium_type . '</p>';
        }

        if (isset($this->getDataPublicArt()['date_created']) && $this->getDataPublicArt()['date_created']) {
            echo '<p class="org-type medium">' . __('Date created:: ', 'apollo') . '' . $this->getDataPublicArt()['date_created'] . '</p>';
        }

        if (isset($this->getDataPublicArt()['dimension']) && $this->getDataPublicArt()['dimension']) {
            echo '<p class="org-type medium">' . __('Medium type: ', 'apollo') . '' . $this->getDataPublicArt()['dimension'] . '</p>';

        }
    }

    public function renderContentGeneral(){
        $file = SONOMA_MODULES_DIR. '/public-art/templates/partials/general.php';
        $data = $this->smGetTemplatePartCustom($file,$this ,true);
        return $data;
    }
    public function renderSocial(){
        $file = SONOMA_MODULES_DIR. '/public-art/templates/partials/social.php';
        $data = $this->smGetTemplatePartCustom($file,$this ,true);
        return $data;
    }
    public function renderListArtist(){

        $file = SONOMA_MODULES_DIR. '/public-art/templates/partials/list-artist.php';
        $data = $this->smGetTemplatePartCustom($file, $this ,true);
        return $data;
    }

    public function renderMap()
    {
        $file = SONOMA_MODULES_DIR. '/public-art/templates/partials/map.php';
        $data = $this->smGetTemplatePartCustom($file,$this ,true);
        return $data;
    }

    public function renderAdditionalFields(){

        $file = SONOMA_MODULES_DIR. '/public-art/templates/partials/add-fields.php';
        $data = $this->smGetTemplatePartCustom($file, $this ,true);
        return $data;
    }
    public function renderMedia(){

        $file = SONOMA_MODULES_DIR. '/public-art/templates/partials/media.php';
        $data = $this->smGetTemplatePartCustom($file, $this ,true);
        return $data;
    }
    public function renderSlider()
    {
        $gallerys  = $this->getPublicArt()->getListImages();
        if(!empty($gallerys[0])){
            $data = SM_View_Helper::handleDataPhotoItems($gallerys);
            return Apollo_App::getSimpleTemplatePart(SONOMA_MODULES_DIR . '/common-templates/slider.php', $data, true);
        }
    }

    public function renderVideo()
    {
        $videos = $this->getPublicArt()->getListVideo();
        $videoTemp = array();
        foreach($videos as $video){
            $videoTemp[] = array(
                'embed' => $video['embed'],
                'desc' => isset($video['desc']) ? base64_decode($video['desc']) : '',
            );
        }
        $videos = $videoTemp;

        if (!empty($videos)){
            $data = SM_View_Helper::handleDataVideos($videos);
            return Apollo_App::getSimpleTemplatePart(SONOMA_MODULES_DIR . '/common-templates/video.php', $data, true);

        }
    }

    public function getListAudio(){
        $audioList =  $this->getPublicArt()->getListAudios(Apollo_DB_Schema::_APL_ORG_AUDIO);
        return $audioList;
    }

    public function renderListAudios(){
        $audioList =  $this->getListAudio();
        if ( !empty($audioList)){
            $file =  SONOMA_MODULES_DIR. '/common-templates/audio-list.php';
            $data = $this->smGetTemplatePartCustom($file, $this ,true);
            return $data;
        }
        return '';

    }

    public function renderAudioItem(){
        $result = array();
        $audioList =  $this->getListAudio();
        $file =  SONOMA_MODULES_DIR. '/organization/templates/partials/audio-item.php';
        if ( !empty($audioList)) {
            foreach($audioList as $item):
                $result[] = $this->smGetTemplatePartCustom($file,$item,true);

            endforeach;
        }
        if (!empty($result)) {
            $result = implode(" ", $result);
        }
        return $result;
    }

    public function render_html()
    {
        $data = $this->smGetTemplatePartCustom($this->template,$this,true);
        return $data;
    }


    public function search()
    {
        $arr_params = array(
            'post_type'      => Apollo_DB_Schema::_PUBLIC_ART_PT,
            'posts_per_page' => $this->pagesize,
            'paged'          => $this->page,
            'post_status'    => array('publish'),
        );

        if ( $keyword = self::getKeyword() ) {
            $arr_params['s'] =  $keyword;
        }

        $termType = get_query_var('taxonomy');
        if ( isset($_GET['term']) && !empty($_GET['term']) ) {
            $arr_tax_query[] = array(
                'taxonomy'=> !empty($termType) ? $termType : Apollo_DB_Schema::_PUBLIC_ART_PT.'-type',
                'terms'   => array($_GET['term']),
            );
            $arr_params['tax_query'] = $arr_tax_query;
        }

        if ( isset($_GET['collection']) && !empty($_GET['collection']) ) {
            $arr_tax_query[] = array(
                'taxonomy'=> Apollo_DB_Schema::_PUBLIC_ART_COL,
                'terms'   => array($_GET['collection']),
            );
            $arr_params['tax_query'] = $arr_tax_query;
        }

        if ( isset($_GET['location']) && !empty($_GET['location']) ) {
            $arr_tax_query[] = array(
                'taxonomy'=> Apollo_DB_Schema::_PUBLIC_ART_LOC,
                'terms'   => array($_GET['location']),
            );
            $arr_params['tax_query'] = $arr_tax_query;
        }

        if ( isset($_GET['medium']) && !empty($_GET['medium']) ) {
            $arr_tax_query[] = array(
                'taxonomy'=> Apollo_DB_Schema::_PUBLIC_ART_MED,
                'terms'   => array($_GET['medium']),
            );
            $arr_params['tax_query'] = $arr_tax_query;
        }

        // set new offset if this is ajax action
        $this->addOffsetToParams($arr_params);

        // no need search in this time
        add_filter('posts_where'  , array(__CLASS__, 'filter_where_tbl'), 10, 1);
        add_filter('posts_join'   , array(__CLASS__, 'filter_join_tbl' ), 10, 1);
        add_filter('posts_orderby', array(__CLASS__, 'filter_order_tbl'), 10, 1);
        add_filter('posts_search' , array(__CLASS__, 'posts_search'    ), 10, 1);
        add_filter('posts_groupby', array($this    , 'filter_groupby'  ), 10, 1);

        $this->result = query_posts($arr_params);
        Apollo_Next_Prev::updateSearchResult( $GLOBALS['wp_query']->request, Apollo_DB_Schema::_PUBLIC_ART_PT );

        remove_filter('posts_orderby', array(__CLASS__, 'filter_order_tbl'), 10);
        remove_filter('posts_join'   , array(__CLASS__, 'filter_join_tbl' ), 10);
        remove_filter('posts_where'  , array(__CLASS__, 'filter_where_tbl'), 10);
        remove_filter('posts_search' , array(__CLASS__, 'posts_search'    ), 10, 1);
        remove_filter('posts_groupby', array($this    , 'filter_groupby'  ), 10, 1);

        $this->total_pages = ceil( $this->total = $GLOBALS['wp_query']->found_posts / $this->pagesize );
        $this->total       = $this->total = $GLOBALS['wp_query']->found_posts;
        $this->template    = SONOMA_MODULES_DIR . '/public-art/templates/'. $this->_get_template_name(of_get_option(Apollo_DB_Schema::_PUBLIC_ART_DEFAULT_VIEW_TYPE,SONOMA_DIRECTORY_LISTING_DEFAULT_VIEW_TYPE));

        $this->resetPostData();
    }

    public static function filter_where_tbl ($where)
    {
        global $wpdb;

        // JUST ALPHA
        if ( isset($_GET['_alpha']) && !empty($_GET['_alpha']) ) {
            $_alpha = $_GET['_alpha'];
            $wpdb->escape_by_ref($_alpha);
            return $where . " AND CAST({$wpdb->posts}.post_title AS CHAR) LIKE '" . $_alpha . "%'";
        }

        // NAME
        if ( isset($_GET['_name']) && !empty($_GET['_name']) ) {
            $_name = $_GET['_name'];
            $wpdb->escape_by_ref($_name);
            return $where . " AND CAST({$wpdb->posts}.post_title AS CHAR) LIKE '" . $_name . "%'";
        }

        // OTHER CASE
        if ( isset($_GET['city']) && !empty($_GET['city']) ) {
            $city = $_GET['city'];
            $wpdb->escape_by_ref($city);
            $where .= " AND CAST(mt_city.meta_value AS CHAR) LIKE '%" . $city . "%' ";
        }

        if ( isset($_GET['zip']) && !empty($_GET['zip']) ) {
            $zip = $_GET['zip'];
            $wpdb->escape_by_ref($zip);
            $where .= " AND CAST(mt_zip.meta_value AS CHAR) LIKE '" . $zip . "%' ";
        }

        /* Artist */
        if ( isset($_GET['_artist']) && !empty($_GET['_artist']) ) {
            $_artist = sprintf('%s%s%s', 's', $_GET['_artist'], 'e' );
            $wpdb->escape_by_ref($_artist);
            $where .= " AND CAST(mt_artist.meta_value AS CHAR) LIKE '%" . $_artist . "%' ";
        }

        $where .= " AND {$wpdb->posts}.post_type = '" . Apollo_DB_Schema::_PUBLIC_ART_PT . "' ";
        return $where;
    }

    public static function filter_join_tbl ($join)
    {
        global $wpdb;
        $mtTbl = $wpdb->{Apollo_Tables::_APL_PUBLIC_ART_META};

        if ( isset($_GET['city']) && !empty($_GET['city']) ) {
            $join .= " INNER JOIN {$mtTbl} mt_city ON "
                  .  "(mt_city.meta_key = '" . Apollo_DB_Schema::_PUBLIC_ART_CITY ."' AND {$wpdb->posts}.ID = mt_city.apollo_public_art_id)";
        }

        if ( isset($_GET['zip']) && !empty($_GET['zip']) ) {
            $join .= " INNER JOIN {$mtTbl} mt_zip ON "
                  .  "(mt_zip.meta_key = '" . Apollo_DB_Schema::_PUBLIC_ART_ZIP . "' AND {$wpdb->posts}.ID = mt_zip.apollo_public_art_id)";
        }

        if ( isset($_GET['_artist']) && !empty($_GET['_artist']) ) {
            $join .= " INNER JOIN {$mtTbl} mt_artist ON "
                  .  "(mt_artist.meta_key = '" . Apollo_DB_Schema::_APL_PUBLIC_ART_ARTIST_SEARCHING . "' AND {$wpdb->posts}.ID = mt_artist.apollo_public_art_id)";
        }

        $join .= self::getJoinTaxByKeyword();
        return $join;
    }

    /* last name is not query with city in same query: Good to know that. because we don't need join an extra query to sort by last_name  */
    public static function filter_order_tbl ($order)
    {
        global $wpdb;
        $order = $wpdb->posts . '.post_title ASC';
        return $order;
    }
}
