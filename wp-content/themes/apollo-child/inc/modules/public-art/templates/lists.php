<?php

if ( $results = $this->get_results() ) {
    foreach ( $results as $p ): 
        $e       = get_org( $p );
        $summary = $e->get_summary(SM_Common_Const::_SM_LISTING_DESC_MAX_TRIM_WORDS);
    ?>
        <div class="uk-width-1-1 article-item item-orange" data-url="<?php echo $e->get_permalink() ?>" data-type="link">
            <div class="article-title uk-grid">
                <div class="content-title uk-width-large-2-10 uk-width-medium-3-10">
                    <a href="<?php echo $e->get_permalink() ?>">
                        <?php echo $e->get_image( SM_CIRCLE_IMAGE_SIZE, array('class' => 'teaser-bg'), array( 'aw' => true, 'ah' => true ), 'normal' ) ?>
                    </a>
                </div>

                <div class="content-teaser uk-width-large-8-10 uk-width-medium-7-10">
                    <p>
                        <a href="<?php echo $e->get_permalink() ?>">
                            <span class="ev-tt" data-ride="" data-n="2">
                                <?php echo $e->get_title() ?>
                            </span>
                        </a>                            
                    </p>
                    <div class="category-link">
                        <?php
                            $category = $e->generate_categories();
                            if ($category) : ?>
                                <?php echo $category?>
                            <?php endif;?>
                    </div>
                    <div class="description">
                        <?php echo $summary['text'] ?><?php echo $summary['have_more'] ? '...' : '' ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach;
} else if ( !isset($_GET['s']) && !isset($_GET['keyword']) ) {
    _e( 'No results', 'apollo' );
}
