<?php
$listArtists = $model->getListArtist();
if (!empty($listArtists)):
    ?>
    <div class="blog-bkl">
        <div class="a-block">
            <h4><?php _e('Associated artists', 'apollo'); ?></h4>
            <div class="el-blk">
                <?php
                $_html= '';
                foreach ( $model->getListArtist() as $art )
                {
                    $artist = get_artist($art);
                    $_html .= '<div class="org-type artist">';
                    if ($artist->is_linked()) {
                        $_html.='<a class="link" href="'.$artist->get_permalink(). '"><div class="artist-pic">';
                        $_html.=  $artist->get_image( 'thumbnail');
                        $_html.='</div><span>'.$artist->post->post_title.'</span></a>';
                    } else {
                        $_html.=  '<div class="artist-pic">'.$artist->get_image( 'thumbnail' ).'</div><span>'.$artist->post->post_title.'</span>';
                    }
                    $_html.='</div>';
                }
                echo $_html;
                ?>
            </div>
        </div>
    </div>
<?php endif; ?>
