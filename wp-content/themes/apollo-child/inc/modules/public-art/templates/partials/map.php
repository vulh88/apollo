<?php if ( $model->getDataPublicArt()['latitude'] && $model->getDataPublicArt()['longitude'] ) {
?>

    <div class="blog-bkl">
        <div class="a-block">
            <h4><?php _e('Location','apollo');?><span><i class="fa fa-map-marker"></i></span></h4>
        </div>
        <div class="a-block-ct locatn">
            <div class="loc-address">
            
                <?php if(isset($model->getDataPublicArt()['address']) && $model->getDataPublicArt()['address']){
                    ?>
                    <p><?php echo $model->getDataPublicArt()['address'] ?></p>
                        <p><?php echo $model->getDataPublicArt()['latitude'].', '.$model->getDataPublicArt()['longitude']?></p>
                <?php }
                ?>
            </div>
            <div class="lo-left public-art">
                <div id="art_detail_map_canvas" style="min-height: 400px"
                     data-lat = "<?php echo $model->getDataPublicArt()['latitude'] ?>"
                     data-long = "<?php echo $model->getDataPublicArt()['longitude'] ?>"
                     data-address="<?php echo $model->getDataPublicArt()['str_address_all'] ? $model->getDataPublicArt()['str_address_all'] : ''; ?>"
                     data-relation_id="_popup_google_map_full"
                     data-nonce="<?php echo wp_create_nonce('save-coor') ?>"
                     data-img_location="<?php echo get_stylesheet_directory_uri() ?>/assets/images/icon-location.png">
                </div>
                <p class="t-r"> <a href="javascript:void(0);" data-target="#_popup_google_map" id="_event_trigger_fullmap"><?php _e('Full map and directions','apollo');?></a></p>
            </div>
            <div class="pba-display-nearby">
                <label for="pba_display_nearby"><?php echo __('Display other nearby public art','apollo')?></label>
                <input type="checkbox" name="display-nearby" value="" id="pba_display_nearby" />
            </div>
        </div>
    </div>
<?php } ?>
