
<div class="art-social">
    <div class="el-blk">
        <?php
        if(isset($model->getDataPublicArt()['email']) && !empty($model->getDataPublicArt()['email'])){
        ?>
        <div class="art-social-item"><i class="fa fa-envelope fa-lg">&nbsp;&nbsp;&nbsp;</i><a href="mailto:<?php echo $model->getDataPublicArt()['email'] ?>">
                <?php  _e('Email','apollo') ?></a>
            <div class="slash">/</div>
        </div>
        <?php } ?>

        <?php
        if(isset($model->getDataPublicArt()['web']) && !empty($model->getDataPublicArt()['web'])){
        ?>
        <div class="art-social-item"><i class="fa fa-link fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $model->getDataPublicArt()['web'] ?>">
                <?php _e('Website','apollo') ?></a>
            <div class="slash">/</div>
        </div>
        <?php } ?>


    </div>
    <div class="el-blk location">
        <?php
            if(isset($model->getDataPublicArt()['address']) && !empty($model->getDataPublicArt()['address'])){
        ?>
        <p><span><i class="fa fa-map-marker fa-lg">&nbsp;&nbsp;&nbsp;</i></span>
            <label><?php echo $model->getDataPublicArt()['address']; ?></label>
        </p>
        <?php } ?>
        <?php
        if(isset($model->getDataPublicArt()['phone']) && !empty($model->getDataPublicArt()['phone'])){
        ?>
        <p><span><i class="fa fa-phone fa-lg">&nbsp;&nbsp;</i></span>
            <label><?php echo $model->getDataPublicArt()['phone']; ?></label>
        </p>
        <?php } ?>

    </div>
    <div class="el-blk">
        <div class="b-btn fl">
            <?php echo $model->renderBookmarkBtn( array( 'class' => 'btn btn-b' ), get_post($model->getId() ) ); ?>
        </div>
    </div>
</div>