<div class="blog-bkl organization clearfix">
    <img src="<?php echo SONOMA_FRONTEND_ASSETS_URI;?>/images/demo/creative-sonoma-logo.png" class="org-logo-smn" alt="Creative Sonoma" title="Creative Sonoma" />
    <h1 class="p-ttl"><?php echo $model->getPublicArt()->get_title() ?></h1>

    <?php $model->getPublicArt()->renderIcons($this->getDataPublicArt()['icon_fields'], $this->getDataPublicArt()['icons']); ?>

    <?php  $model->renderPublicArtType();?>
    <?php  $model->renderPublicArtCollections();?>
    <?php  $model->renderPublicArtLocations();?>

    <div class="rating-box rating-action rating-art">
        <?php if( of_get_option(Apollo_DB_Schema::_ENABLE_THUMBS_UP,1)) : ?>
        <div class="like" id="_event_rating_<?php echo $model->getId() . '_' . Apollo_DB_Schema::_PUBLIC_ART_PT ?>" data-ride="ap-event_rating" data-i="<?php echo $model->getId() ?>" data-t="<?php echo Apollo_DB_Schema::_PUBLIC_ART_PT ?>"> <a href="javascript:void(0);"><span class="_count">&nbsp;</span></a></div>
        <?php endif ?>
        <?php if ( $model->isAllowComment() ): ?>
            <div class="cm-comment"> <a href="#event_comment_block" data-uk-smooth-scroll="{offset: 121}"><?php _e( 'Comment', 'apollo' ) ?></a></div>
        <?php endif; ?>
    </div>
    <div class="el-blk clearfix">
        <div class="art-pic">
            <?php echo $model->getPublicArt()->get_image('medium'); ?>
        </div>
        <?php echo $model->renderSocial();?>
    </div>
    <div class="el-blk clearfix">
        <div class="art-desc apl-internal-content rich-txt rich-txt--hight-first-child">
            <?php
            echo $model->getPublicArt()->get_full_content();
            ?>
        </div>
    </div>
    <div class="el-blk">
        <?php  $model->renderPublicArtMediums(); ?>
    </div>
</div>
