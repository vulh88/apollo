<div class="content-wrapper content-wrapper--master-layout">
    <div class="block block-details-content block-details--public-art">

        <div class="breadcrumbs">
            <ul class="nav">
                <ul class="nav">
                    <li><a href="<?php echo home_url() ?>"><?php _e( 'Home', 'apollo' ) ?></a></li>
                    <li><a href="<?php echo home_url('public-art') ?>"><?php _e( 'Public art ', 'apollo' ) ?></a></li>
                    <li> <span><?php echo $model->getPublicArt()->get_title() ?></span></li>
                </ul>
            </ul>
            <div class="search-bkl social-share">
                <div class="b-share-cat art">
                    <?php $model->getSharingInfo(['print', 'sendmail'], $model->getId()); ?>
                    <input name="eventid" value="<?php echo $model->getId(); ?>" type="hidden" />
                </div>
            </div>
        </div>

        <div class="detail-conten-body">
            <input name="eventid" value="<?php echo $model->getId() ?>" type="hidden">
            <?php
                Apollo_Next_Prev::getNavLink( $model->getId(), Apollo_DB_Schema::_PUBLIC_ART_PT );
                echo $model->renderContentGeneral();
                echo $model->renderMap();
                echo $model->renderListArtist();
                echo $model->renderAdditionalFields();
                echo $model->renderMedia();
            ?>

            <?php
                // If comments are open or we have at least one comment, load up the comment template.
                if ( $model->isAllowComment() ):?>
                <div class="blog-bkl astro-featr">
                    <div class="a-block" id="event_comment_block">
                        <h4><?php _e('COMMENTS', 'apollo') ?><span> <i class="fa fa-comment"></i></span></h4>    
                    </div><!-- #comments -->
                    <div class="a-block-ct">
                        <?php comments_template('/partials/comments.php'); ?>
                    </div>
                </div>
            <?php endif;?>
        </div> 
    </div>
</div>

<?php
if ( $model->getDataPublicArt()['latitude'] && $model->getDataPublicArt()['longitude'] ) {
    require_once APOLLO_TEMPLATES_DIR.'/events/google-map-popup.php';
}
?>