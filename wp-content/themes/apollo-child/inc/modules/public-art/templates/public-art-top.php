<?php 
    
    $searchData = array(
        'keyword' => isset($_GET['keyword'])?Apollo_App::clean_data_request($_GET['keyword']):'',
        'term' => isset($_GET['term'])?Apollo_App::clean_data_request($_GET['term']):'',
        'location' => isset($_GET['location'])?Apollo_App::clean_data_request($_GET['location']):'',
        'collection' => isset($_GET['collection'])?Apollo_App::clean_data_request($_GET['collection']):'',
        '_name' => isset($_GET['_name'])?Apollo_App::clean_data_request($_GET['_name']):'',
        'medium' => isset($_GET['medium'])?Apollo_App::clean_data_request($_GET['medium']):'',
        'zip'  => isset($_GET['zip'])?Apollo_App::clean_data_request($_GET['zip']):'',
        'city'  => isset($_GET['zip'])?Apollo_App::clean_data_request($_GET['city']):of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY,''),
        '_artist'  => isset($_GET['_artist'])?Apollo_App::clean_data_request($_GET['_artist']):'',
        '_alpha'  => isset($_GET['_alpha'])?Apollo_App::clean_data_request($_GET['_alpha']):'',
    );
    $searchData['city'] = urldecode($searchData['city']);
    $pub_art = new Apollo_Public_Art( false, Apollo_DB_Schema::_PUBLIC_ART_PT);
    $artists = $pub_art->getArtistPublicArt();

    $cityList = Apollo_App::getCityByTerritory(false, '', true);
    $zipList = Apollo_App::getZipByTerritory(false, false, $searchData['city'], true);
    $listCityState = Apollo_App::getCityStateByTerritory();
    $isTopSearchMobileVersion = isset($_GET['top-search-mobile']) && !empty($_GET['top-search-mobile']) ? 'ts-mobile' : '';

    $defaultPublicArtTypeValue = $model->getDefaultValueForTerm( Apollo_DB_Schema::_PUBLIC_ART_PT.'-type', $searchData['term']);
    $defaultPublicArtColValue = $model->getDefaultValueForTerm(Apollo_DB_Schema::_PUBLIC_ART_COL, $searchData['collection']);
    $defaultPublicArtLocValue = $model->getDefaultValueForTerm(Apollo_DB_Schema::_PUBLIC_ART_LOC, $searchData['location']);
    $defaultPublicArtMedValue = $model->getDefaultValueForTerm(Apollo_DB_Schema::_PUBLIC_ART_MED, $searchData['medium']);

?>
<div class="top-search venue-s-t-m <?php echo $isTopSearchMobileVersion; ?>">
    <div class="inner">
        <div class="top-search-row">
            <form method="get" id="search-public-art-m-t" action="<?php echo home_url(Apollo_DB_Schema::_PUBLIC_ART_PT) ?>" class="form-event">
                <div class="search-row">
                    <div class="el-blk search-input-wrapper">
                        <button type="button" class="btn btn-l s"><i class="fa fa-search fa-flip-horizontal fa-lg"></i></button>
                        <input type="text" name="keyword" value="<?php echo Apollo_App::clean_data_request(get_query_var('keyword'), true) ?>" placeholder="<?php _e('Search by Keyword', 'apollo') ?>" class="inp inp-txt event-search event-search-custom">
                        <input type="hidden" name="do_search" value="<?php echo isset($_GET['do_search']) && !empty($_GET['do_search']) ? 'yes' : 'no'; ?>" />
                    </div>                    

                    <?php
                    $artNames = $ar = Apollo_Public_Art::get_list_artnames();

                    if ( $artNames ):
                        ?>
                        <div class="el-blk">
                            <div class="select-bkl">
                                <select name="_name" class="chosen" id="pa-name-select">
                                    <option value="0"><?php _e('Public Art Name', 'apollo') ?></option>
                                    <?php foreach($artNames as $artName): ?>
                                        <option <?php selected($searchData['_name'], $artName['post_title']) ?> value="<?php echo $artName['post_title'] ?>"><?php echo $artName['post_title'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ($publicArtTypes = Apollo_Public_Art::get_tree_event_style(Apollo_DB_Schema::_PUBLIC_ART_PT, true)): ?>
                    <div class="el-blk">
                        <div class="select-bkl">
                            <select name="term" class="chosen" id="pa-type-select">
                                <option value="0"><?php _e('Public Art Type', 'apollo') ?></option>
                                <?php
                                Apollo_Public_Art::build_option_tree($publicArtTypes, $defaultPublicArtTypeValue);
                                ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>
                    <?php endif; ?>

                    <?php if ($mediums = Apollo_Public_Art::get_tree_medium(true)): ?>
                    <div class="el-blk">
                        <div class="select-bkl">
                            <select name="medium" class="chosen" id="pa-medium-select">
                                <option value="0"><?php _e('Medium', 'apollo') ?></option>
                                <?php
                                Apollo_Org::build_option_tree($mediums, $defaultPublicArtMedValue);
                                ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>
                    <?php endif; ?>
                
                    <?php
                    if ( $artists ):
                        ?>
                        <div class="el-blk">
                            <div class="select-bkl">
                                <select name="_artist" class="chosen" id="pa-artist-select">
                                    <option value="0"><?php _e('Artist', 'apollo') ?></option>
                                    <?php
                                    foreach( $artists as $artist ) {
                                        ?>
                                        <option <?php selected($searchData['_artist'], $artist->ID) ?> value="<?php echo $artist->ID ?>"><?php echo $artist->post_title ?></option>
                                    <?php } ?>
                                </select>
                                <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                            </div>
                        </div>

                        <?php
                    endif; ?>
                </div>
                <div class="search-row">
                    <?php if(!empty($cityList)):
                        ?>
                        <div class="el-blk">
                            <div class="select-bkl">
                                <select name="city" class="chosen apl-territory-city" id="pa-city-select">
                                    <?php foreach($listCityState as $k => $cityItem): ?>
                                        <optgroup <?php echo $k ? 'label="'.$k.'"' : '' ?>>

                                            <?php if (!$k): ?>
                                                <option value="0"><?php echo $cityItem; ?></option>
                                            <?php endif; ?>

                                            <?php if ($cityItem && is_array($cityItem)):
                                                foreach ( $cityItem as $city ):
                                                    $selected = '';
                                                    if( $city ==  $searchData['city'])
                                                        $selected = 'selected';
                                                    ?>
                                                    <option <?php echo $selected ?> value="<?php echo  $city ?>"><?php echo $city ?></option>
                                                <?php endforeach; endif; ?>
                                        </optgroup>
                                    <?php endforeach; ?>
                                </select>
                                <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                            </div>
                        </div>
                    <?php endif; ?>
                
                    <?php
                    if(!empty($zipList)):
                        ?>
                        <div class="el-blk">
                            <div class="select-bkl">
                                <select name="zip" class="chosen apl-territory-zipcode" id="pa-zip-select">
                                    <?php foreach($zipList as $k => $zip ):

                                        ?>
                                        <option value="<?php echo $k ?>" <?php selected($k,  $searchData['zip']) ?>><?php echo $zip ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php
                    if ($locations = Apollo_Public_Art::get_tree_location(true)):
                    ?>
                    <div class="el-blk">
                        <div class="select-bkl">
                            <select name="location" id="pa-location-select">
                                <option value="0"><?php _e('Location', 'apollo') ?></option>
                                <?php
                                Apollo_Org::build_option_tree($locations, $defaultPublicArtLocValue);
                                ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>
                    <?php endif; ?>

                    <?php
                    if ($collection = Apollo_Public_Art::get_tree_collection(true)):
                    ?>
                    <div class="el-blk">
                        <div class="select-bkl">
                            <select name="collection" id="pa-collection-select">
                                <option value="0"><?php _e('Collection', 'apollo') ?></option>
                                <?php
                                Apollo_Org::build_option_tree($collection, $defaultPublicArtColValue);
                                ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="sub-blk">
                        <button type="submit" class="btn btn-l lgr fr f-w-btn-search"><?php _e('SEARCH', 'apollo') ?></button>
                    </div>
                </div>
                <div class="el-blk custom-el-blk"><span class="tt"><?php _e('Search by alpha name:', 'apollo') ?></span>
                    <div class="character-board">
                        <?php
                        for( $i = 97 ; $i <= 122; $i++): ?><a <?php if ( isset( $_REQUEST['_alpha'] ) && strtolower( $_REQUEST['_alpha'] ) == strtolower( chr($i) ) ) echo 'class="active"'; ?> href="<?php echo home_url(Apollo_DB_Schema::_PUBLIC_ART_PT) ?>?_alpha=<?php echo chr($i) ?>&do_search=yes"><?php echo strtoupper(chr($i)) ?></a><?php endfor; ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>