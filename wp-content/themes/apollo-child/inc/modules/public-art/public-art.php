<?php

/**
 * Class SM_Public_Art_Mod
 */
require_once __DIR__.'/inc/sm_public_art_model.php';

class SM_Public_Art_Mod extends SM_Mod_Abstract
{
    private $_isPublicMapRequest = false;
    public function __construct()
    {
        $this->_isPublicMapRequest = get_query_var('_apollo_public_art_map') === 'true';
        $this->renderPageTemplate();
    }

    public function renderPageTemplate()
    {
        $smPublicArtModel = new SM_Public_Art_Model();


        // determine list page or detail page

        if($this->_isPublicMapRequest){
            // search map template
            $file = SONOMA_MODULES_DIR . '/public-art/templates/public-art-search-map.php';
        } elseif ( is_single() && !$this->_isPublicMapRequest) {
            // detail template
            $file = SONOMA_MODULES_DIR . '/public-art/templates/public-art-detail.php';
        } else {
            // list template
            $file = SONOMA_MODULES_DIR . '/public-art/templates/public-art.php';
        }

        // load horizontal search bar
        if( !$this->_isPublicMapRequest){
            $fileTemplatePart = SONOMA_MODULES_DIR . '/public-art/templates/public-art-top.php';
            if ( file_exists($fileTemplatePart) ) {
                $this->smGetTemplatePartCustom( $fileTemplatePart, $smPublicArtModel );
            }
        }



        // load page
        if( file_exists( $file ) ) {
            $this->smGetTemplatePartCustom($file, $smPublicArtModel);
        }
    }
}

new SM_Public_Art_Mod();
