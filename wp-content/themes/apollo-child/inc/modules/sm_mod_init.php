<?php

/**
 *
 * Class SM_Mod_Init
 */


if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

require_once SONOMA_MODULES_DIR.'/sm_mod_abstract.php';
require_once SONOMA_MODULES_DIR.'/sm_mod_model.php';
require_once SONOMA_MODULES_DIR.'/sm_mod_media_setting.php';
require_once SONOMA_INCLUDES_DIR . '/helpers/SM_View_Helper.php';

class SM_Mod_Init
{

    public function __construct($type){

        $isAvailableModule = Apollo_App::is_avaiable_module($type);
        $file = SONOMA_MODULES_DIR . '/' . $type . '/' . $type . '.php';
        if ($type == SM_Common_Const::_SM_SINGLE_CONTENT_POST_TYPE) {
            global $post;
            $isAvailableModule = Apollo_App::is_avaiable_module($post->post_type);
            $file = SONOMA_MODULES_DIR . '/' . $post->post_type . '/' . $post->post_type . '.php';
        }
        if (!file_exists($file) ||  ( is_single() && !$isAvailableModule ) ) {
            _e('Page not available.', 'apollo');
        } else {
            require_once $file;
        }

    }

}
