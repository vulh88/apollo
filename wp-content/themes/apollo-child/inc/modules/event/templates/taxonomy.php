<?php
$eventCatModel = $model;
$smTaxonomyTemplateDir = SONOMA_MODULES_DIR . '/event/templates';
?>
<div class="content-wrapper content-wrapper--master-layout event-category-circle">
    <div class="block block-details-content block-events">
    <?php
    // BREADCRUMBS
    Apollo_App::the_breadcrumb();

    Apollo_App::getSimpleTemplatePart(SONOMA_MODULES_DIR . '/event/templates/event-top.php', true); ?>

    <div class="detail-conten-body">
        <?php
        // PREPARE DATA

        /* Thienld: new instance to handle caching logic for event category page */
        $eSpotLightFromCache = array();
        $eFeaturedFromCache = array();
        $eventCacheData = apply_filters('apl_event_cache_get_cache_data',$eventCatModel->getCurrentTerm()->term_id);
        $isEnabledLocalCache = apply_filters('apl_event_cache_check_local_cache_enable','');
        if($isEnabledLocalCache && !empty($eventCacheData)){
            $eSpotLightFromCache = $eventCacheData[APLC_Inc_Event_Category_Cache::EVENT_SPOTLIGHT_CACHE];
            $eFeaturedFromCache = $eventCacheData[APLC_Inc_Event_Category_Cache::EVENT_FEATURED_CACHE];
        }

        $hidden_spotlight = get_apollo_term_meta( $eventCatModel->getCurrentTerm()->term_id, Apollo_DB_Schema::_APL_EVENT_TAX_HIDE_SPOT, true );

        /*top theme tool desc*/
        $themeToolData = $eventCatModel->themeToolClass->getThemeToolData();
        if ($themeToolData && $themeToolData->top_desc):?>
            <div class="tax-top-content">
                <p>
                    <?php echo Apollo_App::convertContentEditorToHtml($themeToolData->top_desc, true);?>
                </p>
            </div>
        <?php endif;

        // ================== RENDER SPOTLIGHT ==================
        if( of_get_option(Apollo_DB_Schema::_ENABLE_CATEGORY_PAGE_SPOTLIGHT) && !$hidden_spotlight) {

            $availableSpotlightTransientCache = !empty($eSpotLightFromCache);

            if($availableSpotlightTransientCache) {
                $spotlightEventID = (int) $eSpotLightFromCache['event_ids'][0];

                $eventCatModel->set_displayed_event( $spotlightEventID );
            } else {
                $spotlights = $eventCatModel->getSpotLightsEvent();
                $spotlightEvent = $spotlights['datas'];
                $spotlightEventID = isset($spotlights['datas'][0]->ID) ? $spotlights['datas'][0]->ID : '';
                if ( count( $spotlightEvent ) ) {
                    $eventCatModel->set_displayed_event( ( int ) $spotlightEventID );
                }

                if($isEnabledLocalCache){
                    do_action('apl_event_cache_set_data_cache',APLC_Inc_Event_Category_Cache::EVENT_SPOTLIGHT_CACHE, $spotlightEvent);
                }
            }

            $spotlightHtml = $eventCatModel->renderWithData(sprintf('%s/taxonomy/html/%s', $smTaxonomyTemplateDir, of_get_option(Apollo_DB_Schema::_EVENT_CATEGORY_SPOTLIGHT_STYLE, 'default') == 'default'
                ? 'spotlight-event-left.php' : 'spotlight-event.php'), $spotlightEventID);

            echo $spotlightHtml;
        }
        // ================== END RENDER SPOTLIGHT ==================

        //================== RENDER FEATUREEVENTS ==================
        $availableFeatureTransientCache = !empty($eFeaturedFromCache);
        if($availableFeatureTransientCache){
            $featured_events = $eFeaturedFromCache['event_ids'];
            foreach($featured_events as $eID){
                $eventCatModel->set_displayed_event( ( int ) $eID );
            }
        } else {
            $features =  $eventCatModel->getFeaturesEvent();
            $featured_events = $eventCatModel->auto_fill_featured_events();
            if($isEnabledLocalCache){
                do_action('apl_event_cache_set_data_cache',APLC_Inc_Event_Category_Cache::EVENT_FEATURED_CACHE,$featured_events);
            }
        }

        /**
         * Apply file caching
         * @author vulh
         * @Ticket #14877
         */
        $featuredCacheFileClass = aplc_instance('APLC_Inc_Files_EventCategoryFeaturedBlock', $eventCatModel->getCurrentTerm()->term_id);
        $featuredHtml = $availableFeatureTransientCache ? $featuredCacheFileClass->get() : $featuredCacheFileClass->remove();

        if (!$featuredHtml) {
            $featuredHtml = $eventCatModel->renderWithData( $smTaxonomyTemplateDir.'/taxonomy/html/features-event.php', $featured_events );
            $featuredCacheFileClass->save($featuredHtml);
        }
        echo $featuredHtml;

        // ================== END RENDER FEATUREEVENTS ==================
        ?>
        <section class="list-more-category list-more-category-others-event">
            <div></div>
        </section>
        <?php
        $step = Apollo_Display_Config::SINGLE_TAXONOMY_OTHER_EVENT_SHOW_STEP;
        $baseStart = 0;
        ?>
        <div class="load-more b-btn apl-others-event-load-more hidden">
            <a href="javascript:void(0);"
               data-ride="ap-more"
               data-holder=".list-more-category-others-event>:last"
               data-sourcetype="url"
               data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_category_show_more_other_event&start='.$baseStart.'&pagesize='.$step.'&term_id='.$eventCatModel->getCurrentTerm()->term_id.'&displayed_ids=' . implode(',', $eventCatModel->get_displayed_events())) ?>"
               data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
               data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
               data-container=".list-more-category-others-event"
               class="btn-b arw"><?php _e('SHOW MORE', 'apollo') ?>
            </a>
        </div>
        <?php

        /*Render bottom theme tool desc*/
        $themeToolData = $eventCatModel->themeToolClass->getThemeToolData();
        if ( $themeToolData ) {
            // set current term to query later
            $themeToolData->term_id = $eventCatModel->getCurrentTerm()->term_id;

            // @ticket #11493: display associated organizations and associated venues
            echo $eventCatModel->renderWithData($smTaxonomyTemplateDir.'/taxonomy/html/partial/associated-orgs.php', $themeToolData);
            echo $eventCatModel->renderWithData($smTaxonomyTemplateDir.'/taxonomy/html/partial/associated-venues.php', $themeToolData);

            if ( $themeToolData->bottom_desc ) {
                ?>
                <div class="tax-bottom-content">
                    <p><?php echo Apollo_App::convertContentEditorToHtml($themeToolData->bottom_desc, TRUE) ?></p>
                </div>
                <?php
            }
        }
        ?>
        </div>
    </div>
</div>
