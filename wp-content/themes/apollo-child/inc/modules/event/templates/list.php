<div class="content-wrapper content-wrapper--master-layout">
    <div class="block block-events block-events--org block-events--art clearfix">
        <div class="breadcrumbs">
            <ul class="nav">
                <li><a href="<?php echo get_home_url(get_current_blog_id()) ?>"><?php _e('Home', 'apollo') ?></a></li>

                <?php if ( isset( $_GET['keyword'] ) || isset( $_GET['artist_style'] )
                    || isset( $_GET['artist_medium'] ) || isset( $_GET['city'] ) || isset( $_GET['last_name'] ) ) { ?>
                    <li> <a href="<?php echo home_url() ?>/event"><?php _e('Event', 'apollo') ?></a></li>
                    <li> <span><?php _e('Search', 'apollo') ?></span></li>
                <?php } else { ?>
                    <li> <span><?php _e('Event', 'apollo') ?></span></li>
                <?php } ?>
            </ul>
        </div>
        <?php Apollo_App::getSimpleTemplatePart(SONOMA_MODULES_DIR . '/event/templates/event-top.php', true); ?>
        <?php
        global $post;
        $current_url = home_url() . $_SERVER["REQUEST_URI"];
        //include search function here.
        //end include search function here.
        $search_obj = $model;
        $search_obj->set_main_post($post);
        $search_obj->search();

        $viewDefault = 2; //of_get_option(Apollo_DB_Schema::_EVENT_DEFAULT_VIEW_TYPE)

        ?>

        <?php if ( isset($_GET['keyword']) ): ?>
            <div class="search-tt mar-b-0 search-title-size">
                <?php
                // @ticket #11462, #11487: format start date & end date: Y-m-d -> M d, Y
                $search_obj->render_result_title(Apollo_DB_Schema::_EVENT_PT, 'M d, Y');
                ?>
            </div>
        <?php endif; ?>

        <?php
        if ( $search_obj->get_results() ):
            ?>

            <?php
            if ( $search_obj->have_pag()):
                ?>
                <div class="search-bkl">
                    <?php
                    $pag_html = '';
                    ?>
                    <div class="blk-paging">
                        <?php $pag_html = $search_obj->render_pagination();
                        echo $pag_html;
                        ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="search-bkl">
                <div class="social-share">
                    <div class="b-share-cat art">
                        <?php
                        SocialFactory::social_btns( array(
                                'info' => $search_obj->getShareInfo(),
                                'id' => !empty($post) && is_object($post) ? $post->ID : 0,
                                'data_btns' => array( 'event-print'),
                                'event_print_url' => $search_obj->getPrintEventUrl()
                            )
                        );
                        ?>
                    </div>
                </div>
            </div>
            <?php
            /*@ticket #17347 */
            $discountDescription = of_get_option(Apollo_DB_Schema::_EVENT_ENABLE_DISCOUNT_DESCRIPTION, 0);
            $handleClass = "";
            if($discountDescription){
                $handleClass =   'apl-event-discount ' . $search_obj->getCurrentTemplateName();
            }

            ?>
            <div class="search-bkl <?php echo $handleClass?>">

                <?php if($discountDescription) {
                    $discountImage = of_get_option(Apollo_DB_Schema::_EVENT_DISCOUNT_DESCRIPTION_IMAGE, '');
                    $dicountText = of_get_option(Apollo_DB_Schema::_EVENT_DISCOUNT_DESCRIPTION_TEXT, '');
                    ?>
                    <div class="apl-event-discount-description">
                        <?php if($discountImage !== '')  { ?>
                            <div>
                                <image src="<?php echo $discountImage?>"/>
                            </div>
                        <?php }?>
                        <?php if($dicountText !== '') { ?>
                            <p><?php echo __($dicountText, 'apollo') ?></p>
                        <?php } ?>
                    </div>
                <?php } ?>

                <nav class="type <?php echo (isset($_GET['by_my_location']) && $_GET['by_my_location']) ? 'mult-type' : ''?>" style="display: none">
                    <ul>
                        <li <?php echo !$search_obj->is_list_page($viewDefault) ? 'class="current"' : '' ?> ><a href="<?php echo $search_obj->get_template_btn_url() ?>"><i class="fa fa-th fa-2x"></i></a></li>
                        <li <?php echo $search_obj->is_list_page($viewDefault) ? 'class="current"' : '' ?>><a href="<?php echo $search_obj->get_template_btn_url( 'list' ) ?>"><i class="fa fa-bars fa-2x"></i></a></li>
                    </ul>
                </nav>
            </div>
        <?php endif; ?>

        <div class="search-bkl">
            <?php
            if ( $search_obj->is_list_page($viewDefault) ) {
                $container = $class = "search-artist-list";
                $search_obj->setTemplate(SONOMA_MODULES_DIR . '/event/templates/partials/listing/lists.php');
            }
            else {
                $container = $class = 'article-circle search-artist-thumb';
                $search_obj->setTemplate(SONOMA_MODULES_DIR . '/event/templates/partials/listing/thumbs.php');
            }
            ?>
            <nav class="article <?php echo $class; ?> ">
                <?php echo $search_obj->render_html(); ?>
            </nav>
        </div>

        <?php
        $is_view_more = $search_obj->have_more() && $search_obj->is_onepage();
        ?>
        <input name="search-have-more" value="<?php echo $search_obj->have_more() ?>" type="hidden" >

        <?php if ( ! $search_obj->is_map_page() ):
            $query_string = $search_obj->get_current_query_string();
            $query_string = add_query_arg(array(
                'page' => 2,
            ), $query_string);

            $query_string = ltrim($query_string, '?');
            ?>
        <?php endif; ?>

        <div class="search-bkl">
            <?php if ( $search_obj->have_pag()):  ?>
                <div class="blk-paging">
                    <?php echo $pag_html; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>



