<?php
$_a_event = $model->singlePost;

if($model->singlePost->isPrivateEvent()){
    $routing = of_get_option(Apollo_DB_Schema::_ROUTING_FOR_404_ERROR,'404');
    wp_redirect(home_url($routing));
    exit;
}

// Include log form for system log
Apollo_App::includeTemplate(APOLLO_TEMPLATES_DIR . '/partials/log-form.php', array(
    'id' => $model->getId(),
    'post_type' => Apollo_DB_Schema::_EVENT_PT
));

?>
<div class="content-wrapper content-wrapper--master-layout">
    <div class="block block-details-content block-details-event">
        <div class="breadcrumbs">
            <ul class="nav">
                <ul class="nav">
                    <li><a href="<?php echo home_url() ?>"><?php _e('Home', 'apollo') ?></a></li>
                    <?php echo $model->singlePost->get_primary_term_link('<li>', '</li>'); ?>
                    <li><span><?php echo $model->singlePost->get_title() ?></span></li>
                </ul>
            </ul>
            <div class="search-bkl social-share">
                <div class="b-share-cat art">
                    <?php echo $model->singlePost->getSharingInfo('above_title'); ?>
                </div>
            </div>
        </div>
        <?php Apollo_App::getSimpleTemplatePart(SONOMA_MODULES_DIR . '/event/templates/event-top.php', true); ?>
        <div class="detail-conten-body">
            <input name="eventid" value="<?php echo $model->getId() ?>" type="hidden">
            <?php Apollo_Next_Prev::getNavLink($model->getId(), Apollo_DB_Schema::_EVENT_PT);
            echo $model->renderGeneralContent();
            echo $model->renderAdmission();
            echo $model->renderIndividualDateTime();
            ?>

            <div class="blog-bkl">
                <div class="a-block">
                    <?php
                    echo $model->renderSlider();
                    echo $model->renderVideo()
                    ?>
                </div>
            </div>

            <?php
            echo $model->renderLocation();
            echo $model->renderParkingInfo();
            echo $model->renderAccessibility();
            echo $model->renderEventOrg();
            echo $model->renderAssociatedArtist();
            ?>

            <div class="blog-bkl astro-featr">
                <div class="a-block" id="event_comment_block">
                    <?php if ($model->isAllowComment()) { ?>
                        <h4><?php _e('COMMENTS', 'apollo') ?><span> <i class="fa fa-comment"></i></span></h4>
                        <div class="a-block-ct">
                            <?php comments_template('/partials/comments.php'); ?>
                        </div>
                    <?php } ?>
                </div><!-- #comments -->
            </div>
        </div>

        <?php require_once APOLLO_TEMPLATES_DIR . '/events/google-map-popup.php' ?>
        <?php require_once APOLLO_TEMPLATES_DIR . '/events/chooes-export-popup.php' ?>

    </div>
</div>
