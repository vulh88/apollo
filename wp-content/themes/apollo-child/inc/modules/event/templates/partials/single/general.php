<?php
$thumbsUpEnable = $model->singlePost->getThemeOption()->getThumbsUpEnable();
$excerpt = $model->singlePost->get_excerpt(200);
$even_end_date = Apollo_App::apollo_get_meta_data($model->getId(), Apollo_DB_Schema::_APOLLO_EVENT_END_DATE);
?>
<div class="blog-bkl organization clearfix">
    <img src="<?php echo SONOMA_FRONTEND_ASSETS_URI; ?>/images/demo/creative-sonoma-logo.png" class="org-logo-smn"
         alt="Creative Sonoma" title="Creative Sonoma"/>

    <h1 class="p-ttl">
        <?php echo $model->singlePost->get_title(); ?>
        <?php echo $model->singlePost->getThumbsUpPosition('next_title'); ?>
    </h1>

    <?php if ($model->singlePost->isHasExpiredEvent($even_end_date)) { ?>
        <p class="event-expired error"><i
                    class="fa fa-exclamation-triangle fa-lg"></i><span><?php _e('Please note, this event has expired.', 'apollo') ?></span>
        </p>
    <?php } ?>

    <?php $model->singlePost->renderIconsPosition('after_title'); ?>

    <p class="meta auth"><?php echo $model->singlePost->renderOrgVenueHtml(true); ?></p>

    <div class="search-bkl social-share">
        <div class="b-share-cat art">
            <?php echo $model->singlePost->getSharingInfo('under_present_by'); ?>
        </div>
    </div>

    <div class="rating-box rating-action rating-art">
        <div class="box-action-wrap <?php echo (!$thumbsUpEnable && !$model->isAllowComment()) ? 'box-action--empty' : ''; ?>">
            <?php echo $model->singlePost->getThumbsUpPosition('default'); ?>
            <?php if ($model->isAllowComment()): ?>
                <div class="cm"><a href="javascript:;"><?php _e('Comment', 'apollo') ?></a></div>
            <?php endif; ?>
        </div>
        <?php $model->singlePost->renderIconsPosition('after_thumbs_up') ?>
    </div>

    <div class="el-blk clearfix">
        <?php echo $model->singlePost->getDateDisplay('default'); ?>
        <div class="art-pic">
            <?php
            echo $model->singlePost->getDateDisplay('strip-top');
            echo $model->singlePost->get_image('medium');
            ?>
        </div>

        <div class="art-social">
            <div class="el-blk">
                <?php
                if ($excerpt['text']): ?>
                    <div class="a-txt-fea" id="_ed_sum_short">
                        <?php echo $model->singlePost->get_full_excerpt(); ?>
                    </div>
                <?php endif; ?>

                <div class="b-btn __inline_block_fix_space apl-detail-event-button-group">

                    <?php echo $model->singlePost->renderTickerBtn(array('class' => 'btn btn-b')) ?>

                    <?php echo $model->singlePost->renderCheckDCBtn(array('class' => 'btn btn-b')) ?>

                    <?php echo $model->singlePost->renderBookmarkBtn(array('class' => 'btn-bm btn btn-b')); ?>
                </div>
                <?php echo $model->getDescription() ?>
            </div>
        </div>
    </div>

</div>