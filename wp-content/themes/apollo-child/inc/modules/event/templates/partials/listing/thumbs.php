<?php
if ($results = $this->get_results()) {
    foreach ($results as $p):

        $e = get_event($p);
        ?>
        <div class="uk-width-large-1-4 uk-width-medium-1-3 uk-width-small-1-2 uk-width-1-1 article-item item-orange"
             data-url="<?php echo $e->get_permalink() ?>" data-type="link">
            <a href="<?php echo $e->get_permalink() ?>" class="event-org-lk"></a>
            <div class="article-title">
                <?php echo $e->get_image(SM_CIRCLE_IMAGE_SIZE, array('class' => 'teaser-bg'),
                    array(
                        'aw' => true,
                        'ah' => true,
                    ),
                    'normal'
                ) ?>
                <div class="content-teaser">
                    <p>
                        <a href="<?php echo $e->get_permalink() ?>"><span class="ev-tt" data-ride="" data-n="2"><?php echo $e->get_title($p)?></span></a>
                        <br>
                    </p>

                </div>
            </div>

        </div>

    <?php endforeach;

} else if (!isset($_GET['s']) && !isset($_GET['keyword'])) {
    _e('No results', 'apollo');
}