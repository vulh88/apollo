<?php
$accessibility = $model->singlePost->getAccessibility($model->getVenue());
$eventListACB = $accessibility['list_acb'];
$eventACBInfo = $accessibility['acb_info'];
$listFullACB = $accessibility['list_full_acb'];
if ($eventListACB || $eventACBInfo): ?>
    <?php if (isset($listFullACB) && isset($eventListACB)) : ?>
        <div class="blog-bkl">
            <div class="a-block">
                <?php echo Apollo_App::renderSectionLabels(__('ACCESSIBILITY INFO', 'apollo'), '<span> <i class="fa fa-book fa-2x"></i></span>'); ?>
                <div class="a-block-ct">
                    <?php if (!empty($listFullACB) && !empty($eventListACB)) : ?>
                        <ul class="access-listing ed-access-listing">
                            <?php ksort($listFullACB); ?>
                            <?php foreach ($listFullACB as $img => $label) {
                                if (in_array($img, $eventListACB)) {
                                    echo '
                        <li>
                            <ul class="ACLlist">
                                <li>
                                <img src="' . get_template_directory_uri() . '/assets/images/event-accessibility/2x/' . $img . '.png" alt="' . $label . '"/>
                                <label>' . $label . '</label>
                                </li>
                            </ul>
                        </li>';
                                }
                            }
                            ?>
                        </ul>
                    <?php endif; ?>
                    <?php
                    if (isset($eventACBInfo) && !empty($eventACBInfo)) :
                        $acbInfo200 = Apollo_App::getStringByLength($eventACBInfo, 200);
                        $acbFullContent = Apollo_App::convertContentEditorToHtml($eventACBInfo);
                        if ($acbInfo200['text']):
                            ?>
                            <div class="apl-internal-content" id="_acb_sum_short">
                                <?php echo $acbInfo200['text']; ?>
                                <?php if ($acbInfo200['have_more'] === true): ?>
                                    <a href="javascript:void(0);" data-type="vmore" class="vmore"
                                       data-target="#_acb_sum_full"
                                       data-own='#_acb_sum_short'><?php _e('View more', 'apollo') ?></a>
                                <?php endif; ?>
                            </div>
                            <div class="hidden apl-internal-content" id="_acb_sum_full">
                                <?php echo $acbFullContent; ?>
                                <a href="javascript:void(0);" data-type="vmore" class="vmore"
                                   data-target="#_acb_sum_short"
                                   data-own='#_acb_sum_full'><?php _e('View less', 'apollo') ?></a>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
<?php
endif;
?>