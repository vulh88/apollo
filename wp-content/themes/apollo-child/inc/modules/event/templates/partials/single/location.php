<?php
global $wp;
$str_address_all = $model->singlePost->getStrAddress();
$str_address_location = $model->singlePost->getFullAddress();
//@ticket #16193: [Google map] Add a theme option to turn their map section 'off'
$isTurnOffMapSection = of_get_option('turn_off_map_section', 0);
$isStatic = of_get_option(APL_Business_Module_Theme_Option::WHATS_NEARBY_STATIC, 0);
$whats_nearby_text = of_get_option(Apollo_DB_Schema::_TEXT_OF_NEARBY, __("What's Nearby", 'apollo'));
$whats_nearby_description = of_get_option(Apollo_DB_Schema::_TEXT_OF_NEARBY_DESCRIPTION, __("See what's near this venue", 'apollo'));
$map_api_key = of_get_option(Apollo_DB_Schema::_GOOGLE_API_KEY_BROWSER, '');
$isDisplayWhatNearBy = false;
$sCoor = $model->singlePost->getCoordinates($model->getVenue());
$staticMapLocation = !empty($sCoor) ? $sCoor : urlencode($str_address_all);
?>
<div class="blog-bkl">
    <div class="a-block offers-dates-times hidden">
        <?php echo do_shortcode('[apollo_offer_dates_times]'); ?>
    </div>
    <div class="clearfix"></div>
    <?php if (!empty($str_address_all)): ?>
        <div class="a-block locatn location-wrapper">
            <?php echo Apollo_App::renderSectionLabels(__('LOCATION', 'apollo'), '<span> <i class="fa fa-map-marker"></i></span>'); ?>
            <div class="a-block-ct">
                <div>
                    <b><?php echo $model->singlePost->get_location_name() ?></b>

                    <?php if (!empty($str_address_location)): ?>
                        <p><?php echo $str_address_location ?></p>
                    <?php endif; ?>
                </div>
                <div class="clearfix"></div>
                <div class="lo-meta clearfix hidden">
                    <p>
                        <b><?php _e('Accessibility Information:', 'apollo') ?> </b> <?php _e('Currently, no accessibility information is available for this event.', 'apollo') ?>
                    </p>
                    <p>
                        <b><?php _e('Connect with this Organization:', 'apollo') ?></b>
                        <a href="http://" target="_blank" class="lo-link fb"><i class="fa fa-facebook fa-lg"></i></a>
                        <a href="http://" target="_blank" class="lo-link tw"><i class="fa fa-twitter fa-lg"></i></a>
                        <a href="http://" target="_blank" class="lo-link donat">DONATE</a>
                    </p>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php
    if (empty($isDisplayWhatNearBy) && !$isTurnOffMapSection): ?>
        <?php if ($isStatic && (!isset($_GET['nr']) || $_GET['nr'] != 1)): ?>
            <div class="lo-left">
                <a href="https://www.google.com/maps/place/<?php echo $staticMapLocation ?>/">
                    <img src="https://maps.googleapis.com/maps/api/staticmap?center=<?php echo $staticMapLocation ?>&zoom=14&scale=1&size=620x350&maptype=roadmap&key=<?php echo $map_api_key; ?>&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0xff0000%7Clabel:%7C<?php echo $staticMapLocation ?>"
                         width=100% height=100% alt="Google Map of <?php echo $str_address_all ?>">
                </a>
                <form method="get" action="<?php echo home_url($wp->request); ?>#map" rel='nofollow'
                      class="comment-form">
                    <input type="hidden" name="nr" value="1"/>
                    <div style="display: inline-grid;">
                        <P>
                            <?php echo $whats_nearby_description ? $whats_nearby_description : __("See what's near this venue", 'apollo') ?>
                        </P>

                        <p class="t-l b-btn">
                            <input type="submit" id="submit" class="btn btn-b"
                                   value="<?php echo $whats_nearby_text ? $whats_nearby_text : __("WHAT'S NEAR BY", 'apollo') ?>"
                                   style="float: left;">
                        </p>
                    </div>
                </form>
            </div>

        <?php else: ?>
            <div class="a-block locatn location-wrapper">
                <div class="a-block-ct">
                    <form id="search-map" method="POST" action="" class="search-map-frm hidden"><i
                                class="fa fa-search fa-flip-horizontal"></i>
                        <input type="text" placeholder="Find Restaurants near this venue...">
                    </form>
                    <div <?php if (!$str_address_all) echo 'style="display: none"' ?> class="lo-left">
                        <div class="lo-map" id="_event_map"
                             data-coor="<?php echo $sCoor ?>"
                             data-address="<?php echo $str_address_all ?>"
                             data-relation_id="_popup_google_map_full"
                             data-alert="<?php _e('We\'re sorry. A map for this listing is currently unavailable.', 'apollo') ?>"
                             data-nonce="<?php echo wp_create_nonce('save-coor') ?>"
                             data-img_location="<?php echo get_stylesheet_directory_uri() ?>/assets/images/icon-location.png">
                        </div>
                        <p class="t-r apl-map-direction"><a href="javascript:void(0);" data-target="#_popup_google_map"
                                                            id="_event_trigger_fullmap"><?php _e('Full map and directions', 'apollo') ?></a>
                        </p>
                        <!-- @ticket #17305 -->
                        <div class="location-btn b-btn">
                            <a href="javascript:void(0);" id="_event_get_directions"
                               class="btn btn-b"><?php _e('GET DIRECTIONS', 'apollo') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>
</div>
