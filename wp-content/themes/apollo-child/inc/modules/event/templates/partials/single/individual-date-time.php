<?php
$arr_time = $model->singlePost->get_periods_time(" LIMIT " . Apollo_Display_Config::MAX_DATETIME . " ");
$additionalTime = $model->singlePost->get_meta_data(Apollo_DB_Schema::_APL_EVENT_ADDITIONAL_TIME, Apollo_DB_Schema::_APOLLO_EVENT_DATA);
if ($arr_time || $additionalTime):
    ?>
    <div class="blog-bkl ">
        <div class="a-block datetime">
            <?php echo Apollo_App::renderSectionLabels(__('INDIVIDUAL DATES & TIMES', 'apollo') . '*', '<span> <i class="fa fa-clock-o"></i></span>'); ?>
            <div class="a-block-ct">
                <div id="apl-event-more-time">
                    <?php $arr_time = $model->singlePost->get_periods_time(" LIMIT " . Apollo_Display_Config::MAX_DATETIME . " "); ?>

                    <?php if ($arr_time): ?>
                        <div class="clearfix">
                            <ul class="ind-time list-more-time">
                                <?php
                                echo $model->singlePost->render_periods_time($arr_time);
                                ?>
                            </ul>
                        </div>
                    <?php endif; ?>

                    <?php if ($model->singlePost->have_more_periods_time()): ?>
                        <p class="t-r">
                            <a href="javascript:void(0);"
                               data-container="#apl-event-more-time"
                               data-ride="ap-more"
                               data-holder=".list-more-time>:last"
                               data-sourcetype="url"
                               data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_get_more_event_time&event_id=' . $model->getId() . '&page=2') ?>"
                               data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
                               data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
                               class="btn-b arw"><?php _e('View more', 'apollo') ?>
                            </a>
                        </p>
                    <?php endif; ?>
                </div>

                <?php
                if (!empty($additionalTime)):
                    ?>
                    <div class="apl-internal-content"><p>
                            <b><?php _e('Additional time info: ', 'apollo') ?> </b> <?php echo Apollo_App::convertContentEditorToHtml($additionalTime) ?>
                        </p></div>
                <?php endif; ?>
                <div class="not">
                    <span>* </span> <?php _e('Event durations (if noted) are approximate. Please check with the presenting organization or venue to confirm start times and duration', 'apollo') ?>
                    .
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>