<?php
$parkingInfo = !empty($model->getVenueData()[Apollo_DB_Schema::_VENUE_PARKING_INFO]) ? $model->getVenueData()[Apollo_DB_Schema::_VENUE_PARKING_INFO] : '';
if (!empty($parkingInfo)) : ?>

    <div class="blog-bkl">
        <div class="a-block">
            <?php echo Apollo_App::renderSectionLabels(__('PARKING INFO', 'apollo'), '<span> <i class="fa fa-bus fa-2x"></i></span>'); ?>
            <div class="a-block-ct">
                <?php
                $fullContent = Apollo_App::convertContentEditorToHtml($parkingInfo);
                ?>
                <div class="apl-internal-content">
                    <?php echo $fullContent; ?>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>
