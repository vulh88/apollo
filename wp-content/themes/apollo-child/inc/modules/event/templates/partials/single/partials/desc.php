<?php
    $characters = $model->singlePost->getThemeOption()->getCharacterDescription();
    $content = $model->singlePost->get_content($characters);
?>

<?php if( $model->singlePost->getThemeOption()->getDisplayWithoutViewMore()) { ?>
    <div class="a-desc apl-internal-content desc-evt " id="_ed_full">
        <?php echo $model->singlePost->get_full_content() ?>
    </div>
<?php }else { ?>
    <div class="a-desc apl-internal-content desc-evt" id="_ed_short">
        <?php echo $content['text']; ?>
        <?php if($content['have_more'] === true): ?>
            <a href="javascript:void(0);" data-type="vmore" class="vmore" data-target="#_ed_full" data-own='#_ed_short'><?php _e('View more', 'apollo') ?></a>
        <?php endif; ?>
    </div>
    <div class="a-desc apl-internal-content hidden desc-evt " id="_ed_full">
        <?php echo $model->singlePost->get_full_content() ?>
        <a href="javascript:void(0);" data-type="vmore" class="vmore" data-target="#_ed_short" data-own='#_ed_full'><?php _e('View less', 'apollo') ?></a>
    </div>
<?php } ?>