<?php
// ADMISSION INFO
$admission_detail = $model->singlePost->getMetaInMainData(Apollo_DB_Schema::_ADMISSION_DETAIL);
$admission_contact = $model->singlePost->getMetaInMainData(Apollo_DB_Schema::_ADMISSION_PHONE);
$admission_email = $model->singlePost->getMetaInMainData(Apollo_DB_Schema::_ADMISSION_TICKET_EMAIL);
$additionalTime = $model->singlePost->get_meta_data(Apollo_DB_Schema::_APL_EVENT_ADDITIONAL_TIME, Apollo_DB_Schema::_APOLLO_EVENT_DATA);
$website = $model->singlePost->getMetaInMainData(Apollo_DB_Schema::_WEBSITE_URL);
$enable_individual_date_time = $model->singlePost->getThemeOption()->getEnableIndividualDateTime();
// get event's organization
$registeredOrg = $model->singlePost->get_register_org(); // = NULL if organization is TMP ORG
$registeredOrgID = empty($registeredOrg) ? '' : $registeredOrg->org_id;

if (!empty($admission_detail) || !empty($admission_contact) || !empty($website) || !empty($admission_email) ||
    (!empty($additionalTime) && !$enable_individual_date_time)) : ?>
    <div class="blog-bkl ">
        <div class="a-block">
            <?php echo Apollo_App::renderSectionLabels(__('ADMISSION INFO', 'apollo'), '<span> <i class="fa fa-info"></i></span>'); ?>
            <div class="a-block-ct">
                <?php if (!empty($admission_detail)): ?>
                    <div class="apl-internal-content"><?php echo Apollo_App::convertContentEditorToHtml($admission_detail); ?></div>
                <?php endif; ?>
                <?php if (!empty($admission_contact)): ?>
                    <p><b><?php _e("Contact", "apollo"); ?>: </b><?php echo $admission_contact ?></p>
                <?php endif; ?>
                <?php if (!empty($admission_email)): ?>
                    <p><b><?php _e("Email", "apollo"); ?>: </b> <a
                                href="mailto:<?php echo $admission_email ?>"> <?php echo $admission_email ?></a></p>
                <?php endif; ?>

                <?php if ($website): ?>
                    <p>
                        <i class="fa fa-link fa-lg">&nbsp;&nbsp;&nbsp;</i>
                        <a class="under_hover"
                           target="_blank"
                           href="<?php echo $website ?>"
                           data-ride="ap-logclick"
                           data-action="apollo_log_click_activity"
                           data-activity="click-official-website"
                           data-item_id="<?php echo $model->getId(); ?>"
                           data-start="<?php echo $model->singlePost->{Apollo_DB_Schema::_APOLLO_EVENT_START_DATE}; ?>"
                           data-end="<?php echo $model->singlePost->{Apollo_DB_Schema::_APOLLO_EVENT_END_DATE}; ?>" ,
                           data-org-id="<?php echo $registeredOrgID; ?>"><?php _e('Official Website', 'apollo') ?></a>
                    </p>
                <?php endif; ?>
                <?php
                if (!empty($additionalTime) && !$enable_individual_date_time):
                    ?>
                    <div class="apl-internal-content"><p>
                            <b><?php _e('Additional time info: ', 'apollo') ?> </b> <?php echo Apollo_App::convertContentEditorToHtml($additionalTime) ?>
                        </p></div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
