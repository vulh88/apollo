<?php
// REPAIR DATA - JUST SHOW SIMPLE EVENT
if (!empty($datas)):

    require_once SONOMA_MODULES_DIR . '/event/inc/single-model.php';
    $eventModel = new SM_Event_Model($datas);
    $_a_event = $eventModel->singlePost;

    // STARTDATE-ENDDATE
    $_arr_show_date = $_a_event->getShowTypeDateTime('picture');

    /* number vote */
    $ivote = $_a_event->getVote();

    $length_summary = 200;

    $thumbsUpEnable = $eventModel->singlePost->getThemeOption()->getThumbsUpEnable();
    ?>
    <article class="content-page category-detail category-page-event-box spotlight-top">
        <div class="social-share">
            <div class="b-share-cat art">
                <?php $eventModel->singlePost->getSharingInfo('top'); ?>
                <input name="eventid" value="<?php echo $_a_event->id; ?>" type="hidden"/>
            </div>
        </div>

        <h1 class="p-ttl">
            <a href="<?php echo $_a_event->get_permalink() ?>"><?php echo $_a_event->get_title(TRUE) ?></a>
            <?php echo $eventModel->singlePost->getThumbsUpPosition('next_title'); ?>
        </h1>

        <?php $eventModel->singlePost->renderIconsPosition('after_title'); ?>

        <p class="meta auth"><?php echo $_a_event->renderOrgVenueHtml(true) ?></p>

        <p class="sch-date"><?php echo $_a_event->render_sch_date(); ?></p>

        <?php if ($thumbsUpEnable) : ?>
            <div class="cat-rating-box rating-action">
                <div class="box-action-wrap">
                    <?php echo $eventModel->singlePost->getThumbsUpPosition('default'); ?>
                    <?php $eventModel->singlePost->renderIconsPosition('after_thumbs_up') ?>
                </div>
            </div>
        <?php endif ?>
        <a href="<?php echo $_a_event->get_permalink() ?>">
            <div class="pic">
                <?php echo $_a_event->get_image(SM_CIRCLE_IMAGE_SIZE, array('class' => 'teaser-bg')); ?>
            </div>
        </a>
        <div class="b-btn cat-detail __inline_block_fix_space">
            <?php echo $_a_event->renderTickerBtn(array('class' => 'btn btn-cat-detail')) ?>

            <?php echo $_a_event->renderCheckDCBtn(array('class' => 'btn btn-cat-detail')) ?>

            <?php echo $_a_event->renderBookmarkBtn(array('class' => 'btn btn-cat-detail btn-bm')); ?>
        </div>
        <p class="desc">
            <?php
            $summary = $_a_event->get_summary($length_summary);
            echo $summary['text'] ?> <?php if ($_a_event->is_have_more_summary($length_summary)): ?>...<?php endif ?>
        </p>
        <?php if ($_a_event->is_have_more_summary($length_summary)): ?>
            <a href="<?php echo $_a_event->get_permalink() ?>" class="vmore"><?php _e('View more', 'apollo') ?></a>
        <?php endif; ?>
    </article>
    <?php require_once APOLLO_TEMPLATES_DIR . '/events/google-map-popup.php' ?>
    <?php require_once APOLLO_TEMPLATES_DIR . '/events/chooes-export-popup.php' ?>

<?php endif; ?>
