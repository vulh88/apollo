<?php if (count($datas) > 0):
    $spotlightStyle = of_get_option(Apollo_DB_Schema::_EVENT_CATEGORY_SPOTLIGHT_STYLE, 'default') ?>
    <section class="list-category <?php echo $spotlightStyle == 'default' ? 'spotlight-left' : '' ?>">
        <?php
        foreach ($datas as $data):
            require_once SONOMA_MODULES_DIR . '/event/inc/single-model.php';
            $eventModel = new SM_Event_Model($data);
            $_a_event = $eventModel->singlePost;
            $this->set_displayed_event(( int )$_a_event->id);

            /// STARTDATE-ENDDATE
            $_arr_show_date = $_a_event->getShowTypeDateTime('picture');
            $length_summary = 200;

            /** @Ticket #14350 */
            $thumbsUpEnable = $eventModel->singlePost->getThemeOption()->getThumbsUpEnable();
            ?>
            <article class="category-itm">
                <a href="<?php echo $_a_event->get_permalink() ?>">
                    <div class="pic">
                        <?php echo $_a_event->get_image(SM_CIRCLE_IMAGE_SIZE, array('class' => 'teaser-bg')); ?>
                    </div>
                </a>
                <div class="category-t">
                    <h2 class="category-ttl">
                        <a href="<?php echo $_a_event->get_permalink() ?>"> <?php echo $_a_event->get_title(true) ?></a>
                        <?php echo $eventModel->singlePost->getThumbsUpPosition('next_title'); ?>
                    </h2>

                    <?php $eventModel->singlePost->renderIconsPosition('after_title'); ?>

                    <p class="meta auth">
                        <?php echo $_a_event->renderOrgVenueHtml(true) ?>
                    </p>

                    <p class="sch-date"><?php echo $_a_event->render_sch_date(); ?></p>

                    <?php if ($thumbsUpEnable) : ?>
                        <div class="cat-rating-box rating-action">
                            <?php echo $eventModel->singlePost->getThumbsUpPosition('default'); ?>
                            <?php $eventModel->singlePost->renderIconsPosition('after_thumbs_up') ?>
                        </div>
                    <?php endif ?>

                    <p class="desc">
                        <?php
                        $summary = $_a_event->get_summary($length_summary);
                        echo $summary['text']; ?><?php if ($_a_event->is_have_more_summary($length_summary)) echo '...'; ?>
                    </p>

                    <?php if ($_a_event->is_have_more_summary($length_summary)): ?>
                        <a href="<?php echo $_a_event->get_permalink() ?>" class="vmore"><?php _e("View more", 'apollo') ?></a>
                    <?php endif; ?>

                    <div class="b-btn category">
                        <?php
                        echo $_a_event->renderTickerBtn(array('class' => 'btn btn-category'));
                        echo $_a_event->renderCheckDCBtn(array('class' => 'btn btn-category'));
                        echo $_a_event->renderBookmarkBtn(array('class' => 'btn btn-category btn-bm apl-check-user-added'));
                        ?>
                    </div>
                </div>
            </article>
        <?php endforeach; ?>
    </section>
<?php endif; ?>