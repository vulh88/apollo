<article class="content-page astro-detail category-lft category-page-event-box ">
    <?php
    if (empty($datas)): ?>
        <br>
        <p><?php _e('There are no events available for this category at this time.', 'apollo') ?></p>
    <?php else:
        require_once SONOMA_MODULES_DIR . '/event/inc/single-model.php';
        $eventModel = new SM_Event_Model($datas);
        $_a_event = $eventModel->singlePost;
        $characters = 200;
        $a_200_content = $_a_event->get_summary($characters);
        $length_summary = Apollo_Display_Config::EVENT_OVERVIEW_NUMBER_CHAR;
        $thumbsUpEnable = $eventModel->singlePost->getThemeOption()->getThumbsUpEnable();
        ?>
        <div class="social-share">
            <div class="b-share-cat art">
                <?php echo $eventModel->singlePost->getSharingInfo('above_title'); ?>
                <input name="eventid" value="<?php echo $_a_event->id; ?>" type="hidden"/>
            </div>
        </div>

        <div class="social-icon-pck">
            <div class="page-tool"></div>

            <h1 class="p-ttl category-lft-title">
                <a href="<?php echo $_a_event->get_permalink() ?>"><?php echo $_a_event->get_title(true) ?></a>
                <?php echo $eventModel->singlePost->getThumbsUpPosition('next_title'); ?>
            </h1>

            <?php $eventModel->singlePost->renderIconsPosition('after_title'); ?>
            <p class="meta auth"><?php echo $_a_event->renderOrgVenueHtml(true); ?></p>
            <div class="apl-wrap-under-presented">
                <div class="social-share">
                    <div class="b-share-cat art">
                        <?php echo $eventModel->singlePost->getSharingInfo('under_present_by'); ?>
                    </div>
                </div>
                <p class="sch-date"><?php echo $_a_event->render_sch_date(); ?></p>
                <div class="rating-box rating-action">
                    <div class="box-action-wrap">
                        <?php echo $eventModel->singlePost->getThumbsUpPosition('default');
                        if (isset($allow_comment) && $allow_comment): ?>
                            <div class="cm"><a href="javascript:;"><?php _e('Comment', 'apollo') ?></a></div>
                        <?php endif; ?>
                    </div>
                    <?php $eventModel->singlePost->renderIconsPosition('after_thumbs_up') ?>
                </div>
            </div>

            <div class="astro-featr ct-s-c-d-p">
                <article class="blog-itm ">
                    <a href="<?php echo $_a_event->get_permalink() ?>">
                        <div class="pic">
                            <div class="article-title">
                                <?php echo $_a_event->get_image(SM_CIRCLE_IMAGE_SIZE, array('class' => 'teaser-bg'),
                                    array(
                                        'aw' => true,
                                        'ah' => true,
                                    ),
                                    'normal'
                                ) ?>
                            </div>
                        </div>
                    </a>

                    <?php

                    $summary = $_a_event->get_full_excerpt();
                    if ($summary):
                        ?>
                        <div class="a-txt-fea" id="_ed_sum_short">
                            <?php echo $summary; ?>
                        </div>
                    <?php endif; ?>

                    <div class="b-btn __inline_block_fix_space">

                        <?php echo $_a_event->renderTickerBtn(array('class' => 'btn btn-b')) ?>

                        <?php echo $_a_event->renderCheckDCBtn(array('class' => 'btn btn-b')) ?>

                        <?php echo $_a_event->renderBookmarkBtn(array('class' => 'btn-bm btn btn-b')); ?>
                    </div>

                    <div class="a-desc apl-internal-content desc-evt" id="_ed_short">
                        <p class="desc">
                            <?php echo $a_200_content['text'] . '' ?>
                            <?php if ($_a_event->is_have_more_summary($length_summary)) echo '...'; ?>
                        </p>
                        <a href="<?php echo $_a_event->get_permalink() ?>"
                           class="vmore"><?php _e('View more', 'apollo') ?></a>
                    </div>
                </article>
            </div>
            <?php
            include('social-meta.php');
            ?>
        </div>
        <?php require_once APOLLO_TEMPLATES_DIR . '/events/google-map-popup.php' ?>
        <?php require_once APOLLO_TEMPLATES_DIR . '/events/chooes-export-popup.php' ?>

    <?php endif; ?>

</article>
