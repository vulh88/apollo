<?php

// no need to display because:
//  + user hasn't re-active the theme to use this feature
//  + or leave as normal is active
if ( !isset($datas->associated_orgs) || $datas->leave_as_normal ) {
    return;
}

/**
 * @ticket #11493: display associated organizations
 */
include_once APOLLO_INCLUDES_DIR . '/src/org/inc/Apollo_List_Associated_Org.php';

$description    = $datas->associated_orgs_desc;
$associatedOrgs = !is_array($datas->associated_orgs) ? maybe_unserialize($datas->associated_orgs) : $datas->associated_orgs;
$list           = new Apollo_List_Associated_Org($associatedOrgs);

?>

<?php if ( $associatedOrgs ) : ?>
    <div class="evt-blk" style="margin-top:30px;">
        <div class="event-contact"><?php _e( 'Associated Organizations', 'apollo' ); ?></div>

        <!-- Display associated organizations description -->
        <?php if ( $description ): ?>
            <div class="apl-internal-content">
                <p><?php echo Apollo_App::convertContentEditorToHtml($description, TRUE) ?></p>
            </div>
        <?php endif; ?>

        <!-- Display associated organizations list -->
        <div class="search-bkl apollo-associated-orgs-list">
            <section class="list-more-category">
                <?php if ( !$list->isEmpty() ): ?>
                    <div id="apollo-view-more-associated-orgs-container">
                        <?php echo $list->renderOrgHtml(); ?>
                    </div>
                <?php endif; ?>

                <?php if ( $list->isShowMore() ): ?>
                    <div class="load-more b-btn">
                        <a href="javascript:void(0);"
                           data-ride="ap-more"
                           data-holder="#apollo-view-more-associated-orgs-container>:last"
                           data-sourcetype="url"
                           data-container=".apollo-associated-orgs-list"
                           data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_show_more_associated_orgs&page=2&term_id=' . $datas->term_id) ?>"
                           data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
                           data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
                           class="btn-b arw"><?php _e('SHOW MORE', 'apollo') ?>
                        </a>
                    </div>
                <?php endif; ?>
            </section>
        </div>
    </div>
<?php endif; ?>

