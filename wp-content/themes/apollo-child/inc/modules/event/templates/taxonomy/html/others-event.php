<?php
/** @Ticket #14350 */
foreach ($datas as $data):
    require_once SONOMA_MODULES_DIR . '/event/inc/single-model.php';
    $eventModel = new SM_Event_Model($data);
    $_a_event = $eventModel->singlePost;
    $thumbsUpEnable = $eventModel->singlePost->getThemeOption()->getThumbsUpEnable();
    // $this->set_displayed_event( ( int ) $_a_event->id );
    $_arr_show_date = $_a_event->getShowTypeDateTime('picture');
    $length_summary = Apollo_Display_Config::EVENT_OVERVIEW_NUMBER_CHAR;
    ?>
    <div class="more-cat-itm category-page-event-box">
        <a href="<?php echo $_a_event->get_permalink() ?>">
            <div class="more-pic">
                <?php echo $_a_event->get_image(SM_CIRCLE_IMAGE_SIZE, array('class' => 'teaser-bg')); ?>
            </div>
        </a>
        <div class="more-ct">
            <h3>
                <a href="<?php echo $_a_event->get_permalink() ?>"><?php echo $_a_event->get_title(TRUE) ?></a>
                <?php echo $eventModel->singlePost->getThumbsUpPosition('next_title'); ?>
            </h3>

            <p class="meta auth"><?php echo $_a_event->renderOrgVenueHtml(true) ?></p>
            <p class="p-date">
                <?php
                if ($_arr_show_date['type'] !== 'none') {
                    $us = $_arr_show_date['unix_start'];
                    $ue = $_arr_show_date['unix_end'];

                    if ($_arr_show_date['type'] === 'one') {
                        echo Apollo_App::apl_date('M d', $us) . ', <span>' . date('Y', $us) . '</span>';
                    } else if ($_arr_show_date['type'] === 'two') {
                        // same year
                        if (date('Y', $ue) === date('Y', $us)) {
                            echo Apollo_App::apl_date('M d', $us) . ' - ' . Apollo_App::apl_date('M d', $ue) . ', <span>' . date('Y', $us) . '</span>';
                        } else {
                            echo Apollo_App::apl_date('M d', $us) . ', <span>' . date('Y', $us) . '</span>' . ' - ' . Apollo_App::apl_date('M d', $ue) . ', <span>' . date('Y', $ue) . '</span>';
                        }
                    }
                } ?>

            </p>
            <?php if ($thumbsUpEnable) : ?>
                <div class="more-cat-rating-box">
                    <div class="box-action-wrap">
                        <?php echo $eventModel->singlePost->getThumbsUpPosition('default'); ?>
                        <?php $eventModel->singlePost->renderIconsPosition('after_thumbs_up') ?>
                    </div>
                </div>
            <?php endif ?>
        </div>
        <div class="b-btn">
            <?php echo $_a_event->renderTickerBtn(array('class' => 'btn btn-b')) ?>

            <?php echo $_a_event->renderCheckDCBtn(array('class' => 'btn btn-b')) ?>

            <?php echo $_a_event->renderBookmarkBtn(array('class' => 'btn btn-b btn-bm'));
            ?>
        </div>
    </div>
<?php endforeach; ?>


