<?php

class SM_Event_Mod extends SM_Mod_Abstract
{
    public function __construct(){

        $this->renderPageTemplate();
    }

    public function renderPageTemplate(){

        $eventModel = true;

        if (is_single()){
            require_once __DIR__.'/inc/single-model.php';
            $eventModel = new SM_Event_Model();
            $file = SONOMA_MODULES_DIR . '/event/templates/single.php';
        }
        else if (is_tax()) {
            require_once __DIR__.'/inc/taxonomy-model.php';
            global $apl_current_term_object;
            $currentTerm = $apl_current_term_object ? $apl_current_term_object : Apollo_App::getCurrentTerm();
            $eventModel = new SM_Event_Taxonomy_Model($currentTerm);
            $file = SONOMA_MODULES_DIR . '/event/templates/taxonomy.php';
        }
        else{
            require_once __DIR__.'/inc/list-model.php';
            $eventModel = new SM_Events_Model(true);
            $file = SONOMA_MODULES_DIR . '/event/templates/list.php';
        }

        if( file_exists($file) ){
            $this->smGetTemplatePartCustom($file, $eventModel);
        }

    }
}

new SM_Event_Mod();