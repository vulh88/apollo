<?php

class SM_Event_Model
{
    public $singlePost = '';
    private $id;
    public $venue;
    public $venueData;

    public function __construct($postID = '')
    {
        if(!empty($postID)){
            $this->setId($postID);
            $this->singlePost = get_event($postID);
        }
        else{
            global $post;
            $this->setId($post->ID);
            $this->singlePost = get_event($post);
        }

        $this->setVenue();
        $this->setVenueData();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function isAllowComment()
    {
        return of_get_option(Apollo_DB_Schema::_ENABLE_COMMENT, 1) && comments_open();
    }

    public function getVenueData()
    {
        return $this->venueData;
    }

    public function setVenueData()
    {
        $venueData = array();

        if ($this->venue->id) {
            $venueData = Apollo_App::unserialize(Apollo_App::apollo_get_meta_data($this->venue->id, Apollo_DB_Schema::_APL_VENUE_DATA));
        }

        $this->venueData = $venueData;
    }

    public function getVenue()
    {
        return $this->venue;
    }

    public function setVenue()
    {
        $this->venue = get_venue($this->singlePost->{Apollo_DB_Schema::_APOLLO_EVENT_VENUE});
    }

    public function getDescription()
    {
        return Apollo_App::getSimpleTemplatePart(SONOMA_MODULES_DIR . '/event/templates/partials/single/partials/desc.php', $this, true);
    }

    public function renderGeneralContent()
    {
        return Apollo_App::getSimpleTemplatePart(SONOMA_MODULES_DIR . '/event/templates/partials/single/general.php', $this, true);
    }

    public function renderAdmission()
    {
        return Apollo_App::getSimpleTemplatePart(SONOMA_MODULES_DIR . '/event/templates/partials/single/admission.php', $this, true);
    }

    public function renderIndividualDateTime()
    {
        $enable_individual_date_time = $this->singlePost->getThemeOption()->getEnableIndividualDateTime();
        if ($enable_individual_date_time) {
            return Apollo_App::getSimpleTemplatePart(SONOMA_MODULES_DIR . '/event/templates/partials/single/individual-date-time.php', $this, true);
        }
    }

    public function renderSlider()
    {
        $gallerys = explode(',', $this->singlePost->get_meta_data(Apollo_DB_Schema::_APOLLO_EVENT_IMAGE_GALLERY));

        if(!empty($gallerys[0])){
            $data = SM_View_Helper::handleDataPhotoItems($gallerys);
            return Apollo_App::getSimpleTemplatePart(SONOMA_MODULES_DIR . '/common-templates/slider.php', $data, true);
        }

    }

    public function renderVideo()
    {
        $videos = $this->singlePost->getMetaInMainData(Apollo_DB_Schema::_VIDEO);

        if (!empty($videos)){
            $data = SM_View_Helper::handleDataVideos($videos);
            return Apollo_App::getSimpleTemplatePart(SONOMA_MODULES_DIR . '/common-templates/video.php', $data, true);

        }
    }

    public function renderLocation()
    {
        return Apollo_App::getSimpleTemplatePart(SONOMA_MODULES_DIR . '/event/templates/partials/single/location.php', $this, true);
    }

    public function renderParkingInfo()
    {
        return Apollo_App::getSimpleTemplatePart(SONOMA_MODULES_DIR . '/event/templates/partials/single/parking-info.php', $this, true);
    }

    public function renderAccessibility()
    {
        return Apollo_App::getSimpleTemplatePart(SONOMA_MODULES_DIR . '/event/templates/partials/single/accessibility.php', $this, true);
    }

    public function renderEventOrg()
    {
        return Apollo_App::getSimpleTemplatePart(SONOMA_MODULES_DIR . '/event/templates/partials/single/events-org.php', $this, true);
    }

    public function renderAssociatedArtist()
    {
        return Apollo_App::getSimpleTemplatePart(SONOMA_MODULES_DIR . '/event/templates/partials/single/associated-artists.php', $this, true);
    }
}