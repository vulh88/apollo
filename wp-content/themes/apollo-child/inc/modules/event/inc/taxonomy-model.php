<?php
require_once APOLLO_TEMPLATES_DIR .'/taxonomy/inc/single.php';

class SM_Event_Taxonomy_Model extends Apollo_Single_Category
{
    public function __construct($term)
    {
        parent::__construct($term);
    }

    public function getCurrentTerm() {
        return $this->term;
    }
}