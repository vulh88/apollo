<?php

include_once APOLLO_INCLUDES_DIR. '/src/event/inc/class-event-search.php';

class SM_Events_Model extends Apollo_Event_Page
{
    public function __construct($exceptPostContentInSelect = false, $isCounting = false, $officialTags = false)
    {
        parent::__construct($exceptPostContentInSelect, $isCounting, $officialTags);
    }

    public function setTemplate($template) {
        $this->template = $template;
    }
}