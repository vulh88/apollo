<?php

/**
 * Class SM_Page_Mod
 */

require_once __DIR__.'/inc/sm_page_model.php';
class SM_Page_Mod extends SM_Mod_Abstract
{
    public function __construct(){

        $this->renderPageTemplate();
    }

    public function renderPageTemplate(){
	    $smPageModel = new SM_Page_Model();
        if ( is_page_template('default') ){
            $file = SONOMA_MODULES_DIR.'/page/templates/default.php';
        } elseif (is_page_template('page_custom.php')){
            $file = SONOMA_MODULES_DIR.'/page/templates/custom.php';
        } elseif (is_page_template('page_sonoma_fire_2017.php')){

            $file = SONOMA_MODULES_DIR.'/page/templates/sonoma_fire_2017.php';
        } else {
            $file = SONOMA_MODULES_DIR.'/page/templates/template_hooked.php';
        }

        $this->smGetTemplatePartCustom($file,$smPageModel);
    }

}

new SM_Page_Mod();