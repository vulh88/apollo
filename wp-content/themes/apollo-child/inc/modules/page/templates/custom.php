<div class="content-wrapper content-wrapper--master-layout">

	<?php
	$themeUri = sm_get_stylesheet_directory_uri();
	 if( !empty($model)) : ?>
	<div class="block block-about teaser teaser-bottom clearfix"
	     style=" height:830px;width:100%;background-image:url(<?php echo $model->getSrc();?>);background-repeat:no-repeat;background-position:50%;object-fit:cover;background-size:cover;overflow:hidden">
		<div class="teaser-content" data-uk-scrollspy="{cls:'uk-animation-fade'}">
			<h1><?php echo $model->getTitle() ?></h1>
			<div class="">
				<?php echo $model->getDesc() ; ?>
			</div>
			<a href="#feature" class="let-down" data-uk-smooth-scroll="{offset: 89}">
				<i class="icon ico-down"></i>
			</a>
		</div>
	</div>
	<!-- END: ABOUT FEATURE-->
	<div id="feature" class="block block-feature bg-org-ft teaser teaser-left org-color clearfix"
	     style=" background:url(<?php echo $model->getSrc2() ?>) no-repeat 100% 0;background-size:cover"
	     data-uk-scrollspy="{cls:'uk-animation-fade'}">
		<div class="teaser-content">
			<h1 class="ttl-feature"><?php echo $model->getTitle2() ?></h1>
			<div class="">
				<?php echo $model->getDesc2(); ?>
			</div>
		</div>
	</div>
	<!-- END: block-feature -->
	<div class="block block-events block-board clearfix" data-uk-scrollspy="{cls:'uk-animation-fade'}">
			 <h1>WHO DOES IT</h1>
			 <?php $model->renderGalleryTemplatePart();?>
		 </div>
	<?php endif; ?>
</div>