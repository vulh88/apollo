<!-- BLOCK AUDIOS -->
<?php $blockData = $template_args['data']; ?>
<div class="sub-content-blk custom-block">
    <h1 class="block-title"><?php echo $blockData['title'] ?></h1>
    <p>
        <?php echo $blockData['description']; ?>
    </p>
</div>
<div id="apl-pbm-block-audio" class="sub-content-blk custom-block custom-block-large">
    <div class="_js-btm-slider-wrap">
        <div class="bottom-slider">
            <ul class="slides">

                <?php foreach ($blockData['list'] as $k=> $item) : ?>
                    <?php
                    $audioEmbed =  isset($item['audio_info']['embed']) ? $item['audio_info']['embed'] : "";
                    $audioThumb = isset($item['audio_info']['thumb']) ? $item['audio_info']['thumb'] : "";
                    $audioDescription = isset($item['audio_info']['desc']) ? $item['audio_info']['desc'] : "";
                    ?>

                    <li class="audio">
                        <img width="388" height="400" src="<?php echo $audioThumb ?>"
                             class="attachment-medium size-medium wp-post-image" alt=""
                             sizes="(max-width: 388px) 100vw, 388px" />
                        <div class="hover-content">
                            <a href="#audio-content-<?php echo $k ?>" class="link colorbox"></a>
                            <div class="wrapper-middle">
                                <h1 class="content-title"><?php echo $audioDescription ?></h1>
                                <h2 class="content-subtitle"></h2>
                            </div>
                        </div>
                    </li>

                <?php endforeach; ?>

            </ul>
        </div>

        <?php foreach ($blockData['list'] as $k=> $item) : ?>
            <?php
            $audioEmbed = $item['audio_info']['embed'] ;
            $audioType = $item['audio_info']['type'] ;
            $audioDescription = isset($item['audio_info']['desc']) ? $item['audio_info']['desc'] : "";
            ?>
            <div class="hidden">
                <div id="audio-content-<?php echo $k ?>" class="audio-popup-content">
                    <?php
                    switch ($audioType) {
                        case 'file':
                            ?>
                            <p>
                                <audio controls="controls" style="border:0px;width:100%;height:50px;">
                                    <source src='<?php echo $audioEmbed ?>' type="audio/ogg">
                                    <source src='<?php echo $audioEmbed ?>' type="audio/mpeg">
                                    <embed height="50" width="100%" src='<?php echo $audioEmbed ?>'> </embed>
                                </audio>
                            </p>
                            <?php
                            break;
                        case 'embed':
                            ?>
                            <div class="audio-iframe text-center">
                                <iframe src="<?php echo $audioEmbed ?>" frameborder="0"></iframe>
                            </div>
                            <?php
                            break;
                        default:
                            echo '<p>';
                            $val = str_replace('\\','', $audioEmbed);
                            echo $val;
                            echo '  </p>';
                    }
                    ?>

                    <p class="audio-description"><?php echo $audioDescription; ?></p>

                </div>
            </div>

        <?php endforeach; ?>

    </div>
</div>
<!-- END BLOCK AUDIOS -->