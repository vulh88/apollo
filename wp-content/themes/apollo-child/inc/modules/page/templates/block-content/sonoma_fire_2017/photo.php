<!-- BLOCK PHOTOS -->
<?php $blockData = $template_args['data']; ?>
<div class="sub-content-blk custom-block">
    <h1 class="block-title"><?php echo $blockData['title']  ?></h1>
    <p>
        <?php echo $blockData['description']; ?>
    </p>
</div>
<div class="sub-content-blk custom-block custom-block-large">
    <div class="_js-btm-slider-wrap">
        <div class="bottom-slider">
            <ul class="slides">

                <?php foreach ($blockData['list'] as $item) :
                    $image_src = wp_get_attachment_image_src( $item['media_id'], 'thumbnail');
                    $image_src = $image_src[0];
                    ?>
                    <li>
                        <img src="<?php echo $image_src ?>"
                             class="attachment-medium size-medium wp-post-image" alt="" srcset="<?php echo $image_src ?>"
                             sizes="(max-width: 388px) 100vw, 388px" />
                        <div class="hover-content">
                            <a href="<?php echo get_permalink($item['ID']) ?>" target="_blank" class="link"></a>
                            <div class="wrapper-middle">
                                <h1 class="content-title"><?php echo $item['post_title'] ?></h1>
                                <h2 class="content-subtitle"></h2>
                            </div>
                        </div>
                    </li>
                <?php endforeach; ?>

            </ul>
        </div>
    </div>
</div>
<!-- END BLOCK PHOTOS -->