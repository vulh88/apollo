<!-- BLOCK EVENTS -->
<?php $blockData = $template_args['data']; ?>
<div class="sub-content-blk custom-block">
    <h1 class="block-title"><?php echo $blockData['title'] ?></h1>
    <p>
        <?php echo $blockData['description'] ?>
    </p>
</div>
<div id="block-classifieds" class="sub-content-blk custom-block custom-block-large">
    <div class="wrapper-events">
        <?php foreach($blockData['list']['data'] as $item) : ?>
            <a class="item-events" target="_blank" href="<?php echo get_permalink($item['ID']) ?>">
                <div class="thumb" style="background-image: url(<?php echo $item['feature_image_url'] ?>)">
                    <i class="fa fa-clock-o"></i>
                </div>
                <div class="content">
                    <h3><?php echo $item['post_title'] ?></h3>
                    <time>
                        <?php echo $item['exp_date'] ; ?>
                    </time>
                </div>

            </a>
        <?php endforeach; ?>
        <?php if($blockData['list']['have_more']) : ?>
            <div class="custom-block text-right" style="width: 100%">
                <a href="javascript:void(0);"
                   data-container="#block-classifieds"
                   data-holder="#block-classifieds .item-events:last"
                   data-sourcetype="url"
                   data-pbm_have_multi_load_more=true
                   data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apl_pbm_load_more_post&type=classified&post_id=' . $template_args['post_id'] . '&offset=' . $blockData['list']['offset']) ?>"
                   data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
                   data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
                   class="pbm-load-more b-btn arw"><?php _e('Show more', 'apollo') ?>
                </a>
            </div>
        <?php endif; ?>
    </div>
</div>
    <!-- END BLOCK EVENTS -->