<!-- BLOCK VIDEOS -->
<?php $blockData = $template_args['data']; ?>
<div class="sub-content-blk custom-block">
    <h1 class="block-title"><?php echo $blockData['title'] ?></h1>
    <p>
        <?php echo $blockData['description']; ?>
    </p>
</div>
<div id="apl-pbm-block-video" class="sub-content-blk custom-block custom-block-large">
    <div class="_js-btm-slider-wrap">
        <div class="bottom-slider">
            <ul class="slides">

                <?php
                    $protocol = isset($_SERVER["HTTPS"]) ? 'https' : 'http';
                    foreach ($blockData['list'] as $item) : ?>
                    <?php
                    $videoCode = isset($item['video_info']['code']) ? $item['video_info']['code'] : "";
                    $videoType = isset($item['video_info']['video_type']) ? $item['video_info']['video_type'] : "";
                    $videoHref =  isset($item['video_info']['embed']) ? $item['video_info']['embed'] : "";
                    $videoThumb = isset($item['video_info']['thumb']) ? $item['video_info']['thumb'] : "";
                    $videoDescription = isset($item['video_info']['desc']) ? $item['video_info']['desc'] : "";

                    if($videoType === "youtube") {
                        $videoHref = $protocol . "://www.youtube.com/embed/{$videoCode}?rel=0&amp;wmode=transparent";
                    } elseif($videoType === "vimeo") {
                        $videoHref = $protocol . "://player.vimeo.com/video/{$videoCode}";
                    }
                    ?>


                    <li class="video">
                        <img width="388" height="400" src="<?php echo $videoThumb ?>"
                             class="attachment-medium size-medium wp-post-image" alt=""
                             sizes="(max-width: 388px) 100vw, 388px" />
                        <div class="hover-content">
                            <a href="<?php echo $videoHref ?>" class="link colorbox <?php echo $videoType ?>"></a>
                            <div class="wrapper-middle">
                                <h1 class="content-title"><?php echo $videoDescription ?></h1>
                                <h2 class="content-subtitle"></h2>
                            </div>
                        </div>
                    </li>

                <?php endforeach; ?>

            </ul>
        </div>

    </div>
</div>
<!-- END BLOCK VIDEOS -->