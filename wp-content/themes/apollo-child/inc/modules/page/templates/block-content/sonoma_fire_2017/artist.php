<!-- BLOCK CREATIVES -->
<?php $blockData = $template_args['data']; ?>
<div class="sub-content-blk custom-block">
    <h1 class="block-title"><?php echo $blockData['title'] ?></h1>
    <p>
        <?php echo $blockData['description']; ?>
    </p>
</div>
<div id="block-artists" class="sub-content-blk custom-block article-circle article-circle-custom mini custom-block-large-x fix-circles-5-items">

    <?php foreach ($blockData['list']['data'] as $item) : ?>
        <div class="uk-width-large-1-4 uk-width-medium-1-3 uk-width-small-1-2 uk-width-1-1 article-item item-orange">
            <a href="<?php echo get_permalink($item['ID']) ?>" target="_blank" class="event-org-lk"></a>
            <div class="article-title">
                <img src="<?php echo $item['feature_image_url'] ?>"
                     alt="<?php echo $item['post_title'] ?>"
                     width=300
                     height=300
                     class="teaser-bg wp-post-image" />
                <div class="content-teaser">
                    <p>
                        <a href="<?php echo get_permalink($item['ID']) ?>">
                            <span class="ev-tt" data-ride="" data-n="2"><?php echo $item['post_title'] ?></span>
                        </a>
                        <br>
                    </p>

                </div>
            </div>
        </div>
    <?php endforeach; ?>

</div>

<?php if($blockData['list']['have_more']) : ?>
    <div class="sub-content-blk custom-block-large custom-block text-right">
        <a href="javascript:void(0);"
           data-container="#block-artists"
           data-holder="#block-artists .uk-width-large-1-4:last"
           data-sourcetype="url"
           data-pbm_have_multi_load_more=true
           data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apl_pbm_load_more_post&type=artist&post_id=' . $template_args['post_id'] . '&offset=' . $blockData['list']['offset']) ?>"
           data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
           data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
           class="pbm-load-more b-btn arw"><?php _e('Show more', 'apollo') ?>
        </a>
    </div>
<?php endif; ?>
<!-- END BLOCK CREATIVES -->