<!-- BLOCK FIRE -->
<?php $blockData = $template_args['data']; ?>
<div class="sub-content-blk custom-block rich-txt">
    <h1 class="block-title text-color-primary"><?php echo $blockData['title'] ?></h1>

    <p>
        <?php echo $blockData['description'] ?>
    </p>
</div>
<!-- END BLOCK FIRE -->