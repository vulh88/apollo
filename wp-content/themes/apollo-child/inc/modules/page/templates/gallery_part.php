<?php
$label= isset($template_args['data']) && !empty($template_args['data']['label']) ? $template_args['data']['label'] : '';
$topDesc= isset($template_args['data']) && !empty($template_args['data']['top_desc']) ? $template_args['data']['top_desc'] : '';
$bottomDesc= isset($template_args['data']) && !empty($template_args['data']['bottom_desc']) ? $template_args['data']['bottom_desc'] : '';
$data = isset($template_args['data']) && !empty($template_args['data']['data']) ? $template_args['data']['data'] : '';
$model = isset($template_args['model'])  ? $template_args['model'] : '';

?>
<?php if (!empty($data)) { ?>
	<h2><?php echo $label ?></h2>
	<div class="gallery-description rich-txt"><?php echo $topDesc; ?></div>
	<div class="article article-circle">
		<?php $model->renderGalleryItem($data) ?>
	</div>
	<div class="gallery-description rich-txt"><?php echo $bottomDesc; ?></div>

<?php } ?>


