<div class="content-wrapper content-wrapper--master-layout">

    <?php
    $themeUri = sm_get_stylesheet_directory_uri();
    if( !empty($model)) :
        ?>

        <div class="block block-events block-events--org clearfix">

            <div class="breadcrumbs">
                <ul class="nav">
                    <ul class="nav">
                        <li><a href="<?php echo home_url() ?>"><?php _e( 'Home', 'apollo' ) ?></a></li>
                        <li><?php echo $model->getTitle() ?></li>
                    </ul>
                </ul>
                <div class="search-bkl social-share">
                    <div class="b-share-cat art">
                        <?php
                        $model->getSharingInfo(array(), $model->getPostId());
                        ?>
                    </div>
                </div>
            </div>

            <?php
                $model->renderBlockContent();
            ?>

        </div>

    <?php endif; ?>

</div>