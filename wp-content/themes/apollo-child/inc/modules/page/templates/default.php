<div class="content-wrapper content-wrapper--master-layout">

	<?php
	$themeUri = sm_get_stylesheet_directory_uri();
	if( !empty($model)) : ?>
	<div class="main-wrap" style="background-image: url(<?php echo $model->getBackgroundImage(); ?>)">
		<div class="block block-default-tmp">
			<div class="default-ttl">
				<h3><?php echo $model->getTitle() ?></h3>
			</div>
			<div class="default-content rich-txt">
				<?php echo $model->getDesc()?>
			</div>
		</div>

	</div>
	<?php endif; ?>

</div>