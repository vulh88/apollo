<?php
$targetLink = isset($template_args['data']) && !empty($template_args['data']['url']) ? $template_args['data']['url'] : '';
$aLink = $targetLink ? '<a href="'.$targetLink.'" class="circle-k"></a>' : '';
$name = isset($template_args['data']) && !empty($template_args['data']['name']) ? $template_args['data']['name'] : '';
$title = isset($template_args['data']) && !empty($template_args['data']['title']) ? $template_args['data']['title'] : '';
$id = isset($template_args['data']) && !empty($template_args['data']['id']) ? $template_args['data']['id'] : '';
$class = isset($template_args['class']) && !empty($template_args['class']) ? $template_args['class'] : '';
$image = wp_get_attachment_image_src($id,SM_CIRCLE_IMAGE_SIZE);
$image = !empty($image) ? $image[0] : '';


?>


<div class="article-item <?php echo $class  ?>">
    <?php echo $aLink; ?>
    <div class="article-title">
        <img src="<?php echo $image?>" class="teaser-bg" alt="">
        <div class="content-teaser">
            <p>
                <span><?php echo $name?></span><br/>
                <small><?php echo $title?></small>
            </p>
        </div>
    </div>
</div>