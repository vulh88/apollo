<?php 
$templateNameFilePath = get_page_template();
if(file_exists($templateNameFilePath)) {
?>
<div class="content-wrapper content-wrapper--master-layout">
    <div class="main-wrap">
        <div class="block block-default-tmp">
            <div class="default-content rich-txt">
                <?php include_once $templateNameFilePath; ?>
            </div>
        </div>
    </div>
</div>
<?php } else {
    get_template_part('404');
}