<?php

/**
 * Class SM_Page_Admin
 */
class SM_Page_Admin
{

    public function __construct() {
        $post_id = isset($_GET['post']) && !empty($_GET['post']) ? $_GET['post'] : 0;
        $multiThumbnail = new MultiPostThumbnails(array('label' => 'Secondary Image', 'id' => 'secondary-image', 'post_type' => 'page'));
        $backgroundThumbnail = new MultiPostThumbnails(array('label' => __('BackGround Image','apollo'), 'id' => 'background-image', 'post_type' => 'page'));
        if( get_page_template_slug( $post_id ) == 'page_custom.php'){
            add_action('add_meta_boxes', array($multiThumbnail, 'add_metabox'));
            add_action('add_meta_boxes', array($this, 'add_metabox'));
        }else{
            add_action('add_meta_boxes', array($backgroundThumbnail, 'add_metabox'));
        }
        add_action('add_meta_boxes', array($this, 'add_summary_metabox'));
        add_action('save_post', array($this, 'save'), 20, 2 );

    }

    public function output_data_page($post)
    {
        require_once APOLLO_ADMIN_DIR . '/apollo-meta-box-function.php';
        echo '<div class="options_group event-box" >';
        $title = get_post_meta($post->ID, SM_Common_Const::_SM_PAGE_TITLE, true);

        // Description
        apollo_wp_text_input(array(
            'id' => SM_Common_Const::_SM_PAGE_TITLE,
            'label' => __('Title 2', 'apollo'),
            'desc_tip' => 'true',
            'description' => "",
            'value'  => $title,
        ));

        $desc = get_post_meta($post->ID, SM_Common_Const::_SM_PAGE_DESCRIPTION, true);
        // Description
        apollo_wp_editor(array(
            'id' => SM_Common_Const::_SM_PAGE_DESCRIPTION,
            'name' => SM_Common_Const::_SM_PAGE_DESCRIPTION,
            'label' => __('Description 2', 'apollo'),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apl-editor',
            'settings' => array(
                'editor_height' => 300,
            ),
            'value' => $desc
        ));

        echo '</div>';

    }
    public  function summary_meta_box( $post ) {
        $postSummaryValue = get_post_meta($post->ID,'post-summary',true);
        ?>
        <textarea class="full h-100" name="post-summary" placeholder=""><?php echo $postSummaryValue; ?></textarea>
        <div><?php echo __( 'Post summary are optional hand-crafted summaries of your content that can be used in your theme', 'apollo' ) ?></div>
        <?php
    }


    public function add_metabox($post_type)
    {
        add_meta_box("Sonoma_page_data", __('Block 2', 'apollo'), array($this, 'output_data_page'), $post_type, 'normal', 'high');
    }
    public function add_summary_metabox($post_type)
    {
        if( $post_type == 'page') {
            add_meta_box("Sonoma_page_summary", __('Summary', 'apollo'), array($this, 'summary_meta_box'), $post_type, 'normal', 'high');
        }

    }


    public  function save($post_id, $post)
    {
        if( $post->post_type == 'page') {
            $pageTitle2 = isset($_POST[SM_Common_Const::_SM_PAGE_TITLE]) ? $_POST[SM_Common_Const::_SM_PAGE_TITLE] : '';
            update_post_meta($post_id, SM_Common_Const::_SM_PAGE_TITLE, $pageTitle2);
            $pageDesc2 = isset($_POST[SM_Common_Const::_SM_PAGE_DESCRIPTION]) ? $_POST[SM_Common_Const::_SM_PAGE_DESCRIPTION] : '';
            update_post_meta($post_id, SM_Common_Const::_SM_PAGE_DESCRIPTION, $pageDesc2);
            $postSummary = isset($_POST['post-summary']) ? $_POST['post-summary'] : '';
            update_post_meta($post_id, 'post-summary', $postSummary);
        }
    }

}
new SM_Page_Admin();