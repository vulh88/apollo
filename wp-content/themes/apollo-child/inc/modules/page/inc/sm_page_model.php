<?php

/**
 * Created by PhpStorm.
 * User: truong
 * Date: 23/09/2016
 * Time: 16:51
 */
class SM_Page_Model extends SM_Mod_Model
{

	private $post_id;
	private $title;
	private $desc;
	private $src;
	private $title2;
	private $desc2;
	private $src2;
    private $summary;
    private $backgroundImage;

    /**
     * @return mixed
     */
    public function getBackgroundImage()
    {
        return $this->backgroundImage;
    }

    /**
     * @param mixed $backgroundImage
     */
    public function setBackgroundImage($backgroundImage)
    {
        $this->backgroundImage = $backgroundImage;
    }

    /**
     * @return mixed
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * @param mixed $summary
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;
    }

	public function __construct() {
		$this->getDataPage();
	}


	public function getDataPage(){
		global $post;
		$page = new Apollo_ABS_Module( $post, 'page' );

		$this->setPostId(get_the_ID());
        $summary = get_post_meta( $this->post_id, 'post-summary', true );
		$image2  = get_post_meta( $this->post_id, 'page_secondary-image_thumbnail_id', true );
		$src     = wp_get_attachment_image_src( get_post_thumbnail_id( $this->post_id ), 'full' );
		$src = $src && isset( $src[0] ) ? $src[0] : "";
		$src2    = wp_get_attachment_image_src( $image2, 'full' );
		$src2 = $src2 && isset( $src2[0] ) ? $src2[0] : "";
        $bgImage  = get_post_meta( $this->post_id, 'page_background-image_thumbnail_id', true );
        $attachmentBgImage    = wp_get_attachment_image_src( $bgImage, 'full' );
        $bgImageUrl = $attachmentBgImage && isset( $attachmentBgImage[0] ) ? $attachmentBgImage[0] : "";
        if (  is_page_template('default')){
            $bgImageUrl =  $bgImageUrl ? $bgImageUrl : SONOMA_IMAGES_URL."/demo/bg_circle.png";
        }
        $this->setSummary($summary);
		$this->setSrc($src);
		$this->setTitle($page->get_full_title());
		$this->setDesc($page->get_full_content());
		$this->setTitle2( get_post_meta( $this->post_id, SM_Common_Const::_SM_PAGE_TITLE, true ));
		$this->setDesc2( get_post_meta( $this->post_id,  SM_Common_Const::_SM_PAGE_DESCRIPTION, true ));
		$this->setSrc2( $src2);
        $this->setBackgroundImage($bgImageUrl);

	}

	public function getPostsAssociatedPage() {
        $data = apply_filters('apl_pbm_get_data_posts_associated', [
            'posts' => [],
            'common' => array(
                'post_id' => $this->post_id,
            )
        ]);
        return $data['posts'];
    }

    public function renderBlockContent() {
        $section_data = $this->getPostsAssociatedPage();
        foreach ($section_data as $section) {
            $file = SONOMA_MODULES_DIR . '/page/templates/block-content/sonoma_fire_2017/' . $section['block_name'] . '.php';
            Apollo_App::getTemplatePartCustom($file, array(
                'data' => $section,
                'post_id' => $this->post_id
            ), false);
        }
    }

    public function renderClassForGalleryItem($number)
    {
        if (is_numeric($number)) {
            switch ($number % 3):
                case 1:
                    return 'item-white-left';
                    break;
                case 2:
                    return 'item-white-center';
                    break;
                case 0:
                    return 'item-white-right';
                    break;
            endswitch;
        }
    }

    public function renderGalleryTemplatePart()
    {
        $data = $this->getCustomGalleryField();
        $file = SONOMA_MODULES_DIR . '/page/templates/gallery_part.php';
        if (!empty($data)) {
            foreach ($data as $gallery) {
                Apollo_App::getTemplatePartCustom($file, array('data' => $gallery, 'model' => $this), false);
            }
        }

    }

    public function renderGalleryItem($data)
    {

        $file = SONOMA_MODULES_DIR . '/page/templates/gallery_item_part.php';
        $count = 1;
        $result = array();
        if (!empty($data)) {
            foreach ($data as $item) {
                $result[] = Apollo_App::getTemplatePartCustom($file, array('data' => $item,
                    'number' => $count,
                    'class' => $this->renderClassForGalleryItem($count)), false);
                $count++;
            }

        }
        if (!empty($result)) {
            $result = implode(" ", $result);
        }
        return $result;
    }

	/**
	 * @return mixed
	 */
	public function getPostId() {
		return $this->post_id;
	}

	/**
	 * @param mixed $post_id
	 */
	public function setPostId( $post_id ) {
		$this->post_id = $post_id;
	}

	/**
	 * @return mixed
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @param mixed $title
	 */
	public function setTitle( $title ) {
		$this->title = $title;
	}

	/**
	 * @return mixed
	 */
	public function getDesc() {
		return $this->desc;
	}

	/**
	 * @param mixed $desc
	 */
	public function setDesc( $desc ) {
		$this->desc = $desc;
	}

	/**
	 * @return mixed
	 */
	public function getSrc() {
		return $this->src;
	}

	/**
	 * @param mixed $src
	 */
	public function setSrc( $src ) {
		$this->src = $src;
	}

	/**
	 * @return mixed
	 */
	public function getTitle2() {
		return $this->title2;
	}

	/**
	 * @param mixed $title2
	 */
	public function setTitle2( $title2 ) {
		$this->title2 = $title2;
	}

	/**
	 * @return mixed
	 */
	public function getDesc2() {
		return $this->desc2;
	}

	/**
	 * @param mixed $desc2
	 */
	public function setDesc2( $desc2 ) {
		$this->desc2 = $desc2;
	}

	/**
	 * @return mixed
	 */
	public function getSrc2() {
		return $this->src2;
	}

	/**
	 * @param mixed $src2
	 */
	public function setSrc2( $src2 ) {
		$this->src2 = $src2;
	}

}