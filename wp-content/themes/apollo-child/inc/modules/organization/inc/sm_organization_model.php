<?php

/**
 * Created by PhpStorm.
 * User: truong
 * Date: 23/09/2016
 * Time: 16:51
 */
class SM_Organization_Model extends SM_Mod_Model
{
    private $org;
    private $id;

    /**
     * @return mixed
     */
    public function getOrg()
    {
        return $this->org;
    }

    /**
     * @param mixed $org
     */
    public function setOrg($org)
    {
        $this->org = $org;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

	public function __construct() {
		parent::__construct();
        $this->setDataOrg();
	}

	public function setDataOrg(){
        $this->pagesize = Apollo_App::aplGetPageSize( SM_Common_Const::_SM_ITEMS_NUM_LISTING_PAGE, Apollo_DB_Schema::_ORGANIZATION_NUM_ITEMS_LISTING_PAGE );
        global $post;
        $this->setOrg( get_org($post) );
        $this->setId( $post->ID );
    }
    
    public function isAllowComment(){
        return of_get_option(Apollo_DB_Schema::_ENABLE_COMMENT,1) && comments_open();
    }


    public function getDataOrg(){
        $data = $this->getOrg()->getOrgData();
        return $data;
    }

    public function getListClassified(){
        include_once APOLLO_TEMPLATES_DIR.'/classified/list-classifieds.php';
        $list_classified = new List_Classified_Adapter( 1, Apollo_Display_Config::PAGESIZE_UPCOM );
        $list_classified->get_org_classified( $this->getId() );
        return $list_classified;
    }

    public function getListAudio(){
        $audioList =  $this->getOrg()->getListAudios(Apollo_DB_Schema::_APL_ORG_AUDIO);
        return $audioList;
    }

    public function renderOrgType()
    {
        $orgType = $this->getOrg()->generate_categories();
        if ($orgType) {
            echo '<div class="org-type">' . $orgType . ' </div>';
        }
    }

    public function renderTopContent(){
        $content = '';
        $currentTerm = get_query_var('term');
        $currentTerm = get_term_by('slug', $currentTerm, get_query_var( 'taxonomy' ));
        if (!empty($currentTerm)) {
            $displayPublic = get_apollo_term_meta($currentTerm->term_id, Apollo_DB_Schema::_APL_TAXONOMY_DISPLAY_PUBLIC_FIELD, true);
            $termDescription = !empty($currentTerm) ? $currentTerm->description : '';
            if ($termDescription && !empty($displayPublic)) {
                if ($displayPublic) {
                    $content = Apollo_App::the_content($termDescription);
                }
            }
        }
        return $content;
    }

    public function renderContentGeneral(){
        $file = SONOMA_MODULES_DIR. '/organization/templates/partials/general.php';
        $data = $this->smGetTemplatePartCustom($file,$this ,true);
        return $data;
    }
    public function renderSocial(){
        $file = SONOMA_MODULES_DIR. '/organization/templates/partials/social.php';
        $data = $this->smGetTemplatePartCustom($file,$this ,true);
        return $data;
    }
    public function renderListClassified(){

        $file = SONOMA_MODULES_DIR. '/organization/templates/partials/list-classified.php';
        $data = $this->smGetTemplatePartCustom($file, $this ,true);
        return $data;
    }

    public function renderAdditionalFields(){

        $file = SONOMA_MODULES_DIR. '/organization/templates/partials/add-fields.php';
        $data = $this->smGetTemplatePartCustom($file, $this ,true);
        return $data;
    }
    public function renderMedia(){

        $file = SONOMA_MODULES_DIR. '/organization/templates/partials/media.php';
        $data = $this->smGetTemplatePartCustom($file, $this ,true);
        return $data;
    }

    public function renderSlider()
    {
        $gallerys  = $this->getOrg()->getListImages();
        if(!empty($gallerys[0])){
            $data = SM_View_Helper::handleDataPhotoItems($gallerys);
            return Apollo_App::getSimpleTemplatePart(SONOMA_MODULES_DIR . '/common-templates/slider.php', $data, true);
        }
    }

    public function renderVideo()
    {
        $videos = $this->getOrg()->getListVideo();
        $videoTemp = array();
        foreach($videos as $video){
            $videoTemp[] = array(
                'embed' => $video['embed'],
                'desc' => isset($video['desc']) ? base64_decode($video['desc']) : '',
            );
        }
        $videos = $videoTemp;

        if (!empty($videos)){
            $data = SM_View_Helper::handleDataVideos($videos);
            return Apollo_App::getSimpleTemplatePart(SONOMA_MODULES_DIR . '/common-templates/video.php', $data, true);

        }
    }

    public function renderListAudios(){
        $audioList =  $this->getListAudio();
        if ( !empty($audioList)){
            $file =  SONOMA_MODULES_DIR. '/common-templates/audio-list.php';
            $data = $this->smGetTemplatePartCustom($file, $this ,true);
            return $data;
        }
        return '';

    }

    public function renderAudioItem(){
        $result = array();
        $audioList =  $this->getListAudio();
        $file =  SONOMA_MODULES_DIR. '/organization/templates/partials/audio-item.php';
        if ( !empty($audioList)) {
            foreach($audioList as $item):
                $result[] = $this->smGetTemplatePartCustom($file,$item,true);

            endforeach;
        }
        if (!empty($result)) {
            $result = implode(" ", $result);
        }
        return $result;
    }


    public function render_html()
    {
        $data = $this->smGetTemplatePartCustom($this->template,$this,true);
        return $data;
    }
    
    public function search() {
        $arr_params = array(
            'post_type'         => Apollo_DB_Schema::_ORGANIZATION_PT,
            'posts_per_page'    => $this->pagesize,
            'paged'             => $this->page,
            'post_status'       => array('publish'),

        );

        if($keyword = self::getKeyword()) {
            $arr_params['s'] =  $keyword;
        }

        if(isset($_GET['term']) && !empty($_GET['term'])) {
            $arr_tax_query[] = array(
                'taxonomy'=> Apollo_DB_Schema::_ORGANIZATION_PT.'-type',
                'terms' => array($_GET['term']),
            );
            $arr_params['tax_query'] = $arr_tax_query;
        }

        // Set new offset if this is ajax action
        $this->addOffsetToParams($arr_params);

        //No need search in this time
        add_filter('posts_where', array(__CLASS__, 'filter_where_org_tbl'), 10, 1);
        add_filter('posts_join', array(__CLASS__, 'filter_join_org_tbl'), 10, 1);
        add_filter('posts_orderby', array(__CLASS__, 'filter_order_org_tbl'), 10, 1);
        add_filter('posts_search', array(__CLASS__, 'posts_search'), 10, 1);
        add_filter('posts_groupby', array($this, 'filter_groupby'), 10, 1);

        $this->result = query_posts($arr_params);
        Apollo_Next_Prev::updateSearchResult($GLOBALS['wp_query']->request,Apollo_DB_Schema::_ORGANIZATION_PT);
        remove_filter('posts_orderby', array(__CLASS__, 'filter_order_org_tbl'), 10);
        remove_filter('posts_join', array(__CLASS__, 'filter_join_org_tbl'), 10);
        remove_filter('posts_where', array(__CLASS__, 'filter_where_org_tbl'), 10);
        remove_filter('posts_search', array(__CLASS__, 'posts_search'), 10);
        remove_filter('posts_groupby', array($this, 'filter_groupby'), 10);

        $this->total_pages = ceil($this->total = $GLOBALS['wp_query']->found_posts / $this->pagesize);

        $this->total = $this->total = $GLOBALS['wp_query']->found_posts;

        $this->template = SONOMA_MODULES_DIR. '/organization/templates/'. $this->_get_template_name(of_get_option(Apollo_DB_Schema::_ORGANIZATION_DEFAULT_VIEW_TYPE,SONOMA_DIRECTORY_LISTING_DEFAULT_VIEW_TYPE));

        $this->resetPostData();
    }

    public static function filter_where_org_tbl ($where) {
        global $wpdb;
        $sqlString = '';
        $enableRegionSelection = Apollo_App::enableMappingRegionZipSelection();

        //query by city
        /**
         *  Because city input by user, they can input special character.
         *  We use id for param to search correct
         */
        if (isset($_GET['city']) && !empty($_GET['city'])) {
            $city = $_GET['city'];
            $sqlString .= 'AND  ' . $wpdb->posts . '.ID IN (
                    SELECT em.apollo_organization_id
                    FROM ' . $wpdb->{Apollo_Tables::_APL_ORG_META} . ' em
                    WHERE em.apollo_organization_id  = ' . $wpdb->posts . '.ID
                        AND em.meta_value  REGEXP BINARY  \'.*"_org_city";s:[0-9]+:"' . $city . '".*\'
                        AND em.meta_key = "' . Apollo_DB_Schema::_APL_ORG_ADDRESS . '"
                )';

        }

        if(isset($_GET['region']) && !empty($_GET['region'])) {
            $region = urldecode($_GET['region']);
            $wpdb->escape_by_ref($region);

            /**
             * Get event ids in zipcodes region selection
             */
            if ($enableRegionSelection) {

                if (!$zipcodes = Apollo_Seach_Form_Data::getRegionZipcodes($region)) {
                    $zipcodes = array(false);
                }

                $zipQuery = '"'. implode('","', $zipcodes). '"';
                $sqlString .= 'AND  '.$wpdb->posts.'.ID IN (
                    SELECT em.apollo_organization_id
                    FROM '.$wpdb->{Apollo_Tables::_APL_ORG_META}.' em
                    WHERE em.apollo_organization_id  = '.$wpdb->posts.'.ID
                        AND em.meta_value  IN('.$zipQuery.')
                        AND em.meta_key = "'.Apollo_DB_Schema::_ORG_ZIP.'"
                )';
            }
            else {
                $sqlString .= 'AND  ' . $wpdb->posts . '.ID IN (
                    SELECT em.apollo_organization_id
                    FROM ' . $wpdb->{Apollo_Tables::_APL_ORG_META} . ' em
                    WHERE em.apollo_organization_id  = ' . $wpdb->posts . '.ID
                        AND em.meta_value  REGEXP BINARY  \'.*"_org_region";s:[0-9]+:"' . $region . '".*\'
                        AND em.meta_key = "' . Apollo_DB_Schema::_APL_ORG_ADDRESS . '"
                )';
            }
        }


        //query by State
        if(isset($_GET['state']) && !empty($_GET['state'])) {
            $state = $_GET['state'];
            $sqlString .= 'AND  '.$wpdb->posts.'.ID IN (
                SELECT em.apollo_organization_id
                FROM '.$wpdb->{Apollo_Tables::_APL_ORG_META}.' em
                WHERE em.apollo_organization_id  = '.$wpdb->posts.'.ID
                    AND em.meta_value LIKE \'%'.$state.'%\'
                    AND em.meta_key = "'.Apollo_DB_Schema::_APL_ORG_ADDRESS.'"
            )';
        }

        //query by zip
        if (!$enableRegionSelection && isset($_GET['zip']) && !empty($_GET['zip'])) {
            $zip = $_GET['zip'];
            $sqlString .= 'AND  ' . $wpdb->posts . '.ID IN (
                    SELECT em.apollo_organization_id
                    FROM ' . $wpdb->{Apollo_Tables::_APL_ORG_META} . ' em
                    WHERE em.apollo_organization_id  = ' . $wpdb->posts . '.ID
                        AND em.meta_value  REGEXP BINARY  \'.*"_org_zip";s:[0-9]+:"' . $zip . '".*\'
                        AND em.meta_key = "' . Apollo_DB_Schema::_APL_ORG_ADDRESS . '"
                )';

        }

        /*Handle logic here */
        /**
         * TruongHN : remove  the organization, which has checked 'do not display'
         * checkbox from Organization Directory
         */
        $isTermPage = isset($_GET['term']);
        $isSearchPage = isset($_GET['s']);
        $isSearchListingPage = isset($_GET['keyword']);
        $isListPage = ( (!$isTermPage && !$isSearchPage ) || $isSearchListingPage );
        if($isListPage){
            $sqlString .= 'AND  ' . $wpdb->posts . '.ID NOT IN (
                    SELECT em.apollo_organization_id
                    FROM ' . $wpdb->{Apollo_Tables::_APL_ORG_META} . ' em
                    WHERE em.apollo_organization_id  = ' . $wpdb->posts . '.ID
                       AND em.meta_key = "' . Apollo_DB_Schema::_ORG_DO_NOT_DISPLAY . '"
                       AND em.meta_value = "yes"
                )';

        }

        return $where . $sqlString;
    }

    public static function filter_join_org_tbl ($join) {
        $join .= self::getJoinTaxByKeyword();
        return $join;
    }

    /* last name is not query with city in same query: Good to know that. because we don't need join an extra query to sort by last_name  */
    public static function filter_order_org_tbl () {
        global $wpdb;
        $order= $wpdb->posts.'.post_title ASC';
        return $order;
    }
}