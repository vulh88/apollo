
<div class="art-social">
    <div class="el-blk">
        <?php
        if(isset($model->getDataOrg()['email']) && !empty($model->getDataOrg()['email'])){
        ?>
        <div class="art-social-item"><i class="fa fa-envelope fa-lg">&nbsp;&nbsp;&nbsp;</i><a href="mailto:<?php echo $model->getDataOrg()['email'] ?>">
                <?php  _e('Email','apollo') ?></a>
            <div class="slash">/</div>
        </div>
        <?php } ?>

        <?php
        if(isset($model->getDataOrg()['web']) && !empty($model->getDataOrg()['web'])){
        ?>
        <div class="art-social-item"><i class="fa fa-link fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $model->getDataOrg()['web'] ?>">
                <?php _e('Website','apollo') ?></a>
            <div class="slash">/</div>
        </div>
        <?php } ?>

        <?php
        if(isset($model->getDataOrg()['blog']) && !empty($model->getDataOrg()['blog'])){
        ?>
        <div class="art-social-item"><i class="fa fa-star fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $model->getDataOrg()['blog'] ?>">
                <?php _e('Blog','apollo') ?></a>
            <div class="slash">/</div>
        </div>
        <?php } ?>


        <?php
        if(isset($model->getDataOrg()['facebook']) && !empty($model->getDataOrg()['facebook'])){
        ?>
        <div class="art-social-item"><i class="fa fa-facebook fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $model->getDataOrg()['facebook'] ?>">
                <?php _e('Facebook','apollo') ?></a>
            <div class="slash">/</div>
        </div>
        <?php } ?>

        <?php
        if(isset($model->getDataOrg()['twitter']) && !empty($model->getDataOrg()['twitter'])){
        ?>
        <div class="art-social-item"><i class="fa fa-twitter fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $model->getDataOrg()['twitter'] ?>">
                <?php _e('Twitter','apollo') ?></a>
            <div class="slash">/</div>
        </div>
        <?php } ?>

        <?php
        if(isset($model->getDataOrg()['inst']) && !empty($model->getDataOrg()['inst'])){
        ?>
        <div class="art-social-item"><i class="fa fa-camera-retro fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $model->getDataOrg()['inst'] ?>">
                <?php _e('Instagram','apollo') ?>  </a>
            <div class="slash">/</div>
        </div>
        <?php } ?>

        <?php
        if(isset($model->getDataOrg()['linked']) && !empty($model->getDataOrg()['linked'])){
        ?>
        <div class="art-social-item"><i class="fa fa-linkedin-square fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $model->getDataOrg()['linked'] ?>">
                <?php _e('LinkedIn','apollo') ?>   </a>
                <div class="slash">/</div>
        </div>
        <?php } ?>

        <?php
        if(isset($model->getDataOrg()['pinterest']) && !empty($model->getDataOrg()['pinterest'])){
            ?>
            <div class="art-social-item"><i class="fa fa-pinterest fa-lg">&nbsp;&nbsp;&nbsp;</i><a href="<?php echo $model->getDataOrg()['pinterest'] ?>">
                    <?php _e('Pinterest','apollo') ?>   </a>
                    <div class="slash">/</div>
            </div>
        <?php } ?>

        <?php
        if(isset($model->getDataOrg()['donate']) && !empty($model->getDataOrg()['donate'])){
            ?>
            <div class="art-social-item"><i class="fa fa-usd fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $model->getDataOrg()['donate'] ?>">
                    <?php _e('Donate','apollo') ?>   </a>
            </div>
        <?php } ?>
    </div>
    <div class="el-blk location">
        <?php
            if(isset($model->getDataOrg()['address']) && !empty($model->getDataOrg()['address'])){
        ?>
        <p><span><i class="fa fa-map-marker fa-lg">&nbsp;&nbsp;&nbsp;</i></span>
            <label><?php echo $model->getDataOrg()['address']; ?></label>
        </p>
        <?php } ?>
        <?php
        if(isset($model->getDataOrg()['phone']) && !empty($model->getDataOrg()['phone'])){
        ?>
        <p><span><i class="fa fa-phone fa-lg">&nbsp;&nbsp;</i></span>
            <label><?php echo $model->getDataOrg()['phone']; ?></label>
        </p>
        <?php } ?>
        <?php
        if(isset($model->getDataOrg()['fax']) && !empty($model->getDataOrg()['fax'])){
        ?>
        <p><span><i class="fa fa-fax fa-lg">&nbsp;</i></span>
            <label><?php echo $model->getDataOrg()['fax']; ?></label>
        </p>
        <?php } ?>
    </div>
    <div class="el-blk">
        <div class="b-btn fl">
            <?php echo $model->renderBookmarkBtn( array( 'class' => 'btn btn-b' ),get_post($model->getId()) ); ?>
        </div>
    </div>
</div>