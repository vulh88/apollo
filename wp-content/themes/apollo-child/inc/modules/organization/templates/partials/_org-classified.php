       
<?php 
    if ( $this->classified ):
    
    foreach ( $this->classified as $class ): 
    
    if ( is_int( $class ) ) {
        $class = get_classified( get_post( $class ) );
    } else {
        $class = get_classified( $class );
    }

    $expired_date =  $class->{Apollo_DB_Schema::_APL_CLASSIFIED_EXP_DATE};
?>
<div class="more-frm-itm">
    <div class="more-pic">
        <a href="<?php echo $class->get_permalink(); ?>">
            <?php echo $class->get_image( 'thumbnail' ); ?>
        </a>    
    </div>
    <div class="more-ct custom">
        <h3> <a href="<?php echo $class->get_permalink(); ?>"><?php echo $class->get_title(); ?></a></h3>
        <p class="p-date">
            <?php echo APL::dateUnionDateShort($expired_date); ?>
        </p>
        <?php if( of_get_option(Apollo_DB_Schema::_ENABLE_THUMBS_UP,1)) : ?>
        <div class="rating-box rating-action">
            <div class="like parent-not-underline-hover" data-ride="ap-event_rating" id="_event_rating_<?php echo $class->id.'_'.Apollo_DB_Schema::_CLASSIFIED ?>" data-i="<?php echo $class->id ?>" data-t="<?php echo Apollo_DB_Schema::_CLASSIFIED ?>"> <a href="javascript:void(0);" ><span class="_count"></span></a></div>
        </div>
        <?php endif ?>
    </div>

    
    <div  class="b-btn custom">
        <?php echo SM_Mod_Model::renderBookmarkBtn( array( 'class' => 'btn-bm btn btn-b' ), get_post($class->id) ); ?>
    </div>
</div>

<?php endforeach; ?>
                                

<?php endif; ?>