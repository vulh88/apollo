<div class="content-wrapper content-wrapper--master-layout">
    <div class="block block-events block-events--org clearfix">
        <div class="breadcrumbs">
            <ul class="nav">
                <li><a href="<?php echo get_home_url(get_current_blog_id()) ?>"><?php _e('Home', 'apollo') ?></a></li>

                <?php if (isset($_GET['keyword']) || isset($_GET['city'])
                    || isset($_GET['state']) || isset($_GET['zip'])
                ) { ?>
                    <li><a href="<?php echo home_url() ?>/organization"><?php _e('Organization', 'apollo') ?></a></li>
                    <li><span><?php _e('Search', 'apollo') ?></span></li>
                <?php } else { ?>
                    <li><span><?php _e('Organization', 'apollo') ?></span></li>
                <?php } ?>
            </ul>
            <div class="social-share">
                <div class="b-share-cat art">
                    <?php
                    echo $model->getSharingInfo();
                    ?>
                </div>
            </div>
        </div>

        <?php
        if (!Apollo_App::is_avaiable_module(Apollo_DB_Schema::_ORGANIZATION_PT)) {
            wp_safe_redirect('/');
        }

        global $post;
        $current_url = home_url() . $_SERVER["REQUEST_URI"];

        $model->set_main_post($post);
        $model->search();
        ?>

        <?php if (isset($_GET['keyword'])): ?>
            <div class="search-tt mar-b-0">
                <?php $model->render_result_title() ?>
            </div>
        <?php endif; ?>

        <?php if ($model->get_results()): ?>

                <div class="search-bkl clearfix">
                    <?php
                    $pag_html = '';
                    ?>
            <?php if ($model->have_pag()):?>
                    <div class="blk-paging">
                        <?php
                        $pag_html = $model->render_pagination();
                        echo $pag_html;
                        ?>
                    </div>
            <?php endif; ?>
                    <div class="view-mode">
                        <nav class="type">
                            <ul>
                                <li <?php echo !$model->is_list_page(of_get_option(Apollo_DB_Schema::_ORGANIZATION_DEFAULT_VIEW_TYPE)) ? 'class="current"' : '' ?> >
                                    <a href="<?php echo $model->get_template_btn_url() ?>"><i
                                            class="fa fa-th fa-2x"></i></a></li>
                                <li <?php echo $model->is_list_page(of_get_option(Apollo_DB_Schema::_ORGANIZATION_DEFAULT_VIEW_TYPE)) ? 'class="current"' : '' ?>>
                                    <a href="<?php echo $model->get_template_btn_url('list') ?>"><i
                                            class="fa fa-bars fa-2x"></i></a></li>
                            </ul>
                        </nav>
                    </div>
                </div>

            <?php
            $page = get_post(Apollo_App::getOrgHeaderPage());
            if (!empty($page)) { ?>
                <div class="sub-content-blk custom-block rich-txt">
                    <?php echo Apollo_App::the_content($page->post_content); ?>
                </div>
            <?php } ?>

            <div class="sub-content-blk custom-block <?php echo $model->renderTopContent() ? 'padding-10px' : '' ?>">
                <?php echo $model->renderTopContent() ?>
            </div>

        <?php endif; ?>

        <?php
        if ($model->is_list_page(of_get_option(Apollo_DB_Schema::_ORGANIZATION_DEFAULT_VIEW_TYPE))) {
            $container = $class = "search-artist-list";
        } else {
            $container = $class = 'article-circle search-artist-thumb';
        }
        ?>
        <div class="article <?php echo $class; ?>">
            <?php echo $model->render_html(); ?>
        </div>

        <?php
        $is_view_more = $model->have_more() && $model->is_onepage();
        ?>
        <input name="search-have-more" value="<?php echo $model->have_more() ?>" type="hidden">

        <?php if (!$model->is_map_page()):
            $query_string = $model->get_current_query_string();
            $query_string = add_query_arg(array(
                'page' => 2,
            ), $query_string);

            $query_string = ltrim($query_string, '?');
            ?>
            <div <?php if (!$is_view_more) echo 'style="display: none"'; ?> id="load-more" class="load-more b-btn load-more-search load-more-artist-search-page">
                <a href="javascript:void(0);"
                   data-ride="ap-more"
                   data-holder=".<?php echo $container ?>>:last"
                   data-sourcetype="url"
                   data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_get_more_search_org&' . $query_string) ?>"
                   data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
                   data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
                   class="btn-b arw"><?php _e('SHOW MORE', 'apollo') ?>
                </a>
            </div>
        <?php endif; ?>

        <div class="search-bkl">
            <?php if ($model->have_pag()): ?>
                <div class="blk-paging end-pager">
                    <?php echo $pag_html; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>