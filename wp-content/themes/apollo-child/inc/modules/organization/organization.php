<?php

/**
 * Class SM_Organization_Mod
 */

require_once __DIR__.'/inc/sm_organization_model.php';
class SM_Organization_Mod extends SM_Mod_Abstract
{
    public function __construct(){

        $this->renderPageTemplate();
    }

    public function renderPageTemplate(){
        $smOrgModel = new SM_Organization_Model();
        $file = SONOMA_MODULES_DIR.'/organization/templates/organization.php';
        if (is_single()){
            $file = SONOMA_MODULES_DIR.'/organization/templates/org-detail.php';
        }
        $fileTemplatePart = SONOMA_MODULES_DIR.'/organization/templates/organization-top.php';
        if ( file_exists($fileTemplatePart) ){
            $this->smGetTemplatePartCustom($fileTemplatePart,$smOrgModel);
        }
        if( file_exists($file) ){
            $this->smGetTemplatePartCustom($file,$smOrgModel);
        }

    }

}

new SM_Organization_Mod();