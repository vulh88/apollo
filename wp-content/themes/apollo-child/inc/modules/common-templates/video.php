<h4><?php _e('VIDEOS', 'apollo') ?><span> <i class="fa fa-film"></i></span></h4>
<div class="a-block-ct-video">
    <?php echo $model['video_iframe']; ?>
    <div class="blank"></div>
    <ul class="video-description">
        <li>
            <p><?php echo nl2br($model["first_desc_video"]) ?></p>
        </li>
    </ul>
    <div id="video-pager">
        <?php echo $model["thumb"] ?>
    </div>
    <div class="vd-loader"><a><i class="fa fa-spinner fa-spin fa-3x"></i> </a></div>
</div>
<input type="hidden" name="total-videos" value="<?php echo $model["total"] ?>"/>