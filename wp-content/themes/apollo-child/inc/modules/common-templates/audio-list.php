
<h4><?php _e('AUDIO','apollo') ?><span> <i class="fa fa-file-audio-o"></i></span></h4>
<div class="a-block-ct">
    <ul class="audio-list">
        <?php
        echo $model->renderAudioItem();
         ?>
    </ul>
</div>
