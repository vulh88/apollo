<?php

/**
 * Class SM_Classified_Mod
 */
require_once __DIR__ . '/inc/sm_classified_model.php';

class SM_Classified_Mod extends SM_Mod_Abstract
{
    public function __construct()
    {
        $this->renderPageTemplate();
    }

    public function renderPageTemplate()
    {
       $smClassifiedModel = new SM_Classified_Model();        
       $file = SONOMA_MODULES_DIR . '/classified/templates/classified.php';        
       if (is_single()) {            
           $file = SONOMA_MODULES_DIR . '/classified/templates/classified-detail.php';        
           }        
           $fileTemplatePart = SONOMA_MODULES_DIR . '/classified/templates/classified-top.php';        
           if (file_exists($fileTemplatePart)) {            
               $this->smGetTemplatePartCustom($fileTemplatePart, $smClassifiedModel);        
               }        if (file_exists($file)) {           
                    $this->smGetTemplatePartCustom($file, $smClassifiedModel);        
                    }

    }

}


new SM_Classified_Mod();