<div class="blk-list-table classified-column">
    <div class="table-info">
        <?php
        if ($results = $this->get_results()) { ?>
            <table class="table">
                <thead>
                <?php
                $operator = isset($_REQUEST['keyword']) || isset($_REQUEST['term']) || isset($_REQUEST['view']) ? $model->getCurFullUrl() . '&' : '?';
                $order = isset($_REQUEST['order']) && $_REQUEST['order'] == 'asc' ? 'desc' : 'asc';
                $ascActiveClass = isset($_REQUEST['order']) && $_REQUEST['order'] == 'asc' ? 'sort-active' : '';
                $descActiveClass = isset($_REQUEST['order']) && $_REQUEST['order'] == 'desc' ? 'sort-active' : '';
                ?>
                <tr class="bg-gray">
                    <th>
                        <a href="<?php echo $operator ?>orderby=post_date&order=<?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] == 'post_date' ? $order : 'asc' ?>"><?php echo __('posted',
                                'apollo'); ?></a>
                        <a href="<?php echo $operator ?>orderby=post_date&order=asc"
                           class="icon-sort ic-sort-up <?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] == 'post_date' ? $ascActiveClass : '' ?>"><i
                                class="fa fa-caret-up"></i></a>
                        <a href="<?php echo $operator ?>orderby=post_date&order=desc"
                           class="icon-sort ic-sort-down <?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] == 'post_date' ? $descActiveClass : '' ?>"><i
                                class="fa fa-caret-down"></i></a>
                    </th>

                    <th id="column-title"><a
                            href="<?php echo $operator ?>orderby=post_title&order=<?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] == 'post_title' ? $order : 'asc' ?>"><?php echo __('title',
                                'apollo'); ?></a>
                        <a href="<?php echo $operator ?>orderby=post_title&order=asc"
                           class="icon-sort ic-sort-up <?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] == 'post_title' ? $ascActiveClass : '' ?>"><i
                                class="fa fa-caret-up"></i></a>
                        <a href="<?php echo $operator ?>orderby=post_title&order=desc"
                           class="icon-sort ic-sort-down <?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] == 'post_title' ? $descActiveClass : '' ?>"><i
                                class="fa fa-caret-down"></i></a>
                    </th>
                    <th id="column-org">
                        <a href="<?php echo $operator ?>orderby=organization&order=<?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] == 'organization' ? $order : 'asc' ?>"><?php echo __('organization',
                                'apollo'); ?></a>
                        <a href="<?php echo $operator ?>orderby=organization&order=asc"
                           class="icon-sort ic-sort-up <?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] == 'organization' ? $ascActiveClass : '' ?>"><i
                                class="fa fa-caret-up"></i></a>
                        <a href="<?php echo $operator ?>orderby=organization&order=desc"
                           class="icon-sort ic-sort-down <?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] == 'organization' ? $descActiveClass : '' ?>"><i
                                class="fa fa-caret-down"></i></a>
                    </th>
                    <th id="city-thead">
                        <a href="<?php echo $operator ?>orderby=city&order=<?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] == 'city' ? $order : 'asc' ?>"><?php echo __('city',
                                'apollo'); ?></a>
                        <a href="<?php echo $operator ?>orderby=city&order=asc"
                           class="icon-sort ic-sort-up <?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] == 'city' ? $ascActiveClass : '' ?>"><i
                                class="fa fa-caret-up"></i></a>
                        <a href="<?php echo $operator ?>orderby=city&order=desc"
                           class="icon-sort ic-sort-down <?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] == 'city' ? $descActiveClass : '' ?>"><i
                                class="fa fa-caret-down"></i></a>
                    <th id="deadline-thead">
                        <a href="<?php echo $operator ?>orderby=deadline&order=<?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] == 'deadline' ? $order : 'asc' ?>"><?php echo __('deadline',
                                'apollo'); ?></a>
                        <a href="<?php echo $operator ?>orderby=deadline&order=asc"
                           class="icon-sort ic-sort-up <?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] == 'deadline' ? $ascActiveClass : '' ?>"><i
                                class="fa fa-caret-up"></i></a>
                        <a href="<?php echo $operator ?>orderby=deadline&order=desc"
                           class="icon-sort ic-sort-down <?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] == 'deadline' ? $descActiveClass : '' ?>"><i
                                class="fa fa-caret-down"></i></a>
                </tr>
                </thead>
                <tbody class="classified-table-view">
                <?php


                foreach ($results as $p):

                    $e = get_classified($p);
                    $data = $e->getDataDetail();
                    $deadlineDate = $data['deadline_date'] ? date('m/d/Y', strtotime($data['deadline_date'])) : '';
                    ?>
                    <tr>
                        <td><?php echo date('m/d/Y', strtotime($p->post_date)) ?></td>
                        <td id="td-title"><a
                                href="<?php echo $e->get_permalink() ?>"><?php echo $e->get_full_title() ?></a>
                        </td>
                        <td><?php echo $e->classified_org_links(); ?></td>
                        <td><?php echo $data['city'] ?> </td>
                        <td><?php echo $deadlineDate; ?></td>
                    </tr>
                    <?php
                endforeach; ?>
                </tbody>
            </table>
        <?php } else { ?>
            <br/>
            <?php _e('No results', 'apollo'); ?>
        <?php }
        ?>
    </div>
</div>
<style type="text/css">
    .table {
        width: 1000px !important;
        margin: 0 auto;
        border-collapse: collapse;
    }

    .table-info {
        overflow-x: auto;
        overflow-y: hidden;
    }

    .table-bordered > tbody > tr > td,
    .table-bordered > tbody > tr > th,
    .table-bordered > tfoot > tr > td,
    .table-bordered > tfoot > tr > th,
    .table-bordered > thead > tr > td,
    .table-bordered > thead > tr > th {
        border: 1px solid #ddd;
    }

    th#deadline-thead {
        width: 10%;
    }

    th#city-thead {
        width: 13%;
    }

    th#column-title {
        width: 15%;
    }
    @media (max-width: 767px) {
        /*
        * If on table column have new item on thead
        * Must be follows add new item on thead bellow
        */
        .classified-column .table-info .table tbody > tr > td:nth-of-type(1):before {
            content: '<?php _e('Posted', 'apollo'); ?>';
        }
        .classified-column .table-info .table tbody > tr > td:nth-of-type(2):before {
            content: '<?php _e('Title', 'apollo'); ?>';
        }
        .classified-column .table-info .table tbody > tr > td:nth-of-type(3):before {
            content: '<?php _e('Organization', 'apollo'); ?>';
        }
        .classified-column .table-info .table tbody > tr > td:nth-of-type(4):before {
            content: '<?php _e('City', 'apollo'); ?>';
        }
        .classified-column .table-info .table tbody > tr > td:nth-of-type(5):before {
            content: '<?php _e('Deadline', 'apollo'); ?>';
        }
    }
</style>