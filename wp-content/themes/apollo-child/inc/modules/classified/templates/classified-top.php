<?php

$classifiedCategories = Apollo_Org::get_tree_event_style(Apollo_DB_Schema::_CLASSIFIED);

$searchData = array(
    'keyword' => isset($_GET['keyword'])?Apollo_App::clean_data_request($_GET['keyword']):'',
    'term' => isset($_GET['term'])?Apollo_App::clean_data_request($_GET['term']):'',
    'state' => isset($_GET['state'])?$_GET['state'] : of_get_option(Apollo_DB_Schema::_APL_DEFAULT_STATE,''),
    'city' => isset($_GET['city'])? urldecode($_GET['city']) : of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY,''),
    'region' => isset($_GET['region'])?Apollo_App::clean_data_request($_GET['region']):'',
);

$cityList = Apollo_App::getCityByTerritory(false, false, true);
$stateList = Apollo_App::getStateByTerritory();
$listCityState = Apollo_App::getCityStateByTerritory();
$regionList = Apollo_App::get_regions();
$isTopSearchMobileVersion = isset($_GET['top-search-mobile']) && !empty($_GET['top-search-mobile']) ? 'ts-mobile' : '';
$defaultTermValue = $model->getDefaultValueForTerm( Apollo_DB_Schema::_CLASSIFIED.'-type', $searchData['term']);

?>
<div class="top-search organization-s-t-m <?php echo $isTopSearchMobileVersion; ?>">
    <div class="inner">
        <div class="top-search-row">
            <form method="get" id="search-classified-m-t" action="<?php echo $model->getCustomUrlBySlug(); ?>" class="form-event">
                <div class="search-row">
                    <div class="el-blk search-input-wrapper">           
                        <button type="button" class="btn btn-l s"><i class="fa fa-search fa-flip-horizontal fa-lg"></i></button>
                        <input type="text" name="keyword" value="<?php echo Apollo_App::clean_data_request(get_query_var('keyword'), true) ?>" placeholder="<?php _e('Search by Keyword', 'apollo') ?>" class="inp inp-txt event-search event-search-custom">
                        <input type="hidden" name="do_search" value="<?php echo isset($_GET['do_search']) && !empty($_GET['do_search']) ? 'yes' : 'no'; ?>" />
                    </div>
                    <!--Categories -->
                    <div class="el-blk">
                        <div class="select-bkl">
                            <select name="term" class="chosen" id="classified-type-select" >
                                <option value=""><?php _e('Classified Type','apollo') ?></option>
                                <?php
                                Apollo_Org::build_option_tree($classifiedCategories, $defaultTermValue);
                                ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>

                    <!--State -->
                    <div class="el-blk">
                        <div class="select-bkl">
                            <select class="chosen" name="state" id="classified-state-select">
                                <?php foreach($stateList as $k => $val){
                                    $selected = '';
                                    if($k == $searchData['state'])
                                        $selected = 'selected';
                                    ?>
                                    <option <?php echo $selected; ?> value="<?php echo $k ?>"><?php echo $val ?></option>
                                <?php } ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>

                    <!--City -->
                    <div class="el-blk">
                        <div class="select-bkl">
                            <select name="city" class="chosen" id="classified-city-select">
                                <?php foreach($listCityState as $k => $cityItem): ?>

                                    <optgroup <?php echo $k ? 'label="'.$k.'"' : '' ?>>

                                        <?php if (!$k): ?>
                                            <option value="0"><?php echo $cityItem; ?></option>
                                        <?php endif; ?>

                                        <?php if ($cityItem && is_array($cityItem)):
                                            foreach ( $cityItem as $city ):
                                                $selected = '';
                                                if( $city ==  $searchData['city'])
                                                    $selected = 'selected';
                                                ?>
                                                <option <?php echo $selected ?> value="<?php echo  $city ?>"><?php echo $city ?></option>
                                            <?php endforeach; endif; ?>
                                    </optgroup>
                                <?php endforeach; ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>

                    <!-- Author: thienld - add filter region via task #8158 -->
                    <?php if($regionList ): ?>
                        <div class="el-blk">
                            <div class="select-bkl">
                                <select class="chosen" name="region" data-refer-to="select[data-role=zip]" data-role="region" >
                                    <?php foreach($regionList as $k => $val){
                                        $selected = '';
                                        if($k == $searchData['region'])
                                            $selected = 'selected';
                                        if(!empty($val)) {
                                            ?>
                                            <option <?php echo $selected; ?> value="<?php echo $k ?>"><?php echo $val ?></option>
                                        <?php } } ?>
                                </select>
                                <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="sub-blk">
                        <button type="submit" class="btn btn-l lgr fr f-w-btn-search"><?php _e('SEARCH', 'apollo') ?></button>
                    </div>
                </div> 
            </form>
        </div>
    </div>
</div>