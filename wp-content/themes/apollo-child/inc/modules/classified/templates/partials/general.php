<div class="blog-bkl organization clearfix">
    <img src="<?php echo SONOMA_FRONTEND_ASSETS_URI;?>/images/demo/creative-sonoma-logo.png" class="org-logo-smn" alt="Creative Sonoma" title="Creative Sonoma" />
    <h1 class="p-ttl"><?php echo $model->getClassified()->get_title() ?></h1>
    <div class="meta auth classified">
        <?php
            $postedByVal = $model->getClassified()->renderOrgHtml();
            echo !empty($postedByVal) ? $postedByVal . ' ;  ' : '';
        ?>
        <span><?php _e('Posted on','apollo'); ?>&nbsp;</i></span>
        <label><?php echo APL::dateUnionDateShort($model->getClassified()->get_post_data()->post_date); ?></label>
    </div>
    <?php $model->getClassified()->renderIcons($this->getData()['icon_fields'], $this->getData()['icons']); ?>

    <!--Org type -->
    <?php $categories = $model->getClassified()->generate_categories(); ?>
    <div class="org-type">
        <?php
            if ( $categories ) {
                echo $categories;
            }
            if( !empty($deadlineDate) ) {
                echo $categories ? ' - ' : '';
        ?>
            <span><?php _e('DEADLINE : ','apollo'); ?>&nbsp;</i></span>
            <label><?php echo APL::dateUnionDateShort($this->getData()['deadline_date']); ?></label>
        <?php }
        ?>
    </div>

    <div class="rating-box rating-action rating-art">
        <?php if (of_get_option(Apollo_DB_Schema::_ENABLE_THUMBS_UP, 1)) : ?>
            <div class="like"
                 id="_event_rating_<?php echo $model->getId() . '_' . Apollo_DB_Schema::_CLASSIFIED ?>"
                 data-ride="ap-event_rating" data-i="<?php echo $model->getId() ?>"
                 data-t="<?php echo Apollo_DB_Schema::_CLASSIFIED ?>">
                <a href="javascript:void(0);"><span class="_count">&nbsp;</span></a></div>
        <?php endif ?>
        <?php if ($model->isAllowComment()): ?>
            <div class="cm-comment"><a href="#event_comment_block" data-uk-smooth-scroll="{offset: 121}"><?php _e('Comment', 'apollo') ?></a></div>
        <?php endif; ?>
    </div>
    <div class="el-blk clearfix">
        <div class="art-pic">
            <?php
                if ( of_get_option(Apollo_DB_Schema::_CLASSIFIED_ENABLE_PRIMARY_IMAGE, 1) ) {
                    echo $model->getClassified()->get_image('medium');
                }
            ?>
        </div>
        <?php echo $model->renderSocial(); ?>
    </div>
    <div class="el-blk clearfix">
        <div class="art-desc apl-internal-content rich-txt rich-txt--hight-first-child">
            <?php echo $model->getClassified()->get_full_content(); ?>
        </div>
    </div>
</div>

<?php echo $model->renderPackingInfo(); ?>
