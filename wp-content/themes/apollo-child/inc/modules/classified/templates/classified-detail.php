<div class="content-wrapper content-wrapper--master-layout">
    <div class="block block-details-content">
        <div class="breadcrumbs">
            <ul class="nav">
                <ul class="nav">
                    <li><a href="<?php echo home_url() ?>"><?php _e('Home', 'apollo') ?></a></li>
                    <li><a href="<?php echo $model->getCustomUrlBySlug(); ?>"><?php $model->getCustomLabel(); ?></a></li>
                    <li><span><?php echo $model->getClassified()->get_title() ?></span></li>
                </ul>
            </ul>
            <div class="search-bkl social-share">
                <div class="b-share-cat art">
                    <?php $model->getSharingInfo(['print', 'sendmail'], $model->getId()); ?>
                    <input name="eventid" value="<?php echo $model->getId(); ?>" type="hidden"/>
                </div>
            </div>
        </div>
        <div class="detail-conten-body">
            <input name="eventid" value="<?php echo $model->getId() ?>" type="hidden">
            <?php
                Apollo_Next_Prev::getNavLink($model->getId(), Apollo_DB_Schema::_CLASSIFIED);
                echo $model->renderContentGeneral();
                echo $model->renderAdditionalFields();
                echo $model->renderMedia();
            ?>
            <div class="blog-bkl astro-featr">
                <div class="a-block" id="event_comment_block">
                    <?php if ($model->isAllowComment()) { ?>
                        <h4><?php _e('COMMENTS', 'apollo') ?><span> <i class="fa fa-comment"></i></span></h4>
                        <div class="a-block-ct">
                            <?php comments_template('/partials/comments.php'); ?>
                        </div>
                    <?php } ?>
                </div><!-- #comments -->
            </div>
        </div>
    </div>
</div>