       
<?php 
    if ( $this->public_art ):
    
    foreach ( $this->public_art as $art ):

        if (empty($art->ID)) {
            continue;
        }
        $art = get_public_art( $art->ID );

        $artType = $art->generate_categories();
?>
<div class="more-frm-itm">
    <div class="more-pic">
        <a href="<?php echo $art->get_permalink(); ?>">
            <?php echo $art->get_image( 'thumbnail' ); ?>
        </a>    
    </div>
    <div class="more-ct custom">
        <h3> <a href="<?php echo $art->get_permalink(); ?>"><?php echo $art->get_title(); ?></a></h3>
        
        <?php
        if ( $artType ):
        ?>
        <div class="org-type"><?php echo $artType ; ?></div>
        <?php endif; ?>
        <?php if( of_get_option(Apollo_DB_Schema::_ENABLE_THUMBS_UP,1)) : ?>
        <div class="rating-box rating-action">
            <div class="like parent-not-underline-hover" data-ride="ap-event_rating" id="_event_rating_<?php echo $art->id.'_'.Apollo_DB_Schema::_PUBLIC_ART_PT ?>" data-i="<?php echo $art->id ?>" data-t="<?php echo Apollo_DB_Schema::_PUBLIC_ART_PT ?>"> <a href="javascript:void(0);" ><span class="_count"></span></a></div>
        </div>
        <?php endif ?>
    </div>

    
    <div  class="b-btn custom">
        <?php echo SM_Mod_Model::renderBookmarkBtn( array( 'class' => 'btn-bm btn btn-b' ), get_post($art->id) ); ?>
    </div>
</div>

<?php endforeach; ?>
                                

<?php endif; ?>