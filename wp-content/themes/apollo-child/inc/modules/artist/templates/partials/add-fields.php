<?php
    $questions = @unserialize(get_option(Apollo_DB_Schema::_APL_ARTIST_OPTIONS));
    $answers = @unserialize($model->getArtist()->get_meta_data(Apollo_DB_Schema::_APL_ARTIST_QUESTIONS));

    $str = '';

    // get artist style
    $styles = $model->getArtist()->generate_artist_styles(TRUE);
    if ($styles) {
        $str .= '<div class="el-blk">
                    <p class="custom apl-simple-link"><strong>' . __('Artist Style', 'apollo') . ': </strong>' . $styles . '</p>
                </div>';
    }

    // get artist medium
    $mediums = $model->getArtist()->generate_artist_mediums(TRUE);
    if ($mediums) {
        $str .= '<div class="el-blk">
                    <p class="custom apl-simple-link"><strong>' . __('Artist Medium', 'apollo') . ': </strong>' . $mediums . '</p>
                </div>';
    }

    if ($answers) {
        foreach ($answers as $k => $val):
            if (!$val) continue;
            $str .= '<div class="el-blk">
                        <p class="custom"><strong>' . (isset($questions[$k]) ? $questions[$k] : '') . '</strong></p>
                        <p class="custom">' . $val . '</p>
                    </div>';
        endforeach;
    }


    if ($str):
?>
    <div class="blog-bkl">
        <div class="a-block">
            <h4>
                <?php
                $val = of_get_option(Apollo_DB_Schema::_TEMP_ARTIST_MORE_ABOUT, __('MORE ABOUT THIS ARTIST', 'apollo'));
                echo $val ? $val : __('MORE ABOUT THIS ARTIST', 'apollo');
                ?>
            </h4>
            <?php echo $str; ?>
        </div>
    </div>
<?php endif;
$model->getArtist()->render_cf_detail();
?>
