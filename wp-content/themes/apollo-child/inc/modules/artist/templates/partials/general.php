<div class="blog-bkl organization clearfix">
    <img src="<?php echo SONOMA_FRONTEND_ASSETS_URI;?>/images/demo/creative-sonoma-logo.png" class="org-logo-smn" alt="Creative Sonoma" title="Creative Sonoma" />
    <h1 class="p-ttl"><?php echo $model->getArtist()->get_title() ?></h1>

    <?php
    $icon_fields = maybe_unserialize( get_option( Apollo_DB_Schema::_APL_ARTISTS_ICONS ) );
    $icons = $model->getArtist()->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_POST_ICONS, Apollo_DB_Schema::_APL_ARTIST_DATA );

    $model->getArtist()->renderIcons($icon_fields, $icons);
    $model->renderType();
    ?>

    <div class="rating-box rating-action rating-art">
        <?php if (of_get_option(Apollo_DB_Schema::_ENABLE_THUMBS_UP, 1)) : ?>
            <div class="like"
                 id="_event_rating_<?php echo $model->getId() . '_' . Apollo_DB_Schema::_ORGANIZATION_PT ?>"
                 data-ride="ap-event_rating" data-i="<?php echo $model->getId() ?>"
                 data-t="<?php echo Apollo_DB_Schema::_ORGANIZATION_PT ?>">
                <a href="javascript:void(0);"><span class="_count">&nbsp;</span></a></div>
        <?php endif ?>
        <?php if ($model->isAllowComment()): ?>
            <div class="cm-comment"><a href="#event_comment_block" data-uk-smooth-scroll="{offset: 121}"><?php _e('Comment', 'apollo') ?></a></div>
        <?php endif; ?>
    </div>
    <div class="el-blk clearfix">
        <div class="art-pic">
            <?php echo $model->getArtist()->get_image('medium'); ?>
        </div>
        <?php echo $model->renderSocial(); ?>
    </div>
    <div class="el-blk clearfix">
        <div class="art-desc apl-internal-content rich-txt rich-txt--hight-first-child">
            <?php  echo $model->getArtist()->get_full_content(); ?>
        </div>
    </div>
</div>