

<?php if (!$model->getListPublicArt()->isEmpty()): ?>

    <div class="blog-bkl classified clearfix">
        <div class="a-block">
            <h4><?php _e('Public Art', 'apollo') ?></h4>
        </div>
        <div id="apollo-view-more-classified-container">
            <?php echo $model->getListPublicArt()->render_html(SONOMA_MODULES_DIR. '/artist/templates/partials/_artist-public-art.php') ?>
        </div>

        <?php if ($model->getListPublicArt()->isShowMore()): ?>
            <div class="load-more b-btn">
                <a href="javascript:void(0);"
                   data-container="#apollo-view-more-classified-container"
                   data-ride="ap-more"
                   data-holder="#apollo-view-more-classified-container>:last"
                   data-sourcetype="url"
                   data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_show_more_artist_public_art&page=2') ?>&current_artist_id=<?php echo $model->getId(); ?>&user_id=<?php echo $model->getArtist()->post->post_author ?>"
                   data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
                   data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
                   class="btn-b arw"><?php _e('SHOW MORE', 'apollo') ?>
                </a>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>
