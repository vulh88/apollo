    <!-- Override your template here -->
<?php

$rq_artist_medium = isset($_GET['artist_medium']) ? $_GET['artist_medium'] : '';
$rq_artist_style = isset($_GET['artist_style']) ? $_GET['artist_style'] : '';


$rq_term = isset($_GET['term']) ? $_GET['term'] : '';
$rq_keyword = isset($_GET['keyword']) ?Apollo_App::clean_data_request( $_GET['keyword'] ) : '';

$title = isset( $instance['title'] ) && $instance['title'] ? $instance['title'] : __('Find An Artist', 'apollo');


$searchData = array(
    'city' => isset($_GET['city'])? urldecode($_GET['city']) : of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY,''),
    'zip' => isset($_GET['zip'])?urldecode($_GET['zip']):'',
);

$cityList = Apollo_App::getCityByTerritory(false, false, true);
$zipList = Apollo_App::getZipByTerritory(false, false, false, true);
$listCityState = Apollo_App::getCityStateByTerritory();

$isTopSearchMobileVersion = isset($_GET['top-search-mobile']) && !empty($_GET['top-search-mobile']) ? 'ts-mobile' : '';

$defaultArtistTypeValue = $model->getDefaultValueForTerm( Apollo_DB_Schema::_ARTIST_TYPE, $rq_term);
$defaultArtistStyleValue = $model->getDefaultValueForTerm(Apollo_DB_Schema::_ARTIST_STYLE_TAX, $rq_artist_style);
$defaultArtistMediumValue = $model->getDefaultValueForTerm(Apollo_DB_Schema::_ARTIST_MEDIUM_TAX, $rq_artist_medium);



?>
<div class="top-search artist-s-t-m <?php echo $isTopSearchMobileVersion; ?>">
    <div class="inner">
        <div class="top-search-row">
            <form method="get" id="search-artist-m-t" action="<?php echo $model->getCustomUrlBySlug();?>" class="form-event">
                <div class="search-row">
                    <div class="el-blk search-input-wrapper">
                        <button type="button" class="btn btn-l s"><i class="fa fa-search fa-flip-horizontal fa-lg"></i></button>
                        <input type="text" name="keyword" value="<?php echo Apollo_App::clean_data_request(get_query_var('keyword'),  true) ?>" placeholder="<?php _e('Search by Keyword', 'apollo') ?>" class="inp inp-txt event-search event-search-custom">
                        <input type="hidden" name="do_search" value="<?php echo isset($_GET['do_search']) && !empty($_GET['do_search']) ? 'yes' : 'no'; ?>" />
                    </div>
                    <?php
                    $artist_types = Apollo_Artist::get_tree_artist_type();

                    if(!empty($artist_types)):
                        ?>
                        <div class="el-blk">
                            <div class="select-bkl">
                                <select name="term" class="chosen" id="artist-type-select">
                                    <option value=""><?php _e( 'Artist Type', 'apollo' ) ?></option>
                                    <?php Apollo_Artist::build_option_tree($artist_types, $defaultArtistTypeValue) ?>
                                </select>
                                <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                            </div>
                        </div>
                    <?php endif; ?>


                    <?php
                    $list_artist_style = Apollo_Artist::get_tree_artist_style();

                    if(!empty($list_artist_style)):
                        ?>
                        <div class="el-blk">
                            <div class="select-bkl">
                                <select name="artist_style" class="chosen" id="artist-style-select">
                                    <option value=""><?php _e( 'Artist Style', 'apollo' ) ?></option>
                                    <?php Apollo_Artist::build_option_tree($list_artist_style, $defaultArtistStyleValue) ?>
                                </select>
                                <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php
                    $list_artist_medium = Apollo_Artist::get_tree_artist_medium();

                    if(!empty($list_artist_medium)):
                        ?>
                        <div class="el-blk">
                            <div class="select-bkl">
                                <select name="artist_medium" class="chosen" id="artist-medium-select">
                                    <option value=""><?php _e( 'Artist Medium', 'apollo' ) ?></option>
                                    <?php Apollo_Artist::build_option_tree($list_artist_medium, $defaultArtistMediumValue) ?>
                                </select>
                                <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php
                    if(!empty($cityList)):
                        ?>
                        <div class="el-blk">
                            <div class="select-bkl">
                                <select name="city" class="chosen" id="artist-city-select">
                                    <?php foreach($listCityState as $k => $cityItem): ?>
                                        <optgroup <?php echo $k ? 'label="'.$k.'"' : '' ?>>

                                            <?php if (!$k): ?>
                                                <option value="0"><?php echo $cityItem; ?></option>
                                            <?php endif; ?>

                                            <?php if ($cityItem && is_array($cityItem)):
                                                foreach ( $cityItem as $city ):
                                                    $selected = '';
                                                    if( $city ==  $searchData['city'])
                                                        $selected = 'selected';
                                                    ?>
                                                    <option <?php echo $selected ?> value="<?php echo  $city ?>"><?php echo $city ?></option>
                                                <?php endforeach; endif; ?>
                                        </optgroup>
                                    <?php endforeach; ?>
                                </select>
                                <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="search-row">
                    <?php
                    if(!empty($zipList)):
                        ?>
                        <div class="el-blk">
                            <div class="select-bkl">
                                <select name="zip" class="chosen" id="artist-zip-select">
                                    <?php foreach($zipList as $k => $zip ):
                                        if ( ! $zip ) continue;
                                        ?>
                                        <option value="<?php echo $k ?>" <?php selected($zip, $searchData['zip']) ?>><?php echo $zip ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="sub-blk">
                        <button type="submit" class="btn btn-l lgr fr f-w-btn-search"><?php _e('SEARCH', 'apollo') ?></button>
                    </div>
                </div>
                <div class="el-blk custom-el-blk"><span class="tt"><?php _e('Search by '. Apollo_App::getSingleLabelByModuleName(Apollo_DB_Schema::_ARTIST_PT)  .'’s last name', 'apollo') ?>:</span>
                    <div class="character-board">
                        <?php
                        for( $i = 97 ; $i <= 122; $i++): ?><a <?php if ( isset( $_REQUEST['last_name'] ) && strtolower( $_REQUEST['last_name'] ) == strtolower( chr($i) ) ) echo 'class="active"'; ?> href="<?php echo $model->getCustomUrlBySlug() ?>?last_name=<?php echo chr($i) ?>&do_search=yes"><?php echo strtoupper(chr($i)) ?></a><?php endfor; ?>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>