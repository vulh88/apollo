<?php

/**
 * Class SM_Artist_Mod
 */
require_once __DIR__ . '/inc/sm_artist_model.php';

class SM_Artist_Mod extends SM_Mod_Abstract
{
    public function __construct()
    {
        $this->renderPageTemplate();
    }

    public function renderPageTemplate()
    {
        $smClassifiedModel = new SM_Artist_Model();
        $file = SONOMA_MODULES_DIR . '/artist/templates/artist.php';
        if (is_single()) {
            $file = SONOMA_MODULES_DIR . '/artist/templates/artist-detail.php';
        }
        
        $fileTemplatePart = SONOMA_MODULES_DIR . '/artist/templates/artist-top.php';

        if (file_exists($fileTemplatePart)) {
            $this->smGetTemplatePartCustom($fileTemplatePart, $smClassifiedModel);
        }

        if (file_exists($file)) {
            $this->smGetTemplatePartCustom($file, $smClassifiedModel);
        }
    }

}

new SM_Artist_Mod();