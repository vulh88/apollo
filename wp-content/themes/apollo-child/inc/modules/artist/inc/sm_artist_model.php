<?php

/**
 * Created by PhpStorm.
 * User: truong
 * Date: 23/09/2016
 * Time: 16:51
 */
class SM_Artist_Model extends SM_Mod_Model
{
    /**
     * @var
     */
    private $artist;
    public static $_order_by;
    public static $_order = 'ASC';

    /**
     * @var
     */
    private $id;

    /**
     * @return bool|int
     */
    public function getPagesize()
    {
        return $this->pagesize;
    }

    /**
     * @param bool|int $pagesize
     */
    public function setPagesize($pagesize)
    {
        $this->pagesize = $pagesize;
    }

    /**
     * @return mixed
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * @param mixed $artist
     */
    public function setArtist($artist)
    {
        $this->artist = $artist;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getData()
    {
        $data = $this->getArtist()->getData();
        $data['email'] = $this->getArtist()->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_DISPLAY_EMAIL, Apollo_DB_Schema::_APL_ARTIST_DATA ) != 'yes' ?
            $this->getArtist()->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_EMAIL, Apollo_DB_Schema::_APL_ARTIST_DATA ) : '';
        $data['web']  = $this->getArtist()->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_WEBURL, Apollo_DB_Schema::_APL_ARTIST_DATA );
        $data['blog']  = $this->getArtist()->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_BLOGURL, Apollo_DB_Schema::_APL_ARTIST_DATA );
        $data['facebook']  = $this->getArtist()->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_FB, Apollo_DB_Schema::_APL_ARTIST_DATA );
        $data['twitter']  = $this->getArtist()->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_TW, Apollo_DB_Schema::_APL_ARTIST_DATA );
        $data['inst']  = $this->getArtist()->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_INS, Apollo_DB_Schema::_APL_ARTIST_DATA );
        $data['linked']  = $this->getArtist()->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_LK, Apollo_DB_Schema::_APL_ARTIST_DATA );
        $data['pinterest']  = $this->getArtist()->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_PR, Apollo_DB_Schema::_APL_ARTIST_DATA );
        $display_phone =  $this->getArtist()->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_DISPLAY_PHONE, Apollo_DB_Schema::_APL_ARTIST_DATA );
        $data['phone']= $display_phone != 'yes' ?  $this->getArtist()->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_PHONE, Apollo_DB_Schema::_APL_ARTIST_DATA ):'';

        $display_address =  $this->getArtist()->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_DISPLAY_ADDRESS, Apollo_DB_Schema::_APL_ARTIST_DATA );
        $data['address'] = $display_address != 'yes' ? $this->getArtist()->get_full_address() : '';
        return $data;
    }

    public function __construct()
    {
        parent::__construct();
        $this->setData();
        $order = of_get_option( Apollo_DB_Schema::_ARTIST_LISTING_ORDER,'ALPHABETICAL_ASC');

        switch ( $order ) {
            case 'ALPHABETICAL_ASC':
                SM_Artist_Model::$_order_by = 'tblMetaSort.meta_value';
                SM_Artist_Model::$_order = 'ASC';
                break;

            case 'ALPHABETICAL_DESC':
                SM_Artist_Model::$_order_by = 'tblMetaSort.meta_value';
                SM_Artist_Model::$_order = 'DESC';
                break;


            default:
                SM_Artist_Model::$_order_by = 'tblMetaSort.meta_value';
                SM_Artist_Model::$_order = 'ASC';
                break;
        }


    }

    public  function renderTopContent(){
        $content = '';
        $currentTerm = get_query_var('term');
        $currentTerm = get_term_by('slug', $currentTerm, get_query_var( 'taxonomy' ));
        if (!empty($currentTerm)) {
            $displayPublic = get_apollo_term_meta($currentTerm->term_id, Apollo_DB_Schema::_APL_TAXONOMY_DISPLAY_PUBLIC_FIELD, true);
            $termDescription = !empty($currentTerm) ? $currentTerm->description : '';
            if ($termDescription && !empty($displayPublic)) {
                if ($displayPublic) {
                    $content = Apollo_App::the_content($termDescription);
               }
            }
        }
        return $content;
    }
    public function setData()
    {
        $this->pagesize = Apollo_App::aplGetPageSize( SM_Common_Const::_SM_ITEMS_NUM_LISTING_PAGE, Apollo_DB_Schema::_ARTIST_NUM_ITEMS_LISTING_PAGE );
        global $post;
        $this->setArtist(get_artist($post));
        $this->setId($post->ID);
    }

    public function render_html()
    {
        return $this->smGetTemplatePartCustom($this->template, $this, true);
    }

    public function search() {

        $arr_params = array(
            'post_type'         => Apollo_DB_Schema::_ARTIST_PT,
            'posts_per_page'    => $this->pagesize,
            'paged'             => $this->page,
            'post_status'       => array('publish'),
        );

        if($keyword = self::getKeyword()) {
            $arr_params['s'] =  $keyword;
        }
        $termType = get_query_var('taxonomy');
        $arr_tax_query = array();
        if(isset($_GET['term']) && !empty($_GET['term'])) {
            $arr_tax_query[] = array(
                'taxonomy'=> !empty($termType) ? $termType : 'artist-type',
                'terms' => array($_GET['term']),
            );
        }

        if(isset($_GET['artist_medium']) && !empty($_GET['artist_medium'])) {
            $arr_tax_query[] = array(
                'taxonomy'  =>Apollo_DB_Schema::_ARTIST_MEDIUM_TAX,
                'terms'     => array($_GET['artist_medium']),
            );
        }

        if(isset($_GET['artist_style']) && !empty($_GET['artist_style'])) {
            $arr_tax_query[] = array(
                'taxonomy'  =>Apollo_DB_Schema::_ARTIST_STYLE_TAX,
                'terms'     => array($_GET['artist_style']),
            );
        }

        $arr_params['tax_query'] = $arr_tax_query;

        // Set new offset if this is ajax action
        $this->addOffsetToParams($arr_params);

        add_filter('posts_where', array(__CLASS__, 'filter_where_artist_tbl'), 10, 1);
        add_filter('posts_join', array(__CLASS__, 'filter_join_artist_tbl'), 10, 1);
        add_filter('posts_orderby', array(__CLASS__, 'filter_order_artist_tbl'), 10, 1);
        add_filter( 'posts_search', array(__CLASS__, 'posts_search'), 10, 1);
        add_filter( 'posts_groupby', array($this, 'filter_groupby'), 10, 1 );

        $this->result = query_posts($arr_params);
        Apollo_Next_Prev::updateSearchResult($GLOBALS['wp_query']->request,Apollo_DB_Schema::_ARTIST_PT);
        remove_filter('posts_orderby', array(__CLASS__, 'filter_order_artist_tbl'), 10);
        remove_filter('posts_join', array(__CLASS__, 'filter_join_artist_tbl'), 10);
        remove_filter('posts_where', array(__CLASS__, 'filter_where_artist_tbl'), 10);
        remove_filter( 'posts_search', array(__CLASS__, 'posts_search'), 10, 1);
        remove_filter( 'posts_groupby', array($this, 'filter_groupby'), 10, 1 );

        $this->total_pages = ceil($this->total = $GLOBALS['wp_query']->found_posts / $this->pagesize);
        $this->total = $this->total = $GLOBALS['wp_query']->found_posts;

        $this->template = SONOMA_MODULES_DIR . '/artist/templates/' . $this->_get_template_name(of_get_option(Apollo_DB_Schema::_ARTIST_DEFAULT_VIEW_TYPE,SONOMA_DIRECTORY_LISTING_DEFAULT_VIEW_TYPE));

        $this->resetPostData();
    }

    public static function filter_where_artist_tbl ($where) {
        global $wpdb;

        $where .= " AND
            $wpdb->posts.ID NOT IN (
                SELECT apollo_artist_id FROM {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}}
                WHERE meta_key = '".Apollo_DB_Schema::_APL_ARTIST_DO_NOT_LINKED."' AND meta_value = 'yes'
            )
        ";

        // JUST LAST NAME
        if(isset($_GET['last_name']) && !empty($_GET['last_name'])) {

            $last_name = $_GET['last_name'];
            $wpdb->escape_by_ref($last_name);
            return $where ." AND CAST(tblMetaSort.meta_value AS CHAR) LIKE '". $last_name ."%'";
        }

        // OTHER CASE
        if(isset($_GET['city']) && !empty($_GET['city'])) {
            $city = $_GET['city'];

            $wpdb->escape_by_ref($city);
            $where .= " AND CAST(mt_city.meta_value AS CHAR) LIKE '". $city ."%' ";
        }

        if(isset($_GET['zip']) && !empty($_GET['zip'])) {
            $zip = $_GET['zip'];

            $wpdb->escape_by_ref($zip);
            $where .= " AND CAST(mt_zip.meta_value AS CHAR) LIKE '". $zip ."%' ";
        }

        $where .= " AND {$wpdb->posts}.post_type = '".Apollo_DB_Schema::_ARTIST_PT."' ";

        return $where;
    }

    public static function filter_join_artist_tbl ($join) {
        global $wpdb;

        // Click on a letter bellow the search box
        if((isset($_GET['last_name']) && !empty($_GET['last_name']))) { // because city need join with meta table
            return $join . " INNER JOIN {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}} as tblMetaSort ON ({$wpdb->posts}.ID = tblMetaSort.apollo_artist_id) AND tblMetaSort.meta_key = '" . Apollo_DB_Schema::_APL_ARTIST_LNAME . "'";
        }

        // NOT LAST NAME
        $join .= " LEFT JOIN {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}} as tblMetaSort ON ({$wpdb->posts}.ID = tblMetaSort.apollo_artist_id) AND tblMetaSort.meta_key = '" . Apollo_DB_Schema::_APL_ARTIST_LNAME . "'";

        if(isset($_GET['city']) && !empty($_GET['city'])) {
            $join .=
                " INNER JOIN {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}} mt_city ON "
                . "(mt_city.meta_key = '". Apollo_DB_Schema::_APL_ARTIST_CITY ."' AND {$wpdb->posts}.ID = mt_city.apollo_artist_id)";
        }

        if(isset($_GET['zip']) && !empty($_GET['zip'])) {
            $join .=
                " INNER JOIN {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}} mt_zip ON "
                . "(mt_zip.meta_key = '". Apollo_DB_Schema::_APL_ARTIST_ZIP ."' AND {$wpdb->posts}.ID = mt_zip.apollo_artist_id)";
        }

        $join .= self::getJoinTaxByKeyword();

        return $join;
    }

    /* last name is not query with city in same query: Good to know that. because we don't need join an extra query to sort by last_name  */
    public static function filter_order_artist_tbl ($order) {
        return sprintf("%s  %s", SM_Artist_Model::$_order_by, SM_Artist_Model::$_order);
    }


    public function getListPublicArt(){

        include_once APOLLO_TEMPLATES_DIR . '/public-art/list-public-arts.php';
        $list_pub_art = new List_Public_Art_Adapter(1, Apollo_Display_Config::PAGESIZE_UPCOM);
        $list_pub_art->get_artist_public_art($this->getId());
        return $list_pub_art;
    }

    public function renderType()
    {
        $type = $this->getArtist()->generate_artist_categories();
        if ($type) {
            echo '<div class="artist-type">' . $type . ' </div>';
        }
    }

    public function renderContentGeneral()
    {
        return $this->smGetTemplatePartCustom(SONOMA_MODULES_DIR . '/artist/templates/partials/general.php', $this, true);
    }

    public function renderDocuments()
    {
        return $this->smGetTemplatePartCustom(SONOMA_MODULES_DIR . '/artist/templates/partials/documents.php', $this, true);
    }

    public function renderSocial()
    {
        return $this->smGetTemplatePartCustom(SONOMA_MODULES_DIR . '/artist/templates/partials/social.php', $this, true);
    }

    public function renderAdditionalFields()
    {
        return $this->smGetTemplatePartCustom(SONOMA_MODULES_DIR . '/artist/templates/partials/add-fields.php', $this, true);
    }

    public function renderMedia()
    {
        return $this->smGetTemplatePartCustom(SONOMA_MODULES_DIR . '/artist/templates/partials/media.php', $this, true);
    }

    public function renderPublicArt()
    {
        return $this->smGetTemplatePartCustom(SONOMA_MODULES_DIR . '/artist/templates/partials/list-public-art.php', $this, true);
    }

    public function isAllowComment()
    {
        return of_get_option(Apollo_DB_Schema::_ENABLE_COMMENT, 1) && comments_open();
    }

    public function getListAudio()
    {
        return $this->getArtist()->getListAudios(Apollo_DB_Schema::_APL_ARTIST_AUDIO);
    }

    public function renderListAudios()
    {
        $audioList = $this->getListAudio();
        if (!empty($audioList)) {
            return $this->smGetTemplatePartCustom(SONOMA_MODULES_DIR . '/common-templates/audio-list.php', $this, true);
        }
        return '';

    }

    public function renderAudioItem(){
        $result = array();
        $audioList =  $this->getListAudio();
        $file =  SONOMA_MODULES_DIR. '/common-templates/audio-item.php';
        if ( !empty($audioList)) {
            foreach($audioList as $item):
                $result[] = $this->smGetTemplatePartCustom($file,$item,true);

            endforeach;
        }
        if (!empty($result)) {
            $result = implode(" ", $result);
        }
        return $result;
    }

    public function renderSlider()
    {
        $gallerys  =   explode( ',' , $this->getArtist()->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_IMAGE_GALLERY ) );

        if(!empty($gallerys[0])){
            $data = SM_View_Helper::handleDataPhotoItems($gallerys);
            return Apollo_App::getSimpleTemplatePart(SONOMA_MODULES_DIR . '/common-templates/slider.php', $data, true);
        }

    }

    public function renderVideo()
    {
        $videos = $this->getArtist()->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_VIDEO, Apollo_DB_Schema::_APL_ARTIST_DATA );
        if (!empty($videos)){
            $data = SM_View_Helper::handleDataVideos($videos);
            return Apollo_App::getSimpleTemplatePart(SONOMA_MODULES_DIR . '/common-templates/video.php', $data, true);

        }
    }

    /**
     * @return string
     */
    public function getCustomLabel(){
        return Apollo_App::getCustomLabelByModuleName(Apollo_DB_Schema::_ARTIST_PT);
    }

    /**
     * @return string
     */
    public function getCustomUrlBySlug(){
        return Apollo_App::getCustomUrlByModuleName(Apollo_DB_Schema::_ARTIST_PT);
    }
}