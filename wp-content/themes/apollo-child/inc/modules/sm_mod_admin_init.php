<?php


if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}


class SM_Mod_Admin_Init
{

    public function __construct(){
        $availableModules =array('page','post');
        foreach ($availableModules as $type){
            $file = SONOMA_MODULES_DIR.'/'.$type.'/admin/admin.php';
            if (  file_exists(  $file)  ) {
                require_once $file;
            }
        }

    }

}

new SM_Mod_Admin_Init();