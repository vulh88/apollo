<?php

/**
 * Created by PhpStorm.
 * User: truong
 * Date: 23/09/2016
 * Time: 16:51
 */
class SM_Venue_Model extends SM_Mod_Model
{

    /**
     * @var
     */
    private $venue;

    /**
     * @var
     */
    private $id;

    /**
     * @return mixed
     */
    public function getVenue()
    {
        return $this->venue;
    }

    /**
     * @param mixed $venue
     */
    public function setVenue($venue)
    {
        $this->venue = $venue;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function getData()
    {
        $data = $this->getVenue()->getVenueData();
        $data['str_address'] = $this->getStrAddress();
        return $data;
    }

    public function __construct()
    {
        parent::__construct();
        $this->setData();
    }

    public function setData()
    {
        $this->pagesize = Apollo_App::aplGetPageSize( SM_Common_Const::_SM_ITEMS_NUM_LISTING_PAGE, Apollo_DB_Schema::_VENUE_NUM_ITEMS_LISTING_PAGE );
        global $post;
        $this->setVenue(get_venue($post));
        $this->setId($post->ID);
    }

    public function getSharingInfo($addition_field = [], $id = 0)
    {
        SocialFactory::social_btns(array(
            'info' => $this->getVenue()->getShareInfo(),
            'id' => $id,
            'data_btns' => $addition_field
        ));
    }

    public function renderType()
    {
        $orgType = $this->getVenue()->generate_categories();
        if ($orgType) {
            echo '<div class="org-type">' . $orgType . ' </div>';
        }
    }

    public function renderContentGeneral()
    {
        return $this->smGetTemplatePartCustom(SONOMA_MODULES_DIR . '/venue/templates/partials/general.php', $this, true);
    }

    public function renderSocial()
    {
        return $this->smGetTemplatePartCustom(SONOMA_MODULES_DIR . '/venue/templates/partials/social.php', $this, true);
    }

    public function renderAdditionalFields()
    {
        return $this->smGetTemplatePartCustom(SONOMA_MODULES_DIR . '/venue/templates/partials/add-fields.php', $this, true);
    }

    public function renderMedia()
    {
        return $this->smGetTemplatePartCustom(SONOMA_MODULES_DIR . '/venue/templates/partials/media.php', $this, true);
    }

    public function isAllowComment()
    {
        return of_get_option(Apollo_DB_Schema::_ENABLE_COMMENT, 1) && comments_open();
    }

    public function renderSlider()
    {
        $gallerys = $this->getVenue()->getListImages();

        if(!empty($gallerys[0])){
            $data = SM_View_Helper::handleDataPhotoItems($gallerys);
            return Apollo_App::getSimpleTemplatePart(SONOMA_MODULES_DIR . '/common-templates/slider.php', $data, true);
        }

    }

    public function renderVideo()
    {
        $videos = $this->getVenue()->getListVideo();
        $videoTemp = array();
        foreach ($videos as $video) {
            $videoTemp[] = array(
                'embed' => $video['embed'],
                'desc' => isset($video['desc']) ? base64_decode($video['desc']) : '',
            );
        }
        $videos = $videoTemp;

        if (!empty($videos)){
            $data = SM_View_Helper::handleDataVideos($videos);
            return Apollo_App::getSimpleTemplatePart(SONOMA_MODULES_DIR . '/common-templates/video.php', $data, true);

        }
    }

    public function render_html()
    {
        return $this->smGetTemplatePartCustom($this->template, $this, true);
    }

    public function renderAccessibilityInfomation()
    {
        return $this->smGetTemplatePartCustom(SONOMA_MODULES_DIR . '/venue/templates/partials/accessibility-infomation.php', $this, true);
    }

    public function renderPackingInfo()
    {
        return $this->smGetTemplatePartCustom(SONOMA_MODULES_DIR . '/venue/templates/partials/packing-info.php', $this, true);
    }


    public function renderLocation()
    {
        return $this->smGetTemplatePartCustom(SONOMA_MODULES_DIR . '/venue/templates/partials/location.php', $this, true);
    }

    public function search()
    {
        $arr_params = array(
            'post_type' => Apollo_DB_Schema::_VENUE_PT,
            'posts_per_page' => $this->pagesize,
            'paged' => $this->page,
            'post_status' => array('publish'),

        );

        if ($keyword = self::getKeyword()) {
            $arr_params['s'] = $keyword;
        }

        if (isset($_GET['term']) && !empty($_GET['term'])) {
            $arr_tax_query[] = array(
                'taxonomy' => Apollo_DB_Schema::_VENUE_PT . '-type',
                'terms' => array($_GET['term']),
            );
            $arr_params['tax_query'] = $arr_tax_query;
        }

        // Set new offset if this is ajax action
        $this->addOffsetToParams($arr_params);

        //No need search in this time
        add_filter('posts_where', array(__CLASS__, 'filter_where_tbl'), 10, 1);
        add_filter('posts_join', array(__CLASS__, 'filter_join_tbl'), 10, 1);
        add_filter('posts_orderby', array(__CLASS__, 'filter_order_tbl'), 10, 1);
        add_filter('posts_search', array(__CLASS__, 'posts_search'), 10, 1);
        add_filter('posts_groupby', array($this, 'filter_groupby'), 10, 1);

        $this->result = query_posts($arr_params);
        Apollo_Next_Prev::updateSearchResult($GLOBALS['wp_query']->request, Apollo_DB_Schema::_VENUE_PT);
        remove_filter('posts_orderby', array(__CLASS__, 'filter_order_tbl'), 10);
        remove_filter('posts_join', array(__CLASS__, 'filter_join_tbl'), 10);
        remove_filter('posts_where', array(__CLASS__, 'filter_where_tbl'), 10);
        remove_filter('posts_search', array(__CLASS__, 'posts_search'), 10, 1);
        remove_filter('posts_groupby', array($this, 'filter_groupby'), 10, 1);

        $this->total_pages = ceil($this->total = $GLOBALS['wp_query']->found_posts / $this->pagesize);

        $this->total = $this->total = $GLOBALS['wp_query']->found_posts;

        $this->template = SONOMA_MODULES_DIR . '/venue/templates/' . $this->_get_template_name(of_get_option(Apollo_DB_Schema::_VENUE_DEFAULT_VIEW_TYPE,SONOMA_DIRECTORY_LISTING_DEFAULT_VIEW_TYPE));
        $this->resetPostData();
    }

    public static function filter_where_tbl($where)
    {
        global $wpdb;
        $sqlString = '';
        $enableRegionSelection = Apollo_App::enableMappingRegionZipSelection();

        //query by city
        /**
         *  Because city input by user, they can input special character.
         *  We use id for param to search correct
         */
        if (isset($_GET['city']) && !empty($_GET['city'])) {
            $city = urldecode($_GET['city']);
            $sqlString .= 'AND  ' . $wpdb->posts . '.ID IN (
                    SELECT em.apollo_venue_id
                    FROM ' . $wpdb->{Apollo_Tables::_APL_VENUE_META} . ' em
                    WHERE em.apollo_venue_id  = ' . $wpdb->posts . '.ID
                        AND em.meta_value  REGEXP BINARY  \'.*"_venue_city";s:[0-9]+:"' . $city . '".*\'
                        AND em.meta_key = "' . Apollo_DB_Schema::_APL_VENUE_ADDRESS . '"
                )';

        }

        if (isset($_GET['region']) && !empty($_GET['region'])) {
            $region = urldecode($_GET['region']);
            $wpdb->escape_by_ref($region);

            /**
             * Get event ids in zipcodes region selection
             */
            if ($enableRegionSelection) {
                if (!$zipcodes = Apollo_Seach_Form_Data::getRegionZipcodes($region)) {
                    $zipcodes = array(false);
                }

                $zipQuery = '"' . implode('","', $zipcodes) . '"';
                $sqlString .= 'AND  ' . $wpdb->posts . '.ID IN (
                    SELECT em.apollo_venue_id
                    FROM ' . $wpdb->{Apollo_Tables::_APL_VENUE_META} . ' em
                    WHERE em.apollo_venue_id  = ' . $wpdb->posts . '.ID
                        AND em.meta_value  IN(' . $zipQuery . ')
                        AND em.meta_key = "' . Apollo_DB_Schema::_VENUE_ZIP . '"
                )';

            } else {
                $sqlString .= 'AND  ' . $wpdb->posts . '.ID IN (
                    SELECT em.apollo_venue_id
                    FROM ' . $wpdb->{Apollo_Tables::_APL_VENUE_META} . ' em
                    WHERE em.apollo_venue_id  = ' . $wpdb->posts . '.ID
                        AND em.meta_value  REGEXP BINARY  \'.*"_venue_region";s:[0-9]+:"' . $region . '".*\'
                        AND em.meta_key = "' . Apollo_DB_Schema::_APL_VENUE_ADDRESS . '"
                )';
            }
        }

        //query by State
        if (isset($_GET['state']) && !empty($_GET['state'])) {
            $state = $_GET['state'];
            $sqlString .= 'AND  ' . $wpdb->posts . '.ID IN (
                SELECT em.apollo_venue_id
                FROM ' . $wpdb->{Apollo_Tables::_APL_VENUE_META} . ' em
                WHERE em.apollo_venue_id  = ' . $wpdb->posts . '.ID
                    AND em.meta_value LIKE \'%' . $state . '%\'
                    AND em.meta_key = "' . Apollo_DB_Schema::_APL_VENUE_ADDRESS . '"
            )';
        }

        /**
         *  Only apply this rule if admin side does not enable the region selection
         */
        if (!$enableRegionSelection && isset($_GET['zip']) && !empty($_GET['zip'])) {
            $zip = urldecode($_GET['zip']);
            $sqlString .= 'AND  ' . $wpdb->posts . '.ID IN (
                SELECT em.apollo_venue_id
                FROM ' . $wpdb->{Apollo_Tables::_APL_VENUE_META} . ' em
                WHERE em.apollo_venue_id  = ' . $wpdb->posts . '.ID
                    AND em.meta_value  REGEXP BINARY  \'.*"_venue_zip";s:[0-9]+:"' . $zip . '".*\'
                    AND em.meta_key = "' . Apollo_DB_Schema::_APL_VENUE_ADDRESS . '"
            )';

        }


        //query by custom-accessbility
        if (isset($_GET['custom']) && !empty($_GET['custom'])) {
            $customs = $_GET['custom'];

            if (is_array($customs) && count($customs) > 0) {
                $and = 'AND';
                foreach ($customs as $custom) {
                    $sqlString .= $and . '  ' . $wpdb->posts . '.ID IN (
                        SELECT em.apollo_venue_id
                        FROM ' . $wpdb->{Apollo_Tables::_APL_VENUE_META} . ' em
                        WHERE em.apollo_venue_id  = ' . $wpdb->posts . '.ID
                            AND em.meta_value  LIKE "%' . $custom . '%"
                            AND em.meta_key = "' . Apollo_DB_Schema::_VENUE_ACCESSIBILITY . '"
                    )';
                    //$and = 'OR';
                }
            }
        }


        return $where .= $sqlString;
    }

    public static function filter_join_tbl($join)
    {

        $join .= self::getJoinTaxByKeyword();

        return $join;
    }

    /* last name is not query with city in same query: Good to know that. because we don't need join an extra query to sort by last_name  */
    public static function filter_order_tbl($order)
    {
        global $wpdb;
        $order = $wpdb->posts . '.post_title ASC';
        return $order;
    }

    public function getStrAddress($is_get_venue_name = false)
    {
        if (isset($this->_arr_address[$is_get_venue_name])) {
            return $this->_arr_address[$is_get_venue_name];
        }

        $arr = array();
        if ($is_get_venue_name) {
            $arr = array_merge($arr, array(Apollo_DB_Schema::_VENUE_NAME));
        }
        $arr = array_merge($arr, array(
            Apollo_DB_Schema::_VENUE_ADDRESS1,
            Apollo_DB_Schema::_VENUE_ADDRESS2,
            Apollo_DB_Schema::_VENUE_CITY,
            Apollo_DB_Schema::_VENUE_STATE,
            Apollo_DB_Schema::_VENUE_ZIP,
        ));

        return $this->get_location_data($arr);
    }


    public function get_location_data($fields)
    {

        if (empty($fields)) {
            return array();
        }

        $venue_id = $this->id;
        if ($venue_id) {
            $venue = get_venue(get_post($venue_id));
            if (!$venue) return false;
        } else {
            $venue = @unserialize($this->get_meta_data(Apollo_DB_Schema::_APL_EVENT_TMP_VENUE));
        }

        $arr_address = array();
        $_is_register_venue = $venue instanceof stdClass || $venue instanceof WP_Post || $venue instanceof Apollo_ABS_Module;

        $is_have_addr = false;
        foreach ($fields as $_field) {

            if ($_is_register_venue) {
                if ($_field == Apollo_DB_Schema::_VENUE_NAME) {
                    $arr_address[] = $venue->get_title();
                    continue;
                }

                /* If have address */
                if ($is_have_addr && $_field === Apollo_DB_Schema::_VENUE_ADDRESS2) {
                    continue;
                }
                /* meta data */
                if ($val = $venue->get_meta_data($_field, Apollo_DB_Schema::_APL_VENUE_ADDRESS)) {
                    $arr_address[] = $val; // _venue_address2

                    if ($_field == Apollo_DB_Schema::_VENUE_ADDRESS1) {
                        $is_have_addr = true;
                    }
                }

                continue;
            }

            if (isset($venue[$_field]) && $venue[$_field]) {
                $arr_address[] = $venue[$_field];
            }
        }
        return implode(", ", $arr_address);
    }
}