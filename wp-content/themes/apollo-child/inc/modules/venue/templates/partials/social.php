<div class="art-social">
    <div class="el-blk">
        <?php if (isset($model->getData()['email']) && !empty($model->getData()['email'])) { ?>
            <div class="art-social-item"><i class="fa fa-envelope fa-lg">&nbsp;&nbsp;&nbsp;</i><a
                    href="mailto:<?php echo $model->getData()['email'] ?>">
                    <?php _e('Email', 'apollo') ?></a>
                <div class="slash">/</div>
            </div>
        <?php } ?>

        <?php if (isset($model->getData()['web']) && !empty($model->getData()['web'])) { ?>
            <div class="art-social-item"><i class="fa fa-link fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK"
                                                                                              href="<?php echo $model->getData()['web'] ?>">
                    <?php _e('Website', 'apollo') ?></a>
                <div class="slash">/</div>
            </div>
        <?php } ?>

        <?php if (isset($model->getData()['blog']) && !empty($model->getData()['blog'])) { ?>
            <div class="art-social-item"><i class="fa fa-star fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK"
                                                                                              href="<?php echo $model->getData()['blog'] ?>">
                    <?php _e('Blog', 'apollo') ?></a>
                <div class="slash">/</div>
            </div>
        <?php } ?>


        <?php if (isset($model->getData()['facebook']) && !empty($model->getData()['facebook'])) { ?>
            <div class="art-social-item"><i class="fa fa-facebook fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK"
                                                                                                  href="<?php echo $model->getData()['facebook'] ?>">
                    <?php _e('Facebook', 'apollo') ?></a>
                <div class="slash">/</div>
            </div>
        <?php } ?>

        <?php if (isset($model->getData()['twitter']) && !empty($model->getData()['twitter'])) { ?>
            <div class="art-social-item"><i class="fa fa-twitter fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK"
                                                                                                 href="<?php echo $model->getData()['twitter'] ?>">
                    <?php _e('Twitter', 'apollo') ?></a>
                <div class="slash">/</div>
            </div>
        <?php } ?>

        <?php if (isset($model->getData()['inst']) && !empty($model->getData()['inst'])) { ?>
            <div class="art-social-item"><i class="fa fa-camera-retro fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK"
                                                                                                      href="<?php echo $model->getData()['inst'] ?>">
                    <?php _e('Instagram', 'apollo') ?>  </a>
                <div class="slash">/</div>
            </div>
        <?php } ?>

        <?php if (isset($model->getData()['linked']) && !empty($model->getData()['linked'])) { ?>
            <div class="art-social-item"><i class="fa fa-linkedin-square fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK"
                                                                                                         href="<?php echo $model->getData()['linked'] ?>">
                    <?php _e('LinkedIn', 'apollo') ?>   </a>
                <div class="slash">/</div>
            </div>
        <?php } ?>

        <?php if (isset($model->getData()['pinterest']) && !empty($model->getData()['pinterest'])) { ?>
            <div class="art-social-item"><i class="fa fa-pinterest fa-lg">&nbsp;&nbsp;&nbsp;</i><a
                    href="<?php echo $model->getData()['pinterest'] ?>">
                    <?php _e('Pinterest', 'apollo') ?>   </a></div>
        <?php } ?>

        <?php if (isset($model->getData()['donate']) && !empty($model->getData()['donate'])) { ?>
            <div class="art-social-item"><i class="fa fa-usd fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK"
                                                                                             href="<?php echo $model->getData()['donate'] ?>">
                    <?php _e('Donate', 'apollo') ?>   </a></div>
        <?php } ?>
    </div>
    <div class="el-blk location">
        <?php if (isset($model->getData()['address']) && !empty($model->getData()['address'])) { ?>
            <p><span><i class="fa fa-map-marker fa-lg">&nbsp;&nbsp;&nbsp;</i></span>
                <label><?php echo $model->getData()['address']; ?></label>
            </p>
        <?php } ?>
        <?php if (isset($model->getData()['phone']) && !empty($model->getData()['phone'])) { ?>
            <p><span><i class="fa fa-phone fa-lg">&nbsp;&nbsp;</i></span>
                <label><?php echo $model->getData()['phone']; ?></label>
            </p>
        <?php } ?>
        <?php if (isset($model->getData()['fax']) && !empty($model->getData()['fax'])) { ?>
            <p><span><i class="fa fa-fax fa-lg">&nbsp;</i></span>
                <label><?php echo $model->getData()['fax']; ?></label>
            </p>
        <?php } ?>
    </div>
    <div class="el-blk">
        <div class="b-btn fl">
            <?php echo $model->renderBookmarkBtn(array('class' => 'btn btn-b'), get_post($model->getId() )); ?>
        </div>
    </div>
</div>