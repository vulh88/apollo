<!--map -->
    <h4 class="hidden">MAKE IT A NIGHT <span> <i class="fa fa-map-marker"></i></span></h4>
    <div class="a-block-ct">
        <form id="search-map" method="POST" action="" class="search-map-frm hidden"><i class="fa fa-search fa-flip-horizontal"></i>
            <input type="text" placeholder="<?php _e( 'Find Restaurants near this venue...', 'apollo'); ?>">
        </form>
        <div <?php if ( ! $str_address_all ) echo 'style="display: none"'  ?> class="lo-left">
            <div class="lo-map" id="_event_map"
                 data-coor = "<?php echo $sCoor ?>"
                 data-address="<?php echo $str_address_all ?>"
                 data-alert="<?php _e('We\'re sorry. A map for this listing is currently unavailable.', 'apollo') ?>"
                 data-relation_id="_popup_google_map_full"
                 data-nonce="<?php echo wp_create_nonce('save-coor') ?>"
                 data-img_location="<?php echo get_stylesheet_directory_uri() ?>/assets/images/icon-location.png">
            </div>
            <p class="t-r"> <a href="javascript:void(0);" data-target="#_popup_google_map" id="_event_trigger_fullmap"><?php _e('Full map and directions','apollo'); ?></a></p>
        </div>
    </div>
<!---end map -->