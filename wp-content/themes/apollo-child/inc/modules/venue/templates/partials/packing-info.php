<?php
    //parking info + PUBLIC HOURS + PUBLIC ADMISSION FEES
    if ( isset($this->getData()['data_parking']) && is_array($this->getData()['data_parking']) && !empty($this->getData()['data_parking']) ) {
        foreach ( $this->getData()['data_parking'] as $k => $item ) {
            if( !empty($item) ) {
                $title = strtoupper(str_replace('_',' ', $k));
                ?>
                <div class="blog-bkl">
                    <div class="a-block">
                        <h4><?php echo $title ?></h4>
                        <div class="el-blk">
                            <p><?php echo $item ?></p>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
    }
?>