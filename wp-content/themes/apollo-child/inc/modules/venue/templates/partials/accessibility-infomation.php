<?php
    $access  = isset($model->getData()['data_access']) ? $model->getData()['data_access'] : '';
    if ( is_array($access) && count($access) > 0 ):
?>
<div class="blog-bkl">
    <div class="a-block venue">
        <h4><?php _e('Accessibility Information','apollo') ?></h4>
        <ul class="access-listing">
            <?php
                $columns = array(
                    'left' => 'ACLlist',
                    'right' => 'ACRlist'
                );
                foreach($columns as $col => $cssClass){
                    $accessLeft = isset($access[$col])?$access[$col]:array();
                    if(isset($accessLeft) && count($accessLeft) > 0){
                        ?>
                        <li>
                            <ul class="<?php echo $cssClass; ?>">
                                <?php
                                foreach($accessLeft as $item){?>
                                    <li><img src="<?php echo $item['link'] ?>">
                                        <label><?php echo $item['label'] ?></label>
                                    </li>
                                <?php }
                                ?>
                            </ul>
                        </li>
                    <?php
                    }
                }
            ?>
        </ul>
        <?php

          if(isset($model->getData()['accessibility_info'] ) && !empty($model->getData()['accessibility_info'] )):
        ?>
                <p><?php echo $model->getData()['accessibility_info'] ?></p>
        <?php
          endif; ?>
    </div>
</div>
<?php
    endif;