<?php

$lat = $model->getVenue()->get_meta_data(Apollo_DB_Schema::_VENUE_LATITUDE);
$lng = $model->getVenue()->get_meta_data(Apollo_DB_Schema::_VENUE_LONGITUDE);

$str_address_all = isset($model->getData()['str_address']) ? $model->getData()['str_address'] : '';

if ($lat && $lng) {
    $sCoor = "$lat,$lng";
    $arrCoor = array($lat, $lng);
} else {
    $arrCoor = Apollo_Google_Coor_Cache::getCoorForVenue($str_address_all);
    if(!is_array($arrCoor) || empty($arrCoor)){
        $arrCoor = Apollo_Google_Coor_Cache::getCoordinateByAddressInLocalDB($str_address_all);
    }
    $sCoor = $arrCoor ? implode(",", $arrCoor) : '';
}


if(!empty($str_address_all) || ($lat && $lng)): ?>
    <div class="blog-bkl">
        <div class="a-block">
            <h4><?php _e('LOCATION','apollo') ?><span><i class="fa fa-map-marker"></i></span></h4>
        </div>
        <div class="a-block-ct locatn">
            <div class="loc-address"><span><b><?php echo $model->getVenue()->get_title(); ?></b></span>
                <?php if ($str_address_all): ?>
                <p><?php echo $str_address_all; ?></p>
                <?php endif; ?>
            </div>
            <?php if(!empty($arrCoor)): ?>
            <div class="lo-left venue">
                <?php include 'map.php'; ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>





