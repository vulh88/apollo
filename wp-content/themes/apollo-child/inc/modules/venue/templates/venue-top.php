<!-- Override your template here -->
<?php
    global $apollo_modules;
    Apollo_Venue::updateAllVenueForSearch();
    $module_name = isset( $apollo_modules[get_post_type()] ) ? $apollo_modules[get_post_type()]['sing'] : '';
    $listState = Apollo_App::getStateByTerritory();
    $venueCategories = Apollo_Org::get_tree_event_style(Apollo_DB_Schema::_VENUE_PT);

    $searchData = array(
        'keyword' => isset($_GET['keyword'])?Apollo_App::clean_data_request($_GET['keyword']):'',
        'term' => isset($_GET['term'])?Apollo_App::clean_data_request($_GET['term']):'',
        'state' => isset($_GET['state'])?$_GET['state'] : of_get_option(Apollo_DB_Schema::_APL_DEFAULT_STATE,''),
        'city' => isset($_GET['city'])? urldecode($_GET['city']): of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY,''),
        'zip' => isset($_GET['zip'])?urldecode($_GET['zip']):'',
        'custom' => isset($_GET['custom'])?$_GET['custom']:array(),
        'region' => isset($_GET['region'])?urldecode($_GET['region']):'',
    );

    $cityList = Apollo_App::getCityByTerritory(false, false, true);
    $zipList = Apollo_App::getZipByTerritory(false,false, false, true);
    $regionList = Apollo_App::get_regions();
    
    $listCityState = Apollo_App::getCityStateByTerritory();

    $isTopSearchMobileVersion = isset($_GET['top-search-mobile']) && !empty($_GET['top-search-mobile']) ? 'ts-mobile' : '';
    $defaultTermValue = $model->getDefaultValueForTerm( Apollo_DB_Schema::_VENUE_PT.'-type', $searchData['term']);

?>
<div class="top-search venue-s-t-m <?php echo $isTopSearchMobileVersion; ?>">
    <div class="inner">
        <div class="top-search-row venue-s-t-m">
            <form method="get" id="search-venue-m-t" action="<?php echo home_url('/venue')?>" class="form-event">
                <div class="search-row">
                        <div class="el-blk search-input-wrapper">
                            <button type="button" class="btn btn-l s"><i class="fa fa-search fa-flip-horizontal fa-lg"></i></button>
                            <input type="text" name="keyword" value="<?php echo Apollo_App::clean_data_request(get_query_var('keyword'), true) ?>" placeholder="<?php _e('Search by Keyword', 'apollo') ?>" class="inp inp-txt event-search event-search-custom">
                            <input type="hidden" name="do_search" value="<?php echo isset($_GET['do_search']) && !empty($_GET['do_search']) ? 'yes' : 'no'; ?>" />
                        </div>
                        <div class="el-blk">
                            <div class="select-bkl">
                                <select name="term" class="chosen" id="venue-type-select">
                                    <option value=""><?php _e('Venue Type','apollo') ?></option>
                                    <?php
                                    Apollo_Org::build_option_tree($venueCategories, $defaultTermValue);
                                    ?>
                                </select>
                                <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                            </div>
                        </div>

                        <!--state -->
                        <div class="el-blk">
                            <div class="select-bkl">
                                <select class="chosen" name="state" id="venue-state-select">
                                    <?php foreach($listState as $k => $val){ ?>
                                        <option <?php selected($k, $searchData['state']) ?> value="<?php echo $k ?>"><?php echo $val ?></option>
                                    <?php } ?>
                                </select>
                                <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                            </div>
                        </div>
                        <!--end state -->

                        <div class="el-blk">
                            <div class="select-bkl">
                                <select name="city" class="chosen" id="venue-city-select">

                                    <?php foreach($listCityState as $k => $cityItem): ?>
                                        <optgroup <?php echo $k ? 'label="'.$k.'"' : '' ?>>

                                            <?php if (!$k): ?>
                                                <option value="0"><?php echo $cityItem; ?></option>
                                            <?php endif; ?>
                                            <?php if ($cityItem && is_array($cityItem)):
                                                foreach ( $cityItem as $city ):
                                                    $selected = '';
                                                    if( $city ==  $searchData['city'])
                                                        $selected = 'selected';
                                                    ?>
                                                    <option <?php echo $selected ?> value="<?php echo $city ?>"><?php echo $city ?></option>
                                                <?php endforeach; endif; ?>
                                        </optgroup>
                                    <?php endforeach; ?>

                                </select>
                                <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                            </div>
                        </div>

                        <!--start zip -->
                        <div class="el-blk">
                            <div class="select-bkl">
                                <select class="chosen" name="zip" id="venue-zip-select">

                                    <?php foreach($zipList as $k => $zipItem){
                                        $selected = '';

                                        if($k == $searchData['zip'])
                                            $selected = 'selected';
                                        ?>
                                        <option <?php echo $selected; ?> value="<?php echo urlencode($k); ?>"><?php echo $zipItem; ?></option>
                                    <?php } ?>


                                </select>
                                <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                            </div>
                        </div>
                        <!--End zip -->
                    </div>
                    <div class="search-row">
                        <!--Region -->
                        <?php if($regionList ): ?>
                            <div class="el-blk">
                                <div class="select-bkl">
                                    <select class="chosen" name="region" data-refer-to="select[data-role=zip]" data-role="region" >
                                        <?php foreach($regionList as $k => $val){
                                            $selected = '';
                                            if($k == $searchData['region'])
                                                $selected = 'selected';
                                            if(!empty($val)){
                                            ?>
                                            <option <?php echo $selected; ?> value="<?php echo $k ?>"><?php echo $val ?></option>
                                        <?php } } ?>
                                    </select>
                                    <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="el-blk accessibilyty-search">
                            <!--                    <p>--><?php //_e('Accessibility Information','apollo')?><!--</p>-->
                            <?php
                            global $apollo_event_access;
                            ?>
                            <div class="accessibilyty-icons-search">
                                <dl class="accessibilyty-dropdown">
                                    <dt>
                                        <a href="javascript:void(0)">
                                            <span class="access-drop"><?php _e('Select Accessibility', 'apollo');?></span>
                                            <p class="multiSel"> </a>
                                    </dt>
                                    <dd>
                                        <div class="mutliSelect" id="venue-accessibility-checkbox">
                                            <ul>
                                                <?php

                                                foreach($apollo_event_access as $img=>$label){
                                                    $checked = '';
                                                    if(is_array($searchData['custom']) && count($searchData['custom'])){
                                                        if(in_array($img,$searchData['custom'])){
                                                            $checked ='checked';
                                                        }
                                                    }
                                                    ?>
                                                    <li>
                                                        <input <?php echo $checked; ?> type="checkbox" value="<?php echo $img ?>" name="custom[]">
                                                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/event-accessibility/2x/<?php echo $img ?>.png">
                                                        <label><?php echo $label; ?></label></span>
                                                    </li>
                                                    <?php
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                    </dd>
                                </dl>
                            </div>
                        </div>
                        <div class="sub-blk">
                            <button type="submit" class="btn btn-l lgr fr f-w-btn-search"><?php _e('SEARCH', 'apollo') ?></button>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</div>