<?php

/**
 * Class SM_Artist_Mod
 */
require_once __DIR__ . '/inc/sm_venue_model.php';

class SM_Venue_Mod extends SM_Mod_Abstract
{
    public function __construct()
    {
        $this->renderPageTemplate();
    }

    public function renderPageTemplate()
    {
        $model = new SM_Venue_Model();
        $file = SONOMA_MODULES_DIR . '/venue/templates/venue.php';
        if (is_single()) {
            $file = SONOMA_MODULES_DIR . '/venue/templates/venue-detail.php';
        }
        $fileTemplatePart = SONOMA_MODULES_DIR . '/venue/templates/venue-top.php';

        if (file_exists($fileTemplatePart)) {
            $this->smGetTemplatePartCustom($fileTemplatePart, $model);
        }

        if (file_exists($file)) {
            $this->smGetTemplatePartCustom($file, $model);
        }
    }
}

new SM_Venue_Mod();