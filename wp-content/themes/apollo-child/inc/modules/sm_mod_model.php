<?php
/**
 * Created by PhpStorm.
 * User: truong
 * Date: 28/09/2016
 * Time: 15:12
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class SortMdArray { // this class help to sort multidimensional array by key.
    public $sort_order = 'asc'; // default
    public $sort_key = 'position'; // default

    public function sortByKey(&$array) {
        usort($array, array(__CLASS__, 'sortByKeyCallback'));
    }
    function sortByKeyCallback($a, $b) {
        $return = '';
        $v1 = is_object($a) ? $a->{$this->sort_key} : $a[$this->sort_key];
        $v2 = is_object($b) ? $b->{$this->sort_key} : $b[$this->sort_key];

        if($this->sort_order == 'asc') {
            $return = $v1 - $v2;
        } else if($this->sort_order == 'desc') {
            $return = $v2 - $v1;
        }
        return $return;
    }
}

require_once APOLLO_TEMPLATES_DIR. '/pages/lib/class-apollo-page-module.php';

class SM_Mod_Model extends  Apollo_Page_Module {


    public function __construct(){
        add_action('wp_ajax_add_bookmark', array($this, 'prefix_ajax_add_bookmark'),99);
        add_action( 'wp_ajax_nopriv_add_bookmark', array( $this, 'prefix_ajax_add_bookmark' ),99 );
        parent::__construct();

    }
    public function getCustomGalleryField(){
        global $post;
        $fields = Apollo_Custom_Field::get_fields_by_type($post->post_type, 'gallery');

        $result = array();
        if ($fields):
            $sortable = new SortMdArray();
            $sortable->sort_key = 'cf_order';
            $sortable->sortByKey($fields);
            foreach ($fields as $field):

                $label = Apollo_App::getMetaDataForGallery($post->ID, $post->post_type, 'apl_'.$field->name.'_label');
                $label = !empty($label) ? $label : $field->lablel;
                $topDesc =  Apollo_App::getMetaDataForGallery($post->ID, $post->post_type, 'apl_'.$field->name.'_desc');
                $topDesc =  !empty($topDesc) ? $topDesc : '';
                $bottomDesc =  Apollo_App::getMetaDataForGallery($post->ID, $post->post_type, 'apl_'.$field->name.'_bottom_desc');
                $bottomDesc =  !empty($bottomDesc) ? $bottomDesc : '';
                $galleriesData = Apollo_App::getMetaDataForGallery($post->ID, $post->post_type, $field->name);
                $result[] = array('label' => $label, 'top_desc' => $topDesc, 'bottom_desc' => $bottomDesc  ,'data' => $galleriesData);
            endforeach;
        endif;
        return $result;
    }

    public  function smGetTemplatePartCustom( $file, $model, $return = false ) {

        $fileBasename = basename($file);

        if ( !file_exists($file) && file_exists( get_stylesheet_directory() . '/' . $fileBasename . '.php' ) ) {
            $file = get_stylesheet_directory() . '/' . $fileBasename . '.php';
        }elseif ( !file_exists($file) && file_exists( get_template_directory() . '/' . $fileBasename . '.php' ) ){
            $file = get_template_directory() . '/' . $fileBasename . '.php';
        }
        ob_start();
        require( $file );
        $data = ob_get_clean();

        if ( $return ){
            return $data;
        }
        echo $data;
    }

    public function getSharingInfo( $additionalFields = array(), $id = 0 )
    {
        $arrShareInfo = $this->getShareInfo();
        SocialFactory::social_btns( array( 'info' => $arrShareInfo, 'id' => $id ,
            'data_btns' =>  $additionalFields  ) );
    }

    public function get_title($obj) {

        if ( ! $obj ) return false;
        return $this->title_filter($obj->post_title);
    }
    //title rule
    public  function title_filter($title){
        //we apply for listing page
        if(!is_single()){
            $maxWords = SM_Common_Const::_SM_LISTING_TITLE_MAX_TRIM_WORDS;
            if(strlen ($title) >= $maxWords){
                return substr($title,0,$maxWords).'...';
            }else{
                return $title;
            }
        }
        return $title;
    }
    public static function isEnableAddItBtn($type) {
        $key = $type.'_enable_add_it_btn';
        return of_get_option( $key ) !== '0';
    }

    public function prefix_ajax_add_bookmark() {
        global $post;
        if ( ! is_user_logged_in() || ! self::isEnableAddItBtn($post->post_type) ) {
            return wp_send_json( array('action' => 'not_login', 'message' => __('You must login to bookmark!', 'apollo')) );
        }

        $user_id    = get_current_user_id();
        $post_id    = Apollo_App::clean_data_request( $_POST['id'] );
        $post_type  = Apollo_App::clean_data_request( $_POST['post_type'] );

        if ( get_post_status($post_id) !== 'publish' ) {
            return wp_send_json( array('action' => 'not_login', 'message' => __('This event does not exist', 'apollo')) );
        }

        if ( self::has_bookmark( $post_id ) ) {
            self::remove_bookmark( $user_id, $post_id );
            $action = 'remove';
        } else {
            $issuccess = self::insert_bookmark(
                array(
                    'user_id'       => $user_id,
                    'post_id'       => $post_id,
                    'post_type'     => $post_type,
                    'bookmark_date' => date('Y-m-d'),
                )
            );
            $action = 'add';
        }

        // LOG FUNC
        $dataForLog =  array(
            'user_id' => $user_id,
            'item_id' => $post_id,
            'activity' => $action === 'add' ? Apollo_Activity_System::BOOKMARK : Apollo_Activity_System::UNBOOKMARK,

            'item_type' => $post_type,
            'url' => get_permalink($post_id),
            'title' => get_the_title($post_id),
            'timestamp' => time(),
        );
        Apollo_Event_System::trigger(Apollo_Event_System::ACTIVIY, $dataForLog);

        wp_send_json( array( 'action' => $action ) );
    }

    public static function renderBookmarkBtn($arr_params = array(), $post = ''){

        /**
         * Only display if this value empty or equal to 0
         *
         */

        if ( empty($post) ) return '';
        if ( ! self::isEnableAddItBtn($post->post_type) ) return '';

        $default = array(
            'class' => 'btn btn-category',
        );

        $arr_params = wp_parse_args($arr_params, $default);

        /* Is have bookmark */
        $has_bookmark = Apollo_User::has_bookmark( $post->ID );
        if($has_bookmark) {
            $arr_params['class'] .= ' bookmark_highlight';
        }

        $sattr = '';
        foreach($arr_params as $attn => $value) {
            $sattr .= $attn . ' = "'. $value.'"';
        }

        ob_start();
        $keyAddIt = $post->post_type.'_text_of_add_it';
        $keyAddedIt = $post->post_type.'_text_of_added_it';
        $addTxt = of_get_option($keyAddIt, __('ADD IT', 'apollo'));
        $addedTxt = of_get_option($keyAddedIt, __('ADDED', 'apollo'));
        $additTipText = __( 'Click to bookmark this listing', 'apollo' );

        ?>
        <a href="javascript:void(0);"
           onclick="ajaxAddBookmark('<?php echo admin_url("admin-ajax.php"); ?>','<?php echo $post->ID ?>','<?php echo $post->post_type ?>', this)"
           data-added-text="<?php echo $addedTxt ?>"
           data-add-text="<?php echo $addTxt ?>"
           data-addit="<?php echo ! $has_bookmark ? '1' : 0 ?>"
            <?php echo $sattr ?>>
            <span><?php echo $has_bookmark ? $addedTxt : $addTxt ?></span>
            <div class="show-tip"><?php echo $additTipText ?></div>
        </a>
        <?php

        return ob_get_clean();

    }

    public function getDefaultValueForTerm($taxonomy = '', $defaultValue){
        $term = '' ;
        if(is_single() && !empty($taxonomy)){
            global $post;
            $terms =  get_the_terms($post, $taxonomy);
            $term = !empty($terms) ? $terms[0] : '';
        }
        return !empty($term) && is_object($term) ? $term->term_id : $defaultValue;
    }




}
