<?php

class SM_View_Helper
{
    public static function handleDataPhotoItems($galleries)
    {
        $big_sliders = '';
        $thumb_sliders = '';
        $first_desc = '';

        $popup_big_sliders = '';
        $popup_thumb_sliders = '';
        if (!empty($galleries)) {
            foreach ($galleries as $k => $attachment_id) {
                $post_img = get_post($attachment_id);
                $caption = !empty($post_img) ? $post_img->post_excerpt : '';

                if ($k === 0) $first_desc = $caption;

                if (!get_attached_file($attachment_id)) continue;

                $attach_img = wp_get_attachment_image_src($attachment_id, 'large');
                $attach_thumb = wp_get_attachment_image_src($attachment_id, 'thumbnail');
                $big_sliders .= '<li><img width="' . $attach_img[1] . '" height="' . $attach_img[2] . '" src="' . $attach_img[0] . '" data-original="' . $attach_img[0] . '" ></li>';
                $thumb_sliders .= '<a data-slide-index="' . $k . '" href="" class="thumbnail-photo" data-desc="' . htmlentities($caption) . '">
                                    <img width="' . $attach_thumb[1] . '" height="' . $attach_thumb[2] . '" src="' . $attach_thumb[0] . '" >
                                </a>';

                $selected = $k == 0 ? 'selected' : '';
                $p_attach_img = wp_get_attachment_image_src($attachment_id, 'full');
                $p_attach_thumb = wp_get_attachment_image_src($attachment_id, 'medium');

                $popup_big_sliders .= '<div data-caption="' . ($post_img ? $post_img->post_excerpt : '') . '" data-srcth="' . $p_attach_thumb[0] . '" data-src="' . $p_attach_img[0] . '" data-target="' . $k . '" class="photo ' . $selected . '"></div>';

                $current_class = $k == 0 ? 'class="current"' : "";
                $popup_thumb_sliders .= '<li><a href="#" ' . $current_class . ' data-id="' . $k . '"><img src="' . $p_attach_thumb[0] . '" ></a></li>';

            }

            return array(
                'first_desc'    => nl2br($first_desc),
                'big_sliders'    => $big_sliders,
                'thumb_sliders'    => $thumb_sliders,
                'popup_big_sliders'    => $popup_big_sliders,
                'popup_thumb_sliders'    => $popup_thumb_sliders,
                'total' => count($galleries)
            );
        }
    }

    public static function handleDataVideos($videos){
        $first_desc_video = '';
        $thumbs = '';
        $videoIframe  = '';

        if ( !empty($videos)){
            foreach ( $videos as $k => $v ):

                if ( $k === 0 ) $first_desc_video = $v['desc'];

                if ( ! $v['embed'] ) continue;
                if(Apollo_App::is_vimeo($v['embed'])  ){
                    $matches= array();
                    if(!strpos($v['embed'], "player")) {
                        preg_match('/\/\/(www\.)?vimeo.com\/(\d+)($|\/)/', $v['embed'], $matches);
                        if (!empty($matches))
                            $v['embed'] = "https://player.vimeo.com/video/" . $matches[2];
                    }
                }
                $videoObj = new Apollo_Video($v['embed']);
                $code = $videoObj->getCode();

                if ( ! $code ) continue;
                $vimeoUrl = Apollo_App::is_vimeo($v['embed']) ? $v['embed'] : '';
                $thumbs .= '<a data-vimeo="'.$vimeoUrl.'" data-desc="'.(nl2br($v['desc'])).'" data-src="'.$videoObj->getSrc().'" data-code="'.$code.'" data-slide-index="'.$k.'" href="">
                    <img class="lazy" data-original=" '. $videoObj->getThumb(). '">
                </a>';

                if ( $k == 0 ):
                    $videoIframe .= '<div class="video-wrapper"><div class="lazyYT" data-youtube-id="'.$code.'" id="player">';
                    if ( ! $k && Apollo_App::is_vimeo( $v['embed'] ) ) {
                        $videoIframe .= '<iframe id="player" frameborder="0" allowfullscreen="1" title="" width="640" height="360" src="' . $v['embed'] . '"></iframe>';

                    }
                    $videoIframe .= '</div></div>';

                endif;

            endforeach;
        }
        return array(
            'video_iframe'  => $videoIframe,
            'first_desc_video'  => $first_desc_video,
            'thumb'  => $thumbs,
            'total'  => count($videos),
        );
    }
}