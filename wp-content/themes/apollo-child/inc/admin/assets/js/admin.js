/**
 * Created by truong on 26/09/2016.
 */

(function ($) {

    var SonomaGallery = {
        handleAddSportLightGallery: function (e, $el) {
            e.preventDefault();

            var $image_gallery_ids = $('#event_image_gallery');
            var $event_images = $('#event_images_container ul.event_images');
            var attachment_ids = $image_gallery_ids.val();

            var event_gallery_frame = SonomaGallery.openMediaFrame($el);

            // When an image is selected, run a callback.
            event_gallery_frame.on('select', function () {
                var selection = event_gallery_frame.state().get('selection');
                var error = false;

                selection.map(function (attachment) {
                    attachment = attachment.toJSON();
                    if (attachment.height < SM_ADMIN.circle_image_size_h || attachment.width < SM_ADMIN.circle_image_size_w) {
                        $('.media-frame-content li[data-id = ' + attachment.id + ']').addClass('attachment-error');
                        error = true;
                    } else if (attachment.id) {
                        attachment_ids = attachment_ids ? attachment_ids + "," + attachment.id : attachment.id;

                        $event_images.append('\
                        <li class="image" data-attachment_id="' + attachment.id + '">\
                                <img src="' + attachment.url + '" />\
                                <ul class="actions">\
                                        <li><a href="#" class="delete" title="' + $el.data('delete') + '">' + $el.data('text') + '</a></li>\
                                </ul>\
                        </li>');
                    }
                });

                if (error) {
                    alert(SM_ADMIN.gallery_error);
                    event_gallery_frame.open();
                }

                $image_gallery_ids.val(attachment_ids);
            });
        },
        openMediaFrame: function (el) {
            // Create the media frame.
            var media_frame = wp.media.frames.event_gallery = wp.media({
                // Set the title of the modal.
                title: el.data('choose'),
                button: {
                    text: el.data('update')
                },
                library: {
                    type: 'image'
                },
                multiple: true
            });

            // Finally, open the modal.
            media_frame.open();

            return media_frame;
        },
        bindEventToElement: function () {
            $('.add_spotlight_images').on('click', 'a', function (event) {
                SonomaGallery.handleAddSportLightGallery(event, $(this));
            });
        }
    };

    $(document).ready(function () {
        SonomaGallery.bindEventToElement();
    });
})(jQuery);