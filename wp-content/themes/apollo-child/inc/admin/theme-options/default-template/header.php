
<div class="header header-wrapper clearfix">
    <div class="logo"><a href="/" class="logo-background"><img src="/wp-content/themes/apollo-child/assets/images/demo/logo_white.png" alt="Creative Sonoma logo"></a></div>
    <a href="javascript:;" class="hamburger-menu"><i class="uk-icon-navicon"></i></a>
    <div class="navi">
        <div class="navi-first-line clearfix">
            <ul class="menu">
                <li class="menu-item"><a href="/our-work" data-menu-name="our-work">Our Work</a></li>
                <li class="menu-item"><a href="/grants" data-menu-name="grants">Grants</a></li>
                <li class="menu-item "><a href="/whats-happening" data-menu-name="whats-happening">What's Happenning?</a></li>
                <li class="menu-item"><a href="/good-stuff" data-menu-name="good-stuff">Good Stuff</a></li>
                <li class="menu-item has-child"><a href="/connect" data-menu-name="connect">Connect</a>
                    <div class="submenu">
                        <ul class="menu">
                            <li class="menu-item "><a href="javascript:;" data-menu-name="connect">Submenu item1</a></li>
                            <li class="menu-item"><a href="javascript:;" data-menu-name="connect">Submenu item2</a></li>
                            <li class="menu-item"><a href="javascript:;" data-menu-name="connect">Submenu item3</a></li>
                            <li class="menu-item"><a href="javascript:;" data-menu-name="connect">Submenu item4</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
        <div class="navi-second-line clearfix">
            <ul class="menu">
                <li class="menu-item"><a href="/about-us" data-menu-name="about-us">Creative Sonoma</a></li>
                <li class="menu-item"><a href="/creatives-at-work" data-menu-name="creatives-at-work">Creatives at Work</a></li>
                <li class="menu-item"><a href="/art-right-here" data-menu-name="art-right-here">Art Right Here</a></li>
                <li class="menu-item"><a href="/marketplace" data-menu-name="marketplace">Marketplace</a></li>
            </ul>
        </div>
    </div>
    <div class="gov_logo">
        <a href="/" class="gov-logo-background"><img src="http://www.creativesonoma.org/wp-content/uploads/sites/www.creativesonoma.org/images/2016/11/County-Seal-Color-with-background.jpg" alt="County of Sonoma, California"></a>
    </div>
</div>