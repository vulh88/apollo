<div class="ft-top-wrap">
    <div class="ft-contact clearfix">
        <div class="contact-info"><h3 class="ttl-info">CREATIVE SONOMA</h3>
            <p>The mission of Creative Sonoma is to support and advance the creative community of Sonoma
                County.</div>
        <div class="contact-list">
            <ul>
                <li><a href="#">Contact Us</a>
                <li><a href="#">Privacy Policy</a>
                <li><a href="#">Terms of Service</a>
                <li><a href="#">Site Map</a>
            </ul>
        </div>
        <div class="contact-form"><h3 class="ttl-sign-up">RECEIVE OUR EMAIL NEWSLETTER</h3>
            <div class="frm-sign-up clearfix">
                <form action="https://visitor.r20.constantcontact.com/d.jsp?llr=fwluzdmab&p=oi&m=1112524074693&sit=jdceq4thb&f=88aa5dc2-a677-49dc-9af3-f986c94058d4" method="post" name="ccoptin" target="_blank">
                    <input name="llr" value="mq77wlcab" type="hidden" />
                    <input name="m" value="1102052225548" type="hidden" />
                    <input name="p" value="oi" type="hidden" />
                    <input placeholder="email address is required" class="ipt" name="ea" type="email" />
                    <input name="sit" value="zak945edb" type="hidden" />
                    <input class="button"  value="SIGN ME UP"/>
                </form>
            </div>
        </div>
    </div>
    <p class="coppy-right">© 2016 Creative Sonoma. All Rights Reserved.</div>