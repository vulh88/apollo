<div class="block block-feature teaser teaser-left clearfix" data-uk-scrollspy="{cls:'uk-animation-fade'}" id="feature-block"
     style="background-image: url('wp-content/themes/apollo-child/assets/images/demo/next_lv_01.jpg');">
    <div class=teaser-content>
        <h1>Featured</h1>
        <h2 >Sub-Title Goes Here</h2>
        <div class=content >
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
            laboris nisi ut aliquip ex ea commodo consequat.
        </div>
        <a href=javascript class="btn btn-viewmore">Read More</a>
    </div>
</div>
