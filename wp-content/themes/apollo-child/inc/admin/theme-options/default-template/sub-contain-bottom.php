<h1>WHAT'S HAPPENING</h1>
<h2>Latest News</h2>
<div class="article article-circle">
    <div class="article-item item-orange">
        <div class="article-title"><img src="wp-content/themes/apollo-child/assets/images/demo/SR-Symph-Girl-Guitar.jpg" class="teaser-bg" alt="">
            <div class="content-teaser">
                <p><span>TITLE OF ARTICLE</span><br>
                    <small>AUG 8, 2016</small>
            </div>
        </div>
    </div>
    <div class="article-item item-blue">
        <div class="article-title"><img src="wp-content/themes/apollo-child/assets/images/demo/Transcendance-Theatre-Rehersal-2.jpg" class="teaser-bg"
                                      alt="">
            <div class="content-teaser">
                <p><span>TITLE OF ARTICLE</span><br>
                    <small>AUG 8, 2016</small>
            </div>
        </div>
    </div>
    <div class="article-item item-green">
        <div class="article-title"><img src="wp-content/themes/apollo-child/assets/images/demo/img_detail_01.jpg" class="teaser-bg" alt="">
            <div class="content-teaser">
                <p><span>TITLE OF ARTICLE</span><br>
                    <small>AUG 8, 2016</small>
            </div>
        </div>
    </div>
</div>
<div class="btm-slider-wrap">
    <div class="bottom-slider">
        <ul class="slides">
            <li>
                <img src="wp-content/themes/apollo-child/assets/images/demo/Transcendence Theatre Rehersal.jpg" />
            </li>
            <li>
                <img src="wp-content/themes/apollo-child/assets/images/demo/Transcendace Theatre Group Shot 4.jpg" />
            </li>
            <li>
                <img src="wp-content/themes/apollo-child/assets/images/demo/Transcendence Theatre Rehersal.jpg" />
            </li>
            <li >
                <img src="wp-content/themes/apollo-child/assets/images/demo/Transcendace Theatre Group Shot 4.jpg" />
            </li>
            <li>
                <img src="wp-content/themes/apollo-child/assets/images/demo/HCA by Vanessa.JPG" />
            </li>
            <li>
                <img src="wp-content/themes/apollo-child/assets/images/demo/Transcendance-Theatre-Rehersal-2.jpg"/>
            </li>
        </ul>
    </div>
    <div class="cust-nav">
        <a href="#" class="sli-icon flex-prev icon-prev">
            <i class="icon ico-prev"></i>
        </a>
        <a href="#" class="sli-icon flex-next icon-next">
            <i class="icon ico-next"></i>
        </a>
    </div>
</div>