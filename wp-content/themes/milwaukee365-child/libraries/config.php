<?php
define('SONOMA_THEME_NAME', 'apollo-child');
define('SONOMA_FRONTEND_ASSETS_URI', sm_get_stylesheet_directory_uri().'/assets' );

/**
 * Define Directory Location Constants
 */
define( 'SONOMA_PARENT_DIR', sm_get_stylesheet_directory() );

define( 'SONOMA_INCLUDES_DIR', SONOMA_PARENT_DIR. '/inc' );
define( 'SONOMA_ADMIN_DIR', SONOMA_INCLUDES_DIR . '/admin' );
define( 'SONOMA_MODULES_DIR', SONOMA_INCLUDES_DIR . '/modules' );
define( 'SONOMA_HELPER_DIR', SONOMA_INCLUDES_DIR . '/helpers' );

define( 'SONOMA_EVENT_INCLUDES_DIR', SONOMA_INCLUDES_DIR. '/modules/event' );
define( 'SONOMA_EVENT_TEMPLATE_DIR', SONOMA_EVENT_INCLUDES_DIR. '/templates' );
define( 'SONOMA_ORG_INCLUDES_DIR', SONOMA_INCLUDES_DIR. '/modules/organization' );
define( 'SONOMA_ORG_TEMPLATE_DIR', SONOMA_ORG_INCLUDES_DIR. '/templates' );

/**
 * Default template
 */


define( 'SONOMA_THEME_HEADER', SONOMA_ADMIN_DIR. '/theme-options/default-template/header.php' );
define( 'SONOMA_THEME_TOP_HEADER', SONOMA_ADMIN_DIR. '/theme-options/default-template/top-header.php' );
define( 'SONOMA_THEME_FOOTER1', SONOMA_ADMIN_DIR. '/theme-options/default-template/footer1.php' );
define( 'SONOMA_THEME_FOOTER2', SONOMA_ADMIN_DIR. '/theme-options/default-template/footer2.php' );
define( 'SONOMA_THEME_FOOTER3', SONOMA_ADMIN_DIR. '/theme-options/default-template/siteinfo.php' );
define( 'SONOMA_THEME_FOOTER4', SONOMA_ADMIN_DIR. '/theme-options/default-template/footer.php' );
define( 'SONOMA_THEME_FOOTER5', SONOMA_ADMIN_DIR. '/theme-options/default-template/footer5.php' );
define( 'SONOMA_THEME_SUB_CONTAIN_TOP', SONOMA_ADMIN_DIR. '/theme-options/default-template/sub-contain-top.php' );
define( 'SONOMA_THEME_SUB_CONTAIN_BOTTOM', SONOMA_ADMIN_DIR. '/theme-options/default-template/sub-contain-bottom.php' );
define( 'SONOMA_THEME_OVERRIDE_CSS', SONOMA_ADMIN_DIR. '/theme-options/default-template/override.less' );



$GLOBALS['sonoma_theme_options_default_templates'] = array(
    Apollo_DB_Schema::_HEADER => SONOMA_THEME_HEADER ,
    Apollo_DB_Schema::_FOOTER3 => SONOMA_THEME_FOOTER3,
    Apollo_DB_Schema::_FOOTER4 => SONOMA_THEME_FOOTER4,
    Apollo_DB_Schema::_BOTTOM_SUB_CONTAIN => SONOMA_THEME_SUB_CONTAIN_BOTTOM,
    Apollo_DB_Schema::_TOP_SUB_CONTAIN => SONOMA_THEME_SUB_CONTAIN_TOP,
    Apollo_DB_Schema::_FOOTER1 => SONOMA_THEME_FOOTER1,
);

$GLOBALS['sonoma_array_taxonomy'] = array(
    'venue-type' => Apollo_DB_Schema::_VENUE_PT,
);

$GLOBALS['sonoma_array_fields_hidden'] = array();
/**
 * Define URL Location Constants
 */
define( 'SONOMA_PARENT_URL', sm_get_stylesheet_directory_uri() );

define( 'SONOMA_ASSETS', SONOMA_PARENT_URL. '/assets' );

define( 'SONOMA_CSS_URL', SONOMA_ASSETS . '/css' );
define( 'SONOMA_JS_URL', SONOMA_ASSETS . '/js' );
define( 'SONOMA_IMAGES_URL', SONOMA_ASSETS . '/images' );

define( 'SONOMA_INCLUDES_URL', SONOMA_PARENT_URL. '/inc' );
define( 'SONOMA_ADMIN_URL', SONOMA_INCLUDES_URL. '/admin' );

define( 'SONOMA_ADMIN_ASSETS', SONOMA_ADMIN_URL. '/assets' );

define( 'SONOMA_ADMIN_CSS_URL', SONOMA_ADMIN_ASSETS . '/css' );
define( 'SONOMA_ADMIN_JS_URL', SONOMA_ADMIN_ASSETS . '/js' );
define( 'SONOMA_ADMIN_IMAGES_URL', SONOMA_ADMIN_ASSETS . '/images' );

define( 'SONOMA_DIRECTORY_LISTING_DEFAULT_VIEW_TYPE', 1 ); // default view type = 1 : 'thumbs' : 2 : 'lists'

/* ThienLD : define circle image size width & height */
define ( 'SONOMA_CIRCLE_IMAGE_WIDTH', 250);
define ( 'SONOMA_CIRCLE_IMAGE_HEIGHT', 250);


define( 'MILWAUKEE365_CSS_VERSION', '1.5' );


$GLOBALS['sonoma_available_modules'] = array(
    Apollo_DB_Schema::_VENUE_PT
);