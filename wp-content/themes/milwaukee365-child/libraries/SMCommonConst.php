<?php

class SMCommonConst
{
    const _SM_PAGE_DESCRIPTION = '_sm_page_description';
    const _SM_PAGE_TITLE = '_sm_page_title';
    const _SM_ITEMS_NUM_LISTING_PAGE = 25;
    const _SM_SINGLE_CONTENT_POST_TYPE = '_sm_content_post_type';
    const _SM_PUBLIC_ART_PT = 'public-art';
    const _SM_LISTING_TITLE_MAX_TRIM_WORDS = 60;
    const _SM_LISTING_DESC_MAX_TRIM_WORDS = 220;
}