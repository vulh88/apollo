<?php
/**
 * Scripts and stylesheets
 */

// include custom jQuery
// function updatejQuery() {

//     wp_deregister_script('jquery');
//     wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', array(), null, true);

// }
// add_action('wp_enqueue_scripts', 'updatejQuery');



function milwaukee365_scripts() {
    global $post;
    $themeUri = sm_get_stylesheet_directory_uri();
    $url_slug = isset($post) && !empty($post) ? $post->post_name : '';
    $url_slug = stripslashes($url_slug);
    $assetCss = array(
        'milwaukee365-style-css' => $themeUri . '/assets/css/style.css',
        'milwaukee365-style-dev-css' => $themeUri . '/style.css',
    );

    // $assetJS = array(
    //     'milwaukee365-functions-js'   => '/assets/js/functions.js',
    // );

    wp_dequeue_style('apollo_primary_color_css');

    foreach ( $assetCss as $key => $css ) {
        wp_enqueue_style($key, $css, false, MILWAUKEE365_CSS_VERSION);
    }

    wp_enqueue_style('milwaukee365_primary_color_css', get_primary_color_css_url(), false, null);
    // wp_enqueue_script('milwaukee365-functions-js', $themeUri. $assetJS['milwaukee365-functions-js'], array('jquery'), null,  true);


    // Add additional js
    // $addiJsFile = get_html_file_url(Apollo_DB_Schema::_ADDITIONAL_JS, 'js');
    // if ($addiJsFile) {
    //     wp_enqueue_script('milwaukee365-additional-js', $addiJsFile, array('jquery'), null,  true);
    // }


    if ( of_get_option( Apollo_DB_Schema::_ACTIVATE_OVERRIDE_CSS ) && file_exists(get_override_css_abs_path() ) ) {
        wp_dequeue_style('apollo_override_css');
        wp_enqueue_style('apollo_override_css', get_override_css_url(), false, null);
    }


}
add_action('wp_enqueue_scripts', 'milwaukee365_scripts', 105);