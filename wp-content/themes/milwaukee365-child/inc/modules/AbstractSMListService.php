<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

require_once APOLLO_TEMPLATES_DIR. '/pages/lib/class-apollo-page-module.php';

abstract class AbstractSMListService extends Apollo_Page_Module {

    /**
     * Set template
     */
    abstract public function setTemplate();

    /**
     * Set page size
     */
    abstract public function setPageSize();


    public function __construct(){
        parent::__construct();

        $this->setTemplate();
        $this->setPageSize();
    }

    /**
     * Return HTML template
     * @return string
     */
    public function render_html()
    {
        return SMViewHelper::smGetTemplatePartCustom($this->template, $this, true);
    }

    public function getDefaultValueForTerm($taxonomy = '', $defaultValue){
        $term = '' ;
        if(is_single() && !empty($taxonomy)){
            global $post;
            $terms =  get_the_terms($post, $taxonomy);
            $term = !empty($terms) ? $terms[0] : '';
        }
        return !empty($term) && is_object($term) ? $term->term_id : $defaultValue;
    }
}
