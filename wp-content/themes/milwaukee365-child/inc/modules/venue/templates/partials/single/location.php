<?php

$lat = $model->getSinglePost()->get_meta_data(Apollo_DB_Schema::_VENUE_LATITUDE);
$lng = $model->getSinglePost()->get_meta_data(Apollo_DB_Schema::_VENUE_LONGITUDE);

$address = $model->extraInfo['data']['str_address'];

if ($lat && $lng) {
    $sCoor = "$lat,$lng";
    $arrCoor = array($lat, $lng);
} else {
    $arrCoor = Apollo_Google_Coor_Cache::getCoorForVenue($address);
    if(!is_array($arrCoor) || empty($arrCoor)){
        $arrCoor = Apollo_Google_Coor_Cache::getCoordinateByAddressInLocalDB($address);
    }
    $sCoor = $arrCoor ? implode(",", $arrCoor) : '';
}


if(!empty($address) || ($lat && $lng)): ?>
    <div class="blog-bkl">
        <div class="a-block">
            <h4><?php _e('LOCATION','apollo') ?><span><i class="fa fa-map-marker"></i></span></h4>
        </div>
        <div class="a-block-ct locatn">
            <div class="loc-address"><span><b><?php echo $model->getSinglePost()->get_title(); ?></b></span>
                <?php if ($address): ?>
                <p><?php echo $address; ?></p>
                <?php endif; ?>
            </div>
            <?php if(!empty($arrCoor)): ?>
            <div class="lo-left venue">
                <?php include 'map.php'; ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>





