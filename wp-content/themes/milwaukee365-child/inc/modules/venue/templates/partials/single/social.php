<div class="art-social">
    <div class="el-blk">
        <?php if (isset($model->extraInfo['data']['email']) && !empty($model->extraInfo['data']['email'])) { ?>
            <div class="art-social-item"><i class="fa fa-envelope fa-lg">&nbsp;&nbsp;&nbsp;</i><a
                    href="mailto:<?php echo $model->extraInfo['data']['email'] ?>">
                    <?php _e('Email', 'apollo') ?></a>
                <div class="slash">/</div>
            </div>
        <?php } ?>

        <?php if (isset($model->extraInfo['data']['web']) && !empty($model->extraInfo['data']['web'])) { ?>
            <div class="art-social-item"><i class="fa fa-link fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK"
                                                                                              href="<?php echo $model->extraInfo['data']['web'] ?>">
                    <?php _e('Website', 'apollo') ?></a>
                <div class="slash">/</div>
            </div>
        <?php } ?>

        <?php if (isset($model->extraInfo['data']['blog']) && !empty($model->extraInfo['data']['blog'])) { ?>
            <div class="art-social-item"><i class="fa fa-star fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK"
                                                                                              href="<?php echo $model->extraInfo['data']['blog'] ?>">
                    <?php _e('Blog', 'apollo') ?></a>
                <div class="slash">/</div>
            </div>
        <?php } ?>

        <?php if (isset($model->extraInfo['data']['facebook']) && !empty($model->extraInfo['data']['facebook'])) { ?>
            <div class="art-social-item"><i class="fa fa-facebook fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK"
                                                                                                  href="<?php echo $model->extraInfo['data']['facebook'] ?>">
                    <?php _e('Facebook', 'apollo') ?></a>
                <div class="slash">/</div>
            </div>
        <?php } ?>

        <?php if (isset($model->extraInfo['data']['twitter']) && !empty($model->extraInfo['data']['twitter'])) { ?>
            <div class="art-social-item"><i class="fa fa-twitter fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK"
                                                                                                 href="<?php echo $model->extraInfo['data']['twitter'] ?>">
                    <?php _e('Twitter', 'apollo') ?></a>
                <div class="slash">/</div>
            </div>
        <?php } ?>

        <?php if (isset($model->extraInfo['data']['inst']) && !empty($model->extraInfo['data']['inst'])) { ?>
            <div class="art-social-item"><i class="fa fa-camera-retro fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK"
                                                                                                      href="<?php echo $model->extraInfo['data']['inst'] ?>">
                    <?php _e('Instagram', 'apollo') ?>  </a>
                <div class="slash">/</div>
            </div>
        <?php } ?>

        <?php if (isset($model->extraInfo['data']['linked']) && !empty($model->extraInfo['data']['linked'])) { ?>
            <div class="art-social-item"><i class="fa fa-linkedin-square fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK"
                                                                                                         href="<?php echo $model->extraInfo['data']['linked'] ?>">
                    <?php _e('LinkedIn', 'apollo') ?>   </a>
                <div class="slash">/</div>
            </div>
        <?php } ?>

        <?php if (isset($model->extraInfo['data']['pinterest']) && !empty($model->extraInfo['data']['pinterest'])) { ?>
            <div class="art-social-item"><i class="fa fa-pinterest fa-lg">&nbsp;&nbsp;&nbsp;</i><a
                    href="<?php echo $model->extraInfo['data']['pinterest'] ?>">
                    <?php _e('Pinterest', 'apollo') ?>   </a></div>
        <?php } ?>

        <?php if (isset($model->extraInfo['data']['donate']) && !empty($model->extraInfo['data']['donate'])) { ?>
            <div class="art-social-item"><i class="fa fa-usd fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK"
                                                                                             href="<?php echo $model->extraInfo['data']()['donate'] ?>">
                    <?php _e('Donate', 'apollo') ?>   </a></div>
        <?php } ?>
    </div>
    <div class="el-blk location">
        <?php if (isset($model->extraInfo['data']['address']) && !empty($model->extraInfo['data']['address'])) { ?>
            <p><span><i class="fa fa-map-marker fa-lg">&nbsp;&nbsp;&nbsp;</i></span>
                <label><?php echo $model->extraInfo['data']['address']; ?></label>
            </p>
        <?php } ?>
        <?php if (isset($model->extraInfo['data']['phone']) && !empty($model->extraInfo['data']['phone'])) { ?>
            <p><span><i class="fa fa-phone fa-lg">&nbsp;&nbsp;</i></span>
                <label><?php echo $model->extraInfo['data']['phone']; ?></label>
            </p>
        <?php } ?>
        <?php if (isset($model->extraInfo['data']['fax']) && !empty($model->extraInfo['data']['fax'])) { ?>
            <p><span><i class="fa fa-fax fa-lg">&nbsp;</i></span>
                <label><?php echo $model->extraInfo['data']['fax']; ?></label>
            </p>
        <?php } ?>
    </div>
    <div class="el-blk">
        <div class="b-btn fl">
            <?php echo $model->renderBookmarkBtn(array('class' => 'btn btn-b'), get_post($model->getId() )); ?>
        </div>
    </div>
</div>