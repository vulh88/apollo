<?php
    //parking info + PUBLIC HOURS + PUBLIC ADMISSION FEES
    if ( isset($model->extraInfo['data_parking']) && is_array($model->extraInfo['data_parking']) && !empty($model->extraInfo['data_parking']) ) {
        foreach ( $model->extraInfo['data_parking'] as $k => $item ) {
            if( !empty($item) ) {
                $title = strtoupper(str_replace('_',' ', $k));
                ?>
                <div class="blog-bkl">
                    <div class="a-block">
                        <h4><?php echo $title ?></h4>
                        <div class="el-blk">
                            <p><?php echo $item ?></p>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
    }
?>