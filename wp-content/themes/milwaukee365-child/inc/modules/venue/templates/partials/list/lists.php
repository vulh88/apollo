<?php
if ($results = $model->get_results()) {
    foreach ($results as $p):

        $e = get_venue($p);
        $summary = $e->get_summary(SMCommonConst::_SM_LISTING_DESC_MAX_TRIM_WORDS);
        $image = '';
        if (has_post_thumbnail($e->id)) {
            $image = $e->get_image( 'medium', array(),
                array(
                    'aw' => true,
                    'ah' => true,
                ),
                'normal'
            );
        }
        ?>
        <li>
            <div class="search-img <?php echo !$image ? 'no-place-holder-cate' : ''; ?>"><a href="<?php echo $e->get_permalink() ?>"><?php echo $image; ?></a></div>
            <div class="info-content" data-url="<?php echo $e->get_permalink() ?>" data-type="link">
                <div class="search-info"><a href="<?php echo $e->get_permalink() ?>"><span class="ev-tt"><?php echo $e->get_title() ?></span></a>
                    <div class="career"><?php echo $e->generate_categories(); ?> </div>
                    <div class="s-desc"><?php echo $summary['text']  ?> <?php echo $summary['have_more'] ? '...' : '' ?></div>
                </div>
            </div>
        </li>

    <?php endforeach;

} else if (!isset($_GET['s']) && !isset($_GET['keyword'])) {
    _e('No results', 'apollo');
}