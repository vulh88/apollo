<div class="blog-bkl organization clearfix">
    <h1 class="p-ttl"><?php echo $model->getSinglePost()->get_title() ?></h1>
    <div class="meta auth venue">
        <?php  $model->renderType();?>
    </div>
    <?php $model->getSinglePost()->renderIcons($model->extraInfo['data']['icon_fields'], $model->extraInfo['data']['icons']); ?>

    <div class="rating-box rating-action rating-art">
        <?php if (of_get_option(Apollo_DB_Schema::_ENABLE_THUMBS_UP, 1)) : ?>
            <div class="like"
                 id="_event_rating_<?php echo $model->getId() . '_' . Apollo_DB_Schema::_VENUE_PT ?>"
                 data-ride="ap-event_rating" data-i="<?php echo $model->getId() ?>"
                 data-t="<?php echo Apollo_DB_Schema::_VENUE_PT ?>">
                <a href="javascript:void(0);"><span class="_count">&nbsp;</span></a></div>
        <?php endif ?>
        <?php if ($model->isAllowComment()): ?>
            <div class="cm-comment"><a href="#event_comment_block" data-uk-smooth-scroll="{offset: 121}"><?php _e('Comment', 'apollo') ?></a></div>
        <?php endif; ?>
    </div>
    <div class="el-blk clearfix">
        <div class="art-pic">
            <?php
            echo $model->getSinglePost()->get_image('medium');
            ?>
        </div>
        <?php require (SONOMA_INCLUDES_DIR. '/modules/venue/templates/partials/single/social.php'); ?>
    </div>
    <div class="el-blk clearfix">
        <div class="art-desc apl-internal-content rich-txt rich-txt--hight-first-child">
            <?php echo $model->getSinglePost()->get_full_content(); ?>
        </div>
    </div>
</div>

<?php
    require (SONOMA_INCLUDES_DIR. '/modules/venue/templates/partials/single/location.php');
    require (SONOMA_INCLUDES_DIR. '/modules/venue/templates/partials/single/accessibility-infomation.php');
    require (SONOMA_INCLUDES_DIR. '/modules/venue/templates/partials/single/packing-info.php');
?>