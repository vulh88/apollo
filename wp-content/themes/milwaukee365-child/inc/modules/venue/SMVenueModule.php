<?php

/**
 * Class SMVenueModule
 */
class SMVenueModule
{
    public function __construct()
    {
        $this->renderPageTemplate();
    }

    public function renderPageTemplate()
    {
        if (is_single()) {
            require_once __DIR__ . '/inc/SMVenueService.php';
            $service = new SMVenueService();
            $file = SONOMA_MODULES_DIR . '/venue/templates/venue.php';
        }
        else { // Listing or search page
            require_once __DIR__ . '/inc/SMVenuesService.php';
            $service = new SMVenuesService();
            $file = SONOMA_MODULES_DIR . '/venue/templates/venues.php';
        }

        if (file_exists($file)) {
            SMViewHelper::smGetTemplatePartCustom($file, $service);
        }
    }
}

new SMVenueModule();