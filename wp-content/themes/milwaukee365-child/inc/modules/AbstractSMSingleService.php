<?php
/**
 * Created by PhpStorm.
 * User: truong
 * Date: 28/09/2016
 * Time: 15:12
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

abstract class AbstractSMSingleService  {

    /**
     * @var $single
     */
    protected $single;

    /**
     * @var $id
     */
    protected $id;

    /**
     * @var $extraInfo
     */
    public $extraInfo;

    /**
     * @var $videoGallery
     */
    protected $videoGallery;

    /**
     * @var $photoGallery
     */
    protected $photoGallery;

    /**
     * Set id of the current post type
     * @param integer $id
     * @return mixed
     */
    public abstract function setId($id);

    /**
     * Get single post type
     * @return mixed
     */
    public abstract function setSinglePost();

    /**
     * Set video gallery
     * @return mixed
     */
    public abstract function setVideoGallery();

    /**
     * Set photo gallery
     * @return mixed
     */
    public abstract function setPhotoGallery();

    /**
     * Set extra info
     * @return mixed
     */
    public abstract function setExtraInfo();


    public function __construct(){
        add_action('wp_ajax_add_bookmark', array($this, 'prefix_ajax_add_bookmark'),99);
        add_action( 'wp_ajax_nopriv_add_bookmark', array( $this, 'prefix_ajax_add_bookmark' ),99 );

        $this->setSinglePost();

        $id = !empty($this->single->id) ? $this->single->id : '';
        $this->setId($id);

        $this->setExtraInfo();
        $this->setVideoPhoto();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function getSinglePost()
    {
        return $this->single;
    }

    public function getSharingInfo($addition_field = [], $id = 0)
    {
        SocialFactory::social_btns(array(
            'info' => $this->getSinglePost()->getShareInfo(),
            'id' => $id,
            'data_btns' => $addition_field
        ));
    }

    public function renderType()
    {
        $orgType = $this->getSinglePost()->generate_categories();
        if ($orgType) {
            echo '<div class="org-type">' . $orgType . ' </div>';
        }
    }

    public static function isEnableAddItBtn($type) {
        $key = $type.'_enable_add_it_btn';
        return of_get_option( $key ) !== '0';
    }

    public function prefix_ajax_add_bookmark() {
        global $post;
        if ( ! is_user_logged_in() || ! self::isEnableAddItBtn($post->post_type) ) {
            return wp_send_json( array('action' => 'not_login', 'message' => __('You must login to bookmark!', 'apollo')) );
        }

        $user_id    = get_current_user_id();
        $post_id    = Apollo_App::clean_data_request( $_POST['id'] );
        $post_type  = Apollo_App::clean_data_request( $_POST['post_type'] );

        if ( get_post_status($post_id) !== 'publish' ) {
            return wp_send_json( array('action' => 'not_login', 'message' => __('This event does not exist', 'apollo')) );
        }

        if ( self::has_bookmark( $post_id ) ) {
            self::remove_bookmark( $user_id, $post_id );
            $action = 'remove';
        } else {
            $issuccess = self::insert_bookmark(
                array(
                    'user_id'       => $user_id,
                    'post_id'       => $post_id,
                    'post_type'     => $post_type,
                    'bookmark_date' => date('Y-m-d'),
                )
            );
            $action = 'add';
        }

        // LOG FUNC
        $dataForLog =  array(
            'user_id' => $user_id,
            'item_id' => $post_id,
            'activity' => $action === 'add' ? Apollo_Activity_System::BOOKMARK : Apollo_Activity_System::UNBOOKMARK,

            'item_type' => $post_type,
            'url' => get_permalink($post_id),
            'title' => get_the_title($post_id),
            'timestamp' => time(),
        );
        Apollo_Event_System::trigger(Apollo_Event_System::ACTIVIY, $dataForLog);

        wp_send_json( array( 'action' => $action ) );
    }

    public static function renderBookmarkBtn($arr_params = array(), $post = ''){

        /**
         * Only display if this value empty or equal to 0
         *
         */

        if ( empty($post) ) return '';
        if ( ! self::isEnableAddItBtn($post->post_type) ) return '';

        $default = array(
            'class' => 'btn btn-category',
        );

        $arr_params = wp_parse_args($arr_params, $default);

        /* Is have bookmark */
        $has_bookmark = Apollo_User::has_bookmark( $post->ID );
        if($has_bookmark) {
            $arr_params['class'] .= ' bookmark_highlight';
        }

        $sattr = '';
        foreach($arr_params as $attn => $value) {
            $sattr .= $attn . ' = "'. $value.'"';
        }

        ob_start();
        $keyAddIt = $post->post_type.'_text_of_add_it';
        $keyAddedIt = $post->post_type.'_text_of_added_it';
        $addTxt = of_get_option($keyAddIt, __('ADD IT', 'apollo'));
        $addedTxt = of_get_option($keyAddedIt, __('ADDED', 'apollo'));
        $additTipText = __( 'Click to bookmark this listing', 'apollo' );

        ?>
        <a href="javascript:void(0);"
           onclick="ajaxAddBookmark('<?php echo admin_url("admin-ajax.php"); ?>','<?php echo $post->ID ?>','<?php echo $post->post_type ?>', this)"
           data-added-text="<?php echo $addedTxt ?>"
           data-add-text="<?php echo $addTxt ?>"
           data-addit="<?php echo ! $has_bookmark ? '1' : 0 ?>"
            <?php echo $sattr ?>>
            <span><?php echo $has_bookmark ? $addedTxt : $addTxt ?></span>
            <div class="show-tip"><?php echo $additTipText ?></div>
        </a>
        <?php

        return ob_get_clean();

    }

    /**
     * Set video photo
    */
    public function setVideoPhoto()
    {
        // Set video, photo gallery data
        $this->videoGallery = $this->setVideoGallery();
        $this->photoGallery = $this->setPhotoGallery();

        // Process display video photo gallery
        $this->setDataPhotoItems($this->extraInfo);
        $this->setVideoData($this->extraInfo);
    }

    /**
     * Set video data
     *
     * @param $extraInfo
     */
    public function setVideoData(&$extraInfo)
    {
        $first_desc_video = '';
        $videos = $this->videoGallery;
        $thumbs = '';
        $videoWrapper = '';
        if (!empty($videos)) {

            foreach ($videos as $k => $v):

                if ($k === 0) $first_desc_video = $v['desc'];


                if (!$v['embed']) continue;
                if (Apollo_App::is_vimeo($v['embed'])) {
                    $matches = array();
                    if (!strpos($v['embed'], "player")) {
                        preg_match('/\/\/(www\.)?vimeo.com\/(\d+)($|\/)/', $v['embed'], $matches);
                        if (!empty($matches))
                            $v['embed'] = "https://player.vimeo.com/video/" . $matches[2];
                    }
                }
                $videoObj = new Apollo_Video($v['embed']);
                $code = $videoObj->getCode();

                if (!$code) continue;
                $vimeoUrl = Apollo_App::is_vimeo($v['embed']) ? $v['embed'] : '';
                $thumbs .= '<a data-vimeo="' . $vimeoUrl . '" data-desc="' . (nl2br($v['desc'])) . '" data-src="' . $videoObj->getSrc() . '" data-code="' . $code . '" data-slide-index="' . $k . '" href="">
                    <img class="lazy" data-original=" ' . $videoObj->getThumb() . '">
                </a>';

                if ($k == 0):
                    $videoWrapper .= '<div class="video-wrapper"><div class="lazyYT" data-youtube-id="' . $code . '" id="player">';
                    if (!$k && Apollo_App::is_vimeo($v['embed'])) {
                        $videoWrapper .= '<iframe id="player" frameborder="0" allowfullscreen="1" title="" width="640" height="360" src="' . $v['embed'] . '"></iframe>';

                    }
                    $videoWrapper .= '</div></div>';

                endif;

            endforeach;
        }

        $extraInfo['video_wrapper'] = $videoWrapper;
        $extraInfo['video_first_desc'] = $first_desc_video;
        $extraInfo['video_thumbs'] = $thumbs;
        $extraInfo['videos'] = $this->_getListVideos();
    }

    /**
     * Set photo data
     *
     * @param $extraInfo
     */
    public function setDataPhotoItems(&$extraInfo)
    {
        $galleries = $this->photoGallery;

        $big_sliders = '';
        $thumb_sliders = '';
        $first_desc = '';

        $popup_big_sliders = '';
        $popup_thumb_sliders = '';
        if (!empty($galleries)) {
            foreach ($galleries as $k => $attachment_id) {
                $post_img = get_post($attachment_id);
                $caption = !empty($post_img) ? $post_img->post_excerpt : '';

                if ($k === 0) $first_desc = $caption;

                if (!get_attached_file($attachment_id)) continue;

                $attach_img = wp_get_attachment_image_src($attachment_id, 'large');
                $attach_thumb = wp_get_attachment_image_src($attachment_id, 'thumbnail');
                $big_sliders .= '<li><img width="' . $attach_img[1] . '" height="' . $attach_img[2] . '" src="' . $attach_img[0] . '" data-original="' . $attach_img[0] . '" ></li>';
                $thumb_sliders .= '<a data-slide-index="' . $k . '" href="" class="thumbnail-photo" data-desc="' . htmlentities($caption) . '">
                                    <img width="' . $attach_thumb[1] . '" height="' . $attach_thumb[2] . '" src="' . $attach_thumb[0] . '" >
                                </a>';

                $selected = $k == 0 ? 'selected' : '';
                $p_attach_img = wp_get_attachment_image_src($attachment_id, 'full');
                $p_attach_thumb = wp_get_attachment_image_src($attachment_id, 'medium');

                $popup_big_sliders .= '<div data-caption="' . ($post_img ? $post_img->post_excerpt : '') . '" data-srcth="' . $p_attach_thumb[0] . '" data-src="' . $p_attach_img[0] . '" data-target="' . $k . '" class="photo ' . $selected . '"></div>';

                $current_class = $k == 0 ? 'class="current"' : "";
                $popup_thumb_sliders .= '<li><a href="#" ' . $current_class . ' data-id="' . $k . '"><img src="' . $p_attach_thumb[0] . '" ></a></li>';

            }

            $extraInfo['first_desc'] = nl2br($first_desc);
            $extraInfo['big_sliders'] = $big_sliders;
            $extraInfo['thumb_sliders'] = $thumb_sliders;
            $extraInfo['popup_big_sliders'] = $popup_big_sliders;
            $extraInfo['popup_thumb_sliders'] = $popup_thumb_sliders;
        }
    }

    private function _getListVideos()
    {
        if (!$videos = $this->videoGallery)
            return false;

        $videoTemp = array();
        foreach ($videos as $video) {
            $videoTemp[] = array(
                'embed' => $video['embed'],
                'desc' => isset($video['desc']) ? base64_decode($video['desc']) : '',
            );
        }
        $videos = $videoTemp;
        return $videos;
    }

    /**
     * Get photo gallery
     * @return string
     */
    public function getPhotoGallery() {
        return $this->photoGallery;
    }
}
