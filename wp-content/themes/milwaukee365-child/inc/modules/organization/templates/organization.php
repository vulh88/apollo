<?php

$singleDir = SONOMA_ORG_TEMPLATE_DIR. '/partials/single';

$org = $model->getSinglePost();
$data = $org->getOrgData();
$allow_comment = of_get_option(Apollo_DB_Schema::_ENABLE_COMMENT,1) && comments_open();
?>
<div class="breadcrumbs">
    <ul class="nav">
        <ul class="nav">
            <li><a href="<?php echo home_url() ?>"><?php _e( 'Home', 'apollo' ) ?></a></li>
            <li><a href="<?php echo home_url('organization') ?>"><?php _e( 'Organization ', 'apollo' ) ?></a></li>
            <li> <span><?php echo $org->get_title() ?></span></li>
        </ul>
    </ul>
</div>
<?php

Apollo_Next_Prev::getNavLink($org->id,$post->post_type);
?>
<?php
include( $singleDir. '/general.php');
include( $singleDir. '/list-event.php');
include( $singleDir. '/list-classified.php');
include( $singleDir. '/add-fields.php');
include( $singleDir. '/media.php');

/** @Ticket #13250 */
include APOLLO_TEMPLATES_DIR. '/content-single/common/jetpack-related-posts.php';

?>

<div class="blog-bkl astro-featr">
    <div class="a-block" id="event_comment_block">
        <?php
        // If comments are open or we have at least one comment, load up the comment template.
        if ( $allow_comment ) {
            ?>
            <h4><?php _e('COMMENTS', 'apollo') ?><span> <i class="fa fa-comment"></i></span></h4>
            <div class="a-block-ct">
                <?php
                comments_template('/templates/comments.php');
                ?>
            </div>
            <?php
        }
        ?>
    </div><!-- #comments -->
</div>

