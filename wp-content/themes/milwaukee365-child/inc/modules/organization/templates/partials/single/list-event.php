<?php
include_once APOLLO_TEMPLATES_DIR.'/events/list-events.php';
$list_events_up = new List_Event_Adapter( 1, Apollo_Display_Config::PAGESIZE_UPCOM );
$list_events_up->get_org_up_comming_events( $org->id);

$list_events_past = new List_Event_Adapter( 1, Apollo_Display_Config::PAGESIZE_UPCOM );
$list_events_past->get_org_past_event( $org->id );
?>
<?php if ( ! $list_events_up->isEmpty() || ! $list_events_past->isEmpty()  ): ?>
    <div class="blog-bkl artist-tab">
        <nav class="nav-tab">
            <ul class="tab-list">
                <?php if ( ! $list_events_up->isEmpty() ): ?>
                    <li class="selected"><a href="#" data-id="1"><?php _e( 'UPCOMING EVENTS', 'apollo' )  ?></a></li>
                <?php endif; ?>

                <?php if ( ! $list_events_past->isEmpty() ): ?>
                    <li><a href="#" data-id="2"><?php _e( 'PAST EVENTS', 'apollo' ) ?></a></li>
                <?php endif; ?>
            </ul>
        </nav>
        <div class="tab-bt art"></div>
    </div>
<?php endif; ?>

<?php if ( ! $list_events_up->isEmpty() ): ?>

    <div data-target="1" class="blog-bkl tab">
        <div class="a-block-ct">
            <div id="apollo-view-more-upcomming-container"><?php echo $list_events_up->render_html( '_org-events.php' ) ?></div>
        </div>
        <?php if ( $list_events_up->isShowMore() ): ?>
            <div class="load-more b-btn">
                <a href="javascript:void(0);"
                   data-ride="ap-more"
                   data-container="#apollo-view-more-upcomming-container"
                   data-holder="#apollo-view-more-upcomming-container>:last"
                   data-sourcetype="url"
                   data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_show_more_upcoming_org_event&page=2') ?>&current_org_id=<?php echo $org->id; ?>&user_id=<?php echo $org->post->post_author ?>"
                   data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
                   data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
                   class="btn-b arw"><?php _e('SHOW MORE', 'apollo') ?>
                </a>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>

<?php if ( ! $list_events_past->isEmpty() ): ?>
    <div data-target="2" class="blog-bkl tab" style="display: none;">
        <div id="apollo-view-more-past-container">
            <?php echo $list_events_past->render_html( '_org-events.php' ) ?>
        </div>
        <?php if ( $list_events_past->isShowMore() ): ?>
            <div class="load-more b-btn">
                <a href="javascript:void(0);"
                   data-container="#apollo-view-more-past-container"
                   data-ride="ap-more"
                   data-holder="#apollo-view-more-past-container>:last"
                   data-sourcetype="url"
                   data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_show_more_past_org_event&page=2') ?>&current_org_id=<?php echo $org->id; ?>&user_id=<?php echo $org->post->post_author ?>"
                   data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
                   data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
                   class="btn-b arw"><?php _e('SHOW MORE', 'apollo') ?>
                </a>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>
