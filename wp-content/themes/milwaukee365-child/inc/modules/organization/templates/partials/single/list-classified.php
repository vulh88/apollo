<?php
include_once APOLLO_TEMPLATES_DIR.'/classified/list-classifieds.php';
$list_classified = new List_Classified_Adapter( 1, Apollo_Display_Config::PAGESIZE_UPCOM );
$list_classified->get_org_classified( $org->id);
?>

<?php if ( ! $list_classified->isEmpty() ): ?>

    <div class="blog-bkl classified <?php echo isset($blockLabelStyle) ? $blockLabelStyle : ''; ?>">
        <div class="a-block">
            <h4><?php _e('Related classified','apollo') ?></h4>
        </div>
        <div id="apollo-view-more-classified-container">
            <?php echo $list_classified->render_html( '_org-classified.php' ) ?>
        </div>

        <?php if ( $list_classified->isShowMore() ): ?>
            <div class="load-more b-btn">
                <a href="javascript:void(0);"
                   data-container="#apollo-view-more-classified-container"
                   data-ride="ap-more"
                   data-holder="#apollo-view-more-classified-container>:last"
                   data-sourcetype="url"
                   data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_show_more_org_classified&page=2') ?>&current_org_id=<?php echo $org->id; ?>&user_id=<?php echo $org->post->post_author ?>"
                   data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
                   data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
                   class="btn-b arw"><?php _e('SHOW MORE', 'apollo') ?>
                </a>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>
