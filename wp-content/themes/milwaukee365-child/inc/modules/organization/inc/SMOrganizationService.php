<?php


class SMOrganizationService extends AbstractSMSingleService
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Set id of the current post type
     * @param integer $id
     * @return mixed
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get single post type
     * @return mixed
     */
    public function setSinglePost()
    {
        global $post;
        $this->single = get_org($post);
    }

    /**
     * Set extra info
     * @return mixed
     */
    public function setExtraInfo()
    {
        $this->extraInfo['audio'] = $this->getSinglePost()->getListAudios(Apollo_DB_Schema::_APL_ORG_AUDIO);
    }


    public function isAllowComment()
    {
        return of_get_option(Apollo_DB_Schema::_ENABLE_COMMENT, 1) && comments_open();
    }

    /**
     * Set video gallery
     * @return mixed
     */
    public function setVideoGallery()
    {
        return $this->getSinglePost()->getListVideo();
    }

    /**
     * Set photo gallery
     * @return mixed
     */
    public function setPhotoGallery()
    {
        return $this->getSinglePost()->getListImages();
    }
}