<?php

/**
 * Class SMOrganizationModule
 */
class SMOrganizationModule
{
    public function __construct()
    {
        $this->renderPageTemplate();
    }

    public function renderPageTemplate()
    {
        if (is_single()) {
            require_once __DIR__ . '/inc/SMOrganizationService.php';
            $service = new SMOrganizationService();
            $file = SONOMA_MODULES_DIR . '/organization/templates/organization.php';
        }
        else { // Listing or search page
            require_once __DIR__ . '/inc/SMOrganizationsService.php';
            $service = new SMOrganizationsService();
            $file = SONOMA_MODULES_DIR . '/organization/templates/organizations.php';
        }

        if (file_exists($file)) {
            SMViewHelper::smGetTemplatePartCustom($file, $service);
        }
    }
}

new SMOrganizationModule();