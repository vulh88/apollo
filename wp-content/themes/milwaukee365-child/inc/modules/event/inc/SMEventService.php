<?php


class SMEventService extends AbstractSMSingleService
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Set id of the current post type
     * @param integer $id
     * @return mixed
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get single post type
     * @return mixed
     */
    public function setSinglePost()
    {
        global $post;
        $this->single = get_event($post);
    }

    /**
     * Set extra info
     * @return mixed
     */
    public function setExtraInfo()
    {

    }

    /**
     * Set video gallery
     * @return mixed
     */
    public function setVideoGallery()
    {
        // Get videos and gallery
        return $this->single->getMetaInMainData( Apollo_DB_Schema::_VIDEO );
    }

    /**
     * Set photo gallery
     * @return mixed
     */
    public function setPhotoGallery()
    {
        return explode( ',' , $this->single->get_meta_data( Apollo_DB_Schema::_APOLLO_EVENT_IMAGE_GALLERY ) );
    }
}