<?php

/**
 * Class SMEventModule
 */
class SMEventModule
{
    public function __construct()
    {
        $this->renderPageTemplate();
    }

    public function renderPageTemplate()
    {
        if (is_single()) {
            require_once __DIR__ . '/inc/SMEventService.php';
            $service = new SMEventService();
            $file = SONOMA_MODULES_DIR . '/event/templates/event.php';
        }
        else if (is_tax()) {
            require_once __DIR__ . '/inc/SMEventTaxonomyService.php';
            global $apl_current_event_term;
            $service = new SMEventTaxonomyService($apl_current_event_term);
            $file = SONOMA_MODULES_DIR . '/event/templates/taxonomy.php';
        }
        else { // Listing or search page
            require_once __DIR__ . '/inc/SMEventsService.php';
            $service = new SMEventsService(true);
            $file = SONOMA_MODULES_DIR . '/event/templates/events.php';
        }

        if (file_exists($file)) {
            SMViewHelper::smGetTemplatePartCustom($file, $service);
        }
    }
}

new SMEventModule();