<?php


$singleDir = SONOMA_EVENT_TEMPLATE_DIR. '/partials/single';

$_a_event = $model->getSinglePost();

$icon_fields = maybe_unserialize( get_option( Apollo_DB_Schema::_APL_EVENT_ICONS ) );
$even_dataIcons = Apollo_App::apollo_get_meta_data($_a_event->id ,Apollo_DB_Schema::_APL_EVENT_POST_ICONS,Apollo_DB_Schema::_APOLLO_EVENT_DATA);
$even_end_date = Apollo_App::apollo_get_meta_data($_a_event->id ,Apollo_DB_Schema::_APOLLO_EVENT_END_DATE);

if($_a_event->isPrivateEvent()){
    $routing = of_get_option(Apollo_DB_Schema::_ROUTING_FOR_404_ERROR,'404');
    wp_redirect(home_url($routing));
    exit;
}
///SOCIAL SHARE INFO
$_o_summary = $_a_event->get_summary();
$arrShareInfo = $_a_event->getShareInfo();

/// CONTENT
$characters = of_get_option(Apollo_DB_Schema::_APL_EVENT_CHARACTERS_DESCRIPTION,500);
$a_200_content = $_a_event->get_content($characters);


/// SHOW BUY TICKET OR DISCOUNT
$ticket_url     = $_a_event->getMetaInMainData( Apollo_DB_Schema::_ADMISSION_TICKET_URL );
$discount_url   = $_a_event->getMetaInMainData( Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL );

/// ADMISSION INFO
$admission_detail  = $_a_event->getMetaInMainData( Apollo_DB_Schema::_ADMISSION_DETAIL );
$admission_contact = $_a_event->getMetaInMainData( Apollo_DB_Schema::_ADMISSION_PHONE );
$admission_email = $_a_event->getMetaInMainData( Apollo_DB_Schema::_ADMISSION_TICKET_EMAIL );

$is_get_venue_name = false;

$str_address_all = $_a_event->getStrAddress();
$str_address_location = $_a_event->getFullAddress();

// Get terms
$orgs       = Apollo_App::get_post_type_item( $_a_event->{Apollo_DB_Schema::_APOLLO_EVENT_ORGANIZATION} );
$venues     = Apollo_App::get_post_type_item( $_a_event->{Apollo_DB_Schema::_APOLLO_EVENT_VENUE} );

// Get org events
include_once APOLLO_TEMPLATES_DIR.'/events/list-events.php';
$org_events_adapter = new List_Event_Adapter( isset($_REQUEST['page']) ? max(1, intval($_REQUEST['page'])) : 1, Apollo_Display_Config::PAGESIZE_UPCOM);


$_venue = get_venue($_a_event->{Apollo_DB_Schema::_APOLLO_EVENT_VENUE});
$latVn = $lngVn = false;
if ($_venue) {
    $latVn = $_venue->get_meta_data(Apollo_DB_Schema::_VENUE_LATITUDE);
    $lngVn = $_venue->get_meta_data(Apollo_DB_Schema::_VENUE_LONGITUDE);
}

if ($latVn && $lngVn) {
    $arrCoor= array($latVn, $lngVn);
} else {
    $arrCoor = Apollo_Google_Coor_Cache::getCoordinateByAddressInLocalDB($str_address_all);
}

$sCoor = $arrCoor ? implode(",", $arrCoor) : '';

$allow_comment = of_get_option(Apollo_DB_Schema::_ENABLE_COMMENT,1) && comments_open();

// Get accessibility information
global $apollo_event_access;
$listFullACB = $apollo_event_access;
ksort( $listFullACB );

/** @Ticket #12936 - Get Venue Accessibility  */

$eventListACB = $_a_event->getMetaInMainData( Apollo_DB_Schema::_E_CUS_ACB );
$eventACBInfo = $_a_event->getMetaInMainData( Apollo_DB_Schema::_E_ACB_INFO );

$venue_access_mode = of_get_option(Apollo_DB_Schema::_ENABLE_VENUE_ACCESSIBILITY_MODE, 1);
if (isset($_venue) && $_venue->id) {
    $venue_meta_data_info = Apollo_App::apollo_get_meta_data($_venue->id ,Apollo_DB_Schema::_APL_VENUE_DATA);
    if (!is_array($venue_meta_data_info)) {
        $venue_meta_data_info = unserialize($venue_meta_data_info);
    }

    if ($venue_access_mode) {
        $eventListACB = isset($venue_meta_data_info[Apollo_DB_Schema::_E_CUS_ACB]) ? $venue_meta_data_info[Apollo_DB_Schema::_E_CUS_ACB] : array();
        $eventACBInfo = isset($venue_meta_data_info[Apollo_DB_Schema::_E_ACB_INFO]) ? $venue_meta_data_info[Apollo_DB_Schema::_E_ACB_INFO] : '' ;
    }
}

$website      = $_a_event->getMetaInMainData( Apollo_DB_Schema::_WEBSITE_URL );
?>
<div class="breadcrumbs">
    <ul class="nav">
        <li><a href="<?php echo get_home_url(get_current_blog_id()) ?>"><?php _e('Home', 'apollo') ?></a></li>
        <?php echo $_a_event->get_primary_term_link( '<li>', '</li>' ); ?>
        <li> <span><?php echo $_a_event->get_title() ?></span></li>
    </ul>
</div>
<?php
Apollo_Next_Prev::getNavLink($_a_event->id,$_a_event->post->post_type);
?>

<article class="content-page astro-detail event-detail-article">

    <?php
    // Include log form for system log
    Apollo_App::includeTemplate(APOLLO_TEMPLATES_DIR. '/partials/log-form.php', array(
        'id'    => $_a_event->id,
        'post_type' => Apollo_DB_Schema::_EVENT_PT
    ));


    ob_start();
    ?>
    <div class="b-share-cat">

        <?php
        $checkTime = $_a_event->get_periods_time(" LIMIT 0, ". Apollo_Display_Config::MAX_DATETIME);
        $arraySocial =  array(
            'info' => $arrShareInfo,
            'id' => $_a_event->id,
            'data_btns' =>
                array(  'print',
                    'sendmail',
                )
        );

        if( is_array($checkTime) && count($checkTime) > 0){
            $arraySocial['data_btns'][] = 'ics';
            $arraySocial['data_btns'][] = 'googleCalendar';
        }

        SocialFactory::social_btns($arraySocial );
        ?>
    </div>

    <?php
    $socialMediaButtons = ob_get_clean();
    $socialMediaButtonsLocation = of_get_option(Apollo_DB_Schema::_EVENT_SOCIAL_MEDIA_LOCATION_DETAIL, 1);
    $icon_position = of_get_option(Apollo_DB_Schema::_EVENT_SHOW_ICON_POSITION, '');
    $thumbsUpEnable = of_get_option(Apollo_DB_Schema::_ENABLE_THUMBS_UP,1);
    $thumbsUpPosition = of_get_option(Apollo_DB_Schema::_EVENT_SHOW_THUMBS_UP_POSITION, 'default');
    if ($socialMediaButtonsLocation == 1) echo $socialMediaButtons;
    ?>

    <div class="social-icon-pck">
        <input name="eventid" value="<?php echo $_a_event->id; ?>" type="hidden" />
        <div class="page-tool">
        </div>
        <?php if( $thumbsUpEnable && $thumbsUpPosition == 'next_title') : ?>
            <div class="event-thumbs-up-next-the-title">
                <h1 class="p-ttl">
                    <?php echo $_a_event->get_title() ?>
                    <div class="like" id="_event_rating_<?php echo $_a_event->id . '_' . 'event' ?>" data-ride="ap-event_rating" data-i="<?php echo $_a_event->id ?>" data-t="event"> <a href="javascript:void(0);"><span class="_count">&nbsp;</span></a></div>
                </h1>
            </div>
        <?php else : ?>
            <h1 class="p-ttl"><?php echo $_a_event->get_title() ?></h1>
        <?php endif; ?>

        <?php if( $_a_event->isHasExpiredEvent($even_end_date)) { ?>
            <p class="event-expired error"><i class="fa fa-exclamation-triangle fa-lg"></i><span><?php _e('Please note, this event has expired.','apollo')?></span></p>
        <?php } ?>

        <?php if ($icon_position  == 'after_title'): ?>
            <?php $_a_event->renderIcons($icon_fields,$even_dataIcons); ?>
        <?php endif; ?>
        <p class="meta auth"><?php echo $_a_event->renderOrgVenueHtml(); ?></p>

        <?php if ($socialMediaButtonsLocation == 2) echo $socialMediaButtons; ?>

        <div class="rating-box rating-action">
            <div class="box-action-wrap">
                <?php if( $thumbsUpEnable && $thumbsUpPosition == 'default') : ?>
                    <div class="like parent-not-underline-hover" id="_event_rating_<?php echo $_a_event->id . '_' . 'event' ?>" data-ride="ap-event_rating" data-i="<?php echo $_a_event->id ?>" data-t="event"> <a href="javascript:void(0);"><span class="_count">&nbsp;</span></a></div>
                <?php endif ?>
                <?php
                if ( $allow_comment ):
                    ?>
                    <div class="cm"> <a href="javascript:;"><?php _e( 'Comment', 'apollo' ) ?></a></div>

                <?php endif; ?>
            </div>
            <!--    <div class="bok-mk"><a href="javascript:;">Bookmark</a></div>-->

            <?php if ($icon_position == 'after_thumbs_up' || $icon_position == ''): ?>
                <div class="icons-list-after-thumbsup">
                    <?php $_a_event->renderIcons($icon_fields,$even_dataIcons); ?>
                </div>
            <?php endif; ?>

        </div>


        <div class="astro-featr ct-s-c-d-p">
            <article class="blog-itm">
                <?php  $_a_event->render_circle_date(); ?>

                <div class="pic">
                    <?php echo $_a_event->get_image('medium') ?>
                </div>

                <?php

                $a_200_excerpt = $_a_event->get_excerpt(200);

                $a_full_excerpt = $_a_event->get_excerpt();
                if ( $a_200_excerpt['text'] ):
                    ?>
                    <div class="a-txt-fea" id="_ed_sum_short">
                        <?php echo $_a_event->get_full_excerpt(); ?>
                    </div>
                <?php endif; ?>

                <div class="b-btn __inline_block_fix_space">

                    <?php echo $_a_event->renderTickerBtn(array(
                        'class' => 'btn btn-b'
                    )) ?>

                    <?php echo $_a_event->renderCheckDCBtn(array(
                        'class' => 'btn btn-b'
                    )) ?>

                    <?php echo $_a_event->renderBookmarkBtn( array( 'class' => 'btn-bm btn btn-b' ) ); ?>
                </div>
                <?php if(of_get_option(Apollo_DB_Schema::_ENABLE_DISPLAY_WITHOUT_VIEW_MORE)) { ?>
                    <div class="a-desc apl-internal-content desc-evt " id="_ed_full">
                        <?php echo $_a_event->get_full_content() ?>
                    </div>
                <?php }else { ?>
                    <div class="a-desc apl-internal-content desc-evt" id="_ed_short">
                        <?php
                        echo $a_200_content['text'];
                        ?>
                        <?php if($a_200_content['have_more'] === true): ?>
                            <a href="javascript:void(0);" data-type="vmore" class="vmore" data-target="#_ed_full" data-own='#_ed_short'><?php _e('View more', 'apollo') ?></a>
                        <?php endif; ?>
                    </div>
                    <div class="a-desc apl-internal-content hidden desc-evt " id="_ed_full">
                        <?php echo $_a_event->get_full_content() ?>
                        <a href="javascript:void(0);" data-type="vmore" class="vmore" data-target="#_ed_short" data-own='#_ed_full'><?php _e('View less', 'apollo') ?></a>
                    </div>
                <?php } ?>

                <div class="clearfix"></div>


                <?php
                $enable_individual_date_time = of_get_option(Apollo_DB_Schema::_ENABLE_INDIVIDUAL_DATE_TIME,1);
                $additionalTime =  $_a_event->get_meta_data(Apollo_DB_Schema::_APL_EVENT_ADDITIONAL_TIME,Apollo_DB_Schema::_APOLLO_EVENT_DATA);

                // Add additional datetime info to admission info block if $enable_individual_date_time = false
                include $singleDir. '/admission.php';

                if($enable_individual_date_time){
                    include $singleDir. '/individual-date-time.php';
                }

                include SONOMA_MODULES_DIR. '/common-templates/videos-image-gallery.php';

                $isDisplayWhatNearBy = of_get_option(APL_Business_Module_Theme_Option::WHATS_NEARBY_MAP_ACTIVE)
                    && Apollo_App::is_avaiable_module(Apollo_DB_Schema::_BUSINESS_PT);

                include $singleDir. '/location.php';
                if ($isDisplayWhatNearBy) {
                    include $singleDir. '/what-near-by.php';
                } else {
                    include $singleDir. '/parking-info.php';
                }

                include $singleDir. '/accessibility.php';
                include $singleDir. '/events-org.php';
                include $singleDir. '/associated-artists.php';

                ?>

                <div class="a-block review hidden "> <!-- @todo TuanPHPVN hidden here -->
                    <h4>MEMBER REVIEWS<span> <i class="fa fa-user"></i></span></h4>
                    <div class="a-block-ct">
                        <p class="t-c">There are currently no reviews/comments for this event. Be the first to add a review/comment , and let folks know what you think!</p>
                    </div>
                </div>

                <?php /** @Ticket #13250 */
                include APOLLO_TEMPLATES_DIR. '/content-single/common/jetpack-related-posts.php';
                ?>

                <div class="a-block" id="event_comment_block">
                    <?php
                    // If comments are open or we have at least one comment, load up the comment template.
                    if ( $allow_comment ) {
                        ?>
                        <h4><?php _e('COMMENTS', 'apollo') ?><span> <i class="fa fa-comment"></i></span></h4>
                        <div class="a-block-ct">
                            <?php
                            comments_template('/templates/comments.php');
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div><!-- #comments -->

                <?php require_once APOLLO_TEMPLATES_DIR.'/events/google-map-popup.php' ?>
                <?php require_once APOLLO_TEMPLATES_DIR.'/events/chooes-export-popup.php' ?>


            </article>
        </div>
</article>