<div class="a-block offers-dates-times hidden">
    <?php
    echo do_shortcode('[apollo_offer_dates_times]');
    ?>
</div>
<div class="clearfix"></div>
<?php if(!empty($str_address_all)): ?>
    <div class="a-block locatn location-wrapper">
        <h4><?php _e( 'LOCATION', 'apollo' ) ?> <span> <i class="fa fa-map-marker"></i></span></h4>
        <div class="a-block-ct">
            <div>

                <b><?php echo $_a_event->get_location_name() ?></b>


                <?php if(!empty($str_address_location)): ?>
                    <p><?php echo $str_address_location ?></p>
                <?php endif; ?>

            </div>
            <div class="clearfix"></div>
            <div class="lo-meta clearfix hidden">
                <p> <b><?php _e('Accessibility Information:', 'apollo') ?> </b> <?php _e('Currently, no accessibility information is available for this event.', 'apollo') ?></p>
                <p> <b><?php _e('Connect with this Organization:', 'apollo') ?>                 </b><a href="http://" target="_blank" class="lo-link fb"><i class="fa fa-facebook fa-lg"></i></a><a href="http://" target="_blank" class="lo-link tw"><i class="fa fa-twitter fa-lg"></i></a><a href="http://" target="_blank" class="lo-link donat">DONATE</a></p>
            </div>
        </div>
    </div>
<?php endif; ?>


<?php if (empty($isDisplayWhatNearBy)): ?>
<div class="a-block locatn location-wrapper">
    <div class="a-block-ct">
        <form id="search-map" method="POST" action="" class="search-map-frm hidden"><i class="fa fa-search fa-flip-horizontal"></i>
            <input type="text" placeholder="Find Restaurants near this venue...">
        </form>
        <div <?php if ( ! $str_address_all ) echo 'style="display: none"'  ?> class="lo-left">
            <div class="lo-map" id="_event_map"
                 data-coor = "<?php echo $sCoor ?>"
                 data-address="<?php echo $str_address_all ?>"
                 data-relation_id="_popup_google_map_full"
                 data-alert="<?php _e('We\'re sorry. A map for this listing is currently unavailable.', 'apollo') ?>"
                 data-nonce="<?php echo wp_create_nonce('save-coor') ?>"
                 data-img_location="<?php echo get_stylesheet_directory_uri() ?>/assets/images/icon-location.png">
            </div>
            <p class="t-r"> <a href="javascript:void(0);" data-target="#_popup_google_map" id="_event_trigger_fullmap"><?php _e('Full map and directions', 'apollo') ?></a></p>
        </div>
    </div>
</div>
<?php endif; ?>