<?php if(!empty($venue_meta_data_info[Apollo_DB_Schema::_VENUE_PARKING_INFO])) : ?>

<div class="a-block adm">
    <h4><?php _e("PARKING INFO", "apollo"); ?><span> <i class="fa fa-bus fa-2x"></i></span></h4>
    <div class="a-block-ct">
        <?php
        $fullContent = Apollo_App::convertContentEditorToHtml($venue_meta_data_info[Apollo_DB_Schema::_VENUE_PARKING_INFO]);
        ?>
        <div class="apl-internal-content">
            <?php echo $fullContent; ?>
        </div>
    </div>
</div>

<?php endif; ?>
