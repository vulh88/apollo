<?php
// get event's organization
$registeredOrg   = $_a_event->get_register_org(); // = NULL if organization is TMP ORG
$registeredOrgID = empty($registeredOrg) ? '' : $registeredOrg->org_id;

if(!empty($admission_detail) || !empty($admission_contact) || !empty($website) || !empty($admission_email) ||
    (!empty($additionalTime) && !$enable_individual_date_time) ) : ?>
    <div class="a-block adm">
        <h4><?php _e("ADMISSION INFO", "apollo"); ?><span> <i class="fa fa-info"></i></span></h4>
        <div class="a-block-ct">
            <?php if(!empty($admission_detail)): ?>
                <div class="apl-internal-content"><?php echo Apollo_App::convertContentEditorToHtml($admission_detail); ?></div>
            <?php endif; ?>
            <?php if(!empty($admission_contact)): ?>
                <p> <b><?php _e("Contact", "apollo"); ?>: </b><?php echo $admission_contact ?></p>
            <?php endif; ?>
            <?php if(!empty($admission_email)): ?>
                <p> <b><?php _e("Email", "apollo"); ?>: </b> <a href="mailto:<?php echo $admission_email?>" > <?php echo $admission_email ?></a></p>
            <?php endif; ?>

            <?php if ( $website ): ?>
                <p>
                    <i class="fa fa-link fa-lg">&nbsp;&nbsp;&nbsp;</i>
                    <a class="under_hover"
                       target="_blank"
                       href="<?php echo $website ?>"
                       data-ride="ap-logclick"
                       data-action="apollo_log_click_activity"
                       data-activity="click-official-website"
                       data-item_id="<?php echo $_a_event->id; ?>"
                       data-start="<?php echo $_a_event->{Apollo_DB_Schema::_APOLLO_EVENT_START_DATE}; ?>"
                       data-end="<?php echo $_a_event->{Apollo_DB_Schema::_APOLLO_EVENT_END_DATE}; ?>",
                       data-org-id="<?php echo $registeredOrgID; ?>"><?php _e('Official Website', 'apollo') ?></a>
                </p>
            <?php endif; ?>

            <?php
            // Add additional datetime info to admission info block if $enable_individual_date_time = false
            if(!empty($additionalTime) && !$enable_individual_date_time):
            ?>
            <div class="apl-internal-content"><p><b><?php _e('Additional time info: ','apollo') ?> </b> <?php echo Apollo_App::convertContentEditorToHtml($additionalTime) ?></p></div>
            <?php endif;  ?>


        </div>
    </div>
<?php endif; ?>
