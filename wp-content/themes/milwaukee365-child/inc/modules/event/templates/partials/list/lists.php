<?php
if ( $results = $this->get_results() ) {
    foreach ($results as $p):
        $e = get_event($p->ID);
        $summary = $e->post->post_excerpt;
        ?>
        <li class="apl-search-event-list">
            <div class="search-img lazy-load-image-wrapper"><a
                    href="<?php echo $e->get_permalink() ?>"><?php echo $e->get_image('thumbnail', array(),
                        array(
                            'aw' => true,
                            'ah' => true,
                        ),
                        'normal'
                    ) ?></a></div>

            <div data-type="link" data-url="<?php echo $e->get_permalink() ?>" class="info-content">
                <div class="search-info"><a href="<?php echo $e->get_permalink() ?>"><span
                            class="ev-tt"><?php echo $e->get_title() ?></span></a>

                    <p class="meta auth"><?php echo $e->renderOrgVenueHtml() ?></p>
                </div>
                <div class="apl-event-date">
                    <div class="searchdate">
                    <span class="sch-date">
                        <?php $e->render_sch_date() ?>
                    </span>
                    </div>
                    <?php

                    $e_times = $e->render_periods_time($e->get_periods_time(" LIMIT " . Apollo_Display_Config::MAX_DATETIME . " "), '<div class="item">', '</div>');
                    ?>
                    <div class="ico-date <?php echo !$e_times ? 'hidden' : '' ?>"><a><i class="fa fa-clock-o fa-2x"></i></a>

                        <div class="show-date">
                            <?php
                            echo $e_times;
                            if ($e->have_more_periods_time()) {
                                echo '<div class="item">...</div>   ';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="apl-event-desc">
                        <p><?php echo $summary; ?></p>
                    </div>
                </div>
            </div>
        </li>
        <div class="vertical-light"></div>
    <?php endforeach;
}
else {
    _e( 'No results', 'apollo' );

}