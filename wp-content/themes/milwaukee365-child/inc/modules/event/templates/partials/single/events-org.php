<?php
$reg_org = $_a_event->get_register_org();
if ( Apollo_App::is_avaiable_module( 'organization' ) && $reg_org ):
    $org_id = $reg_org->{Apollo_DB_Schema::_APL_E_ORG_ID};
    $list_html_events = $org_events_adapter->get_html_org_upcomming_events( $org_id, $_a_event->id );
    $event_org = get_org( $org_id );
    if ( ! $org_events_adapter->isEmpty() && $event_org->get_title() ): ?>
        <div class="a-block more-from">
            <h4><?php echo sprintf( __( 'MORE FROM %s', 'apollo' ), $event_org->get_title()  ) ?></h4>
            <div class="a-block-ct">
                <div class="more-frm-list">
                    <div id="apollo-view-more-upcomming-org-event">
                        <?php echo $list_html_events; ?>
                    </div>
                </div>
                <?php if ( $org_events_adapter->isShowMore() ): ?>
                    <div class="load-more b-btn wp-ct-event">
                        <a href="javascript:void(0);"
                           data-container="#apollo-view-more-upcomming-org-event"
                           data-ride="ap-more"
                           data-holder="#apollo-view-more-upcomming-org-event>:last"
                           data-sourcetype="url"
                           data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_show_more_upcomming_org_event&page=2'
                               . '&org_id='.$org_id ) ?>&current_event_id=<?php echo $_a_event->id; ?>"
                           data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
                           data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
                           class="btn-b arw ct-btn-sm-event"><?php _e('SHOW MORE', 'apollo') ?>
                        </a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php endif; ?>

    <?php
    $data = $event_org->getOrgData();

    $hasEmail = isset($data['email']) && !empty($data['email']);
    $hasWeb = isset($data['web']) && !empty($data['web']);
    $hasFb = isset($data['facebook']) && !empty($data['facebook']);
    $hasTwitter = isset($data['twitter']) && !empty($data['twitter']);
    $hasIns = isset($data['inst']) && !empty($data['inst']);
    $hasLinkin = isset($data['linked']) && !empty($data['linked']);
    $hasDonate = isset($data['donate']) && !empty($data['donate']);

    if ($hasEmail || $hasWeb || $hasFb || $hasTwitter || $hasIns || $hasLinkin || $hasDonate):


    ?>
    <div class="a-block apl-connect-with">
        <h4><?php echo sprintf( __( 'CONNECT WITH %s', 'apollo' ), $event_org->get_title()  ) ?></h4>

        <div class="art-social apl-full-width apl-none-float clearfix">
            <div class="el-blk">
                <?php
                if($hasEmail){
                    ?>
                    <div class="art-social-item"><i class="fa fa-envelope fa-lg">&nbsp;&nbsp;&nbsp;</i><a href="mailto:<?php echo $data['email'] ?>">
                            <?php  _e('Email','apollo') ?></a>
                        <div class="slash">/</div>
                    </div>
                <?php } ?>

                <?php
                if($hasWeb){
                    if (false === strpos($data['web'], '://')) {
				    $data['web'] = 'http://' . $data['web'];
					}
                    ?>
                    <div class="art-social-item"><i class="fa fa-link fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $data['web'] ?>">
                            <?php _e('Website','apollo') ?></a>
                        <div class="slash">/</div>
                    </div>
                <?php } ?>

                <?php
                if($hasFb){
                    ?>
                    <div class="art-social-item"><i class="fa fa-facebook fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $data['facebook'] ?>">
                            <?php _e('Facebook','apollo') ?></a>
                        <div class="slash">/</div>
                    </div>
                <?php } ?>

                <?php
                if($hasTwitter){
                    ?>
                    <div class="art-social-item"><i class="fa fa-twitter fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $data['twitter'] ?>">
                            <?php _e('Twitter','apollo') ?></a>
                        <div class="slash">/</div>
                    </div>
                <?php } ?>

                <?php
                if($hasIns){
                    ?>
                    <div class="art-social-item"><i class="fa fa-camera-retro fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $data['inst'] ?>">
                            <?php _e('Instagram','apollo') ?>  </a>
                        <div class="slash">/</div>
                    </div>
                <?php } ?>

                <?php
                if($hasLinkin){
                    ?>
                    <div class="art-social-item"><i class="fa fa-linkedin-square fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $data['linked'] ?>">
                            <?php _e('LinkedIn','apollo') ?>   </a>
                        <div class="slash">/</div>
                    </div>
                <?php } ?>


                <?php
                if($hasDonate){
                    ?>
                    <div class="art-social-item"><i class="fa fa-usd fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $data['donate'] ?>">
                            <?php _e('Donate','apollo') ?>   </a></div>
                <?php } ?>
            </div>

        </div>
    </div>
    <?php endif; ?>

<?php endif; ?>