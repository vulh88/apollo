<?php
$tabName = of_get_option(APL_Business_Module_Theme_Option::TAB_NAME);
$tabDesc = of_get_option(APL_Business_Module_Theme_Option::INTRODUCTION);
$map_api_key = of_get_option(Apollo_DB_Schema::_GOOGLE_API_KEY_BROWSER, '');
$map_pref = of_get_option(APL_Business_Module_Theme_Option::WHATS_NEARBY_STATIC);
global $wp;
?>
<?php if( $_a_event->isHasExpiredEvent($even_end_date)) { ?>
<p class="t-l">
<a href='https://www.google.com/maps/search/?api=1&query=<?php echo $str_address_all; ?>' target='new'>Full map and directions</a>

<?php }else{ ?>
<?php if ( (isset($_GET['nr']) && $_GET['nr'] == 1) || $map_pref < 1 ) { ?>
<div class="a-block locatn" id="apl-wnby-block">
    <a id="map">
    <div class="a-block-ct">
        <div class="lo-left">
            <div
                data-relation_id="_popup_google_map_full"
                data-radius="<?php echo of_get_option(APL_Business_Module_Theme_Option::DISPLAY_BS_TYPE_WITHIN, 1609) ?>"
                data-coor = "<?php echo $sCoor ?>"
                data-nonce="<?php echo wp_create_nonce('save-coor') ?>"
                data-address="<?php echo $str_address_all ?>"
                data-phone="<?php echo $_venue ? $_venue->get_meta_data(Apollo_DB_Schema::_VENUE_PHONE, Apollo_DB_Schema::_APL_VENUE_DATA) : '' ?>"
                data-url="<?php echo $_venue ? $_venue->get_permalink() : '' ?>"
                data-title="<?php echo $_venue ? $_venue->get_title() : '' ?>"
                id="what_near_by_map"></div>
            <p class="t-r"> <a href="javascript:void(0);"
                               data-target="#_popup_google_map"
                               id="_event_trigger_fullmap"><?php _e('Full map and directions', 'apollo') ?></a></p>
        </div>
    </div>
<h4><?php echo $tabName ? $tabName : __("WHAT'S NEAR BY", 'apollo') ?> <span> <i class="fa fa-map-marker"></i></span></h4>
    <?php
    $defaultMarkers = Apollo_App::getDefaultMarkersBusIcons();

    $defaultBusFilterTypes = array();
    $displayType = of_get_option(APL_Business_Module_Theme_Option::DISPLAY_BUSINESS_TYPES);
    if ($displayType) {
        foreach($displayType as $type => $val):
            if ($val != '1') continue;
            $defaultBusFilterTypes[$type] = array(
                'text'  => of_get_option($type.'_text'),
                'marker'  => of_get_option($type.'_marker'),
                'val'  => of_get_option($type),
            );
        endforeach;
    }
    $themeUri = get_template_directory_uri();
    ?>



    <div class="nofi-map" style="<?php echo count($defaultBusFilterTypes)> 10 ? 'padding: 15px;' : ''; ?>">
        <div class="filter-map business">
            <p class="filter-map-title-mobile"><?php  _e('FILTER MAP BY','apollo'); ?></p>
            <table class="tbl">
                <tr>
                    <td rowspan="2" class="vtop"><?php _e('FILTER MAP BY', 'apollo') ?></td>
                    <td>
                        <input checked type="radio" name="map_filter" value="all" id="m-all">
                        <label for="m-all"> <img class="lazy"
                                                 src="<?php echo $themeUri ?>/assets/images/ico-g-all.png"
                                                 data-original="<?php echo $themeUri ?>/assets/images/ico-g-all.png"
                                                 alt="ico-g-all.png"> <?php _e('All', 'apollo') ?></label>
                    </td>
                    <?php if( isset($displayType[APL_Business_Module_Theme_Option::BARS_CLUBS_SELECT])
                        && $displayType[APL_Business_Module_Theme_Option::BARS_CLUBS_SELECT] == 1):

                        $typeData = $defaultBusFilterTypes[APL_Business_Module_Theme_Option::BARS_CLUBS_SELECT];

                        if ($typeData && isset($typeData['marker']) && $typeData['marker']) {
                            $icon = $typeData['marker'];
                        } else {
                            $icon = $defaultMarkers[APL_Business_Module_Theme_Option::BARS_CLUBS_SELECT]['marker'];
                        }

                        $text = $typeData['text'];
                        ?>
                        <td class="normal-hidden">
                            <input data-cat="<?php echo $typeData ? $typeData['val'] : '' ?>" type="radio" name="map_filter" value="<?php echo APL_Business_Module_Theme_Option::BARS_CLUBS_SELECT ?>" id="club">
                            <label for="m-drink"> <img
                                    src="<?php echo $icon ?>"
                                    class="lazy"
                                    data-original="<?php echo $icon ?>"
                                    alt="ico-g-cup.png"> <?php echo $text ?></label>
                        </td>
                    <?php endif; ?>
                    <td>
                        <input type="radio" name="map_filter" value="venue" id="m-venue">
                        <?php $venueIco = Apollo_Map_Calc::getDefaultMarker(); ?>
                        <label for="m-venue"> <img
                                class="lazy"
                                data-original="<?php echo $venueIco ?>"
                                src="<?php echo $venueIco ?>"> <?php _e('Venue only', 'apollo') ?></label>
                    </td>
                </tr>
                <tr>

                    <?php if( isset($displayType[APL_Business_Module_Theme_Option::DINING_SELECT])
                        && $displayType[APL_Business_Module_Theme_Option::DINING_SELECT] == 1):

                        $typeData = $defaultBusFilterTypes[APL_Business_Module_Theme_Option::DINING_SELECT];

                        if ($typeData && isset($typeData['marker']) && $typeData['marker']) {
                            $icon = $typeData['marker'];
                        } else {
                            $icon = $defaultMarkers[APL_Business_Module_Theme_Option::DINING_SELECT]['marker'];
                        }


                        $text = $typeData['text'];
                        ?>
                        <td class="normal-hidden">
                            <input data-cat="<?php echo $typeData ? $typeData['val'] : '' ?>" type="radio" name="map_filter" value="<?php echo APL_Business_Module_Theme_Option::DINING_SELECT ?>" id="m-food">
                            <label for="m-food"> <img
                                    class="lazy"
                                    src="<?php echo $icon; ?>"
                                    data-original="<?php echo $icon; ?>"
                                    alt="ico-g-knife.png"> <?php echo $text ?></label>
                        </td>
                    <?php endif; ?>
                    <?php if( isset($displayType[APL_Business_Module_Theme_Option::ACCOMMODATION_SELECT])
                        && $displayType[APL_Business_Module_Theme_Option::ACCOMMODATION_SELECT] == 1):

                        $typeData = $defaultBusFilterTypes[APL_Business_Module_Theme_Option::ACCOMMODATION_SELECT];

                        if ($typeData && isset($typeData['marker']) && $typeData['marker']) {
                            $icon = $typeData['marker'];
                        } else {
                            $icon = $defaultMarkers[APL_Business_Module_Theme_Option::ACCOMMODATION_SELECT]['marker'];
                        }

                        $text = $typeData['text'];
                        ?>
                        <td  class="normal-hidden">
                            <input data-cat="<?php echo $typeData ? $typeData['val'] : '' ?>" type="radio" name="map_filter" value="<?php echo APL_Business_Module_Theme_Option::ACCOMMODATION_SELECT ?>" id="m-accom">
                            <label for="m-accom"> <img
                                    class="lazy"
                                    src="<?php echo $icon ?>"
                                    data-original="<?php echo $icon ?>"
                                    alt="ico-g-wscreen.png"> <?php echo $text ?></label>
                        </td>
                    <?php endif; ?>

                    <?php if( isset($displayType[APL_Business_Module_Theme_Option::LOCAL_BUSINESS_SELECT])
                        && $displayType[APL_Business_Module_Theme_Option::LOCAL_BUSINESS_SELECT] == 1):

                        $typeData = $defaultBusFilterTypes[APL_Business_Module_Theme_Option::LOCAL_BUSINESS_SELECT];

                        if ($typeData && isset($typeData['marker']) && $typeData['marker']) {
                            $icon = $typeData['marker'];
                        } else {
                            $icon = $defaultMarkers[APL_Business_Module_Theme_Option::LOCAL_BUSINESS_SELECT]['marker'];
                        }

                        $text = $typeData['text'];
                        ?>
                        <td  class="normal-hidden">
                            <input data-cat="<?php echo $typeData ? $typeData['val'] : '' ?>" type="radio" name="map_filter" value="<?php echo APL_Business_Module_Theme_Option::LOCAL_BUSINESS_SELECT ?>" id="m-accom">
                            <label for="m-accom"> <img
                                    class="lazy"
                                    src="<?php echo $icon ?>"
                                    data-original="<?php echo $icon ?>"
                                    alt="ico-g-wscreen.png"> <?php echo $text ?></label>
                        </td>
                    <?php endif; ?>
                </tr>
            </table>
        </div>


    </div>

    <?php
        $googleMapIcon = of_get_option(Apollo_DB_Schema::_GOOGLE_MAP_MARKER_ICON);
        if(!isset($googleMapIcon) || empty($googleMapIcon) ){
            $googleMapIcon = '/wp-content/themes/apollo/assets/images/icon-location.png';
        }
    ?>
    <div class="nofi-more list-markers" style="<?php echo count($defaultBusFilterTypes)> 10 ?'display:none;' : ''; ?>">
        <div class="n-star" data-main-flag>
            <img
                class="lazy"
                src="<?php echo $googleMapIcon ?>"
                data-original="<?php echo $googleMapIcon ?>"/>
            <p><?php echo $_venue ? $_venue->get_title() : '' ?></p>
        </div>
        <?php foreach($defaultBusFilterTypes as $ftype => $data): ?>
            <?php if( !empty($displayType[$ftype]) ): ?>
            <div class="n-blk <?php echo $ftype ?>">
                <label> <span><?php echo $data['text'] ?></span></label>
                <div class="n-blk-ct">
                    <ul data-src="<?php echo $data['marker'] ? $data['marker'] : $defaultMarkers[$ftype]['marker'] ?>">

                    </ul>
                </div>
            </div>
            <?php endif; ?>
        <?php endforeach; ?>
        <a href="javascript:;" data-target="nofi-more" class="nofi-exp ct-nofi-exp"><?php _e('View more nearby businesses', 'apollo') ?> [+]</a>
        </br>
    </div>

</div>
<?php }else{//end full ?>
    <div class="lo-left">
        <a href="https://www.google.com/maps/place/<?php echo $str_address_all ?>/"><img src="https://maps.googleapis.com/maps/api/staticmap?center=<?php echo $str_address_all ?>&zoom=14&scale=1&size=620x350&maptype=roadmap&key=<?php echo $map_api_key; ?>&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0xff0000%7Clabel:%7C<?php echo $str_address_all ?>" width=100% height=100% alt="Google Map of <?php echo $str_address_all ?>"></a>
        <form method="get" action="<?php echo home_url( $wp->request ); ?>#map" rel='nofollow' class="comment-form" >
            <input type="hidden" name="nr" value="1" />
            <div style="display: inline-grid;">
                <P>
                    <?php echo $tabDesc ? $tabDesc : __("See what's near this venue", 'apollo') ?>
                </P>


                <p class="t-l b-btn">
                    <input type="submit" id="submit" class="btn btn-b" value="<?php echo $tabName ? $tabName : __("WHAT'S NEAR BY", 'apollo') ?>" style="float: left;">
                </p>
        </form>
    </div>
    </div>
<?php }  ?>
<?php } ?>

<?php
include SONOMA_MODULES_DIR. '/event/templates/partials/single/parking-info.php';
?>

<input  type="hidden" id="data-view-more" data-viewmore="<?php _e('View more nearby businesses', 'apollo')?> [+]"/>
<input  type="hidden" id="data-view-less" data-viewless="<?php _e('Collapse', 'apollo') ?> [-]"/>
