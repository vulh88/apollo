<?php if(count($datas) > 0):
    $spotlightStyle = of_get_option(Apollo_DB_Schema::_EVENT_CATEGORY_SPOTLIGHT_STYLE, 'default')
    ?>
    <section class="list-category <?php echo $spotlightStyle == 'default' ? 'spotlight-left' : '' ?>">

    <?php
        $icon_position = of_get_option(Apollo_DB_Schema::_EVENT_SHOW_ICON_POSITION, '');
        $icon_fields = maybe_unserialize( get_option( Apollo_DB_Schema::_APL_EVENT_ICONS ) );
        foreach($datas as $data):
        $_a_event = get_event($data);

        $this->set_displayed_event( ( int ) $_a_event->id );

        /// STARTDATE-ENDDATE
        $_arr_show_date = $_a_event->getShowTypeDateTime('picture');
        $length_summary = Apollo_Display_Config::EVENT_OVERVIEW_NUMBER_CHAR;
        $even_dataIcons = Apollo_App::apollo_get_meta_data($_a_event->id ,Apollo_DB_Schema::_APL_EVENT_POST_ICONS,Apollo_DB_Schema::_APOLLO_EVENT_DATA);

        /** @Ticket #14350 */
        $thumbsUpEnable = of_get_option(Apollo_DB_Schema::_ENABLE_THUMBS_UP,1);
        $thumbsUpPosition = of_get_option(Apollo_DB_Schema::_EVENT_SHOW_THUMBS_UP_POSITION, 'default');
    ?>
    <article class="category-itm category-page-event-box">

        <?php if (of_get_option(Apollo_DB_Schema::_CATEGORY_PAGE_DATE_DISPLAY_OPTIONS,'default') == 'default') {?>
            <?php  $_a_event->render_circle_date(); ?>
        <?php } ?>
        <div class="pic">
            <?php if (of_get_option(Apollo_DB_Schema::_CATEGORY_PAGE_DATE_DISPLAY_OPTIONS,'default') == 'strip-top') {?>
                <?php  $_a_event->render_date_strip_top(); ?>
            <?php } ?>
            <a href="<?php echo $_a_event->get_permalink() ?>"><?php echo $_a_event->get_image('medium', array(), array(
                        'aw' => true,
                        'ah' => true,
                    )) ?></a></div>
        <div class="category-t">
            <?php if ($thumbsUpEnable && $thumbsUpPosition == 'next_title') : ?>
                <div class="event-thumbs-up-next-the-title">
                    <h2 class="category-ttl">
                        <a href="<?php echo $_a_event->get_permalink() ?>"> <?php echo $_a_event->get_title(true) ?></a>
                        <div class="like" id="_event_rating_<?php echo $_a_event->id . '_'. 'event' ?>" data-ride="ap-event_rating" data-i="<?php echo $_a_event->id ?>" data-t="event"> <a href="javascript:void(0);"><span class="_count">&nbsp;</span></a></div>
                    </h2>
                </div>
            <?php else : ?>
                <h2 class="category-ttl"> <a href="<?php echo $_a_event->get_permalink() ?>"> <?php echo $_a_event->get_title(true) ?></a></h2>
            <?php endif; ?>
            <?php if ($icon_position  == 'after_title'): ?>
                <?php $_a_event->renderIcons($icon_fields,$even_dataIcons); ?>
            <?php endif; ?>
            <p class="meta auth">
                <?php echo $_a_event->renderOrgVenueHtml() ?>
            </p>
            <?php if( $thumbsUpEnable ) : ?>
            <div class="cat-rating-box rating-action">
                <?php if ( $thumbsUpPosition == 'default' ) : ?>
                <div class="box-action-wrap">
                    <div class="like" id="_event_rating_<?php echo $_a_event->id . '_'. 'event' ?>" data-ride="ap-event_rating" data-i="<?php echo $_a_event->id ?>" data-t="event"> <a href="javascript:void(0);"><span class="_count">&nbsp;</span></a></div>
                </div>
                <?php endif; ?>
                <?php if ($icon_position == 'after_thumbs_up' || $icon_position == ''): ?>
                    <div class="icons-list-after-thumbsup">
                        <?php $_a_event->renderIcons($icon_fields,$even_dataIcons); ?>
                    </div>
                <?php endif; ?>
            </div>
            <?php endif ?>
            <p class="desc">
                <?php
                $summary =$_a_event->get_summary($length_summary);
                echo $summary['text']; ?><?php if($_a_event->is_have_more_summary($length_summary)) echo '...'; ?>
            </p>

            <?php if($_a_event->is_have_more_summary($length_summary)): ?>
                <a href="<?php echo $_a_event->get_permalink() ?>" class="vmore"><?php _e("View more", 'apollo') ?></a>
            <?php endif; ?>

            <div class="b-btn category">

                <?php
                    echo $_a_event->renderTickerBtn(array(
                            'class' => 'btn btn-category'
                        ));
                    echo $_a_event->renderCheckDCBtn(array(
                        'class' => 'btn btn-category'
                    ));
                    echo $_a_event->renderBookmarkBtn(array(
                            'class' => 'btn btn-category btn-bm apl-check-user-added'
                        ));
                ?>
            </div>
        </div>
    </article>
    <?php endforeach; ?>
</section>
<?php endif; ?>