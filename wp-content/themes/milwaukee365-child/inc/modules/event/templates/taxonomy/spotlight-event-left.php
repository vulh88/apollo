<article class="content-page astro-detail category-lft category-page-event-box">

<?php
if(empty($datas)):
?>
<br>
<p><?php _e('There are no events available for this category at this time.', 'apollo') ?></p>
<?php
else:
    $_a_event = get_event($datas);
    $characters = 200;
    $a_200_content = $_a_event->get_summary($characters);
    $arrShareInfo = $_a_event->getShareInfo();
    $length_summary = Apollo_Display_Config::EVENT_OVERVIEW_NUMBER_CHAR;

    ?>
    <div class="b-share-cat">

        <?php
        $checkTime = $_a_event->get_periods_time(" LIMIT 0, ". Apollo_Display_Config::MAX_DATETIME);
        $arraySocial =  array(
            'info' => $arrShareInfo,
            'id' => $_a_event->id,
            'data_btns' =>
                array(  'print',
                    'sendmail',
                )
        );

        if( is_array($checkTime) && count($checkTime) > 0){
            $arraySocial['data_btns'][] = 'ics';
            $arraySocial['data_btns'][] = 'googleCalendar';
        }
        SocialFactory::social_btns($arraySocial );
        ?>
        <input name="eventid" value="<?php echo $_a_event->id; ?>" type="hidden" />
    </div>

    <?php
    $socialMediaButtons = ob_get_clean();
    $socialMediaButtonsLocation = of_get_option(Apollo_DB_Schema::_EVENT_SOCIAL_MEDIA_LOCATION_DETAIL, 1);
    $icon_position = of_get_option(Apollo_DB_Schema::_EVENT_SHOW_ICON_POSITION, '');
    $even_dataIcons = Apollo_App::apollo_get_meta_data($_a_event->id ,Apollo_DB_Schema::_APL_EVENT_POST_ICONS,Apollo_DB_Schema::_APOLLO_EVENT_DATA);
    $icon_fields = maybe_unserialize( get_option( Apollo_DB_Schema::_APL_EVENT_ICONS ) );
    /** @Ticket #14350 */
    $thumbsUpEnable = of_get_option(Apollo_DB_Schema::_ENABLE_THUMBS_UP,1);
    $thumbsUpPosition = of_get_option(Apollo_DB_Schema::_EVENT_SHOW_THUMBS_UP_POSITION, 'default');

    if ($socialMediaButtonsLocation == 1) echo $socialMediaButtons;
    ?>

    <div class="social-icon-pck">
        <div class="page-tool">
        </div>
        <?php if ( $thumbsUpEnable && $thumbsUpPosition == 'next_title' ) : ?>
            <div class="event-thumbs-up-next-the-title">
                <h1 class="p-ttl category-lft-title">
                    <a href="<?php echo $_a_event->get_permalink() ?>"><?php echo $_a_event->get_title(true) ?></a>
                    <div class="like" id="_event_rating_<?php echo $_a_event->id . '_' . 'event' ?>" data-ride="ap-event_rating" data-i="<?php echo $_a_event->id ?>" data-t="event"> <a href="javascript:void(0);"><span class="_count">&nbsp;</span></a></div>
                </h1>
            </div>
        <?php else : ?>
            <h1 class="p-ttl category-lft-title"><a href="<?php echo $_a_event->get_permalink() ?>"><?php echo $_a_event->get_title(true) ?></a></h1>
        <?php endif; ?>

        <?php if ($icon_position  == 'after_title'): ?>
            <?php $_a_event->renderIcons($icon_fields,$even_dataIcons); ?>
        <?php endif; ?>
        <p class="meta auth"><?php echo $_a_event->renderOrgVenueHtml(); ?></p>

        <?php if ($socialMediaButtonsLocation == 2) echo $socialMediaButtons; ?>

        <div class="rating-box rating-action">
            <div class="box-action-wrap">
            <?php if( $thumbsUpEnable && $thumbsUpPosition == 'default' ) : ?>
                <div class="like parent-not-underline-hover" id="_event_rating_<?php echo $_a_event->id . '_' . 'event' ?>" data-ride="ap-event_rating" data-i="<?php echo $_a_event->id ?>" data-t="event"> <a href="javascript:void(0);"><span class="_count">&nbsp;</span></a></div>
            <?php endif ?>
            <?php
            if ( isset($allow_comment) && $allow_comment ):
                ?>
                <div class="cm"> <a href="javascript:;"><?php _e( 'Comment', 'apollo' ) ?></a></div>

            <?php endif; ?>
            </div>
            <!--    <div class="bok-mk"><a href="javascript:;">Bookmark</a></div>-->

            <?php if ($icon_position == 'after_thumbs_up' || $icon_position == ''): ?>
                <div class="icons-list-after-thumbsup">
                    <?php $_a_event->renderIcons($icon_fields,$even_dataIcons); ?>
                </div>
            <?php endif; ?>
        </div>


        <div class="astro-featr ct-s-c-d-p">
            <article class="blog-itm">
                <?php  $_a_event->render_circle_date(); ?>

                <div class="pic">
                    <a href="<?php echo $_a_event->get_permalink() ?>"><?php echo $_a_event->get_image('medium') ?></a>
                </div>

                <?php

                $summary = $_a_event->get_full_excerpt();
                if ( $summary ):
                    ?>
                    <div class="a-txt-fea" id="_ed_sum_short">
                        <?php echo $summary; ?>
                    </div>
                <?php endif; ?>


                <div class="b-btn __inline_block_fix_space">

                    <?php echo $_a_event->renderTickerBtn(array(
                        'class' => 'btn btn-b'
                    )) ?>

                    <?php echo $_a_event->renderCheckDCBtn(array(
                        'class' => 'btn btn-b'
                    )) ?>

                    <?php echo $_a_event->renderBookmarkBtn( array( 'class' => 'btn-bm btn btn-b' ) ); ?>
                </div>


                <div class="a-desc apl-internal-content desc-evt" id="_ed_short">
                    <p class="desc">
                        <?php echo $a_200_content['text'].''?>
                        <?php if($_a_event->is_have_more_summary($length_summary)) echo '...'; ?>
                    </p>
                    <a href="<?php echo $_a_event->get_permalink() ?>" class="vmore" ><?php _e('View more', 'apollo') ?></a>

                </div>


            </article>
        </div>
        <?php
        include('social-meta.php');
        ?>
    </div>
    <?php require_once APOLLO_TEMPLATES_DIR.'/events/google-map-popup.php' ?>
    <?php require_once APOLLO_TEMPLATES_DIR.'/events/chooes-export-popup.php' ?>

<?php endif; ?>

</article>
