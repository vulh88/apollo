<?php
    $icon_position = of_get_option(Apollo_DB_Schema::_EVENT_SHOW_ICON_POSITION, '');
    $icon_fields = maybe_unserialize( get_option( Apollo_DB_Schema::_APL_EVENT_ICONS ) );
    /** @Ticket #14350 */
    $thumbsUpEnable = of_get_option(Apollo_DB_Schema::_ENABLE_THUMBS_UP,1);
    $thumbsUpPosition = of_get_option(Apollo_DB_Schema::_EVENT_SHOW_THUMBS_UP_POSITION, 'default');
    foreach($datas as $data ):
    $_a_event = get_event($data);
    // $this->set_displayed_event( ( int ) $_a_event->id );
    $_arr_show_date = $_a_event->getShowTypeDateTime('picture');
    $length_summary = Apollo_Display_Config::EVENT_OVERVIEW_NUMBER_CHAR;
    $even_dataIcons = Apollo_App::apollo_get_meta_data($_a_event->id ,Apollo_DB_Schema::_APL_EVENT_POST_ICONS,Apollo_DB_Schema::_APOLLO_EVENT_DATA);
?>
    <div class="more-cat-itm category-page-event-box">
        <div class="more-pic"><a href="<?php echo $_a_event->get_permalink() ?>"><?php echo $_a_event->get_image('thumbnail', array(), array(
                        'aw' => true,
                        'ah' => true,
                    )) ?></a></div>
        <div class="more-ct">
            <?php if ( $thumbsUpEnable && $thumbsUpPosition == 'next_title' ) : ?>
                <div class="event-thumbs-up-next-the-title">
                    <h3>
                        <a href="<?php echo $_a_event->get_permalink() ?>"><?php echo $_a_event->get_title(TRUE) ?></a>
                        <div class="like" id="_event_rating_<?php echo $_a_event->id . '_'. 'event' ?>" data-ride="ap-event_rating" data-i="<?php echo $_a_event->id ?>" data-t="event"> <a href="javascript:void(0);"><span class="_count">&nbsp;</span></a></div>
                    </h3>
                </div>
            <?php else : ?>
                <h3> <a href="<?php echo $_a_event->get_permalink() ?>"><?php echo $_a_event->get_title(TRUE) ?></a></h3>
            <?php endif; ?>

            <?php if ($icon_position  == 'after_title') {
                $_a_event->renderIcons($icon_fields,$even_dataIcons);
            } ?>

            <p class="meta auth"><?php echo $_a_event->renderOrgVenueHtml() ?></p>
            <p class="p-date">
                <?php

                    if($_arr_show_date['type'] !== 'none') {
                        $us = $_arr_show_date['unix_start'];
                        $ue = $_arr_show_date['unix_end'];

                        if($_arr_show_date['type'] === 'one'){
                            echo Apollo_App::apl_date('M d', $us).', <span>'.date('Y', $us).'</span>';
                        }
                        else if($_arr_show_date['type'] === 'two') {
                            // same year
                            if(date('Y', $ue) === date('Y', $us)) {
                                echo Apollo_App::apl_date('M d', $us).' - '.Apollo_App::apl_date('M d', $ue).', <span>'.date('Y', $us).'</span>';
                            }
                            else {
                                echo Apollo_App::apl_date('M d', $us).', <span>'.date('Y', $us).'</span>'.' - '.Apollo_App::apl_date('M d', $ue).', <span>'.date('Y', $ue).'</span>';
                            }
                        }
                } ?>

            </p>
    <?php if( $thumbsUpEnable ) : ?>
            <div class="more-cat-rating-box">
                <?php if ( $thumbsUpPosition == 'default' ) : ?>
                <div class="box-action-wrap">
                    <div class="like" id="_event_rating_<?php echo $_a_event->id . '_'. 'event' ?>" data-ride="ap-event_rating" data-i="<?php echo $_a_event->id ?>" data-t="event"> <a href="javascript:void(0);"><span class="_count">&nbsp;</span></a></div>
                </div>
                <?php endif; ?>
                <?php if ($icon_position == 'after_thumbs_up' || $icon_position == ''): ?>
                    <div class="icons-list-after-thumbsup">
                        <?php $_a_event->renderIcons($icon_fields,$even_dataIcons); ?>
                    </div>
                <?php endif; ?>
            </div>
    <?php endif ?>
        </div>
        <div class="b-btn">
            <?php echo $_a_event->renderTickerBtn(array(
                    'class' => 'btn btn-b'
            )) ?>
            <?php echo $_a_event->renderCheckDCBtn(array(
                    'class' => 'btn btn-b'
                )) ?>
            <?php echo $_a_event->renderBookmarkBtn(array(
                    'class' => 'btn btn-b btn-bm'
            )) ?></div>
    </div>
<?php endforeach; ?>


