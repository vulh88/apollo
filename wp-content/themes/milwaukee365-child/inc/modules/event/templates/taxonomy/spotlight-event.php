<?php
// REPAIR DATA - JUST SHOW SIMPLE EVENT
if(!empty($datas)):

$_a_event = get_event($datas);

/// STARTDATE-ENDDATE
$_arr_show_date = $_a_event->getShowTypeDateTime('picture');

///SOCIAL SHARE INFO
$arrShareInfo = $_a_event->getShareInfo();

/* number vote */
$ivote = $_a_event->getVote();

$length_summary = Apollo_Display_Config::EVENT_OVERVIEW_NUMBER_CHAR;
$icon_position = of_get_option(Apollo_DB_Schema::_EVENT_SHOW_ICON_POSITION, '');
$even_dataIcons = Apollo_App::apollo_get_meta_data($_a_event->id ,Apollo_DB_Schema::_APL_EVENT_POST_ICONS,Apollo_DB_Schema::_APOLLO_EVENT_DATA);
$icon_fields = maybe_unserialize( get_option( Apollo_DB_Schema::_APL_EVENT_ICONS ) );
/** @Ticket #14350 */
$thumbsUpEnable = of_get_option(Apollo_DB_Schema::_ENABLE_THUMBS_UP,1);
$thumbsUpPosition = of_get_option(Apollo_DB_Schema::_EVENT_SHOW_THUMBS_UP_POSITION, 'default');

?>
<article class="content-page category-detail category-page-event-box">
    <div class="b-share-cat">
        <?php
            SocialFactory::social_btns( array( 'info' => $arrShareInfo, 'id' => $_a_event->id ) );
        ?>
    </div>
    <?php if ( $thumbsUpEnable && $thumbsUpPosition == 'next_title' ) : ?>
        <div class="event-thumbs-up-next-the-title">
            <h1 class="p-ttl">
                <a href="<?php echo $_a_event->get_permalink() ?>"><?php echo $_a_event->get_title(TRUE) ?></a>
                <div class="like" id="_event_rating_<?php echo $_a_event->id .'_'.'event' ?>" data-ride="ap-event_rating" data-i="<?php echo $_a_event->id ?>" data-t="event"> <a href="javascript:void(0);"><span class="_count">&nbsp;</span></a></div>
            </h1>
        </div>
    <?php else : ?>
        <h1 class="p-ttl"><a href="<?php echo $_a_event->get_permalink() ?>"><?php echo $_a_event->get_title(TRUE) ?></a></h1>
    <?php endif; ?>

    <?php if ($icon_position  == 'after_title') {
        $_a_event->renderIcons($icon_fields,$even_dataIcons);
    } ?>

    <p class="meta auth"><?php echo $_a_event->renderOrgVenueHtml() ?></p>
    <?php if( $thumbsUpEnable ) : ?>
    <div class="cat-rating-box rating-action">
        <?php if( $thumbsUpPosition == 'default' ) : ?>
        <div class="box-action-wrap">
            <div class="like" id="_event_rating_<?php echo $_a_event->id .'_'.'event' ?>" data-ride="ap-event_rating" data-i="<?php echo $_a_event->id ?>" data-t="event"> <a href="javascript:void(0);"><span class="_count">&nbsp;</span></a></div>
        </div>
        <?php endif; ?>
        <?php if ($icon_position == 'after_thumbs_up' || $icon_position == ''): ?>
            <div class="icons-list-after-thumbsup">
                <?php $_a_event->renderIcons($icon_fields,$even_dataIcons); ?>
            </div>
        <?php endif; ?>
    </div>
    <?php endif ?>
    <div class="pic">


        <a href="<?php echo $_a_event->get_permalink() ?>">
            <span class="date">
            <?php  $_a_event->render_circle_date(); ?>
            </span>
            <?php echo $_a_event->get_image('medium', array(), array(
                    'aw' => true,
                    'ah' => true,
                )) ?></a>
    </div>
    <div class="b-btn cat-detail __inline_block_fix_space">
        <?php echo $_a_event->renderTickerBtn(array(
                'class' => 'btn btn-cat-detail'
            )) ?>

        <?php echo $_a_event->renderCheckDCBtn(array(
                'class' => 'btn btn-cat-detail'
            )) ?>

        <?php echo $_a_event->renderBookmarkBtn(array(
                'class' => 'btn btn-cat-detail btn-bm'
            )) ?>
    </div>
    <p class="desc">
        <?php
        $summary = $_a_event->get_summary($length_summary );
        echo $summary['text'] ?> <?php if($_a_event->is_have_more_summary($length_summary )): ?>...<?php endif ?>
    </p>
    <?php if($_a_event->is_have_more_summary($length_summary )): ?>
        <a href="<?php echo $_a_event->get_permalink() ?>" class="vmore" ><?php _e('View more', 'apollo') ?></a>
    <?php endif; ?>
</article>
<?php endif; ?>
