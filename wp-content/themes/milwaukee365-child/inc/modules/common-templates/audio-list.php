<?php
$audioList = $model->extraInfo['audio'];
if(is_array($audioList) && count($audioList) > 0 ){
    ?>
    <h4><?php _e('AUDIO','apollo') ?><span> <i class="fa fa-file-audio-o"></i></span></h4>
    <div class="a-block-ct">
        <ul class="audio-list">
            <?php
            foreach($audioList as $item):
                ?>
                <li>
                    <p> <strong><?php echo $item['desc'] ?></strong></p>

                    <?php
                    if($item['type'] == 'file'){
                        ?>
                        <p>
                            <audio controls="controls" style="border:0px;width:100%;height:50px;">
                                <source src='<?php echo $item['link'] ?>' type="audio/ogg">
                                <source src='<?php echo $item['link'] ?>' type="audio/mpeg">
                                <embed height="50" width="100%" src='<?php echo $item['link'] ?>'> </embed>
                            </audio>
                        </p>
                        <?php
                    }else{
                        echo ' <p style="margin-top: 25px">';
                        $val = str_replace('\\','',$item['link']);

                        echo $val;
                        echo '  </p>';
                    }
                    ?>

                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php } ?>