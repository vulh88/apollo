<?php  $photos =  $model->extraInfo['photos'];
if(  $photos) { ?>
<div class="<?php echo isset($_vg_block_class) && $_vg_block_class ? $_vg_block_class : 'a-block' ?>">
    <h4><?php _e('PHOTOS', 'apollo') ?><span> <i class="fa fa-picture-o"></i></span></h4>
    <div class="a-block-ct-photo">
        <div class="photo-inner">
            <a href="#" class="fullscreen"> <i class="fa fa-arrows-alt fa-2x"></i></a>

            <ul class="bxslider">
                <?php echo $model->extraInfo['big_sliders']; ?>
            </ul>
            <ul class="bx-description">
                <li>
                    <p><?php echo nl2br($model->extraInfo['first_desc']) ?></p>
                </li>
            </ul>
            <div id="bx-pager">
                <?php echo $model->extraInfo['thumb_sliders']; ?>
            </div>
        </div>
        <div class="loader"><a><i class="fa fa-spinner fa-spin fa-3x"></i> </a></div>
    </div>
    <!-- Video box pop up -->
    <div class="photo-box">
        <?php echo $model->extraInfo['popup_big_sliders']; ?>
        <div class="thumb-slider">
            <ul class="slider-video">
                <?php echo $model->extraInfo['popup_thumb_sliders'];; ?>
            </ul>
        </div>
        <div class="closevideo"><i class="fa fa-times fa-lg"></i></div>
    </div><!-- End video box pop up -->
</div>
<?php }?>

<input type="hidden" name="total-gallerys" value="<?php count($photos) ?>"/>
