<?php
$photos =  $model->getPhotoGallery();
$videos =  $model->extraInfo['videos'];

if (count($photos) == 1 && !$photos[0]) {
    $photos = false;
}

if ($photos || $videos):
?>
<div class="<?php echo isset($_vg_block_class) && $_vg_block_class ? $_vg_block_class : 'a-block' ?>">

    <?php
    if(  $photos) { ?>
    <h4><?php _e('PHOTOS', 'apollo') ?><span> <i class="fa fa-picture-o"></i></span></h4>
    <div class="a-block-ct-photo">
        <div class="photo-inner">
            <a href="#" class="fullscreen"> <i class="fa fa-arrows-alt fa-2x"></i></a>

            <ul class="bxslider">
                <?php echo $model->extraInfo['big_sliders']; ?>
            </ul>
            <ul class="bx-description">
                <li>
                    <p><?php echo nl2br($model->extraInfo['first_desc']) ?></p>
                </li>
            </ul>
            <div id="bx-pager">
                <?php echo $model->extraInfo['thumb_sliders']; ?>
            </div>
        </div>
        <div class="loader"><a><i class="fa fa-spinner fa-spin fa-3x"></i> </a></div>
    </div>

    <!-- Video box pop up -->
    <div class="photo-box">
        <?php echo $model->extraInfo['popup_big_sliders'] ?>
        <div class="thumb-slider">
            <ul class="slider-video">
                <?php echo $model->extraInfo['popup_big_sliders'] ?>
            </ul>
        </div>
        <div class="closevideo"><i class="fa fa-times fa-lg"></i></div>
    </div><!-- End video box pop up -->
    <?php }?>


    <?php
    if(  !empty($videos )) { ?>
    <h4><?php _e( 'VIDEOS', 'apollo' ) ?><span> <i class="fa fa-film"></i></span></h4>
    <div class="a-block-ct-video">
       <?php echo $model->extraInfo['video_wrapper']; ?>
        <div class="blank"></div>
        <ul class="video-description">
            <li>
                <p><?php echo nl2br($model->extraInfo['video_first_desc']) ?></p>
            </li>
        </ul>
        <div id="video-pager">
            <?php echo $model->extraInfo['video_thumbs'] ?>
        </div>
        <div class="vd-loader"><a><i class="fa fa-spinner fa-spin fa-3x"></i> </a></div>
    </div>
    <?php } ?>


</div>
<input type="hidden" name="total-videos" value="<?php echo $videos ? count($videos) : 0 ?>" />

<input type="hidden" name="total-gallerys" value="<?php echo count($photos) ? count($photos) : 0 ?>"/>
<?php endif ?>

