<?php

Class SMAdminModuleMediaSetting {



    public function __construct() {
        // Init
        add_action( 'admin_menu', array( __CLASS__, 'init' ) );

    }


    /**
     * Init for the option page
     *
     * @access public
     * @return void
     * @authorTruonghn
     */
    public static function init() {
        // Check if admin
        if ( ! is_admin() ) {
            return;
        }
        // Add the setting field for this size
        add_settings_field( 'circle_image_size',  __( 'Spotlight circle image size', 'apollo' ), array(
            __CLASS__,
            'image_sizes'
        ), 'media', 'default', array( 'name' => 'circle_image_size', 'width' => SONOMA_CIRCLE_IMAGE_WIDTH , 'height' => SONOMA_CIRCLE_IMAGE_HEIGHT ) );
        $attrImageSize = array('_h' => SONOMA_CIRCLE_IMAGE_HEIGHT ,'_w' => SONOMA_CIRCLE_IMAGE_WIDTH );
        foreach ($attrImageSize as  $attr => $value ){
            register_setting( 'media', SM_CIRCLE_IMAGE_SIZE.''.$attr );
            if( !get_option( SM_CIRCLE_IMAGE_SIZE.''.$attr)){

                update_option( SM_CIRCLE_IMAGE_SIZE.''.$attr, $value);
            }
        }
    }

    /**
     * Display the row of the image size
     *
     * @access public
     * @param mixed $args
     * @return void
     * @author TruongHN
     */
    public static function image_sizes( $args ) {

        if ( is_integer( $args['name'] ) ) {
            return;
        }

        $height = get_option( SM_CIRCLE_IMAGE_SIZE.'_h' ) ? get_option( SM_CIRCLE_IMAGE_SIZE.'_h' ) : $args['height'];
        $width  = get_option( SM_CIRCLE_IMAGE_SIZE.'_w' ) ? get_option( SM_CIRCLE_IMAGE_SIZE.'_w' ) : $args['width'];

        ?>

        <label class="sis-label" for="<?php echo SM_CIRCLE_IMAGE_SIZE.'_w'; ?>">
            <?php _e( 'Max width', 'simple-image-sizes' ); ?>
            <input name="<?php echo SM_CIRCLE_IMAGE_SIZE.'_w'; ?>" class='w small-text' type="number" step="1" min="0"   id="<?php echo SM_CIRCLE_IMAGE_SIZE.'_w'; ?>"
                   value="<?php echo esc_attr( $width ); ?>"/>
        </label>
        <label class="sis-label" for="<?php echo SM_CIRCLE_IMAGE_SIZE.'_h'; ?>">
            <?php _e( 'Max height', 'apollo' ); ?>
            <input name="<?php echo SM_CIRCLE_IMAGE_SIZE.'_h'; ?>" class='h small-text' type="number" step="1" min="0"  id="<?php  echo SM_CIRCLE_IMAGE_SIZE.'_h' ?>" value="<?php echo esc_attr( $height ); ?>"/>
        </label>
        <br>

    <?php }


}

new SMAdminModuleMediaSetting();