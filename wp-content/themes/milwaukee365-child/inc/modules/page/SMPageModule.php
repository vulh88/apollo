<?php

class SMPageModule
{
    public function __construct(){

        $this->renderPageTemplate();
    }

    public function renderPageTemplate(){
        if(Apollo_App::isDashboardPage()) {
            $file = SONOMA_MODULES_DIR . '/page/templates/dashboard.php';
        }
        else if ( get_query_var('_apollo_page_blog' ) == 'true' ) {
            if ( ! Apollo_App::is_avaiable_module( 'post' ) ) {
                wp_safe_redirect( '/' );
            }
            $file = SONOMA_MODULES_DIR . '/page/templates/blog.php';
        }
        else {
            $file = SONOMA_MODULES_DIR . '/page/templates/default.php';
        }
        if (file_exists($file)) {
            SMViewHelper::smGetTemplatePartCustom($file, array());
        }
    }

}

new SMPageModule();