
<?php
    // Fix redirect classified page
    if ( get_the_ID() == Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ADD_CLASSIFIED )) {
        wp_redirect('/');
    }

while ( have_posts() ) : the_post(); ?>
    <?php the_content() ?>
<?php endwhile; ?>

<?php
if ( ! get_the_content() ) {
    _e( 'Please insert content for page', 'apollo');
}
?>