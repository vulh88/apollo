<!--<link rel='stylesheet' id='apollo_reset_css-css'  href='--><?php //echo get_template_directory_uri() ?><!--/assets/css/reset.css' type='text/css' media='all' />-->
<?php

if ( get_the_ID() == Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ADD_CLASSIFIED )) {
	wp_redirect('/');
}

if ( get_the_ID() != Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_FORGOT_PASS_PAGE)
	&& get_the_ID() != Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_REGISTER_PAGE)
	&& get_the_ID() != Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_LOGIN_PAGE) ) {
	?>

	<!--<link rel='stylesheet' id='apollo_reset_css-css'  href='--><?php //echo get_template_directory_uri() ?><!--/assets/css/reset.css' type='text/css' media='all' />-->
<?php } ?>
<?php Apollo_App::the_breadcrumb(); ?>

<?php while ( have_posts() ) : the_post();

	add_filter( 'excerpt_length', function() {
		return Apollo_Display_Config::MAX_SHORT_DESC;
	}, 999 );
	add_filter( 'excerpt_more', function () {return '';});

	$arrShareInfo = array(
		'url' => get_the_permalink(),
		'summary' => get_the_excerpt(),
	);
	?>
	<div class="b-share-cat">
		<?php SocialFactory::social_btns( array( 'info' => $arrShareInfo, 'id' => get_the_ID() ) ); ?>
	</div>

	<article class="apl-internal-content" id="post-<?php the_ID(); ?>" <?php post_class('content-page'); ?>>
		<?php the_content() ?>
	</article>

<?php endwhile; ?>

<?php
if ( ! get_the_content() ) {
	_e( 'Please insert content for page', 'apollo');
}
?>

<?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>