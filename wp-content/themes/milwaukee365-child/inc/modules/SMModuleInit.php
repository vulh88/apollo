<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

require_once SONOMA_MODULES_DIR.'/AbstractSMSingleService.php';
require_once SONOMA_MODULES_DIR.'/AbstractSMListService.php';
require_once SONOMA_MODULES_DIR.'/SMAdminModuleMediaSetting.php';
require_once SONOMA_HELPER_DIR.'/SMViewHelper.php';

class SMModuleInit
{

    public function __construct($type){

        $isAvailableModule = Apollo_App::is_avaiable_module($type);


        if ($type == SMCommonConst::_SM_SINGLE_CONTENT_POST_TYPE) {
            global $post;
            $isAvailableModule = Apollo_App::is_avaiable_module($post->post_type);
            $type = $post->post_type;
        }

        $file = sprintf(SONOMA_MODULES_DIR. '/'. $type . '/SM' . ucfirst($type) . 'Module.php');

        if (!file_exists($file) ||  ( is_single() && !$isAvailableModule ) ) {
            _e('Page not available.', 'apollo');
        }
        else {
            require_once $file;
        }

    }

}
