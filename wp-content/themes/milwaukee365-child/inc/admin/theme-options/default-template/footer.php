<section class="copyright t-c">
    <div class="inner">
        <p>&copy; 2015 Utah Arts &amp; Cultural Coaltion - All rights reserved.</p>

        <p class="space"><a href="#">Contact US</a> | <a href="#">Privacy Policy/Terms of Service</a></p>

        <p>NowPlayingUtah is the comprehensive one-stop source for arts and cultural events, performances, exhibitions, sports and recreation throughtout the state of Utah.</p>
    </div>
</section>