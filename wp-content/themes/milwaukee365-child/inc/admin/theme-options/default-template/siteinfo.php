<footer class="footer">
    <div class="inner">
        <section class="row">
            <div class="ft-blk wc-3">
                <div class="ft-ct">
                    <a href="#" target="_blank"><img src="/wp-content/themes/apollo/assets/uploads/logo_ntive.png" alt="logo nagative"></a>
                    <p>Sizzling blues combined with tasty micro brews headline the Brews and Blues event, sponsored by the Saratoga Chamber of Commerce. Performing will be the.</p>
                </div>
            </div>
            <div class="ft-blk wc-3 tw-blk">
                <h4 class="ft-ttl">LATEST TWEETS</h4>
                <div class="ft-ct">
                    <ul>
                        <li class="item">
                            <div class="img">
                                <a href="#" target="_blank"><img src="/wp-content/themes/apollo/assets/uploads/b1.jpg" alt="tw pic"></a>
                            </div>
                            <p class="name"><a href="#" target="_blank">Sally-Anne Kaminski </a>@socialkeyword</p>
                            <p class="dsc"> <a href="#" target="_blank">`.@timsaekoo </a>at <a href="#" target="_blank">link@tint </a> is top notch. HIGHLY recommend!</p>
                            <p class="dateline"> <a href="#" target="_blank">AUG 21.2014</a></p>
                        </li>
                        <li class="item">
                            <div class="img">
                                <a href="#" target="_blank"><img src="/wp-content/themes/apollo/assets/uploads/b2.jpg" alt="tw pic"></a>
                            </div>
                            <p class="name"><a href="#" target="_blank">Anne Liston</a>@socialkeyword</p>
                            <p class="dsc">
                                at Just like timelines on twitter.com, embeddable timelines are interactive and enable your visitors to reply<a href="#" target="_blank">link@tint </a> is top notch. HIGHLY recommend!
                            </p>
                            <p class="dateline"> <a href="#" target="_blank">AUG 21.2014</a></p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="ft-blk wc-3 contact-info">
                <h4 class="ft-ttl">CONTACT INFO</h4>
                <div class="ft-ct">
                    <p> <span><i class="fa fa-map-marker fa-lg"></i></span>
                        <label class="add">Address:</label>1900 Logeenston Ave., Michigan, USA
                    </p>
                    <p> <span> <i class="fa fa-phone fa-lg"></i></span>
                        <label class="phone">Phone:</label>(0531) 281-21-21
                    </p>
                    <p> <span> <i class="fa fa-envelope-o fa-lg"></i></span>
                        <label class="email">Email:</label>contact.us@yourcompany.com
                    </p>
                </div>
            </div>
        </section>
    </div>
</footer>