
<?php

$currentTerm = get_query_var('term');
$currentTerm = get_term_by('slug', $currentTerm, get_query_var( 'taxonomy' ));
$GLOBALS['apl_current_event_term'] = $currentTerm ? $currentTerm->term_id : ''; // For the custom widget
$GLOBALS['apl_current_event_term_object'] = $currentTerm; // For the custom widget

$tax = $currentTerm->taxonomy;

switch($tax) {
    case 'event-type':
        new SMModuleInit('event');
}
