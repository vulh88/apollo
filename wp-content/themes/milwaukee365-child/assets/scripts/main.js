var controller = new ScrollMagic.Controller();


function notFoundAnim() {

    if ($('.not-found').length) {

        var bigNumber = $('#bigNumber'),
            smallText = $('.not-found h2 span'),
            hiddenText = $('.not-found h2 strong');

        var mySplitText = new SplitText(bigNumber, { type: "chars" }),
            chars = mySplitText.chars; //an array of all the divs that wrap each character
        zero = $('.not-found h1 div:nth-child(2)');



        var notFound = new TimelineMax()
            .to(bigNumber, 1, { y: "0%" })
            .to(smallText, 1, { opacity: 1 })
            .to(zero, 0.5, { y: "200%" })
            .set(zero, { y: "-200%", text: "1" })
            .to(zero, 0.5, { y: "0%" })
            .to(hiddenText, 1, { display: "block", autoAlpha: 1, maxHeight: "40px" }, "-=1")
            .duration(3)
            .delay(1);

    }

}


// SLIDES HEADER UP ON SCROLL DOWN, AND DOWN ON SCROLL UP
function toggleHeader() {
    // Hide Header on on scroll down
    var didScroll;
    var lastScrollTop = 0;
    var delta = 5;
    var navbarHeight = $('.js-header').outerHeight();

    $(window).scroll(function(event) {
        didScroll = true;
    });

    setInterval(function() {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);

    function hasScrolled() {
        var st = $(this).scrollTop();

        // Make sure they scroll more than delta
        if (Math.abs(lastScrollTop - st) <= delta)
            return;

        // if they're not at the top add a class
        if (st !== 0) {
            $('.js-header').addClass('unpinned');
        } else {
            $('.js-header').removeClass('unpinned');
        }

        // If they scrolled down and are past the navbar, add class .nav-up.
        // This is necessary so you never see what is "behind" the navbar.
        if (st > lastScrollTop && st > navbarHeight) {
            // Scroll Down
            $('.js-header').removeClass('nav-down').addClass('nav-up');
        } else {
            // Scroll Up
            if (st + $(window).height() < $(document).height()) {
                $('.js-header').removeClass('nav-up').addClass('nav-down');
            }
        }

        lastScrollTop = st;
    }
}

// NAV TRIGGER ANIMATION
function slideMenu() {

    var menuOpen = false;

    $('.js-nav-trigger').click(function() {
        $(this).toggleClass('open');
        $('.js-slide-menu').toggleClass('open');
        $('body').toggleClass('menu-open');

        $('.js-trip-planner-trigger').removeClass('open');
        $('.js-trip-planner').removeClass('open');



        if (menuOpen === false) {
            menuOpen = true;
        } else {
            menuOpen = false;
        }
    });

    $('main').click(function() {
        if (menuOpen === true) {
            $('body').removeClass('menu-open');
            $('.js-nav-trigger').removeClass('open');
            $('.js-slide-menu').removeClass('open');
            menuOpen = false;
        }
    });

    // MOBILE COLLAPSEABLE NAV ITEMS
    $('.js-open-children').click(function() {
        var children = $(this).siblings('.slide-menu__children');
        // checks if first level
        if ($(this).parent().hasClass('parent')) {
            var parent = $(this).parent('.parent');
            var uncle = parent.siblings('.parent');

            // closes other parents
            if (uncle.children('.js-open-children').hasClass('open')) {
                uncle.children('.js-open-children').toggleClass('open');
                uncle.find('.open').toggleClass('open');
            }
        }

        if ($(this).hasClass('open')) {
            // close all open children
            children.find('.open').toggleClass('open');
        }
        $(this).toggleClass('open');
        children.toggleClass('open');
    });


}


function selectDropdowns() {
    $('.js-dropdown').each(function() {
        $(this).selectric({
            inheritOriginalWidth: false,
            allowWrap: false,
            expandToItemText: true
        }).on('change', function(event, element, selectric) {
            var $target = $(event.currentTarget);
            if ($target.hasClass("js-dropdown-filter")) {
                var query = "";
                var filters = $(".js-dropdown-filter").children("option").filter(":selected").each(function() {
                    var $this = $(this);
                    if ($this.val() !== "") {
                        query += $this.parent()[0].name + "=" + escape($this.val()) + "&";
                    }
                });
                var url = window.location + '';
                url = url.split(/[?#]/)[0];
                window.location = url + "?" + query;
            }
        });
    });
}

function pinEventSearch() {
    var duration = 1;

    function setDuration() {
        if ($('.wc-l').height() - $('.widget-search').height() > 0) {
            return (duration * ($('.wc-l').height() - $('.widget-search').height()));
        } else {
            return 1;
        }
        

    }

    $(window).bind("load", function() {
        setDuration();
    });


    $('.tab-list a').click(function(){
        setTimeout(function() { 
            setDuration();
            console.log($('.wc-l').height()); 

        }, 3000)
        
    });

    $('.widget-search').each(function(index, elem) {

        if ($(window).width() >= 1024) {

            var pinnedSearch = new ScrollMagic.Scene({
                triggerElement: elem,
                triggerHook: "onLeave",
                duration: setDuration,
                offset: -90
            })
            .setPin(elem)
            .addTo(controller);
            //.addIndicators();

        } 

        

    });






}




function verticalSliderInit() {

    $('.vertical-slide__details').matchHeight({ byRow: false });

    $verticalSlider = $('.js-vertical-slider').not('.slick-initialized'),
        slides = $verticalSlider.find('.vertical-slider__slide').length,
        $paging = $verticalSlider.find('.js-vertical-slider-pagination');



    if (slides < 5) {

        $verticalSlider.slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            arrows: false,
            vertical: true,
            variableHeight: false,
            draggable: true,
            slide: 'article',
            dots: true,
            dotsClass: 'vertical-slider__pagination js-vertical-slider-pagination',
            responsive: [{
                breakpoint: 768,
                settings: {
                    vertical: false
                }

            }]
        });

    } else {

        $verticalSlider.slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            arrows: true,
            vertical: true,
            draggable: true,
            slide: 'article',
            variableHeight: false,
            dots: false,
            responsive: [{
                breakpoint: 1023,
                settings: {
                    vertical: false
                }

            }]
        });
    }

    $(window).resize(function() {

        $('.vertical-slide__details').matchHeight({ destroy: true });
        $('.vertical-slide__details').matchHeight({ byRow: false });
        $verticalSlider.slick('refresh');

    });

}





function search() {

    var searchBar = $('.js-search-bar'),
        searchContainer = searchBar.find('.container'),
        searchInput = searchBar.find('input'),
        searchOpen = $('.js-search-trigger'),
        searchClose = $('.js-search-close'),
        searchResults = $('.js-search-results'),
        searchResult = $('.js-search-result');

    $(searchBar).css('visibility', 'visible');

    var searchInit = new TimelineMax()
        .fromTo(searchBar, 1, { scaleX: 0 }, { scaleX: 1, transformOrigin: "right center" })
        .fromTo(searchContainer, 1, { opacity: 0 }, { opacity: 1 })
        .duration(0.5)
        .paused(true);

    var searchAnim = new TimelineMax()
        .fromTo(searchResults, 1, { scaleY: 0 }, { scaleY: 1, transformOrigin: "top center" })
        .staggerFromTo(searchResult, 1, { opacity: 0, yPercent: "100%" }, { opacity: 1, yPercent: "0%" }, 0.5)
        .duration(0.5)
        .paused(true);

    searchOpen.click(function() {
        searchInit.play();
        searchInput.focus();
        var typingTimer; //timer identifier
        var doneTypingInterval = 1000; //time in ms
        searchInput.on('keyup', function() {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(doneTyping, doneTypingInterval);
        });

        //on keydown, clear the countdown 
        searchInput.on('keydown', function() {
            clearTimeout(typingTimer);
        });

        //user is "finished typing," do something
        function doneTyping() {
            //alert($(searchInput).val());
            //do something
            searchAnim.play();
            $('body').addClass('search-open');

            WebService.GetPredictiveSearchResults($(searchInput).val(), searchServiceSuccess, searchServiceFailed);

            function searchServiceSuccess(response) {
                var json = jQuery.parseJSON(response);
                if (json != null && json.results != null && json.results.length > 0) {
                    searchResults.html('');

                    $.each(json.results, function(index, result) {
                        var html = '<article class="header__search-result js-search-result">' +
                            '<a href="' + result.url + '"><h1>' + result.title + '</h1></a>' +
                            '<p>' + result.content + '</p>' +
                            '</article>';

                        searchResults.html(searchResults.html() + html);
                    });
                    searchResults.html(searchResults.html() + '<a href="/search?searchText=' + escape($(searchInput).val()) + '" class="cta cta--flex">View All Results</a>');
                } else {
                    searchResults.html("<p>Your search returned no results.</p>");
                }
            }

            function searchServiceFailed(error) {
                console.log("ERROR", "Search failed.");
                console.log("Stack Trace: " + error.get_stackTrace() + "/r/n" + "Error: " + error.get_message() + "/r/n" + "Status Code: " + error.get_statusCode());
            }
        }
    });

    searchClose.click(function() {
        searchAnim.reverse().eventCallback("onReverseComplete", searchInit.reverse());
        $('body').removeClass('search-open');
        searchInput.val("");
    });

}


var $ = jQuery.noConflict();

jQuery(document).ready(function($) {

    console.log('test');

    // POLYFILL FOREACH
    (function() {
        if (typeof NodeList.prototype.forEach === "function") return false;
        NodeList.prototype.forEach = Array.prototype.forEach;
    })();

    function initializeJS() {
        toggleHeader();
        slideMenu();
        //pinEventSearch();

        $('.search-list-thumb .div-one').matchHeight();

    }
    initializeJS();

});