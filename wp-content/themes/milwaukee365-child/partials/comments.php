<?php
if (post_password_required()) {
  return;
}
?>

<div id="comments" class="comments-area">
  <?php if ( have_comments() ) : ?>
    <h2 class="comments-title">
      <?php
      echo  sprintf( _n( '1 comment', '%s comments', get_comments_number(), 'apollo' ), number_format_i18n( get_comments_number() ) );
      ?>
      <a href="#respond" data-uk-smooth-scroll="{offset: 121}> <?php _e( 'Add yours', 'apollo' ) ?> <i class="fa fa-long-arrow-right"></i></a></h2>
    <ol class="commentlist">
      <?php
      wp_list_comments( array(
          'style'      => 'ul',
          'short_ping' => false,
          'avatar_size'=> 34,
          'walker' => new apollo_Walker_Comment(),
      ) );
      ?>
    </ol><!-- .comment-list -->
  <?php endif; // have_comments() ?>

  <?php if ( ! comments_open() ) : ?>
    <p class="no-comments"><?php _e( 'Comments are closed.', 'apollo' ); ?></p>
  <?php else: ?>
    <?php apollo_comment_form(array(
        'title_reply' => __('Leave a comment', 'apollo'),
        'id_form' => 'commentform',
    )); ?>
  <?php endif; ?>

</div>
