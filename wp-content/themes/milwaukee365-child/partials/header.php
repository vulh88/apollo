<?php

// Upgrade data
if ( isset( $_GET['apollo_upgrade'] ) && $_GET['apollo_upgrade'] = Apollo_DB_Schema::_APL_UPGRADE_KEY ) {
    require_once APOLLO_INCLUDES_DIR. '/scripts/class-apollo-upgrade-tool.php';
}

// Utility tools
if ( isset( $_GET['action'] ) && (isset( $_GET['site_ids']) || isset( $_GET['private_site']) )) {
    require_once APOLLO_INCLUDES_DIR. '/scripts/class-apollo-utility-tool.php';
}
/**
 * Vandd: @ticket #12150
 * Allow "scrolling with page" option can be run with both cases of authenticated and anonymous users
 */
$style_header = '';
$scrollWithPage = '';
$style_top_bar = of_get_option(Apollo_DB_Schema::_SCROLL_WITH_PAGE) ? 'position :relative;' :  'position :fixed;';
$scrollWithPage = of_get_option(Apollo_DB_Schema::_SCROLL_WITH_PAGE) ? '' :  'scroll-with-page';
if (!of_get_option(Apollo_DB_Schema::_SCROLL_WITH_PAGE) && of_get_option(Apollo_DB_Schema::_ENABLE_TOP_HEADER)  ) {
    $style_header = 'margin-top : 70px;';
}

$navStyle = of_get_option(Apollo_DB_Schema::_NAVIGATION_BAR_STYLE);
$borderBottom = ($navStyle == 'two-rows') || of_get_option(Apollo_DB_Schema::_ENABLE_TWO_ROW_NAVIGATION) ? 'two-row' : '';
?>

<?php if(of_get_option(Apollo_DB_Schema::_ENABLE_TOP_HEADER)) { ?>
    <div id="topbar" class="<?php echo $scrollWithPage; ?>" style="<?php echo $style_top_bar ?>">
        <div class="inner">
            <?php echo Apollo_App::get_static_html_content(Apollo_DB_Schema::_TOP_HEADER); ?>
        </div>
    </div>
<?php  }?>
    <header class="header js-header">
    <div class="header__logo">
        <a href="<?php echo site_url(); ?>"><img class="logo" src="<?php echo site_url(); ?>/<?php echo apply_filters( 'get_child_theme', '' );?>/assets/images/mke365-logo.png"></a>
    </div>
    <!-- DESKTOP MENU -->
    <div class="header__nav-container--desktop -hide-on-mobile">
        <div class="header__nav">
            <nav class="header__nav--utility">
                <ul>
                    <li>
                        <a href="<?php echo site_url(); ?>/login/">Login</a>
                    </li>
                    <li>
                        <a href="<?php echo site_url(); ?>/register/">Register</a>
                    </li>
                </ul>
            </nav>
            <nav class="header__nav--primary" role="navigation">
                <ul class="header__nav-dropdown-triggers">
                    <li class="header__nav-dropdown-trigger">
                        <a href="http://www.visitmilwaukee.org/plan-a-visit/" target="_blank">Plan a Visit</a>
                    </li>
                    <li class="header__nav-dropdown-trigger ">
                        <a href="http://www.visitmilwaukee.org/neighborhoods/ " target="_blank">Neighborhoods</a>                        
                    </li>
                    <li class="header__nav-dropdown-trigger ">
                        <a href="http://www.visitmilwaukee.org/about-mke/ " target="_blank">About MKE</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <!-- MOBILE MENU -->
    <div class="slide-menu -hide-on-desktop">
        <nav id="ml-menu" class="menu">
            <nav class="menu__breadcrumbs" aria-label="You are here"><a href="#">Menu</a></nav>
            <div class="menu__wrap">
                <ul data-menu="main" class="menu__level menu__level--current" tabindex="-1" role="menu" aria-label="Menu">
                    <li class="menu__item" role="menuitem">
                        <!-- PARENT LINK -->
                        <a class="menu__parent" href="http://visitmilwaukee.org/plan-a-visit/" target="_blank">Plan a Visit</a>
                    </li>
                    <li class="menu__item" role="menuitem">
                        <!-- PARENT LINK -->
                        <a class="menu__parent" href="http://visitmilwaukee.org/neighborhoods/" target="_blank">Neighborhoods</a>
                    </li>
                    <li class="menu__item" role="menuitem">
                        <!-- PARENT LINK -->
                        <a class="menu__parent" href="http://visitmilwaukee.org/events/" target="_blank">Events</a>
                    </li>
                    <li class="menu__item" role="menuitem">
                        <!-- PARENT LINK -->
                        <a class="menu__parent" href="http://visitmilwaukee.org/about-mke/" target="_blank">About MKE</a>
                    </li>
                </ul>
                
            </div>
        </nav>
        <nav class="slide-menu__nav--utility">
            <ul>
                <li>
                    <a href="<?php echo site_url(); ?>/login/">Login</a>
                </li>
                <li>
                    <a href="<?php echo site_url(); ?>/login/">Register</a>
                </li>
                
            </ul>
        </nav>
    </div>
    <div class="header__search">
    </div>
    <div class="header__nav-trigger-container">
        <div class="header__nav-trigger js-nav-trigger">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
</header>



<?php

global $current_blog;
/* Thienld : displaying top search event horizontal on desktop version */
/* Vulh update new position: inside alt spotlight */
$GLOBALS['aplTopEventWidget'] = Apollo_App::displaySearchEventHorizontalOnDesktop();
if (of_get_option(Apollo_DB_Schema::_HOR_EVENT_SEARCH_LOC, 'default') == 'default' || !Apollo_App::is_homepage()) {
    echo $GLOBALS['aplTopEventWidget'];
}
