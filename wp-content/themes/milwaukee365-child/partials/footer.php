<footer class="footer js-footer">
    <div class="footer__content">
        <div class="container">
            <div class="footer__left-column">
                <h1>See You Soon</h1>
                <p>VISIT Milwaukee is an award-winning convention &amp; visitors bureau providing information on premier events, hotel accommodations, transportation, restaurants and fun things to do in Milwaukee, Wisconsin! Experience the warm hospitality of Milwaukee's local businesses. We hope to see you soon!</p>
                <div class="ctas">
                    <a id="p_lt_WebPartZone4_Footer_Footer_linkCta" class="cta cta--bordered" href="https://milwaukee.extranet.simpleviewcrm.com/login/#/login" target="_blank">Partner Login</a>
                </div>
            </div>
            <div class="footer__right-column">
                <div class="">
                    <nav class="footer__nav">
                        <ul>
                            <li class=" ">
                                <a href="http://visitmilwaukee.org/about-us/">About Us</a>
                            </li>
                            <li class=" ">
                                <a href="http://visitmilwaukee.org/about-us/contact-us/job-openings/">Careers</a>
                            </li>
                            <li class=" ">
                                <a href="http://visitmilwaukee.org/about-us/contact-us/volunteer-opportunities/">Volunteer</a>
                            </li>
                            <li class=" ">
                                <a href="http://visitmilwaukee.org/articles/">Articles</a>
                            </li>
                            <li class=" ">
                                <a href="http://visitmilwaukee.org/about-us/contact-us/visit-milwaukee-staff/">Contact</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="">
                    <ul class="footer__nav-icons">
                        <li><a href="http://www.facebook.com/visitmilwaukee" title="Facebook" target="_blank"><i class="nc-icon nc-logo-facebook"></i></a></li>
                        <li><a href="http://twitter.com/#!/visitmilwaukee/" title="Twitter" target="_blank"><i class="nc-icon nc-logo-twitter"></i></a></li>
                        <li><a href="http://www.youtube.com/user/visitmilwaukee" title="YouTube" target="_blank"><i class="nc-icon nc-logo-youtube"></i></a></li>
                        <li><a href="http://instagram.com/visitmilwaukee" title="Instagram" target="_blank"><i class="nc-icon nc-logo-instagram"></i></a></li>
                        <li><a href="https://www.linkedin.com/company/visit-milwaukee" title="LinkedIn" target="_blank"><i class="nc-icon nc-logo-linkedin"></i></a></li>
                        <li><a href="https://www.pinterest.com/visitmilwaukee/" title="Pinterest" target="_blank"><i class="nc-icon nc-social-1_logo-pinterest"></i></a></li>
                    </ul>
                    <address class="footer__address">
                        648 N. PLANKINTON AVENUE
                        <br>SUITE 220
                        <br>MILWAUKEE, WI 53203-2926
                    </address>
                    <div class="footer__contact--phone">
                        <div>
                            <span>LOCAL NUMBER</span><a href="tel:414-273-3950">414-273-3950</a>
                        </div>
                        <div>
                            <span>TOLL FREE</span><a href="tel:800-554-1448">800-554-1448</a>
                        </div>
                    </div>
                    <div class="footer__copyright">
                        <div class="container">
                            <span>© 2018 VISIT Milwaukee. All rights reserved.</span>
                            <div>|</div>
                            <a href="http://visitmilwaukee.org/about-us/privacy-policy/" target="_blank">Privacy Policy</a>
                            <div>|</div>
                            <a href="http://visitmilwaukee.org/about-us/privacy-policy/terms-of-use/" target="_blank">Terms of Use</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__copyright">
        <span>© 2018 VISIT Milwaukee. All rights reserved.</span>
        <div>|</div>
        <a href="http://visitmilwaukee.org/about-us/privacy-policy/" target="_blank">Privacy Policy</a>
        <div>|</div>
        <a href="http://visitmilwaukee.org/about-us/privacy-policy/terms-of-use/" target="_blank">Terms of Use</a>
    </div>
    <div class="footer__image">
        <span>Wonderfully<br>Random.</span>
        <img src="<?php echo site_url(); ?>/<?php echo apply_filters( 'get_child_theme', '' );?>/assets/images/footer-image.png">
    </div>
</footer>




<?php wp_footer();?>


<script src="<?php echo site_url(); ?>/<?php echo apply_filters( 'get_child_theme', '' );?>/assets/scripts/vendor/jquery.selectric.js"></script>
<script src="<?php echo site_url(); ?>/<?php echo apply_filters( 'get_child_theme', '' );?>/assets/scripts/vendor/jquery.matchHeight.js"></script>
<script src="<?php echo site_url(); ?>/<?php echo apply_filters( 'get_child_theme', '' );?>/assets/scripts/vendor/slick.js"></script>
<script src="<?php echo site_url(); ?>/<?php echo apply_filters( 'get_child_theme', '' );?>/assets/scripts/vendor/TweenMax.js"></script>
<script src="<?php echo site_url(); ?>/<?php echo apply_filters( 'get_child_theme', '' );?>/assets/scripts/vendor/ScrollMagic.js"></script>
<script src="<?php echo site_url(); ?>/<?php echo apply_filters( 'get_child_theme', '' );?>/assets/scripts/vendor/animation.gsap.js"></script>
<script src="<?php echo site_url(); ?>/<?php echo apply_filters( 'get_child_theme', '' );?>/assets/scripts/vendor/debug.addIndicators.js"></script>
<script src="<?php echo site_url(); ?>/<?php echo apply_filters( 'get_child_theme', '' );?>/assets/scripts/vendor/ScrollToPlugin.js"></script>
<script src="<?php echo site_url(); ?>/<?php echo apply_filters( 'get_child_theme', '' );?>/assets/scripts/polyfills.js"></script>
<script src="<?php echo site_url(); ?>/<?php echo apply_filters( 'get_child_theme', '' );?>/assets/scripts/main.js"></script>