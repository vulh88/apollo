<div class="apl-internal-content page-not-found">
    <div class="content-wrapper">
        <div class="block block-events">
            <?php if (!empty($content)) {
                echo $content;
            } else { ?>
                <div class="alert alert-warning">
                    <h1><?php _e('Coming soon', 'sage'); ?></h1>
                </div>
            <?php } ?>
        </div>
    </div>
</div>