<?php

// Upgrade data
if ( isset( $_GET['apollo_upgrade'] ) && $_GET['apollo_upgrade'] = Apollo_DB_Schema::_APL_UPGRADE_KEY ) {
    require_once APOLLO_INCLUDES_DIR. '/scripts/class-apollo-upgrade-tool.php';
}

// Utility tools
if ( isset( $_GET['action'] ) && (isset( $_GET['site_ids']) || isset( $_GET['private_site']) )) {
    require_once APOLLO_INCLUDES_DIR. '/scripts/class-apollo-utility-tool.php';
}
/**
 * Vandd: @ticket #12150
 * Allow "scrolling with page" option can be run with both cases of authenticated and anonymous users
 */
$style_header = '';
$scrollWithPage = '';
$style_top_bar = of_get_option(Apollo_DB_Schema::_SCROLL_WITH_PAGE) ? 'position :relative;' :  'position :fixed;';
$scrollWithPage = of_get_option(Apollo_DB_Schema::_SCROLL_WITH_PAGE) ? '' :  'scroll-with-page';
if (!of_get_option(Apollo_DB_Schema::_SCROLL_WITH_PAGE) && of_get_option(Apollo_DB_Schema::_ENABLE_TOP_HEADER)  ) {
    $style_header = 'margin-top : 70px;';
}

$navStyle = of_get_option(Apollo_DB_Schema::_NAVIGATION_BAR_STYLE);
$borderBottom = ($navStyle == 'two-rows') || of_get_option(Apollo_DB_Schema::_ENABLE_TWO_ROW_NAVIGATION) ? 'two-row' : '';
?>

<?php if(of_get_option(Apollo_DB_Schema::_ENABLE_TOP_HEADER)) { ?>
    <div id="topbar" class="<?php echo $scrollWithPage; ?>" style="<?php echo $style_top_bar ?>">
        <div class="inner">
            <?php echo Apollo_App::get_static_html_content(Apollo_DB_Schema::_TOP_HEADER); ?>
        </div>
    </div>
<?php  }?>
    <header class="header <?php echo $borderBottom;?>" style="<?php echo $style_header;?>">
        <div class="inner">
            <div class="top-head" >
                <?php language_selector(); ?>
                <?php echo Apollo_App::get_static_html_content( Apollo_DB_Schema::_HEADER ); ?>
                <div class="mobile-menu">
                    <a href="javascript:;" data-tgr=".main-menu.tablet-show" class="mb-menu tablet-show"><?php _e( 'MENU', 'apollo' ) ?></a>
                    <div class="main-menu tablet-show <?php echo $scrollWithPage; ?>">
                        <div class="inner">
                            <?php $is_collapse_children_mobile = of_get_option( Apollo_DB_Schema::_COLLAPSEALLCHILDRENTMOBILE ) ?>
                            <nav class="mn-menu <?php echo $is_collapse_children_mobile ? 'has-toogle__menu-child' : '' ?>">
                                <?php
                                if ( has_nav_menu( 'primary_navigation' ) ) {
                                    if ($navStyle == 'two-tiers'){
                                        $menu_str = wp_nav_menu( array(
                                            'theme_location' => 'primary_navigation',
                                            'menu_id'     => 'main_nav',
                                            'echo'           => false,
                                        ) );
                                    } else {
                                        $menu_str = wp_nav_menu( array(
                                            'theme_location' => 'primary_navigation',
                                            'menu_class'     => 'nav',
                                            'echo'           => false,
                                        ) );
                                    }

                                } else {
                                    $menu_str = wp_page_menu(array(
                                        'echo' => false
                                    ));
                                }

                                echo $menu_str;
                                ?>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-menu pc-show <?php echo $navStyle ?> <?php echo $scrollWithPage; ?>">
            <div class="inner">
                <nav class="mn-menu">
                    <?php
                    echo $menu_str;
                    ?>
                </nav>
            </div>
            <?php if ($navStyle == 'two-tiers'){ ?>
                <div class="menu-background"></div>
            <?php } ?>
        </div>
    </header>

<?php

global $current_blog;
/* Thienld : displaying top search event horizontal on desktop version */
/* Vulh update new position: inside alt spotlight */
$GLOBALS['aplTopEventWidget'] = Apollo_App::displaySearchEventHorizontalOnDesktop();
if (of_get_option(Apollo_DB_Schema::_HOR_EVENT_SEARCH_LOC, 'default') == 'default' || !Apollo_App::is_homepage()) {
    echo $GLOBALS['aplTopEventWidget'];
}

/* Thienld : displaying top search widget modules on mobile devices */
Apollo_App::displaySearchVenueWidgetOnMobile();