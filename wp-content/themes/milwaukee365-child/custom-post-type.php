<?php

    $currentTerm = get_query_var('term');

    if (get_query_var('_apollo_org_search') === 'true') {
        new SMModuleInit(Apollo_DB_Schema::_ORGANIZATION_PT);
    }
    elseif (get_query_var('_apollo_artist_search') === 'true') {
        new SMModuleInit(Apollo_DB_Schema::_ARTIST_PT);
    }
    elseif (get_query_var('_apollo_venue_search') === 'true') {
        new SMModuleInit(Apollo_DB_Schema::_VENUE_PT);
    }
    elseif (get_query_var('_apollo_org_search') === 'true') {
        new SMModuleInit(Apollo_DB_Schema::_ORGANIZATION_PT);
    }
    elseif (get_query_var('_apollo_event_search') === 'true') {
        new SMModuleInit(Apollo_DB_Schema::_EVENT_PT);
    }
    elseif (get_query_var('_apollo_classified_search') === 'true') {
        new SMModuleInit(Apollo_DB_Schema::_CLASSIFIED);
    }
    elseif (get_query_var('_apollo_public_art_search') === 'true') {
        new SMModuleInit(Apollo_DB_Schema::_PUBLIC_ART_PT);
    }
    elseif (get_post_type() == Apollo_DB_Schema::_SPOT_PT) {
        get_template_part('partials/comming-soon');
    }
    elseif ( !empty($currentTerm) ) {
        global $sonoma_array_taxonomy;
        $currentTerm = get_term_by('slug', $currentTerm, get_query_var( 'taxonomy' ));
        $curPage = isset($_GET['page']) ? $_GET['page'] : 1;
        $_GET['paged'] = $curPage;
        $_GET['term'] = !empty($currentTerm) ? $currentTerm->term_id : '';
        $type = !empty($currentTerm) && isset($sonoma_array_taxonomy) && !empty($sonoma_array_taxonomy) ? $sonoma_array_taxonomy[$currentTerm->taxonomy] : get_post_type();
        new SMModuleInit($type);
    }
    else {
        if ( is_single()){
            new SMModuleInit(SMCommonConst::_SM_SINGLE_CONTENT_POST_TYPE);
        } else{
            get_template_part('404');
        }

    }


