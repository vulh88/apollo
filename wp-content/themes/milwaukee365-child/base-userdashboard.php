<!doctype html>
<!--[if IE 8]><html lang="en" class="no-js ie ie8"><![endif]-->
<!--[if IE 9]><html lang="en" class="no-js ie ie9"><![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>>
<!--<![endif]-->
<?php get_template_part('partials/head');
 $classBody = Apollo_App::isDashboardPage() ? 'user-dashboard-body' : 'artist-timeline' ?>
<body <?php body_class($classBody); ?>>
<?php 
    // Add override css
    if ( of_get_option( Apollo_DB_Schema::_ACTIVATE_OVERRIDE_CSS ) && file_exists(get_override_css_abs_path() ) ) {
        $primaryCss = get_override_css_url();
    } else {
        $primaryCss = '';
    }

    // Text Translation
    Apollo_App::wplmRenderCSSTextTrans();

?>    
<script>
    var cb = function() {
        var temUrl = '<?php echo get_template_directory_uri() ?>',
            jsArr = [
                temUrl+ '/assets/css/jquery-ui-calendar.css',
                'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,800italic,400,300,700,800',
            ];  

        var total = jsArr.length;    

        for( var i = 0; i < total; i++ ) {
            var l       = document.createElement('link'); 
                l.rel   = 'stylesheet';

            l.href = jsArr[i];
            var h = document.getElementsByTagName('head')[0]; h.parentNode.insertBefore(l, h);
        }
    };
    
    window.addEventListener('load', cb);
</script>


  <!--[if lt IE 8]>
    <div class="alert alert-warning">
      <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'apollo'); ?>
    </div>
  <![endif]-->

  <?php
    get_template_part('header');


    $is_group_dashboard = false;
    $is_one_column = false;
    if(Apollo_App::isDashboardPage()) {
        $is_group_dashboard = true;
    }
    else if(get_query_var('_rw_is_one_column') === 'true') {
        $is_one_column = true;
    }
    
    
    global $post;
    $pageTemplate = get_page_template_slug($post);
    

  ?>
  <section class="main <?php echo Apollo_App::isFullTemplatePage($pageTemplate) ? 'apl-full-width-page' : '' ?>">
      <div class="inner">


              <div class="row <?php echo $is_group_dashboard ? '' : 'two-col' ?>">
                    <?php if($is_group_dashboard) {?>
                      <?php include apollo_sidebar_dashboard_path(); ?>

                      <div class="dsb-r">
                          <div class="dsb-ct">
                          <?php
                              $handeCode = Apollo_App::getInfoADCurrentHandle();
							  
                              if(!empty($handeCode) && $handeCode['is_static'] === true) {
                                  include $handeCode['src'];
                              }
                              else {
                                  include apollo_template_path();
                              }
                          ?>
                          </div>
                      </div>
                  <?php } else {?>
                        <?php  include APOLLO_TEMPLATES_DIR.'/artists/html/timeline.php' ;?>
                  <?php } ?>


              </div>



          </div>
      </div>
  </section> <!-- end main content -->


  <?php get_template_part('partials/footer'); ?>
  
</body>
</html>