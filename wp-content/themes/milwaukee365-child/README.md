# APOLLO Milwaukee365 MILWAUKEE365 THEME STRUCTURES

## I Introduction
Milwaukee365 Theme is new skin of Apollo Theme and is built under kind of Wordpress Child Theme. The folder contains the whole implementations is 'milwaukee365-child'. By using child-theme, we can inherit all features and functions from Apollo Parent Theme and feel free to implement any customizations based on the requirements in Milwaukee365 without any conflicts or influences to its parent. 

## II. Folder structures
    assets : Front-end Resource 
    | css
    | fonts
    | images
    | js
    libraries : the specific libraries which need to use and define in Milwaukee365. 
    | SMCommonConst.php : define common constant
    | config.php : Define Directory Location Constants,  URL Location Constants
    | init.php : place for adding the hooks when initializing the theme
    | MultiPostThumbnails.php : an additional library which allows for uploading/choosing multiple thumbnail/featured image in each editing post screen. 
    | scripts.php : place for enqueueing css/js scripts which need to used for Milwaukee365 Theme.
    partials : contains the large template part which are based on the master Milwaukee365 template (milwaukee365-child/base.php)
    inc : managing modules and admin 
    | admin : contains hooks/customization when child theme Milwaukee365 is activated
    | | assets : Milwaukee365 WP Admin Resources 
    | | | styles 
    | | | js
    | | theme-options : contains additional/customized logic in ThemeOption. 
    | | | default-template : contains default templates which are used for auto-generating when Milwaukee365 Theme is active/re-active in any cases of these files are not existing. 
    | | SMAdminAssets.php : place for enqueueing css/js scripts which need to used for Milwaukee365 Theme Admin Area.
    | modules : contains the available modules which are implemented only in Milwaukee365 Theme
    | | common-templates : contains common templates which can be used any places in all of Milwaukee365's modules
    | | {module_name} : e.g (inc/modules/artist)
    | | | admin : custom logic and template of module in WP Admin area. 
    | | | inc : contains model of the module
    | | | | sm_{module_name}_model.php : the model file is used for interacting between querying data and determining the specific template for rendering.
    | | | templates : contains all template-parts on the module
    | | | SM{module_name}Module.php : e.g(inc/modules/venue/SMVenueModule.php) : work as controller to manage routes in the module.  
    | | SMModuleAdminInit.php : initializing all module in admin side.
    | | SMModuleInit.php : initializing all module in client side.
    | | SMListService.php : Front-end base class for the listing page 
    | | SMSingleService.php : Front-end base class for the single page (detail page)
    | | SMAdminModuleMediaSetting.php : initializing all module in admin side but relating to media settings. 
   
    
## II. Installations
    1. Place the Milwaukee365 Skin under folder 'milwaukee365-child' within '/wp-content/themes/'. 
    2. Go to Network Admin and ENABLE Milwaukee365 Theme as in this image http://prntscr.com/d1keqt . 
    3. Back to the site which need to apply the Milwaukee365 Skin and ACTIVE it in Appearance > Themes > Milwaukee365 Theme.
    4. Visit the site and enjoy the Milwaukee365 Theme. 

## III. Media Settings 
    A. Image Sizes Information
                Image Sizes                 Image Names use in code         |        Enabled Retina mode 
        1. THUMBNAIL - 150x150        |           thumbnail                 |              300x300             
        2. MEDIUM    - 350x450        |            medium                   |              700x900 
        3. LARGE     - 1024x700       |            large                    |             2048x1400 
        4. CIRCLE    - 250x250        |       circle_image_size             |              500x500 
        5. ORIGINAL - image real size |            full                     |       real-width*2xreal-height*2
    B. Using Image Sizes in client site
        1. THUMBNAIL 
            - The images are rendered in Directory Detail pages at Gallery Photos block in small view area http://prntscr.com/d1lepw .
            - The images are rendered in the associated list posts in Directory Detail page http://prntscr.com/d1lnc0 . 
        2. MEDIUM    
            - The images are rendered in Blog Listing page http://prntscr.com/d1lj4z .  
            - The image is rendered in Directory Detail pages http://prntscr.com/d1ljpg . 
        3. LARGE     
            - The images are rendered in Directory Detail pages at Gallery Photos block in large view area http://prntscr.com/d1ldz2 .   
            - The image is rendered in Blog Author page http://prntscr.com/d1lgh2 .    
        4. CIRCLE    
            - The circle images in each Spotlight in Homepage - Main Slider http://prntscr.com/d1ksr9
            - The circle images in Directory listing pages (view type: 'Thumbs' - http://prntscr.com/d1l9g6 & 'List' - http://prntscr.com/d1lau8). Currently, it's applied for 5 available modules in Milwaukee365 Theme (organization,artist,venue,public-art,classified)
            - The circle image in the Gallery Images which is rendered in page custom template http://prntscr.com/d1lbbh .   
        5. ORIGINAL  
            - The background image for Spotlight in Homepage - Main Slider http://prntscr.com/d1krjq . 
            - The background image for Top & Middle positions http://prntscr.com/d1lkzf in page - custom template. 
            - The background iamge for Feature image in Blog post detail page http://prntscr.com/d1llzt . 

## V. CODING STYLE GUILDS
    1. To custom base template for MAIN SITE in FE, do it in base.php
    2. To custom base template for User Dashboard in FE, do it in base-userdashboard.php
    3. Building post types in FE as modules separately. Currently, there is an example module is venue (inc/modules/venue)
    => We can follow this skeleton for a module as in Folder Structure to build the new ones, such as Classified, Organization, etc
    4. Keep all the additional hooks/implementation/customization via Admin into inc/admin
    5. Keep all the additional hooks/implementation/customization via FrontPage into inc/modules
    6. Keep all of the additional common constants which are used only in the current child theme into libraries/CMCommonConst.php
    7. Keep all of defining by using php define('') function into libraries/config.php
    8. Keep all of either global hooks or static function in functions.php or libraries/SMExtras.php
    9. Keep all enqueue or dequeue scripts in FrontPage into libraries/scripts.php
    10. Keep all hooks of initializing the current child theme into libraries/init.php
    11. Shouldn't add more any codes to page.php & single.php. The whole customization of them are implemented into post and page modules. See (V. CODING STYLE GUILDS > 4.) 
    12. Follow the way in custom-post-type.php to add a new module of a custom post type. 
    13. To make a page which is bound with a selected custom template value from WPAdmin > Page
    
    - Don't add more any codes into page_custom.php 
    - Follow the way in inc/modules/page/page.php for implementation the new one.

## VI. NOTES
    - Our theme structure is building adherent to https://roots.io/sage/

## LOGS

##### - Release milwaukee365 theme version 1.0.0  2017-11-28