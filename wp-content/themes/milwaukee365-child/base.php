<?php  if(Apollo_App::isDashboardPage() || get_query_var('_apollo_artist_timeline') === 'true' ) : ?>
    <?php get_template_part('base-userdashboard'); ?>
<?php else : ?>
    <!doctype html>
    <!--[if IE 8]><html lang="en" class="no-js ie ie8"><![endif]-->
    <!--[if IE 9]><html lang="en" class="no-js ie ie9"><![endif]-->
    <!--[if gt IE 9]><!-->
    <html class="no-js" <?php language_attributes(); ?>>
    <!--<![endif]-->
    <?php get_template_part('partials/head'); ?>
    <body <?php body_class(); ?>>
    <?php
        echo Apollo_App::renderScriptInBodyTag();
        if ( SMExtras::isTheFirstAccessToPage()) {
            get_template_part('partials/content-animation-homepage');
        }

        $is_one_column = false;
        if(get_query_var('_rw_is_one_column') === 'true') {
            $is_one_column = true;
        }

        global $post;
        $pageTemplate = get_page_template_slug($post);

    ?>
    <div class="layout layout-fluid">
        <header>
            <?php get_template_part('partials/header') ?>
        </header>
        <?php echo do_shortcode('[apollo_home_spotlight]'); ?>
        <?php SMViewHelper::smGetTemplatePartCustom(SONOMA_MODULES_DIR . '/event/templates/taxonomy/slider.php', array()); ?>
        <section class="main">
            <div class="inner">

            <section class="local-navigation-container">


                    <?php
                        if ( has_nav_menu( 'primary_navigation' ) ) {
                            $navStyle = of_get_option(Apollo_DB_Schema::_NAVIGATION_BAR_STYLE);
                            if ($navStyle == 'two-tiers'){
                                $menu_str = wp_nav_menu( array(
                                    'theme_location' => 'primary_navigation',
                                    'menu_id'     => '',
                                    'echo'           => false,
                                ) );
                            } else {
                                $menu_str = wp_nav_menu( array(
                                    'theme_location' => 'primary_navigation',
                                    'menu_class'     => 'local-navigation',
                                    'echo'           => false,
                                ) );
                            }

                        } else {
                            $menu_str = wp_page_menu(array(
                                'echo' => false
                            ));
                        }

                        echo $menu_str;
                        ?>

               
                </section>
                <?php
                    Apollo_App::displaySearchVenueWidgetOnMobile();
                    Apollo_App::displaySearchArtistWidgetOnMobile();
                    Apollo_App::displaySearchBusinessWidgetOnMobile();
                    Apollo_App::displaySearchClassifiedWidgetOnMobile();
                    Apollo_App::displaySearchEducationWidgetOnMobile();
                    Apollo_App::displaySearchEventWidgetOnMobile();
                    Apollo_App::displaySearchOrganizationWidgetOnMobile();
                    Apollo_App::displaySearchPublicArtWidgetOnMobile();
                ?>

                

                <div class="row two-col ">
                    <?php if(!is_404()) :
                        if ($is_one_column) :
                            SMViewHelper::smGetTemplatePart();
                        else :
                            $homeLayOut = of_get_option(Apollo_DB_Schema::_DEFAULT_HOME_LAYOUT);
                            // Only apply fullwidth 960 for home page
                            if ( Apollo_App::is_homepage() && $homeLayOut == 'full_960'   ) :
                                ?>
                                <!-- start left side -->
                                <div class="wc-f">
                                    <?php SMViewHelper::smGetTemplatePart(); ?>
                                </div> <!-- left side -->
                                <?php
                            else :
                                if ( !Apollo_App::isFullTemplatePage($pageTemplate) ) :
                                    ?>
                                    <div class="v-line"> </div>
                                <?php endif; ?>

                                <!-- start left side -->
                                <div class="wc-l">
                                    <?php SMViewHelper::smGetTemplatePart(); ?>
                                </div> <!-- left side -->

                                <!-- start right side -->
                                <?php
                                if ( !Apollo_App::isFullTemplatePage($pageTemplate) ) :
                                    ?>
                                    <div class="wc-r">
                                        <?php
                                            include apollo_sidebar_path();
                                        ?>
                                    </div> <!-- right side -->
                                    <?php
                                endif;
                            endif;
                        endif;
                    else :
                        get_template_part('404');
                    endif; ?>
                    <?php

                    if (!isset($_SESSION)) {
                        session_start();
                    }
                    if( !isset($_SESSION['home_page'])) {
                        $_SESSION['home_page'] = true;
                    }
                    ?>
                </div>
            </div>
        </section>
    </div>
    <footer>
        <?php get_template_part('partials/footer') ?>
    </footer>

    </body>
    </html>
<?php endif; ?>
