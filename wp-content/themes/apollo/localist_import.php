<?php
/*
Template Name: LOCALIST IMPORT
*/


global $wpdb;


if (empty($_POST)){

?>


<form method=post>
 <strong>Enter Date: </strong> <input style="border:solid 1px grey !important;" type=text name=date placeholder=YYYY-MM-DD><br>
 <strong>Number of Events: </strong> <input style="border:solid 1px grey !important;" type=text name=num placeholder="1 thru 100"><br><br>
 <input style="background:silver;font-size:16px;" type=submit value=" submit "><br>
</form>

<?php

exit;
}

//print_r($_POST);


$good_date = validateDate($_POST['date']);
if ($good_date) {} else { echo "Bad date"; exit;}


if (current_user_can( 'administrator')) {


$file = '/srv/multisite/wp1/wp-content/themes/apollo/events_imported.txt';
$imported_ids = unserialize(file_get_contents($file)); // or die("Cannot open $file");

$seen_ids = array();

if (!empty($imported_ids)){
 $seen_ids = array_merge($seen_ids,$imported_ids);
}


//echo "<br>SEEN IDS BEG="; print_r($seen_ids);


//////////////////////////////////////
///
// Read JSON file and get into an array
///
//////////////////////////////////////


$json = json_decode(file_get_contents('https://events.nmu.edu/api/2/events?end='.$_POST['date'].'&pp='.$_POST['num'].'&type[]=106362&type[]=106366&type[]=106367&type[]=106374&type[]=106448&type[]=106468&type[]=107744&type[]=109366&type[]=106449&type[]=106457&type[]=106467'));

echo "<br>Begin LOCALIST Import<br><br>";

$count=1;

foreach ($json as $key => $rec){

		foreach ($rec as $row){

				foreach ($row as $obj){

						if (is_object($obj)) {

							//print_r($obj); die;

						if (in_array($obj->id,$seen_ids)) continue;


//####################################### begin wordpress processing #########################################

						

	$venue = $obj->location_name;
	$address = $obj->geo->street;
	$address2 = '';
	$city = $obj->geo->city;
	$state = $obj->geo->state;
	$zip = $obj->geo->zip;


	$contact = $obj->custom_fields->primary_contact;
	
	if (preg_match('/@/', $obj->custom_fields->primary_contact_phone_or_email)){
 		$email = $obj->custom_fields->primary_contact_phone_or_email;
	} else {	
		$phone = $obj->custom_fields->primary_contact_phone_or_email;
	}

	$image = $obj->custom_fields->photo_url;


$status = 'pending';


// Create post object
$my_post = array(
  'post_title'    => $obj->title,
  'post_content'  => $obj->description_text,
  'post_status'   => $status,
  'post_author'   => 1,
  'post_type' => "event"
);

// Insert the post into the database

$post_id = wp_insert_post( $my_post );


//################################################################

// Add Featured Image to Post
$image_url        = $obj->photo_url; // Define the image URL here
$image_name       = 'wp-header-logo.png';
$upload_dir       = wp_upload_dir(); // Set upload folder
$image_data       = file_get_contents($image_url); // Get image data
$unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
$filename         = basename( $unique_file_name ); // Create image file name

// Check folder permission and define file location
if( wp_mkdir_p( $upload_dir['path'] ) ) {
    $file = $upload_dir['path'] . '/' . $filename;
} else {
    $file = $upload_dir['basedir'] . '/' . $filename;
}

// Create the image  file on the server
file_put_contents( $file, $image_data );

// Check image file type
$wp_filetype = wp_check_filetype( $filename, null );

// Set attachment data
$attachment = array(
    'post_mime_type' => $wp_filetype['type'],
    'post_title'     => sanitize_file_name( $filename ),
    'post_content'   => '',
    'post_status'    => 'inherit'
);

// Create the attachment
$attach_id = wp_insert_attachment( $attachment, $file, $post_id );

// Include image.php
require_once(ABSPATH . 'wp-admin/includes/image.php');

// Define attachment metadata
$attach_data = wp_generate_attachment_metadata( $attach_id, $file );

// Assign metadata to attachment
wp_update_attachment_metadata( $attach_id, $attach_data );

// And finally assign featured image to post
set_post_thumbnail( $post_id, $attach_id );

//################################################################



$event_data = array (
  '_website_url' => $obj->localist_url,
  '_admission_detail' => '',
  '_admission_phone' => '',
  '_admission_ticket_url' => $obj->ticket_url,
  '_admission_ticket_email' => '',
  '_admission_discount_url' => '',
  '_contact_name' => $contact,
  '_contact_email' => $email,
  '_contact_phone' => $phone
);

// Add eventmeta

$event_data = serialize($event_data);

$wpdb->replace(
$wpdb->prefix.'apollo_eventmeta',
array(
'apollo_event_id' => $post_id,
'meta_key' => '_apollo_event_data',
'meta_value' => serialize($event_data)
)
);


// Add date start


$wpdb->replace(
$wpdb->prefix.'apollo_eventmeta',
array(
'apollo_event_id' => $post_id,
'meta_key' => '_apollo_event_start_date',
'meta_value' => $obj->first_date
)
);

// Add end date


$wpdb->replace(
$wpdb->prefix.'apollo_eventmeta',
array(
'apollo_event_id' => $post_id,
'meta_key' => '_apollo_event_end_date',
'meta_value' => $obj->last_date
)
);

$date_start = $obj->first_date;
$date_end = $obj->last_date;

// Add day of the week if possible

if ($date_start == $date_end){

$dayofweek =  date('w',strtotime($date_start)); 
			$days_array = array( $dayofweek );

$dayser = serialize(serialize($days_array));

$querystr = "insert into ".$wpdb->prefix."apollo_eventmeta (apollo_event_id, meta_key, meta_value) values
		($post_id, '_apollo_event_days_of_week', '$dayser')";
		$addit = $wpdb->get_results($querystr, OBJECT);
}


// Add temp venue info

$venue_info = array('_venue_name'=>$venue,'_venue_address1'=>$address,'_venue_address2'=>$address2,'_venue_city'=>$city,'_venue_state'=>$state,'_venue_zip'=>$zip,'_venue_website_url'=> '');
$venue_data = serialize(serialize($venue_info));

$querystr = "insert into ".$wpdb->prefix."apollo_eventmeta (apollo_event_id, meta_key, meta_value) values ($post_id, '_apl_event_tmp_venue', '$venue_data')";
$insert =  $wpdb->get_results($querystr, OBJECT);


//######################################## end wordpress processing###########################################

						array_push($seen_ids, $obj->id);
					}
			

				}

		}


}


//echo "<br>SEEN IDS END="; print_r($seen_ids);

$file = '/srv/multisite/wp1/wp-content/themes/apollo/events_imported.txt';
file_put_contents($file, serialize($seen_ids)) or die("cannot put contents $file");

echo "<br>Import Complete";


} else {echo "login to use this feature";}  // end if user can


function validateDate($date)
{
    if (substr($date,0,4) >= 2017) {
    	$d = DateTime::createFromFormat('Y-m-d', $date);
    	return $d && $d->format('Y-m-d') === $date;
    }
	return false;
}

/*

stdClass Object
(
    [id] =&gt; 1040051
    [title] =&gt; Alexis Rockman Learning Lab and Display
    [url] =&gt; events.nmu.edu/alexis-rockman
    [updated_at] =&gt; 2017-10-19T07:11:31-04:00
    [created_at] =&gt; 2017-09-12T15:22:59-04:00
    [facebook_id] =&gt; 
    [first_date] =&gt; 2017-09-25
    [last_date] =&gt; 2017-10-30
    [hashtag] =&gt; 
    [urlname] =&gt; alexis_rockman_learning_lab_and_display
    [user_id] =&gt; 409930
    [directions] =&gt; 
    [allows_reviews] =&gt; 1
    [allows_attendance] =&gt; 1
    [location] =&gt; 
    [room_number] =&gt; 
    [location_name] =&gt; DeVos Art Museum
    [created_by] =&gt; 409930
    [updated_by] =&gt; 446803
    [city_id] =&gt; 
    [neighborhood_id] =&gt; 
    [school_id] =&gt; 209
    [campus_id] =&gt; 
    [recurring] =&gt; 1
    [free] =&gt; 1
    [private] =&gt; 
    [verified] =&gt; 1
    [rejected] =&gt; 
    [sponsored] =&gt; 1
    [venue_id] =&gt; 408369
    [ticket_url] =&gt; 
    [ticket_cost] =&gt; 
    [keywords] =&gt; Array
        (
        )

    [tags] =&gt; Array
        (
            [0] =&gt; alexis-rockman
        )

    [description_text] =&gt; A display of three large scale prints of Rockman's Great Lakes Cycle series of paintings, 25 field drawings and digital projections. Held in conjunction with the Rabbit Island Artist in Residence Exhibition, featuring visual works of five artists who spend weeks on the remote island in Lake Superior.
    [photo_id] =&gt; 545113
    [detail_views] =&gt; 170
    [event_instances] =&gt; Array
        (
            [0] =&gt; stdClass Object
                (
                    [event_instance] =&gt; stdClass Object
                        (
                            [id] =&gt; 3018832
                            [ranking] =&gt; 0
                            [event_id] =&gt; 1040051
                            [start] =&gt; 2017-10-19T00:00:00-04:00
                            [end] =&gt; 
                            [all_day] =&gt; 1
                        )

                )

        )

    [address] =&gt; 1401 Presque Isle Ave, Marquette, MI 49855
    [description] =&gt;
    [featured] =&gt; 
    [geo] =&gt; stdClass Object
        (
            [latitude] =&gt; 46.556459
            [longitude] =&gt; -87.401081
            [street] =&gt; 1401 Presque Isle Avenue
            [city] =&gt; Marquette
            [state] =&gt; MI
            [country] =&gt; US
            [zip] =&gt; 49855
        )

    [filters] =&gt; stdClass Object
        (
            [event_types] =&gt; Array
                (
                    [0] =&gt; stdClass Object
                        (
                            [name] =&gt; Academic
                            [id] =&gt; 106366
                        )

                    [1] =&gt; stdClass Object
                        (
                            [name] =&gt; Community
                            [id] =&gt; 106367
                        )

                )

        )

    [custom_fields] =&gt; stdClass Object
        (
            [primary_contact] =&gt; J. Leonard
            [primary_contact_phone_or_email] =&gt; jileonar@nmu.edu
        )

    [localist_url] =&gt; https://events.nmu.edu/event/alexis_rockman_learning_lab_and_display
    [localist_ics_url] =&gt; https://events.nmu.edu/event/alexis_rockman_learning_lab_and_display.ics
    [photo_url] =&gt; https://d3e1o4bcbhmj8g.cloudfront.net/photos/545113/huge/72e1221237101f9f3976a6f939de1811f865dd7d.jpg
    [venue_url] =&gt; https://events.nmu.edu/devos_art_museum
)


*/


?>