const { mix } = require('laravel-mix');

var jsPath      = 'assets/js/',
    cssPath     = 'assets/css/';

mix.setPublicPath('/');


mix.styles([
    cssPath+'font-awesome.min.css',
    cssPath+'select2.min.css'
], cssPath + 'apl-font-style.css');

//add css
mix.styles([
    cssPath+'style.css',
    cssPath +'global-style.css',
    cssPath+'dev.css',
    cssPath+'business-spotlight.css',
    cssPath+'tell-a-friend.css'
], cssPath + 'apl-dev-style.css');

mix.styles([
    cssPath+'reset.css',
    cssPath+'jquery-ui-calendar.css',
    cssPath+'photo-slider.css'
], cssPath + 'apl-reset-style.css');

mix.styles([
    cssPath+ 'plugins/select2-import.css',
    cssPath+'github.css',
    cssPath+'handsontable.css',
    cssPath+'pikaday.css',
    cssPath+'jquery-validation-engine/css/validationEngine.jquery.css'
], cssPath + 'apl-dashboard-style.css');

mix.scripts( [
        jsPath+ 'apl-ajaxsetup.js',
        jsPath+ 'plugins/moment.js',
        jsPath+ 'jquery-lazy-load.js',
        jsPath+ 'lazyYT.js',
        jsPath+ 'plugins/select2.js',
        jsPath+ 'plugins.js',
        jsPath+ 'ex-plugins.js',
        jsPath+ 'start.js',
        jsPath+ 'global.js',
        jsPath+ 'function.js',
        jsPath+ 'widget-js.js',
        jsPath+ 'event.js',
        jsPath+ 'apollo-map.js',
        jsPath+ 'common-function.js',
        jsPath+ 'plugins/timelinejs/js/storyjs-embed.js',
        jsPath+ 'tell-a-friend.js',
        jsPath+ 'apl-global-search.js',
        jsPath+ 'apl_select2.js',
        jsPath+ 'apl-artist-form.js',
        jsPath+ 'apl-custom-ninja-form-plugin.js',
        jsPath+ 'apl-register.js'
    ],
    jsPath + '/apollo-global-js-combine.js');

mix.scripts([
        //====================== Form validation here =================================//
        jsPath+ 'jquery-validation-engine/js/jquery.validationEngine.js',
        jsPath+ 'jquery-validation-engine/js/languages/jquery.validationEngine-en.js',
        jsPath+ 'form-abstract-validation.js',
        jsPath+ 'form-event-validation.js',
        jsPath+ 'form-classified-validation.js',
        jsPath+ 'form-venue-validation.js',
        jsPath+ 'form-educator-validation.js',
        jsPath+ 'form-program-validation.js',
        jsPath+ 'form-artist-validation.js',
        jsPath+ 'form-organization-validation.js',
        jsPath+ 'form-myaccount-validation.js',
        jsPath+ 'apl-dashboard-photos.js',
        jsPath+ 'collapse-expand.js',
        jsPath+ 'apl-dashboard-program.js'
    ],
    jsPath + '/apollo-js-dashboard-combine.js');

mix.scripts( [
        jsPath+ 'import-plugin/select2.js',
        jsPath+ 'import-plugin/pikaday.js',
        jsPath+ 'import-plugin/moment.js',
        jsPath+ 'import-plugin/ZeroClipboard.js',
        jsPath+ 'import-plugin/handsontable.js',
        jsPath+ 'import-plugin/handsontable-chosen-editor.js',
        jsPath+ 'import-plugin/highlight.pack.js',
        jsPath+ 'import-plugin/start-event.js'
    ],
    jsPath + '/apollo-js-dashboard-import-combine.js');

if (mix.config.production) {
    mix.version();
}