<?php

// this is list of taxonomies and theirs classes
$arrTaxWithClass = array(
    'classified-type' => array('template' => Apollo_Taxonomy_Template::_APOLLO_TEMPLATE_CUSTOM_TAXONOMY_CLASSIFIED),
    'artist-type' => array('template' => Apollo_Taxonomy_Template::_APOLLO_TEMPLATE_CUSTOM_TAXONOMY_ARTIST),
    'educator-type' => array('template' => Apollo_Taxonomy_Template::_APOLLO_TEMPLATE_CUSTOM_TAXONOMY_EDUCATORS),
    'organization-type' => array('template' => Apollo_Taxonomy_Template::_APOLLO_TEMPLATE_CUSTOM_TAXONOMY_ORGANIZATION),
    'program-type' => array('template' => Apollo_Taxonomy_Template::_APOLLO_TEMPLATE_CUSTOM_TAXONOMY_PROGRAMS),
    'public-art-type' => array('template' => Apollo_Taxonomy_Template::_APOLLO_TEMPLATE_CUSTOM_TAXONOMY_PUBLIC_ART),
    'venue-type' => array('template' => Apollo_Taxonomy_Template::_APOLLO_TEMPLATE_CUSTOM_TAXONOMY_VENUE),
    'business-type' => array('template' => Apollo_Taxonomy_Template::_APOLLO_TEMPLATE_CUSTOM_TAXONOMY_BUSINESS),
    'news-type' => array('template' => Apollo_Taxonomy_Template::_APOLLO_TEMPLATE_CUSTOM_TAXONOMY_NEWS),
    'artistic-discipline' => array('template' => Apollo_Taxonomy_Template::_APOLLO_TEMPLATE_CUSTOM_TAXONOMY_PROGRAMS),
);
global $apl_current_term_object;
$currentTerm = $apl_current_term_object ? $apl_current_term_object : Apollo_App::getCurrentTerm();

$GLOBALS['apl_current_event_term'] = $currentTerm ? $currentTerm->term_id : ''; // For the custom widget

/** @Ticket #13061 */
$GLOBALS['apl_current_taxonomy_slug'] = $currentTerm ? $currentTerm->slug : ''; // For the custom widget
$GLOBALS['apl_current_taxonomy_type'] = $currentTerm ? $currentTerm->taxonomy : ''; // For the custom widget

$termDescription = !empty($currentTerm) ? $currentTerm->description : "";
$currentTaxType = !empty($currentTerm) ? $currentTerm->taxonomy : "";
if(isset($arrTaxWithClass[$currentTaxType]) && !empty($arrTaxWithClass[$currentTaxType]['template'])){
    $currentCustomTaxonomy = $arrTaxWithClass[$currentTaxType];
    $templateName = $arrTaxWithClass[$currentTaxType]['template'];
    $termID = $currentTerm->term_id;
    // re-create $_GET and require correct search instance.
    $_GET['term'] = $termID;
    $curPage = isset($_GET['page']) ? $_GET['page'] : 1;
    $_GET['paged'] = $curPage;
    require_once __DIR__.'/templates/search/templates/'.$templateName.'.php';
} else {
    // PAGE: CATEGORY
    // MAIN LIB
    require __DIR__.'/templates/taxonomy/inc/single.php';
    // BREADCRUMBS
    Apollo_App::the_breadcrumb();

    /* Thienld: new instance to handle caching logic for event category page */
    $eSpotLightFromCache = array();
    $eFeaturedFromCache = array();
    $eventCacheData = apply_filters('apl_event_cache_get_cache_data',$currentTerm->term_id);
    $isEnabledLocalCache = apply_filters('apl_event_cache_check_local_cache_enable','');
    if($isEnabledLocalCache && !empty($eventCacheData)){
        $eSpotLightFromCache = $eventCacheData[APLC_Inc_Event_Category_Cache::EVENT_SPOTLIGHT_CACHE];
        $eFeaturedFromCache = $eventCacheData[APLC_Inc_Event_Category_Cache::EVENT_FEATURED_CACHE];
    }

   // PREPARE DATA
    $term = $currentTerm;
    $hander_page = new Apollo_Single_Category($term);

    $hidden_spotlight = get_apollo_term_meta( $currentTerm->term_id, Apollo_DB_Schema::_APL_EVENT_TAX_HIDE_SPOT, true );

    /*top theme tool desc*/
    $themeToolData = $hander_page->themeToolClass->getThemeToolData();
    if ($themeToolData && $themeToolData->top_desc):?>
        <div class="tax-top-content">
            <p>
                <?php echo Apollo_App::convertContentEditorToHtml($themeToolData->top_desc, true);?>
            </p>
        </div>
   <?php endif;

    // ================== RENDER SPOTLIGHT ==================
    if( of_get_option(Apollo_DB_Schema::_ENABLE_CATEGORY_PAGE_SPOTLIGHT) && !$hidden_spotlight) {
        $availableSpotlightTransientCache = !empty($eSpotLightFromCache);

        if($availableSpotlightTransientCache) {
            $spotlightEventID = (int) $eSpotLightFromCache['event_ids'][0];

            $hander_page->set_displayed_event( $spotlightEventID );
        } else {
            $spotlights = $hander_page->getSpotLightsEvent();
            $spotlightEvent = $spotlights['datas'];
            $spotlightEventID = isset($spotlights['datas'][0]->ID) ? $spotlights['datas'][0]->ID : '';
            if ( count( $spotlightEvent ) ) {
                $hander_page->set_displayed_event( ( int ) $spotlightEventID );
            }

            if($isEnabledLocalCache){
                do_action('apl_event_cache_set_data_cache',APLC_Inc_Event_Category_Cache::EVENT_SPOTLIGHT_CACHE, $spotlightEvent);
            }
        }


//        $spotlightCacheFileClass = aplc_instance('APLC_Inc_Files_EventCategorySpotlightBlock', $currentTerm->term_id);
//        $spotlightHtml = $availableSpotlightTransientCache ? $spotlightCacheFileClass->get() : $spotlightCacheFileClass->remove();

        //if (!$spotlightHtml) {
            $spotlightHtml = $hander_page->renderWithData(sprintf('%s/taxonomy/html/%s', APOLLO_TEMPLATES_DIR, of_get_option(Apollo_DB_Schema::_EVENT_CATEGORY_SPOTLIGHT_STYLE, 'default') == 'default'
                ? 'spotlight-event-left.php' : 'spotlight-event.php'), $spotlightEventID);

            //$spotlightCacheFileClass->save($spotlightHtml);
        //}
        echo $spotlightHtml;
    }
    // ================== END RENDER SPOTLIGHT ==================




    //================== RENDER FEATUREEVENTS ==================
    $availableFeatureTransientCache = !empty($eFeaturedFromCache);
    if($availableFeatureTransientCache){
        $featured_events = $eFeaturedFromCache['event_ids'];
        foreach($featured_events as $eID){
            $hander_page->set_displayed_event( ( int ) $eID );
        }
    } else {
        $features =  $hander_page->getFeaturesEvent();
        $featured_events = $hander_page->auto_fill_featured_events();
        if($isEnabledLocalCache){
            do_action('apl_event_cache_set_data_cache',APLC_Inc_Event_Category_Cache::EVENT_FEATURED_CACHE,$featured_events);
        }
    }

    /**
     * Apply file caching
     * @author vulh
     * @Ticket #14877
     */
    $featuredCacheFileClass = aplc_instance('APLC_Inc_Files_EventCategoryFeaturedBlock', $currentTerm->term_id);
    $featuredHtml = $availableFeatureTransientCache ? $featuredCacheFileClass->get() : $featuredCacheFileClass->remove();

    if (!$featuredHtml) {
        $featuredHtml = $hander_page->renderWithData( APOLLO_TEMPLATES_DIR.'/taxonomy/html/features-event.php', $featured_events );
        $featuredCacheFileClass->save($featuredHtml);
    }
    echo $featuredHtml;

    // ================== END RENDER FEATUREEVENTS ==================



        ?>
        <section class="list-more-category list-more-category-others-event">
            <div></div>
        </section>
            <?php
            $step = Apollo_Display_Config::SINGLE_TAXONOMY_OTHER_EVENT_SHOW_STEP;
            $baseStart = 0;
            ?>
            <div class="load-more b-btn apl-others-event-load-more hidden">
                <a href="javascript:void(0);"
                   data-ride="ap-more"
                   data-holder=".list-more-category-others-event>:last"
                   data-sourcetype="url"
                   data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_category_show_more_other_event&start='.$baseStart.'&pagesize='.$step.'&term_id='.$term->term_id.'&displayed_ids=' . implode(',', $hander_page->get_displayed_events())) ?>"
                   data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
                   data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
                   data-container=".list-more-category-others-event"
                   class="btn-b arw"><?php _e('SHOW MORE', 'apollo') ?>
                </a>
            </div>
    <?php

    /*Render bottom theme tool desc*/
    //$themeToolData = $hander_page->themeToolClass->isThemeTool();
    $themeToolData = $hander_page->themeToolClass->getThemeToolData();
    if ( $themeToolData ) {
        // set current term to query later
        $themeToolData->term_id = $term->term_id;

        // @ticket #11493: display associated organizations and associated venues
        echo $hander_page->renderWithData(APOLLO_TEMPLATES_DIR.'/taxonomy/html/partial/associated-orgs.php', $themeToolData);
        echo $hander_page->renderWithData(APOLLO_TEMPLATES_DIR.'/taxonomy/html/partial/associated-venues.php', $themeToolData);

        if ( $themeToolData->bottom_desc ) {
            ?>
            <div class="tax-bottom-content">
                <p><?php echo Apollo_App::convertContentEditorToHtml($themeToolData->bottom_desc, TRUE) ?></p>
            </div>
            <?php
        }
    }
    ?>

<?php
}
