<?php 

// Upgrade data
if ( isset( $_GET['apollo_upgrade'] ) && $_GET['apollo_upgrade'] = Apollo_DB_Schema::_APL_UPGRADE_KEY ) {
    require_once APOLLO_INCLUDES_DIR. '/scripts/class-apollo-upgrade-tool.php';
}

// Utility tools
if ( isset( $_GET['action'] ) && (isset( $_GET['site_ids']) || isset( $_GET['private_site']) )) {
    require_once APOLLO_INCLUDES_DIR. '/scripts/class-apollo-utility-tool.php';
}
/**
 * Vandd: @ticket #12150
 * Allow "scrolling with page" option can be run with both cases of authenticated and anonymous users
 */
$scrollWithPage = of_get_option(Apollo_DB_Schema::_SCROLL_WITH_PAGE) ? '' :  'scroll-with-page';


$navStyle = of_get_option(Apollo_DB_Schema::_NAVIGATION_BAR_STYLE);
$borderBottom = ($navStyle == 'two-rows') || of_get_option(Apollo_DB_Schema::_ENABLE_TWO_ROW_NAVIGATION) ? 'two-row' : '';
$globalSearchElement = apply_filters('add_global_search_below_nav', '');

/*@ticket #18331: 0002504: Arts Education Customizations - Displaying alternate masthead, nav menu, right column and footer for a list of defined pages/modules*/
$generateHtmlContent = new GenerateStaticHTMLFactory();

?>

<?php if(of_get_option(Apollo_DB_Schema::_ENABLE_TOP_HEADER)) { ?>
    <div id="topbar" class="<?php echo $scrollWithPage; ?>">
        <div class="inner">
            <?php echo Apollo_App::get_static_html_content(Apollo_DB_Schema::_TOP_HEADER); ?>
        </div>
    </div>
<?php  }?>
<header class="header <?php echo $borderBottom;?> <?php echo !empty($globalSearchElement['class']) ? $globalSearchElement['class'] : '' ?>" >
    <div class="inner">
        <div class="top-head" >
            <?php language_selector(); ?>
            <?php echo $generateHtmlContent->generateHtml(Apollo_DB_Schema::_HEADER ) ;?>
            <div class="mobile-menu">
                <a href="javascript:;" data-tgr=".main-menu.tablet-show" class="mb-menu tablet-show"><?php _e( 'MENU', 'apollo' ) ?></a>
                <?php
                 if(!empty($globalSearchElement['icon']) && !empty($globalSearchElement['form_search']))
                 {
                     echo $globalSearchElement['icon'];
                     echo $globalSearchElement['form_search'];
                 }
                ?>
                <?php
                    $tabletMenuClass = apply_filters('apl_tablet_menu_class', '');
                ?>

                <div class="main-menu tablet-show <?php echo $scrollWithPage; ?> <?php $tabletMenuClass ?>">
                    <div class="inner">
                        <?php
                        $cacheFilePrimaryNavigation = aplc_instance('APLC_Inc_Files_PrimaryNavigation');
                        $menu_str = $cacheFilePrimaryNavigation->getContent();
                        ?>
                        <?php $is_collapse_children_mobile = of_get_option( Apollo_DB_Schema::_COLLAPSEALLCHILDRENTMOBILE ) ?>
                        <nav class="mn-menu <?php echo $is_collapse_children_mobile ? 'has-toogle__menu-child' : '' ?>">
                            <?php
                            echo $menu_str;
                            ?>
                        </nav>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="main-menu pc-show <?php echo $navStyle ?> <?php echo $scrollWithPage; ?>">
        <div class="inner">
            <nav class="mn-menu">
                <?php
                echo $menu_str;
                if(!empty($globalSearchElement['icon'])) { echo $globalSearchElement['icon'];}
                ?>
            </nav>
            <?php if(!empty($globalSearchElement['form_search'])) { echo $globalSearchElement['form_search'];} ?>
        </div>
<?php if ($navStyle == 'two-tiers'){ ?>
        <div class="menu-background"></div>
    <?php } ?>
    </div>
</header>

<?php 

global $current_blog;
/* Thienld : displaying top search event horizontal on desktop version */
/* Vulh update new position: inside alt spotlight */
$GLOBALS['aplTopEventWidget'] = Apollo_App::displaySearchEventHorizontalOnDesktop();
if (of_get_option(Apollo_DB_Schema::_HOR_EVENT_SEARCH_LOC, 'default') == 'default' || !Apollo_App::is_homepage()) {
    echo $GLOBALS['aplTopEventWidget'];
}

/* Thienld : displaying top search widget modules on mobile devices */
Apollo_App::displaySearchArtistWidgetOnMobile();
Apollo_App::displaySearchBusinessWidgetOnMobile();
Apollo_App::displaySearchClassifiedWidgetOnMobile();
Apollo_App::displaySearchEducationWidgetOnMobile();
Apollo_App::displaySearchEventWidgetOnMobile();
Apollo_App::displaySearchOrganizationWidgetOnMobile();
Apollo_App::displaySearchPublicArtWidgetOnMobile();
Apollo_App::displaySearchVenueWidgetOnMobile();