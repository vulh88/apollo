<?php  $blogCustomLabel = Apollo_App::getCustomLabelByModuleName(Apollo_DB_Schema::_BLOG_POST_PT); ?>
<div class="breadcrumbs blog">
    <ul class="nav">
      <li><a href="<?php echo Apollo_App::changeBreadcrumbLink(Apollo_DB_Schema::_BLOG_POST_PT) ?>" ><?php _e( 'Home' ) ?></a></li>

        <?php if (get_query_var('_apollo_page_blog')) { ?>
            <li><span><?php echo $blogCustomLabel; ?></span></li>
        <?php } else { ?>
            <li><span><a href="<?php echo Apollo_App::getCustomUrlByModuleName(Apollo_DB_Schema::_BLOG_POST_PT); ?>"><?php echo $blogCustomLabel; ?></a></span>
            </li>
            <li> <span><?php if (is_author()) {
                        echo get_the_author();
                    } else if (is_category()) {
                        single_cat_title();
                    } else {
                        echo get_the_date('F Y');
                    } ?></span></li>
        <?php } ?>

    </ul>
</div>

<?php
if (is_author()) {
    include apollo_author_page();
}
else {
    include APOLLO_SHORTCODE_DIR . '/blog/template.php';
} ?>



