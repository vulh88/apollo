<?php
/**
 * Theme activation
 */
if ( is_admin() && isset( $_GET['activated'] ) && 'themes.php' == $GLOBALS['pagenow'] ) {
	wp_redirect( admin_url( 'themes.php?page=theme_activation_options' ) );
	exit;
}

function apollo_theme_activation_options_init() {
	register_setting(
		'apollo_activation_options',
		Apollo_DB_Schema::_APOLLO_THEME_ACTIVATION_OPTIONS,
		'apollo_theme_activation_sanitize_callback'
	);
}

add_action( 'admin_init', 'apollo_theme_activation_options_init' );

function apollo_activation_options_page_capability( $capability ) {
	return 'edit_theme_options';
}

add_filter( 'option_page_capability_apollo_activation_options', 'apollo_activation_options_page_capability' );

function apollo_theme_activation_options_add_page() {
	$apollo_activation_options = apollo_get_theme_activation_options();

    if ( get_option( 'apollo_re_active_theme' ) == 1 ) {
        $apollo_activation_options = false;
        delete_option( 'apollo_re_active_theme' );
    }

    	if ( ! $apollo_activation_options ) {
		$theme_page = add_theme_page(
			__( 'Theme Activation', 'apollo' ),
			__( 'Theme Activation', 'apollo' ),
			'edit_theme_options',
			'theme_activation_options',
			'apollo_theme_activation_options_render_page'
		);
	} else {
		if ( is_admin() && isset( $_GET['page'] ) && $_GET['page'] === 'theme_activation_options' ) {
			global $wp_rewrite;
            $wp_rewrite->init();
            $wp_rewrite->flush_rules();
			wp_redirect( admin_url( 'themes.php' ) );
			exit;
		}
	}
}

add_action( 'admin_menu', 'apollo_theme_activation_options_add_page', 50 );

function apollo_get_theme_activation_options() {
	return get_option( Apollo_DB_Schema::_APOLLO_THEME_ACTIVATION_OPTIONS );
}

function apollo_theme_activation_options_render_page() {
	?>
	<div class="wrap">
		<h2><?php printf( __( '%s Theme Activation', 'apollo' ), wp_get_theme() ); ?></h2>

		<div class="update-nag">
			<?php _e( 'These settings are optional and should usually be used only on a fresh installation', 'apollo' ); ?>
		</div>
		<?php settings_errors(); ?>

		<form method="post" action="options.php">
			<?php settings_fields( 'apollo_activation_options' ); ?>
			<table class="form-table">
				<tr valign="top">
					<th scope="row"><?php _e( 'Create static front page?', 'apollo' ); ?></th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span><?php _e( 'Create static front page?', 'apollo' ); ?></span></legend>
                            <select name="<?php echo Apollo_DB_Schema::_APOLLO_THEME_ACTIVATION_OPTIONS; ?>[<?php echo Apollo_DB_Schema::_CREATE_FRONT_PAGE; ?>]" id="create_front_page">
								<option selected="selected" value="true"><?php echo _e( 'Yes', 'apollo' ); ?></option>
								<option value="false"><?php echo _e( 'No', 'apollo' ); ?></option>
							</select>

							<p class="description"><?php printf( __( 'Create a page called Home and set it to be the static front page', 'apollo' ) ); ?></p>
						</fieldset>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row"><?php _e( 'Change permalink structure?', 'apollo' ); ?></th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span><?php _e( 'Update permalink structure?', 'apollo' ); ?></span></legend>
                            <select name="<?php echo Apollo_DB_Schema::_APOLLO_THEME_ACTIVATION_OPTIONS; ?>[<?php echo Apollo_DB_Schema::_CHANGE_PERMALINK_STRUCTURE ?>]"
							        id="change_permalink_structure">
								<option selected="selected" value="true"><?php echo _e( 'Yes', 'apollo' ); ?></option>
								<option value="false"><?php echo _e( 'No', 'apollo' ); ?></option>
							</select>

							<p class="description"><?php printf( __( 'Change permalink structure to /&#37;postname&#37;/', 'apollo' ) ); ?></p>
						</fieldset>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row"><?php _e( 'Create navigation menu?', 'apollo' ); ?></th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span><?php _e( 'Create navigation menu?', 'apollo' ); ?></span></legend>
                            <select name="<?php echo Apollo_DB_Schema::_APOLLO_THEME_ACTIVATION_OPTIONS; ?>[<?php echo Apollo_DB_Schema::_CREATE_NAVIGATION_MENUS ?>]"
							        id="create_navigation_menus">
								<option selected="selected" value="true"><?php echo _e( 'Yes', 'apollo' ); ?></option>
								<option value="false"><?php echo _e( 'No', 'apollo' ); ?></option>
							</select>

							<p class="description"><?php printf( __( 'Create the Primary Navigation menu and set the location', 'apollo' ) ); ?></p>
						</fieldset>
					</td>
				</tr>

				<tr valign="top">
					<th scope="row"><?php _e( 'Add pages to menu?', 'apollo' ); ?></th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span><?php _e( 'Add pages to menu?', 'apollo' ); ?></span></legend>
                            <select name="<?php echo Apollo_DB_Schema::_APOLLO_THEME_ACTIVATION_OPTIONS; ?>[<?php echo Apollo_DB_Schema::_ADD_PAGES_TO_PRIMARY_NAVIGATION ?>]"
							        id="add_pages_to_primary_navigation">
								<option value="true"><?php echo _e( 'Yes', 'apollo' ); ?></option>
								<option selected="selected" value="false"><?php echo _e( 'No', 'apollo' ); ?></option>
							</select>

							<p class="description"><?php printf( __( 'Add all current published pages to the Primary Navigation', 'apollo' ) ); ?></p>
						</fieldset>
					</td>
				</tr>

				<tr valign="top">
					<th scope="row"><?php _e( 'Setup Default Size ?', 'apollo' ); ?></th>

					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span><?php _e( 'Setup Default Size for wordpress?', 'apollo' ); ?></span></legend>
                            <input type="hidden" name="<?php echo Apollo_DB_Schema::_APOLLO_THEME_ACTIVATION_OPTIONS; ?>[<?php echo Apollo_DB_Schema::_FLAG_SETUP_FIRST_SIZE ?>]" value="false"/>
							<table class="form-table">
                                <?php
                                $tsw = get_option('thumbnail_size_w');
                                $tsh = get_option('thumbnail_size_h');
                                $thumbnail_size_w = $tsw ? $tsw : 150;//esc_attr( get_option( ''thumbnail_size_w' ) );
                                $thumbnail_size_h = $tsh ? $tsh : 150;//esc_attr( get_option( ''thumbnail_size_h' ) );
                                ?>
								<tr>
									<th scope="row"><?php _e('Thumbnail size') ?></th>
									<td>
										<label for="thumbnail_size_w"><?php _e('Width'); ?></label>
										<input name="thumbnail_size_w" type="number" step="1" min="0" id="thumbnail_size_w" value="<?php echo $thumbnail_size_w; ?>" class="small-text" />
										<label for="thumbnail_size_h"><?php _e('Height'); ?></label>
										<input name="thumbnail_size_h" type="number" step="1" min="0" id="thumbnail_size_h" value="<?php echo $thumbnail_size_h; ?>" class="small-text" /><br />
										<input name="thumbnail_crop" type="checkbox" id="thumbnail_crop" value="1" <?php checked('1', get_option('thumbnail_crop')); ?>/>
										<label for="thumbnail_crop"><?php _e('Crop thumbnail to exact dimensions (normally thumbnails are proportional)'); ?></label>
									</td>
								</tr>

                                <?php
                                $msw = get_option('medium_size_w');
                                $msh = get_option('medium_size_h');
                                $medium_size_w = $msw ? $msw : 450;//form_option('medium_size_w');
                                $medium_size_h = $msh ? $msh : 350;// form_option('medium_size_h');
                                ?>
								<tr>
									<th scope="row"><?php _e('Medium size') ?></th>
									<td><fieldset><legend class="screen-reader-text"><span><?php _e('Medium size'); ?></span></legend>
											<label for="medium_size_w"><?php _e('Max Width'); ?></label>
											<input name="medium_size_w" type="number" step="1" min="0" id="medium_size_w" value="<?php echo $medium_size_w; ?>" class="small-text" />
											<label for="medium_size_h"><?php _e('Max Height'); ?></label>
											<input name="medium_size_h" type="number" step="1" min="0" id="medium_size_h" value="<?php echo $medium_size_h; ?>" class="small-text" />
										</fieldset></td>
								</tr>

                                <?php
                                $lsw = get_option('large_size_w');
                                $lsh = get_option('large_size_h');
                                $large_size_w = $lsw ? $lsw : 1024;//form_option('large_size_w');
                                $large_size_h = $lsh ? $lsh : 700;// form_option('large_size_h');
                                ?>
								<tr>
									<th scope="row"><?php _e('Large size') ?></th>
									<td><fieldset><legend class="screen-reader-text"><span><?php _e('Large size'); ?></span></legend>
											<label for="large_size_w"><?php _e('Max Width'); ?></label>
											<input name="large_size_w" type="number" step="1" min="0" id="large_size_w" value="<?php echo $large_size_w ?>" class="small-text" />
											<label for="large_size_h"><?php _e('Max Height'); ?></label>
											<input name="large_size_h" type="number" step="1" min="0" id="large_size_h" value="<?php echo $large_size_h ?>" class="small-text" />
										</fieldset></td>
								</tr>
							</table>
							<?php do_settings_fields('media', 'default'); ?>

							<p class="description"><?php printf( __( 'Set beginning size for theme', 'apollo' ) ); ?></p>
						</fieldset>
					</td>
				</tr>

			</table>
			<?php submit_button(); ?>
		</form>
	</div>

<?php
}

function apollo_theme_activation_sanitize_callback($input) {

	if ( strpos( wp_get_referer(), 'page=theme_activation_options' ) === FALSE ) {
		return;
	}

	if ( isset($input[Apollo_DB_Schema::_CREATE_FRONT_PAGE]) && $input[Apollo_DB_Schema::_CREATE_FRONT_PAGE] === 'true' ) {

		$default_pages  = array ( __( 'Home', 'apollo' ) );
		$existing_pages = get_pages();
		$temp           = array ();

		foreach ( $existing_pages as $page ) {
			$temp[] = $page->post_title;
		}

		$pages_to_create = array_diff( $default_pages, $temp );

		foreach ( $pages_to_create as $new_page_title ) {
			$add_default_pages = array (
				'post_title'   => $new_page_title,
				'post_content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum consequat, orci ac laoreet cursus, dolor sem luctus lorem, eget consequat magna felis a magna. Aliquam scelerisque condimentum ante, eget facilisis tortor lobortis in. In interdum venenatis justo eget consequat. Morbi commodo rhoncus mi nec pharetra. Aliquam erat volutpat. Mauris non lorem eu dolor hendrerit dapibus. Mauris mollis nisl quis sapien posuere consectetur. Nullam in sapien at nisi ornare bibendum at ut lectus. Pellentesque ut magna mauris. Nam viverra suscipit ligula, sed accumsan enim placerat nec. Cras vitae metus vel dolor ultrices sagittis. Duis venenatis augue sed risus laoreet congue ac ac leo. Donec fermentum accumsan libero sit amet iaculis. Duis tristique dictum enim, ac fringilla risus bibendum in. Nunc ornare, quam sit amet ultricies gravida, tortor mi malesuada urna, quis commodo dui nibh in lacus. Nunc vel tortor mi. Pellentesque vel urna a arcu adipiscing imperdiet vitae sit amet neque. Integer eu lectus et nunc dictum sagittis. Curabitur commodo vulputate fringilla. Sed eleifend, arcu convallis adipiscing congue, dui turpis commodo magna, et vehicula sapien turpis sit amet nisi.',
				'post_status'  => 'publish',
				'post_type'    => 'page'
			);

			$result = wp_insert_post( $add_default_pages );
		}

		$home = get_page_by_title( __( 'Home', 'apollo' ) );
		update_option( 'show_on_front', 'page' );
		update_option( 'page_on_front', $home->ID );

		$home_menu_order = array (
			'ID'         => $home->ID,
			'menu_order' => - 1
		);
		wp_update_post( $home_menu_order );
	}

	if ( isset($input[Apollo_DB_Schema::_CHANGE_PERMALINK_STRUCTURE]) && $input[Apollo_DB_Schema::_CHANGE_PERMALINK_STRUCTURE] === 'true' ) {

		if ( get_option( 'permalink_structure' ) !== '/%postname%/' ) {
			global $wp_rewrite;
			$wp_rewrite->set_permalink_structure( '/%postname%/' );
            $wp_rewrite->init();
            $wp_rewrite->flush_rules();
		}
	}

	if ( isset($input[Apollo_DB_Schema::_CREATE_NAVIGATION_MENUS]) && $input[Apollo_DB_Schema::_CREATE_NAVIGATION_MENUS] === 'true' ) {

		$apollo_nav_theme_mod = FALSE;

		$primary_nav = wp_get_nav_menu_object( __( 'Primary Navigation', 'apollo' ) );

		if ( ! $primary_nav ) {
			$primary_nav_id                             = wp_create_nav_menu( __( 'Primary Navigation', 'apollo' ), array ( 'slug' => 'primary_navigation' ) );
			$apollo_nav_theme_mod['primary_navigation'] = $primary_nav_id;
		} else {
			$apollo_nav_theme_mod['primary_navigation'] = $primary_nav->term_id;
		}

		if ( $apollo_nav_theme_mod ) {
			set_theme_mod( 'nav_menu_locations', $apollo_nav_theme_mod );
		}
	}

	if ( isset($input[Apollo_DB_Schema::_ADD_PAGES_TO_PRIMARY_NAVIGATION]) ) {
		$apollo_theme_activation_options[Apollo_DB_Schema::_ADD_PAGES_TO_PRIMARY_NAVIGATION] = FALSE;

		$primary_nav         = wp_get_nav_menu_object( __( 'Primary Navigation', 'apollo' ) );
		$primary_nav_term_id = (int) $primary_nav->term_id;
		$menu_items          = wp_get_nav_menu_items( $primary_nav_term_id );

        if ( ! $menu_items || empty( $menu_items ) ) {

            $is_not_active = $input[Apollo_DB_Schema::_ADD_PAGES_TO_PRIMARY_NAVIGATION] === 'false';

            if($is_not_active) {
                $pages = array(
                    get_page_by_title( 'Home' )
                );
            }
            else {
                $pages = get_pages();
            }

            if(!empty($pages)) {
                foreach ( $pages as $page ) {
                    $item = array (
                        'menu-item-object-id' => $page->ID,
                        'menu-item-object'    => 'page',
                        'menu-item-type'      => 'post_type',
                        'menu-item-status'    => 'publish'
                    );
                    wp_update_nav_menu_item( $primary_nav_term_id, 0, $item );
                }
            }
		}
	}

	if(isset($input[Apollo_DB_Schema::_FLAG_SETUP_FIRST_SIZE]) && $input[Apollo_DB_Schema::_FLAG_SETUP_FIRST_SIZE] === 'false') {
		// HACK FEATURE
		$media_options =  array( 'thumbnail_size_w', 'thumbnail_size_h', 'thumbnail_crop', 'medium_size_w', 'medium_size_h', 'large_size_w', 'large_size_h', 'image_default_size', 'image_default_align', 'image_default_link_type' );

		foreach ( $media_options as $option ) {
			$option = trim( $option );
			$value = null;
			if ( isset( $_POST[ $option ] ) ) {
				$value = $_POST[ $option ];
				if ( ! is_array( $value ) )
					$value = trim( $value );
				$value = wp_unslash( $value );
			}
			update_option( $option, $value );
		}
		// END HACK

		$input[Apollo_DB_Schema::_FLAG_SETUP_FIRST_SIZE] = true;
	}

	return $input;
}

function apollo_deactivation() {
	delete_option( Apollo_DB_Schema::_APOLLO_THEME_ACTIVATION_OPTIONS );
}

add_action( 'switch_theme', 'apollo_deactivation' );
