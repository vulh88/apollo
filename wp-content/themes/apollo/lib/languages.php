<?php
function language_selector(){
    if (!function_exists('icl_get_languages')) return false;
    $languages = icl_get_languages('skip_missing=0&orderby=code');
    if(!empty($languages)){
        echo '<ul id="lansel">';
        $i=0;
        foreach($languages as $l){

            $i++;
            echo '<li>';
              if(!$l['active']) echo '<a href="'.$l['url'].'">';
            echo '<img src="'.$l['country_flag_url'].'" alt="'.$l['language_code'].'"/>';
              if(!$l['active']) echo '</a>';
            echo '</li>';

        }
        echo '</ul>';
    }
  
}

function my_text_strings( $translated_text, $text, $domain ) {
  switch ( $translated_text ) {
    case '{Login}' :
      $translated_text = __( 'LOGIN', 'apollo' );
      break;
    case '{Logout}' :
      $translated_text = __( 'LOGOUT', 'apollo' );
      break;
  }
  return $translated_text;
}
add_filter( 'gettext', 'my_text_strings', 20, 3 );