<?php
/**
 * Clean up the_excerpt()
 */
function apollo_excerpt_more($more) {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'apollo') . '</a>';
}
add_filter('excerpt_more', 'apollo_excerpt_more');

/**
 * Manage output of wp_title()
 */
function apollo_wp_title($title) {
  if (is_feed()) {
    return $title;
  }

  $title .= get_bloginfo('name');

  return $title;
}
add_filter('wp_title', 'apollo_wp_title', 10);


/**
 * Author : ThienLD
 * [START] Sonoma's functions need use for Apollo Parent . These functions must be checked if existing functions for using anywhere in parent theme
 */

if(! function_exists('apl_sm_get_current_active_theme')){
    function apl_sm_get_current_active_theme(){
      $currentTheme = '';
      if ( has_filter(APL_SM_HOOK_FILTER_CURRENT_ACTIVE_THEME)) {
        $currentTheme = apply_filters(APL_SM_HOOK_FILTER_CURRENT_ACTIVE_THEME,'');
      }
      return $currentTheme;
    }
}

/**
 * Author : ThienLD
 * [END] Sonoma's functions need use for Apollo Parent . These functions must be checked if existing functions for using anywhere in parent theme
 */


/**
 * get pagesize default
 * @author TruongHn
 * @return int
 */
function apl_get_page_size_default(){
    $currentTheme = function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '';
    return $currentTheme == APL_SM_DIR_THEME_NAME ? SM_Common_Const::_SM_ITEMS_NUM_LISTING_PAGE  :  Apollo_Display_Config::APL_DEFAULT_NUMBER_ITEMS_FIRST_PAGE;
}

