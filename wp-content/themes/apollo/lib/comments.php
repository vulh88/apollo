<?php
/**
 * Use Bootstrap's media object for listing comments
 *
 * @link http://getbootstrap.com/components/#media
 */
class apollo_Walker_Comment extends Walker_Comment {
  function start_lvl(&$output, $depth = 0, $args = array()) {
    $GLOBALS['comment_depth'] = $depth + 1; ?>
    <ul class="children">
    <?php
  }

  function end_lvl(&$output, $depth = 0, $args = array()) {
    $GLOBALS['comment_depth'] = $depth + 1;
    echo '</ul>';
  }

  function start_el(&$output, $comment, $depth = 0, $args = array(), $id = 0) {
    $depth++;
    $GLOBALS['comment_depth'] = $depth;
    $GLOBALS['comment'] = $comment;

    if (!empty($args['callback'])) {
      call_user_func($args['callback'], $comment, $args, $depth);
      return;
    }

    extract($args, EXTR_SKIP); ?>

  <li id="li-comment-<?php comment_ID(); ?>" <?php comment_class('comment even thread-even depth-'.$depth); ?>>
    <?php include(locate_template('templates/comment.php')); ?>
  <?php
  }

  function end_el(&$output, $comment, $depth = 0, $args = array()) {
    if (!empty($args['end-callback'])) {
      call_user_func($args['end-callback'], $comment, $args, $depth);
      return;
    }
    // Close ".media-body" <div> located in templates/comment.php, and then the comment's <li>
    echo "</article></li>\n";
  }
}

function apollo_get_avatar($avatar, $type) {
  if (!is_object($type)) { return $avatar; }

  $avatar = str_replace("class='avatar", "class='avatar pull-left media-object", $avatar);
  return $avatar;
}
add_filter('get_avatar', 'apollo_get_avatar', 10, 2);

/**
 * Copy from core wordpress
 * @param array $args
 * @param null $post_id
 */
function apollo_comment_form( $args = array(), $post_id = null ) {

    if ( null === $post_id )
        $post_id = get_the_ID();


    $captcha = new Apollo_Captcha();

    $commenter = wp_get_current_commenter();    /* get captcha again */
    $commenter['captcha_code'] = $captcha->getCode(); // Generate by captcha class here. also for image.
    $commenter['captcha_image_path'] = $captcha->getImagePath();
    $commenter['captcha_check_path'] = $captcha->getUrlCheckCaptcha();
    $user = wp_get_current_user();
    $user_identity = $user->exists() ? $user->display_name : '';

    $args = wp_parse_args( $args );
    if ( ! isset( $args['format'] ) )
        $args['format'] = current_theme_supports( 'html5', 'comment-form' ) ? 'html5' : 'xhtml';

    $req      = get_option( 'require_name_email' );
    $aria_req = ( $req ? " aria-required='true'" : '' );
    $html5    = 'html5' === $args['format'];
    $fields   =  array(
        'author' => '<div class="inp-half">' . '<label for="author">' . __( 'Your name' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
            '<input data-error_holder="#_comment_author_err" data-error_hide="hidden" data-check="required" id="author" name="author" class="inp inp-txt" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></div>',
        'email'  => '<div class="inp-half"><label for="email">' . __( 'Your email' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
            '<input data-error_holder="#_comment_email_err" data-error_hide="hidden" data-check="required,email" id="email" class="inp inp-txt" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></div>',
    );

    $required_text = sprintf( ' ' . __('Required fields are marked %s'), '<span class="required">*</span>' );

    /**
     * Filter the default comment form fields.
     *
     * @since 3.0.0
     *
     * @param array $fields The default comment fields.
     */
    $fields = apply_filters( 'comment_form_default_fields', $fields );
    $defaults = array(
        'fields'               => $fields,
        'comment_field'        => '<div class="clearfix"><label for="comment">' . __( 'Comment', 'apollo' ) .( $req ? ' <span class="required">*</span>' : '' ) . '</label> <textarea id="comment" data-error_holder="#_comment_content_err" data-error_hide="hidden" data-check="required" name="comment" cols="45" rows="8" class="inp inp-txt textarea" aria-required="true"></textarea></div>',
        /** This filter is documented in wp-includes/link-template.php */
        'must_log_in'          => '<p class="must-log-in">' . sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',
        /** This filter is documented in wp-includes/link-template.php */
        'logged_in_as'         => '<p class="logged-in-as">' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>' ), get_edit_user_link(), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',
        'comment_notes_before' => '<p class="comment-notes">' . __( 'Your email address will not be published.' ) . ( $req ? $required_text : '' ) . '</p>',

        'captcha_input_code'        => '<div class="inp-half">' . '<label for="_captcha">' . __( 'CAPTCHA Code' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
            '<input
            data-error_holder="#_comment_captcha_err"
            data-captcha_image="#respond #_captcha_image"
            data-error_hide="hidden"
            data-check="required,captcha"
            data-owner_form="#commentform"
            data-url_check_captcha="'.$commenter['captcha_check_path'].'"
            id="_captcha" name="captcha_code" class="inp inp-txt" type="text" value="" size="30"' . $aria_req . ' /></div>',

        'captcha_input_image'        => '<div class="inp-half">' .'<img id="_captcha_image" src="'.$commenter['captcha_image_path'].'" />'. '</div>',
        'comment_notes_after'  => '<p class="pre-code">' . sprintf( __( '<b>You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes</b>: %s' ), ' <code>' . allowed_tags() . '</code>' ) . '</p>',
        'id_form'              => 'commentform',
        'id_submit'            => 'submit',
        'name_submit'          => 'submit_comment_event',
        'title_reply'          => __( 'Leave a Reply' ),
        'title_reply_to'       => __( 'Leave a Reply to %s' ),
        'cancel_reply_link'    => __( 'Cancel reply' ),
        'label_submit'         => __( 'Post Comment' ),
        'format'               => 'xhtml',
    );

    /**
     * Filter the comment form default arguments.
     *
     * Use 'comment_form_default_fields' to filter the comment fields.
     *
     * @since 3.0.0
     *
     * @param array $defaults The default comment form arguments.
     */
    $args = wp_parse_args( $args, apply_filters( 'comment_form_defaults', $defaults ) );

    ?>
    <?php if ( comments_open( $post_id ) ) : ?>
        <?php
        /**
         * Fires before the comment form.
         *
         * @since 3.0.0
         */
        do_action( 'comment_form_before' );

        $data_elem = "#respond #author, #respond #email, #respond #comment";
        $data_elem_sync = "";
        if(!is_user_logged_in()) {
            $data_elem .= ", #respond #_captcha";
            $data_elem_sync = "#respond #_captcha";
        }
        ?>
        <div id="respond" class="cm-frm" data-elems="<?php echo $data_elem ?>" data-elems-sync="<?php echo $data_elem_sync ?>">
            <h3 id="reply-title" class="comment-reply-title"><?php comment_form_title( $args['title_reply'], $args['title_reply_to'] ); ?> <small><?php cancel_comment_reply_link( ' ' ); ?></small></h3>

            <?php if(!is_user_logged_in()): ?>
                <?php echo $args['comment_notes_before']; ?>
             <?php else: ?>
                <?php
                /**
                 * Filter the 'logged in' message for the comment form for display.
                 *
                 * @since 3.0.0
                 *
                 * @param string $args_logged_in The logged-in-as HTML-formatted message.
                 * @param array  $commenter      An array containing the comment author's
                 *                               username, email, and URL.
                 * @param string $user_identity  If the commenter is a registered user,
                 *                               the display name, blank otherwise.
                 */
                echo apply_filters( 'comment_form_logged_in', $args['logged_in_as'], $commenter, $user_identity );
                ?>
            <?php endif; ?>

            <div class="_comment_err_container">
                <span class="error" id="_comment_email_err">
                    <span class="_required hidden">* <?php _e('Email address is required!', 'apollo') ?></span>
                    <span class="_email hidden">* <?php _e('This is not email address!', 'apollo') ?></span>
                </span>
                <span class="error" id="_comment_author_err">
                    <span class="_required hidden">* <?php _e('Author name is required!', 'apollo') ?></span>
                </span>
                <span class="error" id="_comment_content_err">
                    <span class="_required hidden">* <?php _e('Comment content is required!', 'apollo') ?></span>
                </span>
                <span class="error" id="_comment_captcha_err">
                    <span class="_required hidden">* <?php _e('Captcha is required!', 'apollo') ?></span>
                    <span class="_captcha hidden">* <?php _e('Captcha is not match!', 'apollo') ?></span>
                </span>
            </div>

            <?php if ( get_option( 'comment_registration' ) && !is_user_logged_in() ) : ?>
                <?php echo $args['must_log_in']; ?>
                <?php
                /**
                 * Fires after the HTML-formatted 'must log in after' message in the comment form.
                 *
                 * @since 3.0.0
                 */
                do_action( 'comment_form_must_log_in_after' );
                ?>
            <?php else : ?>
                <form action="<?php echo site_url( '/wp-comments-post.php' ); ?>" name="comment_event_form" method="post" id="<?php echo esc_attr( $args['id_form'] ); ?>" class="comment-form"<?php echo $html5 ? ' novalidate' : ''; ?>>
                    <?php
                    /**
                     * Fires at the top of the comment form, inside the <form> tag.
                     *
                     * @since 3.0.0
                     */
                    do_action( 'comment_form_top' );
                    ?>
                    <?php if ( is_user_logged_in() ) : ?>

                        <?php
                        /**
                         * Fires after the is_user_logged_in() check in the comment form.
                         *
                         * @since 3.0.0
                         *
                         * @param array  $commenter     An array containing the comment author's
                         *                              username, email, and URL.
                         * @param string $user_identity If the commenter is a registered user,
                         *                              the display name, blank otherwise.
                         */
                        do_action( 'comment_form_logged_in_after', $commenter, $user_identity );
                        ?>
                    <?php else : ?>
                        <?php
                        /**
                         * Fires before the comment fields in the comment form.
                         *
                         * @since 3.0.0
                         */
                        do_action( 'comment_form_before_fields' );

                        ?>
                        <div class="clearfix">
                        <?php
                        foreach ( (array) $args['fields'] as $name => $field ) {
                            /**
                             * Filter a comment form field for display.
                             *
                             * The dynamic portion of the filter hook, $name, refers to the name
                             * of the comment form field. Such as 'author', 'email', or 'url'.
                             *
                             * @since 3.0.0
                             *
                             * @param string $field The HTML-formatted output of the comment form field.
                             */
                            echo apply_filters( "comment_form_field_{$name}", $field ) . "\n";
                        }
                        ?>
                            </div>
                        <?php
                        /**
                         * Fires after the comment fields in the comment form.
                         *
                         * @since 3.0.0
                         */
                        do_action( 'comment_form_after_fields' );
                        ?>
                    <?php endif; ?>
                    <?php
                    /**
                     * Filter the content of the comment textarea field for display.
                     *
                     * @since 3.0.0
                     *
                     * @param string $args_comment_field The content of the comment textarea field.
                     */
                    echo apply_filters( 'comment_form_field_comment', $args['comment_field'] );

                    echo $args['comment_notes_after']; ?>

                    <?php if ( !is_user_logged_in() ) : ?>
                    <div class="clearfix">
                        <?php
                        echo $args['captcha_input_code'];
                        echo $args['captcha_input_image'];
                        ?>
                    </div>
                    <?php endif; ?>

                    <?php
                        $mmClass = Apollo_App::isMaintenanceMode() ? 'apl-mm-comment' : '';
                    ?>

                    <p class="t-l b-btn">
                        <input data-message="<?php _e("Sorry for the inconvenience.Our website is currently undergoing scheduled maintenance", "apollo") ?>" name="<?php echo esc_attr( $args['name_submit'] ); ?>" type="submit" id="<?php echo esc_attr( $args['id_submit'] ); ?>" class="btn btn-b <?php echo $mmClass ?>" value="<?php echo esc_attr( $args['label_submit'] ); ?>" />
                        <?php comment_id_fields( $post_id ); ?>
                        <input type="hidden" name="_apollo_submit_comment_event" value="1">
                    </p>
                    <?php
                    /**
                     * Fires at the bottom of the comment form, inside the closing </form> tag.
                     *
                     * @since 1.5.0
                     *
                     * @param int $post_id The post ID.
                     */
                    do_action( 'comment_form', $post_id );
                    ?>
                </form>
            <?php endif; ?>
        </div><!-- #respond -->
        <?php
        /**
         * Fires after the comment form.
         *
         * @since 3.0.0
         */
        do_action( 'comment_form_after' );
    else :
        /**
         * Fires after the comment form if comments are closed.
         *
         * @since 3.0.0
         */
        do_action( 'comment_form_comments_closed' );
    endif;
}
