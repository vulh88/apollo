<?php
/**
 * apollo initial setup and constants
 */
function apollo_setup() {
	// Make theme available for translation
	load_theme_textdomain( 'apollo', get_template_directory() . '/lang' );

	// Register wp_nav_menu() menus
	// http://codex.wordpress.org/Function_Reference/register_nav_menus
	register_nav_menus( array (
		'primary_navigation' => __( 'Primary Navigation', 'apollo' )
	) );

	// Add post thumbnails
	// http://codex.wordpress.org/Post_Thumbnails
	// http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
	// http://codex.wordpress.org/Function_Reference/add_image_size
	add_theme_support( 'post-thumbnails' );

	/* Retina images */

	// Thienld : add image size serves for printing search result events
	add_image_size ( 'print-event-img-size', 150, 150, array('left','top') );

	add_image_size ( 'apl_recent_posts', 100, 100 );

  // Add post formats
  // http://codex.wordpress.org/Post_Formats
  add_theme_support( 'post-formats', array ( 'aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio' ) );

  // Add HTML5 markup for captions
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support( 'html5', array ( 'caption' ) );

  // Tell the TinyMCE editor to use a custom stylesheet
  add_editor_style( '/assets/css/editor-style.css' );
}

add_action( 'after_setup_theme', 'apollo_setup' );

/**
 * Register sidebars
 */
function apollo_widgets_init() {
	register_sidebar( array (
		'name'          => __( 'Apollo Primary', 'apollo' ),
		'id'            => Apollo_DB_Schema::_SIDEBAR_PREFIX . '-primary',
		'before_widget' => '<section class="widget %1$s %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array (
		'name'          => __( 'Footer', 'apollo' ),
		'id'            => 'sidebar-footer',
		'before_widget' => '<section class="widget %1$s %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );
    global $apollo_modules;
    $avaiable_modules = Apollo_App::get_avaiable_modules();
    if( $avaiable_modules ) {
        foreach( $avaiable_modules as $am ) {
            if ( $am == Apollo_DB_Schema::_EVENT_PT || $am == Apollo_DB_Schema::_AGENCY_PT || $am == Apollo_DB_Schema::_PHOTO_SLIDER_PT || $am ==  Apollo_DB_Schema::_SYNDICATION_PT ) continue;
            register_sidebar( array (
                'name'          => sprintf( '%s Area', $am == 'post' ? __('Blog', 'apollo') :$apollo_modules[$am]['sing'] ),
                'id'            => Apollo_DB_Schema::_SIDEBAR_PREFIX . '-'. str_replace('-', '_', $am),
                'before_widget' => '<section class="widget %1$s %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h3>',
                'after_title'   => '</h3>',
            ) );
        }
    }

	register_sidebar( array (
		'name'          => __( 'Area Of All Modules', 'apollo' ),
		'id'            => 'sidebar-modules-area',
		'before_widget' => '<section class="widget %1$s %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );
}

add_action( 'widgets_init', 'apollo_widgets_init' );

function disable_kses_content() {
    if(apollo_get_current_user_role() === 'Administrator') {
        remove_filter('content_save_pre', 'wp_filter_post_kses');
    }
}
add_action('init','disable_kses_content',20);