<?php
/**
 * Theme wrapper
 *
 */
function apollo_template_path()
{
    if(get_query_var('_apollo_artist_search') === 'true') {
        return new apollo_Wrapping('templates/search/templates/artists.php');
    }
    if(get_query_var('_apollo_educator_search') === 'true') {
        return new apollo_Wrapping('templates/search/templates/educators.php');
    }
    if(get_query_var('_apollo_event_search') === 'true') {

        return new apollo_Wrapping('templates/search/templates/event.php');
    }
    
    if( get_query_var( '_apollo_program_search' ) === 'true' ) {
        return new apollo_Wrapping('templates/search/templates/programs.php');
    }
    if( get_query_var( '_apollo_org_search' ) === 'true' ) {
        return new apollo_Wrapping('templates/search/templates/org.php');
    }
    if( get_query_var( '_apollo_venue_search' ) === 'true' ) {

        return new apollo_Wrapping('templates/search/templates/venue.php');
    }
    if( get_query_var( '_apollo_classified_search' ) === 'true' ) {

        return new apollo_Wrapping('templates/search/templates/classified.php');
    }
    if( get_query_var( '_apollo_public_art_search' ) === 'true' ) {

        return new apollo_Wrapping('templates/search/templates/public-art.php');
    }
    if(get_query_var('_apollo_artist_timeline') === 'true') {
        return new apollo_Wrapping('templates/artists/timeline.php');
    }
    // Thienld : add template for list/search business page
    if( get_query_var( '_apollo_business_search' ) === 'true' ) {
        return new apollo_Wrapping('templates/search/templates/business.php');
    }
    // Vulh : add template for list authors page
    if( get_query_var( '_apollo_author_search' ) === 'true' ) {
        return new apollo_Wrapping('templates/search/templates/authors.php');
    }
    //
    if(get_query_var( '_apollo_clean_form' ) === 'true') {
        /** @Ticket #13470 */
        return new apollo_Wrapping('templates/partials/clean-form.php');
    }
    if( get_query_var( '_apollo_news_search' ) === 'true' ) {
        /*@ticket #18127: 0002503: News Module*/
        return new apollo_Wrapping('templates/search/templates/news.php');
    }
    else {
        return apollo_Wrapping::$main_template;
    }

}
function apollo_author_page(){
    return new  apollo_Wrapping('templates/content-single/author.php');
}

function apollo_sidebar_path()
{
    return new apollo_Wrapping('templates/sidebar.php');
}

function apollo_sidebar_path_artist()
{
    /** @Ticket #13364 */
    return new apollo_Wrapping('templates/artists/listing-page/sidebar-artist.php');
}
function apollo_sidebar_path_org()
{
    return new apollo_Wrapping('templates/pages/org/template/sidebar-organization.php');
}

function apollo_sidebar_dashboard_path()
{
    return new apollo_Wrapping('templates/pages/dashboard/sidebar-dashboard.php');
}

class apollo_Wrapping
{
    // Stores the full path to the main template file
    static $main_template;

    // Stores the base name of the template file; e.g. 'page' for 'page.php' etc.
    static $base;

    public function __construct($template = 'base.php')
    {
        $this->slug = basename($template, '.php');
        $this->templates = array($template);

        if (self::$base) {
            $str = substr($template, 0, -4);
            array_unshift($this->templates, sprintf($str . '-%s.php', self::$base));
        }
    }

    public function __toString()
    {
        $this->templates = apply_filters('apollo/wrap_' . $this->slug, $this->templates);
        return locate_template($this->templates);
    }

    static function wrap($main)
    {

        //call static file
        if($GLOBALS['wp']->matched_rule === '^map/?$') {
            include APOLLO_TEMPLATES_DIR.'/map.php';
            die;
        }

        // Left part
        self::$main_template = $main;
        self::$base = basename(self::$main_template, '.php');

        if (self::$base === 'index') {
            self::$base = false;
        }
        
        if ( get_query_var( '_apollo_print_event_page' ) ) {
            $template = 'templates/events/html/print.php';
        } 
        else if ( get_query_var( '_apollo_print_artist_page' ) ) {
            $template = 'templates/artists/print.php';
        }
        else if ( get_query_var( '_apollo_print_educator_page' ) ) {
            $template = 'templates/educator/print.php';
        }
        else if ( get_query_var( '_apollo_print_org_page' ) ) {
            $template = 'templates/org/print.php';
        }
        else if ( get_query_var( '_apollo_print_venue_page' ) ) {
            $template = 'templates/venue/print.php';
        }
        else if ( get_query_var( '_apollo_print_classified_page' ) ) {
            $template = 'templates/classified/html/print.php';
        }
        else if ( get_query_var( '_apollo_print_public_art_page' ) ) {
            $template = 'templates/public-art/html/print.php';
        }
        else if ( get_query_var( '_apollo_print_post_page' ) ) {
            $template = 'templates/post/html/print.php';
        }
        //TrilM Export calendar
        else if ( get_query_var( '_apollo_export_calendar_event_page' ) ) {
            $template = 'templates/events/html/export-calendar.php';
        }
        else if (get_query_var('_apollo_api')) {
            return APOLLO_API_DIR. '/Route.php';
        }
        else if (get_query_var('_apollo_cron')) {
            return APOLLO_PARENT_DIR. '/lib/cron.php';
        }
        else if ( get_query_var( '_apollo_print_news_page' ) ) {
            /*@ticket #18127: 0002503: News Module*/
            $template = 'templates/news/html/print.php';
        }
        else {
            $pageTemplate = get_page_template();

            $customTemplates = array(
                'apl-blank-page.php',
                'test_template.php',
                'educator-landing-template.php'
            );
            $cusTempFilename = substr($pageTemplate, strrpos($pageTemplate, '/') + 1);
            $template = in_array($cusTempFilename, $customTemplates) ? $cusTempFilename : 'base.php';
        }
        
        return new apollo_Wrapping( $template );
    }
}

add_filter('template_include', array('apollo_Wrapping', 'wrap'), 99);

