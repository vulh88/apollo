<?php


/**
 * Scripts and stylesheets
 *
 * Enqueue stylesheets in the following order:
 * 1. /theme/assets/css/main.css
 *
 * Enqueue scripts in the following order:
 * 1. jquery-1.11.1.min.js via Google CDN
 * 2. /theme/assets/js/vendor/modernizr.min.js
 * 3. /theme/assets/js/scripts.js (in footer)
 *
 * Google Analytics is loaded after enqueued scripts if:
 * - An ID has been defined in config.php
 * - You're not logged in as an administrator
 */
function apollo_scripts() {
    /**
     * The build task in Grunt renames production assets with a hash
     * Read the asset names from assets-manifest.json
     */
    $themeUri       = get_stylesheet_directory_uri();
    $jsRootPath = $themeUri. '/assets/js/';
    $cssRootPath = $themeUri. '/assets/css/';
    $isDashboard    = Apollo_App::isDashboardPage();
    $isSearchWidgetRequest = Apollo_App::isSearchWidgetRequest();

    // For the search widget
    if($isSearchWidgetRequest === true) {
        $commonFnKey = 'apollo-js-common-functions';
        wp_enqueue_script( $commonFnKey, $jsRootPath . 'common-function.js', array('jquery','underscore'), null, false);
        wp_localize_script( $commonFnKey , 'APL', Apollo_App::jsCommunityWithClient());
        wp_localize_script( $commonFnKey , 'APL_Retina', array('apply' => of_get_option( Apollo_DB_Schema::_ENABLE_RETINA_DISPLAY )) );
        wp_enqueue_script( 'jquery-ui-datepicker' );
        wp_enqueue_script( 'jquery-ui-autocomplete' );
        return;
    }


    // Eject CSS
    aplEnqueueStyle($isDashboard, $cssRootPath);

    /**
     * Javascripts
     */
    wp_enqueue_script( 'jquery-ui-datepicker' );
    wp_enqueue_script( 'jquery-ui-autocomplete' );

    if($isDashboard) {
        wp_enqueue_script('apollo-js-editor-config', $themeUri . '/assets/ckeditor/ckeditor.js', array('jquery'), null, true);
        wp_enqueue_script('apollo-js-editor', $themeUri . '/assets/ckeditor/custom_ckeditor/ckeditor_config.js', array('jquery'), null, true);
    }

    // JQuery Datepicker
    wp_enqueue_script('apollo-jquery-datepicker-es', $jsRootPath. 'jquery-ui-i18n.min.js', array('jquery'), null, true);

    // Global JS
    aplEnqueueJs($jsRootPath);

    // JS For User Dashboard
    if($isDashboard){
        aplEnqueueDashboardJs($jsRootPath, $themeUri);

        if (get_query_var('_apollo_import_event_page') == 1) {
            aplEnqueueDashboardImportJs($jsRootPath);
        }
    }

    // Add additional js
    if ($addiJsFile = get_html_file_url(Apollo_DB_Schema::_ADDITIONAL_JS, 'js')) {
        wp_enqueue_script('apollo-additional-js', $addiJsFile, array('jquery'), null, true);
    }

    // JS Localize
    aplLocalizeJs();

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    // script for time line
    wp_register_script('apollo-js-timeline', $themeUri. '/assets/plugins/timelinejs/js/storyjs-embed.js', array('jquery', 'apollo-global-js-combine'), null, true);

    /*@ticket #17123 */
    $artistCustomSlug = of_get_option(Apollo_DB_Schema::_ARTIST_CUSTOM_SLUG, 'artist');
    if($GLOBALS['wp']->matched_rule === '^('. $artistCustomSlug .')/timeline/?$') {
        wp_enqueue_script('apollo-js-timeline');
    }

}
add_action('wp_enqueue_scripts', 'apollo_scripts', 100);


/**
 * Google Analytics snippet from HTML5 Boilerplate
 *
 * Cookie domain is 'auto' configured. See: http://goo.gl/VUCHKM
 */
function apollo_google_analytics() { ?>
    <script>

        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','<?php echo of_get_option(Apollo_DB_Schema::_GOOGLE_ANALYTICS_KEY); ?>','auto');ga('send','pageview');
    </script>

<?php }

if (of_get_option(Apollo_DB_Schema::_GOOGLE_ANALYTICS_KEY)) { //&& (WP_ENV === 'production' || current_user_can('manage_options'))) {
    add_action('wp_footer', 'apollo_google_analytics', 20);
}


/***
 *
 * @Author: TriLM
 * Admin calendar scripts and style
 *
 */
if (is_admin()) {
    include(APOLLO_ADMIN_DIR.'/post-types/meta-boxes/event/event-calendar/class-apollo-admin-script-style.php');
}

/**
 * Enqueue JS
 * @param string $jsRootPath
 *
 */
function aplEnqueueJs($jsRootPath) {
    $hookName = 'apollo-global-js-combine';
    $themeUri = get_stylesheet_directory_uri();
    wp_enqueue_script($hookName, apl_mix('/assets/js/apollo-global-js-combine.js', $themeUri), array('jquery', 'underscore'), false, true);
}

/**
 * Enqueue Dashboard JS
 * @param string $jsRootPath
 * @param string $themeUri
 *
 */
function aplEnqueueDashboardJs($jsRootPath, $themeUri) {

    wp_enqueue_script('apollo-js-editor-config', $themeUri . '/assets/ckeditor/ckeditor.js', array('jquery'), null, true);
    wp_enqueue_script('apollo-js-editor', $themeUri . '/assets/ckeditor/custom_ckeditor/ckeditor_config.js', array('jquery'), null, true);

    $hookName = 'apollo-js-dashboard-combine';
    $themeUri = get_stylesheet_directory_uri();
    wp_enqueue_script($hookName, apl_mix('/assets/js/apollo-js-dashboard-combine.js', $themeUri), array('jquery'), false, true);

    /* ENQUEUE BY CONDITION */
    if( $GLOBALS['wp']->matched_rule === "^user/add-event/step-1/?$"
        || $GLOBALS['wp']->matched_rule === '^user/add-event/step-1/?([0-9]{1,})/?$/?$'
        || $GLOBALS['wp']->matched_rule === '^user/copy-event/step-1/?([0-9]{1,})/?$/?$'
        || $GLOBALS['wp']->matched_rule === "^user/add-event/step-2b/?$"
        || $GLOBALS['wp']->matched_rule === '^user/add-event/step-2b/?([0-9]{1,})/?$/?$' ) {

        wp_enqueue_script('angular', 'https://ajax.googleapis.com/ajax/libs/angularjs/1.4.2/angular.min.js', array('jquery'), '1.2.28', true);
        wp_enqueue_script('angular-apl-calendar', $jsRootPath. "angular-apl-calendar.js", array('angular', $hookName ), '1.0.0', true);
    }

}


/**
 * Enqueue Dashboard Importing JS
 * @param string $jsRootPath
 *
 */
function aplEnqueueDashboardImportJs($jsRootPath) {

    $hookName = 'apollo-js-dashboard-import-combine';
    $themeUri = get_stylesheet_directory_uri();
    wp_enqueue_script($hookName, apl_mix('/assets/js/apollo-js-dashboard-import-combine.js', $themeUri), array('jquery'), false, true);


    // check current user can use import event tool so that enqueue script filestyle
    /** Vandd - @Ticket #12435 */
    $importOption = of_get_option(Apollo_DB_Schema::_EVENT_ENABLE_IMPORT_TAB_FOR_ALL_USERS, false);
    if( strpos($GLOBALS['wp']->matched_rule, '^user/import-events') !== false
        && ( Apollo_App::user_can_import_events() || $importOption )
    ) {
        wp_enqueue_script('plupload');
    }

}


function aplLocalizeJs() {

    // Current apollo template type
    $dependKeyApl = 'apollo-global-js-combine';
    wp_localize_script( $dependKeyApl , 'APL', Apollo_App::jsCommunityWithClient());
    wp_localize_script( $dependKeyApl , 'APL_Retina', array('apply' => of_get_option( Apollo_DB_Schema::_ENABLE_RETINA_DISPLAY )) );
}

/**
 * Enqueue CSS
 * @param boolean $isDashboard
 * @param boolean $cssRootPath
 *
 */
function aplEnqueueStyle($isDashboard, $cssRootPath) {

    $globalCss = array(
        'apl-font-style',
        'apl-dev-style', // Please do not change the id here because the Sonoma theme is using it as an extension key
        'apl-reset-style'
    );

    $themeUri = get_stylesheet_directory_uri();

    foreach($globalCss as $hookName) {
        wp_enqueue_style($hookName, apl_mix("/assets/css/$hookName.css", $themeUri), false);
    }


    if ($isDashboard) {
        $hookName = 'apl-dashboard-style';
        wp_enqueue_style($hookName, apl_mix("/assets/css/$hookName.css", $themeUri), false);
    }

    // Enable two row navigation menu
    if(of_get_option(Apollo_DB_Schema::_NAVIGATION_BAR_STYLE) == 'two-rows' || of_get_option(Apollo_DB_Schema::_ENABLE_TWO_ROW_NAVIGATION) ) {
        wp_enqueue_style('apl_two_row_nav_css', $cssRootPath. 'two_row_navigation.css', false, null);
    }

    // Enable two tiers navigation menu
    if(of_get_option(Apollo_DB_Schema::_NAVIGATION_BAR_STYLE) == 'two-tiers') {
        wp_enqueue_style('apl_two_tiers_nav_css', $cssRootPath. 'two_tiers_navigation.css', false, null);
    }

    /** @Ticket #15062 - Override css version */
    $overrideCssVersion = of_get_option(Apollo_DB_Schema::_OVERRIDE_CSS_VERSION, '1.0');
    if ( file_exists(get_primary_color_css_abs_path() ) ) {
        wp_enqueue_style('apollo_primary_color_css', get_primary_color_css_url(), false, $overrideCssVersion);
    }

    // Add override css
    if ( of_get_option( Apollo_DB_Schema::_ACTIVATE_OVERRIDE_CSS ) && file_exists(get_override_css_abs_path() ) ) {

        wp_enqueue_style('apollo_override_css', get_override_css_url(), false, $overrideCssVersion);
    }
}

