<?php
/**
 * Cleaner walker for wp_nav_menu()
 *
 * Walker_Nav_Menu (WordPress default) example output:
 *   <li id="menu-item-8" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8"><a href="/">Home</a></li>
 *   <li id="menu-item-9" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9"><a href="/sample-page/">Sample Page</a></l
 *
 * apollo_Nav_Walker example output:
 *   <li class="menu-home"><a href="/">Home</a></li>
 *   <li class="menu-sample-page"><a href="/sample-page/">Sample Page</a></li>
 */
class apollo_Nav_Walker extends Walker_Nav_Menu {
  function check_current($classes) {
    return preg_match('/(current[-_])|cur|dropdown/', $classes);
  }

  function start_lvl(&$output, $depth = 0, $args = array()) {
    if (of_get_option(Apollo_DB_Schema::_NAVIGATION_BAR_STYLE) == 'two-tiers') {
      if ( ! $depth ) {
        $output .= "\n<ul class=\"\">\n";
      } else {
        $output .= "\n<ul class=\"menu-hover\">\n";
      }
    } else {
      if ( ! $depth ) {
        $output .= "\n<ul class=\"sub-menu level-1\">\n";
      } else {
        $output .= "\n<ul class=\"sub-menu level-2\">\n";
      }
    }
  }

  function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
    $item_html = '';
    parent::start_el($item_html, $item, $depth, $args);
    if (of_get_option(Apollo_DB_Schema::_NAVIGATION_BAR_STYLE) == 'two-tiers'){
      if (!$item->is_dropdown && ($depth === 0)) {
        $item_html .= '<ul class=""><li class=""><a href="'.$item->url.'">'.__($item->title,"apollo").'</a></li></ul>';
      }else if ($item->is_dropdown && ($depth === 1)) {
        $item_html = str_replace('class="', ' data-lvl="level-2" class="has-child ', $item_html);
      }
    }
    if ($item->is_dropdown && ($depth === 0)) {
      $item_html = str_replace('<a', '<a class="dropdown-toggle" data-toggle="dropdown" data-target="#"', $item_html);
      
      $item_html = str_replace('</a>', ' <b class="caret"></b></a>', $item_html);
       $item_html = str_replace('class="', 'data-lvl="level-1" class="has-child ', $item_html); 
      
    } else if ($item->is_dropdown && ($depth === 1)) {
        $item_html = str_replace('class="', ' data-lvl="level-2" class="has-child ', $item_html);
    }
    elseif (stristr($item_html, 'li class="divider')) {
      $item_html = preg_replace('/<a[^>]*>.*?<\/a>/iU', '', $item_html);
    }
    elseif (stristr($item_html, 'li class="dropdown-header')) {
      $item_html = preg_replace('/<a[^>]*>(.*)<\/a>/iU', '$1', $item_html);
    }

    $item_html = apply_filters('apollo/wp_nav_menu_item', $item_html);
    $output .= $item_html;
  }

  function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) {
    $element->is_dropdown = ((!empty($children_elements[$element->ID]) && (($depth + 1) < $max_depth || ($max_depth === 0))));
    
    if ($element->is_dropdown) {
      $element->classes[] = 'dropdown';
    }
  
    parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
  }
}

/**
 * Remove the id="" on nav menu items
 * Return 'menu-slug' for nav menu classes
 */
function apollo_nav_menu_css_class($classes, $item) {
  $id = get_the_ID();
  if($id){
    $idPrimaryCategory = get_metadata('apollo_'.Apollo_DB_Schema::_EVENT_PT,$id,Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID);
  }
  $idItem = !empty($idPrimaryCategory) ? $idPrimaryCategory[0] : 0;
  $slug = sanitize_title($item->title);
  if (of_get_option(Apollo_DB_Schema::_NAVIGATION_BAR_STYLE) == 'two-tiers'){
    $classes = preg_replace('/(current(-menu-|[-_]page[-_])(item|ancestor))/', 'selected', $classes);
    if($item->object_id == $idItem){
      $classes = preg_replace('/(current(-menu-|[-_]page[-_])(item|parent|ancestor))/', 'selected', $classes);
    }
  } else {
    $classes = preg_replace('/(current(-menu-|[-_]page[-_])(item|ancestor))/', 'cur', $classes);
    if($item->object_id == $idItem){
      $classes = preg_replace('/(current(-menu-|[-_]page[-_])(item|parent|ancestor))/', 'cur', $classes);
    }
  }
  $classes = preg_replace('/^((menu|page)[-_\w+]+)+/', '', $classes);

  $classes[] = 'menu-' . $slug;

  $classes = array_unique($classes);

  return array_filter($classes, 'is_element_empty');
}
add_filter('nav_menu_css_class', 'apollo_nav_menu_css_class', 10, 2);
add_filter('nav_menu_item_id', '__return_null');

/**
 * Clean up wp_nav_menu_args
 *
 * Remove the container
 * Use apollo_Nav_Walker() by default
 */
function apollo_nav_menu_args($args = '') {
  $apollo_nav_menu_args['container'] = false;

  if (!$args['items_wrap']) {
    $apollo_nav_menu_args['items_wrap'] = '<ul class="%2$s">%3$s</ul>';
  }
  
  if (!$args['depth']) {
    //$apollo_nav_menu_args['depth'] = 2;
  }

  if (!$args['walker']) {
    $apollo_nav_menu_args['walker'] = new apollo_Nav_Walker();
  }

  return array_merge($args, $apollo_nav_menu_args);
}
add_filter('wp_nav_menu_args', 'apollo_nav_menu_args');
