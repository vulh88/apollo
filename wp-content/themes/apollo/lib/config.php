<?php



define('APOLLO_THEME_NAME', 'apollo');

/**
 * Define Directory Location Constants 
 */
define( 'APOLLO_PARENT_DIR', get_template_directory() );

define( 'APOLLO_INCLUDES_DIR', APOLLO_PARENT_DIR. '/inc' );
define( 'APOLLO_INCLUDES_VERDOR', APOLLO_PARENT_DIR. '/vendor' );
define( 'APOLLO_SRC_DIR', APOLLO_PARENT_DIR. '/inc/src' );
define( 'APOLLO_API_DIR', APOLLO_INCLUDES_DIR. '/Lib/Apis' );
define( 'APOLLO_HELPER_DIR', APOLLO_INCLUDES_DIR. '/Lib/Helpers' );
define( 'APOLLO_ADMIN_DIR', APOLLO_INCLUDES_DIR . '/admin' );
define( 'APOLLO_ADMIN_SRC', APOLLO_INCLUDES_DIR . '/admin/src' );
define( 'APOLLO_WIDGETS_DIR', APOLLO_INCLUDES_DIR . '/widgets' );
define( 'APOLLO_SHORTCODE_DIR', APOLLO_INCLUDES_DIR . '/shortcodes' );
define( 'APOLLO_EMAILS_DIR', APOLLO_INCLUDES_DIR . '/emails' );
define( 'APOLLO_ADMIN_ASSETS_DIR', APOLLO_ADMIN_DIR . '/assets' );
define( 'APOLLO_ADMIN_LIB_DIR', APOLLO_ADMIN_DIR . '/lib' );

define( 'APOLLO_ADMIN_CSS_DIR', APOLLO_ADMIN_ASSETS_DIR . '/css' );
define( 'APOLLO_ADMIN_JS_DIR', APOLLO_ADMIN_ASSETS_DIR . '/js' );
define( 'APOLLO_ADMIN_IMAGES_DIR', APOLLO_ADMIN_ASSETS_DIR . '/images' );
define('APLC', APOLLO_ADMIN_DIR. '/tools/cache');

// ticket #11106: define JS and CSS version
define('APOLLO_CURRENT_REACTIVE_THEME_VERSION', '1.0.0');
define('APOLLO_ADMIN_JS_VERSION', '4.7.4');
define('APOLLO_ADMIN_CSS_VERSION', '4.7.9');
define('APOLLO_ADMIN_SYNDICATION_CSS_VERSION', '1.1');

/**
 * Define URL Location Constants 
 */
define( 'APOLLO_PARENT_URL', get_template_directory_uri() );

define( 'APOLLO_INCLUDES_URL', APOLLO_PARENT_URL. '/inc' );
define( 'APOLLO_SRC_URL', APOLLO_INCLUDES_URL. '/src' );
define( 'APOLLO_ADMIN_URL', APOLLO_INCLUDES_URL. '/admin' );

define( 'APOLLO_ADMIN_ASSETS', APOLLO_ADMIN_URL. '/assets' );
define( 'APOLLO_CRONSECRET', 'LEVhUHmdGeZWXcYZv6IsQpsxaHNszov2FUnQl9DW' );
define( 'APOLLO_ADMIN_CSS_URL', APOLLO_ADMIN_ASSETS . '/css' );
define( 'APOLLO_ADMIN_JS_URL', APOLLO_ADMIN_ASSETS . '/js' );
define( 'APOLLO_ADMIN_IMAGES_URL', APOLLO_ADMIN_ASSETS . '/images' );

/**
 * Default template
 */
define( 'APOLLO_TEMPLATES_DIR', get_stylesheet_directory().'/templates' );
define( 'APOLLO_FRONTEND_ASSETS_URI', get_stylesheet_directory_uri().'/assets' );
define( 'APOLLO_THEME_ACTIVATE_SPOT_TYPE', 'large' );
define( 'APOLLO_THEME_ACTIVATE_OVERRIDE_CSS', 0 );
define( 'APOLLO_THEME_PRIMARY_COLOR', '#fcb234' );
define( 'APOLLO_THEME_HOVERING_SEARCH_CONTENT_COLOR', '#999999' );
define( 'APOLLO_THEME_HEADER', APOLLO_ADMIN_DIR. '/theme-options/default-template/header.php' );
define( 'APOLLO_THEME_404', APOLLO_ADMIN_DIR. '/theme-options/default-template/404.php' );
define( 'APOLLO_THEME_TOP_HEADER', APOLLO_ADMIN_DIR. '/theme-options/default-template/top-header.php' );

define( 'APOLLO_THEME_FOOTER1', APOLLO_ADMIN_DIR. '/theme-options/default-template/footer1.php' );
define( 'APOLLO_THEME_FOOTER2', APOLLO_ADMIN_DIR. '/theme-options/default-template/footer2.php' );
define( 'APOLLO_THEME_FOOTER3', APOLLO_ADMIN_DIR. '/theme-options/default-template/siteinfo.php' );
define( 'APOLLO_THEME_FOOTER4', APOLLO_ADMIN_DIR. '/theme-options/default-template/footer.php' );
define( 'APOLLO_THEME_FOOTER5', APOLLO_ADMIN_DIR. '/theme-options/default-template/footer5.php' );

/* Print Search Event */
define( 'APOLLO_THEME_HEADER_PRINT_SEARCH_EVENT', APOLLO_ADMIN_DIR. '/theme-options/default-template/print-event-search/header.php' );
define( 'APOLLO_THEME_FOOTER_PRINT_SEARCH_EVENT', APOLLO_ADMIN_DIR. '/theme-options/default-template/print-event-search/footer.php' );


define( 'APOLLO_THEME_SUB_CONTAIN_TOP', APOLLO_ADMIN_DIR. '/theme-options/default-template/sub-contain-top.php' );
define( 'APOLLO_THEME_SUB_CONTAIN_BOTTOM', APOLLO_ADMIN_DIR. '/theme-options/default-template/sub-contain-bottom.php' );
define( 'APOLLO_THEME_OVERRIDE_CSS', APOLLO_ADMIN_DIR. '/theme-options/default-template/override.less' );
define( 'APOLLO_DEFAULT_LAYOUT', 'right_sidebar_one_column' );
define( 'APOLLO_DEFAULT_BACKGROUND_COLOR', '#ffffff' );
define( 'DEFAULT_ORDER_START_DATE', 'START_DATE_ASC');
define( 'APOLLO_US_STATES', 'AL, AK, AZ, AR, CA, CO, CT, DE, FL, GA, HI, ID, IL, IN, IA, KS, KY, LA, ME, MD, MA, MI, MN, MS, MO, MT, NE, NV, NH, NJ, NM, NY, NC, ND, OH, OK, OR, PA, RI, SC, SD, TN, TX, UT, VT, VA, WA, WV, WI, WY' );

define( 'APOLLO_REGIONS', '' );

define( 'APOLLO_DEFAULT_EMAIL_TEMPLATES_DIR', APOLLO_ADMIN_DIR. '/theme-options/default-template/email' );

define( 'APOLLO_STATISTIC_BASE_DIR', APOLLO_PARENT_DIR.'/statistics');


/* syndication module handler define */
define('APOLLO_ADMIN_SYNDICATE_DIR', APOLLO_ADMIN_DIR . '/post-types/meta-boxes/syndication/syndicate');
define('APOLLO_ADMIN_SYNDICATE_IFRAME_DIR', APOLLO_ADMIN_SYNDICATE_DIR . '/iframe-views');
define('APOLLO_ADMIN_SYNDICATE_ASSETS_URL', get_template_directory_uri() . '/assets/syndicate');
define('APOLLO_ADMIN_SYNDICATION_DIR', APOLLO_ADMIN_DIR . '/post-types/meta-boxes/syndication');
define('APOLLO_ADMIN_SYNDICATION_TEMPLATE_DIR', APOLLO_ADMIN_SYNDICATION_DIR . '/templates');
define('_APL_SYNDICATED_DATA_HANDLER_FLAG','syndicated_data');

/* iframe search widget module define */
define('_APL_IFRAME_SEARCH_WIDGET_FLAG','iframe_search_widget_form');

/**
 * Author: ThienLD
 * [START] Apollo - Sonoma Child Theme definitions */

define('APL_SM_DIR_THEME_NAME','apollo-child');
define('APL_SM_HOOK_FILTER_CURRENT_ACTIVE_THEME','_apl_get_current_active_theme');

/**
 * [END] Apollo - Sonoma Child Theme definitions */


require_once APOLLO_INCLUDES_DIR. '/class-apollo-const.php';

$GLOBALS[ 'apollo_event_access' ]  = array(
    'volumn'             => __( 'Volume Control Telephone', 'apollo' ),
    'braille'           => __( 'Braille', 'apollo' ),
    'closed-caption'    => __( 'Closed Caption', 'apollo' ),
    'info'              => __( 'Info', 'apollo' ),
    'large-print'       => __( 'Large Print', 'apollo' ),
    'assisted'         => __( 'Assisted Listening System', 'apollo' ),
    'low-vison'         => __( 'Low Vision', 'apollo' ),
    'open-caption'      => __( 'Open Caption', 'apollo' ),
    'sign-lang'         => __( 'Sign Language', 'apollo' ),
    'ttd'               => __( 'TTD/TTY', 'apollo' ),
    'audio'            => __( 'Audio Description', 'apollo' ),
    'wheelchair'        => __( 'Wheelchair Access', 'apollo' ),
);

add_filter('body_class', 'apollo_body_class');

// Apollo modules
$modules = array( 
    'event'         => array( 
                        'sing'          => __( 'Event', 'apollo' ), 
                        'plural'        => __( 'Events', 'apollo' ),
                        'taxonomy_slug' => 'categories', 
                        'taxonomy_name' => __( 'Event Categories', 'apollo' ) ),
    
    'organization'  => array( 'sing' => __( 'Organization', 'apollo' ), 'plural' => __( 'Organizations', 'apollo' ) ), 
    'venue'         => array( 'sing' => __( 'Venue', 'apollo' ), 'plural' => __( 'Venues', 'apollo' ) ), 
    'artist'        => array( 'sing' => __( 'Artist', 'apollo' ), 'plural' => __( 'Artists', 'apollo' ) ), 
    'classified'    => array( 'sing' => __( 'Classified', 'apollo' ), 'plural'  => __( 'Classifieds', 'apollo' ) ), 
    'public-art'    => array( 'sing' => __( 'Public Art', 'apollo' ), 'plural'  => __( 'Public Art', 'apollo' ) ),
    'education'     => array( 'sing' => __( 'Education', 'apollo' ), 'plural'   => __( 'Educations', 'apollo' ), 
            'childs'    => array( 
                'educator'  => array( 'sing' => __( 'Educator', 'apollo' ), 'plural' => __( 'Educators', 'apollo' ) ),
                'program'  => array( 'sing' => __( 'Program', 'apollo' ), 'plural' => __( 'Programs', 'apollo' ) ),
                'edu_evaluation' => array( 'sing' => __( 'Evaluation', 'apollo' ), 'plural' => __( 'Evaluations', 'apollo' ) ),
                'grant_education' => array( 'sing' => __( 'Grant Education', 'apollo' ), 'plural' => __( 'Grant Education', 'apollo' ) ),
            ) ), 
    'business'      => array( 'sing' => __( 'Business', 'apollo' ), 'plural'    => __( 'Businesses', 'apollo' ) ),
    'syndication'      => array(
        'sing' => __( 'Syndication', 'apollo' ),
        'plural'    => __( 'Syndication', 'apollo' ),
        'custom_args' => array(
            'supports' => array( 'title' ),
        )
    ),
    'iframesw'      => array(
        'sing' => __( 'Search Widget', 'apollo' ),
        'plural'    => __( 'Search Widget', 'apollo' ),
        'custom_args' => array(
            'supports' => array( 'title' ),
        )
    ),
    'photo-slider' => array (
        'sing' => __( 'Photo Slider', 'apollo' ),
        'plural'    => __( 'Photo Slider', 'apollo' ),
        'custom_args' => array(
            'supports' => array( 'title', 'editor' ),
        )
    ),
    'news'    => array( 'sing' => __( 'News', 'apollo' ), 'plural'  => __( 'News', 'apollo' ) )
);
$GLOBALS[ 'apollo_modules' ]        = $modules ;


$GLOBALS['apl_email_templates'] = array(
    'confirm_regis_email'     => array(
        'label' => __('Confirmation Email', 'apollo'),
        'data'  => array(
            'subject' => 'Activate your account {site_domain}',
            'heading' => 'Activate your account on {site_domain}',
            'type' => 'html',
        ),
    ),
    'regis'     => array(
        'label' => __('Registration', 'apollo'),
        'data'  => array(
            'subject' => 'Welcome to {site_domain}',
            'heading' => 'New Account Information on {site_domain}',
            'type' => 'html',
        ),
    ),

    'forgo'     => array(
        'label' => __('Forgot Password', 'apollo'),
        'data'  => array(
            'subject' => 'Reset New Password on {site_domain}',
            'heading' => 'Get new password on {site_domain}',
            'type' => 'html',
        ),
    ),

    'grant_education'     => array(
        'label' => __('Grant Education', 'apollo'),
        'data'  => array(
            'subject' => 'Grant Education Information',
            'heading' => 'Grant Education Information',
            'type' => 'html',
        ),
    ),
    /* Thienld : add email template configuration for alert new org and email are created */
    'confirm_new_item_created'     => array(
        'label' => __('Confirm created new post to Administrator', 'apollo'),
        'data'  => array(
            'subject' => 'New post created',
            'heading' => 'New post created',
            'type' => 'html',
        ),
    ),

    /**@Ticket #13638 */
    'confirm_new_item_created_to_owner'     => array(
        'label' => __('Confirm created new post to Owner', 'apollo'),
        'data'  => array(
            'subject' => 'New post created',
            'heading' => 'New post created',
            'type' => 'html',
        ),
    ),
    /* Thienld : end of adding email template configuration for alert new org and email are created */

    // vulh: add email template configuration for notice new event is published
    'published_new_event'     => array(
        'label' => __('Confirm approved new event to user', 'apollo'),
        'data'  => array(
            'subject' => 'Confirm approved new event to user',
            'heading' => 'Confirm approved new event to user',
            'type' => 'html',
        ),
    ),

    'password_change'     => array(
        'label' => __('Notice of Password Change', 'apollo'),
        'data'  => array(
            'subject' => 'Notice of Password Change',
            'heading' => 'Notice of Password Change',
            'type' => 'html',
        ),
    ),

    'confirm_item_edited'     => array(
        'label' => __('Confirm edited post to Administrator', 'apollo'),
        'data'  => array(
            'subject' => 'Notice of Edited Post ',
            'heading' => 'Notice of Edited Post',
            'type' => 'html',
        ),
    ),

    /*
     *@Ticket #13913
     * Author: hieuluong
     * add email template configuration for share with a friend
    */
    'share_post_with_friend'     => array(
        'label' => __('Tell A Friend Email', 'apollo'),
        'data'  => array(
            'subject' => 'Tell A Friend',
            'heading' => 'Tell A Friend',
            'type' => 'html',
        ),
    ),

    /** @Ticket #15850 - Reject email*/
    'confirm_rejected_item_to_owner'     => array(
        'label' => __('Confirm Rejected Event', 'apollo'),
        'data'  => array(
            'subject' => 'Notify user of rejected event',
            'heading' => 'Notify user of rejected event',
            'type' => 'html',
        ),
    ),

    /**
     * @Ticket #18443
     */
    'published_new_program'     => array(
        'label' => __('Arts Ed Program Submission Approval', 'apollo'),
        'data'  => array(
            'subject' => 'Arts Ed Program Submission Approval',
            'heading' => 'Arts Ed Program Submission Approval',
            'type' => 'html',
        )
    ),
);

$GLOBALS['apollo_email_option'] = array(
    'from_name'     => '{site_title}',
    'footer_text'   => __( 'Artsopolis', 'apollo' )
);


/**
 * apollo_theme_wplm
 * 
 */

$arrTOWPLM = array(
    Apollo_DB_Schema::_TEXT_OF_DISCOUNT_OFFER,
    Apollo_DB_Schema::_TEMP_ARTIST_MORE_ABOUT,
    Apollo_DB_Schema::_EDUCATOR_TEACHERS_TITLE,
    Apollo_DB_Schema::_APL_EVENT_BUY_TICKET_TEXT,
    Apollo_DB_Schema::_APL_EVENT_CHECK_DISCOUNTS_TEXT,
    Apollo_DB_Schema::_APL_EVENT_BOOTH_OVERRIDE,
    Apollo_DB_Schema::_APL_EVENT_BOOTH_OVERRIDE_URL,
    Apollo_DB_Schema::_APL_LOAD_MORE_TXT_OFFER_DATESTIME_WIDGET,
    Apollo_Display_Config::EVENT_VIDEO_TEXT,
    Apollo_DB_Schema::EVENT_GALLERY_TEXT,
    Apollo_DB_Schema::_HOME_CAT_FEATURES,
    APL_Theme_Option_Site_Config_SubTab::GLOBAL_SEARCH_TABS,
    Apollo_DB_Schema::_TOPTEN_LABEL_TAB,
    Apollo_DB_Schema::_FEATURED_EVENTS_LABEL,
    APL_Business_Module_Theme_Option::TAB_NAME,
    APL_Business_Module_Theme_Option::INTRODUCTION,
    
    Apollo_DB_Schema::_ES_FROM_NAME,
    Apollo_DB_Schema::_ES_EMAIL_FOOTER_TEXT,

    APL_Theme_Option_Site_Config_SubTab::ARTIST_TYPE_SEARCH_WIDGET_LABEL,
    APL_Theme_Option_Site_Config_SubTab::ARTIST_STYLE_SEARCH_WIDGET_LABEL,
    APL_Theme_Option_Site_Config_SubTab::ARTIST_MEDIUM_SEARCH_WIDGET_LABEL,
);

// Init email translation
global $apl_email_templates;
$emailActionKeys = array_keys($apl_email_templates);
if ($emailActionKeys) {
    foreach ($emailActionKeys as $emailActionKey) :
        $arrTOWPLM[] = $emailActionKey.'_'. Apollo_DB_Schema::_ES_EMAIL_SUBJECT;
        $arrTOWPLM[] = $emailActionKey.'_'. Apollo_DB_Schema::_ES_EMAIL_HEADING;
    endforeach;
}
// End Init email translation

// Init translation for business text
$arrBus = array(
    APL_Business_Module_Theme_Option::ACCOMMODATION_SELECT,
    APL_Business_Module_Theme_Option::BARS_CLUBS_SELECT,
    APL_Business_Module_Theme_Option::DINING_SELECT,
);
foreach ($arrBus as $bus):
    $arrTOWPLM[] = $bus.'_text';
endforeach;

$GLOBALS['apollo_theme_wplm'] = $arrTOWPLM;


/* End apollo_theme_wplm ---------------------------------------------------- */

/**
 * Widgets Area
 */
$GLOBALS['widget_locations'] = array(
    'std' => __("Standard (right column on desktop; below body content on mobile)", 'apollo'),
    'std_cus_pos' => __("Standard custom position 1 (right column on desktop; between spotlight and featured blocks on mobile)", 'apollo'),
    'desktop_only' => __("Desktop only", 'apollo'),
    'mobile_only' => __("Mobile only", 'apollo'),
    'mobile_only_cus_pos' => __("Mobile only custom position 1 (between spotlight and featured blocks)", 'apollo'),
);

$GLOBALS[ 'apollo_widget_modules' ] = array(
    'post'          => array( 'label' => __( 'Blog', 'apollo' )),
    'event'         => array( 'label' => __( 'Event', 'apollo' )),
    'organization'  => array( 'label' => __( 'Organization', 'apollo' )),
    'venue'         => array( 'label' => __( 'Venue', 'apollo' )),
    'artist'        => array( 'label' => __( 'Artist', 'apollo' )),
    'classified'    => array( 'label' => __( 'Classified', 'apollo' )),
    'public-art'    => array( 'label' => __( 'Public Art', 'apollo' )),
    'education'     => array( 'label' => __( 'Education', 'apollo' )),
    'business'      => array( 'label' => __( 'Business', 'apollo' )),
    'post'          => array( 'label' => __( 'Blog', 'apollo' )),
);


/**
 * Filter the body_class
 *
 * Throwing different body class for the different layouts in the body tag
 */
function apollo_body_class( $classes ) {

    //event here
    $class = '';
    if ( get_query_var( '_apollo_event_search' ) === 'true'  ) {
        $class = 'event-landing-page';
    }
    else if ( get_query_var( '_apollo_business_search' ) === 'true'  ) {
        $class = 'business-landing-page';
    }
    else if (get_query_var( '_apollo_educator_search' ) === 'true'){
        $class = 'educator-landing-page';
    }
    else if(get_query_var( '_apollo_program_search' ) === 'true'){
        $class = 'program-landing-page';
    }
    else if(get_query_var('_apollo_page_blog' ) == 'true'
        || get_query_var( '_apollo_author_search' ) === 'true'
        || is_category() || is_author()
        || (is_single() && get_post_type() == Apollo_DB_Schema::_BLOG_POST_PT)
    ){
        $class = 'blog-landing-page';
    }

    // Remove class home on the sub-page
    if ($class) {
        $classes = array_diff($classes, array('home'));
    }

    /**
     * @ticket #18422: 0002522: wpdev54 Customization - Change listing page graphics 'square'.
    */
    $graphicType = of_get_option(Apollo_DB_Schema::_APL_GRAPHIC_TYPE, '');
    if ($graphicType) $classes[] = $graphicType;

    if ($class) $classes[] = $class;

    return $classes;
}

/**
 * Define which pages shouldn't have the sidebar
 *
 * See lib/sidebar.php for more details
 */
function apollo_display_sidebar() {
  $sidebar_config = new apollo_Sidebar(
    /**
     * Conditional tag checks (http://codex.wordpress.org/Conditional_Tags)
     * Any of these conditional tags that return true won't show the sidebar
     *
     * To use a function that accepts arguments, use the following format:
     *
     * array('function_name', array('arg1', 'arg2'))
     *
     * The second element must be an array even if there's only 1 argument.
     */
    array(
      'is_404',
      'is_front_page'
    ),
    /**
     * Page template checks (via is_page_template())
     * Any of these page templates that return true won't show the sidebar
     */
    array(
     
    )
  );

  return apply_filters('apollo/display_sidebar', $sidebar_config->display);
}

/**
 * $content_width is a global variable used by WordPress for max image upload sizes
 * and media embeds (in pixels).
 *
 * Example: If the content area is 640px wide, set $content_width = 620; so images and videos will not overflow.
 * Default: 1140px is the default Bootstrap container width.
 */
if (!isset($content_width)) { $content_width = 1140; }

/**
 * Override upload dir
 */
add_filter( 'upload_dir', 'change_upload_dir' );
function change_upload_dir( $upload ) {
    //TriLM-custom upload folder
    $uploadPathCustom = Class_Upload_Path_Custom::getConfig();

    $dir = Class_Upload_Path_Custom::getUploadDir($uploadPathCustom);
    $url = Class_Upload_Path_Custom::getUploadURL($uploadPathCustom);
    
    $subdir = '';
	if ( get_option( 'uploads_use_yearmonth_folders' ) ) {
		
        // Generate the yearly and monthly dirs
        $time = current_time( 'mysql' );
		$y = substr( $time, 0, 4 );
		$m = substr( $time, 5, 2 );
		$subdir = "/$y/$m";
	}
    
    $_upload = array( 
        'path'      => $dir. '/images'. $subdir,
        'url'       => $url. '/images'.$subdir,
        'subdir'    => $subdir,
        'basedir'   => $dir,
        'baseurl'   => $url,
        'error'     => $upload['error'],
        'css_dir'   => $dir. '/css',
        'css_url'   => $url. '/css',
        'html_url'  => $url. '/template',
        'syndication_dir'  => $dir. '/'. Class_Upload_Path_Custom::UPLOAD_SYNDICATION_DIR,
        'syndication_url'  => $url. '/'. Class_Upload_Path_Custom::UPLOAD_SYNDICATION_DIR,
        'html_dir'  => $dir. '/template',
        'event_import_url'  => $url. '/event-import',
        'event_import_dir'  => $dir. '/event-import',
        'logs_dir'  => $dir. '/logs',
        'cache'     => $dir. '/cache',
        'cache_event'     => $dir. '/cache/event',
        'cache_home'     => $dir. '/cache/home',
        'cache_artist_syndication'     => $dir. '/cache/syndication/artist',
    );
    wp_mkdir_p( $_upload['path'] );
    wp_mkdir_p( $_upload['css_dir'] );
    wp_mkdir_p( $_upload['html_dir'] );
    wp_mkdir_p( $_upload['event_import_dir'] );
    wp_mkdir_p( $_upload['logs_dir'] );
    wp_mkdir_p( $_upload['cache'] );
    wp_mkdir_p( $_upload['cache_event'] );
    wp_mkdir_p( $_upload['cache_home'] );
    wp_mkdir_p( $_upload['syndication_dir'] );
    wp_mkdir_p( $_upload['cache_artist_syndication'] );
    
    return $_upload;
}

 if ( ! function_exists( 'apollo_blog_paging_nav' ) ) :
    /**
     * Display navigation to next/previous set of posts when applicable.
     *
     * @global WP_Query   $wp_query   WordPress Query object.
     * @global WP_Rewrite $wp_rewrite WordPress Rewrite object.
     */
    function apollo_blog_paging_nav() {
        global $wp_query, $wp_rewrite;

        // Don't print empty markup if there's only one page.
        if ( $wp_query->max_num_pages < 2 ) {
            return;
        }

        $paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
        $pagenum_link = html_entity_decode( get_pagenum_link() );
        $query_args   = array();
        $url_parts    = explode( '?', $pagenum_link );

        if ( isset( $url_parts[1] ) ) {
            wp_parse_str( $url_parts[1], $query_args );
        }

        $pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
        $pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

        $format  = $wp_rewrite->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
        $format .= $wp_rewrite->using_permalinks() ? user_trailingslashit( $wp_rewrite->pagination_base . '/%#%', 'paged' ) : '?paged=%#%';

        // Set up paginated links.
        $links = paginate_links( array(
            'base'      => $pagenum_link,
            'format'    => $format,
            'total'     => $wp_query->max_num_pages,
            'current'   => $paged,
            'mid_size'  => 3,
            'type'      => 'list',
            'add_args'  => array_map( 'urlencode', $query_args ),
            'prev_text' => '<',
            'next_text' => '>',
        ) );

        if ( $links ) :

            ?>
            <nav class="paging" role="navigation">
                    <?php echo $links; ?>
            </nav><!-- .navigation -->
            <?php
        endif;
    }
endif;
