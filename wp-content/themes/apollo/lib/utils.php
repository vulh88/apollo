<?php

add_action( 'post_edit_form_tag' , 'post_edit_form_tag' );

function post_edit_form_tag( ) {
    echo ' enctype="multipart/form-data"';
}

/**
 * Utility functions
 */
function is_element_empty($element) {
    $element = trim($element);
    return !empty($element);
}

// Tell WordPress to use searchform.php from the templates/ directory
function apollo_get_search_form($form) {
    $form = '';
    locate_template('/templates/searchform.php', true, false);
    return $form;
}
add_filter('get_search_form', 'apollo_get_search_form');

function apollo_get_current_user_role() {
    global $wp_roles;
    $current_user = wp_get_current_user();
    $roles = $current_user->roles;
    $role = array_shift($roles);
    return isset($wp_roles->role_names[$role]) ? translate_user_role($wp_roles->role_names[$role] ) : false;
}

/**
 * Returns instance of singleton class
 *
 * @param string $class
 * @param boolean $params
 * @param bool $renew
 * @return object
 */
function aplc_instance($class, $params = false, $renew = false) {
    static $instances = array();

    if (!isset($instances[$class]) || $renew) {

        require_once(APLC. str_replace('_', '/', substr($class, 4)). '.php');

        if (!empty($params)) {
            $instances[$class] = new $class($params);
        }
        else {
            $instances[$class] = new $class();
        }
    }

    $v = $instances[$class];   // Don't return reference
    return $v;
}

/**
 * Requires and keeps track of which files has already been loaded.
 *
 * @param string $path Absolute path to the file
 */
function aplc_require_once($path) {
    static $files = array();

    if (!isset($files[$path])) {
        $files[$path] = 1;
        require_once $path;
    }
}

/**
 * Get array value by key
 * @param $key
 * @param array $array
 * @param null $default
 * @return null
 * @author ThienLD
 */
function getArrayValueByKey($key, $array = array(), $default = null)
{
    if (is_array($array) && array_key_exists($key, $array)) {
        return $array[$key];
    }
    return $default;
}


/**
 * Adds support for a theme option.
 */
if ( !function_exists( 'optionsframework_init' ) ) {
	define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/admin/assets/' );
    
	require_once( APOLLO_ADMIN_DIR. '/theme-options/options-framework.php' );
}

require_once APOLLO_INCLUDES_DIR.'/class-apollo-logs.php';
require_once APOLLO_INCLUDES_DIR.'/class-upload-config.php';

require_once APOLLO_INCLUDES_DIR. '/class-apollo-messages.php';

/** @Ticket #13029 */
/** @Ticket #13364 */
require_once  APOLLO_INCLUDES_DIR. '/src/artist/inc/APL_Artist_Function.php';

/** @Ticket #15217 */
require_once  APOLLO_INCLUDES_DIR. '/src/event/inc/AplEventFunction.php';

/* ALL GLOBAL CONFIG ABOUT APOLLO LOAD HERE */
require_once APOLLO_INCLUDES_DIR. '/Lib/Association/ApolloAssociationFunction.php';

require_once APOLLO_INCLUDES_DIR. '/class-apollo-app-function.php';

/** @Ticket #17838 */
require_once APOLLO_INCLUDES_DIR. '/src/business/inc/APL_Business_Function.php';

/** @Ticket #13983 */
require_once APOLLO_INCLUDES_DIR . '/Lib/Territory/APL_Lib_Territory_Neighborhood.php';

require_once APOLLO_INCLUDES_DIR. '/class-apollo-next-prev.php';
require_once APOLLO_INCLUDES_DIR. '/class-apollo-search-form-data.php';
require_once APOLLO_INCLUDES_DIR. '/class-apollo-custom-field.php';

/* EVENT THEME TOOL CLASS */ 
require_once APOLLO_INCLUDES_DIR. '/class-apollo-theme-tool.php';

/* create autoload class */
spl_autoload_register(array('Apollo_App', 'autload'), true, true);

/* VIDEO */
require_once APOLLO_INCLUDES_DIR. '/class-apollo-video.php';

/* QUERY */
require_once APOLLO_INCLUDES_DIR. '/class-apollo-query.php';

/* WIDGETS */
require_once APOLLO_INCLUDES_DIR. '/apollo-widgets-function.php';

/* SHORTCODES */
require_once APOLLO_INCLUDES_DIR. '/apollo-shortcode-function.php';

/* POST TYPE */
require_once APOLLO_INCLUDES_DIR. '/class-apollo-post-types.php';
require_once APOLLO_INCLUDES_DIR. '/module-classes/class-apollo-module.php';
require_once APOLLO_INCLUDES_DIR. '/module-classes/class-apollo-event.php';
require_once APOLLO_INCLUDES_DIR. '/module-classes/class-apollo-grant-education.php';
require_once APOLLO_INCLUDES_DIR. '/module-classes/class-apollo-artist.php';
require_once APOLLO_INCLUDES_DIR. '/module-classes/class-apollo-org.php';
require_once APOLLO_INCLUDES_DIR. '/module-classes/class-apollo-venue.php';
require_once APOLLO_INCLUDES_DIR. '/module-classes/class-apollo-educator.php';
require_once APOLLO_INCLUDES_DIR. '/module-classes/class-apollo-program.php';
require_once APOLLO_INCLUDES_DIR. '/module-classes/class-apollo-business.php';
require_once APOLLO_INCLUDES_DIR. '/module-classes/class-apollo-edu-evaluation.php';
require_once APOLLO_INCLUDES_DIR. '/module-classes/class-apollo-classified.php';
require_once APOLLO_INCLUDES_DIR. '/module-classes/class-apollo-public-art.php';
require_once APOLLO_INCLUDES_DIR. '/module-classes/class-apollo-holder-event.php';
require_once APOLLO_INCLUDES_DIR. '/module-classes/class-apollo-solr-search.php';
require_once APOLLO_INCLUDES_DIR. '/module-classes/class-apollo-iframe-search-widget.php';
require_once APOLLO_INCLUDES_DIR. '/apollo-login-hook.php';

/* Apollo Custom RSS Feed */
require_once APOLLO_INCLUDES_DIR. '/feeds/apollo-rss-feed.php';


/* FORM */ 
require_once APOLLO_INCLUDES_DIR. '/submit-data/class-apollo-submit-form.php';
require_once APOLLO_INCLUDES_DIR. '/submit-data/class-apollo-submit-edu-evaluation.php';
require_once APOLLO_INCLUDES_DIR. '/submit-data/class-apollo-submit-grant-education.php';
/** @Ticket #13482 */
require_once APOLLO_INCLUDES_DIR. '/src/educator/inc/class-apollo-submit-educator.php';
require_once APOLLO_INCLUDES_DIR. '/src/program/inc/class-apollo-submit-program.php';
/** @Ticket #13364 */
require_once APOLLO_INCLUDES_DIR. '/src/artist/inc/class-apollo-submit-artist.php';

require_once APOLLO_INCLUDES_DIR. '/apollo-event-function.php';
require_once APOLLO_INCLUDES_DIR. '/apollo-module-function.php';
require_once APOLLO_INCLUDES_DIR. '/apollo-term-function.php';
require_once APOLLO_INCLUDES_DIR. '/apollo-post-type-function.php';

if (is_admin() ) {
    require_once APOLLO_ADMIN_DIR .'/class-apollo-admin.php';
}

/* SOCIAL FUNCTIONS */
require_once APOLLO_INCLUDES_DIR. '/class-apollo-social-factory.php';

/* ALL HOOK OF SINGLE EVENTS */
require_once APOLLO_INCLUDES_DIR. '/apollo-single-hooks.php';


require_once APOLLO_INCLUDES_DIR. '/class-apollo-filter.php';

require_once APOLLO_INCLUDES_DIR. '/class-apollo-action.php';

require_once APOLLO_INCLUDES_DIR. '/apollo-emails-function.php';

require_once APOLLO_INCLUDES_DIR. '/class-apollo-captcha.php';

require_once APOLLO_INCLUDES_DIR. '/class-apollo-user.php';

require_once APOLLO_INCLUDES_DIR. '/class-apollo-rewrite.php';

require_once APOLLO_INCLUDES_DIR. '/class-apollo-permission.php';

require_once APOLLO_INCLUDES_DIR. '/class-apollo-creator.php';

/* ALL UTIL FUNCTIONS - PUT HERE */
require_once APOLLO_INCLUDES_DIR. '/class-apollo-global-util.php';

require_once APOLLO_INCLUDES_DIR. '/class-apollo-blog.php';

require_once APOLLO_INCLUDES_DIR. '/apollo-rules.php';


require_once APOLLO_INCLUDES_DIR. '/submit-data/apollo-form/apollo-form-inc.php';
require_once APOLLO_INCLUDES_DIR. '/submit-data/class-apollo-submit-form.php';
/** @Ticket #13470 */
require_once APOLLO_INCLUDES_DIR. '/src/org/inc/class-apollo-submit-organization.php';
require_once APOLLO_INCLUDES_DIR. '/src/org/inc/class-apollo-validation-organization.php';
/** @Ticket #13448 */
require_once APOLLO_INCLUDES_DIR. '/src/venue/inc/class-apollo-submit-venue.php';
require_once APOLLO_INCLUDES_DIR. '/submit-data/class-apollo-submit-classified.php';
//require_once APOLLO_INCLUDES_DIR. '/apollo-reset-option.php';

/**
 * ThienLD: include the main module file to initialize the modules within /apollo/inc/src/{modules}
*/
// ticket #15085 - module Page Builder with the new architecture
require_once APOLLO_SRC_DIR. '/page-builder/page-builder.php';

require_once APOLLO_SRC_DIR . '/event/event.php';
require_once APOLLO_SRC_DIR . '/report/report.php';

/* Apl Cross Domain */
require_once APOLLO_API_DIR . '/Inc/CrossDomain/AplCrossDomain.php';

/*@ticket #18331 0002504: Arts Education Customizations - Displaying alternate masthead, nav menu, right column and footer for a list of defined pages/modules*/
require_once APOLLO_SRC_DIR . '/common/generate-html-content/GenerateStaticHTMLFactory.php';

/**
 * Wordpress translation date format
 */

function aplJsTranslate() {
    $data = array(
        "Jan"   =>  __("Jan", 'apollo'),
        "Feb"   => __("Feb", 'apollo'),
        "Mar"   => __("Mar", 'apollo'),
        "Apr"   => __("Apr", 'apollo'),
        "May"   => __("May", 'apollo'),
        "Jun"   => __("Jun", 'apollo'),
        "Jul"   => __("Jul", 'apollo'),
        "Aug"   => __("Aug", 'apollo'),
        "Sep"   => __("Sep", 'apollo'),
        "Oct"   => __("Oct", 'apollo'),
        "Nov"   => __("Nov", 'apollo'),
        "Dec"   => __("Dec", 'apollo')
    );
    return array_combine(array_keys($data), array_values($data));
}

if ( Apollo_App::hasWPLM() ) {
    
    function translate_date_format($format) {
        if (function_exists('icl_translate') && function_exists('icl_register_string')) {
            icl_register_string('Formats', 'Date Format', $format);
            $format = icl_translate('Formats', 'Date Format', $format);
        }
        return $format;
    }
    add_filter('option_date_format', 'translate_date_format');

    if (function_exists('icl_register_string')) {
        icl_register_string('Formats', 'MONTH', 'M');
        icl_register_string('Formats', 'month', 'F');
    }
}

if(file_exists(APOLLO_INCLUDES_DIR. '/admin/tools/cache/Inc/event-category-caching.php')){
    require_once APOLLO_INCLUDES_DIR. '/admin/tools/cache/Inc/event-category-caching.php';
}


if (!function_exists('apl_instance')):
    /**
     * Returns instance of singleton class
     *
     * @param string $class
     * @return object
     */
    function apl_instance($class, $args = array(), $rootPath = APOLLO_INCLUDES_DIR) {
        static $apl_instances = array();

        if (!isset($apl_instances[$class]) || count($args)) {

            apl_require_once($rootPath. '/'. str_replace('_', '/', substr($class, 4)) .'.php');

            $apl_instances[$class] = new $class($args);
        }

        $v = $apl_instances[$class];   // Don't return reference
        return $v;
    }
endif;


if(!function_exists('apl_require_once')):
    /**
     * Requires and keeps track of which files has already been loaded.
     *
     * @param string $path Absolute path to the file
     */
    function apl_require_once($path) {
        static $apl_files = array();

        if (!isset($apl_files[$path])) {
            $apl_files[$path] = 1;
            require_once $path;
        }
    }
endif;