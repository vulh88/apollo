<!doctype html>
<!--[if IE 8]><html lang="en" class="no-js ie ie8"><![endif]-->
<!--[if IE 9]><html lang="en" class="no-js ie ie9"><![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>>
<!--<![endif]-->
<?php get_template_part('templates/head'); ?>
<body <?php body_class('apollo'); ?>>
<?php
    echo Apollo_App::renderScriptInBodyTag();

    // Add override css
    if ( of_get_option( Apollo_DB_Schema::_ACTIVATE_OVERRIDE_CSS ) && file_exists(get_override_css_abs_path() ) ) {
        $primaryCss = get_override_css_url();
    } else {
        $primaryCss = '';
    }

    // Text Translation
    Apollo_App::wplmRenderCSSTextTrans();

?>
<script>
    var cb = function() {
        var temUrl = '<?php echo get_template_directory_uri() ?>',
            jsArr = [
                temUrl+ '/assets/css/jquery-ui-calendar.css',
                'https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,800italic,400,300,700,800',
            ];

        var total = jsArr.length;

        for( var i = 0; i < total; i++ ) {
            var l       = document.createElement('link');
                l.rel   = 'stylesheet';

            l.href = jsArr[i];
            var h = document.getElementsByTagName('head')[0]; h.parentNode.insertBefore(l, h);
        }
    };

    window.addEventListener('load', cb);
</script>

<?php
require_once __DIR__.'/templates/social/fbjs-embed.php';
require_once __DIR__.'/templates/social/twjs-embed.php';
//require_once __DIR__.'/templates/social/ptjs-embed.php';
require_once __DIR__.'/templates/social/gpjs-embed.php';
?>

  <!--[if lt IE 8]>
    <div class="alert alert-warning">
      <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'apollo'); ?>
    </div>
  <![endif]-->

  <?php
    do_action('get_header');
    get_template_part('header');

//    get_template_part('templates/slider');
    /** @Ticket #13552 */
    echo do_shortcode('[apollo_home_spotlight]');

    // load business type slider (ticket #11188)
    get_template_part('templates/business/slider');

    // load event type slider (ticket #13573)
    get_template_part('templates/events/slider');

    $is_group_dashboard = false;
    $is_one_column = false;
    if(Apollo_App::isDashboardPage()) {
        $is_group_dashboard = true;
    }
    else if(get_query_var('_rw_is_one_column') === 'true') {
        $is_one_column = true;
    }


    global $post;
    $pageTemplate = get_page_template_slug($post);

    if (get_query_var('_apollo_public_art_map') ) {
        include APOLLO_TEMPLATES_DIR .'/public-art/map-search.php';
    }
    else {
    ?>
  <section class="main <?php echo Apollo_App::isFullTemplatePage($pageTemplate) ? 'apl-full-width-page' : '' ?>">
      <div class="inner">
          <?php if (! $isNotFound = is_404()) {
            ?>

              <?php
              if($maintenanceMode = Apollo_App::forcePageMaintenanceMode()) :
                    include APOLLO_TEMPLATES_DIR .'/pages/maintenance-mode.php';
              else :
                if (Apollo_App::is_homepage()) :
                    the_widget('Apollo_Home_Blog_Feature', array(), array(
                        'position' => 'top',
                        'type'  => 'full',
                    ));
                endif;
               ?>
                  <div class="row <?php echo $is_group_dashboard ? '' : 'two-col' ?>">
                      <?php if($is_group_dashboard): ?>
                          <?php include apollo_sidebar_dashboard_path(); ?>

                          <div class="dsb-r">
                              <div class="dsb-ct">
                              <?php
                                  $handeCode = Apollo_App::getInfoADCurrentHandle();

                                  if(!empty($handeCode) && $handeCode['is_static'] === true) {
                                      include $handeCode['src'];
                                  }
                                  else {
                                      include apollo_template_path();
                                  }
                              ?>
                              </div>
                          </div>
                      <?php elseif($is_one_column): ?>
                          <?php include apollo_template_path(); ?>
                      <?php else:
                            $homeLayOut = of_get_option(Apollo_DB_Schema::_DEFAULT_HOME_LAYOUT);

                            // Only apply fullwidth 960 for home page
                            if ( Apollo_App::is_homepage() && $homeLayOut == 'full_960'   ) {
                                ?>
                                <!-- start left side -->
                                <div class="wc-f">
                                    <?php include apollo_template_path(); ?>
                                </div> <!-- left side -->

                                <?php
                            } else {

                                if ( !Apollo_App::isFullTemplatePage($pageTemplate) ) :
                                ?>
                                <div class="v-line"> </div>
                                <?php endif; ?>

                                <!-- start left side -->
                                <div class="wc-l">
                                    <?php
                                        if (Apollo_App::is_homepage()) :
                                            the_widget('Apollo_Home_Blog_Feature', array(), array(
                                                'position' => 'top',
                                                'type'  => 'left',
                                            ));
                                        endif;
                                        include apollo_template_path();

                                        if (Apollo_App::is_homepage()) :
                                            the_widget('Apollo_Home_Blog_Feature', array(), array(
                                                'position' => 'bottom',
                                                'type'  => 'left',
                                            ));
                                        endif;
                                    ?>
                                </div> <!-- left side -->

                                <!-- start right side -->
                                <?php
                                if ( !Apollo_App::isFullTemplatePage($pageTemplate) ) :
                                ?>
                                <div class="wc-r">
                                    <?php
                                    include apollo_sidebar_path();
                                    ?>
                                </div> <!-- right side -->
                                <?php
                                endif;
                            }
                            endif; ?>

                  </div>
              <?php endif; ?>
          <?php
             }
              else {
                  include apollo_template_path();
              }
          ?>

          </div>
      </div>
  </section> <!-- end main content -->


    <?php if (Apollo_App::is_homepage() && isset($isNotFound) && !$isNotFound): ?>

        <?php
            if ( $featuredBlogTopContent =  Apollo_App::get_static_html_content( Apollo_DB_Schema::_FEATURED_BLOG_TOP_CONTENT) ):
                echo $featuredBlogTopContent;
            endif;
        ?>

        <section class="main <?php echo Apollo_App::isFullTemplatePage($pageTemplate) ? 'apl-full-width-page' : '' ?> apl_bottom_home_blog_feature_widget">
            <div class="inner">
                <?php
                    the_widget('Apollo_Home_Blog_Feature', array(), array(
                        'position' => 'bottom',
                        'type'  => 'full',
                    ));
                ?>
            </div>
        </section>

    <?php endif; ?>

<?php } ?>

  <?php get_template_part('footer'); ?>

</body>
</html>