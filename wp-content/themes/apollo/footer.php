
<?php
    $generateHtmlContent = new GenerateStaticHTMLFactory();
    echo $generateHtmlContent->generateHtml( Apollo_DB_Schema::_FOOTER1 );
    echo $generateHtmlContent->generateHtml(Apollo_DB_Schema::_FOOTER2 );
    echo $generateHtmlContent->generateHtml( Apollo_DB_Schema::_FOOTER3 ); // New Footer 3
    echo Apollo_App::get_static_html_content( Apollo_DB_Schema::_FOOTER4 ); // Old footer
    echo Apollo_App::get_static_html_content( Apollo_DB_Schema::_FOOTER5 );
    
    wp_footer(); 
?>
