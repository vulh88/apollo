<?php

/**
 * apollo includes
 *
 * The $apollo_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 */
$apollo_includes = array(
    'lib/config.php',          // Configuration
    'lib/utils.php',           // Utility functions
    'lib/init.php',            // Initial theme setup and constants
    'lib/wrapper.php',         // Theme wrapper class
    'lib/sidebar.php',         // Sidebar class
    'lib/activation.php',      // Theme activation
    'lib/titles.php',          // Page titles
    'lib/nav.php',             // Custom nav modifications
    'lib/gallery.php',         // Custom [gallery] modifications
    'lib/comments.php',        // Custom comments modifications
    'lib/extras.php',          // Custom functions
    'lib/languages.php',       // Language
    'lib/retina.php',          // Support Retina
    'lib/scripts.php',      // Scripts and stylesheets,
);


foreach ($apollo_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'apollo'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

function apollo_wp($obj) {
    // override 404 mechanism
    global $wp_query;

    if(
        strpos($GLOBALS['wp']->matched_rule, '^artist/page') !== false
        || strpos($GLOBALS['wp']->matched_rule, '^event/page') !== false
        || strpos($GLOBALS['wp']->matched_rule, '^educator/page') !== false
        || strpos($GLOBALS['wp']->matched_rule, '^program/page') !== false
        || strpos($GLOBALS['wp']->matched_rule, '^organization/page') !== false
        || strpos($GLOBALS['wp']->matched_rule, '^venue/page') !== false
        || strpos($GLOBALS['wp']->matched_rule, '^classified/page') !== false
        || strpos($GLOBALS['wp']->matched_rule, '^public-art/page') !== false
    ) {
        $wp_query->is_404 = false;
    }


    if (is_page() ) {
        global $post;

        // Submit teacher evaluation
        if ( $post && $post->post_name == Apollo_Page_Creator::ID_SUBMIT_EVALUATION_DONE ) {
            if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EDUCATION ) || ! isset($_SESSION['apollo_teacher_eval']) ) {
                wp_safe_redirect('/');
            } else {
                unset($_SESSION['apollo_teacher_eval']);
            }
        }
    }

    /**
     * Replace content of user dashboard
     */

    if ( strpos($GLOBALS['wp']->matched_rule, '^user/dashboard') !== false ):
        add_filter('the_content', function($content) {
            $user = wp_get_current_user();

            $create_event_btn = Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EVENT_PT ) ? '' : 'style="display:none;"';
            $content = str_replace('%create_event_btn%', $create_event_btn, $content);

            return str_replace(array(
                    '{username}',
                    '{create_event_btn}',

                ), array(
                    $user->user_nicename,
                    $create_event_btn,

            ), $content);
        });
    endif;

    $locationOrgPhotoForm = of_get_option(Apollo_DB_Schema::_ORGANIZATION_PHOTO_FORM_LOCATION, 1);
    if(strpos($GLOBALS['wp']->matched_rule, '^user/org/photos') !== false && $locationOrgPhotoForm != 1):
        $wp_query->is_404 = true;
    endif;

    /**
     * @ticket #18695: Add the option for image upload to the main artist profile form.
     */
    $locationArtistPhotoForm = of_get_option(APL_Theme_Option_Site_Config_SubTab::_ARTIST_PHOTO_FORM_LOCATION, 1);
    if(strpos($GLOBALS['wp']->matched_rule, '^user/artist/photos') !== false && $locationArtistPhotoForm != 1):
        $wp_query->is_404 = true;
    endif;
    // check current user can use import event tool
    /** Vandd - @Ticket #12435 */
    $importOption = of_get_option(Apollo_DB_Schema::_EVENT_ENABLE_IMPORT_TAB_FOR_ALL_USERS, false);
    if( strpos($GLOBALS['wp']->matched_rule, '^user/import-events') !== false
        && !Apollo_App::user_can_import_events()
        && !$importOption
    ):
        $wp_query->is_404 = true;
    endif;

    if ( ! is_admin() ) {
        // Thienld : hook filter admin_url to integrate with WPML - Multilinguage Plugin
        add_filter( 'admin_url', 'Apollo_App::handleAdminUrlIntegrateWPML',90,3);
    }
}

add_action('wp', 'apollo_wp', 10, 2);

/**
 * Some cases plugin remove the rewrite rules, new rules will be lost
 * we need to reset the rewrite rule
 */
add_action( 'wp_loaded', function() {
    $rules = get_option('rewrite_rules');
    if ( ! isset($rules['^user/dashboard/?$']) && !is_admin() ) {
        global $wp_rewrite;
        $wp_rewrite->init();
        $wp_rewrite->flush_rules();
    }

    /**
     * Thienld : hook wp_loaded to handle logic syndicate export data with multiple export types
     */
    if(Apollo_App::is_avaiable_module( Apollo_DB_Schema::_SYNDICATION_PT) && isset( $_GET['syndicated_data'] )){
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/syndication/syndicate/class-apollo-syndicate.php';
    }

    /**
     * Thienld : hook wp_loaded to handle logic of feature print search event results
     */
    if(Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EVENT_PT) && isset($_GET['print_event_search_result'])){
        require_once APOLLO_INCLUDES_DIR. '/src/event/inc/class-event-search.php';
        $search_obj = new Apollo_Event_Page(true);
        $search_obj->printSearchEventResult();
    }

    /**
     * Thienld : hook wp_loaded to render IFrame search widget
     */
    if(Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EVENT_PT)
        && Apollo_App::is_avaiable_module( Apollo_DB_Schema::_IFRAME_SEARCH_WIDGET_PT)
        && isset($_GET[_APL_IFRAME_SEARCH_WIDGET_FLAG])
        && (isset($_GET['ifswid']) && intval($_GET['ifswid']) > 0)
    ){
        wp_head();
        echo Apollo_App::aplGetTemplatePart(array(
            'file' => APOLLO_TEMPLATES_DIR. '/search/templates/iframe-search-widget.php',
            'template_args' => array('ifswid' => intval($_GET['ifswid'])),
            'cached' => true,
            'file_to_class_cache_handler' => APOLLO_ADMIN_DIR. '/tools/cache/Inc/search-widget-cache-class.php',
            'class_name_cache_handler' => 'APLC_Search_Widget_Cache_Class'
        ));
        wp_footer();
        exit;
    }

});



/**
 * Allow user preview the pending post, not apply for administrator
 */
if (isset($_GET['post_type']) && isset($_GET['p'])) {
    add_filter('the_posts', 'show_future_posts');
}

function show_future_posts($posts) {
    global $wp_query, $wpdb, $current_user;
    $roles = ['administrator', 'editor', 'apl_account_manager'];
    $user_roles = $current_user->roles;
    if(is_single() && $wp_query->post_count == 0 ) {
        $posts = $wpdb->get_results($wp_query->request);

        if ( $posts && $posts[0]->post_author != get_current_user_id()
            && $posts[0]->post_status != 'publish' && ! ( $roles && isset($user_roles[0]) && in_array($user_roles[0], $roles) ) ) {

            /* If the current user was set a pending review from admin */
            if ( Apollo_User::isAssociatedItems($posts[0]->ID, $posts[0]->post_type) ) {
                return $posts;
            }

            return false;
        }
    }

    return $posts;
}

/* Truonghn #6614 - custom email template when change password in admin, network */
function custom_email_password_change($is_send, $original_user = array(), $user_data = array()){

    $user = new WP_User($original_user['ID']);
    do_action('apollo_email_password_change',$user->display_name, $user->user_email, get_bloginfo('admin_email'));
}
add_filter( 'send_password_change_email', 'custom_email_password_change',1,3);
/*  end  custom custom email template when change password in admin, network*/

/* Truonghn - Change the title of "search results" page */
function wpdocs_filter_wp_title( $title, $sep ) {

    $resultTitle = __('Search Results | ','apollo');

    if(get_query_var('_apollo_artist_search') === 'true') {
        $title = isset($_GET['keyword']) ? $resultTitle : Apollo_App::getCustomLabelByModuleName(Apollo_DB_Schema::_ARTIST_PT) . " | ";
        $title .= get_bloginfo('title');
    }
    if(get_query_var('_apollo_educator_search') === 'true') {
        $title = isset($_GET['keyword']) ? $resultTitle : __('Educators | ','apollo');
        $title .= get_bloginfo('title');
    }
    if(get_query_var('_apollo_event_search') === 'true') {

        $title = isset($_GET['keyword']) ? $resultTitle : __('Events | ','apollo');
        $title .= get_bloginfo('title');
    }
    if( get_query_var( '_apollo_program_search' ) === 'true' ) {
        $title = isset($_GET['keyword']) ? $resultTitle : __('Programs | ','apollo');
        $title .= get_bloginfo('title');
    }
    if( get_query_var( '_apollo_org_search' ) === 'true' ) {
        $title = isset($_GET['keyword']) ? $resultTitle : __('Organizations | ','apollo');
        $title .= get_bloginfo('title');
    }
    if( get_query_var( '_apollo_venue_search' ) === 'true' ) {
        $title = isset($_GET['keyword']) ? $resultTitle : __('Venue | ','apollo');
        $title .= get_bloginfo('title');
    }
    if( get_query_var( '_apollo_classified_search' ) === 'true' ) {
        /**
         * ThienLD : handle custom classified slug in ticket #12697
         */
        $title = isset($_GET['keyword']) ? $resultTitle : Apollo_App::getCustomLabelByModuleName(Apollo_DB_Schema::_CLASSIFIED) . " | ";
        $title .= get_bloginfo('title');
    }
    if( get_query_var( '_apollo_public_art_search' ) === 'true' ) {
        $title = isset($_GET['keyword']) ? $resultTitle : __('Public Art | ','apollo');
        $title .= get_bloginfo('title');
    }
    if( get_query_var( '_apollo_page_blog' ) === 'true' ) {
        $title = isset($_GET['keyword']) ? $resultTitle : Apollo_App::getCustomLabelByModuleName(Apollo_DB_Schema::_BLOG_POST_PT) . " | ";
        $title .= get_bloginfo('title');
    }

    if( get_query_var( '_apollo_business_search' ) === 'true' ) {
        $title = isset($_GET['keyword']) ? $resultTitle : __('Business | ','apollo');
        $title .= get_bloginfo('title');
    }

    return $title;
}
add_filter( 'wp_title', 'wpdocs_filter_wp_title', 99, 2 );

/**
 * Display re-activation message GLOBAL when our Apollo theme has new features
 *
 * @ticket #11493
 */
function apollo_display_re_activation_message() {
    Apollo_App::reActivationMessage();
}
add_action( 'admin_notices', 'apollo_display_re_activation_message' );

/**
 *  Vandd
 * @ticket #12148
 * Change the menu on a custom post type with the Menu Swapper plugin
 */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if(is_plugin_active('menu-swapper/menu-swapper.php')){

    /**
     * @return string
     */
    function mswp_get_current_post_type()
    {
        $currentTaxType = get_query_var( 'taxonomy' );

        if (get_query_var('_apollo_educator_search') === 'true'
            || get_post_type() == Apollo_DB_Schema::_EDUCATOR_PT
            || in_array($currentTaxType, array('educator-type'))
        ) {

            $post_type = Apollo_DB_Schema::_EDUCATOR_PT;

        } elseif (get_query_var('_apollo_program_search') === 'true' || get_post_type() == Apollo_DB_Schema::_PROGRAM_PT
                || in_array($currentTaxType, array('program-type', 'artistic-discipline'))
        ) {

            $post_type = Apollo_DB_Schema::_PROGRAM_PT;

        }
        elseif(get_query_var('_apollo_page_blog' ) == 'true'
            || get_query_var( '_apollo_author_search' ) === 'true'
            || is_category() || is_author()
            || (is_single() && get_post_type() == Apollo_DB_Schema::_BLOG_POST_PT)
        ){
            /*@ticket #18220 */
            $post_type = Apollo_DB_Schema::_BLOG_POST_PT;
        }
        else {
            $post_type = '';
        }
        return $post_type;
    }

    /**
     * @param $args
     * @return mixed
     */
    function mswp_swap_theme_location_filter_custom($args)
    {
        $post_type = mswp_get_current_post_type();
        switch ($post_type){
            case Apollo_DB_Schema::_EDUCATOR_PT :
                $swap_loc = of_get_option(Apollo_DB_Schema::_EDUCATOR_MSWP_LOC_POST_META, '');
                $target_loc = of_get_option(Apollo_DB_Schema::_EDUCATOR_MSWP_TARGET_POST_META, 'none');
                $enable = of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_MSWP, false);
                break;
            case Apollo_DB_Schema::_PROGRAM_PT :
                $swap_loc = of_get_option(Apollo_DB_Schema::_PROGRAM_MSWP_LOC_POST_META, '');
                $target_loc = of_get_option(Apollo_DB_Schema::_PROGRAM_MSWP_TARGET_POST_META, 'none');
                $enable = of_get_option(Apollo_DB_Schema::_PROGRAM_ENABLE_MSWP, false);
                break;
            /*@ticket #18220 Arts Education Customizations - handle the blog main and detail pages to use the arts education nav menu*/
            case Apollo_DB_Schema::_BLOG_POST_PT :
                $swap_loc = of_get_option(Apollo_DB_Schema::_BLOG_MSWP_LOC_POST_META, '');
                $target_loc = of_get_option(Apollo_DB_Schema::_BLOG_MSWP_TARGET_POST_META, 'none');
                $enable = of_get_option(Apollo_DB_Schema::_BLOG_ENABLE_MSWP, false);
                break;
            default :
                $swap_loc = '';
                $target_loc = '';
                $enable = false;
        }
        //Check if swap & target are set at all
        if( $swap_loc != '' && $target_loc != 'none' && $enable){

            //Check whether we should swap this location
            if( $target_loc == $args['theme_location'] ||
                $target_loc == 'all' ){
                $args['theme_location'] = $swap_loc;

                if( isset( $args['menu'] ) ){
                    //Special for WPML
                    if( defined( 'ICL_SITEPRESS_VERSION' ) && function_exists( 'icl_object_id' ) ) {
                        if( is_object( $args['menu'] ) ){
                            $locations = get_nav_menu_locations();
                            if ( isset( $args[ 'theme_location' ] ) && isset( $locations[ $args[ 'theme_location' ] ] ) ) {
                                $args[ 'menu' ] = icl_object_id( $locations[ $args[ 'theme_location' ] ], 'nav_menu' );
                            }
                        }
                    }
                    else $args['menu'] = ''; //unset( $args['menu'] );
                }
            }
        }
        return $args;
    }
    add_filter( 'wp_nav_menu_args' , 'mswp_swap_theme_location_filter_custom' , 40 );

}

function jetpackme_remove_rp() {
    if ( class_exists( 'Jetpack_RelatedPosts' )) {
        $jprp = Jetpack_RelatedPosts::init();
        $callback = array( $jprp, 'filter_add_target_to_dom' );
        remove_filter( 'the_content', $callback, 40 );
    }
}
add_filter( 'wp', 'jetpackme_remove_rp', 20 );

function current_year() {
$i = date("Y");
return $i;
}
add_shortcode('current_year', 'current_year');

/**
 * ThienLD: Add Hook Filter Menu Attribute Link to correct some cases of inputting in Admin Panel with the relative urls
*/
function wpd_nav_menu_link_atts( $atts, $item, $args, $depth ){
    if( '/' == substr( $atts['href'], 0, 1 ) ){
        $atts['href'] = site_url($atts['href']);
    }
    if( '/' != substr( $atts['href'], -1 ) ){
        $atts['href'] .= '/';
    }

    return $atts;
}
add_filter( 'nav_menu_link_attributes', 'wpd_nav_menu_link_atts', 20, 4 );

add_action('template_redirect', 'apl_add_last_modified_header');
function apl_add_last_modified_header() {

    //Check if we are in a single post of any type (archive pages has not modified date)
    if( is_single() ) {
        global $post;
        if( $post ) {
            $the_time = get_post_modified_time( "D, d M Y H:i:s", true, $post, true ). ' GMT';
            header("Last-Modified: " . $the_time );
        }
    }

}

/*@ticket #18025: Apply webpack on Apollo*/
if ( ! function_exists('apl_mix')) {
    function apl_mix( $path, $manifest_directory = '' ) {
        static $manifest;

        $manifest_path = APOLLO_PARENT_DIR . '/mix-manifest.json' ;

        if ( ! file_exists( $manifest_path ) ) {
            return  '/' . $path ;
        }
        if ( ! $manifest ) {
            $manifest = json_decode( file_get_contents( $manifest_path ), true );
        }

        $path = str_replace( $manifest_directory, '', $path );

        if ( ! array_key_exists( $path, $manifest ) ) {
            return  '/' .$path ;
        }

        $path = $manifest[ $path ];
        $path = ltrim( $path, '/' );
        return trailingslashit( $manifest_directory ) . $path ;
    }
}


/*@ticket #18136 0002477: Global Search modifiction - New global search option*/
$currentTheme =  !empty(wp_get_theme()) ? wp_get_theme()->stylesheet : '';
if($currentTheme === 'apollo' && !is_admin()){
    add_filter('add_global_search_below_nav', function(){

        global $post;
        $globalSearchEducator = of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_SEARCH_GLOBAL_CUSTOM, 0);

        /*@ticket #18190  Arts Education Customizations - Customize the search box below the nav menu - item 3*/
        //for educator module
        if($globalSearchEducator){

            $currentTaxType = get_query_var( 'taxonomy' );

            //listing page, single page & tax page
            if(($post && ($post->post_type == 'educator' || $post->post_type == 'program'))
                || in_array($currentTaxType, array('artistic-discipline', 'program-type', 'educator-type'))
                || get_query_var('_apollo_educator_search') === 'true'
                || get_query_var('_apollo_program_search') === 'true'
                ){
                return getGlobalSearchElement(home_url('program'),'keyword',  __('Keywords', 'apollo'));
            }

        }

        /** @Ticket #18489 */
        $globalSearchBlog  = of_get_option(Apollo_DB_Schema::_BLOG_ENABLE_OVERRIDE_SEARCH_GLOBAL_CUSTOM, false);
        if ($globalSearchBlog && ((is_single() && $post && $post->post_type == 'post') || get_query_var('cat') || get_query_var('author'))) {
            $blogDirectModule = of_get_option(Apollo_DB_Schema::_BLOG_OVERRIDE_SEARCH_GLOBAL_CUSTOM_DIRECT_MODULE, '');
            if (!empty($blogDirectModule)) {
                $blogDirectModule = $blogDirectModule == Apollo_DB_Schema::_EDUCATION ? Apollo_DB_Schema::_PROGRAM_PT : $blogDirectModule;
                return getGlobalSearchElement(home_url($blogDirectModule),'keyword', __('Keywords', 'apollo'));
            }
        }

        /*@ticket #18190  Arts Education Customizations - Customize the search box below the nav menu - item 3*/
        //for custom page
        if($post && $post->post_type == 'page'){
            $enablePageSearchCustom = get_post_meta($post->ID, 'page-global-search', true);
            $directToModule = get_post_meta($post->ID, 'direct-to-module', true);
            $directToModule = $directToModule == 'education' ? 'program' : $directToModule;
            if($enablePageSearchCustom == 'on'){
                return getGlobalSearchElement(home_url($directToModule),'keyword', __('Keywords', 'apollo'));
            }
        }

        //for all modules
        $placeGlobalSearch = of_get_option(APL_Theme_Option_Site_Config_SubTab::PLACE_GLOBAL_SEARCH, 0);
        if($placeGlobalSearch){
            return getGlobalSearchElement(home_url(), 's', __('Quick Search ...', 'apollo'));
        }

        return array();
    });

    function getGlobalSearchElement($homeUrl, $name, $keywords){
        return array(
            'class' => 'apl-place-global-search-below-nav',
            'icon' => '<span class="nav-global-search"><i class="fa fa-search fa-lg"></i></span>',
            'form_search' => "<form method='get' action='$homeUrl' class='form-search'>
                                <input name='$name' type='text' placeholder='$keywords' class='inp inp-txt solr-search'>
                                <i class='fa fa-search fa-flip-horizontal fa-lg'></i>
                                <button type='submit' class='btn btn-link'>SEARCH </button>
                             </form>"
        );
    }
}

/**
 * @ticket #18225: Load css of visual composer
 */
if (is_plugin_active('visualcomposer/plugin-wordpress.php')){
    add_filter('set_url_scheme', function ($url, $scheme, $orig_scheme) {

        $positionVisual = strpos($url, 'visualcomposer-assets');
        if ($positionVisual) {

            $visualLink = substr($url, $positionVisual, strlen($url));

            $blog_id = get_current_blog_id();

            if ($blog_id == 1) {
                return content_url() . "/uploads/" . $visualLink;
            } else {
                return content_url() . "/uploads/sites/{$blog_id}/" . $visualLink;
            }
        }
        return $url;
    }, 10, 3);
}


/** Check syndication artist and org */
if (!function_exists('aplCheckSyndicationArtistOrg')) {
    function aplCheckSyndicationArtistOrg($postType = 'artist') {
        $site_ids = get_sites( array(
            'spam'       => 0,
            'deleted'    => 0,
            'number'     => 1000,
            'fields'     => 'ids',
            'order'      => 'DESC',
            'orderby'    => 'id',
        ) );
        if (!empty($site_ids)) {
            foreach ($site_ids as $siteID) {
                $site = get_blog_details($siteID);
                if (!empty($site_ids)) {
                    // init variables
                    $site     = get_object_vars($site);
                    $blogName = empty($site['blogname']) ? $site['domain'] : $site['blogname'];

                    // flush rewrite rules for current blog
                    switch_to_blog( $siteID );
                    flush_rewrite_rules();
                    if ($postType == Apollo_DB_Schema::_ORGANIZATION_PT) {
                        $allKey = Apollo_DB_Schema::_SYNDICATION_ALL_ORGANIZATION;
                        $selectedKey = Apollo_DB_Schema::_SYNDICATION_SELECTED_ORGANIZATION;
                        $limitKey = Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_LIMIT;
                        $cacheKey = '';
                        $orderByKey = Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_ORDERBY;
                        $orderKey = Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_ORDER;
                    } else {
                        $allKey = APL_Syndication_Const::_SYNDICATION_ALL_ARTIST;
                        $selectedKey = APL_Syndication_Const::_SYNDICATION_SELECTED_ARTIST;
                        $limitKey = APL_Syndication_Const::_SYNDICATION_ARTIST_LIMIT;
                        $cacheKey = APL_Syndication_Const::_SYNDICATION_ARTIST_ENABLE_CACHE;
                        $orderByKey = APL_Syndication_Const::_SYNDICATION_ARTIST_ORDERBY;
                        $orderKey = APL_Syndication_Const::_SYNDICATION_ARTIST_ORDER;
                    }
                    $syndicationAll = get_option($allKey, '');
                    $syndicationSelected = get_option($selectedKey, '');
                    $syndicationLimit = get_option($limitKey, '');
                    $syndicationCache = !empty($cacheKey) ? get_option($cacheKey, '') : '';
                    $syndicationOrderBy = get_option($orderByKey, '');
                    $syndicationOrder = get_option($orderKey, '');
                    $haveData = '';
                    if ($syndicationAll || $syndicationSelected || $syndicationLimit || $syndicationCache || $syndicationOrderBy || $syndicationOrder) {
                        $haveData = 'style="color: red;"';
                    }
                    echo '<div '.$haveData.'>';
                    echo '<div>';
                    echo "<h1><a {$haveData} href='".home_url()."'>{$blogName}</a></h1>";
                    echo '<div>';
                    echo "<p>All Data: {$syndicationAll}</p>";
                    echo "<p>Selected items Data: {$syndicationSelected}</p>";
                    echo "<p>Limit Data: {$syndicationLimit}</p>";
                    if ($postType == Apollo_DB_Schema::_ARTIST_PT) {
                        echo "<p>Cache Data: {$syndicationCache}</p>";
                    }
                    echo "<p>Order By Data: {$syndicationOrderBy}</p>";
                    echo "<p>Order Data: {$syndicationOrder}</p>";
                    echo'</div>';
                    echo '</div>';
                    echo '</div>';
                }
            }

            switch_to_blog( 1 );
        }

    }
}

if (isset($_GET['apl-check-syndication']) && isset($_GET['apl-check-syndication-type'])) {
    aplCheckSyndicationArtistOrg($_GET['apl-check-syndication-type']);
    exit;
}