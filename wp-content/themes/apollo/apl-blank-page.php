<?php
/*
Template Name: home_page_features7
*/

//print_r($_SERVER["REQUEST_URI"]);

	global $wpdb;

$event_sql = "SELECT DISTINCT
	wp.ID,
	wp.post_title,
	wp.post_content,
	wp.post_date,
	em3.meta_value
FROM
	$wpdb->posts wp
JOIN $wpdb->apollo_post_term pt on pt.post_id = wp.ID AND (_tab_location IS NOT NULL OR _is_home_spotlight IS NOT NULL)
LEFT JOIN $wpdb->apollo_eventmeta emv ON emv.apollo_event_id = wp.ID
AND emv.meta_key = '_apollo_event_venue'
LEFT JOIN $wpdb->apollo_venuemeta vm ON vm.apollo_venue_id = emv.meta_value
AND vm.meta_key = '_venue_zip'
LEFT JOIN $wpdb->apollo_venuemeta vmc ON vm.apollo_venue_id = emv.meta_value
AND vmc.meta_key = '_venue_city'
JOIN $wpdb->apollo_eventmeta em2 ON em2.apollo_event_id = wp.ID
AND em2.meta_key = '_apollo_event_end_date'
AND em2.meta_value >= CURDATE()
JOIN $wpdb->apollo_eventmeta em3 ON em3.apollo_event_id = wp.ID
AND em3.meta_key = '_apollo_event_start_date'
AND em3.meta_value <= DATE_ADD(CURDATE(),INTERVAL 7 DAY)
WHERE
	1 + 1
AND post_type = 'event'
AND post_status IN ('publish')
ORDER BY em3.meta_value
";


//echo "$event_sql<P>";

$posts =  $wpdb->get_results($event_sql);
//print_r($posts);
//exit;
?>
<?php @header('Content-Type: application/xml');
echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
	<channel>
		<cf:treatAs xmlns:cf="http://www.microsoft.com/schemas/rss/core/2005">list</cf:treatAs>
			<title><![CDATA[Events - <?= get_bloginfo('name');?>]]></title>
		<link><![CDATA[<?= get_bloginfo('url');?>]]></link>
		<atom:link href="<?= get_bloginfo('url');?>/home_page_features7b/" rel="self" type="application/rss+xml" />
		<description><![CDATA[RSS Feed]]></description>
<?php
foreach($posts as $row){

$post_info = get_post($row->ID);
$start_date = get_apollo_meta($row->ID, Apollo_DB_Schema::_APOLLO_EVENT_START_DATE, true);
$end_date = get_apollo_meta($row->ID, Apollo_DB_Schema::_APOLLO_EVENT_END_DATE, true);

if($start_date == $end_date)
{
$event_dates = date("m/d/Y", strtotime($start_date));
}
else
{
$event_dates = date("m/d/Y", strtotime($start_date))." - ".date("m/d/Y", strtotime($end_date));
}

$location = get_apollo_meta($row->ID, Apollo_DB_Schema::_APOLLO_EVENT_VENUE, true);
$venue_info = get_post($location)->post_title;
if($location > 0)
{
$location = get_post($location)->post_title;
}
else
{
$location = '';
}
//print_r($post_info);
?>
<?php if($_SERVER["REQUEST_URI"] == "/home_page_features7/"): ?>

			<item>

				<link><![CDATA[<?=get_permalink($row->ID); ?>]]></link>
				<guid><![CDATA[<?=get_permalink($row->ID); ?>]]></guid>

				<enclosure url='<?=get_the_post_thumbnail_url($row->ID); ?>' type='image/jpeg' />
				<title><![CDATA[<?=$post_info->post_title ?>]]></title>
				<description><![CDATA[<div class="date"><?=$event_dates; ?></div><div class="location"><?=$location; ?></div>]]></description>

			<pubDate><?=date("D, d M Y H:i:s T", strtotime($post_info->post_date)); ?></pubDate>


			</item>
<?php else: ?>
			<item>
				<link><![CDATA[<?=get_permalink($row->ID); ?>]]></link>
				<guid><![CDATA[<?=get_permalink($row->ID); ?>]]></guid>

				<title><![CDATA[<?=$post_info->post_title ?>]]></title>
				<description><![CDATA[<?=$event_dates; ?> <?=$location; ?>]]></description>

			</item>
<?php endif; ?>
<?php
}
//end items
?>
			</channel>
</rss>