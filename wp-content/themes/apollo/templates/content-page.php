<?php
    if(Apollo_App::isDashboardPage()) {
        require __DIR__.'/pages/dashboard.php';
    }
    else if ( get_query_var('_apollo_page_blog' ) == 'true' ) {
        if ( ! Apollo_App::is_avaiable_module( 'post' ) ) {
            wp_safe_redirect( '/' );
        }
        require APOLLO_TEMPLATES_DIR.'/pages/blog.php';
    }
    else {
        require __DIR__.'/pages/normal.php';
    }

/** @Ticket #14684 - Show link logout */
//    if (isset($_COOKIE['wp-postpass_' . COOKIEHASH]) && $post->post_type == 'page') {
//        $wp_hasher = new PasswordHash(8, true);
//        if ($wp_hasher->CheckPassword($post->post_password, $_COOKIE['wp-postpass_' . COOKIEHASH])) {
//            echo '<a class="apl-protected-logout" data-cookiehash="'. COOKIEHASH .'">'. __('logout', 'apollo') .'</a>';
//        }
//    }
?>