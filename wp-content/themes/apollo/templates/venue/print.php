<?php
$id = get_query_var( '_apollo_venue_id' );
$venue = get_venue(get_post( $id ));
if ( ! $venue->post ) {
    echo Apollo_App::getTemplatePartCustom('templates/404-print');
    exit;
}
$data = $venue->getVenueData();
$logo =  Apollo_App::getHeaderLogo(Apollo_DB_Schema::_HEADER );
$str_address_all = isset($data['str_address'])?$data['str_address']:'';
$arrCoor = Apollo_Google_Coor_Cache::getCoorForVenue($str_address_all);
if(!is_array($arrCoor) || empty($arrCoor)){
    $arrCoor = Apollo_Google_Coor_Cache::getCoordinateByAddressInLocalDB($str_address_all);
}
$sCoor = $arrCoor ? implode(",", $arrCoor) : '';
$browser_key = of_get_option(Apollo_DB_Schema::_GOOGLE_API_KEY_BROWSER);
$_arr_field = array(
    Apollo_DB_Schema::_VENUE_ADDRESS1,
    Apollo_DB_Schema::_VENUE_CITY,
    Apollo_DB_Schema::_VENUE_STATE,
    Apollo_DB_Schema::_VENUE_ZIP,
);
?>

<html>
<head><title><?php _e( _e('Print detail venue | ', 'apollo' ) . $venue->get_title(), 'apollo' ) ?></title>
    <?php include APOLLO_TEMPLATES_DIR. '/partials/no-index.php' ?>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css"/>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/print.css" type="text/css"/>
    <style>
        <?php echo Apollo_App::getPrimarycolorCSSContent() ?>
    </style>

    <?php if($arrCoor === false): ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <?php endif; ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $browser_key ?>"></script>
</head>
<body id="artist-print-page">
<div class="top">
    <div class="top-head">
        <div class="logo print-page"><a href="<?php echo home_url() ?>"> <img src="<?php echo $logo; ?>"/></a></div>
    </div>
</div>
<div class="wrapper">
    <div class="block-detail">
        <div class="pic art"><a><?php echo $venue->get_image( 'medium' ); ?></a></div>
        <div class="artist-des">
            <h2 class="tt"><?php  echo $venue->get_title() ?></h2>
            <p><?php echo  $venue->generate_categories(true); ?></p>

            <?php
            if(isset($data['web']) && $data['web']){
                ?>
                <p> <i class="fa fa-link fa-lg"></i><span><?php _e('Website:','apollo') ?>
                        <a href="#" target="blank"><?php echo $data['web']; ?></a></span>
                </p>
            <?php
            }
            ?>
            <?php
            if(isset($data['phone']) && $data['phone']){
                ?>
                <p> <i class="fa fa-phone fa-lg">&nbsp;</i><span><?php echo $data['phone'] ?></span></p>
            <?php
            }
            ?>
            <?php
            if(isset($data['fax']) && $data['fax']){
                ?>
                <p> <i class="fa fa-fax fa-lg">&nbsp;</i><span><?php echo $data['fax'] ?></span></p>
            <?php
            }
            ?>
            <?php
            if(isset($data['address']) && $data['address']){
                ?>
                <p> <i class="fa fa-map-marker fa-lg">&nbsp;</i><span><?php echo $data['address'] ?></span></p>
            <?php
            }
            ?>
        </div>
        <div class="desc">
            <?php
            $content = $venue->get_full_content();
            echo Apollo_App::convertContentEditorToHtml($content);
            ?>
        </div>
    </div>
    <div class="h-line"></div>
    
    <?php  $venue->renderAddFieldPrintDetailPage($data['add_fields']); ?>
    
    <div class="block-detail">
        <div class="tt-bar"><a><i style="  margin-top: -5px;" class="fa fa-map-marker fa-2x"></i></a><span><?php _e( 'Location Info', 'apollo' ) ?></span></div>
        <div class="tt-info">
            <div class="loc">
                <?php
                echo '<p style="margin-top: 8px"> <strong style="font-size:14pt">'.$venue->get_title().'</strong></p>';

                ?>


                <?php

                echo '<p>';
                if(isset($data['address1']) && !empty($data['address1'])){
                    ?>
                    <?php echo $data['address1'] ?>
                <?php
                echo '</p>';
                }


                echo '<p class="bottom">';
                if(isset($data['city']) && !empty($data['city'])){
                    echo $data['city'].',' ?>
                <?php
                }

                if(isset($data['state']) && !empty($data['state'])){
                    echo $data['state'] ?>
                <?php
                }
                if(isset($data['zip']) && !empty($data['zip'])){
                    echo $data['zip'];
                }
                echo '</p>';
                ?>


            </div>

            <?php if(is_array($arrCoor) && count($arrCoor) > 0): ?>
                <div class="map">
                    <div class="lo-map" id="_event_map"></div>
                </div>
            <?php endif; ?>

        </div>
    </div>


    <div class="block-detail">
        <?php
        if(!empty($data['facebook']) || !empty($data['twitter']) || !empty($data['inst']) || !empty($data['linked']) || !empty($data['pinterest'])  ):
            ?>
            <div class="block-detail"><a><i class="fa fa-external-link fa-2x"></i></a><span class="question"><?php _e('SOCIAL MEDIA LINKS','apollo'); ?></span>
                <div class="el-blk">
                    <?php
                    if(isset($data['facebook']) && !empty($data['facebook'])){
                        ?>
                        <div class="art-social-item"><a href="<?php echo $data['facebook'] ?>" target="_blank"><?php echo $data['facebook'] ?></a></div>
                    <?php
                    }
                    ?>

                    <?php
                    if(isset($data['twitter']) && !empty($data['twitter'])){
                        ?>
                        <div class="art-social-item"><a href="<?php echo $data['twitter'] ?>" target="_blank"><?php echo $data['twitter'] ?></a></div>
                    <?php
                    }
                    ?>

                    <?php
                    if(isset($data['inst']) && !empty($data['inst'])){
                        ?>
                        <div class="art-social-item"><a href="<?php echo $data['inst'] ?>" target="_blank"><?php echo $data['inst'] ?></a></div>
                    <?php
                    }
                    ?>

                    <?php
                    if(isset($data['linked']) && !empty($data['linked'])){
                        ?>
                        <div class="art-social-item"><a href="<?php echo $data['linked'] ?>" target="_blank"><?php echo $data['linked'] ?></a></div>
                    <?php
                    }
                    ?>

                    <?php
                    if(isset($data['pinterest']) && !empty($data['pinterest'])){
                        ?>
                        <div class="art-social-item"><a href="<?php echo $data['pinterest'] ?>" target="_blank"><?php echo $data['pinterest'] ?></a></div>
                    <?php
                    }
                    ?>

                </div>
            </div>
        <?php endif; ?>
    </div>
</body>
<?php if(is_array($arrCoor) && count($arrCoor) > 0): ?>
    <script>

        var latlng = '';

        function m_initialize() {
            var mapOptions = {
                zoom: 15,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: true
            };

            var map = new google.maps.Map(document.getElementById('_event_map'),
                mapOptions);

            var marker = new google.maps.Marker({
                position: map.getCenter(),
                map: map,
                title: '',
                icon: '<?php echo get_stylesheet_directory_uri() ?>/assets/images/icon-location.png'
            });
        }

        <?php if(is_array($arrCoor) && count($arrCoor)>0): ?>

        function gm_initialize() {
            var geocoder = new google.maps.Geocoder();

            var address = '<?php echo $str_address_all; ?>';

            geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    var lat = <?php echo $arrCoor[0] ?>;
                    var lng = <?php echo $arrCoor[1] ?>);

                    latlng = new google.maps.LatLng(lat, lng);
                    m_initialize(latlng);

                    $.ajax({
                        url: '<?php echo admin_url("admin-ajax.php") ?>',
                        data: {
                            nonce: '<?php echo wp_create_nonce("save-coor") ?>',
                            action: 'apollo_save_coordinate',
                            lat: lat,
                            lng: lng,
                            addr: address
                        }
                    });

                } else {
                    alert(<?php _e('We\'re sorry. A map for this listing is currently unavailable.', 'apollo') ?>);
                }
            });
        }

        google.maps.event.addDomListener(window, 'load', gm_initialize);

        <?php else: ?>
        latlng = new google.maps.LatLng('<?php echo $arrCoor[0] ?>', '<?php echo $arrCoor[1] ?>');
        google.maps.event.addDomListener(window, 'load', m_initialize);
        <?php endif; ?>
    </script>
<?php endif; ?>

<style>
    .logo.print-page img{
        max-height:81px;
    }

</style>
</html>