<?php
//check is_active module here
if( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_VENUE_PT )){
    //load page content here
    $post = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_VENUE_PROFILE));

    if(Apollo_App::isInputMultiplePostMode()){
        /* Handle for case of allowing to add multiple org */
        if(strpos($GLOBALS['wp']->matched_rule, '^user/venue/add-new') !== false){
            $isAddingNewVenue = 1;
        } else {
            $isAddingNewVenue = 0;
        }

        if(strpos($GLOBALS['wp']->matched_rule, '^user/venue/profile/(\d+)') !== false &&
            ( intval(get_query_var( '_edit_post_id', 0)) === 0 )
        ){
            wp_redirect(site_url(APL_Dashboard_Hor_Tab_Options::VENUE_ADD_NEW_URL));
        }
    }


    $formOrg = new Apollo_Venue_Form(
        'venue-frm', //form Id
        'venue-frm-name', //form name
        'POST', //form method
        $_SERVER['REQUEST_URI'], //action url
        array(), //external form elements
        $_SERVER['REQUEST_METHOD'] //current request method
    );
    //set process and validate class for this form
    if(Apollo_App::isInputMultiplePostMode()) {
        $formOrg->setIsAddingNewVenue($isAddingNewVenue);
    }
    $formOrg->setOptionValidateClass('Apollo_Submit_Venue');
    $formOrg->setFormDescription('<p>'.$post->post_content.'</p>');
    $formOrg->renderForm();
}

