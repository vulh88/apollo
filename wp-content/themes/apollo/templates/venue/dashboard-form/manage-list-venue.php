<?php
if (Apollo_App::is_avaiable_module(Apollo_DB_Schema::_VENUE_PT)) {

    require_once APOLLO_TEMPLATES_DIR . '/pages/dashboard/inc/apollo-account-dashboard-dataprovider.php';
    $user_id = get_current_user_id();
    $pageSize = Apollo_Display_Config::DASHBOARD_ACCOUNT_BOOKMARK_PAGESIZE;
    ?>
    <div class="dsb-welc">
        <div class="bookmark" data-elem_need_coordinate_selector=".event-map>a" id="_apl_bookmark_container">
            <div class="wc-l account-listing">

                <div class="blog-blk" style="display: block;">
                    <?php
                    $datas_org = Apollo_Account_Dashboard_Provider::getListPostsByCurrentUser(array(
                        'pagesize' => $pageSize,
                        'start' => 0,
                        'post_type' => array(
                            Apollo_DB_Schema::_VENUE_PT
                        ),
                        'user_id' => $user_id,
                        'have_paging' => true,
                    ));
                    
                    if (count($datas_org['datas']) > 0):

                        ?>
                        <div class="blk-bm-events" id="_apol_venue">
                            <span class="h-t-t h-t-t-l"><?php _e("My Venues","apollo"); ?></span>
                            <nav class="events">
                                <?php
                                /** @Ticket #13448 */
                                $template = APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/partial/_venue/list-venue.php';
                                echo APL::renderTemplateWithData($datas_org, $template);
                                ?>
                            </nav>
                        </div>
                    <?php
                    if (intval($datas_org['total']) > $pageSize):
                    ?>
                        <div class="blk-paging"></div>
                    <?php endif; ?>

                        <script type="text/javascript">
                            var APL_DB_INLINE_CUSTOM = APL_DB_INLINE_CUSTOM || {};
                            APL_DB_INLINE_CUSTOM['items_<?php echo Apollo_DB_Schema::_VENUE_PT; ?>'] = <?php echo $datas_org['total'] ?>;
                            APL_DB_INLINE_CUSTOM['itemsOnPage'] = <?php echo $pageSize ?>;
                            APL_DB_INLINE_CUSTOM['baselink'] = '<?php echo admin_url('admin-ajax.php') ?>';
                        </script>
                        <?php
                    else:
                        _e('No result', 'apollo');
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>