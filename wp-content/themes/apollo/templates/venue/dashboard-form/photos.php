<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/26/2015
 * Time: 2:20 PM
 */

if (Apollo_App::is_avaiable_module(Apollo_DB_Schema::_VENUE_PT)) {

    if (! Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_VENUE_PT)) {

        // Multiple mode
        if (Apollo_App::isInputMultiplePostMode()) {
            if(strpos($GLOBALS['wp']->matched_rule, '^user/venue/photos') !== false) {
                wp_redirect(site_url(APL_Dashboard_Hor_Tab_Options::VENUE_ADD_NEW_URL));
            }
        }
        else { // Single mode
            wp_redirect('/user/venue/profile/?warning');
        }
    }



    echo '   <div class="dsb-welc">';
    //load page content here
    $content = '';
    $page = get_page_by_path( Apollo_Page_Creator::ID_VENUE_PROFILE_PHOTO,ARRAY_A,'page');
    $content = isset($page['post_content'])?$page['post_content']:'';
    echo '<div class="dsb-ct" style="margin-bottom: 15px">';
    echo '<p>';
    echo $content;
    echo '</p>';
    echo '</div>';


    //form begin here
    $formOrg = new Apollo_Venue_Photo_Form(
        'venue-frm', //form Id
        'venue-frm-name', //form name
        'POST', //form method
        $_SERVER['REQUEST_URI'], //action url
        array(), //external form elements
        $_SERVER['REQUEST_METHOD'] //current request method
    );
    //set process and validate class for this form
    $formOrg->setOptionValidateClass('Apollo_Submit_Venue');
    $formOrg->renderForm();
    //end form
    echo '</div>';

}