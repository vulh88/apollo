<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/27/2015
 * Time: 6:05 PM
 */
if( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_VENUE_PT )){

    /**
     * Whether checking the current user has profile or not
     */
    if (! Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_VENUE_PT)) {

        // Multiple mode
        if (Apollo_App::isInputMultiplePostMode()) {
            if(strpos($GLOBALS['wp']->matched_rule, '^user/venue/video') !== false) {
                wp_redirect(site_url(APL_Dashboard_Hor_Tab_Options::VENUE_ADD_NEW_URL));
            }
        }
        else { // Single mode
            wp_redirect('/user/venue/profile/?warning');
        }
    }



    //load page content here
    $page = get_page_by_path( Apollo_Page_Creator::ID_VENUE_PROFILE_VIDEO,ARRAY_A,'page');
    $content =  isset($page['post_content'])?$page['post_content']:'';
    echo '<div class="dsb-welc" style="">';
        echo '<div class="dsb-welc custom"><h1>'.__('Video','apollo').'</h1></div>';
    echo '</div>';
    $venueForm = new Apollo_Venue_Video_Form(
        'Organization-video-frm', //form Id
        'Organization-video-frm-name', //form name
        'POST', //form method
        $_SERVER['REQUEST_URI'], //action url
        array(), //external form elements
        $_SERVER['REQUEST_METHOD'] //current request method
    );
    //set process and validate class for this form
    $venueForm->setOptionValidateClass('Apollo_Submit_Venue');
    if(!empty($content)){
        $venueForm->setFormDescription('<p style="margin-bottom: 22px">'.__($content,'apollo').'</p>');
    }
    $venueForm->renderForm();
    include APOLLO_TEMPLATES_DIR. '/pages/dashboard/html/partial/_common/video-box.php';
}
