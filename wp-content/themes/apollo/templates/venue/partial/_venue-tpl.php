<?php foreach( $associatedVenues as $venue ): ?>
    <?php
        $venue      = get_venue($venue);
        $summary100 = $venue->get_summary(100);
    ?>
    <div class="more-cat-itm">
        <div class="more-pic">
            <a href="<?php echo $venue->get_permalink() ?>"><?php echo $venue->get_image( 'thumbnail', array(), array('aw' => true, 'ah' => true), 'normal') ?></a>
        </div>

        <div class="more-ct">
            <div data-type="link" data-url="<?php echo $venue->get_permalink() ?>">
                <h3>
                    <a href="<?php echo $venue->get_permalink() ?>">
                        <span class="ev-tt">
                            <?php
                                $title = $venue->get_title();
                                echo ( $title == false || empty($title) ) ? __('No title', 'apollo') : $title;
                            ?>
                        </span>
                    </a>
                </h3>
            </div>
            <div class="s-desc">
                <?php echo $summary100['text']  ?> <?php echo $summary100['have_more'] ? '...' : '' ?>
            </div>
        </div>
    </div>
<?php endforeach; ?>

