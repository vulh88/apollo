<?php
$type = get_query_var('_timeline_type');
$typeName = $type;

/*@ticket #17123 */
if($type === of_get_option(Apollo_DB_Schema::_ARTIST_CUSTOM_SLUG, 'artist')){
    $typeName = Apollo_App::getCustomLabelByModuleName(Apollo_DB_Schema::_ARTIST_PT);
    $type = Apollo_DB_Schema::_ARTIST_PT;
}

?>
<div class="breadcrumbs">
    <ul class="nav">
        <li><a href="<?php echo home_url() ?>"><?php _e( 'Home', 'apollo' ) ?></a></li>
        <li><a href="<?php echo Apollo_App::getCustomUrlByModuleName($type) ?>"><?php _e( ucfirst($typeName), 'apollo' ) ?></a></li>
        <li> <span><?php _e('Timeline') ?></span></li>
    </ul>
</div>

<div class="timeline-wrapper">
    <?php
             global $wpdb;

             $wpdb->escape_by_ref($type);

        $sql = "
            select p.post_content as content, p.ID as id, timeline.year_award as year_award, timeline.award_category as award_category from {$wpdb->{Apollo_Tables::_APL_TIMELINE}} timeline
            inner join  {$wpdb->posts} p on p.ID = timeline.p_id
            INNER JOIN {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}} as tblMetaSort ON (p.ID = tblMetaSort.apollo_artist_id) AND tblMetaSort.meta_key = '" . Apollo_DB_Schema::_APL_ARTIST_LNAME . "'
            where timeline.type = '{$type}' and p.post_status = 'publish'
            order by year_award ASC, tblMetaSort.meta_value ASC
        ";

        $results = $wpdb->get_results($sql);

        // build info mation
        $arrArtistInYear = array();
        $max_artist_in_year = 0;
        $arrInfo = array();
        if(!empty($results)) {
            foreach($results as $key => $arr_obj) {
                // get primary image
                $attachment_id = get_post_thumbnail_id( $arr_obj->id );
                $arr_primary_image_info = wp_get_attachment_image_src($attachment_id, 'large');
                $src = $arr_primary_image_info[0];
                if(empty($src)) {
                    $src = APOLLO_ADMIN_IMAGES_URL. '/placeholder-1.png';
                }
                // get last name first name
                $arrData = Apollo_App::unserialize(get_apollo_meta($arr_obj->id, Apollo_DB_Schema::_APL_ARTIST_DATA, true));

                $first_name = $arrData[Apollo_DB_Schema::_APL_ARTIST_FNAME];
                $last_name = $arrData[Apollo_DB_Schema::_APL_ARTIST_LNAME];

                // year
                $year = $arr_obj->year_award;
                $award_category = $arr_obj->award_category;
                $link = get_permalink($arr_obj->id);

                // get list term
                $artist = get_artist( $arr_obj->id );

                $arr_text = Apollo_App::getStringByLength($arr_obj->content, 150);

                $text = '';

                $sarr_artist_type =   $artist->generate_artist_categories(false);
                if(!empty($sarr_artist_type)) {
                    $text = '<p data-ride="ap-clamp" data-n="1">'. $sarr_artist_type .'</p>';
                }

                $text .= '<p data-ride="ap-clamp" data-n="3">' . $arr_text['text'] ;
                if($arr_text['have_more']) {
                    $text .= '...';
                }

                $text .= '</p>';

                // ADD AWARD CATEGORY
                $text .= '<p> AWARD: ' . implode(", ", array_filter(array($award_category, $year))) . '</p>';

                $text = preg_replace('/<p[^>]*><\\/p[^>]*>/', '', $text);

                $arrInfo[] = (object)array(
                    'asset' =>  (object) array(
                        'media' => $src,
                        "credit" => "",
                        "caption" => "",
                        "link"  => $link,
                    ),
                    "startDate" => $year,
                    "endDate" => $year,
                    "headline" => '<a data-ride="ap-clamp" data-n="3" href="' .$link . '">'.$first_name . ' '. $last_name.'</a>',
                    "text" => $text ,
                    "award_category" => $award_category,
                );

                $arrArtistInYear[$year] = isset($arrArtistInYear[$year]) ? $arrArtistInYear[$year] + 1 : 1;
            }

            $max_artist_in_year = max($arrArtistInYear);
            $max_artist_in_year = min(8, max(3, $max_artist_in_year));
        }
     ?>
    <script>
        var storyjs_jsonp_data =<?php echo json_encode((object)array('timeline' => (object)array(
            'date' => $arrInfo,
            'type' => 'default'
        ))); ?>
    </script>
<!-- BEGIN Timeline Embed -->
<?php if(!empty($results)): ?>
    <div id="timeline-embed"></div>
    <script type="text/javascript">
        var timeline_config = {
            width: "100%",
            height: "100%",
            source: storyjs_jsonp_data,
            debug: true,
            timespan: 'year',
            num_nav_rows: '<?php echo $max_artist_in_year ?>',
            start_page: true,
            end_page: true

        }
    </script>
<?php else: ?>
    <div>Coming soon...</div>
<?php endif; ?>

</div>