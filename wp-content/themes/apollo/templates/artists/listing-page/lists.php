<?php

    if ( $results = $this->get_results() ) {
        /** @Ticket #13137 */
        $hasPlaceholderImg = of_get_option(Apollo_DB_Schema::_ENABLE_PLACEHOLDER_IMG, 1);
        foreach ( $results as $p ): 
            
            $e = get_artist( $p );
            $summary100 = $e->get_summary(100);
            $link = $e->get_permalink();
            $image = $e->get_image_with_option_placeholder('thumbnail', array(),
                array(
                    'aw' => true,
                    'ah' => true,
                ),
                'normal', $hasPlaceholderImg);
        ?>
            <li>
                <div class="search-img <?php echo !$image ? 'no-place-holder-cate' : ''; ?>"><a href="<?php echo $link ?>"><?php echo $image; ?></a></div>
                <div class="info-content" data-url="<?php echo $link ?>" data-type="link">
                    <div class="search-info"><a href="<?php echo $link ?>"><span class="ev-tt"><?php echo $e->get_title() ?></span></a>
                        <div class="career"><?php echo $e->generate_artist_categories(); ?> </div>
                        <div class="s-desc"><?php echo $summary100['text']  ?> <?php echo $summary100['have_more'] ? '...' : '' ?></div>
                    </div>
                </div>
            </li>
        <?php 
        endforeach;
    } else if ( !isset($_GET['s']) ) {
        _e( 'No results', 'apollo' );
    }