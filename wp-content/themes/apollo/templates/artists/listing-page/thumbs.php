<?php 
    if ( $results = $this->get_results() ) {
        foreach ( $results as $p ): 
            
            $e = get_artist( $p );
            $summary = $e->get_summary();
            $link = $e->get_permalink();
        ?>
        <li>
            <div class="div-one" data-url="<?php echo $link ?>" data-type="link">
                <div class="search-img"><a href="<?php echo $link ?>"><?php echo $e->get_image( 'medium', array(),
                            array(
                                'aw' => true,
                                'ah' => true,
                            ),
                            'normal'
                        ) ?></a></div>
                <div class="search-info"><a href="<?php echo $link ?>"><span class="ev-tt"  data-n="2"><?php echo $e->get_title() ?></span></a>
                    <div class="career" data-ride="ap-clamp" data-n="2"><?php
                        echo $e->generate_artist_categories();
                        ?></div>
                    <div class="s-desc" data-ride="ap-clamp" data-n="9"><?php echo $summary['text']  ?></div>
                </div>
            </div>
        </li>
        
<?php endforeach;

} else if ( !isset($_GET['s'])) {
    _e( 'No results', 'apollo' );
}       