<?php 
    ob_start();
    if(isset($listEvent) && count($listEvent) > 0) :
    foreach ($listEvent as $event ):
        $s_event = get_event( get_post( $event->ID ) );
?>

<li>
    <div class="event-img"><a href="<?php echo $s_event->get_permalink() ?>"><?php echo $s_event->get_image() ?></a></div>
    <div class="event-checkbox">
        <input type="checkbox" name="eventid[]" value="<?php echo $s_event->id ?>" class="check-event"><span class="event"></span>
    </div>
    <div class="event-info">
        <a href="<?php echo $s_event->get_permalink() ?>">
            <span class="ev-tt"><?php echo $s_event->get_title(); ?></span> </a>
            <p class="meta auth"><?php echo $s_event->renderOrgVenueHtml(); ?></p>

            <span class="ev-date"><?php echo $s_event->render_sch_date(); ?></span>           
    </div>
</li>    
<?php 
    endforeach;
    endif;
    return ob_get_clean();
?>