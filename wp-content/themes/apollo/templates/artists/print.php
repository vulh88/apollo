<?php 
    $id = get_query_var( '_apollo_artist_id' );
    $artist = get_artist(get_post( $id ));
    $logo =  Apollo_App::getHeaderLogo(Apollo_DB_Schema::_HEADER );
    if ( ! $artist->post ) {
        echo Apollo_App::getTemplatePartCustom('templates/404-print');
        exit;
    }
?>
<html>
    <head><title><?php _e( _e('Print detail artist | ', 'apollo' ) . $artist->get_title(), 'apollo' ) ?></title>
        <?php include APOLLO_TEMPLATES_DIR. '/partials/no-index.php' ?>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css"/>
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"/>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/print.css" type="text/css"/>
        <style>
            <?php echo Apollo_App::getPrimarycolorCSSContent() ?>
        </style>
    </head>
    <body id="artist-print-page">
        <div class="top">
          <div class="top-head">
              <div class="logo print-page"><a href="<?php echo home_url() ?>"> <img src="<?php echo $logo; ?>"/></a></div>
          </div>
        </div>
        <div class="wrapper">
            <div class="block-detail">
              <div class="pic art"><a><?php echo $artist->get_image( 'medium' ); ?></a></div>
              <div class="artist-des">
                    <h2 class="tt"><?php echo $artist->get_title() ?></h2>
                    <p><?php 
                        $cats = $artist->get_categories();
                        $cat_name = $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_ANOTHER_CAT, Apollo_DB_Schema::_APL_ARTIST_DATA );
                        if ( $cats ) {
                            $arr_cat = array();
                            foreach( $cats as $c ):
                                $arr_cat[] = $c->name;
                            endforeach;
                        }

                        if ( $cat_name ) {
                            $arr_cat[] = $cat_name;
                        }

                        if ( $cat_name ) echo implode( ' - ' , $arr_cat );

                    ?></p>

                    <?php 
                        $email = $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_EMAIL, Apollo_DB_Schema::_APL_ARTIST_DATA );

                        if ( $email && $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_DISPLAY_EMAIL, Apollo_DB_Schema::_APL_ARTIST_DATA ) != 'yes' ):
                    ?>
                    <p> <i class="fa fa-envelope fa-lg"></i><span><?php echo $email ?></span></p>
                    <?php endif; ?>

                    <?php 
                        $blogurl = $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_BLOGURL, Apollo_DB_Schema::_APL_ARTIST_DATA );
                        $weburl = $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_WEBURL, Apollo_DB_Schema::_APL_ARTIST_DATA );
                        if ( $weburl ):
                    ?>
                        <p> <i class="fa fa-link fa-lg"></i><span><?php _e( 'Website', 'apollo' ) ?>: <a href="<?php echo $weburl ?>" target="_blank"><?php echo $weburl ?></a></p>
                    <?php endif; ?>

                    <?php
                       if ( $blogurl ):
                    ?>
                        <p> <i class="fa fa-star fa-lg"></i><span><?php _e( 'Blog URL', 'apollo' ) ?>: <a href="<?php echo $blogurl ?>" target="_blank"><?php echo $blogurl ?></a></p>
                    <?php endif; ?>

                    <?php 
                        $val = $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_PHONE, Apollo_DB_Schema::_APL_ARTIST_DATA );
                        $display_phone = $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_DISPLAY_PHONE, Apollo_DB_Schema::_APL_ARTIST_DATA );
                        if ( $val && $display_phone != 'yes' ):
                    ?>
                    <p> <i class="fa fa-phone fa-lg">&nbsp;</i><span><?php echo $val ?></span></p>
                    <?php endif; ?>

                    <?php 
                        $val = $artist->get_full_address();
                        if ( $val ):
                    ?>
                    <p> <i class="fa fa-map-marker fa-lg">&nbsp;&nbsp;</i>
                        <span>
                            <?php echo $val ?>
                        </span>
                    </p>
                    <?php endif; ?>
              </div>

              <div class="desc">
                <p>
                  <?php 
                        $content = $artist->get_content(10000);
                        echo $content['text'];
                    ?>
                </p>

              </div>
            </div>

            <?php
                    $questions = @unserialize( get_option( Apollo_DB_Schema::_APL_ARTIST_OPTIONS ) );
                    $answers = @unserialize( $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_QUESTIONS ) );
                    if ( $answers && $questions ):
                ?>
            <div class="h-line"></div>
            <div class="block-detail">
                <a><i class="fa fa-info-circle fa-2x"></i></a>
                <span class="question"><?php _e( 'MORE ABOUT THE ARTIST', 'apollo' ) ?></span>
                <?php foreach( $answers as $k => $val ): ?>
                <div class="el-blk">
                    <p class="custom"><strong><?php echo isset( $questions[$k] ) ? $questions[$k] : '' ?></strong></p>
                    <p class="custom"><?php echo $val ?></p>
                </div>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>


            <?php 
                $fb = $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_FB, Apollo_DB_Schema::_APL_ARTIST_DATA );
                $tw = $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_TW, Apollo_DB_Schema::_APL_ARTIST_DATA );
                $ins = $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_INS, Apollo_DB_Schema::_APL_ARTIST_DATA );
                $lk = $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_LK, Apollo_DB_Schema::_APL_ARTIST_DATA );

                if ( $fb || $tw || $ins || $lk ):
            ?>

            <div class="h-line"></div>
            <div class="block-detail"><a><i class="fa fa-external-link fa-2x"></i></a><span class="question"><?php _e( 'SOCIAL MEDIA LINKS', 'apollo' ) ?></span>

                <div class="el-blk">
                    <?php if ( $fb ): ?>
                    <div class="art-social-item">
                        <a target="_blank" href="<?php echo $fb ?>"><?php echo $fb; ?></a>
                    </div>
                    <?php endif; ?>

                    <?php if ( $tw ): ?>
                    <div class="art-social-item">
                        <a target="_blank" href="<?php echo $tw ?>"><?php echo $tw; ?></a>
                    </div>
                    <?php endif; ?>

                    <?php if ( $ins ): ?>
                    <div class="art-social-item">
                        <a target="_blank" href="<?php echo $ins ?>"><?php echo $ins; ?></a>
                    </div>
                    <?php endif; ?>

                    <?php if ( $lk ): ?>
                    <div class="art-social-item">
                        <a target="_blank" href="<?php echo $lk ?>"><?php echo $lk; ?></a>
                    </div>
                    <?php endif; ?>
                </div>

            </div>

            <?php endif; ?>

        </div>
    </body>
    <style>
        .logo.print-page img{
            max-height:81px;
        }
    </style>
</html>