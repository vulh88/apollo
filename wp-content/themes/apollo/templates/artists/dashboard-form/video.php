<?php

if( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ARTIST_PT )){

    /**
     * Whether checking the current user has profile or not
     */
    if (! Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_ARTIST_PT)) {

        // Multiple mode
        if (Apollo_App::isInputMultiplePostMode()) {
            if(strpos($GLOBALS['wp']->matched_rule, '^user/artist/video') !== false) {
                wp_redirect(site_url(APL_Dashboard_Hor_Tab_Options::ARTIST_ADD_NEW_URL));
            }
        }
        else { // Single mode
            wp_redirect('/user/artist/profile?warning');
        }
    }


    //load page content here
    $artistForm = new Apollo_ARtist_Video_Form(
        'Organization-video-frm', //form Id
        'Organization-video-frm-name', //form name
        'POST', //form method
        $_SERVER['REQUEST_URI'], //action url
        array(), //external form elements
        $_SERVER['REQUEST_METHOD'] //current request method
    );

    echo '<div class="dsb-welc" style="">';
    echo '<div class="dsb-welc custom"><h1>' . $artistForm->setTitleArtistElementForm(__('Video','apollo')). '</h1></div>';
    echo '</div>';

    $postId = Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_ARTIST_PT);
    if(APL_Artist_Function::checkArtistMember(isset($postId)? $postId :'')){
        $page = get_page_by_path( Apollo_Page_Creator::ID_ARTIST_VIDEO,ARRAY_A,'page');
        $content =  isset($page['post_content'])?$page['post_content']:'';
    }
    //set process and validate class for this form
    $artistForm->setOptionValidateClass('Apollo_Submit_Artist');
    if(!empty($content)){
        $artistForm->setFormDescription('<p class="apl-internal-content" style="margin-bottom: 22px">'.__($content,'apollo').'</p>');
    }
    $artistForm->renderForm();
    include APOLLO_TEMPLATES_DIR. '/pages/dashboard/html/partial/_common/video-box.php';
}
