<?php

/**
 * @ticket #15248 - [CF] 20180307 - [2049#c12311] Manage events assignment for each artist in multiple selections mode
 */

if (!Apollo_App::is_avaiable_module(Apollo_DB_Schema::_ARTIST_PT)) {
    return;
}

if (Apollo_App::isInputMultiplePostMode()) {
    if (strpos($GLOBALS['wp']->matched_rule, '^user/artist/events') !== false && (intval(get_query_var('_edit_post_id', 0)) === 0)) {
        wp_redirect(site_url(APL_Dashboard_Hor_Tab_Options::ARTIST_ADD_NEW_URL));
    }
}

echo '<div class="dsb-ct"';
echo '<p>';
echo '<div class="dsb-welc custom"><h1>'. __('Event', 'apollo') . '</h1></div>';
echo '</p>';

/**
 * This form just render the horizontal tab
 */
$form = new Apollo_Artist_Event_Form(
    'Artist-event-frm',         // form Id
    'Artist-event-frm-name',    // form name
    'POST',                     // form method
    $_SERVER['REQUEST_URI'],    // action url
    array(),                    // external form elements
    $_SERVER['REQUEST_METHOD']  // current request method
);
$form->setOptionValidateClass('Apollo_Submit_Artist');
$form->renderForm();

/**
 * We will re-use the form in the single mode here
 */
require_once __DIR__ . '/../events.php';

echo '</div>';
