<?php

if( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ARTIST_PT )){

    if(Apollo_App::isInputMultiplePostMode()) {
        if(strpos($GLOBALS['wp']->matched_rule, '^user/artist/photos') !== false &&
            ( intval(get_query_var( '_edit_post_id', 0)) === 0 )
        ){
            wp_redirect(site_url(APL_Dashboard_Hor_Tab_Options::ARTIST_ADD_NEW_URL));
        }
    }

    //load page content here
    $page = get_page_by_path( Apollo_Page_Creator::ID_ARTIST_PHOTO_FOR_MULTIPLE, ARRAY_A,'page');
    $content =  isset($page['post_content'])?$page['post_content']:'';

    echo '<div class="dsb-ct" style="">';
    echo '<p>';
    echo '<div class="dsb-welc custom"><h1>'.__('Photo','apollo').'</h1></div>';
    echo '</p>';

    $formArtist = new Apollo_Artist_Photo_Form(
        'Artist-photo-frm', //form Id
        'Artist-photo-frm-name', //form name
        'POST', //form method
        $_SERVER['REQUEST_URI'], //action url
        array(), //external form elements
        $_SERVER['REQUEST_METHOD'] //current request method
    );
    //set process and validate class for this form
    $formArtist->setOptionValidateClass('Apollo_Submit_Artist');
    $formArtist->setFormDescription('<p class="apl-internal-content" style="margin-bottom: 22px">'.__($content,'apollo').'</p>');
    $formArtist->renderForm($content);

    echo '</div>';
}
