<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/27/2015
 * Time: 6:05 PM
 */
if( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ARTIST_PT )){

    if (! Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_ARTIST_PT)) {

        // Multiple mode
        if (Apollo_App::isInputMultiplePostMode()) {
            if(strpos($GLOBALS['wp']->matched_rule, '^user/artist/audio') !== false) {
                wp_redirect(site_url(APL_Dashboard_Hor_Tab_Options::ARTIST_ADD_NEW_URL));
            }
        }
        else { // Single mode
            wp_redirect('/user/artist/profile?warning');
        }
    }
    
    $formArtist = new Apollo_Artist_Audio_Form(
        'Artist-audio-frm', //form Id
        'Artist-audio-frm-name', //form name
        'POST', //form method
        $_SERVER['REQUEST_URI'], //action url
        array(), //external form elements
        $_SERVER['REQUEST_METHOD'] //current request method
    );

    echo '<div class="dsb-ct" style="">';
    echo '<p>';
    echo '<div class="dsb-welc custom"><h1>'. $formArtist->setTitleArtistElementForm(__('Audio','apollo')) .'</h1></div>';
    echo '</p>';

    $postId = Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_ARTIST_PT);
    if(APL_Artist_Function::checkArtistMember(isset($postId)? $postId :'')){
        //load page content here
        $page = get_page_by_path( Apollo_Page_Creator::ID_ARTIST_AUDIO,ARRAY_A,'page');
        $content =  isset($page['post_content'])?$page['post_content']:'';
        $formArtist->setFormDescription('<p class="apl-internal-content" style="margin-bottom: 22px">'.__($content,'apollo').'</p>');
    }
    //set process and validate class for this form
    $formArtist->setOptionValidateClass('Apollo_Submit_Artist');
    $formArtist->renderForm();

    echo '</div>';
}
