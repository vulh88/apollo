<?php
// check is_active module here
if (Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ARTIST_PT )) {
    // load page content here
    $post = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ARTIST_PROFILE));

    if (Apollo_App::isInputMultiplePostMode()) {
        // handle for case of allowing to add multiple artist
        $isAddingNewArtist = (strpos($GLOBALS['wp']->matched_rule, '^user/artist/add-new') !== false) ? 1 : 0;

        if (strpos($GLOBALS['wp']->matched_rule, '^user/artist/profile/(\d+)') !== false && (intval(get_query_var('_edit_post_id', 0)) === 0)) {
            wp_redirect(site_url(APL_Dashboard_Hor_Tab_Options::ARTIST_ADD_NEW_URL));
        }
    }

    $form = new Apollo_Artist_Form(
        'artist-frm',              // form Id
        'artist-frm-name',         // form name
        'POST',                    // form method
        $_SERVER['REQUEST_URI'],   // action url
        array (),                  // external form elements
        $_SERVER['REQUEST_METHOD'] // current request method
    );

    // set process and validate class for this form
    if (Apollo_App::isInputMultiplePostMode()) {
        $form->setIsAddingNewArtist($isAddingNewArtist);
    }
    $form->setOptionValidateClass('Apollo_Submit_Artist');
    $form->setFormDescription('<p class="apl-internal-content">'.$post->post_content.'</p>');
    $form->renderForm();
}

