<?php

    /**
     * Whether checking the current user has profile or not
     */
    if (! Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_ARTIST_PT)) {

        // Multiple mode
        if (Apollo_App::isInputMultiplePostMode()) {
            if(strpos($GLOBALS['wp']->matched_rule, '^user/artist/events') !== false) {
                wp_redirect(site_url(APL_Dashboard_Hor_Tab_Options::ARTIST_ADD_NEW_URL));
            }
        }
        else { // Single mode
            wp_redirect('/user/artist/profile/?warning');
        }
    }


    include_once APOLLO_TEMPLATES_DIR.'/events/list-events.php';
    include APOLLO_TEMPLATES_DIR. '/pages/dashboard/inc/event/apollo-submit-event.php';
    $submit_obj = new Apollo_Submit_Event();

    $list_events = new List_Event_Adapter(1);
    $upcomming_events = $list_events->get_upcomming_user_events('', false, ' ORDER BY post_title ASC');

    $submit_obj->clean_session();

    /**
     * Multiple mode
     *
     * If the current site enables the Agency module (multiple mode)
     * we will get current artist ID in the URL to get appropriate
     * events of that artist.
     *
     * @ticket #15248 - [CF] 20180307 - [2049#c12311] Manage events assignment for each artist in multiple selections mode
     */
    $eids = array();
    $artistID = Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_ARTIST_PT);


    $page_size = Apollo_Display_Config::DASHBOARD_ARTIST_EVENTS_PAGESIZE;

    $user_artist = get_artist($artistID);
    $events = $user_artist->getAssociatedEvents(true);

    foreach ($events['data'] as $event) {
        $eids[] = $event->ID;
    }

?>

<div class="dsb-ct">
    <div class="dsb-welc">

        <div class="events">
            <div class="evt-blk add-artist-event">
                <?php wp_nonce_field(Apollo_Const::_APL_NONCE_ACTION_ARTIST_EVENT_PAGE, Apollo_Const::_APL_NONCE_NAME, false); ?>
                <div class="event-list">
                    <select id="sel-events-in-artist"  name="up_event" data-enable-remote=0 >
                        <option value=""><?php _e( 'Current/Upcoming Events', 'apollo' ) ?></option>
                        <?php
                        $all_eids = array();
                        foreach( $upcomming_events as $e ) {
                            if (!$e) {
                                continue;
                            }

                            $all_eids[] = $e->ID;

                            echo '<option ' . ($eids && in_array($e->ID, $eids) ? 'data-notallow="true"' : 'data-notallow="false"') . ' value="' . $e->ID . '">' . $e->post_title . '</option>';
                        }
                        ?>
                    </select>
                </div>

                <div class="b-btn">
                    <a href="javascript:void(0);" class="btn-bm btn btn-b"
                       data-rise="assign-event-to-artist"
                       data-eid=""
                       data-module-apply="artist"
                       data-action="apollo_assign_event_to_artist"
                       data-holder="#apl_artist_events>:last"
                       data-source="#sel-events-in-artist"
                       data-artist-id="<?php echo $artistID ?>"
                       data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
                       data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
                       data-msg="<?php _e( 'Please select an event', 'apollo' ) ?>" >
                        <?php _e( 'ADD EVENT', 'apollo' ) ?>
                    </a>
                </div>

            </div>
        </div>

        <div class="blk-bm-events" id="_apol_bmevent">
            <h1><?php _e( 'Selected Events', 'apollo' ) ?></h1>
            <?php Apollo_App::getPageContentByPath( Apollo_Page_Creator::ID_ARTIST_EVENT ); ?>
            <?php if (isset($events['data']) && count($events['data']) > 0) : ?>
                <div class="event-check-all">
                    <input type="checkbox" name="event-ck" data-ride="ap-checkboxall" data-child_selector="#_apol_bmevent .check-event" class="check-event-all">
                </div>
            <?php endif; ?>

            <nav class="events" id="apl_artist_events">
                <?php
                if (isset($events['data']) && count($events['data']) > 0) {
                    $listEvent = $events['data'];
                    echo include APOLLO_TEMPLATES_DIR. '/artists/listing-page/selected-events.php';
                } else {
                    echo '<li>'.__( 'No selected events', 'apollo' ).'</li>';
                }
                ?>
            </nav>
        </div>

        <?php  if (count($eids) > 0) : ?>
            <div class="blk-paging"
                 data-items="<?php echo $events['total'] ?>"
                 data-itemsOnPage="<?php echo $page_size ; ?>"
                 data-container_selector="#apl_artist_events"
                 data-baseurl="<?php echo admin_url('admin-ajax.php') ?>"
                 data-ajaxaction="apollo_get_more_events_assign_to_artist"
                 data-nextitemsonpage="<?php echo $page_size ?>"
                 data-module-apply="artist"
                 data-artist-id="<?php echo $artistID ?>"
                 data-ride="ap-pagination" >

            </div>

            <div class="b-btn add-artist-event">
                <a type="submit" class="btn-bm btn btn-b"
                   data-ride="ap-artist-unassign-events-selected"
                   data-action="apollo_artist_unassign_events_from_selected"
                   data-data_source_selector="[name^='eventid']:checked"
                   data-module-apply="artist"
                   data-artist-id="<?php echo $artistID ?>"
                   data-data="ids:value" data-msg="<?php _e( 'Please select at least an event', 'apollo' ) ?>"
                ><?php _e( 'REMOVE SELECTED EVENT', 'apollo' ) ?></a>
            </div>
        <?php endif; ?>

    </div>
</div>
