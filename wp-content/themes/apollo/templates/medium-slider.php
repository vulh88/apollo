<?php

        include_once APOLLO_TEMPLATES_DIR.'/events/list-events.php';
        $events = List_Event_Adapter::get_home_spotlight_events();
if ( $events ):
    
                $bg = of_get_option( Apollo_DB_Schema::_EVENT_SPOTLIGHT_BG_IMG );
                if ($bg):
            ?>  
                <style>
                    li.apl-slider-bg {
                        background-image: url(<?php echo $bg ?>);
                        background-repeat: no-repeat;
                        background-position: bottom right;
                    }
                    li.apl-slider-bg .slider-content {
                        background-color: transparent;
                    }
                </style>
                <?php endif; ?>

    <?php
    $bgEventSlider = of_get_option( Apollo_DB_Schema::_SLIDER_BACKGROUND );
    if ($bgEventSlider):
        ?>
        <style>
            .second-slider.second-slider-v1 .slider-content::after {
                border-left: 19px solid <?php echo $bgEventSlider ?>;
            }
        </style>
    <?php endif; ?>
<div class="second-slider <?php echo of_get_option(Apollo_DB_Schema::_SPOT_TYPE,'') == 'slider' ? 'second-slider-v1' : ''; ?>">
    <div class="flexslider">
        <ul class="slides">
            <?php
            $customExpiredtime = false; // Send it to cache
            /*@ticket #17348: 0002410: wpdev55 - Requirements part2 - [Event] Display event discount text*/
            $icon_fields = maybe_unserialize( get_option( Apollo_DB_Schema::_APL_EVENT_ICONS ) );
            $discountConfig = AplEventFunction::getConfigDiscountIcon();
            foreach ( $events as $i => $event ):
                $event = get_event( $event );

                if (!$i) {
                    $customExpiredtime = $event->get_meta_data(Apollo_DB_Schema::_APOLLO_EVENT_END_DATE);
                    $customExpiredtime = date('Y-m-d H:i:s', strtotime($customExpiredtime.' +1 day'));
                }
                $_arr_show_date = $event->getShowTypeDateTime();
                $_o_summary = $event->get_summary(of_get_option(Apollo_DB_Schema::_NUM_CHARS_SPOTLIGHT_EVENT_DESC, Apollo_Display_Config::HOME_SMALL_SPOTLIGHT_EVENT_OVERVIEW_NUMBER_CHAR));
                
                ///SOCIAL SHARE INFO
                $_o_s_summary = $event->get_summary();
                $arrShareInfo = $event->getShareInfo();
                
                if (! $slImg = $event->getSliderImage()) {
                    continue;
                }
                if ($event->isPrivateEvent()) {
                    continue;
                }

                $even_dataIcons = $event->getEventDataIcons($discountConfig['display_all_icon'], $icon_fields)
                ?>
                
                <li class="apl-slider-bg">
                    <?php  $event->render_circle_date(); ?>
                    <a href="<?php echo $event->get_permalink() ?>"><div style="background:url(<?php echo $slImg; ?>) center center; background-repeat: no-repeat; background-size:cover;" class="slider-pic">    </div></a>
                    <div class="slider-content">
                        <h2 class="blog-ttl"><a href="<?php echo $event->get_permalink(); ?>"><?php echo $event->get_title(true); ?></a></h2>
                        <p class="meta auth"><?php echo $event->renderOrgVenueHtml(); ?></p>

                        <p class="desc">
                            <?php  echo $_o_summary['text']. ( $_o_summary['have_more'] ? '...' : '' ); ?>
                            <a href="<?php echo $event->get_permalink(); ?>" class="vmore"><?php _e( 'View more', 'apollo' ) ?></a>
                        </p>
                        <div class="apl-event-discount-icon-text">
                            <?php if($discountConfig['enable-discount-description']){
                                $event->renderIcons($icon_fields,$even_dataIcons);
                                $event->generateDiscountText($discountConfig['include_discount_url']);
                            }?>
                        </div>

                    </div>
                    <div class="b-share-cat">
                        <?php
                            SocialFactory::social_btns( array( 'info' => $arrShareInfo, 'id' => $event->id ), false );
                        ?>
                    </div>
                </li>
            <?php endforeach; ?>    
        </ul>
        <div class="silder-footer"> </div>
    </div>
    <div class="loader"><a><i class="fa fa-spinner fa-spin fa-3x"></i></a></div>
  </div>

<?php

endif; ?>