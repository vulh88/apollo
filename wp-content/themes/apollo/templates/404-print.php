<html>
    <?php get_template_part('templates/head'); ?>
    <body>
        <?php get_template_part('header'); ?>
        <?php $template = isset($pageTemplate) ? $pageTemplate : ''; ?>
        <section class="main <?php echo Apollo_App::isFullTemplatePage($template) ? 'apl-full-width-page' : '' ?>">
            <div class="inner">
                <div class="error-container t-c">
                    <ul class="error">
                        <li>
                            <div class="magnify"><img src="<?php echo (get_template_directory_uri()."/assets/images/ico-error-404.png"); ?>" alt="page-not-found"></div>
                            <div class="text-404"><span class="top"><?php _e( 'OOPS', 'apollo' ) ?></span><span class="bottom"><?php _e( 'Error 404', 'apollo' ) ?></span></div>
                        </li>
                        <li>
                            <p><?php _e( "SORRY! The page you're looking for cannot be found. Return", 'apollo' ) ?>               <a href="<?php echo get_home_url(); ?>" target="_parent"> <?php _e( 'Home', 'apollo' ) ?></a></p>
                        </li>
                    </ul>
                </div>
            </div>
        </section> <!-- end main content -->
    </body>
    <style type="text/css">
        .top-blk, .mobile-menu, .main-menu, .top-search {
            display: none !important;
        }

        .top-head{
            border-bottom: 1px solid #ccc;
        }
    </style>
</html>