<?php 
$page = get_page_by_title( __( 'Home', 'apollo' ) );
$content = $page ? $page->post_content : '';

if($content && strpos($content, '[apollo_blog') === false){ ?>
    <div class="art-desc apl-internal-content">
        <?php   echo Apollo_App::convertContentEditorToHtml($content, false); ?>
    </div>
    <?php
}

/**
 * Have the blog shortcode then render default template
 * Do that to avoid some page before updating not have shortcode
 */

if (strpos($content, '[apollo_blog') !== false) {
    $scBlogContent = Apollo_App::convertContentEditorToHtml($content, false, false);
    echo do_shortcode($scBlogContent);
} else {
    include APOLLO_SHORTCODE_DIR . '/blog/default-template.php';
}
