<?php 
    $temOpt = of_get_option( Apollo_DB_Schema::_HOME_FEATURED_TYPE, false );
    $htDateOpts = of_get_option(Apollo_DB_Schema::_HOME_FEATURED_DATE_OPTION,'');

    /*@ticket #17250: Octave Theme - [Feature block] New feature block title - item 3, 4, 5, 6, 7*/
    $featureBlockTitleStyle = of_get_option(Apollo_DB_Schema::_FEATURE_BLOCK_TITLE_STYLE, '');

    /**
     * For overriding spacing for date bubble and date flag
     * @Ticket #16983
     */
    $dateBubbleClass = isset($htDateOpts) && ($htDateOpts == 'flags' || $htDateOpts == 'circle_tl') ? 'oc-date-bubble-or-flag' : '';

    $rootPath = APOLLO_TEMPLATES_DIR. '/events/html/home-featured';
    if ( $temOpt == 'ver' || $temOpt == 'ver1' ) {
        require $rootPath. '/vertical.php';
    } else if ( $temOpt == 'ver-no-title') {
        require $rootPath. '/vertical-no-title.php';
    } else if ( $temOpt == 'hor' ) {
        require $rootPath. '/horizontal.php';
    } else {
        require $rootPath. '/default-template.php';
    }
