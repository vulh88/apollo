<?php
if(empty($_GET['id'])) {
    wp_die("Something missing!");
}

$id = intval($_GET['id']);

// Find the position of postid

if(isset($_GET['type']) && $_GET['type'] === Apollo_DB_Schema::_VENUE_PT ){
    $at = get_venue($id);
    $address = $at->getStrAddress();
}
if(isset($_GET['type']) && $_GET['type'] === Apollo_DB_Schema::_PUBLIC_ART_PT ){
    $at = get_public_art($id);
    $address = $at->getStrAddress();
}
else {
    $at = get_event($id);
    $address = $at->getStrAddress();
}

if($at->isEmpty()) {
    wp_die(__("Something missing!", 'apollo'));
}

if(empty($address)) {
    wp_die(__("Something missing!", 'apollo'));
}

$arrCoor = Apollo_Google_Coor_Cache::getCoordinateByAddressInLocalDB($address);

$browser_key = of_get_option(Apollo_DB_Schema::_GOOGLE_API_KEY_BROWSER);
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php _e('Google Address', 'apollo') ?></title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <style>
        html, body, #map-canvas {
            height: 100%;
            margin: 0px;
            padding: 0px
        }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $browser_key ?>"></script>
    <?php if($arrCoor === false): ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <?php endif; ?>

    <script>

        var latlng = '';

        function m_initialize() {
            var mapOptions = {
                zoom: 15,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: true
            };

            var map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);

            var marker = new google.maps.Marker({
                position: map.getCenter(),
                map: map,
                title: '',
                icon: '<?php echo get_stylesheet_directory_uri() ?>/assets/images/icon-location.png'
            });
        }

        <?php if($arrCoor === false): ?>

        function gm_initialize() {
            var geocoder = new google.maps.Geocoder();

            var address = '<?php echo $address ?>';

            geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    var lat = results[0].geometry.location.lat();
                    var lng = results[0].geometry.location.lng();

                    latlng = new google.maps.LatLng(lat, lng);
                    m_initialize(latlng);

                    $.ajax({
                        url: '<?php echo admin_url("admin-ajax.php") ?>',
                        data: {
                            nonce: '<?php echo wp_create_nonce("save-coor") ?>',
                            action: 'apollo_save_coordinate',
                            lat: lat,
                            lng: lng,
                            addr: address
                        }
                    });

                } else {
                    alert("<?php _e('We\'re sorry. A map for this listing is currently unavailable.', 'apollo');?>");
                }
            });
        }

        google.maps.event.addDomListener(window, 'load', gm_initialize);

        <?php else: ?>
        latlng = new google.maps.LatLng('<?php echo $arrCoor[0] ?>', '<?php echo $arrCoor[1] ?>');
        google.maps.event.addDomListener(window, 'load', m_initialize);
        <?php endif; ?>
    </script>
</head>
<body>
<div id="map-canvas"></div>
</body>
</html>