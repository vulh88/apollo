<?php
global $post;
Apollo_Next_Prev::initSearchResult($post->post_type);
if ( ! file_exists( APOLLO_TEMPLATES_DIR.'/content-single/'.$post->post_type.'.php' ) ) {
    _e( 'Page not available.', 'apollo' );

} else {
    include APOLLO_TEMPLATES_DIR.'/content-single/'.$post->post_type.'.php';
}

