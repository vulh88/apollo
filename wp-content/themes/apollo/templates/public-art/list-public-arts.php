<?php

/**
 * @page: content current page
 */
class List_Public_Art_Adapter {

    public $public_art;
	public $pageSize;
	public $page;
	public $totalPost;
    
    public $_order_by;
    public $_order = 'ASC';
    public $_template;
    public $is_topten = false;


    public function __construct($page, $pageSize = Apollo_Display_Config::PAGE_SIZE ) {
       
        // Check module is avaiable
        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_PUBLIC_ART_PT ) ) {
            return false;
        }

        
		if(empty($pageSize)) {
			$pageSize = Apollo_Display_Config::PAGE_SIZE;
		}
        
		$this->pageSize = intval( $pageSize );
		$this->page = $page;
	}


    
    public function render_html( $template = '' ) {
        ob_start();
        include $template ? $template : $this->_template;
        return ob_get_clean();
    }
    
	public function getPublicArt() {
		return $this->public_art;
	}
    
	public function isEmpty() {
		return empty($this->public_art);
	}


	public function isShowMore() {
		$current_pos = ($this->page * $this->pageSize);
		return $current_pos < $this->totalPost;
	}


    public function get_artist_public_art($artist_id,$offset = true, $unlimit = false){;
        $this->pageSize = of_get_option(Apollo_DB_Schema::_NUM_VIEW_MORE,Apollo_Display_Config::_NUM_VIEW_MORE);
        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ARTIST_PT ) ) {
            return false;
        }

        global $wpdb, $post;
        $_offset = $offset ? ($this->page-1) * $this->pageSize. ', ' : '';

        $arr_order = Apollo_Sort_Manager::getSortOneFieldForPostType(Apollo_DB_Schema::_PUBLIC_ART_PT);
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.ID FROM {$wpdb->posts} p
            INNER JOIN {$wpdb->{Apollo_Tables::_APL_ARTIST_PUBLIC_ART}} a_pub ON a_pub.post_id = p.ID AND a_pub." . Apollo_DB_Schema::_APL_A_ART_ID . " = '$artist_id'";

        $sql .=   " WHERE 1=1 AND p.post_status = 'publish' AND p.post_type = '". Apollo_DB_Schema::_PUBLIC_ART_PT ."' ";
        $sql .= " GROUP BY p.ID ";
        if($arr_order['type_sort'] === 'post') {
            $sql .= " ORDER BY p.{$arr_order['order_by']} {$arr_order['order']} ";
        }

        if(!$unlimit){
            $sql .= " LIMIT $_offset $this->pageSize ";
        }

        $this->public_art   = $wpdb->get_results($sql);
        $this->totalPost    = $wpdb->get_var("SELECT FOUND_ROWS()");
        $currentTheme = function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '';
        if ( $currentTheme == APL_SM_DIR_THEME_NAME) {
            $this->_template   = SONOMA_MODULES_DIR. '/artist/templates/partials/_artist-public-art.php';
        } else {
            $this->_template   = APOLLO_PARENT_DIR.'/templates/public-art/_artist-public-art.php';
        }

        return $this->render_html($this->_template);
    }

}
