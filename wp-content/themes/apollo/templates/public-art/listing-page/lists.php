<?php

    if ( $results = $this->get_results() ) {
        foreach ( $results as $p ): 
            
            $e = get_public_art( $p );
            $summary100 = $e->get_summary(100);
        ?>
            <li>
                <div class="search-img"><a href="<?php echo $e->get_permalink() ?>"><?php echo $e->get_image('medium', array(),
                        array(
                            'aw' => true,
                            'ah' => true,
                        ),
                            'normal'
                        ) ?></a></div>
                <div class="info-content" data-url="<?php echo $e->get_permalink() ?>" data-type="link">
                    <div class="search-info"><a href="<?php echo $e->get_permalink() ?>"><span class="ev-tt"><?php echo $e->get_title() ?></span></a>
                        <?php 
                        $catHtml = $e->generate_categories();
                        if ($catHtml):
                        ?>
                        <div class="career"><?php _e('Category: '); echo $catHtml; ?> </div>
                        <?php endif; ?>
                        
                        <?php 
                        $locHtml = $e->generate_locations();
                        if ($locHtml):
                        ?>
                        <div class="career" ><?php _e('Location: '); echo $locHtml; ?> </div>
                        <?php endif; ?>
                        
                        <?php 
                        $colHtml = $e->generate_colections();
                        if ($colHtml):
                        ?>
                        <div class="career"><?php _e('Collection: '); echo $colHtml; ?> </div>
                        <?php endif; ?>
                        
                    </div>
                </div>
            </li>
        <?php 
        endforeach;
    } else if ( !isset($_GET['s']) && !isset($_GET['keyword']) ) {
        _e( 'No results', 'apollo' );
    }