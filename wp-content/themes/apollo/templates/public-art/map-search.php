<?php
$aplPubArtLat = of_get_option(Apollo_DB_Schema::_PUBLIC_ART_SETTING_LATITUDE,'');
$aplPubArtLat = str_replace(',','.',$aplPubArtLat);
$aplPubArtLong = of_get_option(Apollo_DB_Schema::_PUBLIC_ART_SETTING_LONGITUDE,'');
$aplPubArtLong = str_replace(',','.',$aplPubArtLong);
$aplPubArtZoomMagnification = of_get_option(Apollo_DB_Schema::_PUBLIC_ART_SETTING_ZOOM_MAGNIFICATION,'');
$mainCategories = Apollo_Org::get_tree_event_style(Apollo_DB_Schema::_PUBLIC_ART_PT);
$radiusArray =  Apollo_Map_Calc::getRadiusFilter();
?>
<section class="main">
    <div class="inner">
        <div class="wc-f map map-search">
            <div class="row two-col">
                <div class="v-line"> </div>
                <div class="wc-l">
                    <div class="breadcrumbs">
                        <ul class="nav">
                            <li><a href="<?php echo home_url() ?>"><?php _e( 'Home', 'apollo' ) ?></a></li>
                            <li><a href="<?php echo home_url('public-art') ?>"><?php _e( 'Public Art', 'apollo' ) ?></a></li>
                            <li> <span><?php _e('Public Art Map', 'apollo') ?></span></li>
                        </ul>


                    </div>

                    <div id="public-art-map-container">
                        <div id="art_map_canvas" data-position-text="<?php _e('My position','apollo') ?>"></div>
                        <div class="art_map_slider_content" id="art_map_slider_content">
                            <input type="hidden" id="_apl_pa_setting_lat" value="<?php echo $aplPubArtLat; ?>" />
                            <input type="hidden" id="_apl_pa_setting_long" value="<?php echo $aplPubArtLong; ?>" />
                            <input type="hidden" id="_apl_pa_setting_zoom_magnification" value="<?php echo $aplPubArtZoomMagnification; ?>" />
                            <ul id="art_map_slider">

                            </ul>
                        </div>
                    </div>
                    
                </div>
                <div class="wc-r ">
                    <div class="map-filter r-blk r-search">
                        <h3 class="r-ttl">
                            <i class="fa fa-search fa-flip-horizontal fa-lg">
                            </i>
                            <span><?php _e('Find Public Art In Map','apollo') ?></span>
                        </h3>
                        <div class="artist-search">
                            <form class="form-event " id="frm-map-search">
                                <div class="el-blk full">
                                    <input  type="text"  class="inp inp-txt" id="filter-name" placeholder="<?php _e('Public Art Name','apollo'); ?>">
                                    <div class="show-tip"><?php _e('Public Art Name', 'apollo') ?></div>
                                </div>

                                <div class="el-blk">
                                    <div class="select-bkl">
                                        <select name="term" class="chosen" id="filter-radius" >
                                            <option value=""><?php _e('Radius','apollo') ?></option>
                                            <?php
                                            foreach($radiusArray as $key => $name){
                                                echo '<option value="'.$key.'">'.$name.'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="el-blk">
                                    <div class="select-bkl">
                                        <select name="term" class="chosen" id="filter-category" >
                                            <option value=""><?php _e('Public Art Type','apollo') ?></option>
                                            <?php
                                            Apollo_Org::build_option_tree($mainCategories,'');
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="el-blk">
                                    <div class="select-bkl">
                                        <select name="medium" class="chosen" id="filter-medium">
                                            <option value="0"><?php _e('Medium', 'apollo') ?></option>
                                            <?php
                                            Apollo_Org::build_option_tree(Apollo_Public_Art::get_tree_medium(),'');
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="el-blk">
                                    <div class="select-bkl">
                                        <select name="location" class="chosen" id="filter-location">
                                            <option value="0"><?php _e('Location', 'apollo') ?></option>
                                            <?php
                                            Apollo_Org::build_option_tree(Apollo_Public_Art::get_tree_location(), '');
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="el-blk">
                                    <div class="select-bkl">
                                        <select name="collection" class="chosen" id="filter-collection">
                                            <option value="0"><?php _e('Collection', 'apollo') ?></option>
                                            <?php
                                            Apollo_Org::build_option_tree(Apollo_Public_Art::get_tree_collection(),'');
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="b-btn bt">
                                    <input type="button" class="btn btn-b search-btn" id="filter-button" class="" value="<?php _e('SEARCH','apollo') ?>" />
                                    <input type="button" class="btn btn-b search-btn btn-reset" id="filter-reset-button" class="" value="<?php _e('RESET','apollo') ?>" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
</section>