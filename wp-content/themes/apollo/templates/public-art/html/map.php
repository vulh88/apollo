<section class="main">
    <div class="inner">
        <div class="wc-f map">
            <div class="breadcrumbs">
                <ul class="nav">
                    <li><a href="<?php echo home_url() ?>"><?php _e( 'Home', 'apollo' ) ?></a></li>
                    <li><a href="<?php echo home_url('public-art') ?>"><?php _e( 'Public Art', 'apollo' ) ?></a></li>
                    <li> <span><?php _e('Public Art Map', 'apollo') ?></span></li>
                </ul>
            </div>

            <div class="blog-bkl">
                <div class="a-block-ct locatn">
                    <div class="loc-address">
                        <p></p>
                    </div>
                </div>
            </div>

            <div id="art_map_canvas"></div>

        </div>
    </div>
</section>