<?php
$id = get_query_var( '_apollo_public_art_id' );
$public_art = get_public_art(get_post( $id ));
$data = $public_art->getDataDetail();
$logo =  Apollo_App::getHeaderLogo(Apollo_DB_Schema::_HEADER );

$browser_key = of_get_option(Apollo_DB_Schema::_GOOGLE_API_KEY_BROWSER);
if ( ! $public_art->post ){
    echo Apollo_App::getTemplatePartCustom('templates/404-print');
    exit;
}
?>

<html>
<head><title><?php _e( _e( 'Print detail public art | ', 'apollo' ) . $public_art->get_title(), 'apollo' ) ?></title>
    <?php include APOLLO_TEMPLATES_DIR. '/partials/no-index.php' ?>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css"/>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/print.css" type="text/css"/>
    <style>
        <?php echo Apollo_App::getPrimarycolorCSSContent() ?>
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $browser_key ?>"></script>
</head>
<body id="artist-print-page">
<div class="top">
    <div class="top-head">
        <div class="logo print-page"><a href="<?php echo home_url() ?>"> <img src="<?php echo $logo; ?>"/></a></div>
    </div>
</div>
<div class="wrapper">
    <div class="block-detail">
        <div class="pic art"><a><?php echo $public_art->get_image( 'medium' ); ?></a></div>
        <div class="artist-des">
            <h2 class="tt"><?php  echo $public_art->get_title() ?></h2>

            <p><?php echo $public_art->generate_categories(true) ?></p>
            <?php
            if(isset($data['email']) && $data['email']){
                ?>
                <p> <i class="fa fa-envelope fa-lg"></i><span><?php _e('Email:','apollo') ?>
                    <?php echo $data['email']; ?></span>
                </p>
                <?php
            }
            ?>
            <?php
            if(isset($data['web']) && $data['web']){
                ?>
                <p> <i class="fa fa-link fa-lg"></i><span><?php _e('Website:','apollo') ?>
                        <a href="<?php echo $data['web'] ?>" target="blank"><?php echo $data['web']; ?></a></span>
                </p>
            <?php
            }
            ?>
            <?php
            if(isset($data['phone']) && $data['phone']){
                ?>
                <p> <i class="fa fa-phone fa-lg">&nbsp;</i><span><?php echo $data['phone'] ?></span></p>
            <?php
            }
            ?>
            <?php
            if(isset($data['address']) && $data['address']){
                ?>
                <p> <i class="fa fa-map-marker fa-lg">&nbsp;</i><span><?php echo $data['address'] ?></span></p>
            <?php
            }
            ?>
        </div>
        <div class="desc">
            <?php
            $content = $public_art->get_full_content();
            echo Apollo_App::convertContentEditorToHtml($content);
            ?>
        </div>
        <div style="clear:both"></div>
        <?php
        $medium_type = $public_art->generate_public_art_mediums(true);
        if(isset($medium_type) && $medium_type){
            ?>
            <p><strong><?php _e('Medium type: ', 'apollo'); ?></strong> <?php echo $medium_type;?></p>
            <?php
        }
        ?>
        <?php
        if(isset($data['date_created']) && $data['date_created']){
        ?>
            <p><strong><?php _e('Date created: ', 'apollo'); ?></strong> <?php echo $data['date_created'] ?></p>
        <?php
        }
        ?>
        <?php
        if(isset($data['dimension']) && $data['dimension']){
        ?>
            <p><strong><?php _e('Dimensions: ', 'apollo'); ?></strong> <?php echo $data['dimension'] ?></p>
        <?php } ?>

    </div>
    <?php
        if( $data['latitude'] && $data['longitude']){
    ?>
    <div class="h-line"></div>
    <div class="block-detail">
        <div class="tt-bar"><a><i class="fa fa-map-marker fa-2x"></i></a><span><?php _e( 'Location Info', 'apollo' ) ?></span></div>
        <div class="tt-info">
            <div class="loc">
                <p> <strong style="font-size:14pt"><?php echo $data['_address'] ?></strong></p>
                <p class="bottom"><?php echo $data['address']; ?></p>
            </div>
            <div class="map">
                <div class="lo-map" id="_event_map"></div>
            </div>

        </div>
    </div>
    <?php } ?>
    <div class="h-line"></div>
    <?php  $public_art->renderAddFieldPrintDetailPage($data['add_fields']); ?>
    <script>

        var latlng = '';

        function m_initialize() {
            var mapOptions = {
                zoom: 15,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: true
            };

            var map = new google.maps.Map(document.getElementById('_event_map'),
                mapOptions);

            var marker = new google.maps.Marker({
                position: map.getCenter(),
                map: map,
                title: '',
                icon: '<?php echo get_stylesheet_directory_uri() ?>/assets/images/icon-location.png'
            });
        }

        <?php if( $data['latitude'] && $data['longitude']) : ?>
            latlng = new google.maps.LatLng('<?php echo $data['latitude'] ?>', '<?php echo $data['longitude'] ?>');
            google.maps.event.addDomListener(window, 'load', m_initialize);
        <?php endif; ?>
    </script>
</body>
<style>
    .logo.print-page img{
        max-height:81px;
    }

</style>
</html>