<?php

class Apollo_Page_Module {
    
    protected $pagesize    = Apollo_Display_Config::APL_DEFAULT_NUMBER_ITEMS_FIRST_PAGE;

    /**
     * @return bool|int
     */
    public function getPagesize()
    {
        return $this->pagesize;
    }
    protected $page        = 1;
    protected $total_pages = 1;
    protected $offset      = 0;

    public  $result      = array();
    protected $total       = 0;
    protected $amount      = 0;
    protected $template    = '';
    public  $tabsearchs  = array();
    public  $type        = '';
    protected $base_url    = '';
    protected  $main_post = 0;
    protected $query_string = '';
    public static $keyword = '';
    public $basePathTemplate = '';
    public static $specialString = array("'", "‘", "’");

    /* @ticket #15646 */
    public static $isCounting = false;

    /**
     * @return mixed
     */
    public static function getIsCounting()
    {
        return self::$isCounting;
    }

    /**
     * @param mixed $isCounting
     */
    public static function setIsCounting($isCounting)
    {
        self::$isCounting = $isCounting;
    }

    /**
     * @var $viewType
     * #11686
     */
    public $viewType;
    
    
    public function __construct() {

        global $wpdb;

        /* ThienLd : sql injection avoiding handler */
        Apollo_App::sqlInjectionHandler();
        $wpdb->escape_by_ref(self::$keyword);

        // Stop handle limit, page when counting
        if (self::getIsCounting()) {
            return true;
        }

        /* HieuLuong : get type display default */
        if(isset($_REQUEST['s']) && empty($_REQUEST['view'])) {
            $_REQUEST['view'] = of_get_option(Apollo_DB_Schema::_APL_SEARCH_TYPE_VIEW, Apollo_DB_Schema::_APL_SEARCH_TYPE_VIEW_DEFAULT);
        }

        $this->page     = get_query_var('paged') !== '' ? get_query_var('paged') : 1;
        $this->page     = (isset($_GET['page']) && !empty($_GET['page'])) ? $_GET['page'] : 1;
        $this->page     = max(1, $this->page);

        $this->pagesize = of_get_option(Apollo_DB_Schema::_APL_NUMBER_ITEMS_FIRST_PAGE, Apollo_Display_Config::APL_DEFAULT_NUMBER_ITEMS_FIRST_PAGE);

        // Reset pagesize for ajax viewmore action
        if (Apollo_App::is_ajax()) {
            $this->pagesize = of_get_option(Apollo_DB_Schema::_APL_NUMBER_ITEMS_VIEW_MORE, Apollo_Display_Config::APL_DEFAULT_NUMBER_ITEMS_VIEW_MORE);
        }

        $arrayQS = self::apolloGetAllQueryStringToArray($_SERVER['QUERY_STRING']);
        if (isset($arrayQS['keyword'])) self::$keyword = $arrayQS['keyword'];
    }

    public static function getKeyword() {
        return self::$keyword;
    }

    public function setKeyword($keyword = '')
    {
        global $wpdb;
        self::$keyword = $keyword;
        $wpdb->escape_by_ref(self::$keyword);
    }

    public static function posts_search($p) {
        $keyword = self::getKeyword();

        if ( ! $keyword ) return $p;
        
        $termSearch = self::getWhereTaxByKeyword($p);
        global $wpdb;
        $searchTitle = self::replaceSpecialCharacter("{$wpdb->posts}.post_title", self::$specialString, $keyword);
        return str_replace('AND (((', " AND ( $termSearch $searchTitle OR ((( ", $p) . ')';
    }
    
    function filter_groupby($groupby) {
        global $wpdb;
        return "$wpdb->posts.ID";
    }
    
    public function set_main_post($post) {
        $this->main_post = $post;
    }

    public function get_current_page () {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return int
     */
    public function getTotalPages()
    {
        return $this->total_pages;
    }

    /**
     * @param int $total_pages
     */
    public function setTotalPages($total_pages)
    {
        $this->total_pages = $total_pages;
    }

    protected function resetPostData() {
        wp_reset_query();
    }
    
    /**
     * Get template name
     * @return string
     */
    protected function _get_template_name($option = 1) {
        // Set the render template
        $template = isset( $_REQUEST['view'] ) ? $_REQUEST['view'] : '';
        switch ( $template ):
            case 'list':
                return 'lists.php';
            case 'thumb':
                return 'thumbs.php';
            case 'map':
                return 'map.php';
            case 'table':
                return 'lists-table.php';
            default:
                if ( $option  == 1 )
                    return 'thumbs.php';
                if ( $option  == 2 )
                    return 'lists.php';
                return 'lists-table.php';
        endswitch;
    }
    
    public function get_common_url() {
        if(empty($this->main_post)) {
            throw new Exception("Please set_main_post before use this function!");
        }
        return $this->base_url = get_permalink($this->main_post->ID);
    }
    
    public function get_current_query_string() {
        $old_request = remove_query_arg('paged');

        $qs_regex = '|\?.*?$|';
        preg_match( $qs_regex, $old_request, $qs_match );

        if ( !empty( $qs_match[0] ) ) {
            $query_string = $qs_match[0];
        } else {
            $query_string = '';
        }

        return $this->query_string = $query_string;
    }
    
    /**
     * Render search results
     * @return string
     */
    public function render_html() {
        ob_start();
        require_once $this->template;
        return ob_get_clean();
    }

    public function have_more() {
        return ($this->page) * $this->pagesize < intval( $this->total );
    }

    /**
     * Checking have more items for pagination
     *
     * @return boolean
     *
     */
    public function have_more_on_viewmore() {
        /** #Ticket #16605 - Number results on first page */
        $numItemFirstPage = of_get_option(Apollo_DB_Schema::_APL_NUMBER_ITEMS_FIRST_PAGE, Apollo_Display_Config::APL_DEFAULT_NUMBER_ITEMS_FIRST_PAGE);
        $numViewMore = of_get_option(Apollo_DB_Schema::_APL_NUMBER_ITEMS_VIEW_MORE, Apollo_Display_Config::APL_DEFAULT_NUMBER_ITEMS_VIEW_MORE);

        return ($numItemFirstPage + ($numViewMore*($this->page - 1))) < intval( $this->total );
    }

    public function have_pag() {
        return $this->total > $this->pagesize;
    }

    /**
     * Check current request is onepage
     * @return boolean
     */
    public function is_onepage() {
        return isset( $_REQUEST['onepage'] ) && $_REQUEST['onepage'] === '1';
    }

    public static function apolloGetAllQueryStringToArray($queryString){
        try{
            $urlParts = array();
            parse_str($queryString, $urlParts);
            return $urlParts;
        }catch (Exception $ex){
        }
        return array();
    }

    private function pagination() {

        if ( $this->is_onepage() ) return false;

        $prev_arrow = is_rtl() ? '&rarr;' : '<';
        $next_arrow = is_rtl() ? '&larr;' : '>';

        $total = $this->total_pages;
        $big = 999999999; // need an unlikely integer

        if( $total > 1 )  {
            $basePagingLink = str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) );
//            $addedArgs = array();
            $arrayQS = self::apolloGetAllQueryStringToArray($_SERVER['QUERY_STRING']);
            if(get_query_var('term') !== ''){
                if(isset($arrayQS['page'])){
                    unset($arrayQS['page']);
                }
                $format = '?page=%#%';
                $addedArgs = $arrayQS;
                $basePagingLink = null;
            } elseif( get_option('permalink_structure') ) {
                if(isset($arrayQS['page'])){
                    unset($arrayQS['page']);
                }
                $addedArgs = $arrayQS;
                $format = '?page=%#%';
                $basePagingLink = null;
            } else {
                if(isset($arrayQS['paged'])){
                    unset($arrayQS['paged']);
                }
                $addedArgs = $arrayQS;
                $format = '?paged=%#%';
                $basePagingLink = null;
            }

            if(isset($_GET['page']) && !empty($_GET['page'])){
                $curPage = $_GET['page'];
            } elseif(isset($_GET['paged']) && !empty($_GET['paged'])) {
                $curPage = $_GET['paged'];
            } else {
                $curPage = 1;
            }

            $pagingArgs = array(
                'format'		=> $format,
                'current'		=> max( 1, $curPage),
                'total' 		=> $total,
                'mid_size'		=> 3,
                'type' 			=> 'list',
                'prev_text'		=> $prev_arrow,
                'next_text'		=> $next_arrow,
            );

            if(!empty($addedArgs)){
                $pagingArgs['add_args'] = $addedArgs;
            }
            if(!empty($basePagingLink)){
                $pagingArgs['base'] = $basePagingLink;
            }
            return paginate_links($pagingArgs);
        }
        return false;
    }

    public function render_pagination() {
        $pag = $this->pagination();
        $pag_str = '';
        if ( $pag ):
            $pag_str .= '<span class="pg-tt">'.__( 'Pages', 'apollo' ).':';
            $pag_str .= '<nav class="paging">'.$pag.'</nav>';
            $pag_str .= '</span>';
        endif;
        return $pag_str;
    }

    /**
     * Get result by post type
     * @return array
     */
    public function get_results() {
        return $this->result;
    }

    public function is_thumb_page() {
        return ! isset( $_REQUEST['view'] ) || $_REQUEST['view'] == 'thumb' || ! $_REQUEST['view'];
    }

    public function is_list_page($option = 1) {
        if(isset( $_REQUEST['view'] ) && $_REQUEST['view'] == 'list') {
            return true;
        }
        else if ( isset($option) && $option == 2 && (  !isset( $_REQUEST['view'] )  || $_REQUEST['view'] !== 'thumb') ) {
            return true;
        }
        return false;
    }

    /**
     * Logic here :
     *  $option =1 : thumb
     *  $option = 2: list
     * $option = 3 : table
     * @param int $option
     * @param string $type
     * @return bool
     * @author : TruongHN
     */

    public function is_view_type_page($option = 3, $type = 'table')
    {
        $defaultView = of_get_option(Apollo_DB_Schema::_CLASSIFIED_DEFAULT_VIEW_TYPE, 3);
        if(isset( $_REQUEST['view'] ) && $_REQUEST['view'] == $type) {
            return true;
        }
        else if (  $defaultView == $option && (  !isset( $_REQUEST['view'] )  ) ) {
            return true;
        }
        return false;
    }

    public function is_map_page() {
        return isset( $_REQUEST['view'] ) && $_REQUEST['view'] == 'map';
    }
    
    public function get_template_btn_url( $template = 'thumb' ) {
       
        return untrailingslashit(get_bloginfo('url')) . add_query_arg(array(
                'view' => $template,
                'onepage' => $this->is_onepage(),
                'type' => $this->type,
            ));
    }
    
    public function get_display_type_url( $onepage = '' ) {
        global $wp_rewrite;

        $template = isset( $_REQUEST['view'] ) ? $_REQUEST['view'] : '';

        $old_request = add_query_arg(array(
            'view' => $template,
            'onepage' => $onepage,
            'type' => $this->type,
        ));


        $old_request = remove_query_arg('paged', $old_request);

        $qs_regex = '|\?.*?$|';
        preg_match( $qs_regex, $old_request, $qs_match );

        if ( !empty( $qs_match[0] ) ) {
            $query_string = $qs_match[0];
            $old_request = preg_replace( $qs_regex, '', $old_request );
        } else {
            $query_string = '';
        }

        $old_request = preg_replace( "|$wp_rewrite->pagination_base/\d+/?$|", '', $old_request);
        $old_request = preg_replace( '|^' . preg_quote( $wp_rewrite->index, '|' ) . '|i', '', $old_request);

        return untrailingslashit(get_bloginfo('url')) . $old_request. $query_string;
    }
    
    public function render_result_title($postType = '', $dateFormat = 'Y-m-d', $isEcho = true) {
        $queries = $_GET;
        $text = '';
        if(is_array($queries) && count($queries) > 0){
            $prefix = '';
            foreach ($queries as $k => $query){

                // START: @ticket #11487
                if ( $k == 'date_format') {
                    continue;
                }
                if (in_array($k, array('apl-lat', 'apl-lng', 'min_lat', 'max_lat', 'min_lng', 'max_lng', 'print_event_search_result', 'total_result'))) {
                    continue;
                }
                // END: @ticket #11487

                $query = Apollo_App::convertIdToName($k,$query,$postType);

                if($text != ''){
                    $prefix = '+';
                }

                /** @Ticket - #12940 */
                if ($k == 'accessibility') {
                    $text .= $prefix . Apollo_App::convertAccessToText($query);
                }

                /** @Ticket - #13392 */
                if ($k == 'free_event') {
                    $text .= ' ' . $prefix . __(' Free Event', 'apollo');
                }

                /** @Ticket #13544 */
                if ($k == 'is_discount') {
                    $text .= $prefix . __( ' Include Discount offers', 'apollo' );
                }

                if ($k == 'by_my_location') {
                    $text .= $prefix . __('Filter by my location', 'apollo');
                    continue;
                }

                // @ticket #11462: format start date & end date
                if ( in_array($k, ['start_date', 'end_date']) && !empty($query) ) {
                    if ( isset($queries['date_format']) ) {
                        // @ticket #11487: reformat date from m-d-Y to Y-m-d
                        $query = date($dateFormat, strtotime(Apollo_App::formatDateFromMDYToYMD($query)));
                    } else {
                        $query = date($dateFormat, strtotime($query));
                    }
                }

                if($k == 'end_date' && $query != '' ){
                    if(isset($queries['start_date']) && $queries['start_date'] != ''){
                        $prefix = '-';
                    }
                }

                if(
                    $k != 'top-search-mobile' &&
                    $k != 'view' &&
                    $k != 'accessibility' &&
                    $k != 'free_event' &&
                    $query
                ){

                    $text .= " $prefix $query";
                }

            }
        }

        if($isEcho){
            $text = empty($text) ? sprintf( __( '%s result(s) found', 'apollo' ), $this->total) : str_replace("\\", "",sprintf( __( '%s result(s) found in your search for %s', 'apollo' ), $this->total, $text));
            echo '<p class="search-title-content ">'.$text.'</p>';

        }else{
            return empty($text) ? '' : '<p class="search-title-content ">'.str_replace("\\", "", sprintf( __( 'SEARCH RESULTS FOR: %s', 'apollo' ), $text)).'</p>';
        }

    }
    
    public static function getJoinTaxByKeyword() {
        
        global $wpdb;
        $keyword =  self::getKeyword();

        if (!$keyword) return '';
        
        return "
            LEFT JOIN $wpdb->term_relationships as wp_term_relation ON $wpdb->posts.ID = wp_term_relation.object_id
            LEFT JOIN $wpdb->term_taxonomy as wp_term_taxonomy ON wp_term_taxonomy.term_taxonomy_id = wp_term_relation.term_taxonomy_id
            LEFT JOIN $wpdb->terms as wp_terms1 ON wp_terms1.term_id = wp_term_taxonomy.term_id ";
    }
    
    public static function getWhereTaxByKeyword($where) {
      
        $keyword =  self::getKeyword();
        if (!$keyword) return '';

        return ' wp_terms1.name LIKE "%'.$keyword.'%" ';
    }

    public static function getCurFullUrl(){
        $currentDomain = home_url();
        $currentDomain = rtrim($currentDomain,"/");
        $fullCurUrl = $_SERVER['REQUEST_URI'];
        $fullCurUrl = str_replace($currentDomain,'',$fullCurUrl);
        $result = $currentDomain . '/' . ltrim($fullCurUrl,"/");
        return $result;
    }

    public function getShareInfo() {
        return array(
            'url' => self::getCurFullUrl(),
            'summary' => '',
            'media' => '',
        );
    }

    /**
     * Add offset to WP_Query
     *
     * @param $params reference array
     *
     */
    public function addOffsetToParams(&$params) {

        if ($this->page == 1 || !Apollo_App::is_ajax()) return $params;

        $params['offset'] = $this->getAjaxOnePageOffset();
        if(isset($params['posts_per_page'])) {
            $params['posts_per_page'] = $this->pagesize;
        }
    }

    public function getAjaxOnePageOffset() {
        // get a number of view more items in Theme Options
        /** #Ticket #16605 - Number results on first page */
        $offset = of_get_option(Apollo_DB_Schema::_APL_NUMBER_ITEMS_FIRST_PAGE, Apollo_Display_Config::APL_DEFAULT_NUMBER_ITEMS_FIRST_PAGE);

        if ($this->page > 2) {
            $offset += $this->pagesize * ($this->page - 2);
        }
        return intval($offset);
    }

    /**
     * Render join clause of the custom search fields
     * Optimize from @ticket #13823
     *
     * @param $metaTable
     * @param $foreignKey
     * @return string
     */
    protected static function renderJoinCustomFieldSearch($metaTable, $foreignKey)
    {
        global $wpdb;

        $join = '';

        $renderFunction = function(&$group_fields, $fieldName = 'custom_field', $fieldKey = 'custom_field_key') {
            if (!empty($_GET[$fieldName]) ) {
                foreach ($_GET[$fieldName] as $i => $field) {
                    if (!empty($field)) {

                        $exp = "(mt_cf.meta_value REGEXP '.*\"%s\";s:[0-9]+:\"%s\".*' OR mt_cf.meta_value REGEXP '.*\"%s\";a:[0-9]+:{.*.:\"%s\";.*}.*')";


                        if ($cusKey = !empty($_GET[$fieldKey][$i]) ? $_GET[$fieldKey][$i] : '') {
                            $cusKey = wp_unslash($cusKey);
                            // The first regular expression for normal string serialize, the second one is array serialize
                            $conditions = sprintf($exp, $cusKey, $field, $cusKey, $field);
                        }
                        else {
                            $field = wp_unslash($field);
                            $conditions = sprintf($exp, $field, $field, $field, $field);
                        }
                        array_push($group_fields, $conditions);
                    }
                }
            }
        };

        $group_fields = array();

        /** @Ticket #13237 */
        $renderFunction($group_fields);
        $renderFunction($group_fields, 'custom_field_cb', 'custom_field_cb_key');

        if (!empty($group_fields)) {
            $metaTable = $wpdb->{$metaTable};
            $query_field = " AND ( " . implode(' AND ', $group_fields) . " ) ";
            $join .= " INNER JOIN $metaTable mt_cf ON "
                . "(mt_cf.meta_key = '" . Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA . "' ". $query_field ." AND {$wpdb->posts}.ID = mt_cf.$foreignKey)";
        }

        return $join;
    }

    /**
     * @ticket #18851:  Make the search terms to be a little more flexible with apostrophes
     * @param $fieldName
     * @param $keyword
     * @param $specialCharacters
     * @return mixed
     */
    public static function replaceSpecialCharacter($fieldName, $specialCharacters, $keyword){
        $keyword2 =  explode(' ', $keyword);
        $sqlQuery = '';
        if ($keyword2) {
            $titleKeyword = array();
            foreach ($keyword2 as $k) {
                if (!empty(trim($k))) {
                    $k = str_replace($specialCharacters, '', $k);
                    $titleKeyword[] = " $fieldName LIKE '%{$k}%' ";
                }
            }
            if (!empty($titleKeyword)) {
                $sqlQuery = " OR ( ". implode(' AND ', $titleKeyword) ." ) ";
            }
        }
        if (!empty($sqlQuery)) {
            $replaceText = $fieldName;
            foreach ($specialCharacters as $char) {
                $replaceText = sprintf("REPLACE($replaceText, '\%s', '')", $char);
            }
            $sqlQuery = str_replace($fieldName, $replaceText, $sqlQuery);
        }

        return $sqlQuery;
    }

}
