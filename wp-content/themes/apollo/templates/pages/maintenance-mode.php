<div class="apl-internal-content page-not-found">
    <div class="apl-maintenance-mode">
        <div class="padding-top-100"></div>
        <?php  if( !empty( $maintenanceMode['content'] )) :
            echo  Apollo_App::convertContentEditorToHtml(Apollo_App::stripSlash($maintenanceMode['content']));
        endif; ?>
        <div class="padding-bot-100"></div>
    </div>
</div>