<?php  $blogCustomLabel = Apollo_App::getCustomLabelByModuleName(Apollo_DB_Schema::_BLOG_POST_PT); ?>
    <div class="breadcrumbs blog">
        <ul class="nav">
            <li><a href="<?php echo Apollo_App::changeBreadcrumbLink(Apollo_DB_Schema::_BLOG_POST_PT)?>" ><?php _e( 'Home' ) ?></a></li>

            <?php if ( get_query_var( '_apollo_page_blog' ) ) { ?>
                <li><span><?php echo $blogCustomLabel; ?></span></li>
            <?php } else { ?>

                <li><span><a href="<?php echo Apollo_App::getCustomUrlByModuleName(Apollo_DB_Schema::_BLOG_POST_PT); ?>"><?php echo $blogCustomLabel; ?></a></span></li>
                <li> <span><?php if (is_author() ) {
                            echo get_the_author();
                        } else {
                            single_cat_title();
                        } ?></span></li>
            <?php } ?>

        </ul>
    </div>

<?php
if ( have_posts() ): the_post();
    $content = get_the_content();


    if($content && strpos($content, '[apollo_blog') === false){ ?>
        <div class="art-desc apl-internal-content">
            <?php   echo Apollo_App::convertContentEditorToHtml($content, false); ?>
        </div>
        <?php
    }

    /**
     * Have the blog shortcode then render default template
     * Do that to avoid some page before updating not have shortcode
     */

    if (strpos($content, '[apollo_blog') !== false) {
        $scBlogContent = Apollo_App::convertContentEditorToHtml($content, false, false);
        echo do_shortcode($scBlogContent);
    } else {
        include APOLLO_SHORTCODE_DIR. '/blog/template.php';
    }

endif;
