<?php
/**
 * Created by IntelliJ IDEA.
 * User: tuanphp
 * Date: 27/11/2014
 * Time: 17:46
 */

class Apollo_Account_Dashboard_Provider {
    public static function getBookmark($arrInfo = array())
    {
        global $wpdb;

        $default = array(
          'pagesize' => '10',
          'start' => 0,
          'post_type' => array(
                'event',
          ),
          'have_paging' => true,
        );

        $arrInfo = wp_parse_args($arrInfo, $default);

        $start = (int)$arrInfo['start'];
        $pagesize = (int)$arrInfo['pagesize'];


        $bookmark_table = $wpdb->prefix.Apollo_Tables::_BOOKMARK;

        $_query_total  = '';

        if(isset($arrInfo['have_paging']) && $arrInfo['have_paging'] === true) {
            $_query_total = "SQL_CALC_FOUND_ROWS";
        }

        $sql = "SELECT $_query_total * FROM {$bookmark_table} as bm";

        // JOIN
        $sql .= " LEFT JOIN $wpdb->posts as posts ON (bm.post_id =posts.ID) ";


        // WHERE
        $where = array();
        if(isset($arrInfo['post_type']) && !empty($arrInfo['post_type'])) {
            $_in = "'".implode("', '", $arrInfo['post_type'])."'";
            $where[] = "bm.post_type in ({$_in})";
        }

        if(isset($arrInfo['user_id'])) {
            $user_id = $arrInfo['user_id'];
            $where[] = "user_id = {$user_id}";
        }

        if(!empty($where)) {
            $sql .= ' WHERE '.implode(" AND ", $where);
        }

        // GROUP
        $sql .= " GROUP BY posts.ID";

        // ORDER BY
        $sql .= " ORDER BY bm.bookmark_id DESC";


        // LIMIT
        $sql .= " LIMIT {$start}, {$pagesize}";

        if(isset($arrInfo['have_paging']) && $arrInfo['have_paging']) {
            return array(
                'datas' => $wpdb->get_results($sql),
                'total' => $wpdb->get_var("SELECT FOUND_ROWS()"),
            );
        }

        return array(
            'datas' => $wpdb->get_results($sql),
            'total' => 'Not implement',
        );

    }

    public static function getUserActivity($arrInfo = array())
    {
        global $wpdb;

        $default = array(
            'pagesize' => '10',
            'start' => 0,
            'user_id' => get_current_user_id(),
            'have_paging' => true,
        );

        $arrInfo = wp_parse_args($arrInfo, $default);

        $start = (int)$arrInfo['start'];
        $pagesize = (int)$arrInfo['pagesize'];


        $activity_table = $wpdb->prefix.Apollo_Tables::_APL_USER_ACTIVITY;

        $_query_total  = '';

        if(isset($arrInfo['have_paging']) && $arrInfo['have_paging'] === true) {
            $_query_total = "SQL_CALC_FOUND_ROWS";
        }

        $sql = "SELECT $_query_total * FROM {$activity_table} as ati";


        // WHERE
        $where = array();
        if(isset($arrInfo['user_id'])) {
            $user_id = $arrInfo['user_id'];
            $where[] = "user_id = {$user_id}";
        }

        if(!empty($where)) {
            $sql .= ' WHERE '.implode(" AND ", $where);
        }

        // ORDER BY
        $sql .= " ORDER BY ati.timestamp DESC";


        // LIMIT
        $sql .= " LIMIT {$start}, {$pagesize}";

        if(isset($arrInfo['have_paging']) && $arrInfo['have_paging']) {
            return array(
                'datas' => $wpdb->get_results($sql),
                'total' => $wpdb->get_var("SELECT FOUND_ROWS()"),
            );
        }

        return array(
            'datas' => $wpdb->get_results($sql),
            'total' => 'Not implement',
        );
    }

    /* Thienld : get list org by current user id */
    public static function getListPostsByCurrentUser($arrInfo = array())
    {
        global $wpdb;

        $default = array(
            'pagesize' => 10,
            'start' => 0,
            'have_paging' => true,
            'post_status' => array('pending','publish')
        );

        $arrInfo = wp_parse_args($arrInfo, $default);
        
        $start = (int)$arrInfo['start'];
        $pagesize = (int)$arrInfo['pagesize'];


        $_query_total  = '';

        if(isset($arrInfo['have_paging']) && $arrInfo['have_paging'] === true) {
            $_query_total = "SQL_CALC_FOUND_ROWS";
        }

        $sql = "SELECT $_query_total * FROM ".$wpdb->posts." as p";

        // WHERE
        $where = array();

        if(!empty($arrInfo['post_type'])) {
            $_in = "'".implode("', '", $arrInfo['post_type'])."'";
            $where[] = "p.post_type IN ({$_in}) ";
        }

        $postType =  !empty($arrInfo['post_type']) ? $arrInfo['post_type'] : array();
        if(isset($arrInfo['user_id'])) {
            $user_id = $arrInfo['user_id'];
            /** @Ticke #16209 - don't get with author is administrator or super admin */
            /**
             * @ticket #19641: show classified for owner is admin, super admin
             */
            if ((is_super_admin($user_id) || user_can($user_id, 'manage_options')) && !in_array(Apollo_DB_Schema::_CLASSIFIED,$postType) ) {
                $user_id = 0;
            }
            $where[] = "p.post_author = {$user_id}";
        }

        if(!empty($where)) {
            $sql .= ' WHERE ( ('.implode(" AND ", $where). ')';
            
            /**
             * vulh 
             * Get venue or org from the current user profile
             */
            if (!empty($arrInfo['post_type']) && count($arrInfo['post_type']) == 1) {
                $user_id = !empty($arrInfo['user_id']) ? $arrInfo['user_id'] : get_current_user_id();

                /* @ticket #15252 */
                $postType = isset($arrInfo['post_type'][0]) ? $arrInfo['post_type'][0] : '';
                $postIds  = ApolloAssociationFunction::getAssociatedModules( $user_id, $postType );

                if (count($postIds) > 0) {
                    $sql .= " OR (p.ID in (" . implode(', ', $postIds) . " ) ) ";
                }
            }

            $sql .= ' )';
        }

        if(isset($arrInfo['post_status']) && !empty($arrInfo['post_status'])) {
            $_in = "'".implode("','", $arrInfo['post_status'])."'";
            if(!empty($where)){
                $sql .= " AND p.post_status IN ({$_in}) ";
            } else {
                $sql .= " p.post_status IN ({$_in}) ";
            }
        }

        //Huyenln add filter by post_id
        if (!empty($arrInfo['post_id'])) {
            $sql .= "AND p.ID = ".$arrInfo['post_id']." ";
        }

        // ORDER BY
        $sql .= " ORDER BY p.post_title ASC";
       
        // LIMIT
        if($pagesize){
            $sql .= " LIMIT {$start}, {$pagesize}";
        }

        if(isset($arrInfo['have_paging']) && $arrInfo['have_paging']) {
            return array(
                'datas' => $wpdb->get_results($sql),
                'total' => $wpdb->get_var("SELECT FOUND_ROWS()"),
            );
        }

        return array(
            'datas' => $wpdb->get_results($sql),
            'total' => 'Not implement',
        );

    }
}
