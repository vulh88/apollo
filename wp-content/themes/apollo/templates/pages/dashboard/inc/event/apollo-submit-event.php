<?php
if ( !defined( 'ABSPATH' ) ) exit;

class Apollo_Submit_Event {

    private $type = Apollo_DB_Schema::_EVENT_PT;
    public $event_name = '';
    public $tags = '';
    public $event_desc = '';
    public $event_summary = '';
    public $event_id = '';
    public $meta_tbl = '';
    public $from_id = '';
    private $event_status = 'draft';
    private $event_author = '';
    private $_current_step = -1;

    /**
     * @return int
     */
    public function getCurrentStep()
    {
        return $this->_current_step;
    }

    /**
     * @param int $current_step
     */
    public function setCurrentStep($current_step)
    {
        $this->_current_step = $current_step;
    }

    // Event data
    public $event_data = array(
        Apollo_DB_Schema::_WEBSITE_URL => '',
        Apollo_DB_Schema::_E_CONTACT_NAME => '',
        Apollo_DB_Schema::_E_CONTACT_EMAIL => '',
        Apollo_DB_Schema::_E_CONTACT_PHONE => '',
        Apollo_DB_Schema::_ADMISSION_PHONE => '',
        Apollo_DB_Schema::_ADMISSION_TICKET_EMAIL => '',
        Apollo_DB_Schema::_ADMISSION_TICKET_URL => '',
        Apollo_DB_Schema::_E_CUS_ACB => '',
        Apollo_DB_Schema::_VIDEO => '',
        Apollo_DB_Schema::_APL_EVENT_ADDITIONAL_TIME => '',
    );
    
    public $venue = '';
    public $tmp_venue = array(
        Apollo_DB_Schema::_VENUE_NAME => '',
        Apollo_DB_Schema::_VENUE_ADDRESS1 => '',
        Apollo_DB_Schema::_VENUE_EMAIL => '',
        Apollo_DB_Schema::_VENUE_ZIP => '',
        Apollo_DB_Schema::_VENUE_STATE => '',
        Apollo_DB_Schema::_VENUE_WEBSITE_URL => '',
        Apollo_DB_Schema::_VENUE_CITY => '',
        Apollo_DB_Schema::_VENUE_NEIGHBORHOOD => '',
    );
    
    private $start_date = '';
    private $end_date = '';
    private $free_event = '';
    
    private $tmp_org = '';
    private $r_org = '';
    private $co_org = array();
    
    private $pri_cat = '';
    private $add_cat = array();
    public $event_tags = array();
    public  $attachment_id = '';
    
    private $days_of_week = array();
    
    public $errors = array();

    public $nonceName   = '';
    public $nonceAction = '';
    private $event_artist_checked = '';
    private $event_artist_unchecked = '';

    /**
     * @var boolean whether checking is saved as a draft or not
    */
    public $saveDraft;

    public function __construct( $event_id = '' ) {
        $this->start_date = $this->end_date = current_time( 'Y-m-d' );
        $this->event_id = $event_id;

        // ticket #11047: add nonce info for Add Event pages
        $this->nonceName   = Apollo_Const::_APL_ADD_EVENT_NONCE_NAME;
        $this->nonceAction = Apollo_Const::_APL_NONCE_ACTION_ADD_EVENT_PAGE;

        /** @Ticket #15756 */
        $this->setSaveDraft(isset( $_POST['apl-fe-save-draft'] ));
    }

    /**
     * @return string
     */
    public function getEventName()
    {
        return $this->event_name;
    }

    /**
     * @param string $event_name
     */
    public function setEventName($event_name)
    {
        $this->event_name = $event_name;
    }

    /**
     * @return bool
     */
    public function isSaveDraft()
    {
        return $this->saveDraft;
    }

    /**
     * @param bool $saveDraft
     */
    public function setSaveDraft($saveDraft)
    {
        $this->saveDraft = $saveDraft;
    }



    public function render_html( $template, $data ) {
        ob_start();
        include APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/partial/_event/'. $template;
        return ob_get_contents();
    }

    public function submit_event_step3()
    {
        // ticket #11047: validate nonce field
        if ( !isset($_POST[$this->nonceName]) || !wp_verify_nonce($_POST[$this->nonceName], $this->nonceAction) ) {
            return false;
        }

        if ( $this->event_id ) {
            $p = get_post( $this->event_id );
            wp_update_post( array(
                'ID' => $this->event_id,
                'post_status' => $p->post_status == 'publish' ? $p->post_status : 'pending',
            ) );

            /* Thienld: new instance to handle caching logic for event category page */
            if(!empty($p) && $p->post_status == 'publish'){
                // only empty cache if on action editing PUBLISHED event
                do_action('apl_event_cache_empty_all_cache');
            }

            if(has_action('apollo_email_confirm_new_item_created') && $this->is_adding_event()){
                do_action('apollo_email_confirm_new_item_created',$this->event_id,Apollo_DB_Schema::_EVENT_PT,false);
                /** @Ticket #13638 */
                do_action('apollo_email_confirm_new_item_created_to_owner',$this->event_id,Apollo_DB_Schema::_EVENT_PT,false);
            }

            //Truonghn :Send mail to admin when the post is edited
            if(has_action('apollo_email_confirm_item_edited') && !$this->is_adding_event() && !current_user_can('manage_options') && $p->post_status == 'publish'){
                do_action('apollo_email_confirm_item_edited',$this->event_id,Apollo_DB_Schema::_EVENT_PT,false);
            }

            /** @Ticket #13638 */
            if ($p->post_status == 'publish') {
                if (!Apollo_App::apollo_get_meta_data($p->ID,Apollo_DB_Schema::_E_CONFIRM)) {

                    $email = Apollo_App::apollo_get_meta_data($p->ID,Apollo_DB_Schema::_E_CONTACT_EMAIL, Apollo_DB_Schema::_APOLLO_EVENT_DATA);
                    $contactName = Apollo_App::apollo_get_meta_data($p->ID, Apollo_DB_Schema::_E_CONTACT_NAME, Apollo_DB_Schema::_APOLLO_EVENT_DATA);
                    if ($email) {
                        update_apollo_meta($p->ID, Apollo_DB_Schema::_E_CONFIRM, 1);
                        do_action( 'apollo_email_published_new_event', $contactName, $p->post_title, get_the_permalink($p->ID), $email );
                    }

                }

            }


            $this->clean_session();
        }
        wp_redirect(home_url(). '/user/submit-event-success' );
        //wp_redirect(home_url(). '/user/events' );

    }

    public function clean_session() {
        /* Reset the template */
        if ( isset( $_SESSION['apl_add_org_btn'] ) )    unset( $_SESSION['apl_add_org_btn'] );
        if ( isset( $_SESSION['apl_add_venue_btn'] ) )  unset( $_SESSION['apl_add_venue_btn'] );
        if ( isset( $_SESSION['apl_add_cat_btn'] ) )    unset( $_SESSION['apl_add_cat_btn'] );
        if ( isset( $_SESSION['apl_add_acc_btn'] ) )    unset( $_SESSION['apl_add_acc_btn'] );
        if ( isset( $_SESSION['apl_num_org_cor'] ) )    unset( $_SESSION['apl_num_org_cor'] );
        if ( isset( $_SESSION['apl_add_video_btn'] ) )    unset( $_SESSION['apl_add_video_btn'] );

        if ( isset( $_SESSION['apollo']['copy_img'] ) )    unset( $_SESSION['apollo']['copy_img'] );

        $isess = intval($this->event_id);

        if ( isset( $_SESSION['apollo'][Apollo_SESSION::EVENT_ADD_PIMAGE][$isess] ) ) {
            unset ($_SESSION['apollo'][Apollo_SESSION::EVENT_ADD_PIMAGE][$isess]);
        }
        if ( isset( $_SESSION['apollo'][Apollo_SESSION::EVENT_ADD_GALLERY][$isess] ) ) {
            unset ($_SESSION['apollo'][Apollo_SESSION::EVENT_ADD_GALLERY][$isess]);
        }

        unset( $_SESSION['added_event_id'] );
    }

    public function submit_event_dates()
    {
        $require = __( '%s is required', 'apollo' );
        $maximumDateRange = __( 'Events may not last more than %s years. Please revise your end date to less than %s years.', 'apollo' );
        $key_start = Apollo_DB_Schema::_APOLLO_EVENT_START_DATE;
        $key_end = Apollo_DB_Schema::_APOLLO_EVENT_END_DATE;

        $this->start_date  = isset( $_POST[$key_start] ) ?
            Apollo_App::clean_data_request( $_POST[$key_start] ) : '';

        $this->end_date  = isset( $_POST[$key_end] ) ?
            Apollo_App::clean_data_request( $_POST[$key_end] ) : '';

        if ( ! $this->start_date ) {
            if (!$this->isSaveDraft()) {
                $this->errors[$key_start] = sprintf( $require, __( 'Start date', 'apollo' ) );
            }
        } else {
            if (! Apollo_App::is_valid_date( $this->start_date)) {
                $this->errors[$key_start] = __( 'Start date is not valid. Sample format date: yyyy-mm-dd.', 'apollo' );
            }
        }

        if ( ! $this->end_date ) {
            if (!$this->isSaveDraft()) {
                $this->errors[$key_end] = sprintf( $require, __( 'End date', 'apollo' ) );
            }
        } else {
            if (! Apollo_App::is_valid_date( $this->end_date)) {
                $this->errors[$key_end] = __( 'End date is not valid. Sample format date: yyyy-mm-dd.', 'apollo' );
            }
        }
        $startDateTime = strtotime($this->start_date);
        $endDateTime = strtotime($this->end_date);
        $dateDiff = ($endDateTime - $startDateTime)/(86400*365);
        $maximunRange = Apollo_DB_Schema::_APL_EVENT_MAXIMUM_YEAR;
        if($dateDiff > $maximunRange){
            $this->errors[$key_start] = sprintf( $maximumDateRange, $maximunRange,$maximunRange );
            $this->errors[$key_end] = ' ';
        }

        if ( $this->start_date && $this->end_date && Apollo_App::is_valid_date( $this->start_date) && Apollo_App::is_valid_date( $this->end_date) && ! Apollo_App::is_valid_event_date( $this->start_date, $this->end_date ) ) {
            $this->errors[$key_end] = __( 'The end date must be greater than the start date and one of them must be greater than or equal today', 'apollo' );
        }

        // Update days of week
        $days_of_week = isset( $_POST[Apollo_DB_Schema::_APOLLO_EVENT_DAYS_OF_WEEK] ) ?
            Apollo_App::clean_array_data( $_POST[Apollo_DB_Schema::_APOLLO_EVENT_DAYS_OF_WEEK] ) : '';

        if ( ! $days_of_week && !$this->isSaveDraft()) {
            $this->errors[Apollo_DB_Schema::_APOLLO_EVENT_DAYS_OF_WEEK] = sprintf( $require, __( 'Days of the week', 'apollo' ) );
        }

        if ( $this->has_error() ) {
            return false;
        }

        //old days of week
        $old_dows = maybe_unserialize( $this->get_value( Apollo_DB_Schema::_APOLLO_EVENT_DAYS_OF_WEEK ) );

		$lst_rm_days = array();//list days had been remove from days of week

		if ( $old_dows ) {
			foreach ( $old_dows as $old_dow ) {

				if ( !in_array($old_dow, $days_of_week) ) {
					$lst_rm_days[] = $old_dow;
				}

			}
		}
		//remove date unselect in db
		if ( $lst_rm_days != null ) {

            // Keep log update day in weeks
            $logObj = new Apollo_Logs("Updated day in week event_id = $this->event_id, arr_day = ".implode(',', $lst_rm_days)." ", __FUNCTION__, true);
            $logObj->logs();

			$arrdate_event = array();
			foreach ( $lst_rm_days as $val_date ) {
				$event_date = Apollo_Calendar::$arrWeeks[$val_date];
				$arrdate_event = array_merge( $arrdate_event, Apollo_Calendar::getStepsDateByDay($this->event_id, $event_date, $event_date) ) ;
			}

			if ($arrdate_event != null) {
				Apollo_Calendar::deleteEventByListDate($this->event_id , implode(',', $arrdate_event) );

                $logObj = new Apollo_Logs("Delete the list event date/time of event_id = $this->event_id, arr_date ".implode(',', $arrdate_event)." ", __FUNCTION__, true);
                $logObj->logs();
			}
		}

        // Save event dates
        update_apollo_meta( $this->event_id, $key_start, $this->start_date );
        update_apollo_meta( $this->event_id, $key_end, $this->end_date );
        update_apollo_meta( $this->event_id, Apollo_DB_Schema::_APOLLO_EVENT_DAYS_OF_WEEK, maybe_serialize( $days_of_week ) );
        $this->_reset_event_calendar();

        if ($this->isSaveDraft()) {

            /** @Ticket $15925 */
            $eventMetaData = get_apollo_meta($this->event_id, Apollo_DB_Schema::_APOLLO_EVENT_DATA , true);

            $this->event_data = is_array($eventMetaData) ? $eventMetaData : unserialize($eventMetaData);

            // Checking whether should add "User draft" text after the event title or not
            $this->event_data[Apollo_DB_Schema::_E_USER_DRAFT] = $this->checkDisplayDraftLabel();

            // Update the event meta data
            update_apollo_meta( $this->event_id, Apollo_DB_Schema::_APOLLO_EVENT_DATA, serialize($this->event_data) );

            if (!isset($_SESSION['apl-fe-draft_saved'])) {
                $_SESSION['apl-fe-draft_saved'] = true;
            }
            if (isset($_POST['save-draft-step-2b']) && $_POST['save-draft-step-2b'] == 'step-2b') {
                wp_redirect(home_url(). '/user/add-event/step-2b/'. $this->event_id );
            } else {
                wp_redirect(home_url(). '/user/add-event/step-2a/'. $this->event_id );
            }
        } else {
            wp_redirect(home_url(). '/user/add-event/step-2b/'. $this->event_id );
        }
    }

    public function submit_event()
    {
        // ticket #11047: validate nonce field
        if ( !isset($_POST[$this->nonceName]) || !wp_verify_nonce($_POST[$this->nonceName], $this->nonceAction) ) {
            $this->errors['nonce_error'] = __( 'Cannot save data. Some error occurred !', 'apollo' );
            return false;
        }

        // check user login
        if ( ! is_user_logged_in() ) {
            return false;
        }

        /* ThienLd : sql injection avoiding handler */
        // It break the editor
        //Apollo_App::sqlInjectionHandler();

        $this->set_data();
        $this->set_form_errors();

        if ( $this->has_error() ) {
            return false;
        }

        $this->save_event();

    }

    private function _add_event( $post ) {
        unset( $post['ID'] );
        $this->event_id = wp_insert_post( $post );

        /* Keep event for the next step */
        $_SESSION['added_event_id'] = $this->event_id;
    }

    private function _update_event( $post ) {

        $post['ID'] = $this->event_id;
        wp_update_post( $post );

        /** @Ticket #15217 Update eventID to list review-edit */
        if ( !is_super_admin() && !is_user_admin() && $this->event_status == 'publish' ) {
            AplEventFunction::eventUpdateListReviewEdit($this->event_id);
        }
    }

    public function save_event() {

        $this->event_desc = Apollo_App::convertCkEditorToTinyCME($this->event_desc);

        $post = array(
            'ID'            => '',
            'post_title'    => $this->getEventName(),
            'post_content'  => $this->event_desc,
            'post_excerpt'  => $this->event_summary,
            'post_author'   => $this->event_author ? $this->event_author : get_current_user_id(),
            'post_type'     => $this->type,
        );

        if ($this->isSaveDraft()) {
            $post['post_status'] = 'draft';
            if (empty($post['post_title']) && empty($post['post_content']) && empty($post['post_excerpt'])) {
                $post['post_title'] = __('no title', 'apollo');
            }
        }

        if ( (get_query_var( '_apollo_event_id' ) && is_numeric(get_query_var( '_apollo_event_id' ))) || $this->is_adding_event() ) {

            $this->_update_event( $post );
            $isess = $this->event_id;

        } // In the first time you submit an event
        else {
            $this->_add_event( $post );
        }

        $this->_update_meta_data();
        $this->_update_org();
        $this->_update_category();
        $this->_update_venue();
        /** @Ticket #14774 */
        APL_Artist_Function::saveArtistEvent($this->event_id, $this->event_artist_checked, $this->event_artist_unchecked);
        //feature image
        /** @Ticket #12729 - Remove session */
        Apollo_Submit_Form::saveFeaturedImageHandler($this->event_id, Apollo_DB_Schema::_EVENT_PT);


        $enable_photo_event = of_get_option( Apollo_DB_Schema::_ENABLE_PHOTO_EVENT ,1);
        if($enable_photo_event){
            Apollo_Submit_Form::saveGalleryHandler($this->event_id, $this->type);
            /* END - SAVE GALLERY*/
        }


        $event_id = get_query_var( '_apollo_event_id' );
        $startDate = get_apollo_meta($event_id, Apollo_DB_Schema::_APOLLO_EVENT_START_DATE, true);
        $endDate = get_apollo_meta($event_id, Apollo_DB_Schema::_APOLLO_EVENT_END_DATE, true);

        if ($this->isSaveDraft()) {
            /** @Ticket $15925 */
            if (!isset($_SESSION['apl-fe-draft_saved'])) {
                $_SESSION['apl-fe-draft_saved'] = true;
            }

            wp_redirect(home_url(). '/user/add-event/step-1/'. ($this->event_id == 0 ? '' : $this->event_id) );
        } else {
            wp_redirect(home_url(). '/user/add-event/step-2a/'. $event_id );

            if ($startDate && $endDate) {
                wp_redirect(home_url(). '/user/add-event/step-2b/'. $event_id );
            } else {
                wp_redirect(home_url(). '/user/add-event/step-2a/'. $event_id );
            }
        }

    }

    /**
     * @Ticket #15925 - Check display ' - User draft' after event title.
     * @return boolean
     */
    private function checkDisplayDraftLabel() {

        if($this->isSaveDraft() || $this->event_status == 'draft'){
            // Author must be a subscriber
            if (!empty($this->event_author)) {
                $author =  new WP_User($this->event_author);
            } else {
                $event = get_post( $this->event_id );
                $author =  new WP_User( $event->post_author);
            }

            if (!in_array('subscriber', $author->roles)) {
                return 0;
            }

            /** If user role is subscriber then update event status is 'User draft' */
            return apollo_get_current_user_role() == 'Subscriber' ? 1 : 0;
        }

        return 0;
    }

    private function _update_meta_data() {

        $discountUrl = Apollo_App::apollo_get_meta_data($this->event_id,  Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL, Apollo_DB_Schema::_APOLLO_EVENT_DATA);

        $this->event_data[Apollo_DB_Schema::_APL_EVENT_POST_ICONS] = Apollo_App::apollo_get_meta_data($this->event_id,Apollo_DB_Schema::_APL_EVENT_POST_ICONS,Apollo_DB_Schema::_APOLLO_EVENT_DATA);

        $this->event_data[Apollo_DB_Schema::_APL_EVENT_ADDITIONAL_TIME] = Apollo_App::apollo_get_meta_data($this->event_id,Apollo_DB_Schema::_APL_EVENT_ADDITIONAL_TIME,Apollo_DB_Schema::_APOLLO_EVENT_DATA);
        $this->event_data[Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL] = $discountUrl;

        /** @Ticket #15925 */
        $this->event_data[Apollo_DB_Schema::_E_USER_DRAFT] = $this->checkDisplayDraftLabel();

        $tempVenue = array();
        foreach($this->tmp_venue  as $index => $value){
            if ( !empty($value ) && $index != Apollo_DB_Schema::_VENUE_WEBSITE_URL) {
                $tempVenue[$index] = stripslashes(stripslashes( $value));
            }else{
                $tempVenue[$index] = $value;
            }
        }
        $_arr_meta = array(
            Apollo_DB_Schema::_APOLLO_EVENT_DATA            => serialize( $this->event_data ),
            Apollo_DB_Schema::_APL_EVENT_TMP_VENUE          => serialize( $tempVenue ),
            Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL       => $discountUrl,
            Apollo_DB_Schema::_APL_EVENT_TMP_ORG            => $this->tmp_org,
            Apollo_DB_Schema::_APOLLO_EVENT_FREE_ADMISSION  => $this->free_event,
        );

        // Update meta data

        foreach( $_arr_meta as $key => $val ):

            /* client select the primary */
            if ( $key == Apollo_DB_Schema::_APL_EVENT_TMP_ORG && $this->_is_save_primary_org() ) {
                update_apollo_meta( $this->event_id, Apollo_DB_Schema::_APL_EVENT_TMP_ORG, '' );
                continue;
            }

            /* client select the existed venue */
            if ( $key == Apollo_DB_Schema::_APL_EVENT_TMP_VENUE && $this->_is_save_venue() ) {
                update_apollo_meta( $this->event_id, Apollo_DB_Schema::_APL_EVENT_TMP_VENUE, '' );
                continue;
            }
            update_apollo_meta( $this->event_id, $key, $val );
        endforeach;
        $this->_update_additional_field();
    }

    /**
     * Update additional data.
     */
    private function _update_additional_field() {
        //update custom field
        $group_fields = Apollo_Custom_Field::get_group_fields( Apollo_DB_Schema::_EVENT_PT );
        $customFieldValue = array();

        if(is_array($group_fields) && !empty($group_fields)){
            foreach($group_fields as $group){
                if(isset($group['fields']) && !empty($group['fields'])){
                    foreach($group['fields'] as $field){
                        if(isset($_POST[$field->name])){
                            if(is_array( $_POST[$field->name] )){
                                $customFieldValue[$field->name] = $_POST[$field->name];
                            }
                            elseif($field->cf_type == 'wysiwyg'){
                                $customFieldValue[$field->name] = Apollo_App::convertCkEditorToTinyCME($_POST[$field->name]);
                            }
                            else{
                                $customFieldValue[$field->name] = Apollo_App::clean_data_request($_POST[$field->name]);
                            }
                            if(Apollo_Custom_Field::has_explain_field($field)){
                                $exFieldName = $field->name.'_explain';
                                $customFieldValue[$exFieldName] = Apollo_App::clean_data_request($_POST[$exFieldName]);
                            }

                            if(Apollo_Custom_Field::has_other_choice($field)){
                                $exFieldName = $field->name.'_other_choice';
                                $customFieldValue[$exFieldName] = Apollo_App::clean_data_request($_POST[$exFieldName]);
                            }

                        } else if ($this->event_id) {
                            $customFieldValue[$field->name] = Apollo_App::apollo_get_meta_data($this->event_id, $field->name, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA);
                        }
                    }
                }
            }
        }
        update_apollo_meta($this->event_id, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA, serialize($customFieldValue));
    }

    private function _reset_event_calendar()  {
        if(!empty($this->event_id))  {
            if($this->start_date !== $this->ostart_date || $this->end_date !== $this->oend_date ) {

                // Keep log update day in weeks
                $logObj = new Apollo_Logs("Delete event date/time in range event_id = $this->event_id, "
                    . " start_date = $this->start_date, end_date = $this->end_date, "
                    . " old_start_date = $this->ostart_date, old_end_date = $this->oend_date ", __FUNCTION__, true);
                $logObj->logs();

                Apollo_Calendar::deleteInRange($this->event_id,$this->start_date,$this->end_date,$this->ostart_date,$this->oend_date );
            }
        }
    }

    private function _update_org() {

        if (is_array($this->co_org)) {
            array_unshift( $this->co_org , $this->r_org );
        }

        // Save org in event org table
        $apl_query  = new Apl_Query( Apollo_Tables::_APL_EVENT_ORG );
        $org_field  = Apollo_DB_Schema::_APL_E_ORG_ID;

        // Delete exist
        $apl_query->delete( " post_id = {$this->event_id}" );

        // Client enter the tmp org so we don't need to save primary from the submitted data
        if ( ! $this->_is_save_primary_org() ) return false;

        if ( ! $this->co_org ) return false;

        $i = 0;
        foreach ( $this->co_org as $org_id ):

            if ( ! $org_id || ! get_post_status( $org_id ) ) {
                continue;
            }

            $apl_query->insert( array(
                'post_id'  => $this->event_id,
                $org_field => $org_id,
                Apollo_DB_Schema::_APL_E_ORG_IS_MAIN => ++$i == 1 ? 'yes' : 'no',
                Apollo_DB_Schema::_APL_E_ORG_ORDERING => $i,
            ));
        endforeach;
    }

    private function _update_venue() {
        if ( $this->_is_save_venue() ) {
            update_apollo_meta( $this->event_id, Apollo_DB_Schema::_APOLLO_EVENT_VENUE, $this->venue );
        } else {
            update_apollo_meta( $this->event_id, Apollo_DB_Schema::_APOLLO_EVENT_VENUE, '' );
        }

    }

    private function _update_category() {

        $_input_term = is_array( $this->add_cat ) ? $this->add_cat : array();
        $_output_term = array();

        // Set tags
        /** @Ticket #17452 Disabled Tags section */
        if (of_get_option(Apollo_DB_Schema::_ENABLE_TAGS_SECTION_FE_FORM, true)) {
            $_input_tags = is_array( $this->event_tags ) ? $this->event_tags : array();
            wp_set_post_terms( $this->event_id, array_unique($_input_tags) , Apollo_DB_Schema::_EVENT_TAG );
        }

        array_unshift( $_input_term ,  $this->pri_cat );

        if ( $_input_term ):
            foreach ( $_input_term as $term_id ):
                if ( !get_term( $term_id, $this->type. '-type' ) ) continue;
                $_output_term[] = $term_id;
            endforeach;
        endif;

        if ( ! $_output_term ) return false;

        // Update taxonomies for event
        wp_set_post_terms( $this->event_id, $_output_term , $this->type. '-type' );

        // Update primary event
        if ( $this->pri_cat && get_term( $this->pri_cat, $this->type. '-type' ) ) {
            update_apollo_meta( $this->event_id, Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID, $this->pri_cat );
        }
    }
    public function re_edit_step2() {


        // Set apollo event meta data
        $this->start_date = get_apollo_meta( $this->event_id, Apollo_DB_Schema::_APOLLO_EVENT_START_DATE, true );
        $this->end_date  = get_apollo_meta( $this->event_id, Apollo_DB_Schema::_APOLLO_EVENT_END_DATE, TRUE );
        $this->days_of_week  = get_apollo_meta( $this->event_id, Apollo_DB_Schema::_APOLLO_EVENT_DAYS_OF_WEEK, TRUE );

        $this->ostart_date = $this->start_date;
        $this->oend_date  = $this->end_date;

    }
    public function re_edit() {

        $event = get_event( get_post( $this->from_id ? $this->from_id : $this->event_id ) );

        $this->event_status = $event->post->post_status;
        $this->event_author = $event->post->post_author;

        $this->setEventName($event->get_title(true));
        $this->event_desc  = $event->post->post_content;
        $this->event_summary  = strip_tags($event->post->post_excerpt);

        // Set apollo event meta data
        $this->event_data = maybe_unserialize( $event->get_meta_data( Apollo_DB_Schema::_APOLLO_EVENT_DATA ) );

        $this->free_event = $event->get_meta_data( Apollo_DB_Schema::_APOLLO_EVENT_FREE_ADMISSION );
        $this->tmp_org = $event->get_meta_data( Apollo_DB_Schema::_APL_EVENT_TMP_ORG );

        $orgs = $event->all_event_orgs();

        if ( $orgs ) {
            foreach ( $orgs as $o ) {
                if ( $o->{Apollo_DB_Schema::_APL_E_ORG_IS_MAIN} == 'yes' ) {
                    $this->r_org = $o->ID;
                    continue;
                }
                $this->co_org[] = $o->ID;
            }
        }

        $terms =  wp_get_post_terms( $this->event_id ? $this->event_id : $this->from_id, Apollo_DB_Schema::_EVENT_PT. '-type' );

        $this->pri_cat = $event->get_meta_data( Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID );
        if ( $terms ) {
            foreach ( $terms as $t ) {
                $this->add_cat[] = $t->term_id;
            }
        }

        $this->venue = $event->get_meta_data( Apollo_DB_Schema::_APOLLO_EVENT_VENUE );
        $this->tmp_venue = maybe_unserialize( $event->get_meta_data( Apollo_DB_Schema::_APL_EVENT_TMP_VENUE ) );


        /*
         * Initial image data whe edit and copy event
         */
        $isess = $this->from_id ? intval($this->from_id) : intval($this->event_id);

        /**
         * Clone data image
         */
        if ( $this->from_id && ( ! isset( $_SESSION['apollo']['copy_img'] ) || $_SESSION['apollo']['copy_img'] == false ) ) {
            $_SESSION['apollo']['copy_img'] = true;
            $this->_clone_img($this->from_id);
        }

        // Get tags
        $this->event_tags = wp_get_post_terms($this->event_id, Apollo_DB_Schema::_EVENT_TAG);
    }

    private function _clone_img($pid) {
        // Prepare thumbnail data

        $img_id = get_post_thumbnail_id( $pid );

        // get primary image
        if(!empty($img_id)) {
            $this->attachment_id = $img_id;
            $image_array = wp_get_attachment_image_src( $img_id, 'full' );
            $image = get_attached_file($img_id);

            /* rename filename */
            $upload_path = Apollo_App::getUploadBaseInfo('path');
            $upload_url = Apollo_App::getUploadBaseInfo('url');
            $uname = wp_unique_filename( $upload_path, basename($image) );

            $filename2 = $upload_path. '/' . $uname;

            @copy( $image, $filename2 );

            $url = $upload_url . '/' . $uname;
            $_SESSION['apollo']['event'.Apollo_SESSION::SUFFIX_ADD_PIMAGE][0] = array(
                'file'  => $filename2,
                'oname' => $uname,
                'type'  => get_post_mime_type( $img_id ),
                'url'   => $url,
            );

        }

        $meta_key = Apollo_App::getMetaKeyGalleryImgByTarget('event');
        $sids = get_apollo_meta($pid, $meta_key, true);

        $sids = trim($sids);
        if(!empty($sids)) {

            $arrId = explode(",", $sids);
            foreach($arrId as $_ => $id) {

                $image_array = wp_get_attachment_image_src( $id, 'full' );
                $url = $image_array[0];
                $image = get_attached_file($id);
                $_meta_data = wp_get_attachment_metadata($id);
                $caption_gallerys[] = $_meta_data['image_meta']['caption'];

                $upload_path = Apollo_App::getUploadBaseInfo('path');
                $upload_url = Apollo_App::getUploadBaseInfo('url');

                $uname = wp_unique_filename($upload_path, basename($image));

                @copy($image, $upload_path . '/' . $uname);

                $_SESSION['apollo']['event' . Apollo_SESSION::SUFFIX_ADD_GALLERY][0][$id . 'u'] = array(
                    'file' => $upload_path . '/' . $uname,
                    'oname' => $uname,
                    'type' => get_post_mime_type( $id ),
                    'url' => $upload_url . '/' . $uname,
                );
            }

            $_POST['caption_gallerys'] = $caption_gallerys;
        }

    }

    public function set_data() {
        $ipt = $_POST;

        $this->setEventName(isset( $ipt['event_name'] ) ? Apollo_App::clean_data_request( $ipt['event_name'] ) : '');
        $this->event_desc  = isset( $ipt['event_desc'] ) ? $ipt['event_desc'] : '';

        $this->event_summary  = isset( $ipt['event_summary'] ) ? Apollo_App::clean_data_request( $ipt['event_summary'] ) : '';

        $event_desc_placeholder = __( 'Event Description', 'apollo' );

        $ipt[Apollo_DB_Schema::_APOLLO_EVENT_DATA][Apollo_DB_Schema::_ADMISSION_DETAIL] = Apollo_App::convertCkEditorToTinyCME($ipt[Apollo_DB_Schema::_APOLLO_EVENT_DATA][Apollo_DB_Schema::_ADMISSION_DETAIL]);
        $ipt[Apollo_DB_Schema::_APOLLO_EVENT_DATA][Apollo_DB_Schema::_E_ACB_INFO] = Apollo_App::convertCkEditorToTinyCME($ipt[Apollo_DB_Schema::_APOLLO_EVENT_DATA][Apollo_DB_Schema::_E_ACB_INFO   ]);

        if (strip_tags( $this->event_desc ) == $event_desc_placeholder.'(*)' ) $this->event_desc = '';

        // Set apollo event meta data
        if ( isset( $ipt[Apollo_DB_Schema::_APOLLO_EVENT_DATA] ) ) {
            $_arr_video = array();
            //enable video
            $enable_video_event = of_get_option( Apollo_DB_Schema::_ENABLE_VIDEO_EVENT,1 );
            if($enable_video_event){
                $video_embed = isset( $ipt['video_embed'] ) ? $ipt['video_embed'] : '';
                $video_desc = isset( $ipt['video_desc'] ) ? $ipt['video_desc'] : '';

                if ( $video_embed ) {
                    $i = 0;
                    foreach ( $video_embed as $v ) {
                        $_arr_video[] = array( 'embed' => $v, 'desc' => isset( $video_desc[$i] ) ?  $video_desc[$i] : '' );
                        $i++;
                    }
                }
            } else {
                $_arr_video =   Apollo_App::apollo_get_meta_data($this->event_id,Apollo_DB_Schema::_VIDEO,Apollo_DB_Schema::_APOLLO_EVENT_DATA);
            }

            $this->event_data = Apollo_App::clean_array_data($ipt[Apollo_DB_Schema::_APOLLO_EVENT_DATA]);
            $this->event_data[Apollo_DB_Schema::_VIDEO] = Apollo_App::clean_array_data($_arr_video);
        }

        $this->start_date  = isset( $ipt[Apollo_DB_Schema::_APOLLO_EVENT_START_DATE] ) ?
            Apollo_App::clean_data_request( $ipt[Apollo_DB_Schema::_APOLLO_EVENT_START_DATE] ) : '';

        $this->end_date  = isset( $ipt[Apollo_DB_Schema::_APOLLO_EVENT_END_DATE] ) ?
            Apollo_App::clean_data_request( $ipt[Apollo_DB_Schema::_APOLLO_EVENT_END_DATE] ) : '';

        $this->free_event = isset( $ipt[Apollo_DB_Schema::_APOLLO_EVENT_FREE_ADMISSION] ) ?
            Apollo_App::clean_data_request( $ipt[''.Apollo_DB_Schema::_APOLLO_EVENT_FREE_ADMISSION.''] ) : '';

        $this->tmp_org = isset( $ipt[Apollo_DB_Schema::_APL_EVENT_TMP_ORG] ) ?
            Apollo_App::clean_data_request( $ipt[''.Apollo_DB_Schema::_APL_EVENT_TMP_ORG.''] ) : '';

        $org_key = Apollo_DB_Schema::_APOLLO_EVENT_ORGANIZATION;

        $this->r_org = isset( $ipt[$org_key][0] ) ? $ipt[$org_key][0] : '';
        unset( $ipt[$org_key][0] );
        $this->co_org = isset($ipt[$org_key]) && is_array($ipt[$org_key]) ? array_values( $ipt[$org_key] ) : array();


        $org_tmp = Apollo_DB_Schema::_APL_EVENT_TMP_ORG;
        $this->tmp_org = isset( $ipt[$org_tmp] ) ?
            Apollo_App::clean_data_request( $ipt[''.$org_tmp.''] ) : '';

        $this->pri_cat = isset( $ipt[Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID] ) ?
            Apollo_App::clean_data_request( $ipt[''.Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID.''] ) : '';

        $this->add_cat = isset( $ipt['add_cat'] ) ?
            $ipt['add_cat']: '';

        $this->event_tags = isset( $ipt['tags'] ) ?
            $ipt['tags']: '';

        $this->venue = isset( $ipt[Apollo_DB_Schema::_APOLLO_EVENT_VENUE] ) ?
            Apollo_App::clean_data_request( $ipt[''.Apollo_DB_Schema::_APOLLO_EVENT_VENUE.''] ) : '';

        if ( isset( $ipt[Apollo_DB_Schema::_APL_EVENT_TMP_VENUE] ) ) {
            /**
             * @ticket #16898: [CF] 20180730 - [Event admin] Please add latitude/longitude data to the 'temp' venue - Item 4
             * Backup lat lng of a venue because they are not allowed updating from the FE form
             */
            $tmpVenueLat = isset($this->tmp_venue[Apollo_DB_Schema::_VENUE_LATITUDE]) ? $this->tmp_venue[ Apollo_DB_Schema::_VENUE_LATITUDE] : '';
            $tmpVenueLng = isset($this->tmp_venue[Apollo_DB_Schema::_VENUE_LONGITUDE]) ? $this->tmp_venue[ Apollo_DB_Schema::_VENUE_LONGITUDE] : '';
            $this->tmp_venue = $ipt[Apollo_DB_Schema::_APL_EVENT_TMP_VENUE];
            $this->tmp_venue[Apollo_DB_Schema::_VENUE_LATITUDE] = $tmpVenueLat;
            $this->tmp_venue[Apollo_DB_Schema::_VENUE_LONGITUDE] = $tmpVenueLng;
        }
        /** @Ticket #14774 */
        if ( isset( $ipt['apl-event-checked-list-art'] ) ) {
            $this->event_artist_checked = Apollo_App::clean_data_request($ipt['apl-event-checked-list-art']);
        }
        if ( isset( $ipt['apl-event-unchecked-list-art'] ) ) {
            $this->event_artist_unchecked = Apollo_App::clean_data_request($ipt['apl-event-unchecked-list-art']);
        }
    }

    public function has_error() {
        return ! empty( $this->errors );
    }

    public function set_form_errors() {

        $require = __( '%s is required', 'apollo' );
        $valid   = __( '%s is invalid', 'apollo' );


        if ( ! $this->getEventName() ) {
            if (!$this->isSaveDraft()) {
                $this->errors['event_name'] = sprintf( $require, __( 'Event name', 'apollo' ) );
            }
        }
        else if(strlen( $this->getEventName() ) > 100 ) {
            $this->errors['event_name'] = __( 'Event name exceed 100 characters', 'apollo' );
        }

        $maxNumSum = of_get_option(Apollo_DB_Schema::_APL_EVENT_CHARACTERS_SUM, 250);
        if(strlen( $this->event_summary ) > $maxNumSum ) {
            $this->errors['event_summary'] = sprintf(__( 'Event Summary exceed %s characters', 'apollo' ), $maxNumSum);
        }

        if ( ! $this->event_desc && !$this->isSaveDraft()) {
            $this->errors['event_desc'] = sprintf( $require, __( 'Event Description', 'apollo' ) );
        }

        $key = Apollo_DB_Schema::_WEBSITE_URL;
        if ( isset( $this->event_data[$key] ) && $this->event_data[$key] ) {
            if (!filter_var($this->event_data[$key], FILTER_VALIDATE_URL)) {
                $this->errors[$key] = sprintf( $valid, __( 'Website Url' ) );
            }
        }

        $key = Apollo_DB_Schema::_E_CONTACT_EMAIL;

        if ( (! isset( $this->event_data[$key] ) || ! $this->event_data[$key]) && !$this->isSaveDraft() ) {
            $this->errors[$key] = sprintf( $require, __( 'Contact Email', 'apollo' ) );
        } else if (!empty($this->event_data[$key]) && ! filter_var( $this->event_data[$key], FILTER_VALIDATE_EMAIL ) ) {
            $this->errors[$key] = sprintf( $valid, __( 'Contact Email', 'apollo' ) );
        }

        $key = Apollo_DB_Schema::_E_CONTACT_NAME;
        if ( (!isset($this->event_data[$key]) || ! $this->event_data[$key]) && !$this->isSaveDraft() ) {
            $this->errors[$key] = sprintf( $require, __( 'Contact Name', 'apollo' ) );
        }

        $key = Apollo_DB_Schema::_ADMISSION_TICKET_EMAIL;
        if ( isset( $this->event_data[$key] ) && $this->event_data[$key]
            && ( ! filter_var( $this->event_data[$key], FILTER_VALIDATE_EMAIL ) ) ) {
            $this->errors[$key] = sprintf( $valid, __( 'Ticket / Infomation Email', 'apollo' ) );
        }

        $key = Apollo_DB_Schema::_ADMISSION_TICKET_URL;
        if ( isset( $this->event_data[$key] ) && $this->event_data[$key]
            && ( ! filter_var( $this->event_data[$key], FILTER_VALIDATE_URL ) ) ) {
            $this->errors[$key] = sprintf( $valid, __( 'Ticket Website', 'apollo' ) );
        }

        $key = Apollo_DB_Schema::_VENUE_WEBSITE_URL;
        if ( isset( $this->tmp_venue[$key] )
            && $this->tmp_venue[$key]
            && ! filter_var( $this->tmp_venue[$key], FILTER_VALIDATE_URL ) ) {
            $this->errors[$key] = sprintf( $valid, __( 'Venue Website', 'apollo' ) );
        }

        /* Set org error */
        if (!$this->isSaveDraft()){
            if ( $this->_is_save_primary_org() ) {
                if ( ! $this->r_org )
                    $this->errors[Apollo_DB_Schema::_APOLLO_EVENT_ORGANIZATION] = sprintf( $require, __( 'Registered Organization', 'apollo' ) );
            } else if ( ! $this->tmp_org ) {
                $this->errors[Apollo_DB_Schema::_APL_EVENT_TMP_ORG] = sprintf( $require, __( 'Organization Name', 'apollo' ) );
            }
        }

        if ( ! $this->pri_cat && !$this->isSaveDraft()) {
            $this->errors[Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID] = sprintf( $require, __( 'Primary Category' ) );
        }

        /* Set error venue */
        if (!$this->isSaveDraft()) {
            if ( $this->_is_save_venue() ) {
                if ( ! $this->venue ) {
                    $this->errors[Apollo_DB_Schema::_APOLLO_EVENT_VENUE] = sprintf( $require, __( 'Venue', 'apollo' ) );
                }
            } else {
                $key = Apollo_DB_Schema::_VENUE_NAME;
                if ( ! isset( $this->tmp_venue[$key] ) || ! $this->tmp_venue[$key] ) {
                    $this->errors[$key] = sprintf( $require, __( 'New Venue Name', 'apollo' ) );
                }
            }
        }
        
        $videos = $this->event_data[Apollo_DB_Schema::_VIDEO];
        
        if ( $videos ) {
            $_arr = array();
            $i = 0;
            foreach ( $videos as $v ) {
                if ( $v['embed'] && ! Apollo_App::is_youtube_url( $v['embed'] ) && !Apollo_App::is_vimeo( $v['embed'] ) ) {
                    $this->errors['video'. $i] = __( 'This field required youtube or vimeo link', 'apollo' );
                } else if ( isset( $this->errors['video'. $i] ) ) {
                     unset( $this->errors['video'. $i] );
                }
                $i++;
            }
        } 
      
    }
    
    public function the_errors() {
        if ( ! $this->errors ) return false;

        if ( isset($this->errors['nonce_error']) ) {
            return '<span class="error"> '. $this->errors['nonce_error'] . '</span>';
        }
        return '<span class="error wrap"> * '.__( 'There are some errors in your submitted data. Please check them again !', 'apollo' ).'</span>';
    }
    
    public function the_error_class( $field ) {
        if ( isset( $this->errors[$field] ) ) return 'inp-error';
    }
    
    public function the_field_error( $field ) {
        
        if ( $this->errors && isset( $this->errors[$field] ) && $this->errors[$field] ) {
            return '<span class="error"> '.$this->errors[$field].'</span>';
        }
    }

    public function getErrorByFieldName( $fieldName ) {

        if ( isset( $this->errors[$fieldName] ) && $this->errors[$fieldName] ) {
            return $this->errors[$fieldName];
        }
        return "";
    }

    public function get_value( $field, $parent = '' ) {

        if ( $parent == Apollo_DB_Schema::_APOLLO_EVENT_DATA ) {
            $parent = 'event_data';
        }

        if ( $parent == Apollo_DB_Schema::_APL_EVENT_TMP_VENUE ) {
            $parent = 'tmp_venue';
        }
        
        switch ( $field ) {
            case Apollo_DB_Schema::_APOLLO_EVENT_START_DATE:
                $field = 'start_date';
            break;
        
            case Apollo_DB_Schema::_APOLLO_EVENT_END_DATE:
                $field = 'end_date';
            break;
        
            case Apollo_DB_Schema::_APOLLO_EVENT_FREE_ADMISSION:
                $field = 'free_event';
            break;
        
            case Apollo_DB_Schema::_APOLLO_EVENT_ORGANIZATION:
                $field = 'r_org';
            break;
        
            case Apollo_DB_Schema::_APL_EVENT_TMP_ORG:
                $field = 'tmp_org';
            break;
        
            case Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID:
                $field = 'pri_cat';
            break;
        
            case Apollo_DB_Schema::_APOLLO_EVENT_ORGANIZATION:
                $field = 'r_org';
            break;
        
            case Apollo_DB_Schema::_APOLLO_EVENT_VENUE:
                $field = 'venue';
            break;
            
            case Apollo_DB_Schema::_APOLLO_EVENT_DAYS_OF_WEEK:
                $field = 'days_of_week';
            break;
        }
        
        if ( ! $parent ) return isset( $this->{$field} ) ? $this->{$field} : '';
        $parent = $this->{$parent};
        return ( isset( $parent[$field] ) ) ? $parent[$field] : '';
    }
    
    public function the_input( $key, $label, $parent_key = '', $check_error = false, $star = '(*)', $val = '', $classes = '', $data_attributes = array() ) {
        
        if ( ! $val ) {
            $val = $this->get_value( $key, $parent_key );
        }
        
        if ( $check_error ) {
            $error_msg = $this->the_field_error( $key, $parent_key );
            $error_class = $this->the_error_class( $key );
            
        } else {
            $error_msg = '';
            $error_class = '';
        }

        if(!empty($data_attributes)){
            $dataAttrValue = implode(' ',$data_attributes);
        } else {
            $dataAttrValue = '';
        }

        if ( $parent_key ) $key = $parent_key.'['. $key .']';
        echo '<div class="el-blk full">';
        echo '<input id="'.$key.'" name="'.$key.'" type="text" placeholder="'.$label.' '.$star.'"
               value="'.  esc_attr($val).'"
               class="'.$classes.' inp-txt-event '.$error_class.'" '.$dataAttrValue.'  >'.$error_msg.'';
        echo '<div class="show-tip">'.$label.'</div>';
        echo '</div>';       
    }

    public function the_select( $key, $label, $parent_key = '', $check_error = false, $star = '(*)',$list = array(),$id = '', $val = '', $classes = '', $wrap_class='', $data_attributes=array()) {

        if ( ! $val ) {
            $val = $this->get_value( $key, $parent_key );
        }

        if ( $check_error ) {
            $error_msg = $this->the_field_error( $key, $parent_key );
            $error_class = $this->the_error_class( $key );

        } else {
            $error_msg = '';
            $error_class = '';
        }

        /** @Ticket $14143 */
        if(!empty($data_attributes)){
            $dataAttrValue = implode(' ',$data_attributes);
        } else {
            $dataAttrValue = '';
        }

        if ( $parent_key ) $key = $parent_key.'['. $key .']';
        echo '<div class="el-blk full '.$wrap_class.'">';
        ?>
        <select name="<?php echo $key ?>" id="<?php echo $id ?>"  class="event <?php echo $classes ?> <?php
        echo $error_class.'" ' .$dataAttrValue. '>'.$error_msg.'';
        ?>">
            <?php
            $str ='';
            foreach( $list as $k => $v ) {
                $selected = $val ? selected( strtolower($k), strtolower($val),false ) : '';
                $str .= '<option '.$selected.' value="'.$k.'">'.$v.'</option>';
            }
            echo $str;
            ?>
        </select>
        <?php
        echo '<div class="show-tip">'.$label.'</div>';
        echo '</div>';
    }
    
    public function get_data_form_action( $key ) {
        return ( isset( $_SESSION[$key] )  && $_SESSION[$key] == 'hide' ) 
                    || ! isset( $_SESSION[$key] ) ?
            'add' : 'hide';
    }
    
    public function get_none_style_form( $key, $valfield ) {
        
        if ( ( $this->event_id || $this->from_id ) ) {
            $nonestyle = ! $valfield ? 'style="display: none"' : '';
        } else {
            $nonestyle = ( ! isset( $_SESSION[ $key ] ) 
                || $_SESSION[$key] !='add' )  ? 'style="display: none"' : '';
        }
        
        return $nonestyle;
    }
    
    /*
     * Check should save primary or tmp org
     * * true then save existed org else save tmp
     **/
    private function _is_save_primary_org() {
        $isEnableRegORG = of_get_option(APL_Theme_Option_Site_Config_SubTab::_APL_ENABLE_EVENT_REGISTERED_ORGANIZATIONS_FIELD, 1);
        return $isEnableRegORG && ( ! isset( $_SESSION['apl_add_org_btn'] ) || $_SESSION['apl_add_org_btn'] != 'add' )
                && Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ORGANIZATION_PT );
    }
    
    /*
     * Check should save primary or tmp venue
     * true then save existed venue else save tmp
     **/
    private function _is_save_venue() {
        
        return ( ! isset( $_SESSION['apl_add_venue_btn'] ) || $_SESSION['apl_add_venue_btn'] != 'add' )
            && Apollo_App::is_avaiable_module( Apollo_DB_Schema::_VENUE_PT );
    }
    
    public function is_adding_event() {
        if ( isset( $_SESSION['added_event_id'] ) && $_SESSION['added_event_id'] ) {
            return $_SESSION['added_event_id'];
        }
        return false;
    }
    
    public function get_gallery_data_image() {
        
        $_arr_gallery = explode( ',' , get_apollo_meta( $this->event_id, 
            Apollo_DB_Schema::_APOLLO_EVENT_IMAGE_GALLERY, true ) );
        
        if ( ! $_arr_gallery ) return false;
        
        foreach ( $_arr_gallery as $attach_id ):
            
            $image_array = wp_get_attachment_image_src( $attach_id, 'full' );
            $url         = $image_array[0];
            $image       = get_attached_file( $attach_id );
            
            $_SESSION['apollo'][Apollo_SESSION::EVENT_ADD_GALLERY][$attach_id] = array(
                'file'  => $image,
                'oname' => basename( $image ),
                'type'  => get_post_mime_type( $attach_id ),
                'url'   => $url,
            );
        endforeach;
        return $_SESSION['apollo'][Apollo_SESSION::EVENT_ADD_GALLERY];
    }

    public static function getCurrentEventId()
    {
        if ( isset( $_SESSION['added_event_id'] ) && $_SESSION['added_event_id'] ) {
            return $_SESSION['added_event_id'];
        }

        if ( get_query_var('_apollo_event_id') && is_numeric(get_query_var('_apollo_event_id')) ) {
            return intval( get_query_var('_apollo_event_id') );
        }

        return 0;

    }
    
    public static function event_steps( $step = 1 ) {
        ?>
        <div class="evt-blk">
            <ul class="steps">
                <li <?php if($step==1) echo 'class="active"'; ?>><a>1</a><p><?php _e( 'Enter Event Details', 'apollo' ) ?></p></li>
                <li <?php if($step==2) echo 'class="active"'; ?>><a>2</a><p><?php _e( 'Enter Dates & Times', 'apollo' ) ?></p></li>
                <li <?php if($step==3) echo 'class="active"'; ?>><a>3</a><p><?php _e( 'Preview & Submit', 'apollo' ) ?></p></li>
            </ul>
        </div>
        <?php
    }

    public function deleteEvent()
    {
        if (
            ! isset( $_POST['_aplNonceField'] )
            || ! wp_verify_nonce( $_POST['_aplNonceField'], 'deleteEventAction' )
        ) {
            return false;
        }

        $id = (int) $this->event_id;
        $event = get_event($id);

        if (!$event->post || $event->post->post_author != get_current_user_id()) return false;

        wp_trash_post($id);
        
        return true;
    }

    /**
     * Vandd - @Ticket #12434
     * Vandd - @Ticket #12611 - 02/06/2017
     */
    public function checkOrgProfileRequired(){
        if( $this->getCurrentStep() === 1 && Apollo_App::is_avaiable_module(Apollo_DB_Schema::_ORGANIZATION_PT) ){
            $orgProfileOption = of_get_option(Apollo_DB_Schema::_ENABLE_REQUIRED_ORG_PROFILE, false);

            if($orgProfileOption && !Apollo_User::getCurrentAssociatedID(Apollo_DB_Schema::_ORGANIZATION_PT, '', array('publish'))){

                if (Apollo_App::isInputMultiplePostMode()) {
                    wp_redirect(site_url(APL_Dashboard_Hor_Tab_Options::ORG_ADD_NEW_URL. '/?required_org'));

                }else{
                    wp_redirect('/user/org/profile/?required_org');
                }
            }
        }
    }
}