<?php
/**
 * Created by IntelliJ IDEA.
 * User: tuanphp
 * Date: 24/12/2014
 * Time: 11:40
 */

class Dashboard_Scripts  {

    public  static $_imagesTmp = '_imagesTmp';

   

    public static function registerCalendarAngular()
    {
        global $isDev;
        wp_enqueue_script('angular', 'https://ajax.googleapis.com/ajax/libs/angularjs/1.4.2/angular.min.js', array('jquery'), '1.2.28', true);
        $hookName = 'apl-moment';
        if($isDev){
            $hookName = 'apollo-js-function';
        }
        wp_enqueue_script('angular-apl-calendar', APOLLO_FRONTEND_ASSETS_URI."/js/angular-apl-calendar.js", array('angular', $hookName ), '1.0.0', true);
    
    }
}
