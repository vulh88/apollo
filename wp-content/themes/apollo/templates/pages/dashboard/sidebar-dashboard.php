<div class="dsb-l nav-sidebar">
    <nav class="dsb-main-nav">
        <ul>
            <?php
                $tree_menus = Apollo_App::getArrRewriteDashboard();
                foreach($tree_menus as $request_l => $arr):
            ?>
                    <?php if(!isset($arr['childs'])): ?>
                    <li class="li-h-menu active">
                        <a href="<?php echo Apollo_App::createPermalink($request_l) ?>" class="h-menu">
                            <div class="l-ico">
                                <i class="<?php echo isset($arr['attr']) && isset($arr['attr']['class']) ?  $arr['attr']['class']: ''; ?>"></i>
                            </div>
                            <span><?php echo $arr['text'] ?></span>
                        </a>
                    </li>
                    <?php endif; ?>

                    <?php if(isset($arr['childs'])): ?>
                    <li>
                        <i class="arr fa fa-angle-right"></i>
                        <?php
                            $isForced = isset($arr['force-redirect-to']) && !empty($arr['force-redirect-to']);
                            $parentHref = $isForced ? $arr['force-redirect-to'] : '#';
                            $dataAttrForceRedirect = $isForced ? 'data-force-redirected="true"' : '';
                        ?>
                        <a href="<?php echo $parentHref; ?>" <?php echo $dataAttrForceRedirect; ?>  data-tgr="<?php echo $arr['id'] ?>" class="has-child">
                            <div class="l-ico">
                                <i class="<?php echo isset($arr['attr']) && isset($arr['attr']['class']) ?  $arr['attr']['class']: ''; ?>"></i>
                            </div>

                            <span class="<?php echo isset($arr['attr']) && isset($arr['attr']['text_class']) ?  $arr['attr']['text_class']: ''; ?>"><?php echo $arr['text'] ?></span></a>
                        <div id="#<?php echo $arr['id'] ?>" class="sub-nav">
                            <ul>
                        <?php foreach($arr['childs'] as $_request_l => $_arr ):
                                $id = isset($_arr['attr']) && $_arr['attr']['id'] ? 'id="'.$_arr['attr']['id'].'"' : '';
                                $class = isset($_arr['attr'], $_arr['attr']['class']) ? $_arr['attr']['class'] : '';
                            ?>
                                <li class="active <?php echo $class ?>"> <a <?php echo $id ?> href="<?php echo Apollo_App::createPermalink($_request_l) ?>"><?php echo isset($_arr['text']) ? $_arr['text'] : '' ?></a></li>
                        <?php endforeach; ?>
                            </ul>
                        </div>
                    </li>
                    <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </nav>
</div>