<?php
    $id = get_query_var('_apollo_classified_id');
    $classified = get_classified($id);
    $data = $classified->getDataDetail();
    $allow_comment = of_get_option(Apollo_DB_Schema::_ENABLE_COMMENT,1) && comments_open();
    $classifiedCustomLabel = Apollo_App::getCustomLabelByModuleName(Apollo_DB_Schema::_CLASSIFIED);

    $submitInstance = new Apollo_Submit_Classified('');
    if ( isset( $_POST['case_action'] ) && $_POST['case_action'] == 'submit-listing-btn' ) {
        $isSubmit = $submitInstance->submitListing($id);
    }

?>

<div class="dsb-ct">
    <div class="dsb-welc">
        <div class="events step-3">
            <div class="evt-blk">
                <div class="event-tt"><?php _e( 'PREVIEW YOUR LISTING', 'apollo' ) ?></div>
            </div>
            <div class="evt-blk">
                <p>
                    <?php _e( 'Below is the information you are about to submit. If you would like to edit any of this information you may return to the previous screen using the “Return to Edit” button below. If you are satisfied with your submission you may click “Submit Listing” below to complete the process.
                  Please note that we reserve the right to edit submissions for grammar, style and accuracy.', 'apollo' ) ?>
                </p>
            </div>

            <?php
                $templateDir = APOLLO_TEMPLATES_DIR. '/pages/dashboard/html/classified/preview';
                include( $templateDir. '/general.php');
                include( $templateDir. '/datetime.php');
                include( $templateDir. '/contact-info.php');
                include( $templateDir. '/add-fields.php');
                include( $templateDir. '/media.php');
            ?>

            <div class="evt-blk">
                <form action="" method="post" id="classified-preview-frm" name="classified-preview"
                      class="admin_form" enctype="multipart/form-data">
                    <?php wp_nonce_field($submitInstance->nonceAction, $submitInstance->nonceName, false); ?>
                    <input type="hidden" value="" name="case_action" id="case_action" />
                    <a href="javascript:apollo_submit_form( 'classified-preview', 'submit-listing-btn' )">
                        <div id="event-submit-btn"><span><?php _e( 'SUBMIT LISTING', 'apollo' ) ?></span></div>
                    </a>
                </form>

                <a href="<?php echo home_url() ?>/user/edit-classified/<?php echo $id; ?>">
                    <div id="arrow-3"><span><?php _e( 'RETURN TO EDIT', 'apollo' ) ?></span></div>
                </a>
            </div>

        </div>
    </div>
</div>