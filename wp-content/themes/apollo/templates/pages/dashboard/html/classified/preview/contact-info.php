<div class="evt-blk">
    <div class="tt-bar clearfix">
        <div class="cl"> <a>

                <i class="fa fa-user fa-3x"></i></a>
            <h4><?php _e( 'Contact Info', 'apollo' ) ?></h4>
        </div>
        <div class="line-bar" style="width: 470px;"></div>
    </div>

    <?php $email = $classified->get_meta_data( Apollo_DB_Schema::_CLASSIFIED_CONTACT_EMAIL, Apollo_DB_Schema::_APL_CLASSIFIED_DATA ) ?>
    <div class="information">
        <p>
            <label><?php _e( 'Contact name', 'apollo' ) ?>:
            </label><?php echo $classified->get_meta_data( Apollo_DB_Schema::_CLASSIFIED_CONTACT_NAME, Apollo_DB_Schema::_APL_CLASSIFIED_DATA ); ?>
        </p>
        <p>
            <label><?php _e( 'Contact email', 'apollo' ) ?>:  </label><a href="mailto:<?php echo $email; ?>" class="email-contact">
                <?php echo $email ?></a>
        </p>

        <p>
            <label><?php _e( 'Contact phone', 'apollo' ) ?>:
            </label><?php echo $classified->get_meta_data( Apollo_DB_Schema::_CLASSIFIED_CONTACT_PHONE, Apollo_DB_Schema::_APL_CLASSIFIED_DATA ); ?>
        </p>
    </div>
</div>