
<div class="art-social">
    <div class="el-blk">
<?php
        if(isset($data['email']) && !empty($data['email'])){
        ?>
        <div class="art-social-item"><i class="fa fa-envelope fa-lg">&nbsp;&nbsp;&nbsp;</i><a href="mailto:<?php echo $data['email'] ?>">
                <?php  _e('Email','apollo') ?></a>
            <div class="slash">/</div>
        </div>
        <?php } ?>

        <?php
        if(isset($data['web']) && !empty($data['web'])){
        ?>
        <div class="art-social-item"><i class="fa fa-link fa-lg">&nbsp;&nbsp;&nbsp;</i><a href="<?php echo $data['web'] ?>" target="_blank">
                <?php _e('Website','apollo') ?></a>
            <div class="slash">/</div>
        </div>
        <?php } ?>

        <?php
        if(isset($data['blog']) && !empty($data['blog'])){
        ?>
        <div class="art-social-item"><i class="fa fa-star fa-lg">&nbsp;&nbsp;&nbsp;</i><a href="<?php echo $data['blog'] ?>" target="_blank">
                <?php _e('Blog','apollo') ?></a>
            <div class="slash">/</div>
        </div>
        <?php } ?>


        <?php
        if(isset($data['facebook']) && !empty($data['facebook'])){
        ?>
        <div class="art-social-item"><i class="fa fa-facebook fa-lg">&nbsp;&nbsp;&nbsp;</i><a href="<?php echo $data['facebook'] ?>" target="_blank">
                <?php _e('Facebook','apollo') ?></a>
            <div class="slash">/</div>
        </div>
        <?php } ?>

        <?php
        if(isset($data['twitter']) && !empty($data['twitter'])){
        ?>
        <div class="art-social-item"><i class="fa fa-twitter fa-lg">&nbsp;&nbsp;&nbsp;</i><a href="<?php echo $data['twitter'] ?>" target="_blank">
                <?php _e('Twitter','apollo') ?></a>
            <div class="slash">/</div>
        </div>
        <?php } ?>

        <?php
        if(isset($data['inst']) && !empty($data['inst'])){
        ?>
        <div class="art-social-item"><i class="fa fa-camera-retro fa-lg">&nbsp;&nbsp;&nbsp;</i><a href="<?php echo $data['inst'] ?>" target="_blank">
                <?php _e('Instagram','apollo') ?>  </a>
            <div class="slash">/</div>
        </div>
        <?php } ?>

        <?php
        if(isset($data['linked']) && !empty($data['linked'])){
        ?>
        <div class="art-social-item"><i class="fa fa-linkedin-square fa-lg">&nbsp;&nbsp;&nbsp;</i><a href="<?php echo $data['linked'] ?>" target="_blank">
                <?php _e('LinkedIn','apollo') ?>   </a></div>
        <?php } ?>
    </div>
    <div class="el-blk location">
        <?php
        if(isset($data['name']) && !empty($data['name'])){
            ?>
            <p><span><i class="fa fa-user">&nbsp;&nbsp;&nbsp;</i></span>
                <label><?php echo $data['name']; ?></label>
            </p>
        <?php } ?>
        <?php
            if(isset($data['address']) && !empty($data['address'])){
        ?>
        <p><span><i class="fa fa-map-marker fa-lg">&nbsp;&nbsp;&nbsp;</i></span>
            <label><?php echo $data['address']; ?></label>
        </p>
        <?php } ?>
        <?php
        if(isset($data['phone']) && !empty($data['phone'])){
        ?>
        <p><span><i class="fa fa-phone fa-lg">&nbsp;&nbsp;</i></span>
            <label><?php echo $data['phone']; ?></label>
        </p>
        <?php } ?>
        <?php
        if(isset($data['fax']) && !empty($data['fax'])){
        ?>
        <p><span><i class="fa fa-fax fa-lg">&nbsp;</i></span>
            <label><?php echo $data['fax']; ?></label>
        </p>
        <?php } ?>
    </div>
</div>
