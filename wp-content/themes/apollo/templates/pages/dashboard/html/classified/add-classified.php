<?php
//check is_active module here
if( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_CLASSIFIED )){
    //load page content here
    $post = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ADD_CLASSIFIED));
    ?>
    <div class="dsb-ct" id="add-classified">
    <?php
        $formClassified = new Apollo_Classified_Form(
            'Classified-frm', //form Id
            'Classified-frm-name', //form name
            'POST', //form method
            $_SERVER['REQUEST_URI'], //action url
            array(), //external form elements
            $_SERVER['REQUEST_METHOD'] //current request method
        );
        //set process and validate class for this form
        $formClassified->setOptionValidateClass('Apollo_Submit_Classified');
        $formClassified->setFormDescription('<p>'.$post->post_content.'</p>');
        $formClassified->renderForm();
    ?>
    </div>
<?php } ?>


