<?php
    $gallerys  = $classified->getListImages();
    /**
     * @ticket #19131: Octave Theme - Change all detail page section labels same the section labels on the homepage - item 2
     */
    $notApplySectionLabel = false;
    ob_start();
    include APOLLO_TEMPLATES_DIR. '/content-single/common/video-image-gallery.php';
    $content = ob_get_clean();
    
    if (trim($content) !== '' ):
?>
<div class="blog-bkl">
    <div class="a-block">
        <?php echo $content; ?>
    </div>
</div>
<?php endif; ?>