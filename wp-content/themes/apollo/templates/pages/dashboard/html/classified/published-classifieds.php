<?php
//check is_active module here
if( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_CLASSIFIED )){
    $classified_id = get_query_var('_apollo_classified_id', 0);
    if(intval($classified_id) !== 0) {
        Apollo_Permission_System::processPermission(array(
            'type' => 'access',
            'user_id' => get_current_user_id(),
            'event_id' => $classified_id,
            'fail_action' => 'redirect',
            'fail_data' => array(
            ),
            'post_type' => Apollo_DB_Schema::_CLASSIFIED
        ));
    }

    ?>
    <div class="dsb-ct">
        <div class="dsb-welc">
            <div class="events">

                <div class="evt-blk add-artist-event clearfix classified-listing-form"
                     data-ride="ap-load_asyn"
                     data-url="<?php echo admin_url('admin-ajax.php?action=apollo_load_edit_classified_html&pid='); ?><%id%>"
                     data-id = "<?php echo $classified_id ?>"
                     data-params = 'id'
                     data-methodinsert="append"
                     data-type="load_edit_program"
                </div>
            </div>
            <?php
            $msg = '';
            if (isset($_POST['classified_id']) && !empty($_POST['classified_id'])
                && get_post_status($_POST['classified_id'] ) == 'publish' ) {
                $classified = new Apollo_Submit_Classified('');
                if ($classified->deleteClassified($_POST['classified_id'])) {
                    $msg = __('Delete classified successfully!', 'apollo');
                }
            } ?>
            <div class="events">
                <h4 class="noti-delete-success"><?php echo $msg ?></h4>
            </div>
            <?php if($classified_id !== 0) {
                require APOLLO_TEMPLATES_DIR.'/pages/dashboard/html/classified/add-classified.php';
            }
            ?>
        </div>
    </div>

<?php }?>
