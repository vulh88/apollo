<?php
$id = Apollo_App::getClassifiedSuccessPage();
if ($id && $page = get_post(Apollo_App::getClassifiedSuccessPage())) {
    echo $page->post_content;
}
else {
    echo require_once APOLLO_ADMIN_DIR. '/_templates/submit-classified-success.php';
}