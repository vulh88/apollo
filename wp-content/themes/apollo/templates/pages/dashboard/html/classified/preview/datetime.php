<div style="margin-top:-15px" class="evt-blk">
    <div class="tt-bar clearfix">
        <div class="cl"> <a><i class="fa fa-clock-o fa-3x"></i></a>
            <h4><?php _e( 'Dates & Times', 'apollo' ) ?></h4>
        </div>
        <div class="line-bar" style="width: 470px;"></div>
    </div>

    <div class="information">
        <p>
            <label><?php _e( 'Expiration date', 'apollo' ) ?>:
            </label><?php echo $classified->get_meta_data( Apollo_DB_Schema::_CLASSIFIED_EXP_DATE, Apollo_DB_Schema::_APL_CLASSIFIED_DATA ); ?>
        </p>

        <p>
            <label><?php _e( 'Deadline date', 'apollo' ) ?>:
            </label><?php echo $classified->get_meta_data( Apollo_DB_Schema::_CLASSIFIED_DEADLINE_DATE, Apollo_DB_Schema::_APL_CLASSIFIED_DATA ); ?>
        </p>
    </div>
</div>