<?php
    $dataParking = isset($data['data_parking'])?$data['data_parking']:array();
    $deadlineDate =  isset($data['deadline_date']) ? $data['deadline_date'] : '';
?>
<div class="blog-bkl organization">
    <h1 class="p-ttl"><?php echo $classified->get_title() ?></h1>
    <div class="meta auth classified">
        <?php
            $postedByVal = $classified->renderOrgHtml();
            echo !empty($postedByVal) ? $postedByVal . ' ;  ' : '';
        ?>
        <span><?php _e('Posted on','apollo'); ?>&nbsp;</i></span>
        <label><?php echo APL::dateUnionDateShort($classified ->get_post_data()->post_date); ?></label>
    </div>
    <?php $classified->renderIcons($data['icon_fields'], $data['icons']); ?>

    <!--Org type -->
    <?php $categories = $classified->generate_categories(); ?>
    <div class="org-type">
        <?php if ($categories) {echo $categories;}
            if( !empty($deadlineDate)){
                echo $categories ? ' - ' : '';?>
             <span><?php _e('DEADLINE : ','apollo'); ?>&nbsp;</i></span>
             <label><?php echo APL::dateUnionDateShort($data['deadline_date']); ?></label>
            <?php }
        ?>
    </div>

    <?php
    /** @Ticket #13137 */
    $image = $classified->get_image('medium', array(),
        array(
            'aw' => true,
            'ah' => true,
        ),
        'normal', '');
    ?>
    <div class="el-blk">
        <div class="art-pic <?php echo !$image ? 'no-place-holder-cate' : ''; ?>">
            <?php
            $enableImage =  of_get_option(Apollo_DB_Schema::_CLASSIFIED_ENABLE_PRIMARY_IMAGE,1);
            if($enableImage){
                echo $image;
            }
             ?>
        </div>
        <?php include(APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/classified/preview/social.php'); ?>
    </div>
    <div class="el-blk">
        <div class="art-desc apl-internal-content">
            <?php
                echo $classified->get_full_content();
            ?>
        </div>
    </div>
</div>


<?php

    //parking info + PUBLIC HOURS + PUBLIC ADMISSION FEES
    if(is_array($dataParking) && count(($dataParking))>0){
        foreach($dataParking as $k => $item){
            if(!empty($item)){
                $title = str_replace('_',' ',$k);
                $title = strtoupper($title);?>
                <div class="blog-bkl">
                    <div class="a-block">
                        <h4><?php echo $title ?></h4>
                        <div class="el-blk">
                            <p><?php echo $item ?></p>
                        </div>
                    </div>
                </div>
            <?php
            }
        }
    }
?>

