<?php

    /* @ticket #15306 */
    /* @ticket #15971 */
    $venues   = ApolloAssociationFunction::getAssociatedItemsByAgency(Apollo_Tables::_APL_AGENCY_VENUE, 'venue_id');
    $venue_id  = intval(get_query_var('_apollo_venue_id'));
    $currentTab = get_query_var('_agency_venue_tab', 'profile');
    // Get all publish educator

?>
<div class="dsb-ct">
    <div class="dsb-welc">
        <div class="events">

            <div class="evt-blk add-artist-event">

                <div class="event-list">
                    <select id="e-up-lists" class="event" name="up_event"
                            data-enable-remote="1"
                            data-source-url="apollo_agency_get_association_remote_data_to_select2_box"
                            data-post-type="<?php echo Apollo_DB_Schema::_VENUE_PT; ?>"
                    >
                        <option value=""><?php _e( 'Select venue ', 'apollo' ) ?></option>
                        <?php

                        $all_venues = array();
                        foreach( $venues as $e ):

                            if ( ! $e ) continue;
                            $all_venues[] = $e->venue_id;
                            $epost = get_post( $e->venue_id );
                            if ( !in_array($epost->post_status, array('publish', 'pending')) ) continue;
                            echo '<option '.( $venue_id == $e->venue_id ? "selected" : "" ).' value="'.$epost->ID.'">'.$epost->post_title.'</option>';
                        endforeach;
                        ?>
                    </select>
                </div>

                <div class="b-btn">
                    <a href="javascript:void(0);" class="btn-bm btn btn-b"
                       data-rise="add-agency-educator" data-eid=""
                       data-url="<?php echo home_url(). '/user/agency/venue/' ?>"
                       data-source="#e-up-lists"

                       data-msg="<?php _e( 'Please select an venue', 'apollo' ) ?>" >
                        <?php _e( 'UPDATE VENUE ', 'apollo' ) ?>
                    </a>
                </div>
            </div>
        </div>
        <?php
        $post = get_post($venue_id);

        $isValidVenue = in_array($venue_id, array_map('current', $venues));
        if (!$venue_id || !$isValidVenue || !$post || $post->post_type != Apollo_DB_Schema::_VENUE_PT
            || !in_array($post->post_status, array('publish', 'pending'))) {
            return;
        }

        echo '<div class="agency-profile">';
        $currentTab = get_query_var('_agency_venue_tab');
        switch($currentTab){
            /** @Ticket #13448 */
            case 'profile':
                require_once APOLLO_TEMPLATES_DIR.'/venue/dashboard-form/profile.php';
                break;
            case 'photo':
                require_once APOLLO_TEMPLATES_DIR.'/venue/dashboard-form//photos.php';
                break;
            case 'video':
                require_once APOLLO_TEMPLATES_DIR.'/venue/dashboard-form/videos.php';
                break;
        }
        echo '</div>';
        ?>
    </div>
</div>

<script>
    jQuery(function( $ ) {
        //var _op_val = $( 'input[name="post_title"]' ).val();
        //$( '#e-up-lists option[value=<?php echo $venue_id ?>]' ).text(_op_val);
    });
</script>
