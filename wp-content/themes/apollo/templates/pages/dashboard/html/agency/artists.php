<?php
    /* @ticket #15268 - [CF] 20180312 - [Artist] Add artist menu under the Agency module */
    /* @ticket #15971 */
    $artists   = ApolloAssociationFunction::getAssociatedItemsByAgency(Apollo_Tables::_APL_AGENCY_ARTIST, 'artist_id');
    $artistId  = intval(get_query_var('_apollo_artist_id'));
    $currentTab = get_query_var('_agency_artist_tab', 'profile');
?>

<div class="dsb-ct">
    <div class="dsb-welc">
        <div class="events">
            <div class="evt-blk add-artist-event">
                <div class="event-list">
                    <select id="e-up-lists"
                            class="event" name="up_event"
                            data-enable-remote="1"
                            data-source-url="apollo_agency_get_association_remote_data_to_select2_box"
                            data-post-type="<?php echo Apollo_DB_Schema::_ARTIST_PT; ?>"
                    >
                        <option value=""><?php _e('Select artist ', 'apollo') ?></option>

                        <?php
                            foreach($artists as $e):
                                if (!$e) continue;

                                $post = get_post($e->artist_id);
                                if ( !in_array($post->post_status, array('publish', 'pending')) ) continue;
                                echo '<option ' . ($artistId == $e->artist_id ? "selected" : "" ).' value="' . $post->ID . '">' . $post->post_title . '</option>';
                            endforeach;
                        ?>
                    </select>
                </div>

                <div class="b-btn">
                    <a href="javascript:void(0);"
                       class="btn-bm btn btn-b"
                       data-rise="add-agency-educator"
                       data-eid=""
                       data-url="<?php echo home_url() . '/user/agency/artist/' ?>"
                       data-source="#e-up-lists"
                       data-msg="<?php _e( 'Please select an artist', 'apollo' ) ?>" >
                        <?php _e('UPDATE ARTIST', 'apollo') ?>
                    </a>
                </div>
            </div>
        </div>

        <?php
            $post = get_post($artistId);
            $isValidArtist = in_array($artistId, array_map('current', $artists));
            if (!$artistId || !$isValidArtist || !$post || $post->post_type != Apollo_DB_Schema::_ARTIST_PT
                || !in_array($post->post_status, array('publish', 'pending'))) {
                return;
            }

            echo '<div class="agency-profile">';
            switch ($currentTab) {
                case 'profile':
                    require_once APOLLO_TEMPLATES_DIR . '/artists/dashboard-form/profile.php';
                    break;
                case 'photo':
                    require_once APOLLO_TEMPLATES_DIR . '/artists/dashboard-form/multiple/photos.php';
                    break;
                case 'video':
                    require_once APOLLO_TEMPLATES_DIR . '/artists/dashboard-form/video.php';
                    break;
                case 'audio':
                    require_once APOLLO_TEMPLATES_DIR . '/artists/dashboard-form/audio.php';
                    break;
                case 'events':
                    require_once APOLLO_TEMPLATES_DIR . '/artists/dashboard-form/multiple/events.php';
                    break;
            }
            echo '</div>';
        ?>
    </div>
</div>
