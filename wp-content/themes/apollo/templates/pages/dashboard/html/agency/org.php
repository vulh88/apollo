<?php

    /* @ticket #15268 */
    /* @ticket #15971 */
    $orgs   = ApolloAssociationFunction::getAssociatedItemsByAgency(Apollo_Tables::_APL_AGENCY_ORG, 'org_id');
    $org_id  = intval(get_query_var('_apollo_org_id'));
    $currentTab = get_query_var('_agency_org_tab', 'profile');

?>
<div class="dsb-ct">
    <div class="dsb-welc">
        <div class="events">                  
            
            <div class="evt-blk add-artist-event">

                <div class="event-list">
                    <select id="e-up-lists" class="event" name="up_event"
                        data-enable-remote="1"
                        data-source-url="apollo_agency_get_association_remote_data_to_select2_box"
                        data-post-type="<?php echo Apollo_DB_Schema::_ORGANIZATION_PT; ?>"
                    >
                        <option value=""><?php _e( 'Select organization ', 'apollo' ) ?></option>
                        <?php

                        $all_orgs = array();
                        foreach( $orgs as $e ):

                            if ( ! $e ) continue;
                            $all_orgs[] = $e->org_id;
                            $epost = get_post( $e->org_id );
                            if ( !in_array($epost->post_status, array('publish', 'pending')) ) continue;
                            echo '<option '.( $org_id == $e->org_id ? "selected" : "" ).' value="'.$epost->ID.'">'.$epost->post_title.'</option>';
                        endforeach;
                        ?>
                    </select>
                </div>
                
                <div class="b-btn">
                    <a href="javascript:void(0);" class="btn-bm btn btn-b" 
                       data-rise="add-agency-educator" data-eid=""
                       data-url="<?php echo home_url(). '/user/agency/org/' ?>"
                       data-source="#e-up-lists"
                    
                    data-msg="<?php _e( 'Please select an organization', 'apollo' ) ?>" >
                        <?php _e( 'UPDATE ORGANIZATION ', 'apollo' ) ?>
                    </a>    
                </div>
            </div>
        </div>
        <?php 
            $post = get_post($org_id);

            $isValidOrg = in_array($org_id, array_map('current', $orgs));
            if (!$org_id || !$isValidOrg || !$post || $post->post_type != Apollo_DB_Schema::_ORGANIZATION_PT
                || !in_array($post->post_status, array('publish', 'pending'))) {
                return;
            }
            
            echo '<div class="agency-profile">';
            $currentTab = get_query_var('_agency_org_tab');
            switch($currentTab){
                case 'profile':
                    require_once APOLLO_TEMPLATES_DIR.'/org/dashboard-form/profile.php';
                    break;
                case 'photo':
                    require_once APOLLO_TEMPLATES_DIR.'/org/dashboard-form/photos.php';
                    break;
                case 'audio':
                    require_once APOLLO_TEMPLATES_DIR.'/org/dashboard-form/audio.php';
                    break;
                case 'video':
                    require_once APOLLO_TEMPLATES_DIR.'/org/dashboard-form/video.php';
                    break;
            }
            echo '</div>';
        ?>
    </div>  
</div>

<script>
    jQuery(function( $ ) {
        //var _op_val = $( 'input[name="post_title"]' ).val();
        //$( '#e-up-lists option[value=<?php echo $org_id ?>]' ).text(_op_val);
    });
</script>
