<?php

    /* @ticket #15306 */
    global $wpdb;
    $agencyIds = ApolloAssociationFunction::getAssociatedModules(get_current_user_id(), Apollo_DB_Schema::_AGENCY_PT);

    // get all published educators
    $apl_query = new Apl_Query($wpdb->prefix . Apollo_Tables::_APL_AGENCY_EDUCATOR);
    $educators   = $apl_query->get_where("agency_id IN (" . implode(', ', $agencyIds) . " )", " DISTINCT edu_id ", " LIMIT 15 ");
    $educator_id  = intval(get_query_var('_apollo_educator_id'));
    $currentTab = get_query_var('_agency_educator_tab', 'profile');

?>
<div class="dsb-ct">
    <div class="dsb-welc">
        <div class="events">                  
            
            <div class="evt-blk add-artist-event">

                <div class="event-list">
                    <select id="e-up-lists" class="event" name="up_event">
                        <option value=""><?php _e( 'Select educator', 'apollo' ) ?></option>
                        <?php
                        
                        $all_eids = array();
                        foreach( $educators as $e ):
                            if ( ! $e ) continue;
                            $all_eids[] = $e->edu_id;
                            $epost = get_post( $e->edu_id );
                            if ( !in_array($epost->post_status, array('publish', 'pending')) ) continue;
                            echo '<option '.( $educator_id == $e->edu_id ? "selected" : "" ).' value="'.$epost->ID.'">'.$epost->post_title.'</option>';
                        endforeach;
                        ?>
                    </select>
                </div>
                
                <div class="b-btn">
                    <a href="javascript:void(0);" class="btn-bm btn btn-b" 
                       data-rise="add-agency-educator" data-eid=""
                       data-url="<?php echo home_url(). '/user/agency/educator/' ?>"
                       data-source="#e-up-lists"
                    
                    data-msg="<?php _e( 'Please select an educator', 'apollo' ) ?>" >
                        <?php _e( 'UPDATE EDUCATOR', 'apollo' ) ?>
                    </a>    
                </div>
            </div>
        </div>
        <?php 
            $post = get_post($educator_id);

            $isValidEducator = in_array($educator_id, array_map('current', $educators));
            if (!$educator_id || !$isValidEducator || !$post || $post->post_type != Apollo_DB_Schema::_EDUCATOR_PT
                || !in_array($post->post_status, array('publish', 'pending'))) {
                return;
            }
            
            echo '<div class="agency-profile">';
            require APOLLO_TEMPLATES_DIR.'/educator/dashboard-form/profile.php';
            echo '</div>';
        ?>
    </div>  
</div>

<script>
    jQuery(function( $ ) {
        var _op_val = $( 'input[name="post_title"]' ).val();
        $( '#e-up-lists option[value=<?php echo $educator_id ?>]' ).text(_op_val);
    });
</script>
