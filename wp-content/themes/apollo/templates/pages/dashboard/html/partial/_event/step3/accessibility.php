<?php
        $acc = $event->get_meta_data( Apollo_DB_Schema::_E_CUS_ACB, Apollo_DB_Schema::_APOLLO_EVENT_DATA );
        global $apollo_event_access;
        if ( $acc  ):
            
            
        ?>
<div <?php echo $is_show_accessibility_info ?>>
<div style="margin-top:-15px" class="evt-blk">
    <div class="tt-bar clearfix">
        <div class="cl"> <a>

            <i class="fa fa-book fa-3x"></i></a>
            <h4><?php _e( 'Accessibility Info', 'apollo' ) ?></h4>
        </div>
        <div class="line-bar" style="width: 470px;"></div>
    </div>
    
    <div class="information">
        
        <ul class="acces-list">
            <?php foreach ( $acc as $a ): ?>
            <li>
                <ul>
                    <li><img src="<?php echo get_template_directory_uri() ?>/assets/images/event-accessibility/2x/<?php echo $a ?>.png" width="30px" height="36px"></li>
                    <li>
                      <label><?php echo $apollo_event_access[$a] ?></label>
                    </li>
                </ul>
            </li>
            <?php endforeach; ?>
        </ul>
        <p><?php echo $event->get_meta_data( Apollo_DB_Schema::_E_ACB_INFO, Apollo_DB_Schema::_APOLLO_EVENT_DATA ) ?></p>
    </div>
    </div>
</div>
<?php endif; ?>