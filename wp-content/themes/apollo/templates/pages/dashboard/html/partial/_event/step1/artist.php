<?php
/** @Ticket #18746 - De-activate the association section */
$enableArtistsBlock = of_get_option(Apollo_DB_Schema::_EVENT_ENABLE_ARTISTS_SECTION_FE_FORM, 1);
?>
<div class="evt-blk <?php echo !$enableArtistsBlock ? 'hidden' : ''; ?>" id="artist-event-blk">
        <div class="event-contact"><?php _e('Artist', 'apollo' ) ?></div>
    <h1><?php _e( 'SELECT ARTIST', 'apollo' ) ?></h1>
    <p><?php _e( 'Please select the artists for your event.', 'apollo' ) ?></p>
    <?php
        $postID = isset($eventObj) ? $eventObj->id : '' ;
    ?>
    <div id="artist-list" post_id="<?php echo $postID; ?>">
        <div class="group-search">
            <input id="search-artists-for-event-frontend" type="text" class="inp inp-txt event-artist-frontend" placeholder="<?php _e('Search by Artist name', 'apollo'); ?>"/>
            <span id="remove-search-text" class="remove-search hidden"><i class="fa fa-times"></i></span>
        </div>
        <ul id="apollo-artist-list-in-frm">
        </ul>
        <input type="hidden" name="apl-event-checked-list-art" id="apl-checked-list-art" value=""/>
        <input type="hidden" name="apl-event-unchecked-list-art" id="apl-unchecked-list-art" value=""/>
        <div class="wrap-load-more-artist-for-event hidden">
            <a id="event-artist-load-more" class="btn-noW"><?php _e('SHOW MORE', 'apollo'); ?></a>
        </div>
    </div>
</div>