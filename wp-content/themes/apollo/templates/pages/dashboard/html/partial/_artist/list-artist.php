<?php foreach($arrdata['datas'] as $data ):
    // GET MORE DATA
    $_artist = get_artist($data->ID);
    $edit_artist_link = site_url(APL_Dashboard_Hor_Tab_Options::ARTIST_PROFILE_URL) . '/'.$data->ID;
?>
    <li>
        <div class="event-img">
            <a target="_blank" href="<?php echo $_artist->get_permalink(); ?>">
                <img src="<?php echo $_artist->get_thumb_image_url() ?>">
            </a>
        </div>
        <div class="event-info">
            <a target="_blank" href="<?php echo $_artist->get_permalink(); ?>">
                <span class="ev-tt"><?php echo $_artist->get_title() ?><?php echo isset($_artist->post) && isset($_artist->post->post_status) && $_artist->post->post_status == 'pending' ? '*' : ''; ?></span>
            </a>
            <a  class="icons-info" title="<?php _e('Edit', 'apollo') ?>" href="<?php echo $edit_artist_link; ?>"><i class="fa fa-pencil-square-o"></i></a>
        </div>
    </li>
<?php endforeach; ?>