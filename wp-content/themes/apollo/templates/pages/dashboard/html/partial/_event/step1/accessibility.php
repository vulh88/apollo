<?php 
    global $apollo_event_access;

    $_arr_val = $submit_obj->get_value( Apollo_DB_Schema::_E_CUS_ACB, Apollo_DB_Schema::_APOLLO_EVENT_DATA );

    // Default mode: "hide"
    // $data_action = $submit_obj->get_data_form_action( 'apl_add_acc_btn' );
    // $none_style = $submit_obj->get_none_style_form( 'apl_add_acc_btn', $_arr_val );

    // @ticket #11432 : change default mode to "open"
    $data_action = 'add';
    $none_style = '';
?>

<?php
//enable show Accessibility info field
$is_show_accessibility_info = '';
$enable_accessibility = of_get_option(APL_Theme_Option_Site_Config_SubTab::_APL_ENABLE_EVENT_ACCESSIBILITY_FIELD, 1);
$enable_venue_accessibility_mode = of_get_option(Apollo_DB_Schema::_ENABLE_VENUE_ACCESSIBILITY_MODE, 1);

if ($enable_accessibility ) {
    if(!$enable_venue_accessibility_mode)
        $is_show_accessibility_info = ' style="display: block !important"';
} else{
    $is_show_accessibility_info = ' style="display: none !important"';
}
?>
<div class="evt-blk access" <?php echo $is_show_accessibility_info ?>>
    <div class="event-contact"><?php _e( 'Accessibility Information', 'apollo' ) ?></div>
    <div data-mod="acc" data-action="<?php echo $data_action; ?>" class="expend-access add-cat-expand">
        <i class="fa <?php echo $none_style ? 'fa-chevron-circle-down' : 'fa-chevron-circle-up' ?> fa-3x"></i>
    </div>
    <div <?php echo $none_style; ?> class="access-list no-padding">
        <p><?php _e( 'Accessibility Details: Please add any additional information such as special performance dates/times for patrons with disabilities, specific entrance and exit information for wheelchairs and other disabled individuals, etc.', 'apollo' ) ?></p>
      <ul class="access-listing">


<?php
    $i = 0;
    $total = count( $apollo_event_access );
    $step = 6;
    ksort($apollo_event_access);
    foreach ( $apollo_event_access as $img => $label ):
        echo '<li><ul class="ACLlist">';
?>      
    <li>
        <input value="<?php echo $img ?>" <?php echo $_arr_val && in_array( $img, $_arr_val ) ? 'checked' : '' ?> 
               name="<?php echo Apollo_DB_Schema::_APOLLO_EVENT_DATA ?>[<?php echo Apollo_DB_Schema::_E_CUS_ACB ?>][]" type="checkbox">
      <img src="<?php echo get_template_directory_uri() ?>/assets/images/event-accessibility/2x/<?php echo $img ?>.png">
      <label><?php echo $label ?></label>
    </li>

      <?php echo '</ul></li>' ?>

<?php endforeach; ?>
          
      </ul>
      <?php $key = Apollo_DB_Schema::_E_ACB_INFO; ?>  
        <div class="el-blk full">
            <?php wp_editor($submit_obj->get_value( $key, Apollo_DB_Schema::_APOLLO_EVENT_DATA ), "apl-e-acb-info",
                array(
                    'editor_height' => 250,
                    'editor_class'  => $submit_obj->the_error_class( $key ),
                    'textarea_name' => Apollo_DB_Schema::_APOLLO_EVENT_DATA.'['. $key. ']'
                )); ?>

            <?php  echo $submit_obj->the_field_error( $key ); ?>
            <div class="show-tip"><?php  _e( 'Additional accessibility info', 'apollo' ) ?></div>
        </div>    
    </div>
  </div>
