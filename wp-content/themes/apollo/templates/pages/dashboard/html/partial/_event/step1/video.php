<?php 
    

    $videos = isset( $submit_obj->event_data[Apollo_DB_Schema::_VIDEO] ) && $submit_obj->event_data[Apollo_DB_Schema::_VIDEO] ? 
                $submit_obj->event_data[Apollo_DB_Schema::_VIDEO] : '';
    
    $video_val = $videos && isset( $videos[0] ) ? $videos[0]['embed'] : '';

    /**
     * Default mode: "hide"
     * $data_action = $submit_obj->get_data_form_action( 'apl_add_video_btn' );
     * $none_display = $submit_obj->get_none_style_form( 'apl_add_video_btn', $video_val );
     *
     * From @ticket #11461: change default mode to "open"
     */
    $data_action  = 'add';
    $none_display = '';

    $v0 = isset( $videos[0] ) && $videos[0] ? $videos[0] : '';
    $name = __('ADD VIDEO LINK','apollo');
    $style = "style='display:none'";
?> 
<div class="evt-blk venue">
    
    <div id="event-video" class="event-contact"><?php $text = of_get_option(Apollo_DB_Schema::EVENT_VIDEO_TEXT,Apollo_Display_Config::EVENT_VIDEO_TEXT) ;_e( $text, 'apollo' ); ?></div>
    <div data-mod="video" data-action="<?php echo $data_action; ?>" class="add-cat-expand expend-video">
        <i class="fa <?php echo $none_display ? 'fa-chevron-circle-down' : 'fa-chevron-circle-up' ?> fa-3x"></i>
    </div>
    <p class="v-desc <?php echo $none_display ? 'hidden' : ''; ?>" ><?php _e('This form accepts URL links provided by YouTube.com and Vimeo “Share” options.', 'apollo') ?></p>
    <div <?php echo $none_display; ?> class="video-area">
        <div class="des-list">
            <?php
            $required = of_get_option(Apollo_DB_Schema::_ENABLE_REQUIREMENT_EVENT_URL_FIELD) ? '(*)' : '';
            $classRequired = of_get_option(Apollo_DB_Schema::_ENABLE_REQUIREMENT_EVENT_URL_FIELD) ? 'validate[required]' : '';
                if ( isset($videos[0]['embed']) && !empty($videos[0]['embed']) ) {
                    $style = '';
                    $name = __('ADD MORE LINKS','apollo');
                    $i = 0;


                    foreach( $videos as $v ) {
                        if ( ! $v['embed'] ) continue;
                    ?>
                        <div id="video-group<?php echo $i; ?>" class="video-item">
                            <div class="el-blk full">
                                <div class="divider"></div>
                                <input type="hidden" name="<?php echo Apollo_DB_Schema::_APOLLO_EVENT_DATA ?>[<?php echo Apollo_DB_Schema::_VIDEO ?>]" />

                                <input name="video_embed[]" type="text" id="video-link" placeholder="<?php _e( 'YouTube or Vimeo URL'.$required, 'apollo' ) ?>"
                           class="inp-txt-event <?php echo $submit_obj->the_error_class( 'video'. $i ) ?> <?php echo $classRequired?>" value="<?php echo $v ? $v['embed'] : '' ?>" />

                                <?php echo $submit_obj->the_field_error( 'video'. $i ) ?>
                                <div class="show-tip"><?php _e( 'YouTube or Vimeo URL', 'apollo' ) ?></div>
                            </div>

                            <div class="el-blk full">
                                <textarea name="video_desc[]" id="video-desc" placeholder="<?php _e('Video description', 'apollo') ?>" class="desc-video"><?php echo $v['desc'] ?></textarea>
                                <div class="show-tip"><?php _e('Video description', 'apollo') ?></div>
                            </div>
                            <div class="delVideo"><i class="fa fa-times"></i></div>
                        </div>
                    <?php $i++; }
                }

            ?>
            
        </div>

        <div class="video-btn">
            <button id="video-addmore" data-img="<?php echo get_template_directory_uri() ?>/assets/images/delete-org.png" data-add = "<?php _e('ADD VIDEO LINK', 'apollo') ?>" data-addmore = "<?php _e('ADD MORE LINKS', 'apollo') ?>" class="btn-noW btn-add-row"><?php _e( $name, 'apollo' ) ?></button>
        </div>
        <div class="video-btn">
          <button id="video-preview" <?php echo $style ?> class="btn-noW btn-review" data-alert = "<?php _e('* Please input valid youtube or vimeo link.','apollo');?>"><?php _e( 'PREVIEW', 'apollo' ) ?></button>
        </div>
    </div>
</div>
<!--html group content -->
<div style="display:none" >
    <div id="video-group" class="video-item">
        <div class="el-blk full">
            <input type="hidden" name="<?php echo Apollo_DB_Schema::_APOLLO_EVENT_DATA ?>[<?php echo Apollo_DB_Schema::_VIDEO ?>]" />
            <input name="video_embed[]" type="text" id="video-link" placeholder="<?php _e( 'YouTube or Vimeo URL '.$required, 'apollo' ) ?>"
                   class="inp-txt-event <?php echo $submit_obj->the_error_class( 'video0' )  ?> <?php echo $classRequired?>" value="" />
            <div class="show-tip"><?php _e( 'YouTube or Vimeo URL', 'apollo' ) ?></div>
        </div>
        <div class="el-blk full">
            <textarea name="video_desc[]" id="video-desc" placeholder="<?php _e('Video description', 'apollo') ?>" class="desc-video"></textarea>
            <div class="show-tip"><?php _e('Video description', 'apollo') ?></div>
        </div>
        <div class="delVideo"><i class="fa fa-times"></i></div>
    </div>
</div>
<!--end html group content --->
<?php include APOLLO_TEMPLATES_DIR. '/pages/dashboard/html/partial/_common/video-box.php'; ?>
