<?php foreach($arrdata['datas'] as $data ):
    // GET MORE DATA
    $_a_artist = get_artist($data);
    $etype =  Apollo_DB_Schema::_ARTIST_PT;

    $table_meta = 'apollo_'.$etype;

    $career_info = $_a_artist->get_career_info();

    ?>
    <li>
        <div class="event-img"><a href="<?php echo $_a_artist->get_permalink() ?>"><img src="<?php echo $_a_artist->get_thumb_image_url() ?>"></a></div>
        <div class="event-checkbox">
            <input type="checkbox" autocomplete="off" name="eventid[<?php echo $etype ?>]" value="<?php echo $data->bookmark_id ?>" class="check-event"><span class="event"></span>
        </div>
        <div class="event-info">
            <a href="<?php echo $_a_artist->get_permalink() ?>">
                <span class="ev-tt">
                    <?php echo $data->post_title ?>
                </span> </a>
                <p class="meta auth">
                    <?php echo $career_info ?>
                </p>
        </div>
    </li>
<?php endforeach; ?>