<?php foreach($arrdata['datas'] as $data ):
    // GET MORE DATA
    $public_art = get_public_art($data);
    $etype =  Apollo_DB_Schema::_PUBLIC_ART_PT;

    $table_meta = 'apollo_'.$etype;
   
    $address = $public_art->get_full_address();
    ?>
    <li>
        <div class="event-img"><a href="<?php echo $public_art->get_permalink() ?>"><img src="<?php echo $public_art->get_thumb_image_url() ?>"></a></div>
        <div class="event-checkbox">
            <input type="checkbox" autocomplete="off" name="eventid[<?php echo $etype ?>]" value="<?php echo $data->bookmark_id ?>" class="check-event"><span class="event"></span>
        </div>
        <div class="event-map"><a target="_blank" href="<?php echo get_home_url() ?>/map?id=<?php echo $public_art->id ?>&type=<?php echo $etype ?>" data-address="<?php echo $address ?>"><img src="<?php echo APOLLO_FRONTEND_ASSETS_URI ?>/images/google-map.png"></a></div>
        <div class="event-info">
            <a href="<?php echo $public_art->get_permalink() ?>">
                <span class="ev-tt"><?php echo $public_art->get_title() ?></span>
            </a>
        </div>
    </li>
<?php endforeach; ?>