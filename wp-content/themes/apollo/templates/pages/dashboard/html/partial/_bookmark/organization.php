<?php foreach($arrdata['datas'] as $data ):
    // GET MORE DATA
    $_org = get_org($data);
    $etype =  Apollo_DB_Schema::_ORGANIZATION_PT;

    $table_meta = 'apollo_'.$etype;
    $start_date = get_metadata($table_meta, $data->post_id, Apollo_DB_Schema::_APOLLO_EVENT_START_DATE, true);
    $end_date = get_metadata($table_meta, $data->post_id, Apollo_DB_Schema::_APOLLO_EVENT_END_DATE, true);

    ?>
    <li>
        <div class="event-img"><a href="<?php echo $_org->get_permalink() ?>"><img src="<?php echo $_org->get_thumb_image_url() ?>"></a></div>
        <div class="event-checkbox">
            <input type="checkbox" autocomplete="off"
                   name="eventid[<?php echo $etype ?>]" value="<?php echo $data->bookmark_id ?>" class="check-event"><span class="event"></span>
        </div>
        
        <div class="event-info">
            <a href="<?php echo $_org->get_permalink() ?>">
                <span class="ev-tt"><?php echo $_org->get_title() ?></span>
            </a>
        </div>

    </li>
<?php endforeach; ?>