<div class="evt-blk">
    
    <div class="tt-bar clearfix">
        <div class="cl"> <a>

            <i class="fa fa-building-o fa-3x"></i></a>
            <h4><?php _e( 'Venue Info', 'apollo' ) ?></h4>
        </div>
        <div class="line-bar" style="width: 470px;"></div>
    </div>
    
    <div class="information">
      <p>
        <?php 
            $_arr_field_location = array(
                Apollo_DB_Schema::_VENUE_ADDRESS1,
                Apollo_DB_Schema::_VENUE_CITY,
                Apollo_DB_Schema::_VENUE_STATE,
                Apollo_DB_Schema::_VENUE_ZIP,
            );
            echo $event->get_location_name(); 
            
        ?> <br/>
        <?php echo $event->get_location_data( $_arr_field_location ); ?>
      </p>
    </div>
</div>