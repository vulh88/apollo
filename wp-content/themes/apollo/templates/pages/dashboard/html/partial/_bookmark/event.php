<?php foreach($arrdata['datas'] as $data ):
    // GET MORE DATA
    $_a_event = get_event($data);
    $etype =  Apollo_DB_Schema::_EVENT_PT;

    $table_meta = 'apollo_'.$etype;
    $start_date = get_metadata($table_meta, $data->post_id, Apollo_DB_Schema::_APOLLO_EVENT_START_DATE, true);
    $end_date = get_metadata($table_meta, $data->post_id, Apollo_DB_Schema::_APOLLO_EVENT_END_DATE, true);

    $obj_organization = $_a_event->getOrganizationEvent();
    ?>
    <li>
        <div class="event-img"><a href="<?php echo $_a_event->get_permalink() ?>"><img src="<?php echo $_a_event->get_thumb_image_url() ?>"></a></div>
        <div class="event-checkbox">
            <input type="checkbox" autocomplete="off" name="eventid[<?php echo $etype ?>]" value="<?php echo $data->bookmark_id ?>" class="check-event"><span class="event"></span>
        </div>
        <div class="event-map"><a href="<?php echo get_home_url() ?>/map/?id=<?php echo $_a_event->id ?>" target="_blank" ><img src="<?php echo APOLLO_FRONTEND_ASSETS_URI ?>/images/google-map.png"></a></div>
        <div class="event-info">
            <a href="<?php echo $_a_event->get_permalink() ?>">
                <span class="ev-tt">
                    <?php echo $data->post_title ?>
                </span> </a>
                <p class="meta auth">
                    <?php echo $_a_event->renderOrgVenueHtml() ?>
                </p>
                
                <?php if(!empty($start_date) || !empty($end_date)): ?><span class="ev-date"><?php echo APL::dateUnionDateShort($start_date, $end_date) ?></span><?php endif ?>
           
        </div>

    </li>
<?php endforeach; ?>