<div style="margin-top:-15px" class="evt-blk">
    <div class="tt-bar clearfix">
        <div class="cl"> <a><i class="fa fa-clock-o fa-3x"></i></a>
          <h4><?php _e( 'Dates & Times', 'apollo' ) ?></h4>
        </div>
        <div class="line-bar" style="width: 470px;"></div>
    </div>

    <div class="information">
        <p><i class="fa fa-calendar fa-2x"></i>
          <label height="26px"><?php _e( 'Dates', 'apollo' ) ?>:  </label><span>
              <?php echo $event->get_meta_data( Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ); ?> - 
          <?php echo $event->get_meta_data( Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ); ?></span>
        </p>
        
        <?php 
            $arr_time = $event->get_periods_time( " LIMIT ".Apollo_Display_Config::MAX_DATETIME." " );
            $in_dt = $event->render_periods_time( $arr_time );
            if ( $in_dt ):
        ?>
        <p>
          <label><?php _e( 'Individual Date & Times', 'apollo' ) ?>:</label>
        </p>
        
        <div class="clearfix">
            <ul class="ind-time list-more-time">
                <?php 
                    echo $in_dt;
                ?>
            </ul>
        </div>
        
        <?php if ( $event->have_more_periods_time() ): ?>
        <p class="t-c"> 
            <a href="javascript:void(0);"
               data-ride="ap-more"
               data-holder=".list-more-time>:last"
               data-sourcetype="url"
               data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_get_more_event_time&event_id='.$event->id.'&page=2') ?>"
               data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
               data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
               class="btn-b arw"><?php _e('View more', 'apollo') ?>
            </a>
        </p>
        <?php endif;
        endif;
        ?>

        <div <?php echo $is_show_additional_time_info; ?>>
        <?php
        $additionalTime =  $event->get_meta_data(Apollo_DB_Schema::_APL_EVENT_ADDITIONAL_TIME,Apollo_DB_Schema::_APOLLO_EVENT_DATA);
        if(!empty($additionalTime)):
        ?>
        <p class="apl-internal-content">
            <label><?php _e('Additional time information :','apollo') ?> </label>
            <?php
                echo  Apollo_App::convertContentEditorToHtml($additionalTime);
            ?>
        </p>
        <?php endif; ?>
        <p>
            <i class="fa fa-link fa-2x"></i>
            <label> <a target="_blank" href="<?php echo $event->get_meta_data( Apollo_DB_Schema::_WEBSITE_URL, Apollo_DB_Schema::_APOLLO_EVENT_DATA ); ?>"><?php _e( 'Official Website', 'apollo' ) ?></a></label>
        </p>
    </div>
</div>