<div class="video-box">
    <div class="wrapper-top-thumb">

    </div>

    <div class="thumb-slider">
        <ul class="slider-video">
            <?php
            $thumbs = "";
            if ( $arrData['video_embed'] && $arrData['video_desc'] ) :
                $i = 1;
                foreach ( $arrData['video_embed'] as $v ):
                    if ( ! $v['embed'] ) continue;

                    $thumbs .= '<li><a href="javascript:void(0);" data-id="'.$i.'"><img src="http://img.youtube.com/vi/'. $v .'/0.jpg"></a></li>';
                    $i++;
                endforeach;
            endif;
            ?>
        </ul>
    </div>
    <div class="closevideo"><i class="fa fa-times fa-lg"></i></div>
    <div style="display: none" class="add-event-video-loader"><a><i class="fa fa-spinner fa-spin fa-3x"></i> </a></div>
</div>  