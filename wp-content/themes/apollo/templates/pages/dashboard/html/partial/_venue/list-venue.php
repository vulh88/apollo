<?php foreach($arrdata['datas'] as $data ):
    // GET MORE DATA
    $_venue = get_venue($data->ID);
    $edit_venue_link = site_url(APL_Dashboard_Hor_Tab_Options::VENUE_PROFILE_URL) . '/'.$data->ID;
?>
    <li>
        <div class="event-img">
            <a target="_blank" href="<?php echo $_venue->get_permalink(); ?>">
                <img src="<?php echo $_venue->get_thumb_image_url() ?>">
            </a>
        </div>
        <div class="event-info">
            <a target="_blank" href="<?php echo $_venue->get_permalink(); ?>">
                <span class="ev-tt"><?php echo $_venue->get_title() ?><?php echo isset($_venue->post) && isset($_venue->post->post_status) && $_venue->post->post_status == 'pending' ? '*' : ''; ?></span>
            </a>
            <a  class="icons-info" title="<?php _e('Edit', 'apollo') ?>" href="<?php echo $edit_venue_link; ?>"><i class="fa fa-pencil-square-o"></i></a>
        </div>
    </li>
<?php endforeach; ?>