<div class="evt-blk">
    <div class="event-contact"><?php _e( 'Your Contact Information', 'apollo' ); ?></div>
    <p>
      <?php _e( 'Contact information is necessary in case we need to follow up with you regarding your submission. 
      This information will NOT appear on our website.', 'apollo' ) ?>
    </p>
    <?php 
        $current_user = wp_get_current_user();

        $detault_c_name = $submit_obj->get_value( Apollo_DB_Schema::_E_CONTACT_NAME, Apollo_DB_Schema::_APOLLO_EVENT_DATA );
        if ( ! isset( $_POST['case_action'] ) && !$detault_c_name && ! $submit_obj->event_id && ! $submit_obj->is_adding_event() ) {
            $detault_c_name = $current_user->user_firstname. ' '. $current_user->user_lastname;
        }
        
        $default_c_email = $submit_obj->get_value( Apollo_DB_Schema::_E_CONTACT_EMAIL, Apollo_DB_Schema::_APOLLO_EVENT_DATA );
        if ( ! isset( $_POST['case_action'] ) && !$default_c_email && ! $submit_obj->event_id && ! $submit_obj->is_adding_event() ) {
            $default_c_email = ! $submit_obj->event_id && ! $submit_obj->is_adding_event() ? $current_user->user_email : '';
        }
    ?>

    <?php
    $detault_c_name = trim($detault_c_name) ? $detault_c_name : '';
    $submit_obj->the_input( Apollo_DB_Schema::_E_CONTACT_NAME,
        __( 'Contact name', 'apollo' ), $e_data_key, true, '(*)', $detault_c_name, 'validate[required]',
        array(
            'data-errormessage-value-missing="'.__('Contact Name is required','apollo').'"'
        ) ); ?>

    <?php $submit_obj->the_input( Apollo_DB_Schema::_E_CONTACT_EMAIL, 
        __( 'Contact Email', 'apollo' ), $e_data_key, true, '(*)', $default_c_email, 'validate[funcCall[disableSpace], required,custom[email]]',
        array(
            'data-errormessage-value-missing="'.__('Contact Email is required','apollo').'"'
        ) ); ?>

    <?php $submit_obj->the_input( Apollo_DB_Schema::_E_CONTACT_PHONE,
        __( 'Contact Phone (format: 999-999-9999)', 'apollo' ), $e_data_key, false, '', '', 'validate[custom[phone]]' ); ?>

</div>