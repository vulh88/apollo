<div class="evt-blk full">
    <p class="note-required">* <?php _e( 'Required', 'apollo' )  ?></p>
    <?php
    /**
     * @ticket #19185: [CF] 20190215 - Add a character limit field for the event title field
     */
    $currentTheme =  !empty(wp_get_theme()) ? wp_get_theme()->stylesheet : '';
    $eventTitleLimit = 100;
    if(in_array($currentTheme, array('apollo', 'octave-child'))){
        $eventTitleLimit = of_get_option(APL_Theme_Option_Site_Config_SubTab::_APL_EVENT_TITLE_CHARACTERS_LIMIT, APL_Theme_Option_Site_Config_SubTab::_APL_EVENT_TITLE_CHARACTERS_LIMIT_DEFAULT);
    }
        $submit_obj->the_input( 'event_name',
        __( 'Event name', 'apollo' ),
        '',
        true,
        '(*)',
        '',
        'validate[required]',
        array(
            'data-errormessage-value-missing="'.__('Event name is required','apollo').'"',
            "maxlength=$eventTitleLimit",
        )
    ); ?>
    <span class="txt"><?php echo sprintf(__( 'Title may not exceed %s characters, including spaces.', 'apollo' ), $eventTitleLimit) ?></span>
</div>

<div class="evt-blk">
    <?php
    //enable show summary field
    if ( of_get_option(APL_Theme_Option_Site_Config_SubTab::_APL_ENABLE_EVENT_SUMMARY_FIELD, 1) ):
    ?>
    <p class="event-summary-text">
        <?php
        $maxNumSum = of_get_option(Apollo_DB_Schema::_APL_EVENT_CHARACTERS_SUM, 250);
        $summary   = of_get_option(Apollo_DB_Schema::_APL_EVENT_SUMMARY_FIELD, Apollo_Display_Config::_APL_FE_EVENT_FORM_SUMMARY_FIELD);
        $classes   = $submit_obj->the_error_class( 'event_summary' );

        // is summary require ?
        if ( of_get_option(Apollo_DB_Schema::_ENABLE_REQUIREMENT_EVENT_SUMMARY) ) {
            $summary  = $summary . ' (*)';
            $classes .= ' validate[required]';
        }

        _e( $summary, 'apollo' );
        ?>
    </p>
    <div class="el-blk full">
        <textarea maxlength="<?php echo $maxNumSum ?>"
                  name="event_summary"
                  class="inp-desc-event <?php echo $classes; ?>"
                  data-errormessage-value-missing="<?php echo __('Summary is required','apollo')?>"><?php echo $submit_obj->get_value( 'event_summary' ) ?></textarea>
        <?php echo $submit_obj->the_field_error( 'event_summary' ) ?>
        <div class="show-tip"><?php echo sprintf(__( 'Type your summary here which may not exceed %s characters, including spaces', 'apollo' ), $maxNumSum) ?></div>
    </div>
    <?php endif; ?>

    <div class="clearfix"></div>
    <p class="event-description-text">
        <?php
            $description_charter_limit = intval(of_get_option(APL_Theme_Option_Site_Config_SubTab::_APL_EVENT_DESCRIPTION_CHARACTER_LIMIT, ''));
            $description = of_get_option(Apollo_DB_Schema::_APL_EVENT_DESCRIPTION_FIELD, Apollo_Display_Config::_APL_FE_EVENT_FORM_DESCRIPTION_FIELD);
            _e( $description, 'apollo' );
        ?>
    </p>
    <div class="el-blk full">
        <?php wp_editor($submit_obj->event_desc, 'apl-event-desc', array(
            'editor_height' => 250,
            'editor_class'  => $submit_obj->the_error_class('event_desc'),
            'textarea_name' => 'event_desc',
        ));?>

        <div class="show-tip"><?php _e( 'Type your description here' . ($description_charter_limit > 0 ? sprintf(__( ' which may not exceed %s characters, including spaces', 'apollo' ), $description_charter_limit) : '' ), 'apollo' ) ?></div>
        <?php echo $submit_obj->the_field_error( 'event_desc' ) ?>
        <?php
            echo $description_charter_limit > 0 ? ('<div id="character_count_event_desc" style="font-style: italic; padding: 10px 0px;">' . __('Characters left: ') . $description_charter_limit) . '</div>' : '';
        ?>
    </div>

    <?php $wb_key = Apollo_DB_Schema::_WEBSITE_URL ?>
    <?php $requirement = of_get_option(Apollo_DB_Schema::_ENABLE_REQUIREMENT_EVENT_URL_FIELD) ? true : false;
          $required = of_get_option(Apollo_DB_Schema::_ENABLE_REQUIREMENT_EVENT_URL_FIELD) ? '(*)' : '';
          $classRequired = of_get_option(Apollo_DB_Schema::_ENABLE_REQUIREMENT_EVENT_URL_FIELD) ? 'apl_url validate[required,funcCall[disableSpace]]' : 'apl_url validate[funcCall[disableSpace], custom[url]]';
    $submit_obj->the_input( Apollo_DB_Schema::_WEBSITE_URL,
    __( 'Event website', 'apollo' ), Apollo_DB_Schema::_APOLLO_EVENT_DATA, $requirement, $required, '', $classRequired ); ?>
    
</div>