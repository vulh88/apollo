<div class="evt-blk add-co" <?php echo $noneStyleRegisterORG; ?>>
    <?php

    $co_org = $submit_obj->get_value( 'co_org' );
    $num_selected_cor = count( $co_org );

    for ( $i = 0; $i < $num_selected_cor; $i++ ):

        $options = $orgSelect2->getItemsInPostTypeToDropDownList(array(
            'post_status' => array('publish','pending'),
            'selected_item' => $p->ID
        ));

        ?>
        <div class="more-org">
            <div class="event-list no-mrn" style="float:left;">
                <select id="org-select<?php echo $i+1; ?>" class="apl_select2 event" name="_apollo_event_organization[]">
                    <option value=""><?php _e( 'Select Co-presenter', 'apollo' ) ?> :</option>
                    <?php if ( $orgs ):
                        foreach ( $orgs as $p ):
                            ?>
                            <option <?php echo $co_org && isset( $co_org[$i] ) && $co_org[$i] == $p->ID ? 'selected' : '' ?>
                                value="<?php echo $p->ID ?>"><?php echo $p->post_title ?></option>
                        <?php endforeach; endif; ?>
                </select>
                <!--                    <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>-->
            </div>
            <div class="del"><i class="fa fa-times"></i></div>
        </div>

    <?php endfor;

    $none_cor = (isset( $num_selected_cor ) && $num_selected_cor >= 5) || $isEditEventWithNoneOrg || !$isEnableRegORG ? 'style="display: none"' : '';
    ?>
    <div <?php echo $none_cor; ?> class="event-btn no-mar-l fL apl-add-cor-pre">
        <button data-request-org="<?php echo isset($requestOrg) && $requestOrg ? $requestOrg[0]->ID : 0 ?>"
                data-title="<?php _e( 'Select Co-presenter', 'apollo' ) ?>"
                data-img="<?php echo get_template_directory_uri() ?>/assets/images/delete-org.png"
                type="button"
                id="add-co"
                class="btn-noW">
            <?php _e( 'ADD CO-PRESENTER', 'apollo' ) ?>
        </button>
    </div>
</div>