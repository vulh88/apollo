<div class="evt-blk">
        <div class="event-contact"><?php _e('Ticket / Admission / Registration Information', 'apollo' ) ?></div>
  <p><?php _e( 'Please list your ticket contact information below. Please be as detailed as possible with ticket prices, when tickets are available, if available at door, locations for the seats, and discounts for different groups (Seniors? Students? etc.). Enter "Free Admission" if the event is free and open to the public.', 'apollo' ) ?></p>

  <?php
    $key = Apollo_DB_Schema::_ADMISSION_DETAIL;
    $ticket_description_charter_limit = intval(of_get_option(APL_Theme_Option_Site_Config_SubTab::_APL_EVENT_TICKET_REGISTRATION_DESCRIPTION_CHARACTER_LIMIT, ''));
  ?>
  <div class="el-blk full">
    <?php wp_editor($submit_obj->get_value( $key, $e_data_key ), 'apl-event-addmission', array(
        'editor_height' => 250,
        'textarea_name' => sprintf('%s[%s]', $e_data_key, $key),
    )) ?>
    <div class="show-tip"><?php _e( 'Ticket / Admission prices or Class Fee / Tuition... or “FREE” if applicable', 'apollo' ) ?></div>
      <?php
      echo $ticket_description_charter_limit > 0 ? ('<div id="character_count_event_addmission" style="font-style: italic; padding: 10px 0px;">' . __('Characters left: ') . $ticket_description_charter_limit) . '</div>' : '';
      ?>
  </div>
  
  <?php $key = Apollo_DB_Schema::_ADMISSION_PHONE ?>
  <div class="el-blk full">
  	<p class='title'><?php _e( 'Please enter the phone number that patrons should be directed to for either ticket or general information about your event.', 'apollo' ) ?></p>
    <input type="text" name="<?php echo $e_data_key ?>[<?php echo $key ?>]" value="<?php echo $submit_obj->get_value( $key, $e_data_key ) ?>" placeholder="<?php _e( 'Ticket / Information Phone (format: 999-999-9999)', 'apollo' ) ?>" class="inp-txt-event validate[custom[phone]]" />
    <div class="show-tip"><?php _e( 'Ticket / Information Phone (format: 999-999-9999)', 'apollo' ) ?></div>
  </div>

  <?php
  $requirement = of_get_option(Apollo_DB_Schema::_ENABLE_REQUIREMENT_EVENT_URL_FIELD) ? true : false;
  $required = of_get_option(Apollo_DB_Schema::_ENABLE_REQUIREMENT_EVENT_URL_FIELD) ? '(*)' : '';
  $classRequired = of_get_option(Apollo_DB_Schema::_ENABLE_REQUIREMENT_EVENT_URL_FIELD) ? 'apl_url validate[required,funcCall[disableSpace],custom[url]]' : 'apl_url validate[funcCall[disableSpace], custom[url]]';
  $submit_obj->the_input( Apollo_DB_Schema::_ADMISSION_TICKET_URL,
      __( 'Ticket Website (do not use if your event is Free)', 'apollo' ), $e_data_key, $requirement, $required, '', $classRequired); ?>
  
  <?php $submit_obj->the_input( Apollo_DB_Schema::_ADMISSION_TICKET_EMAIL, 
      __( 'Ticket / Information email', 'apollo' ), $e_data_key, true, '', '', '  validate[funcCall[disableSpace],custom[email]]' ); ?>

    <?php $free_event = of_get_option(Apollo_DB_Schema::_EVENT_ENABLE_FREE_EVENT, false); ?>
    <?php $key = Apollo_DB_Schema::_APOLLO_EVENT_FREE_ADMISSION; ?>
    <div class="el-blk full <?php echo !$free_event ? 'hidden' : ''; ?>">
        <input name="<?php echo $key ?>"
               type="checkbox" class="free-event" value="yes"
            <?php echo $submit_obj->get_value( $key ) == 'yes' ? 'checked' : '' ?> >
        <label class="free-evt"><?php _e( 'Free Event', 'apollo' ) ?></label>
    </div>
</div>