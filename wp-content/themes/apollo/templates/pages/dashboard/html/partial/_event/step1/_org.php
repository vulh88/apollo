<?php

    $selected_id = $submit_obj->get_value( Apollo_DB_Schema::_APOLLO_EVENT_ORGANIZATION );

    $is_avaiable = Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ORGANIZATION_PT );
    $is_selected_tmp = isset( $_SESSION['apl_add_org_btn'] ) && $_SESSION['apl_add_org_btn'] == 'add';

    // Check is editting an event with TMP ORG
    if ($isEditEventWithNoneOrg = $submit_obj->event_id && !$selected_id) {
        $_SESSION['apl_add_org_btn'] = 'add';
    }

    $isEnableRegORG = of_get_option(APL_Theme_Option_Site_Config_SubTab::_APL_ENABLE_EVENT_REGISTERED_ORGANIZATIONS_FIELD, 1);
    $noneStyleRegisterORG = !$is_avaiable || $is_selected_tmp || $isEditEventWithNoneOrg || !$isEnableRegORG ? 'style="display: none"' : '';

    $customValidateField = $noneStyleRegisterORG === '' ? 'custom-validate-field' : '';

    // get org by slug
    if ( !empty($orgSlug) && ! $submit_obj->event_id ) {
        $requestOrg = get_posts(array(
            'name'        => $orgSlug,
            'post_type'   => Apollo_DB_Schema::_ORGANIZATION_PT,
            'post_status' => 'publish',
            'numberposts' => 1
        ));
    }

    // In the first time we load the registered venue
    $selected_id = $submit_obj->get_value( Apollo_DB_Schema::_APOLLO_EVENT_ORGANIZATION );
    if ( ! $submit_obj->event_id && ! $selected_id && ! $submit_obj->is_adding_event() ) {
        $selected_id = Apollo_User::getCurrentAssociatedID(Apollo_DB_Schema::_ORGANIZATION_PT);
    }

    $orgSelect2 = apl_instance('APL_Lib_Helpers_Select2Org', array(
        'post_type' => Apollo_DB_Schema::_ORGANIZATION_PT,
        'selected_item' => $selected_id,
    ));
    $orgs = $orgSelect2->getItemsInPostTypeToDropDownList(array(
        'post_status' => array('publish','pending')
    ));



?>
<div class="evt-blk org" <?php echo $noneStyleRegisterORG ?>>
    <div class="event-contact"><?php _e( 'Presenting Organization/Business', 'apollo' ); ?></div>
    <?php
    //enable show Registered Organizations drop menu field
    $is_show_registered_organizations_info = ' style="display: none !important"';
    if ($isEnableRegORG){
        $is_show_registered_organizations_info = ' style="display: block !important"';
    }
    ?>
    <div <?php echo $is_show_registered_organizations_info; ?>>
        <h1><?php _e( 'SELECT REGISTERED ORGANIZATION/BUSINESS', 'apollo' ) ?> (*)</h1>
        <p><?php _e( 'Please select the presenting organization/business of your event from the following drop menu.', 'apollo' ) ?></p>
        <div class="event-list no-mrn">
            <?php $key = Apollo_DB_Schema::_APOLLO_EVENT_ORGANIZATION; ?>
            <select id="org-select"
                    class="apl_select2 event <?php echo $submit_obj->the_error_class( $key ); ?> <?php echo $customValidateField; ?> custom-validate-organization-field"
                    name="<?php echo $key ?>[]"
                    data-custom-validate="select2"
                    data-error-message="<?php echo __('Registered Organization is required','apollo'); ?>"
                    data-default-option="<?php _e('Select Registered Organization','apollo') ?>"
                    data-post-type="<?php echo Apollo_DB_Schema::_ORGANIZATION_PT ?>"
                    data-enable-remote="<?php echo $orgSelect2->getEnableRemote() ? 1 : 0; ?>"
                    data-source-url="apollo_get_remote_data_to_select2_box">
                <option value=""><?php _e( 'Select Registered Organization', 'apollo' ) ?> (*):</option>
                <?php if ( $orgs ):
                    foreach ( $orgs as $p ):
                        ?>
                        <option <?php echo $selected_id == $p->ID ? 'selected' : '' ?>
                                value="<?php echo $p->ID ?>"><?php echo $p->post_title ?><?php $p->post_status != 'publish' ? _e('*','apollo') : ''?></option>
                    <?php endforeach; endif; ?>
            </select>
            <?php $error = $submit_obj->the_field_error( $key ); ?>
            <!--        <div class="arrow-down --><?php //echo $error ? 'inp-error' : '' ?><!--"><i class="fa fa-sort-desc fa-lg"></i></div>-->
            <?php if ( ! $selected_id ) echo $error; ?>

        </div>
    </div>
</div>
<?php
$is_checked_actived_member = of_get_option(Apollo_DB_Schema::_ORGANIZATION_ACTIVE_MEMBER_FIELD);
if(!$is_checked_actived_member) {
?>

<div class="evt-blk org" <?php echo $noneStyleRegisterORG ?>>
    <p><?php _e( "Don't see your organization/business in the drop menu?", "apollo" ) ?></p>
    <div class="event-btn no-mar-l fL">
        <button onclick="apollo_is_add_tmp( 'add', 'org' )" type="button" id="add-org" class="btn-noW"><?php _e( 'ADD NEW ORGANIZATION', 'apollo' ) ?></button>
    </div>
</div>

<?php
}
    $noneStyleTMPORG = ($is_avaiable && ! $is_selected_tmp && !$isEditEventWithNoneOrg && $isEnableRegORG) || $is_checked_actived_member ? 'style="display: none"' : '';

?>
<div class="evt-blk add-organization" <?php echo $noneStyleTMPORG ?>>
    <div class="event-contact"><?php _e( 'Presenting Organization/Business', 'apollo' ); ?></div>

    <p class="tmp-org-text"><?php _e( 'Enter the name of the Presenting Organization:', 'apollo' ) ?></p>
    <div class="event-list no-mrn w-100-per">
        <?php $submit_obj->the_input( Apollo_DB_Schema::_APL_EVENT_TMP_ORG,
                    __( 'Organization’s name', 'apollo' ),
                    '',
                    true,
                    '(*)',
                    '',
                    'validate[required]',
                    array(
                        'data-errormessage-value-missing="'.__('Organization Name is required','apollo').'"'
                    )
        ); ?>
    </div>

    <?php if ( $is_avaiable && $isEnableRegORG ): ?>
        <div class="event-btn no-mar-l">
            <button onClick="apollo_is_add_tmp( 'back', 'org' )" type="button" id="back-selec-list" class="btn-noW"><?php _e( 'GO BACK TO SELECTION LIST', 'apollo' ) ?></button>
        </div>
    <?php endif; ?>
</div>

<?php
    if ($orgSelect2->getEnableRemote()) {
        require_once '_org-co-presenter/remote.php'; // Remote data
    } else {
        require_once '_org-co-presenter/simple.php';
    }
?>
