<?php

if ($arrdata['datas']):
    foreach($arrdata['datas'] as $data ):
        // GET MORE DATA
        $_a_program = get_program($data);
        $editLink = site_url(APL_Dashboard_Hor_Tab_Options::PROGRAM_PROFILE_URL) . '/'.$_a_program->id;
        $start_date = get_apollo_meta($data->ID, Apollo_DB_Schema::_APL_PROGRAM_STARTD, true);
        $end_date = get_apollo_meta($data->ID, Apollo_DB_Schema::_APL_PROGRAM_ENDD, true);
        /**
         * @ticket #19655: [CF] 20190403 - FE Program dashboard - Add Copy/Delete Buttons
         */
        $copyLink = site_url(APL_Dashboard_Hor_Tab_Options::PROGRAM_COPY_URL) . '/'.$data->ID;
        $deleteConfigMsg = __('Are you sure you would like to delete this program?', 'apollo');
        $enableCopyButton = of_get_option(APL_Theme_Option_Site_Config_SubTab::_PROGRAM_ENABLE_DISPLAY_COPY_BUTTON, 0);
        $enableDeleteButton = of_get_option(APL_Theme_Option_Site_Config_SubTab::_PROGRAM_ENABLE_DISPLAY_DELETE_BUTTON, 0);
        ?>
        <li>
            <div class="event-img"><a href="<?php echo $_a_program->get_permalink() ?>"><?php echo $_a_program->get_image('thumbnail') ?></a></div>
            <div class="event-info">
                <a href="<?php echo $_a_program->get_permalink() ?>"><span class="ev-tt"><?php echo $data->post_title ?>
                        <?php echo $_a_program->post->post_status == 'pending' ? '<small><i>'.__('(Pending)', 'a,   pollo').'</i></small>' : '' ?>
                    </span> </a>
                <div class="group-btn">
                    <a title="<?php _e('Edit', 'apollo') ?>" href="<?php echo $editLink ?>"><i
                                class="fa fa-pencil-square-o"></i></a>

                    <?php if ($enableCopyButton): ?>
                        <a title="<?php _e('make a Copy', 'apollo') ?>" href="<?php echo $copyLink ?>"><i
                                    class="fa fa-files-o"></i></a>
                    <?php endif; ?>
                    <?php if ($enableDeleteButton): ?>
                        <form action="" method="post">
                            <input type="hidden" name="program_id" value="<?php echo $data->ID ?>"/>
                            <input type="hidden" name="case_action" value="delete_program"/>
                            <?php wp_nonce_field('deleteProgramAction', '_aplNonceField'); ?>
                            <a title="<?php _e('Delete this Program', 'apollo') ?>" href="#"
                               data-msg="<?php echo $deleteConfigMsg ?>" class="apl-delete-event"><i
                                        class="fa fa-times"></i></a>
                        </form>
                    <?php endif; ?>
                </div>
                <?php if(!empty($start_date) || !empty($end_date)): ?><span class="ev-date"><?php echo APL::dateUnionDateShort($start_date, $end_date) ?></span><?php endif ?>
            </div>
        </li>
    <?php endforeach;
else:
    _e('No Result', 'apollo');
endif;
?>