<?php

    $data = maybe_unserialize( Apollo_App::apollo_get_meta_data( $submit_obj->event_id , Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA) );
    $additionalFields = new Apollo_Submit_Form(Apollo_DB_Schema::_EVENT_PT,array());
    $dataRender = array();
    if (!empty($data)) {
        foreach ($data as $key => $value) {
            if (isset($_POST[$key])) {
                $dataRender[$key] =$_POST[$key];
            } else {
                $dataRender[$key] = $value;
            }
        }
    }
    $dataRequest = $additionalFields->getCustomFieldsFromRequest();
    if (!empty($dataRequest) && !empty($dataRequest[0])) {
        foreach ($dataRequest[0] as $rKey => $rValue) {
            if (isset($_POST[$rKey])) {
                $dataRender[$rKey] = $rValue;
            }
        }
    }
    $additionalFields->custom_fields_form($dataRender, true);
?>

