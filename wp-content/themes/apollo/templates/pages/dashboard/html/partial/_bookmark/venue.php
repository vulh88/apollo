<?php foreach($arrdata['datas'] as $data ):
    // GET MORE DATA
    $_venue = get_venue($data);
    $etype =  Apollo_DB_Schema::_VENUE_PT;

    $table_meta = 'apollo_'.$etype;
    $start_date = get_metadata($table_meta, $data->post_id, Apollo_DB_Schema::_APOLLO_EVENT_START_DATE, true);
    $end_date = get_metadata($table_meta, $data->post_id, Apollo_DB_Schema::_APOLLO_EVENT_END_DATE, true);
    $address = $_venue->get_location_data(array(
        Apollo_DB_Schema::_VENUE_ADDRESS1,
        Apollo_DB_Schema::_VENUE_CITY,
        Apollo_DB_Schema::_VENUE_STATE,
        Apollo_DB_Schema::_VENUE_ZIP,
    ));
    ?>
    <li>
        <div class="event-img"><a href="<?php echo $_venue->get_permalink() ?>"><img src="<?php echo $_venue->get_thumb_image_url() ?>"></a></div>
        <div class="event-checkbox">
            <input type="checkbox" autocomplete="off" name="eventid[<?php echo $etype ?>]" value="<?php echo $data->bookmark_id ?>" class="check-event"><span class="event"></span>
        </div>
        <div class="event-map"><a target="_blank" href="<?php echo get_home_url() ?>/map?id=<?php echo $_venue->id ?>&type=<?php echo $etype ?>" data-address="<?php echo $address ?>"><img src="<?php echo APOLLO_FRONTEND_ASSETS_URI ?>/images/google-map.png"></a></div>
        <div class="event-info">
            <a href="<?php echo $_venue->get_permalink() ?>">
                <span class="ev-tt"><?php echo $_venue->get_title() ?></span>
            </a>
        </div>
    </li>
<?php endforeach; ?>