<?php
$user = get_current_user_id();

/**
 * @ticket #19641: [CF] 20190402 - [Classified] Add the Associated User(s) field to main admin list and detail form - Item 5
 */
require_once APOLLO_TEMPLATES_DIR . '/pages/dashboard/inc/apollo-account-dashboard-dataprovider.php';
$classifiedList = Apollo_Account_Dashboard_Provider::getListPostsByCurrentUser(array(
    'start' => 0,
    'post_type' => array(
        Apollo_DB_Schema::_CLASSIFIED
    ),
    'user_id' => $user,
    'pagesize' => false,
    'have_paging' => false,
));

$arr_classified = !empty($classifiedList['datas']) ? $classifiedList['datas'] : array() ;
$classified_id = $_GET['pid'];
$deleteConfigMsg = __('Are you sure you would like to delete this classified?', 'apollo');
?>
<div class="event-list">
    <select class="event apl_select2" name="up_event" id="select_classified">
        <option value=""><?php _e( 'Select classified', 'apollo' ) ?></option>
        <?php

        foreach( $arr_classified as $obj ):
            ?>
            <option value="<?php echo $obj->ID ?>" <?php selected($classified_id, $obj->ID) ?> ><?php echo $obj->post_title ?></option>
        <?php
        endforeach;
        ?>
    </select>
</div>

<div class="b-btn">
    <a href="javascript:void(0);" class="btn-bm btn btn-b"
       data-ride="ap-redirect"
       data-url="<?php echo home_url(). '/user/published-classifieds/' ?><%id%>"
       data-id='exp:$("#select_classified").val()'
       data-params ='id'
       data-mainparam = 'id'
       data-emptymsg="<?php _e( 'Please select a classified', 'apollo' ) ?>" >
        <?php _e( 'EDIT CLASSIFIED', 'apollo' ) ?>
    </a>
</div>
<?php if( $classified_id != 0){ ?>
    <div class="b-btn classified-form">
        <form action="/user/published-classifieds/" method="post">
            <input type="hidden" name="classified_id" value="<?php echo $classified_id ?>" />
            <?php wp_nonce_field( 'deleteClassifiedAction', '_aplNonceField' ); ?>
            <a href="javascript:void(0);" class="btn-bm btn btn-b delete-classified" data-msg="<?php echo $deleteConfigMsg ?>">
                <?php _e('DELETE','apollo') ?>
            </a>
        </form>
    </div>
<?php } ?>

<?php Apollo_App::getPageContentByPath(Apollo_Page_Creator::ID_PUBLISHED_CLASSIFIEDS) ?>