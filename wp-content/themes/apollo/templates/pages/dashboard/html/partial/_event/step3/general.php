<div style="margin-top:-15px" class="evt-blk">
    <article class="category-itm">
        <?php 
            $_arr_show_date = $event->getShowTypeDateTime('picture');
        ?>
        <?php  $event->render_circle_date(); ?>
        
        <div class="pic"><a href="<?php echo $event->get_permalink(); ?>"><?php echo $event->get_image('thumbnail') ?></a></div>
        <div class="category-t">
            <h2 class="category-ttl"> <a href="<?php echo $event->get_permalink(); ?>"> <?php echo $event->get_title(); ?></a></h2>
            <h3><?php echo $event->the_categories() ?></h3>
            <h3><?php echo $event->event_org_links(); ?></h3>
            <div class="desc" <?php echo $is_show_summary_info ?>>
                <?php
                    echo $event->get_full_excerpt();
                ?>
            </div>
        </div>
        <div class="fullwidth"><?php echo $event->get_full_content() ?></div>
    </article>
</div>