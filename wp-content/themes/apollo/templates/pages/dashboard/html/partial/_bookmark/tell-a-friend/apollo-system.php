<?php
$captcha = new Apollo_Captcha();
$commenter = wp_get_current_commenter();
$commenter['captcha_code'] = $captcha->getCode();
$commenter['captcha_image_path'] = $captcha->getImagePath();
$commenter['captcha_check_path'] = $captcha->getUrlCheckCaptcha();
$text_inp_email = '<input type="email"  name="email" placeholder="' . __('Email', 'apollo') . ' *" id="_tell_a_friend_email"
            data-error_holder="#_tell_a_friend_email_err">';
$text_inp_email .= '<div class="_tell_a_friend_err_container clearfix"><span class="error" id="_tell_a_friend_email_err"><span class="_required hidden">* Email address is required!</span></span></div>';

echo '<div id="popup-tell-a-friend-captcha-wrap">';
echo '<form action="' . admin_url('/admin-ajax.php?action=apollo_sendmail_tell_a_friend') . '" id="tell-a-friend" 
            method="post" 
            data-success="' . __('Send email successfully', 'apollo') . '"
            data-error="' . __('Can not send email') . '"
            >';


if($adminPage && !empty($adminPage->post_content)){
    $content =$adminPage->post_content;
    $content = str_replace('{site_name}',$blogInfo->domain,$content);
    $content = str_replace('{support_email}',$support_siteemail,$content);
    $content = str_replace('{link_to_friend}',$content_to_myfriend,$content);
    $content = str_replace('class="btn"', 'class="btn hidden"', $content);
    $content = str_replace('class="note"', 'class="note hidden"', $content);
    $content = str_replace( __('If you have any questions, please contact: ', 'apollo'),
        $text_inp_email . __('If you have any questions, please contact: ', 'apollo'), $content);

    echo apply_filters('the_content',$content);
} else {?>
    <h1><?php _e('Tell A Friend', 'apollo'); ?></h1>
    <p><?php _e('Keep your friends and family in the loop by emailing this page!', 'apollo'); ?>
        <Tell><?php _e('them about all the exciting events, organizations, venues, and more that you\'re finding on ', 'apollo'); ?> <?php echo $blogInfo->domain ?></br></br></Tell>
    </p>
    <?php
    echo $text_inp_email;
    if ($support_siteemail): ?>
        <p><?php _e('If you have any questions, please contact: ', 'apollo'); ?>
            <a href="mailto:<?php echo $support_siteemail ?>">  <?php echo $support_siteemail ?></a>
        </p>
    <?php endif; ?>

<?php } ?>

<?php

echo '<div class="inp-half">' .
    '<input
        placeholder="' . __("Enter the captcha code") . ' *"
        data-error_holder="#_tell_a_friend_captcha_err"
        data-captcha_image="#_tell_a_friend_captcha_image"
        data-check="required,captcha"
        data-url_check_captcha="' . $commenter['captcha_check_path'] . '"
        id="_tell_a_friend_captcha" name="captcha_code" class="inp inp-txt popup-tell-a-friend-inp" type="text" value="" size="30" aria-required="true" /></div>';
echo '<div class="inp-half">' . '<img id="_tell_a_friend_captcha_image" src="' . $commenter['captcha_image_path'] . '" />' . '</div><div class="clearfix"></div>';
echo '<div class="_tell_a_friend_err_container clearfix">
                <span class="error" id="_tell_a_friend_captcha_err">
                    <span class="_required hidden">* ' . __('Captcha is required!', 'apollo') . '</span>
                    <span class="_captcha hidden">* ' . __('Captcha does not match!', 'apollo') . '</span>
                </span>
</div>';
echo '<button id="btn-submit-tell-friend" class="btn btn-b btn-submit" type="submit" >' . __('SEND MESSAGE') . '</button>';
echo '</form>';
echo '</div>';