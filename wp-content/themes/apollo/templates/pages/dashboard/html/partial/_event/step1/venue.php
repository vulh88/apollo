<?php

    $is_avaiable =  Apollo_App::is_avaiable_module( Apollo_DB_Schema::_VENUE_PT );
    $is_add_tmp = isset( $_SESSION['apl_add_venue_btn'] ) && $_SESSION['apl_add_venue_btn'] == 'add';
    $key = Apollo_DB_Schema::_APOLLO_EVENT_VENUE;
    $venue_statuses = array('publish','pending');

    // In the first time we load the registered venue
    $selected_id = $submit_obj->get_value( Apollo_DB_Schema::_APOLLO_EVENT_VENUE );

    // Check is editting an event with TMP Venue
    if ($isEdittingEventWithTmpVenue = $submit_obj->event_id && !$selected_id) {
        $_SESSION['apl_add_venue_btn'] = 'add';
    }

    $none_style = $is_add_tmp || $isEdittingEventWithTmpVenue ? 'style="display: none"' : '';
    $customValidateField = $none_style === '' ? 'custom-validate-field' : '';

    if ( ! $submit_obj->event_id && ! $selected_id && ! $submit_obj->is_adding_event() ) {
        $selected_id = Apollo_User::getCurrentAssociatedID(Apollo_DB_Schema::_VENUE_PT);
    }

    $venueSelect2 = apl_instance('APL_Lib_Helpers_Select2', array(
        'post_type' => Apollo_DB_Schema::_VENUE_PT,
        'selected_item' => $selected_id,
    ));
    $venues = $venueSelect2->getItemsInPostTypeToDropDownList(array(
        'post_status' => $venue_statuses
    ));

    /** @Ticket #13856 */
    $cityVal = $submit_obj->get_value(Apollo_DB_Schema::_VENUE_CITY, Apollo_DB_Schema::_APL_EVENT_TMP_VENUE)
?>

<div class="evt-blk venue-above-text <?php echo $none_style ? 'access-mode' : ''; ?>">
    <div class="event-contact"><?php _e( 'Venue/Location Info', 'apollo' ) ?></div>

    <?php
        if ( $is_avaiable ):

    ?>
    <div class="venue" <?php echo $none_style; ?> >

        <div class="event-list no-mrn">

            <h1><?php _e( 'SELECT REGISTERED VENUE', 'apollo' ) ?> (*)</h1>
            <p><?php _e( 'Please select the venue/location of your event from the following drop menu', 'apollo' ) ?>:</p>

            <select name='<?php echo $key; ?>'
                    class="apl_select2 <?php echo ! $selected_id ? $submit_obj->the_error_class( $key ) : ''; ?> <?php echo $customValidateField; ?>  custom-validate-venue-field"
                    id="_apollo_event_venue"
                    data-custom-validate="select2"
                    data-error-message="<?php echo __('Registered Venue is required','apollo'); ?>"
                    data-custom-select2-post-status="<?php echo implode(',',$venue_statuses); ?>"
                    data-post-type="<?php echo Apollo_DB_Schema::_VENUE_PT ?>"
                    data-default-option="<?php _e('Select Venue:','apollo') ?>"
                    data-enable-remote="<?php echo $venueSelect2->getEnableRemote() ? 1 : 0; ?>"
                    data-source-url="apollo_get_remote_data_to_select2_box"
                >
                <option value=""><?php _e( 'Select Venue:', 'apollo' ) ?></option>
                <?php
                    if ( $venues ):
                            foreach ( $venues as $v ):
                        ?>
                            <option
                                <?php echo $selected_id == $v->ID ? 'selected' : '' ?>
                        value="<?php echo $v->ID ?>"><?php echo $v->post_title ?><?php $v->post_status != 'publish' ? _e('*','apollo') : ''?></option>
                        <?php
                        endforeach;
                    endif;
                ?>
            </select>
            <?php  $error = $submit_obj->the_field_error( $key ); echo ! $selected_id ? $error : ''; ?>
<!--            <div class="arrow-down --><?php //echo $error ? 'inp-error' : '' ?><!--"><i class="fa fa-sort-desc fa-lg"></i></div>-->
        </div>
        <div class="evt-blk add-locale"><p><?php _e( 'Don\'t see your venue/location in the drop menu', 'apollo' ) ?>?</p></div>
        <div class="event-btn no-mar-l">
            <button onclick="apollo_is_add_tmp( 'add', 'venue' )" type="submit" id="add-venue" class="btn-noW"><?php _e( 'ADD NEW VENUE', 'apollo' ) ?></button>
        </div>

    </div>
    <?php endif; ?>

</div>

<?php $none_style = $is_add_tmp || ! $is_avaiable || $isEdittingEventWithTmpVenue ? '' : 'style="display: none"' ?>
<div class="evt-blk newvenue <?php echo $none_style ? 'access-mode' : ''; ?>" <?php echo $none_style; ?>>
    <p><?php _e( 'Before entering a new venue/location in the fields below, please check the drop menu to see if your venue/location is already listed.', 'apollo' ) ?></p>

    <?php
    $parent_key = Apollo_DB_Schema::_APL_EVENT_TMP_VENUE;

    $submit_obj->the_input( Apollo_DB_Schema::_VENUE_NAME,
        __( 'New Venue Name', 'apollo' ), $parent_key, true,
        '(*)',
        '',
        'validate[required]',
        array(
            'data-errormessage-value-missing="'.__('New Venue Name is required','apollo').'"'
        )
    );

    $submit_obj->the_input( Apollo_DB_Schema::_VENUE_ADDRESS1,
        __( 'Venue Address', 'apollo' ), $parent_key, false, '' );

    $stateVal = $submit_obj->get_value(Apollo_DB_Schema::_VENUE_STATE, Apollo_DB_Schema::_APL_EVENT_TMP_VENUE);

    $submit_obj->the_select( Apollo_DB_Schema::_VENUE_STATE,__( 'Venue State', 'apollo' ), $parent_key, false, '',Apollo_App::getStateByTerritory(),'apl-us-states','','apl-territory-state' );

    $submit_obj->the_select( Apollo_DB_Schema::_VENUE_CITY,__( 'Venue City', 'apollo' ), $parent_key, false, '',Apollo_App::getCityByTerritory(false, $stateVal),'apl-us-cities','','apl-territory-city' );


    /** @Ticket #14143 */

    $requiredNb = of_get_option(APL_Theme_Option_Site_Config_SubTab::_VENUE_ENABLE_NEIGHBORHOOD_REQUIREMENT, 0);
    $nbLabel = $requiredNb ? __( 'Select Neighborhood (*)', 'apollo' ) : __( 'Select Neighborhood', 'apollo' );
    $defaultNB = array ('' => $nbLabel);
    $neighborhoodOptions = APL_Lib_Territory_Neighborhood::getNeighborhoodByCity($cityVal, $stateVal);

    $wrapNeighborhood = 'neighborhood-box ' . (empty($neighborhoodOptions) ? 'hidden' : '');
    $neighborhoodOptions = $defaultNB + $neighborhoodOptions;
    $nbAttributes = array(
        'data-custom-validate="select2"',
        'data-error-message="'.__('Neighborhood is required','apollo').'"'
    );
    $nbClass = 'apl-territory-neighborhood ' . ($requiredNb ? 'custom-validate-field ' : '');

    $submit_obj->the_select( Apollo_DB_Schema::_VENUE_NEIGHBORHOOD,$nbLabel, $parent_key, true, '(*)',
        $neighborhoodOptions,'apl-neighborhood','',$nbClass, $wrapNeighborhood, $nbAttributes);


    $submit_obj->the_select( Apollo_DB_Schema::_VENUE_ZIP,__( 'Venue Zip', 'apollo' ), $parent_key, false, '',Apollo_App::getZipByTerritory(),'apl-us-zip','','apl-territory-zipcode' );

    if(Apollo_App::showRegion() && !Apollo_App::enableMappingRegionZipSelection()){
        $submit_obj->the_select( Apollo_DB_Schema::_VENUE_REGION,__( 'Venue Region', 'apollo' ), $parent_key, false, '',Apollo_App::get_regions(),'apl-regions','','' );
    }

    $submit_obj->the_input( Apollo_DB_Schema::_VENUE_WEBSITE_URL,
        __( 'Venue Website', 'apollo' ), $parent_key, true, '', '', 'apl_url' );

    ?>

     <?php if( $is_avaiable ): ?>
    <div class="event-btn no-mar-l fL">
        <button onclick="apollo_is_add_tmp( 'back', 'venue' )" type="submit" id="back-venue-list" class="btn-noW"><?php _e( 'GO BACK TO SELECTION LIST', 'apollo' ) ?></button>
    </div>
    <?php endif; ?>
</div>
