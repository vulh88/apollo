<?php

$deleteConfigMsg = __('Are you sure you would like to delete this event?', 'apollo');
if ($arrdata['datas']):
    foreach($arrdata['datas'] as $data ):
        // GET MORE DATA
        $_a_event = get_event($data);
        $etype =  Apollo_DB_Schema::_EVENT_PT;

        $table_meta = 'apollo_'.$etype;
        $start_date = get_metadata($table_meta, $data->ID, Apollo_DB_Schema::_APOLLO_EVENT_START_DATE, true);
        $end_date = get_metadata($table_meta, $data->ID, Apollo_DB_Schema::_APOLLO_EVENT_END_DATE, true);

        $obj_organization = $_a_event->getOrganizationEvent();
        $editLink = site_url(APL_Dashboard_Hor_Tab_Options::EVENT_PROFILE_URL) . '/'.$_a_event->id;
        $copyLink = site_url(APL_Dashboard_Hor_Tab_Options::EVENT_COPY_URL) . '/'.$_a_event->id;
        $deleteLink = site_url(APL_Dashboard_Hor_Tab_Options::EVENT_DELETE_URL) . '/'.$_a_event->id;
        $enableEditButton = of_get_option(Apollo_DB_Schema::_ENABLE_DISPLAY_EDIT_BUTTON, 1);
        $enableDeleteButton = of_get_option(Apollo_DB_Schema::_ENABLE_DISPLAY_DELETE_BUTTON, 1);

        ?>
        <li>
            <div class="event-img"><a href="<?php echo $_a_event->get_permalink() ?>"><img src="<?php echo $_a_event->get_thumb_image_url() ?>"></a></div>
            <div class="event-info">
                <a href="<?php echo $_a_event->get_permalink() ?>"><span class="ev-tt"><?php echo !empty($data->post_title) ? $data->post_title : __('(no title)', 'apollo'); ?>
                    <?php echo $_a_event->post->post_status == 'pending' ? '<small><i>'.__('(Pending)', 'apollo').'</i></small>' : '' ?>
                    </span> </a>
                <p class="meta auth"><?php echo $_a_event->renderOrgVenueHtml() ?></p>
                <div class="group-btn">
                    <?php if($enableEditButton == 1) { ?>
                        <a title="<?php _e('Edit', 'apollo') ?>" href="<?php echo $editLink ?>"><i class="fa fa-pencil-square-o"></i></a>
                    <?php } ?>

                    <?php if (!isset($arrdata['hideCopy']) || !$arrdata['hideCopy']): ?>
                    <a title="<?php _e('make a Copy', 'apollo') ?>" href="<?php echo $copyLink ?>"><i class="fa fa-files-o"></i></a>
                    <?php endif; ?>
                    <?php
                        if ($enableDeleteButton == 1 ) : ?>
                            <form action="" method="post">
                                <input type="hidden" name="event_id" value="<?php echo $_a_event->id ?>" />
                                <?php wp_nonce_field( 'deleteEventAction', '_aplNonceField' ); ?>
                                <a title="<?php _e('Delete this Event', 'apollo') ?>" href="#" data-msg="<?php echo $deleteConfigMsg ?>" class="apl-delete-event" ><i class="fa fa-times"></i></a>
                            </form>
                        <?php endif; ?>
                </div>

                <?php if(!empty($start_date) || !empty($end_date)): ?><span class="ev-date"><?php echo APL::dateUnionDateShort($start_date, $end_date) ?></span><?php endif ?>
            </div>
        </li>
    <?php endforeach;
else:
    _e('No Result', 'apollo');
endif;
?>