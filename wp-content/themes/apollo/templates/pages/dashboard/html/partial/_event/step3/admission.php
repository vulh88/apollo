<div style="margin-top:-15px" class="evt-blk">
    
    <div class="tt-bar clearfix">
        <div class="cl"> <a><i class="fa fa-bars fa-3x"></i></a>
          <h4><?php _e( 'Admission Info', 'apollo' ) ?></h4>
        </div>
        <div class="line-bar" style="width: 470px;"></div>
    </div>
    
    <div class="information">
        <p>
            <label><?php _e( 'Admission Details', 'apollo' ) ?>: </label>
                <?php echo Apollo_App::convertContentEditorToHtml($event->get_meta_data( Apollo_DB_Schema::_ADMISSION_DETAIL, Apollo_DB_Schema::_APOLLO_EVENT_DATA )) ?>
        </p>
        <p>
          <label><?php _e( 'Ticket / Info Phone', 'apollo' ) ?>: </label>
          <?php echo $event->get_meta_data( Apollo_DB_Schema::_ADMISSION_PHONE, Apollo_DB_Schema::_APOLLO_EVENT_DATA ); ?>
        </p>
        <div style="margin-left:45px" class="event-btn">
            <?php
                $buyTicketText = of_get_option(Apollo_DB_Schema::_APL_EVENT_BUY_TICKET_TEXT, __('BUY TICKETS', 'apollo'));
            ?>
          <button type="submit" class="btn-noW"><?php echo $buyTicketText ?></button>
        </div>
    </div>
</div>
