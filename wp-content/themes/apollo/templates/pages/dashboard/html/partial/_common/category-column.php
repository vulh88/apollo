<ul class="cat-listing tbl<?php echo $k ?>">
    <?php 
        $total = count( $primarys );
        if ( $total > 2 ) {
            $columns = array(
                array_slice( $primarys , 0, ceil( $total / 2 ) ),
                array_slice( $primarys , ceil( $total / 2 ), $total - 1 )
            );
        } else {
            $columns = array(
                $primarys,
            );
        }
        
        
        foreach ( $columns as $k => $column ):
            
    ?>
    <li>
        <ul class="Llist c<?php echo $k ?>">
            <?php 
            foreach ( $column as $p ):  ?>
            <li>
                <input <?php echo is_array($cats) && $p->term_id && in_array( $p->term_id, $cats ) ? 'checked' : '' ?>   
                    name="<?php echo $input_name ?>" type="checkbox" value="<?php echo $p->term_id ?>" >
                <label><?php echo $p->name ?></label>
                <?php
                    $childs = get_terms( $type, array( 'parent' => $p->term_id, 'hide_empty' => false ) );
                    if ( $childs ):
                ?>
                <ul>
                    <?php foreach( $childs as $c ): ?>
                    <li>
                        <input <?php echo is_array($cats) && $c->term_id && in_array( $c->term_id, $cats ) ? 'checked' : '' ?>
                            name="<?php echo $input_name ?>" type="checkbox" value="<?php echo $c->term_id ?>" >
                        <label><?php echo $c->name ?></label>
                    </li>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>
            </li>
            <?php endforeach; ?>
        </ul>
    </li>
    <?php endforeach; ?>
</ul>