<div class="apl-fe-save-draft-content">
    <h1> <?php _e('Unable to complete your event submission at this time?', 'apollo') ?></h1>
    <p>
        <?php _e('You can save what you\'ve completed so far and log back in later to complete your submission. 
        You can find your saved event under the "Drafts" tab of your user dashboard\'s "Events" section. ', 'apollo') ?>
    </p>
</div>