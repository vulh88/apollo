<?php foreach($arrdata['datas'] as $data ):
    // GET MORE DATA
    $_a_classified = get_classified($data);
    $etype =  Apollo_DB_Schema::_CLASSIFIED;
    $table_meta = 'apollo_'.$etype;
    $expired_date = get_metadata($table_meta, $data->post_id, Apollo_DB_Schema::_APL_CLASSIFIED_EXP_DATE, true);

    ?>
    <li>
        <div class="event-img"><a href="<?php echo $_a_classified->get_permalink() ?>"><img src="<?php echo $_a_classified->get_thumb_image_url() ?>"></a></div>
        <div class="event-checkbox">
            <input type="checkbox" autocomplete="off" name="eventid[<?php echo $etype ?>]" value="<?php echo $data->bookmark_id ?>" class="check-event"><span class="event"></span>
        </div>
        <div class="event-info">
            <a href="<?php echo $_a_classified->get_permalink() ?>">
                <span class="ev-tt">
                    <?php echo $data->post_title ?>
                </span> </a>
            <p class="meta auth">
                <?php echo $_a_classified->renderOrgHtml() ?>
            </p>

            <?php if(!empty($expired_date)): ?><span class="ev-date"><?php echo APL::dateUnionDateShort($expired_date) ?></span><?php endif ?>

        </div>

    </li>
<?php endforeach; ?>