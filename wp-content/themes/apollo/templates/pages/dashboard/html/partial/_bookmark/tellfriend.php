<?php
if (!defined('ABSPATH')) {
    die;
}

if (!is_multisite()) {
    die;
}
?>

<?php
$site = get_blog_details(get_current_blog_id());

// If site has been deleted
$domain = $site->domain;
$support_siteemail = of_get_option(Apollo_DB_Schema::_SUPPORT_EMAIL);

// create content

$link = '';

$aplQuery = new Apl_Query(Apollo_Tables::_BOOKMARK);

foreach ($arrdata as $id) {
    if ($is_single) {
        $link .= "\n" . get_permalink($id);
    } else {
        $bookMarkData = $aplQuery->get_row("bookmark_id=$id");
        $link .= $bookMarkData ? "\n" . get_permalink($bookMarkData->post_id) : '';
    }

}
if (empty($link)) {
    ?>
    <div class="tellAF-popup">
        <div class="ct">
            <h1><?php _e('There are some missing. Maybe this event has been deleted!', 'apollo') ?></h1>

        </div>
        <div class="close"><img src="<?php APOLLO_FRONTEND_ASSETS_URI ?>/images/ico-close.png"></div>
    </div>
    <?php
} else {

    $_subject = sprintf(__('A Message From %s', 'apollo'), $domain);
    $_body = sprintf(__('Dear Friend...

I found this on %s and thought you might be interested.
Click on the link to check it out:', 'apollo'), $domain);
    $content_to_myfriend = <<<MAIL
mailto: ?Subject=$_subject&Body=$_body

MAIL;

    $content_to_myfriend .= $link;

    $content_to_myfriend = str_replace(array("\r\n", "\r", "\n"), '%0D%0A', $content_to_myfriend);

    $blogInfo = get_blog_details();

    //page content defined in admin > page >  TAF Popup
    $adminPage = get_page_by_path(Apollo_Page_Creator::ID_TAF_POPUP);

    $method_send_mail = of_get_option(APL_Theme_Option_Site_Config_SubTab::_APL_SHARE_EMAIL_WITH_FRIENT_METHOD, 'apollo_system');
    $base_dir = APOLLO_TEMPLATES_DIR. '/pages/dashboard/html/partial/_bookmark/tell-a-friend';
    ?>
    <div class="tellAF-popup">
        <div class="ct">
            <?php
            if($method_send_mail == 'apollo_system') {
                include $base_dir. '/apollo-system.php';
            } else {
                include $base_dir. '/client-email.php';
            }
            ?>

        </div>
        <div class="close"><i class="fa fa-times fa-lg"></i></div>
    </div>

<?php } ?>