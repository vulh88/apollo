<?php 
    $step = get_query_var('_apollo_step2a') == 'true' ? 'step-2b' : 'step-2a';
/** @Ticket #15925 */
if (isset($_SESSION['apl-fe-draft_saved']) && (!isset( $_POST['case_action'] ) ||  $_POST['case_action'] !== 'submit_event_dates')) {
    include APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/event/popup-confirm.php';
    unset($_SESSION['apl-fe-draft_saved']);
}
?>

<form action="<?php echo home_url() ?>/user/add-event/<?php echo $step ?>/<?php if( isset( $eid ) ) echo $eid; ?>" method="post" id="admin-frm-step1" name="admin_frm_step2" class="admin_form" enctype="multipart/form-data">
    <?php wp_nonce_field($submit_obj->nonceAction, $submit_obj->nonceName, false); ?>
    <input type="hidden" value="" name="case_action" id="case_action" />
    <input type="hidden" value="<?php echo $eid; ?>"  id="event_id" />
    <div class="evt-blk <?php if ( !get_query_var( '_apollo_step2a' ) ) echo 'has-event-time' ?>">
        <div class="event-contact"><?php _e( 'Dates', 'apollo' ) ?></div>
        <div class="eventfrom form-event">
            <?php echo $submit_obj->the_field_error( 'nonce_error' ) ?>
            <?php echo $submit_obj->the_field_error( $key_start ) ?>
            <?php echo $submit_obj->the_field_error( $key_end ) ?>
            <div class="calendar-ipt clearfix">                  
                <div class="wrap-event-date">
                    <label><?php _e( 'Start day', 'apollo' ) ?> (*):</label>
                    <input name="<?php echo $key_start ?>" value="<?php echo $submit_obj->get_value( $key_start ) ?>" type="text"
                            accesskey=""accept=""size="12" id="c-s"
                            class="inp inp-txt caldr <?php echo $submit_obj->the_error_class( $key_start ); ?>"
                           data-errormessage-value-missing="<?php _e('Start date is required','apollo') ?>"
                           autocomplete="off"
                        >
                </div>
                <div class="wrap-event-date">
                    <label><?php _e( 'End day', 'apollo' ) ?> (*):</label>
                    <input name="<?php echo $key_end; ?>" value="<?php echo $submit_obj->get_value( $key_end ) ?>" type="text" size="12" id="c-e"
                        class="inp inp-txt caldr <?php echo $submit_obj->the_error_class( $key_end ); ?>" name="<?php echo $key_end ?>"
                           data-errormessage-value-missing="<?php _e('End date is required','apollo') ?>"
                           autocomplete="off"
                        >
                </div>
            </div>
        </div>
        
        <div class="evt-blk days-of-weeks">
            <?php echo $submit_obj->the_field_error( Apollo_DB_Schema::_APOLLO_EVENT_DAYS_OF_WEEK );
                $date_of_weeks = array( 
                    __('Sun', 'apollo'),
                    __('Mon', 'apollo'),
                    __('Tue', 'apollo'),
                    __('Wed', 'apollo'),
                    __('Thu', 'apollo'),
                    __('Fri', 'apollo'),
                    __('Sat', 'apollo'),
                    );
                echo "<ul class=\"custom-validate-field clearfix\" data-custom-validate=\"multi-checkbox\" data-error-message=\"".__('Days of the week is required','apollo')."\">";
                echo '<li>'.__('Days of the week(*)', 'apollo').'</li>';
                foreach( $date_of_weeks as $k => $d_o_w ) {
                    $checked = $days_of_week && in_array($k, $days_of_week) ? 'checked' : '';
                    echo '<li><input '.$checked.' name="'.Apollo_DB_Schema::_APOLLO_EVENT_DAYS_OF_WEEK.'[]" type="checkbox" value="'.$k.'" /><span onClick="checkCBBefore(this)">'.$d_o_w.'</span></li>';
                }
                echo "</ul>";
            ?>
        </div>
        
        <div class="b-btn event-save-date">
            <a href="" id="apl-event-save-date-draft" class="btn-bm btn btn-b hidden" ><?php _e( 'SAVE DRAFT', 'apollo' ) ?></a>
            <?php if ($step == 'step-2b') : ?>
                <input type="hidden" name="save-draft-step-2b" value="step-2b"/>
            <?php endif; ?>
            <a id="event-save-date" data-start="<?php echo $submit_obj->get_value( $key_start ) ?>"
               data-end="<?php echo $submit_obj->get_value( $key_end ) ?>" data-ct="<?php _e( 'Editing dates will cause your times to be reset. Would you like to proceed?', 'apollo' ) ?>"
               class="btn-bm btn btn-b" >
                <?php 
                if ( get_query_var( '_apollo_step2a' ) == 'true' ) {
                    _e( 'EDIT DATES', 'apollo' );
                } else {
                    _e( 'SAVE DATES', 'apollo' );
                }
                
                ?>
            </a>    
        </div>
        
    </div>
    
</form>
<input type="hidden" id="list-msg-calendar"
       data-today ="<?php _e('TODAY','apollo') ?>"
       data-mon ="<?php _e('MON','apollo') ?>"
       data-tue ="<?php _e('TUE','apollo') ?>"
       data-wed ="<?php _e('WED','apollo') ?>"
       data-thu ="<?php _e('THU','apollo') ?>"
       data-fri ="<?php _e('FRI','apollo') ?>"
       data-sat ="<?php _e('SAT','apollo') ?>"
       data-sun ="<?php _e('SUN','apollo') ?>"
       data-am  ="<?php _e('AM','apollo') ?>"
       data-pm  ="<?php _e('PM','apollo') ?>"
       data-3more  ="<?php _e('[+3] More','apollo') ?>"
       data-edit="<?php _e('EDIT','apollo') ?>"
       data-del="<?php _e('DELETE','apollo') ?>"
       data-add-new="<?php _e('ADD NEW TIME','apollo') ?>"
       data-start-time  ="<?php _e('Start time','apollo') ?>"
       data-end-time  ="<?php _e('End time','apollo') ?>"
       data-missing-error  ="<?php _e('Something missing. Please try again!','apollo') ?>"
       data-unknow-error  ="<?php _e('An unknown error occurred.','apollo') ?>"
       data-remove-date = "<?php _e('Are you sure? This action will delete all times in this date.', 'apollo'); ?>"
       data-remove-date-events ="<?php _e('Are you sure? This action will delete all times of the event.','apollo') ?>"
       data-end-larger-start="<?php _e('End time must larger than start time!','apollo') ?>"
       data-invalid-time="<?php _e('Invalid time. Please choice another time','apollo') ?>"

    />
