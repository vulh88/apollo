<div class="video-box">
    <div class="audio-container">
        <div class="wrapper-top-thumb">

        </div>
        <div class="thumb-slider">
            <ul class="slider-video">
                <?php
                $thumbs = "";
                    if ( $videos ) :
                        $i = 1;
                        foreach ( $videos as $v ):
                        if ( ! $v['embed'] ) continue;

                        $thumbs .= '<li><a href="#" data-id="'.$i.'"><img src="http://img.youtube.com/vi/'. $v['embed'] .'/0.jpg"></a></li>';
                        endforeach;
                    endif;
                ?>
            </ul>
        </div>
    </div>
    <div class="closevideo"><i class="fa fa-times fa-lg"></i></div>
    <div style="display: none" class="add-event-video-loader"><a><i class="fa fa-spinner fa-spin fa-3x"></i> </a></div>
</div>  