<?php
    /** @Ticket #14118 */
    if(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_EVENT_FORM_PRIMARY_TYPE)){
        $text_primary_type = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_EVENT_FORM_PRIMARY_TYPE));
        $content_primary_type =$text_primary_type ? $text_primary_type->post_content : '';
    }else{
        $content_primary_type = include APOLLO_ADMIN_DIR.'/_templates/event-form-primary-type.php';
    }
?>

<div class="evt-blk">
    <div class="event-contact"><?php _e( 'Category Type', 'apollo' ) ?></div>
    <div class="apl-internal-content category-cms-page"><?php echo Apollo_App::convertContentEditorToHtml($content_primary_type) ?></div>
    <div class="event-list cat below-primary-text">
        <?php
            // Get all theme tools
            $themeToolClass = new Apollo_Theme_Tool();
            $themeToolData = $themeToolClass->getAllThemeTools();
          
            $key = Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID;
            $type = Apollo_DB_Schema::_EVENT_PT. '-type';
            $primarys = get_terms( $type, array( 'parent' => false, 'hide_empty' => false ) );
            $selected_pri_id = $submit_obj->get_value( $key );

            $hiddenCats = Apollo_App::hiddenCategoryIDs();

        ?>
        <select name="<?php echo $key; ?>"
                id="_apl_event_term_primary"
                class="event <?php echo $submit_obj->the_error_class( $key ); ?> custom-validate-field"
                data-custom-validate="select2"
                data-error-message="<?php echo __('Primary category is required','apollo'); ?>"
            >
            <option value=""><?php _e( 'Select Primary Category', 'apollo' ) ?> (*):</option>
            <?php

                if ( $primarys ):
                    foreach( $primarys as $p ):
                        
                        if ($themeToolData && in_array($p->term_id, $themeToolData)) continue;

                        if (in_array($p->term_id, $hiddenCats)) continue;
            ?>
            <option <?php echo $selected_pri_id == $p->term_id ? 'selected' : '' ?> value="<?php echo $p->term_id ?>"><?php echo $p->name ?></option>
            <?php endforeach; endif; ?>
        </select>
        <?php $error = $submit_obj->the_field_error( $key ); ?>
<!--        <div class="arrow-down --><?php //echo $error ? 'inp-error' : '' ?><!--"><i class="fa fa-sort-desc fa-lg"></i></div>-->
        <?php  echo $error; ?>
        
    </div>

    <p><?php _e( 'Now select any Secondary categories that apply:', 'apollo' ) ?></p>
    <div class="add-more-cat"><span class="cat-txt"><?php _e( 'Add more category types', 'apollo' ) ?></span>
        <?php 
            $_arr_add_cat = $submit_obj->get_value( 'add_cat' );
            $data_action = $submit_obj->get_data_form_action( 'apl_add_cat_btn' );
            
            $none_style = $submit_obj->get_none_style_form( 'apl_add_cat_btn', $_arr_add_cat && count( $_arr_add_cat ) > 1 );
        ?>
        <div data-mod="cat" data-action="<?php echo $data_action; ?>" class="expend add-cat-expand">
            <i class="fa <?php echo $none_style ? 'fa-chevron-circle-down' : 'fa-chevron-circle-up' ?>   fa-3x"></i>
        </div>
        
        <div class="cat-list" <?php echo $none_style ?> >
            <ul class="cat-listing tbl0">
                
                <?php 
                    $total = count( $primarys );
                    if ( $total > 2 ) {
                        $columns = array(
                            array_slice( $primarys , 0, ceil( $total / 2 ) ),
                            array_slice( $primarys , ceil( $total / 2 ), $total - 1 )
                        );
                    } else {
                        $columns = array(
                            $primarys,
                        );
                    }
                   
                    foreach( $columns as $k => $column ):
                ?>
                
                <li>
                  <ul class="Llist c<?php echo $k ?>">
                      <?php 
                      foreach ( $column as $p ): 
                          
                          if ($themeToolData && in_array($p->term_id, $themeToolData)) continue;

                          if (in_array($p->term_id, $hiddenCats)) continue;

                          $disable_style = $selected_pri_id == $p->term_id ? 'style="opacity: 0.5"' : '';
                          ?>
                      <li>
                          <input <?php echo $disable_style; ?> name="add_cat[]" type="checkbox" 
                                 <?php echo ! $disable_style && $_arr_add_cat && in_array( $p->term_id , $_arr_add_cat ) ? 'checked' : '' ?>
                                 value="<?php echo $p->term_id ?>" >
                          <label <?php echo $disable_style; ?>><?php echo $p->name ?></label>
                          <?php
                              $childs = get_terms( $type, array( 'parent' => $p->term_id, 'hide_empty' => false ) );
                              if ( $childs ):
                          ?>
                          <ul>
                              <?php foreach( $childs as $c ): 
                                  
                                  if ($themeToolData && in_array($p->term_id, $themeToolData)) continue;
                                  if (in_array($c->term_id, $hiddenCats)) continue;
                                ?>
                              <li>
                                  <input <?php echo $_arr_add_cat && in_array( $c->term_id , $_arr_add_cat ) ? 'checked' : '' ?> name="add_cat[]" type="checkbox" value="<?php echo $c->term_id ?>" >
                                  <label><?php echo $c->name ?></label>
                              </li>
                              <?php endforeach; ?>
                          </ul>

                          <?php endif; ?>
                      </li>
                      <?php endforeach; ?>

                  </ul>
                </li>
                <?php endforeach; ?>
                
            </ul>
        </div>
    </div>
    <?php if (of_get_option(Apollo_DB_Schema::_ENABLE_TAGS_SECTION_FE_FORM, true)) : ?>
    <div class="el-blk full event-tags">
        
        <h1><?php _e('TAGS', 'apollo') ?></h1>
        <p><?php _e('To include a tag with your listing enter the tag in the field below and then click the "Add Tag" button to confirm. You can enter more than one tag by repeating this procedure. Good examples of tags are the name of the neighborhood where your event takes place, the style/genre of music, cuisine type(s), artist name(s), venue, visual art style, etc. Please limit tags to no more than 10 per event listing.', 'apollo') ?></p>
        <div class="wapper-block">
            <input maxlength="150" name="tag" type="text" placeholder="<?php _e('Tag Name', 'apollo') ?>" class="inp-txt-event ">
            <input type="button" name="tag-btn" class="btn-noW" value="<?php _e('Add Tag', 'apollo') ?>" />
        </div>
        <?php 
            $tags = get_terms(Apollo_DB_Schema::_EVENT_TAG);
        ?>
        <div class="tags-list">
            <?php
            if ( $submit_obj->event_tags ) {
                foreach( $submit_obj->event_tags as $tag ) {
                    $tagVal = is_object($tag) ? $tag->name :  $tag;
                    ?>
                    <span><i class="fa fa-times"></i><?php echo $tagVal ?><input name="tags[]" type="hidden" value="<?php echo $tagVal ?>" /></span>
                    <?php
                }
            }
            ?>
        </div>




    </div>
    <?php endif; ?>
    
  </div>
