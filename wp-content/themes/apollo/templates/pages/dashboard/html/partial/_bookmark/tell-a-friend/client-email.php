<?php
if($adminPage && !empty($adminPage->post_content)){
    $content =$adminPage->post_content;
    $content = str_replace('{site_name}',$blogInfo->domain,$content);
    $content = str_replace('{support_email}',$support_siteemail,$content);
    $content = str_replace('{link_to_friend}',$content_to_myfriend,$content);
    echo apply_filters('the_content',$content);
} else {?>
    <h1><?php _e('Tell A Friend', 'apollo'); ?></h1>
    <p><?php _e('Keep your friends and family in the loop by emailing this page!', 'apollo'); ?>
        <Tell><?php _e('them about all the exciting events, organizations, venues, and more that you\'re finding on ', 'apollo'); ?> <?php echo $blogInfo->domain ?></br></br></Tell>
    </p>
    <?php if ($support_siteemail): ?>
    <p><?php _e('If you have any questions, please contact: ', 'apollo'); ?><a
            href="mailto:<?php echo $support_siteemail ?>">  <?php echo $support_siteemail ?></a></p>
<?php endif; ?>

    <div class="btn">
        <a class="btn-b" href="<?php echo $content_to_myfriend ?>"><?php _e('SEND MESSAGE', 'apollo'); ?></a>
    </div>
    <p class="note"><?php _e('This allow you to send via your own address book using MS Outlook or other local email client.', 'apollo'); ?></p>

    <?php
} ?>