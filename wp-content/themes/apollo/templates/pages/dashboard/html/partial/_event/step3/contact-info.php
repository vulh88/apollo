<div class="evt-blk">
    <div class="tt-bar clearfix">
        <div class="cl"> <a>

            <i class="fa fa-user fa-3x"></i></a>
            <h4><?php _e( 'Contact Info', 'apollo' ) ?></h4>
        </div>
        <div class="line-bar" style="width: 470px;"></div>
    </div>
   
    <div class="information">
      <p>
          <label><?php _e( 'Contact name', 'apollo' ) ?>: 
          </label><?php echo $event->get_meta_data( Apollo_DB_Schema::_E_CONTACT_NAME, Apollo_DB_Schema::_APOLLO_EVENT_DATA ); ?>
      </p>
      <p>
          <label><?php _e( 'Contact email', 'apollo' ) ?>:  </label><a href="mailto:<?php echo $event->get_meta_data( Apollo_DB_Schema::_E_CONTACT_EMAIL, Apollo_DB_Schema::_APOLLO_EVENT_DATA ); ?>" class="email-contact"> 
              <?php echo $event->get_meta_data( Apollo_DB_Schema::_E_CONTACT_EMAIL, Apollo_DB_Schema::_APOLLO_EVENT_DATA ); ?></a>
      </p>
    </div>
  </div>