<?php
require_once __DIR__.'/../../inc/apollo-account-dashboard-dataprovider.php';

$item_name = '_activity';
?>
<div class="dsb-welc">
    <div class="activity" id="_apl_useractivity">
        <h1><?php _e('My Activity', 'apollo') ?></h1>
            <?php
            // RENDER EVENTS
            $pagesize = Apollo_Display_Config::DASHBOARD_ACCOUNT_ACTIVITY_PAGESIZE;
            $datas_at = Apollo_Account_Dashboard_Provider::getUserActivity(array(
                'pagesize' => $pagesize,
                'start' => 0,
                'have_paging' => true,
            ));

            if(count($datas_at['datas']) > 0):?>

            <nav class="act">
                <?php
                $template = __DIR__.'/../partial/'.$item_name.'.php';
                echo APL::renderTemplateWithData($datas_at, $template);
                ?>
            </nav>
            <?php
                endif;
            ?>
    </div>
    <div class="blk-paging"
         data-items="<?php echo $datas_at['total'] ?>"
         data-itemsOnPage="<?php echo $pagesize ; ?>"
         data-container_selector="#_apl_useractivity .act"
         data-baseurl="<?php echo admin_url('admin-ajax.php') ?>"
         data-ajaxaction="apollo_get_user_activity"
         data-nextitemsonpage="<?php echo $pagesize ?>"
         data-ride="ap-pagination" ></div>
</div>