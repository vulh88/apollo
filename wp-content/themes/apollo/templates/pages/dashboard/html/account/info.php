<?php
    /* Active menu by javascript . */

$aerror = new WP_Error();
// get default data
$user = wp_get_current_user();
global $wpdb;

if(isset($_POST['csrf'])) {
    // process save data here
    $first_name = isset($_POST['first_name']) ? trim($_POST['first_name']) : '';
    $last_name = isset($_POST['last_name']) ? trim($_POST['last_name']) : '';
    $zip_code = isset($_POST['zip_code']) ? trim($_POST['zip_code']) : '';
    $email = isset($_POST['email']) ? trim($_POST['email']) : '';
    $country = isset($_POST['country']) ? trim($_POST['country']) : '';

    $pass1 = isset($_POST['pass1']) ? trim($_POST['pass1']) : '';
    $pass2 = isset($_POST['pass2']) ? trim($_POST['pass2']) : '';

    $first_name = sanitize_text_field($first_name);
    $last_name = sanitize_text_field($last_name);

    $zip_code = sanitize_text_field($zip_code);
    $email = sanitize_email($email);
    $country = sanitize_text_field($country);

    // check require
    if('' === $first_name) {
        $aerror->add('first_name', __('Please enter First Name', 'apollo'));
    }
    if('' === $last_name ) {
        $aerror->add('last_name', __('Please enter Last Name', 'apollo'));
    }
    if('' === $zip_code ) {
        $aerror->add('zip_code', __('Please enter Zip Code', 'apollo'));
    }

    if($country == '' ) {
        $aerror->add('country', __('Please select country', 'apollo'));
    }

    if('' !== $pass1 || '' !== $pass2) {
        if (strlen($pass1) < 5) {
            $aerror->add('password', __('Password must be greater than 5 characters', 'apollo'));
        }

        if ($pass1 !== $pass2) {
            $aerror->add('password', __('Password does not match', 'apollo'));
        }
    }


    if ( strlen( $first_name ) > 100 ) {
        $aerror->add( 'first_name', __( 'First name must be less than 100 characters', 'apollo' ) );
    }

    if ( strlen( $last_name ) > 100 ) {
        $aerror->add( 'last_name', __( 'Last name must be less than 100 characters', 'apollo' ) );
    }

    if($email !== $user->user_email) {
        if(!is_email($email)) {
            $aerror->add( 'email', __( 'Email address is not valid', 'apollo' ) );
        }
        elseif(email_exists($email)) {
            $aerror->add( 'email', __( 'Email already exists. Please choice another one!', 'apollo' ) );
        }
    }

    // check csrf token
    if(!isset($_POST['csrf']) || ($_POST['csrf'] !== $_SESSION['csrf'])) {
        $aerror->add( 'token', __( 'Invalid token. Try again ?', 'apollo' ) );
    }

    if(0 === count($aerror->get_error_codes())) {
        //first name
        $meta_value = $first_name;
        $user_id = $user->ID;
        $meta_key = 'first_name';
        $wpdb->update($wpdb->usermeta, compact('meta_value'), compact('user_id', 'meta_key') );

        // last name
        $meta_value = $last_name;
        $user_id = $user->ID;
        $meta_key = 'last_name';
        $wpdb->update($wpdb->usermeta, compact('meta_value'), compact('user_id', 'meta_key') );

        /** @Ticket #18744 */
        if (!empty($country)) {
            $oldCountry = isset($user->country) ? $user->country : '';
            update_user_meta($user->ID, 'country', $country, $oldCountry);
        }

        // zip_code name
        $meta_value = $zip_code;
        $user_id = $user->ID;
        $meta_key = 'zip_code';
        $_azip_code = get_user_meta($user->ID, $meta_key);
        if(!empty($_azip_code)) {
            update_user_meta($user_id, $meta_key, $meta_value, $user->zip_code);
        }
        else {
            add_user_meta($user_id, $meta_key, $meta_value, true);
        }


        // email address
        if($email !== $user->user_email) {
            $user_email = $email;
            $data = compact('user_email');
            $ID = $user_id;
            $ok = $wpdb->update( $wpdb->users, $data, compact( 'ID' ) );
        }

        // update pass
        if('' !== $pass1) {
            $user_pass = wp_hash_password( $pass1 );
            $data = compact('user_pass');
            $ID = $user_id;
            $ok = $wpdb->update( $wpdb->users, $data, compact( 'ID' ) );
            do_action('apollo_email_password_change',$user->display_name, $user->user_email, get_bloginfo('admin_email'));
        }
    }
}
else {

    $first_name = get_user_meta( $user->ID, 'first_name', true);
    $last_name = get_user_meta($user->ID, 'last_name', true);

    $email = $user->user_email;
    $zip_code = get_user_meta($user->ID, 'zip_code', true);
    $country = get_user_meta($user->ID, 'country', true);
}

$_SESSION['csrf'] = md5(rand(1,10));
$countriesList = Apollo_App::getAllCountries();

?>
<div class="dsb-welc">
    <h1><?php _e('Account Information', 'apollo') ?></h1>
    <div class="account-info">
        <?php if(isset($_POST['csrf']) && 0 === count($aerror->get_error_codes())): ?>
            <div class="_apollo_success">
                <i class="fa fa-check"></i>
                <?php echo __(Apollo_Form_Static::_SUCCESS_MESSAGE, 'apollo') ?>
            </div>
        <?php endif; ?>
        <?php if(count($aerror->get_error_codes()) > 0): ?>
            <?php foreach($aerror->get_error_messages() as $k_error => $msg ): ?>
                <span class="error"> * <?php echo $msg ?></span>
            <?php endforeach; ?>
        <?php endif; ?>


        <div class="info-frm">
            <form id="acc-frm" method="post" action="<?php echo $_SERVER['REQUEST_URI'] ?>">
                <div class="el-blk full">
                    <input type="text" name="first_name" value="<?php echo $first_name ?>" placeholder="<?php _e('First name ', 'apollo') ?>(*)" class="inp inp-txt  validate[required]<?php echo '' !== $aerror->get_error_message('first_name') ? 'inp-error' : '' ?>">
                    <div class="show-tip"><?php _e( 'First name', 'apollo' ) ?></div>
                </div>
                
                <div class="el-blk full">
                    <input type="text" name="last_name" value="<?php echo $last_name ?>" placeholder="<?php _e('Last name ', 'apollo') ?> (*)" class="inp inp-txt validate[required] <?php echo '' !== $aerror->get_error_message('last_name') ? 'inp-error' : '' ?>">
                    <div class="show-tip"><?php _e( 'Last name', 'apollo' ) ?></div>
                </div>
                <?php
                if (!empty($countriesList)) : ?>
                    <div class="el-blk full">
                        <?php echo Apollo_App::renderCountryHtml($countriesList, $country); ?>
                        <div class="show-tip"><?php _e( 'Country', 'apollo' ) ?></div>
                    </div>
                <?php endif; ?>
                <div class="el-blk full">
                    <input type="text" name="zip_code" value="<?php echo $zip_code ?>" placeholder="<?php _e('Zip code ', 'apollo') ?> (*)" class="inp inp-txt validate[required] <?php echo '' !== $aerror->get_error_message('zip_code') ? 'inp-error' : '' ?>">
                    <div class="show-tip"><?php _e( 'Zip code', 'apollo' ) ?></div>
                </div>
                
                <div class="el-blk full">
                    <input type="text" name="email" value="<?php echo $email ?>" placeholder="<?php _e('Email ', 'apollo') ?> (*)" class="inp inp-txt validate[required,funcCall[disableSpace],custom[email]]  <?php echo '' !== $aerror->get_error_message('email') ? 'inp-error' : '' ?>">
                    <div class="show-tip"><?php _e( 'Email', 'apollo' ) ?></div>
                </div>
                
                <div class="el-blk full">
                    <input type="password" name="pass1" placeholder="<?php _e('New password', 'apollo') ?>" class="inp inp-txt <?php echo '' !== $aerror->get_error_message('password') ? 'inp-error' : '' ?>">
                    <div class="show-tip"><?php _e( 'New password', 'apollo' ) ?></div>
                </div>
                
                <div class="el-blk full">
                    <input type="password" name="pass2" placeholder="<?php _e('Confirm password', 'apollo') ?>" class="inp inp-txt <?php echo '' !== $aerror->get_error_message('password') ? 'inp-error' : '' ?>">
                    <div class="show-tip"><?php _e( 'Confirm password', 'apollo' ) ?></div>
                </div>
                <div class="b-btn">
                    <button type="submit" class="btn btn-b"><?php _e('SUBMIT RECORD', 'apollo') ?></button>
                    <input type="hidden" name="csrf" value="<?php echo $_SESSION['csrf'] ?>" />
                </div>
            </form>

        </div>
    </div>
</div>