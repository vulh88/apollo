<?php
require_once __DIR__.'/../../inc/apollo-account-dashboard-dataprovider.php';
$user_id = get_current_user_id();
$pageSize = Apollo_Display_Config::DASHBOARD_ACCOUNT_BOOKMARK_PAGESIZE;
$postTypeArray = array();

//check enable module
//event
if(Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EVENT_PT)){
    $postTypeArray[ Apollo_DB_Schema::_EVENT_PT] = array('label' => __('Events', 'apollo'));
}
//org
if(Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ORGANIZATION_PT)){
    $postTypeArray[ Apollo_DB_Schema::_ORGANIZATION_PT] = array('label' => __('Organization', 'apollo'));
}
//venue
if(Apollo_App::is_avaiable_module( Apollo_DB_Schema::_VENUE_PT)){
    $postTypeArray[ Apollo_DB_Schema::_VENUE_PT] = array('label' => __('Venues', 'apollo'));
}
//artist
if(Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ARTIST_PT)){
    $postTypeArray[ Apollo_DB_Schema::_ARTIST_PT] = array('label' => Apollo_App::getCustomLabelByModuleName(Apollo_DB_Schema::_ARTIST_PT));
}
//classified
if(Apollo_App::is_avaiable_module( Apollo_DB_Schema::_CLASSIFIED)){
    $postTypeArray[ Apollo_DB_Schema::_CLASSIFIED] = array('label' => Apollo_App::getCustomLabelByModuleName(Apollo_DB_Schema::_CLASSIFIED));
}
//public art
if(Apollo_App::is_avaiable_module( Apollo_DB_Schema::_PUBLIC_ART_PT)){
    $postTypeArray[ Apollo_DB_Schema::_PUBLIC_ART_PT] = array('label' => __('Public Art', 'apollo'));
}
?>
<div class="dsb-welc">
    <div class="bookmark" data-elem_need_coordinate_selector=".event-map>a" id="_apl_bookmark_container">
        <h1><?php _e('My Bookmarks', 'apollo') ?></h1>
        <p><?php _e('To send one or more of your saved links to a friend, simply select the check box next to each listing below and click the "Tell a Friend" button. You can send multiple links in one message.', 'apollo') ?></p>

        <div class="wc-l account-listing">
            <nav class="nav-tab">
                <ul class="tab-list">
                    <?php foreach($postTypeArray as $postType => $value):  ?>
                        <li class="selected"><a href="#" data-id="<?php echo strtolower(str_replace(' ','_',$postType)) ?>"><?php echo  $value['label'] ?></a></li>
                    <?php endforeach; ?>

                </ul>
            </nav>
            <div class="tab-bt tab-bt-custom"></div>
            <?php foreach($postTypeArray as $postType => $value):  ?>
                <div data-target="<?php echo strtolower(str_replace(' ','_',$postType)) ?>" class="blog-blk" style="display: block;">
                    <?php
                    $datas_bm = Apollo_Account_Dashboard_Provider::getBookmark(array(
                        'pagesize' => $pageSize,
                        'start' => 0,
                        'post_type' => (array)$postType,
                        'user_id' => $user_id,
                        'have_paging' => true,
                    ));

                    if(count($datas_bm['datas']) > 0):
                        ?>
                        <div class="blk-bm-events" id="_apol_bm<?php echo $postType ?>">
                            <h1><?php echo $value['label'] ?></h1>
                            <div class="event-check-all">
                                <input type="checkbox" name="event-ck" data-ride="ap-checkboxall" autocomplete="off"
                                       data-child_selector="#_apol_bm<?php echo $postType ?> .check-event" class="check-event-all">
                            </div>
                            <nav class="events">
                                <?php
                                $template = __DIR__.'/../partial/_bookmark/'.$postType.'.php';
                                echo APL::renderTemplateWithData($datas_bm, $template);
                                ?>
                            </nav>
                        </div>
                        <?php
                            if ( $datas_bm['total'] > $pageSize ):
                        ?>
                        <div class="blk-paging"></div>
                        <?php endif; ?>
                        
                        <script type="text/javascript">
                            var APL_DB_INLINE = APL_DB_INLINE || {};
                            APL_DB_INLINE['items_<?php echo $postType ?>'] = <?php echo $datas_bm['total'] ?>;
                            APL_DB_INLINE['itemsOnPage'] = <?php echo $pageSize ?>;
                            APL_DB_INLINE['baselink'] = '<?php echo admin_url('admin-ajax.php') ?>';
                        </script>
                    <?php
                    else:
                        _e('No result', 'apollo');
                    endif;
                    ?>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="b-btn my-account-listing">
            <button type="submit" class="btn btn-b"
                    data-ride="ap-tellfriendbylink"
                    data-action_after="open:popup"
                    data-url="<?php echo admin_url('admin-ajax.php?action=apollo_set_bookmark_for_myfriend') ?>"
                    data-data_source_selector="[name^='eventid']:checked"
                    data-data="ids:value"
                    data-alert="<?php _e('Please select your bookmark', 'apollo') ?>"
                ><?php _e('TELL A FRIEND', 'apollo') ?></button>
            <button type="submit" class="btn btn-b"
                    data-ride="ap-checkaction"
                    data-alert="<?php _e('Please select your bookmark', 'apollo') ?>"
                    data-action_after="redirect"
                    data-url="<?php echo admin_url('admin-ajax.php?action=apollo_remove_bookmark') ?>"
                    data-data_source_selector="[name^='eventid']:checked"
                    data-change_status="delete"
                    data-data="ids:value"
                ><?php _e('REMOVE SELECTED', 'apollo') ?></button>
        </div>
    </div>
</div>