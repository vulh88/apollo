<?php


// PAGE: DASHBOARD: EVENTS
include_once APOLLO_TEMPLATES_DIR.'/events/list-events.php';
include APOLLO_TEMPLATES_DIR. '/pages/dashboard/inc/event/apollo-submit-event.php';


if (!empty($_POST['event_id'])) {
    $event = new Apollo_Submit_Event($_POST['event_id']);

    if ($event->deleteEvent($_POST['event_id'])) {
        $msg = __('Delete event successfully', 'apollo');
    }
}

$pageSize =  defined('APOLLO_NUMBER_DASHBOARD_EVENT') ? APOLLO_NUMBER_DASHBOARD_EVENT : Apollo_Display_Config::DASHBOARD_EVENTS;

$submit_obj = new Apollo_Submit_Event();

$list_events = new List_Event_Adapter(1);
$upcomming_events = $list_events->get_upcomming_user_events_for_dashboard('', " LIMIT $pageSize ");
$upEventTotal = $list_events->totalPost;

$draft_events = $list_events->get_draft_user_events_for_dashboard('', " LIMIT $pageSize ");
$draftEventTotal = $list_events->totalPost;

$past_events = $list_events->get_past_user_events_for_dashboard('', " LIMIT $pageSize ");
$pastEventTotal = $list_events->totalPost;

/**
 * @ticket #18932: Display 'private' events on FE user dashboard
 */
$private_events = $list_events->get_private_user_events_for_dashboard('', " LIMIT $pageSize ");
$privateEventTotal = $list_events->totalPost;

/**
 * @ticket #19007: add tab unconfirmed
 */
$unconfirmed_events = $list_events->get_unconfirmed_user_events_for_dashboard('', " LIMIT $pageSize ");
$unconfirmedEventTotal = $list_events->totalPost;

$submit_obj->clean_session();
$keyWord = isset($_GET['event-name']) && !empty($_GET['event-name']) ?  $_GET['event-name'] : '';

?>

<div class="dsb-welc evt-dashboard">
    <div class="bookmark">
        <h1><?php _e('Events', 'apollo') ?></h1>

        <div class="evt-blk">
            <div class="event-btn">
                <a href="<?php echo home_url() ?>/user/add-event/step-1">
                    <button type="submit" class="btn-submit"><?php _e( 'SUBMIT A NEW EVENT', 'apollo' ) ?></button>
                </a>
            </div>
        </div>

        <?php
        if (isset($msg)):
        ?>
        <div class="_apollo_success">
            <i class="fa fa-check"></i> <?php echo $msg ?></div>
        <?php endif; ?>

        <p><?php _e('Please select one of the following options:', 'apollo') ?></p>

        <div class="wc-l account-listing">
            <nav class="nav-tab">
                <ul class="tab-list">
                    <li><a href="#" data-id="current-up-events"><?php echo _e('Current/Upcoming Events', 'apollo') . " ($upEventTotal)";  ?></a></li>
                    <li><a href="#" data-id="draft-events"><?php echo _e('Draft Events', 'apollo') . " ($draftEventTotal)" ?></a></li>
                    <li><a href="#" data-id="past-events"><?php echo _e('Past Events', 'apollo') . " ($pastEventTotal)" ?></a></li>
                    <li><a href="#" data-id="private-events"><?php echo _e('Private Events', 'apollo') . " ($privateEventTotal)" ?></a></li>
                    <li><a href="#" data-id="unconfirmed-events"><?php echo _e('Unconfirmed Events', 'apollo') . " ($unconfirmedEventTotal)" ?></a></li>
                </ul>
            </nav>

            <div class="tab-bt tab-bt-custom"></div>
            <div class="event-btn search-event-name">
                <form method="get"  action="<?php echo home_url() ?>/user/events/" class="form-search form-search-event-name">
                    <input name="event-name" type="text" value="<?php echo $keyWord?>"
                           placeholder="<?php _e('Search Events...', 'apollo');?>" class="inp inp-txt solr-search">
                    <button type="submit" class="btn btn-link"><i class="fa fa-search fa-flip-horizontal fa-lg"></i></button>
                </form>
            </div>
            <div data-target="current-up-events" class="blog-blk">

                <div class="blk-bm-events" id="_apol_upcomming_event">
                    <nav class="events">
                        <?php
                        $template = APOLLO_TEMPLATES_DIR. '/pages/dashboard/html/partial/_event-dashboard/events.php';
                        echo APL::renderTemplateWithData(array('datas' => $upcomming_events), $template);
                        ?>
                    </nav>
                </div>
                <div class="blk-paging"></div>
                <script type="text/javascript">
                    var APL_DB_INLINE_CUSTOM = APL_DB_INLINE_CUSTOM || {};
                    APL_DB_INLINE_CUSTOM['items_upcomming_event'] = <?php echo $upEventTotal ?>;
                    APL_DB_INLINE_CUSTOM['itemsOnPage'] = <?php echo $pageSize ?>;
                    APL_DB_INLINE_CUSTOM['baselink'] = '<?php echo admin_url('admin-ajax.php') ?>';
                </script>
            </div>

            <div data-target="draft-events" class="blog-blk">

                <div class="blk-bm-events" id="_apol_draft_event">
                    <nav class="events">
                        <?php
                        $template = APOLLO_TEMPLATES_DIR. '/pages/dashboard/html/partial/_event-dashboard/events.php';
                        echo APL::renderTemplateWithData(array('datas' => $draft_events), $template);
                        ?>
                    </nav>
                </div>
                <div class="blk-paging"></div>
                <script type="text/javascript">
                    var APL_DB_INLINE_CUSTOM = APL_DB_INLINE_CUSTOM || {};
                    APL_DB_INLINE_CUSTOM['items_draft_event'] = <?php echo $draftEventTotal; ?>;
                    APL_DB_INLINE_CUSTOM['itemsOnPage'] = <?php echo $pageSize ?>;
                    APL_DB_INLINE_CUSTOM['baselink'] = '<?php echo admin_url('admin-ajax.php') ?>';
                </script>
            </div>

            <div data-target="past-events" class="blog-blk">
                <div class="blk-bm-events" id="_apol_past_event">
                    <nav class="events">
                        <?php
                        $template = APOLLO_TEMPLATES_DIR. '/pages/dashboard/html/partial/_event-dashboard/events.php';
                        echo APL::renderTemplateWithData(array('datas' => $past_events), $template);
                        ?>
                    </nav>
                </div>
                <div class="blk-paging"></div>
                <script type="text/javascript">
                    var APL_DB_INLINE_CUSTOM = APL_DB_INLINE_CUSTOM || {};
                    APL_DB_INLINE_CUSTOM['items_past_event'] = <?php echo $pastEventTotal ?>;
                    APL_DB_INLINE_CUSTOM['itemsOnPage'] = <?php echo $pageSize ?>;
                    APL_DB_INLINE_CUSTOM['baselink'] = '<?php echo admin_url('admin-ajax.php') ?>';
                </script>
            </div>

            <!--@ticket #18932: Display 'private' events on FE user dashboard -->
            <div data-target="private-events" class="blog-blk">
                <div class="blk-bm-events" id="_apol_private_event">
                    <nav class="events">
                        <?php
                        $template = APOLLO_TEMPLATES_DIR. '/pages/dashboard/html/partial/_event-dashboard/events.php';
                        echo APL::renderTemplateWithData(array('datas' => $private_events), $template);
                        ?>
                    </nav>
                </div>
                <div class="blk-paging"></div>
                <script type="text/javascript">
                    var APL_DB_INLINE_CUSTOM = APL_DB_INLINE_CUSTOM || {};
                    APL_DB_INLINE_CUSTOM['items_private_event'] = <?php echo $privateEventTotal ?>;
                    APL_DB_INLINE_CUSTOM['itemsOnPage'] = <?php echo $pageSize ?>;
                    APL_DB_INLINE_CUSTOM['baselink'] = '<?php echo admin_url('admin-ajax.php') ?>';
                </script>
            </div>

            <!-- @ticket #19007: add tab unconfirmed -->
            <div data-target="unconfirmed-events" class="blog-blk">
                <div class="blk-bm-events" id="_apol_unconfirmed_event">
                    <nav class="events">
                        <?php
                        $template = APOLLO_TEMPLATES_DIR. '/pages/dashboard/html/partial/_event-dashboard/events.php';
                        echo APL::renderTemplateWithData(array('datas' => $unconfirmed_events), $template);
                        ?>
                    </nav>
                </div>
                <div class="blk-paging"></div>
                <script type="text/javascript">
                    var APL_DB_INLINE_CUSTOM = APL_DB_INLINE_CUSTOM || {};
                    APL_DB_INLINE_CUSTOM['items_unconfirmed_event'] = <?php echo $unconfirmedEventTotal ?>;
                    APL_DB_INLINE_CUSTOM['itemsOnPage'] = <?php echo $pageSize ?>;
                    APL_DB_INLINE_CUSTOM['baselink'] = '<?php echo admin_url('admin-ajax.php') ?>';
                </script>
            </div>
        </div>

    </div>
</div>

