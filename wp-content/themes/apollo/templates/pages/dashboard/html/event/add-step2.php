<?php //
include APOLLO_TEMPLATES_DIR. '/pages/dashboard/inc/event/apollo-submit-event.php';

$key_start = Apollo_DB_Schema::_APOLLO_EVENT_START_DATE;
$key_end   = Apollo_DB_Schema::_APOLLO_EVENT_END_DATE;

// repair data
$eid = 0;

if ( get_query_var('_apollo_event_id') ) {
    $eid = intval( get_query_var('_apollo_event_id') );
} else {
    if ( isset( $_SESSION['added_event_id'] ) && $_SESSION['added_event_id'] ) {
        $eid = intval($_SESSION['added_event_id']);
    }
}

if($eid === 0)
{
    wp_die("Something is missing");
}

if($eid !== 0) {
    Apollo_Permission_System::processPermission(array(
            'type' => 'access-event',
            'user_id' => get_current_user_id(),
            'event_id' => $eid,
            'fail_action' => 'redirect',
            'fail_data' => array(
                'url' => home_url(). '/user/events',
            ),
        ));
}

$submit_obj = new Apollo_Submit_Event($eid);

$post = get_event($eid);
$additionalTime = $post->get_meta_data(Apollo_DB_Schema::_APL_EVENT_ADDITIONAL_TIME, Apollo_DB_Schema::_APOLLO_EVENT_DATA);

$submit_obj->re_edit_step2($eid);
if ( isset( $_POST['case_action'] ) && $_POST['case_action'] === 'submit_event_dates' ) {
    $submit_obj->submit_event_dates();
}

$start_date = $submit_obj->get_value( $key_start );
$end_date = $submit_obj->get_value( $key_end );
$days_of_week = maybe_unserialize( $submit_obj->get_value( Apollo_DB_Schema::_APOLLO_EVENT_DAYS_OF_WEEK ) );

$d1 = date_create($start_date);
$d2 = date_create($end_date);

$_startTimeSecond = strtotime($start_date);
$_endTimeSecond = strtotime($end_date);
$_months = ($_endTimeSecond - $_startTimeSecond) / ( 24 * 60 * 60 * 30 );
?>
<div class="events" id="add-event-calendar" ng-app="dashboard">
<?php Apollo_Submit_Event::event_steps(2); ?>
<div class="evt-blk">
    <div class="event-tt"><?php _e( 'DATES AND TIMES', 'apollo' ) ?></div>
</div>

<?php include APOLLO_TEMPLATES_DIR. '/pages/dashboard/html/partial/_event/step1/datetime.php'; ?>

<?php

    if ( get_query_var( '_apollo_step2a' ) == 'true' ) :
?>
<div class="evt-blk filter">
    <div class="event-contact"><?php _e( 'Times', 'apollo' ) ?></div>
    <div class="time-filter">
        <div class="start">
            <label>&nbsp; <?php _e('Start time', 'apollo') ?> *</label>
            <select class="time _hour1" >
                <option value="01">01</option>
                <option value="02">02</option>
                <option value="03">03</option>
                <option value="04">04</option>
                <option value="05">05</option>
                <option value="06">06</option>
                <option value="07">07</option>
                <option value="08">08</option>
                <option value="09">09</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
            </select><span class="arrowH"></span>
            <select class="time mins _minute1">
                <option value="00">00</option>
                <option value="05">05</option>
                <option value="10">10</option>
                <option value="15">15</option>
                <option value="20">20</option>
                <option value="25">25</option>
                <option value="30">30</option>
                <option value="35">35</option>
                <option value="40">40</option>
                <option value="45">45</option>
                <option value="50">50</option>
                <option value="55">55</option>
            </select><span class="arrowM"></span>
            <select class="time ap _ampm1">
                <option value="AM"><?php _e('AM', 'apollo') ?></option>
                <option selected value="PM"><?php _e('PM', 'apollo') ?></option>
            </select><span class="arrowAP"></span><span class="clock"><i class="fa fa-clock-o"></i></span>
        </div>
        <div class="end">
            <label>&nbsp; <?php _e('End time', 'apollo') ?></label>
            <select class="time _hour2">
                <option value="--">--</option>
                <option value="01">01</option>
                <option value="02">02</option>
                <option value="03">03</option>
                <option value="04">04</option>
                <option value="05">05</option>
                <option value="06">06</option>
                <option value="07">07</option>
                <option value="08">08</option>
                <option value="09">09</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
            </select><span class="arrowH"></span>
            <select class="time mins _minute2">
                <option value="--">--</option>
                <option value="00">00</option>
                <option value="05">05</option>
                <option value="10">10</option>
                <option value="15">15</option>
                <option value="20">20</option>
                <option value="25">25</option>
                <option value="30">30</option>
                <option value="35">35</option>
                <option value="40">40</option>
                <option value="45">45</option>
                <option value="50">50</option>
                <option value="55">55</option>
            </select><span class="arrowM"> </span>
            <select class="time ap _ampm2">
                <option value="--">--</option>
                <option value="AM"><?php _e('AM', 'apollo') ?></option>
                <option  value="PM"><?php _e('PM', 'apollo') ?></option>
            </select><span class="arrowAP"></span><span class="clock"><i class="fa fa-clock-o"></i></span><span class="op"><?php _e('(optional)', 'apollo') ?></span>
        </div>
    </div>
    <div class="clear-all"><a href="javascript:void(0);" id="calendar_clear_all"><img src="<?php echo APOLLO_FRONTEND_ASSETS_URI ?>/images/clear-all-ico.png"><span><?php _e('CLEAR ALL', 'apollo') ?></span></a></div>
    <div class="apply-all"><a href="javascript:void(0);" id="calendar_choice_all"><i class="fa fa-file-text fa-3x"></i><span><?php _e('APPLY TO ALL', 'apollo') ?></span></a></div>
</div>


<ul id="calendar_err">

</ul>
<?php
$_str_days_of_week = $days_of_week ? implode(',', $days_of_week) : array();
if($_months < Apollo_App::configDateRange()):
    ?>
<div class="calendar-container" ng-controller="calendar">
    <calendar data-dws="<?php echo $_str_days_of_week ?>" selected="day" data-type="1" data-eid = "<?php echo $eid ?>" data-sd="<?php echo $start_date ?>" data-ed = "<?php echo $end_date ?>"></calendar>



</div> <!-- end calendar -->
<?php else: ?>
    <div class="calendar-container sevenday">
        <calendar data-dws="<?php echo $_str_days_of_week ?>" selected="day" data-type="2" data-eid = "<?php echo $eid ?>" data-sd="<?php echo $start_date ?>" data-ed = "<?php echo $end_date ?>"></calendar>
    </div>
<?php endif ?>

<div class="viewmore-box">
    <img src="<?php echo APOLLO_FRONTEND_ASSETS_URI ?>/images/viewmore-bg.png" class="arrow-box">
    <div class="closeviewmore"><img src="<?php echo APOLLO_FRONTEND_ASSETS_URI ?>/images/close-ico.png"></div>
</div>

    <?php
    //enable show Registered Organizations drop menu field
    $is_show_additional_time_info = ' style="display: none !important"';
    if (of_get_option(APL_Theme_Option_Site_Config_SubTab::_APL_ENABLE_EVENT_TIMEINFO_FIELD, 1) ) {
        $is_show_additional_time_info = ' style="display: block !important"';
    }
    ?>
    <!-- -->
    <div class="el-blk full" <?php echo $is_show_additional_time_info; ?>>
        <p><?php _e('Please add any additional time information about this event, such as pre or post-event reception, post show talks.','apollo'); ?></p>
        <?php
            wp_editor($additionalTime, 'additional_time',
            array(
                'editor_height' => 250
            ));
        ?>
        <div class="show-tip"><?php _e('Additional time information'); ?></div>
    </div>
    <!-- -->

<div class="evt-blk apl-fe-control">
    <?php $event_id = get_query_var( '_apollo_event_id' ) ? get_query_var( '_apollo_event_id' ) : '' ?>
    <a id="next-to-step3" data-alert="<?php _e('Each date should have at least one start-time', 'apollo') ?>" data-href="<?php echo home_url(). '/user/add-event/step-3/'. $event_id ?>">
        <div id="arrow" class="apl-fe-event-proceed">
            <span>
                <?php _e( 'Proceed to ', 'apollo' ) ?>
                <div>
                    <?php _e('STEP 3', 'apollo') ?>
                </div>
            </span>
        </div>
    </a>
</div>
<div class="evt-blk apl-fe-control">
    <?php  include APOLLO_TEMPLATES_DIR.'/pages/dashboard/html/partial/_event/draft-button.php'; ?>
    <a href="">
        <div id="apl-fe-save-draft-step2"><span><?php _e( 'SAVE DRAFT', 'apollo' ) ?></span></div>
    </a>
</div>
</div>

<?php endif; ?>


