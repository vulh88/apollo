<?php
include APOLLO_INCLUDES_DIR. '/scripts/class-apollo-event-import.php';
$import = new Apollo_Event_Import();
$importData = $import->getAllImports();

$eiToolInstruction = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_IMPORT_EVENT_INSTRUCTIONS));
$maximumYear = Apollo_DB_Schema::_APL_EVENT_MAXIMUM_YEAR;
$arrayErrMessages = array(
    "cv-required" => __("Required Field.", 'apollo'),
    "cv-invalid-date" => __("Invalid date format! The date format should be MM/DD/YYYY", 'apollo'),
    "cv-url" => __("Url is invalid.", 'apollo'),
    "cv-email" => __("Email is invalid.", 'apollo'),
    "cv-selected-date" => __("Selected date must be larger than current date.", 'apollo'),
    "cv-sd-ed" => __("Start Date must be less or equal than End Date.", 'apollo'),
    "cv-ed-sd" => __("End Date must be larger or equal than Start Date.", 'apollo'),
    "cv-maximum-year" => sprintf(__("Events may not last more than %s years. Please revise your end date to less than %s years.", 'apollo'),$maximumYear,$maximumYear),
    "cv-required-start-time" => __("Enter at least one 'start time'", 'apollo')
);

if($eiToolInstruction) : ?>
<div class="dsb-welc">
    <div class="apl-internal-content">
        <?php echo Apollo_App::convertContentEditorToHtml($eiToolInstruction->post_content); ?>
    </div>
</div>
<?php endif; ?>

<?php
    if ($importData):
?>

<div class="dsb-welc">
    <div class="events">
        <div class="evt-blk add-artist-event">
            <div class="event-list">
                <select class="event apl_select2" name="up_event" id="select-event-import-file">
                    <option value=""><?php _e( 'Select Import', 'apollo' ) ?></option>
                    <?php foreach($importData as $im):
                        $imData = explode('-', $im->file_name);

                        if (!$imData) continue;

                        $time = date( 'M, d, Y h:i A', intval($imData[0]));
                        $imData[0] = $time;
                        if (isset($imData[2])) {
                            unset($imData[2]);
                        }
                        ?>
                    <option <?php echo $im->imported == 1 ? 'disabled' : '' ?> value="<?php echo $im->file_name ?>"><?php echo implode(' - ', $imData) ?>.xml
                        <?php echo $im->imported == 1 ? '['.__('Imported', 'apollo').']' : '' ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="b-btn">
                <a  id="apl-load-event-import-file"
                    href="javascript:void(0);" class="btn-bm btn btn-b"
                   data-emptymsg="<?php _e( 'Please select an import', 'apollo' ) ?>" >
                    <?php _e( 'EDIT IMPORT', 'apollo' ) ?>
                </a>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<div class="wrap-ipcsv">
    <div class="events-import-blk">
        <div class="wrap-input-file" id="wrap-input-file">
            <div class="b-btn">
                <a  id="import-event-by-csv-file-button"
                    href="javascript:void(0);" class="btn-bm btn btn-b"
                    data-emptymsg="<?php _e( 'Please select an import csv file', 'apollo' ) ?>" >
                    <?php _e( 'IMPORT (*.csv)', 'apollo' ) ?>
                </a>
            </div>
        </div>
    </div>
</div>


<div class="events">
    <div class="evt-blk">
        <h1><?php _e('Bulk Import', 'apollo') ?></h1>
        <p><?php _e('Please enter your event information...', 'apollo') ?></p>
        <p class="import-event error" style="display:none"><?php _e('There are some errors in your submitted data. Please check them again !', 'apollo') ?></p>

    </div>
    <div class="evt-blk-import">
        <!-- The error messages is for validation table cell -->
        <?php if(!empty($arrayErrMessages)) : ?>
            <?php foreach($arrayErrMessages as $key => $msg) : ?>
                <input type="hidden" id="<?php echo $key; ?>" value="<?php echo $msg; ?>" />
            <?php endforeach; ?>
        <?php endif; ?>
        <!-- End The error messages is for validation table cell -->
        <div class="import-btn-group">
            <a href="#" title="<?php _e('Import Event', 'apollo') ?>"
               data-msg="<?php _e('Please enter data to at least a row', 'apollo') ?>" id="btn-submit"
               data-dump="#event-list" data-instance="hot" name="dump"
               ><i class="fa fa-share-square-o"></i></a>

            <a href="#" title="<?php _e('Save for later', 'apollo') ?>" id="save-later"
               ><i class="fa fa-floppy-o"></i></a>

            <a href="#" title="<?php _e('Add New', 'apollo') ?>" id="btn-add-new"
               data-msg="<?php _e('All the data on the form will be resetted?','apollo') ?>" >
                <i class="fa fa-file"></i></a>

            <a href="#" title="<?php _e('Reset', 'apollo') ?>"
               id="btn-reset" data-msg="<?php _e('Are you sure to reset all the current data?','apollo') ?>">
                <i class="fa fa-refresh"></i></a>

        </div>
        <div class="full-screen"><a href="#"><?php _e('View full screen', 'apollo') ?></a></div>

        <div class="event-import-wrapper">
          <div id="event-list"></div>
        </div>
        <div class="event-btn import">
        <a href="#" title="<?php _e('Close', 'apollo') ?>" class="close-fs">
                    <i class="fa fa-times"></i></a>
        </div>
    </div>
</div>

