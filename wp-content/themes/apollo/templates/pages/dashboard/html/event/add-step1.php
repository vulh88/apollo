<?php

/** @Ticket #15925 */
if (isset($_SESSION['apl-fe-draft_saved'])) {
    include APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/event/popup-confirm.php';
    unset($_SESSION['apl-fe-draft_saved']);
}

$max_upload_gallery_program = of_get_option(Apollo_DB_Schema::_MAX_UPLOAD_GALLERY_IMG,5);
$max_upload_size_in_b = wp_max_upload_size();
$max_upload_size_in_mb = number_format($max_upload_size_in_b / (1024 * 1024), 2);
if(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_EVENT_PHOTO)){
    $text_image = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_EVENT_PHOTO));
    $content_image =$text_image ? $text_image->post_content : '';
}else{
    $content_image = include APOLLO_ADMIN_DIR.'/_templates/event-photo.php';
}


/*
* @Ticket 13842
*/
function apl_char_count_event_description( $initArray )
{
    $label_character_left = __('Characters left: ');
    $description_charter_limit = intval(of_get_option(APL_Theme_Option_Site_Config_SubTab::_APL_EVENT_DESCRIPTION_CHARACTER_LIMIT, ''));
    $ticket_description_charter_limit = intval(of_get_option(APL_Theme_Option_Site_Config_SubTab::_APL_EVENT_TICKET_REGISTRATION_DESCRIPTION_CHARACTER_LIMIT, ''));
    $initArray['setup'] = <<<JS
[function(ed) {
    ed.on('init', function (e) {
        updateCharacterCount()
    });
    ed.on('focus', function ( e) {
        updateCharacterCount()
    });
    
    ed.onKeyDown.add(function(ed, e) {
        if(tinymce.EditorManager.activeEditor.selection.editor.id == 'apl-event-desc' || tinymce.EditorManager.activeEditor.selection.editor.id == 'apl-event-addmission') {
            if(tinymce.EditorManager.activeEditor.selection.editor.id == 'apl-event-desc') {
                var max = $description_charter_limit;
                var ele_character_count = 'character_count_event_desc'
            }
            if(tinymce.EditorManager.activeEditor.selection.editor.id == 'apl-event-addmission') {
                var max = $ticket_description_charter_limit;
                var ele_character_count = 'character_count_event_addmission'
            }
            if(max > 0) {
                var content = tinyMCE.activeEditor.getContent().replace(/(<[a-zA-Z\/][^<>]*>|\[([^\]]+)\])|/ig,''); 
                var allowedKeys = [8, 37, 38, 39, 40, 46];
                var charCount = proccessLimitCharacters(content, max, ele_character_count);
                if (allowedKeys.indexOf(e.keyCode) != -1) return true;
                if (charCount < 1) {
                    ed.stopPropagation();
                    ed.preventDefault();
                    tinymce.dom.Event.cancel(e);    
                    return false;
                }
            }
        }
    });
    
    ed.on('change, keyup', function(e) {
         if(tinymce.EditorManager.activeEditor.selection.editor.id == 'apl-event-desc' || tinymce.EditorManager.activeEditor.selection.editor.id == 'apl-event-addmission') {
            if(tinymce.EditorManager.activeEditor.selection.editor.id == 'apl-event-desc') {
                var max = $description_charter_limit;
                var ele_character_count = 'character_count_event_desc'
            }
            if(tinymce.EditorManager.activeEditor.selection.editor.id == 'apl-event-addmission') {
                var max = $ticket_description_charter_limit;
                var ele_character_count = 'character_count_event_addmission'
            }
            if(max > 0) {
                var content = tinyMCE.activeEditor.getContent().replace(/(<[a-zA-Z\/][^<>]*>|\[([^\]]+)\])|/ig,'');
                var charCount = proccessLimitCharacters(content, max, ele_character_count);
                if (charCount == 0) {
                    return
                } else if (charCount < 1) {
                    tinyMCE.activeEditor.setContent(content.substr(0, max))
                }
            }
        }
    });
    
    function proccessLimitCharacters(content, max, ele) {
        var len = content.length;
        var charCount = max - len;
        if(document.getElementById(ele)){
            document.getElementById(ele).innerHTML = "$label_character_left" + (charCount < 0 ? 0 : charCount);
        }
        return charCount;
    }
    
    function updateCharacterCount() {
        if(ed.id == 'apl-event-desc'){
            var max = $description_charter_limit;
            var ele_character_count = 'character_count_event_desc'
            proccessLimitCharacters(ed.getContent().replace(/(<[a-zA-Z\/][^<>]*>|\[([^\]]+)\])|/ig,''), max, ele_character_count)
        } else if(ed.id == 'apl-event-addmission'){
            var max = $ticket_description_charter_limit;
            var ele_character_count = 'character_count_event_addmission'
            proccessLimitCharacters(ed.getContent().replace(/(<[a-zA-Z\/][^<>]*>|\[([^\]]+)\])|/ig,''), max, ele_character_count)
        }
    }
    
}][0]
JS;
    return $initArray;
}
add_filter( 'tiny_mce_before_init', 'apl_char_count_event_description' );
/*
* @end Ticket
*/

include APOLLO_TEMPLATES_DIR. '/pages/dashboard/inc/event/apollo-submit-event.php';


$submit_obj = new Apollo_Submit_Event();
/** Vandd - @Ticket #12434 */
$submit_obj->setCurrentStep(1);
$submit_obj->checkOrgProfileRequired();

// Init: cleaning session if we submit an event at the first time
if (empty($_POST)) {
    // ThienLD : clean session on adding new event step 1
    $submit_obj->clean_session();
}

$_update_event_id = ! get_query_var( '_apollo_copy_event' ) && get_query_var('_apollo_event_id') && is_numeric(get_query_var('_apollo_event_id')) ?
    get_query_var( '_apollo_event_id' ) : '';


if ( get_query_var('_apollo_event_id') && is_numeric(get_query_var('_apollo_event_id')) ) {
    $submit_obj->event_id = get_query_var('_apollo_event_id');
} else if ( $event_id = $submit_obj->is_adding_event() ) {
    $submit_obj->event_id = $event_id;
} else if ( $from_id = get_query_var( '_from_id' ) ) {
    $submit_obj->event_id = 0;
    $submit_obj->from_id = $from_id;
}

if(intval($submit_obj->event_id) !== 0) {
    Apollo_Permission_System::processPermission(array(
            'type' => 'access-event',
            'user_id' => get_current_user_id(),
            'event_id' => $submit_obj->event_id,
            'fail_action' => 'redirect',
            'fail_data' => array(
                'url' => home_url(). '/user/events',
            ),
        ));
}

$GLOBALS['_apl_pid'] =  $submit_obj->event_id;

if ( (get_query_var('_apollo_event_id') && is_numeric(get_query_var('_apollo_event_id'))) || get_query_var( '_from_id' ) || $submit_obj->is_adding_event() )
    $submit_obj->re_edit( $submit_obj->from_id );

if ( (isset( $_POST['case_action'] ) && $_POST['case_action'] == 'submit_event') || $submit_obj->isSaveDraft() ) {
    $submit_obj->submit_event();
}

$e_data_key = Apollo_DB_Schema::_APOLLO_EVENT_DATA;

// ticket #11077: get org slug in the URL to pre-select organization item in drop down list
$orgSlug = get_query_var( '_apollo_event_org_slug' );

?>
<form action="<?php echo home_url() ?>/user/add-event/step-1/<?php echo $_update_event_id; ?>" method="post" id="admin-frm-step1" name="admin_frm_step1" class="admin_form" enctype="multipart/form-data">
    <?php wp_nonce_field($submit_obj->nonceAction, $submit_obj->nonceName, false); ?>
    <input type="hidden" value="" name="case_action" id="case_action" />
    <input type="hidden" value="<?php echo $submit_obj->from_id; ?>" name="copy_event" />
    <div class="dsb-welc">
        <div class="events">

            <?php Apollo_Submit_Event::event_steps(1); ?>

            <div class="evt-blk">
                <div class="event-tt"><?php _e( 'ENTER EVENT DETAILS', 'apollo' ) ?>
                    <?php
                    if ( $submit_obj->event_id && $eventObj = get_event( $submit_obj->event_id ) ):
                    ?>
                    <a class="view-page-link" href="<?php echo $eventObj->get_permalink() ?>" target="_blank">(<?php _e( 'View Page', 'apollo' ) ?>)</a>
                    <?php endif; ?>
                </div>
            </div>

            <?php Apollo_App::getPageContentByPath(Apollo_Page_Creator::ID_ADD_EVENT) ?>

            <div class="evt-blk">
                <?php echo $submit_obj->the_errors(); ?>
            </div>

            <?php $base_dir = APOLLO_TEMPLATES_DIR. '/pages/dashboard/html/partial/_event/step1';
            include $base_dir. '/event-general.php';
            include $base_dir. '/_org.php';
            include $base_dir. '/category.php';
            include $base_dir. '/admission.php';
            include $base_dir. '/venue.php';
            include $base_dir. '/accessibility.php';
            /** @Ticket #14086 */
            include $base_dir. '/add-fields.php';
            ?>

            <?php $enable_photo_event = of_get_option( Apollo_DB_Schema::_ENABLE_PHOTO_EVENT ,1); ?>
                <div class="evt-blk">
                    <div class="event-contact"><?php _e( 'Images', 'apollo' ) ?></div>
                    <p><?php echo Apollo_App::convertContentEditorToHtml($content_image) ?></p>
                    <nav class="nav-tab">
                        <ul class="tab-image-list">
                            <li class="selected"><a href="#" data-id="1" id="_pimg_c"><?php _e( 'PRIMARY IMAGE', 'apollo' ) ?></a></li>
                            <?php
                                if ($enable_photo_event):
                            ?>
                            <li><a href="#" data-id="2" id="_gc"><?php $text = of_get_option(Apollo_DB_Schema::EVENT_GALLERY_TEXT,Apollo_Display_Config::EVENT_GALLERY_TEXT) ;_e( $text, 'apollo' ); ?></a></li>
                            <?php endif; ?>
                        </ul>
                    </nav>

                    <input type="hidden" name="action" id="_apl_action_upload" value="apollo_add_event_upload_pimage_tmp">
                    <?php include __DIR__.'/../partial/_event/step1/primary-image.php' ?>

                    <?php
                    if ($enable_photo_event):
                    ?>
                    <?php include __DIR__.'/../partial/_event/step1/gallery.php' ?>
                    <?php endif; ?>
                </div>

            <?php $enable_video_event = of_get_option( Apollo_DB_Schema::_ENABLE_VIDEO_EVENT,1 ); ?>
            <?php if($enable_video_event){
            ?>
                <?php include $base_dir. '/video.php'; ?>
            <?php }?>
            <?php
                if (Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ARTIST_PT )) {
                    include  $base_dir . '/artist.php';
                }
            ?>
            <?php include $base_dir. '/contact.php'; ?>

        </div>
        <div class="evt-blk apl-fe-control">
            <a href="javascript:apollo_submit_form( 'admin_frm_step1', 'submit_event' )">
                <div id="arrow" class="apl-fe-event-proceed">
                    <span>
                        <?php _e( 'Proceed to ', 'apollo' ) ?>
                        <div>
                            <?php _e('STEP 2', 'apollo') ?>
                        </div>
                    </span>
                </div>
            </a>
        </div>
        <div class="evt-blk apl-fe-control">
            <?php  include APOLLO_TEMPLATES_DIR.'/pages/dashboard/html/partial/_event/draft-button.php'; ?>
            <a href="">
                <div id="apl-fe-save-draft"><span><?php _e( 'SAVE DRAFT', 'apollo' ) ?></span></div>
            </a>
        </div>
    </div>

</form>
<script>
    jQuery(document).ready(function($){
        var allowedKeys = [8, 37, 38, 39, 40, 46];

        function proccessLimitCharacters(content, max, ele) {
            var len = content.length;
            var charCount = max - len;
            if($(ele).length > 0){
                $(ele).html("<?php echo __('Characters left: ') ?>" +  (charCount < 0 ? 0 : charCount));
            }
            return charCount;
        }

        <?php if($description_charter_limit > 0) { ?>
        $("textarea#apl-event-desc").keydown(function(e){
            var max = <?php echo $description_charter_limit ?>;
            if(max > 0) {
                var content = $("textarea#apl-event-desc").val().replace(/(<[a-zA-Z\/][^<>]*>|\[([^\]]+)\])|/ig,'');
                var charCount = proccessLimitCharacters(content, max,  '#character_count_event_desc');
                if (allowedKeys.indexOf(e.keyCode) != -1) return true;
                if (charCount < 0) {
                    e.stopPropagation();
                    e.preventDefault();
                    return false;
                }
            }
        });

        $("textarea#apl-event-desc").on('change click cut paste input mousemove propertychange selectionchange keyup keypress',function (){
            var max = <?php echo $description_charter_limit ?>;
            if(max > 0) {
                var content = $("textarea#apl-event-desc").val().replace(/(<[a-zA-Z\/][^<>]*>|\[([^\]]+)\])|/ig,'');
                var charCount = proccessLimitCharacters(content, max, '#character_count_event_desc');
                if (charCount < 0) {
                    $("textarea#apl-event-desc").val(content.substring(0, max))
                }
            }
        });
        $("textarea#apl-event-desc").change();

    <?php } ?>

        <?php if($ticket_description_charter_limit > 0) { ?>
        $("textarea#apl-event-addmission").keydown(function(e){
            var max = <?php echo $ticket_description_charter_limit ?>;
            if(max > 0) {
                var content = $("textarea#apl-event-addmission").val().replace(/(<[a-zA-Z\/][^<>]*>|\[([^\]]+)\])|/ig,'');
                var charCount = proccessLimitCharacters(content, max,  '#character_count_event_addmission');
                if (allowedKeys.indexOf(e.keyCode) != -1) return true;
                if (charCount < 0) {
                    e.stopPropagation();
                    e.preventDefault();
                    return false;
                }
            }
        });

        $("textarea#apl-event-addmission").on('change click cut paste input mousemove propertychange selectionchange keyup keypress',function (){
            var max = <?php echo $ticket_description_charter_limit ?>;
            if(max > 0) {
                var content = $("textarea#apl-event-addmission").val().replace(/(<[a-zA-Z\/][^<>]*>|\[([^\]]+)\])|/ig,'');
                var charCount = proccessLimitCharacters(content, max, '#character_count_event_addmission');
                if (charCount < 0) {
                    $("textarea#apl-event-addmission").val(content.substring(0, max))
                }
            }
        });
        $("textarea#apl-event-addmission").change();

    <?php } ?>
    });
</script>