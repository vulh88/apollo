<?php
include APOLLO_TEMPLATES_DIR. '/pages/dashboard/inc/event/apollo-submit-event.php';
if ( ( isset( $_SESSION['added_event_id'] ) && $_SESSION['added_event_id'] ) || get_query_var( '_apollo_event_id' ) ) {
    $event_id = get_query_var( '_apollo_event_id' ) ? get_query_var( '_apollo_event_id' ) : $_SESSION['added_event_id'];

    $event = get_event( get_post( $event_id ) );
    $isSubmit = true;

    if(intval($event_id) !== 0) {
        Apollo_Permission_System::processPermission(array(
                'type' => 'access-event',
                'user_id' => get_current_user_id(),
                'event_id' => $event_id,
                'fail_action' => 'redirect',
                'fail_data' => array(
                    'url' => home_url(). '/user/events',
                ),
            ));
    }

    $submit_obj = new Apollo_Submit_Event( $event->id );
    if ( isset( $_POST['case_action'] ) && $_POST['case_action'] == 'submit_event_step3' ) {
        $isSubmit = $submit_obj->submit_event_step3();
    }

} else {
    wp_redirect(home_url(). '/user/add-event/' );
}

//enable show Registered Organizations drop menu field
$is_show_additional_time_info = ' style="display: none !important"';
if (of_get_option(APL_Theme_Option_Site_Config_SubTab::_APL_ENABLE_EVENT_TIMEINFO_FIELD, 1) ) {
    $is_show_additional_time_info = ' style="display: block !important"';
}

//enable show summary field
$is_show_summary_info = ' style="display: none !important"';
if ( of_get_option(APL_Theme_Option_Site_Config_SubTab::_APL_ENABLE_EVENT_SUMMARY_FIELD, 1) ) {
    $is_show_summary_info = ' style="display: block !important"';
}

//enable show Accessibility info
$is_show_accessibility_info = '';
$enable_accessibility = of_get_option(APL_Theme_Option_Site_Config_SubTab::_APL_ENABLE_EVENT_ACCESSIBILITY_FIELD, 1);
$enable_venue_accessibility_mode = of_get_option(Apollo_DB_Schema::_ENABLE_VENUE_ACCESSIBILITY_MODE, 1);

if ($enable_accessibility ) {
    if(!$enable_venue_accessibility_mode)
        $is_show_accessibility_info = ' style="display: block !important"';
} else{
    $is_show_accessibility_info = ' style="display: none !important"';
}

?>
<div class="dsb-ct">
    <div class="dsb-welc">
        <div class="events step-3">                  
            <?php Apollo_Submit_Event::event_steps(3); ?>
            <?php if( !$isSubmit ): ?>
                <div>
                    <span class="error"><?php _e("Cannot save data. Some error occurred !", 'apollo') ?></span>
                </div>
            <?php endif; ?>
            <div class="evt-blk">
                <div class="event-tt"><?php _e( 'PREVIEW YOUR LISTING', 'apollo' ) ?></div>
            </div>
            <div class="evt-blk">
                <p>
                  <?php _e( 'Below is the information you are about to submit. If you would like to edit any of this information you may return to the previous screen using the “Return to Edit” button below. If you are satisfied with your submission you may click “Submit Listing” below to complete the process. 
                  Please note that we reserve the right to edit submissions for grammar, style and accuracy.', 'apollo' ) ?>
                </p>
            </div>
            
            <?php 

            
                $base_dir = APOLLO_TEMPLATES_DIR. '/pages/dashboard/html/partial/_event/step3';
                include $base_dir. '/general.php'; 
                include $base_dir. '/admission.php'; 
                include $base_dir. '/contact-info.php'; 
                include $base_dir. '/datetime.php'; 
                include $base_dir. '/venue.php'; 
                include $base_dir. '/accessibility.php'; 
            ?>
            
            <div class="evt-blk">
                <form action="" method="post" id="admin-frm-step3" name="admin_frm_step3" 
                      class="admin_form" enctype="multipart/form-data">
                    <?php wp_nonce_field($submit_obj->nonceAction, $submit_obj->nonceName, false); ?>
                    <input type="hidden" value="" name="case_action" id="case_action" />
                    <a href="javascript:apollo_submit_form( 'admin_frm_step3', 'submit_event_step3' )">
                        <div id="event-submit-btn"><span><?php _e( 'SUBMIT LISTING', 'apollo' ) ?></span></div>
                    </a>
                </form>
                
                <?php $event_id = get_query_var( '_apollo_event_id' ); ?>
                <a href="<?php echo home_url() ?>/user/add-event/step-1/<?php echo $event_id; ?>">
                    <div id="arrow-3"><span><?php _e( 'RETURN TO EDIT', 'apollo' ) ?></span></div>
                </a>
            </div>
        </div>
    </div>
</div>