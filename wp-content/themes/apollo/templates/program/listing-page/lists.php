<?php

    if ( $results = $this->get_results() ) {
        foreach ( $results as $p ): 
            
            $e = get_program( $p );
            $summary100 = $e->get_summary(215);
           
        ?>
            <li>
                <div class="search-img"><a href="<?php echo $e->get_permalink() ?>"><?php echo $e->get_image('medium', array(),
                        array(
                            'aw' => true,
                            'ah' => true,
                        ),
                            'normal'
                        ) ?></a></div>
                <div class="info-content" data-url="<?php echo $e->get_permalink() ?>" data-type="link">
                    <div class="search-info"><a href="<?php echo $e->get_permalink() ?>"><span class="ev-tt"><?php echo $e->get_title() ?></span></a>
                       
                        <div class="s-desc edu-des"><?php echo $summary100['text']  ?> <?php echo $summary100['have_more'] ? '...' : '' ?></div>
                    </div>
                </div>
            </li>
        <?php 
        endforeach;
    } else if ( !isset($_GET['s']) && !isset($_GET['keyword']) ) {
        _e( 'No results', 'apollo' );
    }
