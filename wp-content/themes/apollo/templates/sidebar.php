<?php
    // Distribute sidebar

    $avaiable_modules = Apollo_App::get_avaiable_modules();

    $current_type = Apollo_Static_Sidebar::get_current_type();

    if ( !Apollo_App::is_homepage() && ( $avaiable_modules && in_array( $current_type, $avaiable_modules )) )  {
        do_shortcode("[apollo_widgets location='std' key='sidebar-modules-area' current_mod='".$current_type."']");
    }

    if ( !Apollo_App::is_homepage() && ( $avaiable_modules && in_array( $current_type, $avaiable_modules ) && $current_type != Apollo_DB_Schema::_EVENT_PT) )  {
        $key = Apollo_DB_Schema::_SIDEBAR_PREFIX . '-'.  str_replace('-', '_', $current_type);
    }
    else {
        $key = Apollo_DB_Schema::_SIDEBAR_PREFIX . '-primary';
    }
    do_shortcode("[apollo_widgets location='std' key='$key']");


