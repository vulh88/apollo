<?php
    //PAGE: HOME
    if ( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EVENT_PT )
        && in_array(of_get_option( Apollo_DB_Schema::_SPOT_TYPE ), array('medium','slider'))
        && is_front_page() ) :
        // Count again the expired time
        Apollo_App::getTemplatePartCustom(APOLLO_TEMPLATES_DIR. '/medium-slider.php',array(),false,true);
    endif;


    // Only display widget on mobile
    $key = Apollo_DB_Schema::_SIDEBAR_PREFIX . '-primary';
    do_shortcode("[apollo_widgets location='std_cus_pos' key='$key']");


    $active_top_ten = of_get_option( Apollo_DB_Schema::_ACTIVATE_TOPTEN_TAB );
    $features_category = of_get_option( Apollo_DB_Schema::_HOME_CAT_FEATURES, false, true );
    include_once APOLLO_TEMPLATES_DIR.'/events/list-events.php';
    $main_content = ( $active_top_ten || $features_category ) && Apollo_App::is_avaiable_module(Apollo_DB_Schema::_EVENT_PT );

    if ( $top_sub_contain =  Apollo_App::get_static_html_content( Apollo_DB_Schema::_TOP_SUB_CONTAIN) ):

        $t_content = do_shortcode($top_sub_contain);
        $t_content = str_replace('<p></p>','',$t_content);
        if( !empty($t_content) ):
?>
            <div class="sub-content-blk top <?php echo ! $main_content ? 'no-border reset-m-p' : '' ?>">
                <?php echo $t_content; ?>
            </div>
        <?php endif;

    endif;


if ( get_option( 'show_on_front' ) == 'posts' ) {
    include APOLLO_TEMPLATES_DIR. '/home/latest-posts.php';
} else {
    include APOLLO_TEMPLATES_DIR. '/home/featured-events.php';
}

/**
 * Ticket #12641
 */
if (of_get_option(Apollo_DB_Schema::_HOME_CMS_PAGE_EVENT_FEATURED) == 1) {
    $pageId = Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_EVENTS_HOME_FEATURED);
    $eventHomeFeatured = $pageId ? get_post($pageId) : false;
}

if (!empty($eventHomeFeatured) && !empty($eventHomeFeatured->post_content)) { ?>
    <div class="home-event-cms-page apl-internal-content"><p><?php echo $eventHomeFeatured->post_content ?></p></div>
    <?php
}


if ( $bot_sub_contain =  Apollo_App::get_static_html_content( Apollo_DB_Schema::_BOTTOM_SUB_CONTAIN) ):
        $b_content = do_shortcode($bot_sub_contain);
        $b_content = str_replace('<p></p>','',$b_content);
        if( !empty($b_content) ):
?>
            <div class="sub-content-blk <?php echo ! $main_content && ! $top_sub_contain ? 'no-border reset-m-p' : '' ?>">
                <?php echo $b_content; ?>
            </div>
<?php endif;
endif;