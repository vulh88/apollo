<div class="breadcrumbs">
    <ul class="nav">
        <li><a href="<?php echo get_home_url(get_current_blog_id()) ?>"><?php _e('Home', 'apollo') ?></a></li>
        <?php $classifiedCustomLabel = Apollo_App::getCustomLabelByModuleName(Apollo_DB_Schema::_CLASSIFIED); ?>
        <?php if ( isset( $_GET['keyword'] ) || isset( $_GET['city'] )
            || isset( $_GET['state'] ) || isset( $_GET['zip'] )) { ?>
            <li> <a href="<?php echo Apollo_App::getCustomUrlByModuleName(Apollo_DB_Schema::_CLASSIFIED); ?>"><?php echo $classifiedCustomLabel; ?></a></li>
            <li> <span><?php _e('Search', 'apollo') ?></span></li>
        <?php } else { ?>
            <li><a href="<?php echo Apollo_App::getCustomUrlByModuleName(Apollo_DB_Schema::_CLASSIFIED); ?>"><?php echo $classifiedCustomLabel; ?></a></li>
            <?php if (!empty($currentTerm)) {?>
            <li> <span><?php echo $currentTerm->name; ?></span></li>
        <?php } } ?>
    </ul>
</div>

<?php
if(!Apollo_App::is_avaiable_module( Apollo_DB_Schema::_CLASSIFIED )) {
    wp_safe_redirect( '/' );
}

global $post;
$current_url = home_url() . $_SERVER["REQUEST_URI"];
include APOLLO_SRC_DIR. '/classified/inc/class-classified-search.php';
$search_obj = new Apollo_Classified_Page();
$search_obj->set_main_post($post);
$search_obj->search();
?>

<?php if ( isset($_GET['keyword']) ): ?>
    <div class="search-tt mar-b-0">
        <?php $search_obj->render_result_title() ?>
    </div>
<?php endif; ?>
<div class="search-bkl">
    <div class="b-share-cat art">
        <?php
        SocialFactory::social_btns( array(
                'info' => $search_obj->getShareInfo(),
                'id' => !empty($post) && is_object($post) ? $post->ID : 0,
            )
        );
        ?>
    </div>
</div>

<?php
if ( $search_obj->get_results() ):
    ?>

    <?php
    if ( $search_obj->have_pag()):
        ?>
        <div class="search-bkl">
            <?php
            $pag_html = '';
            ?>
            <div class="blk-paging">
                <?php $pag_html = $search_obj->render_pagination();
                echo $pag_html;
                ?>
            </div>
            <nav class="pg">
                <li class="txt"><span><?php _e( 'Show', 'apollo' ) ?>:</span></li>
                <li>
                    <div class="pg-rad">
                        <input data-href="<?php echo $search_obj->get_display_type_url() ?>"
                               type="radio" name="radioshowpg" value="pg" <?php echo ! $search_obj->is_onepage() ? 'checked="checked"' : '' ?> class="rad-pg"><span class="pg"></span><span><?php _e( 'Paginated', 'apollo' ) ?></span>
                    </div>
                </li>

                <li>
                    <div class="pg-rad">
                        <input data-href="<?php echo $search_obj->get_display_type_url( '1' ); ?>"
                               type="radio" name="radioshowpg" value="op" <?php echo $search_obj->is_onepage() ? 'checked="checked"' : '' ?> class="rad-pg"><span class="pg"></span><span><?php _e( 'One page', 'apollo' ) ?> </span>
                    </div>
                </li>
            </nav>
        </div>
    <?php endif; endif; ?>

    <?php
    if(  $search_obj->is_view_type_page()  ){
        $tax_terms =  wp_get_object_terms(Apollo_App::getUnexpiredClassifiedIds(),'classified-type');?>
        <div class="blk-show-list">
            <ul class="list-show">
                <label class="ttl-list <?php echo !isset($_REQUEST['term']) && !isset($_REQUEST['keyword']) ? 'tag-active' : ''?>"><?php _e('Sort by', 'apollo'); ?>:<a href="<?php echo Apollo_App::getCustomUrlByModuleName(Apollo_DB_Schema::_CLASSIFIED); ?>/?view=table&type" ><span>&nbsp;All&nbsp;</span></a></label>
                <?php foreach ($tax_terms as $tax) { ?>
                <li class="<?php echo isset($_REQUEST['term']) && $tax->term_id == $_REQUEST['term'] ? 'tag-active' : ''?>"><a href="<?php echo Apollo_App::getCustomUrlByModuleName(Apollo_DB_Schema::_CLASSIFIED); ?>/?view=table&term=<?php echo $tax->term_id ?>"><?php echo $tax->name?></a></li>
                <?php } ?>
            </ul>
        </div>
    <?php }  ?>
    <?php if ( $search_obj->get_results() ):
    $page = get_post(Apollo_App::getClassifiedHeaderPage());
    if( !empty($page)){ ?>
        <div class="sub-content-blk custom-block"">
            <?php
            echo Apollo_App::the_content($page->post_content);
            ?>
        </div>
    <?php }
    $displayPublic = 0;
    if(!empty($currentTerm)){
        $displayPublic = get_apollo_term_meta($currentTerm->term_id, Apollo_DB_Schema::_APL_TAXONOMY_DISPLAY_PUBLIC_FIELD, true);
    }

    if (!empty($termDescription) && !empty($displayPublic)) {
        if ($displayPublic) { ?>
            <div class="sub-content-blk custom-block padding-10px">
                <?php echo Apollo_App::the_content($termDescription); ?>
            </div>
        <?php }
    } ?>
    <div class="search-bkl">
        <nav class="type">
            <ul>
                <li <?php echo $search_obj->is_view_type_page() ? 'class="current"' : '' ?> ><a href="<?php echo $search_obj->get_template_btn_url('table') ?>"><i class="fa fa-table fa-2x"></i></a></li>
                <li <?php echo $search_obj->is_view_type_page(1,'thumb') ? 'class="current"' : '' ?> ><a href="<?php echo $search_obj->get_template_btn_url() ?>"><i class="fa fa-th fa-2x"></i></a></li>
                <li <?php echo $search_obj->is_view_type_page(2,'list') ? 'class="current"' : '' ?>><a href="<?php echo $search_obj->get_template_btn_url( 'list' ) ?>"><i class="fa fa-bars fa-2x"></i></a></li>
            </ul>
        </nav>
    </div>
<?php  endif; ?>


<?php
if ($search_obj->is_view_type_page() ) {
$operator = isset($_REQUEST['keyword']) || isset($_REQUEST['term']) || isset($_REQUEST['view']) ?  $search_obj->getCurFullUrl().'&' : '?';
$order = isset($_REQUEST['order']) && $_REQUEST['order'] == 'asc' ? 'desc' : 'asc';
$ascActiveClass =  isset($_REQUEST['order']) && $_REQUEST['order'] == 'asc' ? 'sort-active' : '';
$descActiveClass =  isset($_REQUEST['order']) && $_REQUEST['order'] == 'desc' ? 'sort-active' : '';

?>
<div class="blk-list-table classified-column">
    <div class="table-info">
        <?php if ( $search_obj->get_results() ){ ?>
        <table class="table">
            <thead>
            <tr class="bg-gray">
                <th><a href="<?php echo $operator ?>orderby=post_date&order=<?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] =='post_date' ?$order : 'asc'?>"><?php echo __('posted','apollo'); ?></a>
                    <a href="<?php echo $operator ?>orderby=post_date&order=asc" class="icon-sort ic-sort-up <?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] =='post_date' ? $ascActiveClass : ''?>"><i class="fa fa-caret-up"></i></a>
                    <a href="<?php echo $operator ?>orderby=post_date&order=desc" class="icon-sort ic-sort-down <?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] =='post_date' ? $descActiveClass : ''?>"><i class="fa fa-caret-down"></i></a>
                </th>

                <th id="column-title"><a href="<?php echo $operator ?>orderby=post_title&order=<?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby']=='post_title' ?$order : 'asc'?>"><?php echo __('title','apollo'); ?></a>
                    <a  href="<?php echo $operator ?>orderby=post_title&order=asc" class="icon-sort ic-sort-up <?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] =='post_title' ? $ascActiveClass : ''?>"><i class="fa fa-caret-up"></i></a>
                    <a  href="<?php echo $operator ?>orderby=post_title&order=desc" class="icon-sort ic-sort-down <?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] =='post_title' ? $descActiveClass : ''?>"><i class="fa fa-caret-down"></i></a>
                </th>
                <th  id="column-org">
                    <a href="<?php echo $operator ?>orderby=organization&order=<?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] =='organization' ?$order : 'asc'?>"><?php echo __('organization','apollo'); ?></a>
                    <a  href="<?php echo $operator ?>orderby=organization&order=asc" class="icon-sort ic-sort-up <?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] =='organization' ? $ascActiveClass : ''?>"><i class="fa fa-caret-up"></i></a>
                    <a  href="<?php echo $operator ?>orderby=organization&order=desc" class="icon-sort ic-sort-down <?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] =='organization' ? $descActiveClass : ''?>"><i class="fa fa-caret-down"></i></a>
                </th>
                <th>
                    <a href="<?php echo $operator ?>orderby=city&order=<?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] =='city' ?$order : 'asc'?>"><?php echo __('city','apollo'); ?></a>
                    <a href="<?php echo $operator ?>orderby=city&order=asc" class="icon-sort ic-sort-up <?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] =='city' ? $ascActiveClass : ''?>"><i class="fa fa-caret-up"></i></a>
                    <a href="<?php echo $operator ?>orderby=city&order=desc" class="icon-sort ic-sort-down <?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] =='city' ? $descActiveClass : ''?>"><i class="fa fa-caret-down"></i></a>
                <th>
                    <a href="<?php echo $operator ?>orderby=deadline&order=<?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] =='deadline' ?$order : 'asc'?>"><?php echo __('deadline','apollo'); ?></a>
                    <a href="<?php echo $operator ?>orderby=deadline&order=asc" class="icon-sort ic-sort-up <?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] =='deadline' ? $ascActiveClass : ''?>" ><i class="fa fa-caret-up"></i></a>
                    <a href="<?php echo $operator ?>orderby=deadline&order=desc" class="icon-sort ic-sort-down <?php echo isset($_REQUEST['orderby']) && $_REQUEST['orderby'] =='deadline' ? $descActiveClass : ''?>"><i class="fa fa-caret-down"></i></a>
            </tr>
            </thead>
            <tbody class="classified-table-view">
    <?php echo $search_obj->render_html();

        $container = 'classified-table-view';
    ?>
            </tbody>
        </table>
        <?php } else {
            _e( 'No results', 'apollo' );
        }
        ?>

    </div>
</div>

<?php } else { ?>
    <div class="search-bkl">
        <?php

        if ($search_obj->is_view_type_page(1, 'thumb')) {
            $container = $class = 'search-artist-thumb';
        } elseif ($search_obj->is_view_type_page(2, 'list')) {
            $container = $class = 'search-classified-list';
        } ?>
        <nav class="<?php echo $class; ?>">
            <?php echo $search_obj->render_html(); ?>
        </nav>

    </div>
    <?php } ?>


<?php
$is_view_more = $search_obj->have_more() && $search_obj->is_onepage();
?>
<input name="search-have-more" value="<?php echo $search_obj->have_more() ?>" type="hidden" >

<?php if ( ! $search_obj->is_map_page() ):
    $query_string = $search_obj->get_current_query_string();
    $query_string = add_query_arg(array(
        'page' => 2,
    ), $query_string);

    $query_string = ltrim($query_string, '?');
    ?>
    <div <?php if ( ! $is_view_more ) echo 'style="display: none"';  ?> id="load-more" class="load-more b-btn load-more-search load-more-artist-search-page">
        <a href="javascript:void(0);"
           data-ride="ap-more"
           data-holder=".<?php echo $container ?>>:last"
           data-sourcetype="url"
           data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_get_more_search_classified&' . $query_string) ?>"
           data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
           data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
           class="btn-b arw"><?php _e('SHOW MORE', 'apollo') ?>
        </a>
    </div>
<?php endif; ?>

<div class="search-bkl">
    <?php if ( $search_obj->have_pag()):  ?>
        <div class="blk-paging">
            <?php echo $pag_html; ?>
        </div>
    <?php endif; ?>
</div>