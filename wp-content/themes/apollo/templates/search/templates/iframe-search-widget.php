<?php
$ifswid = isset($template_args['ifswid']) ? $template_args['ifswid'] : '';
$sciptTakeTo404Page = "<script>window.location.href = '".site_url('/404')."'; </script>";
if(empty($ifswid)){
    echo $sciptTakeTo404Page;
    die;
}
$iframePost = get_post($ifswid);
if(empty($iframePost) || $iframePost->post_type != Apollo_DB_Schema::_IFRAME_SEARCH_WIDGET_PT || $iframePost->post_status != 'publish'){
    echo $sciptTakeTo404Page;
    die;
}
$iFrameModuleHandler = null;
if(class_exists('Apollo_IFrame_Search_Widget')){
    $iFrameModuleHandler = get_iframe_search_widget($ifswid);
}
if(empty($iFrameModuleHandler)){
    _e("The IFrame Search Widget Form Class Handler does not exist.","apollo");
    exit;
}

$title = __('Find An Event', 'apollo');
$themeUri       = get_stylesheet_directory();
/* load dependent js/css for search widget */
$searchWidgetScriptPaths = array(
    $themeUri . '/assets/js/jquery-lazy-load.js',//$assetJS['apollo-jquery-lazy-load'],
    $themeUri . '/assets/js/plugins/select2.js',//$assetJS['apollo-jquery-lazy-load'],
    $themeUri . '/assets/js/plugins.js',//$assetJS['apollo-js-plugins'],
    $themeUri . '/assets/js/ex-plugins.js',//$assetJS['apollo-js-plugins'],
    $themeUri . '/assets/js/global.js',//$assetJS['apollo-js-global'],
    $themeUri . '/assets/js/start.js',//$assetJS['apollo-js-start'],
    $themeUri . '/assets/js/function.js',//$assetJS['apollo-js-function'],
);

$searchWidgetCSSPaths = array(
    $themeUri . '/assets/css/select2.min.css',//,$assetCss['apl-css'],
    $themeUri . '/assets/css/style.css',//,$assetCss['apl-css'],
    $themeUri . '/assets/css/dev.css',//,$assetCss['apl-dev_css'],
    $themeUri . '/assets/css/reset.css',//,$assetCss['apl-resetcss'],
    $themeUri . '/assets/css/jquery-ui-calendar.css',//$assetCss['apl-jquery-ui-css'],
);

$iframeSettings = $iFrameModuleHandler->getAllInfoIFrameSearchWidget($ifswid);
$defaultColor = $iframeSettings[APL_Iframe_Search_Widget_Const::META_COLOR];
$htmlBeforeWidget = $iframeSettings[APL_Iframe_Search_Widget_Const::META_HTML_BEFORE];
$htmlBeforeWidget = Apollo_App::the_content($htmlBeforeWidget);
$htmlAfterWidget = $iframeSettings[APL_Iframe_Search_Widget_Const::META_HTML_AFTER];
$htmlAfterWidget = Apollo_App::the_content($htmlAfterWidget);
$enabledVenue = $iframeSettings[APL_Iframe_Search_Widget_Const::META_ENABLE_VENUE];
$venues = $iframeSettings[APL_Iframe_Search_Widget_Const::META_VENUE];
$enabledCity = $iframeSettings[APL_Iframe_Search_Widget_Const::META_ENABLE_CITIES];
$cities = $iframeSettings[APL_Iframe_Search_Widget_Const::META_CITIES];
$defaultCity = $iframeSettings[APL_Iframe_Search_Widget_Const::META_DEFAULT_CITY];
$enabledCategory = $iframeSettings[APL_Iframe_Search_Widget_Const::META_ENABLE_CATEGORY];
$categories= $iframeSettings[APL_Iframe_Search_Widget_Const::META_CATEGORY];
$enabledRegion = $iframeSettings[APL_Iframe_Search_Widget_Const::META_ENABLE_REGION];
$regions = $iframeSettings[APL_Iframe_Search_Widget_Const::META_REGION];
$eventCategories = Apollo_Event::get_tree_event_style();
$isAvailableVenueModule =  Apollo_App::is_avaiable_module( Apollo_DB_Schema::_VENUE_PT );
$searchData = array(
    'keyword' => isset($_GET['keyword'])?$_GET['keyword']:'',
    'start_date' => isset($_GET['start_date'])?$_GET['start_date']:'',
    'end_date' => isset($_GET['end_date'])?$_GET['end_date']:'',
    'term' => isset($_GET['term'])?$_GET['term']:'',
    'event_location' => isset($_GET['event_location'])?$_GET['event_location']:'',
    'view' => isset($_GET['view'])?$_GET['view']:'',
    'save_lst_list' => isset($_GET['save_lst_list'])?$_GET['save_lst_list']:'',
    'city' => isset($_GET['event_city'])? urldecode($_GET['event_city']) :  urldecode(of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY,'')),
    'region' => isset($_GET['event_region'])?$_GET['event_region']:''
);

?>
<div class="wrapper ifsw-blk" style="display:none;">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css"/>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"/>
    <style type="text/css">
        .widget-search{
            display: block !important;
            visibility: visible !important;
        }
        body,  body.custom-background {
            background: none !important;
        }
        body{
            overflow: hidden;
        }
        body > div, body > header, body  > footer, body > section {
            display: none;
        }
        body .wrapper.ifsw-blk{
            display: block;
        }
        .r-blk .r-ttl, .tt-bar a, .map_slider_item.active .title_trim{
            color: <?php echo $defaultColor; ?> !important;
        }
        .main-menu, .blog-itm .b-time,
        .r-search .r-ttl i.fa,
        #_popup_choose_event .fa-close
        {
            background: <?php echo $defaultColor; ?> !important;
        }
        .form-event .s-rw .lst-lik a:hover, .btn.btn-l:hover, .newsletter-frm .btn.btn-l:hover, .calendar-ipt .btn.btn-l:hover, .sub-blk .btn.btn-l:hover, .btn.btn-l:hover{
            background: <?php echo $defaultColor; ?> !important;
            border: 1px solid  <?php echo $defaultColor; ?> !important;
        }
        .b-btn .btn-b:hover {
            background: <?php echo $defaultColor; ?> !important;
            border: 1px solid  <?php echo $defaultColor; ?> !important;
        }
        .form-event .s-rw .lst-lik a:hover,
        .form-event .s-rw .lst-lik a.active{
            background: <?php echo $defaultColor; ?> !important;
            border: 1px solid  <?php echo $defaultColor; ?> !important;
        }
        .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
            color: <?php echo $defaultColor; ?> !important;
            border-color: <?php echo $defaultColor; ?> !important;
        }
        .b-btn .btn-b{
            border-color: <?php echo $defaultColor; ?> !important;
        }
        .ui-state-hover, .ui-widget-content .ui-state-hover,
        .ui-widget-header .ui-state-hover, .ui-state-focus,
        .ui-widget-content .ui-state-focus,
        .ui-widget-header .ui-state-focus,
        .fa.fa-caret-up.ttip {
            color: <?php echo $defaultColor; ?> !important;
        }
        span.select2 {
            width: 100% !important;
        }
        .cs-ef.cs-ef.form-event .s-rw .lst-lik span.no-line, .cs-ef.form-event .s-rw .lst-lik a.seven, .cs-ef.form-event .s-rw .lst-lik a.fourteen, .cs-ef.form-event .s-rw .lst-lik a.thirsty{
            display: inline-block !important;
        }
        .ifsw-btn-cs#ifsw-search-btn{
            width: 70%;
        }
        .ifsw-btn-cs#clear-event-form{
            width: 25%;
            margin-top: 0 !important;
        }
        .widget-search.widget-search-below,
        .widget-search.widget-search-above{
            width: 100%;
        }
        .blockUI.blockOverlay{
            background-color: transparent !important;
        }
    </style>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery(window).load(function(){
                jQuery('.ifsw-blk').css({"display":"block"});
                jQuery('.sf-rsb-widget').remove();
            });
        });
    </script>
    <div class="r-blk r-search widget-search widget-search-above"><?php echo $htmlBeforeWidget; ?></div>
    <div class="r-blk r-search widget-search iframe">
        <h3 class="r-ttl"><i class="fa fa-search fa-flip-horizontal fa-lg"> </i><span><?php echo $title ?>        </span></h3>

        <div class="r-blk-ct">
            <form method="get" id="search-event" action="<?php echo home_url() ?>/event/" class="cs-ef form-event" target="_blank">

                <div class="s-rw">
                    <input type="text" name="keyword" value="<?php echo esc_attr(Apollo_App::clean_data_request($searchData['keyword'])) ?>" placeholder="<?php _e('Search by Keyword', 'apollo') ?>" class="inp inp-txt event-search event-search-custom">

                </div>


                <div class="el-blk e-s-wg-c-l">
                    <label><?php _e('Search by date:', 'apollo') ?></label>

                    <div class="calendar-ipt clearfix date-range-blk">
                    <span style="position: relative;" class="custom date-picker">
                        <input data-date-format="mm-dd-yy" name="start_date" value="<?php echo $searchData['start_date']; ?>" type="text"  size="12" id="c-s" placeholder="<?php _e('Start', 'apollo');?>" class="inp inp-txt caldr">
                    </span>
                    <span style="position: relative;" class="custom date-picker">
                        <input data-date-format="mm-dd-yy" name="end_date" value="<?php echo $searchData['end_date']; ?>" type="text" size="12" id="c-e" placeholder="<?php _e('End', 'apollo');?>" class="inp inp-txt caldr">
                    </span>

                        <!-- Ticket #11487: determine current date format -->
                        <input name="date_format" type="hidden" value="m-d-Y">

                    </div>
                </div>
                <?php if(strtoupper($enabledCategory) == 'ON' && !empty($categories)) : ?>
                    <div class="el-blk">
                        <div class=" select-bkl">
                            <select class="" name="term" id="event-category-select">
                                <option value=""><?php echo _e('Select Category','apollo') ?></option>
                                <?php
                                Apollo_Event::build_option_tree($categories,$searchData['term'], Apollo_Display_Config::TREE_MAX_LEVEL, 'event-type');
                                ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if( strtoupper($enabledVenue) == 'ON' && !empty($venues) && $isAvailableVenueModule): ?>
                    <div class="el-blk">
                        <div class=" select-bkl">
                            <select name="event_location" id="event-location-select">
                                <option value=""><?php echo _e('Select Venue','apollo') ?></option>
                                <?php
                                if($venues){
                                    foreach($venues as $eventVenue){
                                        $selected = '';
                                        if($eventVenue->ID == $searchData['event_location'])
                                            $selected = 'selected';
                                        ?>
                                        <option <?php echo $selected; ?> value="<?php echo $eventVenue->ID ?>">
                                            <?php echo $eventVenue->post_title; ?>
                                        </option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if( strtoupper($enabledCity) == 'ON' && !empty($cities) && $isAvailableVenueModule): ?>
                    <div class="el-blk">
                        <div class=" select-bkl">
                            <select name="event_city" id="event-city-select">
                                <?php echo Apollo_App::renderCities($cities, $searchData); ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>
                <?php endif; ?>

                <!-- show dropdownlist regions -->
                <?php if( strtoupper($enabledRegion) == 'ON' && !empty($regions) && $isAvailableVenueModule ): ?>
                    <div class="el-blk">
                        <div class=" select-bkl">
                            <select name="event_region" id="event-region-select">
                                <option value=""><?php echo __("Select Region","apollo"); ?></option>
                                <?php
                                if($regions){
                                    foreach($regions as $region){
                                        $selected = '';
                                        if($region == $searchData['region'])
                                            $selected = 'selected';
                                        ?>
                                        <option <?php echo $selected; ?> value="<?php echo $region ?>">
                                            <?php echo $region; ?>
                                        </option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>
                <?php endif; ?>
                <!-- end show dropdownlist regions -->

                <div class="s-rw el-blk iframe">
                    <label><?php _e('Search by date range', 'apollo') ?>:</label>

                    <div class="lst-lik">
                        <a  data-attr="today" class="today select-date <?php if($searchData['save_lst_list'] == 'today') echo 'active' ?> "><?php _e('Today', 'apollo'); ?></a>
                        <a  data-attr="tomorrow" class="tomorrow select-date   <?php if($searchData['save_lst_list'] == 'tomorrow') echo 'active' ?>"><?php _e('Tomorrow', 'apollo'); ?></a>
                        <a  data-attr="weekend" class="weekend select-date  <?php if($searchData['save_lst_list'] == 'weekend') echo 'active' ?>" ><?php _e('Weekend', 'apollo'); ?></a>
                        <span data-attr="next" class="no-line "><?php _e('next', 'apollo'); ?></span>
                        <a  data-attr="seven-day" class="seven select-date  <?php if($searchData['save_lst_list'] == 'seven-day') echo 'active' ?>"><?php _e('7', 'apollo'); ?></a>
                        <a  data-attr="fourteen-day" class="fourteen select-date  <?php if($searchData['save_lst_list'] == 'fourteen-day') echo 'active' ?>"><?php _e('14', 'apollo'); ?></a>
                        <a  data-attr="thirsty-day" class="thirsty select-date <?php if($searchData['save_lst_list'] == 'thirsty-day') echo 'active' ?>"><?php _e('30', 'apollo'); ?></a></div>
                </div>
                <input type="hidden" id="save-lst-list" name="save_lst_list" value="<?php echo $searchData['save_lst_list'] ?>">
                <div class="b-btn">
                    <button  type="submit" id="ifsw-search-btn" class="ifsw-btn-cs btn btn-b edu-btn">
                        <?php _e( 'SEARCH', 'apollo' ) ?>
                    </button>
                    <button  type="button" id="clear-event-form" style="margin-top: 0 !important;" data-rise="" class="ifsw-btn-cs custom-button btn btn-b edu-btn" data-form="#search-event">
                        <?php _e( 'RESET', 'apollo' ) ?>
                    </button>

                </div>
                <input type="hidden" name="view" value="<?php echo $searchData['view']; ?>">
            </form>
        </div>
    </div>
    <div class="r-blk r-search widget-search widget-search-below"><?php echo $htmlAfterWidget; ?></div>
</div>

    <?php
//    aplDebug($searchWidgetScriptPaths);
//    aplDebug($searchWidgetCSSPaths);
    foreach($searchWidgetScriptPaths as $sp){
        if(file_exists($sp)){
            echo "<script>";
            echo @file_get_contents($sp);
            echo "</script>";
        }
    }
    ?>

    <?php
    foreach($searchWidgetCSSPaths as $sp){
        if(file_exists($sp)){
            echo "<style type=\"text/css\">";
            echo @file_get_contents($sp);
            echo "</style>";
        }
    }
    ?>

