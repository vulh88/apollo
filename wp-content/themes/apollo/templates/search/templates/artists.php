<?php $artistCustomLabel = Apollo_App::getCustomLabelByModuleName(Apollo_DB_Schema::_ARTIST_PT)?>
<div class="breadcrumbs">
    <ul class="nav">
        <li><a href="<?php echo get_home_url(get_current_blog_id()) ?>"><?php _e('Home', 'apollo') ?></a></li>
        
        <?php if ( isset( $_GET['keyword'] ) || isset( $_GET['artist_style'] )
            || isset( $_GET['artist_medium'] ) || isset( $_GET['city'] ) || isset( $_GET['last_name'] ) ) { ?>
        <li> <a href="<?php echo Apollo_App::getCustomUrlByModuleName(Apollo_DB_Schema::_ARTIST_PT); ?>"><?php echo $artistCustomLabel ?></a></li>
        <li> <span><?php _e('Search', 'apollo') ?></span></li>
        <?php } else { ?>
        <li> <span><?php echo $artistCustomLabel ?></span></li>
        <?php } ?>
    </ul>
</div>

<?php
if(!Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ARTIST_PT )) {
    wp_safe_redirect( '/' );
}

include APOLLO_SRC_DIR. '/artist/inc/factory/ArtistsFactory.php';
$factory = new ArtistsFactory();

$artistActiveMemberField = of_get_option(Apollo_DB_Schema::_ARTIST_ACTIVE_MEMBER_FIELD, false);
if($artistActiveMemberField){
    $activatedTab = !empty($_GET['artist_member']) ? $_GET['artist_member'] : 'premium';
    $search_obj = $factory->get($activatedTab);

    $tabMoreArtist = of_get_option(Apollo_DB_Schema::_FILTER_MORE_ARTIST_MEMBER_LABEL, 'normal');
    $artistMemberTabTotals = array(
        'premium'   => 0,
        $tabMoreArtist   => 0,
    );

    $artistMemberTabTotals[$activatedTab] = $search_obj->getTotal();

    foreach ($artistMemberTabTotals as $key => $Total) {
        if ($key == $activatedTab) continue;
        $searchTotalObj = $factory->get($key, true);
        $artistMemberTabTotals[$key] = $searchTotalObj->getTotal();
    }

} else {
    $current_url = home_url() . $_SERVER["REQUEST_URI"];
    $search_obj = $factory->get();
}

global $post;
$search_obj->set_main_post($post);

?>

<?php if ( isset($_GET['keyword']) ): ?>
<div class="search-tt mar-b-0">
    <?php $search_obj->render_result_title() ?>
</div>
<?php endif; ?>

<?php if ( $search_obj->have_pag()): ?>
<div class="search-bkl">
    <?php
    $pag_html = '';
    ?>
        <div class="blk-paging">
            <?php $pag_html = $search_obj->render_pagination();
            echo $pag_html;
            ?>
        </div>
        <nav class="pg">
            <li class="txt"><span><?php _e( 'Show', 'apollo' ) ?>:</span></li>
            <li>
                <div class="pg-rad">
                    <input data-href="<?php echo $search_obj->get_display_type_url() ?>"
                           type="radio" name="radioshowpg" value="pg" <?php echo ! $search_obj->is_onepage() ? 'checked="checked"' : '' ?> class="rad-pg"><span class="pg"></span><span><?php _e( 'Paginated', 'apollo' ) ?></span>
                </div>
            </li>

            <li>
                <div class="pg-rad">
                    <input data-href="<?php echo $search_obj->get_display_type_url( '1' ); ?>"
                           type="radio" name="radioshowpg" value="op" <?php echo $search_obj->is_onepage() ? 'checked="checked"' : '' ?> class="rad-pg"><span class="pg"></span><span><?php _e( 'One page', 'apollo' ) ?> </span>
                </div>
            </li>
        </nav>
</div>
<?php endif; ?>
<div class="search-bkl">
    <div class="b-share-cat art">
        <?php
        SocialFactory::social_btns( array(
                'info' => $search_obj->getShareInfo(),
                'id' => !empty($post) && is_object($post) ? $post->ID : 0,
            )
        );
        ?>
    </div>
</div>
<?php
$page = get_post(Apollo_App::getArtistHeaderPage());
if( !empty($page)){ ?>
    <div class="sub-content-blk custom-block">
        <?php
        echo Apollo_App::the_content($page->post_content);
        ?>
    </div>
<?php }

$displayPublic = isset($currentTerm) && $currentTerm ? get_apollo_term_meta($currentTerm->term_id, Apollo_DB_Schema::_APL_TAXONOMY_DISPLAY_PUBLIC_FIELD, true) : false;

if (isset($termDescription) && !empty($displayPublic)) {
    if ($displayPublic) { ?>
        <div class="sub-content-blk custom-block padding-10px">
            <?php echo Apollo_App::the_content($termDescription); ?>
        </div>
    <?php }
} ?>

<!--@ticket #17079: 0002366: wpdev19 - [Artist] Separate member artists and normal artists in two tabs-->
<?php if($artistActiveMemberField) {
    $artistFeaturedLabel = of_get_option(Apollo_DB_Schema::_ARTIST_FEATURED_LABEL, 'Featured Creatives');
    $artistNormalLabel =  of_get_option(Apollo_DB_Schema::_ARTIST_NORMAL_LABEL, 'More Creatives');
    ?>
        <nav class="nav-tab">
            <ul class="artist-member-tab">
                <li class="<?php echo $search_obj->getArtistMember() == 'premium' ? 'selected' : ''?>">
                    <a href="<?php echo $search_obj->get_template_by_artist_member( 'premium' ) ?>">
                        <?php echo $artistFeaturedLabel ?> (<?php echo $artistMemberTabTotals['premium'] ?>)
                    </a>
                </li>
                <li class="<?php echo $search_obj->getArtistMember() == $tabMoreArtist ? 'selected' : '' ?>">
                    <a href="<?php echo $search_obj->get_template_by_artist_member( $tabMoreArtist ) ?>">
                        <?php echo $artistNormalLabel ?>  (<?php echo $artistMemberTabTotals[$tabMoreArtist] ?>)
                    </a>
                </li>
            </ul>
        </nav>
        <div class="tab-bt tab-bt-custom artist-member-tab-bt"></div>
    <?php } ?>

<?php if ( $search_obj->get_results() ):?>
    <div class="search-bkl">
        <nav class="type">
            <ul>
                <li <?php echo !$search_obj->is_list_page(of_get_option(Apollo_DB_Schema::_ARTIST_DEFAULT_VIEW_TYPE)) ? 'class="current"' : '' ?> ><a href="<?php echo $search_obj->get_template_btn_url() ?>"><i class="fa fa-th fa-2x"></i></a></li>
                <li <?php echo $search_obj->is_list_page(of_get_option(Apollo_DB_Schema::_ARTIST_DEFAULT_VIEW_TYPE)) ? 'class="current"' : '' ?>><a href="<?php echo $search_obj->get_template_btn_url( 'list' ) ?>"><i class="fa fa-bars fa-2x"></i></a></li>
            </ul>
        </nav>
    </div>
<?php endif; ?>

<div class="search-bkl">
    <?php
    if ( $search_obj->is_list_page(of_get_option(Apollo_DB_Schema::_ARTIST_DEFAULT_VIEW_TYPE)) ) {
        $container = $class = "search-artist-list";
    }
    else {
        $container = $class = 'search-artist-thumb';
    }
    ?>
    <nav class="<?php echo $class; ?>">
        <?php echo $search_obj->render_html(); ?>
    </nav>
</div>

<?php $is_view_more = $search_obj->have_more() && $search_obj->is_onepage(); ?>
<input name="search-have-more" value="<?php echo $search_obj->have_more() ?>" type="hidden" >

<?php if ( ! $search_obj->is_map_page() ):
    $query_string = $search_obj->get_current_query_string();
    $query_string = add_query_arg(array(
            'page' => 2,
        ), $query_string);

    $query_string = ltrim($query_string, '?');
    ?>
    <div <?php if ( ! $is_view_more ) echo 'style="display: none"';  ?> id="load-more" class="load-more b-btn load-more-search load-more-artist-search-page">
        <a href="javascript:void(0);"
           data-ride="ap-more"
           data-holder=".<?php echo $container ?>>:last"
           data-sourcetype="url"
           data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_get_more_search_artist&' . $query_string) ?>"
           data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
           data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
           class="btn-b arw"><?php _e('SHOW MORE', 'apollo') ?>
        </a>
    </div>
<?php endif; ?>

<div class="search-bkl">
    <?php if ( $search_obj->have_pag()):  ?>
        <div class="blk-paging">
            <?php echo $pag_html; ?>
        </div>
    <?php endif; ?>
</div>