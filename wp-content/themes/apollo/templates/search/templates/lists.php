<?php

    if ( $results = $this->get_results() ) {
        /** @Ticket #13137 */
        $hasPlaceholderImg = of_get_option(Apollo_DB_Schema::_ENABLE_PLACEHOLDER_IMG, 1);
        foreach ( $results as $p ):

            if($p->post_type === Apollo_DB_Schema::_ARTIST_PT) {
                $e = get_artist( $p );
                $summary100 = $e->get_summary(100);
                $link = $e->get_permalink();
                /** @Ticket #13137 */
                $image = $e->get_image_with_option_placeholder('thumbnail', array(),
                    array(
                        'aw' => true,
                        'ah' => true,
                    ),
                    'normal', $hasPlaceholderImg);

                ?>
                <li>
                    <div class="search-img <?php echo !$image ? 'no-place-holder-cate' : ''; ?>"><a href="<?php echo $link ?>"><?php echo $image; ?></a></div>
                    <div class="info-content" data-type="link" data-url="<?php echo $link ?>">
                        <div class="search-info"><a href="<?php echo $link ?>"><span class="ev-tt"><?php echo $e->get_title() ?></span></a>
                            <div class="career"><?php echo $e->generate_artist_categories() ?> </div>
                        </div>
                    </div>
                </li>
                <?php

            }
            else if($p->post_type === Apollo_DB_Schema::_PROGRAM_PT) {
                $e = get_program( $p );
                
                $cat = $e->get_categories();
                
                $summary = $e->get_content( 100 );
                $cname = $e->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_CNAME, Apollo_DB_Schema::_APL_PROGRAM_DATA );
                $phone = $e->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_PHONE, Apollo_DB_Schema::_APL_PROGRAM_DATA );
                $email = $e->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_EMAIL, Apollo_DB_Schema::_APL_PROGRAM_DATA );
                $location = $e->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_LOCATION, Apollo_DB_Schema::_APL_PROGRAM_DATA );
                
                ?>
                <li>
                    <div class="search-img"><a href="<?php echo $e->get_permalink() ?>"><?php echo $e->get_image( 'thumbnail', array(),
                                array(
                                    'aw' => true,
                                    'ah' => true,
                                ),
                                'normal'
                            ) ?></a></div>
                    <div class="info-content" data-type="link" data-url="<?php echo $e->get_permalink() ?>">
                        <div class="search-info educator-search-info"><a href="<?php echo $e->get_permalink() ?>"><span class="ev-tt"><?php echo $e->get_title() ?></span></a>
                            <?php if ( $location ): ?>
                            <div class="career" data-ride="ap-clamp" data-n="1"><strong><?php _e( 'Location:', 'apollo' ) ?></strong> <?php echo strip_tags( $location ) ?></div>
                            <?php endif; ?>
                            <?php if ( $cname ): ?>
                            <div class="career" data-n="2"><strong><?php _e( 'Contact Info:', 'apollo' ) ?></strong> <?php echo $cname ?></div>
                            <?php endif; ?>
                            <?php if ( $phone ): ?>
                            <div class="career" data-n="2"><strong><?php _e( 'Phone:', 'apollo' ) ?></strong> <?php echo $phone ?></div>
                            <?php endif; ?>
                            <?php if ( $email ): ?>
                            <div class="career" data-n="2"><strong><?php _e( 'Email:', 'apollo' ) ?></strong> <a href="mailto:<?php echo $email ?>"><?php echo $email ?></a></div>
                            <?php endif; ?>
                        </div>
                    </div>
                </li>
                <?php

            }
            else if($p->post_type === Apollo_DB_Schema::_ORGANIZATION_PT) {
                $e = get_org( $p );
                $summary100 = $e->get_summary(100);
                /** @Ticket #13137 */
                $image = $e->get_image_with_option_placeholder('thumbnail', array(),
                    array(
                        'aw' => true,
                        'ah' => true,
                    ),
                    'normal', $hasPlaceholderImg);

                ?>
                <li>
                    <div class="search-img <?php echo !$image ? 'no-place-holder-cate' : ''; ?>"><a href="<?php echo $e->get_permalink() ?>"><?php echo $image; ?></a></div>
                    <div class="info-content" data-type="link" data-url="<?php echo $e->get_permalink() ?>">
                        <div class="search-info"><a href="<?php echo $e->get_permalink() ?>"><span class="ev-tt"><?php echo $e->get_title() ?></span></a>
                            <div class="career"><?php
                                $strCat = str_replace('<a','<span', $e->generate_categories());
                                echo str_replace('</span>','</a>', $strCat); ?> </div>
                            <div class="s-desc"><?php echo $summary100['text']  ?> <?php echo $summary100['have_more'] ? '...' : '' ?></div>
                        </div>
                    </div>
                </li>
            <?php

            }
            else if($p->post_type === Apollo_DB_Schema::_VENUE_PT) {
                $e = get_venue( $p );
                $summary100 = $e->get_summary(100);
                /** @Ticket #13137 */
                $image = $e->get_image_with_option_placeholder('thumbnail', array(),
                    array(
                        'aw' => true,
                        'ah' => true,
                    ),
                    'normal', $hasPlaceholderImg);

                ?>
                <li>
                    <div class="search-img <?php echo !$image ? 'no-place-holder-cate' : ''; ?>"><a href="<?php echo $e->get_permalink() ?>"><?php echo $image; ?></a></div>
                    <div class="info-content" data-type="link" data-url="<?php echo $e->get_permalink() ?>">
                        <div class="search-info"><a href="<?php echo $e->get_permalink() ?>"><span class="ev-tt"><?php echo $e->get_title() ?></span></a>
                            <div class="career"><?php
                                $strCat = str_replace('<a','<span', $e->generate_categories());
                                echo str_replace('</span>','</a>', $strCat);
                                ?> </div>
                            <div class="s-desc"><?php echo $summary100['text']  ?> <?php echo $summary100['have_more'] ? '...' : '' ?></div>
                        </div>
                    </div>
                </li>
            <?php
            
            }
            else if($p->post_type === Apollo_DB_Schema::_CLASSIFIED) {
                $e = get_classified( $p );
                $summary100 = $e->get_summary(100);
                $image = $e->get_image('thumbnail', array(),
                    array(
                        'aw' => true,
                        'ah' => true,
                    ),
                    'normal', $hasPlaceholderImg);

                ?>
                <li>
                    <div class="search-img <?php echo !$image ? 'no-place-holder-cate' : ''; ?>"><a href="<?php echo $e->get_permalink() ?>"><?php echo $image; ?></a></div>
                    <div class="info-content" data-type="link" data-url="<?php echo $e->get_permalink() ?>">
                        <div class="search-info"><a href="<?php echo $e->get_permalink() ?>"><span class="ev-tt"><?php echo $e->get_title() ?></span></a>
                            
                            <?php $strCat = str_replace('<a','<span', $e->generate_categories());
                            if ( $strCat ):
                            ?>
                            <div class="career"><?php
                                echo str_replace('</span>','</a>', $strCat);
                                ?> </div>
                            <?php endif; ?>
                            
                            <div class="post-date">
                                <span data-ride="ap-clamp" data-n="1"><?php _e('Posted: ', 'apollo');echo date( 'M d, Y', strtotime($p->post_date) ) ?></span>
                            </div>
                            <div class="s-desc"><?php echo $summary100['text']  ?> <?php echo $summary100['have_more'] ? '...' : '' ?></div>
                        </div>
                    </div>
                </li>
            <?php
            }
            else if($p->post_type === Apollo_DB_Schema::_PUBLIC_ART_PT) {
                $e = get_public_art( $p );
                $summary100 = $e->get_summary(100);
                ?>
                <li>
                    <div class="search-img"><a href="<?php echo $e->get_permalink() ?>"><?php echo $e->get_image( 'thumbnail', array(),
                                array(
                                    'aw' => true,
                                    'ah' => true,
                                ),
                                'normal'
                            ) ?></a></div>
                    <div class="info-content public-art-list" data-type="link" data-url="<?php echo $e->get_permalink() ?>">
                        <div class="search-info"><a href="<?php echo $e->get_permalink() ?>"><span class="ev-tt"><?php echo $e->get_title() ?></span></a>
                            
                            <?php 
                            $catHtml = $e->generate_categories();
                            if ($catHtml):
                            ?>
                            <div class="career"  data-n="1"><?php _e('Category: '); echo $catHtml; ?> </div>
                            <?php endif; ?>

                            <?php 
                            $locHtml = $e->generate_locations();
                            if ($locHtml):
                            ?>
                            <div class="career"  data-n="1"><?php _e('Location: '); echo $locHtml; ?> </div>
                            <?php endif; ?>

                            <?php 
                            $colHtml = $e->generate_colections();
                            if ($colHtml):
                            ?>
                            <div class="career"  data-n="1"><?php _e('Collection: '); echo $colHtml; ?> </div>
                            <?php endif; ?>

                        </div>
                    </div>
                </li>
            <?php
            }
            else if($p->post_type === Apollo_DB_Schema::_BUSINESS_PT) {
                /* Thienld : $businessObj : business instance */
                $businessObj = get_business( $p );
                $orgID = get_apollo_meta($businessObj->id,Apollo_DB_Schema::_APL_BUSINESS_ORG,true);
                $orgObj = get_org($orgID);
                $bsIsRestaurant = get_apollo_meta($orgObj->id,Apollo_DB_Schema::_ORG_RESTAURANT,true) == "yes";
                $bsTypes = $businessObj->getDisplayBusinessType();
                $bsService = $bsIsRestaurant ? $businessObj->getDisplayBusinessService() : '';
                $summary100 = $businessObj->get_summary(100);
                /** @Ticket #13137 */
                $image = $orgObj->get_image_with_option_placeholder('thumbnail', array(),
                    array(
                        'aw' => true,
                        'ah' => true,
                    ),
                    'normal', $hasPlaceholderImg);

                ?>
                <li>
                    <div class="search-img <?php echo !$image ? 'no-place-holder-cate' : ''; ?>"><a href="<?php echo $businessObj->get_permalink(); ?>"><?php echo $image; ?></a></div>
                    <div class="info-content" data-type="link" data-url="<?php echo $businessObj->get_permalink() ?>">
                        <div class="search-info">
                            <a href="<?php echo $businessObj->get_permalink() ?>"><span class="ev-tt"><?php echo $businessObj->get_title() ?></span></a>
                            <?php if ($bsTypes): ?>
                                <div class="career" data-ride="ap-clamp" data-n="2"><?php _e('Category: '); echo $bsTypes; ?></div>
                            <?php endif; ?>
                            <?php if ($bsService): ?>
                                <div class="career" data-ride="ap-clamp" data-n="2"><?php _e('Service: '); echo $bsService; ?></div>
                            <?php endif; ?>
                            <div class="s-desc"><?php echo $summary100['text']  ?> <?php echo $summary100['have_more'] ? '...' : '' ?></div>
                        </div>
                    </div>
                </li>
                <?php
            }
            else {
                $e = get_event( $p );
                $filterTitle = false;
                $filterTitle = apply_filters('apl_event_listing_page_custom_title', $filterTitle);
                ?>
                <li>
                    <div class="search-img"><a href="<?php echo $e->get_permalink() ?>"><?php echo $e->get_image('thumbnail', array(),
                            array(
                                'aw' => true,
                                'ah' => true,
                            ),
                            'normal'
                            ) ?></a></div>

                    <div data-type="link" data-url="<?php echo $e->get_permalink() ?>" class="info-content">
                        <div class="search-info"><a href="<?php echo $e->get_permalink() ?>"><span class="ev-tt"><?php echo $e->get_title($filterTitle) ?></span></a>
                            <p class="meta auth"><?php echo $e->renderOrgVenueHtml() ?></p>
                        </div>

                        <div class="searchdate">
                    <span class="sch-date">
                        <?php $e->render_sch_date() ?>
                    </span>
                        </div>
                        <?php

                        $e_times =  $e->render_periods_time( $e->get_periods_time( " LIMIT ".Apollo_Display_Config::MAX_DATETIME." " ), '<div class="item">', '</div>' );
                        ?>
                        <div class="ico-date <?php echo ! $e_times ? 'hidden' : ''  ?>"><a><i class="fa fa-clock-o fa-2x"></i></a>
                            <div class="show-date">
                                <?php
                                echo $e_times;
                                if ( $e->have_more_periods_time() ) {
                                    echo '<div class="item">...</div>   ';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </li>

            <?php
            }
        ?>

        <div class="vertical-light"></div>
        <?php 
        endforeach;
    } else {
        _e( 'No results', 'apollo' );
    } 
