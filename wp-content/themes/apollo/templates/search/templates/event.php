<div class="breadcrumbs">
    <ul class="nav">
        <li><a href="<?php echo get_home_url(get_current_blog_id()) ?>"><?php _e('Home', 'apollo') ?></a></li>

        <?php if ( isset( $_GET['keyword'] ) || isset( $_GET['artist_style'] )
            || isset( $_GET['artist_medium'] ) || isset( $_GET['city'] ) || isset( $_GET['last_name'] ) ) { ?>
            <li> <a href="<?php echo home_url() ?>/event"><?php _e('Event', 'apollo') ?></a></li>
            <li> <span><?php _e('Search', 'apollo') ?></span></li>
        <?php } else { ?>
            <li> <span><?php _e('Event', 'apollo') ?></span></li>
        <?php } ?>
    </ul>
</div>

<?php
global $post;
$current_url = home_url() . $_SERVER["REQUEST_URI"];
//include search function here.
include APOLLO_INCLUDES_DIR. '/src/event/inc/class-event-search.php';
//end include search function here.
$search_obj = new Apollo_Event_Page(true);
$search_obj->set_main_post($post);
if (isset($_GET['by_my_location'])
    && (isset($_GET['view']) && $_GET['view'] == 'map')) {
    echo apply_filters('octave_render_map_view', $search_obj);
} else {
    $search_obj->search();

    ?>

    <?php if ( isset($_GET['keyword']) ): ?>
        <div class="search-tt mar-b-0 search-title-size">
            <?php
            // @ticket #11462, #11487: format start date & end date: Y-m-d -> M d, Y
            $search_obj->render_result_title(Apollo_DB_Schema::_EVENT_PT, 'M d, Y');
            ?>
        </div>
    <?php endif; ?>

    <?php
    if ( $search_obj->get_results() ):
        ?>

        <?php
        if ( $search_obj->have_pag()):
            ?>
            <div class="search-bkl">
                <?php
                $pag_html = '';
                ?>
                <div class="blk-paging">
                    <?php $pag_html = $search_obj->render_pagination();
                    echo $pag_html;
                    ?>
                </div>
                <nav class="pg">
                    <li class="txt"><span><?php _e( 'Show', 'apollo' ) ?>:</span></li>
                    <li>
                        <div class="pg-rad">
                            <input data-href="<?php echo $search_obj->get_display_type_url() ?>"
                                   type="radio" name="radioshowpg" value="pg" <?php echo ! $search_obj->is_onepage() ? 'checked="checked"' : '' ?> class="rad-pg"><span class="pg"></span><span><?php _e( 'Paginated', 'apollo' ) ?></span>
                        </div>
                    </li>

                    <li>
                        <div class="pg-rad">
                            <input data-href="<?php echo $search_obj->get_display_type_url( '1' ); ?>"
                                   type="radio" name="radioshowpg" value="op" <?php echo $search_obj->is_onepage() ? 'checked="checked"' : '' ?> class="rad-pg"><span class="pg"></span><span><?php _e( 'One page', 'apollo' ) ?> </span>
                        </div>
                    </li>
                </nav>
            </div>
        <?php endif; ?>
        <div class="search-bkl">
            <div class="b-share-cat art">
                <?php
                SocialFactory::social_btns( array(
                        'info' => $search_obj->getShareInfo(),
                        'id' => !empty($post) && is_object($post) ? $post->ID : 0,
                        'data_btns' => array( 'event-print'),
                        'event_print_url' => $search_obj->getPrintEventUrl()
                    )
                );
                ?>
            </div>
        </div>
        <?php
        /*@ticket #17347 */
        $discountDescription = of_get_option(Apollo_DB_Schema::_EVENT_ENABLE_DISCOUNT_DESCRIPTION, 0);
        $handleClass = "";
        if($discountDescription){
            $handleClass =   'apl-event-discount ' . $search_obj->getCurrentTemplateName();
        }

        ?>
        <div class="search-bkl <?php echo $handleClass?>">

            <?php if($discountDescription) {
                $discountImage = of_get_option(Apollo_DB_Schema::_EVENT_DISCOUNT_DESCRIPTION_IMAGE, '');
                $dicountText = of_get_option(Apollo_DB_Schema::_EVENT_DISCOUNT_DESCRIPTION_TEXT, '');
                ?>
                <div class="apl-event-discount-description">
                    <?php if($discountImage !== '')  { ?>
                        <div>
                            <image src="<?php echo $discountImage?>"/>
                        </div>
                    <?php }?>
                    <?php if($dicountText !== '') { ?>
                        <p><?php echo __($dicountText, 'apollo') ?></p>
                    <?php } ?>
                </div>
            <?php } ?>

            <nav class="type <?php echo (isset($_GET['by_my_location']) && $_GET['by_my_location']) ? 'mult-type' : ''?>">
                <ul>
                    <?php
                    $customFiltering = '';
                    $customFiltering = apply_filters('apl_custom_default_options_filtering', $customFiltering, $search_obj);
                    if (!empty($customFiltering)) :
                        echo $customFiltering;
                    else :
                    ?>
                    <li <?php echo !$search_obj->is_list_page(of_get_option(Apollo_DB_Schema::_EVENT_DEFAULT_VIEW_TYPE)) ? 'class="current"' : '' ?> ><a href="<?php echo $search_obj->get_template_btn_url() ?>"><i class="fa fa-th fa-2x"></i></a></li>
                    <li <?php echo $search_obj->is_list_page(of_get_option(Apollo_DB_Schema::_EVENT_DEFAULT_VIEW_TYPE)) ? 'class="current"' : '' ?>><a href="<?php echo $search_obj->get_template_btn_url( 'list' ) ?>"><i class="fa fa-bars fa-2x"></i></a></li>
                    <?php
                    endif;
                    $mapView = '';
                    echo apply_filters('apl_add_options_filtering', $mapView, $search_obj);
                    ?>
                </ul>
            </nav>
        </div>
    <?php endif; ?>

    <div class="search-bkl">
        <?php
        if ( $search_obj->is_list_page(of_get_option(Apollo_DB_Schema::_EVENT_DEFAULT_VIEW_TYPE)) ) {
            $container = $class = "searching-list";
        }
        else {
            $container = $class = 'search-list-thumb';
        }
        ?>
        <nav class="<?php echo $class; ?> ">
            <?php echo $search_obj->render_html(); ?>
        </nav>
    </div>

    <?php
    $is_view_more = $search_obj->have_more() && $search_obj->is_onepage();
    ?>
    <input name="search-have-more" value="<?php echo $search_obj->have_more() ?>" type="hidden" >

    <?php if ( ! $search_obj->is_map_page() ):
        $query_string = $search_obj->get_current_query_string();
        $query_string = add_query_arg(array(
            'page' => 2,
        ), $query_string);

        $query_string = ltrim($query_string, '?');
        ?>
        <div <?php if ( ! $is_view_more ) echo 'style="display: none"';  ?> id="load-more" class="load-more b-btn load-more-search load-more-artist-search-page">
            <a href="javascript:void(0);"
               data-ride="ap-more"
               data-holder=".<?php echo $container ?>>:last"
               data-sourcetype="url"
               data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_get_more_search_event&' . $query_string) ?>"
               data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
               data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
               class="btn-b arw"><?php _e('SHOW MORE', 'apollo') ?>
            </a>
        </div>
    <?php endif; ?>

    <div class="search-bkl">
        <?php if ( $search_obj->have_pag()):  ?>
            <div class="blk-paging">
                <?php echo $pag_html; ?>
            </div>
        <?php endif; ?>
    </div>
    <?php
}

