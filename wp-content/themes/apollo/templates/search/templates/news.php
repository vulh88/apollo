<?php
if (!Apollo_App::is_avaiable_module(Apollo_DB_Schema::_NEWS_PT)) {
    wp_safe_redirect('/');
}
$newsCustomLabel = Apollo_App::getCustomLabelByModuleName(Apollo_DB_Schema::_NEWS_PT); ?>
    <div class="breadcrumbs blog">
        <ul class="nav">
            <li><a href="/"><?php _e('Home') ?></a></li>
            <?php if (get_query_var('_apollo_news_search')) { ?>
                <li><span><?php echo $newsCustomLabel; ?></span></li>
            <?php } else { ?>
                <li>
                    <span><a href="<?php echo Apollo_App::getCustomUrlByModuleName(Apollo_DB_Schema::_NEWS_PT); ?>"><?php echo $newsCustomLabel; ?></a></span>
                </li>
                <li> <span><?php if (is_author()) {
                            echo get_the_author();
                        } else {
                            single_cat_title();
                        } ?></span></li>
            <?php } ?>

        </ul>
    </div>
<?php
if (have_posts()): the_post();
    include APOLLO_SHORTCODE_DIR . '/news/template.php';

endif;