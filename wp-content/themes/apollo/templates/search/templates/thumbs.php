<?php


    if ( $results = $this->get_results() ) {
        foreach ( $results as $p ): 

            if($p->post_type === Apollo_DB_Schema::_ARTIST_PT) {
                $e = get_artist( $p );
                
                $cat = $e->get_categories();
                
                $summary = $e->get_summary();
                $link = $e->get_permalink();
                ?>
                <li>
                    <div class="div-one" data-url="<?php echo $link ?>" data-type="link">
                        <div class="search-img"><a href="<?php echo $link ?>"><?php echo $e->get_image( 'medium', array(),
                                    array(
                                        'aw' => true,
                                        'ah' => true,
                                    ),
                                    'normal'
                                ) ?></a></div>
                        <div class="search-info"><a href="<?php echo $link ?>"><span class="ev-tt" data-ride="ap-clamp" data-n="2"><?php echo $e->get_title() ?></span></a>
                            <div class="career" data-ride="ap-clamp" data-n="2"><?php
                                echo $e->generate_artist_categories();
                                ?></div>
                            <div class="s-desc" data-ride="ap-clamp" data-n="9"><?php echo $summary['text']  ?></div>
                        </div>
                    </div>
                </li>
                <?php
            }
            else if($p->post_type === Apollo_DB_Schema::_PROGRAM_PT) {
                $e = get_program( $p );
                
                $cat = $e->get_categories();
                
                $summary = $e->get_content( 100 );
               
                $cname = $e->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_CNAME, Apollo_DB_Schema::_APL_PROGRAM_DATA );
                $phone = $e->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_PHONE, Apollo_DB_Schema::_APL_PROGRAM_DATA );
                $email = $e->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_EMAIL, Apollo_DB_Schema::_APL_PROGRAM_DATA );
                $location = $e->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_LOCATION, Apollo_DB_Schema::_APL_PROGRAM_DATA );
                ?>
                <li>
                    <div class="div-one" data-type="link" data-url="<?php echo $e->get_permalink() ?>">
                        <div class="search-img"><a data-ride="" href="<?php echo $e->get_permalink() ?>"><?php echo $e->get_image( 'medium', array(), array(
                                        'aw' => true,
                                        'ah' => true,
                                    ), 'normal' ) ?></a></div>
                        <div class="educator-search-info search-info"><a href="<?php echo $e->get_permalink() ?>"><span class="ev-tt" data-ride="ap-clamp" data-n="2"><?php echo $e->get_title() ?></span></a>
                            <div class="meta auth">
                                <?php if ( $location ): ?>
                                <div class="career" data-ride="ap-clamp" data-n="2"><strong><?php _e( 'Location:', 'apollo' ) ?></strong> <?php echo strip_tags( $location ) ?></div>
                                <?php endif; ?>
                                <?php if ( $cname ): ?>
                                <div class="career" data-n="2"><strong><?php _e( 'Contact Info:', 'apollo' ) ?></strong> <?php echo $cname ?></div>
                                <?php endif; ?>
                                <?php if ( $phone ): ?>
                                <div class="career" data-n="2"><strong><?php _e( 'Phone:', 'apollo' ) ?></strong> <?php echo $phone ?></div>
                                <?php endif; ?>
                                <?php if ( $email ): ?>
                                <div class="career" data-n="2"><strong><?php _e( 'Email:', 'apollo' ) ?></strong> <a href="mailto:<?php echo $email ?>"><?php echo $email ?></a></div>
                                <?php endif; ?>
                                <div class="s-desc" data-ride="ap-clamp" data-n="6"><?php echo $summary['text']  ?></div>
                            </div>

                        </div>
                    </div>
                </li>
                <?php
            }
            else if($p->post_type === Apollo_DB_Schema::_ORGANIZATION_PT) {
                $e = get_org( $p );
                $cat = $e->get_categories();
                $summary = $e->get_summary();
                ?>
                <li>
                    <div class="div-one" data-url="<?php echo $e->get_permalink() ?>" data-type="link">
                        <div class="search-img"><a href="<?php echo $e->get_permalink() ?>"><?php echo $e->get_image( 'medium', array(),
                                    array(
                                        'aw' => true,
                                        'ah' => true,
                                    ),
                                    'normal'
                                ) ?></a></div>
                        <div class="search-info"><a href="<?php echo $e->get_permalink() ?>"><span class="ev-tt" data-ride="ap-clamp" data-n="2"><?php echo $e->get_title() ?></span></a>
                            <div class="career" data-ride="ap-clamp" data-n="2"><?php
                                echo $e->generate_categories();
                                ?></div>
                            <div class="s-desc" data-ride="ap-clamp" data-n="9"><?php echo $summary['text']  ?></div>
                        </div>
                    </div>
                </li>
            <?php
            }
            else if($p->post_type === Apollo_DB_Schema::_VENUE_PT) {
                $e = get_venue( $p );
                $cat = $e->get_categories();
                $summary = $e->get_summary();
                ?>
                <li>
                    <div class="div-one" data-url="<?php echo $e->get_permalink() ?>" data-type="link">
                        <div class="search-img"><a href="<?php echo $e->get_permalink() ?>"><?php echo $e->get_image( 'medium', array(),
                                    array(
                                        'aw' => true,
                                        'ah' => true,
                                    ),
                                    'normal'
                                ) ?></a></div>
                        <div class="search-info"><a href="<?php echo $e->get_permalink() ?>"><span class="ev-tt" data-ride="ap-clamp" data-n="2"><?php echo $e->get_title() ?></span></a>
                            <div class="career" ><?php
                                echo $e->generate_categories(); ?></div>
                            <div class="s-desc" data-ride="ap-clamp" data-n="9"><?php echo $summary['text']  ?></div>
                        </div>
                    </div>
                </li>
            <?php
            }
            else if($p->post_type === Apollo_DB_Schema::_PUBLIC_ART_PT) {
                $e = get_public_art( $p );
                $cat = $e->get_categories();
                $summary = $e->get_summary();
                ?>
                <li>
                    <div class="div-one public-art-thumb" data-url="<?php echo $e->get_permalink() ?>" data-type="link">
                        <div class="search-img"><a href="<?php echo $e->get_permalink() ?>"><?php echo $e->get_image( 'medium', array(),
                                    array(
                                        'aw' => true,
                                        'ah' => true,
                                    ),
                                    'normal'
                                ) ?></a></div>
                        <div class="search-info"><a href="<?php echo $e->get_permalink() ?>"><span class="ev-tt" data-ride="ap-clamp" data-n="2"><?php echo $e->get_title() ?></span></a>
                            
                            <?php 
                            $catHtml = $e->generate_categories();
                            if ($catHtml):
                            ?>
                                <div class="career" ><?php _e('Category: '); echo $catHtml; ?></div>
                            <?php endif; ?>
                           
                            <div class="s-desc" data-ride="ap-clamp" data-n="9"><?php echo $summary['text']  ?></div>
                        </div>
                    </div>
                </li>
            <?php
            }
            else if($p->post_type === Apollo_DB_Schema::_CLASSIFIED) {
                $e = get_classified( $p );
                $cat = $e->get_categories();
                $summary = $e->get_summary();
                ?>
                <li>
                    <div class="div-one" data-url="<?php echo $e->get_permalink() ?>" data-type="link">
                        <div class="search-img"><a href="<?php echo $e->get_permalink() ?>"><?php echo $e->get_image( 'medium', array(),
                                    array(
                                        'aw' => true,
                                        'ah' => true,
                                    ),
                                    'normal'
                                ) ?></a></div>
                        <div class="search-info"><a href="<?php echo $e->get_permalink() ?>"><span class="ev-tt" data-ride="ap-clamp" data-n="2"><?php echo $e->get_title() ?></span></a>
                            
                            <?php 
                            $catHtml = $e->generate_categories();
                            if ($catHtml):
                            ?>
                                <div class="career" ><?php echo $catHtml; ?></div>
                            <?php endif; ?>
                                
                            <div class="post-date">
                                <span data-ride="ap-clamp" data-n="1"><?php _e('Posted: ', 'apollo');echo date( 'M d, Y', strtotime($p->post_date) ) ?></span>
                            </div>
                            <div class="s-desc" data-ride="ap-clamp" data-n="9"><?php echo $summary['text']  ?></div>
                        </div>
                    </div>
                </li>
            <?php
            }
            else if($p->post_type === Apollo_DB_Schema::_BUSINESS_PT) {
                /* Thienld : $businessObj : business instance */
                $businessObj = get_business( $p );
                $orgID = get_apollo_meta($businessObj->id,Apollo_DB_Schema::_APL_BUSINESS_ORG,true);
                $orgObj = get_org($orgID);
//                aplDebug($orgObj->get_image( 'medium', array(),
//                    array(
//                        'aw' => true,
//                        'ah' => true,
//                    ),
//                    'normal'
//                ));
                $bsIsRestaurant = get_apollo_meta($orgObj->id,Apollo_DB_Schema::_ORG_RESTAURANT,true) == "yes";
                $bsTypes = $businessObj->getDisplayBusinessType();
                $bsService = $bsIsRestaurant ? $businessObj->getDisplayBusinessService() : '';
                $summary100 = $businessObj->get_summary(100);
                ?>
                <li>
                    <div class="div-one" data-url="<?php echo $businessObj->get_permalink() ?>" data-type="link">
                        <div class="search-img"><a href="<?php echo $businessObj->get_permalink() ?>"><?php echo $orgObj->get_image( 'medium', array(),
                                    array(
                                        'aw' => true,
                                        'ah' => true,
                                    ),
                                    'normal'
                                ) ?></a></div>
                        <div class="search-info"><a href="<?php echo $businessObj->get_permalink() ?>"><span class="ev-tt" data-ride="ap-clamp" data-n="2"><?php echo $businessObj->get_title() ?></span></a>
                         <?php if ($bsTypes): ?>
                            <div class="career"><?php _e('Category: '); echo $bsTypes; ?></div>
                         <?php endif; ?>
                         <?php if ($bsService): ?>
                             <div class="career"><?php _e('Service: '); echo $bsService; ?></div>
                         <?php endif; ?>
                         <div class="s-desc" data-ride="ap-clamp" data-n="9"><?php echo $summary100['text']  ?></div>
                        </div>
                    </div>
                </li>
                <?php
            }
            else {
                $e = get_event( $p );
                $filterTitle = false;
                $filterTitle = apply_filters('apl_event_listing_page_custom_title', $filterTitle);
                $_arr_show_date = $e->getShowTypeDateTime();
                $e_times =  $e->render_periods_time( $e->get_periods_time( " LIMIT ".Apollo_Display_Config::MAX_DATETIME_SEARCH." " ), '<div class="item">', '</div>' );

                ?>

                <li>
                    <div class="div-one" data-type="link" data-url="<?php echo $e->get_permalink() ?>">
                        <div class="search-img"><a href="<?php echo $e->get_permalink() ?>"><?php echo $e->get_image( 'medium', array(),
                                    array(
                                        'aw' => true,
                                        'ah' => true,
                                    ),
                                    'normal'
                                ) ?></a></div>
                        <div class="search-info">
                            <a href="<?php echo $e->get_permalink() ?>">
                                <span class="ev-tt" <?php echo !$filterTitle ? ' data-ride="ap-clamp" data-n="2" ' : '' ?> ><?php echo $e->get_title($filterTitle) ?></span>
                            </a>
                            <?php if($e->get_type() === 'event'): ?>
                                <p class="meta auth"><?php echo $e->renderOrgVenueHtml() ?></p>
                                <span class="sch-date"><?php $e->render_sch_date() ?></span>
                            <?php elseif($e->get_type() === 'post'):
                                $scategory = get_the_category_list(", ", '', $e->id);

                                $nicename = get_the_author_meta('user_nicename', $e->post->post_author);
                                $created_date = get_the_date('M d, Y',$e->id);
                                ?>
                                <div class="meta auth">
                                    <div>
                                        <span><?php _e('Category:', 'apollo') ?></span>  <span><?php echo $scategory ?></span>
                                    </div>
                                    <div>
                                        <span><?php _e('Author:', 'apollo') ?></span>  <span><?php echo $nicename ?></span>
                                    </div>
                                    <div>
                                        <span><?php _e('Created:', 'apollo') ?></span>  <span><?php echo $created_date ?></span>
                                    </div>
                                </div>
                                <span class="sch-date"><?php $e->render_sch_date() ?></span>
                            <?php endif; ?>
                        </div>
                    </div>

                    <?php if ( $e_times ): ?>
                        <div class="ico-date"><a><i class="fa fa-clock-o fa-2x"></i></a></div>
                        <div class="div-two">
                            <div class="show-events">
                                <?php
                                echo $e_times;
                                if ( $e->have_more_periods_time() ) {
                                    echo '<div class="item">...</div>   ';
                                }
                                ?>
                            </div>
                        </div>
                    <?php endif; ?>

                </li>
                <?php
            }
                ?>
                                
<?php endforeach;

} else {
    _e( 'No results', 'apollo' );
} ?>        