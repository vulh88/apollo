<div class="breadcrumbs">
    <ul class="nav">
        <li><a href="<?php echo get_home_url(get_current_blog_id()) ?>"><?php _e('Home', 'apollo') ?></a></li>

        <?php if ( isset( $_GET['keyword'] ) || isset( $_GET['city'] )
            || isset( $_GET['state'] ) || isset( $_GET['zip'] )) { ?>
            <li> <a href="<?php echo home_url() ?>/public-art"><?php _e('Public Art', 'apollo') ?></a></li>
            <li> <span><?php _e('Search', 'apollo') ?></span></li>
        <?php } else {
            /*@ticket #18344: 0002504: Arts Education Customizations - Add category type to breadcrumb (program)*/
            if(!empty($currentTerm)) : ?>
                <li> <a href="<?php echo home_url('public-art') ?>"><?php _e('Public Art', 'apollo') ?></a></li>
                <li><span><?php echo $currentTerm->name; ?></span></li>
            <?php  else:?>
                <li> <span><?php _e('Public Art', 'apollo') ?></span></li>
            <?php endif;
        } ?>
    </ul>
</div>

<?php
if(!Apollo_App::is_avaiable_module( Apollo_DB_Schema::_PUBLIC_ART_PT )) {
    wp_safe_redirect( '/' );
}

global $post;
$current_url = home_url() . $_SERVER["REQUEST_URI"];
include APOLLO_SRC_DIR. '/public-art/inc/class-public-art-search.php';
$search_obj = new Apollo_Public_Art_Page();
$search_obj->set_main_post($post);
$search_obj->search();
?>

<?php if ( isset($_GET['keyword']) ): ?>
    <div class="search-tt mar-b-0">
        <?php $search_obj->render_result_title() ?>
    </div>
<?php endif; ?>

<?php
if ( $search_obj->get_results() ):
    ?>

    <?php
    if ( $search_obj->have_pag()):
        ?>
        <div class="search-bkl">
            <?php
            $pag_html = '';
            ?>
            <div class="blk-paging">
                <?php $pag_html = $search_obj->render_pagination();
                echo $pag_html;
                ?>
            </div>
            <nav class="pg">
                <li class="txt"><span><?php _e( 'Show', 'apollo' ) ?>:</span></li>
                <li>
                    <div class="pg-rad">
                        <input data-href="<?php echo $search_obj->get_display_type_url() ?>"
                               type="radio" name="radioshowpg" value="pg" <?php echo ! $search_obj->is_onepage() ? 'checked="checked"' : '' ?> class="rad-pg"><span class="pg"></span><span><?php _e( 'Paginated', 'apollo' ) ?></span>
                    </div>
                </li>

                <li>
                    <div class="pg-rad">
                        <input data-href="<?php echo $search_obj->get_display_type_url( '1' ); ?>"
                               type="radio" name="radioshowpg" value="op" <?php echo $search_obj->is_onepage() ? 'checked="checked"' : '' ?> class="rad-pg"><span class="pg"></span><span><?php _e( 'One page', 'apollo' ) ?> </span>
                    </div>
                </li>
            </nav>
        </div>
    <?php endif; ?>
    <div class="search-bkl">
        <div class="b-share-cat art">
            <?php
            SocialFactory::social_btns( array(
                    'info' => $search_obj->getShareInfo(),
                    'id' => !empty($post) && is_object($post) ? $post->ID : 0,
                )
            );
            ?>
        </div>
    </div>
    <?php
    $page = get_post(Apollo_App::getPublicArtHeaderPage());
    if( !empty($page)){ ?>
        <div class="sub-content-blk custom-block"">
            <?php
            echo Apollo_App::the_content($page->post_content);
            ?>
        </div>
    <?php }
    $displayPublic = !empty($currentTerm) ? get_apollo_term_meta($currentTerm->term_id, Apollo_DB_Schema::_APL_TAXONOMY_DISPLAY_PUBLIC_FIELD, true) : false;

    if (isset($termDescription) && $termDescription && !empty($displayPublic)) {
        if ($displayPublic) { ?>
            <div class="sub-content-blk custom-block padding-10px">
                <?php echo Apollo_App::the_content($termDescription); ?>
            </div>
        <?php }
    } ?>
    <div class="search-bkl">
        <nav class="type">
            <ul>
                <li <?php echo !$search_obj->is_list_page(of_get_option(Apollo_DB_Schema::_PUBLIC_ART_DEFAULT_VIEW_TYPE)) ? 'class="current"' : '' ?> ><a href="<?php echo $search_obj->get_template_btn_url() ?>"><i class="fa fa-th fa-2x"></i></a></li>
                <li <?php echo $search_obj->is_list_page(of_get_option(Apollo_DB_Schema::_PUBLIC_ART_DEFAULT_VIEW_TYPE)) ? 'class="current"' : '' ?>><a href="<?php echo $search_obj->get_template_btn_url( 'list' ) ?>"><i class="fa fa-bars fa-2x"></i></a></li>
            </ul>
        </nav>
    </div>
<?php endif; ?>

<div class="search-bkl">
    <?php
    if ( $search_obj->is_list_page(of_get_option(Apollo_DB_Schema::_PUBLIC_ART_DEFAULT_VIEW_TYPE)) ) {
        $container = $class = "search-artist-list";
    }
    else {
        $container = $class = 'search-artist-thumb';
    }
    ?>
    <nav class="<?php echo $class; ?>">
        <?php echo $search_obj->render_html(); ?>
    </nav>
</div>

<?php
$is_view_more = $search_obj->have_more() && $search_obj->is_onepage();
?>
<input name="search-have-more" value="<?php echo $search_obj->have_more() ?>" type="hidden" >

<?php if ( ! $search_obj->is_map_page() ):
    $query_string = $search_obj->get_current_query_string();
    $query_string = add_query_arg(array(
        'page' => 2,
    ), $query_string);

    $query_string = ltrim($query_string, '?');
    ?>
    <div <?php if ( ! $is_view_more ) echo 'style="display: none"';  ?> id="load-more" class="load-more b-btn load-more-search load-more-artist-search-page">
        <a href="javascript:void(0);"
           data-ride="ap-more"
           data-holder=".<?php echo $container ?>>:last"
           data-sourcetype="url"
           data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_get_more_search_public_art&' . $query_string) ?>"
           data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
           data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
           class="btn-b arw"><?php _e('SHOW MORE', 'apollo') ?>
        </a>
    </div>
<?php endif; ?>

<div class="search-bkl">
    <?php if ( $search_obj->have_pag()):  ?>
        <div class="blk-paging">
            <?php echo $pag_html; ?>
        </div>
    <?php endif; ?>
</div>