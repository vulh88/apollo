<div class="breadcrumbs custom-breadcrumbs">
    <ul class="nav">
        <li><a href="<?php echo get_home_url(get_current_blog_id()) ?>"><?php _e('Home', 'apollo') ?></a></li>

<?php if ( isset( $_GET['keyword'] ) || isset( $_GET['city'] )
    || isset( $_GET['state'] ) || isset( $_GET['zip'] )) { ?>
    <li> <a href="<?php echo home_url() ?>/business"><?php _e('Business', 'apollo') ?></a></li>
    <li> <span><?php _e('Search', 'apollo') ?></span></li>
<?php } else { ?>
    <li>
        <?php
        /*@ticket #18250 [CF] 20181308 - [0002470 Business] - Modify the breadcrumb*/
        $breadcrumbName = (is_tax() && !empty($currentTerm)) ? $currentTerm->name :  __('Business', 'apollo');
        ?>
        <span><?php echo $breadcrumbName ?></span>
    </li>
<?php } ?>
</ul>
</div>

<?php
if(!Apollo_App::is_avaiable_module( Apollo_DB_Schema::_BUSINESS_PT )) {
    wp_safe_redirect( '/' );
}

global $post;
$current_url = home_url() . $_SERVER["REQUEST_URI"];
include APOLLO_SRC_DIR. '/business/inc/class-business-search.php';
$search_obj = new Apollo_Business_Page();
$search_obj->set_main_post($post);
$search_obj->search();
?>

<?php if ( isset($_GET['keyword']) ): ?>
    <div class="search-tt mar-b-0">
        <?php $search_obj->render_result_title() ?>
    </div>
<?php endif; ?>

<?php
if ( $search_obj->get_results() ):
    ?>

    <?php
    if ( $search_obj->have_pag()):
        ?>
        <div class="search-bkl">
            <?php
            $pag_html = '';
            ?>
            <div class="blk-paging">
                <?php $pag_html = $search_obj->render_pagination();
                echo $pag_html;
                ?>
            </div>
            <nav class="pg">
                <li class="txt"><span><?php _e( 'Show', 'apollo' ) ?>:</span></li>
                <li>
                    <div class="pg-rad">
                        <input data-href="<?php echo $search_obj->get_display_type_url() ?>"
                               type="radio" name="radioshowpg" value="pg" <?php echo ! $search_obj->is_onepage() ? 'checked="checked"' : '' ?> class="rad-pg"><span class="pg"></span><span><?php _e( 'Paginated', 'apollo' ) ?></span>
                    </div>
                </li>

                <li>
                    <div class="pg-rad">
                        <input data-href="<?php echo $search_obj->get_display_type_url( '1' ); ?>"
                               type="radio" name="radioshowpg" value="op" <?php echo $search_obj->is_onepage() ? 'checked="checked"' : '' ?> class="rad-pg"><span class="pg"></span><span><?php _e( 'One page', 'apollo' ) ?> </span>
                    </div>
                </li>
            </nav>
        </div>
    <?php endif; ?>
    <div class="search-bkl">
        <div class="b-share-cat art">
            <?php
            SocialFactory::social_btns( array(
                    'info' => $search_obj->getShareInfo(),
                    'id' => !empty($post) && is_object($post) ? $post->ID : 0,
                )
            );
            ?>
        </div>
    </div>


    <?php
        if (isset($currentTerm) && $currentTerm->name && of_get_option(APL_Business_Module_Theme_Option::_ENABLE_BUSINESS_TAXONOMY_TITLE, 0)):
    ?>
        <h3 class="ttl-bs-list"><?php echo sprintf(__('%s', 'apollo'), $currentTerm->name) ?></h3>
    <?php endif; ?>


    <?php $displayPublic = isset($currentTerm) ? get_apollo_term_meta($currentTerm->term_id, Apollo_DB_Schema::_APL_TAXONOMY_DISPLAY_PUBLIC_FIELD, true) : '';

    if (!empty($displayPublic)) {
        if ($displayPublic) { ?>
            <div class="sub-content-blk custom-block padding-10px">
                <?php echo Apollo_App::the_content($termDescription); ?>
            </div>
        <?php }
    } ?>

    <?php
        // Default view type menu
        require_once $search_obj->basePathTemplate. '_menu.php';
    ?>

<?php endif; ?>

<?php
    if ($search_obj->viewType == 'list2') {
        $container = 'business-search-list-2';
        $class = 'search-artist-list business-search-list-2 clearfix';
        $wrapperClass = 'business-search-blk-2';
    }
    else {
        $container = $class = $search_obj->is_list_page(of_get_option(APL_Business_Module_Theme_Option::_BUSINESS_DEFAULT_VIEW_TYPE)) ?
            "search-artist-list" : 'search-artist-thumb';
        $wrapperClass = '';
    }
?>

<div class="search-bkl <?php echo $wrapperClass; ?>">
    <nav class="<?php echo $class; ?>">
        <?php echo $search_obj->render_html(); ?>
    </nav>
</div>

<?php
$is_view_more = $search_obj->have_more() && $search_obj->is_onepage();
?>
<input name="search-have-more" value="<?php echo $search_obj->have_more() ?>" type="hidden" >

<?php if ( ! $search_obj->is_map_page() ):
    $query_string = $search_obj->get_current_query_string();
    $query_string = add_query_arg(array(
        'page' => 2,
    ), $query_string);

    if (isset($currentTerm) && $currentTerm->term_id) {
        $query_string = add_query_arg(array(
            'term' => $currentTerm->term_id,
        ), $query_string);
    }

    $query_string = ltrim($query_string, '?');
    ?>
    <div <?php if ( ! $is_view_more ) echo 'style="display: none"';  ?> id="load-more" class="load-more b-btn load-more-search load-more-artist-search-page">
        <a href="javascript:void(0);"
           data-ride="ap-more"
           data-holder=".<?php echo $container ?>>:last"
           data-sourcetype="url"
           data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_get_more_search_business&' . $query_string) ?>"
           data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
           data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
           class="btn-b arw"><?php _e('SHOW MORE', 'apollo') ?>
        </a>
    </div>
<?php endif; ?>

<div class="search-bkl">
    <?php if ( $search_obj->have_pag()):  ?>
        <div class="blk-paging">
            <?php echo $pag_html; ?>
        </div>
    <?php endif; ?>
</div>