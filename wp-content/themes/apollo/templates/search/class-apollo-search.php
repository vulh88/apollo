<?php
require_once APOLLO_TEMPLATES_DIR. '/pages/lib/class-apollo-page-module.php';

class Apollo_Search extends Apollo_Page_Module {

    public function __construct( $page = 1 ) {

        $this->page     = $page;

        $selectedSearchMods = of_get_option( APL_Theme_Option_Site_Config_SubTab::GLOBAL_SEARCH_TABS, false, true );
        $namedMods = of_get_option( APL_Theme_Option_Site_Config_SubTab::GLOBAL_SEARCH_TABS. '_of_asname', false, true );
        if (!empty($selectedSearchMods) && !empty($namedMods)) {
            $selectedSearchMods = array_filter($selectedSearchMods, 'strlen');
            $namedMods = array_filter($namedMods, 'strlen');
            $this->tabsearchs = array_combine($selectedSearchMods, $namedMods);
        }
        else {
            $this->tabsearchs = Apollo_App::modulesData();
        }


        // Set post type
        $this->type = $this->get_first_tab();

        if ( ! $this->type ) Apollo_App::safeRedirect ( '/' );

        parent::__construct();
    }

    private function get_first_tab() {

        if ( isset( $_REQUEST['type'] ) ) {
            return Apollo_App::clean_data_request( $_REQUEST['type'] );
        }

        $ac_mods = Apollo_App::get_avaiable_modules();
        $tab_mods = array_keys( $this->tabsearchs );

        foreach( $tab_mods as $v ):
            $v_compare = $v == Apollo_DB_Schema::_PROGRAM_PT ? Apollo_DB_Schema::_EDUCATION : $v;
            if ( ($ac_mods && in_array( $v_compare , $ac_mods )) || $v_compare === 'page') return $v;
        endforeach;

        return false;
    }


    public function search( $k = '' ) {
        global $wpdb;

        if ( $page = get_query_var( 'paged' ) ) {
            $this->page = $page;
        }

        $post_type = $this->type;

        if ( ( ! $k ) ) {
            $k = get_query_var( 's' );
        }

        $searchObj = self::getSearchInstance($post_type, false);
        $searchObj->setKeyword($k);
        $searchObj->search();
        $this->result = $searchObj->result;
        $this->setTotal($searchObj->getTotal());
        $this->setTotalPages($searchObj->total_pages);
        $this->arrObjTotal[$this->type] = array('total'  => $this->total);

        $this->template = APOLLO_TEMPLATES_DIR. '/search/templates/'. $this->_get_template_name();
    }

    private function get_available_mods_search() {
        $ac_mods = Apollo_App::get_avaiable_modules();

        if ( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EDUCATION ) ) {
            $ac_mods[] = Apollo_DB_Schema::_PROGRAM_PT;
            $ac_mods = array_values( array_diff( $ac_mods , array( Apollo_DB_Schema::_EDUCATION ) ) );
        }

        return $ac_mods ? $ac_mods : array();
    }

    public function getSearchInstance($module = '', $isCounting = false)
    {
        $objSearch = null;

        $specialModules = array(
            Apollo_DB_Schema::_EDUCATOR_PT,
            Apollo_DB_Schema::_PROGRAM_PT,
        );

        $moduleName = $module;
        if (in_array($module, $specialModules)) {
            $moduleName = Apollo_DB_Schema::_EDUCATION;
        }

        if (!Apollo_App::is_avaiable_module($moduleName) && $module != 'page') {
            return $objSearch;
        }

        switch($module)
        {
            case Apollo_DB_Schema::_ARTIST_PT:
                require APOLLO_INCLUDES_DIR. '/src/artist/inc/class-artist-search.php';
                $objSearch = new Apollo_Artist_Page($isCounting);
                break;

            case Apollo_DB_Schema::_BLOG_POST_PT:
                require APOLLO_INCLUDES_DIR. '/src/blog/inc/class-blog-search.php';
                $objSearch = new Apollo_Blog_Page($isCounting);
                break;

            case Apollo_DB_Schema::_BUSINESS_PT:
                require APOLLO_INCLUDES_DIR. '/src/business/inc/class-business-search.php';
                $objSearch = new Apollo_Business_Page($isCounting);
                break;

            case Apollo_DB_Schema::_CLASSIFIED:
                require APOLLO_INCLUDES_DIR. '/src/classified/inc/class-classified-search.php';
                $objSearch = new Apollo_Classified_Page($isCounting);
                break;

            case Apollo_DB_Schema::_EDUCATOR_PT:
                require APOLLO_INCLUDES_DIR. '/src/educator/inc/class-educator-search.php';
                $objSearch = new Apollo_Educator_Page($isCounting);
                break;

            case Apollo_DB_Schema::_EVENT_PT:
                require APOLLO_INCLUDES_DIR. '/src/event/inc/class-event-search.php';
                $objSearch = new Apollo_Event_Page(false, $isCounting);
                break;

            case Apollo_DB_Schema::_PROGRAM_PT:
                require APOLLO_INCLUDES_DIR. '/src/program/inc/class-program-search.php';
                $objSearch = new Apollo_Program_Page($isCounting);
                break;

            case Apollo_DB_Schema::_ORGANIZATION_PT:
                require APOLLO_INCLUDES_DIR. '/src/org/inc/class-org-search.php';
                $objSearch = new Apollo_Org_Page($isCounting);
                break;

            case Apollo_DB_Schema::_PUBLIC_ART_PT:
                require APOLLO_INCLUDES_DIR. '/src/public-art/inc/class-public-art-search.php';
                $objSearch = new Apollo_Public_Art_Page($isCounting);
                break;

            case Apollo_DB_Schema::_VENUE_PT:
                require APOLLO_INCLUDES_DIR. '/src/venue/inc/class-venue-search.php';
                $objSearch = new Apollo_Venue_Page($isCounting);
                break;
            case 'page':
                /*@ticket #17885: 0002466: Promo video homepage - Modify the "Content" field of the widget to include the editing tool bar and access to Media Library*/
                require_once APOLLO_INCLUDES_DIR. '/src/blog/inc/class-blog-search.php';
                $objSearch = new Apollo_Blog_Page($isCounting, 'page');
                break;
            case 'news':
                /*@ticket #18127: 0002503: News Module*/
                require_once APOLLO_INCLUDES_DIR. '/src/blog/inc/class-blog-search.php';
                $objSearch = new Apollo_Blog_Page($isCounting, 'news');
                break;
        }

        return $objSearch;
    }

    public function _get_total_item_all_post_type($k = '', $currentPostType = '')
    {
        foreach ($this->tabsearchs as $module => $name) {
            // not count again when current tab (current module) is selected
            if (empty($currentPostType) || $currentPostType == $module) {
                continue;
            }

            $objSearch = $this->getSearchInstance($module, true);
            if ($objSearch && !isset($this->arrObjTotal[$module])) {
                $objSearch->setKeyword($k);
                $objSearch->search();
                $this->arrObjTotal[$module]['total']     = $objSearch->getTotal();
                $this->arrObjTotal[$module]['post_type'] = $module;
            }
            unset($objSearch);
        }

        return $this->arrObjTotal;

    }

    /**
     * Render tabs search
     */
    public function render_tabs() {

        $ac_mods = $this->get_available_mods_search();

        if ( ! $ac_mods ) $ac_mods = array();

        $url = $this->get_tab_url();
        $i=0;
        foreach ( $this->tabsearchs as $k => $v ):

            if ( ! in_array( $k , $ac_mods ) && $k !== 'page' ) continue;

            $total = isset( $this->arrObjTotal[$k] ) ? $this->arrObjTotal[$k]['total'] : 0;
            $selected = $k == $this->type ? 'selected' : '';

            if ($k == $this->type && $total > 0) {
                $totalStr = ' <span>('.$total.')</span>';
            }
            else {
                $totalStr = '';
            }

            $tabId = 'global-search-tab-'. $k;

            /*@ticket #17123 */
            if ( $k === Apollo_DB_Schema::_ARTIST_PT){
                $v = Apollo_App::getCustomLabelByModuleName(Apollo_DB_Schema::_ARTIST_PT);
            }
            echo '<li class="'.$selected.'" id="'.$tabId.'" data-post-type="' . $k .'"><a data-href="'.$url.'&type='.$k.'" href="javascript:void(0);" >'.$v.$totalStr.'</a></li>';
            $i++;
        endforeach;
    }

    public function get_common_url() {
        $k          = get_query_var('s');
        return home_url() . '/?s='. $k;
    }

    public function get_tab_url() {
        $template = isset( $_REQUEST['view'] ) ? $_REQUEST['view'] : '';
        return $this->get_common_url().'&view='. $template.'&onepage='.$this->is_onepage();
    }

    public function render_result_title($postType = '', $dateFormat = 'Y-m-d', $isEcho = true) {
        $type = isset( $this->tabsearchs[$this->type] ) ? $this->tabsearchs[$this->type] : '';
        echo sprintf( '<p>%s "<a> %s </a>" %s %s</p>', __( "Search", "apollo" ), get_query_var( "s" ), __( 'in', 'apollo' ), $type );
    }

}
