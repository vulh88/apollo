<?php
    $largeArticles = array(of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_LARGE_LEFT,0),of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_LARGE_RIGHT,0));
    $maxWords = intval(of_get_option(Apollo_DB_Schema::_BLOG_NUM_OF_HEADLINE,75));
    $length = 135;
?>

<?php if (!empty($largeArticles)): ?>

<div class="v-line"></div>
<div class="wc-l">
    <section class="list-blog">
        <?php

            foreach ($largeArticles as $article) {
                if (isset($article) && $article) {
                    $blogPost = get_apl_blog($article);

                    $summary = $blogPost->get_summary_blog($length, true);
                    $content = strip_tags($summary['text']);
                    $author_id = get_post($article)->post_author;
                    $url = wp_get_attachment_url(get_post_thumbnail_id($article));
                    $title = get_the_title($article);
                    $title = $blogPost->getStringByLength($title,$maxWords,true);
                    $title = strip_tags($title['text']);

                    ?>
                    <article class="blog-itm">

                        <?php if (has_post_thumbnail($article)) { ?>
                            <a href="<?php echo get_permalink($article) ?>">
                                <figure style="background-image: url('<?php echo $url ?>')" class="fea-img"></figure>
                            </a>
                        <?php } else { ?>
                            <a href="<?php echo get_permalink($article) ?>">
                                <figure
                                    style="background-image: url('<?php echo Apollo_App::defaultBlogImage() ?>')"
                                    class="fea-img"></figure>
                            </a>
                        <?php } ?>
                        <div class="fea-cat">
                            <?php $blogType = of_get_option( Apollo_DB_Schema::_BLOG_CATEGORY_TYPES, 'parent-only');
                            Apollo_App::apl_get_blog_categories($article,$blogType)?>
                        </div>
                        <a href="<?php echo get_permalink($article) ?>"><h3><?php echo $title; ?></h3></a>
                        <div class="fea-author"><?php $blogPost->the_get_author_link() ?></div>
                        <div
                            class="desc" data-ride="ap-clamp"
                            data-n="5"> <?php echo $content ?>
                        </div>

                    </article>
                <?php }
            }
        ?>
</div>
<?php endif; ?>


<?php
    $smallArticles = array(of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_TOP,0),of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_MIDDLE,0),
    of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_BOTTOM,0));

if (!empty($smallArticles)):
?>
<div class="wc-r">
    <div class="fea-list">
        <?php

        foreach ($smallArticles as $article) {
            if (isset($article) && $article) {
                $author_id = get_post($article)->post_author;
                $url = wp_get_attachment_url(get_post_thumbnail_id($article));
                $maxWords = 56;
                $title = get_the_title($article);
                $blogPost = get_apl_blog($article);
                $title = $blogPost->getStringByLength($title,$maxWords,true);
                $title = strip_tags($title['text']);
                ?>
                <div class="fea-itm clearfix">
                    <a href="<?php echo get_permalink($article) ?>">
                        <?php if (has_post_thumbnail($article)) { ?>
                            <figure style="background-image: url('<?php echo $url ?>')" class="fea-pic"></figure>
                        <?php } else { ?>
                            <figure
                                style="background-image: url('<?php echo Apollo_App::defaultBlogImage('small') ?>')"
                                class="fea-pic"></figure>
                        <?php } ?>
                        <div class="fea-info">
                            <div class="fea-cat">
                                <?php $blogType = of_get_option( Apollo_DB_Schema::_BLOG_CATEGORY_TYPES, 'parent-only');
                                Apollo_App::apl_get_blog_categories($article,$blogType)?>
                            </div>
                            <a href="<?php echo get_permalink($article) ?>"><h3><?php echo $title; ?></h3></a>
                            <div class="fea-author"><?php $blogPost->the_get_author_link() ?></div>
                        </div>
                    </a>
                </div>
            <?php }
        }
        ?>
        <!-- END : ITM-->
    </div>
</div>
<?php endif; ?>