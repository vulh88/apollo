<?php
$id = get_query_var('_apollo_news_id');
$post = get_post($id);
$src = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'large');
$src = isset($src[0]) ? $src[0] : '';
$caption = get_post(get_post_thumbnail_id($id));

$logo = Apollo_App::getHeaderLogo(Apollo_DB_Schema::_HEADER);
$date = date('M d, Y', strtotime($post->post_date));
$postCategories = wp_get_post_categories($id);
$excerpt = wp_trim_words($post->post_content, 35);
$author = get_user_by('id', $post->post_author);
$content = apply_filters('the_content', $post->post_content);
$content = str_replace(']]>', ']]&gt;', $content);
$tags = get_the_tag_list('', ', ', '', $id);
if (!$post) {
    echo Apollo_App::getTemplatePartCustom('templates/404-print');
    exit;
}
?>

<html>
<head><title><?php _e(_e('Print detail post | ', 'apollo') . $post->post_title, 'apollo') ?></title>
    <?php include APOLLO_TEMPLATES_DIR . '/partials/no-index.php' ?>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css"/>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/print.css" type="text/css"/>
    <style>
        <?php echo Apollo_App::getPrimarycolorCSSContent() ?>
    </style>
</head>
<body id="post-print-page">
<?php
if ($logo != '') {
    ?>
    <div class="top">
        <div class="top-head">
            <div class="logo print-page">
                <a href="<?php echo home_url() ?>">
                    <img src="<?php echo $logo; ?>"/>
                </a>
            </div>
        </div>
    </div>
<?php } ?>
<div class="wrapper">
    <div class="block-detail">


        <div class="blog-detail-row">
            <div class="pic art">
                <a>
                    <img src="<?php echo $src; ?>">
                </a>
                <div class="pic-caption-title">
                    <?php if (function_exists('the_media_credit_html')) { ?>
                        <p class="title"><?php the_media_credit_html(get_post_thumbnail_id($id)); ?></p>
                    <?php } else {
                        if ($caption->post_title != '') {
                            echo '<p class="title">' . $caption->post_title . '</p>';
                        }
                    } ?>
                    <?php if ($caption->post_excerpt != '') {
                        echo '<p class="caption">' . $caption->post_excerpt . '</p>';
                    } ?>
                </div>
            </div>
            <div class="blog-detail-meta">
                <div class="category_post_date">
                    <ul>
                        <?php
                        if (is_array($postCategories) && count($postCategories) > 0)
                        {
                        echo ' <li class="categories">';
                        foreach ($postCategories

                        as $categoryId){
                        $category = get_category($categoryId);

                        ?>
                        <a><?php echo $category->name ?>
                            <?php
                            }
                            echo '<a><span>|</span></a></li>';
                            }
                            ?>
                            <li class="post_date"><?php echo $date; ?></li>
                    </ul>
                </div>

                <div class="post-des">
                    <h2 class="tt"><?php echo $post->post_title ?></h2>
                </div>

                <div class="des">
                    <?php if ($excerpt): ?>
                        <p><?php echo wp_strip_all_tags($excerpt) ?></p>
                    <?php endif; ?>
                </div>

                <?php if(!empty($author)) : ?>
                    <div class="post-author">
                        <p>
                            <?php echo sprintf(__('By <b> %s </b>','apollo'), isset($author->display_name) ? $author->display_name : $author->user_login) ?>
                        </p>
                    </div>
                <?php endif; ?>
            </div>
        </div>


        <div class="desc">
            <?php echo $content; ?>
        </div>

        <?php if ($tags): ?>
            <div class="blog-tags">
                <label><?php _e('Tags', 'apollo') ?>:</label>
                <?php echo $tags ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="h-line"></div>
</div>
</body>
<style>
    .logo.print-page img {
        max-height: 81px;
    }
</style>
</html>