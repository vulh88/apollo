<head>
    <meta charset="utf-8">
    <?php
    /** @Ticket #13437 */
    if (isset($post) && $post->post_type == Apollo_DB_Schema::_EVENT_PT) {
        include_once ( ABSPATH . 'wp-admin/includes/plugin.php');
        $hook_name = 'single_post_title';
        if (is_plugin_active(plugin_basename('wordpress-seo/wp-seo.php'))) {
            $hook_name = 'wpseo_title';
        }
        add_filter($hook_name, function($title) {
            global $post;
            $_a_event = get_event($post);
            $GLOBALS['a_event'] = $_a_event;
            $org_name = $_a_event->getEventOrgName();
            if ($org_name) {
                $event_title = $_a_event->get_title();
                $present_text = sprintf(__('%s presented by %s'), $event_title, $_a_event->getEventOrgName());
                return str_replace($event_title, $present_text, $title);
            }
            return $title;
        }, 20);
    }
    //Trilm - force title public art map page
    if (get_query_var('_apollo_public_art_map')):
        $pagename = get_query_var('pagename');
        ?>
        <title><?php echo sprintf(__("Public Art Map | %s "), get_bloginfo('name')); ?></title>
        <?php elseif(isset($_GET['action']) && isset($_GET['site_ids'])): ?>
        <title><?php _e("Apollo Utility Tools | Apollo Multisite"); ?></title>
    <?php else : ?>
        <title><?php wp_title('|', true, 'right'); ?></title>
    <?php endif; ?>

    <?php if ($desc = of_get_option(Apollo_DB_Schema::_SITE_SEO_DESCRIPTION, '')): ?>
        <meta name="description"
              content="<?php echo sanitize_text_field($desc) ?>">
    <?php endif; ?>

    <?php if ($keyword = of_get_option(Apollo_DB_Schema::_SITE_SEO_KEYWORDS, '')): ?>
    <meta name="keywords"
          content="<?php echo sanitize_text_field($keyword) ?>">
    <?php endif; ?>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php
        wp_head();
        echo Apollo_App::renderTrackingScript();
    ?>

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri() ?>/assets/icons/favicon.ico"
          type="image/x-icon">

</head>