<article id="comment-<?php echo $comment->comment_ID ?>" class="comment clearfix">
    <div class="comment-avatar">
        <?php echo get_avatar($comment, $size = '60'); ?>
    </div>
    <div class="comment-ct">
        <label class="author-n">
            <?php echo $comment->comment_author ?>
        </label><a href="javascript:void(0);" class="comment-meta-time muted-small">
            <time datetime="<?php echo date('Y-m-d\TH:i:s\+00:00', strtotime($comment->comment_date_gmt)) ?>"><?php printf(__('%1$s', 'apollo'), get_comment_date()); ?></time></a>

        <?php
        $scomment_short_text = Apollo_App::getStringByLength(get_comment_text($comment->comment_ID), 400);
        $scomment_full_text = Apollo_App::getStringByLength(get_comment_text($comment->comment_ID), null);
        ?>
        <div class="comment-content comment" id="_comment_short_<?php echo $comment->comment_ID ?>">
            <?php echo $scomment_short_text['text']; ?>
            <?php if($scomment_short_text['have_more'] === true): ?>
                <a href="javascript:void(0);" data-type="vmore" class="vmore" data-target='#_comment_full_<?php echo $comment->comment_ID ?>' data-own='#_comment_short_<?php echo $comment->comment_ID ?>'><?php _e('View more', 'apollo') ?></a>
            <?php endif; ?>
        </div>

        <div class="comment-content comment hidden" id="_comment_full_<?php echo $comment->comment_ID ?>">
            <?php echo $scomment_full_text['text']; ?>
            <a href="javascript:void(0);" data-type="vmore" class="vmore" data-target='#_comment_short_<?php echo $comment->comment_ID ?>' data-own='#_comment_full_<?php echo $comment->comment_ID ?>'><?php _e('View less', 'apollo') ?></a>
        </div>
        <?php edit_comment_link(__('(Edit)', 'apollo'), '', ''); ?>

        <?php if ($comment->comment_approved == '0') : ?>
            <div class="alert alert-info">
                <?php _e('Your comment is awaiting moderation.', 'apollo'); ?>
            </div>
        <?php endif; ?>
        <p class="t-l"><?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))); ?></p>
    </div>