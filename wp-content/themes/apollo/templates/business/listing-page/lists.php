<?php

    if ( $results = $this->get_results() ) {
        foreach ( $results as $p ):

            /* Thienld : $businessObj : business instance */
            $businessObj = get_business( $p );
            $orgID = get_apollo_meta($businessObj->id,Apollo_DB_Schema::_APL_BUSINESS_ORG,true);
            $orgObj = get_org($orgID);
            $bsIsRestaurant = get_apollo_meta($orgObj->id,Apollo_DB_Schema::_ORG_RESTAURANT,true) == "yes";
            $bsTypes = $businessObj->getDisplayBusinessType();
            $bsService = $bsIsRestaurant ? $businessObj->getDisplayBusinessService() : '';
            $summary100 = $businessObj->get_summary(100);

            /** @Ticket #13061 */
            $businessType = $businessObj->generate_categories();
            global $apl_current_taxonomy_slug, $apl_current_taxonomy_type;
            /** @Ticket #13422 */
            $title = $businessObj->get_title(true);

            /** @Ticket #13137 */
            $hasPlaceholderImg = of_get_option(Apollo_DB_Schema::_ENABLE_PLACEHOLDER_IMG, 1);
            $image = $orgObj->get_image_with_option_placeholder('thumbnail', array(),
                array(
                    'aw' => true,
                    'ah' => true,
                ),
                'normal', $hasPlaceholderImg);

            /** @Ticket #13627 */
            $icon_fields = maybe_unserialize( get_option( Apollo_DB_Schema::_APL_ORG_ICONS ) );
            $icons_in_post = $orgObj->get_meta_data( Apollo_DB_Schema::_APL_ORG_POST_ICONS, Apollo_DB_Schema::_APL_ORG_DATA );
            $discount_button = of_get_option(Apollo_DB_Schema::_APL_BUSINESS_ENABLE_DISCOUNT_BUTTON, 0);
            $discount_url = $orgObj->get_meta_data(Apollo_DB_Schema::_ORG_DISCOUNT_URL);
            ?>
            <li>
                <div class="search-img <?php echo !$image ? 'no-place-holder-cate' : '' ?>"><a href="<?php echo $businessObj->get_permalink() ?>"><?php echo $image; ?></a></div>
                <div class="info-content <?php echo ($icons_in_post ? 'show-icons' : '' ); ?> <?php echo !$image ? 'img-no-display' : ''; ?>" data-type="link" data-url="<?php echo $businessObj->get_permalink() ?>">
                    <div class="search-info">
                        <a href="<?php echo $businessObj->get_permalink() ?>"><span class="ev-tt"><?php echo $title; ?></span></a>
                        <div class="wrap-search-info-content">
                            <?php if ($businessType): ?>
                                <div class="career">
                                    <?php echo $businessType ; ?>
                                </div>
                            <?php endif; ?>
                            <?php if ($icons_in_post) : ?>
                                <div class="icons-list-after-thumbsup">
                                    <?php $orgObj->renderIcons($icon_fields, $icons_in_post); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        <?php
                        if (!$discount_button || empty($discount_url)) : ?>
                            <?php if ($bsService): ?>
                                <div class="career" data-ride="ap-clamp" data-n="2"><?php _e('Service: ', 'apollo'); echo $bsService; ?></div>
                            <?php endif; ?>
                            <div class="s-desc"><?php echo $summary100['text'];  ?> <?php echo $summary100['have_more'] ? '...' : '' ?></div>
                        <?php else :
                                $checkD = of_get_option(Apollo_DB_Schema::_APL_ORGANIZATION_CHECK_DISCOUNTS_TEXT, __('CHECK DISCOUNTS', 'apollo'));
                            ?>
                                <div class="b-btn btn-discount-list">
                                    <a target="_BLANK" href="<?php echo $discount_url; ?>" class="btn btn-b check-discount"><?php echo $checkD ?></a>
                                </div>
                        <?php endif;?>
                    </div>
                </div>
            </li>
        <?php 
        endforeach;
    } else if ( !isset($_GET['s']) && !isset($_GET['keyword']) ) {
        _e( 'No results', 'apollo' );
    }