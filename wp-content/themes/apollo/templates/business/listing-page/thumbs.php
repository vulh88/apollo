<?php
if ( $results = $this->get_results() ) {
    foreach ( $results as $p ):

        /* Thienld : $businessObj : business instance */
        $businessObj = get_business( $p );
        $orgID = get_apollo_meta($businessObj->id,Apollo_DB_Schema::_APL_BUSINESS_ORG,true);
        $orgObj = get_org($orgID);
        $bsIsRestaurant = get_apollo_meta($orgObj->id,Apollo_DB_Schema::_ORG_RESTAURANT,true) == "yes";
        $bsTypes = $businessObj->getDisplayBusinessType();
        $bsService = $bsIsRestaurant ? $businessObj->getDisplayBusinessService() : '';
        $summary100 = $businessObj->get_summary(100);
        /** @Ticket #13061 */
        $businessType = $businessObj->generate_categories();
        global $apl_current_taxonomy_slug, $apl_current_taxonomy_type;
        /** @Ticket #13422 */
        $title = $businessObj->get_title(true);

        ?>
        <li>
            <div class="div-one" data-url="<?php echo $businessObj->get_permalink() ?>" data-type="link">
                <div class="search-img"><a href="<?php echo $businessObj->get_permalink() ?>"><?php echo $orgObj->get_image( 'medium', array(),
                            array(
                                'aw' => true,
                                'ah' => true,
                            ),
                            'normal'
                        ) ?></a></div>
                <div class="search-info"><a href="<?php echo $businessObj->get_permalink() ?>"><span class="ev-tt" data-ride="ap-clamp" data-n="2"><?php echo $title; ?></span></a>
                    <?php if ($businessType): ?>
<!--                        <div class="career" data-ride="ap-clamp" data-n="2">--><?php //_e('Category: '); echo $bsTypes; ?><!--</div>-->
                        <div class="career"><?php echo $businessType ; ?></div>
                    <?php endif; ?>
                    <?php if ($bsService): ?>
                        <div class="career" data-ride="ap-clamp" data-n="2"><?php _e('Service: '); echo $bsService; ?></div>
                    <?php endif; ?>
                    <div class="s-desc" data-ride="ap-clamp" data-n="9"><?php echo $summary100['text'];  ?></div>
                </div>
            </div>
        </li>

    <?php endforeach;

} else if ( !isset($_GET['s']) && !isset($_GET['keyword']) ) {
    _e( 'No results', 'apollo' );
}