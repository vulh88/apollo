<?php
if ($search_obj->viewType == 'default'):

?>

<div class="search-bkl">
    <nav class="type">
        <ul>
            <li <?php echo !$search_obj->is_list_page(of_get_option(APL_Business_Module_Theme_Option::_BUSINESS_DEFAULT_VIEW_TYPE)) ? 'class="current"' : '' ?> ><a href="<?php echo $search_obj->get_template_btn_url() ?>"><i class="fa fa-th fa-2x"></i></a></li>
            <li <?php echo $search_obj->is_list_page(of_get_option(APL_Business_Module_Theme_Option::_BUSINESS_DEFAULT_VIEW_TYPE)) ? 'class="current"' : '' ?>><a href="<?php echo $search_obj->get_template_btn_url( 'list' ) ?>"><i class="fa fa-bars fa-2x"></i></a></li>
        </ul>
    </nav>
</div>


<?php endif; ?>