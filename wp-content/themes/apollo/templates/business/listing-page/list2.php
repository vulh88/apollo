<?php

if ( $results = $this->get_results() ) {
foreach ( $results as $p ):

$businessObj = get_business( $p );
$orgID = get_apollo_meta($businessObj->id,Apollo_DB_Schema::_APL_BUSINESS_ORG,true);
$orgObj = get_org($orgID);

$address = $orgObj->get_full_address();
$phone = $orgObj->get_meta_data(Apollo_DB_Schema::_ORG_PHONE, Apollo_DB_Schema::_APL_ORG_DATA);
$website = $orgObj->get_meta_data(Apollo_DB_Schema::_ORG_WEBSITE_URL, Apollo_DB_Schema::_APL_ORG_DATA);
$bsIsRestaurant = get_apollo_meta($orgObj->id,Apollo_DB_Schema::_ORG_RESTAURANT,true) == "yes";
$bsTypes = $businessObj->getDisplayBusinessType();
$bsService = $bsIsRestaurant ? $businessObj->getDisplayBusinessService() : '';
$summary100 = $businessObj->get_summary(240);
$terms = wp_get_post_terms($p->ID, 'business-type');

/** @Ticket #13061 */
$businessType = $businessObj->generate_categories();
global $apl_current_taxonomy_slug, $apl_current_taxonomy_type;

    /** @Ticket #13422 */
    $title = $businessObj->get_title(true);

    $hasPlaceholderImg = of_get_option(Apollo_DB_Schema::_ENABLE_PLACEHOLDER_IMG, 1);
    $image = $orgObj->get_image_with_option_placeholder('thumbnail', array(),
        array(
            'aw' => true,
            'ah' => true,
        ),
        'normal', $hasPlaceholderImg);

    /** @Ticket #13627 */
    $icon_fields = maybe_unserialize( get_option( Apollo_DB_Schema::_APL_ORG_ICONS ) );
    $icons_in_post = $orgObj->get_meta_data( Apollo_DB_Schema::_APL_ORG_POST_ICONS, Apollo_DB_Schema::_APL_ORG_DATA );
    $discount_button = of_get_option(Apollo_DB_Schema::_APL_BUSINESS_ENABLE_DISCOUNT_BUTTON, 0);
    $discount_url = $orgObj->get_meta_data(Apollo_DB_Schema::_ORG_DISCOUNT_URL);
?>

<li>
    <div class="summary-content-bs clearfix">
        <div class="search-img <?php echo !$image ? 'no-place-holder' : '' ?>"><a href="<?php echo $businessObj->get_permalink(); ?>"><?php echo $image; ?></a></div>
        <div class="info-content <?php echo ($icons_in_post ? 'show-icons' : '' ); ?>">
            <div class="search-info"><a href="<?php echo $businessObj->get_permalink() ?>"><span class="ev-tt"><?php echo $title; ?></span></a>
                <div class="wrap-search-info-content">
                    <?php if ($businessType):?>
                    <div class="s-country"><?php echo $businessType; ?></div>
                    <?php endif; ?>
                    <?php if ($icons_in_post) : ?>
                        <div class="icons-list-after-thumbsup">
                            <?php $orgObj->renderIcons($icon_fields, $icons_in_post); ?>
                        </div>
                    <?php endif; ?>
                    <div class="s-desc"><?php echo $summary100['text'];  ?> <?php echo $summary100['have_more'] ? '...' : '' ?></div>
                    <?php
                    if ($discount_button && !empty($discount_url)) :
                        $checkD = of_get_option(Apollo_DB_Schema::_APL_ORGANIZATION_CHECK_DISCOUNTS_TEXT, __('CHECK DISCOUNTS', 'apollo'));
                    ?>
                        <div class="b-btn btn-discount-list">
                            <a target="_BLANK" href="<?php echo $discount_url; ?>" class="btn btn-b check-discount"><?php echo $checkD ?></a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="summary-contact-bs clearfix">
        <?php if ($address): ?>
        <div class="contact-address-bs"> <span class="lbl-contact"><?php _e('address:', 'apollo') ?></span><span class="contact-txt"><?php echo $address ?></span></div>
        <?php endif; ?>

        <div class="contact-social-info-bs clearfix">
            <?php if ($phone): ?>
            <div class="contact-item-social-bs"><span class="lbl-contact"><?php _e('tel:', 'apollo') ?></span><span class="contact-txt"><?php echo $phone ?></span></div>
            <?php endif; ?>

            <?php if ($website): ?>
            <div class="contact-item-social-bs"><span class="lbl-contact"><?php _e('website:', 'apollo') ?></span><a target="_blank" href="<?php echo $website ?>" class="contact-txt-link"><?php echo $title; ?></a></div>
            <?php endif; ?>
        </div>
    </div>
</li>

    <?php
endforeach;
} else if ( !isset($_GET['s']) && !isset($_GET['keyword']) ) {
    _e( 'No results', 'apollo' );
}