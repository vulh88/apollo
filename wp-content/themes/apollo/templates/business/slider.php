<?php

/**
 * Apply slider for Business Spotlight
 *
 * Ticket #11189
 */

if ( !$slug = get_query_var('term') ) {
    return;
}

$currentTerm = get_term_by('slug',  $slug, get_query_var( 'taxonomy' ));
if ( empty($currentTerm) || !isset($currentTerm->taxonomy) || $currentTerm->taxonomy != 'business-type' ) {
    return;
}

if ( !of_get_option( APL_Business_Module_Theme_Option::_BUSINESS_ENABLE_SPOT , 0) ) {
    return;
}

$sliderType = of_get_option( Apollo_DB_Schema::_SPOT_TYPE_BUSINESS );
if ( !$sliderType ) {
    return;
}

?>

<section class="section-slider">
    <?php
        $num = ($num_opt = of_get_option( Apollo_DB_Schema::_NUM_BUSINESS_SPOTLIGHT )) ? $num_opt : Apollo_Display_Config::NUM_BUSINESS_SPOTLIGHT;

        $businessSpotlights = get_posts( array(
            'post_type'      => Apollo_DB_Schema::_BUSINESS_SPOTLIGHT_PT,
            'posts_per_page' => $num,
            'orderby'        => 'menu_order',
            'order'          => 'ASC',
            'meta_query' => array(
                array(
                    'key'   => Apollo_DB_Schema::_APL_BUSINESS_SPOT_TYPES_PRIMARY_ID,
                    'value' => $currentTerm->term_id,
                )
            )
        ));

        /** @Ticket #13130 */

    $semi_trans = of_get_option(Apollo_DB_Schema::_APL_BUSINESS_SEMI_TRANSPARENT_TYPE, 'below');
    $bg_semi_trans = of_get_option( Apollo_DB_Schema::_APL_BUSINESS_SEMI_TRANSPARENT_TYPE_BACKGROUND_COLOR, '#000000' );

    if ($bg_semi_trans):
        ?>
        <style>
            .main-slider .wrap-inner {
                background-color:  <?php echo $bg_semi_trans ?> !important;
            }
            .business-slider .wrap-inner .i-caption-dine {
                background-color: <?php echo $bg_semi_trans ?> !important;
            }
            .business-slider .i-slider .i-caption-dine {
                background-color: <?php echo $bg_semi_trans ?> !important;
                opacity: 1;
            }
            .i-slider .i-caption-dine {
                background-color: <?php echo $bg_semi_trans ?> !important;
                opacity: 0.7;
            }
        </style>
    <?php endif;

        if ( $businessSpotlights ):
    ?>
            <!--START: SLIDER-->
            <div class="main-slider full">
                <div class="<?php echo ($sliderType == 'small') ? 'inner' : ''; ?>">
                    <div class="main-slider-overlay">
                        <div class="loading" style="display: none;"><a><i class="fa fa-spinner fa-spin fa-3x"></i></a></div>
                    </div>
                    <div class="flexslider <?php echo ($semi_trans != 'def') ? 'business-semi-trans' : ''; ?>"
                         data-speed="<?php echo Apollo_App::getSlideShowSpeed(Apollo_DB_Schema::_BUSINESS_PT) ?>">
                        <ul class="slides <?php echo ($semi_trans != 'def') ? 'business-slider' : ''; ?>">
                            <?php foreach ( $businessSpotlights as $spt ):
                                $spt = get_spot( $spt );

                                $l  = $spt->get_post_meta_data(Apollo_DB_Schema::_APL_BUSINESS_SPOTLIGHT_LINK);
                                $l  = trim($l);
                                $_l = !empty($l) ? $l : 'javascript:void(0);';

                                $termID       = $spt->get_post_meta_data(Apollo_DB_Schema::_APL_BUSINESS_SPOT_TYPES_PRIMARY_ID);
                                $term         = get_term($termID, 'business-type');
                                $open_style   = $spt->get_post_meta_data(Apollo_DB_Schema::_APL_BUSINESS_SPOTLIGHT_LINK_OPEN_STYLE);
                                $target       = $open_style ? 'target = "blank"' : '';
                                $img_id       = $spt->get_image_id();
                                $src_img      = wp_get_attachment_image_src($img_id, 'large');
                                $_o_summary   = $spt->get_summary(Apollo_Display_Config::HOME_SPOTLIGHT_EVENT_OVERVIEW_NUMBER_CHAR);

                                if ( ! $src_img[0] ) continue;
                                ?>
                                <li>
                                    <?php if ($semi_trans == 'def' || $sliderType == 'small') : ?>
                                        <div class="i-slider">
                                            <div class="inner">
                                                <?php if ( $_o_summary['text'] ):
                                                    ?>
                                                    <div class="i-caption-dine clearfix">
                                                        <div class="capt-dine-category">
                                                            <h3><?php echo $term->name; ?></h3>
                                                        </div>
                                                        <div class="capt-dine-content">
                                                            <h4 class="capt-ct-name"><a target="<?php echo $target; ?>" href="<?php echo $_l; ?>"><?php echo $spt->get_title(); ?></a></h4>
                                                            <p class="capt-ct-desc"><?php echo $_o_summary['text'] . ( $_o_summary['have_more'] ? '...' : '' ); ?></p>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <a target="<?php echo $target; ?>" href="<?php echo $_l; ?>">
                                                <img src="<?php echo isset( $src_img[0] ) ? $src_img[0] : ''; ?>" alt="<?php echo $spt->get_title() ?>">
                                            </a>
                                        </div>
                                    <?php else : ?>
                                        <div class="i-slider">
                                            <a target="<?php echo $target; ?>" href="<?php echo $_l; ?>">
                                                <img src="<?php echo isset( $src_img[0] ) ? $src_img[0] : ''; ?>" alt="<?php echo $spt->get_title() ?>">
                                            </a>
                                        </div>
                                        <div class="wrap-inner">
                                            <div class="inner">
                                                <?php if ( $_o_summary['text'] ):
                                                    ?>
                                                    <div class="i-caption-dine clearfix">
                                                        <div class="capt-dine-category">
                                                            <h3><?php echo $term->name; ?></h3>
                                                        </div>
                                                        <div class="capt-dine-content">
                                                            <h4 class="capt-ct-name"><a target="<?php echo $target; ?>" href="<?php echo $_l; ?>"><?php echo $spt->get_title(); ?></a></h4>
                                                            <p class="capt-ct-desc"><?php echo $_o_summary['text'] . ( $_o_summary['have_more'] ? '...' : '' ); ?></p>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <!--END: SLIDER-->
        <?php endif; ?>
</section>
