<?php 
    $id = get_query_var( '_apollo_org_id' );
    $org = get_org(get_post( $id ));
    $data = $org->getOrgData();
    $logo =  Apollo_App::getHeaderLogo(Apollo_DB_Schema::_HEADER );
    if ( ! $org->post ) {
        echo Apollo_App::getTemplatePartCustom('templates/404-print');
        exit;
    }
?>

<html>
    <head><title><?php _e( _e( 'Print detail organization | ', 'apollo' ) . $org->get_title(), 'apollo' ) ?></title>
        <?php include APOLLO_TEMPLATES_DIR. '/partials/no-index.php' ?>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css"/>
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"/>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/print.css" type="text/css"/>
        <style>
            <?php echo Apollo_App::getPrimarycolorCSSContent() ?>
        </style>
    </head>
    <body id="artist-print-page">
    <div class="top">
        <div class="top-head">
            <div class="logo print-page"><a href="<?php echo home_url() ?>"> <img src="<?php echo $logo; ?>"/></a></div>
        </div>
    </div>
    <div class="wrapper">
        <div class="block-detail">
            <div class="pic art"><a><?php echo $org->get_image( 'medium' ); ?></a></div>
            <div class="artist-des">
                <h2 class="tt"><?php  echo $org->get_title() ?></h2>
                <p><?php echo  $org->generate_categories(true) ?></p>

                <?php
                    if(isset($data['web']) && $data['web']){
                ?>
                    <p> <i class="fa fa-link fa-lg"></i><span><?php _e('Website:','apollo') ?>
                            <a href="#" target="blank"><?php echo $data['web']; ?></a></span>
                    </p>
                <?php
                }
                ?>
                <?php
                if(isset($data['phone']) && $data['phone']){
                    ?>
                    <p> <i class="fa fa-phone fa-lg">&nbsp;</i><span><?php echo $data['phone'] ?></span></p>
                <?php
                }
                ?>
                <?php
                if(isset($data['fax']) && $data['fax']){
                    ?>
                    <p> <i class="fa fa-fax fa-lg">&nbsp;</i><span><?php echo $data['fax'] ?></span></p>
                <?php
                }
                ?>
                <?php
                if(isset($data['address']) && $data['address']){
                    ?>
                    <p> <i class="fa fa-map-marker fa-lg">&nbsp;</i><span><?php echo $data['address'] ?></span></p>
                <?php
                }
                ?>
            </div>
            <div class="desc">
                <?php
                    $content = $org->get_full_content();
                    echo Apollo_App::convertContentEditorToHtml($content);
                ?>
            </div>
        </div>
        <div class="h-line"></div>
        
        <?php  $org->renderAddFieldPrintDetailPage($data['add_fields']); ?>
        
        <?php
            if(!empty($data['facebook']) || !empty($data['twitter']) || !empty($data['inst']) || !empty($data['linked']) || !empty($data['pinterest'])  ):
        ?>
        <div class="block-detail"><a><i class="fa fa-external-link fa-2x"></i></a><span class="question"><?php _e('SOCIAL MEDIA LINKS','apollo'); ?></span>
            <div class="el-blk">
                <?php
                    if(isset($data['facebook']) && !empty($data['facebook'])){
                        ?>
                        <div class="art-social-item"><a href="<?php echo $data['facebook'] ?>" target="_blank"><?php echo $data['facebook'] ?></a></div>
                <?php
                    }
                ?>

            <?php
                if(isset($data['twitter']) && !empty($data['twitter'])){
                    ?>
                    <div class="art-social-item"><a href="<?php echo $data['twitter'] ?>" target="_blank"><?php echo $data['twitter'] ?></a></div>
                <?php
                }
                ?>

                <?php
                if(isset($data['inst']) && !empty($data['inst'])){
                    ?>
                    <div class="art-social-item"><a href="<?php echo $data['inst'] ?>" target="_blank"><?php echo $data['inst'] ?></a></div>
                <?php
                }
                ?>

                <?php
                if(isset($data['linked']) && !empty($data['linked'])){
                    ?>
                    <div class="art-social-item"><a href="<?php echo $data['linked'] ?>" target="_blank"><?php echo $data['linked'] ?></a></div>
                <?php
                }
                ?>

                <?php
                if(isset($data['pinterest']) && !empty($data['pinterest'])){
                    ?>
                    <div class="art-social-item"><a href="<?php echo $data['pinterest'] ?>" target="_blank"><?php echo $data['pinterest'] ?></a></div>
                <?php
                }
                ?>

            </div>
        </div>
        <?php endif; ?>
    </div>
    </body>
    <style>
        .logo.print-page img{
            max-height:81px;
        }
    </style>
</html>