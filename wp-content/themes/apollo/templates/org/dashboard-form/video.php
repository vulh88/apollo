<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/27/2015
 * Time: 6:05 PM
 */
if( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ORGANIZATION_PT )){

    /**
     * Whether checking the current user has profile or not
     */
    if (! Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_ORGANIZATION_PT)) {

        // Multiple mode
        if (Apollo_App::isInputMultiplePostMode()) {
            if(strpos($GLOBALS['wp']->matched_rule, '^user/org/video') !== false) {
                wp_redirect(site_url(APL_Dashboard_Hor_Tab_Options::ORG_ADD_NEW_URL));
            }
        }
        else { // Single mode
            wp_redirect('/user/org/profile/?warning');
        }
    }



    //load page content here
    $page = get_page_by_path( Apollo_Page_Creator::ID_ORG_PROFILE_VIDEOS,ARRAY_A,'page');
    $content =  isset($page['post_content'])?$page['post_content']:'';

    echo '<div class="dsb-welc" style="">';
        echo '<div class="dsb-welc custom"><h1>'.__('Video','apollo').'</h1></div>';
    echo '</div>';
    $formOrg = new Apollo_Organization_Video_Form(
        'Organization-video-frm', //form Id
        'Organization-video-frm-name', //form name
        'POST', //form method
        $_SERVER['REQUEST_URI'], //action url
        array(), //external form elements
        $_SERVER['REQUEST_METHOD'] //current request method
    );
    //set process and validate class for this form
    $formOrg->setOptionValidateClass('Apollo_Submit_Organization');
    $formOrg->setFormDescription('<p style="margin-bottom: 22px">'.__($content,'apollo').'</p>');
    $formOrg->renderForm();
    include APOLLO_TEMPLATES_DIR. '/pages/dashboard/html/partial/_common/video-box.php';
}
