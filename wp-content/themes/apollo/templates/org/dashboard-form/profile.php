<?php
//check is_active module here
if( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ORGANIZATION_PT )){
    //load page content here
    $post = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ORG_PROFILE));

    if(Apollo_App::isInputMultiplePostMode()){
        /* Handle for case of allowing to add multiple org */
        if(strpos($GLOBALS['wp']->matched_rule, '^user/org/add-new') !== false){
            $isAddingNewOrg = 1;
        } else {
            $isAddingNewOrg = 0;
        }

        if(strpos($GLOBALS['wp']->matched_rule, '^user/org/profile/(\d+)') !== false &&
            ( intval(get_query_var( '_edit_post_id', 0)) === 0 )
        ){
            wp_redirect(site_url(APL_Dashboard_Hor_Tab_Options::ORG_ADD_NEW_URL));
        }
    }

    $formOrg = new Apollo_Organization_Form(
        'Organization-frm', //form Id
        'Organization-frm-name', //form name
        'POST', //form method
        str_replace('?warning', '', $_SERVER['REQUEST_URI']), //action url
        array(), //external form elements
        $_SERVER['REQUEST_METHOD'] //current request method
    );
    //set process and validate class for this form
    if(Apollo_App::isInputMultiplePostMode()) {
        $formOrg->setIsAddingNewOrg($isAddingNewOrg);
    }
    $formOrg->setOptionValidateClass('Apollo_Submit_Organization');
    $formOrg->setFormDescription('<p>'.$post->post_content.'</p>');
    $formOrg->renderForm();
}

