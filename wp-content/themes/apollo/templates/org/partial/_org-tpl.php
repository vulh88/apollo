<?php foreach( $associatedOrgs as $org ): ?>
    <?php
        $org        = get_org($org);
        $summary100 = $org->get_summary(100);
    ?>
    <div class="more-cat-itm">
        <div class="more-pic">
            <a href="<?php echo $org->get_permalink() ?>"><?php echo $org->get_image( 'thumbnail', array(), array('aw' => true, 'ah' => true), 'normal') ?></a>
        </div>

        <div class="more-ct">
            <div data-type="link" data-url="<?php echo $org->get_permalink() ?>">
                <h3>
                    <a href="<?php echo $org->get_permalink() ?>">
                        <span class="ev-tt">
                            <?php
                                $title = $org->get_title();
                                echo ( $title == false || empty($title) ) ? __('No title', 'apollo') : $title;
                            ?>
                        </span>
                    </a>
                </h3>
            </div>
            <div class="s-desc">
                <?php echo $summary100['text']  ?> <?php echo $summary100['have_more'] ? '...' : '' ?>
            </div>
        </div>
    </div>
<?php endforeach; ?>

