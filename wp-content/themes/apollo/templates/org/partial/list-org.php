<?php foreach($arrdata['datas'] as $data ):
    // GET MORE DATA
    $_org = get_org($data->ID);
    $edit_org_link = site_url(APL_Dashboard_Hor_Tab_Options::ORG_PROFILE_URL) . '/'.$data->ID;
    ?>
    <li>
        <div class="event-img">
            <a target="_blank" href="<?php echo $_org->get_permalink(); ?>">
                <img src="<?php echo $_org->get_thumb_image_url() ?>">
            </a>
        </div>
        <div class="event-info ">
            <a target="_blank" href="<?php echo $_org->get_permalink(); ?>">
                <span class="ev-tt"><?php echo $_org->get_title() ?><?php echo isset($_org->post) && isset($_org->post->post_status) && $_org->post->post_status == 'pending' ? '*' : ''; ?></span>
            </a>
            <a class="icons-info" title="<?php _e('Edit', 'apollo') ?>" href="<?php echo $edit_org_link; ?>">
                <i class="fa fa-pencil-square-o"></i>
            </a>
        </div>
    </li>
<?php endforeach; ?>