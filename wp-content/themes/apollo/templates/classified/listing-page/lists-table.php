<?php

if ( $results = $this->get_results() ) {
    foreach ( $results as $p ):

        $e = get_classified( $p );
        $data = $e->getDataDetail();
        $deadlineDate = $data['deadline_date'] ? date( 'm/d/Y', strtotime($data['deadline_date']) ) : '';
        ?>
        <tr>
            <td><?php echo date( 'm/d/Y', strtotime($p->post_date) ) ?></td>
            <td id="td-title"> <a href="<?php echo $e->get_permalink() ?>"><?php echo $e->get_full_title()?></a></td>
            <td><?php echo $e->classified_org_links();?></td>
            <td><?php echo $data['city']?> </td>
            <td><?php echo $deadlineDate; ?></td>
        </tr>
        <?php
    endforeach; ?>

<?php } else {
    _e( 'No results', 'apollo' );
}
?>
