<?php

    if ( $results = $this->get_results() ) {
        /** @Ticket #13137 */
        $hasPlaceholderImg = of_get_option(Apollo_DB_Schema::_ENABLE_PLACEHOLDER_IMG, 1);
        $enableImage = of_get_option(Apollo_DB_Schema::_CLASSIFIED_ENABLE_PRIMARY_IMAGE,1);
        foreach ( $results as $p ): 
            
            $e = get_classified( $p );
            $data = $e->getDataDetail();
            $deadlineDate =  isset($data['deadline_date']) ? $data['deadline_date'] : '';
            $summary100 = $e->get_summary(200);
            $image = $e->get_image('thumbnail', array(),
                array(
                    'aw' => true,
                    'ah' => true,
                ),
                'normal', $hasPlaceholderImg);
        ?>
            <li>
                <?php if($enableImage) {?>
                <div class="search-img <?php echo !$image ? 'no-place-holder-cate' : ''; ?>"><a href="<?php echo $e->get_permalink() ?>"><?php echo $image; ?></a></div> <?php } ?>
                <div class="info-content <?php echo $enableImage ? '' : 'no-left'; ?>" data-url="<?php echo $e->get_permalink() ?>" data-type="link">
                    <div class="search-info"><a href="<?php echo $e->get_permalink() ?>"><span class="ev-tt"><?php echo $e->get_title_listing_page() ?></span></a>
                        <div class="career"><?php echo $e->generate_categories(); ?> </div>
                        <div class="post-date deadline-date">
                            <span data-ride="ap-clamp" data-n="1">
                                <?php _e('Posted: ', 'apollo');echo date( 'M d, Y', strtotime($p->post_date) ) ?>;
                                <?php if ($deadlineDate) {
                                    _e('Deadline: ', 'apollo');echo date('M d, Y', strtotime($deadlineDate)) ;
                                 }?>
                            </span>
                        </div>
                        <div class="s-desc desc-list-classified"><?php echo $summary100['text']  ?> <?php echo $summary100['have_more'] ? '...' : '' ?>
                        <?php if($summary100['have_more']) : ?>
                            <a href="<?php the_permalink($p->ID); ?>" class="vmore classified-vmore"><?php _e( 'View more' , 'apollo' ); ?></a>
                        <?php endif; ?>
                        </div>
                    </div>
                </div>
            </li>
        <?php 
        endforeach;
    } else if ( !isset($_GET['s']) && !isset($_GET['keyword']) ) {
        _e( 'No results', 'apollo' );
    }