<?php

/**
 * @page: content current page
 */
class List_Classified_Adapter {

    public $classified;
	public $pageSize;
	public $page;
	public $totalPost;

    public $_order_by;
    public $_order = 'ASC';
    public $_field_order;
    public $_template;
    public $displayed_events = array();
    public $is_topten = false;


    public function __construct($page, $pageSize = Apollo_Display_Config::PAGE_SIZE ) {

        // Check module is avaiable
        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_CLASSIFIED ) ) {
            return false;
        }


		if(empty($pageSize)) {
			$pageSize = Apollo_Display_Config::PAGE_SIZE;
		}

		$this->pageSize = intval( $pageSize );
		$this->page = $page;
	}



    public function render_html( $template = '' ) {
        ob_start();
        include $template ? $template : $this->_template;
        return ob_get_clean();
    }

	public function getClassified() {
		return $this->classified;
	}

	public function isEmpty() {
		return empty($this->classified);
	}


	public function isShowMore() {
		$current_pos = ($this->page * $this->pageSize);
		return $current_pos < $this->totalPost;
	}


    public function get_org_classified($org_id, $query = '>=',$and = 'OR',$offset = true){;
        $this->pageSize = of_get_option(Apollo_DB_Schema::_NUM_VIEW_MORE,Apollo_Display_Config::_NUM_VIEW_MORE);
        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ORGANIZATION_PT ) || ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_CLASSIFIED )) {
            return false;
        }
        global $wpdb, $post;
        $apollo_meta_table   = $wpdb->apollo_classifiedmeta;
        $_expired_date_field  = Apollo_DB_Schema::_APL_CLASSIFIED_EXP_DATE;
        $_current_date      = current_time( 'Y-m-d' );

        if (!$this->page) {
            $this->page = 1;
        }

        $_offset = $offset ? ($this->page-1) * $this->pageSize. ', ' : '';

        $arr_order = Apollo_Sort_Manager::getSortOneFieldForPostType('classified');
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.* FROM {$wpdb->posts} p
            INNER JOIN {$apollo_meta_table} mt_expired_d ON p.ID = mt_expired_d.apollo_classified_id AND mt_expired_d.meta_key = '". Apollo_DB_Schema::_APL_CLASSIFIED_EXP_DATE ."'
            INNER JOIN {$apollo_meta_table} mt_ord_id ON p.ID = mt_ord_id.apollo_classified_id AND mt_ord_id.meta_key = '". Apollo_DB_Schema::_APOLLO_CLASSIFIED_ORGANIZATION ."' ";

        $sql .=   " WHERE 1=1 AND p.post_status = 'publish' AND p.post_type = '". Apollo_DB_Schema::_CLASSIFIED ."' AND  CAST( mt_expired_d.meta_value AS DATE )  ".$query." '{$_current_date}'
                    AND mt_ord_id.meta_value = $org_id ";
        $sql .= " GROUP BY p.ID ";
        if($arr_order['type_sort'] === 'post') {
            $sql .= " ORDER BY p.{$arr_order['order_by']} {$arr_order['order']} ";
        }
        $_offset = $_offset ? $_offset : 0;
        $sql .= " LIMIT $_offset $this->pageSize ";
        $this->classified   = $wpdb->get_results($sql);
        $this->totalPost    = $wpdb->get_var("SELECT FOUND_ROWS()");
        $currentTheme = function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '';
        if ( $currentTheme == APL_SM_DIR_THEME_NAME) {
            $this->_template    = SONOMA_MODULES_DIR. '/organization/templates/partials/_org-classified.php';
        } else {
            $this->_template    = APOLLO_PARENT_DIR.'/templates/classified/_org-classified.php';
        }

        return $this->render_html($this->_template);
    }

}
