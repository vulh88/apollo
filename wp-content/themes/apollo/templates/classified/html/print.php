<?php
$id = get_query_var( '_apollo_classified_id' );
$classified = get_classified(get_post( $id ));
if ( ! $classified->post ) {
    echo Apollo_App::getTemplatePartCustom('templates/404-print');
    exit;
}
$data = $classified->getDataDetail();
$logo =  Apollo_App::getHeaderLogo(Apollo_DB_Schema::_HEADER );
$str_address_all = isset($data['str_address'])?$data['str_address']:'';
$arrCoor = Apollo_Google_Coor_Cache::getCoorForVenue($str_address_all);
if(!is_array($arrCoor) || empty($arrCoor)){
    $arrCoor = Apollo_Google_Coor_Cache::getCoordinateByAddressInLocalDB($str_address_all);
}
$sCoor = $arrCoor ? implode(",", $arrCoor) : '';
$browser_key = of_get_option(Apollo_DB_Schema::_GOOGLE_API_KEY_BROWSER);
$_arr_field = array(
    Apollo_DB_Schema::_CLASSIFIED_ADDRESS,
    Apollo_DB_Schema::_CLASSIFIED_CITY,
    Apollo_DB_Schema::_CLASSIFIED_STATE,
);
?>

<html>
<head><title><?php _e(_e('Print detail classified | ', 'apollo' ) . $classified->get_title(), 'apollo' ) ?></title>
    <?php include APOLLO_TEMPLATES_DIR. '/partials/no-index.php' ?>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css"/>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/print.css" type="text/css"/>
    <style>
        <?php echo Apollo_App::getPrimarycolorCSSContent() ?>
    </style>

    <?php if($arrCoor === false): ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <?php endif; ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $browser_key ?>"></script>
</head>
<body id="artist-print-page">
<div class="top">
    <div class="top-head">
        <div class="logo print-page"><a href="<?php echo home_url() ?>"> <img src="<?php echo $logo; ?>"/></a></div>
    </div>
</div>
<div class="wrapper">
    <div class="block-detail">
        <div class="pic art"><a><?php echo $classified->get_image( 'medium' ); ?></a></div>
        <div class="artist-des">
            <h2 class="tt"><?php  echo $classified->get_title() ?></h2>
            
            <?php
                if ( $data['org'] ):
            ?>
            <p style="margin-top:5px" class="meta auth"><?php _e('Posted by', 'apollo') ?> <a href="<?php echo get_permalink($data['org']->ID) ?>" target="_blank"> <?php echo $data['org']->post_title ?></a></p>
            <div class="pt-date"><span><?php _e('Posted:', 'apollo') ?> <?php echo date('M, d, Y ', strtotime($data['org']->post_date)) ?></span></div>
            <p><?php echo $classified->generate_categories(true) ?></p>
            <?php
            endif;
            
            if(isset($data['web']) && $data['web']){
                ?>
                <p> <i class="fa fa-link fa-lg"></i><span><?php _e('Website:','apollo') ?>
                        <a href="#" target="blank"><?php echo $data['web']; ?></a></span>
                </p>
            <?php
            }
            ?>
            <?php
            if(isset($data['phone']) && $data['phone']){
                ?>
                <p> <i class="fa fa-phone fa-lg">&nbsp;</i><span><?php echo $data['phone'] ?></span></p>
            <?php
            }
            ?>
            <?php
            if(isset($data['fax']) && $data['fax']){
                ?>
                <p> <i class="fa fa-fax fa-lg">&nbsp;</i><span><?php echo $data['fax'] ?></span></p>
            <?php
            }
            ?>
            <?php
            if(isset($data['address']) && $data['address']){
                ?>
                <p> <i class="fa fa-map-marker fa-lg">&nbsp;</i><span><?php echo $data['address'] ?></span></p>
            <?php
            }
            ?>
        </div>
        <div class="desc">
            <?php
            $content = $classified->get_full_content();
            echo Apollo_App::convertContentEditorToHtml($content);
            ?>
        </div>
    </div>
    
    <div class="h-line"></div>
    
    <?php  $classified->renderAddFieldPrintDetailPage($data['add_fields']); ?>
   
</body>

<style>
    .logo.print-page img{
        max-height:81px;
    }

</style>
</html>