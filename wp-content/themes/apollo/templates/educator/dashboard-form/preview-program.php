<?php
/** @Ticket #18432 0002504: Arts Education Customizations - Add two steps: Preview and 'Success' page when submitted program form - item 3 */
$programId = intval(get_query_var('_apollo_program_id'));

$submit_obj = new Apollo_Submit_Program();
if (isset($_POST['case_action']) && $_POST['case_action'] == 'apl_submit_program' && !empty($programId)) {
    $submit_obj->submit_program();
}

if (empty($programId)) {
    wp_redirect(home_url('user/education/add-program'));
}

$program = get_program($programId);
$pageId = Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_SUBMIT_PROGRAM_PREVIEW);
$previewPage = '';
if (!empty($pageId)) {
    $previewPage = get_post($pageId);
}

?>
<div class="dsb-ct apl-preview-program" data-status="<?php echo $program->post->post_status; ?>" data-message-warning='<?php _e('IMPORTANT! \r\n Your submission is not complete. Please click on the "Submit Listing" button below to complete your program submission.', 'apollo');?>'>
    <div class="dsb-welc">
        <div class="events step-3">
            <div class="evt-blk">
                <div class="event-tt"><?php _e('PREVIEW PROGRAM', 'apollo') ?></div>
                <?php
                if ($program->post->post_status == 'unconfirmed') : ?>
                <div class="apl-program-warning-message">
                    <p>
                        <?php _e('IMPORTANT! - Your submission is not complete. Please click on the "Submit Listing" button below to complete your program submission.', 'apollo'); ?>
                    </p>
                </div>
                <?php endif; ?>
            </div>
            <?php if (!empty($previewPage->post_content)): ?>
                <div class="evt-blk apl-internal-content">
                    <?php echo Apollo_App::convertContentEditorToHtml($previewPage->post_content); ?>
                </div>
            <?php endif; ?>
            <div class="evt-blk preview-program-content">
                <?php
                include_once APOLLO_TEMPLATES_DIR . '/educator/dashboard-form/preview-program/general.php';
                include_once APOLLO_TEMPLATES_DIR . '/educator/dashboard-form/preview-program/dates.php';
                include_once APOLLO_TEMPLATES_DIR . '/educator/dashboard-form/preview-program/contact.php';
                include_once APOLLO_TEMPLATES_DIR . '/educator/dashboard-form/preview-program/detail.php';
                ?>

                <div class="evt-blk custom-field">
                    <?php $program->render_cf_detail(); ?>
                </div>

                <?php
                include_once APOLLO_TEMPLATES_DIR . '/educator/dashboard-form/preview-program/related-materials.php';
                include_once APOLLO_TEMPLATES_DIR . '/educator/dashboard-form/preview-program/references.php';
                include_once APOLLO_TEMPLATES_DIR . '/educator/dashboard-form/preview-program/cancel-policy.php';
                ?>

            </div>
            <div class="evt-blk">
                <form action="" method="post" id="apl-submit-program" name="apl_submit_program"
                      class="admin_form" enctype="multipart/form-data">
                    <?php wp_nonce_field($submit_obj->nonceAction, $submit_obj->nonceName, false); ?>
                    <input type="hidden" value="" name="case_action" id="case_action"/>
                    <a href="javascript:apollo_submit_form( 'apl_submit_program', 'apl_submit_program' )" class="apl-program-submit-form">
                        <div id="event-submit-btn"><span><?php _e('SUBMIT LISTING', 'apollo') ?></span></div>
                    </a>
                </form>

                <a href="<?php echo home_url("user/education/add-program/$programId") ?>" class="apl-program-return-to-edit">
                    <div id="arrow-3"><span><?php _e('RETURN TO EDIT', 'apollo') ?></span></div>
                </a>

            </div>
        </div>
    </div>
</div>