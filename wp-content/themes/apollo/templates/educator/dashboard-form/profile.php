<?php
//check is_active module here
if( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EDUCATION)){
    //load page content here
    $post = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_EDUCATION_PROFILE));


    $formEdu = new Apollo_Educator_Form(
        'Education-frm', //form Id
        'Education-frm-name', //form name
        'POST', //form method
        str_replace('?warning', '', $_SERVER['REQUEST_URI']), //action url
        array(), //external form elements
        $_SERVER['REQUEST_METHOD'] //current request method
    );
    //set process and validate class for this form
    $formEdu->setOptionValidateClass('Apollo_Submit_Educator');
    $formEdu->setFormDescription('<p>'.$post->post_content.'</p>');
    $formEdu->renderForm();
}
