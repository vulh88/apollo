<?php
$reference = $program->get_meta_data(Apollo_DB_Schema::_APL_PROGRAM_REFERENCES, Apollo_DB_Schema::_APL_PROGRAM_DATA);
$referenceFiles = $program->get_meta_data('upload_pdf_references');
if ($referenceFiles || $reference): ?>
    <div class="evt-blk program-upload-file">
        <div class="tt-bar clearfix">
            <div class="cl"><a><i class="fa fa-file fa-3x"></i></a>
                <h4><?php _e('REFERENCES', 'apollo') ?></h4>
            </div>
        </div>

        <div class="information">
            <div class="apl-internal-content"><?php echo Apollo_App::convertContentEditorToHtml($reference) ?></div>
            <?php if ($referenceFiles): ?>
                <?php $program->getAttachFiles($referenceFiles) ?>
            <?php endif; ?>
        </div>
    </div>
<?php endif ?>