<?php
$prog = $program;
$artistic_discipline_label = of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_ARTISTIC_DISCIPLINE_LABEL, 'Artistic Discipline');
$_arr_prog_type = array(
    'program-type' => array('label' => __('Program type:', 'apollo')),
    'artistic-discipline' => array('label' => __($artistic_discipline_label . ':', 'apollo')),
    'subject' => array('label' => __('Subject:', 'apollo')),
    'population-served' => array('label' => __('Population served:', 'apollo'))

);

if (of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_CULTURAL_ORIGIN_DROP, 1)) {
    $_arr_prog_type['cultural-origin'] = array('label' => __('Cultural Origin:', 'apollo'));
}

?>

<div class="evt-blk program-detail">
    <div class="tt-bar clearfix">
        <div class="cl"><a><i class="fa fa-info-circle fa-3x"></i></a>
            <h4><?php _e('Program Detail', 'apollo') ?></h4>
        </div>
    </div>

    <div class="information">
        <?php
        foreach ($_arr_prog_type as $k => $v):
            if ($k == 'population-served') {
                $tax_name = $prog->get_population_service_str();
            } else {
                $tax_name = $prog->generate_type_string(wp_get_post_terms($prog->id, $k), ', ');
            }

            if (!$tax_name) continue;
            ?>
            <div class="item"><span><?php echo $v['label'] ?> </span>
                <?php echo $tax_name ?>
            </div>
        <?php
        endforeach;
        ?>

        <?php
        $available_date = $prog->get_meta_data(Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_DATE, Apollo_DB_Schema::_APL_PROGRAM_DATA);
        $available_time = $prog->get_meta_data(Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_TIME, Apollo_DB_Schema::_APL_PROGRAM_DATA);
        $location = $prog->get_meta_data(Apollo_DB_Schema::_APL_PROGRAM_LOCATION, Apollo_DB_Schema::_APL_PROGRAM_DATA);
        $fee = $prog->get_meta_data(Apollo_DB_Schema::_APL_PROGRAM_FEE, Apollo_DB_Schema::_APL_PROGRAM_DATA);
        $core = $prog->get_meta_data(Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_CORE, Apollo_DB_Schema::_APL_PROGRAM_DATA);
        $essen = $prog->get_meta_data(Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_ESSENTIAL, Apollo_DB_Schema::_APL_PROGRAM_DATA);
        $space = $prog->get_meta_data(Apollo_DB_Schema::_APL_PROGRAM_SPACE_TECHNICAL, Apollo_DB_Schema::_APL_PROGRAM_DATA);

        $max_student = $prog->get_meta_data(Apollo_DB_Schema::_APL_PROGRAM_MAX_STUDENTS, Apollo_DB_Schema::_APL_PROGRAM_DATA);
        $prog_length_info = $prog->get_meta_data(Apollo_DB_Schema::_APL_PROGRAM_LENGTH_PROGRAM, Apollo_DB_Schema::_APL_PROGRAM_DATA);

        /** @Task #12782 */
        $bilingual_option = of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_BILINGUAL, true);
        $free_option = of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_FREE, true);

        ?>

        <?php if ($bilingual_option) : ?>
            <div class="item"><span><?php _e('Bilingual:', 'apollo') ?> </span>
                <?php echo $prog->get_yn(Apollo_DB_Schema::_APL_PROGRAM_IS_BILINGUAL) ?>
            </div>
        <?php endif; ?>

        <?php if ($free_option) : ?>
            <div class="item"><span><?php _e('Free:', 'apollo') ?> </span>
                <?php echo $prog->get_yn(Apollo_DB_Schema::_APL_PROGRAM_IS_FREE) ?>
            </div>
        <?php endif; ?>

        <?php if ($available_date): ?>
            <div class="item"><i
                        class="fa fa-calendar fa-2x"> </i><span>&nbsp;<a><?php _e('Available dates:', 'apollo') ?></a></span>
                <div class="avai-date apl-internal-content"><?php echo Apollo_App::convertContentEditorToHtml($available_date) ?></div>
            </div>
        <?php endif; ?>

        <?php if ($available_time): ?>
            <div class="item"><i
                        class="fa fa-clock-o fa-2x"> </i><span>&nbsp;<a><?php _e('Available times:', 'apollo') ?></a></span>
                <div class="avai-date apl-internal-content"><?php echo Apollo_App::convertContentEditorToHtml($available_time) ?></div>
            </div>
        <?php endif; ?>

        <?php if ($max_student): ?>
            <div class="item"><span><a><?php _e('Max number of students:', 'apollo') ?></a></span>
                <?php echo $max_student ?>
            </div>
        <?php endif; ?>

        <?php if ($prog_length_info): ?>
            <div class="item"><span><a><?php _e('Length of program:', 'apollo') ?></a></span>
                <?php echo $prog_length_info ?>
            </div>
        <?php endif; ?>

        <?php if ($space): ?>
            <div class="item"><span><a><?php _e('Space / technical requirements:', 'apollo') ?></a></span>
                <div class="apl-internal-content"><?php echo Apollo_App::convertContentEditorToHtml($space) ?></div>
            </div>
        <?php endif; ?>

        <?php if ($location): ?>
            <div class="item"><span><?php _e('Location(s):', 'apollo') ?></span>
                <div class="apl-internal-content"><?php echo Apollo_App::convertContentEditorToHtml($location) ?></div>
            </div>
        <?php endif; ?>

        <?php if ($fee): ?>
            <div class="item"><span><?php _e('Fees / Ticketing:', 'apollo') ?></span>
                <div class="apl-internal-content"><?php echo Apollo_App::convertContentEditorToHtml($fee) ?></div>
            </div>
        <?php endif; ?>

        <?php if ($core && of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_CORE_FIELD, 0)):
            $programCoreLabel = of_get_option(Apollo_DB_Schema::_EDUCATOR_PROGRAM_CORE_LABEL) ? of_get_option(Apollo_DB_Schema::_EDUCATOR_PROGRAM_CORE_LABEL) : 'Program Core'; ?>
            <div class="item"><span><?php _e($programCoreLabel . ' :', 'apollo') ?></span>
                <div class="apl-internal-content"><?php echo Apollo_App::convertContentEditorToHtml($core) ?></div>
            </div>
        <?php endif; ?>

        <?php if ($essen && of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_ESSENTIALS_FIELD, 0)):
            $programEssentialsLabel = of_get_option(Apollo_DB_Schema::_EDUCATOR_PROGRAM_ESSENTIALS_LABEL) ? of_get_option(Apollo_DB_Schema::_EDUCATOR_PROGRAM_ESSENTIALS_LABEL) : 'Program Essentials'; ?>
            <div class="item"><span><?php _e($programEssentialsLabel . ':', 'apollo') ?></span>
                <div class="apl-internal-content"><?php echo Apollo_App::convertContentEditorToHtml($essen) ?></div>
            </div>
        <?php endif; ?>
    </div>
</div>