<div class="evt-blk">
    <article class="category-itm">
        <div class="pic"><a
                    href="<?php echo $program->get_permalink(); ?>"><?php echo $program->get_image('thumbnail') ?></a>
        </div>
        <div class="category-t">
            <h2 class="category-ttl">
                <a href="<?php echo $program->get_permalink(); ?>">
                    <?php echo $program->get_title(); ?></a>
            </h2>
            <div class="desc apl-internal-content">
                <?php echo $program->get_full_content() ?>
            </div>
        </div>
    </article>
</div>