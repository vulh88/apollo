<?php
$startDate = get_apollo_meta($program->id, Apollo_DB_Schema::_APL_PROGRAM_STARTD, true);
$endDate = get_apollo_meta($program->id, Apollo_DB_Schema::_APL_PROGRAM_ENDD, true);
$expridate = get_apollo_meta($program->id, Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND, true);
?>
<div class="evt-blk">
    <div class="tt-bar clearfix">
        <div class="cl"><a><i class="fa fa-clock-o fa-3x"></i></a>
            <h4><?php _e('Dates', 'apollo') ?></h4>
        </div>
    </div>

    <div class="information dates">
        <p>
            <span><?php echo __('Start date: ', 'apollo') ?></span> <?php echo !empty($startDate) ? $startDate : '' ?>
        </p>
        <p>
            <span><?php echo __('End date: ', 'apollo') ?></span> <?php echo !empty($endDate) ? $endDate : '' ?>
        </p>
        <p>
            <span><?php echo __('Expiration date: ', 'apollo') ?></span> <?php echo !empty($expridate) ? $expridate : '' ?>
        </p>
    </div>
</div>