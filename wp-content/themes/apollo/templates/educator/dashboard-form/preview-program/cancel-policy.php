<?php
$cancel_policy = $program->get_meta_data(Apollo_DB_Schema::_APL_PROGRAM_CANCEL_POLICY, Apollo_DB_Schema::_APL_PROGRAM_DATA);

if ($cancel_policy):
    ?>
    <div class="evt-blk">
        <div class="tt-bar clearfix">
            <div class="cl"><a><i class="fa fa-ban fa-3x"></i></a>
                <h4><?php _e('Cancellation Policy', 'apollo') ?></h4>
            </div>
        </div>

        <div class="information dates apl-internal-content">
            <p>
                <?php echo Apollo_App::convertContentEditorToHtml($cancel_policy) ?>
            </p>
        </div>
    </div>
<?php endif; ?>