<?php
$meterials = $program->get_meta_data('upload_pdf_related_materials');
if ($meterials): ?>
    <div class="evt-blk program-upload-file">
        <div class="tt-bar clearfix">
            <div class="cl"><a><i class="fa fa-file fa-3x"></i></a>
                <h4><?php _e('RELATED MATERIALS DOCUMENTS', 'apollo') ?></h4>
            </div>
        </div>

        <div class="information">
            <?php $program->getAttachFiles($meterials) ?>
        </div>
    </div>
<?php endif ?>