<?php
$p_contact_name = $program->get_meta_data(Apollo_DB_Schema::_APL_PROGRAM_CNAME, Apollo_DB_Schema::_APL_PROGRAM_DATA);
$p_phone = $program->get_meta_data(Apollo_DB_Schema::_APL_PROGRAM_PHONE, Apollo_DB_Schema::_APL_PROGRAM_DATA);
$p_email = $program->get_meta_data(Apollo_DB_Schema::_APL_PROGRAM_EMAIL, Apollo_DB_Schema::_APL_PROGRAM_DATA);
$book_org = $program->get_meta_data(Apollo_DB_Schema::_APL_PROGRAM_URL, Apollo_DB_Schema::_APL_PROGRAM_DATA);
?>

<div class="evt-blk">
    <div class="tt-bar clearfix">
        <div class="cl"><a><i class="fa fa-address-book fa-3x"></i></a>
            <h4><?php _e('Booking / scheduling contact', 'apollo') ?></h4>
        </div>
    </div>

    <div class="information">
        <?php if ($p_contact_name): ?>
            <p><i class="fa fa-user fa-lg"></i><a class="vmore none-link"><?php echo $p_contact_name ?></a></p>
        <?php endif; ?>

        <?php if ($p_phone): ?>
            <p><i class="fa fa-phone fa-lg"></i><a class="vmore none-link"><?php echo $p_phone ?></a></p>
        <?php endif; ?>

        <?php if ($p_email): ?>
            <P><i class="fa fa-envelope fa-lg"></i><a href="mailto:<?php echo $p_email ?>"
                                                      class="vmore"><?php echo $p_email ?></a></P>
        <?php endif; ?>

        <?php if ($book_org): ?>
            <P><i class="fa fa-check-square fa-lg"></i><a target="_blank" href="<?php echo $book_org ?>"
                                                          class="vmore"><B><?php _e("BOOK THIS PROGRAM", 'apollo') ?></B></a>
            </P>
        <?php endif; ?>
    </div>
</div>