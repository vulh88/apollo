<?php
/** @Ticket #18432 - Add two steps: Preview and 'Success' page when submitted program form - item 3 */
$pageId = Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_SUBMIT_PROGRAM_SUCCESS);
$successPage = '';
if(!empty($pageId)){
    $successPage = get_post($pageId);
}

echo '<div class="evt-blk apl-internal-content">';
if(!empty($successPage->post_content)){
    echo Apollo_App::convertContentEditorToHtml($successPage->post_content);
}
echo '</div>';