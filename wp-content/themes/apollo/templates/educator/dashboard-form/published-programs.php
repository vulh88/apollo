<?php


// PAGE: DASHBOARD: EVENTS
include_once APOLLO_TEMPLATES_DIR.'/educator/list-programs.php';

$pageSize = Apollo_Display_Config::PUBLISHED_PROGRAMS;

/**
 * @ticket #19655: [CF] 20190403 - FE Program dashboard - Add Copy/Delete Buttons
 */
if (!empty($_POST['program_id']) && !empty($_POST['case_action']) && $_POST['case_action'] == 'delete_program'){
    $submitProgram = new Apollo_Submit_Program();
    if ( $submitProgram->deleteProgram($_POST['program_id']) ) {
        $msg = __('Delete program successfully', 'apollo');
    };
}

$list_programs = new List_Program_Adapter(1);
$upcomming_programs = $list_programs->get_upcomming_programs('', " LIMIT $pageSize ", ' ORDER BY post_title ASC');
$upProgramTotal = $list_programs->totalPost;

$past_events = $list_programs->get_expired_programs('', " LIMIT $pageSize ", ' ORDER BY post_title ASC');
$pastProgramTotal = $list_programs->totalPost;

?>

<div class="dsb-welc evt-dashboard">
    <div class="bookmark">
        <h1><?php _e('Program', 'apollo') ?></h1>

        <p><?php _e('Please select one of the following options:', 'apollo') ?></p>

        <div class="wc-l account-listing">
            <?php if (isset($msg)):?>
                <div class="_apollo_success"><i class="fa fa-check"></i> <?php echo $msg ?></div>
            <?php endif; ?>

            <nav class="nav-tab">
                <ul class="tab-list">
                    <li><a href="#" data-id="current-up-events"><?php _e('Current/Upcoming Programs', 'apollo') ?></a></li>
                    <li><a href="#" data-id="past-events"><?php _e('Expired Programs', 'apollo') ?></a></li>
                </ul>
            </nav>

            <div class="tab-bt tab-bt-custom"></div>

            <div data-target="current-up-events" class="blog-blk">

                <div class="blk-bm-events" id="_apol_upcomming_program">
                    <nav class="events">
                        <?php
                        $template = APOLLO_TEMPLATES_DIR. '/pages/dashboard/html/partial/_program/program_published.php';
                        echo APL::renderTemplateWithData(array('datas' => $upcomming_programs), $template);
                        ?>
                    </nav>
                </div>
                <div class="blk-paging"></div>
                <script type="text/javascript">
                    var APL_DB_INLINE_CUSTOM = APL_DB_INLINE_CUSTOM || {};
                    APL_DB_INLINE_CUSTOM['items_upcomming_program'] = <?php echo $upProgramTotal ?>;
                    APL_DB_INLINE_CUSTOM['itemsOnPage'] = <?php echo $pageSize ?>;
                    APL_DB_INLINE_CUSTOM['baselink'] = '<?php echo admin_url('admin-ajax.php') ?>';
                </script>
            </div>

            <div data-target="past-events" class="blog-blk">
                <div class="blk-bm-events" id="_apol_expired_program">
                    <nav class="events">
                        <?php
                        $template = APOLLO_TEMPLATES_DIR. '/pages/dashboard/html/partial/_program/program_published.php';
                        echo APL::renderTemplateWithData(array('datas' => $past_events), $template);
                        ?>
                    </nav>
                </div>
                <div class="blk-paging"></div>
                <script type="text/javascript">
                    var APL_DB_INLINE_CUSTOM = APL_DB_INLINE_CUSTOM || {};
                    APL_DB_INLINE_CUSTOM['items_expired_program'] = <?php echo $pastProgramTotal ?>;
                    APL_DB_INLINE_CUSTOM['itemsOnPage'] = <?php echo $pageSize ?>;
                    APL_DB_INLINE_CUSTOM['baselink'] = '<?php echo admin_url('admin-ajax.php') ?>';
                </script>
            </div>
        </div>

    </div>
</div>

