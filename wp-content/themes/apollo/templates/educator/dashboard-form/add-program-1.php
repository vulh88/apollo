<?php
//check is_active module here
if( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EDUCATION )){
    //load page content here
    $post = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ADD_PROGRAM));
    ?>
    <div class="dsb-ct" id="add-program">
        <?php
        $formProgram = new Apollo_Program_Form(
            'program-frm', //form Id
            'program-frm-name', //form name
            'POST', //form method
            $_SERVER['REQUEST_URI'], //action url
            array(), //external form elements
            $_SERVER['REQUEST_METHOD'] //current request method
        );
        //set process and validate class for this form
        $formProgram->setOptionValidateClass('Apollo_Submit_Program');
        $formProgram->setFormDescription('<p>'.$post->post_content.'</p>');
        $formProgram->renderForm();
        ?>
    </div>
<?php } ?>
<?php include APOLLO_TEMPLATES_DIR. '/pages/dashboard/html/partial/_common/video-box.php'; ?>
