<?php

return <<<EOF
    <li>
        <div class="event-img"><a href="{$_object->get_permalink()}">{$_object->get_image()}</a></div>
        <div class="event-checkbox">
            <input type="checkbox" name="eventid[]" value="{$_object->id}" class="check-event"><span class="event"></span>
        </div>
        <div class="event-info">
            <a href="{$_object->get_permalink()}">
                <span class="ev-tt">{$_object->get_title()}</span> </a>
        </div>
    </li>
                            
EOF;
?>
                       