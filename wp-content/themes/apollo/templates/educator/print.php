<?php 
    $id = get_query_var( '_apollo_educator_id' );
    $educator = get_educator(get_post( $id ));
    if ( ! $educator->post ) {
        echo Apollo_App::getTemplatePartCustom('templates/404-print');
        exit;
    }
    $logo =  Apollo_App::getHeaderLogo(Apollo_DB_Schema::_HEADER );
    $logo = is_array($logo) ? $logo : array($logo);
?>
<html>
  <head><title><?php _e( _e( 'Print detail educator | ', 'apollo' ) . $educator->get_title(), 'apollo' ) ?></title>
    <?php include APOLLO_TEMPLATES_DIR. '/partials/no-index.php' ?>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css"/>
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"/>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/print.css" type="text/css"/>
        <style>
            <?php echo Apollo_App::getPrimarycolorCSSContent() ?>
            .des-print .item a {
                text-decoration: none;
                color: #666;
            }
            .des-print .item a:hover {
                text-decoration: underline;
            }
        </style>
  </head>
  <body>
    <div class="top">
      <div class="top-head">
          <div class="logo print-page">
              <?php foreach ($logo as $item) :
                  if (!empty($item)):?>
                      <a href="<?php echo home_url() ?>"> <img src="<?php echo $item; ?>"/></a>
                  <?php endif; ?>
              <?php endforeach; ?>
          </div>
      </div>
    </div>
      
    <div class="wrapper">
      <div class="block-detail">
        <div class="pic art"><a><?php echo $educator->get_image( 'medium' ); ?></a></div>
        <div class="artist-des">
          <h2 class="tt"><?php echo !empty($educator->post->post_title) ? $educator->post->post_title : ''; ?></h2>
          <div class="desc">
            <p>
              <?php echo $educator->get_full_content() ?>
            </p>
          </div>
        </div>
      </div>
        
        <?php 
            $dkey = Apollo_DB_Schema::_APL_EDUCATOR_DATA;
            $email = $educator->get_meta_data( Apollo_DB_Schema::_APL_EDUCATOR_EMAIL, $dkey );
            $url = $educator->get_meta_data( Apollo_DB_Schema::_APL_EDUCATOR_URL, $dkey );
            $blog = $educator->get_meta_data( Apollo_DB_Schema::_APL_EDUCATOR_BLOG, $dkey );
            $fb = $educator->get_meta_data( Apollo_DB_Schema::_APL_EDUCATOR_FB, $dkey );
            $tw = $educator->get_meta_data( Apollo_DB_Schema::_APL_EDUCATOR_TW, $dkey );
            $ins = $educator->get_meta_data( Apollo_DB_Schema::_APL_EDUCATOR_INS, $dkey );
            $lk = $educator->get_meta_data( Apollo_DB_Schema::_APL_EDUCATOR_LK, $dkey );
            $phone = $educator->get_meta_data( Apollo_DB_Schema::_APL_EDUCATOR_PHONE1, $dkey );
            $fax = $educator->get_meta_data( Apollo_DB_Schema::_APL_EDUCATOR_FAX, $dkey );
            $address = $educator->get_full_address();
            $county = $educator->get_meta_data( Apollo_DB_Schema::_APL_EDUCATOR_COUNTY, $dkey );

            if ( $email || $url || $blog || $fb || $tw || $ins || $lk || $address || $county ):
        ?>  
        <div class="block-detail"><i class="fa fa-info-circle fa-2x"></i><span class="pg-tt"><?php _e( 'EDUCATOR CONTACT INFO', 'apollo' ) ?></span>
            <div class="des-print">
                <?php if ( $email ): ?>
                <p> <i class="fa fa-envelope fa-lg"></i><span><?php echo $email ?></span></p>
                <?php endif; ?>
                <?php if ( $url ): ?>
                <p> <i class="fa fa-link fa-lg"></i><span><?php echo $url ?></span></p>
                <?php endif; ?>
                <?php if ( $blog ): ?>
                <p> <i class="fa fa-star fa-lg"></i><span><?php echo $blog ?></span></p>
                <?php endif; ?>
                <?php if ( $fb ): ?>
                <p> <i class="fa fa-facebook  fa-lg">&nbsp;</i><span><?php echo $fb ?></span></p>
                <?php endif; ?>
                <?php if ( $tw ): ?>
                <p> <i class="fa fa-twitter fa-lg"></i><span><?php echo $tw ?></span></p>
                <?php endif; ?>
                <?php if ( $ins ): ?>
                <p> <i class="fa fa-camera-retro fa-lg"></i><span><?php echo $ins ?></span></p>
                <?php endif; ?>
                <?php if ( $lk ): ?>
                <p> <i class="fa fa-linkedin-square fa-lg"></i><span> <?php echo $lk ?></span></p>
                <?php endif; ?>
                <?php if ( $address ): ?>
                    <p> <i class="fa fa-map-marker fa-lg">&nbsp;&nbsp;</i><span><?php echo $address ?></span></p>
                <?php endif; ?>
                <?php if ( $county ): ?>
                    <p> <i class="fa fa-university fa-lg"></i><span><?php echo $county ?></span></p>
                <?php endif; ?>
                <?php if ( $phone ): ?>
                    <p> <i class="fa fa-phone fa-lg"></i><span><?php echo $phone ?></span></p>
                <?php endif; ?>
                <?php if ( $fax ): ?>
                    <p><i class="fa fa-fax fa-lg"></i><?php echo $fax ?></p>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>
        
        <?php
            // Get program
        $program_id = get_query_var( '_apollo_program_id' );
        $prog = get_program($program_id);
        if ( $prog->post ):
        ?>
        
        <?php $content = $prog->get_full_content();
        if ( $content ): ?>
        <div class="block-detail">
            <h2 class="tt"><?php echo $prog->get_title() ?></h2>
            <i class="fa fa-play fa-lg"></i><span class="pg-tt"><?php _e( 'Program description', 'apollo' ) ?></span>
            <div class="des-print">
              <p><?php echo $content ?></p>
            </div>
        </div>
        <?php endif; ?>
        
        <?php 
            $pd_key = Apollo_DB_Schema::_APL_PROGRAM_DATA;
            $phone = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_PHONE, $pd_key );
            $email = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_EMAIL , $pd_key );
            $contact_name = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_CNAME, $pd_key );

            if ( $phone || $email ):
        ?>
        <div class="block-detail"><i class="fa fa-play fa-lg"></i><span class="pg-tt"><?php _e( 'Booking / scheduling contact', 'apollo' ) ?></span>
          <div class="des-print">
              <?php if ( $contact_name ): ?>
                      <p><i class="fa fa-user fa-lg"></i><?php echo $contact_name ?></p>
              <?php endif; ?>
              <?php if ( $phone ): ?>
                  <p> <span><i class="fa fa-phone fa-lg">&nbsp;&nbsp;&nbsp;</i><?php echo $phone ?></span></p>
              <?php endif; ?>
              <?php if ( $email ): ?>
                  <P><i class="fa fa-envelope fa-lg">&nbsp;</i><span><?php echo $email ?></span></P>
              <?php endif; ?>
          </div>
        </div>
        <?php endif; ?>
        
        <?php
        $_arr_prog_type = array(
            'artistic-discipline'  => array( 'label'   => __( 'Artistic discipline:', 'apollo' ) ),
            'cultural-origin'  => array( 'label'   => __( 'Cultural Origin:', 'apollo' ) ),   
            'program-type'  => array( 'label'   => __( 'Program type:', 'apollo' ) ),
            'population-served'  => array( 'label'   => __( 'Population served:', 'apollo' ) ),
            'subject'  => array( 'label'   => __( 'Subject:', 'apollo' ) ),
        );
        ?>
        <div class="block-detail"><i class="fa fa-play fa-lg"></i><span class="pg-tt"><?php _e( 'Program detail', 'apollo' ) ?></span>
            <div class="des-print">
                <?php 
                    foreach ( $_arr_prog_type as $k => $v ):
                        if ( $k == 'population-served' ) {
                            $tax_name = $prog->get_population_service_str();
                        } else {
                            $tax_name = $prog->generate_type_string( wp_get_post_terms( $program_id, $k ), ', ' );
                        }
                ?>
                <div class="item"><span><?php echo $v['label'] ?> </span><?php echo $tax_name ?></div>
                <?php
                    endforeach;
                ?>

                <?php 
                    $available_date = strip_tags($prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_DATE, Apollo_DB_Schema::_APL_PROGRAM_DATA ));
                    $location = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_LOCATION, Apollo_DB_Schema::_APL_PROGRAM_DATA );
                    $available_time = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_TIME, Apollo_DB_Schema::_APL_PROGRAM_DATA );
                    $fee = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_FEE, Apollo_DB_Schema::_APL_PROGRAM_DATA );
                    $max_student = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_MAX_STUDENTS, Apollo_DB_Schema::_APL_PROGRAM_DATA );
                    $prog_length_info = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_LENGTH_PROGRAM, Apollo_DB_Schema::_APL_PROGRAM_DATA );
                    $space = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_SPACE_TECHNICAL, Apollo_DB_Schema::_APL_PROGRAM_DATA );
                    $core = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_CORE, Apollo_DB_Schema::_APL_PROGRAM_DATA );
                    $essen = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_ESSENTIAL, Apollo_DB_Schema::_APL_PROGRAM_DATA );

                ?>
                <div class="item"><span><?php _e( 'Bilingual:', 'apollo' ) ?> </span>
                    <?php echo $prog->get_yn( Apollo_DB_Schema::_APL_PROGRAM_IS_BILINGUAL ) ?>
                </div>
                
                <?php if ( $available_date ): ?>
                <div class="item"><span><?php _e( 'Available dates:', 'apollo' ) ?></span>
                    <p class="avai-date"><?php echo Apollo_App::convertContentEditorToHtml($available_date); ?></p>
                </div>
                <?php endif; ?>

                <?php if ( $available_time ): ?>
                    <div class="item"><span><?php _e( 'Available times:', 'apollo' ) ?></span>
                        <p class="avai-date"><?php echo Apollo_App::convertContentEditorToHtml($available_time); ?></p>
                    </div>
                <?php endif; ?>

                <?php if ( $max_student ): ?>
                    <div class="item"><span><a><?php _e( 'Max number of students:', 'apollo' ) ?></a></span>
                        <?php echo $max_student ?>
                    </div>
                <?php endif; ?>

                <?php if ( $prog_length_info ): ?>
                    <div class="item"><span><a><?php _e( 'Length of program:', 'apollo' ) ?></a></span>
                        <?php echo $prog_length_info ?>
                    </div>
                <?php endif; ?>

                <?php if ( $space ): ?>
                    <div class="item"><span><a><?php _e( 'Space / technical requirements:', 'apollo' ) ?></a></span>
                        <p><?php echo Apollo_App::convertContentEditorToHtml($space) ?></p>
                    </div>
                <?php endif; ?>
                
                <?php if ( $location ): ?>
                <div class="item"><span><?php _e( 'Location(s):', 'apollo' ) ?></span>
                    <p><?php echo  Apollo_App::convertContentEditorToHtml($location) ?></p>
                </div>
                <?php endif; ?>

                <?php if ( $fee ): ?>
                    <div class="item"><span><?php _e( 'Fees / Ticketing:', 'apollo' ) ?></span>
                        <div class="apl-internal-content-"><?php echo Apollo_App::convertContentEditorToHtml($fee) ?></div>
                    </div>
                <?php endif; ?>

                <?php if ( $core ):
                    if(of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_CORE_FIELD)) {
                        $programCoreLabel = of_get_option(Apollo_DB_Schema::_EDUCATOR_PROGRAM_CORE_LABEL, __('Program Core', 'apollo')); ?>
                        <div class="item"><span><?php _e( $programCoreLabel.' :', 'apollo' ) ?></span>
                            <div class="apl-internal-content"><?php echo Apollo_App::convertContentEditorToHtml($core) ?></div>
                        </div>
                    <?php } endif; ?>

                <?php if ( $essen ):
                    if(of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_ESSENTIALS_FIELD)) {
                        $programEssentialsLabel = of_get_option(Apollo_DB_Schema::_EDUCATOR_PROGRAM_ESSENTIALS_LABEL, __('Program Essentials', 'apollo')); ?>
                        <div class="item"><span><?php _e( $programEssentialsLabel.':', 'apollo' ) ?></span>
                            <div class="apl-internal-content"><?php echo Apollo_App::convertContentEditorToHtml($essen) ?></div>
                        </div>
                    <?php } endif; ?>
                
            </div>
        </div>

        <?php $prog->render_cf_detail(); ?>
        <?php $educator->render_cf_detail(); ?>

        <?php
        $cancel_policy = $prog->get_meta_data(Apollo_DB_Schema::_APL_PROGRAM_CANCEL_POLICY, Apollo_DB_Schema::_APL_PROGRAM_DATA);
        if ($cancel_policy): ?>
            <div class="block-detail">
                <i class="fa fa-play fa-lg"></i><span
                        class="pg-tt"><?php _e('Cancellation Policy', 'apollo') ?></span>
                <div class="des-print"><?php echo Apollo_App::convertContentEditorToHtml($cancel_policy); ?></div>
            </div>
        <?php endif; ?>

        <?php endif; ?>
        
    </div>
  </body>
  <style>
      .logo.print-page img{
          max-height:81px;
      }
  </style>
</html>