<?php 
    if ( $results = $this->get_results() ) {
        foreach ( $results as $p ): 
            
            $e = get_educator( $p );
            $summary = $e->get_summary();
        ?>
        <li>
            <div class="div-one" data-url="<?php echo $e->get_permalink() ?>" data-type="link">
                <div class="search-img"><a href="<?php echo $e->get_permalink() ?>"><?php echo $e->get_image( 'medium', array(),
                            array(
                                'aw' => true,
                                'ah' => true,
                            ),
                            'normal'
                        ) ?></a></div>
                <div class="search-info">
                    <a href="<?php echo $e->get_permalink() ?>"><span class="ev-tt" data-n="2"><?php echo $e->get_title() ?></span></a>
                    <div class="s-desc" data-ride="ap-clamp" data-n="9"><?php echo $summary['text']  ?></div>
                </div>
            </div>
        </li>
        
<?php endforeach;

} else {
    _e( 'No results', 'apollo' );
} ?>        
