<?php

/**
 * @page: content current page
 */
class List_Program_Adapter {

    public $program;
	public $pageSize;
	public $page;
	public $totalPost;
    
    public $_order_by;
    public $_order = 'ASC';
    public $_field_order;
    public $_template;
    public $displayed_programs = array();
    public $is_topten = false;


    public function __construct($page, $pageSize = Apollo_Display_Config::PAGE_SIZE ) {
       
        // Check module is avaiable
        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_PROGRAM_PT ) ) {
            return false;
        }

		if(empty($pageSize)) {
			$pageSize = Apollo_Display_Config::PAGE_SIZE;
		}
        
		$this->pageSize = intval( $pageSize );
		$this->page = $page;
	}


    
    public function render_html( $template = '' ) {
        ob_start();
        include $template ? $template : $this->_template;
        return ob_get_clean();
    }
    
	public function getProgram() {
		return $this->program;
	}
    
	public function isEmpty() {
		return empty($this->program);
	}


	public function isShowMore() {
		$current_pos = ($this->page * $this->pageSize);
		return $current_pos < $this->totalPost;
	}

    public function get_upcomming_programs( $user_id = '', $limit = false, $orderby = '' ){

        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EDUCATION ) ) {
            return false;
        }
        global $wpdb, $post;
        $apollo_meta_table   = $wpdb->apollo_programmeta;
        $_expired_date_field  = Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND;
        $_current_date      = current_time( 'Y-m-d' );

        $educatorID = Apollo_User::getCurrentAssociatedID(Apollo_DB_Schema::_EDUCATOR_PT);
        $progEduTbl = $wpdb->{Apollo_Tables::_APL_PROGRAM_EDUCATOR};
        $arr_order = Apollo_Sort_Manager::getSortOneFieldForPostType('program');
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.* FROM {$wpdb->posts} p
            LEFT JOIN {$apollo_meta_table} mt_expired_d ON p.ID = mt_expired_d.apollo_program_id AND mt_expired_d.meta_key = '". Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND ."'
            INNER JOIN {$progEduTbl} pe ON pe.prog_id = p.ID AND pe.edu_id = '".$educatorID."'
            ";


        $sql .=   " WHERE 1=1 AND p.post_status = 'publish' AND p.post_type = '". Apollo_DB_Schema::_PROGRAM_PT ."'
            AND  ( CAST( mt_expired_d.meta_value AS DATE )  >= '{$_current_date}' OR mt_expired_d.meta_value = '' OR mt_expired_d.meta_value IS NULL )
        ";

        $sql .= " GROUP BY p.ID ";
        if($arr_order['type_sort'] === 'post') {
            $sql .= " ORDER BY p.{$arr_order['order_by']} {$arr_order['order']} ";
        }

        $sql .= " $limit ";
        $this->program   = $wpdb->get_results($sql);
        $this->totalPost    = $wpdb->get_var("SELECT FOUND_ROWS()");

        return $this->program;
    }

    public function get_expired_programs( $user_id = '', $limit = false, $orderby = '' ){

        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EDUCATION ) ) {
            return false;
        }
        global $wpdb, $post;
        $apollo_meta_table   = $wpdb->apollo_programmeta;
        $_expired_date_field  = Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND;
        $_current_date      = current_time( 'Y-m-d' );

        $educatorID = Apollo_User::getCurrentAssociatedID(Apollo_DB_Schema::_EDUCATOR_PT);
        $progEduTbl = $wpdb->{Apollo_Tables::_APL_PROGRAM_EDUCATOR};
        $arr_order = Apollo_Sort_Manager::getSortOneFieldForPostType('program');
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.* FROM {$wpdb->posts} p
            INNER JOIN {$apollo_meta_table} mt_expired_d ON p.ID = mt_expired_d.apollo_program_id AND mt_expired_d.meta_key = '". Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND ."'
            INNER JOIN {$progEduTbl} pe ON pe.prog_id = p.ID AND pe.edu_id = '".$educatorID."'
            ";

        $sql .=   " WHERE 1=1 AND p.post_status = 'publish' AND p.post_type = '". Apollo_DB_Schema::_PROGRAM_PT ."' AND  CAST( mt_expired_d.meta_value AS DATE )  < '{$_current_date}' ";
        $sql .= " GROUP BY p.ID ";
        if($arr_order['type_sort'] === 'post') {
            $sql .= " ORDER BY p.{$arr_order['order_by']} {$arr_order['order']} ";
        }
        $sql .= " $limit ";
        $this->program   = $wpdb->get_results($sql);
        $this->totalPost    = $wpdb->get_var("SELECT FOUND_ROWS()");

        return $this->program;
    }

}
