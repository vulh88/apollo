<?php
    $venue = get_venue($post);
    $data = $venue->getVenueData();
    $allow_comment = of_get_option(Apollo_DB_Schema::_ENABLE_COMMENT,1) && comments_open();
    $addItButton = $venue->renderBookmarkBtn( array( 'class' => 'btn btn-b' ) );
?>

<div class="breadcrumbs">
    <ul class="nav">
        <ul class="nav">
            <li><a href="<?php echo home_url() ?>"><?php _e( 'Home', 'apollo' ) ?></a></li>
            <li><a href="<?php echo Apollo_App::getCustomUrlByModuleName('venue') ?>"><?php echo Apollo_App::getCustomLabelByModuleName('venue') ?></a></li>
            <li> <span><?php echo $venue->get_title() ?></span></li>
        </ul>
    </ul>
</div>
<?php
Apollo_Next_Prev::getNavLink($venue->id,$post->post_type);
?>
<?php
include( APOLLO_TEMPLATES_DIR. '/content-single/venue/general.php');
//location
include( APOLLO_TEMPLATES_DIR. '/content-single/venue/location.php');
//accessibility information
include( APOLLO_TEMPLATES_DIR. '/content-single/venue/accessibility-infomation.php');
//parking info + PUBLIC HOURS + PUBLIC ADMISSION FEES
include( APOLLO_TEMPLATES_DIR. '/content-single/venue/parking-info.php');

include( APOLLO_TEMPLATES_DIR. '/content-single/venue/list-event.php');
include( APOLLO_TEMPLATES_DIR. '/content-single/venue/add-fields.php');
include( APOLLO_TEMPLATES_DIR. '/content-single/venue/media.php');

/**
 * @ticket #18657: Change the layout structure for Artist, Organization, Venue
 */
$contactTemplate = apply_filters('oc_venue_add_contact_info', '');
if($contactTemplate){
    include_once $contactTemplate;
}

/** @Ticket #13250 */
include APOLLO_TEMPLATES_DIR. '/content-single/common/jetpack-related-posts.php';

?>

<div class="blog-bkl astro-featr">
    <div class="a-block" id="event_comment_block">
        <?php
        // If comments are open or we have at least one comment, load up the comment template.
        if ( $allow_comment ) {
            ?>
            <?php echo Apollo_App::renderSectionLabels(__('COMMENTS', 'apollo'), '<span> <i class="fa fa-comment"></i></span>'); ?>
            <div class="a-block-ct">
                <?php
                comments_template('/templates/comments.php');
                ?>
            </div>
        <?php
        }
        ?>
    </div><!-- #comments -->
</div>
<?php require_once APOLLO_TEMPLATES_DIR.'/events/google-map-popup.php' ?>