<?php
$public_art = get_public_art($post);
$data = $public_art->getDataDetail();

$allow_comment = of_get_option(Apollo_DB_Schema::_ENABLE_COMMENT,1) && comments_open();
$str_address_all = $public_art->get_full_address();
//$arrCoor = Apollo_Google_Coor_Cache::getCoordinateByAddressInLocalDB($str_address_all);
//$sCoor = $arrCoor ? implode(",", $arrCoor) : '';
$addItButton = $public_art->renderBookmarkBtn( array( 'class' => 'btn btn-b' ) );
$medium_type = $public_art->generate_public_art_mediums();
?>
<div class="breadcrumbs">
    <ul class="nav">
        <ul class="nav">
            <li><a href="<?php echo home_url() ?>"><?php _e( 'Home', 'apollo' ) ?></a></li>
            <li><a href="<?php echo home_url('public-art') ?>"><?php _e( 'Public Art ', 'apollo' ) ?></a></li>
            <li> <span><?php echo $public_art->get_title() ?></span></li>
        </ul>
    </ul>
</div>
<?php
Apollo_Next_Prev::getNavLink($public_art->id,$post->post_type);
?>
<?php
    include( APOLLO_TEMPLATES_DIR. '/content-single/public-art/general.php');
    include( APOLLO_TEMPLATES_DIR. '/content-single/public-art/map.php');
    include( APOLLO_TEMPLATES_DIR. '/content-single/public-art/associated-artists.php');
    include( APOLLO_TEMPLATES_DIR. '/content-single/public-art/add-fields.php');
    include( APOLLO_TEMPLATES_DIR. '/content-single/public-art/media.php');

    /**
     * @ticket #19025: wpdev54 Customization - Change layout the public art on the octave theme
     */
    $contactInfoTemplate = apply_filters('oc_public_art_add_contact_info', '');
    if($contactInfoTemplate){
        include $contactInfoTemplate;
    }

    /** @Ticket #13250 */
    include APOLLO_TEMPLATES_DIR. '/content-single/common/jetpack-related-posts.php';

?>

<div class="blog-bkl astro-featr">
    <div class="a-block" id="event_comment_block">
        <?php
        // If comments are open or we have at least one comment, load up the comment template.
        if ( $allow_comment ) {
            ?>
            <?php echo Apollo_App::renderSectionLabels(__('COMMENTS', 'apollo'), '<span> <i class="fa fa-comment"></i></span>'); ?>
            <div class="a-block-ct">
                <?php
                comments_template('/templates/comments.php');
                ?>
            </div>
        <?php
       }
        ?>
    </div><!-- #comments -->
</div>
<?php
if ( $data['latitude'] && $data['longitude'] ) {
    require_once APOLLO_TEMPLATES_DIR.'/events/google-map-popup.php';
}
?>

