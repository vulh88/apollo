<?php
    $questions = @unserialize( get_option( Apollo_DB_Schema::_APL_ARTIST_OPTIONS ) );
    $answers = @unserialize( $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_QUESTIONS ) );
    
    $str = '';
    
     // get artist style
    $styles = $artist->generate_artist_styles(TRUE);
    $enableSearchWdStyle = of_get_option(APL_Theme_Option_Site_Config_SubTab::ARTIST_ENABLE_STYLE_SEARCH_WIDGET,1);

    if ( $styles && $enableSearchWdStyle)  {
        $searchWdStyleLabel = of_get_option(APL_Theme_Option_Site_Config_SubTab::ARTIST_STYLE_SEARCH_WIDGET_LABEL, __("Artist Style", "apollo"));
        $str .= '<div class="el-blk">
            <p class="custom apl-simple-link"><strong>' . $searchWdStyleLabel . ': </strong>'.$styles.'</p>
        </div>';
    }
   
    // get artist medium
    $mediums = $artist->generate_artist_mediums(TRUE);
    $enableSearchWdMedium = of_get_option(APL_Theme_Option_Site_Config_SubTab::ARTIST_ENABLE_MEDIUM_SEARCH_WIDGET, 1);

    if ( $mediums && $enableSearchWdMedium)  {
        $searchWdMediumLabel = of_get_option(APL_Theme_Option_Site_Config_SubTab::ARTIST_MEDIUM_SEARCH_WIDGET_LABEL, __("Artist Medium", "apollo"));
        $str .= '<div class="el-blk">
            <p class="custom apl-simple-link"><strong>' . $searchWdMediumLabel . ': </strong>'.$mediums.'</p>
        </div>';
    }
    
    if( $answers ) {
        foreach( $answers as $k => $val ):
            if ( ! $val ) continue;
            $str .= '<div class="el-blk">
                <p class="custom"><strong>'.( isset( $questions[$k] ) ? $questions[$k] : '' ).'</strong></p>
                <p class="custom">'.$val.'</p>
            </div>';
        endforeach;
    }
    
    
    if ( $str ):
?>
<div class="blog-bkl">
    <div class="a-block">
        <?php
            $val = of_get_option(Apollo_DB_Schema::_TEMP_ARTIST_MORE_ABOUT, __('MORE ABOUT THIS ARTIST', 'apollo'));
            echo Apollo_App::renderSectionLabels($val);
        ?>
        <?php echo $str;  ?>
    </div>
</div>
<?php endif; 
$artist->render_cf_detail();
?>
