<div class="blog-bkl">
    <div class="b-share-cat art">
      <?php
            SocialFactory::social_btns(
                array( 'info' => $arrShareInfo,
                    'id' => $artist->id,
                    'data_btns' => array( 'print', 'sendmail' ) ) );
        ?>
        <input name="eventid" value="<?php echo $artist->id; ?>" type="hidden" />
    </div>

    <h1 class="p-ttl"><?php echo $artist->get_title() ?></h1>
    <?php

        $icon_fields = maybe_unserialize( get_option( Apollo_DB_Schema::_APL_ARTISTS_ICONS ) );

        $icons = $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_POST_ICONS, Apollo_DB_Schema::_APL_ARTIST_DATA );
        $artist->renderIcons($icon_fields, $icons);
    ?>

    <div class="art-career"><?php echo $artist->generate_artist_categories(true) ?></div>

    <div class="rating-box rating-action rating-art">
        <?php if( of_get_option(Apollo_DB_Schema::_ENABLE_THUMBS_UP,1)) : ?>
        <div class="like" id="_event_rating_<?php echo $artist->id . '_' . 'artist' ?>" data-ride="ap-event_rating" data-i="<?php echo $artist->id ?>" data-t="<?php echo Apollo_DB_Schema::_ARTIST_PT ?>"> <a href="javascript:void(0);"><span class="_count">&nbsp;</span></a></div>
        <?php endif ?>
        <?php if ( $allow_comment ): ?>
        <div class="cm"> <a href="javascript:;"><?php _e( 'Comment', 'apollo' ) ?></a></div>
        <?php endif; ?>
    </div>

    <?php $generalDetail = apply_filters('oc_artist_change_general_detail_template', APOLLO_TEMPLATES_DIR. '/content-single/artists/partial/general-detail.php');
    include $generalDetail;
    ?>
</div>