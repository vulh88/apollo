
<?php

if ( $data ):
    /** @Ticket #14350 */
    $thumbsUpEnable = of_get_option(Apollo_DB_Schema::_ENABLE_THUMBS_UP,1);
    $thumbsUpPosition = of_get_option(Apollo_DB_Schema::_EVENT_SHOW_THUMBS_UP_POSITION, 'default');
    foreach ( $data as $event ):

        if ( is_int( $event ) ) {
            $event = get_event( get_post( $event ) );
        } else {
            $event = get_event( $event );
        }

        $orgs         = Apollo_App::get_post_type_item($event->{Apollo_DB_Schema::_APOLLO_EVENT_ORGANIZATION});
        $venues       = Apollo_App::get_post_type_item($event->{Apollo_DB_Schema::_APOLLO_EVENT_VENUE});
        $ticket_url   = $event->get_meta_data(Apollo_DB_Schema::_ADMISSION_TICKET_URL, Apollo_DB_Schema::_APOLLO_EVENT_DATA);
        $discount_url = $event->get_meta_data(Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL, Apollo_DB_Schema::_APOLLO_EVENT_DATA);

        // get event's organization
        $registeredOrg   = $event->get_register_org(); // = NULL if organization is TMP ORG
        $registeredOrgID = empty($registeredOrg) ? '' : $registeredOrg->org_id;

        ?>
        <div class="more-frm-itm">
            <div class="more-pic">
                <a href="<?php echo $event->get_permalink(); ?>">
                    <?php echo $event->get_image( 'thumbnail' ); ?>
                </a>
            </div>
            <div class="more-ct custom">
                <?php if ( $thumbsUpEnable && $thumbsUpPosition == 'next_title' ) : ?>
                    <div class="event-thumbs-up-next-the-title">
                        <h3>
                            <a href="<?php echo $event->get_permalink(); ?>"><?php echo $event->get_title(); ?></a>
                            <div class="like" data-ride="ap-event_rating" id="_event_rating_<?php echo $event->id.'_'.'event' ?>" data-i="<?php echo $event->id ?>" data-t="event"> <a href="javascript:void(0);" ><span class="_count"></span></a></div>
                        </h3>
                    </div>
                <?php else : ?>
                    <h3> <a href="<?php echo $event->get_permalink(); ?>"><?php echo $event->get_title(); ?></a></h3>
                <?php endif; ?>

                <p class="meta auth"><?php echo $event->renderOrgVenueHtml() ?></p>
                <p class="p-date">
                    <?php
                        $event->render_sch_date();
                    ?>
                </p>
                <?php if( $thumbsUpEnable && $thumbsUpPosition == 'default' ) : ?>
                    <div class="rating-box rating-action">
                        <div class="like parent-not-underline-hover" data-ride="ap-event_rating" id="_event_rating_<?php echo $event->id.'_'.'event' ?>" data-i="<?php echo $event->id ?>" data-t="event"> <a href="javascript:void(0);" ><span class="_count"></span></a></div>
                    </div>
                <?php endif ?>
            </div>

            <div  class="b-btn custom">

                <?php if ( $ticket_url ):
                    $buyTicketText = of_get_option(Apollo_DB_Schema::_APL_EVENT_BUY_TICKET_TEXT, __('BUY TICKETS', 'apollo'));
                    ?>
                    <a target="_BLANK"
                       data-ride="ap-logclick"
                       data-action="apollo_log_click_activity"
                       data-activity="click-buy-ticket"
                       data-item_id="<?php echo $event->id; ?>"
                       data-start="<?php echo $event->{Apollo_DB_Schema::_APOLLO_EVENT_START_DATE}; ?>"
                       data-end="<?php echo $event->{Apollo_DB_Schema::_APOLLO_EVENT_END_DATE}; ?>",
                       data-org-id="<?php echo $registeredOrgID; ?>"
                       href="<?php echo $ticket_url; ?>"
                       class="btn btn-b"><?php echo $buyTicketText ?></a>
                <?php endif; ?>
                <?php if ($discount_url):
                    $checkD = of_get_option(Apollo_DB_Schema::_APL_EVENT_CHECK_DISCOUNTS_TEXT, __('CHECK DISCOUNTS', 'apollo'));
                    ?>
                    <a target="_BLANK"
                       data-ride="ap-logclick"
                       data-action="apollo_log_click_activity"
                       data-activity="click-discount"
                       data-item_id="<?php echo $event->id; ?>"
                       data-start="<?php echo $event->{Apollo_DB_Schema::_APOLLO_EVENT_START_DATE}; ?>"
                       data-end="<?php echo $event->{Apollo_DB_Schema::_APOLLO_EVENT_END_DATE}; ?>",
                       data-org-id="<?php echo $registeredOrgID; ?>"
                       style="margin-left: 0px; margin-bottom: 10px"
                       href="<?php echo $discount_url; ?>"
                       class="btn btn-b"><?php echo $checkD ?></a>
                <?php endif ?>
                <?php echo $event->renderBookmarkBtn( array( 'class' => 'btn-bm btn btn-b' ) ); ?>

            </div>
        </div>

    <?php endforeach; ?>


<?php endif; ?>