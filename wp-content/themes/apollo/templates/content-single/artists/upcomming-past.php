<?php
    include_once APOLLO_TEMPLATES_DIR.'/events/list-events.php';

    $upComingEvents = $artist->getAssociatedUpPastEvents(1);

    $pastEvents = $artist->getAssociatedUpPastEvents(1, false);

    $hasUpcoming = !empty($upComingEvents['data']);

    $hasPast= !empty($pastEvents['data']);

if ( $hasPast || $hasUpcoming): ?>
<div class="blog-bkl artist-tab <?php echo Apollo_App::getClassSectionLabels() ?>">
    <nav class="nav-tab">
        <ul class="tab-list">
            <?php if ($hasUpcoming): ?>
            <li class="selected"><a href="#" data-id="1"><?php _e( 'UPCOMING EVENTS', 'apollo' )  ?></a></li>
            <?php endif; ?>

            <?php if ($hasPast): ?>
            <li><a href="#" data-id="2"><?php _e( 'PAST EVENTS', 'apollo' ) ?></a></li>
            <?php endif; ?>
        </ul>
    </nav>
    <div class="tab-bt art"></div>
</div>
<?php endif; ?>

<?php if ($hasUpcoming): ?>

<div data-target="1" class="blog-bkl tab">
    <div class="a-block-ct">
        <div id="apollo-view-more-upcomming-container">
            <?php echo Apollo_App::renderHTML(APOLLO_TEMPLATES_DIR. '/content-single/artists/partial/artist-events.php', $upComingEvents['data']) ?>
        </div>
        <?php if (isset($upComingEvents['have_more']) && $upComingEvents['have_more']): ?>
            <p class="t-r load-more b-btn wp-ct-event">
                <a class="vmore btn-b arw ct-btn-sm-event"  href="javascript:void(0);"
                    data-ride="ap-more"
                    data-container="#apollo-view-more-upcomming-container"
                    data-holder="#apollo-view-more-upcomming-container>:last"
                    data-sourcetype="url"
                    data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_get_more_events_upcomming_past&type=upcomming&module=artist&page=2') ?>
                    &artist_id=<?php echo $artist->id; ?>"
                    data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
                    data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'>
                    <?php _e( 'View more', 'apollo' ) ?>
                </a>
            </p>
        <?php endif; ?>
    </div>
</div>
<?php endif; ?>

<?php if ($hasPast): ?>
<div data-target="2" class="blog-bkl tab" style="display: none;">
    <div id="apollo-view-more-past-container">
        <?php echo Apollo_App::renderHTML(APOLLO_TEMPLATES_DIR. '/content-single/artists/partial/artist-events.php', $pastEvents['data']) ?>
    </div>

    <?php if (isset($pastEvents['have_more']) && $pastEvents['have_more']): ?>
        <p class="t-r load-more b-btn wp-ct-event">
            <a class="vmore btn-b arw ct-btn-sm-event"  href="javascript:void(0);"
                data-container="#apollo-view-more-past-container"
                data-ride="ap-more"
                data-holder="#apollo-view-more-past-container>:last"
                data-sourcetype="url"
                data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_get_more_events_upcomming_past&type=past&module=artist&page=2') ?>
                &artist_id=<?php echo $artist->id; ?>"
                data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
                data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'><?php _e( 'View more', 'apollo' ) ?></a></p>
    <?php endif; ?>
</div>
<?php endif; ?>
