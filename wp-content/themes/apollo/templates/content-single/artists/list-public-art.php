<?php
include_once APOLLO_TEMPLATES_DIR.'/public-art/list-public-arts.php';
$list_pub_art = new List_Public_Art_Adapter( 1, Apollo_Display_Config::PAGESIZE_UPCOM );
$list_pub_art->get_artist_public_art( $artist->id);
?>

<?php if ( ! $list_pub_art->isEmpty() ): ?>

    <div class="blog-bkl classified">
        <div class="a-block">
            <?php echo Apollo_App::renderSectionLabels(__('Public Art','apollo')); ?>
        </div>
        <div id="apollo-view-more-classified-container">
            <?php echo $list_pub_art->render_html( '_artist-public-art.php' ) ?>
        </div>

        <?php if ( $list_pub_art->isShowMore() ): ?>
            <div class="load-more b-btn">
                <a href="javascript:void(0);"
                   data-container="#apollo-view-more-classified-container"
                   data-ride="ap-more"
                   data-holder="#apollo-view-more-classified-container>:last"
                   data-sourcetype="url"
                   data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_show_more_artist_public_art&page=2') ?>&current_artist_id=<?php echo $artist->id; ?>&user_id=<?php echo $artist->post->post_author ?>"
                   data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
                   data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
                   class="btn-b arw"><?php _e('SHOW MORE', 'apollo') ?>
                </a>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>
