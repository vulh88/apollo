<div class="art-social">
    <?php
    if ( $email || $website || $blog || $fb || $tw || $ins ||$lk || $pr ):
        ?>
        <div class="el-blk">

            <?php
            if ( $email) : ?>
                <div class="art-social-item"><i class="fa fa-envelope fa-lg">&nbsp;&nbsp;&nbsp;</i>
                    <a href="mailto:<?php echo $email; ?>"><?php _e( 'Email', 'apollo' ) ?></a>
                    <div class="slash">/</div>
                </div>
            <?php endif; ?>

            <?php
            if ( $website ):
                ?>
                <div class="art-social-item"><i class="fa fa-link fa-lg">&nbsp;&nbsp;&nbsp;</i>
                    <a target="_blank" href="<?php echo $website; ?>"><?php _e( 'Website', 'apollo' ) ?></a>
                    <div class="slash">/</div>
                </div>
            <?php endif; ?>

            <?php if ( $blog ): ?>
                <div class="art-social-item"><i class="fa fa-star fa-lg">&nbsp;&nbsp;&nbsp;</i>
                    <a target="_blank" href="<?php echo $blog; ?>"><?php _e( 'Blog', 'apollo' ) ?></a>
                    <div class="slash">/</div>
                </div>
            <?php endif; ?>

            <?php if ( $fb ):?>
                <div class="art-social-item"><i class="fa fa-facebook fa-lg">&nbsp;&nbsp;&nbsp;</i>
                    <a target="_blank" href="<?php echo $fb; ?>"><?php _e( 'Facebook', 'apollo' ) ?></a>
                    <div class="slash">/</div>
                </div>
            <?php endif; ?>

            <?php if ( $tw ): ?>
                <div class="art-social-item"><i class="fa fa-twitter fa-lg">&nbsp;&nbsp;&nbsp;</i>
                    <a target="_blank" href="<?php echo $tw; ?>"><?php _e( 'Twitter', 'apollo' ) ?></a>
                    <div class="slash">/</div>
                </div>
            <?php endif; ?>

            <?php if ( $ins ):?>
                <div class="art-social-item"><i class="fa fa-camera-retro fa-lg">&nbsp;&nbsp;&nbsp;</i>
                    <a target="_blank" href="<?php echo $ins; ?>"><?php _e( 'Instagram', 'apollo' ) ?></a>
                    <div class="slash">/</div>
                </div>
            <?php endif; ?>

            <?php if ( $lk ): ?>
                <div class="art-social-item"><i class="fa fa-linkedin-square fa-lg">&nbsp;&nbsp;&nbsp;</i>
                    <a target="_blank" href="<?php echo $lk; ?>"><?php _e( 'LinkedIn', 'apollo' ) ?></a>
                    <div class="slash">/</div>
                </div>
            <?php endif; ?>

            <?php if ( $pr ): ?>
                <div class="art-social-item"><i class="fa fa-pinterest fa-lg">&nbsp;&nbsp;&nbsp;</i>
                    <a target="_blank" href="<?php echo $pr; ?>"><?php _e( 'Pinterest', 'apollo' ) ?></a>
                </div>
            <?php endif; ?>

        </div>
    <?php endif; ?>

    <?php
    if ( $address || $contact_info ):
        ?>
        <div class="el-blk location">
            <?php
            if( $address ):
                ?>
                <p class="apl-table"> <span class="apl-table-cell"><i class="fa fa-map-marker fa-lg">&nbsp;&nbsp;&nbsp;</i></span>
                    <label  class="apl-table-cell"><?php echo $address ?></label>
                </p>
            <?php
            endif;
            ?>

            <?php if ( $contact_info ): ?>
                <p> <span><i class="fa fa-phone fa-lg">&nbsp;&nbsp;</i></span>
                    <label><?php echo $contact_info ?></label>
                </p>
            <?php endif; ?>

        </div>
    <?php endif; // End  $address || $contact_info  ?>

    <div class="el-blk">
        <div class="b-btn fl">
            <?php echo $addItBtn ?>
        </div>
    </div>
</div>