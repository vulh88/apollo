<?php
    $dataParking = isset($data['data_parking'])?$data['data_parking']:array();
    $deadlineDate =  isset($data['deadline_date']) ? $data['deadline_date'] : '';
?>
<div class="blog-bkl organization">
    <div class="b-share-cat art">
        <?php
        $arrShareInfo = $classified->getShareInfo();
        SocialFactory::social_btns( array( 'info' => $arrShareInfo, 'id' => $classified->id,
            'data_btns' => array( 'print', 'sendmail' ) ) );
        ?>
        <input name="eventid" value="<?php echo $classified->id; ?>" type="hidden" />
    </div>
    <h1 class="p-ttl"><?php echo $classified->get_title() ?></h1>
    <div class="meta auth classified">
        <?php
            $postedByVal = $classified->renderOrgHtml();
            echo !empty($postedByVal) ? $postedByVal . ' ;  ' : '';
        ?>
        <span><?php _e('Posted on','apollo'); ?>&nbsp;</i></span>
        <label><?php echo APL::dateUnionDateShort($classified ->get_post_data()->post_date); ?></label>
    </div>
    <?php $classified->renderIcons($data['icon_fields'], $data['icons']); ?>

    <!--Org type -->
    <?php $categories = $classified->generate_categories(); ?>
    <div class="org-type">
        <?php if ($categories) {echo $categories;}
            if( !empty($deadlineDate)){
                echo $categories ? ' - ' : '';?>
             <span><?php _e('DEADLINE : ','apollo'); ?>&nbsp;</i></span>
             <label><?php echo APL::dateUnionDateShort($data['deadline_date']); ?></label>
            <?php }
        ?>
    </div>
    <!-- Org type -->
    <div class="rating-box rating-action rating-art">
        <?php if( of_get_option(Apollo_DB_Schema::_ENABLE_THUMBS_UP,1)) : ?>
            <div class="like" id="_event_rating_<?php echo $classified->id . '_' . Apollo_DB_Schema::_CLASSIFIED ?>" data-ride="ap-event_rating" data-i="<?php echo $classified->id ?>" data-t="<?php echo Apollo_DB_Schema::_CLASSIFIED ?>"> <a href="javascript:void(0);"><span class="_count">&nbsp;</span></a></div>
        <?php endif ?>
            <?php if ( $allow_comment ): ?>
                <div class="cm"> <a href="javascript:;"><?php _e( 'Comment', 'apollo' ) ?></a></div>
            <?php endif; ?>

    </div>
    <?php
    /** @Ticket #13137 */
    $image = $classified->get_image('medium', array(),
        array(
            'aw' => true,
            'ah' => true,
        ),
        'normal', '');
    ?>
    <div class="el-blk">
        <div class="art-pic <?php echo !$image ? 'no-place-holder-cate' : ''; ?>">
            <?php
            $enableImage =  of_get_option(Apollo_DB_Schema::_CLASSIFIED_ENABLE_PRIMARY_IMAGE,1);
            if($enableImage){
                echo $image;
            }
             ?>
        </div>
        <?php include(APOLLO_TEMPLATES_DIR . '/content-single/classified/social.php'); ?>
    </div>
    <div class="el-blk">
        <div class="art-desc apl-internal-content">
            <?php
                echo $classified->get_full_content();
            ?>
        </div>
    </div>
</div>


<?php
 
    //parking info + PUBLIC HOURS + PUBLIC ADMISSION FEES
    if(is_array($dataParking) && count(($dataParking))>0){
        foreach($dataParking as $k => $item){
            if(!empty($item)){
                $title = str_replace('_',' ',$k);
                $title = strtoupper($title);?>
                <div class="blog-bkl">
                    <div class="a-block">
                        <h4><?php echo $title ?></h4>
                        <div class="el-blk">
                            <p><?php echo $item ?></p>
                        </div>
                    </div>
                </div>
            <?php
            }
        }
    }
?>

