<!-- @ticket #16639:  Upload PDF files to the FE and Admin Classified forms. -->
<?php
    $upload_pdf = maybe_unserialize(get_apollo_meta($classified->id, 'upload_pdf', true));
if(!empty($upload_pdf)):
?>
<div class="blog-bkl">
    <div class="a-block">
        <?php echo Apollo_App::renderSectionLabels(__( 'Documents', 'apollo' )); ?>
        <div class="des">
            <?php foreach($upload_pdf as $attachment_id):
                $attachment = get_post($attachment_id);
                if ( ! $attachment )                    continue;
                ?>
                <p> <i class="fa fa-file-pdf-o"></i><a download href="<?php echo $attachment->guid ?>" class="vmore"><?php echo !empty($attachment->post_excerpt) ? $attachment->post_excerpt : substr($attachment->guid, strrpos($attachment->guid, '/') + 1, -4)  ?></a></p>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?php endif; ?>