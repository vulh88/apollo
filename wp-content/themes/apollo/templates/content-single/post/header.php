<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/15/2015
 * Time: 10:41 AM
 */

    $arraySocial =  array(
        'info' => $arrShareInfo,
        'id' => $post_id,
        'data_btns' =>
            array(
                'print',
                'sendmail',
            )
    );
    ob_start();

    SocialFactory::social_btns( $arraySocial );
    $shareBtnsOnDefault = ob_get_clean();
    ?>

<div class="b-share-cat">
    <?php echo $shareBtnsOnDefault; ?>
    <input name="eventid" value="<?php echo $post_id; ?>" type="hidden" />
</div>


<div class="blog-tt">
    <div class="blog-name">
        <?php
            if(is_single()){
                ?>
                <div class="namedetail-txt blog-date parent-not-underline-hover"><a><?php echo get_the_title();  ?></a></div>
                <?php
                /**
                 * @ticket #18403: Arts Education Customizations - [Blog detail page] - Add the option suppress the 'Excerpt' field - Item 5
                 */
                $enableExcerpt = of_get_option(APL_Theme_Option_Site_Config_SubTab::_BLOG_ENABLE_EXCERPT, 1);
                $excerpt = get_the_excerpt();
                if ($excerpt && $enableExcerpt): ?>
                <p><?php echo $excerpt ?></p>
                <?php endif; ?>
                <?php
            }else{
                ?>
                <div class="namedetail-txt blog-date parent-not-underline-hover">
                    <?php
                    /*@ticket #177560:  BLOG - Sponsored Content - New Page checkbox and URL field textbox*/
                    $permalink = get_the_permalink();
                    $urlField = isset($urlField) ? $urlField : "href='$permalink'";
                    ?>
                    <a <?php echo $urlField ?>>
                        <?php echo get_the_title();  ?>
                    </a>
                </div>
                <?php
            }
        ?>

    </div>
</div>

<?php
//if ( (($enableDate || $enableAuthor || $enableThumbUp || $enableCommentNumber) && is_single()) || ! is_single()): ?>
<div class="blog-category">

    <?php
    //if ($enableDate): ?>
    <div class="cate-blk parent-not-underline-hover apl-blog-date">
        <i class="fa fa-calendar fa-lg">&nbsp;&nbsp;&nbsp;</i><span style="  float: left;
                        padding: 5px 0px;"><?php echo Apollo_App::apl_date( 'M d, Y', get_post_time() ) ?></span><a class="slash">/</a>
    </div>
    <?php //endif; ?>

    <?php
    /**
     * @ticket #18042: Arts Education Customizations - [Blog listing page] - Suppress 'Author' and 'Cateogry' - Item 3,4
     * Do not show author on the author page
     * On the Category page and blog page: check enable author and enable category in theme option
     */
    $enableAuthor = of_get_option(Apollo_DB_Schema::_BLOG_ENABLE_AUTHOR, 1);
    $enableAuthor = !get_query_var('author') && (is_single() || (!is_single() && $enableAuthor));
    $enableCategory = of_get_option(Apollo_DB_Schema::_BLOG_ENABLE_CATEGORY, 1);

    if($enableAuthor){ ?>
    <div class="cate-blk parent-not-underline-hover apl-blog-author">
        <i class="fa fa-user fa-lg">&nbsp;&nbsp;&nbsp;</i><?php echo the_author_posts_link() ?><a class="slash">/</a>
    </div>
    <?php } ?>

    <?php if(!is_single() && $enableCategory){ ?>
        <div class="cate-blk apl-blog-cat"><i class="fa fa-folder-open fa-lg">&nbsp;&nbsp;</i>
            <?php $blogType = of_get_option( Apollo_DB_Schema::_BLOG_CATEGORY_TYPES, 'parent-only');
            Apollo_App::apl_get_blog_categories($post_id,$blogType)?>
            <a class="slash">/</a>
        </div>
    <?php } ?>

    <?php
    //if ($enableThumbUp || $enableCommentNumber):
    ?>
        <div class="cate-blk  parent-not-underline-hover apl-blog-thumbup">

            <?php
            /**
             * @ticket #18768
             */
            if (!empty($enableThumbUp)):
                ?>
            <div  class="like parent-not-underline-hover" id="_event_rating_<?php echo $post_id. '_' . 'post' ?>"
                  data-ride="ap-event_rating" data-i="<?php echo $post_id ?>"
                  data-t="post"> <a href="javascript:void(0);" style="padding-top: 1px">
                    <span class="_count">&nbsp;</span></a>
            </div>
            <?php endif; ?>

            <?php //if ( $enableCommentNumber && comments_open($post_id) ): ?>
           <?php if (comments_open($post_id)): ?>
            <div class="cate-blk  cmb apl-blog-cmb">
            <i class="fa fa-comment fa-lg">&nbsp;&nbsp;</i>
            <?php
            $cn = get_comments_number( get_the_ID() );
            $cn = $cn && $cn < 10 ? '0'. $cn :  $cn;
            ?>
            <a class="">
                <?php
                $default = sprintf( '%s comment', '<span class="cm-num">(1)</span>' );
                echo  sprintf( _n( $default, '%s comments', $cn, 'apollo' ), '<span class="cm-num">('.number_format_i18n( $cn ). ')</span>' );
                ?>
            </a>
            </div>
            <?php  endif; ?>

        </div>
    <?php //endif; ?>

</div>
<?php
    //endif; // End !$enableDate || !$enableAuthor || !$enableThumbUp || !$enableCommentNumber
?>

