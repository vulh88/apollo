
<div class="blog-category dt-blog">

        <div class="cate-blk apl-blog-cat"><i class="fa fa-folder-open fa-lg">&nbsp;&nbsp;</i>
            <?php $blogType = of_get_option( Apollo_DB_Schema::_BLOG_CATEGORY_TYPES, 'parent-only');
            Apollo_App::apl_get_blog_categories($post_id,$blogType)?>
            <a >&nbsp; | </a>
        </div>


    <?php
    //if ($enableDate): ?>
    <div class="cate-blk parent-not-underline-hover apl-blog-date">
        <i class="fa fa-calendar fa-lg">&nbsp;&nbsp;&nbsp;</i><span style="  float: left;
                        padding: 5px 0px;"><?php echo Apollo_App::apl_date( 'M d, Y', get_post_time() ) ?>
    </div>
    <?php //endif; ?>
</div>
<div class="blog-tt">
    <div class="blog-name">
        <?php
        if(is_single()){
            ?>
            <div class="namedetail-txt blog-date parent-not-underline-hover"><a><?php echo get_the_title();  ?></a></div>
            <?php
            /**
             * @ticket #18403: Arts Education Customizations - [Blog detail page] - Add the option suppress the 'Excerpt' field - Item 5
             */
            $enableExcerpt = of_get_option(APL_Theme_Option_Site_Config_SubTab::_BLOG_ENABLE_EXCERPT, 1);
            $excerpt = get_the_excerpt();
            if ($excerpt && $enableExcerpt): ?>
                <p><?php echo $excerpt ?></p>
            <?php endif; ?>
            <?php
        }else{
            ?>
            <div class="namedetail-txt blog-date parent-not-underline-hover">
                <a href="<?php echo get_the_permalink(); ?>">
                    <?php echo get_the_title();  ?>
                </a>
            </div>
            <?php
        }
        ?>

    </div>
</div>
<div class="blog-category author-blog">

    <div class=" apl-blog-author">
         <i class="fa fa-user fa-lg">&nbsp;&nbsp;&nbsp;</i><?php echo _e('BY ', 'apollo').the_author_posts_link() ?>
    </div>

</div>
<div class="b-share-cat blog-category social-blog">
    <?php
    $arraySocial =  array(
        'info' => $arrShareInfo,
        'id' => $post_id,
        'data_btns' =>
            array(
                'print',
                'sendmail',
            )
    );
    ob_start();
    SocialFactory::social_btns( $arraySocial);
    $shareButtons = ob_get_contents();
    ob_clean();

    echo $shareButtons;
    ?>
    <input name="eventid" value="<?php echo $post_id; ?>" type="hidden" />
</div>


