<?php
$artists = $public_art->getListArtists();
if ($artists):
?>
    <div class="blog-bkl">
        <div class="a-block">
            <?php echo Apollo_App::renderSectionLabels(__('Associated artists', 'apollo')); ?>
            <div class="el-blk">
                <?php
                    $_html= '';
                    foreach ( $artists as $art )
                    {
                        $artist = get_artist($art);
                        $_html .= '<div class="org-type artist">';
                        if ($artist->is_linked()) {
                            $_html.='<a class="link" href="'.$artist->get_permalink(). '"><div class="artist-pic">';
                            $_html.=  $artist->get_image( 'thumbnail');
                            $_html.='</div><span>'.$artist->post->post_title.'</span></a>';
                        } else {
                            $_html.=  '<div class="artist-pic">'.$artist->get_image( 'thumbnail' ).'</div><span>'.$artist->post->post_title.'</span>';
                        }
                        $_html.='</div>';
                    }
                    echo $_html;
                ?>
            </div>
        </div>
    </div>
<?php endif; ?>