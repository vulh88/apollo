<div class="el-blk">
    <div class="art-pic">
        <?php echo $public_art->get_image('medium'); ?>
    </div>
    <?php include(APOLLO_TEMPLATES_DIR . '/content-single/public-art/social.php'); ?>
</div>
<div class="el-blk">
    <div class="art-desc apl-internal-content">
        <?php
        $public_art->the_short_desc($public_art->get_content(500, true), $public_art->get_full_content(), '_ed_sum_short', '_ed_sum_full');
        ?>
    </div>
</div>
<div class="el-blk">
    <?php if (!empty($medium_type)) {?>
        <p class="org-type medium"><?php _e('Medium type: ', 'apollo'); ?><?php echo $medium_type; ?></p>
    <?php } ?>

    <?php if (!empty($data['date_created'])) { ?>
        <p><?php _e('Date created: ', 'apollo'); ?><?php echo $data['date_created'] ?></p>
    <?php } ?>

    <?php if (!empty($data['dimension'])) { ?>
        <p><?php _e('Dimensions: ', 'apollo'); ?><?php echo $data['dimension'] ?></p>
    <?php } ?>
</div>