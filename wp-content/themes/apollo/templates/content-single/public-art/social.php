<div class="art-social">
    <div class="el-blk">
        <?php if(!empty($data['email'])){?>
        <div class="art-social-item"><i class="fa fa-envelope fa-lg">&nbsp;&nbsp;&nbsp;</i><a href="mailto:<?php echo $data['email'] ?>">
                <?php  _e('Email','apollo') ?></a>
            <div class="slash">/</div>
        </div>
        <?php } ?>

        <?php if(!empty($data['web'])){ ?>
        <div class="art-social-item"><i class="fa fa-link fa-lg">&nbsp;&nbsp;&nbsp;</i><a href="<?php echo $data['web'] ?>">
                <?php _e('Website','apollo') ?></a>
            <div class="slash">/</div>
        </div>
        <?php } ?>

    </div>
    <div class="el-blk location">
        <?php if(!empty($data['address'])){ ?>
        <p><span><i class="fa fa-map-marker fa-lg">&nbsp;&nbsp;&nbsp;</i></span>
            <label><?php echo $data['address']; ?></label>
        </p>
        <?php } ?>

        <?php if(!empty($data['phone'])){ ?>
        <p><span><i class="fa fa-phone fa-lg">&nbsp;&nbsp;</i></span>
            <label><?php echo $data['phone']; ?></label>
        </p>
        <?php } ?>
    </div>
    <?php if (!empty($addItButton)) { ?>
        <div class="el-blk">
            <div class="b-btn fl">
                <?php echo $addItButton ?>
            </div>
        </div>
    <?php } ?>
</div>