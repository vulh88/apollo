<?php if ( $data['latitude'] && $data['longitude'] ) { 
    $metaMapDetail = unserialize(Apollo_App::apollo_get_meta_data($public_art->id ,Apollo_DB_Schema::_APL_PUBLIC_ART_DATA));
?>

    <div class="blog-bkl">
        <div class="a-block">
            <?php echo Apollo_App::renderSectionLabels(__('Location', 'apollo'), '<span><i class="fa fa-map-marker"></i></span>'); ?>
        </div>
        <div class="a-block-ct locatn">
            <div class="loc-address">
            
                <?php if(isset($data['address']) && $data['address']){
                    ?>
                    <p><?php echo $data['address'] ?></p>
                    <?php if(isset($metaMapDetail['_public_art_longitude']) && $metaMapDetail['_public_art_longitude']
                        && isset($metaMapDetail['_public_art_latitude']) && $metaMapDetail['_public_art_latitude']){ ?>
                        <p><?php echo $data['latitude'].', '.$data['longitude']?></p>
                    <?php } ?>
                <?php }
                ?>
            </div>
            <div class="lo-left public-art">
                <div id="art_detail_map_canvas"
                     data-lat = "<?php echo $data['latitude'] ?>"
                     data-long = "<?php echo $data['longitude'] ?>"
                     data-address="<?php echo $str_address_all ?>"
                     data-relation_id="_popup_google_map_full"
                     data-nonce="<?php echo wp_create_nonce('save-coor') ?>"
                     data-img_location="<?php echo get_stylesheet_directory_uri() ?>/assets/images/icon-location.png">
                </div>
                <p class="t-r"> <a href="javascript:void(0);" data-target="#_popup_google_map" id="_event_trigger_fullmap"><?php _e('Full map and directions','apollo');?></a></p>
            </div>
            <div class="pba-display-nearby">
                <label for="pba_display_nearby"><?php echo __('Display other nearby public art','apollo')?></label>
                <input type="checkbox" name="display-nearby" value="" id="pba_display_nearby" />
            </div>
        </div>
    </div>
<?php } ?>
