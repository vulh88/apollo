<div class="blog-bkl public-art">
    <div class="b-share-cat art">
        <?php
        $arrShareInfo = $public_art->getShareInfo();
        SocialFactory::social_btns( array( 'info' => $arrShareInfo, 'id' => $public_art->id,
            'data_btns' => array( 'print', 'sendmail' ) ) );
        ?>
        <input name="eventid" value="<?php echo $public_art->id; ?>" type="hidden" />
    </div>
    <h1 class="p-ttl"><?php echo $public_art->get_title() ?></h1>

    <?php $public_art->renderIcons($data['icon_fields'], $data['icons']); ?>

    <?php
        $artType = $public_art->generate_categories();
        if ($artType):
    ?>
    <!--Org type -->
    <div class="org-type"><?php _e('Category: ','apollo'); echo $artType ; ?></div>
    <!-- Org type -->
    <?php endif; ?>
    <?php
    $artCollection = $public_art->generate_public_art_collections();
    if ($artCollection):
        ?>
        <!--Org type -->
        <div class="org-type"><?php _e('Collection: ','apollo'); echo $artCollection ; ?></div>
        <!-- Org type -->
    <?php endif; ?>
    <?php
    $artLocation = $public_art->generate_public_art_locations();
    if ($artLocation):
        ?>
        <!--Org type -->
        <div class="org-type"><?php _e('Location: ','apollo'); echo $artLocation ; ?></div>
        <!-- Org type -->
    <?php endif; ?>
    <div class="rating-box rating-action rating-art">
        <?php if( of_get_option(Apollo_DB_Schema::_ENABLE_THUMBS_UP,1)) : ?>
        <div class="like" id="_event_rating_<?php echo $public_art->id . '_' . Apollo_DB_Schema::_PUBLIC_ART_PT ?>" data-ride="ap-event_rating" data-i="<?php echo $public_art->id ?>" data-t="<?php echo Apollo_DB_Schema::_PUBLIC_ART_PT ?>"> <a href="javascript:void(0);"><span class="_count">&nbsp;</span></a></div>
        <?php endif ?>
        <?php if ( $allow_comment ): ?>
            <div class="cm"> <a href="javascript:;"><?php _e( 'Comment', 'apollo' ) ?></a></div>
        <?php endif; ?>
    </div>
    <?php
    /** @ticket #19025: wpdev54 Customization - Change layout the public art on the octave theme */
    $generalFile = apply_filters('oc_public_art_change_general_detail_template', APOLLO_TEMPLATES_DIR . '/content-single/public-art/partials/general-detail.php');
    include $generalFile;
    ?>
</div>
