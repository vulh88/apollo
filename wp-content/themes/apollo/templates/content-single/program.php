<?php
global $post;

$apolloProgramID = $post->ID;

$apl_query = new Apl_Query( Apollo_Tables::_APL_PROGRAM_EDUCATOR );
$prodEdu = $apl_query->get_row( "prog_id = $post->ID" );
$eduID = $prodEdu->edu_id;
$post = get_post($eduID);

include APOLLO_TEMPLATES_DIR . '/content-single/educator.php';