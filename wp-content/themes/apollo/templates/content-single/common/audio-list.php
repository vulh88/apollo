<?php
if(is_array($audioList) && count($audioList) > 0 ){
?>
<?php  echo Apollo_App::renderSectionLabels(__('AUDIO', 'apollo'), '<span> <i class="fa fa-file-audio-o"></i></span>'); ?>
<div class="a-block-ct">
    <ul class="audio-list">
        <?php
        foreach($audioList as $item):
            ?>
            <li>
                <p> <strong><?php echo $item['desc'] ?></strong></p>

                    <?php
                    if($item['type'] == 'file'){
                        ?>
                        <p>
                        <audio controls="controls" style="border:0px;width:100%;height:50px;">
                            <source src='<?php echo $item['link'] ?>' type="audio/ogg">
                            <source src='<?php echo $item['link'] ?>' type="audio/mpeg">
                            <embed height="50" width="100%" src='<?php echo $item['link'] ?>'> </embed>
                        </audio>
                        </p>
                    <?php
                    }else{
                        echo ' <p style="margin-top: 25px" class="apl-list-audio">';
                        /** @Ticket #19321 - render soundcloud link */
                        if (!$item['type'] && strpos($item['link'], 'https://soundcloud.com') >= 0) {
                            $getSoundCloud = curl_init('https://soundcloud.com/oembed');
                            curl_setopt_array($getSoundCloud, array(
                                CURLOPT_RETURNTRANSFER => 1,
                                CURLOPT_POSTFIELDS => array(
                                    'format' => 'json',
                                    'url' => $item['link']
                                ),
                            ));

                            $result = json_decode(curl_exec($getSoundCloud));
                            curl_close($getSoundCloud);
                            if (isset($result->html)) {
                                echo $result->html;
                            }

                        } else {
                            echo  str_replace('\\','',$item['link']);
                        }
                        echo '  </p>';
                    }
                    ?>

            </li>
        <?php endforeach; ?>
    </ul>
</div>
<?php } ?>