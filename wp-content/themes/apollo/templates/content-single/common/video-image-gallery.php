
<?php
if ( (isset($videos) && $videos) || (isset($gallerys) && $gallerys) ):
    $primary_image = of_get_option(Apollo_DB_Schema::_PRIMARY_IMAGE_TYPE_BUSINESS, 'default');
    ?>
    <div class="<?php echo isset( $_vg_block_class ) && $_vg_block_class ? $_vg_block_class : 'a-block' ?>">

        <?php if ( $primary_image == 'default' && $gallerys && isset( $gallerys[0] ) && $gallerys[0] ) :

            /**
             * @ticket #19131: Octave Theme - Change all detail page section labels same the section labels on the homepage - item 2
             */
                $photoText = __( 'PHOTOS', 'apollo' );
                if(isset($notApplySectionLabel) && !$notApplySectionLabel){
                     echo '<h4>'.$photoText.'<span> <i class="fa fa-picture-o"></i></span></h4>';
                }
                else{
                    echo Apollo_App::renderSectionLabels($photoText, '<span> <i class="fa fa-picture-o"></i></span>');
                }
            ?>
            <div class="a-block-ct-photo">
                <div class="photo-inner">
                    <a href="#" class="fullscreen"> <i class="fa fa-arrows-alt fa-2x"></i></a>

                    <ul class="bxslider">
                        <?php

                        $big_sliders = '';
                        $thumb_sliders = '';
                        $first_desc = '';

                        $popup_big_sliders = '';
                        $popup_thumb_sliders = '';
                        foreach( $gallerys as $k => $attachment_id ) {

                            $post_img = get_post($attachment_id);
                            $caption = !empty($post_img) ? $post_img->post_excerpt : '';

                            if($k === 0) $first_desc = $caption;

                            if ( ! get_attached_file($attachment_id) ) continue;

                            $attach_img   = wp_get_attachment_image_src( $attachment_id, 'large' );
                            $attach_thumb = wp_get_attachment_image_src( $attachment_id, 'thumbnail' );
                            $big_sliders .= '<li><img width="'.$attach_img[1].'" height="'.$attach_img[2].'" src="'.$attach_img[0].'" data-original="'.$attach_img[0].'" ></li>';
                            $thumb_sliders .= '<a data-slide-index="'.$k.'" href="" class="thumbnail-photo" data-desc="' . htmlentities($caption) . '">
                                    <img width="'.$attach_thumb[1].'" height="'.$attach_thumb[2].'" src="'.$attach_thumb[0].'" >
                                </a>';

                            $selected = $k == 0 ? 'selected': '';
                            $p_attach_img   = wp_get_attachment_image_src( $attachment_id, 'full' );
                            $p_attach_thumb = wp_get_attachment_image_src( $attachment_id, 'medium' );

                            $popup_big_sliders .= '<div data-caption="'.($post_img ? $post_img->post_excerpt : '').'" data-srcth="'.$p_attach_thumb[0].'" data-src="'.$p_attach_img[0].'" data-target="'.$k.'" class="photo '.$selected.'"></div>';

                            $current_class = $k == 0 ? 'class="current"' : "";
                            $popup_thumb_sliders .= '<li><a href="#" '.$current_class.' data-id="'.$k.'"><img src="'.$p_attach_thumb[0].'" ></a></li>';
                        }
                        echo $big_sliders;
                        ?>
                    </ul>
                    <ul class="bx-description">
                        <li>
                            <p><?php echo nl2br($first_desc) ?></p>
                        </li>
                    </ul>
                    <div id="bx-pager">
                        <?php echo $thumb_sliders; ?>
                    </div>
                </div>
                <div class="loader"><a><i class="fa fa-spinner fa-spin fa-3x"></i> </a></div>
            </div>

            <!-- Video box pop up -->
            <div  class="photo-box">
                <?php echo $popup_big_sliders; ?>
                <div class="thumb-slider">
                    <ul class="slider-video">
                        <?php echo $popup_thumb_sliders; ?>
                    </ul>
                </div>
                <div class="closevideo"><i class="fa fa-times fa-lg"></i></div>
            </div><!-- End video box pop up -->
        <?php endif; ?>

        <?php
        if ( isset($videos) && $videos && isset( $videos[0] ) && isset( $videos[0]['embed'] ) && $videos[0]['embed'] ):
            $thumbs = '';
            ?>
            <?php  echo Apollo_App::renderSectionLabels(__('VIDEOS', 'apollo'), '<span> <i class="fa fa-film"></i></span>'); ?>
            <div class="a-block-ct-video">
                <?php
                $first_desc = '';
                foreach ( $videos as $k => $v ):

                    if ( $k === 0 ) $first_desc = $v['desc'];

                    if ( ! $v['embed'] ) continue;
                    if(Apollo_App::is_vimeo($v['embed'])  ){
                        $matches= array();
                        if(!strpos($v['embed'], "player")) {
                            preg_match('/\/\/(www\.)?vimeo.com\/(\d+)($|\/)/', $v['embed'], $matches);
                            if (!empty($matches))
                                $v['embed'] = "https://player.vimeo.com/video/" . $matches[2];
                        }
                    }
                    $videoObj = new Apollo_Video($v['embed']);
                    $code = $videoObj->getCode();


                    if ( ! $code ) continue;

                    $vimeoUrl = Apollo_App::is_vimeo($v['embed']) ? $v['embed'] : '';

                    $thumbs .= '<a data-vimeo="'.$vimeoUrl.'" data-desc="'.(nl2br($v['desc'])).'" data-src="'.$videoObj->getSrc().'" data-code="'.$code.'" data-slide-index="'.$k.'" href="">
                    <img class="apl-async-thumb-video" data-original="" data-code="'.$code.'" data-embed="'.$v['embed'].'">
                </a>';

                    if ( $k == 0 ):
                        ?>
                        <div class="video-wrapper">
                            <div class="lazyYT" data-youtube-id="<?php echo $code ?>" id="player">
                                <?php
                                if ( ! $k && Apollo_App::is_vimeo( $v['embed'] ) ) {
                                    ?>
                                    <iframe id="player" frameborder="0" allowfullscreen="1"
                                            title="" width="640" height="360" src="<?php echo $v['embed'] ?>"></iframe>
                                <?php } ?>
                            </div>
                        </div>
                    <?php endif; ?>

                <?php endforeach; ?>
                <div class="blank"></div>
                <ul class="video-description">
                    <li>
                        <p><?php echo nl2br($first_desc) ?></p>
                    </li>
                </ul>
                <div id="video-pager">
                    <?php echo $thumbs ?>
                </div>
                <div class="vd-loader"><a><i class="fa fa-spinner fa-spin fa-3x"></i> </a></div>
            </div>
        <?php endif; ?>

    </div>
    <input type="hidden" name="total-videos" value="<?php echo isset($videos) && $videos ? count( $videos ) : 0 ?>" />
    <input type="hidden" name="total-gallerys" value="<?php echo isset($gallerys) && $gallerys ? count( $gallerys ) : 0 ?>" />

<?php endif; ?>
