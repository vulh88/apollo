<?php
$artists = $_a_event->getAssociatedArtist(true, 1);
if (!empty($artists['data'])):
    ?>
    <div class="blog-bkl" style="margin-top: 20px">
        <div class="a-block">
            <?php echo Apollo_App::renderSectionLabels(__('Associated artists', 'apollo')); ?>
            <div id="apollo-view-more-associated-artists-for-event" class="el-blk">
                <?php
                Apollo_App::getTemplatePartCustom(APOLLO_TEMPLATES_DIR. '/events/html/patials/associated-artist.php',array(
                    'artists' => $artists['data'],
                ),false);
                ?>
            </div>
        </div>
        <div class="clearfix"></div>
        <?php if($artists['have_more']) : ?>
        <div class="load-more b-btn wp-ct-event">
            <a href="javascript:void(0);"
               data-container="#apollo-view-more-associated-artists-for-event"
               data-ride="ap-more"
               data-holder="#apollo-view-more-associated-artists-for-event>:last"
               data-sourcetype="url"
               data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_show_more_artists_associated&page=2&event_id=' . $_a_event->id . '&module=event' ) ?>"
               data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
               data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
               class="btn-b arw ct-btn-sm-event"><?php _e('SHOW MORE', 'apollo') ?>
            </a>
        </div>
        <?php endif; ?>
    </div>
    <div class="clearfix"></div>

<?php endif; ?>
