<?php if(!empty($venue_meta_data_info[Apollo_DB_Schema::_VENUE_PARKING_INFO])) : ?>

<div class="a-block adm">
    <?php  echo Apollo_App::renderSectionLabels(__('PARKING INFO', 'apollo'), '<span> <i class="fa fa-bus fa-2x"></i></span>'); ?>
    <div class="a-block-ct">
        <?php
        $fullContent = Apollo_App::convertContentEditorToHtml($venue_meta_data_info[Apollo_DB_Schema::_VENUE_PARKING_INFO]);
        ?>
        <div class="apl-internal-content">
            <?php echo $fullContent; ?>
        </div>
    </div>
</div>

<?php endif; ?>
