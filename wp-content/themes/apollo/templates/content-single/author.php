<div class=" blog-bkl author-info">
    <h4 class="author-name"><?php echo get_the_author(); ?></h4>
    <acticle id="about">
        <figure class="author-image">
            <?php echo wp_get_attachment_image(get_the_author_meta('image'), 'large');?>
        </figure>
        <div class="author-bio">
            <p><?php the_author_meta('description') ?></p>
        </div>
    </acticle>
</div>
<?php $label = of_get_option(Apollo_DB_Schema::_BLOG_LABEL_RELATED_ARTICLES, 'Related Stories'); ?>
<h2 style="text-align: center"><?php _e(strtoupper($label),'apollo') ?></h2>
<?php
include APOLLO_SHORTCODE_DIR . '/blog/template.php';