<?php
$access  = isset($data['data_access'])?$data['data_access']:'';
if(is_array($access) && count($access) > 0):
?>
<div class="blog-bkl">
    <div class="a-block venue">
        <?php echo Apollo_App::renderSectionLabels(__('Accessibility Information', 'apollo')); ?>
        <ul class="access-listing">
            <?php
                $columns = array(
                    'left' => 'ACLlist',
                    'right' => 'ACRlist'
                );
                foreach($columns as $col => $cssClass){
                    $accessLeft = isset($access[$col])?$access[$col]:array();
                    if(isset($accessLeft) && count($accessLeft) > 0){
                        ?>
                        <li>
                            <ul class="<?php echo $cssClass; ?>">
                                <?php
                                foreach($accessLeft as $item){?>
                                    <li><img src="<?php echo $item['link'] ?>">
                                        <label><?php echo $item['label'] ?></label>
                                    </li>
                                <?php }
                                ?>
                            </ul>
                        </li>
                    <?php
                    }
                }
            ?>
        </ul>
        <?php

          if(isset($data['accessibility_info'] ) && !empty($data['accessibility_info'] )):
        ?>
                <div class="apl-internal-content"><?php echo Apollo_App::convertContentEditorToHtml($data['accessibility_info']) ?></div>
        <?php
          endif; ?>
    </div>
</div>
<?php
    endif;