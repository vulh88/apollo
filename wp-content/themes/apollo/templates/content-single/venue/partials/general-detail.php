<?php
/** @Ticket #13137 */
$image = $venue->get_image_with_option_placeholder('medium', array(),
    array(
        'aw' => true,
        'ah' => true,
    ),
    'normal', '');
?>

<div class="el-blk">
    <div class="art-pic <?php echo !$image ? 'no-place-holder-cate' : ''; ?>">
        <?php echo $image; ?>
    </div>
    <?php include(APOLLO_TEMPLATES_DIR . '/content-single/venue/social.php'); ?>
</div>
<div class="el-blk">
    <div class="art-desc apl-internal-content">
        <?php
        $venue->the_short_desc( $venue->get_content(500, true), $venue->get_full_content(), '_ed_sum_short', '_ed_sum_full' );
        ?>
    </div>
</div>