<?php
$gallerys  = $venue->getListImages();
$videos = $venue->getListVideo();
$videoTemp = array();
foreach($videos as $video){
    $videoTemp[] = array(
        'embed' => $video['embed'],
        'desc' => isset($video['desc'])?base64_decode($video['desc']):'',
    );
}
$videos = $videoTemp;
include APOLLO_TEMPLATES_DIR. '/content-single/common/video-image-gallery.php';
?>
