<div class="blog-bkl organization">
    <div class="b-share-cat art">
        <?php
        $arrShareInfo = $venue->getShareInfo();
        SocialFactory::social_btns( array( 'info' => $arrShareInfo, 'id' => $venue->id,
            'data_btns' => array( 'print', 'sendmail' ) ) );
        ?>
        <input name="eventid" value="<?php echo $venue->id; ?>" type="hidden" />
    </div>
    <h1 class="p-ttl"><?php echo $venue->get_title() ?></h1>

    <?php $venue->renderIcons($data['icon_fields'], $data['icons']); ?>

    <!--Org type -->
    <div class="org-type"><?php echo $venue->generate_categories(); ?>
    </div>
    <!-- Org type -->
    <div class="rating-box rating-action rating-art">
        <?php if( of_get_option(Apollo_DB_Schema::_ENABLE_THUMBS_UP,1)) : ?>
            <div class="like" id="_event_rating_<?php echo $venue->id . '_' . Apollo_DB_Schema::_VENUE_PT ?>" data-ride="ap-event_rating" data-i="<?php echo $venue->id ?>" data-t="<?php echo Apollo_DB_Schema::_VENUE_PT ?>"> <a href="javascript:void(0);"><span class="_count">&nbsp;</span></a></div>
        <?php endif ?>
            <?php if ( $allow_comment ): ?>
                <div class="cm"> <a href="javascript:;"><?php _e( 'Comment', 'apollo' ) ?></a></div>
            <?php endif; ?>

    </div>

    <?php /**
    * @ticket #18657: Change the layout structure for Artist, Organization, Venue
    */
    $generalFile = apply_filters('oc_venue_change_general_detail_template', APOLLO_TEMPLATES_DIR. '/content-single/venue/partials/general-detail.php');
    include $generalFile; ?>
</div>