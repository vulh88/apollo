<!--map -->

    <h4 class="hidden">MAKE IT A NIGHT <span> <i class="fa fa-map-marker"></i></span></h4>
    <div class="a-block-ct">
        <form id="search-map" method="POST" action="" class="search-map-frm hidden"><i class="fa fa-search fa-flip-horizontal"></i>
            <input type="text" placeholder="<?php _e( 'Find Restaurants near this venue...', 'apollo'); ?>">
        </form>
        <div <?php if ( ! $full_address ) echo 'style="display: none"'  ?> class="lo-left">
            <div class="lo-map" id="_event_map"
                 data-coor = "<?php echo $sCoor ?>"
                 data-address="<?php echo $full_address ?>"
                 data-alert="<?php _e('We\'re sorry. A map for this listing is currently unavailable.', 'apollo') ?>"
                 data-relation_id="_popup_google_map_full"
                 data-nonce="<?php echo wp_create_nonce('save-coor') ?>"
                 data-img_location="<?php echo get_stylesheet_directory_uri() ?>/assets/images/icon-location.png">
            </div>
            <p class="t-r"> <a href="javascript:void(0);" data-target="#_popup_google_map" id="_event_trigger_fullmap"><?php _e('Full map and directions','apollo'); ?></a></p>
        </div>
    </div>
    <div class="filter-map hidden">

    </div>
    <div class="nofi-map hidden">
        <p><?php _e('Don’t see your business listed here?','apollo');?><a href="http://" target="_blank"><?php _e('Email us','apollo');?></a>  <?php _e('to add it!','apollo');?></p><a href="javascript:;" data-target="nofi-more" class="nofi-exp"><?php _e('Expand [+]','apollo');?></a>
    </div>
    <div class="nofi-more">
        <div class="n-star"><span> <i class="fa fa-star"></i></span>
            <p><?php _e('ALBUQUERQUE LITTLE THEATRE','apollo');?></p>
        </div>
        <div class="n-blk">
            <label> <span><?php _e('FOOD','apollo');?></span></label>
            <div class="n-blk-ct">
                <ul>
                    <li> <img src="<?php echo get_template_directory_uri() ?>/assets/images/ico-y-knife.png" alt="ico-y-knife.png"><?php _e('Cheese Coffee Caf Downtown','apollo');?> </li>
                    <li> <img src="<?php echo get_template_directory_uri() ?>/assets/images/ico-y-knife.png" alt="ico-y-knife.png"><?php _e('Liu Chinese Fast Food','apollo');?> </li>
                    <li> <img src="<?php echo get_template_directory_uri() ?>/assets/images/ico-y-knife.png" alt="ico-y-knife.png"><?php _e('Vinaigrette','apollo');?></li>
                    <li> <img src="<?php echo get_template_directory_uri() ?>/assets/images/ico-y-knife.png" alt="ico-y-knife.png"><?php _e('Garcias Kitchen The Original','apollo');?> </li>
                    <li> <img src="<?php echo get_template_directory_uri() ?>/assets/images/ico-y-knife.png" alt="ico-y-knife.png"><?php _e('Route 66 Malt Shop','apollo');?> </li>
                    <li> <img src="<?php echo get_template_directory_uri() ?>/assets/images/ico-y-knife.png" alt="ico-y-knife.png"><?php _e('Bottger Mansion of Old Town','apollo');?> </li>
                </ul>
            </div>
        </div>
        <div class="n-blk">
            <label> <span><?php _e('Accommodation','apollo');?></span></label>
            <div class="n-blk-ct">
                <ul>
                    <li> <img src="<?php echo get_template_directory_uri() ?>/assets/images/ico-y-wscreen.png" alt="ico-y-wscreen.png"><?php _e('Bottger Mansion of Old Town','apollo');?></li>
                    <li> <img src="<?php echo get_template_directory_uri() ?>/assets/images/ico-y-wscreen.png" alt="ico-y-wscreen.png"><?php _e('Liu Chinese Fast Food','apollo');?></li>
                </ul>
            </div>
        </div>
        <div class="n-blk">
            <label> <span><?php _e('Drink','apollo');?></span></label>
            <div class="n-blk-ct">
                <ul>
                    <li> <img src="<?php echo get_template_directory_uri() ?>/assets/images/ico-y-cup.png" alt="ico-y-wscreen.png"><?php _e('Bottger Mansion of Old Town','apollo');?> </li>
                    <li> <img src="<?php echo get_template_directory_uri() ?>/assets/images/ico-y-cup.png" alt="ico-y-wscreen.png"><?php _e('Liu Chinese','apollo');?></li>
                    <li> <img src="<?php echo get_template_directory_uri() ?>/assets/images/ico-y-cup.png" alt="ico-y-wscreen.png"><?php _e('Vinaigrette','apollo');?></li>
                </ul>
            </div>
        </div>
    </div>


<!---end map -->