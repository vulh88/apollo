<?php

$lat = $venue->get_meta_data(Apollo_DB_Schema::_VENUE_LATITUDE);
$lng = $venue->get_meta_data(Apollo_DB_Schema::_VENUE_LONGITUDE);

$str_address_all = isset($data['str_address']) ? $data['str_address'] : '';
$full_address = isset($data['address']) ? $data['address'] : $full_address;

if ($lat && $lng) {
    $sCoor = "$lat,$lng";
    $arrCoor = array($lat, $lng);
} else {
    $arrCoor = Apollo_Google_Coor_Cache::getCoorForVenue($full_address);
    if(!is_array($arrCoor) || empty($arrCoor)){
        $arrCoor = Apollo_Google_Coor_Cache::getCoordinateByAddressInLocalDB($full_address);
    }
    $sCoor = $arrCoor ? implode(",", $arrCoor) : '';
}
//@ticket #16193: [Google map] Add a theme option to turn their map section 'off'
$isTurnOffMapSection = of_get_option('turn_off_map_section', 0);
if(!empty($str_address_all) || ($lat && $lng)): ?>
    <div class="blog-bkl">
        <div class="a-block">
            <?php echo Apollo_App::renderSectionLabels(__('LOCATION', 'apollo'), '<span> <i class="fa fa-map-marker"></i></span>'); ?>
        </div>
        <div class="a-block-ct locatn">
            <div class="loc-address"><span><b><?php echo $venue->get_title(); ?></b></span>
                <?php if ($full_address): ?>
                <p><?php echo $full_address; ?></p>
                <?php endif; ?>
            </div>
            <div class="lo-left venue">
                <?php
                    if (!$isTurnOffMapSection){
                        include('map.php');
                    }
                ?>
            </div>
        </div>
    </div>
<?php endif; ?>





