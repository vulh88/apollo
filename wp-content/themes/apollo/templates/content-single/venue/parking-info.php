<?php
$dataParking = isset($data['data_parking'])?$data['data_parking']:array();
if (is_array($dataParking) && count(($dataParking)) > 0) {
    foreach ($dataParking as $k => $item) {
        if (!empty($item)) {
            $title = str_replace('_', ' ', $k);
            $title = strtoupper($title); ?>
            <div class="blog-bkl">
                <div class="a-block">
                    <?php echo Apollo_App::renderSectionLabels($title); ?>
                    <div class="el-blk apl-internal-content">
                        <p><?php echo Apollo_App::convertContentEditorToHtml($item); ?></p>
                    </div>
                </div>
            </div>
            <?php
        }
    }
}