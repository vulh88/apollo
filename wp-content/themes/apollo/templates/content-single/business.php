<?php

/*@ticket #18087: [CF] 20181030 - [ORG][Admin form] Preview Changes button for the business - item 4, 5*/
if (isset($_GET['preview_business']) && $_GET['preview_business'] && !empty($_SESSION['business_data'])) {
    require_once APOLLO_INCLUDES_DIR. '/src/business/inc/APL_Business_Preview.php';
    $business = new APL_Business_Preview($post->ID);
}
else {
    $business = get_business($post->ID);
}

$org = $business->get_org();
$org = get_org($org);
$data = $org->getOrgData();


/** @Ticket #13422 */
$data['title'] = $business->get_title();
$orgAddress = $org->get_full_address();
$lat = $lng = false;
if ($org) {
    $lat = $org->get_meta_data(Apollo_DB_Schema::_ORG_LATITUDE);
    $lng = $org->get_meta_data(Apollo_DB_Schema::_ORG_LONGITUDE);
}

if ($lat && $lng) {
    $sCoor = "$lat,$lng";
    $arrCoor = array($lat, $lng);
} else {
    $arrCoor = Apollo_Google_Coor_Cache::getCoorForVenue($orgAddress);
    if(!is_array($arrCoor) || empty($arrCoor)){
        $arrCoor = Apollo_Google_Coor_Cache::getCoordinateByAddressInLocalDB($orgAddress);
    }
    $sCoor = $arrCoor ? implode(",", $arrCoor) : '';
}

$allow_comment = of_get_option(Apollo_DB_Schema::_ENABLE_COMMENT,1) && comments_open();

/*@ticket #18250 [CF] 20181308 - [0002470 Business] - Modify the breadcrumb*/
$breadcrumbs = $business->generate_categories(false, true);
if(empty($breadcrumbs)){
    $businessLink = home_url('business');
    $businessText = __( 'Business ', 'apollo');
    $breadcrumbs = "<a href='$businessLink'>$businessText</a>";
}
else{
    $breadcrumbs = $breadcrumbs;
}
?>
<div class="breadcrumbs custom-breadcrumbs">
    <ul class="nav">
        <ul class="nav">
            <li><a href="<?php echo home_url() ?>"><?php _e( 'Home', 'apollo' ) ?></a></li>
            <li><?php echo $breadcrumbs ?></li>
            <li> <span><?php echo $business->get_title() ?></span></li>
        </ul>
    </ul>
</div>
<?php
Apollo_Next_Prev::getNavLink($business->id,$post->post_type);
$primary_image_type = of_get_option(Apollo_DB_Schema::_PRIMARY_IMAGE_TYPE_BUSINESS, 'default');
$featureOption = of_get_option(Apollo_DB_Schema::_FEATURE_BUSINESS, 'default');
$blockLabelStyle = of_get_option(Apollo_DB_Schema::_BLOCK_LABEL_STYLE_BUSINESS, 'default') === 'default' ? '' : 'label-style';
?>
<?php
if($primary_image_type === 'default'){
    include( APOLLO_TEMPLATES_DIR. '/content-single/business/general.php');
}else{
    include( APOLLO_TEMPLATES_DIR. '/content-single/business/slider-1.php');
}
include( APOLLO_TEMPLATES_DIR. '/content-single/business/add-fields.php');

include( APOLLO_TEMPLATES_DIR. '/content-single/business/location.php');

/*@ticket #17624 [CF] 20180920 - [Business Detail] Add options to Turn 'On/Off' both block 'Upcoming Events' and 'Nearby Events'*/
if(of_get_option(APL_Business_Module_Theme_Option::_BUSINESS_ENABLE_UPCOMING_EVENTS, 0)){
    include( APOLLO_TEMPLATES_DIR. '/content-single/org/list-event.php');
}

/*@ticket #17624 [CF] 20180920 - [Business Detail] Add options to Turn 'On/Off' both block 'Upcoming Events' and 'Nearby Events'*/
if(of_get_option(APL_Business_Module_Theme_Option::_BUSINESS_ENABLE_NEARBY_EVENTS, 1)){
    include( APOLLO_TEMPLATES_DIR. '/content-single/business/nearby-event.php');
}

include( APOLLO_TEMPLATES_DIR. '/content-single/org/list-classified.php');
include( APOLLO_TEMPLATES_DIR. '/content-single/org/add-fields.php');
include( APOLLO_TEMPLATES_DIR. '/content-single/org/media.php');

/** @Ticket #13250 */
include APOLLO_TEMPLATES_DIR. '/content-single/common/jetpack-related-posts.php';

?>

<div class="blog-bkl astro-featr <?php echo $blockLabelStyle; ?>">
    <div class="a-block" id="event_comment_block">
        <?php
        // If comments are open or we have at least one comment, load up the comment template.
        if ( $allow_comment ) {
            ?>
            <?php echo Apollo_App::renderSectionLabels(__('COMMENTS', 'apollo'), '<span> <i class="fa fa-comment"></i></span>'); ?>
            <div class="a-block-ct">
                <?php
                comments_template('/templates/comments.php');
                ?>
            </div>
            <?php
        }
        ?>
    </div><!-- #comments -->
</div>

