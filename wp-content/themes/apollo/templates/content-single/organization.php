<?php
$org = get_org($post);
$data = $org->getOrgData();
$allow_comment = of_get_option(Apollo_DB_Schema::_ENABLE_COMMENT,1) && comments_open();

/*@ticket #18010: [CF] 20181022 - [Business] Add a new Theme Option for redirecting Org detail URL to Business detail URL - item 2*/
$is_business = $org->get_meta_data(Apollo_DB_Schema::_ORG_BUSINESS,Apollo_DB_Schema::_APL_ORG_DATA);
if ( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_BUSINESS_PT) && $is_business == 'yes' && of_get_option(Apollo_DB_Schema::_ORG_AUTO_REDIRECT_TO_BUSINESS_DETAIL_URL, 0)) {
    $business_link = get_permalink($org->get_meta_data( Apollo_DB_Schema::_APL_ORG_BUSINESS));
    if($business_link){
        wp_redirect($business_link);
        exit();
    }
}

/**
 * @ticket #18672: Display the secondary address fields on the FE organization detail page.
 */
$secondAddress = '';
if(of_get_option(Apollo_DB_Schema::_APL_ENABLE_SECONDARY_ORG_ADDRESS, 0)) {
    $secondAddress = $org->get_full_address(array(
        Apollo_DB_Schema::_SECONDARY_ORG_ADDRESS1,
        Apollo_DB_Schema::_SECONDARY_ORG_ADDRESS2,
        Apollo_DB_Schema::_SECONDARY_ORG_CITY,
        Apollo_DB_Schema::_SECONDARY_ORG_STATE,
        Apollo_DB_Schema::_SECONDARY_ORG_ZIP,
    ), true);
}

$addItButton = $org->renderBookmarkBtn( array( 'class' => 'btn btn-b' ));
?>
<div class="breadcrumbs">
    <ul class="nav">
        <ul class="nav">
            <li><a href="<?php echo home_url() ?>"><?php _e( 'Home', 'apollo' ) ?></a></li>
            <li><a href="<?php echo home_url('organization') ?>"><?php _e( 'Organization ', 'apollo' ) ?></a></li>
            <li> <span><?php echo $org->get_title() ?></span></li>
        </ul>
    </ul>
</div>
<?php

Apollo_Next_Prev::getNavLink($org->id,$post->post_type);
?>
<?php
    include( APOLLO_TEMPLATES_DIR. '/content-single/org/general.php');
    include( APOLLO_TEMPLATES_DIR. '/content-single/org/list-event.php');
    include( APOLLO_TEMPLATES_DIR. '/content-single/org/list-classified.php');
    include( APOLLO_TEMPLATES_DIR. '/content-single/org/add-fields.php');
    include( APOLLO_TEMPLATES_DIR. '/content-single/org/media.php');

    /**
     * @ticket #18657: Change the layout structure for Artist, Organization, Venue
     */
    $contactInfoTemplate = apply_filters('oc_org_add_contact_info', '');
    if($contactInfoTemplate){
        include $contactInfoTemplate;
    }

    /** @Ticket #13250 */
    include APOLLO_TEMPLATES_DIR. '/content-single/common/jetpack-related-posts.php';

?>

<div class="blog-bkl astro-featr">
    <div class="a-block" id="event_comment_block">
        <?php
        // If comments are open or we have at least one comment, load up the comment template.
        if ( $allow_comment ) {
            ?>
            <?php echo Apollo_App::renderSectionLabels(__('COMMENTS', 'apollo'), '<span> <i class="fa fa-comment"></i></span>'); ?>
            <div class="a-block-ct">
                <?php
                comments_template('/templates/comments.php');
                ?>
            </div>
        <?php
       }
        ?>
    </div><!-- #comments -->
</div>

