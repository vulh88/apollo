<?php $post_id = get_the_ID(); ?>
<?php $newsCustomLabel = Apollo_App::getCustomLabelByModuleName(Apollo_DB_Schema::_NEWS_PT); ?>
    <div class="breadcrumbs breadcrumbs-blogdetail">
        <ul class="nav">
            <li><a href="/"><?php _e('Home', 'apollo') ?></a></li>
            <li>
                <span><a href="<?php echo Apollo_App::getCustomUrlByModuleName(Apollo_DB_Schema::_NEWS_PT); ?>"><?php echo $newsCustomLabel; ?></a></span>
            </li>
            <li>
                <?php Apollo_App::apl_get_news_categories($post_id) ?>
            </li>
            <span style="float: left; margin-top: 3px; ;"><?php echo get_the_title(); ?></span>
        </ul>
    </div>

    <div class="blog-bkl apl-internal-content">
        <nav class="blog-list">
            <?php
            if (have_posts()): the_post();

                add_filter('excerpt_length', function () {
                    return Apollo_Display_Config::MAX_SHORT_DESC;
                }, 999);

                add_filter('excerpt_more', function () {
                    return '';
                });
                $title = html_entity_decode(get_the_title());
                $arrShareInfo = array(
                    'url' => get_permalink(),
                    'summary' => get_the_excerpt(),
                    'title' => $title,
                    'media' => wp_get_attachment_thumb_url(get_post_thumbnail_id($post_id))
                );

                ?>
                <li>
                    <?php
                    if (of_get_option(APL_News_Config::_NEWS_DISPLAY_STYLE, 'default') == 'default') {
                        include(APOLLO_TEMPLATES_DIR . '/content-single/news/header.php');
                    }
                    ?>
                    <div class="blog-content">
                        <?php
                        $src = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'large');
                        $caption = get_post(get_post_thumbnail_id($post_id))->post_excerpt;
                        ?>
                        <?php if ($src && isset($src[0])): ?>

                            <div class="blog-pic-detail allow-max-height">
                    <span class="apl-blog-detail-image">
                         <span>
                             <img src="<?php echo $src[0] ?>" class="detail">
                        </span>
                        <?php if (function_exists('the_media_credit_html')) { ?>
                            <p class="media-credit"><?php the_media_credit_html(get_post_thumbnail_id($post_id)); ?></p>
                        <?php } ?>
                    </span>
                            </div>
                            <figcaption class="wp-caption-text"><?php echo $caption; ?></figcaption>
                        <?php endif; ?>
                        <div class="blog-text-detail"><p><?php the_content() ?></p></div>
                    </div>

                    <?php
                    $tags = get_the_tag_list('', ', ');

                    if ($tags):
                        ?>
                        <div class="blog-tags">
                            <label><?php _e('Tags', 'apollo') ?>:</label>
                            <?php echo $tags ?>
                        </div>
                    <?php endif; ?>

                </li>
            <?php endif; ?>

            <?php if (isset($shareBtnsOnDefault)): ?>
                <div class="b-share-cat"><?php echo $shareBtnsOnDefault; ?></div>
            <?php elseif (isset($shareButtons)): ?>
                <div class="b-share-cat blog-category social-blog">
                    <?php if (isset($shareButtons)) echo $shareButtons ?>
                </div>
            <?php endif; ?>

        </nav>
    </div>

<?php
/** @Ticket #13250 */
include APOLLO_TEMPLATES_DIR . '/content-single/common/jetpack-related-posts.php';
?>

    <div class="blog-bkl astro-featr">
        <?php
        // If comments are open or we have at least one comment, load up the comment template.
        if (comments_open() || get_comments_number()) {
            comments_template('/templates/comments.php');
        }
        ?>
    </div>

<?php wp_reset_query(); ?>