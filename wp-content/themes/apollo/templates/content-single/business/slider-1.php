<?php
$image_type = of_get_option(Apollo_DB_Schema::_PRIMARY_IMAGE_TYPE_BUSINESS, 'default');
/** Vandd @ticket #12232*/
$image_size = 'medium';
$image_type_sizes = Apollo_App::get_image_sizes();
if(isset($image_type_sizes['large']) && $image_type_sizes['large']['width'] > Apollo_Display_Config::_PRIMARY_IMAGE_TYPE_MEDIUM_WIDTH_BUSINESS){
    if(isset($image_type_sizes['medium']) && $image_type_sizes['medium']['width'] < Apollo_Display_Config::_PRIMARY_IMAGE_TYPE_MEDIUM_WIDTH_BUSINESS){
        $image_size = 'large';
    }
}else{
    $image_size = 'full';
}
?>
<div class="blog-bkl organization primary-image">
    <div class="b-share-cat art">
        <?php
        $arrShareInfo = $org->getShareInfo();
        SocialFactory::social_btns( array( 'info' => $arrShareInfo, 'id' => $org->id,
            'data_btns' => array( 'print', 'sendmail' ) ) );
        ?>
        <input name="eventid" value="<?php echo $org->id; ?>" type="hidden" />
    </div>
    <h1 class="p-ttl"><?php echo $business->get_title(); ?></h1>

    <?php
    /** @Ticket #13627 */
    $icon_position = of_get_option(Apollo_DB_Schema::_BUSINESS_SHOW_ICON_POSITION, 'after_thumbs_up');
    if ($icon_position == 'after_title'){
        $org->renderIcons($data['icon_fields'], $data['icons']);
    }
    ?>

    <?php
    $businessType = $business->generate_categories();(true);
    if ($businessType):
        ?>
        <!--Org type -->
        <div class="org-type"><?php echo $businessType ; ?></div>
        <!-- Org type -->
    <?php endif; ?>
    <?php
    $enableThumb = of_get_option(Apollo_DB_Schema::_ENABLE_THUMBS_UP,1);
    if($enableThumb || $allow_comment) :
    ?>
    <div class="rating-box rating-action rating-art <?php echo ($data['icons'] ? 'business-show-icons' : ''); ?>">
        <?php if( $enableThumb) : ?>
            <div class="like" id="_event_rating_<?php echo $business->id . '_' . Apollo_DB_Schema::_BUSINESS_PT ?>" data-ride="ap-event_rating" data-i="<?php echo $business->id ?>" data-t="<?php echo Apollo_DB_Schema::_BUSINESS_PT ?>"> <a href="javascript:void(0);"><span class="_count">&nbsp;</span></a></div>
        <?php endif ?>
        <?php if ( $allow_comment ): ?>
            <div class="cm"> <a href="javascript:;"><?php _e( 'Comment', 'apollo' ) ?></a></div>
        <?php endif; ?>
        <?php
        /** @Ticket #13627 */
        if ( $icon_position == 'after_thumbs_up' ) : ?>
            <div class="icons-list-after-thumbsup">
                <?php $org->renderIcons($data['icon_fields'], $data['icons']); ?>
            </div>
        <?php endif;
        ?>
    </div>
    <?php endif; ?>
    <?php
    $primary_image_url = wp_get_attachment_image_src($org->get_image_id(), $image_size);
    ?>
    <?php //get post gallery
    $gallery = $org->getListImages();
    if(sizeof($gallery) == 1 && $gallery[0] == ""){
        $gallery = array();
    }
    $primary_image_id = $org->get_image_id();
    if($primary_image_id != 0){
        array_unshift($gallery, $org->get_image_id());
    }
    if(!empty($gallery)) :
        ?>
        <div class="a-block-ct-photo">
            <div class="photo-inner primary-image <?php echo sizeof($gallery) == 1 ? 'hidden-controls' : ''; ?>">
<!--                <a href="#" class="fullscreen"> <i class="fa fa-arrows-alt fa-2x"></i></a>-->
                <ul class="bxslider">
                    <?php

                    $big_sliders = '';
                    $thumb_sliders = '';
                    if(sizeof($gallery) == 1){
                        $post_img = get_post($gallery[0]);
                        $caption = !empty($post_img) ? $post_img->post_excerpt : '';

                        if (get_attached_file($gallery[0]) ) {
                            $attach_img   = wp_get_attachment_image_src( $gallery[0], $image_size );
                            $big_sliders .= '<li><img width="'.$attach_img[1].'" height="'.$attach_img[2].'" src="'.$attach_img[0].'" data-original="'.$attach_img[0].'" ></li>';
                        }
                    }else{
                        foreach( $gallery as $k => $attachment_id ) {

                            $post_img = get_post($attachment_id);
                            $caption = !empty($post_img) ? $post_img->post_excerpt : '';

                            if ( ! get_attached_file($attachment_id) ) continue;

                            $attach_img   = wp_get_attachment_image_src( $attachment_id, $image_size );
                            $attach_thumb = wp_get_attachment_image_src( $attachment_id, 'thumbnail' );
                            $big_sliders .= '<li><img width="'.$attach_img[1].'" height="'.$attach_img[2].'" src="'.$attach_img[0].'" data-original="'.$attach_img[0].'" ></li>';
                            $thumb_sliders .= '<a data-slide-index="'.$k.'" href="" class="thumbnail-photo" data-desc="' . htmlentities($caption) . '">
                        <img width="'.$attach_thumb[1].'" height="'.$attach_thumb[2].'" src="'.$attach_thumb[0].'" >
                    </a>';

                        }
                    }
                    echo $big_sliders;
                    ?>
                </ul>
                <div id="primary-image-bx-pager">
                    <?php echo $thumb_sliders; ?>
                </div>
            </div>
            <div class="loader"><a><i class="fa fa-spinner fa-spin fa-3x"></i> </a></div>
        </div>

    <?php endif; ?>
    <div class="clearfix"></div>
    <div class="d-content el-blk">
        <?php
            if (empty($gallery)):
                /** @Ticket #13137 */
                $hasPlaceholderImg = of_get_option(Apollo_DB_Schema::_ENABLE_PLACEHOLDER_IMG, 1);
        ?>
        <div class="art-pic <?php echo !$hasPlaceholderImg ? 'no-place-holder-cate' : ''; ?>">
            <img src="<?php echo APOLLO_INCLUDES_URL ?>/admin/assets/images/placeholder-1.png" alt="OIidd's 's" width="300" height="300" class="wp-post-image">
        </div>
        <?php endif; ?>
        <?php
        $business->the_short_desc( $business->get_content(500, true), $business->get_full_content(), '_ed_sum_short', '_ed_sum_full' );
        ?>
        <div class="el-blk">
            <div class="b-btn fl">
                <?php
                /** @Ticket #13517 */
                if (!empty($data[Apollo_DB_Schema::_ORG_DISCOUNT_URL])) :
                    $checkD = of_get_option(Apollo_DB_Schema::_APL_ORGANIZATION_CHECK_DISCOUNTS_TEXT, __('CHECK DISCOUNTS', 'apollo'));
                    ?>
                    <a target="_BLANK"
                       href="<?php echo $data[Apollo_DB_Schema::_ORG_DISCOUNT_URL]; ?>"
                       class="btn btn-b check-discount"><?php echo $checkD ?></a>
                <?php endif; ?>
                <?php echo $org->renderBookmarkBtn( array( 'class' => 'btn btn-b' ) ); ?>
            </div>
        </div>
    </div>
</div>

