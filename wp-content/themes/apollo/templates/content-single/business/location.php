<div class="a-block offers-dates-times hidden" xmlns="http://www.w3.org/1999/html">
    <?php
    echo do_shortcode('[apollo_offer_dates_times]');
    ?>
</div>
<div class="clearfix"></div>


<div class="a-block locatn <?php echo $blockLabelStyle; ?>" id="apl-wnby-block">
    <?php echo Apollo_App::renderSectionLabels(__('LOCATION', 'apollo'), '<span> <i class="fa fa-map-marker"></i></span>'); ?>
    <div class="a-block-ct">
        <div>

            <?php if(!empty($orgAddress)): ?>
                <p><b><?php echo __('Address : ','apollo'); ?></b><?php echo $orgAddress ?></p>
            <?php endif; ?>

        </div>
    </div>
    <div class="clear"></div>
    </br>
    <div class="a-block-ct">
        <form id="search-map" method="POST" action="" class="search-map-frm hidden"><i class="fa fa-search fa-flip-horizontal"></i>
            <input type="text" placeholder="<?php _e( 'Find Restaurants near this venue...', 'apollo'); ?>">
        </form>
        <div <?php if ( ! $orgAddress ) echo 'style="display: none"'  ?> class="lo-left">
            <div class="lo-map" id="_event_map"
                 data-coor = "<?php echo $sCoor ?>"
                 data-address="<?php echo $orgAddress ?>"
                 data-alert="<?php _e('We\'re sorry. A map for this listing is currently unavailable.', 'apollo') ?>"
                 data-relation_id="_popup_google_map_full"
                 data-nonce="<?php echo wp_create_nonce('save-coor') ?>"
                 data-img_location="<?php echo get_stylesheet_directory_uri() ?>/assets/images/icon-location.png">
            </div>
            <p class="t-r"> <a href="javascript:void(0);" data-target="#_popup_google_map" id="_event_trigger_fullmap"><?php _e('Full map and directions','apollo'); ?></a></p>
        </div>
    </div>
</div>


<div id="_popup_google_map" data-active="active">
    <div class="modal-content">
        <div class="copy" id="_popup_google_map_full">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
        <div class="fa fa-close _close" data-owner="#_popup_google_map"></div>
    </div>
    <div class="overlay"  data-owner="#_popup_google_map"></div>
</div>