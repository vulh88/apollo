<?php

$isDisplay = true;
$end_result = '';
$custom_fields = Apollo_Custom_Field::get_group_fields(Apollo_DB_Schema::_BUSINESS_PT);
if ($custom_fields):
    foreach ($custom_fields as $g):
        if ($g['fields']):
            foreach ($g['fields'] as $field):
                if ($field->cf_type == 'multi_checkbox' && $field->name == Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY) {
                    $isDisplay = Apollo_Custom_Field::can_display($field,'dp');
                    $arrCF = array();
                    $CFBusiness = unserialize($field->meta_data);
                    $CFBusiness = explode("\n", $CFBusiness['choice']);
                    foreach ($CFBusiness as $cf) {
                        $cf = explode(":", $cf);
                        $arrTemp = array($cf[0] => $cf[1]);
                        $arrCF = array_merge($arrCF, $arrTemp);
                    }
                }
            endforeach;
        endif;
    endforeach;
endif;

$metaData = $business->getFeaturesMetaKey();

$enableFeatureSec = of_get_option(APL_Business_Module_Theme_Option::_BUSINESS_ENABLE_FEATURE_SEC, 1);

/** @Ticket #13248 */
$featuresLabel = of_get_option(Apollo_DB_Schema::_APL_BUSINESS_SEARCH_WIDGET_LABEL_FEATURES, __( 'Features', 'apollo'));

/**
 * Ticket #17929
 * This cat selects at least a dining
*/

if ( !empty($arrCF) && is_array($arrCF) && $isDisplay && $enableFeatureSec) {
    $q_str = '';
    $featureOption = of_get_option(Apollo_DB_Schema::_FEATURE_BUSINESS, 'default');
    if($featureOption === 'default'){
        if (!$metaData) {
            $end_result = '';
        } else {
            foreach ($arrCF as $key => $value) {
                $key = preg_replace('/\s+/', '', $key);
                if (in_array($key, $metaData)) {
                    $q_str .= '<div class="item "><span><strong>' . $value . ' </strong></span></div>';
                }
            }

            $sectionLabel = Apollo_App::renderSectionLabels($featuresLabel);
            $end_result .= $q_str ? '<div class="blog-bkl more-info feature-business ' . $blockLabelStyle . '">'
                . '<div class="a-block">'
                . $sectionLabel
                . '<div class="des">' . $q_str . '</div>'
                . '</div></div>' : '';
        }
    }else{
        $arrF = array();
        foreach ($arrCF as $key => $value){
            $key = preg_replace('/\s+/', '', $key);
            if($metaData && !empty($metaData) && in_array($key, $metaData)){
                array_push($arrF, trim($value));
            }
        }
        $q_str = empty($arrF) ? '' : join(', ', $arrF);
        $end_result .= $q_str ? '<div class="blog-bkl">'
            . '<div class="d-feature">'
            . '<strong>'. $featuresLabel .'</strong>'
            . '<span>'. $q_str .'</span>'
            . '</div></div>' : '';
    }
}
echo $end_result;




/** @Tikcet #13237 */
if($featureOption != 'default'){
    include( APOLLO_TEMPLATES_DIR. '/content-single/business/information.php');
}
$business->render_cf_detail(Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY, $blockLabelStyle);
