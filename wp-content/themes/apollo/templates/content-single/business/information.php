<div class="blog-bkl d-location">
    <table class="table">
        <tr>
        <?php if (!empty($data['address'])) : ?>
            <td class="d-label"><?php _e('ADDRESS:', 'apollo'); ?></td>
            <td><?php echo isset($data['address']) ? $data['address'] : ''; ?></td>
        <?php endif; ?>
        </tr>
        <tr>
        <?php if (!empty($data['phone'])) : ?>
            <td class="d-label"><?php _e('TEL:', 'apollo'); ?></td>
            <td><?php echo isset($data['phone']) ? $data['phone'] : ''?></td>
        <?php endif; ?>
        </tr>
        <tr>
        <?php if (!empty($data['web'])) : ?>
            <td class="d-label"><?php _e('WEBSITE:', 'apollo'); ?></td>
            <td><?php if (!empty($data['web']) ) : ?><a class="info-item" href="<?php echo isset($data['web']) ? $data['web'] : ''?>" target="_blank"><?php echo isset($data['title']) ? $data['title'] : ''; ?></a><?php endif; ?></td>
        <?php endif; ?>
        </tr>

        <?php
        /** @Ticket #13511 */
        if ( !empty($data['email']) ) : ?>
            <tr>
                <td class="d-label"><?php _e('EMAIL:', 'apollo'); ?></td>
                <td><a class="info-item" href="mailto:<?php echo $data['email']; ?>"><?php echo $data['email']; ?></a> </td>
            </tr>
            <?php
        endif;
        if ( !empty($data['blog']) || !empty($data['facebook']) || !empty($data['twitter']) || !empty($data['inst']) ||
            !empty($data['linked']) || !empty($data['pinterest']) || !empty($data['donate']) ) : ?>
            <tr>
                <td class="d-label apl-social"><?php _e( 'SOCIAL:', 'apollo' ); ?></td>
                <td>
                    <?php
                    if( !empty($data['blog']) ) : ?>
                        <div class="art-social-item"><i class="fa fa-star fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $data['blog'] ?>">
                                <?php _e('Blog','apollo') ?></a>
                            <div class="slash">/</div>
                        </div>
                    <?php endif; ?>

                    <?php
                    if( !empty($data['facebook']) ) : ?>
                        <div class="art-social-item"><i class="fa fa-facebook fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $data['facebook'] ?>">
                                <?php _e('Facebook','apollo') ?></a>
                            <div class="slash">/</div>
                        </div>
                    <?php endif; ?>

                    <?php
                    if( !empty($data['twitter']) ) : ?>
                        <div class="art-social-item"><i class="fa fa-twitter fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $data['twitter'] ?>">
                                <?php _e('Twitter','apollo') ?></a>
                            <div class="slash">/</div>
                        </div>
                    <?php endif; ?>

                    <?php
                    if( !empty($data['inst']) ) : ?>
                        <div class="art-social-item"><i class="fa fa-camera-retro fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $data['inst'] ?>">
                                <?php _e('Instagram','apollo') ?></a>
                            <div class="slash">/</div>
                        </div>
                    <?php endif; ?>

                    <?php
                    if( !empty($data['linked']) ) : ?>
                        <div class="art-social-item"><i class="fa fa-linkedin-square fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $data['linked'] ?>">
                                <?php _e('LinkedIn','apollo') ?></a>
                            <div class="slash">/</div>
                        </div>
                    <?php endif; ?>

                    <?php
                    if( !empty($data['pinterest']) ) : ?>
                        <div class="art-social-item"><i class="fa fa-pinterest fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $data['pinterest'] ?>">
                                <?php _e('Pinterest','apollo') ?></a>
                            <div class="slash">/</div>
                        </div>
                    <?php endif; ?>

                    <?php
                    if( !empty($data['donate']) ) : ?>
                        <div class="art-social-item"><i class="fa fa-usd fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $data['donate'] ?>">
                                <?php _e('Donate','apollo') ?></a>
                            <div class="slash">/</div>
                        </div>
                    <?php endif; ?>

                </td>
            </tr>
        <?php endif; ?>

        <?php
        $hours = $business->getHours();
        $reservations = $business->getReservations();
        $other_info = $business->getOtherInfo();
        ?>
        <?php if(!empty($hours)): ?>
            <tr>
                <td class="d-label"><?php _e('HOURS:', 'apollo'); ?></td>
                <td class="apl-internal-content"> <?php echo Apollo_App::convertContentEditorToHtml($hours) ?></td>
            </tr>
        <?php endif; ?>

        <?php if(!empty($reservations)): ?>
            <tr>
                <td class="d-label"><?php _e('RESERVATIONS:', 'apollo'); ?></td>
                <td class="apl-internal-content"><?php echo Apollo_App::convertContentEditorToHtml($reservations) ?></td>
            </tr>
        <?php endif; ?>

        <?php if(!empty($other_info)): ?>
            <tr>
                <td class="d-label"><?php _e('OTHER INFO:', 'apollo'); ?></td>
                <td class="apl-internal-content"><?php echo Apollo_App::convertContentEditorToHtml($other_info) ?></td>
            </tr>
        <?php endif; ?>
    </table>
</div>