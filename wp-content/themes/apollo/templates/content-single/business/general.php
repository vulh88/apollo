<div class="blog-bkl organization <?php echo $blockLabelStyle; ?>">
    <div class="b-share-cat art">
        <?php
        $arrShareInfo = $org->getShareInfo();
        SocialFactory::social_btns( array( 'info' => $arrShareInfo, 'id' => $org->id,
            'data_btns' => array( 'print', 'sendmail' ) ) );
        ?>
        <input name="eventid" value="<?php echo $org->id; ?>" type="hidden" />
    </div>
    <h1 class="p-ttl"><?php echo $business->get_title(); ?></h1>
    <?php
    /** @Ticket #13627 */
    $icon_position = of_get_option(Apollo_DB_Schema::_BUSINESS_SHOW_ICON_POSITION, 'after_thumbs_up');
    if ($icon_position == 'after_title'){
        $org->renderIcons($data['icon_fields'], $data['icons']);
    }

     ?>

    <?php
    $businessType = $business->generate_categories();
    if ($businessType):
        ?>
        <!--Org type -->
        <div class="org-type"><?php echo $businessType ; ?></div>
        <!-- Org type -->
    <?php endif; ?>

    <div class="rating-box rating-action rating-art <?php echo ($data['icons'] ? 'business-show-icons' : ''); ?>">
        <div class="box-action-wrap">
            <?php if( of_get_option(Apollo_DB_Schema::_ENABLE_THUMBS_UP,1)) : ?>
                <div class="like" id="_event_rating_<?php echo $business->id . '_' . Apollo_DB_Schema::_BUSINESS_PT ?>" data-ride="ap-event_rating" data-i="<?php echo $business->id ?>" data-t="<?php echo Apollo_DB_Schema::_BUSINESS_PT ?>"> <a href="javascript:void(0);"><span class="_count">&nbsp;</span></a></div>
            <?php endif ?>
            <?php if ( $allow_comment ): ?>
                <div class="cm"> <a href="javascript:;"><?php _e( 'Comment', 'apollo' ) ?></a></div>
            <?php endif; ?>
        </div>
        <?php
        /** @Ticket #13627 */
        if ( $icon_position == 'after_thumbs_up' ) : ?>
            <div class="icons-list-after-thumbsup">
                <?php $org->renderIcons($data['icon_fields'], $data['icons']); ?>
            </div>
        <?php endif;
        ?>

    </div>
    <?php
    /** @Ticket #13137 */
    $image = $org->get_image_with_option_placeholder('medium', array(),
        array(
            'aw' => true,
            'ah' => true,
        ),
        'normal', '');
    ?>
    <div class="el-blk">
        <div class="art-pic <?php echo !$image ? 'no-place-holder-cate' : ''; ?>">
            <?php echo $image; ?>
        </div>
        <?php include(APOLLO_TEMPLATES_DIR . '/content-single/org/social.php'); ?>
    </div>
    <div class="el-blk">
        <div class="art-desc apl-internal-content">
            <?php
            $business->the_short_desc( $business->get_content(500, true), $business->get_full_content(), '_ed_sum_short', '_ed_sum_full' );
            ?>
        </div>

        <?php
        $hours = $business->getHours();
        $reservations = $business->getReservations();
        $other_info = $business->getOtherInfo();
        ?>

        <?php
        if($hours || $reservations || $other_info):
            ?>
            <article class="blog-bkl more-info more-info-business">
                <div class="a-block">
                    <div class="des apl-internal-content">
                        <?php if($hours): ?>
                            <div class="item">
                                <span><strong><?php _e('HOURS:', 'apollo'); ?> </strong></span><br/><?php echo Apollo_App::convertContentEditorToHtml($hours) ?>
                            </div>
                        <?php endif; ?>
                        <?php if($reservations): ?>
                            <div class="item">
                                <span><strong><?php _e('RESERVATIONS:', 'apollo'); ?> </strong></span><br/><?php echo Apollo_App::convertContentEditorToHtml($reservations) ?>
                            </div>
                        <?php endif; ?>
                        <?php if($other_info): ?>
                            <div class="item">
                                <span><strong><?php _e('OTHER INFO:', 'apollo'); ?> </strong></span><br/><?php echo Apollo_App::convertContentEditorToHtml($other_info) ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </article>
        <?php endif; ?>
    </div>
</div>
