<?php
include_once APOLLO_TEMPLATES_DIR.'/events/list-events.php';
$list_events_nearby = new List_Event_Adapter( 1, Apollo_Display_Config::PAGESIZE_UPCOM );
$list_events_nearby->get_business_nearby_event( $org->id);
?>
<?php if ( ! $list_events_nearby->isEmpty() ): ?>

    <div    class="blog-bkl <?php echo $blockLabelStyle; ?>">
        <div class="a-block">
        <?php echo Apollo_App::renderSectionLabels(__('Nearby Events', 'apollo')); ?>
        </div>
        <div class="a-block-ct">
            <div id="apollo-view-more-nearby-container"><?php echo $list_events_nearby->render_html( '_org-events.php' ) ?></div>
            <?php if ( $list_events_nearby->isShowMore() ): ?>
                <div class="load-more b-btn">
                    <a href="javascript:void(0);"
                       data-ride="ap-more"
                       data-container="#apollo-view-more-nearby-container"
                       data-holder="#apollo-view-more-nearby-container>:last"
                       data-sourcetype="url"
                       data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_show_more_nearby_business_event&page=2') ?>&current_org_id=<?php echo $org->id; ?>&user_id=<?php echo $org->post->post_author ?>"
                       data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
                       data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
                       class="btn-b arw"><?php _e('SHOW MORE', 'apollo') ?>
                    </a>
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>
