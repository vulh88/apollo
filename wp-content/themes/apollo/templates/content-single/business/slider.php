<?php
$image_type = of_get_option(Apollo_DB_Schema::_PRIMARY_IMAGE_TYPE_BUSINESS, 'default');
/** Vandd @ticket #12232*/
$image_size = 'medium';
$image_type_sizes = Apollo_App::get_image_sizes();
if(isset($image_type_sizes['large']) && $image_type_sizes['large']['width'] > Apollo_Display_Config::_PRIMARY_IMAGE_TYPE_MEDIUM_WIDTH_BUSINESS){
    if(isset($image_type_sizes['medium']) && $image_type_sizes['medium']['width'] < Apollo_Display_Config::_PRIMARY_IMAGE_TYPE_MEDIUM_WIDTH_BUSINESS){
        $image_size = 'large';
    }
}else{
    $image_size = 'full';
}
?>
<div class="blog-bkl organization primary-image">
    <div class="b-share-cat art">
        <?php
        $arrShareInfo = $org->getShareInfo();
        SocialFactory::social_btns( array( 'info' => $arrShareInfo, 'id' => $org->id,
            'data_btns' => array( 'print', 'sendmail' ) ) );
        ?>
        <input name="eventid" value="<?php echo $org->id; ?>" type="hidden" />
    </div>
    <h1 class="p-ttl"><?php echo $business->get_title(); ?></h1>

    <?php $org->renderIcons($data['icon_fields'], $data['icons']); ?>

    <?php
    $businessType = $business->generate_categories();(true);
    if ($businessType):
        ?>
        <!--Org type -->
        <div class="org-type"><?php echo $businessType ; ?></div>
        <!-- Org type -->
    <?php endif; ?>
    <?php
    $enableThumb = of_get_option(Apollo_DB_Schema::_ENABLE_THUMBS_UP,1);
    if($enableThumb || $allow_comment) :
    ?>
    <div class="rating-box rating-action rating-art">
        <?php if( $enableThumb) : ?>
            <div class="like" id="_event_rating_<?php echo $business->id . '_' . Apollo_DB_Schema::_BUSINESS_PT ?>" data-ride="ap-event_rating" data-i="<?php echo $business->id ?>" data-t="<?php echo Apollo_DB_Schema::_BUSINESS_PT ?>"> <a href="javascript:void(0);"><span class="_count">&nbsp;</span></a></div>
        <?php endif ?>
        <?php if ( $allow_comment ): ?>
            <div class="cm"> <a href="javascript:;"><?php _e( 'Comment', 'apollo' ) ?></a></div>
        <?php endif; ?>
    </div>
    <?php endif; ?>
    <?php
    $primary_image_url = wp_get_attachment_image_src($org->get_image_id(), $image_size);
    ?>
    <?php //get post gallery
    $gallery = $org->getListImages();
    if(sizeof($gallery) == 1 && $gallery[0] == ""){
        $gallery = array();
    }
    $primary_image_id = $org->get_image_id();
    if($primary_image_id != 0){
        array_unshift($gallery, $org->get_image_id());
    }
    if(!empty($gallery)) :
        ?>
        <div class="d-slider el-blk">
            <div id="business-slide-big" class="business-slide-big">
                <ul class="slides">
                    <?php foreach ($gallery as $item) :
                        $item_src = wp_get_attachment_image_src($item, $image_size);
                        if($item_src && sizeof($item_src) > 0) :
                            ?>
                            <li><img src="<?php echo $item_src[0]; ?>"></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
            <?php if(sizeof($gallery) > 1) : ?>
            <div id="business-slide-nav" class="business-slide-nav">
                <ul class="slides">
                    <?php foreach ($gallery as $item) :
                        $item_src = wp_get_attachment_image_src($item, 'thumbnail');
                        if($item_src && sizeof($item_src) > 0) :
                            $background = "background-image:url(" . $item_src[0] . ")";
                            ?>
                            <li><div style="<?php echo $background; ?>"></div></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
            <?php endif; ?>
        </div>

    <?php endif; ?>
    <div class="clearfix"></div>
    <div class="d-content el-blk">
        <?php
        $business->the_short_desc( $business->get_content(500, true), $business->get_full_content(), '_ed_sum_short', '_ed_sum_full' );
        ?>
    </div>
</div>

