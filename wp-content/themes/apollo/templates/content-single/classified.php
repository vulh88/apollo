<?php
    $classified = get_classified($post);
    $data = $classified->getDataDetail();
    $allow_comment = of_get_option(Apollo_DB_Schema::_ENABLE_COMMENT,1) && comments_open();
    $classifiedCustomLabel = Apollo_App::getCustomLabelByModuleName(Apollo_DB_Schema::_CLASSIFIED);
?>

<div class="breadcrumbs">
    <ul class="nav">
        <ul class="nav">
            <li><a href="<?php echo home_url() ?>"><?php _e( 'Home', 'apollo' ) ?></a></li>
            <li><a href="<?php echo Apollo_App::getCustomUrlByModuleName(Apollo_DB_Schema::_CLASSIFIED); ?>"><?php echo $classifiedCustomLabel; ?></a></li>
            <li> <span><?php echo $classified->get_title() ?></span></li>
        </ul>
    </ul>
</div>
<?php
include( APOLLO_TEMPLATES_DIR. '/content-single/classified/general.php');
/*@ticket #16639:  Upload PDF files to the FE and Admin Classified forms.*/
include( APOLLO_TEMPLATES_DIR. '/content-single/classified/documents.php');
include( APOLLO_TEMPLATES_DIR. '/content-single/classified/add-fields.php');
include( APOLLO_TEMPLATES_DIR. '/content-single/classified/media.php');

/** @Ticket #13250 */
include APOLLO_TEMPLATES_DIR. '/content-single/common/jetpack-related-posts.php';

?>

<div class="blog-bkl astro-featr">
    <div class="a-block" id="event_comment_block">
        <?php
        // If comments are open or we have at least one comment, load up the comment template.
        if ( $allow_comment ) {
            ?>
            <?php echo Apollo_App::renderSectionLabels(__('COMMENTS', 'apollo'), '<span> <i class="fa fa-comment"></i></span>'); ?>
            <div class="a-block-ct">
                <?php
                comments_template('/templates/comments.php');
                ?>
            </div>
        <?php
        }
        ?>
    </div><!-- #comments -->
</div>
