<?php
global $post, $wp;
$edu = get_educator( $post );
$arrShareInfo = $edu->getShareInfo();

// Get programs

$programs = $edu->get_programs_does_not_expired(); // quocpt change from get_programs to get_programs_does_not_expired method

if(!empty($apolloProgramID)) {
    $prog_id = $apolloProgramID;
} else {
    $prog_id = get_query_var('_apollo_prog_id');
    if (!$edu->is_program_belong_to_educator($prog_id)) {
        $prog_id = empty($prog_id) && $programs ? $programs[0]->prog_id : 0;
    }
}
?>
<input name="eventid" value="<?php echo $edu->id; ?>" type="hidden" />
<div class="breadcrumbs">
    <ul class="nav">
        <li><a href="<?php echo Apollo_App::changeBreadcrumbLink(Apollo_DB_Schema::_EDUCATION) ?>"><?php _e('Home', 'apollo') ?></a></li>
        <li><a href="<?php echo home_url( Apollo_DB_Schema::_EDUCATOR_PT ) ?>"><?php _e( 'Educator', 'apollo' ) ?></a></li>
        <li> <span><?php echo $edu->get_title() ?></span></li>
    </ul>
</div>
<?php
Apollo_Next_Prev::getNavLink($edu->id,$post->post_type);
?>

<article class="content-page">

    <div class="blog-bkl">
        <div class="b-share-cat art">
            <?php
                SocialFactory::social_btns( array( 'info' => $arrShareInfo, 'id' => $edu->id,
                    'data_btns' => array( 'print', 'sendmail' ), 'prog_id' => $prog_id ) );
            ?>
        </div>

        <h1 class="p-ttl"><?php echo $edu->get_title(); ?></h1>
        <?php

            $icon_fields = @unserialize( get_option( Apollo_DB_Schema::_APL_EDUCATORS_ICONS ) );
            $icons = $edu->get_meta_data( Apollo_DB_Schema::_APL_EDUCATOR_POST_ICONS, Apollo_DB_Schema::_APL_EDUCATOR_DATA );
            $edu->renderIcons($icon_fields, $icons);
        ?>

        <div class="el-blk edu-detail">
            <div class="art-pic"><?php echo $edu->get_image('medium') ?></div>

            <?php

                $_e200_excerpt = $edu->get_content(300, true);

                if ( $_e200_excerpt['text'] ):
            ?>
                <div class="edu-detail-desc apl-internal-content" id="_ed_sum_short">
                    <?php echo $_e200_excerpt['text']; ?>
                    <?php if($_e200_excerpt['have_more'] === true): ?>
                        <a href="javascript:void(0);" data-type="vmore" class="vmore" data-target="#_ed_sum_full" data-own='#_ed_sum_short'><?php _e('View more', 'apollo') ?></a>
                    <?php endif; ?>
                </div>
                <div class="edu-detail-desc hidden apl-internal-content" id="_ed_sum_full">
                    <?php echo $edu->get_full_content(); ?>
                    <a href="javascript:void(0);" data-type="vmore" class="vmore" data-target="#_ed_sum_short" data-own='#_ed_sum_full'><?php _e('View less', 'apollo') ?></a>
                </div>
            <?php endif; ?>

        </div>
    </div> <!-- End general info -->

    <?php
    $_arr_contact_info = array(

        Apollo_DB_Schema::_APL_EDUCATOR_EMAIL => array( 'label' => __( 'Email', 'apollo' ), 'icon' => 'fa-envelope' ),
        Apollo_DB_Schema::_APL_EDUCATOR_URL => array( 'label' => __( 'Website', 'apollo' ), 'icon' => 'fa-link' ),
        Apollo_DB_Schema::_APL_EDUCATOR_BLOG => array( 'label' => __( 'Blog', 'apollo' ), 'icon' => 'fa-star' ),
        Apollo_DB_Schema::_APL_EDUCATOR_FB => array( 'label' => __( 'Facebook', 'apollo' ), 'icon' => 'fa-facebook' ),
        Apollo_DB_Schema::_APL_EDUCATOR_TW => array( 'label' => __( 'Twitter', 'apollo' ), 'icon' => 'fa-twitter' ),
        Apollo_DB_Schema::_APL_EDUCATOR_YT => array( 'label' => __( 'Youtube', 'apollo' ), 'icon' => 'fa-youtube' ),
        Apollo_DB_Schema::_APL_EDUCATOR_INS => array( 'label' => __( 'Instagram', 'apollo' ), 'icon' => 'fa-camera-retro' ),
        Apollo_DB_Schema::_APL_EDUCATOR_LK => array( 'label' => __( 'LinkedIn', 'apollo' ), 'icon' => 'fa-linkedin-square' ),
        Apollo_DB_Schema::_APL_EDUCATOR_PTR => array( 'label' => __( 'Pinterest', 'apollo' ), 'icon' => 'fa-pinterest-square' ),
        Apollo_DB_Schema::_APL_EDUCATOR_FLI => array( 'label' => __( 'Flickr', 'apollo' ), 'icon' => 'fa-flickr' ),

    );
    $_contact_str = '';
    foreach( $_arr_contact_info as $k => $v ):
        if ( $val = $edu->get_meta_data( $k, Apollo_DB_Schema::_APL_EDUCATOR_DATA ) ):

            $href   = $val;
            $target = 'target="_blank"';
            if ( $k == Apollo_DB_Schema::_APL_EDUCATOR_EMAIL ) {
                $href   = "mailto:$val";
                $target = '';
            }
            $_contact_str .= <<<EOF
            <p><i class="fa {$v['icon']} fa-lg"></i><a {$target} href="{$href}"><span>{$v['label']}</a></span></p>
EOF;
        endif;
    endforeach;

    $county = $edu->get_meta_data( Apollo_DB_Schema::_APL_EDUCATOR_COUNTY, Apollo_DB_Schema::_APL_EDUCATOR_DATA );
    $_full_address = $edu->get_full_address();

    $phone = $edu->get_meta_data( Apollo_DB_Schema::_APL_EDUCATOR_PHONE1, Apollo_DB_Schema::_APL_EDUCATOR_DATA );
    $website = $edu->get_meta_data( Apollo_DB_Schema::_APL_EDUCATOR_URL, Apollo_DB_Schema::_APL_EDUCATOR_DATA );
    $fax = $edu->get_meta_data( Apollo_DB_Schema::_APL_EDUCATOR_FAX, Apollo_DB_Schema::_APL_EDUCATOR_DATA );

    ?>
    <div class="blog-bkl">
        <div class="a-block">
            <?php
            echo Apollo_App::renderSectionLabels(__('EDUCATOR CONTACT INFO', 'apollo'));
            ?>
            <div class="art-desc">

                <?php if ( $_contact_str || $_full_address || $phone || $fax ): ?>
                <div class="el-blk social">
                    <?php echo $_contact_str ?>

                    <?php if ( $_full_address ): ?>
                    <p><i class="fa fa-map-marker fa-lg"></i><a><?php echo $_full_address?></a></p>
                    <?php endif; ?>

                    <?php if ( $county ): ?>
                    <p><i class="fa fa-university fa-lg"></i><a><?php echo $county ?></a></p>
                    <?php endif; ?>

                    <?php if ( $phone ) : ?>
                    <p><i class="fa fa-phone fa-lg"></i><a><?php echo $phone ?></a></p>
                    <?php endif; ?>

                    <?php if ( $fax ): ?>
                    <p><i class="fa fa-fax fa-lg"></i><a><?php echo $fax ?></a></p>
                    <?php endif; ?>

                </div>
                <?php endif; ?>

                <?php

                    if ( of_get_option( Apollo_DB_Schema::_EDUCATOR_ENABLE_TEACHERS_BTN ) ):
                        $btn_title = of_get_option( Apollo_DB_Schema::_EDUCATOR_TEACHERS_TITLE );
                        if ( ! $btn_title ) {
                            $btn_title = __( 'TEACHERS! APPLY FOR A GRANT FOR THIS PROGRAM', 'apollo' );
                        }
                ?>
                <div class="el-blk">
                    <div class="b-btn t-c"><a href="#" class="btn btn-b"><?php echo $btn_title ?></a></div>
                </div>
                <?php endif; ?>

            </div>
        </div>
    </div> <!-- End contact info -->


    <?php
        if ( $programs  ):

            $prog = get_program($prog_id);

    ?>

    <div class="blog-bkl">
        <div class="a-block">
            <?php echo Apollo_App::renderSectionLabels(__('SELECT A PROGRAM', 'apollo'), false, array('id="program"')); ?>
            <div class="el-blk">
                <div class="select-bkl">
                    <select data-url="<?php echo $edu->get_permalink() ?>" id="edu-prog-link">
                        <option value=""><?php _e( 'Select A Program', 'apollo' ) ?></option>
                        <?php
                            foreach( $programs as $pr ):
                                $_p_pr = get_post( $pr->prog_id );

                                if ( ! $_p_pr || ($_p_pr && $_p_pr->post_status != 'publish') ) continue;
                                $selected = $prog_id == $_p_pr->ID ? 'selected' : '';
                                echo "<option ".$selected." value='$_p_pr->ID'>$_p_pr->post_title</option>";
                            endforeach;
                        ?>
                    </select>
                    <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                </div>
            </div>
        </div>
    </div> <!-- End Select Program -->

    <?php
        if ( $prog->post ):

            $_prog_short_excerpt = $prog->get_content(400);

            // Get videos and gallery
            $videos = $prog->process_videos_data();
            $gallerys = explode( ',' , $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_IMAGE_GALLERY ) );

        if ( get_query_var( '_apollo_prog_id' ) || !empty($apolloProgramID) ):
    ?>
    <script>
        jQuery(function() {
            jQuery('html,body').animate({ scrollTop: jQuery('#program').offset().top }, 1000);
        });
    </script>
    <?php endif; ?>
    <?php if($prog_id ){ ?>
    <div class="blog-bkl educator-content">
        <div class="pgtt-blk">
            <div class="pg-tt">
                <h4><?php echo $prog->get_title() ?></h4>
            </div>
            <div class="triangle-right"><img alt="arrow" src="<?php echo get_template_directory_uri() ?>/assets/images/r-arrow.jpg"></div>
        </div>

        <?php if ( $_prog_short_excerpt['text'] ): ?>
        <div class="pg-bkl">
            <div class="arrow-right"></div>
            <h4><?php _e( 'Program description', 'apollo' ) ?> </h4>
            <div class="des apl-internal-content" id="_prog_short">
                <p>
                    <?php
                        echo $_prog_short_excerpt['text'];
                        if ( $_prog_short_excerpt['have_more'] === true ) echo '...';
                    ?>
                </p>

                <?php if($_prog_short_excerpt['have_more'] === true): ?>
                    <a href="javascript:void(0);" data-type="vmore" class="vmore" data-target="#_prog_full" data-own='#_prog_short'><?php _e('View more', 'apollo') ?></a>
                <?php endif; ?>

            </div>
            <div class="des hidden apl-internal-content" id="_prog_full">
                <?php echo $prog->get_full_content() ?>
            <a href="javascript:void(0);" data-type="vmore" class="vmore" data-target="#_prog_short" data-own='#_prog_full'><?php _e('View less', 'apollo') ?></a>
            </div>
        </div>
        <?php endif; ?>

        <?php
            $p_contact_name = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_CNAME, Apollo_DB_Schema::_APL_PROGRAM_DATA );
            $p_phone = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_PHONE, Apollo_DB_Schema::_APL_PROGRAM_DATA );
            $p_email = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_EMAIL, Apollo_DB_Schema::_APL_PROGRAM_DATA );
            $book_org =  $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_URL, Apollo_DB_Schema::_APL_PROGRAM_DATA );
        ?>
        <div class="pg-bkl">
            <div class="arrow-right"></div>
            <h4><?php _e( 'Booking / scheduling contact', 'apollo' ) ?></h4>
            <div class="des">
                <?php if ( $p_contact_name ): ?>
                <p><i class="fa fa-user fa-lg"></i><a class="vmore none-link"><?php echo $p_contact_name ?></a></p>
                <?php endif; ?>

                <?php if ( $p_phone ): ?>
                <p><i class="fa fa-phone fa-lg"></i><a class="vmore none-link"><?php echo $p_phone ?></a></p>
                <?php endif; ?>

                <?php if ( $p_email ): ?>
                <P><i class="fa fa-envelope fa-lg"></i><a href="mailto:<?php echo $p_email ?>" class="vmore"><?php echo $p_email ?></a></P>
                <?php endif; ?>

                <?php if ( $book_org ): ?>
                <P><i class="fa fa-check-square fa-lg"></i><a target="_blank" href="<?php echo $book_org ?>" class="vmore"><B><?php _e("BOOK THIS PROGRAM", 'apollo') ?></B></a></P>
                <?php endif; ?>
            </div>
        </div>


        <?php

            $_arr_prog_type = array(
                'program-type'  => array( 'label'   => __( 'Program type:', 'apollo' ) ),
            );
            $artistic_discipline_label = of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_ARTISTIC_DISCIPLINE_LABEL) ? of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_ARTISTIC_DISCIPLINE_LABEL) : 'Artistic Discipline';
            if (of_get_option( Apollo_DB_Schema::_EDUCATOR_ENABLE_ARTISTIC_DISCIPLINE_DROP) )
                $_arr_prog_type['artistic-discipline'] = array( 'label'   => __( $artistic_discipline_label.':', 'apollo' ) );
            if (of_get_option( Apollo_DB_Schema::_EDUCATOR_ENABLE_CULTURAL_ORIGIN_DROP) ){
                $_arr_prog_type['cultural-origin'] =  array( 'label'   => __( 'Cultural Origin:', 'apollo' ) );
            }
            if (of_get_option( Apollo_DB_Schema::_EDUCATOR_ENABLE_SUBJECT_DROP) )
                $_arr_prog_type['subject'] =  array( 'label'   => __( 'Subject:', 'apollo' ) );
            if (of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_POPULATION_SERVED_DROP) )
                $_arr_prog_type['population-served'] =  array( 'label'   => __( 'Population served:', 'apollo' ) );
            $prog->get_title();

        ?>
        <div class="pg-bkl taxes">
            <div class="arrow-right"></div>
            <h4><?php _e( 'Program Detail', 'apollo' ) ?></h4>
            <div class="des">
                <?php
                    foreach ( $_arr_prog_type as $k => $v ):
                        if ( $k == 'population-served' ) {
                            $tax_name = $prog->get_population_service_str();
                        } else {
                            $tax_name = $prog->generate_type_string( wp_get_post_terms( $prog_id, $k ), ', ' );
                        }

                        if ( ! $tax_name ) continue;
                ?>
                <div class="item"><span><?php echo $v['label'] ?> </span>
                    <?php echo $tax_name ?>
                </div>
                <?php
                    endforeach;
                ?>

                <?php
                    $available_date = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_DATE, Apollo_DB_Schema::_APL_PROGRAM_DATA );
                    $available_time = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_TIME, Apollo_DB_Schema::_APL_PROGRAM_DATA );
                    $location = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_LOCATION, Apollo_DB_Schema::_APL_PROGRAM_DATA );
                    $fee = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_FEE, Apollo_DB_Schema::_APL_PROGRAM_DATA );
                    $core = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_CORE, Apollo_DB_Schema::_APL_PROGRAM_DATA );
                    $essen = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_ESSENTIAL, Apollo_DB_Schema::_APL_PROGRAM_DATA );
                    $space = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_SPACE_TECHNICAL, Apollo_DB_Schema::_APL_PROGRAM_DATA );

                    $max_student = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_MAX_STUDENTS, Apollo_DB_Schema::_APL_PROGRAM_DATA );
                    $prog_length_info = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_LENGTH_PROGRAM, Apollo_DB_Schema::_APL_PROGRAM_DATA );

                    /** @Task #12782 */
                    $bilingual_option = of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_BILINGUAL, true);

                ?>

                <?php if($bilingual_option) : ?>
                <div class="item"><span><?php _e( 'Bilingual:', 'apollo' ) ?> </span>
                    <?php echo $prog->get_yn( Apollo_DB_Schema::_APL_PROGRAM_IS_BILINGUAL ) ?>
                </div>
                <?php endif; ?>

                <?php if ( $available_date ): ?>
                <div class="item"><i class="fa fa-calendar fa-2x"> </i><span>&nbsp;<a><?php _e( 'Available dates:', 'apollo' ) ?></a></span>
                    <div class="avai-date apl-internal-content"><?php echo Apollo_App::convertContentEditorToHtml($available_date) ?></div>
                </div>
                <?php endif; ?>

                <?php if ( $available_time ): ?>
                <div class="item"><i class="fa fa-clock-o fa-2x"> </i><span>&nbsp;<a><?php _e( 'Available times:', 'apollo' ) ?></a></span>
                    <div class="avai-date apl-internal-content"><?php echo Apollo_App::convertContentEditorToHtml($available_time) ?></div>
                </div>
                <?php endif; ?>

                <?php if ( $max_student ): ?>
                <div class="item"><span><a><?php _e( 'Max number of students:', 'apollo' ) ?></a></span>
                    <?php echo $max_student ?>
                </div>
                <?php endif; ?>

                <?php if ( $prog_length_info ): ?>
                <div class="item"><span><a><?php _e( 'Length of program:', 'apollo' ) ?></a></span>
                    <?php echo $prog_length_info ?>
                </div>
                <?php endif; ?>

                <?php if ( $space ): ?>
                <div class="item"><span><a><?php _e( 'Space / technical requirements:', 'apollo' ) ?></a></span>
                    <div class="apl-internal-content"><?php echo Apollo_App::convertContentEditorToHtml($space) ?></div>
                </div>
                <?php endif; ?>

                <?php if ( $location ): ?>
                <div class="item"><span><?php _e( 'Location(s):', 'apollo' ) ?></span>
                    <div class="apl-internal-content"><?php echo Apollo_App::convertContentEditorToHtml($location) ?></div>
                </div>
                <?php endif; ?>

                <?php if ( $fee ): ?>
                <div class="item"><span><?php _e( 'Fees / Ticketing:', 'apollo' ) ?></span>
                    <div class="apl-internal-content-"><?php echo Apollo_App::convertContentEditorToHtml($fee) ?></div>
                </div>
                <?php endif; ?>

                <?php if ( $core ):
                    if(of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_CORE_FIELD)) {
                        $programCoreLabel = of_get_option(Apollo_DB_Schema::_EDUCATOR_PROGRAM_CORE_LABEL) ? of_get_option(Apollo_DB_Schema::_EDUCATOR_PROGRAM_CORE_LABEL) : 'Program Core'; ?>
                        <div class="item"><span><?php _e( $programCoreLabel.' :', 'apollo' ) ?></span>
                        <div class="apl-internal-content"><?php echo Apollo_App::convertContentEditorToHtml($core) ?></div>
                        </div>
                <?php } endif; ?>

                <?php if ( $essen ):
                    if(of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_ESSENTIALS_FIELD)) {
                        $programEssentialsLabel = of_get_option(Apollo_DB_Schema::_EDUCATOR_PROGRAM_ESSENTIALS_LABEL) ? of_get_option(Apollo_DB_Schema::_EDUCATOR_PROGRAM_ESSENTIALS_LABEL) : 'Program Essentials'; ?>
                        <div class="item"><span><?php _e( $programEssentialsLabel.':', 'apollo' ) ?></span>
                            <div class="apl-internal-content"><?php echo Apollo_App::convertContentEditorToHtml($essen) ?></div>
                            </div>
                <?php } endif; ?>

            </div>
        </div>

        <?php
            // Program custom fields
            $prog->render_cf_detail();

            $meterials = $prog->get_meta_data( 'upload_pdf_related_materials' );
            if ( $meterials ):
        ?>

        <div class="pg-bkl" id="data-prog-meterial">
            <div class="arrow-right"></div>
            <h4><?php _e( 'Supporting Materials', 'apollo' ) ?></h4>
            <div class="des">
                <?php $_all_attach_url = $prog->getAttachFiles($meterials) ?>
            </div>
        </div>

        <?php
            if (!isset($_all_attach_url) || ! $_all_attach_url ) {
                ?>
                <script>
                    jQuery(function($) {
                        $( '#data-prog-meterial' ).remove();
                    });
                </script>
                <?php
            }

        endif;
        $edu->render_cf_detail();
        ?>

        <?php
            $_vg_block_class = 'pg-bkl';
            include APOLLO_TEMPLATES_DIR. '/content-single/common/video-image-gallery.php'
        ?>

        <?php
        $reference = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_REFERENCES, Apollo_DB_Schema::_APL_PROGRAM_DATA );
        $referenceFiles = $prog->get_meta_data( 'upload_pdf_references' );
        if ( $reference || !empty($referenceFiles)):
        ?>
        <div class="pg-bkl taxes">
            <div class="arrow-right"></div>
            <h4><?php _e( 'References', 'apollo' ) ?></h4>
            <div class="des apl-internal-content"><?php echo Apollo_App::convertContentEditorToHtml($reference) ?></div>
            <?php if(!empty($referenceFiles)) : ?>
                <div class="des apl-program-references">
                   <?php $prog->getAttachFiles($referenceFiles) ?>
                </div>
            <?php endif ?>
        </div>
        <?php endif; ?>

        <?php $cancel_policy = $prog->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_CANCEL_POLICY, Apollo_DB_Schema::_APL_PROGRAM_DATA );
        if ( $cancel_policy ):
        ?>
        <div class="pg-bkl taxes">
            <div class="arrow-right"></div>
            <h4><?php _e( 'Cancellation Policy', 'apollo' ) ?></h4>
            <div class="des apl-internal-content"><?php echo Apollo_App::convertContentEditorToHtml($cancel_policy) ?></div>
        </div>
        <?php endif; ?>

    </div>
    <?php } endif; ?>

    <?php endif; ?>

    <div class="line-light"></div>

    <?php /** @Ticket #13250 */
        include APOLLO_TEMPLATES_DIR. '/content-single/common/jetpack-related-posts.php';
    ?>

    <div class="a-block blog-bkl astro-featr" id="event_comment_block">
        <div class="comment-box">
            <?php

                global $post;
                //Default comment is educator
                $commendField = of_get_option(APL_Theme_Option_Site_Config_SubTab::_EDUCATOR_COMMENT_FIELD, 1);

                /**
                 * @ticket #18397: 0002504: Arts Education Customizations - Change logic the comment field of educator - item 5
                 * enable comment on a selected program
                 */
                if(!$commendField && !empty($prog_id)){
                    $currentEducator = $post;
                    $post = $prog->post;
                }
                $allow_comment = of_get_option(Apollo_DB_Schema::_ENABLE_COMMENT,1) && comments_open();

                // If comments are open or we have at least one comment, load up the comment template.
                if ( $allow_comment ) { ?>
                    <?php echo Apollo_App::renderSectionLabels(__('COMMENTS', 'apollo'), '<span> <i class="fa fa-comment"></i></span>'); ?>
                        <div class="a-block-ct">
                    <?php
                    comments_template('/templates/comments.php');

                    /*@ticket #18397: 0002504: Arts Education Customizations - Change logic the comment field of educator - item 5*/
                    if(!empty($currentEducator)) {
                        $post = $currentEducator;
                    }

                  ?>
                        </div>
                <?php
                }
            ?>
        </div>
    </div><!-- #comments -->

</article>