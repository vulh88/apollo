<?php
$audioList =  $org->getListAudios(Apollo_DB_Schema::_APL_ORG_AUDIO);
$gallerys  = $org->getListImages();
$videos = $org->getListVideo();

if (!empty($audioList) || !empty($gallerys[0]) || !empty($videos)) {
?>
<div class="blog-bkl <?php echo isset($blockLabelStyle) ? $blockLabelStyle : ''; ?>">
    <div class="a-block">
        <!--video -->
        <?php include( APOLLO_TEMPLATES_DIR. '/content-single/org/photos-videos.php'); ?>
        <?php include( APOLLO_TEMPLATES_DIR. '/content-single/org/audios.php'); ?>
        <!--end video -->
    </div>
</div>

<?php } ?>