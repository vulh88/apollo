<div class="art-social">
    <div class="el-blk">
        <?php
        if(isset($data['email']) && !empty($data['email'])){
        ?>
        <div class="art-social-item"><i class="fa fa-envelope fa-lg">&nbsp;&nbsp;&nbsp;</i><a href="mailto:<?php echo $data['email'] ?>">
                <?php  _e('Email','apollo') ?></a>
            <div class="slash">/</div>
        </div>
        <?php } ?>

        <?php
        if(isset($data['web']) && !empty($data['web'])){
          if (false === strpos($data['web'], '://')) {
    	    $data['web'] = 'http://' . $data['web'];
    		}
        ?>
        <div class="art-social-item"><i class="fa fa-link fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $data['web'] ?>">
                <?php _e('Website','apollo') ?></a>
            <div class="slash">/</div>
        </div>
        <?php } ?>

        <?php
        if(isset($data['blog']) && !empty($data['blog'])){
        ?>
        <div class="art-social-item"><i class="fa fa-star fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $data['blog'] ?>">
                <?php _e('Blog','apollo') ?></a>
            <div class="slash">/</div>
        </div>
        <?php } ?>


        <?php
        if(isset($data['facebook']) && !empty($data['facebook'])){
        ?>
        <div class="art-social-item"><i class="fa fa-facebook fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $data['facebook'] ?>">
                <?php _e('Facebook','apollo') ?></a>
            <div class="slash">/</div>
        </div>
        <?php } ?>

        <?php
        if(isset($data['twitter']) && !empty($data['twitter'])){
        ?>
        <div class="art-social-item"><i class="fa fa-twitter fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $data['twitter'] ?>">
                <?php _e('Twitter','apollo') ?></a>
            <div class="slash">/</div>
        </div>
        <?php } ?>

        <?php
        if(isset($data['inst']) && !empty($data['inst'])){
        ?>
        <div class="art-social-item"><i class="fa fa-camera-retro fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $data['inst'] ?>">
                <?php _e('Instagram','apollo') ?>  </a>
            <div class="slash">/</div>
        </div>
        <?php } ?>

        <?php
        if(isset($data['linked']) && !empty($data['linked'])){
        ?>
        <div class="art-social-item"><i class="fa fa-linkedin-square fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $data['linked'] ?>">
                <?php _e('LinkedIn','apollo') ?>   </a>
                <div class="slash">/</div>
        </div>
        <?php } ?>

        <?php
        if(isset($data['pinterest']) && !empty($data['pinterest'])){
            ?>
            <div class="art-social-item"><i class="fa fa-pinterest fa-lg">&nbsp;&nbsp;&nbsp;</i><a href="<?php echo $data['pinterest'] ?>">
                    <?php _e('Pinterest','apollo') ?>   </a>
                    <div class="slash">/</div>
            </div>
        <?php } ?>

        <?php
        if(isset($data['donate']) && !empty($data['donate'])){
            ?>
            <div class="art-social-item"><i class="fa fa-usd fa-lg">&nbsp;&nbsp;&nbsp;</i><a target="_BLANK" href="<?php echo $data['donate'] ?>">
                    <?php _e('Donate','apollo') ?>   </a>
                <div class="slash">/</div>
            </div>
        <?php } ?>
    </div>
    <div class="el-blk location">
        <?php if(!empty($data['address'])){ ?>
        <p><span><i class="fa fa-map-marker fa-lg">&nbsp;&nbsp;&nbsp;</i></span>
            <label><?php echo __('Primary address: ', 'apollo') . $data['address']; ?></label>
        </p>
        <?php } ?>
        <?php if(!empty($secondAddress)){ ?>
            <p><span><i class="fa fa-map-marker fa-lg">&nbsp;&nbsp;&nbsp;</i></span>
                <label><?php echo __('Secondary address: ', 'apollo') . $secondAddress; ?></label>
            </p>
        <?php } ?>
        <?php
        if(isset($data['phone']) && !empty($data['phone'])){
        ?>
        <p><span><i class="fa fa-phone fa-lg">&nbsp;&nbsp;</i></span>
            <label><?php echo $data['phone']; ?></label>
        </p>
        <?php } ?>
        <?php
        if(isset($data['fax']) && !empty($data['fax'])){
        ?>
        <p><span><i class="fa fa-fax fa-lg">&nbsp;</i></span>
            <label><?php echo $data['fax']; ?></label>
        </p>
        <?php } ?>
    </div>
    <div class="el-blk">
        <div class="b-btn fl">
            <?php
            /** @Ticket #13517 */
            if (!empty($data[Apollo_DB_Schema::_ORG_DISCOUNT_URL])) :
                $checkD = of_get_option(Apollo_DB_Schema::_APL_ORGANIZATION_CHECK_DISCOUNTS_TEXT, __('CHECK DISCOUNTS', 'apollo'));
            ?>
                <a target="_BLANK"
                   href="<?php echo $data[Apollo_DB_Schema::_ORG_DISCOUNT_URL]; ?>"
                   class="btn btn-b check-discount"><?php echo $checkD ?></a>
            <?php endif; ?>
            <?php echo !empty($addItButton) ? $addItButton : $org->renderBookmarkBtn( array( 'class' => 'btn btn-b' )); ?>
        </div>
    </div>
</div>