<?php
/** @Ticket #13137 */
$image = $org->get_image_with_option_placeholder('medium', array(),
    array(
        'aw' => true,
        'ah' => true,
    ),
    'normal', '');
?>

<div class="el-blk">
    <div class="art-pic <?php echo !$image ? 'no-place-holder-cate' : ''; ?>">
        <?php echo $image; ?>
    </div>
    <?php include(APOLLO_TEMPLATES_DIR . '/content-single/org/social.php'); ?>
</div>
<div class="el-blk">
    <div class="art-desc apl-internal-content">
        <?php
        $org->the_short_desc( $org->get_content(500, true), $org->get_full_content(), '_ed_sum_short', '_ed_sum_full' );
        ?>
    </div>
</div>
