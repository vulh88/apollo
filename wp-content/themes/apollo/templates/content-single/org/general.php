<div class="blog-bkl organization organization-detail-article">
    <div class="b-share-cat art">
        <?php
        $arrShareInfo = $org->getShareInfo();
        SocialFactory::social_btns( array( 'info' => $arrShareInfo, 'id' => $org->id,
            'data_btns' => array( 'print', 'sendmail' ) ) );
        ?>
        <input name="eventid" value="<?php echo $org->id; ?>" type="hidden" />
    </div>
    <h1 class="p-ttl"><?php echo $org->get_title() ?></h1>

    <?php
    $icon_position = of_get_option(Apollo_DB_Schema::_ORGANIZATION_SHOW_ICON_POSITION, '');
    if ($icon_position  == 'after_title') {
        $org->renderIcons($data['icon_fields'], $data['icons']);
    }
    ?>

    <?php 
        $orgType = $org->generate_categories();(true);
        if ($orgType):
    ?>
    <!--Org type -->
    <div class="org-type"><?php echo $orgType ; ?></div>
    <!-- Org type -->
    <?php endif; ?>
    
    <div class="rating-box rating-action rating-art">
        <div class="box-action-wrap">
            <?php if( of_get_option(Apollo_DB_Schema::_ENABLE_THUMBS_UP,1)) : ?>
            <div class="like" id="_event_rating_<?php echo $org->id . '_' . Apollo_DB_Schema::_ORGANIZATION_PT ?>" data-ride="ap-event_rating" data-i="<?php echo $org->id ?>" data-t="<?php echo Apollo_DB_Schema::_ORGANIZATION_PT ?>"> <a href="javascript:void(0);"><span class="_count">&nbsp;</span></a></div>
            <?php endif ?>
            <?php if ( $allow_comment ): ?>
                <div class="cm"> <a href="javascript:;"><?php _e( 'Comment', 'apollo' ) ?></a></div>
            <?php endif; ?>
        </div>

        <?php if ($icon_position == 'after_thumbs_up' || $icon_position == ''): ?>
            <div class="icons-list-after-thumbsup">
                <?php $org->renderIcons($data['icon_fields'], $data['icons']); ?>
            </div>
        <?php endif; ?>

    </div>
    <?php /**
     * @ticket #18657: Change the layout structure for Artist, Organization, Venue
     */
    $generalFile = apply_filters('oc_org_change_general_detail_template', APOLLO_TEMPLATES_DIR. '/content-single/org/partials/general-detail.php');
    include $generalFile;
    ?>
</div>
