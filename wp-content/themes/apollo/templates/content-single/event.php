<?php
// PAGE: EVENT DETAIL
// REPAIR DATA TO FETCH
global $post, $a_event;
$_a_event = !empty($a_event) ? $a_event : get_event($post);

if($_a_event->isPrivateEvent()){
    $routing = of_get_option(Apollo_DB_Schema::_ROUTING_FOR_404_ERROR,'404');
    wp_redirect(home_url($routing));
    exit;
}
$even_end_date = Apollo_App::apollo_get_meta_data($_a_event->id, Apollo_DB_Schema::_APOLLO_EVENT_END_DATE);

/// CONTENT
$characters =  $_a_event->getThemeOption()->getCharacterDescription();
$a_200_content = $_a_event->get_content($characters);

$discountConfig = AplEventFunction::getConfigDiscountIcon();
/** @Ticket #17319 */
$even_dataIcons = $_a_event->getEventDataIcons($discountConfig['display_all_icon'], $_a_event->getThemeOption()->getEventIcons());

$str_address_all = $_a_event->getStrAddress();
$str_address_location = $_a_event->getFullAddress();

// Get org events
include_once APOLLO_TEMPLATES_DIR.'/events/list-events.php';
$org_events_adapter = new List_Event_Adapter(isset($_REQUEST['page']) ? max(1, intval($_REQUEST['page'])) : 1, Apollo_Display_Config::PAGESIZE_UPCOM);

$_venue = get_venue($_a_event->{Apollo_DB_Schema::_APOLLO_EVENT_VENUE});
if (isset($_venue) && $_venue->id) {
    $venue_meta_data_info = Apollo_App::apollo_get_meta_data($_venue->id,Apollo_DB_Schema::_APL_VENUE_DATA);
    if (!is_array($venue_meta_data_info)) {
        $venue_meta_data_info = unserialize($venue_meta_data_info);
    }
}
$sCoor = $_a_event->getCoordinates($_venue);
$staticMapLocation = !empty($sCoor) ? $sCoor : urlencode($str_address_all);

// Get videos and gallery
$videos = $_a_event->getMetaInMainData(Apollo_DB_Schema::_VIDEO);
$gallerys = explode(',', $_a_event->get_meta_data(Apollo_DB_Schema::_APOLLO_EVENT_IMAGE_GALLERY));

$allow_comment = of_get_option(Apollo_DB_Schema::_ENABLE_COMMENT,1) && comments_open();

$isDisplayWhatNearBy = of_get_option(APL_Business_Module_Theme_Option::WHATS_NEARBY_MAP_ACTIVE)
    && Apollo_App::is_avaiable_module(Apollo_DB_Schema::_BUSINESS_PT);
$enable_individual_date_time = $_a_event->getThemeOption()->getEnableIndividualDateTime();
$additionalTime = $_a_event->get_meta_data(Apollo_DB_Schema::_APL_EVENT_ADDITIONAL_TIME, Apollo_DB_Schema::_APOLLO_EVENT_DATA);
?>
<div class="breadcrumbs">
    <ul class="nav">
        <li><a href="<?php echo get_home_url(get_current_blog_id()) ?>"><?php _e('Home', 'apollo') ?></a></li>
        <?php echo $_a_event->get_primary_term_link( '<li>', '</li>' ); ?>
        <li> <span><?php echo $_a_event->get_title() ?></span></li>
    </ul>
</div>
<?php
Apollo_Next_Prev::getNavLink($_a_event->id,$post->post_type);
?>

<article class="content-page astro-detail event-detail-article">

    <?php
    // Include log form for system log
    Apollo_App::includeTemplate(APOLLO_TEMPLATES_DIR. '/partials/log-form.php', array(
        'id'    => $_a_event->id,
        'post_type' => Apollo_DB_Schema::_EVENT_PT
    ));

    ?>
    <div class="b-share-cat">
        <?php echo $_a_event->getSharingInfo('above_title'); ?>
    </div>

    <input name="eventid" value="<?php echo $_a_event->id; ?>" type="hidden"/>
    <div class="page-tool"></div>

    <div class="event-thumbs-up-next-the-title">
        <h1 class="p-ttl">
            <?php echo $_a_event->get_title() ?>
            <?php echo $_a_event->getThumbsUpPosition('next_title') ?>
        </h1>
    </div>

    <?php if( $_a_event->isHasExpiredEvent($even_end_date)) { ?>
        <p class="event-expired error"><i class="fa fa-exclamation-triangle fa-lg"></i><span><?php _e('Please note, this event has expired.','apollo')?></span></p>
    <?php } ?>

    <?php echo $_a_event->renderIconsPosition('after_title', $even_dataIcons); ?>
    <p class="meta auth"><?php echo $_a_event->renderOrgVenueHtml(true); ?></p>

    <?php echo $_a_event->getDateDisplay('plain-text'); ?>

    <div class="apl-wrap-under-presented">
        <div class="b-share-cat">
            <?php echo $_a_event->getSharingInfo('under_present_by'); ?>
        </div>

        <div class="rating-box rating-action">
            <div class="box-action-wrap <?php echo (!$_a_event->getThemeOption()->getThumbsUpEnable() && !$allow_comment) ? 'box-action--empty' : ''; ?>">
                <?php echo $_a_event->getThumbsUpPosition('default') ?>
                <?php if ($allow_comment):?>
                    <div class="cm"><a href="javascript:;"><?php _e('Comment', 'apollo') ?></a></div>
                <?php endif; ?>
            </div>
            <!--    <div class="bok-mk"><a href="javascript:;">Bookmark</a></div>-->
            <?php

            /*@ticket: #17303 0002410: wpdev55 - Requirements part1 - [Page 5] Move Bookmark (save) icon after other share icons*/
            if(of_get_option(Apollo_DB_Schema::_APOLLO_BOOKMARK_BUTTON_LOCATION, 'after_comment') === 'after_comment') {
                $bkIcon = '';
                $bkIcon = apply_filters('octave_render_bookmark_icon', $bkIcon, $_a_event);
                echo $bkIcon;
            }
            ?>
            <?php echo $_a_event->renderIconsPosition('after_thumbs_up', $even_dataIcons); ?>

        </div>
    </div>

    <div class="astro-featr ct-s-c-d-p">
        <article class="blog-itm">
            <?php echo $_a_event->getDateDisplay('default'); ?>
            <div class="pic">
                <?php
                echo $_a_event->getDateDisplay('strip-top');
                echo $_a_event->get_image('medium')
                ?>
            </div>
            <?php
            $a_200_excerpt = $_a_event->get_excerpt(200);

            $a_full_excerpt = $_a_event->get_excerpt();
            if ( $a_200_excerpt['text'] ):
                ?>
                <div class="a-txt-fea" id="_ed_sum_short">
                    <?php echo $_a_event->get_full_excerpt(); ?>
                </div>
            <?php endif; ?>

            <?php if ($_a_event->getThemeOption()->getEventIcons() == 'before_discount_text' || $discountConfig['enable-discount-description']) : ?>
                <div class="apl-event-discount-icon-text">
                    <?php
                    echo $_a_event->renderIconsPosition('before_discount_text', $even_dataIcons);

                    if ($discountConfig['enable-discount-description']) {
                        $_a_event->generateDiscountText($discountConfig['include_discount_url']);
                    }
                    ?>
                </div>
            <?php endif; ?>

            <div class="b-btn __inline_block_fix_space apl-detail-event-button-group">

                <?php
                echo $_a_event->renderTickerBtn(array('class' => 'btn btn-b'));
                echo $_a_event->renderCheckDCBtn(array('class' => 'btn btn-b'));
                $bookmarkIcon = $_a_event->renderBookmarkBtn(array('class' => 'btn-bm btn btn-b'));
                echo apply_filters('octave_custom_bookmark_btn', $bookmarkIcon);
                ?>

                <!-- @ticket #17304: 0002410: wpdev55 - Requirements part1 - [Page 5] Change order of buttons under summary text to -->
                <?php
                $str_address_all = trim($str_address_all);
                if(of_get_option(Apollo_DB_Schema::_ENABLE_MAP_IT_BUTTON, false) && $str_address_all){
                    $target = "#_event_map";
                    if($isDisplayWhatNearBy){
                        $target = "#apl-wnby-block";
                    }
                ?>
                    <a class="btn btn-b" href="<?php echo $target ?>"><?php echo  __('MAP IT', 'apollo'); ?></a>
                <?php } ?>

                <!-- @ticket #17304: 0002410: wpdev55 - Requirements part1 - [Page 5] Change order of buttons under summary text to -->
                <?php if(of_get_option(Apollo_DB_Schema::_ENABLE_ADD_TO_CALENDAR_BUTTON, false)){
                    ?>
                    <a class="btn btn-b add-to-calendar-button"><?php echo __('ADD TO CALENDAR', 'apollo'); ?></a>
                <?php } ?>

            </div>
            <?php if( $_a_event->getThemeOption()->getDisplayWithoutViewMore()) { ?>
                <div class="a-desc apl-internal-content desc-evt " id="_ed_full">
                    <?php echo $_a_event->get_full_content() ?>
                </div>
            <?php }else { ?>
                <div class="a-desc apl-internal-content desc-evt" id="_ed_short">
                    <?php
                        echo $a_200_content['text'];
                    ?>
                    <?php if($a_200_content['have_more'] === true): ?>
                        <a href="javascript:void(0);" data-type="vmore" class="vmore" data-target="#_ed_full" data-own='#_ed_short'><?php _e('View more', 'apollo') ?></a>
                    <?php endif; ?>
                </div>
                <div class="a-desc apl-internal-content hidden desc-evt " id="_ed_full">
                    <?php echo $_a_event->get_full_content() ?>
                    <a href="javascript:void(0);" data-type="vmore" class="vmore" data-target="#_ed_short" data-own='#_ed_full'><?php _e('View less', 'apollo') ?></a>
                </div>
            <?php } ?>
            <div class="clearfix"></div>
            <?php
            // Add additional datetime info to admission info block if $enable_individual_date_time = false
            include APOLLO_TEMPLATES_DIR. '/content-single/events/admission.php';

            if($enable_individual_date_time){
                include APOLLO_TEMPLATES_DIR. '/content-single/events/individual-date-time.php';
            }

            include APOLLO_TEMPLATES_DIR. '/content-single/common/video-image-gallery.php';
            include APOLLO_TEMPLATES_DIR. '/content-single/events/location.php';
            if ($isDisplayWhatNearBy) {
                include APOLLO_TEMPLATES_DIR. '/content-single/events/what-near-by.php';
            }

            include APOLLO_TEMPLATES_DIR. '/content-single/events/parking-info.php';

            include APOLLO_TEMPLATES_DIR. '/content-single/events/accessibility.php';
            include APOLLO_TEMPLATES_DIR. '/content-single/events/events-org.php';
            include APOLLO_TEMPLATES_DIR. '/content-single/events/associated-artists.php';

            ?>

            <div class="a-block review hidden "> <!-- @todo TuanPHPVN hidden here -->
                <h4>MEMBER REVIEWS<span> <i class="fa fa-user"></i></span></h4>
                <div class="a-block-ct">
                    <p class="t-c">There are currently no reviews/comments for this event. Be the first to add a review/comment , and let folks know what you think!</p>
                </div>
            </div>

            <?php /** @Ticket #13250 */
                include APOLLO_TEMPLATES_DIR. '/content-single/common/jetpack-related-posts.php';
            ?>

            <div class="a-block" id="event_comment_block">
                <?php
                // If comments are open or we have at least one comment, load up the comment template.
                if ( $allow_comment ) {
                    ?>
                    <?php echo Apollo_App::renderSectionLabels(__('COMMENTS', 'apollo'), '<span> <i class="fa fa-comment"></i></span>');  ?>
                    <div class="a-block-ct">
                        <?php
                        comments_template('/templates/comments.php');
                        ?>
                    </div>
                    <?php
                }
                ?>
            </div><!-- #comments -->

            <?php require_once APOLLO_TEMPLATES_DIR.'/events/google-map-popup.php' ?>
            <?php require_once APOLLO_TEMPLATES_DIR.'/events/chooes-export-popup.php' ?>
        </article>
    </div>
</article>