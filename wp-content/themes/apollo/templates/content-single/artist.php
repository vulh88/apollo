<?php
    // PAGE: ARTIST DETAIL
    global $post;
    $artist = get_artist( $post );

    if (!$artist->is_linked()) {
        wp_safe_redirect('/');
    }

    $arrShareInfo = $artist->getShareInfo();
    
    // Get videos and gallery
    $videos = $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_VIDEO, Apollo_DB_Schema::_APL_ARTIST_DATA );
    $gallerys = explode( ',' , $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_IMAGE_GALLERY ) );
    $allow_comment = of_get_option(Apollo_DB_Schema::_ENABLE_COMMENT,1) && comments_open();

    $addItBtn = $artist->renderBookmarkBtn( array( 'class' => 'btn-bm btn btn-b' ) );
    $email = $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_DISPLAY_EMAIL, Apollo_DB_Schema::_APL_ARTIST_DATA ) != 'yes' ?
        $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_EMAIL, Apollo_DB_Schema::_APL_ARTIST_DATA ) : '';
    $website = $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_WEBURL, Apollo_DB_Schema::_APL_ARTIST_DATA );

    if ($website && false === strpos($website, '://')) {
        $website = 'http://' . $website;
    }

    $blog = $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_BLOGURL, Apollo_DB_Schema::_APL_ARTIST_DATA );
    $fb = $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_FB, Apollo_DB_Schema::_APL_ARTIST_DATA );
    $tw = $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_TW, Apollo_DB_Schema::_APL_ARTIST_DATA );
    $ins = $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_INS, Apollo_DB_Schema::_APL_ARTIST_DATA );
    $lk = $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_LK, Apollo_DB_Schema::_APL_ARTIST_DATA );
    $pr = $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_PR, Apollo_DB_Schema::_APL_ARTIST_DATA );

    $display_phone = $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_DISPLAY_PHONE, Apollo_DB_Schema::_APL_ARTIST_DATA );
    $contact_info = $display_phone != 'yes' ? $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_PHONE, Apollo_DB_Schema::_APL_ARTIST_DATA ):'';

    $display_address = $artist->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_DISPLAY_ADDRESS, Apollo_DB_Schema::_APL_ARTIST_DATA );
    $address = $display_address != 'yes' ? $artist->get_full_address() : '';

?>

<div class="breadcrumbs">
    <ul class="nav">
        <li><a href="<?php echo home_url() ?>"><?php _e( 'Home', 'apollo' ) ?></a></li>
        <li>
            <a href="<?php echo Apollo_App::getCustomUrlByModuleName(Apollo_DB_Schema::_ARTIST_PT); ?>">
                <?php echo Apollo_App::getCustomLabelByModuleName(Apollo_DB_Schema::_ARTIST_PT) ?>
            </a>
        </li>
        <li> <span><?php echo $artist->get_title() ?></span></li>
    </ul>
</div>
<?php
Apollo_Next_Prev::getNavLink($artist->id,$post->post_type);
?>

<?php

    include APOLLO_TEMPLATES_DIR. '/content-single/artists/general.php';

    include APOLLO_TEMPLATES_DIR. '/content-single/artists/documents.php';

    if ($artist->is_public()){
        include( APOLLO_TEMPLATES_DIR. '/content-single/artists/list-public-art.php');
    }

    include APOLLO_TEMPLATES_DIR. '/content-single/artists/more-about.php';

    echo '<div class="blog-bkl video-wrapper">';
    include APOLLO_TEMPLATES_DIR. '/content-single/common/video-image-gallery.php';
    echo '</div>';

    /**
     * @ticket #18657: Change the layout structure for Artist, Organization, Venue
     */
    $contactTemplate = apply_filters('oc_artist_add_contact_info', '');
    if($contactTemplate){
        include_once $contactTemplate;
    }
    /** @Ticket #13452 */
    include APOLLO_TEMPLATES_DIR. '/content-single/artists/upcomming-past.php';

    //include APOLLO_TEMPLATES_DIR. '/content-single/artists/question.php';
    include APOLLO_TEMPLATES_DIR. '/content-single/artists/audios.php';

    /** @Ticket #13250 */
    include APOLLO_TEMPLATES_DIR. '/content-single/common/jetpack-related-posts.php';
?>


<div class="blog-bkl astro-featr">
    <div class="a-block" id="event_comment_block">
        <?php
        // If comments are open or we have at least one comment, load up the comment template.
        if ( $allow_comment ) {
            ?>
            <?php echo Apollo_App::renderSectionLabels(__('COMMENTS', 'apollo'), '<span> <i class="fa fa-comment"></i></span>'); ?>
                <div class="a-block-ct">
            <?php 
            comments_template('/templates/comments.php');
            ?>
                </div>        
        <?php     
        }
        ?>
    </div><!-- #comments -->
</div>
