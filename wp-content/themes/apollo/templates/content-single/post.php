<?php
    $blogCustomLabel = Apollo_App::getCustomLabelByModuleName(Apollo_DB_Schema::_BLOG_POST_PT);

    /**
     * @Ticket #18768
     */
    $enableThumbUp = of_get_option(APL_Theme_Option_Site_Config_SubTab::_BLOG_ENABLE_THUMBS_UP, 1);
?>
<div class="breadcrumbs breadcrumbs-blogdetail">
    <ul class="nav">
      <li><a href="<?php  echo Apollo_App::changeBreadcrumbLink(Apollo_DB_Schema::_BLOG_POST_PT) ?>" ><?php _e( 'Home', 'apollo' ) ?></a></li>
      <li><span><a href="<?php echo Apollo_App::getCustomUrlByModuleName(Apollo_DB_Schema::_BLOG_POST_PT); ?>"><?php echo $blogCustomLabel; ?></a></span></li>
      <li> <?php the_category(', ') ?></li>
      <span style="float: left; margin-top: 3px; ;"><?php echo get_the_title(); ?></span>
    </ul>
</div>
<?php
$post_id = get_the_ID();
?>

<div class="blog-bkl apl-internal-content">

    <?php
    $blogPageContent = Apollo_App::get_post_info(Apollo_Page_Creator::ID_BLOG,'page');
    if($blogPageContent){
        $content = $blogPageContent->post_content;
        $content = apply_filters( 'the_content', $content );
        $content =  str_replace( ']]>', ']]&gt;', $content );
        ?>
        <div class="art-desc apl-internal-content">
            <?php  echo  $content; ?>
        </div>
        <?php
    }
    ?>
    <nav class="blog-list">
        <?php
//        $enableDate   = of_get_option( Apollo_DB_Schema::_BLOG_ENDABLE_DATE, 1 );
//        $enableAuthor = of_get_option( Apollo_DB_Schema::_BLOG_ENDABLE_AUTHOR, 1 );
//        $enableThumbUp = of_get_option( Apollo_DB_Schema::_BLOG_ENDABLE_THUMB_UP, 1 );
//        $enableCommentNumber = of_get_option( Apollo_DB_Schema::_BLOG_ENDABLE_COMMENT, 1 );
        if ( have_posts() ): the_post();

           add_filter( 'excerpt_length', function() {
               return Apollo_Display_Config::MAX_SHORT_DESC;
           }, 999 );

           add_filter( 'excerpt_more', function () {return '';});
            $title = html_entity_decode(get_the_title());
           $arrShareInfo = array(
               'url' => get_permalink(),
               'summary' => get_the_excerpt(),
               'title' => $title,
               'media' => wp_get_attachment_thumb_url( get_post_thumbnail_id( $post_id ) )
           );

           ?>
        <li>
            <?php
                if( of_get_option(Apollo_DB_Schema::_BLOG_DISPLAY_STYLE,'default') == 'default'){
                    include(APOLLO_TEMPLATES_DIR.'/content-single/post/header.php');
                }
                if( of_get_option(Apollo_DB_Schema::_BLOG_DISPLAY_STYLE,'default') == 'header-new'){
                    include(APOLLO_TEMPLATES_DIR.'/content-single/post/header-style-two.php');
                }
            ?>
            <div class="blog-content">
                <?php
                     $src = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'large');
                    $caption =  get_post(get_post_thumbnail_id($post_id))->post_excerpt;
                ?>
                <?php if ( $src && isset($src[0]) ):?>

                <div class="blog-pic-detail allow-max-height">
                    <span class="apl-blog-detail-image">
                         <span>
                         <?php
                         /*@ticket #7547 */
                         $sponsoredContent = get_post_meta($post_id,'post-sponsored-content',true);
                         if ($sponsoredContent && $sponsoredContent === 'on'){
                             echo "<div  class='sponsored-content-text-single'>" .  __('SPONSORED CONTENT', 'apollo') . "</div>";
                         }
                         ?>
                             <img src="<?php echo $src[0] ?>" class="detail">
                        </span>
                        <?php
                        /* @Ticket #19541 */
                        if(!empty($caption)) : ?>
                            <figcaption class="wp-caption-text"><?php echo $caption; ?></figcaption>
                        <?php endif; ?>
                        <?php if (  function_exists( 'the_media_credit_html' ) ) { ?>
                            <p class="media-credit"><?php the_media_credit_html( get_post_thumbnail_id( $post_id ) );?></p>
                        <?php } ?>
                    </span>
                </div>
                <?php endif; ?>
                <div class="blog-text-detail"><p><?php the_content() ?></p></div>
            </div>

            <?php
                $tags = get_the_tag_list( '', ', ' );

                if ( $tags ):
            ?>
            <div class="blog-tags">
                <label><?php _e( 'Tags', 'apollo' ) ?>:</label>
                <?php echo $tags ?>
            </div>
            <?php endif; ?>

        </li>
        <?php endif; ?>

        <?php if (isset($shareBtnsOnDefault)): ?>
            <div class="b-share-cat"><?php echo $shareBtnsOnDefault;?></div>
        <?php elseif(isset($shareButtons)): ?>
            <div class="b-share-cat blog-category social-blog">
                <?php if(isset($shareButtons)) echo $shareButtons ?>
            </div>
        <?php endif; ?>

    </nav>
</div>

<?php
    /** @Ticket #13250 */
    include APOLLO_TEMPLATES_DIR. '/content-single/common/jetpack-related-posts.php';
    /*@ticket #17681 */
    apply_filters('apl_render_blog_detail_footer', '');
?>

<div class="blog-bkl astro-featr">
    <?php
    // If comments are open or we have at least one comment, load up the comment template.
    if ( comments_open() || get_comments_number() ) {
        comments_template('/templates/comments.php');
    }
    ?>
</div>

<?php wp_reset_query(); ?>