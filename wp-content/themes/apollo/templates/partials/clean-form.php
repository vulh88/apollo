<?php
    // Fix redirect classified page
    if ( isset( $_GET['redirect']) && !empty( $_GET['redirect'])) {
        $url = urldecode($_GET['redirect']);
        if (!empty($_GET['action']) && !empty($_GET['post_type'])) {
            /** @Ticket #13286*/
            $_SESSION['save_'. $_GET['post_type']] = $_GET['action'];
        }
        ?>
        <script>
            window.location.href ="<?php echo $url; ?>"
        </script>
    <?php }
    ?>
