<form style="display: none" method="post" data-ride="log">
    <input name="action" value="apollo_system_log" />
    <input name="id" value="<?php echo isset($id) ? $id : ''; ?>" >
    <input name="post_type" value="<?php echo isset($post_type) ? $post_type : '' ?>" >
    <?php wp_nonce_field( 'system_log', 'nonce_field' ); ?>
</form>