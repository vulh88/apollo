<?php
$id = $_a_event->id;
$imageUrl = $_a_event->getImageLink();
$title = $_a_event->get_title(true);
?>
<meta property="og:image" content="<?php echo $imageUrl ?>"/>
<meta property="og:title" content="<?php echo  $title ?>"/>
<meta property="og:site_name" content="<?php get_bloginfo('name') ?>"/>
<meta property="og:url" content="<?php echo $_a_event->get_permalink() ?>"/>