<?php

// no need to display because:
//  + user hasn't re-active the theme to use this feature
//  + or leave as normal is active
if ( !isset($datas->associated_venues) || $datas->leave_as_normal ) {
    return;
}

/**
 * @ticket #11493: display associated venues
 */
include_once APOLLO_INCLUDES_DIR . '/src/venue/inc/Apollo_List_Associated_Venue.php';

$description      = $datas->associated_venues_desc;
$associatedVenues = !is_array($datas->associated_venues) ? maybe_unserialize($datas->associated_venues) : $datas->associated_venues;
$list             = new Apollo_List_Associated_Venue($associatedVenues);

?>

<?php if ( $associatedVenues ) : ?>
    <div class="evt-blk event-associated-venues">
        <div class="event-contact"><?php _e( 'Associated Venues', 'apollo' ); ?></div>

        <!-- Display associated venues description -->
        <?php if ( $description ): ?>
            <div class="apl-internal-content">
                <p><?php echo Apollo_App::convertContentEditorToHtml($description, TRUE) ?></p>
            </div>
        <?php endif; ?>

        <!-- Display associated venues list -->
        <div class="search-bkl apollo-associated-venues-list">
            <section class="list-more-category">
                <?php if ( !$list->isEmpty() ): ?>
                    <div id="apollo-view-more-associated-venues-container">
                        <?php echo $list->renderVenueHtml(); ?>
                    </div>
                <?php endif; ?>

                <?php if ( $list->isShowMore() ): ?>
                    <div class="load-more b-btn">
                        <a href="javascript:void(0);"
                           data-ride="ap-more"
                           data-holder="#apollo-view-more-associated-venues-container>:last"
                           data-sourcetype="url"
                           data-container=".apollo-associated-venues-list"
                           data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_show_more_associated_venues&page=2&term_id=' . $datas->term_id) ?>"
                           data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
                           data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
                           class="btn-b arw"><?php _e('SHOW MORE', 'apollo') ?>
                        </a>
                    </div>
                <?php endif; ?>
            </section>
        </div>
    </div>
<?php endif; ?>

