<?php
// REPAIR DATA - JUST SHOW SIMPLE EVENT
if(!empty($datas)):

$_a_event = get_event($datas);

/// STARTDATE-ENDDATE
$_arr_show_date = $_a_event->getShowTypeDateTime('picture');

///SOCIAL SHARE INFO
$arrShareInfo = $_a_event->getShareInfo();

/* number vote */
$ivote = $_a_event->getVote();

$length_summary = Apollo_Display_Config::EVENT_OVERVIEW_NUMBER_CHAR;
?>
<article class="content-page category-detail category-page-event-box">
    <div class="b-share-cat">
        <?php echo $_a_event->getSharingInfo('top'); ?>
        <input name="eventid" value="<?php echo $_a_event->id; ?>" type="hidden" />
    </div>

    <div class="event-thumbs-up-next-the-title">
        <h1 class="p-ttl">
            <a href="<?php echo $_a_event->get_permalink() ?>"><?php echo $_a_event->get_title(TRUE) ?></a>
            <?php echo $_a_event->getThumbsUpPosition('next_title') ?>
        </h1>
    </div>

    <?php echo $_a_event->renderIconsPosition('after_title'); ?>

    <p class="meta auth"><?php echo $_a_event->renderOrgVenueHtml(true) ?></p>

    <?php echo $_a_event->getDateDisplay('plain-text'); ?>

    <?php if( $_a_event->getThemeOption()->getThumbsUpEnable() ) : ?>
    <div class="cat-rating-box rating-action">
        <?php if( $_a_event->getThemeOption()->getThumbsUpPosition() == 'default' ) : ?>
        <div class="box-action-wrap">
            <?php echo $_a_event->getThumbsUpPosition('default'); ?>
        </div>
        <?php endif; ?>
        <?php
            $bkIcon = '';
            $bkIcon = apply_filters('octave_render_bookmark_icon', $bkIcon, $_a_event);
            echo $bkIcon;
        ?>
        <?php echo $_a_event->renderIconsPosition('after_thumbs_up'); ?>
    </div>
    <?php endif ?>
    <div class="pic">

        <a href="<?php echo $_a_event->get_permalink() ?>">
            <span class="date">
             <?php
             /*@ticket #17730*/
             echo $_a_event->getDateDisplay('default');
             echo $_a_event->getDateDisplay('strip-top');
             ?>
            </span>
            <?php
            /** @Ticket #16332 - change image size for Octave theme*/
                $spotlightImageSize = 'medium';
                $spotlightImageSize = apply_filters('_apl_taxonomy_custom_category_spotlight_size', $spotlightImageSize);
            ?>
            <?php echo $_a_event->get_image($spotlightImageSize, array(), array(
                    'aw' => true,
                    'ah' => true,
                )) ?></a>
    </div>
    <div class="b-btn cat-detail __inline_block_fix_space">
        <?php echo $_a_event->renderTickerBtn(array(
                'class' => 'btn btn-cat-detail'
            )) ?>

        <?php echo $_a_event->renderCheckDCBtn(array(
                'class' => 'btn btn-cat-detail'
            )) ?>

        <?php
        $bookmarkIcon = $_a_event->renderBookmarkBtn(array(
                'class' => 'btn btn-cat-detail btn-bm'
            ));
        echo apply_filters('octave_custom_bookmark_btn', $bookmarkIcon);
        ?>
    </div>
    <p class="desc">
        <?php
        $summary = $_a_event->get_summary($length_summary );
        echo $summary['text'] ?> <?php if($_a_event->is_have_more_summary($length_summary )): ?>...<?php endif ?>
    </p>
    <?php if($_a_event->is_have_more_summary($length_summary )): ?>
        <a href="<?php echo $_a_event->get_permalink() ?>" class="vmore" ><?php _e('View more', 'apollo') ?></a>
    <?php endif; ?>
    <?php require_once APOLLO_TEMPLATES_DIR.'/events/google-map-popup.php' ?>
    <?php require_once APOLLO_TEMPLATES_DIR.'/events/chooes-export-popup.php' ?>

</article>
<?php endif; ?>
