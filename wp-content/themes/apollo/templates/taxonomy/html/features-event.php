<?php if(count($datas) > 0):
    $spotlightStyle = of_get_option(Apollo_DB_Schema::_EVENT_CATEGORY_SPOTLIGHT_STYLE, 'default')
    ?>
    <section class="list-category <?php echo $spotlightStyle == 'default' ? 'spotlight-left' : '' ?>">

    <?php
        $discountConfig = AplEventFunction::getConfigDiscountIcon();

    foreach($datas as $data):
        $_a_event = get_event($data);

        $this->set_displayed_event( ( int ) $_a_event->id );

        /// STARTDATE-ENDDATE
        $_arr_show_date = $_a_event->getShowTypeDateTime('picture');
        $length_summary = Apollo_Display_Config::EVENT_OVERVIEW_NUMBER_CHAR;
        $icon_fields = $_a_event->getThemeOption()->getEventIcons();
        $even_dataIcons = $_a_event->getEventDataIcons($discountConfig['display_all_icon'], $icon_fields);
    ?>
    <article class="category-itm category-page-event-box <?php echo $_a_event->getThemeOption()->getDateDisplay() == 'strip-top' ? 'apl-date-strip-top' : ''?>">

        <?php echo $_a_event->getDateDisplay('default'); ?>
        <div class="pic">
            <?php  echo $_a_event->getDateDisplay('strip-top'); ?>
            <a href="<?php echo $_a_event->get_permalink() ?>"><?php echo $_a_event->get_image('medium', array(), array(
                        'aw' => true,
                        'ah' => true,
                    )) ?></a></div>
        <div class="category-t">
            <div class="event-thumbs-up-next-the-title">
                <h2 class="category-ttl">
                    <a href="<?php echo $_a_event->get_permalink() ?>"> <?php echo $_a_event->get_title(true) ?></a>
                    <?php echo $_a_event->getThumbsUpPosition('next_title') ?>
                </h2>
            </div>

            <?php echo $_a_event->renderIconsPosition('after_title', $even_dataIcons); ?>

            <p class="meta auth">
                <?php echo $_a_event->renderOrgVenueHtml(true) ?>
            </p>

            <?php echo $_a_event->getDateDisplay('plain-text'); ?>

            <?php if( $_a_event->getThemeOption()->getThumbsUpEnable() ) : ?>
            <div class="cat-rating-box rating-action">
                <?php if ( $_a_event->getThemeOption()->getThumbsUpPosition() == 'default' ) : ?>
                <div class="box-action-wrap">
                    <?php echo $_a_event->getThumbsUpPosition('default'); ?>
                </div>
                <?php endif; ?>
                <?php
                    $bkIcon = '';
                    $bkIcon = apply_filters('octave_render_bookmark_icon', $bkIcon, $_a_event);
                    echo $bkIcon;
                ?>
                <?php echo $_a_event->renderIconsPosition('after_thumbs_up', $even_dataIcons); ?>
            </div>
            <?php endif ?>

            <div class="apl-event-discount-icon-text">
                <?php
                echo $_a_event->renderIconsPosition('before_discount_text', $even_dataIcons);

                if($discountConfig['enable-discount-description']){
                    $_a_event->generateDiscountText($discountConfig['include_discount_url']);
                }
                ?>
            </div>

            <p class="desc">
                <?php
                $summary =$_a_event->get_summary($length_summary);
                echo $summary['text']; ?><?php if($_a_event->is_have_more_summary($length_summary)) echo '...'; ?>
            </p>

            <?php if($_a_event->is_have_more_summary($length_summary)): ?>
                <a href="<?php echo $_a_event->get_permalink() ?>" class="vmore"><?php _e("View more", 'apollo') ?></a>
            <?php endif; ?>

            <div class="b-btn category">

                <?php
                    echo $_a_event->renderTickerBtn(array(
                            'class' => 'btn btn-category'
                        ));
                    echo $_a_event->renderCheckDCBtn(array(
                        'class' => 'btn btn-category'
                    ));
                    $bookmarkIcon = $_a_event->renderBookmarkBtn(array(
                        'class' => 'btn btn-category btn-bm apl-check-user-added'
                    ));
                    echo apply_filters('octave_custom_bookmark_btn', $bookmarkIcon);
                ?>
            </div>
        </div>
    </article>
    <?php endforeach; ?>
</section>
<?php endif; ?>