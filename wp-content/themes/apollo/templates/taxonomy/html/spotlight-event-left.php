<article class="content-page astro-detail category-lft category-page-event-box">

<?php
if(empty($datas)):
?>
<br>
<p><?php _e('There are no events available for this category at this time.', 'apollo') ?></p>
<?php
else:
    $_a_event = get_event($datas);
    $characters = 200;
    $a_200_content = $_a_event->get_summary($characters);
    $arrShareInfo = $_a_event->getShareInfo();
    $length_summary = Apollo_Display_Config::EVENT_OVERVIEW_NUMBER_CHAR;

    $icon_fields = $_a_event->getThemeOption()->getEventIcons();
    $discountConfig = AplEventFunction::getConfigDiscountIcon();
    $even_dataIcons = $_a_event->getEventDataIcons($discountConfig['display_all_icon'], $icon_fields);
    ?>

    <div class="b-share-cat art">
        <?php echo $_a_event->getSharingInfo('above_title'); ?>
        <input name="eventid" value="<?php echo $_a_event->id; ?>" type="hidden" />
    </div>

    <div class="social-icon-pck">
        <div class="page-tool"></div>

        <div class="event-thumbs-up-next-the-title">
            <h1 class="p-ttl category-lft-title">
                <a href="<?php echo $_a_event->get_permalink() ?>"><?php echo $_a_event->get_title(true) ?></a>
                <?php echo $_a_event->getThumbsUpPosition('next_title') ?>
            </h1>
        </div>

        <?php echo $_a_event->renderIconsPosition('after_title', $even_dataIcons); ?>
        <p class="meta auth"><?php echo $_a_event->renderOrgVenueHtml(true); ?></p>

        <?php echo $_a_event->getDateDisplay('plain-text'); ?>

        <div class="apl-wrap-under-presented">
            <div class="b-share-cat art">
                <?php echo $_a_event->getSharingInfo('under_present_by'); ?>
            </div>

            <div class="rating-box rating-action">
                <div class="box-action-wrap">
                    <?php echo $_a_event->getThumbsUpPosition('default');
                    $bkIcon = '';
                    $bkIcon = apply_filters('octave_render_bookmark_icon', $bkIcon, $_a_event);
                    echo $bkIcon;
                if ( isset($allow_comment) && $allow_comment ):
                    ?>
                    <div class="cm"> <a href="javascript:;"><?php _e( 'Comment', 'apollo' ) ?></a></div>

                <?php endif; ?>
                </div>
                <!--    <div class="bok-mk"><a href="javascript:;">Bookmark</a></div>-->

                <?php echo $_a_event->renderIconsPosition('after_thumbs_up', $even_dataIcons); ?>
            </div>
        </div>

        <div class="astro-featr ct-s-c-d-p">
            <article class="blog-itm">
                <?php
                /** @Ticket #16332 - change image size for Octave theme*/
                $spotlightImageSize = 'medium';
                $spotlightImageSize = apply_filters('_apl_taxonomy_custom_category_spotlight_size', $spotlightImageSize);
                /*@ticket #17730*/

                echo $_a_event->getDateDisplay('default');
                ?>
                <div class="pic">
                    <?php  echo $_a_event->getDateDisplay('strip-top'); ?>
                    <a href="<?php echo $_a_event->get_permalink() ?>"><?php echo $_a_event->get_image($spotlightImageSize) ?></a>
                </div>

                <?php

                $summary = $_a_event->get_full_excerpt();
                if ( $summary ):
                    ?>
                    <div class="a-txt-fea" id="_ed_sum_short">
                        <?php echo $summary; ?>
                    </div>
                <?php endif; ?>

                <div class="apl-event-discount-icon-text">
                    <?php

                    echo $_a_event->renderIconsPosition('before_discount_text', $even_dataIcons);

                    if($discountConfig['enable-discount-description']){
                        $_a_event->generateDiscountText($discountConfig['include_discount_url']);
                    }
                    ?>
                </div>

                <div class="b-btn __inline_block_fix_space">

                    <?php echo $_a_event->renderTickerBtn(array(
                        'class' => 'btn btn-b'
                    )) ?>

                    <?php echo $_a_event->renderCheckDCBtn(array(
                        'class' => 'btn btn-b'
                    )) ?>

                    <?php
                        $bookmarkIcon = $_a_event->renderBookmarkBtn(array(
                            'class' => 'btn-bm btn btn-b'
                        ));
                        echo apply_filters('octave_custom_bookmark_btn', $bookmarkIcon);
                    ?>
                </div>


                <div class="a-desc apl-internal-content desc-evt" id="_ed_short">
                    <p class="desc">
                        <?php echo $a_200_content['text'].''?>
                        <?php if($_a_event->is_have_more_summary($length_summary)) echo '...'; ?>
                    </p>
                    <a href="<?php echo $_a_event->get_permalink() ?>" class="vmore" ><?php _e('View more', 'apollo') ?></a>

                </div>


            </article>
        </div>
        <?php
        include('social-meta.php');
        ?>
    </div>
    <?php require_once APOLLO_TEMPLATES_DIR.'/events/google-map-popup.php' ?>
    <?php require_once APOLLO_TEMPLATES_DIR.'/events/chooes-export-popup.php' ?>

<?php endif; ?>

</article>
