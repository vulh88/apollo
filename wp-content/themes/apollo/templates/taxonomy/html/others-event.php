<?php
    $discountConfig = AplEventFunction::getConfigDiscountIcon();
    foreach($datas as $data ):
    $_a_event = get_event($data);
    // $this->set_displayed_event( ( int ) $_a_event->id );
    $_arr_show_date = $_a_event->getShowTypeDateTime('picture');
    $length_summary = Apollo_Display_Config::EVENT_OVERVIEW_NUMBER_CHAR;
    $icon_fields = $_a_event->getThemeOption()->getEventIcons();
    $even_dataIcons = $_a_event->getEventDataIcons($discountConfig['display_all_icon'], $icon_fields);
?>
    <div class="more-cat-itm category-page-event-box">
        <div class="more-pic"><a href="<?php echo $_a_event->get_permalink() ?>"><?php echo $_a_event->get_image('thumbnail', array(), array(
                        'aw' => true,
                        'ah' => true,
                    )) ?></a></div>
        <div class="more-ct">
            <div class="event-thumbs-up-next-the-title">
                <h3>
                    <a href="<?php echo $_a_event->get_permalink() ?>"><?php echo $_a_event->get_title(TRUE) ?></a>
                    <?php echo $_a_event->getThumbsUpPosition('next_title') ?>
                </h3>
            </div>

            <p class="meta auth"><?php echo $_a_event->renderOrgVenueHtml(true) ?></p>
            <p class="p-date">
                <?php

                    if($_arr_show_date['type'] !== 'none') {
                        $us = $_arr_show_date['unix_start'];
                        $ue = $_arr_show_date['unix_end'];

                        if($_arr_show_date['type'] === 'one'){
                            echo Apollo_App::apl_date('M d', $us).', <span>'.date('Y', $us).'</span>';
                        }
                        else if($_arr_show_date['type'] === 'two') {
                            // same year
                            if(date('Y', $ue) === date('Y', $us)) {
                                echo Apollo_App::apl_date('M d', $us).' - '.Apollo_App::apl_date('M d', $ue).', <span>'.date('Y', $us).'</span>';
                            }
                            else {
                                echo Apollo_App::apl_date('M d', $us).', <span>'.date('Y', $us).'</span>'.' - '.Apollo_App::apl_date('M d', $ue).', <span>'.date('Y', $ue).'</span>';
                            }
                        }
                } ?>

            </p>
    <?php if( $_a_event->getThemeOption()->getThumbsUpEnable() ) : ?>
            <div class="more-cat-rating-box">
                <?php if (  $_a_event->getThemeOption()->getThumbsUpPosition() == 'default' ) : ?>
                <div class="box-action-wrap">
                    <?php echo $_a_event->getThumbsUpPosition('default'); ?>
                </div>
                <?php endif; ?>
                <?php
                    $bkIcon = '';
                    $bkIcon = apply_filters('octave_render_bookmark_icon', $bkIcon, $_a_event);
                    echo $bkIcon;
                ?>
                <?php echo $_a_event->renderIconsPosition('after_thumbs_up', $even_dataIcons); ?>
            </div>
    <?php endif ?>
            <div class="apl-event-discount-icon-text">
                <?php
                echo $_a_event->renderIconsPosition('before_discount_text', $even_dataIcons);

                if($discountConfig['enable-discount-description']){
                    $_a_event->generateDiscountText($discountConfig['include_discount_url']);
                }
                ?>
            </div>
        </div>
        <div class="b-btn">
            <?php echo $_a_event->renderTickerBtn(array(
                    'class' => 'btn btn-b'
            )) ?>
            <?php echo $_a_event->renderCheckDCBtn(array(
                    'class' => 'btn btn-b'
                )) ?>
            <?php
            $bookmarkIcon = $_a_event->renderBookmarkBtn(array(
                'class' => 'btn btn-b btn-bm'
            ));
            echo apply_filters('octave_custom_bookmark_btn', $bookmarkIcon);
            ?>
        </div>
    </div>
<?php endforeach; ?>


