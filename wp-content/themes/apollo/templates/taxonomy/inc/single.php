<?php
class Apollo_Single_Category {

    protected $term;
    private $aspotlights_event = null;
    private $afeatures_event = null;
    private $aother_event = null;
    public  $themeToolClass = null;

    const NUMBER_SHOW_SPOTLIGHT = Apollo_Display_Config::SINGLE_TAXONOMY_SPOTLIGHT_EVENT_SHOW;
    const NUMBER_SHOW_FEATURES = Apollo_Display_Config::SINGLE_TAXONOMY_FEATURES_EVENT_SHOW;
    const NUMBER_SHOW_OTHERS = Apollo_Display_Config::SINGLE_TAXONOMY_OTHER_EVENT_SHOW;

    private $handlers = array();

    private $otherInfo = array(
        'post_status' => array( 'publish' ),
        'start' => 0,
        'pagesize' => self::NUMBER_SHOW_OTHERS,
    );

    private $spotlightInfo = array(
        'post_status' => array(
            'publish',
        ),
        'start' => 0,
        'pagesize' => self::NUMBER_SHOW_SPOTLIGHT
    );

    private $featureInfo = array(
        'post_status' => array(
            'publish',
        ),
        'start' => 0,
        'pagesize' => self::NUMBER_SHOW_FEATURES
    );

    public $displayed_events = array();

    public function __construct($term) {
        if(is_int($term)) {
            $this->term = get_term($term, 'event-type');
        }
        else {
            $this->term = $term;
        }

        $this->themeToolClass = new Apollo_Theme_Tool($this->term->term_id);
    }


    /**
     * @param $arrCondition
     * @exepct Apollo_Event
     */
    public function getSpotLightsEvent($arrCondition = array()) {
        if($this->aspotlights_event !== null) return $this->aspotlights_event;
        global $wpdb;
        $this->spotlightInfo = wp_parse_args($arrCondition, $this->spotlightInfo);
        $category_id = $this->term->term_id;
        $start = $this->spotlightInfo['start'];
        $pagesize = $this->spotlightInfo['pagesize'];
        $apollo_meta_table = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};
        $post_term_table = $wpdb->{Apollo_Tables::_POST_TERM};
        $current_date = current_time('Y-m-d');
        // Get theme option form order
        $forceOrder = $this->themeToolClass->getOrder();
        $arr_order = Apollo_Sort_Manager::getSortOneFieldForPostType('event', $forceOrder);
        // Get join theme tool
        $jointhemeToolLocation = $this->themeToolClass->getVenueJoin('p');

        $sql = "
        SELECT p.post_content, p.ID, p.post_excerpt, p.post_title, p.post_type  FROM $wpdb->posts as p
            INNER JOIN {$apollo_meta_table} mt_start_d ON p.ID = mt_start_d.apollo_event_id AND mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
            INNER JOIN {$apollo_meta_table} mt_end_d ON p.ID   = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
            $jointhemeToolLocation
        ";
        if(
            $arr_order['type_sort'] === 'apollo_meta'
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE )
        { // sort by meta key
            $sql .= " INNER JOIN {$apollo_meta_table} tblSortMeta ON tblSortMeta.apollo_event_id = p.ID AND tblSortMeta.meta_key = '{$arr_order["metakey_name"]}'";
        }

        // Get where theme tool
        $whereEventIn = $this->themeToolClass->getWhereInEventIds('p', ' AND ');

        $sql .=   " WHERE $whereEventIn p.post_status = 'publish' AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."' AND (
                        ( mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
                        AND CAST( mt_start_d.meta_value AS DATE ) >= '{$current_date}' )
                    OR
                        ( mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
                        AND CAST( mt_end_d.meta_value AS DATE ) >= '{$current_date}' )
                    )";

        $sql .= " AND p.ID IN (
            SELECT post_id FROM ".$wpdb->{Apollo_Tables::_POST_TERM}."
            WHERE term_id = '". $category_id ."' AND ".Apollo_DB_Schema::_IS_CATEGORY_SPOTLIGHT." = 'yes' ) ";

        if (!$this->themeToolClass->isThemeTool()) {
            $sql .= "
                AND p.ID IN (
                    SELECT object_id FROM $wpdb->term_relationships
                    WHERE term_taxonomy_id IN (
                        SELECT term_taxonomy_id FROM $wpdb->term_taxonomy WHERE term_id = $category_id
                    )
                )
            ";
        }

        $sql = Apollo_App::hidePrivateEvent($sql,'p');

        $sql .= " GROUP BY p.ID ";

        if($arr_order['type_sort'] === 'apollo_meta') {
            if(
                $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
                && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE
            ) {
                $sql .= " ORDER BY tblSortMeta.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_START_DATE) {
                $sql .= " ORDER BY mt_start_d.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_END_DATE) {
                $sql .= " ORDER BY mt_end_d.meta_value {$arr_order['order']} ";
            }
        }
        else if($arr_order['type_sort'] === 'post') {
            $sql .= " ORDER BY p.{$arr_order['order_by']} {$arr_order['order']} ";
        }

        $sql .= " LIMIT $start, $pagesize ";

        if(isset($_GET['db'])) var_dump($sql);
        $resultSpotLightEvent = $wpdb->get_results($sql);
        if(empty($resultSpotLightEvent)){
            $resultSpotLightEvent = $this->getSpotLightEventByAutoFill($arrCondition);
        }
        $this->aspotlights_event = array(
            'datas' => $resultSpotLightEvent,
            'total' => 'Not implement',
        );

        return $this->aspotlights_event;
    }

    public function getSpotLightEventByAutoFill($arrCondition = array()) {
        global $wpdb;
        $this->spotlightInfo = wp_parse_args($arrCondition, $this->spotlightInfo);
        $category_id = $this->term->term_id;
        $start = $this->spotlightInfo['start'];
        $pagesize = $this->spotlightInfo['pagesize'];
        $apollo_meta_table = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};
        $current_date = current_time( 'Y-m-d' );
        if($this->themeToolClass->isThemeTool()){
            return $this->getAutoFillSpotLightEventsByThemeTool($arrCondition);
        }
        $sql = "
        SELECT p.post_content, p.ID, p.post_excerpt, p.post_title, p.post_type,
        (case when (DATEDIFF( mt_end_d.meta_value, mt_start_d.meta_value ) >= ".Apollo_App::orderDateRange().") THEN 1000 ELSE 0 END) AS date_range
        FROM $wpdb->posts as p
            INNER JOIN $wpdb->term_relationships  tr ON p.`ID` = tr.`object_id`
            INNER JOIN $wpdb->term_taxonomy tt ON tr.`term_taxonomy_id` = tt.`term_taxonomy_id`
            INNER JOIN {$apollo_meta_table} mt_start_d ON p.ID = mt_start_d.apollo_event_id AND mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
            INNER JOIN {$apollo_meta_table} mt_end_d ON p.ID   = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'

        WHERE p.post_status = 'publish'
              AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."'
              AND (
                ( mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."' AND CAST( mt_start_d.meta_value AS DATE ) >= '{$current_date}' )
                OR
                ( mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."' AND CAST( mt_end_d.meta_value AS DATE ) >= '{$current_date}' )
              )
        AND tt.term_id =". $category_id;
        $sql = Apollo_App::hidePrivateEvent($sql,'p');
        $sql .= "GROUP BY p.ID
        ORDER BY date_range ASC, mt_start_d.meta_value ASC
        LIMIT $start, $pagesize ";

        return $wpdb->get_results($sql);
    }

    public function checkEventCateThemeTool($eventCategoryID){
        if(empty($eventCategoryID)) return false;
        global $wpdb;
        $theme_tool_table = $wpdb->{Apollo_Tables::_APL_THEME_TOOL};
        $sql = "SELECT COUNT(tt.id) AS total
                FROM ". $theme_tool_table ." AS tt
                WHERE tt.cat_id = ". intval($eventCategoryID) ."
                AND tt.post_type = 'event'
            ";
        $result = $wpdb->get_row($sql, ARRAY_A);
        return intval($result['total']) > 0;
    }


    public function getAutoFillSpotLightEventsByThemeTool($arrCondition = array()) {
        global $wpdb;
        $this->spotlightInfo = wp_parse_args($arrCondition, $this->spotlightInfo);
        $start = $this->spotlightInfo['start'];
        $pagesize = $this->spotlightInfo['pagesize'];
        $apollo_meta_table = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};
        $selectedEventsByModeAll = $this->themeToolClass->getSelectedEventIDsByThemeTool(array('outputType' => 'sql', 'selectType' => 'venue', 'eventAlias' => 'p'));
        $selectedEventsByModeIndividual = $this->themeToolClass->getSelectedEventIDsByThemeTool(array('outputType' => 'sql', 'selectType' => 'event', 'eventAlias' => 'p'));
        $current_date = current_time( 'Y-m-d' );
        $sql = "
        SELECT p.post_content, p.ID, p.post_excerpt, p.post_title, p.post_type,
        (case when (DATEDIFF( mt_end_d.meta_value, mt_start_d.meta_value ) >= ".Apollo_App::orderDateRange().") THEN 1000 ELSE 0 END) AS date_range

        FROM $wpdb->posts as p
            INNER JOIN {$apollo_meta_table} mt_start_d ON p.ID = mt_start_d.apollo_event_id AND mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
            INNER JOIN {$apollo_meta_table} mt_end_d ON p.ID   = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
            $selectedEventsByModeAll
        WHERE p.post_status = 'publish'
              AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."'
              AND (
                ( mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."' AND CAST( mt_start_d.meta_value AS DATE ) >= '{$current_date}' )
                OR
                ( mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."' AND CAST( mt_end_d.meta_value AS DATE ) >= '{$current_date}' )
              )
              $selectedEventsByModeIndividual
        GROUP BY p.ID
        ORDER BY date_range ASC, mt_start_d.meta_value ASC
        LIMIT $start, $pagesize ";
        return $wpdb->get_results($sql);
    }

    function themeToolSort() {

    }

    public function getFeaturesEvent($arrCondition = array()) {

        if($this->afeatures_event !== null) return $this->afeatures_event;

        global $wpdb;
        $this->featureInfo = wp_parse_args($arrCondition, $this->featureInfo);

        $arr_post_status = '("'.implode('", "',(array)$this->featureInfo['post_status']).'")';

        $_meta_key_sort = Apollo_DB_Schema::_APOLLO_EVENT_START_DATE;

        $start = $this->featureInfo['start'];
        $pagesize = $this->featureInfo['pagesize'];

        $spotlights_event = $this->getSpotLightsEvent();

        $anotin = array();
        if(count($spotlights_event['datas']) > 0 ){
            foreach($spotlights_event['datas'] as $e) {
                $anotin[] = $e->ID;
            }
        }

        $snotin = '';
        if(!empty($anotin)) {
            $snotin = "('".implode("','", $anotin)."')";
        }

        $apollo_meta_table = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};
        $current_date = current_time('Y-m-d');

        // Get theme option form order
        $forceOrder = $this->themeToolClass->getOrder();
        $arr_order = Apollo_Sort_Manager::getSortOneFieldForPostType('event', $forceOrder);

        // Get join theme tool
        $jointhemeToolLocation = $this->themeToolClass->getVenueJoin('p');

        // Get where theme tool
        $whereEventIn = $this->themeToolClass->getWhereInEventIds('p', ' AND ');

        $sql = "
        SELECT p.post_content, p.ID, p.post_excerpt, p.post_title, p.post_type, mt_start_d.meta_value as start_date, mt_end_d.meta_value as end_date,
            (case when (DATEDIFF( mt_end_d.meta_value, mt_start_d.meta_value ) >= ".Apollo_App::orderDateRange().") THEN 1000 ELSE 0 END) AS date_range

            FROM $wpdb->posts as p

            INNER JOIN {$apollo_meta_table} mt_start_d ON p.ID = mt_start_d.apollo_event_id AND mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
            INNER JOIN {$apollo_meta_table} mt_end_d ON p.ID   = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
            $jointhemeToolLocation
        ";

        if(
            $arr_order['type_sort'] === 'apollo_meta'
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE )
        { // sort by meta key
            $sql .= " INNER JOIN {$apollo_meta_table} tblSortMeta ON tblSortMeta.apollo_event_id = p.ID AND tblSortMeta.meta_key = '{$arr_order["metakey_name"]}'";
        }

        $sql .=   " WHERE $whereEventIn p.post_status = 'publish' AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."'
                    AND  mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
                    AND CAST( mt_end_d.meta_value AS DATE ) >= '{$current_date}'
                    ";

        $sql .= " AND p.ID IN (
            SELECT post_id FROM ".$wpdb->{Apollo_Tables::_POST_TERM}."
            WHERE term_id = '". $this->term->term_id ."' AND ".Apollo_DB_Schema::_IS_CATEGORY_FEATURE." = 'yes' ) ";

        if (! $this->themeToolClass->isThemeTool()) {
            $sql .= "
                AND p.ID IN (
                    SELECT object_id FROM $wpdb->term_relationships
                    WHERE term_taxonomy_id IN (
                        SELECT term_taxonomy_id FROM $wpdb->term_taxonomy WHERE term_id = {$this->term->term_id}
                    )
                )
            ";
        }


        if(!empty($snotin)) {
            $sql .= " AND ( p.ID NOT IN $snotin )";
        }

        $sql = Apollo_App::hidePrivateEvent($sql,'p');

        $sql .= " GROUP BY p.ID ";

        if($arr_order['type_sort'] === 'apollo_meta') {
            $dateRangeOrder = "date_range ASC";
            if(
                $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
                && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE
            ) {
                $sql .= " ORDER BY tblSortMeta.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_START_DATE) {
                $sql .= " ORDER BY $dateRangeOrder, mt_start_d.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_END_DATE) {
                $sql .= " ORDER BY $dateRangeOrder, mt_end_d.meta_value {$arr_order['order']} ";
            }
        }
        else if($arr_order['type_sort'] === 'post') {
            $sql .= " ORDER BY p.{$arr_order['order_by']} {$arr_order['order']} ";
        }

        $sql .= " LIMIT $start, $pagesize    ";

        $this->events = $wpdb->get_results($sql);
        $this->afeatures_event = array(
            'datas' => $this->events,
            'total' => 'Not implement',
        );

        return $this->afeatures_event;
    }

    /**
     * Get upcomming events of category
     *
     * @access public
     * @return string
     */
    public function get_auto_fill_category_upcomming_events( $page_size ) {

        global $wpdb;

        $_auto_fill_where = '';

        if ( $this->displayed_events ) {

            $_condition = '("'. implode( '", "' , ( array ) $this->displayed_events ) . '")' ;
            $_auto_fill_where = " ( p.ID NOT IN $_condition ) AND ";
        }

        $current_date = current_time( 'Y-m-d' );
        $apollo_meta_table = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};
        $category_id = $this->term->term_id;

        $arr_order = Apollo_Sort_Manager::getSortOneFieldForPostType('event');

        $sql = "
            SELECT p.post_content, p.ID, p.post_excerpt, p.post_title, p.post_type, mt_start_d.meta_value as start_date, mt_end_d.meta_value as end_date,

            (case when (DATEDIFF( mt_end_d.meta_value, mt_start_d.meta_value ) >= ".Apollo_App::orderDateRange().") THEN 1000 ELSE 0 END) AS date_range

            FROM {$wpdb->posts} p
            INNER JOIN $wpdb->term_relationships as wp_term_relation ON (p.ID = wp_term_relation.object_id )
            INNER JOIN $wpdb->term_taxonomy as wp_term_taxonomy ON (wp_term_taxonomy.term_taxonomy_id = wp_term_relation.term_taxonomy_id AND wp_term_taxonomy.term_id='$category_id' )

            INNER JOIN {$apollo_meta_table} mt_start_d ON p.ID = mt_start_d.apollo_event_id AND mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
                INNER JOIN {$apollo_meta_table} mt_end_d ON p.ID   = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
            LEFT JOIN   {$wpdb->{Apollo_Tables::_POST_TERM}} pt ON pt.post_id = p.ID
        ";

        if(
            $arr_order['type_sort'] === 'apollo_meta'
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE )
        { // sort by meta key
            $sql .= " INNER JOIN {$apollo_meta_table} tblSortMeta ON tblSortMeta.apollo_event_id = p.ID AND tblSortMeta.meta_key = '{$arr_order["metakey_name"]}'";
        }

        $sql .=   " WHERE $_auto_fill_where p.post_status = 'publish' AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."' AND (
                        ( mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
                        AND CAST( mt_start_d.meta_value AS DATE ) >= '{$current_date}' )
                    OR
                        ( mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
                        AND CAST( mt_end_d.meta_value AS DATE ) >= '{$current_date}' )
                    )";

        $sql .= " AND
                (   pt.post_id is NULL
                    OR (
                        pt.".Apollo_DB_Schema::_IS_CATEGORY_FEATURE." <> 'yes'
                    )
                ) ";
        $sql = Apollo_App::hidePrivateEvent($sql,'p');
        $sql .= " GROUP BY p.ID ";

        if($arr_order['type_sort'] === 'apollo_meta') {
            $dateRangeOrder = "date_range ASC";
            if(
                $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
                && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE
            ) {
                $sql .= " ORDER BY tblSortMeta.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_START_DATE) {
                $sql .= " ORDER BY $dateRangeOrder, mt_start_d.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_END_DATE) {
                $sql .= " ORDER BY $dateRangeOrder, mt_end_d.meta_value {$arr_order['order']} ";
            }
        }
        else if($arr_order['type_sort'] === 'post') {
            $sql .= " ORDER BY p.{$arr_order['order_by']} {$arr_order['order']} ";
        }

        $sql .= " LIMIT $page_size ";
        return $wpdb->get_results( $sql );
    }

    private function setDisplayedEvents($events) {
        if ($events) {
            foreach($events as $event) {
                $this->set_displayed_event($event->ID);
            }
        }
    }

    /**
     * Get upcomming events of category
     *
     * @access public
     * @return string
     */
    public function get_auto_fill_theme_tool_upcomming_events( $page_size ) {

        global $wpdb;

        $_auto_fill_where = '';

        if ($this->events) {
            $this->setDisplayedEvents($this->events);
        }

        if ( $this->displayed_events ) {

            $_condition = '("'. implode( '", "' , ( array ) $this->displayed_events ) . '")' ;
            $_auto_fill_where = " ( p.ID NOT IN $_condition ) AND ";
        }



        $current_date = current_time( 'Y-m-d' );
        $apollo_meta_table = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};
        $category_id = $this->term->term_id;

        // Force theme tool order
        $forceOrder = $this->themeToolClass->getOrder();
        $arr_order = Apollo_Sort_Manager::getSortOneFieldForPostType('event', $forceOrder);

        //Filter by zip if has theme tool exists
        $jointhemeToolLocation = $this->themeToolClass->getVenueJoin('p');

        // Get where theme tool
        $whereEventIn = $this->themeToolClass->getWhereInEventIds('p', ' AND ');

        $sql = "
            SELECT p.post_content, p.ID, p.post_excerpt, p.post_title, p.post_type, mt_start_d.meta_value as start_date, mt_end_d.meta_value as end_date,
            (case when (DATEDIFF( mt_end_d.meta_value, mt_start_d.meta_value ) >= ".Apollo_App::orderDateRange().") THEN 1000 ELSE 0 END) AS date_range

            FROM {$wpdb->posts} p
            $jointhemeToolLocation

            INNER JOIN {$apollo_meta_table} mt_start_d ON p.ID = mt_start_d.apollo_event_id AND mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
                INNER JOIN {$apollo_meta_table} mt_end_d ON p.ID   = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
            LEFT JOIN   {$wpdb->{Apollo_Tables::_POST_TERM}} pt ON pt.post_id = p.ID
        ";

        if(
            $arr_order['type_sort'] === 'apollo_meta'
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE )
        { // sort by meta key
            $sql .= " INNER JOIN {$apollo_meta_table} tblSortMeta ON tblSortMeta.apollo_event_id = p.ID AND tblSortMeta.meta_key = '{$arr_order["metakey_name"]}'";
        }

        $sql .=   " WHERE $whereEventIn $_auto_fill_where p.post_status = 'publish' AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."' AND (
                        ( mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
                        AND CAST( mt_start_d.meta_value AS DATE ) >= '{$current_date}' )
                    OR
                        ( mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
                        AND CAST( mt_end_d.meta_value AS DATE ) >= '{$current_date}' )
                    )";
        $sql = Apollo_App::hidePrivateEvent($sql,'p');

        $sql .= " GROUP BY p.ID ";

        if($arr_order['type_sort'] === 'apollo_meta') {
            $dateRangeOrder = "date_range ASC";
            if(
                $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
                && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE
            ) {
                $sql .= " ORDER BY tblSortMeta.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_START_DATE) {
                $sql .= " ORDER BY $dateRangeOrder, mt_start_d.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_END_DATE) {
                $sql .= " ORDER BY $dateRangeOrder, mt_end_d.meta_value {$arr_order['order']} ";
            }
        }
        else if($arr_order['type_sort'] === 'post') {
            $sql .= " ORDER BY p.{$arr_order['order_by']} {$arr_order['order']} ";
        }

        $sql .= " LIMIT $page_size ";

        return $wpdb->get_results( $sql );
    }

    public function set_displayed_event( $event_id ) {
        if(is_array($event_id)) {
            $this->displayed_events = $event_id;
        }
        else {
            $this->displayed_events[] = $event_id;
        }

    }
    public function get_displayed_events(  ) {
        return $this->displayed_events;
    }

    /**
     * If the administrator has NOT selected any features or less than 7 features,
     * then the featured event block on category pages should "auto fill" with the most current events
     */
    public function auto_fill_featured_events() {
        $num_min_featured = Apollo_Display_Config::MIN_NUM_FEATURED_EVENT;

        if ( count( $this->events ) >= $num_min_featured ) {
            return $this->events;
        }

        if ( $this->themeToolClass->isThemeTool() ) {
            $auto_fill_featureds = $this->get_auto_fill_theme_tool_upcomming_events( $num_min_featured - count( $this->events ) );
            $forceOrder = $this->themeToolClass->getOrder();
        	$arr_order = Apollo_Sort_Manager::getSortOneFieldForPostType('event', $forceOrder);
        } else {
            $auto_fill_featureds = $this->get_auto_fill_category_upcomming_events( $num_min_featured - count( $this->events ) );
            $arr_order = Apollo_Sort_Manager::getSortOneFieldForPostType('event');
        }


        if ( ! $auto_fill_featureds ) {
            return $this->events;
        }
        if ( ! $this->events ) {
            return $auto_fill_featureds;
        }

        $merge_rslt = array_merge( $this->events, $auto_fill_featureds );
        //merged arrays need re-sort
        usort($merge_rslt, function($a, $b) use($arr_order) {

            switch ($arr_order['order_by']) {

                case "meta_value":
                    if($arr_order['metakey_name'] == "_apollo_event_start_date"){
                        return strtotime($a->start_date) - strtotime($b->start_date);
                    }
                    else {
                        return strtotime($a->end_date) - strtotime($b->end_date);
                    }
                    break;

                case "post_title":
                    if($arr_order['order'] == "ASC"){
                        return strnatcmp($a->post_title, $b->post_title);
                    }
                    else {
                        return strnatcmp($b->post_title, $a->post_title);
                    }
                    break;
            }

        });

		return $merge_rslt;
    }

    private function _getOthersThemeToolEvent($arrCondition = array()) {

        if($this->aother_event !== null) return $this->aother_event;
        global $wpdb;

        $_auto_fill_where = '';

        if ( $this->displayed_events ) {

            $_condition = '("'. implode( '", "' , ( array ) $this->displayed_events ) . '")' ;
            $_auto_fill_where = " ( p.ID NOT IN $_condition ) AND ";
        }

        $this->otherInfo = wp_parse_args($arrCondition, $this->otherInfo);

        $category_id = $this->term->term_id;

//        $transientKey = 'otherEventsThemeTool_'.$category_id ;
//        $transientData = maybe_unserialize(get_site_transient($transientKey));
//        if ($transientData && !Apollo_App::is_ajax()) {
//            return $this->aother_event = array(
//                'datas' => $transientData['data'],
//                'total' => $transientData['total'],
//            );
//        }

        $start = $this->otherInfo['start'];
        $this->_getCookiesForDisplayedItem($category_id);

        $post_term_tbl = $wpdb->{Apollo_Tables::_POST_TERM};
        $apollo_meta_table = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};
        $current_date = current_time( 'Y-m-d' );

        // Get theme option form order
        $forceOrder = $this->themeToolClass->getOrder();
        $arr_order = Apollo_Sort_Manager::getSortOneFieldForPostType('event', $forceOrder);

        /* Some kind it will fall if that meta_key or meta_value don't eixts */
        $sql = "
        SELECT SQL_CALC_FOUND_ROWS p.post_content, p.ID, p.post_excerpt, p.post_title, pt.post_id, p.post_type,

                (case when (DATEDIFF( mt_end_d.meta_value, mt_start_d.meta_value ) >= ".Apollo_App::orderDateRange().") THEN 1000 ELSE 0 END) AS date_range

                FROM $wpdb->posts as p

                LEFT JOIN $post_term_tbl pt ON pt.post_id = p.ID AND pt.term_id = $category_id

                INNER JOIN {$apollo_meta_table} mt_start_d ON p.ID = mt_start_d.apollo_event_id AND mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
                INNER JOIN {$apollo_meta_table} mt_end_d ON p.ID   = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
        ";

        //Filter by zip if has theme tool exists
        $whereEventInVenues = $this->themeToolClass->getEventIDsInVenue('p');

        // Get where theme tool
        $whereEventIn = $this->themeToolClass->getWhereInEventIds('p', ' AND ');

        if(
            $arr_order['type_sort'] === 'apollo_meta'
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE )
        { // sort by meta key
            $sql .= " INNER JOIN {$apollo_meta_table} tblSortMeta ON tblSortMeta.apollo_event_id = p.ID AND tblSortMeta.meta_key = '{$arr_order["metakey_name"]}'";
        }

        $sql .=   " WHERE $whereEventInVenues $whereEventIn $_auto_fill_where p.post_status = 'publish' AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."' AND (
                        ( mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
                        AND CAST( mt_start_d.meta_value AS DATE ) >= '{$current_date}' )
                    OR
                        ( mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
                        AND CAST( mt_end_d.meta_value AS DATE ) >= '{$current_date}' )
                    )";

        $sql .= " GROUP BY p.ID ";

        if($arr_order['type_sort'] === 'apollo_meta') {
            $dateRangeOrder = "date_range ASC";
            if(
                $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
                && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE
            ) {
                $sql .= " ORDER BY tblSortMeta.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_START_DATE) {
                $sql .= " ORDER BY $dateRangeOrder, mt_start_d.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_END_DATE) {
                $sql .= " ORDER BY $dateRangeOrder, mt_end_d.meta_value {$arr_order['order']} ";
            }
        }
        else if($arr_order['type_sort'] === 'post') {
            $sql .= " ORDER BY p.{$arr_order['order_by']} {$arr_order['order']} ";
        }

        $sql .= sprintf(" LIMIT %s, %s ", $start, $this->otherInfo['pagesize'] );

        $events = $wpdb->get_results($sql);
        $total = $wpdb->get_var("SELECT FOUND_ROWS()");

        $this->otherInfo['total'] = $total;

        return $this->aother_event = array(
            'datas' => $events,
            'total' => $total,
            'displayed' => $start + $this->otherInfo['pagesize'], // Get all displayed items
            'hasMore'   => $this->isHaveMoreOtherEvent()
        );
    }

    /**
     * Get cookie for displayed items that is stored when click on the show more button
     *
     * @param int $termId
    */
    private function _getCookiesForDisplayedItem($termId) {
        // Get displayed items from the cookies and clear the cookies
        $displayedItemKey = Apollo_Const::_APL_TAX_OTHER_EVENTS_DISPLAYED. $termId;
        $this->otherInfo['pagesize'] = !empty($_COOKIE[$displayedItemKey]) && ! Apollo_App::is_ajax() ? $_COOKIE[$displayedItemKey] : $this->otherInfo['pagesize'];
    }

    public function getOthersEvent($arrCondition = array(), $termID = '') {

        if ( $this->themeToolClass->isThemeTool() ) return $this->_getOthersThemeToolEvent ($arrCondition);

        if($this->aother_event !== null) return $this->aother_event;
        global $wpdb;

        $_auto_fill_where = '';

        if ( $this->displayed_events ) {

            $_condition = '("'. implode( '", "' , ( array ) $this->displayed_events ) . '")' ;
            $_auto_fill_where = " ( p.ID NOT IN $_condition ) AND ";
        }

        $this->otherInfo = wp_parse_args($arrCondition, $this->otherInfo);

        $category_id = $termID ? $termID : $this->term->term_id;

        $start = $this->otherInfo['start'];

        $this->_getCookiesForDisplayedItem($category_id);

        $post_term_tbl = $wpdb->{Apollo_Tables::_POST_TERM};
        $apollo_meta_table = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};

        $current_date = current_time('Y-m-d');

        // Get theme option form order
        $forceOrder = $this->themeToolClass->getOrder();
        $arr_order = Apollo_Sort_Manager::getSortOneFieldForPostType('event', $forceOrder);

        /* Some kind it will fall if that meta_key or meta_value don't eixts */
        $sql = "
        SELECT SQL_CALC_FOUND_ROWS p.post_content, p.ID, p.post_excerpt, p.post_title, pt.post_id, p.post_type,
                (case when (DATEDIFF( mt_end_d.meta_value, mt_start_d.meta_value ) >= ".Apollo_App::orderDateRange().") THEN 1000 ELSE 0 END) AS date_range

                FROM $wpdb->posts as p

                INNER JOIN $wpdb->term_relationships as wp_term_relation ON (p.ID = wp_term_relation.object_id )
                INNER JOIN $wpdb->term_taxonomy as wp_term_taxonomy ON (wp_term_taxonomy.term_taxonomy_id = wp_term_relation.term_taxonomy_id AND wp_term_taxonomy.term_id='$category_id' )

                LEFT JOIN $post_term_tbl pt ON pt.post_id = p.ID AND pt.term_id = $category_id

                INNER JOIN {$apollo_meta_table} mt_start_d ON p.ID = mt_start_d.apollo_event_id AND mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
                INNER JOIN {$apollo_meta_table} mt_end_d ON p.ID   = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
        ";

        if(
            $arr_order['type_sort'] === 'apollo_meta'
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE )
        { // sort by meta key
            $sql .= " INNER JOIN {$apollo_meta_table} tblSortMeta ON tblSortMeta.apollo_event_id = p.ID AND tblSortMeta.meta_key = '{$arr_order["metakey_name"]}'";
        }

        $sql .=   " WHERE $_auto_fill_where p.post_status = 'publish' AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."' AND (
                        ( mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
                        AND CAST( mt_start_d.meta_value AS DATE ) >= '{$current_date}' )
                    OR
                        ( mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
                        AND CAST( mt_end_d.meta_value AS DATE ) >= '{$current_date}' )
                    )";


        $sql = Apollo_App::hidePrivateEvent($sql,'p');

        $sql .= " GROUP BY p.ID ";

        if($arr_order['type_sort'] === 'apollo_meta') {
            $dateRangeOrder = "date_range ASC";
            if(
                $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
                && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE
            ) {
                $sql .= " ORDER BY tblSortMeta.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_START_DATE) {
                $sql .= " ORDER BY $dateRangeOrder, mt_start_d.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_END_DATE) {
                $sql .= " ORDER BY $dateRangeOrder, mt_end_d.meta_value {$arr_order['order']} ";
            }
        }
        else if($arr_order['type_sort'] === 'post') {
            $sql .= " ORDER BY p.{$arr_order['order_by']} {$arr_order['order']} ";
        }

        $sql .= sprintf(" LIMIT %s, %s ", $start, $this->otherInfo['pagesize'] );

        $events = $wpdb->get_results($sql);
        $total = $wpdb->get_var("SELECT FOUND_ROWS()");

        $this->otherInfo['total'] = $total;

        return $this->aother_event = array(
            'datas' => $events,
            'total' => $total,
            'displayed' => $start + $this->otherInfo['pagesize'], // Get all displayed items
            'hasMore'   => $this->isHaveMoreOtherEvent()
        );
    }

    public function isHaveMoreOtherEvent() {
        return $this->otherInfo['pagesize'] + $this->otherInfo['start'] < $this->otherInfo['total'];
    }

    public function getBaseStart() {
        return $this->otherInfo['pagesize'];
    }

    public function renderWithData($template_path, $datas) {
        ob_start();
        include $template_path;
        return ob_get_clean();
    }
}