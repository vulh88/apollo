<?php

if ( $results = $this->get_results() ) {
    foreach ( $results as $p ):

        $authorID = $p->post_author;
        $author = get_userdata( $authorID );

        $name =  $author->display_name ? $author->display_name : $author->user_nicename;
        $link = get_author_posts_url( $author->ID, $author->user_nicename );
        $desc = get_the_author_meta( 'description', $authorID );
        if(!$image = wp_get_attachment_image(get_the_author_meta('image', $authorID), 'large')) {
            $image = apollo_event_placeholder_img();
        }
        ?>
        <li>
            <div class="search-img"><a href="<?php echo $link ?>"><?php echo $image ?></a></div>
            <div class="info-content" data-url="" data-type="link">
                <div class="search-info"><a href="<?php echo $link ?>"><span class="ev-tt"><?php echo $name ?></span></a>
                    <div class="career"><?php echo $p->count > 1 ? sprintf(__('There are %s posts'), $p->count) : sprintf(__('There is %s post'), $p->count) ?></div>
                    <div class="s-desc"><?php echo $desc ?></div>
                </div>
            </div>
        </li>
        <?php
    endforeach;
} else if ( !isset($_GET['s']) && !isset($_GET['keyword']) ) {
    _e( 'No results', 'apollo' );
}