<?php
if ( $results = $this->get_results() ) {

    foreach ( $results as $p ):


        $authorID = $p->post_author;
        $author = get_userdata( $authorID );
        $desc = get_the_author_meta( 'description', $authorID );

        if(!$image = wp_get_attachment_image(get_the_author_meta('image', $authorID), 'large')) {
            $image = apollo_event_placeholder_img();
        }
        $name = $author->display_name ? $author->display_name : $author->user_nicename;

        $link = get_author_posts_url( $author->ID, $author->user_nicename );
        ?>
        <li>
            <div class="div-one" data-url="" data-type="link">
                <div class="search-img"><a href="<?php echo $link ?>"><?php echo $image ?></a></div>
                <div class="search-info">
                    <a href="<?php echo $link ?>">
                        <span class="ev-tt"  data-n="2"><?php echo $name ?></span>
                    </a>
                    <div class="career" data-ride="ap-clamp" data-n="2"><?php echo $p->count > 1 ? sprintf(__('There are %s posts'), $p->count) : sprintf(__('There is %s post'), $p->count) ?></div>
                    <div class="s-desc" data-ride="ap-clamp" data-n="9"><?php echo $desc ?></div>
                </div>
            </div>
        </li>

    <?php endforeach;

} else if ( !isset($_GET['s']) && !isset($_GET['keyword']) ) {
    _e( 'No results', 'apollo' );
}