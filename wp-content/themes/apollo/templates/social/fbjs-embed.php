<div id="fb-root"></div>
<?php if ( of_get_option( Apollo_DB_Schema::_FB_APP_ID ) ): ?>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : <?php echo of_get_option( Apollo_DB_Schema::_FB_APP_ID ); ?>,
            cookie: true,
            xfbml      : true,
            version    : 'v2.1'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<?php endif; ?>