
<?php
    if ( $this->events ):
        /** @Ticket #14350 */
        $thumbsUpEnable = of_get_option(Apollo_DB_Schema::_ENABLE_THUMBS_UP,1);
        $thumbsUpPosition = of_get_option(Apollo_DB_Schema::_EVENT_SHOW_THUMBS_UP_POSITION, 'default');
        $icon_position = of_get_option(Apollo_DB_Schema::_EVENT_SHOW_ICON_POSITION, '');
        $icon_fields = maybe_unserialize( get_option( Apollo_DB_Schema::_APL_EVENT_ICONS ) );
        $discountConfig = AplEventFunction::getConfigDiscountIcon();
    foreach ( $this->events as $event ):

        if ( is_int( $event ) ) {
            $event = get_event( get_post( $event ) );
        } else {
            $event = get_event( $event );
        }

        $orgs         = Apollo_App::get_post_type_item($event->{Apollo_DB_Schema::_APOLLO_EVENT_ORGANIZATION});
        $venues       = Apollo_App::get_post_type_item($event->{Apollo_DB_Schema::_APOLLO_EVENT_VENUE});
        $arr_date     = $event->getShowTypeDateTime('picture');
        $ticket_url   = $event->get_meta_data(Apollo_DB_Schema::_ADMISSION_TICKET_URL, Apollo_DB_Schema::_APOLLO_EVENT_DATA);
        $discount_url = $event->get_meta_data(Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL, Apollo_DB_Schema::_APOLLO_EVENT_DATA);

        // get event's organization
        $registeredOrg   = $event->get_register_org(); // = NULL if organization is TMP ORG
        $registeredOrgID = empty($registeredOrg) ? '' : $registeredOrg->org_id;
        $even_dataIcons = $event->getEventDataIcons($discountConfig['display_all_icon'], $icon_fields);
?>
<div class="more-frm-itm">
    <div class="more-pic">
        <a href="<?php echo $event->get_permalink(); ?>">
            <?php echo $event->get_image( 'thumbnail' ); ?>
        </a>
    </div>
    <div class="more-ct custom">
        <?php if ( $thumbsUpEnable && $thumbsUpPosition == 'next_title' ) : ?>
            <div class="event-thumbs-up-next-the-title">
                <h3>
                    <a href="<?php echo $event->get_permalink(); ?>"><?php echo $event->get_title(); ?></a>
                    <div class="like" data-ride="ap-event_rating" id="_event_rating_<?php echo $event->id.'_'.'event' ?>" data-i="<?php echo $event->id ?>" data-t="event"> <a href="javascript:void(0);" ><span class="_count"></span></a></div>
                </h3>
            </div>
        <?php else : ?>
            <h3> <a href="<?php echo $event->get_permalink(); ?>"><?php echo $event->get_title(); ?></a></h3>
        <?php endif; ?>

        <p class="meta auth"><?php echo $event->renderOrgVenueHtml() ?></p>
        <p class="p-date">
            <?php

            if ( $arr_date['type'] != 'none' ) {
                $start_date = strtotime( $arr_date['start_date'] );
                $end_date = strtotime( $arr_date['end_date'] );
                if ( $arr_date['type'] == 'one' ) {
                    echo Apollo_App::apl_date( 'M j, Y', $start_date );
                } else {
                    if (intval( date( 'Y', $start_date ) ) == intval( date( 'Y', $end_date ) ) ) {
                        echo Apollo_App::apl_date( 'M j', $start_date ). ' - '. Apollo_App::apl_date( 'M j', $end_date ). ', '. date( 'Y', $end_date );
                    } else {
                        echo Apollo_App::apl_date( 'M j, Y', $start_date ). ' - '. Apollo_App::apl_date( 'M j, Y', $end_date );
                    }
                }
            }
            ?>
            </p>
        <?php if( $thumbsUpEnable && $thumbsUpPosition == 'default' ) : ?>
        <div class="rating-box rating-action">
            <div class="like parent-not-underline-hover" data-ride="ap-event_rating" id="_event_rating_<?php echo $event->id.'_'.'event' ?>" data-i="<?php echo $event->id ?>" data-t="event"> <a href="javascript:void(0);" ><span class="_count"></span></a></div>
        </div>
        <?php endif ?>
        <?php
            $bkIcon = '';
            $bkIcon = apply_filters('octave_render_bookmark_icon', $bkIcon, $event);
            echo $bkIcon;
        ?>
        <div class="apl-event-discount-icon-text">
            <?php
            if ($icon_position  == 'before_discount_text'){
                $event->renderIcons($icon_fields,$even_dataIcons);
            }

            if($discountConfig['enable-discount-description']){
                $event->generateDiscountText($discountConfig['include_discount_url']);
            }
            ?>
        </div>
    </div>

    <div  class="b-btn custom">

        <?php if ( $ticket_url ):
            $buyTicketText = of_get_option(Apollo_DB_Schema::_APL_EVENT_BUY_TICKET_TEXT, __('BUY TICKETS', 'apollo'));
        ?>
        <a target="_BLANK"
           data-ride="ap-logclick"
           data-action="apollo_log_click_activity"
           data-activity="click-buy-ticket"
           data-item_id="<?php echo $event->id; ?>"
           data-start="<?php echo $event->{Apollo_DB_Schema::_APOLLO_EVENT_START_DATE}; ?>"
           data-end="<?php echo $event->{Apollo_DB_Schema::_APOLLO_EVENT_END_DATE}; ?>"
           data-org-id="<?php echo $registeredOrgID; ?>"
           href="<?php echo $ticket_url; ?>"
           class="btn btn-b"><?php echo $buyTicketText ?></a>
        <?php endif; ?>
        <?php if ($discount_url):
            $checkD = of_get_option(Apollo_DB_Schema::_APL_EVENT_CHECK_DISCOUNTS_TEXT, __('CHECK DISCOUNTS', 'apollo'));
            ?>
            <a target="_BLANK"
               data-ride="ap-logclick"
               data-action="apollo_log_click_activity"
               data-activity="click-discount"
               data-item_id="<?php echo $event->id; ?>"
               data-start="<?php echo $event->{Apollo_DB_Schema::_APOLLO_EVENT_START_DATE}; ?>"
               data-end="<?php echo $event->{Apollo_DB_Schema::_APOLLO_EVENT_END_DATE}; ?>"
               data-org-id="<?php echo $registeredOrgID; ?>"
               style="margin-left: 0px; margin-bottom: 10px"
               href="<?php echo $discount_url; ?>"
               class="btn btn-b"><?php echo $checkD ?></a>
        <?php endif ?>
        <?php
            $bookmarkIcon = $event->renderBookmarkBtn(array(
                'class' => 'btn-bm btn btn-b'
            ));
            echo apply_filters('octave_custom_bookmark_btn', $bookmarkIcon);
        ?>

    </div>
</div>

<?php endforeach; ?>


<?php endif; ?>