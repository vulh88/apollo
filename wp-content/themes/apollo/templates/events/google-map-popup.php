<?php
/**
 * Created by IntelliJ IDEA.
 * User: tuanphp
 * Date: 31/10/2014
 * Time: 14:37
 */
?>
<div id="_popup_google_map" data-active="active">
    <div class="modal-content">
        <div class="copy" id="_popup_google_map_full">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
        <div class="fa fa-close _close" data-owner="#_popup_google_map"></div>
    </div>
    <div class="overlay"  data-owner="#_popup_google_map"></div>
</div>