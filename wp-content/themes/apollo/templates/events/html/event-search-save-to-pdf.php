<?php
$eventData = isset($template_args['searchResults']) ? $template_args['searchResults'] : array();
$printType = isset($template_args['printType']) ? $template_args['printType'] : '';
$searchTerms = isset($template_args['searchTerms']) ? $template_args['searchTerms'] : '';
$cssHideForPrint = !empty($printType) ? 'style="display:none;"' : '';
$isPrinting = !empty($printType);
$tplHeader = Apollo_App::get_static_html_content(Apollo_DB_Schema::_HEADER_PRINTING_SEARCH_EVENT);
$tplFooter = Apollo_App::get_static_html_content(Apollo_DB_Schema::_FOOTER_PRINTING_SEARCH_EVENT);
?>

<html>
<head><title><?php _e( 'Print Event Search Results', 'apollo' ) ?></title>
    <?php include APOLLO_TEMPLATES_DIR. '/partials/no-index.php' ?>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css"/>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/print.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/save-to-pdf-event.css" type="text/css"/>
    <style type="text/css">
        <?php echo Apollo_App::getPrimarycolorCSSContent(); ?>
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
</head>
<body id="bd-print-page">

<div class="top-custom">
    <div class="top-head" style="padding: 20px;">
        <?php echo $tplHeader; ?>
    </div>
</div>
<div class="wrapper">
    <form action="#" method="post" id="prt-event-form" enctype="multipart/form-data">
    <div class="block-detail" <?php echo $cssHideForPrint; ?>>
        <div class="b-btn">
            <button class="checkAllBtn btn btn-b edu-btn"><?php _e( 'Check All', 'apollo' ) ?></button>
            <button class="unCheckAllBtn btn btn-b edu-btn"><?php _e( 'UnCheck All', 'apollo' ) ?></button>
            <button class="btn btn-b edu-btn" type="submit" value="print" name="print_btn"><?php _e( 'Print', 'apollo' ) ?></button>
            <button class="btn btn-b edu-btn" type="submit" value="saveToPDF" name="save_to_pdf_btn"><?php _e( 'Save To PDF', 'apollo' ) ?></button>
        </div>
    </div>
    <?php if(!empty($searchTerms)) : ?>
        <div class="apl-event-search-terms"><?php echo $searchTerms ?></div>
    <?php endif; ?>
    <?php if(!empty($eventData)) : ?>
            <table class="searching-list" cellpadding="0" cellspacing="0" border="0">
                <?php foreach($eventData as $pCateID => $listEvent) : ?>
                <tr class="block-detail">
                    <td class="hd-blk">
                        <?php
                        $priCateItem = get_term($pCateID,'event-type');
                        $termName = !empty($priCateItem) ? $priCateItem->name : '';
                        ?>
                        <h3 style="font-size:18pt; font-weight: bold; color: #000000; text-transform: uppercase;"><?php echo $termName; ?></h3>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <?php
                    foreach($listEvent as $p) :
                        $e = get_event($p);
                        $registered_org = $e->get_register_org();
                        $org = !empty($registered_org) ? get_org($registered_org->org_id) : '';
                        $org_term_name = $e->get_meta_data( Apollo_DB_Schema::_APL_EVENT_TMP_ORG );
                        $org_term_name = $org_term_name !== false ? $org_term_name : __("Unknown", "apollo");
                        $org_full_title = !empty($org) ? $org->get_full_title() : '';
                        $org_name = !empty($org) && !empty($org_full_title) ? $org_full_title : $org_term_name;
                        $venue_id = $e->get_meta_data(Apollo_DB_Schema::_APOLLO_EVENT_VENUE);
                        $venue = $venue_id !== false ? get_venue($venue_id) : '';
                        $venue_name = !empty($venue) ? $venue->get_full_title() : __("Unknown", "apollo");
                        $summary = $e->post->post_excerpt;
                    ?>
                        <tr class="block-detail">
                            <td style="padding: 10px 0;">
                                <table cellpadding="0" cellspacing="0" border="0">
                                    <tr style="">
                                        <td width="1"></td>
                                        <td height="150" width="150" valign="top" style="background-color: #CCCCCC;" >
                                            <div class="search-img" style="height:150px; padding: 0;">
                                                <?php echo $e->get_image('print-event-img-size', array(),
                                                    array(
                                                        'aw' => false,
                                                        'ah' => true,
                                                    ),
                                                    'fixed'
                                                ) ?>
                                            </div>
                                        </td>
                                        <td width="10"></td>
                                        <td class="info-content">
                                            <div class="search-info">
                                                <span class="ev-tt bold-text" style="font-size: 18pt; font-weight: bold;"><?php echo $e->get_full_title(); ?></span>
                                                <p class="meta auth" style="font-size:14pt;"><?php _e(sprintf('Presented by <span class="bold-text">%s</span> <span class="venue-event"> at <span class="bold-text">%s</span> </span>',$org_name,$venue_name), 'apollo'); ?></p>
                                                <div class="searchdate">
                                                <span class="sch-date" style="font-size: 14pt;">
                                                    <?php $e->render_sch_date() ?>
                                                </span>
                                                </div>
                                                <div class="desc">
                                                <br />
                                                <span class="sch-date" style="font-size: 14pt;">
                                                    <?php echo $summary; ?>
                                                </span>
                                                </div>
                                            </div>
                                            <div class="cbox" <?php echo $cssHideForPrint; ?>>
                                                <input type="checkbox" name="prt-event-cb[]" value="<?php echo $e->id; ?>" class="select-prt-cb" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </table>

    <?php else : ?>
        <div class="block-detail">
            <?php _e("No Event Search Results","apollo"); ?>
        </div>
    <?php endif; ?>
    <div class="block-detail" <?php echo $cssHideForPrint; ?>>
        <div class="b-btn">
            <button class="checkAllBtn btn btn-b edu-btn"><?php _e( 'Check All', 'apollo' ) ?></button>
            <button class="unCheckAllBtn btn btn-b edu-btn"><?php _e( 'UnCheck All', 'apollo' ) ?></button>
            <button class="btn btn-b edu-btn" type="submit" value="print" name="print_btn"><?php _e( 'Print', 'apollo' ) ?></button>
            <button class="btn btn-b edu-btn"  type="submit" value="saveToPDF" name="save_to_pdf_btn"><?php _e( 'Save To PDF', 'apollo' ) ?></button>
        </div>
    </div>
    </form>
</div>
<div class="print-footer">
    <div class="" style="padding: 20px;">
        <?php echo $tplFooter; ?>
    </div>
</div>

</body>

</html>