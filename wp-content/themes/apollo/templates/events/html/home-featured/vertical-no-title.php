<?php
    $verType  = of_get_option(Apollo_DB_Schema::_HOME_FEATURED_TYPE);
    $VerClass = $verType == 'ver1' ? 'four' : '';
?>

<div class="featured-event-blk <?php echo $VerClass ?>">
    <h3  class="<?php echo isset($featureBlockTitleStyle) ? $featureBlockTitleStyle : ''?>">
        <span class="fea-ttl__text"><?php echo of_get_option(Apollo_DB_Schema::_FEATURED_EVENTS_LABEL, Apollo_Display_Config::DEFAULT_FEATURED_EVENTS_LABEL); ?></span>
    </h3>
    <div class="wrapper-featured clearfix wrapper-vertical-no-title">
        <?php
        $page          = isset($_REQUEST['page']) ? max(1, intval($_REQUEST['page'])) : 1;
        $limit         = of_get_option( Apollo_DB_Schema::_NUMBER_FEATURED_EVENTS, Apollo_Display_Config::PAGE_SIZE );
        $adapterTopten = new List_Event_Adapter($page, $limit);

        // RENDER TOP TEN
        if ( $active_top_ten ) {
            $templateType      = of_get_option( Apollo_DB_Schema::_HOME_FEATURED_TYPE );
            $templateName      = __DIR__ . '/items/top-ten/_panel-list-items-ver-no-title.php';
            $flagDetectNewData = $adapterTopten->checkRenewTopTenCache($templateName);
            $flagDetectNewData = true;

            if( $flagDetectNewData ){
                // force get new event data.
                List_Event_Adapter::emptyTopTenFileCachedHandler();
                $adapterTopten->getToptenEventForBlog(get_current_blog_id(), $limit);
                $isEmpty = $adapterTopten->isEmpty();
                $adapterTopten->isGetNewTopTenEvent = true;
                // turn-off flag detection for case : read new visit.json data and re-caching file. Turn off for getting only cached data from cached file html on next rendering.
                update_option(Apollo_DB_Schema::_TOPTEN_CACHE_FLAG_DETECTION,0);
            }
            ?>
            <div class="item-cat vertical">
                <div class="fea-evt-row">
                    <?php echo $adapterTopten->getPanelHtml(); ?>
                </div>
            </div>
            <?php
        }

        if ( $features_category ):
            $adapter_cat = new List_Event_Adapter($page, $limit);
            $html = '';
            if(!empty($adapter_cat->cacheFileClass)){
                $html = $adapter_cat->cacheFileClass->get();
            }
            if (!$html):
                $adapter_cat->displayed_events = $adapterTopten->events;
                ob_start();
                foreach ( $features_category as $i => $fc ):
                    $term = get_term_by( 'slug', $fc, Apollo_DB_Schema::_EVENT_PT.'-type' );
                    if ( ! $term ) continue;

                    /**
                     * We always have 4 tabs
                     */
                    $columnCounter = $adapterTopten->isEmpty() ? 3 : 2;
                    if ( ($i > $columnCounter && $active_top_ten)   || (!$active_top_ten && $i > 4) ) {
                        break;
                    }

                    $adapter_cat->get_home_featured_category_event( $fc, $i );
                    $asName = $adapter_cat->getAsName($i) ? $adapter_cat->getAsName($i) : $term->name;
                    ?>
                    <!-- Start each tab -->
                    <div class="item-cat vertical">
                        <div class="fea-evt-row">
                            <?php
                                echo $adapter_cat->getPanelHtml();
                                if ( $adapter_cat->totalPost < $adapter_cat->pageSize ) {
                                    $adapter_cat->get_auto_fill_tab_upcomming_events( $fc, $adapter_cat->pageSize - $adapter_cat->totalPost );
                                    if ( ! $adapter_cat->isEmpty() ) {
                                        echo $adapter_cat->getPanelHtml();
                                    }
                                }
                            ?>
                        </div>
                    </div>
                    <!-- End Start each tab -->
                    <?php
                endforeach;
                // Save cache
                $html = ob_get_contents();
                ob_end_clean();
                if(!empty($adapter_cat->cacheFileClass)){
                    $adapter_cat->cacheFileClass->save($html, $adapter_cat->getLatestEndDate());
                }
            endif; // End $cache

            echo $html;

        endif; ?>
    </div> <!-- End wrapper-r -->
    <div class="show-more">
        <a data-viewmore="<?php _e('VIEW MORE', 'apollo') ?>" data-viewless="<?php _e('VIEW LESS', 'apollo') ?>" href="#" id="evt-bottom-blk" class="btn btn-l">
            <?php _e('VIEW MORE', 'apollo') ?>
        </a>
    </div>
</div>
