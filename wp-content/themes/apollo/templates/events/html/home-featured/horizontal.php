<div class="featured-event-blk featured-event-hor">
    <h3 class="<?php echo isset($featureBlockTitleStyle) ? $featureBlockTitleStyle : '' ?>">
        <span class="fea-ttl__text"><?php echo of_get_option(Apollo_DB_Schema::_FEATURED_EVENTS_LABEL, Apollo_Display_Config::DEFAULT_FEATURED_EVENTS_LABEL); ?></span>
    </h3>

    <?php
    $page = isset($_REQUEST['page']) ? max(1, intval($_REQUEST['page'])) : 1;

    /*@ticket #16983: Octave Theme - [CF] 20180730 - Apply horizontal style for the home feature event block on the home page*/
    $horizontalStyle = apply_filters('apl_get_featured_event_horizontal_style', array());

    $rowTitleStyle = of_get_option(Apollo_DB_Schema::_ROW_TITLE_STYLE, '');

    $classHorizontalStyle = '';
    $wrapperScrollElement = '';
    $amountEvents = of_get_option('max_total_events', 4);

    if (!empty($horizontalStyle) && !$rowTitleStyle){
        $classHorizontalStyle =  isset($horizontalStyle['fea_evt_list_scroll']) ? $horizontalStyle['fea_evt_list_scroll'] : '';
        $wrapperScrollElement =  isset($horizontalStyle['wrapper_scroll_action']) ? $horizontalStyle['wrapper_scroll_action'] : '';
    }

    /*@ticket 17252 */
    if(isset ($htDateOpts) && $htDateOpts === 'below_tile'){
        $dateBubbleClass = "under-image__event-time";
    }

    $adapter_cat = new List_Event_Adapter($page, $amountEvents);

    if ( $active_top_ten ) {
        $templateType = of_get_option( Apollo_DB_Schema::_HOME_FEATURED_TYPE );
        if ($templateType == 'ver1') {
            $templateName = __DIR__.'/items/top-ten/_panel-list-items-ver1.php';
        } else {
            $templateName = __DIR__.'/items/top-ten/_panel-list-items.php';
        }
        $flagDetectNewData = $adapter_cat->checkRenewTopTenCache($templateName);
        if($flagDetectNewData){
            // force get new event data.
            List_Event_Adapter::emptyTopTenFileCachedHandler();
            $adapter_cat->getToptenEventForBlog(get_current_blog_id(),$amountEvents);
            $isEmpty = $adapter_cat->isEmpty();
            $adapter_cat->isGetNewTopTenEvent = true;
            // turn-off flag detection for case : read new visit.json data and re-caching file. Turn off for getting only cached data from cached file html on next rendering.
            update_option(Apollo_DB_Schema::_TOPTEN_CACHE_FLAG_DETECTION,0);
        }
    ?>
        <div class="item-cat <?php echo isset($dateBubbleClass) ? $dateBubbleClass : '' ?> <?php echo $rowTitleStyle ?>">
            <div class="topten tab-row">
                <span class="tab-tt"><?php echo of_get_option(Apollo_DB_Schema::_TOPTEN_LABEL_TAB,__(Apollo_Display_Config::TOP_TEN_LABEL ,'apollo')); ?></span>
                <?php echo $wrapperScrollElement; ?>
            </div>
            <div class="fea-evt-row <?php echo $classHorizontalStyle ?>">
                <?php
                    echo $adapter_cat->getPanelHtml(true);
                ?>
            </div>
        </div>
    <?php

    }

    if ( $features_category ):
        $html = '';
        if(!empty($adapter_cat->cacheFileClass)){
            $html = $adapter_cat->cacheFileClass->get();
        }
        if (!$html):
            $adapter_cat->displayed_events = $adapter_cat->events;
            ob_start();

            foreach ( $features_category as $i => $fc ):

                $term = get_term_by( 'slug', $fc, Apollo_DB_Schema::_EVENT_PT.'-type' );
                if ( ! $term ) continue;

                $adapter_cat->get_home_featured_category_event( $fc, $i );
                $asName = $adapter_cat->getAsName($i) ? $adapter_cat->getAsName($i) : $term->name;
    ?>
                <!-- Start each tab -->
                <div class="item-cat <?php echo isset($dateBubbleClass) ? $dateBubbleClass : '' ?>  <?php echo $rowTitleStyle ?>">

                    <div style="<?php $adapter_cat->theBorderTab($i) ?>" class="tab-row">
                        <span style="<?php $adapter_cat->theBackgroundTab($i) ?>" class="tab-tt">
                            <a href="<?php echo $adapter_cat->getCategoryLink($i, get_term_link($term)) ?>">
                            <?php echo $asName ?></a>
                        </span>
                        <?php echo $wrapperScrollElement; ?>
                    </div>

                    <div class="fea-evt-row <?php echo $classHorizontalStyle ?>">
                        <!-- Render items -->
                        <?php
                            echo $adapter_cat->getPanelHtml(false, false);
                            if ( $adapter_cat->totalPost < $adapter_cat->pageSize ) {
                                $adapter_cat->get_auto_fill_tab_upcomming_events( $fc, $adapter_cat->pageSize - $adapter_cat->totalPost );
                                if ( ! $adapter_cat->isEmpty() ) {
                                    echo $adapter_cat->getPanelHtml(false, false);
                                }
                            }
                        ?>
                        <?php echo apply_filters('apl_add_view_more_event_category', '',get_term_link($term), $asName); ?>
                        <!-- End Render items -->
                    </div> <!-- End fea-evt-row -->

                </div><!-- End Start each tab -->
    <?php
            endforeach;

            $html = ob_get_contents();
            ob_end_clean();

            if(!empty($adapter_cat->cacheFileClass)){
                $adapter_cat->cacheFileClass->save($html, $adapter_cat->getLatestEndDate());
            }
        endif; // End cache

        echo $html;

    endif;

    ?>


</div>