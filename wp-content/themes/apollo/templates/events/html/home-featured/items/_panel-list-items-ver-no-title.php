<?php
if ( !empty($this->events) ):
    foreach($this->events as $event):
        $_a_event = get_event($event);
        if ($_a_event->isPrivateEvent()) continue;
        if (!$_a_event->get_title()) continue;

        // SOCIAL SHARE INFO
        $_o_summary   = $_a_event->get_summary(of_get_option(Apollo_DB_Schema::_APL_EVENT_HOME_FEATURED_CHARACTERS_DESCRIPTION, 150));
        $arrShareInfo = $_a_event->getShareInfo();

        // detect current layout : right_sidebar_one_column, right_sidebar_two_columns, full_960
        $isFullLayout = false;
        if('full_960' == of_get_option(Apollo_DB_Schema::_DEFAULT_HOME_LAYOUT, 'full_960') ) {
            $isFullLayout  = true;
        }

        // Keep it for the expire time caching
        $this->setLatestEndDate($_a_event->{Apollo_DB_Schema::_APOLLO_EVENT_END_DATE});

        ?>

        <div class="fea-evt-item vertical item-feature-event <?php if (isset($itemClass)) echo $itemClass; ?>">
            <div class="img-wrap">
                <a class="evt-img" href="<?php echo $_a_event->get_permalink() ?>">
                    <?php echo $_a_event->get_image('medium', array(), array('aw' => true, 'ah' => true,), 'normal'); ?>
                </a>
            </div>
            <p class="evt-time <?php if ( $isFullLayout ) echo 'time-full-layout'; ?>"><?php echo $_a_event->render_sch_date() ?></p>
            <a class="evt-title" href="<?php echo $_a_event->get_permalink() ?>"> <?php echo $_a_event->get_title() ?></a>
            <p class="evt-description">
                <?php
                if( of_get_option(Apollo_DB_Schema::_ENABLE_THE_TEXT_DESCRIPTION, 1)) {
                    if ($_o_summary['have_more'] === true):
                        echo $_o_summary['text'] . '...';
                    else:
                        echo $_o_summary['text'];
                    endif;
                }
                ?>
            </p>
            <div class="evt-action">
                <a href="<?php echo $_a_event->get_permalink() ?>" class="btn btn-l">more info</a>

                <?php if ( $isFullLayout ): ?>
                    <?php SocialFactory::social_btns( array( 'info' => $arrShareInfo, 'id' => $_a_event->id ) ); ?>
                <?php else: ?>
                    <div class="social-has-sidebar">
                        <?php SocialFactory::social_btns( array( 'info' => $arrShareInfo, 'id' => $_a_event->id ) ); ?>
                    </div>
                <?php endif ?>
            </div>
        </div>

    <?php endforeach; ?>
<?php endif; ?>
