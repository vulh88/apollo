<?php
$time_start = $_arr_show_date['unix_start'];
$time_end = $_arr_show_date['unix_end'];

$mStart = Apollo_App::apl_date('M', $time_start);
$mEnd   = Apollo_App::apl_date('M', $time_end);
$dStart = date_i18n('d', $time_start);
$dEnd   = date_i18n('d', $time_end);

$htDateOpts = of_get_option(Apollo_DB_Schema::_HOME_FEATURED_DATE_OPTION,'');
$dateBubble = $htDateOpts == 'circle_tl' ? 'evt-date-bubble' :'';

if($_arr_show_date['type'] !== 'none'):
?>
    <div class="left-event-time <?php echo $htDateOpts == 'flags' ? 'nw-evt-blue' : $dateBubble; ?>" style="<?php echo  isset($color) && $color ? 'background:'.$color.'' : 'background-color:' .of_get_option(Apollo_DB_Schema::_PRIMARY_COLOR,'').''; ?>">
        <?php
        if ( ($time_end - $time_start) / (24*3600*30) < Apollo_App::configDateRange() ) {

            if ( $mStart != $mEnd ) {
        ?>
                <div class="month">
                    <span><?php echo $mStart ?></span>
                    <span><?php echo $dStart ?></span>
                </div>

                <?php if($_arr_show_date['type'] === 'two'): ?>
                    <div class="th">
                        <?php _e('-', 'apollo'); ?></div>
                    <div class="date">
                        <span><?php echo $mEnd ?></span>
                        <span><?php echo $dEnd ?></span>
                    </div>
                <?php endif;
            } else { ?>
                <div class="month">
                    <span><?php echo $mStart ?></span>
                    <span><?php echo $_arr_show_date['type'] == 'two' ? $dStart. '&nbsp;' .'-'. '&nbsp;'. $dEnd : $dStart ?></span>
                </div>
            <?php
            }
        } else {
        ?>
            <div class="month ongoing"><?php _e( 'Ongoing', 'apollo' ); ?></div>
        <?php
        }
        ?>
        <?php // These html is implementing for date bubble displaying as flag
            if($htDateOpts == 'flags'):
        ?>
            <div class="evt-arr">
                <svg id="Layer_1" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="9px" height="40px" viewbox="0 0 9 40" enable-background="new 0 0 9 40" xml:space="preserve">
                    <polyline fill="<?php echo isset($color) && $color ? $color : of_get_option(Apollo_DB_Schema::_PRIMARY_COLOR,''); ?>" points="0,0 9,0 2.954,20.023 9,40 0,40 "></polyline>
                </svg>
            </div>
        <?php endif; ?>
    </div>
<?php
endif;