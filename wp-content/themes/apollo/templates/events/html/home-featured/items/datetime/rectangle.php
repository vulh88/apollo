<?php
$time_start = $_arr_show_date['unix_start'];
$time_end = $_arr_show_date['unix_end'];

$mStart = Apollo_App::apl_date('M', $time_start); // Translate 3 characters of date time
$mEnd   = Apollo_App::apl_date('M', $time_end);// Translate 3 characters of date time

$dStart = date_i18n('d', $time_start);
$dEnd   = date_i18n('d', $time_end);

$outGoing = sprintf( '<div class="month ongoing">%s</div>', __( 'Ongoing', 'apollo' ) );

if($_arr_show_date['type'] !== 'none'):
    $notLagerThanThreeMonths = (($time_end - $time_start) /  (24*3600*30)) < Apollo_App::configDateRange();
?>
    <div class="event-time <?php echo $dateClass ?>" style="<?php echo  isset($color) && $color ? 'background:'.$color.'' : '' ?>">
        <?php
        switch($dateClass):
            case 'right-pos':
                if ( $notLagerThanThreeMonths ) {

                    if ( $mStart != $mEnd ) {
                    ?>
                        <div class="month">
                            <span><?php echo $mStart ?></span>
                            <span><?php echo $dStart ?></span>
                        </div>

                        <?php if($_arr_show_date['type'] === 'two'): ?>
                            <div class="th"><?php _e('-', 'apollo') ?></div>
                            <div class="date">
                                <span><?php echo $mEnd ?></span>
                                <span><?php echo $dEnd ?></span>
                            </div>
                        <?php endif;

                    } else { ?>
                        <div class="month">
                            <span><?php echo $mStart ?></span>
                            <span><?php echo $_arr_show_date['type'] === 'two' ? $dStart. '&nbsp;-&nbsp;'. $dEnd : $dStart; ?></span>
                        </div>
                    <?php
                    }

                } else {
                    echo $outGoing;
                }
            break;

            case 'bottom-pos':
            case 'top-pos':
            case 'top-pos rec_above_image':
                ?>
                <div class="md">
                    <?php
                    if ( $notLagerThanThreeMonths ) {
                        $mEnd = $mStart == $mEnd ? '' : $mEnd. '&nbsp;';
                        echo $mStart, '&nbsp;', $dStart;
                        echo $_arr_show_date['type'] == 'two' ?  '&nbsp;-&nbsp;'. $mEnd. $dEnd : '';
                    } else {
                        echo $outGoing;
                    }
                    ?>
                </div>
            <?php
            break;


        endswitch;

    ?>
    </div> <!-- End event-time -->
<?php
endif;