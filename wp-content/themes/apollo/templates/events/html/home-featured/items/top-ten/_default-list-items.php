<?php
$events = isset($template_args['events']) ? $template_args['events'] : array();
$tabColor = isset($template_args['tabColor']) ? $template_args['tabColor'] : '';
$dateBoxLocation = isset($template_args['dateBoxLocation']) ? $template_args['dateBoxLocation'] : '';
if(!empty($events)):
    $count_i = 1;
    $_layout = of_get_option(Apollo_DB_Schema::_DEFAULT_HOME_LAYOUT);
    $hoverEffect = of_get_option(Apollo_DB_Schema::_ACTIVATE_HOVER_EFFECT_IN_GRID,1);

    $discountConfig = AplEventFunction::getConfigDiscountIcon();

    /*@ticket #17348: 0002410: wpdev55 - Requirements part2 - [Event] Display event discount text*/
    $enableDiscountDescription = $discountConfig['enable-discount-description']
        && of_get_option(Apollo_DB_Schema::_DEFAULT_HOME_LAYOUT, 'right_sidebar_one_column') !== 'right_sidebar_two_columns'
        && (of_get_option(Apollo_DB_Schema::_HOME_FEATURED_TYPE, 'sta') === 'sta' || of_get_option(Apollo_DB_Schema::_HOME_FEATURED_TYPE, 'sta') === 'three_columns');

    $icon_fields = maybe_unserialize( get_option( Apollo_DB_Schema::_APL_EVENT_ICONS ) );

    foreach($events as $event):
        //REPARE INFORMATION
        $_a_event = get_event($event);
        if (!$_a_event->get_title()) continue;
        if ($_a_event->isPrivateEvent()) continue;

        /// SHOW BUY TICKET OR DISCOUNT
        $ticket_url     = $_a_event->get_meta_data( Apollo_DB_Schema::_ADMISSION_TICKET_URL , Apollo_DB_Schema::_APOLLO_EVENT_DATA );
        $discount_url   = $_a_event->get_meta_data( Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL , Apollo_DB_Schema::_APOLLO_EVENT_DATA );

        ///SOCIAL SHARE INFO
        $_o_summary = $_a_event->get_summary(of_get_option(Apollo_DB_Schema::_APL_EVENT_HOME_FEATURED_CHARACTERS_DESCRIPTION,150));
        $arrShareInfo = $_a_event->getShareInfo();

        $venues     = Apollo_App::get_post_type_item( $_a_event->{Apollo_DB_Schema::_APOLLO_EVENT_VENUE} );
        $stripDate = $_a_event->renderRectangleDateTime($dateBoxLocation, isset($tabColor) ? $tabColor : '');
        $linkToDetailPage = '';
        if ($_layout == 'right_sidebar_two_columns' && $hoverEffect) {
            $linkToDetailPage = ' data-url="'.$_a_event->get_permalink().'" data-type="link"';
        }
        $even_dataIcons = $_a_event->getEventDataIcons($discountConfig['display_all_icon'], $icon_fields)
        ?>
        <article class="blog-itm <?php if($hoverEffect) echo 'on-hover'?> <?php echo $linkToDetailPage ? 'apl-link-detail-page' : ''; ?>" <?php echo $linkToDetailPage; ?> >
            <?php  echo $_a_event->renderCircleTopLeftDateTime($dateBoxLocation, isset($tabColor) ? $tabColor : ''); ?>
            <div class="pic">
                <?php  echo $dateBoxLocation == 'rec_above_image' ? $stripDate : ''; ?>
                <a href="<?php echo $_a_event->get_permalink() ?>"><?php echo $_a_event->get_image('medium', array(),
                        array(
                            'aw' => true,
                            'ah' => true,
                        ),
                        'normal'
                    );

                    // Get the datetime box in top-left
                    echo $dateBoxLocation != 'rec_above_image' ? $stripDate : '';
                    ?>
                </a>
            </div>
            <div class="blog-t">
                <h2 class="blog-ttl"> <a href="<?php echo $_a_event->get_permalink() ?>"> <?php echo $_a_event->get_title() ?></a></h2>
                <p class="meta auth"><?php echo $_a_event->renderOrgVenueHtml(); ?></p>
                <div class="apl-event-discount-icon-text">
                <?php if($enableDiscountDescription){
                    $_a_event->renderIcons($icon_fields,$even_dataIcons);
                    $_a_event->generateDiscountText($discountConfig['include_discount_url']);
                }?>
                </div>
                <p class="desc">
                    <?php if (of_get_option(Apollo_DB_Schema::_ENABLE_THE_TEXT_DESCRIPTION, 1)) {
                        if ($_o_summary['have_more'] === true): ?>
                            <?php echo $_o_summary['text'] . '...' ?>
                            <a class="read_more"
                               href="<?php echo $_a_event->get_permalink() ?>"><?php _e('More Info', 'apollo') ?></a>
                            <?php
                        else:
                            echo $_o_summary['text'];
                        endif;
                    }
                    ?>
                </p>
                <div class="b-share-cat">
                    <?php
                    SocialFactory::social_btns( array( 'info' => $arrShareInfo, 'id' => $_a_event->id ) );
                    ?>
                </div>


                <div class="b-btn __inline_block_fix_space">

                    <?php
                    // Only apply fullwidth 960 for home page
                    $btnClass = $_layout == 'full_960' && $_SERVER["REQUEST_URI"] == '/' ? 'hfull' : '';
                    echo $_a_event->renderTickerBtn(array(
                        'class' => 'btn btn-b '. $btnClass
                    )) ?>

                    <?php echo $_a_event->renderCheckDCBtn(array(
                        'class' => 'btn btn-b '. $btnClass
                    )) ?>
                    <?php
                    $bookmarkIcon = $_a_event->renderBookmarkBtn(array(
                        'class' => 'btn-bm btn btn-b add-it-btn '. $btnClass
                    ));
                    echo apply_filters('octave_custom_bookmark_btn', $bookmarkIcon);
                    ?>
                </div>

            </div>
        </article>
        <?php $count_i++; ?>
    <?php endforeach;
else: ?>
    <?php _e('Data not found!', 'apollo') ?>
    <?php
endif;