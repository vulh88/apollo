<?php
$events = isset($template_args['events']) ? $template_args['events'] : array();
$hasWrapper = isset($template_args['hasWrapper']) ? $template_args['hasWrapper'] : false;
$tabColor = isset($template_args['tabColor']) ? $template_args['tabColor'] : '';
$dateBoxLocation = isset($template_args['dateBoxLocation']) ? $template_args['dateBoxLocation'] : '';
$itemClass = isset($template_args['itemClass']) ? $template_args['itemClass'] : '';
if(!empty($events)):
    foreach($events as $event):
        //REPARE INFORMATION
        $_a_event = get_event($event);
        if ($_a_event->isPrivateEvent()) continue;
        if (!$_a_event->get_title()) continue;

        $_o_summary = $_a_event->get_summary(of_get_option(Apollo_DB_Schema::_APL_EVENT_HOME_FEATURED_CHARACTERS_DESCRIPTION,150));
        ?>
        <div class="fea-evt-item <?php if (isset($itemClass)) echo $itemClass; ?> apl-link-detail-page" data-url="<?php echo $_a_event->get_permalink(); ?>" data-type="link" >
            <div class="dv-front">
                <a class="event-img" href="<?php echo $_a_event->get_permalink() ?>">
                    <?php
                    echo $_a_event->get_image('medium', array(),
                        array(
                            'aw' => true,
                            'ah' => true,
                        ),
                        'normal'
                    );

                    ?>
                </a>
                <a class="ev-tt" href="<?php echo $_a_event->get_permalink() ?>"> <?php echo $_a_event->get_title() ?></a>
                <div class="meta auth"><?php echo $_a_event->renderOrgVenueHtml(); ?></div>
                <div class="dp"><?php echo $_a_event->render_sch_date() ?></div>
            </div>

        </div>
    <?php endforeach;endif; ?>
