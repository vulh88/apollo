<?php
    $events = isset($template_args['events']) ? $template_args['events'] : array();
    $hasWrapper = isset($template_args['hasWrapper']) ? $template_args['hasWrapper'] : false;
    $tabColor = isset($template_args['tabColor']) ? $template_args['tabColor'] : '';
    $dateBoxLocation = isset($template_args['dateBoxLocation']) ? $template_args['dateBoxLocation'] : '';
    $itemClass = isset($template_args['itemClass']) ? $template_args['itemClass'] : '';
//    if ($hasWrapper) echo '<div class="fea-evt-row">';
    if(!empty($events)):
    foreach($events as $event):
        //REPARE INFORMATION
        $_a_event = get_event($event);
    
        if (!$_a_event->get_title()) continue;
        if ($_a_event->isPrivateEvent()) continue;
    
        $_o_summary = $_a_event->get_summary(of_get_option(Apollo_DB_Schema::_APL_EVENT_HOME_FEATURED_CHARACTERS_DESCRIPTION,150));
        $stripDate = $_a_event->renderRectangleDateTime($dateBoxLocation, isset($tabColor) ? $tabColor : '');
        $permarlink = $_a_event->get_permalink();
        $eventTitle = $_a_event->get_title();
        ?>
        <div class="fea-evt-item <?php if (!empty($itemClass)) echo $itemClass; ?> apl-link-detail-page" data-url="<?php echo $_a_event->get_permalink(); ?>" data-type="link">
            <div class="dv-front">
                <?php  echo $dateBoxLocation == 'rec_above_image' ? $stripDate : ''; ?>
                <a class="event-img" href="<?php echo $_a_event->get_permalink() ?>">
                    <?php 
                        echo $_a_event->get_image('medium', array(),
                            array(
                                'aw' => true,
                                'ah' => true,
                            ),
                                'normal'
                        ); 

                        // Get the datetime box in top-left
                    echo $dateBoxLocation != 'rec_above_image' ? $stripDate : '';
                    ?>
                </a>
                <?php echo apply_filters('custome_style_horizontal_feature_ev_tt', "<a class='ev-tt' href='$permarlink'>$eventTitle</a>" , $_a_event);  ?>
            </div>

            <?php 
                // Get the datetime box in top-left
                echo $_a_event->renderCircleTopLeftDateTime($dateBoxLocation, isset($tabColor) ? $tabColor : ''); 
            ?>

            <div class="dv-back">
                <?php echo apply_filters('custome_style_horizontal_feature_back_title', '', $_a_event) ?>
                <div class="meta auth"><?php echo $_a_event->renderOrgVenueHtml(); ?></div>
                <div class="back-desc">
                    <?php
                    if( of_get_option(Apollo_DB_Schema::_ENABLE_THE_TEXT_DESCRIPTION, 1)) {
                        if ($_o_summary['have_more'] === true):
                            echo $_o_summary['text'] . '...';
                        else:
                            echo $_o_summary['text'];
                        endif;
                    }
                    ?>
                </div>

                <a <?php echo $tabColor ? 'style="color: '.$tabColor.'"' : "" ?> href="<?php echo $_a_event->get_permalink() ?>" class="more"><?php _e('View more', 'apollo') ?></a>
            </div> <!-- end dv-back -->

        </div>
<?php endforeach;endif; ?>
<?php //if (isset($hasWrapper) && $hasWrapper) echo '</div>'; ?>
