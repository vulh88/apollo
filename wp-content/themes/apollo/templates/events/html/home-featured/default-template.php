<?php
    if ( $main_content ):
?>
<div id="featured-event-label" class = "<?php echo isset($featureBlockTitleStyle) ? $featureBlockTitleStyle : '' ?>">
    <span class="fea-ttl__text"><?php echo of_get_option(Apollo_DB_Schema::_FEATURED_EVENTS_LABEL, Apollo_Display_Config::DEFAULT_FEATURED_EVENTS_LABEL); ?></span>
</div>
<nav class="nav-tab nav-feature-event">
    <ul class="tab-list">
        <?php if( $active_top_ten ): ?>
        <li class="selected"><a href="#" data-id="1"><?php echo of_get_option(Apollo_DB_Schema::_TOPTEN_LABEL_TAB,__(Apollo_Display_Config::TOP_TEN_LABEL ,'apollo')); ?></a></li>
        <?php endif; ?>

        <?php
        $page = isset($_REQUEST['page']) ? max(1, intval($_REQUEST['page'])) : 1;
        $category_page_size = of_get_option( Apollo_DB_Schema::_NUMBER_FEATURED_EVENTS, Apollo_Display_Config::PAGE_SIZE );
        $adapter_cat = new List_Event_Adapter($page, $category_page_size);

        if ( $features_category ):
            if (!is_array($features_category)) $features_category = array($features_category);
            foreach ( $features_category as $i => $fc ):
                $asName = $adapter_cat->getAsName($i) ? $adapter_cat->getAsName($i) : '';

                if (!$asName) {
                    if ($term = get_term_by( 'slug', $fc, Apollo_DB_Schema::_EVENT_PT.'-type' )) {
                        $asName = $term->name;
                    }
                }

                echo '<li><a href="#" data-id="'. $fc .'">'. $asName .'</a></li>';
            endforeach;
        endif;
        ?>
    </ul>
</nav>

<div class="tab-bt"></div>
<?php
    else:
        echo ! $top_sub_contain ? '&nbsp' : '';
    endif;
?>

<?php

    $page = isset($_REQUEST['page']) ? max(1, intval($_REQUEST['page'])) : 1;
    $category_page_size = of_get_option( Apollo_DB_Schema::_NUMBER_FEATURED_EVENTS, Apollo_Display_Config::PAGE_SIZE );
    $adapter = new List_Event_Adapter($page, $category_page_size, 'top_ten');

    $classThreecolumn = apply_filters('apl_get_class_three_columns_for_featured_event', '');

    $classItemPerRow = apply_filters('apl_render_class_item_per_row', '');

    if( $active_top_ten ):
            $flagDetectNewData = $adapter->checkRenewTopTenCache(__DIR__.'/items/top-ten/_default-list-items.php');
            if($flagDetectNewData){
                // force get new event data.
                List_Event_Adapter::emptyTopTenFileCachedHandler();
                $adapter->getToptenEventForBlog(get_current_blog_id());
                $isEmpty = $adapter->isEmpty();
                $adapter->isGetNewTopTenEvent = true;
                // turn-off flag detection for case : read new visit.json data and re-caching file. Turn off for getting only cached data from cached file html on next rendering.
                update_option(Apollo_DB_Schema::_TOPTEN_CACHE_FLAG_DETECTION,0, 'no');
            }
        ?>
        <div data-target="1" class="blog-blk <?php echo $classThreecolumn?>">
            <section class="list-blog <?php echo $classItemPerRow; ?>">
            <?php echo $adapter->getHtml(true); ?>
            </section>
        </div>
    <?php endif; ?>

<?php
    if ( $features_category ):

        if (!is_array($features_category)) $features_category = array($features_category);

        $html = '';
        if(!empty($adapter_cat->cacheFileClass)){
            $html = $adapter_cat->cacheFileClass->get();
        }
        if (!$html):
            $adapter_cat->displayed_events = $adapter->events;
            // Thienld : use ob_flush() at here in order for ensuring that all previous buffer are flushed before getting html for block featured event.
            ob_flush();
            ob_start();
            foreach ( $features_category as $i => $fc ):
                if ( ! $fc ) continue;
?>
                <div data-target="<?php echo $fc; ?>" data-cache="<?php echo $adapter_cat->cacheFileClass->localSiteCacheEnabled; ?>"
                     class="blog-blk lazy-load-image-wrapper <?php echo $classThreecolumn ?>">
                    <section class="list-blog <?php echo $classItemPerRow; ?>">
                    <?php
                        $adapter_cat->get_home_featured_category_event( $fc, $i );
                        $adapter_cat->is_topten = false;
                        $empty = 1;
                        if ( ! $adapter_cat->isEmpty() ) {
                            echo $adapter_cat->getHtml();
                            $empty = 0;
                        }

                        if ( $adapter_cat->totalPost < $adapter_cat->pageSize ) {
                            $adapter_cat->get_auto_fill_tab_upcomming_events( $fc, $adapter_cat->pageSize - $adapter_cat->totalPost );
                            if ( ! $adapter_cat->isEmpty() ) {
                            echo $adapter_cat->getHtml();
                                $empty = 0;
                            }
                        }

                        if ( $empty ) {
                            echo $adapter->the_notice_empty();
                        }

                    ?>
                    </section>
                </div>
<?php
            endforeach;

            // Save cache
            $html = ob_get_contents();
            ob_end_clean();
            if(!empty($adapter_cat->cacheFileClass)){
                $adapter_cat->cacheFileClass->save($html, $adapter_cat->getLatestEndDate());
            }
        endif; // End $cache

        echo $html;

    endif; // End $features_category
?>
