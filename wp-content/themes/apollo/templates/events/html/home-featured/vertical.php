<?php
$verType = of_get_option(Apollo_DB_Schema::_HOME_FEATURED_TYPE);
$VerClass = $verType == 'ver1' ? 'four' : '';
?>

<div class="featured-event-blk <?php echo $VerClass ?>">
    <h3 class="<?php echo isset($featureBlockTitleStyle) ? $featureBlockTitleStyle : ''?>">
        <span class="fea-ttl__text"><?php echo of_get_option(Apollo_DB_Schema::_FEATURED_EVENTS_LABEL, Apollo_Display_Config::DEFAULT_FEATURED_EVENTS_LABEL); ?></span>
    </h3>
    <input type="checkbox" id="tb">
    
    <div class="wrapper-l">
        <?php 
        $page = isset($_REQUEST['page']) ? max(1, intval($_REQUEST['page'])) : 1;
        $limit = of_get_option( Apollo_DB_Schema::_NUMBER_FEATURED_EVENTS, Apollo_Display_Config::PAGE_SIZE );
        $adapterTopten = new List_Event_Adapter($page, $limit);
        // Render topten
        if ( $active_top_ten ) {
            $templateType = of_get_option( Apollo_DB_Schema::_HOME_FEATURED_TYPE );
            if ($templateType == 'ver1') {
                $templateName = __DIR__.'/items/top-ten/_panel-list-items-ver1.php';
            } else {
                $templateName = __DIR__.'/items/top-ten/_panel-list-items.php';
            }
            $flagDetectNewData = $adapterTopten->checkRenewTopTenCache($templateName);
            if($flagDetectNewData){
                // force get new event data.
                List_Event_Adapter::emptyTopTenFileCachedHandler();
                $adapterTopten->getToptenEventForBlog(get_current_blog_id(), $limit);
                $isEmpty = $adapterTopten->isEmpty();
                $adapterTopten->isGetNewTopTenEvent = true;
                // turn-off flag detection for case : read new visit.json data and re-caching file. Turn off for getting only cached data from cached file html on next rendering.
                update_option(Apollo_DB_Schema::_TOPTEN_CACHE_FLAG_DETECTION,0);
            }
            ?>
            <div class="item-cat vertical <?php echo $VerClass ?> <?php if( of_get_option(Apollo_DB_Schema::_HOME_FEATURED_DATE_OPTION) != 'circle_tl' && $verType != 'ver1' ) echo 'un-bubble'?>">
                <div class="tab-row">
                    <span class="tab-tt vertical"><?php
                        echo of_get_option(Apollo_DB_Schema::_TOPTEN_LABEL_TAB,__(Apollo_Display_Config::TOP_TEN_LABEL ,'apollo'));
                        ?></span>
                </div>
                <div class="fea-evt-row">
                    <?php
                    echo $adapterTopten->getPanelHtml(true);
                    ?>
                </div>
            </div>
        <?php
        }
        
        if ( $features_category ):
            $adapter_cat = new List_Event_Adapter($page, $limit);
            $html = '';
            if(!empty($adapter_cat->cacheFileClass)){
                $html = $adapter_cat->cacheFileClass->get();
            }
            if (!$html):
                $adapter_cat->displayed_events = $adapterTopten->events;
                ob_start();
                foreach ( $features_category as $i => $fc ):
                    $term = get_term_by( 'slug', $fc, Apollo_DB_Schema::_EVENT_PT.'-type' );
                    if ( ! $term ) continue;

                    /**
                     * We alway have 4 tabs
                     */
                    if ( ($i > 2 && $active_top_ten) || (!$active_top_ten && $i > 3) ) break;

                    if ( ( $i == 2 && !$active_top_ten ) || ($i == 1 && $active_top_ten) ) echo '</div><!-- End wrapper-l -->'
                        . '<div class="show-more"><a data-viewmore="'.__('VIEW MORE', 'apollo').'" data-viewless="'.__('VIEW LESS', 'apollo').'" href="#" id="evt-top-blk" class="btn btn-l">'.__('VIEW MORE', 'apollo').'</a></div>'
                        . '<input type="checkbox" id="bb"><div class="wrapper-r">';

                    $adapter_cat->get_home_featured_category_event( $fc, $i );
                    $asName = $adapter_cat->getAsName($i) ? $adapter_cat->getAsName($i) : $term->name;
            ?>
                    <!-- Start each tab -->
                    <div class="item-cat vertical <?php echo $VerClass ?> <?php if( of_get_option(Apollo_DB_Schema::_HOME_FEATURED_DATE_OPTION) != 'circle_tl' && $verType != 'ver1') echo 'un-bubble'?>">
                        <div class="tab-row" style="<?php $adapter_cat->theBorderTab($i) ?>">
                            <span style="<?php $adapter_cat->theBackgroundTab($i) ?>" class="tab-tt vertical">
                                <a href="<?php echo get_term_link($term) ?>"><?php echo $asName; ?></a>
                            </span>
                        </div>
                        <?php
                            echo $adapter_cat->getPanelHtml();
                            if ( $adapter_cat->totalPost < $adapter_cat->pageSize ) {
                                $adapter_cat->get_auto_fill_tab_upcomming_events( $fc, $adapter_cat->pageSize - $adapter_cat->totalPost );
                                if ( ! $adapter_cat->isEmpty() ) {
                                    echo $adapter_cat->getPanelHtml();
                                }
                            }
                        ?>
                    </div><!-- End Start each tab -->

        <?php
                endforeach;
                // Save cache
                $html = ob_get_contents();
                ob_end_clean();
                if(!empty($adapter_cat->cacheFileClass)){
                    $adapter_cat->cacheFileClass->save($html, $adapter_cat->getLatestEndDate());
                }
            endif; // End $cache

            echo $html;

        endif; ?>
    </div> <!-- End wrapper-r -->
    <div class="show-more"><a data-viewmore="<?php _e('VIEW MORE', 'apollo') ?>" data-viewless="<?php _e('VIEW LESS', 'apollo') ?>" href="#" id="evt-bottom-blk" class="btn btn-l"><?php _e('VIEW MORE', 'apollo') ?></a></div>
   
</div>