<?php
$eventData = isset($template_args['searchResults']) ? $template_args['searchResults'] : array();
$printType = isset($template_args['printType']) ? $template_args['printType'] : '';
$searchTerms = isset($template_args['searchTerms']) ? $template_args['searchTerms'] : '';
$cssHideForPrint = !empty($printType) ? 'style="display:none;"' : '';
$isPrinting = !empty($printType);
$tplHeader = Apollo_App::get_static_html_content(Apollo_DB_Schema::_HEADER_PRINTING_SEARCH_EVENT);
$tplFooter = Apollo_App::get_static_html_content(Apollo_DB_Schema::_FOOTER_PRINTING_SEARCH_EVENT);
?>

<html>
<head><title><?php _e( 'Print Event Search Results', 'apollo' ) ?></title>
    <?php include APOLLO_TEMPLATES_DIR. '/partials/no-index.php' ?>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css"/>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/print.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/print-event.css" type="text/css"/>
    <style type="text/css">
        <?php echo Apollo_App::getPrimarycolorCSSContent(); ?>
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <?php
        function pm_remove_all_scripts() {
            global $wp_scripts;
            $wp_scripts->queue = array();
        }
        add_action('wp_print_scripts', 'pm_remove_all_scripts', 100);
        function pm_remove_all_styles() {
            global $wp_styles;
            $wp_styles->queue = array();
        }
        add_action('wp_print_styles', 'pm_remove_all_styles', 100);

        // Fix ERROR: Sorry, there was an error. Please be sure JavaScript and Cookies are enabled in your browser and try again.
        wp_head();
     ?>
</head>
<body id="bd-print-page">

<div class="top-custom">
    <?php if (!empty($tplHeader)) : ?>
    <div class="top-head">
        <?php echo $tplHeader; ?>
    </div>
    <?php endif; ?>
</div>
<div class="wrapper">
    <form action="#" method="post" id="prt-event-form" enctype="multipart/form-data">
    <?php if (empty($printType) || !empty($searchTerms)) : ?>
        <div class="block-detail">
            <?php if(empty($printType)): ?>
                <div class="b-btn">
                    <button class="checkAllBtn btn btn-b edu-btn"><?php _e( 'Check All', 'apollo' ) ?></button>
                    <button class="unCheckAllBtn btn btn-b edu-btn"><?php _e( 'UnCheck All', 'apollo' ) ?></button>
                    <button class="btn btn-b edu-btn" type="submit" value="print" name="print_btn"><?php _e( 'Print', 'apollo' ) ?></button>
                    <button class="btn btn-b edu-btn" type="submit" value="saveToPDF" name="save_to_pdf_btn"><?php _e( 'Save To PDF', 'apollo' ) ?></button>
                </div>
            <?php endif ?>
            <?php if(!empty($searchTerms)) : ?>
                <div class="apl-event-search-terms"><?php echo $searchTerms ?></div>
            <?php endif; ?>
        </div>
    <?php endif; ?>
    <?php if(!empty($eventData)) : ?>
            <nav class="searching-list">
                <?php foreach($eventData as $pCateID => $listEvent) : ?>
                    <li class="block-detail hd-blk">
                        <?php
                        if($pCateID) {
                            $priCateItem = get_term($pCateID, 'event-type');
                            $termName = !empty($priCateItem) ? $priCateItem->name : '';
                        } else {
                            $termName = __('None Primary Category');
                        }
                        ?>
                        <h3><?php echo strtoupper($termName); ?></h3>
                    </li>
                    <?php
                    $indexOfMaxEventsInEachPrCate = of_get_option(Apollo_DB_Schema::_MAXIMUM_EVENT_DISPLAY_IN_PRIMARY_CATEGORY,'');
                    foreach($listEvent as $index => $p) :
                        if(!empty($indexOfMaxEventsInEachPrCate) && ( $index > (intval($indexOfMaxEventsInEachPrCate) - 1) ) ) :
                            // break loop if number of displayed events within each primary category > configured value of indexOfMaxEventsInEachPrCate
                            break;
                        endif;
                        $e = get_event($p);
                        $registered_org = $e->get_register_org();
                        $org = !empty($registered_org) ? get_org($registered_org->org_id) : '';
                        $org_term_name = $e->get_meta_data( Apollo_DB_Schema::_APL_EVENT_TMP_ORG );
                        $org_term_name = $org_term_name !== false ? $org_term_name : __("Unknown", "apollo");
                        $org_full_title = !empty($org) ? $org->get_full_title() : '';
                        $org_name = !empty($org) && !empty($org_full_title) ? '<a href="'.$org->get_permalink().'" target="_blank">'.$org_full_title.'</a>' : $org_term_name;
                        $venue_id = $e->get_meta_data(Apollo_DB_Schema::_APOLLO_EVENT_VENUE);
                        $venue = $venue_id !== false ? get_venue($venue_id) : '';
                        $venue_name = !empty($venue) ? '<a href="'.$venue->get_permalink().'" target="_blank" >'.$venue->get_full_title().'</a>' : __("Unknown", "apollo");
                        $summary = $e->post->post_excerpt;
                    ?>
                        <li class="block-detail">
                            <div class="search-img"><a
                                    href="<?php echo $e->get_permalink() ?>"><?php echo $e->get_image('print-event-img-size', array(),
                                        array(
                                            'aw' => false,
                                            'ah' => true,
                                        ),
                                        'fixed'
                                    ) ?></a></div>

                            <div data-type="link" data-url="<?php echo $e->get_permalink() ?>" class="info-content">
                                <div class="search-info"><a href="<?php echo $e->get_permalink() ?>"><span
                                            class="ev-tt"><?php echo $e->get_title() ?></span></a>

                                    <p class="meta auth"><?php _e(sprintf('Presented by %s <span class="venue-event"> at %s </span>',$org_name,$venue_name), 'apollo'); ?></p>
                                    <div class="searchdate">
                                    <span class="sch-date">
                                        <?php $e->render_sch_date() ?>
                                    </span>
                                    </div>
                                    <div class="desc">
                                        <p><?php
                                            echo $summary;
                                            ?></p>
                                    </div>
                                </div>
                                <div class="cbox" <?php echo $cssHideForPrint; ?>>
                                    <input type="checkbox" name="prt-event-cb[]" value="<?php echo $e->id; ?>" class="select-prt-cb" />
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </nav>

    <?php else : ?>
        <div class="block-detail">
            <?php _e("No Event Search Results","apollo"); ?>
        </div>
    <?php endif; ?>
    <div class="block-detail" <?php echo $cssHideForPrint; ?>>
        <div class="b-btn">
            <button class="checkAllBtn btn btn-b edu-btn"><?php _e( 'Check All', 'apollo' ) ?></button>
            <button class="unCheckAllBtn btn btn-b edu-btn"><?php _e( 'UnCheck All', 'apollo' ) ?></button>
            <button class="btn btn-b edu-btn" type="submit" value="print" name="print_btn"><?php _e( 'Print', 'apollo' ) ?></button>
            <button type="submit" class="btn btn-b edu-btn" type="submit" value="saveToPDF" name="save_to_pdf_btn"><?php _e( 'Save To PDF', 'apollo' ) ?></button>
        </div>
    </div>
    </form>
</div>
<div class="print-footer">
    <div class="prt-ft-blk">
        <?php echo $tplFooter; ?>
    </div>
</div>
<script type="text/javascript">
    (function ($) {
        window.APOLLO =  window.APOLLO || {};
        window.APOLLO.printEventJSHandler = (function(){
            var module = {};
            var eForm = null;
            module.init = function(){
                $(document).ready(function () {
                    eForm = $("#prt-event-form");
                    <?php if($isPrinting && $printType == 'print') : ?>
                        window.print();
                    <?php else : ?>
                        $(".checkAllBtn").off("click").on("click",checkAll);
                        $(".unCheckAllBtn").off("click").on("click",unCheckAll);
                        $(".printBtn").off("click").on("click",printHandler);
                        $(".saveToPDFBtn").off("click").on("click",saveToPDFHandler);
                        $(".checkAllBtn").trigger("click");
                    <?php endif; ?>

                });
                return module;
            };

            var checkAll = function(e){
                e.preventDefault();
                $(".select-prt-cb").prop('checked', true);
            };

            var unCheckAll = function(e){
                e.preventDefault();
                $(".select-prt-cb").prop('checked', false);
            };

            var printHandler = function(e){
                e.preventDefault();
                if($(".select-prt-cb:checked").length > 0){
                    var postUrl = window.location.href;
                    eForm.attr("action",postUrl);
                    eForm.append('<input type="hidden" name="print_type" value="print" />');
                    eForm.submit();
                } else {
                    alert("Have no selected events, Please select to print");
                }
                return false;
            };

            var saveToPDFHandler = function (e) {
                e.preventDefault();
                if($(".select-prt-cb:checked").length > 0){
                    var postUrl = window.location.href;
                    eForm.attr("action",postUrl);
                    eForm.append('<input type="hidden" name="print_type" value="saveToPDF" />');
                    eForm.submit();
                } else {
                    alert("Have no selected events, Please select to print");
                }
                return false;
            };

            return module.init();
        })();

    })(jQuery);
</script>

</body>

</html>