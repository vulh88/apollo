<?php
$event_id = get_query_var( '_apollo_event_id' );
$event = get_event( get_post( $event_id ) );
if ( ! $event->post ) :
    echo Apollo_App::getTemplatePartCustom('templates/404-print');
    exit;
endif;

global $apollo_event_access;
$full_content = $event->get_content();

$admission_detail  = $event->get_meta_data( Apollo_DB_Schema::_ADMISSION_DETAIL, Apollo_DB_Schema::_APOLLO_EVENT_DATA );
$admission_contact = $event->get_meta_data( Apollo_DB_Schema::_ADMISSION_PHONE, Apollo_DB_Schema::_APOLLO_EVENT_DATA );
$admission_email = $event->get_meta_data( Apollo_DB_Schema::_ADMISSION_TICKET_EMAIL, Apollo_DB_Schema::_APOLLO_EVENT_DATA );
$additionalTime =  $event->get_meta_data(Apollo_DB_Schema::_APL_EVENT_ADDITIONAL_TIME,Apollo_DB_Schema::_APOLLO_EVENT_DATA);

$_arr_show_date = $event->getShowTypeDateTime('picture');
$_arr_str_date  = $event->getShowTypeDateTime('full_string');


$_arr_field = array(
    Apollo_DB_Schema::_VENUE_ADDRESS1,
    Apollo_DB_Schema::_VENUE_CITY,
    Apollo_DB_Schema::_VENUE_STATE,
    Apollo_DB_Schema::_VENUE_ZIP,
);

$str_address_all = $event->getStrAddress();
$arrCoor = Apollo_Google_Coor_Cache::getCoordinateByAddressInLocalDB($str_address_all);
$sCoor = $arrCoor ? implode(",", $arrCoor) : '';

$browser_key = of_get_option(Apollo_DB_Schema::_GOOGLE_API_KEY_BROWSER);

$_arr_acc = $event->get_meta_data( Apollo_DB_Schema::_E_CUS_ACB, Apollo_DB_Schema::_APOLLO_EVENT_DATA );
$logo =  Apollo_App::getHeaderLogo(Apollo_DB_Schema::_HEADER );
$eventACBInfo = $event->get_meta_data( Apollo_DB_Schema::_E_ACB_INFO, Apollo_DB_Schema::_APOLLO_EVENT_DATA );

/*@ticket #16512: Add 'Parking Info' section to the 'print' view. */
$parkingInfo = "";
$_venue = get_venue($event->{Apollo_DB_Schema::_APOLLO_EVENT_VENUE});
$venue_access_mode = of_get_option(Apollo_DB_Schema::_ENABLE_VENUE_ACCESSIBILITY_MODE, 1);
if($_venue && $_venue->id){
    $venue_meta_data_info = Apollo_App::apollo_get_meta_data($_venue->id ,Apollo_DB_Schema::_APL_VENUE_DATA);
    if (!is_array($venue_meta_data_info)) {
        $venue_meta_data_info = unserialize($venue_meta_data_info);
    }
    $parkingInfo = isset($venue_meta_data_info[Apollo_DB_Schema::_VENUE_PARKING_INFO]) ? Apollo_App::convertContentEditorToHtml($venue_meta_data_info[Apollo_DB_Schema::_VENUE_PARKING_INFO]) : "";

    if ($venue_access_mode) {
        $_arr_acc = isset($venue_meta_data_info[Apollo_DB_Schema::_E_CUS_ACB]) ? $venue_meta_data_info[Apollo_DB_Schema::_E_CUS_ACB] : array();
        $eventACBInfo = isset($venue_meta_data_info[Apollo_DB_Schema::_E_ACB_INFO]) ? $venue_meta_data_info[Apollo_DB_Schema::_E_ACB_INFO] : '' ;
    }
}
$enable_individual_date_time = of_get_option(Apollo_DB_Schema::_ENABLE_INDIVIDUAL_DATE_TIME,1);
?><html>
<head><title><?php _e( _e( 'Print detail event | ', 'apollo' ) . $event->get_title(), 'apollo' ) ?></title>
    <?php include APOLLO_TEMPLATES_DIR. '/partials/no-index.php' ?>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css"/>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/print.css" type="text/css"/>
    <style>
        <?php echo Apollo_App::getPrimarycolorCSSContent() ?>
    </style>

    <?php if($arrCoor === false): ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <?php endif; ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $browser_key ?>"></script>

</head>
<body id="bd-print-page">

<div class="top">
    <div class="top-head">
        <div class="logo print-page"><a href="<?php echo home_url() ?>"> <img src="<?php echo $logo; ?>"/></a></div>
    </div>
</div>
<div class="wrapper">
    <div class="block-detail">
        <?php  $event->render_circle_date(); ?>
    </div>
    <div class="pic"><a><?php echo $event->get_image( 'medium' ); ?></a></div>
    <div class="event-des">
        <h2 class="tt"><?php echo $event->get_title(); ?></h2>
        <p class="meta auth"><?php echo $event->renderOrgVenueHtml(); ?></p>
        <?php
        $a_full_excerpt = $event->get_excerpt();
        if ( $a_full_excerpt['text'] ):
            ?>
            <div class="print-summary-event" >
                <?php echo $event->get_full_excerpt(); ?>
            </div>
        <?php endif; ?>

    </div>
    <div class="desc">
        <p></p>
        <?php echo $full_content['text'] ?>
    </div>


    <?php if($admission_contact || $admission_detail) { ?>
        <div class="block-detail">

            <div class="tt-bar"><a><i class="fa fa-bars fa-2x"></i></a><span><?php _e( 'Admission Info', 'apollo' ) ?></span></div>
            <div class="tt-info">
                <div class="apl-internal-content"><?php echo Apollo_App::convertContentEditorToHtml($admission_detail) ?></div>
                <?php if($admission_contact ) { ?>
                    <p> <strong><?php _e( 'Phone', 'apollo' ) ?>: </strong><?php echo $admission_contact; ?></p>
                <?php } ?>
                <?php if($admission_email ) { ?>
                    <p> <strong><?php _e( 'Email', 'apollo' ) ?>: </strong><?php echo $admission_email; ?></p>
                <?php } ?>
                <?php
                if (!empty($additionalTime) && !$enable_individual_date_time) : ?>
                    <div class="apl-internal-content"><p><b><?php _e('Additional time info: ','apollo') ?> </b> <?php echo Apollo_App::convertContentEditorToHtml($additionalTime) ?></p></div>
                <?php endif; ?>
            </div>

        </div>
    <?php } ?>
    <div class="block-detail">
        <div class="tt-bar"><a><i class="fa fa-clock-o fa-2x"></i></a><span><?php _e( 'Dates & Times', 'apollo' ) ?></span></div>
        <div class="tt-info">
            <p><i class="fa fa-calendar fa-2x"></i>
                <label height="26px" style="margin-left:10px"><?php _e( 'Dates', 'apollo' ) ?>:  </label><span style="margin-left:10px">
                              <?php echo $_arr_str_date['start_date'] ?> - <?php echo $_arr_str_date['end_date'] ?>
                          </span>
            </p>
            <?php
            if ($enable_individual_date_time) :
                $in_dt = $event->render_periods_time( $event->get_periods_time() );

                if ( $in_dt ):
                    ?>
                    <p>
                        <label><?php _e( 'Individual Date & Times', 'apollo' ) ?>:</label>
                    </p>
                    <ul class="ind-time">
                        <?php echo $in_dt; ?>
                    </ul>
                <?php endif; ?>
                <?php
                if(!empty($additionalTime)):
                    ?>
                    <div class="apl-internal-content"><p><b><?php _e('Additional time info: ','apollo') ?> </b> <?php echo Apollo_App::convertContentEditorToHtml($additionalTime) ?></p></div>
                <?php endif;  ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="block-detail">
        <div class="tt-bar"><a><i class="fa fa-map-marker fa-2x"></i></a><span><?php _e( 'Location Info', 'apollo' ) ?></span></div>
        <div class="tt-info">
            <div class="loc">
                <p> <strong style="font-size:14pt"><?php echo $event->get_location_name() ?></strong></p>
                <p class="bottom"><?php echo $event->get_location_data( $_arr_field ); ?></p>
            </div>
            <div class="map">
                <div class="lo-map" id="_event_map"></div>
            </div>

        </div>
    </div>

    <?php if ($parkingInfo && $parkingInfo != "") : ?>
    <div class="block-detail">
        <div class="tt-bar"><a><i class="fa fa-bus fa-2x"></i></a><span><?php _e( 'Parking Info', 'apollo' ) ?></span></div>
        <div class="tt-info apl-internal-content">
            <p><?php echo $parkingInfo ?></p>
        </div>
    </div>
    <?php endif ?>

    <?php
    if ( $_arr_acc  || $eventACBInfo):
        ?>
        <div class="block-detail">
            <div class="tt-bar"><a><i class="fa fa-book fa-2x"></i></a><span><?php _e( 'Accessibility Info', 'apollo' ) ?></span></div>
            <div class="tt-info">
                <?php if ( $_arr_acc ){ ?>

                    <ul class="acces-list">
                        <?php
                        foreach ( $apollo_event_access as $img => $label ):
                            if ( ! in_array( $img , $_arr_acc ) ) continue;
                            ?>
                            <li>
                                <ul>
                                    <li><img src="<?php echo get_template_directory_uri() ?>/assets/images/event-accessibility/2x/<?php echo $img ?>.png"></li>
                                    <li>
                                        <label><?php echo $label ?></label>
                                    </li>
                                </ul>
                            </li>
                        <?php endforeach; ?>

                    </ul>
                <?php } ?>
                <?php
                if(isset($eventACBInfo) && !empty($eventACBInfo)) :
                    $acbInfo200 = Apollo_App::getStringByLength($eventACBInfo, 200);
                    $acbFullContent = Apollo_App::convertContentEditorToHtml($eventACBInfo);
                    if ( $acbInfo200['text'] ):
                        ?>
                        <div class="acbFullContent apl-internal-content" >
                            <?php echo $acbFullContent; ?>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>

        </div>
    <?php endif; ?>

</div>
<script>

    var latlng = '';

    function m_initialize() {
        var mapOptions = {
            zoom: 15,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: true
        };

        var map = new google.maps.Map(document.getElementById('_event_map'),
            mapOptions);

        var marker = new google.maps.Marker({
            position: map.getCenter(),
            map: map,
            title: '',
            icon: '<?php echo get_stylesheet_directory_uri() ?>/assets/images/icon-location.png'
        });
    }

    <?php if($arrCoor === false): ?>

    function gm_initialize() {
        var geocoder = new google.maps.Geocoder();

        var address = '<?php echo $event->getStrAddress() ?>';

        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {

                var lat = results[0].geometry.location.lat();
                var lng = results[0].geometry.location.lng();

                latlng = new google.maps.LatLng(lat, lng);
                m_initialize(latlng);

                $.ajax({
                    url: '<?php echo admin_url("admin-ajax.php") ?>',
                    data: {
                        nonce: '<?php echo wp_create_nonce("save-coor") ?>',
                        action: 'apollo_save_coordinate',
                        lat: lat,
                        lng: lng,
                        addr: address
                    }
                });

            } else {
                alert("<?php _e('We\'re sorry. A map for this listing is currently unavailable.', 'apollo');?>");
            }
        });
    }

    google.maps.event.addDomListener(window, 'load', gm_initialize);

    <?php else: ?>
    latlng = new google.maps.LatLng('<?php echo $arrCoor[0] ?>', '<?php echo $arrCoor[1] ?>');
    google.maps.event.addDomListener(window, 'load', m_initialize);
    <?php endif; ?>
</script>

</body>
<style>
    .logo.print-page img{
        max-height:81px;
    }
</style>
</html>

