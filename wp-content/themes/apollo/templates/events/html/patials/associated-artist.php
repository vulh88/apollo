<?php
$artists = isset($template_args['artists']) ? $template_args['artists'] : array();
$_html= '';
foreach ( $artists as $art )
{
    $artist = get_artist($art);
    //print_r($artist);
    $_html .= '<div class="org-type artist">';
    if ($artist->is_linked()) {
        $_html.='<a class="link" href="'.$artist->get_permalink(). '"><div class="artist-pic">';
        $_html.=  $artist->get_image( 'thumbnail');
        $_html.='</div><span>'.$artist->post->post_title. '</span></a>';
    } else {
        $_html.=  '<div class="artist-pic">'.$artist->get_image( 'thumbnail' ).'</div><span>'.$artist->post->post_title.'</span>';
    }
    $_html.='</div>';
}
echo $_html;