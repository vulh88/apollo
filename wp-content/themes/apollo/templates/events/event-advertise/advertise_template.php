<?php
$doNotDisplayTitle = isset($adData['do_not_display_title']) ? (bool)$adData['do_not_display_title'] : false;
?>

<div class="r-blk big event-advertise <?php echo isset($adData['custom_class']) ? $adData['custom_class'] : ''; ?>">
    <?php if ($has_title = isset($adData['title']) && $adData['title'] && !$doNotDisplayTitle): ?>
        <h3 class="r-ttl"><span><?php _e("" . $adData['title'] . "", 'apollo') ?></span></h3>
    <?php endif; ?>

    <div class="r-blk-ct apl-internal-content">
        <?php
        if (isset($adData['content']) && $adData['content'])
            echo $adData['content'];
        ?>
    </div>
</div>