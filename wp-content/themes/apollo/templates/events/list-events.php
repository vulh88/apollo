<?php

/**
 * @page: content current page
 */
class List_Event_Adapter {

    public $events;
    public $pageSize;
    public $page;
    public $totalPost;

    public $_order_by;
    public $_order = 'ASC';
    public $_field_order;
    public $_template;
    public $displayed_events = array();
    public $is_topten = false;
    private $tabColorData = '';
    private $tabBg = '';
    private $currentTabColor = '';
    private $asNameData = '';
    private $asName = '';
    public $latestEndDate = '';
    public $cacheFileClass = '';
    public $isGetNewTopTenEvent = false;

    public function __construct($page, $pageSize = Apollo_Display_Config::PAGE_SIZE) {

        // Check module is avaiable
        if ( ! Apollo_App::is_avaiable_module( 'event' ) ) {
            return false;
        }

        // Init cache file

        $this->cacheFileClass = aplc_instance('APLC_Inc_Files_HomeFeatured');

        // Change meta table
        add_filter( 'posts_join' , array( $this, 'filter_meta_join' ) );
        add_filter( 'posts_orderby', array( $this, 'replace_meta_table' ) );
        add_filter( 'posts_groupby', array( $this, 'replace_meta_table' ) );
        add_filter( 'posts_where', array( $this, 'replace_meta_table' ) );

		if(empty($pageSize)) {
			$pageSize = Apollo_Display_Config::PAGE_SIZE;
		}

		$this->pageSize = intval( $pageSize );
		$this->page = $page;

        // order
        $order = of_get_option( Apollo_DB_Schema::_EVENT_SETTING_ORDER );

        // Tab color
        $this->tabColorData = of_get_option( Apollo_DB_Schema::_HOME_CAT_FEATURES. '_of_color', 'false' );
        // as name
        $this->asNameData = of_get_option( Apollo_DB_Schema::_HOME_CAT_FEATURES. '_of_asname', 'false' );

        /* @ticket #17733: CF] 20180928 - [Homepage][Feature events] Custom link for the 'Spotlight' category page - Item 5 */
        $this->useCustomLink = of_get_option( Apollo_DB_Schema::_HOME_CAT_FEATURES. '_use_cutom_link', array() );
        $this->customLink = of_get_option( Apollo_DB_Schema::_HOME_CAT_FEATURES. '_custom_link', array() );

        switch ( $order ) {
            case 'ALPHABETICAL_ASC':
                $this->_field_order = '';
                $this->_order_by    = 'title';
                $this->_order = 'ASC';
                break;

            case 'ALPHABETICAL_DESC':
                $this->_field_order = '';
                $this->_order_by    = 'title';
                $this->_order = 'DESC';
                break;

            case 'END_DATE_ASC':
                $this->_field_order = Apollo_DB_Schema::_APOLLO_EVENT_END_DATE;
                $this->_order_by    = 'meta_value';
                $this->_order = 'ASC';
                break;

            case 'START_DATE_ASC':
            default:
                $this->_field_order = Apollo_DB_Schema::_APOLLO_EVENT_START_DATE;
                $this->_order_by    = 'meta_value';
                $this->_order = 'ASC';
                break;
        }
    }

    /**
     * Set the latest date of a list featured event for caching
     */
    public function setLatestEndDate($endDate) {
        $endDateTime = strtotime($endDate);
        if (!$this->latestEndDate || (strtotime($this->latestEndDate) > $endDateTime && $endDateTime >= strtotime(current_time('Y-m-d')))) {
            $this->latestEndDate = $endDate;
        }
    }

    /**
     * Get the latest date of a list featured event for caching
     */
    public function getLatestEndDate() {
        return $this->latestEndDate;
    }

    /**
     * Get top ten events
     */
    public function getToptenEventForBlog($blog_id, $limit = "") {

        //TODO: Replace the whole using this function in the whole source to List_Event_Adapter::getTopTenEventsByBlogID() [Added by ThienLD]

        $data = Apollo_Statistic_System::getTopVisitEventForDomain($blog_id);

        if(!empty($data)) {
            $timeToday = strtotime(current_time('Y-m-d'));
            $dataType = of_get_option(Apollo_DB_Schema::_HOME_FEATURED_DATA_TYPE, 'default');
            foreach($data as $i => $d) {

                if ( strtotime($d['start_date']) < $timeToday && strtotime($d['end_date']) < $timeToday ) {
                    if (isset($_GET['db'])) {
                        var_dump($timeToday);
                        aplDebug($d);
                    }
                    continue;
                }

                if (isset($_GET['db'])) {
                    aplDebug($d);
                }

                /** @Ticket 17323 */
                if ( $limit && count($this->events) == $limit ) break;
                if ($dataType == 'only_discount') {
                    $_a_event = get_event($d['item_id']);
                    if ($this->checkDiscountEventForTopTen($d['item_id'], $_a_event)) {
                        $this->events[] = $d['item_id'];
                    }
                } else {
                    $this->events[] = $d['item_id'];
                }

            }
        }
    }

    /**
     * @Ticket #17323
     * @param $eventId
     * @param $eventObj
     * @return bool
     */
    public function checkDiscountEventForTopTen( $eventId, $eventObj ) {
        $discount_type = of_get_option(Apollo_DB_Schema::_EVENT_DISCOUNT_CHECKBOX_FILTER_TYPE, 'only_discount_url');
        if ( $discount_type != 'only_discount_url' ) {
            global $wpdb;
            $current_date = current_time( 'Y-m-d' );
            $ticketField = Apollo_DB_Schema::_ADMISSION_TICKET_URL;
            $sql = "
                SELECT event_id FROM {$wpdb->{Apollo_Tables::_APL_EVENT_CALENDAR}} 
                WHERE event_id = {$eventId} AND {$ticketField} IS NOT NULL AND {$ticketField} <> ''
                     AND date_event >= '{$current_date}'
            ";
            if (!$wpdb->get_col($sql)) {
                return false;
            }
        }
        if (empty($eventObj->get_meta_data(Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL , Apollo_DB_Schema::_APOLLO_EVENT_DATA))) {
            return false;
        }
        return true;
    }

    /**
     * @Ticket #17323
     * @return string
     */
    public function buildSqlQueryByDiscount() {
        $sql = '';
        if (of_get_option(Apollo_DB_Schema::_HOME_FEATURED_DATA_TYPE, 'default') == 'only_discount') {
            global $wpdb;
            require_once APOLLO_SRC_DIR . '/event/inc/class-event-search.php';
            $current_date = current_time( 'Y-m-d' );
            $discount_type = of_get_option(Apollo_DB_Schema::_EVENT_DISCOUNT_CHECKBOX_FILTER_TYPE, 'only_discount_url');
            $eventCalendarQuery = '';
            if ( $discount_type != 'only_discount_url' ) {
                $args = array(
                    'start_date_filter' => '',
                    'end_date_filter' => '',
                    'current_date' => $current_date,
                    'event_alias' => 'ec'
                );
                $eventCalendarQuery = " INNER JOIN {$wpdb->{Apollo_Tables::_APL_EVENT_CALENDAR}} ec ON ec.event_id = p.ID AND 
                                        ( ". Apollo_Event_Page::buildSQLEventsOfEventCalendar($args) ." ) 
                                       ";
            }
            $sql = $eventCalendarQuery . "
                INNER JOIN {$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}} mt_discount ON mt_discount.apollo_event_id = p.ID
                    AND mt_discount.meta_key = '".Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL."' 
                    AND mt_discount.meta_value <> '' 
            ";
        }
        return $sql;
    }

    /**
     * Get Top Ten Events by current blog
     * The list events were got from a json file
     * @param int $blog_id
     * @param int $limit
     * @return array
     * @author ThienLD
     */
    public static function getTopTenEventsByBlogID($blog_id = 0, $limit = 10) {
        $results = [];
        if ( empty($blog_id) || $blog_id === 0 ) {
            return $results;
        }
        $eventsFromJsonFile = Apollo_Statistic_System::getTopVisitEventForDomain($blog_id);
        if(!empty($eventsFromJsonFile)) {
            $timeToday = strtotime(current_time('Y-m-d'));
            foreach($eventsFromJsonFile as $i => $d) {
                if ( strtotime($d['start_date']) < $timeToday && strtotime($d['end_date']) < $timeToday ) {
                    continue;
                }
                if ( $limit && $i === $limit ) {
                    break;
                }
                $results[] = $d['item_id'];
            }
        }
        return $results;
    }

    /**
     * Get upcomming events of tab
     *
     * @access public
     * @return string
     */
    public function get_auto_fill_tab_upcomming_events( $slug, $page_size ) {

        global $wpdb;

        $_auto_fill_where = '';

        if ( $this->displayed_events ) {

            $_condition = '("'. implode( '", "' , ( array ) $this->displayed_events ) . '")' ;
            $_auto_fill_where = " ( p.ID NOT IN $_condition ) AND ";
        }

        $current_date = current_time( 'Y-m-d' );
        $event_meta_table = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};
        $type = Apollo_DB_Schema::_EVENT_PT;
        $term = get_term_by( 'slug' , $slug, $type. '-type' );

        $event_order = Apollo_Sort_Manager::getSortOneFieldForPostType();

        $sql = "
            SELECT p.ID, p.post_title, p.post_content, p.post_name, p.post_type, p.post_excerpt, p.post_author, p.post_status, 
            ".AplEventFunction::buildSqlDateRange('mt_start_d', 'mt_end_d')."
            FROM {$wpdb->posts} p ";

        $sql .= "
            INNER JOIN {$event_meta_table} mt_term_primary
                ON mt_term_primary.apollo_event_id = p.ID
                AND mt_term_primary.meta_key = '".Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID."'
                AND mt_term_primary.meta_value = {$term->term_id}
        ";

        $sql .= "INNER JOIN {$event_meta_table} mt_end_d
            ON p.ID   = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
               AND CAST( mt_end_d.meta_value AS DATE ) >= '{$current_date}'
        ";

        $sql .= "INNER JOIN {$event_meta_table} mt_start_d
                ON p.ID = mt_start_d.apollo_event_id AND mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."' ";

        if(
            $event_order['type_sort'] === 'apollo_meta'
            && $event_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
            && $event_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE )
        { // sort by meta key
            $sql .= " INNER JOIN {$event_meta_table} tblSortMeta ON tblSortMeta.apollo_event_id = p.ID AND tblSortMeta.meta_key = '{$event_order["metakey_name"]}'";
        }

        /** @Ticket #17323 */
        $sql .= $this->buildSqlQueryByDiscount();

        $sql .= " WHERE $_auto_fill_where p.post_type = '". $type ."' AND p.post_status = 'publish' ";
        $sql = Apollo_App::hidePrivateEvent($sql,'p');
        $sql .= " GROUP BY p.ID";

        if($event_order['type_sort'] === 'apollo_meta') {
            $metaOrder = array("date_range ASC");
            if(
                $event_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
                && $event_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE
            ) {
                $metaOrder[] = "tblSortMeta.meta_value {$event_order['order']}";
            }
            else if($event_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_START_DATE) {
                $metaOrder[] = "mt_start_d.meta_value {$event_order['order']}";
            }
            else if($event_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_END_DATE) {
                $metaOrder[] = "mt_end_d.meta_value {$event_order['order']}";
            }
            $metaOrder[] = "date_diff ASC";
            $sql .= " ORDER BY " . implode(', ', $metaOrder) . " ";
        }
        else if($event_order['type_sort'] === 'post') {
            $sql .= " ORDER BY p.{$event_order['order_by']} {$event_order['order']} ";
        }


        $sql .= "LIMIT ".  intval( $page_size ) ;

        $this->events = $wpdb->get_results( $sql );
    }

    /**
     * PAGE: EVENT DETAIL UPCOMMING - SHOW SAME ORGANIZATION
     */
    public function get_html_org_upcomming_events( $org_id, $current_event_id, $offset = false ) {

        if ( ! Apollo_App::is_avaiable_module( 'organization' ) ) {
            return false;
        }

        global $wpdb, $post;

		if ( ! $current_event_id ) {
            $current_event_id = $post->ID;
        }

        $apollo_meta_table   = $wpdb->apollo_eventmeta;
        $_start_date_field  = Apollo_DB_Schema::_APOLLO_EVENT_START_DATE;
        $_end_date_field    = Apollo_DB_Schema::_APOLLO_EVENT_END_DATE;
        $_current_date      = current_time( 'Y-m-d' );

        /**
         * User can change setting view more number in Admin Panel: Theme Options > General > Display Settings > "View more"
         * When view more number change in Admin Panel -> the offset need to change also.
         *
         * Example:
         * page = 1, page size = 5  => offset = 0 (5 is default display number: Apollo_Display_Config::PAGESIZE_UPCOM)
         * page = 2, page size = 30 => offset = 5 (30 is the number was configured in Admin Panel)
         * page = 3, page size = 30 => offset = 35
         * page = 4, page size = 30 => offset = 65
         *
         * @ticket #11345
         */
        if ( $this->page == 2 ) {
            $_offset = Apollo_Display_Config::PAGESIZE_UPCOM;
        } else {
            $_offset = (($this->page - 2) * $this->pageSize) + Apollo_Display_Config::PAGESIZE_UPCOM;
        }
        $_offset = $_offset . ', ';

        $arr_order = Apollo_Sort_Manager::getSortOneFieldForPostType('event');

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.ID, p.post_title, p.post_status, p.post_excerpt, p.post_name, post_type FROM {$wpdb->posts} p

            INNER JOIN {$apollo_meta_table} mt_start_d ON p.ID = mt_start_d.apollo_event_id AND mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
            INNER JOIN {$apollo_meta_table} mt_end_d ON p.ID   = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
            INNER JOIN {$wpdb->{Apollo_Tables::_APL_EVENT_ORG}} e_org ON e_org.post_id = p.ID AND e_org." . Apollo_DB_Schema::_APL_E_ORG_ID . " = '$org_id'
        ";

        if(
            $arr_order['type_sort'] === 'apollo_meta'
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE )
        { // sort by meta key
            $sql .= " INNER JOIN {$apollo_meta_table} tblSortMeta ON tblSortMeta.apollo_event_id = p.ID AND tblSortMeta.meta_key = '{$arr_order["metakey_name"]}'";
        }


        $sql .=   " WHERE p.ID <> {$current_event_id} AND p.post_status = 'publish' AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."' AND (
                        ( mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
                        AND CAST( mt_start_d.meta_value AS DATE ) >= '{$_current_date}' )
                    OR
                        ( mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
                        AND CAST( mt_end_d.meta_value AS DATE ) >= '{$_current_date}' )
                    ) ";
        $sql = Apollo_App::hidePrivateEvent($sql,'p');
        $sql .= " GROUP BY p.ID ";

        if($arr_order['type_sort'] === 'apollo_meta') {
            if(
                $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
                && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE
            ) {
                $sql .= " ORDER BY tblSortMeta.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_START_DATE) {
                $sql .= " ORDER BY mt_start_d.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_END_DATE) {
                $sql .= " ORDER BY mt_end_d.meta_value {$arr_order['order']} ";
            }
        }
        else if($arr_order['type_sort'] === 'post') {
            $sql .= " ORDER BY p.{$arr_order['order_by']} {$arr_order['order']} ";
        }

        $sql .= " LIMIT $_offset $this->pageSize ";

        $this->events       = $wpdb->get_results($sql);
        $this->totalPost    = $wpdb->get_var("SELECT FOUND_ROWS()");

        $this->_template = '_org-events.php';
        return $this->render_html();
    }

    /**
     * Get events in selected category for tab on home page
     *
     * @access public
     * @return string
     */
    public function get_home_featured_category_event( $slug, $indexColor = '' ) {

        global $wpdb;
        $event_meta_table   = $wpdb->apollo_eventmeta;
        $current_date       = current_time('Y-m-d');

        // order part
        $event_order = Apollo_Sort_Manager::getSortOneFieldForPostType();

        // Thienld : handle for sites have install wpml
        // get only spotlight event of current language
        if(Apollo_App::hasWPLM() && Apollo_App::checkExistedTermLangCol()){
            $andWhere = " AND pt.".Apollo_DB_Schema::_TERM_LANGUAGE."='".ICL_LANGUAGE_CODE."' ";
        } else {
            $andWhere = " AND 1=1";
        }

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.ID, p.post_title, p.post_content, p.post_name, p.post_type, p.post_excerpt, p.post_author, p.post_status,
            ".AplEventFunction::buildSqlDateRange('mt_start_d', 'mt_end_d')."

            FROM {$wpdb->posts} p
        ";

        $sql .= " INNER JOIN {$event_meta_table} mt_end_d
                            ON p.ID   = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
                            AND CAST( mt_end_d.meta_value AS DATE ) >= '{$current_date}'
        ";

        $sql .= "
            INNER JOIN {$wpdb->{Apollo_Tables::_POST_TERM}} pt ON pt.post_id = p.ID AND pt.". Apollo_DB_Schema::_TAB_LOCATION ."= '{$slug}'
        ";


        $sql .= " INNER JOIN {$event_meta_table} mt_start_d
                        ON p.ID = mt_start_d.apollo_event_id AND mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
                ";

        if(
            $event_order['type_sort'] === 'apollo_meta'
            && $event_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
            && $event_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE )
        { // sort by meta key
            $sql .= " INNER JOIN {$event_meta_table} tblSortMeta ON tblSortMeta.apollo_event_id = p.ID AND tblSortMeta.meta_key = '{$event_order["metakey_name"]}'";
        }

        /** @Ticket #17323 */
        $sql .= $this->buildSqlQueryByDiscount();


        $sql .=   " WHERE p.post_status = 'publish' AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."' $andWhere ";


        if ($this->displayed_events && is_array($this->displayed_events)) {
            $strDisplayedIDs = implode(',', $this->displayed_events);
            $sql .=  " AND p.ID NOT IN ($strDisplayedIDs)";
        }

        $sql = Apollo_App::hidePrivateEvent($sql,'p');
        $sql .= " GROUP BY p.ID ";

        if($event_order['type_sort'] === 'apollo_meta') {
            $metaOrder = array("date_range ASC");
            if(
                $event_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
                && $event_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE
            ) {
                $metaOrder[] = "tblSortMeta.meta_value {$event_order['order']}";
            }
            else if($event_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_START_DATE) {
                $metaOrder[] = "mt_start_d.meta_value {$event_order['order']}";
            }
            else if($event_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_END_DATE) {
                $metaOrder[] = "mt_end_d.meta_value {$event_order['order']}";
            }
            $metaOrder[] = "date_diff ASC";
            $sql .= " ORDER BY " . implode(', ', $metaOrder) . " ";
        }
        else if($event_order['type_sort'] === 'post') {
            $sql .= " ORDER BY p.{$event_order['order_by']} {$event_order['order']} ";
        }

        $sql .= " LIMIT {$this->pageSize} ";

        $this->events = $wpdb->get_results( $sql );

        $this->totalPost =  $wpdb->get_var("SELECT FOUND_ROWS()");

        // Set current tab color
        $this->setTabColor($indexColor);
    }

    public static function get_home_spotlight_events() {

        global $wpdb;
        $event_meta_table   = $wpdb->apollo_eventmeta;
        $size               = of_get_option( Apollo_DB_Schema::_NUM_HOME_SPOTLIGHT_EVENT );
        $size               = $size ? $size : Apollo_Display_Config::NUM_HOME_SPOTLIGHT;
        $post_term_table    = $wpdb->{Apollo_Tables::_POST_TERM};
        $current_date       = current_time('Y-m-d');

        // Thienld : handle for sites have install wpml
        // get only spotlight event of current language
        $andWhere = "";
        if(Apollo_App::hasWPLM() && Apollo_App::checkExistedTermLangCol()){
            $andWhere = " AND pt.".Apollo_DB_Schema::_TERM_LANGUAGE."='".ICL_LANGUAGE_CODE."' ";
        }
        $andWhere = Apollo_App::hidePrivateEvent($andWhere,'p');
        $sql = " SELECT p.* FROM {$wpdb->posts} p
            INNER JOIN {$event_meta_table} mt_end_d ON p.ID   = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
            INNER JOIN {$post_term_table} pt ON pt.post_id = p.ID AND pt.".Apollo_DB_Schema::_IS_HOME_SPOTLIGHT." = 'yes'

            WHERE p.post_status = 'publish'

                AND(
                    ( mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
                    AND CAST( mt_end_d.meta_value AS DATE ) >= '{$current_date}' )
                )

                ".$andWhere."

            GROUP BY p.ID
            ORDER BY mt_end_d.meta_value ASC
            LIMIT 0, $size
        ";

        $eventSpotlight = $wpdb->get_results( $sql);

        /**
         * @ticket #19409: [CF] 20190311 - Auto-fill the events to the home spotlight - Item 2
         */
        if(count($eventSpotlight) < $size){
            $option = of_get_option(APL_Theme_Option_Site_Config_SubTab::_EVENT_HOME_SPOTLIGHT_AUTO_FILL, 0);
            if($option){
                require_once APOLLO_INCLUDES_DIR. '/src/event/inc/class-event-search.php';
                $objSearch = new Apollo_Event_Page(false, false);

                //autofill event today
                $eventSpotlight = self::autoFillEventSpotlight($size, $objSearch, $eventSpotlight, true);
                //autofill event default
                $eventSpotlight = self::autoFillEventSpotlight($size, $objSearch, $eventSpotlight, false);
            }
        }

        // TODO: do query post using custom where, join filters.
       return $eventSpotlight;
    }

    /**
     * @ticket #19409: [CF] 20190311 - Auto-fill the events to the home spotlight - Item 2
     * @param $numberSpotlight
     * @param $objSearch
     * @param array $eventSpotlight
     * @param bool $isToday
     * @return array
     */
    private static function autoFillEventSpotlight($numberSpotlight, $objSearch, $eventSpotlight = array(), $isToday = false){

        $moreEvent = array();
        $autoSize = $numberSpotlight - count($eventSpotlight);

        if($autoSize > 0){
            if($isToday){
                $objSearch::setSpotlightToday(true);
            }
            else{
                $objSearch::setSpotlightToday(false);
            }

            $eventIds = array();

            if(!empty($eventSpotlight)) {
                foreach ($eventSpotlight as $event) {
                    if (!empty($event->ID)) {
                        $eventIds[] = $event->ID;
                    }
                }
            }
            $objSearch->search($autoSize, false, $eventIds);

            $moreEvent = $objSearch->get_results();
        }

        return !empty($moreEvent) ? array_merge($eventSpotlight, $moreEvent) : $eventSpotlight;
    }

    public function render_html( $template = '' ) {
        ob_start();
        include $template ? $template : $this->_template;
        return ob_get_clean();
    }

    public function getEvents() {
            return $this->events;
    }

    public function isEmpty() {
        return empty($this->events);
    }

    public function getHtml($isTopTen=false) {

        if (!$this->is_topten) {
            $this->setDisplayedEvents();
        }

        // Get date type
        $dateBoxLocation = of_get_option(Apollo_DB_Schema::_HOME_FEATURED_DATE_OPTION);
        $this->currentTabColor = $color = $isTopTen ? of_get_option( Apollo_DB_Schema::_PRIMARY_COLOR ) : $this->currentTabColor;
        // Thienld : handle for optimizing caching topten homepage
        if($isTopTen){
            $parseArgs = array(
                'events' => $this->events,
                'dateBoxLocation' => $dateBoxLocation,
                'tabColor' => $this->currentTabColor
            );
            return  Apollo_App::getTemplatePartCustom(__DIR__.'/html/home-featured/items/top-ten/_default-list-items.php',$parseArgs,true,true,'+1 day');
        }
        else {
            // for default rendering of other scenarios.
            ob_start();
            include __DIR__.'/html/home-featured/items/_default-list-items.php';
            return ob_get_clean();
        }

    }

    /**
     * Set displayed events
     *
     */
    public function setDisplayedEvents() {

        if (!$this->displayed_events) {
            $this->displayed_events = array();
        }
        if ($this->events) {
            foreach($this->events as $e) {
                if(isset($e->ID)) {
                    $this->displayed_events[] = $e->ID;
                }
            }
        }
    }

    public function getPanelHtml($isTopTen = false, $hasWrapper = true) {
        // Get template type
        $templateType = of_get_option( Apollo_DB_Schema::_HOME_FEATURED_TYPE );

        $itemClass = '';
        if ( $templateType == 'ver' || $templateType == 'ver1' ) $itemClass = 'vertical';

        // Get date type
        $dateBoxLocation = of_get_option(Apollo_DB_Schema::_HOME_FEATURED_DATE_OPTION);
        $this->currentTabColor = $color = $isTopTen ? of_get_option( Apollo_DB_Schema::_PRIMARY_COLOR ) : $this->currentTabColor;

        if (!$this->is_topten) {
            $this->setDisplayedEvents();
        }

        if ($templateType == 'ver1') {
            $templateName = __DIR__.'/html/home-featured/items/_panel-list-items-ver1.php';
            $topTenTemplateName = __DIR__.'/html/home-featured/items/top-ten/_panel-list-items-ver1.php';
        } else if ($templateType == 'ver-no-title') {
            $templateName       = __DIR__ . '/html/home-featured/items/_panel-list-items-ver-no-title.php';
            $topTenTemplateName = __DIR__ . '/html/home-featured/items/top-ten/_panel-list-items-ver-no-title.php';
        } else {
            $templateName = __DIR__.'/html/home-featured/items/_panel-list-items.php';
            $topTenTemplateName = __DIR__.'/html/home-featured/items/top-ten/_panel-list-items.php';
        }
        // Thienld : handle for optimizing caching topten homepage
        if($isTopTen){
            $parseArgs = array(
                'events' => $this->events,
                'dateBoxLocation' => $dateBoxLocation,
                'itemClass' => $itemClass,
                'tabColor' => $this->currentTabColor,
                'hasWrapper' => $hasWrapper
            );
            return Apollo_App::getTemplatePartCustom($topTenTemplateName,$parseArgs,true,true,'+1 day');
        }
            else {
            // for default rendering of other scenarios.
            ob_start();
            include $templateName;
            return ob_get_clean();
        }
    }

    public function the_notice_empty() {
        return '&nbsp;';
    }

    public function isShowMore() {
            $current_pos = ($this->page * $this->pageSize);
            return $current_pos < $this->totalPost;
    }

    /**
     * Check is show more events based on "View more" option in Admin panel
     *
     * @ticket #11345
     * @return bool
     */
    public function isShowMoreEvents()
    {
        if ( $this->pageSize < Apollo_Display_Config::PAGESIZE_UPCOM ) {
            $current_pos = ($this->page * $this->pageSize) + (Apollo_Display_Config::PAGESIZE_UPCOM - $this->pageSize);
        } else if ( $this->pageSize > Apollo_Display_Config::PAGESIZE_UPCOM ) {
            $current_pos = ($this->page * $this->pageSize) - ($this->pageSize - Apollo_Display_Config::PAGESIZE_UPCOM);
        } else {
            $current_pos = ($this->page * $this->pageSize);
        }

        return $current_pos < $this->totalPost;
    }

    private function resetPostData() {
            wp_reset_query();
    }

    public function filter_meta_join( $join ) {
        $join = $this->replace_meta_table( $join );
        return str_replace('post_id', 'apollo_event_id', $join);
    }

    public function replace_meta_table( $str ) {
        global $wpdb;
        return str_replace( $wpdb->postmeta, $wpdb->apollo_eventmeta, $str);
    }

    public function get_upcomming_user_events( $user_id = '', $limit = false, $orderby = '' ) {

        global $wpdb;
        $current_date = current_time( 'Y-m-d' );
        $apollo_meta_table = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};

        if ( $limit ) {
            $_offset = ($this->page-1) * $this->pageSize;

            $limit = "LIMIT $_offset, $this->pageSize";
        }

        $arr_order = Apollo_Sort_Manager::getSortOneFieldForPostType('event');

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.ID, p.post_title, p.post_content, p.post_name, p.post_type, p.post_excerpt, p.post_author, p.post_status FROM {$wpdb->posts} p
            INNER JOIN {$apollo_meta_table} mt_start_d ON p.ID = mt_start_d.apollo_event_id AND mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
            INNER JOIN {$apollo_meta_table} mt_end_d ON p.ID   = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
        ";

        if(
            $arr_order['type_sort'] === 'apollo_meta'
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE )
        { // sort by meta key
            $sql .= " INNER JOIN {$apollo_meta_table} tblSortMeta ON tblSortMeta.apollo_event_id = p.ID AND tblSortMeta.meta_key = '{$arr_order["metakey_name"]}'";
        }
        $where_status = "p.post_status = 'publish'";
        if(is_user_logged_in()) {
            $where_status .= "or p.post_status = 'pending'";
        }

        /** @Ticket #13444 - all user*/

        $sql .=   " WHERE ( $where_status ) AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."' AND (
                        ( mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
                        AND CAST( mt_start_d.meta_value AS DATE ) >= '{$current_date}' )
                    OR
                        ( mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
                        AND CAST( mt_end_d.meta_value AS DATE ) >= '{$current_date}' )
                    ) ";
            $sql = Apollo_App::hidePrivateEvent($sql,'p');
        $sql .= " GROUP BY p.ID ";

        if ( ! $orderby ) {
            if($arr_order['type_sort'] === 'apollo_meta') {
                if(
                    $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
                    && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE
                ) {
                    $sql .= " ORDER BY tblSortMeta.meta_value {$arr_order['order']} ";
                }
                else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_START_DATE) {
                    $sql .= " ORDER BY mt_start_d.meta_value {$arr_order['order']} ";
                }
                else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_END_DATE) {
                    $sql .= " ORDER BY mt_end_d.meta_value {$arr_order['order']} ";
                }
            }
            else if($arr_order['type_sort'] === 'post') {
                $sql .= " ORDER BY p.{$arr_order['order_by']} {$arr_order['order']} ";
            }
        } else {
            $sql .= $orderby;
        }

        $sql .= " $limit ";

        $this->events = $wpdb->get_results( $sql );
        $this->totalPost    = $wpdb->get_var("SELECT FOUND_ROWS()");
        return $this->events;
    }

    public function get_upcomming_user_events_for_dashboard( $user_id = '', $limit = false) {
        global $wpdb;
        $current_date = current_time( 'Y-m-d' );
        $apollo_meta_table = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};
        $apollo_event_org_table = $wpdb->{Apollo_Tables::_APL_EVENT_ORG};

        $wpdb->escape_by_ref($user_id);

        if ( ! $user_id ) $user_id = get_current_user_id ();

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.ID, p.post_title, p.post_content, p.post_name, p.post_type, p.post_excerpt, p.post_author, p.post_status FROM {$wpdb->posts} p
        ";

        $sql .= " INNER JOIN {$apollo_meta_table} tblSortMeta ON tblSortMeta.apollo_event_id = p.ID AND tblSortMeta.meta_key = '".Apollo_DB_Schema::_APOLLO_EVENT_START_DATE."' ";

        $where_status = " p.post_status = 'publish'";
        if(is_user_logged_in()) {
            $where_status .= " OR p.post_status = 'pending'";
        }

        $sql .=   " WHERE ( $where_status ) AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."'";

        $sql .= "
            AND
            (
                p.ID IN (SELECT mt_start_d.apollo_event_id FROM $apollo_meta_table mt_start_d
                WHERE mt_start_d.meta_key = '_apollo_event_start_date'
                AND CAST( mt_start_d.meta_value AS DATE ) >= '$current_date' )
                OR
                p.ID IN (SELECT mt_end_d.apollo_event_id FROM $apollo_meta_table mt_end_d
                WHERE mt_end_d.meta_key = '_apollo_event_end_date'
                AND CAST( mt_end_d.meta_value AS DATE ) >= '$current_date' )
            )
        ";

        /*  Allow searching event name in event listing */
        if ( isset($_GET['event-name']) && !empty($_GET['event-name']) ) {
            $wpdb->escape_by_ref($_GET['event-name']);
            $keyWord = $_GET['event-name'];
            $sql .= " AND p.post_title like '%$keyWord%' ";
        }

        /* extra sql to load events are belong to orgs of agency which is associating with current userID */

        $orgIDs = Apollo_User::getUserItemIDs(Apollo_DB_Schema::_ORGANIZATION_PT, $user_id);
        if (empty($orgIDs)) {
            $orgIDs = array(-1);
        }

        $sql .= "
            AND
            (
                p.ID IN (SELECT event_org.post_id FROM $apollo_event_org_table event_org WHERE event_org.org_id IN (".implode(',',$orgIDs)."))
                OR
                p.post_author = '$user_id'
            )
        ";

        $sql = Apollo_App::hidePrivateEvent($sql,'p');
        $sql .= " GROUP BY p.ID ";

        $sql .= ' ORDER BY tblSortMeta.meta_value ASC ';

        $sql .= " $limit ";
        if (isset($_GET['db'])) {echo $sql;}

        $this->events = $wpdb->get_results( $sql );
        $this->totalPost    = $wpdb->get_var("SELECT FOUND_ROWS()");
        return $this->events;
    }

    /**
     * @Ticket #15756 Get event draft
     * @param string $user_id
     * @param bool $limit
     * @return array|null|object
     */
    public function get_draft_user_events_for_dashboard( $user_id = '', $limit = false) {
        global $wpdb;
        $apollo_event_org_table = $wpdb->{Apollo_Tables::_APL_EVENT_ORG};

        $wpdb->escape_by_ref($user_id);

        if ( ! $user_id ) $user_id = get_current_user_id ();

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.ID, p.post_title, p.post_content, p.post_name, p.post_type, p.post_excerpt, p.post_author, p.post_status 
            FROM {$wpdb->posts} p
        ";

        $where_status = " p.post_status = 'draft'";

        $sql .=   " WHERE ( $where_status ) AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."'";

        /*  Allow searching event name in event listing */
        if ( isset($_GET['event-name']) && !empty($_GET['event-name']) ) {
            $wpdb->escape_by_ref($_GET['event-name']);
            $keyWord = $_GET['event-name'];
            $sql .= " AND p.post_title like '%$keyWord%' ";
        }

        $orgIDs = Apollo_User::getUserItemIDs(Apollo_DB_Schema::_ORGANIZATION_PT, $user_id);
        if (empty($orgIDs)) {
            $orgIDs = array(-1);
        }

        $sql .= "
            AND
            (
                p.ID IN (SELECT event_org.post_id FROM $apollo_event_org_table event_org WHERE event_org.org_id IN (".implode(',',$orgIDs)."))
                OR
                p.post_author = '$user_id'
            )
        ";

        $sql = Apollo_App::hidePrivateEvent($sql,'p');
        $sql .= " GROUP BY p.ID ";

        $sql .= ' ORDER BY p.post_title ASC ';

        $sql .= " $limit ";
        if (isset($_GET['db'])) {echo $sql;}

        $this->events = $wpdb->get_results( $sql );
        $this->totalPost    = $wpdb->get_var("SELECT FOUND_ROWS()");
        return $this->events;
    }

    public function get_artist_upcomming_event( $user_id, $all_eids = array() ) {

        $eids = @unserialize( get_user_meta( $user_id, Apollo_DB_Schema::_APL_USER_ARTIST_EVENTS, TRUE ) );

        if ( ! $eids ) return false;

        // Get all upcomming events
        if ( ! $all_eids ) {
            $up_user_events = $this->get_upcomming_user_events( $user_id );
            if ( $up_user_events ) {
                foreach ( $up_user_events as $e ) {
                    $all_eids[] = $e->ID;
                }
            }
        }

        // Remove all the exprise events in the selected list
        $eids = array_values( array_intersect( $all_eids, $eids ) );

        $num_view_more = ($num = of_get_option( Apollo_DB_Schema::_NUM_VIEW_MORE )) ? $num : Apollo_Display_Config::PAGESIZE_UPCOM;

        $pagesize = $this->page == 1 ? Apollo_Display_Config::PAGESIZE_UPCOM : $num_view_more;

        if ( $this->page == 1 ) {
            $_offset = 0;
        } else if ( $this->page == 2 ) {
            $_offset = Apollo_Display_Config::PAGESIZE_UPCOM;
        } else {
            $_offset = $num_view_more == 1 ? $this->page : ( $this->page - 1 ) * $num_view_more ;
        }

        $this->events = $eids ? array_slice( $eids ,  $_offset, $pagesize ) : false;
        $this->totalPost = count( $eids );

        return array_slice( $eids ,  $_offset = ($this->page-1) * $this->pageSize, $this->pageSize );
    }

    public function get_artist_past_event( $user_id, $all_eids = array() ) {

        $eids = @unserialize( get_user_meta( $user_id, Apollo_DB_Schema::_APL_USER_ARTIST_EVENTS, TRUE ) );

        if ( ! $eids ) return false;

        // Get all upcomming events
        if ( ! $all_eids ) {
            $up_user_events = $this->get_past_user_events_artist( $user_id,$eids );

            if ( $up_user_events ) {
                foreach ( $up_user_events as $e ) {
                    $all_eids[] = $e->ID;
                }
            }
        }

        // Remove all the exprise events in the selected list
        $eids = array_values( array_intersect( $all_eids, $eids ) );

        $num_view_more = ($num = of_get_option( Apollo_DB_Schema::_NUM_VIEW_MORE )) ? $num : Apollo_Display_Config::PAGESIZE_UPCOM;

        $pagesize = $this->page == 1 ? Apollo_Display_Config::PAGESIZE_UPCOM : $num_view_more;

        if ( $this->page == 1 ) {
            $_offset = 0;
        } else if ( $this->page == 2 ) {
            $_offset = Apollo_Display_Config::PAGESIZE_UPCOM;
        } else {
            $_offset = $num_view_more == 1 ? $this->page : ( $this->page - 1 ) * $num_view_more ;
        }

        $this->events = $eids ? array_slice( $eids ,  $_offset, $pagesize ) : false;
        $this->totalPost = count( $eids );

        return array_slice( $eids ,  $_offset = ($this->page-1) * $this->pageSize, $this->pageSize );
    }

    public function get_past_user_events_artist( $user_id = '',$eids = '' ) {
        global $wpdb;

        if ( $limit ) {
            $_offset = ($this->page-1) * $this->pageSize;

            $limit = "LIMIT $_offset, $this->pageSize";
        }

        if ( ! $user_id ) $user_id = get_current_user_id ();

        $wpdb->escape_by_ref($user_id);

        $current_date = current_time( 'Y-m-d' );
        $apollo_meta_table = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};

        $arr_order = Apollo_Sort_Manager::getSortOneFieldForPostType('event');

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.* FROM {$wpdb->posts} p
            INNER JOIN {$apollo_meta_table} mt_start_d ON p.ID = mt_start_d.apollo_event_id AND mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
                INNER JOIN {$apollo_meta_table} mt_end_d ON p.ID   = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
        ";

        if(
            $arr_order['type_sort'] === 'apollo_meta'
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE )
        { // sort by meta key
            $sql .= " INNER JOIN {$apollo_meta_table} tblSortMeta ON tblSortMeta.apollo_event_id = p.ID AND tblSortMeta.meta_key = '{$arr_order["metakey_name"]}'";
        }

        $where_status = "p.post_status = 'publish'";

        $sql .=   " WHERE ( $where_status ) AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."' AND (
                        ( mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
                        AND CAST( mt_end_d.meta_value AS DATE ) < '{$current_date}' )
                    ) ";
        $sql = Apollo_App::hidePrivateEvent($sql,'p');
        $sql .= "AND p.ID in(".implode(',',$eids).")";
        $sql .= " GROUP BY p.ID ";

        if ( ! $orderby ) {
            if($arr_order['type_sort'] === 'apollo_meta') {
                if(
                    $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
                    && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE
                ) {
                    $sql .= " ORDER BY tblSortMeta.meta_value {$arr_order['order']} ";
                }
                else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_START_DATE) {
                    $sql .= " ORDER BY mt_start_d.meta_value {$arr_order['order']} ";
                }
                else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_END_DATE) {
                    $sql .= " ORDER BY mt_end_d.meta_value {$arr_order['order']} ";
                }
            }
            else if($arr_order['type_sort'] === 'post') {
                $sql .= " ORDER BY p.{$arr_order['order_by']} {$arr_order['order']} ";
            }
        } else {
            $sql .= $orderby;
        }

        $sql .= " $limit ";

        $this->events = $wpdb->get_results( $sql );
        $this->totalPost    = $wpdb->get_var("SELECT FOUND_ROWS()");
        return $this->events;
    }


    public function get_past_user_events( $user_id = '', $limit = false, $orderby = '' ) {

        global $wpdb;

        if ( $limit ) {
            $_offset = ($this->page-1) * $this->pageSize;

            $limit = "LIMIT $_offset, $this->pageSize";
        }

        if ( ! $user_id ) $user_id = get_current_user_id ();

        $wpdb->escape_by_ref($user_id);

        $current_date = current_time( 'Y-m-d' );
        $apollo_meta_table = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};

        $arr_order = Apollo_Sort_Manager::getSortOneFieldForPostType('event');

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.* FROM {$wpdb->posts} p
            INNER JOIN {$apollo_meta_table} mt_start_d ON p.ID = mt_start_d.apollo_event_id AND mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
                INNER JOIN {$apollo_meta_table} mt_end_d ON p.ID   = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
        ";

        if(
            $arr_order['type_sort'] === 'apollo_meta'
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE )
        { // sort by meta key
            $sql .= " INNER JOIN {$apollo_meta_table} tblSortMeta ON tblSortMeta.apollo_event_id = p.ID AND tblSortMeta.meta_key = '{$arr_order["metakey_name"]}'";
        }

        $where_status = "p.post_status = 'publish'";
        if(is_user_logged_in()) {
            $where_status .= "or p.post_status = 'pending'";
        }

        $sql .=   " WHERE ( $where_status ) AND p.post_author = ".  $user_id." AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."' AND (
                        ( mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
                        AND CAST( mt_end_d.meta_value AS DATE ) < '{$current_date}' )
                    ) ";
        $sql = Apollo_App::hidePrivateEvent($sql,'p');
        $sql .= " GROUP BY p.ID ";

        if ( ! $orderby ) {
            if($arr_order['type_sort'] === 'apollo_meta') {
                if(
                    $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
                    && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE
                ) {
                    $sql .= " ORDER BY tblSortMeta.meta_value {$arr_order['order']} ";
                }
                else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_START_DATE) {
                    $sql .= " ORDER BY mt_start_d.meta_value {$arr_order['order']} ";
                }
                else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_END_DATE) {
                    $sql .= " ORDER BY mt_end_d.meta_value {$arr_order['order']} ";
                }
            }
            else if($arr_order['type_sort'] === 'post') {
                $sql .= " ORDER BY p.{$arr_order['order_by']} {$arr_order['order']} ";
            }
        } else {
            $sql .= $orderby;
        }

        $sql .= " $limit ";

        $this->events = $wpdb->get_results( $sql );
        $this->totalPost    = $wpdb->get_var("SELECT FOUND_ROWS()");
        return $this->events;
    }


    public function get_past_user_events_for_dashboard( $user_id = '', $limit = false) {
        global $wpdb;
        if ( !$limit ) {
            $_offset = ($this->page-1) * $this->pageSize;
            $limit = "LIMIT $_offset, $this->pageSize";
        }

        if ( ! $user_id ) $user_id = get_current_user_id ();

        $wpdb->escape_by_ref($user_id);

        $current_date = current_time( 'Y-m-d' );
        $apollo_meta_table = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};
        $apollo_event_org_table = $wpdb->{Apollo_Tables::_APL_EVENT_ORG};

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.* FROM {$wpdb->posts} p
        ";

        $sql .= " INNER JOIN {$apollo_meta_table} tblSortMeta ON tblSortMeta.apollo_event_id = p.ID AND tblSortMeta.meta_key = '".Apollo_DB_Schema::_APOLLO_EVENT_START_DATE."' ";

        $where_status = "p.post_status = 'publish'";
        if(is_user_logged_in()) {
            $where_status .= " OR p.post_status = 'pending'";
        }

        $sql .=   " WHERE ( $where_status ) AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."'";

        $sql .= "
            AND
            (
                p.ID IN (SELECT mt_end_d.apollo_event_id FROM $apollo_meta_table mt_end_d
                WHERE mt_end_d.meta_key = '_apollo_event_end_date'
                AND CAST( mt_end_d.meta_value AS DATE ) < '$current_date' )
            )
        ";

        /*  Allow searching event name in event listing */
        if ( isset($_GET['event-name']) && !empty($_GET['event-name']) ) {
            $wpdb->escape_by_ref($_GET['event-name']);
            $keyWord = $_GET['event-name'];
            $sql .= " AND p.post_title like '%$keyWord%' ";
        }

        /* extra sql to load events are belong to orgs of agency which is associating with current userID */

        $orgIDs = Apollo_User::getUserItemIDs(Apollo_DB_Schema::_ORGANIZATION_PT, $user_id);
        if (empty($orgIDs)) {
            $orgIDs = array(-1);
        }

        $sql .= "
            AND
            (
                p.ID IN (SELECT event_org.post_id FROM $apollo_event_org_table event_org WHERE event_org.org_id IN (".implode(',',$orgIDs)."))
                OR
                p.post_author = '$user_id'
            )
        ";

        $sql .= " GROUP BY p.ID ";

        $sql .= ' ORDER BY tblSortMeta.meta_value DESC ';

        $sql .= " $limit ";

        if (isset($_GET['db'])) {echo $sql;}

        $this->events = $wpdb->get_results( $sql );
        $this->totalPost    = $wpdb->get_var("SELECT FOUND_ROWS()");
        return $this->events;
    }

    /**
     *  @ticket #18932: Display 'private' events on FE user dashboard
     * @param string $user_id
     * @param bool $limit
     * @return array|null|object
     */
    public function get_private_user_events_for_dashboard($user_id = '', $limit = false) {
        global $wpdb;

        $wpdb->escape_by_ref($user_id);

        $user_id = !empty($user_id) ? $user_id : get_current_user_id ();

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.ID, p.post_title, p.post_content, p.post_name, p.post_type, p.post_excerpt, p.post_author, p.post_status 
            FROM {$wpdb->posts} p
        ";

        $wherePrivate = AplEventFunction::getQueryEventPrivate('p');

        $sql .=   " WHERE ( $wherePrivate ) AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."'";

        /*  Allow searching event name in event listing */
        if ( isset($_GET['event-name']) && !empty($_GET['event-name']) ) {
            $wpdb->escape_by_ref($_GET['event-name']);
            $keyWord = $_GET['event-name'];
            $sql .= " AND p.post_title like '%$keyWord%' ";
        }

        $orgIDs = Apollo_User::getUserItemIDs(Apollo_DB_Schema::_ORGANIZATION_PT, $user_id);
        $orgIDs = !empty($orgIDs) ? $orgIDs : array();
        $whereInOrgIds = '';
        if(!empty($orgIDs)){
            $orgIDs = implode(',',$orgIDs);
            $apollo_event_org_table = $wpdb->{Apollo_Tables::_APL_EVENT_ORG};
            $whereInOrgIds = " OR p.ID IN (SELECT event_org.post_id FROM $apollo_event_org_table event_org WHERE event_org.org_id IN ($orgIDs))";

        }

        $sql .= " AND (p.post_author = '$user_id' $whereInOrgIds)";

        $sql .= " GROUP BY p.ID ";
        $sql .= ' ORDER BY p.post_title ASC ';
        $sql .= $limit ;

        $this->events = $wpdb->get_results( $sql );
        $this->totalPost    = $wpdb->get_var("SELECT FOUND_ROWS()");
        return $this->events;
    }

    /**
     * @ticket #19007: add tab unconfirmed
     * @param string $user_id
     * @param bool $limit
     * @return array|null|object
     */
    public function get_unconfirmed_user_events_for_dashboard($user_id = '', $limit = false) {
        global $wpdb;

        $wpdb->escape_by_ref($user_id);

        $user_id = !empty($user_id) ? $user_id : get_current_user_id ();

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.ID, p.post_title, p.post_content, p.post_name, p.post_type, p.post_excerpt, p.post_author, p.post_status 
            FROM {$wpdb->posts} p
        ";

        $sql .=   " WHERE post_status = 'unconfirmed' AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."'";

        /*  Allow searching event name in event listing */
        if ( isset($_GET['event-name']) && !empty($_GET['event-name']) ) {
            $wpdb->escape_by_ref($_GET['event-name']);
            $keyWord = $_GET['event-name'];
            $sql .= " AND p.post_title like '%$keyWord%' ";
        }

        $orgIDs = Apollo_User::getUserItemIDs(Apollo_DB_Schema::_ORGANIZATION_PT, $user_id);
        $orgIDs = !empty($orgIDs) ? $orgIDs : array();
        $whereInOrgIds = '';
        if(!empty($orgIDs)){
            $orgIDs = implode(',',$orgIDs);
            $apollo_event_org_table = $wpdb->{Apollo_Tables::_APL_EVENT_ORG};
            $whereInOrgIds = " OR p.ID IN (SELECT event_org.post_id FROM $apollo_event_org_table event_org WHERE event_org.org_id IN ($orgIDs))";

        }

        $sql .= " AND (p.post_author = '$user_id' $whereInOrgIds)";

        $sql .= " GROUP BY p.ID ";
        $sql .= ' ORDER BY p.post_title ASC ';
        $sql .= $limit ;

        $this->events = $wpdb->get_results( $sql );
        $this->totalPost    = $wpdb->get_var("SELECT FOUND_ROWS()");
        return $this->events;
    }

    public function get_org_up_comming_events($org_id, $query = '>=',$and = 'OR',$offset = true){;
        $this->pageSize = Apollo_Display_Config::PAGESIZE_UPCOM;
        if ( ! Apollo_App::is_avaiable_module( 'organization' ) ) {
            return false;
        }
        global $wpdb, $post;
        $apollo_meta_table   = $wpdb->apollo_eventmeta;
        $_start_date_field  = Apollo_DB_Schema::_APOLLO_EVENT_START_DATE;
        $_end_date_field    = Apollo_DB_Schema::_APOLLO_EVENT_END_DATE;
        $_current_date      = current_time( 'Y-m-d' );
        $_offset = $offset ? ($this->page-1) * $this->pageSize. ', ' : '';

        $arr_order = Apollo_Sort_Manager::getSortOneFieldForPostType('event');
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.* FROM {$wpdb->posts} p
            INNER JOIN {$apollo_meta_table} mt_start_d ON p.ID = mt_start_d.apollo_event_id AND mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
            INNER JOIN {$apollo_meta_table} mt_end_d ON p.ID   = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
            INNER JOIN {$wpdb->{Apollo_Tables::_APL_EVENT_ORG}} e_org ON e_org.post_id = p.ID AND e_org." . Apollo_DB_Schema::_APL_E_ORG_ID . " = '$org_id'
        ";
        if(
            $arr_order['type_sort'] === 'apollo_meta'
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE )
        { // sort by meta key
            $sql .= " INNER JOIN {$apollo_meta_table} tblSortMeta ON tblSortMeta.apollo_event_id = p.ID AND tblSortMeta.meta_key = '{$arr_order["metakey_name"]}'";
        }
        $sql .=   " WHERE 1=1 AND p.post_status = 'publish' AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."' AND (
                        ( mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
                        AND CAST( mt_start_d.meta_value AS DATE )  ".$query." '{$_current_date}' )
                    ".$and."
                        ( mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
                        AND CAST( mt_end_d.meta_value AS DATE ) ".$query." '{$_current_date}' )
                    ) ";
        $sql = Apollo_App::hidePrivateEvent($sql,'p');
        $sql .= " GROUP BY p.ID ";
        if($arr_order['type_sort'] === 'apollo_meta') {
            if(
                $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
                && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE
            ) {
                $sql .= " ORDER BY tblSortMeta.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_START_DATE) {
                $sql .= " ORDER BY mt_start_d.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_END_DATE) {
                $sql .= " ORDER BY mt_end_d.meta_value {$arr_order['order']} ";
            }
        }
        else if($arr_order['type_sort'] === 'post') {
            $sql .= " ORDER BY p.{$arr_order['order_by']} {$arr_order['order']} ";
        }
        $sql .= " LIMIT $_offset $this->pageSize ";
        $this->events       = $wpdb->get_results($sql);
        $this->totalPost    = $wpdb->get_var("SELECT FOUND_ROWS()");
        $this->_template = APOLLO_PARENT_DIR.'/templates/events/_org-events.php';
        return $this->render_html($this->_template);
    }

    /**
     * Change the order of the PAST EVENTS so that the most recently expired event is first
     * From version 2.1.5
     *
     * @return string
     */
    public function get_org_past_events($org_id, $offset = true){;
        $this->pageSize = Apollo_Display_Config::PAGESIZE_UPCOM;
        if ( ! Apollo_App::is_avaiable_module( 'organization' ) ) {
            return false;
        }
        global $wpdb, $post;
        $apollo_meta_table   = $wpdb->apollo_eventmeta;
        $_start_date_field  = Apollo_DB_Schema::_APOLLO_EVENT_START_DATE;
        $_end_date_field    = Apollo_DB_Schema::_APOLLO_EVENT_END_DATE;
        $_current_date      = current_time( 'Y-m-d' );
        $_offset = $offset ? ($this->page-1) * $this->pageSize. ', ' : '';

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.* FROM {$wpdb->posts} p
            INNER JOIN {$apollo_meta_table} mt_end_d ON p.ID   = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
            INNER JOIN {$wpdb->{Apollo_Tables::_APL_EVENT_ORG}} e_org ON e_org.post_id = p.ID AND e_org." . Apollo_DB_Schema::_APL_E_ORG_ID . " = '$org_id'
        ";

        $sql .=   " WHERE 1=1 AND p.post_status = 'publish' AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."' AND
                        ( mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
                        AND CAST( mt_end_d.meta_value AS DATE ) < '{$_current_date}' )
                   ";
        $sql = Apollo_App::hidePrivateEvent($sql,'p');
        $sql .= " GROUP BY p.ID ";

        $sql .= " ORDER BY mt_end_d.meta_value DESC ";

        $sql .= " LIMIT $_offset $this->pageSize ";
        $this->events       = $wpdb->get_results($sql);
        $this->totalPost    = $wpdb->get_var("SELECT FOUND_ROWS()");
        $this->_template = APOLLO_PARENT_DIR.'/templates/events/_org-events.php';
        return $this->render_html($this->_template);
    }

    public function get_business_nearby_event($org_id,  $query = '>=',$and = 'OR', $offset = true){

        $this->pageSize = Apollo_Display_Config::PAGESIZE_UPCOM;
        if ( ! Apollo_App::is_avaiable_module( 'organization' ) ) {
            return false;
        }
        global $wpdb, $post;
        $event_meta_table   = $wpdb->apollo_eventmeta;
        $venue_meta_table   = $wpdb->apollo_venuemeta;
        $metaAddr = get_apollo_meta($org_id, Apollo_DB_Schema::_APL_ORG_ADDRESS, true);
        if(!is_array($metaAddr)){
            $metaAddr = unserialize($metaAddr);
        }
        $_current_date      = current_time( 'Y-m-d' );
        $zip =0;
        if (is_array($metaAddr)) {
            $zip = $metaAddr[Apollo_DB_Schema::_ORG_ZIP];
        }

        $_offset = $offset ? ($this->page-1) * $this->pageSize. ', ' : '';
        $arr_order = Apollo_Sort_Manager::getSortOneFieldForPostType('event');
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.* FROM {$wpdb->posts} p
            INNER JOIN {$event_meta_table} mt_start_d ON p.ID = mt_start_d.apollo_event_id AND mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
            INNER JOIN {$event_meta_table} mt_end_d ON p.ID   = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
            INNER JOIN {$event_meta_table} e ON e.apollo_event_id = p.ID AND e.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_VENUE ."'
                        and e.meta_value in ( select v.apollo_venue_id from {$venue_meta_table} v  where v.meta_key = '". Apollo_DB_Schema::_VENUE_ZIP ."' and v.meta_value = {$zip} )
        ";
        if(
            $arr_order['type_sort'] === 'apollo_meta'
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE )
        { // sort by meta key
            $sql .= " INNER JOIN {$event_meta_table} tblSortMeta ON tblSortMeta.apollo_event_id = p.ID AND tblSortMeta.meta_key = '{$arr_order["metakey_name"]}'";
        }
        $sql .=   " WHERE 1=1 AND p.post_status = 'publish' AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."' AND (
                        ( mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
                        AND CAST( mt_start_d.meta_value AS DATE )  ".$query." '{$_current_date}' )
                    ".$and."
                        ( mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
                        AND CAST( mt_end_d.meta_value AS DATE ) ".$query." '{$_current_date}' )
                    ) ";
        $sql .= " GROUP BY p.ID ";
        if($arr_order['type_sort'] === 'apollo_meta') {
            if(
                $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
                && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE
            ) {
                $sql .= " ORDER BY tblSortMeta.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_START_DATE) {
                $sql .= " ORDER BY mt_start_d.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_END_DATE) {
                $sql .= " ORDER BY mt_end_d.meta_value {$arr_order['order']} ";
            }
        }
        else if($arr_order['type_sort'] === 'post') {
            $sql .= " ORDER BY p.{$arr_order['order_by']} {$arr_order['order']} ";
        }
        $sql .= " LIMIT $_offset $this->pageSize ";
        $this->events       = $wpdb->get_results($sql);
        $this->totalPost    = $wpdb->get_var("SELECT FOUND_ROWS()");
        $this->_template = APOLLO_PARENT_DIR.'/templates/events/_org-events.php';
        return $this->render_html($this->_template);

    }

    public function get_org_past_event($org_id, $offset = true){
        return $this->get_org_past_events($org_id, $offset);
    }

    public function get_venue_up_comming_events($venue_id, $query = '>=',$and = 'OR',$offset = true){
        global $wpdb, $post;
        $this->pageSize = Apollo_Display_Config::PAGESIZE_UPCOM;
        if ( ! Apollo_App::is_avaiable_module( 'venue' ) ) {
            return false;
        }

        $filterEventByVenueStr  = ' p.ID IN (
                    SELECT em.apollo_event_id
                    FROM '.$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}.' em
                    WHERE
                         em.meta_value = '.$venue_id.'
                        AND em.meta_key = "'.Apollo_DB_Schema::_APOLLO_EVENT_VENUE.'"
                )';



        $apollo_meta_table   = $wpdb->apollo_eventmeta;
        $_start_date_field  = Apollo_DB_Schema::_APOLLO_EVENT_START_DATE;
        $_end_date_field    = Apollo_DB_Schema::_APOLLO_EVENT_END_DATE;
        $_current_date      = current_time( 'Y-m-d' );
        $_offset = $offset ? ($this->page-1) * $this->pageSize. ', ' : '';

        $arr_order = Apollo_Sort_Manager::getSortOneFieldForPostType('event');
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.* FROM {$wpdb->posts} p
            INNER JOIN {$apollo_meta_table} mt_start_d ON p.ID = mt_start_d.apollo_event_id AND mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
            INNER JOIN {$apollo_meta_table} mt_end_d ON p.ID   = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
        ";
        if(
            $arr_order['type_sort'] === 'apollo_meta'
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE )
        { // sort by meta key
            $sql .= " INNER JOIN {$apollo_meta_table} tblSortMeta ON tblSortMeta.apollo_event_id = p.ID AND tblSortMeta.meta_key = '{$arr_order["metakey_name"]}'";
        }
        $sql .=   " WHERE 1=1 AND p.post_status = 'publish' AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."' AND (
                        ( mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
                        AND CAST( mt_start_d.meta_value AS DATE )  ".$query." '{$_current_date}' )
                    ".$and."
                        ( mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
                        AND CAST( mt_end_d.meta_value AS DATE ) ".$query." '{$_current_date}' )
                    )  AND ".$filterEventByVenueStr;
        $sql = Apollo_App::hidePrivateEvent($sql,'p');
        $sql .= " GROUP BY p.ID ";
        if($arr_order['type_sort'] === 'apollo_meta') {
            if(
                $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
                && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE
            ) {
                $sql .= " ORDER BY tblSortMeta.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_START_DATE) {
                $sql .= " ORDER BY mt_start_d.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_END_DATE) {
                $sql .= " ORDER BY mt_end_d.meta_value {$arr_order['order']} ";
            }
        }
        else if($arr_order['type_sort'] === 'post') {
            $sql .= " ORDER BY p.{$arr_order['order_by']} {$arr_order['order']} ";
        }
        $sql .= " LIMIT $_offset $this->pageSize ";

        $this->events       = $wpdb->get_results($sql);
        $this->totalPost    = $wpdb->get_var("SELECT FOUND_ROWS()");
        $this->_template = APOLLO_PARENT_DIR.'/templates/events/_org-events.php';
        return $this->render_html($this->_template);
    }

    /**
     * Past events of venue
     * From version 2.1.5
     *
     * @return string
     */
    public function get_venue_past_events($venue_id, $offset = true){
        global $wpdb;
        $this->pageSize = Apollo_Display_Config::PAGESIZE_UPCOM;
        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_VENUE_PT ) ) {
            return false;
        }

        $filterEventByVenueStr  = ' p.ID IN (
                    SELECT em.apollo_event_id
                    FROM '.$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}.' em
                    WHERE
                         em.meta_value = '.$venue_id.'
                        AND em.meta_key = "'.Apollo_DB_Schema::_APOLLO_EVENT_VENUE.'"
                )';


        $apollo_meta_table   = $wpdb->apollo_eventmeta;
        $_current_date      = current_time( 'Y-m-d' );
        $_offset = $offset ? ($this->page-1) * $this->pageSize. ', ' : '';

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.* FROM {$wpdb->posts} p
            INNER JOIN {$apollo_meta_table} mt_end_d ON p.ID   = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
        ";

        $sql .=   " WHERE 1=1 AND p.post_status = 'publish' AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."' AND (
                        ( mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
                        AND CAST( mt_end_d.meta_value AS DATE ) < '{$_current_date}' )
                    )  AND ".$filterEventByVenueStr;
        $sql = Apollo_App::hidePrivateEvent($sql,'p');
        $sql .= " GROUP BY p.ID ";

        $sql .= " ORDER BY mt_end_d.meta_value DESC ";

        $sql .= " LIMIT $_offset $this->pageSize ";

        $this->events       = $wpdb->get_results($sql);
        $this->totalPost    = $wpdb->get_var("SELECT FOUND_ROWS()");
        $this->_template = APOLLO_PARENT_DIR.'/templates/events/_org-events.php';
        return $this->render_html($this->_template);
    }

    public function theBackgroundTab($index) {
        $this->tabBg = isset($this->tabColorData[$index]) && $this->tabColorData[$index] ? "background:". $this->tabColorData[$index] : '';
        echo $this->tabBg;
    }

    public function getAsName($index) {

        if (!$this->asNameData || $this->asNameData == 'false') return false;

        $this->asName = isset($this->asNameData[$index]) && $this->asNameData[$index] ?  $this->asNameData[$index] : '';
        return $this->asName;
    }

    public function theBorderTab($index) {
        echo isset($this->tabColorData[$index]) && $this->tabColorData[$index] ? "border-color:". $this->tabColorData[$index] : "border-color:". of_get_option(Apollo_DB_Schema::_PRIMARY_COLOR,'');
    }

    public function setTabColor($index) {
        $this->currentTabColor = isset( $this->tabColorData[$index] ) ? $this->tabColorData[$index] : '';
    }

    public function getTabColor() {
        return $this->currentTabColor;
    }

    /* Thienld : handle optimize caching for TopTen Tab */
    /*
        1: have just action write a new visit.json file => need to re-caching
        0: skip.
    */
    public function checkRenewTopTenCache($tpl_file = ''){
        $localSiteCacheEnabled = false;
        $currentBlogID = get_current_blog_id();
        $optionCachedValue = Apollo_App::get_network_local_cache($currentBlogID);
        if(intval($optionCachedValue) === 1){
            $localSiteCacheEnabled = true;
        }
        if($localSiteCacheEnabled === false){
            return true; // in case not using local cache -> force renew topten cache.
        }
        $flagDetection = get_option(Apollo_DB_Schema::_TOPTEN_CACHE_FLAG_DETECTION,1);
        $expiredCachingFile = false;
        if(!empty($tpl_file)){
            require_once APOLLO_INCLUDES_DIR. '/admin/tools/cache/Inc/File.php';
            if(class_exists('APLC_Inc_File')){
                $cachedClassIns = new APLC_Inc_File();
            } else {
                $cachedClassIns = null;
            }
            if ( $cachedClassIns !== null ) {
                $cachedData = $cachedClassIns->getCache(basename($tpl_file));
                if($cachedData === false) {
                    $expiredCachingFile = true;
                }
            }
        }
        return intval($flagDetection) === 1 || $expiredCachingFile === true;
    }

    public static function emptyTopTenFileCachedHandler(){
        try{
            $uploadDir      = wp_upload_dir();
            $cacheDir       = isset($uploadDir['cache']) ? $uploadDir['cache'] : '';
            if(empty($cacheDir)) return;
            $cacheDir = rtrim($cacheDir,'/');
            $ttCachedFile_1 = $cacheDir . '/_default-list-items.html';
            @unlink($ttCachedFile_1);
            $ttCachedFile_2 = $cacheDir . '/_panel-list-items-ver1.html';
            @unlink($ttCachedFile_2);
            $ttCachedFile_3 = $cacheDir . '/_panel-list-items.html';
            @unlink($ttCachedFile_3);
        }catch (Exception $ex){
            aplDebugFile($ex->getMessage(),'emptyTopTenFileCachedHandler');
        }
    }

    /**
     * @ticket #17733: CF] 20180928 - [Homepage][Feature events] Custom link for the 'Spotlight' category page - Item 5
     * @param $index
     * @param $link
     * @return mixed
     */
    public function getCategoryLink($index, $link){

        if (isset($this->useCustomLink[$index]) && $this->useCustomLink[$index]){

            return !empty($this->customLink[$index]) ? $this->customLink[$index] : $link;
        }
        return $link;
    }

}

