<?php
//get event time from post
$arr_time = $_a_event->get_periods_time( " ORDER BY date_event ASC " );
$a_desc_excerpt = $_a_event->get_excerpt(20);
/* @ticket #16609:  Merge the 'Add to Outlook' and 'Add to Google Calendar' icons into one popup window. */
$separationGroup = of_get_option('separation_group','two_icon');
$classSeparationGroup = $separationGroup === "two_icon" ? "" : 'one-icon';
if(is_array($arr_time) && count($arr_time) > 0):
    $directSubmit = 0;
    if(count($arr_time) == 1)
        $directSubmit = 1;
    ?>
    <div id="_popup_choose_event" data-active="active" class="<?php echo $classSeparationGroup?>">
        <div class="modal-content">
            <div class="copy" id="_popup_choose_event_full">
                <div class="a-block-ct">
                    <form action="/export_calendar/event/" method="GET" accept-charset="utf-8" id="export_ics_frm">
                        <input type="hidden" name="action" value="TEMPLATE">
                        <input type="hidden" name="text"     id="g_calendar_event_title" value="<?php echo $_a_event->get_title(); ?>">
                        <input type="hidden" name="location" id="g_calendar_event_location" value="<?php echo isset($str_address_all) ? $str_address_all : ''; ?>">
                        <input type="hidden" name="details"  id="g_calendar_event_desc" value="<?php echo wp_strip_all_tags($_a_event->get_full_content()); ?>">
                        <input type="hidden" name="trp" value="false">
                        <input type="hidden" name="ctz" value="<?php echo Apollo_Ical_calendar::getTimeZone(); ?>">
                        <input type="hidden" name="sprop" value="<?php echo get_the_permalink($_a_event->id) ?>">
                        <div class="el-blk">
                            <div class="select-bkl">
                                <select name="dates" class="s" >
                                    <?php
                                    foreach($arr_time as $item){

                                        $timeFrom  = isset($item->time_from)?$item->time_from:'';
                                        $timeTo = isset($item->time_to)?$item->time_to:'';
                                        $eventDate = isset($item->date_event)?$item->date_event:'';
                                        $eventId = isset($item->event_id)?$item->event_id:'';

                                        $value = array(
                                            'time_from' => $timeFrom,
                                            'time_to' => $timeTo,
                                            'date_event' => $eventDate,
                                            'event_id' => $eventId,
                                        );

                                        $timeFromStr =  $eventDate.' '.$timeFrom;

                                        $timeToStr = $eventDate.' '.$timeFrom;
                                        if(!empty($timeTo))
                                            $timeToStr = $eventDate.' '.$timeTo;


                                        $value = urlencode(json_encode($value));
                                        $end_string = $timeFromStr != $timeToStr && $timeTo !== '' ?  " - ". date_i18n( 'h:i a ', strtotime($timeToStr) ) : '';
                                        
                                        $D = date_i18n(' (D)', strtotime($timeFromStr) );

                                        $render = Apollo_App::apl_date( 'M, d, Y ', strtotime($timeFromStr) ).__( 'at', 'apollo' ).date_i18n( ' h:i a', strtotime($timeFromStr) ).$end_string.$D;
                                        $startTime = Apollo_Ical_calendar::dateToCal($timeFromStr);
                                        $endTime = Apollo_Ical_calendar::dateToCal($timeToStr);
                                        ?>
                                        <option value="<?php echo $startTime; ?>/<?php echo $endTime; ?>"><?php echo $render; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                            </div>
                        </div>
                        <input type="hidden" name="event_id" value="<?php echo $eventId; ?>">
                        <?php if ($separationGroup === "two_icon") : { ?>
                            <div class="b-btn __inline_block_fix_space">
                                <a data-type="<?php echo $directSubmit; ?>"  type="submit" id="calendar_action_button" class="btn btn-b" ><?php _e('Insert to google calendar') ?></a>
                            </div>
                        <?php } else :{ ?>
                            <div class="b-btn __inline_block_fix_space">
                                <a type="submit" id="google-calendar-action-button" class="btn btn-b" ><?php _e('POST TO GOOGLE CALENDAR') ?></a>
                            </div>
                            <div class="b-btn __inline_block_fix_space">
                                <a type="submit" id="outlook-action-button" class="btn btn-b" ><?php _e('POST TO ICAL/OUTLOOK CALENDAR') ?></a>
                            </div>
                        <?php }endif ?>
                    </form>
                </div>

            </div>
            <div class="fa fa-close _close" data-owner="#_popup_choose_event" ></div>
        </div>
        <div class="overlay"  data-owner="#_popup_choose_event"></div>
    </div>
    <?php
endif;