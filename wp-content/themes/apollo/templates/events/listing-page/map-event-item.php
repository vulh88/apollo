<?php
if ($event) :
    $e_times = $event->render_periods_time($event->get_periods_time(" LIMIT " . Apollo_Display_Config::MAX_DATETIME . " "), '<div class="item">', '</div>');
    $discountURL = $event->getMetaInMainData( Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL );
    $even_dataIcons = $event->getEventDataIcons($discountConfig['display_all_icon'], $icon_fields)

    ?>
    <li class="apl-search-event-list">
        <div class="search-img">
            <a href="<?php echo $event->get_permalink(); ?>">
                <?php echo $event->get_image('thumbnail', array(),
                    array(
                        'aw' => true,
                        'ah' => true,
                    ),
                    'normal'
                ) ?>
            </a>
        </div>
        <div class="info-content <?php echo (!$enableBlockIcon && !empty($discountURL)) ? 'has-discount--event' : '' ?>">
            <div class="search-info">
                <a href="<?php echo $event->get_permalink(); ?>">
                    <span class="ev-tt"><?php echo $event->post->post_title; ?></span>
                </a>
                <p class="meta auth"><?php echo $event->renderOrgVenueHtml(); ?></p>
                <div class="apl-event-discount-icon-text">
                    <?php
                    if(!$enableBlockIcon) {
                        $event->renderIcons($icon_fields, $even_dataIcons);
                    }
                    if($discountConfig['enable-discount-description']){
                        $event->generateDiscountText($discountConfig['include_discount_url']);
                    }?>
                </div>
            </div>
            <div class="searchdate">
                <span class="sch-date"><?php $event->render_sch_date(); ?></span>
            </div>

            <?php if($enableBlockIcon) : ?>
            <div class="ico-date <?php echo !$e_times ? 'hidden' : ''; ?>"><a><i class="fa fa-clock-o fa-2x"></i></a>
                <div class="show-date">
                    <?php
                    echo $e_times;
                    if ($event->have_more_periods_time()) {
                        echo '<div class="item">...</div>   ';
                    }
                    ?>
                </div>
            </div>
            <?php endif ?>

            <div class="event-map-desc">
                <p><?php echo $event->post->post_excerpt; ?></p>
            </div>
        </div>
    </li>
    <div class="vertical-light"></div>
<?php
    endif;
?>
