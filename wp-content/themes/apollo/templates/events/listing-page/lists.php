<?php
if ( $results = $this->get_results() ) {
    $filterTitle = false;
    $filterTitle = apply_filters('apl_event_listing_page_custom_title', $filterTitle);

    /*@ticket #17316 */
    require_once APOLLO_SRC_DIR. '/event/inc/class-apollo-event-advertise.php';
    $offset = Apollo_App::is_ajax() ? $this->getAjaxOnePageOffset() : $this->getOffset();

    $eventAdvertise = new Apollo_Event_Advertise(count($results), $offset);
    $eventIndex = $offset;

    /*@ticket #17348: 0002410: wpdev55 - Requirements part2 - [Event] Display event discount text*/
    $discountConfig = AplEventFunction::getConfigDiscountIcon();

    /*@ticket #17349 */
    $enableBlockIcon = of_get_option(Apollo_DB_Schema::_APOLLO_EVENT_ENABLE_CLOCK_ICON, true);
    $icon_fields = maybe_unserialize( get_option( Apollo_DB_Schema::_APL_EVENT_ICONS ) );

    foreach ($results as $p):
        $e = get_event($p->ID);
        $summary = $e->post->post_excerpt;
        $discountURL = $e->getMetaInMainData( Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL );
        $even_dataIcons = $e->getEventDataIcons($discountConfig['display_all_icon'], $icon_fields)

        ?>
        <li class="apl-search-event-list">
            <div class="search-img lazy-load-image-wrapper"><a
                    href="<?php echo $e->get_permalink() ?>"><?php echo $e->get_image('thumbnail', array(),
                        array(
                            'aw' => true,
                            'ah' => true,
                        ),
                        'normal'
                    ) ?></a></div>

            <div data-type="link" data-url="<?php echo $e->get_permalink() ?>" class="info-content <?php echo (!$enableBlockIcon && !empty($discountURL)) ? 'has-discount--event' : '' ?>">
                <div class="search-info"><a href="<?php echo $e->get_permalink() ?>"><span
                            class="ev-tt"><?php echo $e->get_title($filterTitle) ?></span></a>

                    <p class="meta auth"><?php echo $e->renderOrgVenueHtml() ?></p>
                    <div class="apl-event-discount-icon-text">
                        <?php
                        if(!$enableBlockIcon) {
                            $e->renderIcons($icon_fields, $even_dataIcons);
                        }
                        if($discountConfig['enable-discount-description']){
                            $e->generateDiscountText($discountConfig['include_discount_url']);
                        }?>
                    </div>
                </div>
                <div class="apl-event-date">
                    <div class="searchdate">
                    <span class="sch-date">
                        <?php $e->render_sch_date() ?>
                    </span>
                    </div>
                    <?php

                    $e_times = $e->render_periods_time($e->get_periods_time(" LIMIT " . Apollo_Display_Config::MAX_DATETIME . " "), '<div class="item">', '</div>');
                    ?>
                    <?php if($enableBlockIcon) : ?>
                        <div class="ico-date <?php echo !$e_times ? 'hidden' : '' ?>"><a><i class="fa fa-clock-o fa-2x"></i></a>

                        <div class="show-date">
                            <?php
                            echo $e_times;
                            if ($e->have_more_periods_time()) {
                                echo '<div class="item">...</div>   ';
                            }
                            ?>
                        </div>
                    </div>
                    <?php endif ?>
                    <div class="apl-event-desc">
                        <p><?php echo $summary; ?></p>
                    </div>
                </div>
            </div>
        </li>
        <div class="vertical-light"></div>

        <?php
        $eventAdvertise->renderAdvertise($eventIndex);
        $eventIndex++;
    endforeach;

    if(Apollo_App::is_ajax()){
        if (!$this->have_more_on_viewmore()) {
            $eventAdvertise->renderRemainAdvertise();
        }
    }
    else{
        if (!$this->have_more()) {
            $eventAdvertise->renderRemainAdvertise();
        }
    }
}
else {
    _e( 'No results', 'apollo' );

}