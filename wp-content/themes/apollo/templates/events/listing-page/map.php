<?php
    $enableTileView = of_get_option(OC_Common_Const::_OC_ENABLE_LISTING_PAGE_TILE_VIEW, false);
    /*@ticket #17347 */
    $discountDescription = of_get_option(Apollo_DB_Schema::_EVENT_ENABLE_DISCOUNT_DESCRIPTION, 0);
    $handleClass = "";
    if($discountDescription){
        $handleClass =   'apl-event-discount ' . $search_obj->getCurrentTemplateName();
    }
    ?>
<div class="search-bkl">
    <div class="b-share-cat art">
        <?php
        SocialFactory::social_btns( array(
                'info' => $search_obj->getShareInfo(),
                'id' => !empty($post) && is_object($post) ? $post->ID : 0,
                'data_btns' => array( 'event-print'),
                'event_print_url' => $search_obj->getPrintEventUrl()
            )
        );
        ?>
    </div>
</div>
<div class="search-bkl <?php echo $handleClass ?>">
    <?php if($discountDescription) {
        $discountImage = of_get_option(Apollo_DB_Schema::_EVENT_DISCOUNT_DESCRIPTION_IMAGE, '');
        $dicountText = of_get_option(Apollo_DB_Schema::_EVENT_DISCOUNT_DESCRIPTION_TEXT, '');
        ?>
        <div class="apl-event-discount-description">
            <?php if($discountImage !== '')  { ?>
                <div>
                    <image src="<?php echo $discountImage?>"/>
                </div>
            <?php }?>
            <?php if($dicountText !== '') { ?>
                <p><?php echo __($dicountText, 'apollo') ?></p>
            <?php } ?>
        </div>
    <?php } ?>

    <nav class="type mult-type">
        <ul>
            <?php
                $filterIconOption = of_get_option(OC_Common_Const::_OC_EVENT_LISTING_FILTER_ICON, 'only_icon');
                if ($filterIconOption == 'icon_and_text') {
                    $textFilterMap = '<span class="event-filter-text">' . __('Map', 'apollo') . '</span>';
                    $textFilterList = '<span class="event-filter-text">' . __('List', 'apollo') . '</span>';
                    $textFilterTile = '<span class="event-filter-text">' . __('Tile', 'apollo') . '</span>';
                    $iconAndTextClass = 'event-filter-icon-text';
                } else {
                    $textFilterMap = '';
                    $textFilterList = '';
                    $textFilterTile = '';
                    $iconAndTextClass = '';
                }
                if ($enableTileView) :
            ?>
            <li class="<?php echo $iconAndTextClass; ?>">
                <a href="<?php echo $search_obj->get_template_btn_url() ?>">
                    <i class="fa fa-th fa-2x"></i>
                    <?php echo $textFilterTile; ?>
                </a>
            </li>
            <?php endif; ?>
            <li class="<?php echo $iconAndTextClass; ?>">
                <a href="<?php echo $search_obj->get_template_btn_url( 'list' ) ?>">
                    <i class="fa fa-bars fa-2x"></i>
                    <?php echo $textFilterList; ?>
                </a>
            </li>
            <li class="current <?php echo $iconAndTextClass; ?>" id="octave-map-option-filtering">
                <a href="<?php echo $search_obj->get_template_btn_url( 'map' ) ?>">
                    <i class="fa fa-map-marker fa-2x"></i>
                    <?php echo $textFilterMap; ?>
                </a>
            </li>
        </ul>
    </nav>
</div>
<?php
    $searchData = array(
        'keyword' => isset($_GET['keyword'])?$_GET['keyword']:'',
        'start_date' => isset($_GET['start_date'])?$_GET['start_date']:'',
        'end_date' => isset($_GET['end_date'])?$_GET['end_date']:'',
        'term' => isset($_GET['term'])?$_GET['term']:'',
        'event_location' => isset($_GET['event_location'])?$_GET['event_location']:'',
        'event_org' => isset($_GET['event_org'])?$_GET['event_org']:'',
        'view' => isset($_GET['view'])?$_GET['view']:'',
        'save_lst_list' => isset($_GET['save_lst_list'])?$_GET['save_lst_list']:'',
        'city' => isset($_GET['event_city'])? urldecode($_GET['event_city']) :  urldecode(of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY,'')),
        'region' => isset($_GET['event_region'])?$_GET['event_region']:'',
        'accessibility' => isset($_GET['accessibility']) ? $_GET['accessibility'] : array(),
        'is_discount' => isset($_GET['is_discount']) ? $_GET['is_discount'] :  0,
        'neighborhood' => isset($_GET['event_neighborhood']) ? $_GET['event_neighborhood'] :  '',
        'by_my_location' => isset($_GET['by_my_location']) ? $_GET['by_my_location'] : '',
        'apl-lat' => isset($_GET['apl-lat']) ? $_GET['apl-lat'] : '',
        'apl-lng' => isset($_GET['apl-lng']) ? $_GET['apl-lng'] : '',
        'date_format' => isset($_GET['date_format']) ? $_GET['date_format'] : 'm-d-Y',
        'free_event'  => isset($_GET['free_event']) ? $_GET['free_event'] : '',
    );
    $numberNextItem = of_get_option(Apollo_DB_Schema::_APL_NUMBER_ITEMS_VIEW_MORE, Apollo_Display_Config::APL_DEFAULT_NUMBER_ITEMS_VIEW_MORE);
    $mapZoom = of_get_option(OC_Common_Const::_OC_GOOGLE_MAP_ZOOM, 12);
?>
<div class="search-bkl wrap-search-map-event">
    <nav class="search-map--event">
        <div id="map-Event" class="blk-map-content" data-map-zoom="<?php echo $mapZoom; ?>"
             <?php echo AplEventFunction::renderHtmlAttribute($searchData); ?>
        >

        </div>
        <div class="blk-maps-events-filter">
            <div class="maps-events-filter--result">
                <p></p>
            </div>
            <div class="maps-events-filter--view" data-min-lat="" data-max-lat="" data-min-lng="" data-max-lng="">
                <a href="" class="btn-prev octave-disabled" data-page="1"><?php _e('Previous', 'apollo'); ?></a>
                <a href="" class="btn-next" data-page="1"><?php echo sprintf(__('View next %d events', 'apollo'), $numberNextItem); ?></a>
            </div>
        </div>
    </nav>
    <nav class="searching-list blk-event-map-item hidden">

    </nav>
</div>
