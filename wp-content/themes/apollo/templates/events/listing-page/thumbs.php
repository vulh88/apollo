<?php
$filterTitle = false;
$filterTitle = apply_filters('apl_event_listing_page_custom_title', $filterTitle);
if ( $results = $this->get_results() ) {
    foreach ( $results as $p ):

            $e = get_event( $p->ID );

            $_arr_show_date = $e->getShowTypeDateTime();
            $e_times =  $e->render_periods_time( $e->get_periods_time( " LIMIT ".Apollo_Display_Config::MAX_DATETIME_SEARCH." " ), '<div class="item">', '</div>' );

            ?>
            <li>
                <div class="div-one lazy-load-image-wrapper" data-type="link" data-url="<?php echo $e->get_permalink() ?>">
                    <div class="search-img"><a href="<?php echo $e->get_permalink() ?>"><?php echo $e->get_image( 'medium', array(),
                                array(
                                    'aw' => true,
                                    'ah' => true,
                                ),
                                'normal'
                            ) ?></a></div>
                    <div class="search-info"><a href="<?php echo $e->get_permalink() ?>"><span class="ev-tt"  data-n="2" ><?php echo $e->get_title($filterTitle) ?></span></a>
                        <?php if($e->get_type() === 'event'): ?>
                            <p class="meta auth"><?php echo $e->renderOrgVenueHtml() ?></p>
                            <span class="sch-date"><?php $e->render_sch_date() ?></span>
                        <?php elseif($e->get_type() === 'post'):
                            $scategory = get_the_category_list(", ", '', $e->id);

                            $nicename = get_the_author_meta('user_nicename', $e->post->post_author);
                            $created_date = get_the_date('d/m/Y',$e->id);
                            ?>
                            <div class="meta auth">
                                <div>
                                    <span>Category:</span>  <span><?php echo $scategory ?></span>
                                </div>
                                <div>
                                    <span>Author:</span>  <span><?php echo $nicename ?></span>
                                </div>
                                <div>
                                    <span>Created:</span>  <span><?php echo $created_date ?></span>
                                </div>
                            </div>
                            <span class="sch-date"><?php $e->render_sch_date() ?></span>
                        <?php endif; ?>
                    </div>
                </div>

                <?php if ( $e_times ): ?>
                    <div class="ico-date"><a><i class="fa fa-clock-o fa-2x"></i></a></div>
                    <div class="div-two">
                        <div class="show-events">
                            <?php
                            echo $e_times;
                            if ( $e->have_more_periods_time() ) {
                                echo '<div class="item">...</div>   ';
                            }
                            ?>
                        </div>
                    </div>
                <?php endif; ?>

            </li>
    <?php endforeach;
} else {
    _e( 'No results', 'apollo' );
} ?>