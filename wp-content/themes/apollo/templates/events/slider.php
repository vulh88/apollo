<?php

/**
 * Apply slider for Event Spotlight
 *
 * Ticket #11189
 */

if ( (!$slug = get_query_var('term')) && !get_query_var('_apollo_event_search') ) {
    return;
}

$currentTerm = get_term_by('slug',  $slug, get_query_var( 'taxonomy' ));
if ( (empty($currentTerm) || !isset($currentTerm->taxonomy) || $currentTerm->taxonomy != 'event-type' ) && !get_query_var('_apollo_event_search') ) {
    return;
}

if ( !of_get_option( Apollo_DB_Schema::_EVENT_ENABLE_TOP_SPOTLIGHT , 0) && get_query_var('_apollo_event_search') ) {
    return;
}

if ( !of_get_option( Apollo_DB_Schema::_EVENT_CATE_ENABLE_TOP_SPOTLIGHT , 0) && $currentTerm->taxonomy == 'event-type' ) {
    return;
}

$sliderType = of_get_option( Apollo_DB_Schema::_EVENT_TOP_SPOTLIGHT_TYPE, 'small' );

?>

<section class="section-slider">
    <?php
        $total_items = get_query_var('_apollo_event_search') ? Apollo_DB_Schema::_EVENT_TOP_SPOTLIGHT_NUM : Apollo_DB_Schema::_EVENT_CATE_TOP_SPOTLIGHT_NUM;
        $num = ($num_opt = of_get_option( $total_items, Apollo_Display_Config::_EVENT_TOP_SPOTLIGHT_NUM ));
        if (!get_query_var('_apollo_event_search')) {
            $term_query = $currentTerm->term_id;
        } else {
            $term_query = '';
        }
        $meta_query = array(
            array(
                'key'   => Apollo_DB_Schema::_APL_EVENT_SPOT_TYPES_PRIMARY_ID,
                'value' => $term_query,
            )
        );
        $eventSpotlights = get_posts( array(
            'post_type'      => Apollo_DB_Schema::_EVENT_SPOTLIGHT_PT,
            'posts_per_page' => $num,
            'orderby'        => 'date',
            'order'          => 'DESC',
            'meta_query' => $meta_query
        ));

        /** @Ticket #13130 */

    $semi_trans = of_get_option(Apollo_DB_Schema::_EVENT_TOP_SPOTLIGHT_SEMI_TRANSPARENT_TYPE, 'below');
    $bg_semi_trans = of_get_option( Apollo_DB_Schema::_EVENT_TOP_SPOTLIGHT_SEMI_TRANSPARENT_TYPE_BG_COLOR, '#000000' );

    if ($bg_semi_trans):
        ?>
        <style>
            .main-slider .wrap-inner {
                background-color:  <?php echo $bg_semi_trans ?> !important;
            }
            .business-slider .wrap-inner .i-caption-dine {
                background-color: <?php echo $bg_semi_trans ?> !important;
            }
            .business-slider .i-slider .i-caption-dine {
                background-color: <?php echo $bg_semi_trans ?> !important;
                opacity: 1;
            }
            .i-slider .i-caption-dine {
                background-color: <?php echo $bg_semi_trans ?> !important;
                opacity: 0.7;
            }
        </style>
    <?php endif;

        if ( $eventSpotlights ):
    ?>
        
            <?php
            /*@ticket #17932 [CF] 20181012 - [Event] Event landing page slider*/
            $classSlider = (of_get_option( Apollo_DB_Schema::_EVENT_ENABLE_TOP_SPOTLIGHT , 0) && !get_query_var( 'taxonomy' )) ? 'event-landing-page-slider' : ''?>
            <!--START: SLIDER-->
            <div class="main-slider full <?php echo $classSlider ?>">
                <div class="<?php echo ($sliderType == 'small') ? 'inner' : ''; ?>">
                    <div class="main-slider-overlay">
                        <div class="loading" style="display: none;"><a><i class="fa fa-spinner fa-spin fa-3x"></i></a></div>
                    </div>
                    <div class="flexslider <?php echo ($semi_trans != 'def') ? 'business-semi-trans' : ''; ?>"
                         data-speed="<?php echo Apollo_App::getSlideShowSpeed(Apollo_DB_Schema::_EVENT_PT) ?>">
                        <ul class="slides <?php echo ($semi_trans != 'def') ? 'business-slider' : ''; ?>">
                            <?php foreach ( $eventSpotlights as $spt ):
                                $spt = get_spot( $spt );

                                $l  = $spt->get_post_meta_data(Apollo_DB_Schema::_APL_EVENT_SPOTLIGHT_LINK);
                                $l  = trim($l);
                                $_l = !empty($l) ? $l : 'javascript:void(0);';

                                $termID       = $spt->get_post_meta_data(Apollo_DB_Schema::_APL_EVENT_SPOT_TYPES_PRIMARY_ID);
                                $term         = get_term($termID, 'event-type');
                                $open_style   = $spt->get_post_meta_data(Apollo_DB_Schema::_APL_EVENT_SPOTLIGHT_LINK_OPEN_STYLE);
                                $target       = $open_style ? 'target = "blank"' : '';
                                $img_id       = $spt->get_image_id();
                                $src_img      = wp_get_attachment_image_src($img_id, 'large');
                                $_o_summary   = $spt->get_summary(Apollo_Display_Config::HOME_SPOTLIGHT_EVENT_OVERVIEW_NUMBER_CHAR);

                                if ( ! $src_img[0] ) continue;
                                ?>
                                <li>
                                    <?php if ($semi_trans == 'def' || $sliderType == 'small') : ?>
                                        <div class="i-slider">
                                            <div class="inner">
                                                <?php if ( $_o_summary['text'] ):
                                                    ?>
                                                    <div class="i-caption-dine clearfix <?php echo empty($term->name) ? 'empty-category' : ''; ?>">
                                                        <?php if (!empty($term->name)) : ?>
                                                        <div class="capt-dine-category">
                                                            <h3><?php echo $term->name; ?></h3>
                                                        </div>
                                                        <?php endif; ?>
                                                        <div class="capt-dine-content <?php echo empty($term->name) ? 'content-full-width' : ''; ?>">
                                                            <h4 class="capt-ct-name"><a target="<?php echo $target; ?>" href="<?php echo $_l; ?>"><?php echo $spt->get_title(); ?></a></h4>
                                                            <p class="capt-ct-desc"><?php echo $_o_summary['text'] . ( $_o_summary['have_more'] ? '...' : '' ); ?></p>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <a target="<?php echo $target; ?>" href="<?php echo $_l; ?>">
                                                <img src="<?php echo isset( $src_img[0] ) ? $src_img[0] : ''; ?>" alt="<?php echo $spt->get_title() ?>">
                                            </a>
                                        </div>
                                    <?php else : ?>
                                        <div class="i-slider">
                                            <a target="<?php echo $target; ?>"  href="<?php echo $_l; ?>">
                                                <img src="<?php echo isset( $src_img[0] ) ? $src_img[0] : ''; ?>" alt="<?php echo $spt->get_title() ?>">
                                            </a>
                                        </div>
                                        <div class="wrap-inner">
                                            <div class="inner">
                                                <?php if ( $_o_summary['text'] ):
                                                    ?>
                                                    <div class="i-caption-dine clearfix <?php echo empty($term->name) ? 'empty-category' : ''; ?>">
                                                        <?php if (!empty($term->name)) : ?>
                                                        <div class="capt-dine-category">
                                                            <h3><?php echo $term->name; ?></h3>
                                                        </div>
                                                        <?php endif; ?>
                                                        <div class="capt-dine-content <?php echo empty($term->name) ? 'content-full-width' : ''; ?>">
                                                            <h4 class="capt-ct-name"><a target="<?php echo $target; ?>" href="<?php echo $_l; ?>"><?php echo $spt->get_title(); ?></a></h4>
                                                            <p class="capt-ct-desc"><?php echo $_o_summary['text'] . ( $_o_summary['have_more'] ? '...' : '' ); ?></p>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <!--END: SLIDER-->
        <?php endif; ?>
</section>
