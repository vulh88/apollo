<?php
/*
Template Name: Full Width Template
*/
?>
<div class="apl-full-width-page">
<?php
if(is_front_page()):
    get_template_part('templates/content-home', 'page');
else:
    get_template_part('templates/content', 'page');
endif;

?>

</div>    