<?php
    //PAGE: SEARCH
?>
<div class="breadcrumbs">
    <ul class="nav">
        <li><a href="<?php echo get_home_url(get_current_blog_id()) ?>"><?php _e('Home', 'apollo') ?></a></li>
        <li> <span><?php _e( 'Search', 'apollo' ) ?></span></li>
    </ul>
</div>

<?php
    $current_url = home_url() . $_SERVER["REQUEST_URI"];
    include APOLLO_TEMPLATES_DIR. '/search/class-apollo-search.php';
    $search_obj = new Apollo_Search();
    $search_obj->search();
?>

<div class="search-tt">
    <?php $search_obj->render_result_title() ?>
</div>

<?php
    $is_pag = $search_obj->have_pag() && !$search_obj->is_map_page();
    $have_pag = $search_obj->have_pag();
    $pag_html = '';
    if ( $is_pag || $have_pag ):
?>
<div class="search-bkl">
    <?php
    if ( $is_pag ): ?>
    <div class="blk-paging">
        <?php $pag_html = $search_obj->render_pagination();
        echo $pag_html;
        ?>
    </div>
    <?php endif; ?>

    <?php
    if ( $have_pag ):
        ?>
        <nav class="pg">
            <li class="txt"><span><?php _e( 'Show', 'apollo' ) ?>:</span></li>
            <li>
              <div class="pg-rad">
                    <input data-href="<?php echo $search_obj->get_display_type_url() ?>"
                           type="radio" name="radioshowpg" value="pg" <?php echo ! $search_obj->is_onepage() ? 'checked="checked"' : '' ?>class="rad-pg"><span class="pg"></span><span><?php _e( 'Paginated', 'apollo' ) ?></span>
              </div>
            </li>

            <li>
              <div class="pg-rad">
                <input data-href="<?php echo $search_obj->get_display_type_url( '1' ); ?>"
                       type="radio" name="radioshowpg" value="op" <?php echo $search_obj->is_onepage() ? 'checked="checked"' : '' ?>class="rad-pg"><span class="pg"></span><span><?php _e( 'One page', 'apollo' ) ?> </span>
              </div>
            </li>
        </nav>
<?php endif; ?>
</div>
<?php endif; ?>



<div class="search-bkl">
    <nav class="nav-tab">
        <ul class="tab-list-search apl-loading-total-items" data-search="<?php echo get_query_var( "s" ); ?>" >
            <?php $search_obj->render_tabs(); ?>
        </ul>
    </nav>
    <div class="tab-bt"></div>
</div>

<?php if ( $search_obj->get_results() ): ?>
<div class="search-bkl">
    <nav class="type">
        <ul>
            <li <?php echo $search_obj->is_thumb_page() ? 'class="current"' : '' ?> ><a href="<?php echo $search_obj->get_template_btn_url() ?>"><i class="fa fa-th fa-2x"></i></a></li>
            <li <?php echo $search_obj->is_list_page() ? 'class="current"' : '' ?>><a href="<?php echo $search_obj->get_template_btn_url( 'list' ) ?>"><i class="fa fa-bars fa-2x"></i></a></li>
            <li class="hidden" <?php echo $search_obj->is_map_page() ? 'class="current"' : '' ?>><a href="<?php echo $search_obj->get_template_btn_url( 'map' ) ?>"><i class="fa fa-map-marker fa-2x"></i></a></li>
        </ul>
    </nav>
</div>
<?php endif; ?>

<div class="search-bkl _apl_search_page">
    <?php
        if ( $search_obj->is_list_page() ) {
            $class = "searching-list";
            $container = 'searching-list';
        } else if ( $search_obj->is_thumb_page() ) {

            if (
                $search_obj->type == Apollo_DB_Schema::_ARTIST_PT ||
                $search_obj->type == Apollo_DB_Schema::_ORGANIZATION_PT ||
                $search_obj->type == Apollo_DB_Schema::_VENUE_PT ||
                $search_obj->type == Apollo_DB_Schema::_CLASSIFIED ||
                $search_obj->type == Apollo_DB_Schema::_PUBLIC_ART_PT ||
                $search_obj->type == Apollo_DB_Schema::_BUSINESS_PT
            ) {
                $container = $class = 'search-artist-thumb';
            } else {
                $container = $class = 'search-list-thumb';
            }

        } else {
            $container = $class = 'search-list-thumb';
        }
    ?>
    <nav class="<?php echo $class; ?>">
        <?php echo $search_obj->render_html(); ?>
    </nav>
</div>

<?php
    $is_view_more = $search_obj->have_more() && $search_obj->is_onepage();
?>
<input name="search-have-more" value="<?php echo $search_obj->have_more() ?>" type="hidden" >

<?php if ( ! $search_obj->is_map_page() ): ?>
<div <?php if ( ! $is_view_more ) echo 'style="display: none"';  ?> id="load-more" class="load-more b-btn load-more-search">
    <a href="javascript:void(0);"
        data-ride="ap-more"
        data-holder=".<?php echo $container ?>>:last"
        data-sourcetype="url"
        data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_get_more_search&page=2&s='.  get_query_var( 's' ).'') ?>&type=<?php echo $search_obj->type ?>&view=<?php echo isset( $_REQUEST['view'] ) ? $_REQUEST['view'] : '' ?>"
        data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
        data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
        class="btn-b arw"><?php _e('SHOW MORE', 'apollo') ?>
    </a>
</div>
<?php endif; ?>

<div class="search-bkl">
    <div class="blk-paging">
        <?php echo $pag_html; ?>
    </div>
</div>

