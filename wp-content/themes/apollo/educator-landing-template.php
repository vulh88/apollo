<?php
/*
 * @ticket #18192: 0002504: Arts Education Customizations - Adjust the landing page style - item 4
Template Name: Educator Landing Page
*/
global $post;
if (!Apollo_App::is_avaiable_module(Apollo_DB_Schema::_EDUCATION) || !$post) {
    wp_safe_redirect('/');
}
?>
<!doctype html>
<!--[if IE 8]>
<html lang="en" class="no-js ie ie8"><![endif]-->
<!--[if IE 9]>
<html lang="en" class="no-js ie ie9"><![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>>
<!--<![endif]-->
<?php get_template_part('templates/head'); ?>
<body <?php body_class('apollo'); ?>>
<?php echo Apollo_App::renderScriptInBodyTag(); ?>

<!--[if lt IE 8]>
<div class="alert alert-warning">
    <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your
    browser</a> to improve your experience.', 'apollo'); ?>
</div>
<![endif]-->
<?php
/*@ticket #18313 Enable education spotlight*/
$enableEducationSpotlight = get_post_meta($post->ID, 'enable-education-spotlight', true);
if(empty($enableEducationSpotlight) || $enableEducationSpotlight == 'yes' ){
    $educationSpotlights = get_posts( array(
        'post_type'      => Apollo_DB_Schema::_EDUCATION_SPOTLIGHT_PT,
        'orderby'        => 'date',
        'order'          => 'DESC',
    ));
}
else{
    $educationSpotlights = array();
}

$isFullTemplate = get_post_meta($post->ID,'education-full-template',true);
$classFullTemplate = $isFullTemplate == 'yes' ? 'apl-full-width-page' : '';
/*@ticket #18345: Add option hide/show social button on "educator landing page"*/
$arrShareInfo = array(
    'url' => get_the_permalink($post->ID)
);

$showSocialBtn = get_post_meta($post->ID,'education-show-social-btns',true);

do_action('get_header');
get_template_part('header');
/** @Ticket #18963 - page protected */
$pageProtected = post_password_required( $post );
if (!$pageProtected) :
?>
<section class="section-slider educator-landing-slider">
    <div class="main-slider full ">
        <div class="main-slider-overlay" style="display: none;">
            <div class="loading" style="display: block;"><a><i class="fa fa-spinner fa-spin fa-3x"></i></a></div>
        </div>
        <div class="flexslider" data-speed="<?php echo Apollo_App::getSlideShowSpeed(Apollo_DB_Schema::_EDUCATION) ?>">
            <ul class="slides">
                <?php
                    foreach ($educationSpotlights as $slider){
                        $srcImg      = wp_get_attachment_image_src(get_post_thumbnail_id($slider->ID), 'full');
                        $title = isset($slider->post_title) ? $slider->post_title : '';
                        $content = isset($slider->post_content) ? $slider->post_content : '';
                       /*@ticket #18320: 0002504: Arts Education Customizations - add hyperlinks to the landing page slider images (Education spotlights) - item 4*/
                        $link = get_post_meta( $slider->ID, Apollo_DB_Schema::_APL_EDUCATION_SPOTLIGHT_LINK, true );
                        if(empty($link)) $link = "javascript:void(0)";
                        $newTab = get_post_meta( $slider->ID, Apollo_DB_Schema::_APL_EDUCATION_SPOTLIGHT_LINK_OPEN_NEW_TAB, true );
                        $newTab = $newTab == 'on' ? 'target="blank"' : '';
                        ?>
                        <li>
                            <div class="i-slider">
                                <div class="inner">
                                    <?php /*@ticket #18342: Arts Education Customizations - Modify LANDING PAGE SLIDER*/ ?>
                                    <div class="i-caption">
                                        <a href="<?php echo $link?>" <?php echo $newTab?>>
                                            <h4><?php echo $title ?></h4>
                                        </a>
                                        <p><?php echo $content ?></p>
                                    </div>
                                </div>
                                <a href="<?php echo $link?>" <?php echo $newTab?>>
                                    <img src="<?php echo isset( $srcImg[0] ) ? $srcImg[0] : ''; ?>" alt="<?php echo $title ?>">
                                </a>
                            </div>
                        </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</section>
<?php endif; ?>
<section class="main <?php echo $classFullTemplate ?>">
    <div class="inner">
        <?php if (!$isNotFound = is_404()) { ?>
            <div class="row two-col">
                <div class="v-line"></div>
                <div class="wc-l"> <!-- start left side -->
                    <div class="breadcrumbs">
                        <ul class="nav">
                            <li><a href="<?php echo Apollo_App::changeBreadcrumbLink(Apollo_DB_Schema::_EDUCATION) ?>"><?php _e('Home', 'apollo') ?></a></li>
                            <li><span><?php echo the_title();?></span></li>
                        </ul>
                    </div>
                    <?php if ($showSocialBtn == 'yes'){ ?>
                    <div class="b-share-cat">
                        <?php SocialFactory::social_btns( array( 'info' => $arrShareInfo, 'id' => $post->ID ) ); ?>
                    </div>
                    <?php } ?>
                    <div class="apl-internal-content <?php echo $classFullTemplate ?>">
                        <?php
                            /** @Ticket #18963 - page protected */
                            $content = Apollo_App::convertContentEditorToHtml(get_the_content($post->ID), false, false);
                            $content = do_shortcode($content);
                            echo $content;
                            ?>
                    </div>
                </div>
                <?php if($isFullTemplate != 'yes'){ ?>
                <div class="wc-r"> <!-- left side -->
                    <?php do_shortcode("[apollo_widgets location='std' key='apl-sidebar-education']"); ?>
                </div>
                <?php } ?>
            </div>
            <?php
        } else {
            include apollo_template_path();
        }
        ?>

    </div>
</section>
<?php
get_template_part('footer');
?>
</body>
</html>