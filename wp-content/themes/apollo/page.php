<?php
if(is_front_page()):
    get_template_part('templates/content-home', 'page');
else:
    get_template_part('templates/content', 'page');
endif;