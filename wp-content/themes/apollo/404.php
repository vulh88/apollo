<?php
$routing = of_get_option(Apollo_DB_Schema::_ROUTING_FOR_404_ERROR,'404');

if($routing == 'home_page'){
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: ".get_bloginfo('url'));
        exit();
} else {
global $wpdb;

$blog_id = get_current_blog_id();
$table = $blog_id == 1? "wp_apollo_events_deleted" : "wp_".$blog_id."_apollo_events_deleted";

$slug = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
if(end($slug) == '/')$last = array_pop($slug);
$slug = end($slug);

$results = $wpdb->get_row( "SELECT * FROM $table WHERE post_slug LIKE '%".$slug."%'");

if($results){
        header("HTTP/1.1 410 Gone");
?>
<div class="inner"><div class="apl-internal-content page-not-found"><div class="padding-top-100"></div><h1 class="center h1-default">410 PAGE GONE</h1><p class="center">This page has been deleted.</p><div class="padding-bot-100"></div></div></div></div>
<?PHP
        }
        else{

        $content = Apollo_App::get_static_html_content( Apollo_DB_Schema::_PAGE_404 );
        ?>
        <div class="apl-internal-content page-not-found">

            <?php  if( !empty($content) ) {
                    echo $content;
            }else {?>
                <div class="padding-top-100"></div>
                <h1 class="center h1-default"> <?php _e('404 PAGE NOT FOUND!','apollo')?></h1>
                <p class="center" > <?php _e("Sorry, we could't find the page you were looking for. Please try your search again...","applo") ?></p>
                <?php
                /** @Ticket #13013 */
                ?>
                <div class="top-blk search-box">
                    <form method="get" id="searchform" action="<?php echo home_url(); ?>" class="form-search clearfix">
                        <input name="s" placeholder="<?php _e( 'Search ...', 'apollo' ); ?>" class="inp inp-txt solr-search" type="text">
                        <button type="submit" class="btn btn-link"> <i class="fa fa-search fa-flip-horizontal fa-lg"></i></button>
                    </form>
                </div>
                <div class="padding-bot-100"></div>
            <?php  } ?>
        </div>

<?php }} ?>
