$j = jQuery.noConflict();
$j(document).ready(function(){
    $j('.eli-event-tooltip-item-wrapper').tooltipster({
        functionPosition: function(instance, helper, position){
            position.coord.top += 4;
            position.coord.left += 4;
            return position;
        }
    });
});