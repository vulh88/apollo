(function($) {

    $(function() {

        $(document).ready(function() {

            $('body').off('change', '.apl-ninja-custom-educator-field select').on('change', '.apl-ninja-custom-educator-field select', function (e) {
                var current = $(e.currentTarget);
                var eduId = current.val();
                var proElement = $('body').find('.apl-ninja-custom-program-field select');
                if (eduId != 0 && eduId != '' && proElement.length > 0) {
                    $.ajax({
                        url: APL.ajax_url,
                        data: {
                            action: 'apollo_get_program_by_educator_id',
                            edu_id: eduId
                        },
                        beforeSend: function() {
                            $(window).block($.apl.blockUI);
                        },

                        success : function (res) {
                            $(window).unblock();
                            if (res.data.length > 0) {
                                proElement.html('');
                                proElement.append(res.data);
                            }
                        }
                    });
                } else {
                    proElement.html('');
                }
            });
        });

    });

}) (jQuery);