(function($){
    window.APOLLO =  window.APOLLO || {};
    window.APOLLO.AplSelect2 = (function(){
        var module = {};
        module.init = function(){


            $(document).ready( function() {
                $("[name='event_city']").on('select2:open', function() {
                    var dropdownCity = $('.select2-container.select2-container--default');
                    dropdownCity.addClass('apl-select2-hidden');
                    dropdownCity.removeClass('search-display');
                    module.showCurrentSelected(true);
                });

                $("[name='event_city']").on('select2:selecting', function() {
                    module.showCurrentSelected(false);
                });

                $(document).ready(function () {
                    $('body').on('click', '.select2-container.select2-container--default.apl-select2-hidden li.select2-results__option strong', function (e) {
                        if($('body').hasClass('apl-select2-selected')) {
                            $('body').removeClass('apl-select2-selected');
                            var selected = $('[name="event_city"]').find(':selected');
                            var attrName = selected.closest('optgroup').attr('label');
                            $('.apl-select2-hidden li[aria-label="'+attrName+'"]').addClass('display');
                            $('.search-display li[aria-label="'+attrName+'"]').removeClass('display');
                        }
                        var current = $(e.currentTarget);
                        var parent = current.closest('ul').find('li');
                        current = current.closest('li');
                        if (current.hasClass('display')) {
                            current.removeClass('display');
                        } else {
                            if (!parent.hasClass('search-display')) {
                                parent.removeClass('display');
                                var defaultOption = current.closest('.apl-select2-hidden').find('ul li:first-child');
                                if (defaultOption.length > 0) {
                                    $(defaultOption[0]).addClass('display');
                                }
                            }
                            current.addClass('display');
                        }
                    });

                    $('body').on('keyup', '.select2-container.select2-container--default.apl-select2-hidden .select2-search__field', function (e) {
                        var current = $(e.currentTarget);
                        var parent = current.closest('.apl-select2-hidden');
                        if (current.val() != '') {
                            if (!parent.hasClass('search-display')) {
                                parent.addClass('search-display');
                            }
                        } else {
                            if (parent.hasClass('search-display')) {
                                parent.removeClass('search-display');
                                module.showCurrentSelected(true);
                            }
                        }
                    });
                });

            });
            return module;
        };

        module.showCurrentSelected = function (display) {
            var selected = $('[name="event_city"]').find(':selected');
            if (selected.length > 0) {
                $.each(selected, function(index, item){
                    var attrName = $(item).closest('optgroup').attr('label');
                    if (typeof attrName !== 'undefined' && attrName != '') {
                        var parentEle = $('body');
                        var style = 'none';
                        var strCss = 'body.apl-select2-selected .apl-select2-hidden li[aria-label="'+attrName+'"] ul';
                        if (display) {
                            parentEle.addClass('apl-select2-selected');
                            style = 'block';
                        } else {
                            parentEle.removeClass('apl-select2-selected');
                        }
                        $('head').append('<style type="text/css">'+strCss+'{display: '+style+'}</style>');
                    }
                });
            }
        };

        return module.init();
    })();
})(jQuery);
