(function ($) {
    window.APOLLO =  window.APOLLO || {};
    window.APOLLO.formAbstractValidation = (function(){
        var module = {};
        module.constantOffsetTop = 80;
        module.enabledJqueryValidation = true;
        module.init = function(){
            return module;
        };

        module.customValidationForm = function(currentForm){
            var resultValidation = 0;
            $('.custom-validate-field').each(function(index, item){
                var validationType = $(item).attr("data-custom-validate");
                var errorMessage = $(item).attr("data-error-message");
                if(validationType == 'multi-checkbox' || $(item).is(':enabled')){
                    switch (validationType) {
                        case 'multi-checkbox':
                            if(!module.customValidationMultiCheckbox(item)){
                                resultValidation += 1;
                                $(item).after(module.renderErrorMessage(errorMessage));
                            }
                            break;
                        case 'wysiwyg':
                            if(!module.customValidationWYSIWYG(item)){
                                resultValidation += 1;
                                $(item).after(module.renderErrorMessage(errorMessage));
                                $('#cke_' + $(item).attr('data-id')).addClass('inp-error');
                            }
                            break;
                        case 'checkbox':
                            if(!module.customValidationCheckbox(item)){
                                resultValidation += 1;
                                module.renderErrorMessageForCheckboxField(item,errorMessage);
                            }
                            break;
                        case 'select2':
                            if(!module.customValidationSelect2(item)){
                                if (! $(item).is(':hidden')) {
                                    resultValidation += 1;
                                    $(item).parent().append(module.renderErrorMessage(errorMessage));
                                    $(item).parent().find('span.select2-container').addClass('inp-error');
                                }
                            }
                        default:
                            break;
                    }
                }
            });
            return resultValidation === 0;
        }

        module.renderErrorMessageForCheckboxField = function (instance, errorMessage) {
            if($(instance).closest('.qualifi-list').is('ul')){
                $(instance).closest('.qualifi-list').append('<li>' + module.renderErrorMessage(errorMessage) + '</li>');
            } else {
                $(instance).after(module.renderErrorMessage(errorMessage));
            }
        }

        module.customValidationWYSIWYG = function(instance) {
            $('#cke_' + $(instance).attr('data-id')).removeClass('inp-error');
            $(instance).parent().find('.formError').remove();
            CKEDITOR.instances[$(instance).attr('data-id')].updateElement(); // update textarea
            var editorContent = $(instance).val().replace(/<[^>]*>/gi, '').replace(/&nbsp;/gi,''); // strip tags
            var placeholder = $(instance).attr('placeholder');
            editorContent = editorContent.replace(placeholder,'');
            return editorContent.length > 0;
        };

        module.customFormValidationErrorMessageHandler = function(currentForm){
            currentForm.bind("jqv.field.result", function(event, field, errorFound, prompText) {
                if($(field).hasClass('inp-error')){
                    $(field).removeClass('inp-error');
                }
                if(errorFound && typeof prompText !== "undefined" && prompText.length > 0){
                    if($(field).hasClass('apollo-multi-checkbox') || $(field).hasClass('apollo-radio')){
                        var wrapperList = $(field).closest('ul');
                        var errMesssage = wrapperList.attr('data-errormessage-value-missing');
                        wrapperList.find('.formError').remove();
                        if(wrapperList.parent().find('.formError').length === 0){
                            wrapperList.after(module.renderErrorMessage(errMesssage));
                        }
                    } else {
                        var arrErrors = prompText.split('<br/>');
                        $(field).addClass('inp-error');
                        if(arrErrors.length > 1){
                            $(field).parent().find('.formErrorContent').html(arrErrors[0]);
                        }
                    }
                } else {
                    $(field).closest('div').find('.formError').remove();
                }
            });
        }

        module.renderErrorMessage = function(errorMessage) {
            var errorMessageValue = " " + errorMessage;
            return '' +
                '<div class="c-eformError parentFormadmin-frm-step1 formError inline">' +
                '   <div class="formErrorContent">' + errorMessageValue + '   </div>' +
                '</div>';
        }

        module.customValidationMultiCheckbox = function(instance){
            /**
             * Vandd custom validate with relation field
             * relation-field
             */
            var relation = $(instance).closest('.artist-blk').find('.relation-field');
            if(relation.length > 0 && relation.val().trim() != ''){
                return true;
            }
            $(instance).parent().find('.formError').remove();
            return $(instance).find( 'input:not(.hidden):checked').length > 0;
        }

        module.customValidationCheckbox = function(instance){
            var formErrIns = $(instance).closest('.qualifi-list').find('.formError');
            formErrIns.parent('li').remove();
            return $(instance).is(':checked');
        }

        module.customValidationSelect2 = function(instance){
            $(instance).parent().find('.formError').remove();
            $(instance).parent().find('span.select2-container').removeClass('inp-error');
            var select2ID = $(instance).attr("id");
            var selectedOption = $('#'+select2ID).val();
            return parseInt(selectedOption) !== 0 && selectedOption !== "";
        }

        module.scrollToCustomValidationError = function(){
            var firstCustomErrElement = $('.formError:first');
            var destinationTop = firstCustomErrElement.offset().top;
            var destinationLeft = firstCustomErrElement.offset().left;
            $("html, body").animate({
                scrollTop: destinationTop - module.constantOffsetTop
            }, 1100, function(){
                if(firstCustomErrElement.is('input:text') || firstCustomErrElement.is('select')){
                    firstCustomErrElement.focus();
                }
            });
            $("html, body").animate({scrollLeft: destinationLeft},1100);
        }

        /**
         * ThienLD: Temporarily, validated rules for primary image in all FE USer Dashboard Form are placed here. It's rebuilt once the uploading primary image and gallery are rebuilt */
        module.validateRequiredPrimaryImage = function (e, isScroll){
            var isPrimaryImageUpload = $('#_primary_img').attr('src').length > 0;
            if ( isPrimaryImageUpload ) {
                $('.gallery-bkl').removeClass('primary-image-error');
                $('.required-image').hide();
            } else if ( $('#check_primary_image').hasClass('requirement') ) {
                $('.gallery-bkl').addClass('primary-image-error');
                $('.required-image').show();

                isScroll = isScroll ? isScroll : 'scroll';
                if(isScroll == 'scroll'){
                    $('html, body').animate({
                        scrollTop: $(".nav-tab").offset().top
                    }, 1000);
                }
                return false;
            }
            return true;
        };

        return module.init();
    })();

})(jQuery);


function disableSpace(field, rules, i, options){
    var val = field.val();
    var result =  val.replace(/ /g, "");
    field.val(result);
}