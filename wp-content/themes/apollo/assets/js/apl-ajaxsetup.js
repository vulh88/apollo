(function($){
    /** @Ticket #14105 */
    $.ajaxSetup({
        cache: false,
        headers: {
            'X-Apl-Frontend': 1
        }
    });
})(jQuery);
