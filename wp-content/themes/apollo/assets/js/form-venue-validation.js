(function ($) {
    window.APOLLO =  window.APOLLO || {};
    window.APOLLO.formVenueValidation = (function(){
        var module = {},
            currentForm;

        module.init = function(){
            if(window.APOLLO.formAbstractValidation.enabledJqueryValidation) {
                $(document).ready( function() {
                    currentForm = $('#venue-frm');
                    currentForm.validationEngine(
                        'attach',
                        {   promptPosition:'inline',
                            scroll:true,
                            scrollOffset:window.APOLLO.formAbstractValidation.constantOffsetTop
                        }
                    );
                    currentForm.find('.submit-form-with-validation-engine').off('click').on('click', handleCurrentFormSubmission);
                    window.APOLLO.formAbstractValidation.customFormValidationErrorMessageHandler(currentForm);
                });
            }
            return module;
        };

        var handleCurrentFormSubmission = function (event) {
            var submitSuccessfulMessage = $('._apollo_success');
            if(submitSuccessfulMessage.length > 0){
                submitSuccessfulMessage.remove();
            }
            var btnSubmit = $(event.currentTarget);
            btnSubmit.css('pointer-events', '');
            //currentForm.find('.formError').remove();
            var resultValidation = currentForm.validationEngine(
                'validate',
                {   promptPosition:'inline',
                    scroll:true,
                    scrollOffset:window.APOLLO.formAbstractValidation.constantOffsetTop
                }
            );
            var resultCustomValidation = window.APOLLO.formAbstractValidation.customValidationForm(currentForm);

            if(resultValidation && resultCustomValidation){
                btnSubmit.css('pointer-events', 'none');
                return true;
            } else if(resultValidation && !resultCustomValidation) {
                window.APOLLO.formAbstractValidation.scrollToCustomValidationError();
                return false;
            } else {
                btnSubmit.css('pointer-events', '');
                return false;
            }
        };

        return module.init();
    })();
})(jQuery);