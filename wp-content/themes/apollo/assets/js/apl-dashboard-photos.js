var AplDashboardPhotos = function ($) {

    var $window = $(window);

    var aplUploadFeaturedImage = function($this, target, pid) {
        /**
         * ThienLD: handle ticket #12725 */
        if( ! window.APOLLO.formAbstractValidation.validateRequiredPrimaryImage()){
            $this.css('pointer-events', '');
            return false;
        }

        /** Get featured image info */
        var featured_info = {
            img_id: $('.apl-upload-crop #_featured_img_id').val(),
            file: $('.apl-upload-crop #_featured_file').val(),
            oname: $('.apl-upload-crop #_featured_oname').val(),
            type: $('.apl-upload-crop #_featured_type').val(),
            url: $('.apl-upload-crop #_featured_url').val(),
            is_deleted: $('.apl-upload-crop #_featured_is_deleted').val(),
            _is_new_or_modify: $('.apl-upload-crop #_featured_is_new_or_modify').val(),
        };

        /** Get gallery info */
        var galleries = $('#_apl_gallerys .gallery-item');
        var gallery_info = {};
        if( galleries.length > 0 ) {
            $(galleries).each(function(index, item) {
                var img_id = $(item).attr('img-id');
                var gFile = $(item).find('._apl_file').val();
                var gOname = $(item).find('._apl_oname').val();
                var gType = $(item).find('._apl_type').val();
                var gUrl = $(item).find('._apl_url').val();
                gallery_info[img_id] = {
                    file: gFile,
                    oname: gOname,
                    type: gType,
                    url: gUrl
                };
            });
        }
        /** @Ticket #13959 - current tab active */
        var selectedTab = $('.tab-image-list').find('li.selected');

        /*
        * @Ticket 15085
        * TO DO: waiting the refactor - page builder
        */
        var dataFeatureTags = [];
        var dataGalleryTags = [];

        if($(".pbm-feature-image-select-tags").length > 0) {
            dataFeatureTags = $('.pbm-feature-image-select-tags input[name="apl_pbm_fe_selected_feature_image_tags[]"]:checked').map(function () {
                return $(this).val();
            }).get();

            dataGalleryTags = $('.pbm-gallery-select-tags input:checkbox:checked').serializeArray();
        }

        $.ajax({
            url: APL.ajax_url,
            data: {
                target: target,
                pid: pid,
                featured: featured_info,
                gallery: gallery_info,
                action: 'apollo_save_featured_and_gallery_image',
                _aplNonceField:$('#_aplNonceField').val(),
                current_page:'apollo_artist_gallery_page',
                dataFeatureTags: dataFeatureTags,
                dataGalleryTags: dataGalleryTags,
                'caption_gallerys[]': $('[name="caption_gallerys[]"]').map(function(){return $(this).val();}).get()
            },
            beforeSend: function() {
                $window.block($.apl.blockUI);
            },
            success: function(res) {
                if ( $('._apollo_success') ) {
                    $('._apollo_success').remove();
                }

                if(res.status === 'ok') {

                    localStorage.setItem("apl_photo_active_tab", selectedTab.index());
                    location.reload();

                    // $(".submit-blk").before($('<div class="_apollo_success"> <i class="fa fa-check"></i> ' + $.apl.__('Success!') + ' </div>'));
                } else {
                    $(".submit-blk").before($('<div class="error ajax-error"> ' + $.apl.__('Cannot save data. Some error occurred!') + ' </div>'));
                    setTimeout(function(){
                        $(".ajax-error").remove();
                    }, 3000);
                }
                $this.css('pointer-events', '');
            },
            complete: function() {
                $window.unblock();
            }
        });
    }

    function aplSubmitArtistPhoto() {
        $("#apollo_submit_artist_photo").on('click', function(e) {
            e.preventDefault();

            var $this = $(this),
                $center = $('#artist_center_info');
            if ($center.length > 0) {
                var pid = $center.data('pid');
                var target = $center.data('target');
            } else {
                var pid = $('#artist_id').val();
                var target = $('#artist_target').val();
            }


            aplUploadFeaturedImage($this, target, pid);
        })
    }

    function aplSubmitOrgPhoto() {
        //tri add event submit org photos
        $('#apollo_submit_org_photo').on('click',function(e){
            e.preventDefault();
            var $this = $(this),

                pid = $('#org_id').val(),
                target = $('#org_target').val();

            aplUploadFeaturedImage($this, target, pid);
        });
    }

    function aplSubmitVenuePhoto() {
        //tri add event submit venue photos
        $('#apollo_submit_venue_photo').on('click',function(e){
            e.preventDefault();
            var $this = $(this),

                pid = $('#venue_id').val(),
                target = $('#venue_target').val();

            aplUploadFeaturedImage($this, target, pid);
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            if (
                APL.is_dashboard
                || APL['is_singular[event]'] === "1"
                || APL['is_singular[artist]'] === "1"
                || APL['is_singular[educator]'] === "1"
                || APL['is_singular[organization]'] === "1"
                || APL['is_singular[venue]'] === "1"
                || APL['is_singular[classified]'] === "1"
                || APL['is_singular[public_art]'] === "1"
                || APL['is_singular[post]'] === "1"
                || APL['is_listing[post_type]'] === "1"
            ) {
                aplSubmitArtistPhoto();
                aplSubmitOrgPhoto();
                aplSubmitVenuePhoto();
                /** @Ticket #13959 */
                var activePhotoTab = localStorage.getItem('apl_photo_active_tab');
                if (typeof activePhotoTab !== 'undefined' && activePhotoTab !== null) {

                    // It caused some broken template when switching from the second tab to the first one so we need to test more about this feature
                    if (activePhotoTab == 1) {
                        // var imageTab = $('.tab-image-list li').last();
                        // imageTab.trigger('click');
                        // localStorage.removeItem('apl_photo_active_tab');
                    }
                }
            }
        }
    };

}(jQuery);

jQuery(document).ready(function() {
    AplDashboardPhotos.init();
});