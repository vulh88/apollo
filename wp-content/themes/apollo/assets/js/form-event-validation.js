(function ($) {
    window.APOLLO =  window.APOLLO || {};
    window.APOLLO.formEventValidation = (function(){
        var module = {},
            currentForm = null;

        module.init = function(){
            if(window.APOLLO.formAbstractValidation.enabledJqueryValidation) {
                $(document).ready( function() {
                    currentForm = $('#admin-frm-step1');
                    var scrollable = $(currentForm).attr("name") === 'admin_frm_step1';
                    currentForm.validationEngine('attach', {
                        promptPosition:'inline',
                        scroll:scrollable,
                        scrollOffset:window.APOLLO.formAbstractValidation.constantOffsetTop,
                        custom_error_messages:{

                        },
                    });
                    window.APOLLO.formAbstractValidation.customFormValidationErrorMessageHandler(currentForm);
                });
            }
            return module;
        };

        var resetTagFieldVal = function(){
            var tagField = $('.event-tags [name="tag"]');
            if(tagField.length > 0){
                tagField.val('');
            }
        };

        module.handleCurrentFormSubmission = function(currentForm){
            if(!window.APOLLO.formAbstractValidation.enabledJqueryValidation){
                resetTagFieldVal();
                $(currentForm).submit();
            } else {
                $(currentForm).find('.formError').remove();
                var scrollable = $(currentForm).attr("name") === 'admin_frm_step1';
                var resultValidation = $(currentForm).validationEngine('validate', {promptPosition:'inline', scroll:scrollable, scrollOffset:50});
                var resultCustomValidation = window.APOLLO.formAbstractValidation.customValidationForm(currentForm);
                if(resultValidation && !resultCustomValidation) {
                    window.APOLLO.formAbstractValidation.scrollToCustomValidationError();
                    return false;
                }
                if(resultValidation && resultCustomValidation){
                    resetTagFieldVal();
                    $(currentForm).submit();
                }
            }
            return false;
        };

        return module.init();
    })();

/*javascript:apollo_submit_form( 'admin_frm_step1', 'submit_event' )*/

})(jQuery);