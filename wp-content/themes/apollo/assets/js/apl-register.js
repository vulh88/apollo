(function ($) {

    /**
     * @ticket #18933: Apply reCaptcha to the FE registration form
     */
    function addCapchaRegisterForm() {
        $('.regis-wrp button[name="apl_register_submit"]').on('click', function (e) {
            e.preventDefault();
            apollo_submit_form('login-frm', 'apl_register');
        });
    }

    $(document).ready(function () {
        addCapchaRegisterForm();
    });
})(jQuery);