(function($) {

    $(function() {

        $(document).ready(function() {

            /*@ticket #17079: 0002366: wpdev19 - [Artist] Separate member artists and normal artists in two tabs*/
            $('#search-artist').on('submit', function(e) {
                var result = new RegExp('[\?&]artist_member=([^&#]*)').exec(window.location.href);
                if (result){
                    var artistMember = result[1];
                    var input = $("<input>")
                        .attr("type", "hidden")
                        .attr("name", "artist_member").val(artistMember);
                    $('#search-artist').append($(input));
                }
            });

            $('body').on('apl_remove_doc', function (e, res) {
                var oldElement = $('.apollo-artist-upload_pdf-list .apl-remove-old-pdf');
                if (oldElement.length > 0) {
                    var dataAction = res.action_data;
                    if (typeof dataAction !== 'undefined' && dataAction != '') {
                        dataAction = dataAction.split(',');
                        if (typeof dataAction[1] !== 'undefined' && dataAction[1] != '') {
                            var removeId = dataAction[1].trim();
                            var currentIds = oldElement.val();
                            currentIds = currentIds != '' ? currentIds.split(',') : [];
                            if (currentIds.indexOf(removeId) < 0 ) {
                                currentIds.push(removeId);
                                oldElement.val(currentIds.join(','));
                            }
                        }
                    }
                }
            });

            /*@ticket #17126 */
            function controlArtistMemberRequirement(id, submitButton, currentTab){
                var artistMemberRequirement =  $(id + " .artist-member-requirement");

                if(artistMemberRequirement.length){
                    //currentTab:
                    //1: PRIMARY IMAGE
                    //2: GALLERY

                    if(submitButton && submitButton.length){
                        var displayButton = currentTab == 1 ? 'block' : 'none';
                        submitButton.css('display', displayButton);
                    }

                    var displayMemberRequire = currentTab == 1 ? "display: none !important" : "display: block !important";
                    artistMemberRequirement.attr('style', displayMemberRequire);
                }
            }

            /*@ticket #17126 */
            $("#Artist-photo-frm").off('click', '.tab-image-list li').on('click', '.tab-image-list li', function(e) {
                var submitButton =  $("#Artist-photo-frm .submit-blk");
                var currentTab = $(this).find('a').data('id');
                controlArtistMemberRequirement("#Artist-photo-frm", submitButton, currentTab);
            });

            /**
             * @ticket #18695: [CF] 20181221 - Add the option for image upload to the main artist profile form.
             */
            $("#artist-profile-photo-frm").off('click', '.tab-image-list li').on('click', '.tab-image-list li', function(e) {
                var currentTab = $(this).find('a').data('id');
                controlArtistMemberRequirement("#artist-profile-photo-frm", '' , currentTab);
            });

            /*@ticket #17126 */
            $("#artist_center_info").off('click', '.tab-image-list li').on('click', '.tab-image-list li', function(e) {
                var submitButton =  $("#apollo_submit_artist_photo .submit-blk");
                var currentTab = $(this).find('a').data('id');
                controlArtistMemberRequirement("#artist_center_info", submitButton, currentTab);
            });

            /*Begin @ticket #17815 0002366: wpdev19 - The "Donate" button widget is pushing out to the right*/
            var artistBioElement = $('.max_length_artist_description');
            var maxLengthArtistBio = !isNaN(parseInt(artistBioElement.val())) ? artistBioElement.val() : 1500
            var bioStatementTextArea = $('.artist_bio_statement_type textarea.inp-desc-event');

            var countDownBioArtist = function(content){
                var currentLength = content.length,
                    countDownElement = $('.countdown_bio_statement');

                if( currentLength <= maxLengthArtistBio ){
                    countDownElement.removeClass("hidden");
                    countDownElement.text(maxLengthArtistBio - currentLength  + " characters remaining.");
                }
                else{
                    countDownElement.addClass("hidden");
                }
            };

            bioStatementTextArea.off('keyup').on('keyup',function(e){
                countDownBioArtist($(this).val());
                if( $(this).val().length >= maxLengthArtistBio ){
                    $(this).val( $(this).val().substring(0, maxLengthArtistBio));
                }
            });


            bioStatementTextArea.mouseleave(function () {
                $('.countdown_bio_statement').addClass("hidden");
            });

            $(document).on( 'tinymce-editor-init', function( event, editor ) {
                if(tinyMCE.activeEditor){
                    var bioEditor = tinyMCE.activeEditor;
                    if(bioEditor.formElement &&  bioEditor.formElement.id &&  bioEditor.formElement.id === "artist-frm"){
                        bioEditor.onKeyUp.add( function(editor) {
                            var content = editor.getContent().replace(/(<[a-zA-Z\/][^<>]*>|\[([^\]]+)\])/ig, '' );
                            if (content.length > maxLengthArtistBio) {
                                bioEditor.setContent(artistBioElement.attr('data-old-value'));
                                editor.selection.select(editor.getBody(), true);
                                editor.selection.collapse(false);
                                return false;
                            }
                            else{
                                countDownBioArtist(content);
                            }
                        });

                        bioEditor.onKeyDown.add( function(editor) {
                            var content = editor.getContent().replace(/(<[a-zA-Z\/][^<>]*>|\[([^\]]+)\])/ig, '' );
                            if (content.length <= maxLengthArtistBio) {
                                artistBioElement.attr('data-old-value', editor.getContent());
                            }
                        });

                        bioEditor.on('mouseleave', function (){
                            $('.countdown_bio_statement').addClass("hidden");
                        });
                    }
                }
            });

            //end @ticket #17815
        });

    });

}) (jQuery);