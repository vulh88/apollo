ApolloCollapseExpand = function ($) {

    function initDataToggleClass() {
        /*@ticket #18084: [CF] 20181030 - [Business] Collapse/expand the business sub-category types by adding an 'arrow' next to each parent type - item 1*/
        /*@ticket #18221: 0002504: Arts Education Customizations - Add a checkbox option for "Add Menu Arrow" - item 1*/
        $(document).on('click', '[data-toggle-class]', function (params) {
            var className = $(this).data('toggle-class');
            $(this).toggleClass(className);
        });
    }

    /**
     * @ticket #18621
     * Custom Field is multi checkbox has arrow to collapse expanded.
     * The custom field auto expanded if it have element selected
     */
    function expandCustomField() {
        var toggleClass = $('.custom-fields.artist-blk [data-toggle-class]');
        if (toggleClass.length) {
            toggleClass.each(function () {
                if ($(this).closest('.el-blk.radio-box').find('ul.vertical li input[type="checkbox"]:checked').length) {
                    $(this).addClass('show');
                }
            });
        }
    }

    return {
        init: function () {
            initDataToggleClass();
            expandCustomField();
        }
    };
}(jQuery);

jQuery(document).ready(function () {
    ApolloCollapseExpand.init();
});