// @Ticker 13913
jQuery(document).ready(function($){
    var _is_error_tellAF = false;

    $('body').on('submit', 'form#tell-a-friend', function(e) {
        e.preventDefault();
        var $form_tellAF = $('.tellAF-popup');
        var _is_error_tellAF = false;
        var $ipt_email = $("#_tell_a_friend_email");
        var $ipt_captcha = $("#_tell_a_friend_captcha");
        $form_tellAF.find("._tell_a_friend_err_container .error span").addClass('hidden');

        //validate require email
        if ($ipt_email.val().trim(" ") === "") {
            _is_error_tellAF = true;
            $($ipt_email.data('error_holder') + ' ._required').removeClass('hidden');
        }

        //validate require captcha
        if ($ipt_captcha.val().trim(" ") === "") {
            _is_error_tellAF = true;
            $($ipt_captcha.data('error_holder') + ' ._required').removeClass('hidden');
        }

        if(_is_error_tellAF) {
            return;
        }

        //validate captcha
        $form_tellAF.block($.apl.blockUI);
        valiDateCaptcha($form_tellAF, $ipt_email, $ipt_captcha, true);

    });

    function valiDateCaptcha($form_tellAF, $ipt_email, $ipt_captcha, is_submit){
        $.ajax({
            url: $ipt_captcha.data('url_check_captcha') + '&code=' + (is_submit ? $ipt_captcha.val() : ''),
            method: 'GET',
            success: function(response) {
                if(response.error === true) {
                    if(is_submit) {
                        $($ipt_captcha.data('error_holder') + ' ._captcha').removeClass('hidden');
                        $($ipt_captcha.data('error_holder') + ' _captcha').html('* ' + response.msg);
                        $form_tellAF.unblock();
                    } else {
                        $ipt_email.val("");
                    }

                    /*  renew image */
                    $($ipt_captcha.data('captcha_image')).attr('src', response.new_image_link);
                    $ipt_captcha.val("");
                }
                else if(is_submit) {
                    _is_error_tellAF = false;
                    $("#btn-submit-tell-friend").data('no-error', 'true');
                    $.ajax({
                        url: $form_tellAF.find('form').attr('action'),
                        method: 'GET',
                        data: {email: $ipt_email.val(), link_share: window.location.href},
                        success: function (response) {
                            if (response.error === true) {
                                alert($form_tellAF.find('form').data('error'))
                            }
                            else {
                                valiDateCaptcha($form_tellAF, $ipt_email, $ipt_captcha, false);
                                alert($form_tellAF.find('form').data('success'));
                            }
                            $form_tellAF.unblock();
                        },
                        complete: function () {
                            $form_tellAF.unblock();
                        }
                    });
                }
            },
            error: function() {
                $form_tellAF.unblock();
            }
        });
    }
});