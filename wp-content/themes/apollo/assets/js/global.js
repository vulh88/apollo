/**
 * Created by tuanphp on 31/10/2014.
 */

(function($) {
    
    if ( typeof APL != "undefined" ) {
        var $body = $('body'), $window = $(window);


        $(function() {

            // Stop comment when the maintenance mode is enabled
            $('[name="comment_event_form"]').submit(function(e) {
                var btn = $('.apl-mm-comment');
                if (btn.length > 0) {
                    alert(btn.data().message);
                    e.preventDefault();
                }
            });

            if ( $.apl.is_mobile() ) {
                $('body').addClass('on-mobile');
                $( '.single .pt.print' ).hide();
                $( '.single .pt.email' ).hide();
            }

            /* HANDLE TOP SOCIAL BUTTON FUNCTION  */
            $.apl.handle_social_button();

            // Handle see more taxonomies
            $( '.wc-l' ).on( 'click', '#apollo-see-more-taxonomy', $.apl.handle_see_more_taxonomy );

            // Handle content of registered page
            $.apl.handle_registered_page_content();

            /* RESIZE EVENT DESKTOP OR CHANGE MOBILE DIRECTION*/
            $.apl.handle_screen_size();
            $( window ).resize( $.apl.handle_screen_size );

        });



        /***************************************************************************
         *   Register global function for apollo 
         **/
        $.apl = {
            handleDisplayingSearchWidgetOnMobileVersion: function(){
                /**
                 * Thienld : call all module function to handle show/hide for its search widget. */
                $.apl.displayArtistTopSearch();
                $.apl.displayBusinessTopSearch();
                $.apl.displayClassifiedTopSearch();
                $.apl.displayEducationTopSearch();
                $.apl.displayEventTopSearch();
                $.apl.displayOrganizationTopSearch();
                $.apl.displayPublicArtTopSearch();
                $.apl.displayVenueTopSearch();
            },
            getText: function(key) {
                if (APL.jsTrans[key]) return APL.jsTrans[key];
                return key;
            },
            google_map: {
                eventWhatNearBy: function(){},
                has_load: false,
                load_me_and_do: function(){ console.log('Please init me!') },
                google_map_event: function() { /* will override later */ },
                set_has_load: function() { console.log('Please init me!') },
                current_callback: '', /* save current callback at the first time! */
                key: APL.google.apikey.browser /* transfer from server ? - @todo remove later */,
                google_map: function() {},
                loading: function() {},
                event_what_near_by: function() {},
            },
            handle_social_button: function() {},
            handle_see_more_taxonomy: function() {},
            handle_registered_page_content: function() {},
            handle_screen_size: function() {},
            is_mobile: function() {},
            init_autocomplete: function() {},
            send_id_obj: function() {},
            send_selector_ids: function() {},
            clear_form: function() {},
            is_global_page: function() {},
            changeTime: function() {},
            videoFn: {
                'embed': 'input[name="video_embed[]"]',
                'parent' : '.des-list',
                'getFullEmbedSelector': function() {},
                'videoData': [],
                'initLoadVideo': function() {},
                'isYoutubeUrl': function() {},
                'isVimeoUrl': function() {},
                'youTubeRealCode': function() {},
                'getVimeoId': function() {},
                'getThumbByUrl': function() {},
                'getVideoEmbed': function() {},
                'processVideoData': function() {},
                'prcessVimeoData': function() {},

            },
            validateUrl: function() {},
            importEvent: {}
        };

        /**
         * Thienld : Handle js for showing/hiding search widget form on mobile version for all modules
         * */
        $.apl.displayingTopSearchRowOnMobile = function () {
            $('.top-search-row .el-blk').addClass('displayed');
            $('.top-search-row .s-rw').addClass('displayed');
            $('.top-search-row .sub-blk').addClass('displayed');
        };
        
        $.apl.displayArtistTopSearch = function() {
            if($('#search-artist-m-t').length <= 0) return;
            var isDoingSearch = $('.top-search-row input[name="do_search"]');
            var isDoingSearchVal = isDoingSearch.val() == 'yes';
            if(!isDoingSearchVal){
                return;
            }
            var artistType    = $('.top-search-row #artist-type-select');
            var artistTypeVal = artistType.val() == "0" ? '' : artistType.val();
            var artistStyle    = $('.top-search-row #artist-style-select');
            var artistStyleVal = artistStyle.val() == "0" ? '' : artistStyle.val();
            var artistMedium    = $('.top-search-row #artist-type-select');
            var artistMediumVal = artistMedium.val() == "0" ? '' : artistMedium.val();
            var city        = $('.top-search-row #artist-city-select');
            var cityVal     = city.val() == "0" ? '' : city.val();
            var zip        = $('.top-search-row #artist-zip-select');
            var zipVal     = zip.val() == "0" ? '' : zip.val();
            var keyword     = $('.top-search-row input[name="keyword"]');
            var characterBoardActivated = $('.top-search-row .character-board a.active');

            if( !_.isEmpty(artistTypeVal)
                || !_.isEmpty(artistStyleVal)
                || !_.isEmpty(artistMediumVal)
                || !_.isEmpty(cityVal)
                || !_.isEmpty(zipVal)
                || !_.isEmpty(keyword.val())
                || (!_.isEmpty(characterBoardActivated) && characterBoardActivated.length > 0 )
            ){
                $.apl.displayingTopSearchRowOnMobile();
            }
        };

        $.apl.displayBusinessTopSearch = function() {
            if($('#search-business-m-t').length <= 0) return;
            var isDoingSearch = $('.top-search-row input[name="do_search"]');
            var isDoingSearchVal = isDoingSearch.val() == 'yes';
            if(!isDoingSearchVal){
                return;
            }
            var keyword     = $('.top-search-row input[name="keyword"]');
            var bsType    = $('.top-search-row #business-type-select');
            var bsTypePVal = bsType.val() == "0" ? '' : bsType.val();
            var bsServicesChecked = $('.top-search-row #business-service-checkboxes input[type="checkbox"]:checked');

            if( !_.isEmpty(keyword.val())
             || !_.isEmpty(bsTypePVal)
             || (!_.isEmpty(bsServicesChecked) && bsServicesChecked.length > 0 )
            ){
                $.apl.displayingTopSearchRowOnMobile();
            }
        };

        $.apl.displayClassifiedTopSearch = function() {
            if($('#search-classified-m-t').length <= 0) return;
            var isDoingSearch = $('.top-search-row input[name="do_search"]');
            var isDoingSearchVal = isDoingSearch.val() == 'yes';
            if(!isDoingSearchVal){
                return;
            }
            var cfType    = $('.top-search-row #classified-type-select');
            var cfTypeVal = cfType.val() == "0" ? '' : cfType.val();
            var state    = $('.top-search-row #classified-state-select');
            var stateVal = state.val() == "0" ? '' : state.val();
            var city        = $('.top-search-row #classified-city-select');
            var cityVal     = city.val() == "0" ? '' : city.val();
            var keyword     = $('.top-search-row input[name="keyword"]');

            if( !_.isEmpty(cfTypeVal)
                || !_.isEmpty(stateVal)
                || !_.isEmpty(cityVal)
                || !_.isEmpty(keyword.val())
            ){
                $.apl.displayingTopSearchRowOnMobile();
            }
        };

        $.apl.checkEduProgramTypeHaveSelection = function(){
            var hasSelection = false;
            $('.education-pt-select').each(function(index,item){
                if(!_.isEmpty($(item).val()) && $(item).val() != "0"){
                    hasSelection = true;
                    return false;
                }
            });
            return hasSelection;
        };

        $.apl.displayEducationTopSearch = function() {
            if($('#search-education-m-t').length <= 0) return;
            var isDoingSearch = $('.top-search-row input[name="do_search"]');
            var isDoingSearchVal = isDoingSearch.val() == 'yes';
            if(!isDoingSearchVal){
                return;
            }
            var eduPP    = $('.top-search-row #education-pp-select');
            var eduPPVal = eduPP.val() == "0" ? '' : eduPP.val();
            var eduPTCheckingResult = $.apl.checkEduProgramTypeHaveSelection();
            var startDate    = $('.top-search-row .education-s-d');
            var endDate        = $('.top-search-row .education-e-d');
            var eduBilingual    = $('.top-search-row #education-bilingual-checkbox');
            var eduOtherFields = $('.top-search-row #education-other-fields-checkbox input[type="checkbox"]:checked');
            var keyword     = $('.top-search-row input[name="keyword"]');

            if( !_.isEmpty(eduPPVal)
                || eduPTCheckingResult
                || !_.isEmpty(startDate.val())
                || !_.isEmpty(endDate.val())
                || (!_.isEmpty(eduBilingual) && eduBilingual.prop('checked') == true)
                || (!_.isEmpty(eduOtherFields) && eduOtherFields.length > 0 )
                || !_.isEmpty(keyword.val())
            ){
                $.apl.displayingTopSearchRowOnMobile();
            }
        };

        $.apl.displayEventTopSearch = function(){
            if($('#search-event-m-t').length <= 0) return;
            var isDoingSearch = $('.top-search-row input[name="do_search"]');
            var isDoingSearchVal = isDoingSearch.val() == 'yes';
            if(!isDoingSearchVal){
                return;
            }
            var category    = $('.top-search-row #event-category-select');
            var location    = $('.top-search-row #event-location-select');
            var city        = $('.top-search-row #event-city-select');
            var cityVal     = city.val() == "0" ? '' : city.val();
            var region        = $('.top-search-row #event-region-select');
            var regionVal     = region.val() == "0" ? '' : region.val();
            var keyword     = $('.top-search-row input[name="keyword"]');
            var start_date  = $('.top-search-row input[name="start_date"]');
            var end_date    = $('.top-search-row input[name="end_date"]');
            var discount    = $('.top-search-row input[name="is_discount"]');

            if( !_.isEmpty(category.val())
                || !_.isEmpty(location.val())
                || !_.isEmpty(cityVal)
                || !_.isEmpty(regionVal)
                || !_.isEmpty(keyword.val())
                || !_.isEmpty(start_date.val())
                || !_.isEmpty(end_date.val())
                || (typeof discount !== "undefined" && discount.prop('checked') == true)
            ){
                $.apl.displayingTopSearchRowOnMobile();
            }
        };

        $.apl.displayOrganizationTopSearch = function() {
            if($('#search-organization-m-t').length <= 0) return;
            var isDoingSearch = $('.top-search-row input[name="do_search"]');
            var isDoingSearchVal = isDoingSearch.val() == 'yes';
            if(!isDoingSearchVal){
                return;
            }
            var orgType    = $('.top-search-row #org-type-select');
            var orgTypeVal = orgType.val() == "0" ? '' : orgType.val();
            var state    = $('.top-search-row #org-state-select');
            var stateVal = state.val() == "0" ? '' : state.val();
            var city        = $('.top-search-row #org-city-select');
            var cityVal     = city.val() == "0" ? '' : city.val();
            var zip        = $('.top-search-row #org-zip-select');
            var zipVal     = zip.val() == "0" ? '' : zip.val();
            var region        = $('.top-search-row #org-region-select');
            var regionVal     = region.val() == "0" ? '' : region.val();
            var keyword     = $('.top-search-row input[name="keyword"]');

            if( !_.isEmpty(orgTypeVal)
                || !_.isEmpty(stateVal)
                || !_.isEmpty(regionVal)
                || !_.isEmpty(cityVal)
                || !_.isEmpty(zipVal)
                || !_.isEmpty(keyword.val())
            ){
                $.apl.displayingTopSearchRowOnMobile();
            }
        };

        $.apl.displayPublicArtTopSearch = function() {
            if($('#search-public-art-m-t').length <= 0) return;
            var isDoingSearch = $('.top-search-row input[name="do_search"]');
            var isDoingSearchVal = isDoingSearch.val() == 'yes';
            if(!isDoingSearchVal){
                return;
            }
            var paName    = $('.top-search-row #pa-name-select');
            var paNameVal = paName.val() == "0" ? '' : paName.val();
            var paType    = $('.top-search-row #pa-type-select');
            var paTypeVal = paType.val() == "0" ? '' : paType.val();
            var paMedium    = $('.top-search-row #pa-medium-select');
            var paMediumVal = paMedium.val() == "0" ? '' : paMedium.val();
            var paArtist    = $('.top-search-row #pa-artist-select');
            var paArtistVal = paArtist.val() == "0" ? '' : paArtist.val();
            var city        = $('.top-search-row #pa-city-select');
            var cityVal     = city.val() == "0" ? '' : city.val();
            var zip        = $('.top-search-row #pa-zip-select');
            var zipVal     = zip.val() == "0" ? '' : zip.val();
            var paLocation    = $('.top-search-row #pa-location-select');
            var paLocationVal = paLocation.val() == "0" ? '' : paLocation.val();
            var paCollection    = $('.top-search-row #pa-location-select');
            var paCollectionVal = paCollection.val() == "0" ? '' : paCollection.val();
            var keyword     = $('.top-search-row input[name="keyword"]');
            var characterBoardActivated = $('.top-search-row .character-board a.active');


            if( !_.isEmpty(paNameVal)
                || !_.isEmpty(paTypeVal)
                || !_.isEmpty(paMediumVal)
                || !_.isEmpty(paArtistVal)
                || !_.isEmpty(cityVal)
                || !_.isEmpty(zipVal)
                || !_.isEmpty(paLocationVal)
                || !_.isEmpty(paCollectionVal)
                || !_.isEmpty(keyword.val())
                || (!_.isEmpty(characterBoardActivated) && characterBoardActivated.length > 0 )
            ){
                $.apl.displayingTopSearchRowOnMobile();
            }
        };

        $.apl.displayVenueTopSearch = function() {
            if($('#search-venue-m-t').length <= 0) return;
            var isDoingSearch = $('.top-search-row input[name="do_search"]');
            var isDoingSearchVal = isDoingSearch.val() == 'yes';
            if(!isDoingSearchVal){
                return;
            }
            var venueType    = $('.top-search-row #venue-type-select');
            var venueTypeVal = venueType.val() == "0" ? '' : venueType.val();
            var state    = $('.top-search-row #venue-state-select');
            var stateVal = state.val() == "0" ? '' : state.val();
            var city        = $('.top-search-row #venue-city-select');
            var cityVal     = city.val() == "0" ? '' : city.val();
            var zip        = $('.top-search-row #venue-zip-select');
            var zipVal     = zip.val() == "0" ? '' : zip.val();
            var region        = $('.top-search-row #venue-region-select');
            var regionVal     = region.val() == "0" ? '' : region.val();
            var keyword     = $('.top-search-row input[name="keyword"]');
            var accessibility = $('.top-search-row #venue-accessibility-checkbox input[type="checkbox"]:checked');


            if( !_.isEmpty(venueTypeVal)
                || !_.isEmpty(stateVal)
                || !_.isEmpty(regionVal)
                || !_.isEmpty(cityVal)
                || !_.isEmpty(zipVal)
                || !_.isEmpty(keyword.val())
                || (!_.isEmpty(accessibility) && accessibility.length > 0 )
            ){
                $.apl.displayingTopSearchRowOnMobile();
            }
        };
        /**
         * Thienld : End Handle js for showing/hiding search widget form on mobile version for all modules
         * */
        $.apl.validateUrl = function(textval) {
            var urlregex = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
            return urlregex.test(textval);
        };

        $.apl.changeTime = function(hour, minute, ampm) {
            hour = parseInt(hour, 10);
            minute = parseInt(minute, 10);
            ampm  = ampm.toUpperCase();

            if(ampm === 'AM'){
                if(hour === 12) {
                    return '00:'+ ("0" + minute).slice(-2);
                }
                else {
                    return ("0" + hour).slice(-2) + ':' +  ("0" + minute).slice(-2);
                }
            }

            if(ampm === 'PM'){
                if(hour === 12) {
                    return '12:'+ ("0" + minute).slice(-2);
                }
                else {
                    return ("0" + (hour  + 12)).slice(-2) + ':' +  ("0" + minute).slice(-2);
                }
            }
        };
        
        $.apl.videoFn.processVimeoData = function(url, thumbSelector, callback) {
            $.getJSON('http://www.vimeo.com/api/v2/video/' + $.apl.videoFn.getVimeoId(url) + '.json?callback=?', {format: "json"}, function(data) {
                callback({
                    'thumb': data[0].thumbnail_medium,
                    'selector': thumbSelector,
                });
            });
        };

        $.apl.videoFn.initLoadVideo = function() {
            var videoForm = $($.apl.videoFn.getFullEmbedSelector());
            $.each(videoForm, function(i, v) {
                $.apl.videoFn.processVideoData($(v).val())
            });
        };

        $.apl.videoFn.isYoutubeUrl = function(url) {
            var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
            return (url.match(p)) ? RegExp.$1 : false;  
        };

        $.apl.videoFn.isVimeoUrl = function(url) {
            if ( ! url ) return false;
            return url.match(/^.+player.vimeo.com\/(.*\/)?([^#\?]*)/);
        };

        $.apl.videoFn.youTubeRealCode = function(url) {
            var real_code = url ? url.split('watch?v=') : '';
            if ( ! real_code[1] ) {
                real_code = url ? url.split('youtu.be/') : '';
            }
            //embed/Pgehnv-xqAY
            if ( ! real_code[1] ) {
                real_code = url ? url.split('youtube.com/embed/') : '';
            }
            return real_code[1];
        };

        $.apl.videoFn.getVimeoId = function(url) {
            var regExp = /video\/(\d+)/,
                found = url.match(regExp);
            return  found && found[1] != undefined ? found[1] : false;
        };

        $.apl.videoFn.getThumbByUrl = function(url) {
            var thumb;
            $.each($.apl.videoFn.videoData, function( i, v ) {
                if ( v.url == url ) {
                    thumb = v.thumb
                }
            });
            return thumb;
        };

        $.apl.videoFn.getVideoEmbed = function(url) {
            if ( ! url ) return false;
            if ( $.apl.videoFn.isYoutubeUrl(url) ) {
                return 'http://www.youtube.com/embed/'+ $.apl.videoFn.youTubeRealCode(url);
            }
            return url;
        };

        $.apl.videoFn.processVideoData = function(url, _callback) {
            if ( ! url ) return false;

            var existed = false;
            $.each( $.apl.videoFn.videoData, function(i, v) {
                if (v.url == url) {
                    existed = true;
                } 
            } );

            if ( existed ) return true;

            if ( $.apl.videoFn.isYoutubeUrl(url) ) {
                var ytRealcode = $.apl.videoFn.youTubeRealCode(url);
                $.apl.videoFn.videoData.push({
                    'url': url,
                    'thumb': 'http://img.youtube.com/vi/'+ ytRealcode +'/0.jpg"'
                });
            } else if ( $.apl.videoFn.isVimeoUrl(url) ) {
                $.getJSON('http://www.vimeo.com/api/v2/video/' + $.apl.videoFn.getVimeoId(url) + '.json?callback=?', {format: "json"}, function(data) {
                    $.apl.videoFn.videoData.push({
                        'url': url,
                        'thumb': data[0].thumbnail_medium
                    });
                    });
            }
        };

        $.apl.videoFn.getFullEmbedSelector = function() {
            return $.apl.videoFn.parent + ' '+ $.apl.videoFn.embed;
        };

        $.apl.blockUI = {
            message: APL.resource.loading.message,
            css: $.parseJSON(APL.resource.loading.image_style)
        };
        
        $.apl.is_mobile = function() {

            if(typeof sessionStorage !== "undefined") {
                if ( sessionStorage.desktop )
                    return false;
                else if ( localStorage.mobile ) // mobile storage
                    return true;
            }

            // alternative
            var mobile = ['iphone','ipad','android','blackberry','nokia','opera mini','windows mobile','windows phone','iemobile'];
            for ( var i in mobile )
                if (navigator.userAgent.toLowerCase().indexOf(mobile[i]) > 0) return true;

            // nothing found. assume desktop
            return false;
        };

        $.apl.google_map.set_has_load = function() {
            $.apl.google_map.has_load = true;
            eval($.apl.google_map.current_callback)();
        };
        /**/
        $.apl.google_map.load_me_and_do = function(callback) {
            $.apl.google_map.current_callback = callback;
            if(typeof callback !== "string") {
                throw "This callback must be a string";
            }

            if($.apl.google_map.has_load === false) {
                $.apl.google_map.has_load = true;
                $.getScript('https://maps.googleapis.com/maps/api/js?signed_in=false&key=' + APL.google.apikey.browser + '&callback=jQuery.apl.google_map.set_has_load');
            }
            else {
               eval(callback)();
            }
        };

        $.apl.google_map.loading = function($isLoad, containerId) {
            
            if (typeof containerId == 'undefined') {
                containerId = '#public-art-map-container';
            }
            
            $(containerId).block($.apl.blockUI);
            if ($isLoad) {
                $(containerId).block($.apl.blockUI);
            } else {
                $(containerId).unblock($.apl.blockUI);
            }
        };

        $.apl.handle_social_button = function() {
            var social_links = APL.config.social_links,
                fb = $( '.social-top .fb' ),    
                lk = $( '.social-top .lk' ),
                tu = $( '.social-top .tu' ),
                yt = $( '.social-top .yt' ),
                vm = $( '.social-top .vm' ),
                insta = $( '.social-top .insta' );

            if ( ! social_links.lk && ! social_links.fb  && ! social_links.tu && ! social_links.yt && ! social_links.vm && !social_links.insta) {
                $( '.top-head .top-blk.social-top' ).hide();
            } else {
                $( '.top-head .top-blk.social-top' ).show();
            }    

            if ( social_links.lk ) {
                lk.attr( 'href', social_links.lk );
                lk.show();
            } else {
                lk.hide();
            }

            if ( social_links.fb ) {
                fb.attr( 'href', social_links.fb );
                fb.show();
            } else {
                fb.hide();
            }

            if ( social_links.tu ) {
                tu.attr( 'href', social_links.tu );
                tu.show();
            } else {
                tu.hide();
            }

            if ( social_links.yt ) {
                yt.attr( 'href', social_links.yt );
                yt.show();
            } else {
                yt.hide();
            }

            if ( social_links.vm ) {
                vm.attr( 'href', social_links.vm );
                vm.show();
            } else {
                vm.hide();
            }

            /**
             * @ticket #18664: Instagram URL doesn't not display on the front end
             */
            if(insta.length) {
                insta.show();
                if (social_links.insta) {
                    insta.attr('href', social_links.insta);
                }

                var instaLink = insta.attr('href');

                if (!instaLink || instaLink == '#') {
                    insta.hide();
                }
            }
        };

        $.apl.handle_see_more_taxonomy = function() {

            var btn         = $( this ),    
                total       = btn.attr( 'total' ),
                per_page    = btn.attr( 'per-page' ),
                container   = $( '#apollo-taxonomy-container' );

            $.ajax({
                url: APL.ajax_url,
                data: {
                    action:         'apollo_taxonomy_get_more',
                    total:          total,
                    per_page:       per_page,
                    num_displayed:  $('.apollo-taxonomy-row').length,
                    taxonomy:       btn.attr( 'taxonomy' )
                },
                success: function ( res ) {
                    var review_container = 'more-reviews-start-'+ $('.apollo-taxonomy-row').length,
                        result           = eval( "("+ res + ")" );

                    container.append( '<div id="' + review_container + '"></div>' );
                    $( '#'+ review_container ).hide();
                    $( '#'+ review_container ).html( result.html );
                    $( '#'+ review_container ).slideDown( 400 );

                    if ( ! result.display_btn ) {
                        btn.hide();
                    }    
                }
            });
        };

        /**
        * Replace register form by registered infomation after finish register
        * */ 
        $.apl.handle_registered_page_content = function() {

            if ( typeof( apollo_registered_page ) != 'undefined' && apollo_registered_page == 'yes' ) {
                $( 'article' ).html( $( '#apollo-registered-page' ).html() );
            }
        };

        $.apl.handle_screen_size = function() {
            // Fix conflict cs in start.js
//            if(APL.is_front_page === "1") {
//                if ( APL.config.layout.front_page == 'right_sidebar_two_columns' ) {
//                    var device_width = window.innerWidth
//                        || document.documentElement.clientWidth
//                        || document.body.clientWidth;
//                    if ( device_width <= 1024 ) {
//                        $('body').removeClass('grid');
//                    } else {
//                        $('body').addClass('grid');
//                    }
//                }
//            }
        };

        $.apl.__ = function(text) {
            return text; /* @todo Don't translate yet. transfer from server */
        };

        //Tri start check file

        /**
         * Bookmark/unbookmark event/organization/venue
         * @param ajaxurl
         * @param id
         * @param post_type
         */
        ajaxAddBookmark = function(ajaxurl, id, post_type, btn) {
            var _blockScreen = {
                message: APL.resource.loading.message,
                css: jQuery.parseJSON(APL.resource.loading.image_style)
            },
            _btn = $(btn);

            $.ajax({
                type: 'post',
                url: ajaxurl,
                data: {
                    'action': 'add_bookmark',
                    'id': id,
                    'post_type': post_type
                },
                beforeSend: function() {
                    $(window).block(_blockScreen);
                },
                success: function(res) {
                    if (res.action == "not_login") {
                        alert(res.message);
                    } else {

                        if (res.action == "add") {
                            _btn.addClass("bookmark_highlight");
                            _btn.children('span').text( _btn.data().addedText );
                            _btn.attr( 'data-addit', '0' );
                            _btn.attr('title', _btn.data().addedText);
                        } else {
                            _btn.removeClass("bookmark_highlight");
                            _btn.children('span').text( _btn.data().addText );
                            _btn.attr( 'data-addit', '1' );
                            _btn.attr('title', _btn.data().addText);
                        }
                    }
                    $('body').unblock();
                }
            });
        };


        /* LOG FUNC */
        $('body').on('click', '[data-ride="ap-logclick"]', function(e){
            var $this = $(this), data= $this.data();
            var parent = $(e.currentTarget).closest('.apl-link-detail-page');
            if (parent.length > 0) {
                e.stopPropagation();
            }
            $.ajax({
                type: 'GET',
                url: APL.ajax_url,
                data: data,
                success: function(response) {
                    /* For test only */
                    console.log(response);
                }
            });
        });
        /* END - LOG FUNC */

        apollo_submit_form = function( frm, btn ) {

            if ( btn && document.forms[frm].case_action ) {
                document.forms[frm].case_action.value = btn;
            }
            var enable_cap = $('#cptchar_enable').val();
            //login not yet
            if(enable_cap == 1){
                var cpt_val = $('#_captcha').val();
                if(cpt_val == ''){
                    $('#required_err').removeClass('hidden');
                }else{
                    var $form = $('#'+frm);
                    var $elem = $('#_captcha');
                    var _code = $elem.val();
                    //check capt-char here
                    $.ajax({
                        'url': $elem.attr('data-url_check_captcha')+'&code='+_code,
                        'method': 'GET',
                        success: function(response) {
                            /* unblock ui */
                            $form.unblock();
                            /* If error */
                            if(response.error !== false) {
                                $('#captcha_err').removeClass('hidden');
                                $('#required_err').addClass('hidden');
                                $('#_captcha_image').attr('src', response.new_image_link);
                                //$($elem.data('error_holder')+' ._'+v).html('* '+response.msg);
                                //
                                ///* add new image */
                                //$($elem.data('captcha_image')).attr('src', response.new_image_link);
                                //$elem.val("");
                                return true;
                            }
                            else {
                                $('#captcha_err').addClass('hidden');
                                $('#required_err').addClass('hidden');
                                if(typeof window.APOLLO.formEventValidation !== 'undefined'){
                                    window.APOLLO.formEventValidation.handleCurrentFormSubmission(document.forms[frm]);
                                } else {
                                    $(document.forms[frm]).submit();
                                }
                            }
                        },
                        complete: function() {
                            /* unblock ui */
                            $form.unblock();
                        }
                    });
                    //end check capt-char
                }
            }
            else{
                if(typeof window.APOLLO.formEventValidation !== 'undefined'){
                    window.APOLLO.formEventValidation.handleCurrentFormSubmission(document.forms[frm]);
                } else {
                    document.forms[frm].submit();
                }
            }
            //document.forms[frm].submit();

        };

        apollo_is_add_tmp = function ( btn, mod ) {

            $.ajax({
                'url': APL.ajax_url,
                'data': {
                    action: 'apollo_is_add_tmp',
                    btn: btn,
                    mod: mod,
                },
                success: function() {
                    // add and remove class to validate select2 venue field on client side
                    if(mod == 'venue' && btn == 'add'){
                        $('select.custom-validate-venue-field').removeClass('custom-validate-field');
                    }else if(mod == 'venue' && btn == 'back'){
                        $('select.custom-validate-venue-field').addClass('custom-validate-field');
                    }

                    // add and remove class to validate select2 organization field on client side
                    if(mod == 'org' && btn == 'add'){
                        $('select.custom-validate-organization-field').removeClass('custom-validate-field');
                    }else if(mod == 'org' && btn == 'back'){
                        $('select.custom-validate-organization-field').addClass('custom-validate-field');
                    }
                }
            });
        };

        checkCBBefore = function(elm) {

            if ( $(elm).prev().attr('checked') == 'checked' ) {
                $(elm).prev().removeAttr('checked');
            } else {
                $(elm).prev().attr('checked', true);
            }
        };

        /**
         * Call auto complete
         * @param elm
         * @param {string} action Action ajax
         * */
        $.apl.init_autocomplete = function( elm, action ) {
            var obj = $( elm )
            obj.autocomplete({
                source : function (request, response) 
                {     
                    var s = obj.val();
                    $.ajax({
                        url: APL.ajax_url,
                        data: {
                            action: action,
                            s: s,
                        },
                        success: function (data) { 

                            response($.map(eval(data), function(item) {

                                return {
                                    id: item.ID,
                                    value: item.post_title,
                                };
                            }));
                        },
                        error : function (a,b,c) {

                        }
                    });
                },                
                select: function (event, ui) { 
                    console.log(ui.item);
                },
                change: function() {
                    console.log('aaaaa')
                }
            });
        };

        var bind_event_raing = function() {
            var $app_event_rating = $('[data-ride="ap-event_rating"]');
            var length = $app_event_rating.length;
            var uids = [];

            $app_event_rating.each(function (i, v) {
                var $this = $(this),
                    _data = $this.data();

                if($this.attr('binded') === 'true') return true;

                var data = {
                    item_id:  _data.i,
                    item_type: _data.t,
                    item_msg: _data.msg,
                };

                $this.on('click', function() {

                    /* Collect data */
                    data['action'] = 'apollo_update_rating';

                    $.ajax({
                        url: APL.ajax_url,
                        data: data,

                        beforeSend: function() {
                            $(window).block($.apl.blockUI);
                        },
                        success: function(data) {
                            if(data.error === false) {
                                var $_count = $this.find('._count'),  _cc = parseInt($_count.text(), 10);
                                _cc = isNaN(_cc) ? 0 : _cc;
                                $_count.text(_cc + 1);
                            }
                            else {
                                alert(data.data.msg);
                            }
                        },
                        complete: function() {
                            $(window).unblock();

                        }

                    })
                });

                uids.push(_data.i + ':' + _data.t);
                $this.attr('binded', 'true');

                if(i === length-1 ){
                    /* lazy load */
                    data['action'] = 'apollo_get_ratings';

                    /* Action */
                    data['uids'] = uids.join(",");
                    $.ajax({
                        url: APL.ajax_url,
                        data: data,
                        success: function(data) {
                            if(data.error === false) {
                                $.each(data.data.counts, function(i, v) {
                                    $('#_event_rating_' + v.i + '_' + v.t).find('._count').text(v.c);
                                });

                            }
                        }
                    });
                }
            });
        };

        bind_event_raing();

        $('body').on('viewmore', function() {
            bind_event_raing();
        });

        (function() {
            var oalert = window.alert;
            $.apl.oalert = function() {
                oalert.apply(window, arguments);
            };
        })();

        window.alert = function(msg) {
            if (msg == undefined) return;
            alertify.alert(msg);
        };

        $.apl.confirm = function(message, okfn, cancelfn) {
            alertify.confirm(message, function(e) {
                if(e) {
                    okfn();
                }
                else {
                    cancelfn();
                }
            });
        };

        $body.on('click', '[data-type="link"]', function(e) {
            var $this = $(this);
            window.location.href= $this.data('url');
        });

        $body.on('viewmore', function() {
            $window.trigger( 'viewmore');
        });

        /* CLAMP */
        $window.on('load resize custom-clamp',function(e) {
            $('[data-ride="ap-clamp"]').each(function() {
                var $this = $(this), n = $this.data('n');

                var $hiddenItem = $this, $oldItem = $this;
                while($hiddenItem[0].clientHeight === 0) {
                    $oldItem = $hiddenItem;
                    $hiddenItem = $hiddenItem.parent();
                }
                $hiddenItem = $oldItem;

                var previousCss  = $hiddenItem.attr("style");

                $hiddenItem.css({
                    visibility: 'hidden',
                    display:    'block'
                });

                $clamp($this[0], {clamp: n, useNativeClamp: false});

                $hiddenItem.attr("style", previousCss ? previousCss : "");

                $this.data('hasclamp', "true");
            });
        });
        $window.on('viewmore',function(e) {
            $('[data-ride="ap-clamp"]').each(function() {
                var $this = $(this), n = $this.data('n');

                if($this.data("hasclamp")=== "true") return true;

                var $hiddenItem = $this, $oldItem = $this;
                while($hiddenItem[0].clientHeight === 0) {
                    $oldItem = $hiddenItem;
                    $hiddenItem = $hiddenItem.parent();
                }
                $hiddenItem = $oldItem;

                var previousCss  = $hiddenItem.attr("style");
                $hiddenItem.css({
                    visibility: 'hidden',
                    display:    'block'
                });

                $clamp($this[0], {clamp: n, useNativeClamp: false});
                $hiddenItem.attr("style", previousCss ? previousCss : "");

            });
        });
        /* END-CLAMP */

        $.apl.send_id_obj = function() {
            var data = $(this).data();

            if ( ! $( data.source ).val() ) {
                alert(data.msg);
                return;
            };

            $(window).block({
                message: APL.resource.loading.message,
                css: $.parseJSON(APL.resource.loading.image_style)
            });

            var data_form = {
                action: data.action,
                eid: $( data.source ).val(),
                _aplNonceField:$('#_aplNonceField').val()
            };

            //add param 'module' to ajax
            if(data.moduleApply) {
                data_form.module = data.moduleApply
            }

            // add param 'artist id' of multiple mode to ajax
            if (data.artistId) {
                data_form.artistId = data.artistId;
            }

            $.get( 
                APL.ajax_url,
                data_form,
                function(response) {
                    if ( response.status == 'failed') {
                        $(".blk-bm-events").before($('<div class="error ajax-error" style="clear:both;"> ' + $.apl.__('Cannot save data. Some error occurred!') + ' </div>'));
                        setTimeout(function(){
                            $(".ajax-error").remove();
                        }, 3000);
                    } else {
                        window.location.reload();
                    }
                    $(window).unblock();
                }
            );
        };

        $.apl.send_selector_ids = function() {
            var $this = $(this),
                _data = $(this).data(),

                _alist_value = [],
                _key = _data.data.split(':')[0],
                _param = _data.data.split(':')[1];    

            $(_data.data_source_selector).each(function(_,elem) {
                _alist_value.push($(elem).attr(_param));
            });

            if ( ! _alist_value.length ) {
                alert(_data.msg);
                return;
            };

            if ( ! _alist_value.length ) return;

            var data_form = {
                action: _data.action,
                eids: _alist_value.join(','),
                _aplNonceField:$('#_aplNonceField').val()
            };

            //add param 'module' to ajax
            if(_data.moduleApply) {
                data_form.module = _data.moduleApply
            }

            // add param 'artist id' of multiple mode to ajax
            if (_data.artistId) {
                data_form.artistId = _data.artistId;
            }

            $.ajax({
                url: APL.ajax_url,
                data: data_form,
                beforeSend: function() {
                    $(window).block({
                        message: APL.resource.loading.message,
                        css: $.parseJSON(APL.resource.loading.image_style)
                    });
                },
                success: function(res) {
                    if ( res.status == 'failed') {
                        $(".blk-bm-events").before($('<div class="error ajax-error" style="clear:both;"> ' + $.apl.__('Cannot save data. Some error occurred!') + ' </div>'));
                        setTimeout(function(){
                            $(".ajax-error").remove();
                        }, 3000);
                    } else {
                        window.location.reload();
                    }
                },
                complete: function() {
                    $(window).unblock();
                }
            });
        };

        $.apl.clear_form = function () {
            var _data_form = $(this).data();

            $( _data_form.form+ ' input:checkbox' ).attr('checked', false);
            $( _data_form.form+ ' input:text' ).val('');
            $( _data_form.form+ ' select option[selected="selected"]' ).removeAttr('selected');
            $( _data_form.form+ ' select option[selected]' ).removeAttr('selected');
            $( _data_form.form+ ' select' ).val('');
            $( _data_form.form+ ' input:text' ).removeAttr('value');

            if ( $( '#select2-educator-provider-container' ) ) {
                $( '#select2-educator-provider-container' ).text($('#educator-provider option').first().text());
            }

            if ( $('.qualification') ) {
                $('.qualification').removeClass('active');
            }

        };

        $.apl.is_global_page = function() {
            return APL.is_global_page;
        };

        /* CHANGE EDITOR SIZE */
        $window.on('resize', function() {
            var $tools = $( '#wp-post_content-editor-tools'), contentWrapWidth =  $("#wp-post_content-wrap").width();
            $tools.css( {
                position: 'absolute',
                top: 0,
                width: contentWrapWidth
            } );
        });
        /* END */


        /* CLOSE ME PLUGIN */
        $body.on('click', '[data-ride="remove-relation"]', function(e) {
            var $this = $(this), mainwrapper = $this.data('mainwrapper'), target = $this.data('target'), desc = $this.data('confirm'), $target = '', action = $this.data('action');
            if(confirm(desc)) {

                if(typeof $this[target] === 'function' ) {
                    $target = $this[target]();
                }
                else if(typeof $this.find(target) !== 'undefined' && typeof $this.find(target).length !== 'undefined') {
                    $target = $this.closest(target);
                }

                $target.remove();

                if(typeof action !== 'undefined') {

                    var _dataaction = $this.data('action_data'), _arrdataaction = _dataaction.split(','), _postid = $.trim(_arrdataaction[0]), _attachment_id = $.trim(_arrdataaction[1]), _field_name = typeof _arrdataaction[2] !=='undefined' ?  $.trim(_arrdataaction[2]) : '' ;

                    $('body').trigger('apl_remove_doc', {'action_data' : _dataaction});

                    $.ajax({
                        action: 'GET',
                        url: APL.ajax_url + '?action=' + action + '&postid=' + _postid + '&attid=' + _attachment_id + '&field=' + _field_name,
                        success: function(res) {
                            console.log(res);
                        }

                    })
                }

            }

            $body.trigger('remove-relation', {mainwrapper: mainwrapper});

        });
        /* END*/

        /* UPlOAD PLUGIN */
        $body.on('change', '[data-ride="upload_file"]', function() {
            var $this = $(this), maxfilesize = $this.data('maxfilesize');
            if(typeof this.files[0] !== 'undefined' && this.files[0].size > maxfilesize) {
                var alert_text = $this.data('alert');
                alert(alert_text.replace('%s',(maxfilesize / (1024 * 1024)).toFixed(2)));
                this.value= '';
            }

        });
        /* END */

        /* SHADOW MAN */
        $body.on('click', '[data-ride="shadow-man"]', function(e) {
            var $this = $(this),
                maxtimes= $this.data('maxtimes'),
                $mainwrapper = $($this.data('mainwrapper')),
                $template = $mainwrapper.find($this.data('template')+':first'),
                bornname = $this.data('bornname');

            if($template.length <= 0 ) {
                return false;
            }

            if(typeof maxtimes !== 'undefined' && typeof bornname !== 'undefined') {
                if($mainwrapper.find(bornname).length >= maxtimes) {
                    var alert_text = $this.data('alert');
                    alert(alert_text.replace('%s',maxtimes));
                    return false;
                }
            }

            var _append_to = $this.data('append-to'), _selector = _append_to, _method = 'append';
            if(_append_to.indexOf(':') !== '-1') {
                var _arr = _append_to.split(':');
                _selector = _arr[0];
                _method = _arr[1];
            }

            $mainwrapper.find(_selector + ':first')[_method]($template.html());

            $body.trigger('shadown-man', {'mainwrapper': $this.data('mainwrapper')});
        });
        /* END */

        /* Change css for divider */
        jQuery('[data-ride="shadow-man"]').length && (function() {
            jQuery('[data-ride="shadow-man"]').each(function() {
                var $this = $(this), mainwrapper = $this.data('mainwrapper');
                $(mainwrapper + ' .divider-shadow').eq(0).css('display', 'none');

            });
        })();
        $body.on('shadown-man remove-relation', function(e, data) {
            var mainwrapper = data['mainwrapper'];
            $(mainwrapper + ' .divider-shadow').eq(0).css('display', 'none');
        });
        /* END */

        var simpleTemplate = function(tpl, data) {
            var re = /<%([^%>]+)?%>/g, match;
            while(match = re.exec(tpl)) {
                tpl = tpl.replace(match[0], data[match[1]])
            }
            return tpl;
        };

        /* Redirect plugin */
        $body.on('click', '[data-ride="ap-redirect"]', function() {
            var $this = $(this), data= $this.data(), url = data['url'], arrParams = data['params'].split(','), mainParam = data['mainparam'];

            /* get arr value */
            var objData = {};
            $(arrParams).each(function(i, v) {
                var _d = data[v];
                if(typeof _d === 'string' && _d.indexOf('exp') !== -1) {
                    _d = eval(_d.substring(_d.indexOf('exp') + 4));
                }
                objData[v] = _d;
            });

            /* check for eror */
            if(objData[mainParam] === '') {
                alert(data['emptymsg']);
                return false;
            }

            /* replace template */
            url = simpleTemplate(url, objData);

            window.location.href = url;

        });
        /* End */

        /* Plugin Load Asyn */
        $('[data-ride="ap-load_asyn"]').each(function() {
            var $this = $(this), data= $this.data(), url = data['url'], arrParams = data['params'].split(','), methodInsert = data['methodinsert'];

            /* get arr value */
            var objData = {};
            $(arrParams).each(function(i, v) {
                var _d = data[v];
                if(typeof _d === 'string' && _d.indexOf('exp') !== -1) {
                    _d = eval(_d.substring(_d.indexOf('exp') + 4));
                }
                objData[v] = _d;
            });

            /* replace template */
            url = simpleTemplate(url, objData);

            $.ajax({
                url: url,
                method: 'GET',
                success: function(res) {
                    if(methodInsert === 'append') {
                        $this.append(res.html);
                        $body.trigger('load_asyn.'+data['type']);
                    }
                },
                complete: function(a, b) {

                }
            });
        });
        /* End */

    } // End typeof APL != "undefined"
})(jQuery);
