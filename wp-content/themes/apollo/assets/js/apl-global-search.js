ApolloGlobalSearch = function ($) {
 function loadingTotalItems() {
     var globalSearch = $('.apl-loading-total-items');
     if (globalSearch.length > 0) {
         $.ajax({
             url: APL.ajax_url,
             type: "GET",
             data:{
                 action : 'apollo_global_search_get_total_items',
                 s: $(globalSearch).attr('data-search'),
                 currentPostType: globalSearch.find('.selected').attr('data-post-type')
             },

             beforeSend: function () {
                 globalSearch.block($.apl.blockUI);
             },

             success : function (res) {
                 if (res.data) {
                     $.each(res.data, function(index, item) {
                         var currentTab = $(globalSearch).find('#global-search-tab-' + item.post_type);
                         if (currentTab.length > 0 && !currentTab.hasClass('selected') && item.total != 0) {
                             currentTab.find('a').append('<span> ('+ item.total +')</span>')
                         }
                     });
                 }
                 $(globalSearch).unblock();
             }
         });
     }
    }

    var searchGlobalBelowNav = function(){
        var $form1 = $(".apl-place-global-search-below-nav .main-menu .form-search");
        var $form2 = $(".apl-place-global-search-below-nav .mobile-menu .form-search");
        var $click = $(".nav-global-search .fa");
        var $searchFooter = $('.menu-footer ul li.search-footer a');
        $click.off("click").on("click", function(e){
            $form1.toggleClass("open");
            $form2.toggleClass("open");
            $form1.find(".solr-search").focus();
            $form2.find(".solr-search").focus();
        });

        $searchFooter.on("click", function(e){
            e.stopPropagation();
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
            setTimeout(function() {
                $form1.addClass("open");
                $form2.addClass("open");
                $form1.find(".solr-search").focus();
                $form2.find(".solr-search").focus();
            }, 701);
        })

        $(document).bind('click', function (e) {
            e.stopPropagation();
            if (!$click.is(e.target) && $form1.has(e.target).length === 0 && $form2.has(e.target).length === 0 && !$searchFooter.is(e.target) ) {
                $form1.removeClass("open");
                $form2.removeClass("open");
            }
        });
    };


 return {
        init: function () {
            loadingTotalItems();
            searchGlobalBelowNav();
        }
    };

 }(jQuery);

jQuery(document).ready(function() {
    ApolloGlobalSearch.init();
});
