(function ($) {
    window.APOLLO =  window.APOLLO || {};
    window.APOLLO.CommonFunction = (function(){
        var module = {};
        module.init = function(){
            if(typeof _ !== "undefined"){
                _.mixin({
                    remove: function(obj, key){
                        delete obj[key];
                        return obj;
                    }
                });
            }

            $(document).ready( function() {

            });
            $(window).load(function() {

            });
            return module;
        };

        /**
         *
         * Handle url
         * url to array
         * array to url
         * */
        module.uRLToArray = function(url) {
            var request = {};
            var pairs = url.substring(url.indexOf('?') + 1).split('&');
            for (var i = 0; i < pairs.length; i++) {
                if(!pairs[i])
                    continue;
                var pair = pairs[i].split('=');
                request[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
            }
            return request;
        };

        module.arrayToURL = function(array) {
            var pairs = [];
            for (var key in array){
                if (array.hasOwnProperty(key)){
                    var val = encodeURIComponent(array[key]) !== "undefined" ? encodeURIComponent(array[key]) : '';
                    pairs.push(encodeURIComponent(key) + '=' + val);
                }
            }
            return pairs.join('&');
        };

        return module.init();
    })();
})(jQuery);