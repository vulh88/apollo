/**
 * Handle in all page, keep all function in $.apl
 * @author elinext
 */
// tiny helper function to add breakpoints
function getGridSize() {
    // return (window.innerWidth < 480) ? 1 :
    //        (window.innerWidth > 480) ? 3 : 4 ;
    var nbItem ;
    if ( window.innerWidth < 480 ) {
        return nbItem = 1;
    } else if ( (window.innerWidth > 480) && (window.innerWidth <= 736) ) {
        return nbItem = 2;
    } else if ( (window.innerWidth > 736 ) && (window.innerWidth < 1024 ) ) {
        return nbItem = 3;
    } else {
        return nbItem = 4;
    }
}

(function( $ ) {

    var conf = {
        event : "click",
        effect : "fadeIn"
    };

    /**
     * Delete event function in FE
     * */
    $('.account-listing').on('click', '.apl-delete-event', function(e) {
        var $this = $(this);
        e.preventDefault();
        $.apl.confirm($this.attr('data-msg'), function() {
            $this.closest('form').submit();
        },
        function() { });
    });


    $(function() {


        $('.ico-s.apl-start-icon').click(function() {
            $('#top-c-s+.ui-datepicker-trigger').trigger('click');
        });

        $('.ico-e.apl-start-icon').click(function() {
            $('#top-c-e+.ui-datepicker-trigger').trigger('click');
        });

        /**
         * Auto pre-select request organization from URL slug
         * ticket: #11077
         */
        $('body').on('add-more-co', function(event, res) {
            if (res.id == '#org-select1') {
                var requestOrgId = $('#add-co').data('requestOrg');
                if ( requestOrgId ) {
                    $(res.id).find('option[value="' + requestOrgId + '"]').attr('selected', true);
                }
            }
        });

        /**
         * Delete classified function in FE
         * */
        $('.classified-listing-form').on('click', '.delete-classified', function(e) {
            var $this = $(this);
            e.preventDefault();
            $.apl.confirm($this.attr('data-msg'), function() {
                    $this.closest('form').submit();
                },
                function() { });
        });

        /**
         * #14833 [CF] 20180122 - Performance issue - Apply cache for Primary navigation
         * @author hieulc
         */
        var activatePrimaryNavHandler = function () {
            $('.header .mn-menu > ul > li').removeClass('cur');
            var first_lv_mn_item = $('.header .mn-menu > ul > li > a[href="' + document.URL + '"]');
            if (first_lv_mn_item.length > 0) {
                first_lv_mn_item.parent().addClass('cur');
            } else {
                $('.header .mn-menu ul.sub-menu a[href="' + document.URL + '"]').closest('li[data-lvl="level-1"]').addClass('cur');
            }
        };

        if(APL.config.enable_cache === 'undefined' || APL.config.enable_cache) {
            activatePrimaryNavHandler();
        }

        /**
         * ThienLD : update "additional time" content when user add/edit event at step 2b
         */
        $("#next-to-step3").on('click', function(e){
            var dates = $('.date-week .date-cell:not(.disable)'),
                error = false;

            if (dates.length == 0) {
                dates = $('.cl-date .date-cell:not(.disable)');
            }

            $.each(dates, function(i, v) {
                if (!$(v).find('.cl-event-list li').length) {
                    $(v).addClass('cell-error');
                    error = true;
                }
            });
            if (error) {
                alert($(this).data().alert);
            } else {
                var redirectedLink = $(this).data().href;
                fncUpdateAdditionalTimeContent(false,function(){
                    window.location.href = redirectedLink;
                    return false;
                });
            }
        });

        $("#apl-fe-save-draft-step2").on('click', function(e){
            fncUpdateAdditionalTimeContent(true,function(){
                $('#apl-event-save-date-draft').trigger('click');
            });
        });

        var fncUpdateAdditionalTimeContent = function (saveDraft, callback) {
            var _blockScreen = {
                message: APL.resource.loading.message,
                css: jQuery.parseJSON(APL.resource.loading.image_style)
            }
            var eventId = $('#event_id').val();
            var content = '';
            // if the WordPress editor is in "Visual" mode
            if ( $('#wp-additional_time-wrap').hasClass("tmce-active") ) {
                content = tinyMCE.activeEditor.getContent();
            } else {
                // if the WordPress editor is in "Text" mode
                content = $('#additional_time').val();
            }
            $.ajax({
                url: APL.ajax_url,
                type: "POST",
                dataType: "JSON",
                data: {
                    'action'  : 'apollo_update_additional_time_content_in_step2b',
                    'event_id': eventId,
                    'content' : content,
                    '_aplAddEventNonceField' : $("#_aplAddEventNonceField").val(),
                    'apl-fe-save-draft' : saveDraft ? 1 : 0
                },
                beforeSend: function () {
                    $(window).block(_blockScreen);
                },
                success : function (res) {
                    $('body').unblock();
                    if(!_.isEmpty(res) && res.success === "TRUE"){
                        callback();
                    } else {
                        alert(res.error);
                    }
                }
            });
            return false;
        };

        var editors = $('textarea.wp-editor-area');
        $.each(editors, function(i, v) {
            if ($(v).hasClass('inp-error')) {
                $(v).closest('.wp-editor-container').addClass('editor-error');
            }
        });

        // Reset tab
        $('.dsb-main-nav > ul > li').click(function() {
            localStorage.removeItem('activeTab');
        });
        // handle active current menu item in  two-tier menu
        if ( $("#main_nav li ul").find(".selected").length){
            $("#main_nav li ul").find(".selected").first().parent().parent().addClass('selected')
            $('#main_nav > li.selected ').addClass('old-actived');
        }else {
            if ( !$("#main_nav").find(".selected").length){
                $("#main_nav >li").first().addClass('selected');
                $('#main_nav > li.selected ').addClass('old-actived');
            }
        }
        $(".main-menu.tablet-show ul#main_nav-table").addClass('nav');
       $(".main-menu.tablet-show ul#main_nav-table  ul#main_nav-table").each(function() {
           var depth = $(this).parents('ul').length;
           if(depth == 1 ){
               $(this).addClass('sub-menu level-1');
           }
           if(depth == 2 ){
               $(this).removeClass('menu-hover');
               $(this).addClass('sub-menu level-1');
           }

        });



        // Active current tab
        $('.tab-list li a').click(function() {
            localStorage.setItem('activeTab', $(this).attr('data-id'));
        });
        $('.tab-list li').removeClass('selected');
        var active = $('.tab-list li a[data-id='+localStorage.getItem('activeTab')+']').closest('li');
        if (!active.length) {
            active = $('.tab-list li:first-child');
        }
        active.trigger('click');

    })


    $("img.wp-post-image").lazyload(conf);
    $("img.lazy").lazyload(conf);

    /**
     * Remove some button on device
     **/
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        $( '.pt.print, .bg-print' ).hide();
        $( '.pt.email, .bg-mail' ).hide();

        $( 'button[data-ride="ap-tellfriendbylink"]' ).hide();
    }else{
        $( '.pt.print, .bg-print' ).removeClass('hidden');
        $( '.pt.email,  .bg-mail' ).removeClass('hidden');
    }
    /**
     * FIx layout of slider on mobile
     */
    $(window).load(function () {
        if (APL.current_activated_theme !== APL.apl_sonoma_dir_theme_name ){
            $(".flex-next").text('SlideNext');
        }
    });

    /**
     * Override Input file role
     * */
    $('.upload_pdf-wrapper').on('click', '[data-role="file"]', function() {
        $(this).prev().trigger('click');
        var inputText = $(this).next();
        $(this).prev().change(function() {
            inputText.val($(this).val());
        });
    });
    /* End Override Input file role  */

    /* Function define */
    /**
     * [runCalendarIpt description]
     * @param  {[type]} tgt [description]
     * @return {[type]}     [description]
     */
    function runCalendarIpt (tgt) {
        'use strict';
        if (!$('#' + tgt).length) {return;}
        /*$('#' + tgt).datepicker({
         dateFormat: 'yy-mm-dd',
         nextText: '&#187;',
         prevText: '&#171;'
         });*/

        if((typeof APL) != 'undefined'){
            var currentLang = APL.currentLang != null ? APL.currentLang : '';
        }else{
            var currentLang="";
        }

        if (!$('#' + tgt).length) {return;}

        var options = $.extend({}, // empty object
            $.datepicker.regional[currentLang], {
                dateFormat: 'yy-mm-dd',
                nextText: '&#187;',
                prevText: '&#171;',
            }
        );

        $( '#' + tgt ).datepicker(options);


    }

    // Make a Responsive 100% Width YouTube iFrame Embed
    $("iframe").closest("p").css({"padding-bottom": "56.25%","position" : "relative", "width" : "100%", "height" :"0"});

    // Not allow click on Primary Image if the Gallery photos is disabled on event form
    if ($('form#admin-frm-step1 .nav-tab .tab-image-list li').length == 1) {
        $('form#admin-frm-step1 .nav-tab .tab-image-list li').addClass('avoid-clicks');
    }

    // slide accessibilyty area in Venue Page
    $('.accessibilyty-icons-search dl dt').click(function() {
        $('.accessibilyty-icons-search dl dd ul').slideToggle();
    });

    $('.top-search-row input[name="start_date"]').change(function() {
       if (! $('.top-search-row input[name="end_date"]').val()){
           $('.top-search-row  input[name="end_date"]').val($(this).val());
       }
    });

    $(document).mouseup(function (e)
    {
        var container = $(".accessibilyty-icons-search");

        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            $('.accessibilyty-icons-search dl dd ul').hide();
        }
    });

    $('.add-new-state-btn').click(function() {
        $('.state-block-1').toggleClass('hidden');
        $('.state-block-2').toggleClass('hidden');
        $('.state-block-2').find('input').val('');
        if ($('.state-block-1').hasClass('hidden')) {
            $('input[name="add_new_state"]').val(1);
            $(this).text($(this).data().next);
        } else {
            $('input[name="add_new_state"]').val(0);
            $(this).text($(this).data().prev);
        }

    });

    // Auto check parent when select the child
    $('ul.cat-listing li ul li').on('click', 'input[type=checkbox]', function() {
        if ($('#_apl_event_term_primary').length && $('#_apl_event_term_primary').val() == "") return;
        var parent = $(this).closest('ul').closest('li').children('input[type=checkbox]'),
            otherChilds = parent.closest('li').children('ul').find('input:checked');
        if (parent.attr('disabled') != 'disabled' && $(this).attr('checked') != undefined) {
            parent.attr('checked', 'checked');
        } else if (otherChilds.length == 0) {
            parent.removeAttr('checked');
        }
    });

    $('#artist-list').off('click', '.artist-checked').on('click', '.artist-checked', function(e){
        var current = $(e.currentTarget);
        var parent = current.closest('#artist-list');
        var checkedEle = parent.find('#apl-checked-list-art');
        var uncheckedEle = parent.find('#apl-unchecked-list-art');
        var listChecked = checkedEle.val();
        var listUnchecked = uncheckedEle.val();
        listChecked = listChecked != '' ? listChecked.split(',') : [];
        listUnchecked = listUnchecked != '' ? listUnchecked.split(',') : [];
        var currentValue = current.val();
        if (current.is(':checked')) {
            listChecked.push(currentValue);
            if (listUnchecked.length > 0) {
                var indexUnchecked = listUnchecked.indexOf(currentValue);
                if(indexUnchecked > -1){
                    listUnchecked.splice(indexUnchecked, 1);
                }
            }
        } else {
            listUnchecked.push(currentValue);
            if (listChecked.length > 0) {
                var indexChecked = listChecked.indexOf(currentValue);
                if (indexChecked > -1) {
                    listChecked.splice(indexChecked, 1);
                }
            }
        }
        checkedEle.val(listChecked.length > 0 ? listChecked.join(',') : '');
        uncheckedEle.val(listUnchecked.length > 0 ? listUnchecked.join(',') : '');
    });
    $('#event-artist-load-more').off('click').on('click', function(e){
        e.preventDefault();
        var current = $(e.currentTarget);
        var parent = current.closest('#artist-list');
        var listArtist = parent.find('#apollo-artist-list-in-frm li');
        var postID = parent.attr('post_id');
        var inputSearch = $('#search-artists-for-event-frontend');
        $.ajax({
            url: APL.ajax_url,
            type: 'GET',
            dataType: 'JSON',
            data:{
                action : 'apollo_get_more_artists_frontend',
                post_id: postID,
                text_search: inputSearch.val(),
                offset: listArtist.length,
                module: 'event'
            },
            beforeSend: function () {
                $('#apollo-artist-list-in-frm').block(jQuery.apl.blockUI);
            },
            success: function (response) {
                $("#apollo-artist-list-in-frm").append(response.html);
                if(response.have_more) {
                    $(".wrap-load-more-artist-for-event").removeClass('hidden');
                } else {
                    $(".wrap-load-more-artist-for-event").addClass('hidden');
                }
                // if ($(listArtist).length == 0 && typeof response.artist_selected !== 'undefined') {
                //     $('#artist-list #apl-checked-list-art').val(response.artist_selected);
                // }
                $('#apollo-artist-list-in-frm').unblock();
            }
        });
    });
    $('.group-search').off('click', '#remove-search-text').on('click', '#remove-search-text', function(e) {
        var inputText = $('#search-artists-for-event-frontend');
        inputText.val('');
        $(e.currentTarget).addClass('hidden');
        inputText.keyup();
    });
    $('.group-search').off('keyup', '#search-artists-for-event-frontend').on('keyup', '#search-artists-for-event-frontend', function(e){
        e.preventDefault();
        var current = $(e.currentTarget);
        var iconRemove = current.closest('.group-search').find('#remove-search-text');
        if(iconRemove.length > 0){
            if(current.val() != ''){
                if(iconRemove.hasClass('hidden')){
                    iconRemove.removeClass('hidden');
                }
            }else{
                if(!iconRemove.hasClass('hidden')){
                    iconRemove.addClass('hidden');
                }
            }
            delaySearch(function(){
                handle_search_artist(e);
            }, 500);
        }
    });

    var delaySearch = (function(){
        var timer = 0;
        return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();

    function handle_search_artist(e){
        e.preventDefault();
        var inputSearch = $(e.currentTarget);
        if(inputSearch.length > 0 ){
            if(inputSearch.hasClass('remove-search') && inputSearch.val() != ''){
                inputSearch.val('');
                if(!current.hasClass('hidden')){
                    current.addClass('hidden');
                }
            }
            var artistElement = inputSearch.closest('#artist-list');
            if(artistElement.length > 0){
                var postID = artistElement.attr('post_id');

                $.ajax({
                    url: APL.ajax_url,
                    type: 'GET',
                    dataType: 'JSON',
                    data:{
                        action : 'apollo_get_more_artists_frontend',
                        post_id: postID,
                        text_search: inputSearch.val(),
                        module: 'event'
                    },
                    beforeSend: function () {
                        $('#artist-list').block(jQuery.apl.blockUI);
                    },
                    success: function (response) {
                        if(inputSearch) {
                            $("#apollo-artist-list-in-frm").html(response.html);
                        } else {
                            $("#apollo-artist-list-in-frm").append(response.html);
                        }
                        if(response.have_more) {
                            $(".wrap-load-more-artist-for-event").removeClass('hidden');
                        } else {
                            $(".wrap-load-more-artist-for-event").addClass('hidden');
                        }

                        $('#artist-list').unblock();
                    }
                });
            }
        }
    }

    $('.apl-territory-state').on('change', function() {
        var $this = $(this);
        var terrData = APL.terrData;
        zipData = [];
        //cities
        try {
            if($this.val() != 0){
                var cityData = terrData[$this.val()];
                $.each(cityData, function(i1, v1) {
                    $.each(v1, function(i2, v2) {
                        zipData.push(i2);
                    })
                })
            } else {
                $.each(terrData, function(i, v) {
                    if(v && v !== "null" && v!== "undefined"){
                        cityData = $.extend(cityData,v);
                        $.each(v, function(i1, v1) {
                            $.each(v1, function(i2, v2) {
                                zipData.push(i2);
                            })
                        })
                    }
                })
            }
        } catch(e) {
            cityData = '';
        }

        if($('.apl-territory-city').length){
            var opts = $('.apl-territory-city option');
            var html = "<option value=0>"+opts.first().html()+"</option>";

            if (cityData) {

                var cityDataArr = $.map(cityData, function(value, index) {
                    return [index];
                });

                cityDataArr.sort(function(a, b) {
                    if(a < b) return -1;
                    if(a > b) return 1;
                    return 0;
                });

                $.each(cityDataArr, function(i, v) {
                    var value = typeof i == 'number' ? v : i;
                    html += '<option value="'+value+'">'+value+'</option>';
                });
            }
            $('.apl-territory-city').html(html);
            $('.apl-territory-city').next().find('.select2-selection__rendered').html(opts.first().html())
        }


        if($('.apl-territory-zipcode').length > 0){
            var opts2 = $('.apl-territory-zipcode option');
            var html2 = "<option value=0>"+opts2.first().html()+"</option>";

            if (zipData) {
                zipData.sort(function(a, b) {
                    if(a < b) return -1;
                    if(a > b) return 1;
                    return 0;
                });
                $.each(zipData, function(i, v) {
                    html2 += '<option value="'+v+'">'+v+'</option>';
                });
                $('.apl-territory-zipcode').html(html2);
                $('.apl-territory-zipcode').next().find('.select2-selection__rendered').html(opts2.first().html());
            }
        }

        /** @Ticket #13874 */
        var neighborhood = $('.apl-territory-neighborhood');
        var wrapNeighborhood = neighborhood.closest('.neighborhood-box');
        neighborhood.empty();
        if(!wrapNeighborhood.hasClass('hidden')) {
            wrapNeighborhood.addClass('hidden');
        }
        
    });

    $('.apl-territory-city').on('change', function() {
        var $this = $(this);
        var terrData = APL.terrData;
        var selectedState = 0;
        var selectedCity = $this.val();
        var stateData = [];
        var cityData = '';
        var zipData = '';
        var done = false;
        var state = $('.apl-territory-state').val();
        try {

            if(selectedCity != 0){
                if ( state != 0&& typeof state !== "undefined" && !$this.hasClass('is-not-member')) {
                    //case up to down
                    var zipData = terrData[state][selectedCity] ;
                    showNeighborhood(selectedCity, state);
                }
                else {
                    $.each(terrData, function(i, v) {
                        if(v && v !== "null" && v!== "undefined"){
                            stateData.push(i);
                            if(!done) {
                                $.each(v, function (i1, v1) {
                                    if (i1 === selectedCity) {
                                        selectedState = i;
                                        zipData = v1;
                                        cityData = v;
                                        done = true;
                                        return false;//get first state appropriate and break
                                    }
                                })
                            }
                        }
                    })
                    showNeighborhood(selectedCity, selectedState);
                }
            } else {
                showNeighborhood(0, 0);
                if (!$this.hasClass('is-not-member')) {
                    $('.apl-territory-state').trigger('change');
                }
                return;
            }

        } catch(e){
            zipData = '';
        }
        if ( $('.apl-territory-state').val() == 0 ) {
            if (stateData.length > 0) {
                stateData.sort = function(a, b) {
                    if(a < b) return -1;
                    if(a > b) return 1;
                    return 0;
                };
                var opts = $('.apl-territory-state option');
                var html = "<option value=0>"+opts.first().html()+"</option>";
                var SelectedText = opts.first().html();
                $.each(stateData, function(i, v) {
                    var selected = '';
                    if(v == selectedState){
                        selected = 'selected="selected"';
                        SelectedText = v ;
                    }
                    html += '<option value="'+v+'"'+selected+'>'+v+'</option>';
                });
                $('.apl-territory-state').html(html);
                $('.apl-territory-state').next().find('.select2-selection__rendered').html(SelectedText);
            }
        }
        
        /** Do not render cities when artist is not a member */
        if ($this.hasClass('is-not-member')) {
            /** Only change state value */
            $('.apl-territory-state').val(selectedState);
        } else {
            if (cityData) {
                var opts1 = $('.apl-territory-city option');
                var html1 = "<option value=0>"+opts1.first().html()+"</option>";
                var SelectedText = opts1.first().html();

                var cityDataArr = $.map(cityData, function(value, index) {
                    return [index];
                });

                cityDataArr.sort(function(a, b) {
                    if(a < b) return -1;
                    if(a > b) return 1;
                    return 0;
                });

                $.each(cityDataArr, function(i, v) {
                    var selected = v == selectedCity ? 'selected="selected"' : '';
                    html1 += '<option value="'+v+'"'+selected+'>'+v+'</option>';
                    SelectedText = v ;
                });
                $('.apl-territory-city').html(html1);
                $('.apl-territory-city').next().find('.select2-selection__rendered').html(SelectedText);
            }
        }

        

        if($('.apl-territory-zipcode').length) {
            var opts2 = $('.apl-territory-zipcode option');
            var html2 = "<option value=0>" + opts2.first().html() + "</option>";

            if (zipData) {

                var zipDataArr = $.map(zipData, function(value, index) {
                    return [index];
                });

                zipDataArr.sort(function(a, b) {
                    if(a < b) return -1;
                    if(a > b) return 1;
                    return 0;
                });
                $.each(zipDataArr, function (i, v) {
                    html2 += '<option value="' + v + '">' + v + '</option>';
                });
            }

            $('.apl-territory-zipcode').html(html2);
            $('.apl-territory-zipcode').next().find('.select2-selection__rendered').html(opts2.first().html());
        }
    });

    var showNeighborhood= function (city, state) {
        /** @Ticket #13874 */
        var neighborhood = $('.apl-territory-neighborhood');
        if( neighborhood.length > 0 ) {
            var wrapNeighborhood = neighborhood.closest('.neighborhood-box');
            if(city == 0) {
                if(!wrapNeighborhood.hasClass('hidden')) {
                    wrapNeighborhood.addClass('hidden');
                }
                neighborhood.empty();
            } else {
                $.ajax({
                    url: APL.ajax_url,
                    method: 'post',
                    dataType: 'json',
                    data:{
                        action : 'apollo_get_neighborhood_by_city',
                        parent_code: city,
                        state: state
                    },
                    beforeSend: function () {
                        $(window).block($.apl.blockUI);
                    },
                    success: function (response) {
                        if (response.status) {
                            if(response.data != '') {
                                if(wrapNeighborhood.hasClass('hidden')) {
                                    wrapNeighborhood.removeClass('hidden');
                                }
                            } else {
                                if(!wrapNeighborhood.hasClass('hidden')) {
                                    wrapNeighborhood.addClass('hidden');
                                }
                            }
                            neighborhood.empty();
                            neighborhood.append(response.data);
                            var neighborhoodOptions = neighborhood.find('option');
                            neighborhood.closest('.neighborhood-box').find('.select2-selection__rendered').html(neighborhoodOptions.first().html())
                        } else {
                            neighborhood.empty();
                        }
                        $(window).unblock();
                    }
                });
            }
        }
    };

    $('.apl-territory-zipcode').on('change', function() {
        if ( $('.apl-territory-city').val() == 0 ){
            var $this = $(this);
            var terrData = APL.terrData;
            var selectedState = 0;
            var selectedCity = 0;
            var selectedZip = $this.val();
            var stateData = [];
            var cityData = '';
            var zipData = '';
            var done = false;
            try {
                if($this.val() != 0){
                    $.each(terrData, function(i, v) {
                        if(v && v !== "null" && v!== "undefined"){
                            stateData.push(i);
                            $.each(v, function(i1, v1) {
                                $.each(v1, function(i2, v2) {
                                    if(i2  == selectedZip){
                                        cityData = v;
                                        zipData = v1;
                                        selectedState = i;
                                        selectedCity = i1;
                                        done = true;
                                        return false;
                                    }
                                })
                                if(done == true) return false;
                            })
                        }
                    })

                }
            } catch(e){
                zipData = '';
            }

            if (stateData.length > 0) {

                stateData.sort = function(a, b) {
                    if(a < b) return -1;
                    if(a > b) return 1;
                    return 0;
                };

                var opts = $('.apl-territory-state option');
                var html = "<option value=0>"+opts.first().html()+"</option>";
                var SelectedText = opts.first().html();
                $.each(stateData, function(i, v) {
                    var selected = '';
                    if(v == selectedState){
                        selected = 'selected="selected"';
                        SelectedText = v ;
                    }
                    html += '<option value="'+v+'"'+selected+'>'+v+'</option>';

                });
                $('.apl-territory-state').html(html);
                $('.apl-territory-state').next().find('.select2-selection__rendered').html(SelectedText);
            }

            if (cityData) {

                var cityDataArr = $.map(cityData, function(value, index) {
                    return [index];
                });

                cityDataArr.sort(function(a, b) {
                    if(a < b) return -1;
                    if(a > b) return 1;
                    return 0;
                });

                var opts1 = $('.apl-territory-city option');
                var html1 = "<option value=0>"+opts1.first().html()+"</option>";
                var SelectedText = opts1.first().html();
                $.each(cityDataArr, function(i, v) {
                    var selected = '';
                    if(v == selectedCity){
                        selected = 'selected="selected"';
                        SelectedText = v ;
                    }
                    html1 += '<option value="'+v+'"'+selected+'>'+v+'</option>';
                });
                $('.apl-territory-city').html(html1);
                $('.apl-territory-city').next().find('.select2-selection__rendered').html(SelectedText);

                /** @Ticket #14181 */
                showNeighborhood(selectedCity, selectedState);
            }


            if (zipData) {
                var opts2 = $('.apl-territory-zipcode option');
                var html2 = "<option value=0>"+opts2.first().html()+"</option>";
                var SelectedText = opts2.first().html();

                var zipDataArr = $.map(zipData, function(value, index) {
                    return [index];
                });

                zipDataArr.sort(function(a, b) {
                    if(a < b) return -1;
                    if(a > b) return 1;
                    return 0;
                });

                $.each(zipDataArr, function(i, v) {
                    var selected = '';
                    if(v == selectedZip){
                        selected = 'selected="selected"';
                        SelectedText = v ;
                    }
                    html2 += '<option value="'+v+'"'+selected+'>'+v+'</option>';
                });
            }
            $('.apl-territory-zipcode').html(html2);
            $('.apl-territory-zipcode').next().find('.select2-selection__rendered').html(SelectedText);
        }

    });

    $('.clear-edu-search-form').click(function(){
        var frm = $('.search-education');
        frm.find("select").each(function(){
            $(this).val($(this).children('option:first-child').val()).trigger('change');
        });
        frm.find('input').val('');
    });


    /**
     * Hide the my list button
     * */
    var hideBtnGroup = function(elm) {
        console.log(elm);
        var target = elm.data().id;
        if ($('[data-target='+target+'] nav li ').length > 0) {
            $('.b-btn.my-account-listing').show();
        } else {
            $('.b-btn.my-account-listing').hide();
        }
    };

    if ( $('.my-account-listing').length ) {

        $('.wc-l.account-listing ul.tab-list li').click(function() {
            hideBtnGroup($(this).find('a'));
        });
    }
    /*End handle my list button*/

    /*cut p tag empty in detail blog*/
    var blog_content_p = $('.blog-content .blog-text-detail p')
    $.each(blog_content_p, function(i,v) {if($(v).html() == '') $(v).hide()})
    /*end cut p tag empty in detail blog*/

    if ( typeof APL != "undefined" ) {
        $(function() {

            var addEventTag = function(tagVal) {

                var selectedTags = jQuery( '.event-tags .tags-list input'),
                    shouldAdd = true;

                $.each( selectedTags, function(i, v) {
                    if ( tagVal == $(v).val() ) {
                        shouldAdd = false;
                        return;
                    }
                });

                if (shouldAdd) {
                    $('.event-tags .tags-list').append('<span><i class="fa fa-times"></i>'+tagVal+'<input name="tags[]" type="hidden" value="'+tagVal+'" /></span>');
                } else {
                    $('.event-tags [name="tag"]').focus();
                }
            };
            // Add event tags
            $('.event-tags').on('click', '[name="tag-btn"]', function() {
               if ($(this).prev('[name="tag"]').val()) {
                   var tagVal = $(this).prev('[name="tag"]').val();
                   addEventTag(tagVal);
                   $(this).prev('[name="tag"]').val('');
               }
            });

            $('.event-tags .tags-choice').on('click', 'a', function() {
                addEventTag($(this).text());
            });

            $('.event-tags .tags-list').on('click', 'i', function() {
               $(this).closest('span').remove();
            });

            $('.event-tags').on('keyup', '[name="tag"]', function(e) {
                if (e.keyCode == 13) {
                   addEventTag($(this).val());
                   $(this).val('');
                }
            });

            $('.event-tags').on('click', '.choice-from', function(e) {
                e.preventDefault();
                $('.event-tags .tags-choice').toggleClass('hidden');
            });

            if ( APL.need_captcha ) {
                $.ajax({
                    url: APL.ajax_url,
                    data: {
                        action: 'apollo_init_captcha'
                    },
                    beforeSend: function() {

                    },
                    success: function(res) {

                        if ($('#_captcha_image').length) {
                            $('#_captcha_image').attr('src', res.src);
                        }
                    }
                });
            }

            /**
             * Hidden show more on home featured events
             * */
            if ( $('.wrapper-l .item-cat.vertical').first().find('.fea-evt-item.vertical').length <= 3
                    && $('.wrapper-l .item-cat.vertical').last().find('.fea-evt-item.vertical').length <= 3) {
                $('#evt-top-blk').closest('.show-more').hide();
            }

            if ( $('.wrapper-r .item-cat.vertical').first().find('.fea-evt-item.vertical').length <= 3
                    && $('.wrapper-r .item-cat.vertical').last().find('.fea-evt-item.vertical').length <= 3) {
                $('#evt-bottom-blk').closest('.show-more').hide();
            }



            $('.submit-btn').click(function() {
                $(this).css('pointer-events', 'none');
            });

            // Hide empty form element
            var _frmContainers = $('.artist-blk');
            $.each(_frmContainers, function(i, v) {
                if ( ! $(v).html() ) {
                    $(v).hide();
                }
            });

            // Load init video data to get thumbnails
            $.apl.videoFn.initLoadVideo();

            var handleRadioFn = function(radioObj) {
                if (radioObj.val()=='other_choice') {
                    radioObj.closest('.el-blk').find('.other-choice-ipt').show();
                    radioObj.closest('.el-blk').find('.other-choice-hidden').val(1);
                } else {
                    radioObj.closest('.el-blk').find('.other-choice-ipt').hide();
                    radioObj.closest('.el-blk').find('.other-choice-hidden').val(0);
                }
            }

            $('.apl-radio-choice li label').click(function() {
                $(this).prev().attr('checked', 'checked');
                handleRadioFn($(this).prev());
            });

            /**
             * Handle check other choice
             * */
            $('ul.apl-radio-choice li input[type="radio"]').change(function() {
                handleRadioFn($(this));
            });

            /**
             * Init datepicker to date custom field
             * */
            var initDatepickerObjs = $('.apl-validation-datepicker');
            $.each(initDatepickerObjs, function(i, v) {
                if ( $(v).attr('id') ) {

                    var obj = $('#'+ $(v).attr('id'));

                    var currentLang = APL.currentLang != null ? APL.currentLang : '';

                    var options = $.extend({}, // empty object
                        $.datepicker.regional[currentLang], {
                            dateFormat: 'yy-mm-dd',
                            nextText: '&#187;',
                            prevText: '&#171;',
                        }
                    );
                    obj.datepicker(options);
                }
            }); // End * Init datepicker to date custom field
            $.datepicker.setDefaults($.datepicker.regional['en']); // set default language for datime picker
            $.each($('[data-ride="ap-refer-select"]'), function(i, v) {

                $(v).change(function() {
                    var _data = $(this).data(),
                        _target = $($(this).data().target);
                    $.ajax({
                        url: APL.ajax_url,
                        data: {
                            action: _data.action,
                            id: $(this).val()
                        },
                        beforeSend: function() {

                        },
                        success: function(res) {
                            _target.html('<option value="0">'+(_target.children("[value='0']").html())+'</option>'+ res.html);
                            _target.children("option[value='"+$(_target).data().value+"']").attr("selected", 'selected');
                        }
                    });
                });
            });

            /** Trigger change event for the refer source data */
            $.each($('[data-ride="ap-refer-select"]'), function(i, v) {
                if ( $(v).val() != 0 ) {
                    $(v).trigger('change');
                }
            });

            /**
             * handle ads
             * */

            $('.sam-container.sam-place').hide();

            if ( $.apl.is_global_page() ) { /* Remove ads from global pages */
                $('.sam-container.sam-place').hide();

            } else if ( APL.is_page != '1' ) { /* Move ads */

                var ads = $('.sam-container.sam-place');
                if ( ads && ads[0] ) {
                    $('#apl-ads-top').html(ads[0]);
                    $('#apl-ads-top').find('.sam-container.sam-place').show();
                }

                if ( ads && ads[1] ) {
                    $('#apl-ads-bottom').html(ads[1]);
                    $('#apl-ads-bottom').find('.sam-container.sam-place').show();
                }
            } // End * handle ads


            /* handle editor */
            var _editors = $( '.apl-editor' );
            $.each( _editors, function(i, v) {
                CKEDITOR.replace($(v).data().id);
            });

            $( 'form#admin-frm-step1 #event-save-date' ).click(function() {
                var current_start = $('#c-s').val(),
                    current_end = $('#c-e').val();
                if ( ($(this).data().start && current_start != $(this).data().start) ||
                    ( $(this).data().end && current_end != $(this).data().end ) ) {
                    $.apl.confirm($(this).data().ct,
                        function() {
                            javascript:apollo_submit_form( 'admin_frm_step2', 'submit_event_dates' );

                        },
                        function() {

                        }
                    );

                } else {
                    javascript:apollo_submit_form( 'admin_frm_step2', 'submit_event_dates' );
                }
            });



            $(window).on('load', function() {

                // Bind date of event date ranges
                $('input[name="_apollo_event_start_date"]').bind('change paste', function() {
                    var objStart = $('input[name="_apollo_event_start_date"]');
                    var objEnd = $('input[name="_apollo_event_end_date"]');
                    updateStartDateEndDate(objStart,objEnd);
                });

                $('input[name="_apollo_event_end_date"]').bind('change paste', function() {
                    var objStart = $('input[name="_apollo_event_start_date"]');
                    var objEnd = $('input[name="_apollo_event_end_date"]');
                    updateStartDateEndDate(objStart,objEnd);
                });

                // Bind date of event date ranges
                $('.e-s-wg-c-l input[name="start_date"]').bind('change paste', function() {
                    updateStartDateEndDate($('.e-s-wg-c-l input[name="start_date"]'),$('.e-s-wg-c-l input[name="end_date"]'));
                });

                $('.e-s-wg-c-l input[name="end_date"]').bind('change paste', function() {
                    updateStartDateEndDate($('.e-s-wg-c-l input[name="start_date"]'),$('.e-s-wg-c-l input[name="end_date"]'));
                });

                /* #16782: 0002308: Calendar Month search widget - Month view layout */
                $('#apl-search-widget-month-view').bind('change paste', function() {
                    var date = $(this).val();

                    var  $startObj = $('.e-s-wg-c-l input[name="start_date"]'),
                        $endObj   = $('.e-s-wg-c-l input[name="end_date"]');

                    $startObj.val(date);
                    $endObj.val(date);
                });


                function updateStartDateEndDate($startObj,$endObj){
                       var  _start_date = new Date($startObj.val()),
                            _end_date   = new Date($endObj.val());
                    if( !$endObj.val() || _start_date.getTime() > _end_date.getTime() ) {
                        $endObj.val($startObj.val());
                    }
                }

                // Handle placeholder of event desc
                if( $('#editor1').length ) {
                    var placeholder = $('#editor1').attr('placeholder');
                    var ed = CKEDITOR.instances['editor1'];

                    if ( ! apl_strip_tags(ed.getData()).replace(/\s+/, "") ) {
                        ed.setData(placeholder);
                    }

                    CKEDITOR.instances['editor1'].on('focus', function() {
                        if ( apl_strip_tags(this.getData()) == placeholder ) {
                            this.setData('');
                        }
                    });

                    CKEDITOR.instances['editor1'].on('blur', function() {
                        if ( apl_strip_tags(this.getData().replace(/\s+/, "") ) == '' ) {
                            this.setData(placeholder);
                        }
                    });
                }
                /* End handle placeholder event desc */

                // ticket: #11077
                var requestOrgId = $('#add-co').data('requestOrg');
                if ( requestOrgId ) {
                    $('#add-co').trigger('click');
                }
            });



            $( '.cat-listing' ).each(function(i, v) {
                set_col(v);
            });

            //TriLM delete all disabled cell
            function deleteAllDisabledCell(){
                javascript:apollo_submit_form( 'admin_frm_step2', 'submit_event_dates' );
            }


            function set_col(v) {
                var c1      = $(v).find('.c0'),
                    elm_c2  = $(v).find('.c1').children(),
                    nc1     = $(v).find('.c0').find('li').length,
                    nc2     = $(v).find('.c1').find('li').length;

                if ( nc1+9 >= nc2 ) return false;

                $.each( elm_c2, function(i, v1) {
                    c1.append(v1);
                    var nli = parseInt($(v1).children('ul').children().length) + 1;
                    nc1 = parseInt(nc1) + nli;
                    nc2 = parseInt(nc2) - nli;
                    if ( nc2 > nc1+8 ) set_col(v);
                    else return false;
                } );
            }

            $( '.qualification' ).click( function() {
                $(this).next().css('display', '');
            });

            $('[data-rise="clear-form"]').click($.apl.clear_form);
            $('.clear-event-form').click(function(){
                var _data_form = $(this).data();
                /** @Ticket #15742 hidden list city */
                window.APOLLO.AplSelect2.showCurrentSelected(false);

                var longitude = $( _data_form.form+ ' input[name="apl-lat"]' ).val();
                var latitude = $( _data_form.form+ ' input[name="apl-lng"]' ).val();

                $(_data_form.form)[0].reset();

                $( _data_form.form+ ' input[type="text"]' ).val('');
                $( _data_form.form+ ' input[type="hidden"]' ).val('');
                $( _data_form.form+' .caldr ').removeAttr('value');
                $(_data_form.form+ ' #event-category-select option').removeAttr('selected');
                $(_data_form.form+ ' #event-org-sel-rgt option').removeAttr('selected');
                $(_data_form.form+ ' #event-category-select option').removeAttr('selected');
                $(_data_form.form+ ' #event-location-select option').removeAttr('selected');
                $(_data_form.form+ ' #event-city-select option').removeAttr('selected');
                $(_data_form.form+ ' #event-region-select option').removeAttr('selected');

                /** @Ticket #12940 */
                $(_data_form.form+ ' #event-accessibility-icons-search ul li input[type="checkbox"]').removeAttr('checked');

                /** @Ticket #13151 */
                var accessText = $('.apl-accessibility-item');
                if ( accessText.length > 0 ) {
                    var accessSelectedText = accessText.closest('dl.accessibilyty-dropdown').find('dt span.access-drop');
                    if ( accessSelectedText.length > 0 ) {
                        accessSelectedText.text(accessSelectedText.attr('default-text'));
                    }
                }

                /** @Ticket #13392 */
                var freeEvent = $('.free-event');
                if (freeEvent.length > 0) {
                    freeEvent.removeAttr('checked');
                }

                var text = '';
                if ( $(_data_form.form+ ' #event-category-select').length > 0 ) {
                    $(_data_form.form+ ' #event-category-select').select2("val","0");
                     text = $(_data_form.form+ ' #event-category-select').find("option:first-child").text();
                    $(_data_form.form+ ' #select2-event-category-select-container').text(text);

                }
                if ( $(_data_form.form+ ' #event-org-sel-rgt').length > 0 ) {
                    $(_data_form.form+ ' #event-org-sel-rgt').select2("val","0");
                     text = $(_data_form.form+ ' #event-org-sel-rgt').find("option:first-child").text();
                    $(_data_form.form+ ' #select2-event-org-sel-rgt-container').text(text);

                }

                if ( $(_data_form.form+ ' #event-location-select').length > 0) {
                    $( _data_form.form+ ' #event-location-select' ).select2("val","");
                    text = $(_data_form.form+ ' #event-location-select').find("option:first-child").text();
                    $(_data_form.form+ ' #select2-event-location-select-container').text(text);
                }

                if ( $(_data_form.form+ ' #event-city-select').length > 0) {
                    if($(_data_form.form+ " #event-city-select option[value='0']").length > 0){
                        $(_data_form.form+ ' #event-city-select').select2("val","0");
                    } else {
                        $(_data_form.form+ ' #event-city-select').select2("val","");
                    }

                    text = $(_data_form.form+ ' #event-city-select').find("optgroup:first-child > option").text();
                    if(text == ''){
                        text = $(_data_form.form+ ' #event-city-select').find("option:first-child").text();
                    }
                    $(_data_form.form+ ' #select2-event-city-select-container').text(text);
                }

                if ( $(_data_form.form+ ' #event-region-select').length > 0  && $(_data_form.form + ' #event-region-select').data('select2')) {
                    $(_data_form.form+ ' #event-region-select').select2("val","0");
                    text = $(_data_form.form+ ' #event-region-select').find("option:first-child").text();
                    $(_data_form.form+ ' #select2-event-region-select-container').text(text);
                }

                // if ( $(_data_form.form+ ' select[name="event_region"]').length > 0 ) {
                //     $(_data_form.form+ ' select[name="event_region"]').select2("val","");
                // }

                 if ( $(_data_form.form+ ' #event-location-sel-rgt').length > 0 && $(_data_form.form + ' #event-location-sel-rgt').data('select2')) {
                    $(_data_form.form+ ' #event-location-sel-rgt').select2("val","");
                     text = $(_data_form.form+ ' #event-location-sel-rgt').find("option:first-child").text();
                     $(_data_form.form+ ' #select2-event-location-sel-rgt-container').text(text);

                }

                if ( $('.select-date') ) {
                    $('.select-date').removeClass('active');
                }

                /* @Ticket #13620 */
                $(_data_form.form +  " select").prop('selectedIndex', 0);
                $(_data_form.form+ ' input:checkbox').removeAttr('checked');
                $(_data_form.form +  " select.select2-hidden-accessible").each(function() {
                    text_holder = $(this).find("option:first-child").first().text();
                    $(this).val("").trigger("change.select2");
                    $(this).parent().find('.select2 .select2-selection__rendered').text(text_holder);
                });

                $( _data_form.form+ ' input[name="apl-lat"]' ).val(longitude);
                $( _data_form.form+ ' input[name="apl-lng"]' ).val(latitude);
                $('.top-search-row #discount-quick-search').removeClass('active');
                $('.top-search-row #today-quick-search').removeClass('active');
                $('.top-search-row #weekend-quick-search').removeClass('active');
            });

            var $window = $(window);
            /**
             * HANDLE DESCRIPTION EVENT ON SINGLE EVENT
             * @author tuanPHPVN
             * */

            $( '#edu-prog-link' ).change(function() {
                if ( ! $(this).val() ) return;

                window.location.href = $(this).data().url +$(this).val();
            });


            // Handle public art map
            if ($('#art_map_canvas').length || $('#art_detail_map_canvas').length) {
                $.apl.google_map.load_me_and_do("jQuery.apl.google_map.public_art_map");
            }

            $.apl.google_map.public_art_map = function() {

                /* Override map setting if on 'public-art/map' page */
                var eleLat = $('#_apl_pa_setting_lat');
                var eleLong = $('#_apl_pa_setting_long');
                var eleZoomMag = $('#_apl_pa_setting_zoom_magnification');
                var refreshTimeFromServer = APL.google_map_settings.map_refreshed_time_from_server;
                APL.google_map_settings.isPublicArtMapPage = (eleLat.length > 0 && eleLong.length > 0 && eleZoomMag.length > 0);
                APL.google_map_settings.curLatVal = eleLat.length > 0 && eleLat.val().trim().length > 0 ? eleLat.val() : '';
                APL.google_map_settings.curLongVal = eleLong.length > 0 && eleLong.val().trim().length > 0 ? eleLong.val() : '';
                APL.google_map_settings.curZoomMagVal = eleZoomMag.length > 0 && parseInt(eleZoomMag.val()) > 0 ? parseInt(eleZoomMag.val()) : 8;
                /* End override map setting if on 'public-art/map' page */


                map             = false;
                clickmarker     = '';
                infowindow      = new google.maps.InfoWindow();
                geocoder        = new google.maps.Geocoder();
                myposition_icon    = APL.google_map_settings.google_map_my_position_icon;
                map_container   = 'art_map_canvas';
                default_icon    = APL.google_map_settings.google_map_icon;
                markerIconWidth = parseInt(APL.google_map_settings.google_map_icon_width);
                markerIconHeight =  parseInt(APL.google_map_settings.google_map_icon_height);
                currentLat = APL.google_map_settings.isPublicArtMapPage ? APL.google_map_settings.curLatVal : '';
                currentLng = APL.google_map_settings.isPublicArtMapPage ? APL.google_map_settings.curLongVal : '';
                markers = [];
                circleAreas = [];
                myPositionMarker = [];
                isFilter = false;
                currentSliderItem = '';
                infoWindowMaxwidth = 270;
                globalAllowShareLocation = false;
                globalOption = {
                    zoom: APL.google_map_settings.isPublicArtMapPage ? APL.google_map_settings.curZoomMagVal : 8,
                    minZoom: 1,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scrollwheel: true,
                    navigationControl: false,
                    mapTypeControl: false,
                    scaleControl: false,
                    draggable: true,
                    //zoomControl : true,
                    //zoomControlOptions: {
                    //                    style: google.maps.ZoomControlStyle.SMALL,
                    //                    position: google.maps.ControlPosition.RIGHT_BOTTOM
                    //                },
                    rotateControl : false,
                    overviewMapControl : false,
                    disableDefaultUI: true,
                    panControl: false,
                    loginControl : false
                };
                globalMapRespone = '';
                notAllowShareInitZoom = APL.google_map_settings.isPublicArtMapPage ? APL.google_map_settings.curZoomMagVal : 1;
                var bounds = new google.maps.LatLngBounds();

                function set_map (ops, callback) {

                    var w, h, icon = '',
                        lat_lng     = new google.maps.LatLng(ops.latitude, ops.longitude),
                        options     =   globalOption;
                        w = 18,
                        h = 28,
                        icon = make_icon(typeof(ops.src_icon) != 'undefined' ? ops.src_icon : default_icon, w, h);

                    map = new google.maps.Map(document.getElementById(map_container), options);
                    clickmarker = new google.maps.Marker ({
                            position: lat_lng,
                            map: map,
                            draggable: true,
                            icon: icon
                    });

                    clickmarker.setVisible(false);

                    map.setCenter(lat_lng);

                    if (ops.zoom) {
                        map.setZoom(ops.zoom);
                    }

                    _map = map
                    //Handler finished load map
                    google.maps.event.addListener(map, 'idle', function(event) {

                        if (typeof(callback) != 'undefined') {
                            callback({
                                min_max: get_bounds(_map),
                                map: _map,
                                success: true
                            });
                        }
                    });

                    google.maps.event.addListener(clickmarker, 'dragstart', function() {
                        infowindow.close();
                    });

                }

                if ($('#art_detail_map_canvas').length) {
                    var _init_full = false;
                    var _emid = "art_detail_map_canvas";
                    var position = $('#' + _emid);
                    if (position.length === 0) return;
                    var _lat = position.data('lat');
                    var _long = position.data('long');
                    currentLat = lat = parseFloat(_lat);
                    currentLng = lng = parseFloat(_long);
                    var _relation_id = position.data('relation_id');
                    map_container = 'art_detail_map_canvas';
                    var address = $('.loc-address').children('p').text();
                    var thisPublicArtLat = $('#art_detail_map_canvas').attr('data-lat');
                    var thisPublicArtLng = $('#art_detail_map_canvas').attr('data-long');
                    var radius = 1;
                    var firstInit = true;
                    var outSideMapFirstInit = true;
                    var isShowNearBy = false;
                    var publicDetailPage = $('#art_detail_map_canvas').closest('.public-art');

                   /* Truonghn : updates - Task #7626 -  [CF] 20160519 - [Public Art] New checkbox called "Display other nearby public art"  */

                    var initMapPublicArt = function(callback) {
                        map =  set_map({latitude: lat, longitude: lng, zoom: 13},
                            function (response) {
                                if (typeof(callback) != 'undefined') {
                                    callback({
                                        min_max: response.min_max,
                                        lat: lat,
                                        lng: lng,
                                        success: true,
                                        map: response.map
                                    });
                                }
                                var min_max = response.min_max;
                                params = {
                                    min_lat: min_max.min_lat,
                                    max_lat: min_max.max_lat,
                                    min_lng: min_max.min_lng,
                                    max_lng: min_max.max_lng,

                                }
                                if(firstInit){
                                    params.my_pos_lat = thisPublicArtLat;
                                    params.my_pos_lng = thisPublicArtLng;
                                }
                                if( isShowNearBy == true) {
                                    $.ajax({
                                        url: APL.ajax_url,
                                        data: {
                                            action: 'apollo_get_public_art_around_you',
                                            mapData: params
                                        },
                                        success: function (res) {
                                            set_markers(res.data, response.map);
                                        }
                                    });

                                }
                                firstInit = false;
                            });
                    }

                    var mapPublicArtCallback = function (res) {
                        map = res.map;
                        /** @Ticket #16536 - Remove the 'red circle' overlay from the public art detail page map. */
                        if (typeof publicDetailPage !== 'undefined' || publicDetailPage.length != 0) {
                            circleArea(null);
                            return;
                        }
                        circleArea(radius)
                    };

                    initMapPublicArt(mapPublicArtCallback);

                    $("#pba_display_nearby").on("change", function() {
                        firstInit = true;
                        if($(this).prop('checked') === true) {
                            isShowNearBy = true;
                            initMapPublicArt(mapPublicArtCallback);

                        } else {
                            isShowNearBy = false;
                            initMapPublicArt(mapPublicArtCallback);
                        }
                    });
                    /* Truonghn : end updates - Task #7626 -  [CF] 20160519 - [Public Art] New checkbox called "Display other nearby public art"  */

                    /* Handle Full Screen google map */
                    var _a_full_google_map = "#_event_trigger_fullmap";
                    $(_a_full_google_map).on('click', function (e) {
                        $('#_popup_google_map .modal-content').css('z-index', 20);
                        firstInit = true;
                        var $this = $(this);
                        var _pid = $this.data('target');
                        var $_popup = $(_pid);
                        var _height_popup = $_popup.outerHeight();

                        $_popup.addClass($_popup.data('active')).css({
                            'margin-top': -1 * (_height_popup / 2)
                        });
                        map_container = _relation_id;
                        //if (_init_full == false) {
                            initMapPublicArt(function (res) {
                                map = res.map
                                circleArea(radius)
                            });
                            _init_full = true;
                        //}
                        e.stopPropagation();
                    });

                    $("._close, " + $(_a_full_google_map).data('target') + ' .overlay').on('click', function (e) {
                        //firstInit = true;
                        var $this = $(this);
                        var $owner = $($this.data('owner'));
                        $owner.removeClass($owner.data('active'));
                        map_container = 'art_detail_map_canvas';
                        initMapPublicArt(function(res) {
                            map = res.map
                            circleArea(radius)
                        });

                    });

                }
                else {

                    //Do not know location
                    set_current_location = function(ops, callback) {
                        navigator.geolocation.getCurrentPosition(function (position) {
                            var pos = position.coords,
                                lat = parseFloat(pos.latitude),
                                lng = parseFloat(pos.longitude);
                            geocoder.geocode({'latLng': get_lat_lng(lat, lng)}, function (results, status) {
                                var resultAddress = results ? results[0] : '';
                                globalAllowShareLocation = true;
                                map =  set_map({
                                    latitude: lat,
                                    longitude: lng,
                                    html: resultAddress.formatted_address
                                },
                                    function (response) {

                                    if (typeof(callback) != 'undefined') {

                                        callback({
                                            min_max: response.min_max,
                                            lat: lat,
                                            lng: lng,
                                            success: true,
                                            address: resultAddress,
                                            map: response.map
                                        });
                                        renderCustomControl();
                                    }
                                });


                            });
                        }, function () {
                        });
                    }

                    runInNotAllowShareLocationMode();

                }

                function set_markers(data, map, callback) {

                    var i, pos, w = markerIconWidth, h = markerIconHeight, item, image, url, action_set, marker,title = data.post_title, id = data.id, sitelink = data.sitelink, icons_marker = data.marker_icon
                        infowindow          = new google.maps.InfoWindow(),


                    action_set = function ( title, lat, lng, src, link, w, h,thumbnail,description,address,id,sitelink,icons_marker) {
                        pos = new google.maps.LatLng(lat, lng);
                        bounds.extend(pos);
                        if(icons_marker == '')
                            image = make_icon(src, w, h);
                        else
                            image = make_icon(icons_marker, w, h);
                        marker = new google.maps.Marker({
                            position: pos,
                            map: map,
                            icon: image,
                            title: title,
                            optimized: false, // To set border radius in css of img
                            url: link,
                            //animation: google.maps.Animation.DROP,
                        });

                        google.maps.event.addListener(marker, 'click', (function(marker, i) {
                            return function() {
                                var sliderItemId = '#map_item_'+id;
                                activeSliderItem(sliderItemId);
                                scrollSliderToLeft(sliderItemId);
                                if ( typeof activeInfoW != 'undefined' ) activeInfoW.close();
                                var infowindow = new google.maps.InfoWindow({maxWidth: infoWindowMaxwidth});
                                activeInfoW = infowindow;
                                 var html = infoWindowContent(title,thumbnail,link,address,description,sitelink);
                                infowindow.close();
                                infowindow.setContent(html);
                                infowindow.open(map, marker);
                                map.setZoom(15);

                            }
                        })(marker, i));
                        return marker;
                    };


                    bounds = new google.maps.LatLngBounds();
                    $.each(data, function(i, item) {
                      var marker =  action_set(item.post_title, item.lat, item.lng, default_icon, item.link, w, h,item.thumbnail,item.description,item.address,item.id,item.sitelink,item.marker_icon);
                        markers.push(marker);
                        bounds.extend(marker.getPosition());
                    });


                    if (typeof (callback) != 'undefined') {
                        callback({success: true});
                    }
                }


                /**
                * Get max min of map
                *
                * */
                function get_bounds(map) {

                    var bounds  = map.getBounds(),
                        ne      = bounds.getNorthEast(), // top-left
                        sw      = bounds.getSouthWest(); // bottom-right

                    return {
                        min_lat: sw.lat(),
                        max_lat: ne.lat(),
                        min_lng: sw.lng(),
                        max_lng: ne.lng()
                    };
                }
                function get_lat_lng (lat, lng) {
                    return new google.maps.LatLng(lat, lng);
                }
                function make_icon(src, w, h) {
                    return new google.maps.MarkerImage(src,
                    null, null, null, new google.maps.Size(w, h));
                }
                function InitMarker(params,response, callback){
                    console.log( APL.google_map_settings);
                    var _setMarkers = function(markers, _callback) {
                        set_markers(markers, map, function() {


                            //get current lat,ln
                            currentLat = response.lat;
                            currentLng = response.lng;

                            boundAllMarker(function(boundAllMarkerResponse) {
                                if(typeof callback !== "undefined"){
                                    callback(boundAllMarkerResponse);
                                } else {
                                    $.apl.google_map.loading(false);
                                }
                            });

                            //TriLM call function render slider item
                            setTimeout(function() {
                                renderMapItemSlider(markers);
                            }, 500)

                        });

                        if (typeof _callback != "undefined") {
                            _callback({
                                'success': true
                            });
                        }

                    };

                    // Get markers from global, this variable is expired after two days
                    var _localData = JSON.parse(localStorage.getItem('aplArtMarkers'));
                    var _localRefreshedTime = localStorage.getItem('aplArtMarkersRefreshTimeLocal');
                    if (
                            _localData
                            && _localData.timestamp
                            && Math.ceil((new Date().getTime() - _localData.timestamp)/ 86400) <= 2  // 2 days
                            && (
                                _localRefreshedTime == refreshTimeFromServer
                            )
                        ) {
                        _setMarkers(_localData.value);
                        return;
                    }

                    $.ajax({
                        url: APL.ajax_url,
                        data: {
                            action: 'apollo_get_all_public_art',
                        },
                        success: function (res) {
                            _setMarkers(res.data, function() {
                                localStorage.setItem('aplArtMarkers', JSON.stringify({
                                    'value': res.data,
                                    'timestamp': new Date().getTime()
                                }));
                                localStorage.setItem('aplArtMarkersRefreshTimeLocal',refreshTimeFromServer);
                            });

                        }
                    });
                }

                function initMarkerAroundYou(params,response){
                    var ajaxParam =  {
                        action: 'apollo_get_public_art_around_you',
                    };
                    if(params != 0){
                        ajaxParam.mapData = params;
                    }
                    $.ajax({
                        url: APL.ajax_url,
                        data:ajaxParam,
                        success: function (res) {
                            var _data = res.data
                            set_markers(res.data, map, function() {

                                //TriLM call function render slider item
                                renderMapItemSlider(_data);
                                //get current lat,ln
                                currentLat = response.lat;
                                currentLng = response.lng;
                                $.apl.google_map.loading(false);
                            });
                        }

                    });
                }

                /* Applying Map Setting */
                function setMapWithSettings(){
                    if(APL.google_map_settings.isPublicArtMapPage
                        && APL.google_map_settings.curLatVal.length > 0
                        && APL.google_map_settings.curLongVal.length > 0
                    ){
                        var _latlng = {lat: parseFloat(APL.google_map_settings.curLatVal), lng: parseFloat(APL.google_map_settings.curLongVal)};
                        map.setZoom(APL.google_map_settings.curZoomMagVal);
                        map.setCenter(_latlng);
                    }
                }

                function runInNotAllowShareLocationMode(){
                    $.apl.google_map.loading(true);

                    if(globalAllowShareLocation == false){
                        //auto detect lat, lng
                        currentLat = APL.google_map_settings.isPublicArtMapPage && APL.google_map_settings.curLatVal.length > 0 ? APL.google_map_settings.curLatVal : 0;
                        currentLng = APL.google_map_settings.isPublicArtMapPage && APL.google_map_settings.curLongVal.length > 0 ? APL.google_map_settings.curLongVal : 0;
                        set_map({
                            latitude: currentLat,
                            longitude: currentLng
                        },function(respone){
                            /**
                             *   Find map center point
                             *   use this for radius search
                             */
                            if (!window.aplLoadedPAMap) {
                                if(!APL.google_map_settings.isPublicArtMapPage){
                                    currentLat = map.getCenter().lat();
                                    currentLng = map.getCenter().lng();
                                }

                                map.setZoom(notAllowShareInitZoom);
                                var _latlng = {lat: parseFloat(currentLat), lng: parseFloat(currentLng)};
                                InitMarker(params = 0,_latlng, function(response){
                                    if( response.success ) {
                                        setMapWithSettings();
                                        $.apl.google_map.loading(false);
                                    }
                                });

                                renderCustomControl();
                                removeOldMyPosition();
                                window.aplLoadedPAMap = true;
                            } else {
                                $.apl.google_map.loading(false);
                            }

                        });
                    }
                }

                //Trilm Render map items slider
                function boundAllMarker(callback){

                     // Don't zoom in too far on only one marker
                    if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
                       var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.01, bounds.getNorthEast().lng() + 0.01);
                       var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.01, bounds.getNorthEast().lng() - 0.01);
                       bounds.extend(extendPoint1);
                       bounds.extend(extendPoint2);
                    }
                    map.fitBounds(bounds);


                    if(typeof callback !== "undefined") {
                        callback({
                            'success': true,
                            'msg': 'Done bound all marker'
                        });
                    }
                }
                function pointMyPosition(){

                    removeOldMyPosition();
                    var myLatLng = {lat: parseFloat(currentLat), lng: parseFloat(currentLng)};
                    var markerSetting = {
                        position: myLatLng,
                        map: map,
                        title: $('#art_map_canvas').data('position-text'),

                    };
                    if(myposition_icon){
                        var image = make_icon(myposition_icon, markerIconWidth, markerIconHeight);
                        markerSetting.icon = image;
                    }
                    var marker = new google.maps.Marker(markerSetting);
                    //infowindow.setContent('<p class="my_pos">My position</p>');
                    //infowindow.open(map, marker);
                    myPositionMarker.push(marker);
                    $('.current_location_button').removeClass('disable');
                }
                function removeOldCirlce(){
                    //remove old cirlce
                    while(circleAreas.length){
                        circleAreas.pop().setMap(null);
                    }
                }
                function removeOldMyPosition(){
                    //remove old my position
                    while(myPositionMarker.length){
                        myPositionMarker.pop().setMap(null);
                    }
                }
                function circleArea(r){

                    removeOldCirlce();
                    //add new circle
                    var circ = new google.maps.Circle({
                        strokeColor: '#FF0000',
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: '#FF0000',
                        fillOpacity: 0.35,
                        map: map,
                    });
                    circleAreas.push(circ);
                    var latlng = {lat: parseFloat(currentLat), lng : parseFloat(currentLng)};

                    circ.setRadius(r * 1609.34);
                    circ.setCenter(latlng);

                    pointMyPosition();
                    // updates markers
                    google.maps.event.trigger(map,'dragend');
                }
                function renderMapItemSlider(items){
                    var sliderContentID = '#art_map_slider';
                    var sliderItemID = 'map_item_';
                    var count = 0;
                    var maxItem = 10;

                    $(sliderContentID).html("");

                    $.each(items,function(key,value){
                        //if(count == maxItem){
                        //    return true;
                        //}
                        var html = '' +
                            '<li id="'+sliderItemID+value.id+'" class="map_slider_item" data-detail-link="'+value.link+'" data-lat="'+value.lat+'" data-lng="'+value.lng+'" > ' +
                                '<div class="title_trim">'+value.title+'</div>' +
                                '<div class="title">'+value.post_title+'</div>' +
                                '<div class="thumbnail">' +
                                    '<img class="lazy" src="'+value.thumbnail+'">' +
                                '</div>' +
                                '<div class="description">'+
                                    '<p>'+value.description+'</p>'+
                                '</div>'+
                                '<div class="address">'+value.address+'</div>' +
                                '<div class="sitelink">'+value.sitelink+'</div>' +
                            '<input type="hidden" class="icon" value="'+value.marker_icon+'" >' +
                            '</li>';
                        if(
                            value.lat !== '' &&
                            value.lng !== ''
                        ){
                            if(!$(sliderContentID+' #'+sliderItemID+value.id).length){
                                $(sliderContentID).append(html);
                                count ++;
                            }
                        }
                    });
                    if(count == 0){
                        $('#art_map_slider').html('<p>No public art found!</p>');
                    }

                }
                function renderCustomControl(){
                    var html = ' <div class="im_map_control"> \
                        <div class="current_location_button"> \
                            <a><i class="fa fa-compass"></i></a> \
                        </div>\
                        <div class="current_zoom_in_button"> \
                            <a><i class="fa fa-plus"></i></a> \
                        </div>\
                         <div class="current_zoom_out_button"> \
                            <a><i class="fa fa-minus"></i></a> \
                        </div>\
                        </div>';
                    $('#'+map_container).prepend(html);
                }

                function infoWindowContent(title,thumbnail,link,address,description,sitelink){
                      var   html = "<p class='marker_title'><a href='"+sitelink+"'>"+title+"</a></p>";
                            html += "<p class='marker_thumbnail'><a href='"+sitelink+"'><img src='"+thumbnail+"'></a></p>";
                            html += "<div class='marker_address'><p>"+address+"</p></div>";
                            html += "<div class='marker_description'><p>"+description+"</p></div>";
                      return html;
                }
                function jumpToFirstSearchedMarker(data,map){
                    if(data.length > 0){
                        var location = {lat: parseFloat( data[0].lat ), lng : parseFloat( data[0].lng ) }
                        map.setCenter( location );

                    };
                }
                function activeSliderItem(id){
                    $('.map_slider_item').removeClass('active');
                    $(id).addClass('active');

                }
                function scrollSliderToLeft(id){
                    if(currentSliderItem != id) {
                        if($('.art_map_slider_content').length !=0 ) {
                            var viewPortLeft = $('.art_map_slider_content').offset().left;
                            //var viewPortCenter = $('.art_map_slider_content').width();
                            var left = $(id).offset().left;
                            var scroll = $('.art_map_slider_content').scrollLeft();
                            var pos = 0;
                            pos = parseFloat(left) - parseFloat(viewPortLeft) + parseFloat(scroll);
                            $('.art_map_slider_content').animate({scrollLeft: pos}, 800);
                            currentSliderItem = id;
                        }
                    }
                }

                $(document).on('click','.map_slider_item',function(){
                    activeSliderItem('#'+$(this).attr('id'));
                    if(isFilter == false)
                        removeOldCirlce();

                    if ( typeof activeInfoW != 'undefined' ) activeInfoW.close();

                    var lat = parseFloat($(this).attr('data-lat'));
                    var lng = parseFloat($(this).attr('data-lng'));
                    var link = $(this).attr('data-detail-link');
                    var title = $(this).children('.title').text();
                    var desc = $(this).children('.description').children('p').text();
                    var thumbnail = $(this).children('.thumbnail').children('img').prop('src');
                    var address = $(this).children('.address').text();
                    var sitelink = $(this).children('.sitelink').text();
                    var iconLink = $(this).children('.icon').val();

                    if(iconLink == '')
                        iconLink = default_icon;

                    //TriLM temp code them optimize
                    var center = new google.maps.LatLng(lat, lng);
                    var icon = make_icon(iconLink, markerIconWidth, markerIconHeight);
                    var marker = new google.maps.Marker({
                        position: {lat: lat, lng: lng},
                        map: map,
                        icon : icon
                    });

                    var infowindow = new google.maps.InfoWindow({maxWidth:infoWindowMaxwidth});
                    activeInfoW =  infowindow;
                    var html = infoWindowContent(title,thumbnail,link,address,desc,sitelink);
                    infowindow.setContent(html);
                    infowindow.open(map, marker);

                    map.panTo(center);
                    map.setZoom(15);

                    //push marker to global marker
                    markers.push(marker);
                });
                //TriLM filter map
                $(document).on('click','#filter-button',function(){
                    isFilter = true;

                    $.apl.google_map.loading(true);

                    //remove all non-search makers
                    /* Remove All Markers */
                    while(markers.length){
                        markers.pop().setMap(null);
                    }

                    var ftName = $('#filter-name').val();
                    var ftaddress = $('#filter-address').val();
                    var ftCategory = $('#filter-category option:selected').val();
                    var ftMedium = $('#filter-medium option:selected').val();
                    var ftLocation = $('#filter-location option:selected').val();
                    var ftCollection = $('#filter-collection option:selected').val();
                    var ftRadius = $('#filter-radius option:selected').val();
                    if(ftRadius != ''){
                        circleArea(ftRadius);
                    }else{
                        removeOldCirlce();
                    }

                    localStorage.removeItem('aplArtMarkers');

                    //TriLM call function render slider item
                    $.ajax({
                        url: APL.ajax_url,
                        data: {
                            action: 'apollo_get_all_public_art',
                            keyword : ftName,
                            term : ftCategory,
                            address : ftaddress,
                            medium : ftMedium,
                            location : ftLocation,
                            collection : ftCollection,
                            radius : ftRadius,
                            currentLat :  currentLat,
                            currentLng :  currentLng,
                        },
                        success: function (res) {
                            var _data = res.data;
                            set_markers(res.data, map, function(res) {
                                $.apl.google_map.loading(false);
                                if (!$('#filter-radius').val()) {
                                    boundAllMarker(function(res) {
                                        //jumpToFirstSearchedMarker(res.data,map);
                                        $('#art_map_slider').empty();
                                        //TriLM call function render slider item
                                        setMapWithSettings();
                                        $.apl.google_map.loading(false);
                                        setTimeout(function() {
                                            renderMapItemSlider(_data);
                                        }, 500)
                                    });
                                } else {
                                    $.apl.google_map.loading(false);
                                }
                            });
                        }
                    });
                    $('.map-search .main-slider-overlay').hide();
                    $('.map-search .main-slider-overlay .loading').hide();

                });
                $(document).on('click','#filter-reset-button',function(){
                    //if not allow share location
                    if(globalAllowShareLocation == false){
                        removeOldMyPosition();
                        map.setZoom(notAllowShareInitZoom);
                    }
                    $('#filter-name').val('');
                    $('#filter-radius option').removeAttr('selected');
                    $('#filter-category option').removeAttr('selected');
                    $('#filter-medium option').removeAttr('selected');
                    $('#filter-location option').removeAttr('selected');
                    $('#filter-collection option').removeAttr('selected');


                    if ( $( '#select2-filter-radius-container' ) ) {
                        $( '#select2-filter-radius-container' ).text($('#filter-radius option').first().text());
                    }
                    if ( $( '#select2-filter-category-container' ) ) {
                        $( '#select2-filter-category-container' ).text($('#filter-category option').first().text());
                    }
                    if ( $( '#select2-filter-medium-container' ) ) {
                        $( '#select2-filter-medium-container' ).text($('#filter-medium option').first().text());
                    }
                    if ( $( '#select2-filter-location-container' ) ) {
                        $( '#select2-filter-location-container' ).text($('#filter-location option').first().text());
                    }
                    if ( $( '#select2-filter-collection-container' ) ) {
                        $( '#select2-filter-collection-container' ).text($('#filter-collection option').first().text());
                    }

                    $('#filter-button').trigger('click');

                    //center map to my location
                    var myLatLng = {lat: parseFloat(currentLat), lng: parseFloat(currentLng)};
                    map.setCenter( myLatLng );
                });

                $('#art_map_canvas').on('click','.current_location_button',function(){
                     //var latLng = {lat: parseFloat(currentLat), lng: parseFloat(currentLng)};
                     //map.setCenter(latLng);
                    isFilter = false;
                    $.apl.google_map.loading(true);
                     if(!$('#art_map_canvas').hasClass('disable')){
                         $('#art_map_canvas').addClass('disable');
                         set_current_location({}, function (response) {
                             //Send max min lat, lng of map to server to get the list of event, venues, user around this map
                             var min_max = response.min_max;
                             var params = {
                                 min_lat: min_max.min_lat,
                                 max_lat: min_max.max_lat,
                                 min_lng: min_max.min_lng,
                                 max_lng: min_max.max_lng
                             };
                             $('.wc-f .loc-address').html(response.address.formatted_address);

                             //disable this when filter
                             if(isFilter == false){
                                 $('#art_map_slider').html("");
                                 initMarkerAroundYou(params,response);
                                 pointMyPosition();
                             }

                             $.apl.google_map.loading(false);

                             $('#art_map_canvas').removeClass('disable');
                         });

                     }

                });
                $(document).on('click','.current_zoom_in_button',function(){
                    globalOption.zoom =  map.getZoom();
                    if(globalOption.zoom < 20 && globalOption.zoom > 0){
                        globalOption.zoom ++;
                        map.setZoom(globalOption.zoom);
                     }

                });
                $(document).on('click','.current_zoom_out_button',function(){
                    globalOption.zoom =  map.getZoom();
                    if(globalOption.zoom <= 20 && globalOption.zoom > 0){
                        globalOption.zoom --;
                        map.setZoom(globalOption.zoom);
                    }

                });
            };

            if (APL['is_singular[public_art]'] === "1" || APL['is_singular[post]'] === "1") {

                $("[data-type='vmore']").on('click', function(e) {

                    var $this = $(this);
                    var _dtg = $this.data('target');
                    var _dto = $this.data('own');

                    $(_dto).addClass('hidden');
                    $(_dtg).removeClass('hidden');

                });
            }

            if(
                APL['is_singular[event]'] === "1"
                || APL.is_print_event_page || APL['is_singular[artist]'] === "1"
                || APL['is_singular[educator]'] === "1"
                || APL['is_singular[organization]'] === "1"
                || APL['is_singular[venue]'] === "1"
                || APL['is_singular[classified]'] === "1"
                || APL['is_singular[business]'] === "1"
                || APL['is_listing[post_type]'] === "1"

            ) {
                $("[data-type='vmore']").on('click', function(e) {

                    var $this = $(this);
                    var _dtg = $this.data('target');
                    var _dto = $this.data('own');

                    $(_dto).addClass('hidden');
                    $(_dtg).removeClass('hidden');

                });

                /* HANDLE GOOGLE MAP - LAZY LOAD IT! */
                $.apl.google_map.load_me_and_do("jQuery.apl.google_map.google_map_event");

                /* Handle load google map by address  */
                $.apl.google_map.google_map_event = function(e) {

                    var _emid = "_event_map";
                    var $_emid = $('#'+_emid);
                    if($_emid.length === 0) return;

                    var _relation_id_list = $_emid.data('relation_id');
                    var _coor = $_emid.data('coor');


                    var _arr_relation_id_list = _relation_id_list.split(',');
                    _arr_relation_id_list.push(_emid);

                    var initMapForRelationId = function() {
                        $(_arr_relation_id_list).each(function(_, _id) { // same address
                            var map = new google.maps.Map(document.getElementById(_id),
                                mapOptions);




                            var markerConfig = {
                                position: location,
                                map: map,
                                title: $_emid.data('address')
                            };
                            var default_icon    = APL.google_map_settings.google_map_icon;
                            var markerIconWidth = parseInt(APL.google_map_settings.google_map_icon_width);
                            var markerIconHeight =  parseInt(APL.google_map_settings.google_map_icon_height);
                            if(default_icon != ''){
                                markerConfig.icon  = MarkerIcon = new google.maps.MarkerImage(default_icon,
                                    null, null, null, new google.maps.Size(markerIconWidth, markerIconHeight));

                            }else{
                                markerConfig.icon = $_emid.data('img_location');
                            }
                            var marker = new google.maps.Marker(markerConfig);
                            google.maps.event.addDomListener(window, 'resize', function() {
                                map.setCenter( location );
                            });
                        });
                    };


                    var _arrCoor = '', location, mapOptions;

                    if(_coor !== "") {
                        _arrCoor = _coor.split(',');

                        location = new google.maps.LatLng(_arrCoor[0], _arrCoor[1]);
                        mapOptions = {
                            center: location,
                            zoom: 16,
                            scrollwheel: true,
                        };

                        initMapForRelationId();

                    }
                    else {
                        var address = $_emid.data('address');
                        var nonce = $_emid.data('nonce');
                        if(address === "") return;

                        var geocoder = new google.maps.Geocoder();

                        geocoder.geocode( { 'address': address}, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {

                                var lat = results[0].geometry.location.lat();
                                var lng = results[0].geometry.location.lng();

                                location = new google.maps.LatLng(lat, lng);

                                mapOptions = {
                                    center: location,
                                    zoom: 15,
                                    scrollwheel: true
                                };

                                initMapForRelationId();

                                $.ajax({
                                    url: APL.ajax_url,
                                    data: {
                                        nonce: nonce,
                                        action: 'apollo_save_coordinate',
                                        lat: lat,
                                        lng: lng,
                                        addr: address
                                    }
                                });

                            } else {
                                /** Vandd @ticket #12018*/
                                // alert($_emid.data('alert'));
                                var locationBlock = $('.location-wrapper');
                                if(locationBlock.length > 0){
                                    $.each(locationBlock, function(index, item) {
                                        if(!$(item).hasClass('hidden')){
                                            $(item).addClass('hidden');
                                        }
                                    });
                                }

                            }
                        });
                    }
                };


                $('#_event_get_directions').on('click', function(e) {
                    $('#_event_trigger_fullmap').click();
                });

                /* Handle Full Screen google map */
                var _a_full_google_map = "#_event_trigger_fullmap";
                $(_a_full_google_map).on('click', function(e) {

                    $( '#_popup_google_map .modal-content' ).css( 'z-index', 20 );

                    var $this = $(this);
                    var _pid = $this.data('target');
                    var $_popup = $(_pid);
                    var _height_popup = $_popup.outerHeight();

                    $_popup.addClass($_popup.data('active')).css({
                        'margin-top': -1*(_height_popup/2)
                    });

                    e.stopPropagation();
                });

                $("._close, " + $(_a_full_google_map).data('target')+' .overlay').on('click', function(e) {
                    var $this = $(this);
                    var $owner = $($this.data('owner'));
                    $owner.removeClass($owner.data('active'));
                });

                //share calendar here
                $('.calendar_btn').on('click',function(e) {
                    $('#export_ics_frm').attr('action','/export_calendar/event/');
                    $('#export_ics_frm').attr('method','POST');
                    $('#calendar_action_button').text($(this).attr('title'));

                    var $this = $(this);
                    showPopupCalendar($this);
                });
                $('.share-calendar').on('click',function(e){
                    $('#export_ics_frm').attr('action','http://www.google.com/calendar/event');
                    $('#export_ics_frm').attr('method','GET');
                    $('#export_ics_frm').attr('target','_blank');
                    $('#calendar_action_button').text($(this).attr('title'));
                    var $this = $(this);
                    showPopupCalendar($this);

                });
                $('#calendar_action_button').on('click',function(){
                    $('#export_ics_frm').submit();
                });

                //function show popup
                function showPopupCalendar($this){
                    var directSubmit = $('#calendar_action_button').attr('data-type');
                    if(directSubmit == 0){
                        $( '#_popup_choose_event .modal-content' ).css( 'z-index', 20 );
                        console.log($this.attr('target'));
                        var _pid = $this.attr('target');
                        var $_popup = $(_pid);
                        var _height_popup = $_popup.outerHeight();

                        $_popup.addClass($_popup.data('active')).css({
                            'margin-top': -1*(_height_popup/2)
                        });
                        //e.stopPropagation();
                    }else{
                        $('#calendar_action_button').trigger('click');
                    }

                }
            }

            /**
             *
             * GLOBAL REGION
             * @author tuanPHPVN
             */

            /* If user is login - Bad code but i have to */
            var $_register_url = $( '#_register_url').length ? $( '#_register_url') : $( '._register_url');
            var $_login_url = $( '#_login_url').length ? $( '#_login_url') : $( '._login_url');
            var $_logout_url = $( '#_logout_url').length ? $( '#_logout_url') : $( '._logout_url');
            var $_my_account_url = $( '#_my_account_url').length ? $( '#_my_account_url') : $( '._my_account_url');

            /*@ticket 16210:  Apply add bookmark icon to the masthead*/
            var $_bookmark_view = $('.bookmark-view');

            if(APL.is_login) {

                /* hide register and login link */
                $_register_url.remove();
                $_login_url.remove();

                /* Logout link */
                $_logout_url.attr('href', APL.urls.logout_url);
                if ($_logout_url.find('a').length) {
                    $_logout_url.find('a').attr('href', APL.urls.logout_url)
                }
                $_logout_url.show();

                /* My Account link */
                $_my_account_url.attr( 'href', APL.urls.my_account_url );
                if ($_my_account_url.find('a').length) {
                    $_my_account_url.find('a').attr('href', APL.urls.my_account_url)
                }

                $_my_account_url.show();
                if(APL.is_my_account_page === "1" || APL.is_dashboard === "1") {
                    $_my_account_url.addClass('active');
                }

                /*@ticket 16210:  Apply add bookmark icon to the masthead*/
                $_bookmark_view.removeClass("hidden");
            }
            else {
                /* Hide logout and my account link */
                $_logout_url.remove();
                $_my_account_url.remove();

                /* Register */
                $_register_url.show();
                if(APL.is_register_page === "1") {
                    $_register_url.addClass('active');
                }

                $_register_url.attr( 'href', APL.urls.register_url );
                if ($_register_url.find('a').length) {
                    $_register_url.find('a').attr('href', APL.urls.register_url)
                }

                /* Login */
                $_login_url.show();
                if(APL.is_login_page === "1") {
                    $_login_url.addClass('active');
                }

                $_login_url.attr( 'href', APL.urls.login_url );
                if ($_login_url.find('a').length) {
                    $_login_url.find('a').attr('href', APL.urls.login_url)
                }

                /*@ticket 16210:  Apply add bookmark icon to the masthead*/
                $_bookmark_view.addClass("hidden");
            }
            /**
             *
             * COMMENT SINGLE EVENT
             * @author tuanPHPVN
             */
            /* Hanle Comment check by javascript. On the server we are already check. */
            var $form = $('#respond');
            var _trigger_error_bind_comment = false;


            $form.length && $form.find('#submit').on('click', function(e) {
                var $smbutton = $(this);

                /* hard code here. i don't know why form don't accept trigger('submit') */
                if($smbutton.data('no-error') === "true"){
                    return true;
                }




                var _selem = $form.data('elems');
                var _aelem = _selem.split(',');
                var _ssyncelem = $form.data('elems-sync').trim();
                var _asyncelem = _ssyncelem.trim().split(',');

                var _ishavesync = _ssyncelem !== "" && _asyncelem.length > 0;

                var checkError = function($elem, is_submit) {
                    var _achecker = $elem.data('check').split(',');
                    var __is_error = false;

                    $(_achecker).each(function(i, v) {
                        switch(v) {
                            case 'required':
                                if($elem.val().trim(" ") === "") {
                                    $($elem.data('error_holder')+' ._'+v).removeClass($elem.data('error_hide'));
                                    __is_error = true;
                                    return false; /* stop loop */
                                }
                                else {
                                    $($elem.data('error_holder')+' ._'+v).addClass($elem.data('error_hide'));
                                    return true;
                                }
                                break;

                            case 'email':
                                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                                if(!re.test($elem.val())) {
                                    $($elem.data('error_holder')+' ._'+v).removeClass($elem.data('error_hide'));
                                    __is_error = true;
                                    return false; /* stop loop */
                                }
                                else {
                                    $($elem.data('error_holder')+' ._'+v).addClass($elem.data('error_hide'));
                                    return true;
                                }
                                break;
                            case 'captcha':

                                /*This method is expensive - just check when sumit*/
                                if(!is_submit) return;

                                /* ajax calling */
                                /* block ui */
                                $form.block($.apl.blockUI);

                                /* send ajax request */
                                var _code = $elem.val();
                                $.ajax({
                                    'url': $elem.data('url_check_captcha')+'&code='+_code,
                                    'method': 'GET',
                                    success: function(response) {
                                        /* unblock ui */
                                        $form.unblock();

                                        /* If error */
                                        if(response.error !== false) {

                                            $($elem.data('error_holder')+' ._'+v).removeClass($elem.data('error_hide'));
                                            $($elem.data('error_holder')+' ._'+v).html('* '+response.msg);

                                            /* add new image */
                                            $($elem.data('captcha_image')).attr('src', response.new_image_link);
                                            $elem.val("");
                                            return true;
                                        }
                                        else {
                                            /* Success */
                                            $($elem.data('error_holder')+' ._'+v).addClass($elem.data('error_hide'));
                                            $smbutton.data('no-error', 'true');
                                            $smbutton.trigger('click');
                                        }
                                    },
                                    complete: function() {
                                        /* unblock ui */
                                        $form.unblock();
                                    }
                                });

                                break;
                            /* more checker add here... */
                        }
                    });

                    return __is_error;
                }

                /* at the first time */
                if (_trigger_error_bind_comment === false) {
                    $(_aelem).each(function(i, v) {
                        $(v).on('keyup', function() {
                            if(_trigger_error_bind_comment === false) return;

                            var $elem = $(this);
                            if($elem.data('error_holder') === undefined) console.error('This value cannot be null!');
                            if($elem.data('error_hide') === undefined) console.error('This value cannot be null!');

                            var _is_submit = false;
                            checkError($elem, _is_submit);

                        })
                    });
                }

                _trigger_error_bind_comment = true;

                var _is_error = false;
                $(_aelem).each(function(i, v) {
                    var $elem = $(v);
                    if($elem.length ===0 ) return true; /* continue loop */

                    var _is_submit = true;
                    if(checkError($elem, _is_submit)) {
                        _is_error = true;
                        return false; /* stop loop */
                    }
                });

                /* priority normal first */
                if(_is_error) return false;

                if(_ishavesync )
                {
                    $form.block($.apl.blockUI); /* depend on sync element */
                    return false;
                }

                return !_is_error;

            });

            /**
             * DASHBOARD
             * @author tuanPHPVN
             */
            //if( APL.is_search_program == '1' || APL.is_dashboard || APL['is_singular[event]'] === "1" || APL['is_singular[artist]'] === "1" || APL['is_singular[educator]'] === "1" ) {

            if ( APL.is_dashboard ) {
                /* Active parent click */
                if(typeof APL.dashboard_menu_active_info['data-acitve-by'] !== "undefined") {

                    $('nav.dsb-main-nav>ul>li').removeClass('active');
                    $('[data-tgr="'+APL.dashboard_menu_active_info['data-acitve-by']+'"]').parent('li').addClass('active');

                    if(!$.apl.is_mobile()) {
                        var curActivatedTab = $('[data-tgr="'+APL.dashboard_menu_active_info['data-acitve-by']+'"]');
                        curActivatedTab.addClass('is-loading-page');
                        curActivatedTab.trigger('click');
                    }

                }
                else if(typeof APL.dashboard_menu_active_info['class'] !== "undefined" ){
                    /* Single menu */
                    $('.'+APL.dashboard_menu_active_info['class']).parent().parent().addClass('active');
                }

                /* Active menu */
                if(typeof APL.dashboard_menu_active_info['id'] !== "undefined") {
                    /** @Ticket #13448 */
                    if( APL.dashboard_menu_active_info['id'] == '_apollo_user_dashboard_venue_reports') {
                        $('#_apollo_user_dashboard_venue_manage_list').addClass('active');
                    }
                    $('#'+APL.dashboard_menu_active_info['id']).addClass('active');
                }

                /* PROCESS SELECT ALL PATTERN */
                var $arr_checkall = $('[data-ride="ap-checkboxall"]');

                /* bind checkall */
                $arr_checkall.click(function() {
                    var $this = $(this);
                    var _data = $this.data();
                    var $childs = $(_data.child_selector);

                    if ($this.is(':checked') ) {
                        $childs.each(function() { //loop through each checkbox
                            this.checked = true;  //select all checkboxes with class "checkbox1"
                        });
                    }else{
                        $childs.each(function() { //loop through each checkbox
                            this.checked = false;  //select all checkboxes with class "checkbox1"
                        });
                    }

                });

                /* bind childs */
                var check_checkbox_all = function($checkbox_all) {
                    var _data = $checkbox_all.data();
                    var $childs = $(_data.child_selector);

                    var nchecked = 0;
                    $childs.each(function(i,v) {
                        if($(this).is(':checked')) {
                            ++nchecked;
                        }
                    });

                    if(nchecked === $childs.length) {
                        $checkbox_all[0].checked = true;
                    }
                    else {
                        $checkbox_all[0].checked = false;
                    }

                };

                var bind_child_checkbox_checker = function($checkbox_all) {

                    var _data = $checkbox_all.data();
                    var $childs = $(_data.child_selector);

                    $childs.on('click', function() {
                        /* find checked */
                        check_checkbox_all($checkbox_all);
                    });
                };

                $arr_checkall.each(function(i, v) {
                    var $this = $(this);
                    bind_child_checkbox_checker($this);

                });
                /* - END */

                /*Bookmark - Event*/
                typeof APL_DB_INLINE!== "undefined" && $(['event', 'organization', 'venue', 'artist', 'classified', 'public-art']).each(function(i, v) {
                    if(APL_DB_INLINE["items_"+v] !== undefined && APL_DB_INLINE["items_"+v] > APL_DB_INLINE.itemsOnPage) {
                        var _container_id = '#_apol_bm'+ v,
                            _items = parseInt(APL_DB_INLINE["items_"+v]);

                        $(_container_id + ' + .blk-paging').pagination({
                            items: _items,
                            itemsOnPage: APL_DB_INLINE.itemsOnPage,
                            ellipse: '<li>...</li>',
                            heading: '<span class="pg-tt">Pages:</span>',
                            ul: 'nav',
                            ulclass: 'paging',
                            prevText: '<',
                            nextText: '>',
                            onPageClick : function(pageNumber, event) {
                                var updatePage = function() {
                                    $.ajax({
                                        type: 'GET',
                                        url: APL_DB_INLINE.baselink,
                                        data: {
                                            action: 'apollo_get_bookmark',
                                            type: v,
                                            page: pageNumber,
                                            pagesize: APL_DB_INLINE.itemsOnPage
                                        },
                                        beforeSend: function() {
                                            $(window).block({
                                                message: APL.resource.loading.message,
                                                css: $.parseJSON(APL.resource.loading.image_style)
                                            });
                                        },

                                        success: function(data) {
                                            $(_container_id + ' .events').empty();
                                            $(_container_id + ' .events').append(data.html);
                                        },

                                        complete: function() {
                                            $(window).unblock();

                                            /* checkbox again */
                                            var $select_all = $(_container_id+' .check-event-all');
                                            check_checkbox_all($select_all);
                                            bind_child_checkbox_checker($select_all);

                                            /* recanculate map */
                                            $.apl.google_map.load_me_and_do("jQuery.apl.google_map.canculate_coordinate");
                                        }
                                    });
                                };

                                updatePage();
                            }
                        });
                    }
                });

                /*Bookmark - Event*/
                typeof APL_DB_INLINE_CUSTOM !== "undefined" && $(['organization', 'venue', 'artist', 'upcomming_event', 'past_event', 'draft_event', 'upcomming_program','expired_program', 'private_event', 'unconfirmed_event']).each(function(i, v) {
                    if(APL_DB_INLINE_CUSTOM["items_"+v] !== undefined && APL_DB_INLINE_CUSTOM["items_"+v] > APL_DB_INLINE_CUSTOM.itemsOnPage) {
                        var _container_id = '#_apol_'+ v,
                            _items = parseInt(APL_DB_INLINE_CUSTOM["items_"+v]);

                        $(_container_id + ' + .blk-paging').pagination({
                            items: _items,
                            itemsOnPage: APL_DB_INLINE_CUSTOM.itemsOnPage,
                            ellipse: '<li>...</li>',
                            heading: '<span class="pg-tt">Pages:</span>',
                            ul: 'nav',
                            ulclass: 'paging',
                            prevText: '<',
                            nextText: '>',
                            onPageClick : function(pageNumber, event) {
                                var lazyLoadListData = function() {
                                    $.ajax({
                                        type: 'GET',
                                        url: APL_DB_INLINE_CUSTOM.baselink,
                                        data: {
                                            action: 'apollo_get_multiple_post_dashboard_page',
                                            type: v,
                                            page: pageNumber,
                                            pagesize: APL_DB_INLINE_CUSTOM.itemsOnPage
                                        },
                                        beforeSend: function() {
                                            $(window).block({
                                                message: APL.resource.loading.message,
                                                css: $.parseJSON(APL.resource.loading.image_style)
                                            });
                                        },

                                        success: function(data) {
                                            $(_container_id + ' .events').empty();
                                            $(_container_id + ' .events').append(data.html);
                                        },

                                        complete: function() {
                                            $(window).unblock();
                                        }
                                    });
                                };

                                lazyLoadListData();
                            }
                        });
                    }
                });

                /* Remove bookmark selected */
                $('[data-ride="ap-checkaction"]').each(function () {
                    var $this = $(this),
                        _data = $this.data();

                    $this.on('click', function() {

                        /* Collect data */
                        var _alist_value = [],
                            _key = _data.data.split(':')[0],
                            _param = _data.data.split(':')[1];

                        if($(_data.data_source_selector).length == 0) {
                            alert(_data.alert);
                            return false;
                        }

                        $(_data.data_source_selector).each(function(_,elem) {
                            _alist_value.push($(elem).attr(_param));
                        });

                        var _slist_value = _alist_value.join(",");

                        var _adata = {};
                        _adata[_key] = _slist_value;

                        $.ajax({
                            url: _data.url,
                            data: _adata,
                            beforeSend: function() {
                                $(window).block({
                                    message: APL.resource.loading.message,
                                    css: $.parseJSON(APL.resource.loading.image_style)
                                });
                            },
                            success: function(data) {
                                if(data.status >= 1) {
                                    switch (_data.action_after) {
                                        case 'redirect':
                                            location.reload(); /*Refresh page*/
                                            break;
                                    }
                                }
                            },
                            complete: function() {
                                switch (_data.action_after) {
                                    case 'redirect':
                                        /* Do nothing */
                                        break;
                                    default:
                                        $(window).unblock();
                                        break;
                                }

                            }

                        })
                    });
                    /* Action */
                });


                /* Pagination */
                $('[data-ride="ap-pagination"]').each(function() {
                    var $this= $(this),
                        data = $this.data();

                    var d = {
                        items: 10,
                        itemsonpage: 2,
                        ellipse: '<li>...</li>',
                        heading: '<span class="pg-tt">Pages:</span>',
                        ul: 'nav',
                        ulclass: 'paging',
                        prevtext: '<',
                        nexttext: '>',
                        nextitemsonpage: 2
                    };

                    data = $.extend(d, data);

                    /*Show pagination if items > itemsOnPage*/
                    data.itemsonpage = parseInt(data.itemsonpage, 10);
                    data.item = parseInt(data.item, 10);

                    /* check require parameter */
                    if(
                        typeof data.baseurl === "undefined"
                        || typeof data.ajaxaction === "undefined"
                        || typeof data.nextitemsonpage === "undefined"
                    )
                    {
                        console.log('This information is nessessary');
                        return true;
                    }

                    if(data.items <= data.itemsonpage) return true; /* continue */
                    $this.pagination({
                        items: data.items,
                        itemsOnPage: data.itemsonpage,
                        ellipse: data.ellipse,
                        heading: data.heading,
                        ul: data.ul,
                        ulclass: data.ulclass,
                        prevText: data.prevtext,
                        nextText: data.nexttext,
                        onPageClick : function(pageNumber, event) {
                            var _data_form = {
                                action: data.ajaxaction,
                                page: pageNumber,
                                pagesize: data.nextitemsonpage
                            };

                            //add param 'module' to ajax
                            if(data.moduleApply) {
                                _data_form.module = data.moduleApply
                            }

                            // add param 'artist id' of multiple mode to ajax
                            if (data.artistId) {
                                _data_form.artistId = data.artistId;
                            }

                            var updatePage = function() {
                                $.ajax({
                                    type: 'GET',
                                    url: data.baseurl,
                                    data: _data_form,
                                    beforeSend: function() {
                                        $(window).block({
                                            message: APL.resource.loading.message,
                                            css: $.parseJSON(APL.resource.loading.image_style)
                                        });
                                    },

                                    success: function(response) {
                                        $(data.container_selector).empty();
                                        $(data.container_selector).append(response.html);
                                    },

                                    complete: function() {
                                        $(window).unblock();

                                        /* Implement after complete here! */
                                        if(typeof data.oncomplete !== 'undefined') {
                                            data.oncomplete();
                                        }
                                    }
                                });
                            };

                            updatePage();
                        }
                    });
                });

            }

            /* Tell Friend */
            /* bind event close */
            var bindCloseTellFriendPU = function() {
                $('.tellAF-popup .close').off('click').on('click', function(){
                    $('.tellAF-popup').css('display','none');
                    $('.tellafriend').remove();
                });
            };

            if (
                    APL.is_dashboard
                    || APL['is_singular[event]'] === "1"
                    || APL['is_singular[artist]'] === "1"
                    || APL['is_singular[educator]'] === "1"
                    || APL['is_singular[organization]'] === "1"
                    || APL['is_singular[venue]'] === "1"
                    || APL['is_singular[classified]'] === "1"
                    || APL['is_singular[public_art]'] === "1"
                    || APL['is_singular[post]'] === "1"
                    || APL['is_listing[post_type]'] === "1"
                    || APL['is_singular[news]'] === "1"
                    || APL['is_news_listing'] === "1"
            ) {

                $('[data-ride="ap-tellfriendbylink"]').each(function () {
                    var $this = $(this),
                        _data = $this.data();

                    $this.on('click', function() {

                        /* Collect data */
                        var _alist_value = [],
                            _key = _data.data.split(':')[0],
                            _param = _data.data.split(':')[1];


                        //listing page
                        if(APL['is_listing[blog_type]'] === "1"){
                            if ($(this).parent().find(_data.data_source_selector).length == 0) {
                                alert(_data.alert);
                                return false;
                            };
                            $(this).parent().find(_data.data_source_selector).each(function(_,elem) {
                                _alist_value.push($(elem).attr(_param));
                            });
                        }
                        //single page
                        else{
                            if ($(_data.data_source_selector).length == 0) {
                                alert(_data.alert);
                                return false;
                            };

                            $(_data.data_source_selector).each(function(_,elem) {
                                _alist_value.push($(elem).attr(_param));
                            });
                        }
                        var _slist_value = _alist_value.join(",");
                        var _adata = {};
                        _adata[_key] = _slist_value;
                        _adata['is_single'] = _data.single;

                        $.ajax({
                            url: _data.url,
                            data: _adata,
                            beforeSend: function() {
                                $(window).block({
                                    message: APL.resource.loading.message,
                                    css: $.parseJSON(APL.resource.loading.image_style)
                                });
                            },
                            success: function(data) {
                                if(data.status >= 1) {
                                    switch (_data.action_after) {
                                        case 'open:popup':
                                            $('body').append(data.html);
                                            $('body').append('<div class="tellafriend"></div>');
                                            $(".tellAF-popup").css({display: 'block'});

                                            bindCloseTellFriendPU();
                                            break;

                                    }
                                }
                            },
                            complete: function() {
                                switch (_data.action_after) {
                                    default:
                                        $(window).unblock();
                                        break;
                                }

                            }

                        });


                    });
                });

                // $("#apollo_submit_artist_photo").on('click', function(e) {
                //     e.preventDefault();
                //
                //     var $this = $(this),
                //         $center = $('#artist_center_info'),
                //         pid = $center.data('pid'), target = $center.data('target');
                //
                //     /**
                //      * ThienLD: handle ticket #12725 */
                //     if( ! window.APOLLO.formAbstractValidation.validateRequiredPrimaryImage()){
                //         $this.css('pointer-events', '');
                //         return false;
                //     }
                //
                //     /** Get featured image info */
                //     var featured_info = {
                //         file: $('.apl-upload-crop #_featured_file').val(),
                //         oname: $('.apl-upload-crop #_featured_oname').val(),
                //         type: $('.apl-upload-crop #_featured_type').val(),
                //         url: $('.apl-upload-crop #_featured_url').val(),
                //         is_deleted: $('.apl-upload-crop #_featured_is_deleted').val()
                //     };
                //
                //     /** Get gallery info */
                //     var galleries = $('#_apl_gallerys .gallery-item');
                //     var gallery_info = {};
                //     if( galleries.length > 0 ) {
                //         $(galleries).each(function(index, item) {
                //             var img_id = $(item).attr('img-id');
                //             var gFile = $(item).find('._apl_file').val();
                //             var gOname = $(item).find('._apl_oname').val();
                //             var gType = $(item).find('._apl_type').val();
                //             var gUrl = $(item).find('._apl_url').val();
                //             gallery_info[img_id] = {
                //                 file: gFile,
                //                 oname: gOname,
                //                 type: gType,
                //                 url: gUrl
                //             };
                //         });
                //     }
                //
                //     $.ajax({
                //         url: APL.ajax_url,
                //         data: {
                //             target: target,
                //             pid: pid,
                //             featured: featured_info,
                //             gallery: gallery_info,
                //             action: 'apollo_save_featured_and_gallery_image',
                //             _aplNonceField:$('#_aplNonceField').val(),
                //             current_page:'apollo_artist_gallery_page',
                //             'caption_gallerys[]': $('[name="caption_gallerys[]"]').map(function(){return $(this).val();}).get()
                //         },
                //         beforeSend: function() {
                //             $window.block($.apl.blockUI);
                //         },
                //         success: function(res) {
                //             if ( $('._apollo_success') ) {
                //                 $('._apollo_success').remove();
                //             }
                //
                //             if(res.status === 'ok') {
                //                 $(".submit-blk").before($('<div class="_apollo_success"> <i class="fa fa-check"></i> ' + $.apl.__('Success!') + ' </div>'));
                //             } else {
                //                 $(".submit-blk").before($('<div class="error ajax-error"> ' + $.apl.__('Cannot save data. Some error occurred!') + ' </div>'));
                //                 setTimeout(function(){
                //                     $(".ajax-error").remove();
                //                 }, 3000);
                //             }
                //             $this.css('pointer-events', '');
                //         },
                //         complete: function() {
                //             $window.unblock();
                //         }
                //     });
                // })
                //
                // //tri add event submit org photos
                // $('#apollo_submit_org_photo').on('click',function(e){
                //     e.preventDefault();
                //     var $this = $(this),
                //
                //         pid = $('#org_id').val(),
                //         target = $('#org_target').val();
                //
                //     /**
                //      * ThienLD: handle ticket #12725 */
                //     if( ! window.APOLLO.formAbstractValidation.validateRequiredPrimaryImage()){
                //         $this.css('pointer-events', '');
                //         return false;
                //     }
                //
                //     /** Get featured image info */
                //     var featured_info = {
                //         file: $('.apl-upload-crop #_featured_file').val(),
                //         oname: $('.apl-upload-crop #_featured_oname').val(),
                //         type: $('.apl-upload-crop #_featured_type').val(),
                //         url: $('.apl-upload-crop #_featured_url').val(),
                //         is_deleted: $('.apl-upload-crop #_featured_is_deleted').val()
                //     };
                //
                //     /** Get gallery info */
                //     var galleries = $('#_apl_gallerys .gallery-item');
                //     var gallery_info = {};
                //     if( galleries.length > 0 ) {
                //         $(galleries).each(function(index, item) {
                //             var img_id = $(item).attr('img-id');
                //             var gFile = $(item).find('._apl_file').val();
                //             var gOname = $(item).find('._apl_oname').val();
                //             var gType = $(item).find('._apl_type').val();
                //             var gUrl = $(item).find('._apl_url').val();
                //             gallery_info[img_id] = {
                //                 file: gFile,
                //                 oname: gOname,
                //                 type: gType,
                //                 url: gUrl
                //             };
                //         });
                //     }
                //
                //     $.ajax({
                //         url: APL.ajax_url,
                //         data: {
                //             target: target,
                //             pid: pid,
                //             featured: featured_info,
                //             gallery: gallery_info,
                //             action: 'apollo_save_featured_and_gallery_image',
                //             _aplNonceField:$('#_aplNonceField').val(),
                //             'caption_gallerys[]': $('[name="caption_gallerys[]"]').map(function(){return $(this).val();}).get()
                //         },
                //         beforeSend: function() {
                //             $window.block($.apl.blockUI);
                //         },
                //         success: function(res) {
                //             if ( $('._apollo_success') ) {
                //                 $('._apollo_success').remove();
                //             }
                //
                //
                //             if(res.status === 'ok') {
                //                 $(".submit-blk").before($('<div class="_apollo_success"> <i class="fa fa-check"></i> ' + $.apl.__('Success!.') + ' </div>'));
                //             } else {
                //                 $(".submit-blk").before($('<div class="error"> ' + $.apl.__('Cannot save data. Some error occurred!') + ' </div>'));
                //             }
                //             $this.css('pointer-events', '');
                //         },
                //         complete: function() {
                //             $window.unblock();
                //         }
                //     });
                // });

                //tri add event submit venue photos
                // $('#apollo_submit_venue_photo').on('click',function(e){
                //     e.preventDefault();
                //     var $this = $(this),
                //
                //         pid = $('#venue_id').val(),
                //         target = $('#venue_target').val();
                //
                //     /**
                //      * ThienLD: handle ticket #12725 */
                //     if( ! window.APOLLO.formAbstractValidation.validateRequiredPrimaryImage()){
                //         $this.css('pointer-events', '');
                //         return false;
                //     }
                //
                //     /** Get featured image info */
                //     var featured_info = {
                //         file: $('.apl-upload-crop #_featured_file').val(),
                //         oname: $('.apl-upload-crop #_featured_oname').val(),
                //         type: $('.apl-upload-crop #_featured_type').val(),
                //         url: $('.apl-upload-crop #_featured_url').val(),
                //         is_deleted: $('.apl-upload-crop #_featured_is_deleted').val()
                //     };
                //
                //     /** Get gallery info */
                //     var galleries = $('#_apl_gallerys .gallery-item');
                //     var gallery_info = {};
                //     if( galleries.length > 0 ) {
                //         $(galleries).each(function(index, item) {
                //             var img_id = $(item).attr('img-id');
                //             var gFile = $(item).find('._apl_file').val();
                //             var gOname = $(item).find('._apl_oname').val();
                //             var gType = $(item).find('._apl_type').val();
                //             var gUrl = $(item).find('._apl_url').val();
                //             gallery_info[img_id] = {
                //                 file: gFile,
                //                 oname: gOname,
                //                 type: gType,
                //                 url: gUrl
                //             };
                //         });
                //     }
                //
                //     $.ajax({
                //         url: APL.ajax_url,
                //         data: {
                //             target: target,
                //             pid: pid,
                //             featured: featured_info,
                //             gallery: gallery_info,
                //             action: 'apollo_save_featured_and_gallery_image',
                //             _aplNonceField:$('#_aplNonceField').val(),
                //             'caption_gallerys[]': $('[name="caption_gallerys[]"]').map(function(){return $(this).val();}).get()
                //         },
                //         beforeSend: function() {
                //             $window.block($.apl.blockUI);
                //         },
                //         success: function(res) {
                //             if ( $('._apollo_success') ) {
                //                 $('._apollo_success').remove();
                //             }
                //
                //             if(res.status === 'ok') {
                //                 $(".submit-blk").before($('<div class="_apollo_success"> <i class="fa fa-check"></i> ' + $.apl.__('Success!.') + ' </div>'));
                //             } else {
                //                 $(".submit-blk").before($('<div class="error"> ' + $.apl.__('Cannot save data. Some error occurred!') + ' </div>'));
                //             }
                //             $this.css('pointer-events', '');
                //         },
                //         complete: function() {
                //             $window.unblock();
                //         }
                //     });
                // });
            }
            selectPage();
            /* Bind selectbox to autocomplete */
            var checkEnabledRemoteDataToSelect2Box = function(selector,callback){
                $.ajax({
                    url:APL.ajax_url,
                    type:'POST',
                    dataType:'JSON',
                    data:{
                        action: 'apollo_check_apply_remote_data_for_select2_box',
                        element_to_post_type: encodeURI(selector)
                    },
                    success:function(result){
                        callback({
                            selector: selector,
                            isEnabled : result.enabled === "TRUE"
                        });
                    }
                });
            };

            $.fn.aplApplyRemoteDataToSelect2Box = function(){
                var data = this.data();
                if(typeof data.enableRemote === 'undefined' || typeof data.enableRemote == 0){
                    this.select2();
                } else {
                    this.select2({
                        ajax: {
                            url: APL.ajax_url,
                            dataType: 'json',
                            delay: 250,
                            data: function (params) {
                                return {
                                    q: params.term, // search term
                                    page: params.page,
                                    action: this.data().sourceUrl,
                                    post_type:this.data().postType,
                                    default_option: this.data().defaultOption != undefined ? this.data().defaultOption : ''
                                };
                            },
                            processResults: function (data, params) {
                                // parse the results into the format expected by Select2
                                // since we are using custom formatting functions we do not need to
                                // alter the remote JSON data, except to indicate that infinite
                                // scrolling can be used
                                params.page = params.page || 1;

                                return {
                                    results: data.items,
                                    pagination: {
                                        more: (params.page * 15) < data.total_count
                                    }
                                };
                            },
                            cache: true
                        },
                        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                        minimumInputLength: 0,
                        placeholder: data.defaultOption
                    });
                }
            };

            var listChosend = {
                "[id^='e-up-lists']": {},
                "[id^='e-past-lists']": {},
                "[name='_apl_artist_year']" : {},
                "[id^=e-up-lists]": {},
                "#_apl_event_term_primary": {},
                '#edu-prog-link': {},
                '#sel-events-in-artist': {},
                '#educator-provider': {},
                '#apl-us-states': {},
                '#apl-us-cities': {},
                '#apl-us-zip': {},
                '#apl-regions': {},
                '#apl-bs-type': {},
                '.apl-cus-select': {},
                "[id^='event-location-select']": {},
                '#right-event-location-select': {},
                "[id^='event-category-select']": {},
                "[name='event_city']": {},
                "[name='event_region']": {},
                '.chosen': {},
                "[name='archive-dropdown']" : {},
                "[id^='event-zipcode-select']": {},
                "#apl-neighborhood": {},
            };

            for (var selector in listChosend) {
                if(listChosend.hasOwnProperty(selector)
                    && $(selector).length > 0
                    && $(selector).data().enableRemote == 1
                ) {
                    $(selector).aplApplyRemoteDataToSelect2Box();

                } else if(listChosend.hasOwnProperty(selector) && $(selector).length > 0){
                    if (selector == "[name='event_city']") {
                        $(selector).select2({
                            templateResult: function(data) {
                                if (data.element && data.text != '') {
                                    return data.text + '<span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span>'; // optgroup
                                }
                                return data.text;
                            },
                            escapeMarkup: function(m) {
                                return m;
                            }
                        });
                    } else {
                        $(selector).select2();
                    }
                }
            };

            var _select2Selector = $('.apl_select2');
            if( _select2Selector.length > 0) {
                $.each(_select2Selector, function(i, v) {

                    var id = '#'+ $(v).attr('id');

                    if ($(v).data().enableRemote == 1) {
                        $(id).aplApplyRemoteDataToSelect2Box();
                    } else {
                        $(id).select2();
                    }
                });
            }

            $('body').on('add-more-co', function(e, d) {
                if ($(d.id).data().enableRemote == 1) {
                    $(d.id).aplApplyRemoteDataToSelect2Box();
                } else {
                    $(d.id).select2();
                }
            });
            //    } /* E-DB */

            //TriLm disable paging when reload page
            function selectPage(){
                var url      = window.location.href;
                var match = url.match('#page-([^.:]+)');
                if(match!=null){
                    var selectUrl = match[0];
                    var newUrl = url.replace(selectUrl,'');
                    window.history.pushState("object or string", "Title", newUrl);
                    //$('.paging').children('li').children('a[href='+selectUrl+']').trigger('click');
                }
            }

            var list = $('.des-list').children('.group-item');
            changeName(list.length);

            $(document).on('click','.add-new-group',function(){
                var content = $('.des-list').attr('data-html');
                $('.des-list').append(content);
                var button = $(this);
                var list = button.parent().parent().parent().children('.des-list').children('.group-item');
                changeName(list.length);
            });

            $(document).on('click','.delGroup',function(){
                var parent = $(this).parent();

                var list = parent.parent().children('.group-item');
                changeName(list.length - 1);
                parent.remove();
            });

            var list = $('.des-list').children('.group-item');
            changeName(list.length);

            function changeName(list){
                if(list != 0){
                    $('.add-new-group').text($('.add-new-group').data('addmore'));
                    $('.btn-preview').show();
                }else{
                    $('.add-new-group').text($('.add-new-group').data('add'));
                    $('.btn-preview').hide();
                }
            }

            $(document).on('click','.event-associated-vmore a',function(){
                $('.event-associated .toogle-item').toggleClass('hidden');
            });
        });
        /*--------------------------------------------------------------------------*/

        /**
         *
         * Submit Event form
         *
         * */

        var addHttp = function(){
            $(".apl_url").each(function(index, value){
                var v = $.trim($(this).val());
                if (!_.isEmpty(v) && v.indexOf("http") < 0){
                    $(this).val('http://'+v)
                }
            })
        }

        /*@ticket 17168 */
        $(document).on('focus','.apl_url',function(){
            if ( ! $(this).val() ) {
                $(this).val('http://');
            }
        });

        /*@ticket 17168 */
        $(document).on('focusout','.apl_url',function(){
            var value = $(this).val();
            if ( /http:\/{0,2}$/.test(value)) {
                $(this).val('');
            }
        });

        $('#Organization-frm').submit(function( ) {
            addHttp();
        });

        $('#venue-frm').submit(function( ) {
            addHttp();
        });

        $('#artist-frm').submit(function( ) {
            addHttp();
        });
        $('#Classified-frm').submit(function( ) {
            addHttp();
        });
        $('#Education-frm').submit(function( ) {
            addHttp();
        });
        $('#add-program-frm').submit(function( ) {
            addHttp();
        });

        $('#admin-frm-step1').submit(function( ) {
            addHttp();
        });




        $( '.apl-url-format' ).focus(function() {

            if ( ! $(this).val() ) {
                $(this).val('http://');
            }
        });

        /* Handle primary and additional category */
        var primary_elm = 'select[name="_apl_event_term_primary_id"]',
            primary_obj = $( primary_elm ),
            css_pro = 'opacity',
            css_val = '0.5',
            chb_obj = $( '.add-more-cat .cat-listing ul li input' ),
            label_obj = $( '.add-more-cat .cat-listing ul li label' );

        $( '.event-list' ).on( 'change', primary_elm, function() {
            var current_chb_obj = $( '.add-more-cat .cat-listing ul li input[value='+$( this ).val()+']' );

            // Enable all additional cat
            chb_obj.css( css_pro, 1 );
            label_obj.css( css_pro, 1 );

            // Disabled selected primary in additional cat
            current_chb_obj.attr( 'checked', false );
            current_chb_obj.css( css_pro, css_val );
            current_chb_obj.next( 'label' ).css( css_pro, css_val );

        });

        if ( ! primary_obj.val() ) {
            chb_obj.css( css_pro, css_val );
            label_obj.css( css_pro, css_val );
        }

        $( '.add-more-cat .cat-listing ul li' ).on( 'click', 'input', function () {
            if ( ! primary_obj.val() || $(this).val() == primary_obj.val() ) return false;
        });

        $( '.add-more-cat .cat-listing ul li' ).on( 'click', 'label', function () {
            if ( ! primary_obj.val() || $(this).prev().val() == primary_obj.val() ) {
                $(this).prev().attr( 'checked', false );
                return false;
            };
        });

        /* Handle show additional category */
        $( '.add-cat-expand' ).click( function() {
            var action = $(this).attr( 'data-action' ),
                mod    = $(this).attr( 'data-mod' );

            apollo_is_add_tmp( action, mod );

            // Change status of action after click
            if ( action === 'add' ) {
                $( this ).attr( 'data-action', 'hide' );
            } else {
                $( this ).attr( 'data-action', 'add' );
            }

            $(this).closest('.evt-blk.venue').find('.v-desc').toggleClass('hidden');

        });

        $( '.add-co' ).on( 'click', '.del', function() {
            $(this).parent().remove();
            if($('.more-org').length ===5){
                $('#add-co').parent().css('display','none');
            }else{
                $('#add-co').parent().css('display','block');
            }
        });

        // Change id of menu on tablet
        var tablet_menu_id = $( '.tablet-show .mn-menu ul' ).attr( 'id' );
        $( '.tablet-show .mn-menu ul' ).attr( 'id', tablet_menu_id + '-table' );

        // Preview videos
        $( '.video-btn #video-preview' ).click(function(e) {

            e.preventDefault();

            var $this = $(this),
                embedsStr = $.apl.videoFn.getFullEmbedSelector(),
                embeds = $( embedsStr ),
                is_empty = true;
            $.each( embeds, function( i, v) {
                if($(v).val().match(/^.+vimeo.com\/(.*\/)?([^#\?]*)/)){
                    var vimeoRegex = /(?:vimeo)\.com.*(?:videos|video|channels|)\/([\d]+)/i;
                    var parsed = $(v).val().match(vimeoRegex);
                    $(v).val("https://player.vimeo.com/video/" + parsed[1]);
                }
                if ( $(v).val() && ($.apl.videoFn.isYoutubeUrl($(v).val()) || $.apl.videoFn.isVimeoUrl($(v).val())) ) {
                    is_empty = false;
                }
            });

            if ( is_empty ){
                $(".video-item").parent().eq(0).append('<span class="error">'+ $( '.video-btn #video-preview').data('alert') +'</span>');
                setTimeout(function() {
                    $(".video-item").parent().eq(0).find('.error').fadeOut(300, function() {$(this).remove(); });
                }, 2500);

                return false;
            }

            $('.add-event-video-loader').show();
            var bottom_thumb       = '',
                video_wraper    = $('.video-item');
            $( '.video-box .wrapper-top-thumb' ).html( '' );

            var selected = '',
                has_selected = false;
            $.each( video_wraper, function ( i, v ) {

                var url = $(v).children('.el-blk.full').children(embedsStr).val();


                if ( ! has_selected && ! selected && ( $.apl.videoFn.isYoutubeUrl(url) || $.apl.videoFn.isVimeoUrl(url) ) ) {
                    selected = 'selected';
                    has_selected = true;
                } else {
                    selected = '';
                }
                var embedLink = $.apl.videoFn.getVideoEmbed(url),
                    src = $.apl.videoFn.getThumbByUrl(url),
                    img = src != undefined ? '<img src="'+src+'">' : '<img style="display: none" src="" /><span class="video-loading"><i class="fa fa-spinner fa-spin"></i></span>';
                if ( embedLink ) {
                    $( '.video-box .wrapper-top-thumb' ).append( '<div data-src="'+embedLink+'" data-target="'+ i +'" class="video '+ selected +'"><iframe src="'+embedLink+'"></iframe</div>' );
                    bottom_thumb += '<li style="float: left"><a href="#" data-id="'+ i +'">'+img+'</a></li>';
                }

            });


            // Re processing to auto fill the vimeo video
            $.each( video_wraper, function ( i, v ) {

                var url = $(v).children('.el-blk.full').children(embedsStr).val(),
                    imgSrc = $.apl.videoFn.getThumbByUrl(url);

                if ( $.apl.videoFn.isVimeoUrl(url) && imgSrc == undefined ) {
                    var thumbSelector = '[data-id="'+i+'"]';
                    $.apl.videoFn.processVimeoData(url, thumbSelector, function(res) {
                        $(res.selector+ ' img').attr('src', res.thumb).show();
                    });
                }
            });

            $( '.video-box .thumb-slider .slider-video' ).html( bottom_thumb );

            setTimeout(function() {
                $('.add-event-video-loader').hide();
            }, 1000 );

            resizeWindow();

            $(".thumb-slider").easySlider({
                continuous: false,
                nextId: "slider1next",
                prevId: "slider1prev"
            });
            $('.thumb-slider').width(($(window).width()*80)/100);

            var _lB = '<div class="light-box"></div>';
            $('body,html').scrollTop(0);
            $('body').append(_lB);
            $('.video-box').css('display','block');
            $('.video-box').fadeIn(1000, 'linear');
            $('.video-box').width($(window).width());
            $('.video-box').height($(window).height());
            $('.video').height($(window).height()-150);

            $('.closevideo').click(function(){
                $('.video-box').css('display','none');
                $('.light-box').remove();
                $('.wrapper-top-thumb' ).html('');
            });

            setTimeout(function() {
                $('.slider-video .video-loading').html('');
            }, 3000);
        });

        $(document).ready(function(){

            /** @Ticket #12931 */
            if(APL.venue_access_mode != 0) {
                var accessibility = $('.evt-blk.access');
                var newVenue = $('.access-mode');
                if (accessibility.length > 0 && !accessibility.hasClass('hidden') && newVenue.hasClass('newvenue')) {
                    accessibility.addClass('hidden');
                }
            }

            /** @Ticket #14857 */
            var $aplVideoThumb = $('.a-block-ct-video .apl-async-thumb-video');
            if ($aplVideoThumb.length > 0) {
                var curTabContent = $aplVideoThumb.closest('.pbx-wrapper');
                var videos = [];
                $.each($aplVideoThumb, function(index, item) {
                    var videoData = $(item);
                    videos.push({
                        code: videoData.attr('data-code'),
                        embed: videoData.attr('data-embed')  
                    });
                });
                $.ajax({
                    url: APL.ajax_url,
                    type: "POST",
                    dataType: "JSON",
                    data: {
                        'action': 'apollo_get_video_thumbnail',
                        'videos': videos
                    },
                    beforeSend: function () {
                        curTabContent.block($.apl.blockUI);
                    },
                    success: function (res) {
                        curTabContent.unblock();
                        if (!_.isEmpty(res) && !_.isEmpty(res.data)) {
                            $.each(res.data, function(index, item) {
                                var selectorImg = '.apl-async-thumb-video[data-code="'+ item.code +'"]';
                                var elementImg = $(selectorImg);
                                elementImg.attr('data-original', item.thumb);
                                elementImg.attr('src', item.thumb);
                            });
                        }
                    }
                });
            }

            /**
             * ThienLD : handle ticket #12725
             * */
            if(APL.is_dashboard == "1"){
                $('#admin-frm-step1 #arrow').off("click").on("click",window.APOLLO.formAbstractValidation.validateRequiredPrimaryImage);
            }

            // store the slider in a local variable
            var $window = $(window),
                flexslider = { vars:{} };

            
            function checkSliderActive() {
                var $slider = $('._js-btm-slider-wrap .bottom-slider');
                var disableClass = 'slider--disabled';
                $slider.map(function(i, e) {
                    var $element = $(e);
                    if ($element.find('.flex-disabled').length) {
                        $element.addClass(disableClass);
                    }
                    else {
                        $element.removeClass(disableClass);
                    }
                });
            }

            if($('._js-btm-slider-wrap .bottom-slider').length > 0 && typeof $.flexslider == 'function'){
                $('._js-btm-slider-wrap .bottom-slider').flexslider({
                    animation: "slide",
                    animationLoop: true,
                    controlNav: false,
                    directionNav: true,
                    itemWidth: 315,
                    itemMargin: 10,
                    minItems: getGridSize(),
                    maxItems: getGridSize(),
                    pauseOnHover : true,
                });
                checkSliderActive();
            }

            // check grid size on resize event
            $window.resize(function() {
                var gridSize = getGridSize();

                flexslider.vars.maxItems = gridSize;

                setTimeout(function() {
                    $('._js-btm-slider-wrap .bottom-slider').removeData('flexslider');
                    $('._js-btm-slider-wrap .bottom-slider').find('.flex-direction-nav').remove();
                    $('._js-btm-slider-wrap .bottom-slider').flexslider({
                            animation: "slide",
                            animationLoop: true,
                            controlNav: false,
                            directionNav: true,
                            itemWidth: 315,
                            itemMargin: 10,
                            minItems: getGridSize(),
                            maxItems: getGridSize(),
                            pauseOnHover : true,
                    })
                    if ($('._js-btm-slider-wrap .bottom-slider').find('.flex-viewport').length > 1) {
                        $('._js-btm-slider-wrap .bottom-slider').find('.flex-viewport:first-child').remove();
                    }
                    checkSliderActive();
                    }, 200);
            });

            if($('#org-is-restaurant').prop('checked') == false){
                $('.org-bs-area').hide();
            } else {
            }

            /* Thienld : hide group additional fields when has no field */
            $('.custom-fields.artist-blk').each(function(index,item){
                var divTitleCounter = $(item).find('div.title-bar-blk').length;
                var divEleCounter = $(item).find('div.el-blk').length;
                var ulEleCounter = $(item).find('ul.qualifi-list').length;
                var totalEle = divTitleCounter + divEleCounter + ulEleCounter;
                if( totalEle === 1){
                    $(item).remove();
                }
            });

            // Init components
            runCalendarIpt('c-ex');

            // $(document).on('click', '.nav-feature-event .tab-list li', function(e) {
            //     e.preventDefault();
            //     var _curTab = $(this).find('a').attr('data-id');
            //
            //     // load ajax to get featured event of specific featured category
            //     // only load when site not use cache and current tab is not load ajax before
            //     var isUseCache  = $('.blog-blk[data-target=' + _curTab + ']').attr('data-cache'),
            //         totalChilds = $('.blog-blk[data-target=' + _curTab + '] .list-blog').children().length;
            //     if ( isUseCache == '' && totalChilds == 0 ) {
            //         var $curTabContent = $('.blog-blk[data-target=' + _curTab + '] .list-blog');
            //         $.ajax({
            //             url: APL.ajax_url,
            //             type: "POST",
            //             dataType: "JSON",
            //             data: {
            //                 'action': 'apollo_get_home_featured_events_by_category',
            //                 'featured_category': _curTab,
            //             },
            //             beforeSend: function () {
            //                 $curTabContent.empty();
            //                 $curTabContent.block($.apl.blockUI);
            //             },
            //             success: function (res) {
            //                 $curTabContent.unblock();
            //                 if (!_.isEmpty(res) && res.success) {
            //                     $curTabContent.append(res.html);
            //                 }
            //             }
            //         });
            //     }
            // });
            $('.nav-feature-event .tab-list li').first().trigger('click');
        });

        $('#org-is-restaurant').off('click').on('click',function(){
            if($(this).prop('checked') == true){
                $('.org-bs-area').fadeIn('normal');
            } else {
                $('.org-bs-area').fadeOut('normal');
            }
        });

        /*@ticket #17907*/
        $('#Organization-frm #_org_business').off('click').on('click', function(){
            $('.org-business-fields').toggleClass('hidden');
        });

        /**
         * Keep the thumbnail
         * */
        $( $.apl.videoFn.parent ).on( 'change paste', $.apl.videoFn.embed, function() {
            $.apl.videoFn.processVideoData($(this).val());
        });

        $( '.video-btn #video-preview-org' ).click(function(e) {

            e.preventDefault();

            var embeds = $( 'input[name="embed[]"]' ),
                is_empty = true;

            function yt_valid_url(url) {
                if ( url == undefined ) return false;
                var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
                return (url.match(p)) ? RegExp.$1 : false;
            }

            $.each( embeds, function( i, v) {
                if ( $(v).val() && yt_valid_url( $(v).val() ) ) {
                    is_empty = false;
                }
            });
            if ( is_empty ){
                $(".video-item").parent().eq(0).append('<span class="error">* Please input valid youtube or vimeo link. </span>');
                setTimeout(function() {
                    $(".video-item").parent().eq(0).find('.error').fadeOut(300, function() {$(this).remove(); });
                }, 2500);

                return false;
            }

            $('.add-event-video-loader').show();
            var bottom_thumb       = '',
                video_wraper    = $('.video-item');
            $( '.video-box .wrapper-top-thumb' ).html( '' );

            var selected = '',
                has_selected = false;
            $.each( video_wraper, function ( i, v ) {

                var youtube_code = $(v).children('.el-blk.full').children('input[name="embed[]"]').val(),
                    real_code = youtube_code ? youtube_code.split('watch?v=') : '';

                if ( ! has_selected && ! selected && yt_valid_url( youtube_code ) ) {
                    selected = 'selected';
                    has_selected = true;
                } else {
                    selected = '';
                }

                if ( ! real_code[1] ) {
                    real_code = youtube_code ? youtube_code.split('youtu.be/') : '';
                }
                //embed/Pgehnv-xqAY
                if ( ! real_code[1] ) {
                    real_code = youtube_code ? youtube_code.split('youtube.com/embed/') : '';
                }

                if ( yt_valid_url(youtube_code) && real_code && real_code[1] && youtube_code ) {
                    $( '.video-box .wrapper-top-thumb' ).append( '<div data-target="'+ i +'" class="video '+ selected +'"><iframe src="http://www.youtube.com/embed/'+ real_code[1] +'"></iframe</div>' );
                    bottom_thumb += '<li style="float: left"><a href="#" data-id="'+ i +'"><img src="http://img.youtube.com/vi/'+ real_code[1] +'/0.jpg"></a></li>';
                }
            });

            $( '.video-box .thumb-slider .slider-video' ).html( bottom_thumb );

            setTimeout(function() {
                $('.add-event-video-loader').hide();
            }, 1000 );

            resizeWindow();

            $(".thumb-slider").easySlider({
                continuous: false,
                nextId: "slider1next",
                prevId: "slider1prev"
            });
            $('.thumb-slider').width(($(window).width()*80)/100);

            var _lB = '<div class="light-box"></div>';
            $('body,html').scrollTop(0);
            $('body').append(_lB);
            $('.video-box').css('display','block');
            $('.video-box').fadeIn(1000, 'linear');
            $('.video-box').width($(window).width());
            $('.video-box').height($(window).height());
            $('.video').height($(window).height()-150);

            $('.closevideo').click(function(){
                $('.video-box').css('display','none');
                $('.light-box').remove();
                $('.wrapper-top-thumb' ).html('');
            });
        });

        /* Handle select event for editting or copying */
        $( '.event-list' ).on( 'change', '#e-up-lists', function() {
            // Change href for editting
            var a_edit = $( '.up-events .dash-edit' ),
                a_copy = $( '.up-events .dash-copy' );
            a_edit.attr( 'href', a_edit.attr( 'data-href' )+ '/'+ $( this ).val() );
            a_copy.attr( 'href', a_copy.attr( 'data-href' )+ '/'+ $( this ).val() );
        });

        $( '.event-list' ).on( 'change', '#e-past-lists', function() {
            // Change href for editting
            var a_copy = $( '.past-events a' );
            a_copy.attr( 'href', a_copy.attr( 'data-href' )+ '/'+ $( this ).val() );
        });

        $( '.up-events' ).on( 'click', '.dash-edit, .dash-copy', function() {
            // Not select event
            if ( ! $( '#e-up-lists' ).val() ) {
                return false;
            }
        });

        $( '.past-events' ).on( 'click', '.dash-copy', function() {
            // Not select event
            if ( ! $( '#e-past-lists' ).val() ) {
                return false;
            }
        });

        $( '.video-area, .artist-video-area' ).on( 'click', '.delVideo', function() {
            var list = $(this).parent().parent();
            if ( $('.delVideo').length > 1 ) {
                $(this).parent().remove();
                setNameButtonAdd(list);
            } else {
                $(this).parent().hide();
            }


            $(this).find('input[name="video_deleted[]"]').val(1);
        });

        $('.sc.pin').click(function() {
            var href = $( this ).data().href;
            window.open( href ,"_blank" );
            return false;
        });

        // Bind data if client back in browser
        if ( $( '.up-events .dash-edit' ) ) {
            var a_edit = $( '.up-events .dash-edit' ),
                a_copy = $( '.up-events .dash-copy' ),
                a_past_copy = $( '.past-events a' );
            a_edit.attr( 'href', a_edit.attr( 'data-href' )+ '/'+ $( '#e-up-lists' ).val() );
            a_copy.attr( 'href', a_copy.attr( 'data-href' )+ '/'+ $( '#e-up-lists' ).val() );
            a_past_copy.attr( 'href', a_copy.attr( 'data-href' )+ '/'+ $( '#e-past-lists' ).val() );
        }

        // Add text search
        $( '#searchform input' ).val( APL.text_search );
        // Bind data if client back in browser
        if ( $( '.up-events .dash-edit' ) ) {
            var a_edit = $( '.up-events .dash-edit' ),
                a_copy = $( '.up-events .dash-copy' ),
                a_past_copy = $( '.past-events a' );
            a_edit.attr( 'href', a_edit.attr( 'data-href' )+ '/'+ $( '#e-up-lists' ).val() );
            a_copy.attr( 'href', a_copy.attr( 'data-href' )+ '/'+ $( '#e-up-lists' ).val() );
            a_past_copy.attr( 'href', a_copy.attr( 'data-href' )+ '/'+ $( '#e-past-lists' ).val() );
        }

        // Add text search
        $( '#searchform input' ).val( APL.text_search );




        /**
         * handle search
         */
        $( 'input[name="radioshowpg"]' ).click( function() {
            var url = $(this).data().href;
            var arrayUrl = window.APOLLO.CommonFunction.uRLToArray(url);
            _.remove(arrayUrl,'page');
            var newQueryString = window.APOLLO.CommonFunction.arrayToURL(arrayUrl);
            var newURL = url.match(new RegExp("[^?]+"));
            newURL = newURL + '?' + newQueryString;
            window.location.href = newURL;
            return false;
        });

        $( '.search-bkl .tab-list-search li a' ).click( function() {
            var href = $(this).data( 'href' );
            if(href.length > 0 && href != '#'){
                window.location.href = href;
            }
        });

        // Is one page
        if($(window).width() <= 768 && $( 'input[name="search-have-more"]' ) && $( 'input[name="search-have-more"]' ).val() == '1' ){
            $('#load-more').css('display','block');
        }


        function handle_resize_gallery() {
            var parent;
            if ( $('.a-block-ct-video').length ) {
                parent = $('.a-block-ct-video');
            } else {
                parent = $('.a-block-ct-photo');
            }

            var wrapper_width   = parent.width(),
                total_gallerys  = $( 'input[name="total-gallerys"]' ).val(),
                total_videos    = $( 'input[name="total-videos"]' ).val(),
                thumb_w         = 80;

            if ( wrapper_width <= total_gallerys * thumb_w ) {
                $('.a-block-ct-photo .bx-controls .bx-next ').show();
                $('.a-block-ct-photo .bx-controls .bx-prev ').show();
            } else {
                $('.a-block-ct-photo .bx-controls .bx-next ').hide();
                $('.a-block-ct-photo .bx-controls .bx-prev ').hide();
            }

            if ( wrapper_width <= total_videos * thumb_w ) {
                $('.a-block-ct-video .bx-controls .bx-next ').show();
                $('.a-block-ct-video .bx-controls .bx-prev ').show();
            } else {
                $('.a-block-ct-video .bx-controls .bx-next ').hide();
                $('.a-block-ct-video .bx-controls .bx-prev ').hide();
            }
        }

        /**
         * Handle hide video or gallery bx control
         * */
        $( window ).resize(function() {
            handle_resize_gallery();
        });
        handle_resize_gallery();


        $('a[data-rise="add-agency-venue"]').click( $.apl.send_id_obj );
        $('a[data-rise="assign-event-to-artist"]').click( $.apl.send_id_obj );
        $('a[data-rise="add-agency-educator"]').click( function() {
            var data = $(this).data();
            if ( ! $(data.source).val() ) return;

            window.location.href = data.url+ $(data.source).val();
        });

        $('select[data-rise="send-data"]').change( function() {
            var data = $(this).data();
            $( data.target ).attr( 'data-eid', $(this).val() );
        });


        $( 'a[data-ride="ap-artist-unassign-events-selected"]' ).click( $.apl.send_selector_ids);
        $( 'a[data-ride="ap-agency-remove-selected-venues"]' ).click( $.apl.send_selector_ids );

        /**
         * Handle click youtube video
         * */
        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        var player;

        $( '#video-pager a' ).click( function(e) {
            var $this = $(this);
            e.preventDefault();
            $('.a-block-ct-video .video-wrapper').html('<div class="lazyYT lazyYT-container lazyYT-video-loaded"><div id="player"></div></div>');
            $( '.vd-loader' ).show();

            player = new YT.Player('player', {
                videoId: ''+ $this.data().code +'',
                events: {
                    'onReady': on_player_ready,
                    'onStateChange': on_player_state_change
                }
            });

            /**
             * Play vimeo video
             * */
            if ( $this.data().vimeo ) {
                $('.a-block-ct-video .video-wrapper iframe').attr('src', $this.data().vimeo+ '?autoplay=1');

                try {
                    player.startVideo();
                }catch(e) {

                }

            }

            function on_player_ready(event) {
                event.target.playVideo();
            }

            var done = false;
            function on_player_state_change(event) {
                $( '.vd-loader' ).hide();
                if ( event.data == YT.PlayerState.PLAYING && !done ) {
                    setTimeout(stop_video, 6000);
                    done = true;
                }
            }
            function stop_video() {
                //player.stopVideo();
            }
        });/*-----------------------------------------------------------------------*/


        /* Search page */
        if($(window).width() <= 768 && $( 'input[name="search-have-more"]' ) && $( 'input[name="search-have-more"]' ).val() == '1'){
            if($('.search-bkl .blk-paging').length){
                $('.search-bkl .blk-paging').css('display','none');
                $('#load-more').css('display','block');
            }
        }

        /*
         * Remove some button on device
         **/
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            $( '.pt.print' ).hide();
            $( '.pt.email' ).hide();
            $( 'button[data-ride="ap-tellfriendbylink"]' ).hide();
        }

        $( '.closevideo' ).click( function() {
            //$('html,body').animate({ scrollTop: $('#event-video').offset().top }, 1000);
        });

        $('body').on( 'viewmore', function( v, callbackData ) {
            var haveMore = (typeof callbackData.have_more !== 'undefined' && callbackData.have_more === true);
            if (!haveMore) {
                var buttonLoadMoreId = '';
                var buttonLoadMoreClass = '';
                if (callbackData.containerListItems !== 'undefined' && callbackData.containerListItems != window ) {
                    buttonLoadMoreId = $(callbackData.containerListItems + ' #load-more');
                    buttonLoadMoreClass = $(callbackData.containerListItems + ' .load-more');
                } else {
                    buttonLoadMoreId = $('#load-more');
                    buttonLoadMoreClass = $('.load-more');
                }
                if ( buttonLoadMoreId.length > 0 ) {
                    buttonLoadMoreId.hide();
                }
                if (buttonLoadMoreClass.length > 0) {
                    buttonLoadMoreClass.hide();
                }
            }
            if(!_.isEmpty(callbackData.containerListItems) && $(callbackData.containerListItems).length > 0 && !haveMore ){
                $(callbackData.containerListItems).find('.more-frm-itm:last').attr("style","border-bottom:none");
            }
        });

        /*
         * Apply lazy load
         **/
        $('iframe.lazy').lazyload();
        $('img.lazy').lazyload( {
            'retina':true,
        } );

        $('.lazy-load-image-wrapper img').lazyload( {
            'retina':true,
        } );

        /**
         * Only lazy load youtube video
         * */
        var videoPaper = $( '#video-pager [data-code]' );
        if ( videoPaper && $(videoPaper[0]).data() && ! $(videoPaper[0]).data().vimeo ) {
            $('.lazyYT').lazyYT({
                display_duration: true,
                default_ratio: '16:9'

            });
        }


        var big_img = $( '.photo-box .photo' );

        $('.fullscreen').click(function(e){
            var caption;
            $.each( big_img, function(k, v) {
                caption = $(v).data().caption ? '<div class="photo-caption"><p>'+$(v).data().caption+'</p>' : '';
                $(v).html('<img src="'+$(v).data().src+'" />'+caption+'</div>');
            });

        });

        // Handle click on thumb gallery
        $( '.a-block-ct-photo #bx-pager a' ).click(function() {
            var current_img = $(this).attr('data-slide-index');
            if ( current_img == undefined ) current_img = 0;
            // Reset current image
            $( '.photo-box .thumb-slider li a' ).removeClass('current');
            $( '.photo-box .photo' ).removeClass( 'selected' );

            // Active new img
            $( '.photo-box .photo[data-target='+current_img+']' ).addClass( 'selected' );
            $( '.photo-box .thumb-slider li a[data-id='+current_img+']' ).addClass('current');

        });

        // Set screen size to cookie
        document.cookie='apl_scr_w='+$(window).width();

        /* blog */
        $( '.cate-blk.cmb a' ).click(function() {
            $('html,body').animate({ scrollTop: $('#respond').offset().top }, 1000);
        });

        $( '.art-email-label' ).click(function() {
            $( this ).parent().find('input[type="checkbox"]').trigger('click');
        });

        $( '.a-block-ct-video #video-pager img').length && $( '.a-block-ct-video #video-pager img' ).click( function() {
            var elm = $( this );
            $( '.video-description li p' ).html( elm.parent().data().desc );
        });

        $( '.a-block-ct-photo .photo-inner a.thumbnail-photo').length && $( '.a-block-ct-photo .photo-inner a.thumbnail-photo' ).click( function() {
            var elm = $( this );
            $( '.bx-description li p' ).html( elm.data().desc );
        });


        function apl_strip_tags( str ) {
            str=str.toString();
            return str.replace(/<\/?[^>]+>/gi, '');
        }

        $(document).on('click','.select-date',function(){
            $('.select-date').removeClass('active');
            var dataDay = $(this).attr('data-attr');
            var startDate = new Date();
            var endDate = new Date();
            switch(dataDay)
            {
                case 'today':
                    break;
                case 'tomorrow':
                    startDate = endDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate()+1);
                    break;
                case 'weekend':
                    var weekend = 6;
                    var currentDay = startDate.getDay();
                    var count = weekend - currentDay;
                    console.log(count);
                    startDate  = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate()+ count);
                    endDate = new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate()+ (count + 1) );
                    break;
                case 'seven-day':
                    endDate = new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate()+ 7);
                    break;
                case 'fourteen-day':
                    endDate = new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate()+ 14);
                    break;
                case 'thirsty-day':
                    endDate = new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate()+ 30);
                    break;
                default:
            }
            $('#save-lst-list').val(dataDay);
            $(this).addClass('active');

            // ticket #11487 : format date to m-d-Y
            var normalDateFormat = true;
            if ( $('input[name=date_format]') != 'undefined' && $('input[name=date_format]').val() == 'm-d-Y' ) {
                normalDateFormat = false;
            }

            startDate = formatDate(startDate, normalDateFormat);
            endDate = formatDate(endDate, normalDateFormat);
            $('input[name=start_date]').val(startDate);
            $('input[name=end_date]').val(endDate);
        });

        function formatDate(input, normalDateFormat){
            var year   = input.getFullYear().toString();
            var _month = input.getMonth().toString();
            var day    = input.getDate().toString();
            var month  = (parseInt(_month) + 1).toString();

            // START ticket #11487 : format date to m-d-Y
            var returnDate = '';
            if ( normalDateFormat ) {
                returnDate = year + '-' + (month[1] ? month : "0" + month[0]) + '-' + (day[1] ? day : "0" + day[0]);
            } else {
                returnDate = (month[1] ? month : "0" + month[0]) + '-' + (day[1] ? day : "0" + day[0]) + '-' + year;
            }
            // END ticket #11487 : format date to m-d-Y

            return returnDate;
        }

        $('input[name=start_date]').on('change',function(){
            resetListClassData();
            var endDateVal = $('input[name=end_date]').val();
            if(endDateVal.length <= 0){
                $('input[name=end_date]').val($(this).val());
            }
        });

        $('input[name=end_date]').on('change',function(){
            resetListClassData();
        });

        function resetListClassData(){
            checkStartDateEndDate();
            $('.weekend.select-date ').removeClass('active');
            $('#save-lst-list').val('');
        }

        function checkStartDateEndDate(){
            var startDate = $('input[name=start_date]').val();
            var endDate = $('input[name=end_date]').val();
            var start = new Date(startDate);
            var end = new Date(endDate);
            if(start > end){

                $('input[name=end_date]').val(startDate);
            }
            $('.weekend.select-date ').removeClass('active');
        }

        $('input[name=end_date]').on('change',function(){
            resetListClassData();
        });

        $(document).on('click','.btn-add-row',function(){
            var button = $(this);
            var list = button.parent().parent().children('.des-list');
            setNameButtonAdd(list);
        });

        function setNameButtonAdd(list){
            var countChild = list.children('.video-item').length;
            if(countChild <= 0){
                $('.btn-add-row').text($('.btn-add-row').data('add'));
                $('.btn-review').hide();
            }else{
                $('.btn-add-row').text($('.btn-add-row').data('addmore'));
                $('.btn-review').show();
            }
            console.log(countChild);
        };
    }


    /**
     * @author Trilm
     *
     */
     $(document).ready(function(){
         if(!APL.is_disable_solr){

             // Add the Type over the menu items
             $.widget( "custom.catcomplete", $.ui.autocomplete, {

                 _create: function() {
                     this._super();
                     this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
                 },

                 _resizeMenu: function() {
                     this.menu.element.css('height','auto');


                 },

                 _renderMenu: function( ul, items ) {

                     var that = this,
                         currentCategory = "";

                     $.each( items, function( index, item ) {
                         var li;
                         //if ( item.category != currentCategory ) {
                         //    ul.append( "<li class='ui-autocomplete-category " + item.category + "'>" + item.category + "</li>" );
                         //    currentCategory = item.category;
                         //}

                         li = that._renderItemData( ul, item );

                         if ( item.category ) {
                             li.attr( "aria-label", item.category + " : " + item.label );
                         }
                     });
                 },

                 _renderItem: function( ul, item ) {
                     return $( "<li>" )
                         .addClass(item.category)
                         .attr( "data-value", item.value )
                         .append( $( "<a>" ).text( item.label ) )
                         .appendTo( ul );
                 }
             });

             function keywords_go(href, search) {
                 if(search == 1) {
                     var action = $('#form_search_keywords').attr('action');
                     href = action + href;
                 }
                 document.location.href = href;
                 return false;
             }

             $(document).on('keyup',".solr-search-event-widget",function(){
                 var appendContent  = $(this).attr('data-parent-append');
                 $( ".solr-search-event-widget" ).catcomplete(configCatcomplete(appendContent));
             });

             function configCatcomplete(element){
                 return {
                     source: function (request, response) {
                         $(this).addClass('solr-loading');
                         $.ajax({
                             url: APL.ajax_url,
                             data: {
                                 term : request.term,
                                 action : 'apollo_solr_search'
                             },
                             success: function(data) {
                                 response(data);
                                 $('.solr-search-event-widget').removeClass('solr-loading');
                             },
                             error: function() {
                                 $('.solr-search-event-widget').removeClass('solr-loading');
                             }
                         });

                     },
                     minLength: 1,
                     select: function( event, ui ) {
                         if(ui.item.href) {
                             keywords_go( ui.item.href );
                         } else {
                             keywords_go( ui.item.label, 1 );
                         }
                     },
                     delay: 500,
                     appendTo : element
                 };
             }
         }

         $(document).on('keypress keydown','#frm-map-search',function(e){
             var key = e.which;
             if(key == 13)  // the enter key code
             {
                 $(this).find('#filter-button').trigger('click');
                 return false;
             }
         });
     });

})(jQuery);


/**
 * Apollo uploads
 * */
(function($) {
    $(function() {

        var $featureIsModifying = jQuery('.apl-upload-crop #_featured_is_new_or_modify');
        var $featureIsDeleting = jQuery('.apl-upload-crop #_featured_is_deleted');
        var $featureImgID = jQuery('.apl-upload-crop #_featured_img_id');

        if ( typeof plupload != "undefined" && ($('#crop-btn').length || $('#_upload_gallery_btn').length) ) {
            /**
             * Upload and crop
             * */
            var
                obj_min_size_allow = {w: $('.apl-upload-crop [name="min_size_w"]').val(), h: $('.apl-upload-crop [name="min_size_h"]').val()},
                pid = $('.apl-upload-crop [name="pid"]').val(),
                target = $('.apl-upload-crop [name="target"]').val(),
                nonce = $('.apl-upload-crop [name="nonce"]').val(),
                $upload_container_sel   = $("#upload_and_drop_container .upload-img"),
                $crop_contianer_sel     = $("#upload_and_drop_container .crop-img"),
                $primary_img_sel = $("#upload_and_drop_container .crop-img #_primary_img"),
                obj_actual_size = { height: 0, widht: 0 },
                obj_browser_size = { height: 0, widht: 0 },
                default_info_color = 'black',
                obj_want_size = {w: 0, h:0, x: 0, y:0 },
                jcrop_api = undefined,
                $window = $(window)
            ;
            var translate = $('#apl-translate');

            ///////  ImageAreaSelect

            var initImgAreaSelect = function() {

                obj_actual_size.width = parseInt($primary_img_sel.attr('data-o_width'), 10);
                obj_actual_size.height = parseInt($primary_img_sel.attr('data-o_height'), 10);

                obj_browser_size.width = $primary_img_sel.width();
                obj_browser_size.height = $primary_img_sel.height();

                try {
                    $primary_img_sel.Jcrop({
                        allowSelect: true
                        , boxWidth: 0   //Maximum width you want for your bigger images
                        , boxHeight: 0  //Maximum Height for your bigger images
                        , trueSize: [obj_actual_size.width,obj_actual_size.height]
                        , keySupport: false
                        , onChange: function(c) {
                            var _w = Math.round(c.w);
                            var _h = Math.round(c.h);

                            obj_want_size = {w:_w, h: _h, x: c.x, y: c.y};

                            var _info = $('<div class="info" style=" position: absolute; top:50%; left: 50%; width: 70px; height: 14px; font-size: 14px; margin-top: -7px; margin-left: -35px; text-align: center; background-color: #fff; color: '+default_info_color+'">'+ _w+'x'+ _h+'</div>');
                            this.ui.selection.find('.jcrop-tracker').empty().append(_info);

                        }
                    }, function() {
                        jcrop_api = this;
                    });
                } catch(e) {

                }
            };

            if($primary_img_sel.attr('src') !== '') {
                initImgAreaSelect();
            }

            $primary_img_sel.on('load', function(event) {
                initImgAreaSelect();
            });

            /////// CROP DROP

            $(document).on('click','#crop-btn', function (event) {
                event.preventDefault();
                var obj_min_size_allow = {w: $('.apl-upload-crop [name="min_size_w"]').val(), h: $('.apl-upload-crop [name="min_size_h"]').val()};

                if(obj_want_size.h === 0 || obj_want_size.w === 0) {
                    alert( translate.attr('data-select-msg') );
                    return false; /* Do nothing */
                }

                if(obj_want_size.h < obj_min_size_allow.h && obj_min_size_allow.h !== 'auto') {
                    var min_height_msg = translate.attr('data-drop-min-height');
                    alert( min_height_msg.replace("%s", obj_min_size_allow.h) );
                    return false;
                }

                if(obj_want_size.w < obj_min_size_allow.w && obj_min_size_allow.w !== 'auto') {
                    var min_width_msg = translate.attr('data-drop-min-width');
                    alert( min_width_msg.replace("%s", obj_min_size_allow.w) );
                    return false;
                }

                // Validate square image
                if ($('#select-image').attr('data-square') == 1 && obj_want_size.w != obj_want_size.h) {

                    var upload_error_msg = translate.attr('data-upload-event-must-square');
                    var mapObj = {
                        '%s2':obj_min_size_allow.w,
                        '%s3':obj_min_size_allow.h,
                    };
                    var _error_msg = upload_error_msg.replace(/%s1|%s2|%s3/gi, function(matched){
                        return mapObj[matched];
                    });
                    alert(_error_msg);
                    return false;
                }

                /* Render ajax to server and get response */
                /** @Ticket #12729 Get featured image info */
                /** Get featured image info */
                var featured_info = {
                    img_id: $featureImgID.val(),
                    file: $('.apl-upload-crop #_featured_file').val(),
                    oname: $('.apl-upload-crop #_featured_oname').val(),
                    type: $('.apl-upload-crop #_featured_type').val(),
                };

                $.ajax({
                    url: APL.ajax_url,
                    data: {
                        action: 'apollo_upload_and_drop_actually',
                        history: JSON.stringify(
                            [{"c":{"x":obj_want_size.x,"y":obj_want_size.y,"w":obj_want_size.w,"h":obj_want_size.h}}]
                        ),
                        pid: pid,
                        target: target,
                        featured: featured_info
                    },
                    beforeSend: function( ) {
                        $window.block($.apl.blockUI);
                    },
                    success: function(resp) {
                        if ( jcrop_api != undefined ) {
                            jcrop_api.destroy();
                            $primary_img_sel.removeAttr('style');
                            jcrop_api = undefined;
                        }

                        $primary_img_sel.attr('src', resp.data.file.url + '?t=' + new Date().getTime());

                        $primary_img_sel.attr('data-o_width', resp.data.file.meta.width);
                        $primary_img_sel.attr('data-o_height', resp.data.file.meta.height);

                        obj_want_size = {w: 0, h: 0, x: 0, y: 0};

                        /** set data to input hidden */
                        $('.apl-upload-crop #_featured_file').val(resp.data.file.file);
                        $('.apl-upload-crop #_featured_oname').val(resp.data.file.oname);
                        $('.apl-upload-crop #_featured_type').val(resp.data.file.type);
                        $('.apl-upload-crop #_featured_url').val(resp.data.file.url);
                        $featureIsDeleting.val(1);
                        $featureIsModifying.val(1);
                    },
                    complete: function() {
                        $window.unblock();
                    }
                });
            });

            /////// CANCEL DROP
            $(document).on('click','#cancel-crop', function (e) {
                e.preventDefault();

                $.ajax({
                    url: APL.ajax_url,
                    data: {
                        action: 'apollo_upload_and_drop_remove',
                        pid: pid,
                        target: target
                    },
                    beforeSend: function() {
                        $window.block($.apl.blockUI);
                    },
                    success: function(resp) {
                        if ( jcrop_api != undefined ) {
                            jcrop_api.destroy();
                            $primary_img_sel.removeAttr('style');
                            jcrop_api = undefined;
                        }
                    },
                    complete: function() {

                        $upload_container_sel.removeClass('hidden');
                        $crop_contianer_sel.addClass('hidden');
                        $primary_img_sel.attr('src', '');

                        /**
                         * @ticket #18695: validate primary photo (remove photo)
                         * @type {*|HTMLElement}
                         */
                        var featured_file = $('.apl-upload-crop #_featured_file');
                        if (featured_file.length){
                            featured_file.val('');
                        }

                        var featured_url = $('.apl-upload-crop #_featured_url');
                        if (featured_url.length){
                            featured_url.val('');
                        }

                        obj_want_size = {w: 0, h: 0, x: 0, y: 0};

                        $featureIsDeleting.val(1);

                        $window.unblock();
                    }
                });
            });


            /////// FILTER PLUPLOAD

            plupload.addFileFilter('min_img_size', function(min_img_size, file, cb) {

                var self = this, img = new o.Image();

                function finalize(result, msgAttr) {
                    // cleanup
                    img.destroy();
                    img = null;

                    // if rule has been violated in one way or another, trigger an error
                    if (!result) {
                        var upload_error_msg = translate.attr(typeof msgAttr == 'undefined' ?  'data-upload-error-msg' : msgAttr);
                        var mapObj = {
                            '%s1':file.name,
                            '%s2':min_img_size.w,
                            '%s3':min_img_size.h,
                        };
                        var _error_msg = upload_error_msg.replace(/%s1|%s2|%s3/gi, function(matched){
                            return mapObj[matched];
                        });
                        self.trigger('Error', {
                            message : _error_msg,
                            file : file
                        });

                    }
                    cb(result);
                }

                img.onload = function() {
                    // check if resolution cap is not exceeded
                    if(min_img_size.w === 'auto' || min_img_size.h === 'auto')  {
                        finalize(true);
                    }
                    else {
                        var conditionSize = img.width >= min_img_size.w && img.height >= min_img_size.h;
                        if (!conditionSize) {
                            finalize(false, 'data-upload-error-msg');
                        }
                        else { // Whether must be square or not
                            finalize($('#select-image').attr('data-square') == 1 ? img.width == img.height : true, 'data-upload-event-must-square');
                        }
                    }
                };

                img.onerror = function() {
                    finalize(false);
                };

                img.load(file.getSource());
            });

            var upload_and_drop = new plupload.Uploader({
                runtimes : 'html5,flash,silverlight,html4',

                browse_button : 'select-image', // you can pass in id...
                container: document.getElementById('upload_and_drop_container'), // ... or DOM Element itself

                url : APL.ajax_url + '?action=apollo_upload_and_drop&target='+target+'&pid='+pid+'&nonce='+nonce+'',

                filters : {
                    //                    max_file_size : APL.upload_image.filters.max_file_size,
                    mime_types: [
                        {title : translate.attr('data-image-file'), extensions : "jpg,png,jpeg"}
                    ],
                    min_img_size: {'w': obj_min_size_allow.w, 'h': obj_min_size_allow.h }
                },

                // Flash settings
                flah_swf_url : APL.flah_swf_url,

                // Silverlight settings
                silverlight_xap_url : APL.silverlight_xap_url,

                init: {

                    BeforeUpload: function(up, file) {
                        upload_and_drop.settings.multipart_params = {'id': file.id };
                    },

                    StateChanged: function(up) {
                        $window.block($.apl.blockUI);
                    },

                    FilesAdded: function(up, files) {

                        var maxfiles = 1;
                        if(up.files.length > maxfiles )
                        {
                            up.files.splice(maxfiles);
                            alert( translate.attr('data-one-file') );
                        }

                        upload_and_drop.start();
                    },

                    UploadProgress: function(up, file) {
                    },

                    FileUploaded: function(up, file, info) {

                        var res = JSON.parse(info['response']);

                        if(res.error === true) {
                            upload_and_drop.trigger('Error', {
                                code : plupload.GENERIC_ERROR,
                                message : res.data.msg + "."
                            });

                            return false;
                        }

                        $upload_container_sel.addClass('hidden');
                        $crop_contianer_sel.removeClass('hidden')

                        $primary_img_sel.attr('src', res.data.file.url);
                        $primary_img_sel.attr('data-o_width', res.data.file.meta.width);
                        $primary_img_sel.attr('data-o_height', res.data.file.meta.height);

                        /** set data to input hidden */
                        $('.apl-upload-crop #_featured_file').val(res.data.file.file);
                        $('.apl-upload-crop #_featured_oname').val(res.data.file.oname);
                        $('.apl-upload-crop #_featured_type').val(res.data.file.type);
                        $('.apl-upload-crop #_featured_url').val(res.data.file.url);
                        $featureIsDeleting.val(1);
                        $featureIsModifying.val(1);
                    },

                    UploadComplete: function(up, files) {
                        // Called when all files are either uploaded or failed
                        upload_and_drop.splice();
                        upload_and_drop.refresh();

                        $window.unblock();

                    },

                    Error: function(up, err) {
                        /** @Ticket #15077 FILE_EXTENSION_ERROR*/
                        if (err.code == plupload.FILE_EXTENSION_ERROR) {
                            var customMsg = $('.apl-upload-crop');
                            if (customMsg.length > 0 && typeof customMsg.attr('data-message-error') !== 'undefined') {
                                alert(customMsg.attr('data-message-error'));
                            } else {
                                alert(err.message);
                            }
                        } else {
                            alert(err.message);
                        }
                    }
                }
            });

            /* init upload */
            upload_and_drop.init();

            /** End  Upload and crop ***********************************************/


            /**
             * Upload gallerys
             * */
            var gallery_target = $('.apl-gallery-upload [name="target"]').val(),
                gallery_pid = $('.apl-gallery-upload [name="pid"]').val(),
                gallery_maxitem = $('.apl-gallery-upload [name="maxitem"]').val(),
                gallery_ext = $('.apl-gallery-upload [name="ext"]').val(),
                gallery_min_size_w = $('.apl-gallery-upload [name="min_size_w"]').val(),
                gallery_min_size_h = $('.apl-gallery-upload [name="min_size_h"]').val(),

                gallery_maxw = $('.apl-gallery-upload [name="maxw"]').val(),
                gallery_maxh = $('.apl-gallery-upload [name="maxh"]').val();

            var maxfiles = parseInt(gallery_maxitem, 10),
                left_number = 0,
                max_file_size = $('.apl-gallery-upload [name="maxfilesize"]').val()
            ;

            /* Update center info. It nesessary because used for other action ajax. */
            var $center_info = $("#apollo_upload_gallery").parent();
            $center_info.attr('data-target', gallery_target);
            $center_info.attr('data-pid', gallery_pid);

            var
                obj_min_size_allow = {w: gallery_min_size_w, h: gallery_min_size_h},
                obj_max_size_allow = {w: gallery_maxw, h: gallery_maxh},
                $window = $(window),
                $body = $('body'),
                _template_box = $("#_box_template").html()
                , _list_template = $("#_list_template").html()
                , $gallery_console = $("#_gallery_console")
                , $galler_list = $("#_galler_list")
                , $save_gallery_btn = $("#_upload_gallery_btn")
                , $gallery_box = $("#_gallery_box")

            ;

            var bind_plus_upload = function (n_uploaded_image) {
                var maxfiles = parseInt(gallery_maxitem, 10);

                if(n_uploaded_image >= maxfiles) return ; /* Do not need plus any more */

                left_number = maxfiles - n_uploaded_image;

                $galler_list.append('<li><a href="javascript:void(0);" id="_upload_gallery_btn_plus" class="plus"><i class="fa fa-plus-circle fa-3x"></i></a></li>');

                /* Bind plupload */
                var uploader_gallery_plus = new plupload.Uploader({
                    runtimes : 'html5,flash,silverlight,html4',

                    browse_button : '_upload_gallery_btn_plus', // you can pass in id...
                    container: document.getElementById('_upload_gallery'), // ... or DOM Element itself

                    url : APL.ajax_url + '?action=apollo_upload_gallery&pid=' + gallery_pid + '&target=' + gallery_target,

                    filters : {
                        max_file_size : max_file_size,
                        mime_types: [
                            {title : translate.attr('data-image-file'), extensions : gallery_ext}
                        ],
                        min_img_size_gallery: { 'w': obj_min_size_allow.w, 'h': obj_min_size_allow.h }, // 1MP = 1 million pixels
                        max_img_size_gallery: { 'w': obj_max_size_allow.w, 'h': obj_max_size_allow.h } // 1MP = 1 million pixels
                    },

                    // Flash settings
                    flah_swf_url : APL.flah_swf_url,

                    // Silverlight settings
                    silverlight_xap_url : APL.silverlight_xap_url,

                    init: {

                        BeforeUpload: function(up, file) {
                            uploader_gallery_plus.settings.multipart_params = {'uid': file.id};
                        },

                        FilesAdded: function(up, files) {

                            if(up.files.length > left_number )
                            {
                                up.files.splice(left_number);
                                var max_error_msg = translate.attr('data-max-upload-gallery');
                                alert(max_error_msg.replace('%s',maxfiles));
                            }

                            plupload.each(up.files, function(file) {
                                //document.getElementById('_gallery_box').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
                                var _html = _template_box.replace('{i}', file.id).replace('{filename}', file.name).replace('{filesize}', plupload.formatSize(file.size))
                                var $gallery_box = $("#_gallery_box");
                                if($gallery_box.find("li:last").length === 0) {
                                    $gallery_box.append(_html);
                                }else {
                                    $gallery_box.find('li:last').after(_html);
                                }

                            });

                            uploader_gallery_plus.start();
                        },

                        UploadProgress: function(up, file) {
                            $('#_gallery_box_'+file.id+' .status span').css('width', file.percent + '%');
                        },

                        FileUploaded: function(up, file, info) {

                            var res = JSON.parse(info['response']);
                            var href = 'javascript:void(0);';

                            if(res.error === true) {
                                uploader_gallery_plus.trigger('Error', {
                                    code : plupload.GENERIC_ERROR,
                                    message : res.data.gallerys.msg + "."
                                });

                                return false;
                            }

                            $(res.data.gallerys).each(function(_, data) {
                                var $_gallery_box_id = $('#_gallery_box_'+data.uid);
                                if(data.error === true) {
                                    $_gallery_box_id.remove();

                                    return true; /* continue */
                                }

                                var org_gallery = $('#_apl_gallerys');
                                if (org_gallery.length > 0) {
                                    org_gallery.append(data.input_hidden);
                                }

                                /*
                                * @Ticket 15203
                                * TO DO: waiting the refactor - page builder
                                */
                                $galler_list.find('li._list_:last').after(_list_template.replace(/{i}/g, data.uid).replace('{href}', href).replace('{src}', data.url));

                                var _overview = $_gallery_box_id.find('.overview');

                                _overview.attr('src', data.url);
                                _overview.removeClass('hidden');
                            });

                            $save_gallery_btn.addClass('hidden');
                        },

                        UploadComplete: function(up, files) {
                            if(files.length >= left_number) {
                                uploader_gallery_plus.destroy();
                                $("#_upload_gallery_btn_plus").parent().remove();
                                left_number = 0;
                            }
                            else {
                                left_number -= files.length;
                            }

                            uploader_gallery_plus.splice();
                            uploader_gallery_plus.refresh();

                        },

                        Error: function(up, err) {
                            if($gallery_console.css('display') === 'none') {
                                $gallery_console.fadeIn('fast');
                            }

                            if(err.code === plupload.FILE_SIZE_ERROR) {

                                var max_size_msg = translate.attr('data-max-size-gallery');
                                var mapObj = {
                                    '%s1':err.file.name,
                                    '%s2':(parseInt(max_file_size, 10) / (1024 * 1024)).toFixed(2),
                                };
                                var _error_size_gallery_msg = max_size_msg.replace(/%s1|%s2/gi, function(matched){
                                    return mapObj[matched];
                                });
                                $gallery_console.append("<div>" + _error_size_gallery_msg + "</div>");
                            }
                            else {
                                $gallery_console.append("<div>" + err.message + "</div>");
                            }

                            var idtimer = setTimeout(function () {
                                $gallery_console.fadeOut('slow');
                                $gallery_console.empty();
                            }, 8000);

                            $.fn.pluploader["gallery.timers"].push(idtimer);
                        }
                    }
                });

                /* init upload */
                uploader_gallery_plus.init();
            };


            plupload.addFileFilter('min_img_size_gallery', function(obj_min_size_allow, file, cb) {

                var self = this, img = new o.Image();

                function finalize(result) {

                    // if rule has been violated in one way or another, trigger an error
                    if (!result) {
                        var upload_error_msg = translate.attr('data-gallery-min-resolution-msg');
                        var mapObj = {
                            '%s1':file.name,
                            '%s2':obj_min_size_allow.w,
                            '%s3':obj_min_size_allow.h,
                        };
                        var _error_msg = upload_error_msg.replace(/%s1|%s2|%s3/gi, function(matched){
                            return mapObj[matched];
                        });
                        self.trigger('Error', {
                            message : _error_msg,
                            file : file
                        });

                    }
                    cb(result);
                }

                img.onload = function() {
                    // check if resolution cap is not exceeded
                    if(obj_min_size_allow.w === 'auto' || obj_min_size_allow.h === 'auto') {
                        finalize(true);
                    }
                    else {
                        finalize(img.width >= obj_min_size_allow.w && img.height >= obj_min_size_allow.h);
                    }

                };

                img.onerror = function() {
                    finalize(false);
                };

                img.load(file.getSource());
            });
            plupload.addFileFilter('max_img_size_gallery', function(obj_max_size_allow, file, cb) {

                var self = this, img = new o.Image();

                function finalize(result) {
                    // cleanup
                    img.destroy();
                    img = null;

                    // if rule has been violated in one way or another, trigger an error
                    if (!result) {
                        var upload_error_msg = translate.attr('data-gallery-max-resolution-msg');
                        var mapObj = {
                            '%s1':file.name,
                            '%s2':obj_max_size_allow.w,
                            '%s3':obj_max_size_allow.h,
                        };
                        var _error_msg = upload_error_msg.replace(/%s1|%s2|%s3/gi, function(matched){
                            return mapObj[matched];
                        });
                        self.trigger('Error', {
                            message : _error_msg,
                            file : file
                        });

                    }
                    cb(result);
                }

                img.onload = function() {
                    // check if resolution cap is not exceeded
                    if(obj_max_size_allow.w === 'auto' || obj_max_size_allow.h === 'auto') {
                        finalize(true);
                    }
                    else {
                        finalize(img.width < obj_max_size_allow.w && img.height < obj_max_size_allow.h);
                    }

                };

                img.onerror = function() {
                    finalize(false);
                };

                img.load(file.getSource());
            });

            $.fn.pluploader = {
                'gallery.hasshowerror': false,
                'gallery.timers': []
            };

            if($('#_upload_gallery').length > 0){
                var uploader_gallery = new plupload.Uploader({
                    runtimes : 'html5,flash,silverlight,html4',

                    browse_button : '_upload_gallery_btn', // you can pass in id...
                    container: document.getElementById('_upload_gallery'), // ... or DOM Element itself

                    url : APL.ajax_url + '?action=apollo_upload_gallery&pid=' + gallery_pid + '&target=' + gallery_target,

                    filters : {
                        max_file_size : max_file_size,
                        mime_types: [
                            {title : translate.attr('data-image-file'), extensions : gallery_ext}
                        ],
                        min_img_size_gallery: { 'w': obj_min_size_allow.w, 'h': obj_min_size_allow.h }, // 1MP = 1 million pixels
                        max_img_size_gallery: { 'w': obj_max_size_allow.w, 'h': obj_max_size_allow.h } // 1MP = 1 million pixels
                    },

                    // Flash settings
                    flah_swf_url : APL.flah_swf_url,

                    // Silverlight settings
                    silverlight_xap_url : APL.silverlight_xap_url,

                    init: {

                        BeforeUpload: function(up, file) {
                            uploader_gallery.settings.multipart_params = {'uid': file.id};
                        },

                        FilesAdded: function(up, files) {

                            if(up.files.length > maxfiles )
                            {
                                up.files.splice(maxfiles);
                                var _max_file_msg = translate.attr('data-max-file-gallery');
                                alert(_max_file_msg.replace('%s',maxfiles));
                            }

                            plupload.each(up.files, function(file) {
                                //document.getElementById('_gallery_box').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
                                document.getElementById('_gallery_box').innerHTML += _template_box.replace('{i}', file.id).replace('{filename}', file.name).replace('{filesize}', plupload.formatSize(file.size));
                            });

                            uploader_gallery.start();
                        },

                        UploadProgress: function(up, file) {
                            $('#_gallery_box_'+file.id+' .status span').css('width', file.percent + '%');
                        },

                        FileUploaded: function(up, file, info) {

                            var res = JSON.parse(info['response']);
                            var href = 'javascript:void(0);';

                            if(res.error === true) {
                                uploader_gallery.trigger('Error', {
                                    code : plupload.GENERIC_ERROR,
                                    message : res.data.gallerys.msg + "."
                                });

                                return false;
                            }

                            $(res.data.gallerys).each(function(_, data) {
                                var $_gallery_box_id = $('#_gallery_box_'+data.uid);
                                if(data.error === true) {
                                    $_gallery_box_id.remove();

                                    return true; /* continue */
                                }
                                var org_gallery = $('#_apl_gallerys');
                                if (org_gallery.length > 0) {
                                    org_gallery.append(data.input_hidden);
                                }

                                /*
                                * @Ticket 15203
                                * TO DO: waiting the refactor - page builder
                                */
                                $galler_list.append(_list_template.replace(/{i}/g, data.uid).replace('{href}', href).replace('{src}', data.url));
                                var _overview = $_gallery_box_id.find('.overview');

                                _overview.attr('src', data.url);
                                _overview.removeClass('hidden');
                            });

                            $save_gallery_btn.addClass('hidden');

                        },

                        UploadComplete: function(up, files) {
                            var n_uploaded_image = up.files.length;
                            bind_plus_upload(n_uploaded_image);
                        },

                        Error: function(up, err) {
                            if($gallery_console.css('display') === 'none') {
                                $gallery_console.fadeIn('fast');
                            }

                            if(err.code === plupload.FILE_SIZE_ERROR) {
                                var max_size_msg = translate.attr('data-max-size-gallery');
                                var mapObj = {
                                    '%s1':err.file.name,
                                    '%s2':(parseInt(APL.upload_image.filters.max_file_size, 10) / (1024 * 1024)).toFixed(2),
                                };
                                var _error_size_gallery_msg = max_size_msg.replace(/%s1|%s2/gi, function(matched){
                                    return mapObj[matched];
                                });
                                $gallery_console.append("<div>" + _error_size_gallery_msg + "</div>");
                            }
                            else {
                                $gallery_console.append("<div>" + err.message + "</div>");
                            }

                            var idtimer = setTimeout(function () {
                                $gallery_console.fadeOut('slow');
                                $gallery_console.empty();
                            }, 8000);

                            $.fn.pluploader["gallery.timers"].push(idtimer);
                        }
                    }
                });
                uploader_gallery.init();
            }

            $save_gallery_btn.on('click', function(e) {
                $($.fn.pluploader["gallery.timers"]).each(function(_, v) {
                    clearTimeout(v);
                });
                $.fn.pluploader["gallery.timers"] = [];
                $gallery_console.empty();
            });

            /* cancel upload */


            $('body').on('click', '.gallery-list .rm_img', function(e) {
                var $this = $(this);
                var uid = $this.data('id');
                var currentImage = $(e.currentTarget).closest('a').children('img');
                if(currentImage.length > 0){
                    var url = $(currentImage[0]).attr('src');
                } else {
                    var url = '';
                }
                /** @Ticket #14138 */
                var isDelete = $('#_apl_gallerys #_gallerys_is_deleted');
                if (isDelete.length > 0) {
                    var rmImgs = isDelete.val();
                    var currentImgId = $this.data('img-id');
                    if (typeof currentImgId !== 'undefined'){
                        if (rmImgs == '') {
                            rmImgs = currentImgId;
                        } else {
                            rmImgs = rmImgs + ',' + currentImgId;
                        }
                        isDelete.val(rmImgs);
                    }
                }

                $.ajax({
                    url: APL.ajax_url,
                    beforeSend: function() {
                        $window.block($.apl.blockUI);
                    },
                    data: {
                        action: 'apollo_remove_gallery',
                        uid: uid,
                        pid: gallery_pid,
                        target: gallery_target
                    },
                    success: function (res) {
                        left_number++;

                    },
                    complete: function() {
                        $this.parents('li._list_').remove();
                        $('#_gallery_box_'+uid).remove();
                        $window.unblock();

                        /*  check if don't have any image left remove cancel upload */
                        if($galler_list.find('li._list_').length === 0 ) {
                            uploader_gallery.splice();
                            uploader_gallery.refresh();

                            $gallery_console.empty();

                            $save_gallery_btn.removeClass('hidden');

                            $galler_list.empty();
                        }
                        else if($("#_upload_gallery_btn_plus").length === 0){
                            bind_plus_upload($galler_list.find('li._list_').length);
                        }
                        var attribute_remove = "[data-url='"+ url +"']";
                        $('#_apl_gallerys .gallery-item').filter(attribute_remove).remove();
                    }
                });
            });

            /* Check to see enough length */
            if($galler_list.find('li._list_').length < maxfiles && $galler_list.find('li._list_').length !== 0) {
                bind_plus_upload($galler_list.find('li._list_').length);
            }

            /** End Upload gallerys ************************************************/
        }
    }); // End typeof APL != "undefined"

    $('body').off('click', '.apl-protected-logout').on('click', '.apl-protected-logout', function(e) {
        e.preventDefault();
        var current = $(e.currentTarget);
        $.ajax({
            type: "POST",
            dataType: "JSON",
            data: {
                action : 'apollo_set_cookie_protected_pass',
                apl_cookiehash : current.attr('data-cookiehash')
            },
            url: APL.ajax_url,
            beforeSend: function() {
            },
            success: function (res) {
                if (res.data) {
                    window.location.reload();
                }
            },
            complete: function(res) {

            }
        });
    });

    $(function() {
        var editors = $('[data-ride="editor"]');
        $.each(editors, function(i, v) {
            try {
                var _id = $(v).attr('id');
                CKEDITOR.replace(_id);
            } catch(e) {
                console.log(e)
            }
        });
    });

})(jQuery);
