(function($){
    var AplDashboardProgram = function () {

        var showPopupWarning = function () {
            var previewPage = $('.apl-preview-program');
            if (previewPage.length > 0) {
                if (previewPage.attr('data-status') == 'unconfirmed') {
                    window.onbeforeunload = function() {
                        return previewPage.attr('data-message-warning');
                    };
                }
            }
        };

        var checkRedirectPage = function (e) {
            var previewPage = $('.apl-preview-program');
            var current = $(e.currentTarget);

            if (previewPage.length > 0) {
                /** Check submit or return to edit action */
                if (current.hasClass('apl-program-return-to-edit')
                    || current.hasClass('apl-program-submit-form')
                    || current.find('.apl-program-submit-form').length > 0)
                {
                    window.onbeforeunload = null;
                    return true;
                }

                if (previewPage.attr('data-status') == 'unconfirmed') {
                    var textConfirm = previewPage.attr('data-message-warning').replace(/(\\n|\\r|\\r\\n)/gm, '\n');
                    window.alert(textConfirm);
                    window.onbeforeunload = null;
                    e.stopPropagation();
                    return false;
                }
            }
        };

        var allActionsRedirectPage = function () {
            var parent = $('body .apl-preview-program');
            if (parent.length > 0) {
                $('body a').not('[href="#"]').not('[href=""]').off('click').on('click', checkRedirectPage);
                $('body form').off('submit').on('submit', checkRedirectPage);
            }
        };


        return {
            init: function () {
                showPopupWarning();
                allActionsRedirectPage();
            }
        };

    }();

    $(document).ready(function() {
        AplDashboardProgram.init();
    });
})(jQuery);
