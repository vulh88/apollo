
(function( $ ) {

    $('.list-markers [data-main-flag]').click(function() {
        try {
            window.aplInfowindow.close();
            window.mainInfowindow.close();
        } catch(e) {}
        
        window.mainInfowindow.open(window.aplMap, window.aplMap.marker);
    });

    $('.list-markers').on('click', 'ul li', function() {
        var _map = new $.apl.google_map.eventBusinessMap();
        if (window.mainInfowindow) window.mainInfowindow.close();    
        _map.setLat($(this).attr('data-lat'));
        _map.setLng($(this).attr('data-lng'));
        _map.openInfoWindow($(this).attr('data-type'), $(this).attr('data-index'));
        $('html,body').animate({ scrollTop: $('#apl-wnby-block').offset().top }, 500);
    });

    $('.filter-map.business [name=map_filter]').click(function() {
        var _map = new $.apl.google_map.eventBusinessMap();

        var filterElms = $('.filter-map.business [name=map_filter]'),
            filterTypes = [];

        // Remove all markers of the filter types
        $.each(filterElms, function(i, v) {
            var type = $(v).val();
            filterTypes.push(type);
            var _mods = _map.getGlobalMarker(type);
            if(_mods != undefined && _mods.length) {
                $.each(_mods, function(j, marker) {
                    _map.deleteMarker(marker);
                });
                
                // Hide the in-active type slider
                $('.'+ type).hide();
            }
        });

        // Display activate markers
        var _activeTypes = $(this).val() == 'all' ? filterTypes : [$(this).val()]; // check all
        
        $.each(_activeTypes, function(i, type) {
            // Show the in-active type slider
            $('.'+ type).show();

            if(type != 'all'){
                var _count = $('.'+ type +' li').length;
                $('.'+ type +' li').show();
                $('.nofi-exp').attr('id','' + type );
                if (_count > 10){
                    $('.nofi-exp').show();
                    $('.nofi-exp').text($('#data-view-more').attr('data-viewmore'));
                    $('.'+ type +' li:gt(9)').hide();
                }else {
                    $('.nofi-exp').hide();
                }
            }

            var _activeMarkers = _map.getGlobalMarker(type);
            if(_activeMarkers != undefined && _activeMarkers.length) {
                $.each(_activeMarkers, function(j, marker) {
                    _map.restoreMarker(marker);
                });
            }
        });
        if (_activeTypes == filterTypes){

            /**
             * @ticket #18745:Modify the default logic to display items per pages
             */
            var listItem = $('.nofi-more .n-blk ul');
            if(listItem.length){
                listItem.each(function(){
                    var count = $(this).find('li').length;
                    if(count > 10 ){
                        $('.nofi-exp').show();
                        /*li:gt(index) */
                        $(this).find('li:gt(9)').hide();
                    }
                });
            }

            $('.nofi-exp').attr('id','');
            $('.nofi-exp').text($('#data-view-more').attr('data-viewmore'));
        }

    });

    $(function() {
        
        // Handle public art map
        if ($('#what_near_by_map').length) {
            
            $.apl.google_map.load_me_and_do("jQuery.apl.google_map.eventWhatNearBy");
            $.apl.google_map.eventWhatNearBy = function() {
                var _map = new $.apl.google_map.eventBusinessMap();
                _map.getLatLng(function(resLatLng) {
                    
                    if (! resLatLng.success) {
                        $('#apl-wnby-block').hide();
                        return;
                    }
                    
                    $('#apl-wnby-block').block($.apl.blockUI);
                    _map.setMap(function(res) {
                        
                        if (res.success && window.ajaxCall) {
                            _map.setMarkers(function(resMarker) {
                                $('#apl-wnby-block').unblock($.apl.blockUI);
                                
                                /**
                                 * Set full map
                                 * 
                                 * */
                                if ($(_map.elmMap).data().relation_id) {
                                    var _fullMap = new $.apl.google_map.eventBusinessMap();
                                    _fullMap.elmMap = '#'+ $(_map.elmMap).data().relation_id;
                                    _fullMap.setLat(resLatLng.lat);
                                    _fullMap.setLng(resLatLng.lng);
                                    
                                    _fullMap.setFullMap(function() {
                                         //Call map once time
                                        if (!window.setFullMap) {
                                            $.each(resMarker.data, function(type, obj) {
                                                if (obj.data) _fullMap.renderMarkers(obj.data, type, obj.iconSrc);
                                                window.setFullMap = true;
                                            });
                                            $('.nofi-more').show();

                                            /**
                                             * @ticket #18745:Modify the default logic to display items per pages
                                             */
                                            var listItem = $('.nofi-more .n-blk ul');
                                            if(listItem.length){
                                                listItem.each(function(){
                                                  var count = $(this).find('li').length;
                                                  if(count > 10 ){
                                                      $('.nofi-exp').show();
                                                      /*li:gt(index) */
                                                      $(this).find('li:gt(9)').hide();
                                                  }
                                              });
                                            }
                                        }
                                        window.ajaxCall = false;
                                    });
                                } // End set full map
                                
                            });
                            window.ajaxCall = false;
                        }
                        
                    });
                });
            };
        }
    });
      
    $.apl.google_map.aplMap = function() {
        this.elmMap = '#what_near_by_map';
        this.map = false;
        this.marker = false;
        this.lat = false;
        this.lng = false;
        this.lng = false;
        this.iconWidth = parseInt(APL.google_map_settings.google_map_icon_width);
        this.iconHeight = parseInt(APL.google_map_settings.google_map_icon_height);
        this.iconSrc = APL.google_map_settings.google_map_icon;
        this.circle = false;
        
        var radius = parseFloat($(this.elmMap).data().radius);
        this.radius = radius ? radius : 1609;
        
        this.setMap = function(callback) {
            
            if (!this.lat || !this.lng) {
                $(this.elmMap).closest('.a-block.locatn').hide();
                return;
            }
            var $this = this;
            
            window.ajaxCall = true;
            this.map = new google.maps.Map(document.getElementById(this.getElmMap()), {
                zoom: 12,
                center: {lat: this.lat, lng: this.lng}
            });
            window.aplMap = this.map;
            window.aplMap.marker = this.marker = new google.maps.Marker({
                map: this.map,
                draggable: true,
                animation: google.maps.Animation.DROP,
                position: {lat: this.lat, lng: this.lng},
                icon: this.makeIcon()
            });
            //this.marker.addListener('click', $.apl.google_map.aplMap.prototype.toggleBounce);
            
            // Add circle overlay and bind to marker
            this.circle = new google.maps.Circle({
                map: this.map,
                radius: this.radius,    // 1 miles in metres
                fillColor: '#AA0000'
            });
//            this.circle.bindTo('center', this.marker, 'position');
            
            // Set zoom
            this.map.setZoom(Math.ceil(this.getZoomLevel()));
            
            // Keep the main info window
            window.mainInfowindow = new google.maps.InfoWindow({
                    content: $this.renderMaincontentInw()
                });

            window.aplMap.marker.addListener('click', function() {
                try{
                    window.aplInfowindow.close();
                } catch(e) {}

                window.mainInfowindow.open($this.getMap(), window.aplMap.marker);
            });
            
            //Handler finished load map
            google.maps.event.addListener(this.map, 'idle', function(event) {

                if (typeof(callback) != 'undefined') {
                    callback({
                        success: true
                    });
                }
            });
        };


        this.setFullMap = function(callback) {
            
            if (!this.lat || !this.lng) {
                $(this.elmMap).closest('.a-block.locatn').hide();
                return;
            }
            var $this = this;
            
            this.map = new google.maps.Map(document.getElementById(this.getElmMap()), {
                zoom: 12,
                center: {lat: this.lat, lng: this.lng}
            });
            window.aplFullMap = this.map;
            window.aplFullMap.marker = this.marker = new google.maps.Marker({
                map: this.map,
                draggable: true,
                animation: google.maps.Animation.DROP,
                position: {lat: this.lat, lng: this.lng},
                icon: this.makeIcon()
            });
            //this.marker.addListener('click', $.apl.google_map.aplMap.prototype.toggleBounce);
            
            // Add circle overlay and bind to marker
            this.circle = new google.maps.Circle({
                map: this.map,
                radius: this.radius,    // 1 miles in metres
                fillColor: '#AA0000'
            });
//            this.circle.bindTo('center', this.marker, 'position');
            
            // Set zoom
            this.map.setZoom(Math.ceil(this.getZoomLevel()));
            
            // Keep the main info window
            window.fullInfowindow = new google.maps.InfoWindow({
                content: this.renderMaincontentInw($('#what_near_by_map').data())
            });

            window.aplFullMap.marker.addListener('click', function() {
                try{
                    window.fullInfowindow.close();
                } catch(e) {}
               
                window.fullInfowindow.open($this.getMap(), window.aplFullMap.marker);
            });
            
            //Handler finished load map
            google.maps.event.addListener($this.map, 'idle', function(event) {

                if (typeof(callback) != 'undefined') {
                    callback({
                        success: true
                    });
                }
            });
        };

        this.getZoomLevel = function() {
            var radius = this.circle.getRadius(),
                scale  = radius / 500;
            return 16 - Math.log(scale) / Math.log(2);
        };
        
        this.toggleBounce = function(marker) {
            var _marker = marker != undefined ? marker : this.marker;
            if (_marker.getAnimation() !== null) {
                _marker.setAnimation(null);
            } else {
                _marker.setAnimation(google.maps.Animation.BOUNCE);
            }
        };

        this.makeIcon = function(src) {
            var _src = src && src != undefined ? src : this.iconSrc;
            return new google.maps.MarkerImage(_src,
                null, null, null, new google.maps.Size(this.iconWidth, this.iconHeight));
        };

        this.setLat = function(lat) {
            this.lat = parseFloat(lat);
        };

        this.setLng = function(lng) {
            this.lng = parseFloat(lng);
        };

        this.setMarkersData = function(markersData) {
            this.markersData = markersData;
        };

        this.getByAddress = function(address, callback) {
            var geocoder = new google.maps.Geocoder(),
                $this    = this;
            geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    $this.setLat(results[0].geometry.location.lat());
                    $this.setLng(results[0].geometry.location.lng());
                    callback({
                        success: true
                    });
                } else {
                    callback({
                        success: false
                    });
                    $($this.elmMap).closest('#apl-wnby-block').hide();
                    /** Vandd @ticket #12018 */
                    var locationBlock = $('.location-wrapper');
                    if(locationBlock.length > 0){
                        $.each(locationBlock, function(index, item) {
                            if(!$(item).hasClass('hidden')){
                                $(item).addClass('hidden');
                            }
                        });
                    }
                }
            });
        };
        
        this.getMap = function() {
            return this.map ? this.map : window.aplMap;
        };
        
        this.getBounds = function get_bounds() {

            var bounds  = this.getMap().getBounds(),
                ne      = bounds.getNorthEast(), // top-left
                sw      = bounds.getSouthWest(); // bottom-right
            return {
                min_lat: sw.lat(),
                max_lat: ne.lat(),
                min_lng: sw.lng(),
                max_lng: ne.lng()
            };
        };

        this.getElmMap = function() {
            return this.elmMap.replace('#', '');
        };
    };

    /**
     * eventBusinessMap class
     *
     * */
    $.apl.google_map.eventBusinessMap = function() {
        $.apl.google_map.aplMap.apply(this, arguments); // Call parent constructor
        this.filterTypes = ['venue', 'acc', 'din', 'bar'];
    };

    $.apl.google_map.eventBusinessMap.prototype = $.apl.google_map.aplMap.prototype; // Clone method from parent class
    $.extend($.apl.google_map.eventBusinessMap.prototype, $.apl.google_map.aplMap.prototype);
    $.apl.google_map.eventBusinessMap.prototype.getLatLng = function(callback) {
        var emid    = $(this.elmMap),
            $this   = this;
        if(emid.length === 0) {
            if (typeof callback != undefined) {
                callback({
                    success: false
                });
            }
            return;
        };


        var _arrCoor    = '',
            _coor       = emid.data('coor');
       
        if(_coor !== "") {
            _arrCoor = _coor.split(',');
            $this.setLat(_arrCoor[0]);
            $this.setLng(_arrCoor[1]);
            if (typeof callback != undefined) {
                callback({
                    success: true,
                    lat: parseFloat(_arrCoor[0]),
                    lng: parseFloat(_arrCoor[1]),
                });
            }
        }
        else {
            var address = emid.data('address'),
                nonce = emid.data('nonce');
          
            if(address === "") {
                if (typeof callback != undefined) {
                    callback({
                        success: false
                    });
                }
                return;
            };
            
            this.getByAddress(address, function(res) {
                $.ajax({
                    url: APL.ajax_url,
                    data: {
                        nonce: nonce,
                        action: 'apollo_save_coordinate',
                        lat: $this.lat,
                        lng: $this.lng,
                        addr: address
                    },
                    success: function() {
                        if (typeof callback != undefined) {
                            callback({
                                success: true,
                                lat: parseFloat($this.lat),
                                lng: parseFloat($this.lng),
                            });
                        }
                    }
                });
            });
        }
        
    };
    
    
    
    $.apl.google_map.eventBusinessMap.prototype.renderMaincontentInw = function(_mainData) {
        if (_mainData == undefined) {
            var _mainData = $(this.elmMap).data();
        }
        
        if (!_mainData) return false;
        
        if (!_mainData.title) return false;
        return '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
            '<h1 class="firstHeading"><a href="'+_mainData.url+'">'+_mainData.title+'</a></h1>'+
            '<div id="bodyContent">'+
            _mainData.address +
            _mainData.phone +
            '</div>'+
        '</div>';
    };
    
    $.apl.google_map.eventBusinessMap.prototype.renderInfoWindow = function(type, i) {
        var _dataTypes = this.getGlobalItemSlider(type);

        if (!_dataTypes && !_dataTypes[i]) return '';

        var address = _dataTypes[i].address ? '<p>'+_dataTypes[i].address+'</p>' : '',
            phone = _dataTypes[i].phone ? '<p>'+_dataTypes[i].phone+'</p>' : '',
            website = _dataTypes[i].website_url ? '<p><a href="'+_dataTypes[i].website_url+'" target="_blank">'+_dataTypes[i].website_text+'</a></p>' : '';
        
        return '<div class="apl-gmap-info">'+
            '<div>'+
            '</div>'+
            '<h1 class="firstHeading"><a href="'+_dataTypes[i].url+'">'+_dataTypes[i].post_title+'</a></h1>'+
            '<div id="bodyContent">'+
            address +
            phone +
            website +
            '</div>'+
            '</div>';
    };

    $.apl.google_map.eventBusinessMap.prototype.setMarkers = function(callback) {
        
        var $this = this;
        
        // Data for the markers consisting of a name, a LatLng and a zIndex for the
        // order in which these markers should display on top of each other.

        var minMaxData = this.getBounds(),
            setMarkers = function(data) {
                $.each(data, function(type, obj) {
                    if (obj.data) $this.renderMarkers(obj.data, type, obj.iconSrc);
                });
                if (typeof (callback) != 'undefined') {
                    callback({success: true, data: data});
                }
            },
            filterTypes = $this.getFilterTypes();
        
        
        // Get markers from global, this variable is expired after two days
        var _localData = JSON.parse(localStorage.getItem('aplBusMarkers'));
        
        // Check for clear cache
        var removeCache = false;
        if (_localData != null && _localData.filterTypes && filterTypes ) {
           
            if (_localData.filterTypes.length != filterTypes.length) {
                removeCache = true;
            } else {
                $.each(_localData.filterTypes, function(i, v) {
                    
                    $.each(filterTypes, function(i1, v1) {
                        if (v1.id == v.id && v1.val != v.val) {
                            removeCache = true;
                        }
                    });
                    
                });
            }
        }
        
        if (removeCache) console.log('remove cache')
        
        if (!removeCache && _localData != null && _localData.filterTypes != undefined && _localData && _localData.timestamp 
                && Math.ceil((new Date().getTime() - _localData.timestamp)/ 86400) <= 2  // <= 2 days
                && _localData.min_lat == minMaxData.min_lat
                && _localData.max_lat == minMaxData.max_lat
                && _localData.min_lng == minMaxData.min_lng
                && _localData.max_lng == minMaxData.max_lng
            ) {
            
            setMarkers(_localData.value);
            return;
        }
        
        minMaxData.action = 'apollo_get_what_near_by';
        
        $.ajax({
            url: APL.ajax_url,
            data: minMaxData,
            success: function (res) {
                
                setMarkers(res.data);
                
                localStorage.setItem('aplBusMarkers', JSON.stringify({
                    'value': res.data,
                    'min_lat': minMaxData.min_lat,
                    'max_lat': minMaxData.max_lat,
                    'min_lng': minMaxData.min_lng,
                    'max_lng': minMaxData.max_lng,
                    'timestamp': new Date().getTime(),
                    'filterTypes': filterTypes 
                }));
                
            }
        });
       
    };
    
    $.apl.google_map.eventBusinessMap.prototype.getFilterTypes = function() {
        var obj = $('.filter-map.business input[type=radio]'),
            output = [];
        if (! obj) return false;    
        $.each(obj, function(i, v) {
            
            output.push({
                'id': $(v).val(),
                'val': $(v).data().cat != undefined ? $(v).data().cat : ''
            });
        });
        return output;
    };
    
    $.apl.google_map.eventBusinessMap.prototype.renderMarkers = function(data, type, srcIcon) {
        var _n          = data.length,
            $this       = this,
            locations   = data;

        var htmlSlider = '';
        for (var i = 0; i < _n; i++) {

            var location = locations[i],
                marker = new google.maps.Marker({
                    position: {lat: parseFloat(location.lat), lng: parseFloat(location.lng)},
                    map: this.getMap(),
                    icon: this.makeIcon(srcIcon),
                    optimized: false, // To set border radius in css of img
                    animation: google.maps.Animation.DROP,
                    url: '',
                    id: type+ '-'+ i
                });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    
                    if (window.mainInfowindow) {
                        window.mainInfowindow.close();
                    }
                    
                    if (window.fullInfowindow) {
                        window.fullInfowindow.close();
                    }
                    
                    try {
                       window.aplInfowindow.close(); 
                    } catch(e){}
                    
                    window.aplInfowindow = new google.maps.InfoWindow({maxWidth: 200});
               
                    window.aplInfowindow.close();
                    window.aplInfowindow.setContent($.apl.google_map.aplMap.prototype.renderInfoWindow(type, i));
                    window.aplInfowindow.open(this.getMap(), marker);
                };
            })(marker, i));

            // Keep global markers of each type for filtering
            this.setGlobalMarker(type, marker);
            this.setGlobalItemSlider(type, locations[i]);
            htmlSlider += this.renderSliderItem(location, srcIcon, i, type);
        }

        // Set html for slider
        this.setSliderHtml(type, htmlSlider);
    }

    $.apl.google_map.eventBusinessMap.prototype.setGlobalMarker = function(type, marker) {
        if (!window.busMarkersData) window.busMarkersData = {};
        if (! eval('window.busMarkersData.'+ type)) eval('window.busMarkersData.'+ type+ '=[]');
        eval('window.busMarkersData.'+ type+ '.push(marker)');
    }

    $.apl.google_map.eventBusinessMap.prototype.setGlobalItemSlider = function(type, data) {
        if (!window.busItemSliderData) window.busItemSliderData = {};
        if (! eval('window.busItemSliderData.'+ type)) eval('window.busItemSliderData.'+ type+ '=[]');
        eval('window.busItemSliderData.'+ type+ '.push(data)');
    }

    $.apl.google_map.eventBusinessMap.prototype.getGlobalItemSlider = function(type) {
        return eval('window.busItemSliderData.'+ type+ '');
    }

    $.apl.google_map.eventBusinessMap.prototype.getGlobalMarker = function(type) {
        return eval('window.busMarkersData.'+ type+ '');
    }

    $.apl.google_map.eventBusinessMap.prototype.deleteMarker = function(marker) {
        marker.setMap(null);
    }

    $.apl.google_map.eventBusinessMap.prototype.restoreMarker = function(marker) {
        marker.setMap(this.getMap());
    };

    $.apl.google_map.eventBusinessMap.prototype.renderSliderItem = function(data, srcIcon, index, type) {
        return '<li data-index="'+index+'" data-type="'+type+'" data-lat="'+data.lat+'" data-lng="'+data.lng+'"><img src="'+srcIcon+'">'+ data.post_title +'</li>';
    };

    $.apl.google_map.eventBusinessMap.prototype.setSliderHtml = function(type, html) {
      //  console.log(html)
        if (! html) {
            // Hide slider item
            $('.'+ type ).hide();
            // Hide the filter item
            $('[value="'+type+'"]' ).closest('td').hide();
            return;
        }
        $('.'+ type+ ' ul').html(html);
        $('.'+ type ).show();
        
        $('[value="'+type+'"]' ).closest('td').show();

    };

    $.apl.google_map.eventBusinessMap.prototype.openInfoWindow = function(type, i) {
        
        if (window.aplInfowindow) {
            window.aplInfowindow.close();
        }
        
        try {
            var center  = new google.maps.LatLng(this.lat, this.lng),
                marker  = this.getGlobalMarker(type)[i];
                
            var infowindow  = new google.maps.InfoWindow({maxWidth:200}),
                html        = this.renderInfoWindow(type, i);
            infowindow.setContent(html);
            infowindow.open(this.getMap(), marker);
            window.aplInfowindow = infowindow;
        } catch (e) {
            
        }

//        this.getMap().panTo(center);
    };

}) (jQuery);