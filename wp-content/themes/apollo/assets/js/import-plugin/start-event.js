(function($) {

    $.apl.importEvent = {
        totalColumns: 24,
        totalRows: 50,
        isShowErrorMsg: true,
        firstIndexRowError: -1,
        firstIndexColError: -1,
        dateFormat: 'MM/DD/YYYY',
        saveImport: function () {},
        saveImportForLater: function () {},
        hotData: false,
        getSegmentData: function() {},
        segmentStep: 3,
    };

    $(document).ready(function($){

        if (APL.event_import == undefined) return;

        var conId = 'event-list',
            container = document.getElementById(conId),
            $$ = function(id) {
                return document.getElementById(id);
            },
            save     = $$('btn-submit'),
            load     = $$('apl-load-event-import-file'),
            reset    = $$('btn-reset'),
            addNew   = $$('btn-add-new'),
            saveForLater = $$('save-later'),
            orgs = [], venues = [], pri_cats = [], sub_cats = [];

        //if (APL.event_import.orgs) {
        //    $(APL.event_import.orgs).each(function(i, v) {
        //        orgs.push({'id': parseInt(v.ID), 'text': v.post_title});
        //    });
        //}

        //if (APL.event_import.venues) {
        //    $(APL.event_import.venues).each(function(i, v) {
        //        venues.push({'id': parseInt(v.ID), 'text': v.post_title});
        //    });
        //}

        if (APL.event_import.pri_cats) {
            $(APL.event_import.pri_cats).each(function(i, v) {
                pri_cats.push({'id': parseInt(v.term_id), 'text': v.name});
            });
        }

       if (APL.event_import.sub_cats) {
           $(APL.event_import.sub_cats).each(function(i, v) {
               sub_cats.push({'id': parseInt(v.term_id), 'text': v.name});
           });
       }
     
       var _dateTimeData = ['8:00 AM', '8:15 AM', '8:30 AM', '8:45 AM',
               '9:00 AM', '9:15 AM', '9:30 AM', '9:45 AM',
               '10:00 AM', '10:15 AM',  '10:30 AM', '10:45 AM',
               '11:00 AM', '11:15 AM', '11:30 AM', '11:45 AM',
               '12:00 PM', '12:15 PM', '12:30 PM', '12:45 PM',
               '1:00 PM', '1:15 PM', '1:30 PM', '1:45 PM',
               '2:00 PM', '2:15 PM', '2:30 PM', '2:45 PM',
               '3:00 PM', '3:15 PM', '3:30 PM', '3:45 PM',
               '4:00 PM', '4:15 PM', '4:30 PM', '4:45 PM',
               '5:00 PM', '5:15 PM', '5:30 PM', '5:45 PM',
               '6:00 PM', '6:15 PM', '6:30 PM', '6:45 PM',
               '7:00 PM', '7:15 PM', '7:30 PM', '7:45 PM',
               '8:00 PM', '8:15 PM', '8:30 PM', '8:45 PM',
               '9:00 PM', '9:15 PM', '9:30 PM', '9:45 PM',
               '10:00 PM', '10:15 PM', '10:30 PM', '10:45 PM',
               '11:00 PM', '11:15 PM', '11:30 PM', '11:45 PM'
              ],
           dateTimeData = [],
           emailValidator,
           requiredValidator,
           urlValidator,
           dateValidator,
           startDateValidator,
           endDateValidator,
           maximumYearRangeValidator;


        startDateValidator = function (currentCell, startDateVal, callback) {
            var startDate = moment(startDateVal);
            var endDateVal = hot.getDataAtRowProp(currentCell.attr('row'), 'end-date');
            var endDate = moment(endDateVal);
            if (startDate > endDate) {
                callback(false, $.apl.importEvent.getTextTransByKey('cv-sd-ed'));
            }else if(maximumYearRangeValidator(currentCell,endDateVal) == false){
                callback(false, $.apl.importEvent.getTextTransByKey('cv-maximum-year'));
            }
            else {
                callback(true);
            }
        };

        endDateValidator = function (currentCell, endDateVal, callback) {
            var endDate = moment(endDateVal);
            var startDateVal = hot.getDataAtRowProp(currentCell.attr('row'), 'start-date');
            var startDate = moment(startDateVal);
            var nowVal = moment().format($.apl.importEvent.dateFormat);
            var nowDate = moment(nowVal);
            if (endDate < nowDate) {
                callback(false, $.apl.importEvent.getTextTransByKey('cv-selected-date'));
            }else if(maximumYearRangeValidator(currentCell,endDateVal) == false){
                callback(false, $.apl.importEvent.getTextTransByKey('cv-maximum-year'));
            }
            else if (endDate < startDate) {
                callback(false, $.apl.importEvent.getTextTransByKey('cv-ed-sd'));
            } else {
                callback(true);
            }
        };

        maximumYearRangeValidator = function(currentCell, endDateVal){
            var startDate = new Date(endDateVal),
                endDate = new Date(hot.getDataAtRowProp(currentCell.attr('row'), 'start-date')),
                startDateYear = startDate.getFullYear(),
                endDateYear = endDate.getFullYear(),
                yearDiff = endDateYear - startDateYear;
            if(Math.abs(yearDiff) > APL.event_maximum_year){
               return false;
            }
           return true;
        };

        dateValidator = function (value, callback) {
            var cell = $(this);
            var required = cell.attr('required');
            if (required && _.isEmpty(value)) {
                callback(false, $.apl.importEvent.getTextTransByKey('cv-required'));
                return;
            }

            var validateDate = function(testdate) {
                var date_regex1 = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/ ;
                var date_regex2 = /^([1-9]|1[0-2])\/([1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/ ;
                var date_regex3 = /^(0[1-9]|1[0-2])\/([1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/ ;
                var date_regex4 = /^([1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/ ;
                return date_regex1.test(testdate) || date_regex2.test(testdate) || date_regex3.test(testdate) || date_regex4.test(testdate);
            }

            if (!validateDate(value)) {
                callback(false, $.apl.importEvent.getTextTransByKey('cv-invalid-date'));
                return;
            }

            if (!_.isEmpty(value)) {
                var dataField = cell.attr('data');
                if (dataField == 'start-date') {
                    startDateValidator(cell, value, callback);
                    return;
                }
                if (dataField == 'end-date') {
                    endDateValidator(cell, value, callback);
                    return;
                }
                //maximumYearRangeValidator(cell,value,callback);

            }
            callback(true);
        };

        urlValidator = function (value, callback) {
            if ($.apl.validateUrl(value) || !value) {
                callback(true);
            } else {
                callback(false, $.apl.importEvent.getTextTransByKey('cv-url'));
            }
        };

        emailValidator = function (value, callback) {
            if (/.+@.+/.test(value) || !value) {
                callback(true);
            }
            else {
                callback(false, $.apl.importEvent.getTextTransByKey('cv-email'));
            }

        };

        requiredValidator = function(value, callback) {
            if (value) {
                callback(true);
            } else {
                callback(false, $.apl.importEvent.getTextTransByKey('cv-required'));
            }
        };

        $.each( _dateTimeData, function(i, v) {
            var vArr = v.split(':'),
                _v   = $.apl.changeTime(vArr[0], vArr[1].split(' ')[0], vArr[1].split(' ')[1]);
            dateTimeData.push({id: parseFloat(_v.replace(':', '')), text: v});
        });

        var columns = [
            {
                // cell 1 Event Name
                data:"eventname",
                placeholder:"Event name",
                validator: requiredValidator,
                allowInvalid: true,
            },
           {
               placeholder:"ORG Name",
               data:"orgname",
               editor: 'select2',
               renderer: $.apl.importEvent.customDropdownRendererAjaxOrg,
               width: '150px',
               validator: requiredValidator,
               select2Options: {
                   dropdownAutoWidth: true,
                   width: 'resolve',
                   ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                       url: APL.ajax_url,
                       dataType: 'json',
                       quietMillis: 250,
                       data: function (term, page) {
                           return {
                               q: term, // search term
                               page: page,
                               'action': 'apollo_import_get_taxes',
                               post_type: 'organization',
                           };
                       },
                       results: function (data, page) { // parse the results into the format expected by Select2.
                           // since we are using custom formatting functions we do not need to alter the remote JSON data
                           if (typeof globalOptionListOrg != 'undefined' && globalOptionListOrg) {
                               globalOptionListOrg = globalOptionListOrg.concat(data.items);
                           } else {
                               globalOptionListOrg = data.items;
                           }
                           return {
                               results: data.items,
                               more: data.hasMore
                           };
                       },
                       cache: true,
                   },
                   minimumInputLength: 0,
                   //formatResult: $.apl.importEvent.repoFormatResult, // omitted for brevity, see the source of this page
                   //formatSelection: $.apl.importEvent.repoFormatSelection, // omitted for brevity, see the source of this page
                   dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
                   escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
               }
           },

            {
                placeholder:"Venue Name",
                data:"venuename",
                id: 'venuename',
                editor: 'select2',
                renderer: $.apl.importEvent.customDropdownRendererAjaxVenue,
                width: '150px',
                select2Options: {
                    dropdownAutoWidth: true,
                    width: 'resolve',
                    ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                        url: APL.ajax_url,
                        dataType: 'json',
                        quietMillis: 250,
                        data: function (term, page) {
                            return {
                                q: term, // search term
                                page: page,
                                'action': 'apollo_import_get_taxes',
                                post_type: 'venue',
                            };
                        },
                        results: function (data, page) { // parse the results into the format expected by Select2.
                            // since we are using custom formatting functions we do not need to alter the remote JSON data

                            if (typeof globalOptionListVenue != 'undefined' && globalOptionListVenue) {
                                globalOptionListVenue = globalOptionListVenue.concat(data.items);
                            } else {
                                globalOptionListVenue = data.items;
                            }

                            return {
                                results: data.items,
                                more: data.hasMore
                            };
                        },
                        cache: true,
                    },
                    minimumInputLength: 0,
                    //formatResult: $.apl.importEvent.repoFormatResult, // omitted for brevity, see the source of this page
                    //formatSelection: $.apl.importEvent.repoFormatSelection, // omitted for brevity, see the source of this page
                    dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
                    escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
                }
            },


            {
                // cell 4 Event Description
                data:"description",
                placeholder:"Event description",
                validator: requiredValidator,
            },
            {
                data:"event-category",
                placeholder:"Event category",
                renderer: $.apl.importEvent.customDropdownRenderer,
                width: 150,
                editor: "select2",
                validator: requiredValidator,
                select2Options: {
                    data: pri_cats,
                    dropdownAutoWidth: true,
                    width: 'resolve'
                }
            },
            {
                data:"event-sub-category",
                placeholder:"Event sub category",
                renderer: $.apl.importEvent.customDropdownRenderer,
                width: 150,
                editor: "select2",
                select2Options: {
                    data: sub_cats,
                    dropdownAutoWidth: true,
                    width: 'resolve'
                }

            },
            {
              // cell 7 Event URL
              data:"event-url",
              placeholder:"Please paste full URL including http:// or https://",
              validator: urlValidator,
              allowInvalid: true
            },
            {
              // cell 8 Event Phone
              data:"event-phone",
              placeholder:"Event Phone"
            },
            {
              // cell 9 Event Email
              data:"event-email",
              placeholder:"Event email",
              validator: emailValidator, 
              allowInvalid: true
            },
            {
              // cell 10 Ticket Info
              data:"ticket-info",
              placeholder:"Admission"
            },
            {
              // cell 11 Ticket URL
              data:"ticket-url",
              placeholder:"Ticket URL",
              validator: urlValidator,
              allowInvalid: true
            },
            {
              // cell 12 Start Date
              data:"start-date",
              required:true,
              placeholder:"Start date",
              type: 'date',
              dateFormat: $.apl.importEvent.dateFormat,
              correctFormat: true,
              validator: dateValidator,
              defaultDate: moment().format($.apl.importEvent.dateFormat)
            },
            {
              // cell 13 End Date
              data:"end-date",
              required:true,
              placeholder:"End date",
              type: 'date',
              dateFormat: $.apl.importEvent.dateFormat,
              correctFormat: true,
              validator: dateValidator,
              defaultDate: moment().format($.apl.importEvent.dateFormat)
            },
         ];
         
        // Add date 
        var j = 0;
        for (var i = 1; i < 8; i++) {
         
            columns.push({
                // cell 14 Monday
                 data: 'day'+ (i < 7 ? i : 0),
                 placeholder:"Enter start time",
                 width: 150,
                 editor: 'select2',
                 renderer: $.apl.importEvent.customDropdownRenderer,
                 select2Options: { 
                     data: dateTimeData,
                     dropdownAutoWidth: true,                        
                     width: 'resolve'
                 }
            });
        } 
        
        columns = columns.concat([
           {
                // cell 21 Image
                data: "image", 
                renderer: $.apl.importEvent.coverRenderer,
                cover: "",
                placeholder:"Please paste complete image URL path including http:// or https://",
                validator: urlValidator,
                allowInvalid: true
            },

            {
                // cell 22 Contact Name
                data:"contactname",
                placeholder:"Contact name",
                validator: requiredValidator, 
            },
            {
                // cell 23 Contact Phone
                data:"contactphone",
                placeholder:"Contact phone"
            },
            {
               //cell 24 Contact Email
               data:"contactemail",
               placeholder:"Contact email",
               validator: emailValidator,
            }  
        ]);

        hot = new Handsontable(container, {
            data: $.apl.importEvent.getData(),
            startRows: 50,
            startCols: 27,
            colWidths: [150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150,150],
            colHeaders: ['Event Name', 'Org Name', 'Venue Name', 'Event Description', 'Event Category', 'Event Sub-Category', 'Event URL', 'Event Phone', 'Event Email', 'Admission', 'Ticket URL', 'Start Date', 'End Date', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', 'Image',  'Your Contact Name','Your Contact Phone', 'Your Contact Email'],
            rowHeaders: true,
            stretchH: 'all',
            autoWrapRow: true,
            contextMenu: true,
            manualColumnResize: true,
            manualRowResize: true,
            afterLoadData: function(){
                // Reset this variable to false to avoid loss data
                aplImportReset = false;
            },
            beforeRender: function(){
                $(window).block($.apl.blockUI);
            },
            afterRender: function(){
                $(window).unblock();
            },
            columns: columns
        });


        Handsontable.hooks.add('afterChange', function(arr, op) {
            if(op=="edit") {
                var row = arr[0][0],
                    value = arr[0][3];
                if(arr[0][1] == 'start-date' && !hot.getDataAtCell(row, 12)) {
                    hot.setDataAtCell( row, 12, value );
                    return false;
                }
            }
        });

        Handsontable.hooks.add('afterRender', function(Changes, source) {
            $.apl.importEvent.isShowErrorMsg = false;
            $.apl.importEvent.validate();
            $.apl.importEvent.isShowErrorMsg = true;
        });

        Handsontable.hooks.add('beforeChange', function() {
        });

        Handsontable.hooks.add('afterValidate', function(result) {

        });

        Handsontable.Dom.addEvent(reset, 'click', function() {
            aplImportReset = true;
            var _confirm = $.apl.importEvent.validate(true) ? true : confirm($(this).data().msg);
            if (_confirm) {
                hot.clear();
                $('html,body').animate({ scrollTop: $('.dsb-r').offset().top }, 500);
            }
        });

        Handsontable.Dom.addEvent(addNew, 'click', function() {
            var _confirm = $.apl.importEvent.validate(true) ? true : confirm($(this).data().msg);
            if (_confirm) {
                $("#select-event-import-file").select2("val", null);
                hot.clear();
            }
        });

        Handsontable.Dom.addEvent(saveForLater, 'click', function() {

            if (!$.apl.importEvent.validate()) {
                $.apl.importEvent.scrollToErrorFirstCell();
                return false;
            }

            $.apl.importEvent.saveImportForLater(false);
        });

        Handsontable.Dom.addEvent(save, 'click', function() {

            if (!$.apl.importEvent.validate()) {
                $.apl.importEvent.scrollToErrorFirstCell();
                return false;
            }
            $.apl.importEvent.saveImport();
        });

        if (load) {
            Handsontable.Dom.addEvent(load, 'click', function() {
                if (!$('#select-event-import-file').val()) return;
                $.ajax({
                    url: APL.ajax_url,
                    method: 'get',
                    dataType: 'json',
                    data: {
                        action: 'apollo_load_event_import_data',
                        filename: $('#select-event-import-file').val(),
                    },
                    beforeSend: function() {
                        $(window).block($.apl.blockUI);
                    },
                    success: function(res) {
                        $(window).unblock();
                        loadData = res.data
                        hot.loadData($.apl.importEvent.autoFillData(res.data));
                    }
                });

            });
        }


        $.apl.importEvent.viewFullScreen(hot);
        $.apl.importEvent.closeFS(hot);
        //$.apl.importEvent.retriveData(hot);

        /*Thienld: handle script to import event from CSV file */
        var $window = $(window);
        var eventAJAXImportHandler = new plupload.Uploader({
            runtimes : 'html5,flash,silverlight,html4',

            browse_button : 'import-event-by-csv-file-button', // you can pass in id...
            container: document.getElementById('wrap-input-file'), // ... or DOM Element itself

            url : APL.ajax_url + '?action=apollo_import_event_by_csv_file',

            filters : {
                max_file_size : '20971520b',
                mime_types: [
                    {title : 'CSV Import', extensions : "csv"}
                ],
            },
            init: {
                BeforeUpload: function(up, file) {
                },
                StateChanged: function(up) {
                    $window.block($.apl.blockUI);
                },
                FilesAdded: function(up, files) {
                    eventAJAXImportHandler.start();
                },
                UploadProgress: function(up, file) {
                },
                FileUploaded: function(up, file, info) {
                    var res = JSON.parse(info['response']);
                    if(res.success !== true) {
                        eventAJAXImportHandler.trigger('Error', {
                            code : plupload.GENERIC_ERROR,
                            message : res.msg
                        });
                        return false;
                    } else {
                        loadData = res.data;
                        hot.loadData($.apl.importEvent.autoFillData(res.data));
                    }
                },
                UploadComplete: function(up, files) {
                    // Called when all files are either uploaded or failed
                    eventAJAXImportHandler.splice();
                    eventAJAXImportHandler.refresh();
                    $window.unblock();

                },
                Error: function(up, err) {
                    alert(err.message);
                }
            }
        });
        /* init upload */
        eventAJAXImportHandler.init();
    });

    $.apl.importEvent.getSegmentData = function() {

        if ($.apl.importEvent.hotData === false) {
            $.apl.importEvent.hotData = hot.getData();
        }

        if ($.apl.importEvent.hotData.length == 0) {
            return false;
        }

        var $segmentData = $.apl.importEvent.hotData.slice(0, $.apl.importEvent.segmentStep);
        if ($segmentData.length == $.apl.importEvent.segmentStep) {
            $.apl.importEvent.hotData = $.apl.importEvent.hotData.slice($.apl.importEvent.segmentStep)
        } else {
            $.apl.importEvent.hotData = [];
        }

        /*remove empty object */
        for (var i in $.apl.importEvent.hotData) {
            if (!$.apl.importEvent.hotData[i] || !Object.keys($.apl.importEvent.hotData[i]).length) {
                delete $.apl.importEvent.hotData[i];
            }
        }

        return $.apl.importEvent.compressDataForAJAXSend($segmentData);
    }

    $.apl.importEvent.saveImport = function() {
        var input = $.apl.importEvent.getSegmentData();

        $.ajax({
            url: APL.ajax_url,
            method: 'post',
            dataType: 'json',
            data: {
                action: 'apollo_import_save_event',
                events: input,
                filename: $.apl.importEvent.getXmlFilename()
            },
            beforeSend: function() {
                if (!$('body').find('.blockUI').length) {
                    $(window).block($.apl.blockUI);
                }
            },
            success: function(res) {
                // If still remain data
                if (Object.keys($.apl.importEvent.hotData).length) {
                    $.apl.importEvent.saveImport();
                }
                else {
                    $(window).unblock();

                    // Reset data
                    $.apl.importEvent.hotData = false;

                    if (res.success) {
                        hot.clear();
                        alert(res.msg);
                        setTimeout(function() {
                            window.location.reload();
                        }, 2000);

                    } else {
                        alert(res.msg);
                    }
                }

            }
        });
    };

    $.apl.importEvent.saveImportForLater = function(tmpFile) {
        var input = $.apl.importEvent.getSegmentData();

        $.ajax({
            url: APL.ajax_url,
            method: 'post',
            dataType: 'json',
            data: {
                action: 'apollo_import_save_event_for_later',
                events: input,
                filename: $.apl.importEvent.getXmlFilename(),
                tmpFile: tmpFile
            },
            beforeSend: function() {
                if (!$('body').find('.blockUI').length) {
                    $(window).block($.apl.blockUI);
                }
            },
            success: function(res) {

                if (res.isMerging != 'undefined' && res.isMerging) {
                    $.apl.importEvent.saveImportForLater(res.tmpFile);
                }

                if (res.success) {
                    $(window).unblock();
                    // Reset data
                    $.apl.importEvent.hotData = false;
                    alert(res.msg);
                    if (!res.isEdit) {
                        setTimeout(function() {
                            window.location.reload();
                        }, 2000);
                    }
                }
            }
        });
    }

    $.apl.importEvent.autoFillData = function(data) {

        try {
            var output = [];

            if (data.length == undefined) {
                output.push(data);
            } else {
                output = data;
            }

            var total = output.length;

            if ($.apl.importEvent.totalRows > total) {
                var _total = $.apl.importEvent.totalRows - total;
                for ( var i = 0; i < _total; i++ ) {
                    output.push({});
                }
            }
        } catch(e) {
            for ( var i = 0; i < $.apl.importEvent.totalRows; i++ ) {
                output.push({});
            }
        }

        return output;
    };

    // view full screen
    $.apl.importEvent.viewFullScreen = function(hot){
        $('.full-screen a').click(function(){
            $('.evt-blk-import').addClass('fscreen');
            $('.close-fs').addClass('show');
            $('.wtHolder').css('width', $(window).width());
            $('.ht_clone_top').css('width', $(window).width());
            hot.selectCell(0, 0);
        });
    };

    $.apl.importEvent.closeFS = function(hot){
        $('.close-fs').click(function(){
            $('.evt-blk-import').removeClass('fscreen');
            $('.close-fs').removeClass('show');
            $('.wtHolder').css('width', $(window).width());
            $('.ht_clone_top').css('width', $(window).width());
            hot.selectCell(0, 0);
        });
    };

    $.apl.importEvent.retriveData = function(hot){
        $('#btn-submit').click(function(){
        });
    };

    $.apl.importEvent.coverRenderer = function(instance, td, row, col, prop, value, cellProperties) {
        var escaped = Handsontable.helper.stringify(value),
            img;

        if (escaped.indexOf('http') === 0) {
            img = document.createElement('IMG');
            img.src = value;

            Handsontable.Dom.addEvent(img, 'mousedown', function (e){
                e.preventDefault(); // prevent selection quirk
            });

            Handsontable.Dom.empty(td);
            td.appendChild(img);
        }
        else {
            // render as text
            Handsontable.renderers.TextRenderer.apply(this, arguments);
        }

        return td;
    };



    $.apl.importEvent.repoFormatResult = function (repo) {
        var markup = '<div class="row-fluid">'+repo.text+'</div>';
        console.log('test')
        return markup;
    };
    //
    $.apl.importEvent.repoFormatSelection = function (repo) {
        return repo.text;
    };

    $.apl.importEvent.customDropdownRenderer1 = function(instance, td, row, col, prop, value, cellProperties) {
        var selectedId;
        var optionsList = cellProperties.chosenOptions.data;

        if(typeof optionsList === "undefined" || typeof optionsList.length === "undefined" || !optionsList.length) {
            Handsontable.TextCell.renderer(instance, td, row, col, prop, value, cellProperties);
            return td;
        }

        var values = (value + "").split(",");
        value = [];
        for (var index = 0; index < optionsList.length; index++) {

            if (values.indexOf(optionsList[index].id + "") > -1) {
                selectedId = optionsList[index].id;
                value.push(optionsList[index].label);
            }
        }
        value = value.join(", ");

        Handsontable.TextCell.renderer(instance, td, row, col, prop, value, cellProperties);

        // Add icons
        $(td).append('<div class="apl-htAutocompleteArrow">▼</div>');

        return td;
    };

    $.apl.importEvent.customDropdownRendererAjaxOrg = function(instance, td, row, col, prop, value, cellProperties) {
        var selectedId;
        if (typeof globalOptionListOrg != 'undefined' && globalOptionListOrg) {
            var optionsList = globalOptionListOrg;
            for (var index = 0; index < optionsList.length; index++)
            {
                if (parseInt(value) === optionsList[index].id)
                {
                    selectedId = optionsList[index].id;
                    value = optionsList[index].text;
                    break;
                }
            }
        } else {
            /**
             * Fix load data for element is using ajax in case edit import
             * */
            if (typeof loadData != 'undefined' && loadData) {
                var totalData = loadData.length;
                if (row <= totalData) {
                    selectedId = value;
                    value = loadData[row]['_orgname'];
                }
            }
        }

        // Force remove value when you reset data
        if (typeof aplImportReset != 'undefined' && aplImportReset) {
            value = '';
        }

        Handsontable.DropdownCell.renderer.apply(this, arguments);
        // you can use the selectedId for posting to the DB or server
    };

    $.apl.importEvent.customDropdownRendererAjaxVenue = function(instance, td, row, col, prop, value, cellProperties) {
        var selectedId;


        if (typeof globalOptionListVenue != 'undefined' && globalOptionListVenue) {
            var optionsList = globalOptionListVenue;
            for (var index = 0; index < optionsList.length; index++)
            {
                if (parseInt(value) === optionsList[index].id)
                {
                    selectedId = optionsList[index].id;
                    value = optionsList[index].text;
                    break;
                }
            }
        } else {
            /**
             * Fix load data for element is using ajax in case edit import
             * */
            if (typeof loadData != 'undefined' && loadData) {
                var totalData = loadData.length;
                if (row <= totalData) {
                    selectedId = value;
                    value = loadData[row]['_venuename'];
                }
            }
        }

        // Force remove value when you reset data
        if (typeof aplImportReset != 'undefined' && aplImportReset) {
            value = '';
        }

        Handsontable.DropdownCell.renderer.apply(this, arguments);
        // you can use the selectedId for posting to the DB or server
    };

    $.apl.importEvent.customDropdownRenderer = function(instance, td, row, col, prop, value, cellProperties) {
        var selectedId;
        var optionsList = cellProperties.select2Options.data;

        if (optionsList) {
            for (var index = 0; index < optionsList.length; index++)
            {
                if (parseInt(value) === optionsList[index].id)
                {
                    selectedId = optionsList[index].id;
                    value = optionsList[index].text;
                }
            }
        }

        Handsontable.DropdownCell.renderer.apply(this, arguments);
        // you can use the selectedId for posting to the DB or server
    };

    $.apl.importEvent.customRender = function(instance, td, row, col, prop, value, cellProperties) {
        // change to the type of renderer you need; eg. if this is supposed
        // to be a checkbox, use CheckboxRenderer instead of TextRenderer
        Handsontable.renderers.TextRenderer.apply(this, arguments);

        // get the jquery selector for the td or add the class name using native JS
        $(td).addClass("error");

        return td;
    };

    $.apl.importEvent.showError = function(msg, isAlert) {

        if (! $.apl.importEvent.isShowErrorMsg) {
            return;
        }

        if ( !msg ) msg = $('.import-event.error').text();

        if (isAlert) {
            alert(msg);
            return;
        }

        if (msg) {
            $('.import-event.error').text(msg);
        }

        $('.import-event.error').fadeIn();
        $('html,body').animate({ scrollTop: $('.dsb-r').offset().top }, 500);
        setTimeout(function() {
            $('.import-event.error').fadeOut('slow');
        }, 8000);
    };
    $.apl.importEvent.getErrorMessageHtml = function(message){
        return '<span class="cell-error">'+message+'</span>';
    };

    $.apl.importEvent.scrollToErrorFirstCell = function () {
        setTimeout(function() {
            //timeout is needed because Handsontable normally deselects
            //current cell when you click outside the table
            hot.selectCell(
                $.apl.importEvent.firstIndexRowError, // row
                $.apl.importEvent.firstIndexColError, // col
                $.apl.importEvent.firstIndexRowError, // endRow
                $.apl.importEvent.firstIndexColError, // endCol
                true // scrollToCell
            );
        }, 10);
    };

    $.apl.importEvent.validateStartTime = function(data, row, arrErrorRows){
        /**
         * @ticket #18868: [CF] 20190109 - FE Event Import tool - Add time to start time dropdown
         * Check at least one start time must be entered
         */
        if(data["start-date"] && data["end-date"]){

            var startDate = new Date(data["start-date"]),
                endDate = new Date(data["end-date"]),
                emptyStartTime = true,
                validTimes = [];

            var validateDate = new Date(startDate);

            /**
             * check start time from startDate to endDate (include days of week)
             */
            for(var i = 0; i < 7; i++){
                if (validateDate > endDate) {
                    break;
                }
                var dayofWeek = "day" + validateDate.getDay();
                validTimes.push(validateDate.getDay());
                if(data[dayofWeek]){
                    emptyStartTime = false;
                    break;
                }

                validateDate.setDate(validateDate.getDate() + 1);
            }

            if(emptyStartTime){

                var messageError = $.apl.importEvent.getErrorMessageHtml( $.apl.importEvent.getTextTransByKey('cv-required-start-time')),
                    currentColumn = '';

                /**
                 * push error message for invalid dates
                 */
                for(var i = 0; i < validTimes.length; i++){
                    //sunday(value = 0)
                    currentColumn = !validTimes[i] ? (12+7) : (12 + validTimes[i]);
                    var column = hot.getCell(row, currentColumn);
                    if(!$(column).hasClass('htInvalid')){
                        $(column).addClass('htInvalid');
                        $(column).append(messageError);
                    }
                }

                if (arrErrorRows.indexOf(row) == -1) {
                    arrErrorRows.push(row);
                }

                if($.apl.importEvent.firstIndexRowError === -1 || $.apl.importEvent.firstIndexColError === -1){
                    $.apl.importEvent.firstIndexRowError = row;
                    $.apl.importEvent.firstIndexColError = currentColumn;
                }
            }
        }
    };

    $.apl.importEvent.validate = function(checkEmpty) {

        if (typeof checkEmpty === 'undefined') checkEmpty = false;

        var data = hot.getData(),
            arrEmptyRows = [],
            arrErrorRows = [],
            totalColumns = $.apl.importEvent.totalColumns,
            totalRows    = $.apl.importEvent.totalRows,
            emptyRowNum  = 0,
            $this        = $(this);
        $.apl.importEvent.firstIndexRowError = -1;
        $.apl.importEvent.firstIndexColError = -1;
        $('span.cell-error').remove();
        // Each row
        $.each(data, function(row, v) {
            
            if (hot.isEmptyRow(row)) {
                emptyRowNum++;
                return;
            }

            $.apl.importEvent.validateStartTime(v, row, arrErrorRows);

            var _emptyColNum = 0;
            for (var col=0; col<totalColumns; col++) {
                var cell = hot.getCell(row, col),
                    cellProperties = hot.getCellMeta(row, col),
                    $cell = $(cell);

                if (!hot.getDataAtCell(row, col)) {
                    _emptyColNum++;
                }


                if ( cellProperties.validator != undefined
                    && typeof cellProperties.validator != 'object'
                    && cellProperties.validator != undefined ) {

                    cellProperties.validator( hot.getDataAtCell(row, col), function(r, errorMessage ) {
                        if (!r) {
                            if (!checkEmpty) $cell.addClass('htInvalid');
                            $cell.append($.apl.importEvent.getErrorMessageHtml(errorMessage));
                            // Add row has error
                            if (arrErrorRows.indexOf(row) == -1) {
                                arrErrorRows.push(row);
                            }
                            if($.apl.importEvent.firstIndexRowError === -1 || $.apl.importEvent.firstIndexColError === -1){
                                $.apl.importEvent.firstIndexRowError = row;
                                $.apl.importEvent.firstIndexColError = col;
                            }
                        }
                    });
                }
            }
      
            // All columns are empty
            if (_emptyColNum == totalColumns) {
                arrErrorRows = [];
            } else if (arrErrorRows.length > 0) {

                var rowHeaders = jQuery('.ht_clone_left.handsontable tr')
                $(rowHeaders[row+1]).children().first().css('color', '#ff0000').css('font-weight', 'bold');
            }
        });

        
        if (emptyRowNum == totalRows && $.apl.importEvent.isShowErrorMsg) {
            
            if (checkEmpty) return true; // all field is empty
            
            alert('Please enter at least a row'); // should not put the text in js
            return false;
        }
        
        if ( arrErrorRows.length > 0 ) {
            
            if (checkEmpty) return false; // all field is not empty
            
            $.apl.importEvent.showError('', $('.event-btn.import .close-fs').hasClass('show'));
            return false;
        }
        
        return true;
    };
    $.apl.importEvent.getData =  function() {
        var data = [];
        for(var $i = 0; $i < $.apl.importEvent.totalRows; $i++ ) {
            data.push({});
        }
        return data;
    };
    
    $.apl.importEvent.getXmlFilename = function() {
        return $('#select-event-import-file').length ? $('#select-event-import-file').val() : '';
    };

    $.apl.importEvent.escapeQuoteInData = function(eachRow){
        var result = {};
        _.each(eachRow, function(item,key){
            result[key] = item.replace(/\"/g,'@@@');
        });
        return result;
    }

    $.apl.importEvent.compressDataForAJAXSend = function(passData){
        var results = [];
        _.each(passData, function (item) {
            if(!_.isEmpty(item)){
                //item = $.apl.importEvent.escapeQuoteInData(item);
                results.push(item);
            }
        });

        return results;

        //var stringifyData = JSON.stringify(results);
        //return encodeURIComponent(stringifyData);
    };

    $.apl.importEvent.getTextTransByKey = function(key) {
        var inpHide = $('#' + key);
        return !_.isEmpty(inpHide) ? inpHide.val() : '';
    };
    
    /// select2 plugin
    (function (Handsontable) {
        "use strict";

        var Select2Editor = Handsontable.editors.TextEditor.prototype.extend();

        Select2Editor.prototype.prepare = function (row, col, prop, td, originalValue, cellProperties) {

            Handsontable.editors.TextEditor.prototype.prepare.apply(this, arguments);

            this.options = {};

            if (this.cellProperties.select2Options) {
                this.options = $.extend(this.options, cellProperties.select2Options);
            }
        };

        Select2Editor.prototype.createElements = function () {
            this.$body = $(document.body);

            this.TEXTAREA = document.createElement('input');
            this.TEXTAREA.setAttribute('type', 'text');
            this.$textarea = $(this.TEXTAREA);

            Handsontable.Dom.addClass(this.TEXTAREA, 'handsontableInput');

            this.textareaStyle = this.TEXTAREA.style;
            this.textareaStyle.width = 0;
            this.textareaStyle.height = 0;

            this.TEXTAREA_PARENT = document.createElement('DIV');
            Handsontable.Dom.addClass(this.TEXTAREA_PARENT, 'handsontableInputHolder');

            this.textareaParentStyle = this.TEXTAREA_PARENT.style;
            this.textareaParentStyle.top = 0;
            this.textareaParentStyle.left = 0;
            this.textareaParentStyle.display = 'none';

            this.TEXTAREA_PARENT.appendChild(this.TEXTAREA);

            this.instance.rootElement.appendChild(this.TEXTAREA_PARENT);

            var that = this;
            this.instance._registerTimeout(setTimeout(function () {
                that.refreshDimensions();
            }, 0));
        };

        var onSelect2Changed = function () {
            this.close();
            this.finishEditing();
        };
        var onSelect2Closed = function () {
            this.close();
            this.finishEditing();
        };
        var onBeforeKeyDown = function (event) {
            var instance = this;
            var that = instance.getActiveEditor();

            var keyCodes = Handsontable.helper.keyCode;
            var ctrlDown = (event.ctrlKey || event.metaKey) && !event.altKey; //catch CTRL but not right ALT (which in some systems triggers ALT+CTRL)

            //Process only events that have been fired in the editor
            if (!$(event.target).hasClass('select2-input') || event.isImmediatePropagationStopped()) {
                return;
            }
            if (event.keyCode === 17 || event.keyCode === 224 || event.keyCode === 91 || event.keyCode === 93) {
                //when CTRL or its equivalent is pressed and cell is edited, don't prepare selectable text in textarea
                event.stopImmediatePropagation();
                return;
            }

            var target = event.target;

            switch (event.keyCode) {
                case keyCodes.ARROW_RIGHT:
                    if (Handsontable.Dom.getCaretPosition(target) !== target.value.length) {
                        event.stopImmediatePropagation();
                    } else {
                        that.$textarea.ImportSelect2('close');
                    }
                    break;

                case keyCodes.ARROW_LEFT:
                    if (Handsontable.Dom.getCaretPosition(target) !== 0) {
                        event.stopImmediatePropagation();
                    } else {
                        that.$textarea.ImportSelect2('close');
                    }
                    break;

                case keyCodes.ENTER:
                    var selected = that.instance.getSelected();
                    var isMultipleSelection = !(selected[0] === selected[2] && selected[1] === selected[3]);
                    if ((ctrlDown && !isMultipleSelection) || event.altKey) { //if ctrl+enter or alt+enter, add new line
                        if (that.isOpened()) {
                            that.val(that.val() + '\n');
                            that.focus();
                        } else {
                            that.beginEditing(that.originalValue + '\n')
                        }
                        event.stopImmediatePropagation();
                    }
                    event.preventDefault(); //don't add newline to field
                    break;

                case keyCodes.A:
                case keyCodes.X:
                case keyCodes.C:
                case keyCodes.V:
                    if (ctrlDown) {
                        event.stopImmediatePropagation(); //CTRL+A, CTRL+C, CTRL+V, CTRL+X should only work locally when cell is edited (not in table context)
                    }
                    break;

                case keyCodes.BACKSPACE:
                case keyCodes.DELETE:
                case keyCodes.HOME:
                case keyCodes.END:
                    event.stopImmediatePropagation(); //backspace, delete, home, end should only work locally when cell is edited (not in table context)
                    break;
            }

        };

        Select2Editor.prototype.open = function (keyboardEvent) {
            this.refreshDimensions();
            this.textareaParentStyle.display = 'block';
            this.instance.addHook('beforeKeyDown', onBeforeKeyDown);

            this.$textarea.css({
                height: $(this.TD).height() + 4,
                'min-width': $(this.TD).outerWidth() - 4
            });

            //display the list
            this.$textarea.show();

            //make sure that list positions matches cell position
           //this.$textarea.offset($(this.TD).offset());

            var self = this;
            this.$textarea.ImportSelect2(this.options)
                .on('change', onSelect2Changed.bind(this))
                .on('select2-close', onSelect2Closed.bind(this));

            self.$textarea.ImportSelect2('open');

            if (keyboardEvent && keyboardEvent.keyCode) {
                var key = keyboardEvent.keyCode;
                var keyText = (String.fromCharCode((96 <= key && key <= 105) ? key-48 : key)).toLowerCase();
                self.$textarea.ImportSelect2('search', keyText);
            }
        };

        Select2Editor.prototype.init = function () {
            Handsontable.editors.TextEditor.prototype.init.apply(this, arguments);
        };

        Select2Editor.prototype.close = function () {
            this.instance.listen();
            this.instance.removeHook('beforeKeyDown', onBeforeKeyDown);
            this.$textarea.off();
            this.$textarea.hide();
            Handsontable.editors.TextEditor.prototype.close.apply(this, arguments);
        };

        Select2Editor.prototype.val = function (value) {
            if (typeof value == 'undefined') {
                return this.$textarea.val();
            } else {
                this.$textarea.val(value);
            }
        };


        Select2Editor.prototype.focus = function () {

            this.instance.listen();

            // DO NOT CALL THE BASE TEXTEDITOR FOCUS METHOD HERE, IT CAN MAKE THIS EDITOR BEHAVE POORLY AND HAS NO PURPOSE WITHIN THE CONTEXT OF THIS EDITOR
            //Handsontable.editors.TextEditor.prototype.focus.apply(this, arguments);
        };

        Select2Editor.prototype.beginEditing = function (initialValue) {
            var onBeginEditing = this.instance.getSettings().onBeginEditing;
            if (onBeginEditing && onBeginEditing() === false) {
                return;
            }

            Handsontable.editors.TextEditor.prototype.beginEditing.apply(this, arguments);

        };

        Select2Editor.prototype.finishEditing = function (isCancelled, ctrlDown) {
            this.instance.listen();
            return Handsontable.editors.TextEditor.prototype.finishEditing.apply(this, arguments);
        };

        Handsontable.editors.Select2Editor = Select2Editor;
        Handsontable.editors.registerEditor('select2', Select2Editor);

    })(Handsontable);
    
})(jQuery);
