var APLWidget = function ($) {

    function aplAccessibilityItemClick(e) {
        var current = $(e.currentTarget);
        var parent = current.closest('.mutliSelect');
        if ( parent.hasClass('right') ) {
            var parentName = '#' + parent.attr('id') + '.right';
        } else {
            var parentName = '#' + parent.attr('id') + '.top';
        }

        var accessName = current.attr('name');
        renderItemMultiSelected(parentName, accessName);
    }

    function renderItemMultiSelected(parent, accessName) {
        // event: accessName = accessibility[];
        // venue: accessName = custom[];
        var accessInput = parent + ' input[name="'+accessName+'"]:checked';
        var access = $(accessInput);
        var selectText = $(parent).closest('dl.accessibilyty-dropdown').find('dt span.access-drop');
        var text = '';
        if ( access.length > 0 && access.length < 3) {
            $.each(access, function( index, item) {
                text += $(item).attr('access-name') + ', ';
            });
            if (selectText.length > 0 && text != '') {
                selectText.text(text.slice(0, -2));
            }
        } else if ( access.length > 2 ) {
            if (selectText.length > 0) {
                text = access.length + ' ' + selectText.attr('selected-text');
                selectText.text( text );
            }
        } else {
            if(selectText.length > 0) {
                selectText.text( selectText.attr('default-text') );
            }
        }
    }

    /**
     * Hide the wrapper bottom home blog feature if the content is empty
     * */
    function hideBottomHomeFeatureBlogWidgetWrapper() {
        try {
            var $homeBlog = $('.home .apl_bottom_home_blog_feature_widget .inner');
            if ($homeBlog.length && $homeBlog.html().replace(/\s/g, '') == "") {
                $('.apl_bottom_home_blog_feature_widget').hide();
            }
        }
        catch (e) {
            console.log(e);
        }
    }

    /** @Ticket #17838 show/hide Dining Services
     * Listing: Only show dining service when the current category is dining
     * Detail page: only show when the business selected dining cat or is a restaurant
     * */
    var showHideBusinessServices = function (current, onChange) {
        var isDining = current.find(':selected');
        var inputDining = current.closest('form').find('.is_dining_cate');
        var diningServices = current.closest('form').find('.apl-business-dining-services');
        var diningValue = 0;
        var showServices = false;
        if (diningServices.hasClass('is-restaurant') && !onChange) {
            /** Business is restaurant */
            showServices = true;
            diningValue = 1;
        } else if (diningServices.hasClass('is-single') && !onChange) {
            /** Current selected categories is dining */
            var listCateElement = current.closest('body').find('.single-business-cate-ids');
            var listIds = listCateElement.attr('data-ids');
            if (typeof listIds !== 'undefined' && listIds != '') {
                var listDiningCate = current.find('[data-dining="1"]');
                if (listDiningCate.length > 0) {
                    listIds = listIds.split(',');
                    $.each(listDiningCate, function (index, item) {
                        if (listIds.indexOf($(item).val()) > -1) {
                            showServices = true;
                            diningValue = 1;
                            return true;
                        }
                    });
                }
            }
        } else if (typeof isDining !== 'undefined' && isDining.val() != '' && isDining.attr('data-dining') == 1) {
            /** Change business type */
            showServices = true;
            diningValue = 1;
        }

        if (diningServices.length > 0) {
            if (showServices) {
                diningServices.removeClass('hidden');
            } else {
                diningServices.addClass('hidden');
                diningServices.find('[name="business_service[]"]').prop('checked', false);
            }
        }

        if (inputDining.length > 0) {
            inputDining.val(diningValue);
        }
    };

    /*@ticket #17933, 17934: render associate term for business detail page */
    var renderAssociateBusinessTerm = function (listTerms) {

        var assoicateTerm = $('.single-business .single-business-cate-ids').data('ids');

        if(assoicateTerm){
            assoicateTerm = JSON.parse("[" + assoicateTerm + "]");
            $.each(listTerms, function(index) {
                if(index){
                    var parentId = $(this)[0].getAttribute('data-first-parent');
                    if (!isNaN(parseInt(parentId)) && assoicateTerm.indexOf(parseInt(parentId)) === -1){
                        $(this).remove();
                    }
                }
            });
        }
    };

    var checkBusinessWidget = function () {
        var desktop = $('#search-business select[name="term"]');
        if (desktop.length > 0) {
            showHideBusinessServices(desktop, false);
            renderAssociateBusinessTerm($('#search-business select[name="term"] option'));
        }
        var mobile  = $('#search-business-m-t select[name="term"]');
        if (mobile.length > 0) {
            showHideBusinessServices(mobile, false);
            renderAssociateBusinessTerm($('#search-business-m-t select[name="term"] option'));
        }
    };

    return {
        //main function to initiate the module
        init: function () {
            /** @Ticket #13151 */
                //Event
            renderItemMultiSelected('#event-accessibility-icons-search.right', 'accessibility[]');
            renderItemMultiSelected('#event-accessibility-icons-search.top', 'accessibility[]');

            //Venue
            renderItemMultiSelected('#venue-accessibility-checkbox.right', 'custom[]');
            renderItemMultiSelected('#venue-accessibility-checkbox.top', 'custom[]');

            $('.apl-accessibility-item').off('click').on('click', aplAccessibilityItemClick);

            hideBottomHomeFeatureBlogWidgetWrapper();

            $('#search-business').off('change', 'select[name="term"]').on('change', 'select[name="term"]', function(e){
                var current = $(e.currentTarget);
                showHideBusinessServices(current, true);
            });
            $('#search-business-m-t').off('change', 'select[name="term"]').on('change', 'select[name="term"]', function(e){
                var current = $(e.currentTarget);
                showHideBusinessServices(current, true);
            });
            checkBusinessWidget();
        }
    };

}(jQuery);

jQuery(document).ready(function() {
    APLWidget.init();
});
