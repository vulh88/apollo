(function ($) {
    window.APOLLO =  window.APOLLO || {};
    window.APOLLO.formArtistValidation = (function(){
        var module = {},
            currentForm;

        module.init = function(){
            if(window.APOLLO.formAbstractValidation.enabledJqueryValidation) {
                $(document).ready( function() {
                    currentForm = $('#artist-frm');
                    currentForm.validationEngine(
                        'attach',
                        {   promptPosition:'inline',
                            scroll:true,
                            scrollOffset:window.APOLLO.formAbstractValidation.constantOffsetTop
                        }
                    );
                    currentForm.find('.submit-form-with-validation-engine').off('click').on('click', handleCurrentFormSubmission);
                    window.APOLLO.formAbstractValidation.customFormValidationErrorMessageHandler(currentForm);
                });
            }
            return module;
        };

        var handleCurrentFormSubmission = function (event) {
            var submitSuccessfulMessage = $('._apollo_success');
            if(submitSuccessfulMessage.length > 0){
                submitSuccessfulMessage.remove();
            }
            var btnSubmit = $(event.currentTarget);
            btnSubmit.css('pointer-events', '');
            //currentForm.find('.formError').remove();
            var resultValidation = currentForm.validationEngine(
                'validate',
                {   promptPosition:'inline',
                    scroll: false,
                    scrollOffset:window.APOLLO.formAbstractValidation.constantOffsetTop
                }
            );
            var resultCustomValidation = window.APOLLO.formAbstractValidation.customValidationForm(currentForm);

            var error = false;

            if(resultValidation && resultCustomValidation){
                btnSubmit.css('pointer-events', 'none');
            } else if(resultValidation && !resultCustomValidation) {
                btnSubmit.css('pointer-events', '');
                error = true;
            }

            /**
             * @ticket #18830: Move the error to display in the image upload section under the 'PRIMARY IMAGE' label
             */
            var photoError = 0;
            if( $('#artist-profile-photo-frm').length && !window.APOLLO.formAbstractValidation.validateRequiredPrimaryImage(event, 'no-scroll')){
                btnSubmit.css('pointer-events', '');
                error = true;
                photoError = 1;
            }


            /**
             * @ticket #18829:[FE Artist Form] Modify the error message
             * if there are more than one error a message must appear at the top of the page
             */
            var formError = currentForm.find('.formError').length;
            if(photoError +  formError > 1){
                showMessageError(currentForm);
                $('html, body').animate({scrollTop: 100}, 1000);
            }
            else{

                if(photoError){
                    $('html, body').animate({scrollTop: $('#artist-profile-photo-frm').offset().top}, 1000);}

                if(formError){
                    window.APOLLO.formAbstractValidation.scrollToCustomValidationError();
                }
            }

            return !error;
        };


        var showMessageError = function(currentForm){
            var errorComponent = $('.apl-message-error');
            if(currentForm.length && errorComponent.length){
                errorComponent.removeClass('hidden');
            }
        };

        return module.init();
    })();
})(jQuery);