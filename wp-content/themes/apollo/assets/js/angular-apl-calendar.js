var aplHeaders = {
    "Apl-Frontend": 1
};

moment.locale('es');
moment.locale('es', {
    monthsShort : [
        jQuery.apl.getText("Jan"),
        jQuery.apl.getText("Feb"), 
        jQuery.apl.getText("Mar"), 
        jQuery.apl.getText("Apr"), 
        jQuery.apl.getText("May"), 
        jQuery.apl.getText("Jun"),
        jQuery.apl.getText("Jul"), 
        jQuery.apl.getText("Aug"), 
        jQuery.apl.getText("Sep"), 
        jQuery.apl.getText("Oct"), 
        jQuery.apl.getText("Nov"), 
        jQuery.apl.getText("Dec")
    ]
});

if ( typeof angular != "undefined" ) {
    jQuery.fn._calendar = {};
    jQuery.fn._calendar.sstart = " ";
    jQuery.fn._calendar.send = " ";
    var list_msg = jQuery('#list-msg-calendar');
    var calendar_header = '<div class="month-bar">' +
            '<span class="start_month">{{sdm.format("MMM")}}</span><span ng-class="{ y: true, hidden: sameYear()}">({{sdm.format("YYYY")}})</span>' + '<span class="title"> - </span><span class="end_month">{{edm.format("MMM")}}</span><span class="y">({{edm.format("YYYY")}})</span>' +
            '<div class="today hidden"> <span>'+list_msg.data('today')+'</span></div>' +
            '<div class="next-month hidden">' +
            '<div class="prev-b" id="_apl_ca_prev" ng-click="previous()"><i  class="fa fa-chevron-left fa-2x"></i></div>' +
            '<div class="next-b" id="_apl_ca_next" ng-click="next()"><i class="fa fa-chevron-right fa-2x"></i></div>' +
            '</div>' +
            '</div>'
        ;

    var calendar_week = '<div class="cl-head">' +
        '<div class="day sun">'+list_msg.data('sun')+'</div>' +
        '<div class="day">'+list_msg.data('mon')+'</div>' +
        '<div class="day">'+list_msg.data('tue')+'</div>' +
        '<div class="day">'+list_msg.data('wed')+'</div>' +
        '<div class="day">'+list_msg.data('thu')+'</div>' +
        '<div class="day">'+list_msg.data('fri')+'</div>' +
        '<div class="day">'+list_msg.data('sat')+'</div>' +
        '</div>';

    var calendar_day = '<div class="cl-date">' +
        '<div class="date-week" ng-repeat="week in weeks"> ' +
        '<div class="date-cell" data-date="{{day.date_value}}" ng-class="{ today: day.isToday, disable: day.outrange, \'different-month\': !day.isCurrentMonth, selected: day.date.isSame(selected) }" ng-repeat="day in week.days" ng-click="select(day, $event)">' +
        '<div class="delete-calendar-cell" ng-click="deleteSelect(day,$event)"><i class="fa fa-times"></i></div>'+
        '<div class="undo-delete-calendar-cell" ng-click="undoDeleteSelect(day,$event)"><i class="fa fa-plus"></i></div>'+
        '<div class="day">' +
        '<span ng-class="{sun: day.name == \'SU\', dName: true}">{{day.name}}</span>' +
        '<span ng-class="{sun: day.name == \'SU\', num: day.name != \'SU\'}">{{day.number}}</span> ' +
        '</div>' +

        '<div class="contain-vm-large event"> ' +
        '<ul class="cl-event-list">' +
        '<li class="evt-itm" ng-repeat="eventtime in eventtimes[day.date_value] |  orderBy:orderTime | limitTo:3" ng-click="edittime(eventtime,$index, $event)"><a href="javascript:void(0);"><span class="from">{{eventtime.time_from | timeampm}}</span><span ng-class="{\'hidden\': isnulltt(eventtime.time_to)}" >-</span><span class="to">{{eventtime.time_to | timeampm}}</span></a></li>' +
        '</ul> ' +
        '</div>' +


        '<div class="contain-vm-small event"> ' +
        '<ul class="cl-event-list">' +
        '<li class="evt-itm" ng-repeat="eventtime in eventtimes[day.date_value] |  orderBy:orderTime " ng-click="edittime(eventtime,$index, $event)"><a href="javascript:void(0);"><span class="from">{{eventtime.time_from | timeampm}}</span><span ng-class="{\'hidden\': isnulltt(eventtime.time_to)}" >-</span><span class="to">{{eventtime.time_to | timeampm}}</span></a></li>' +
        '</ul> ' +
        '</div>' +

        '<span ng-class="{viewmore: true, hidden: !moreThanThree(eventtimes[day.date_value])}" ng-click="viewmore(eventtimes[day.date_value], $event)">'+list_msg.data('3more')+'</span>' +

        '</div>' +
        '</div>' +
        '</div>';

    var calendar_2 = '<div class="cl-head">' +
        '<div class="day sun">'+list_msg.data('sun')+'</div>' +
        '<div class="day">'+list_msg.data('mon')+'</div>' +
        '<div class="day">'+list_msg.data('tue')+'</div>' +
        '<div class="day">'+list_msg.data('wed')+'</div>' +
        '<div class="day">'+list_msg.data('thu')+'</div>' +
        '<div class="day">'+list_msg.data('fri')+'</div>' +
        '<div class="day">'+list_msg.data('sat')+'</div>' +
        '</div>' +
        '<div class="cl-date">' +
        '<div class="date-cell" ng-class="{ disable: disableDayInCa2(key, enableDays) }" ng-repeat=" (key, d) in specialc.days  " data-date="{{key}}" ng-click="select(key, $event)">' +
        '<div class="day"><span ng-class="{dName: true, sun: key === \'SUN\' }">{{d}}</span></div>' +
        '<div class="event">' +
        '<ul class="cl-event-list">' +
        '<li class="evt-itm" ng-repeat="t in specialc.eventtimes[key] | orderBy:orderTime " ng-click="edittime(t,$index, $event)"><a href="javascript:void(0);"><span class="from">{{t.time_from | timeampm}}</span><span ng-class="{\'hidden\': isnulltt(t.time_to)}" >-</span><span class="to">{{t.time_to | timeampm}}</span></a></li>' +
        '</ul>' +
        '</div>' +
        '</div>' +
        '</div>';


    var _lB = '<div class="light-box" onclick="javascript:void(0);"></div>';

    var calendar_t = calendar_header + calendar_week + calendar_day;


    function get_calendar_timer(is_edit) {
        var calendar_timer_boxAdd = "";
        calendar_timer_boxAdd = '<div id="box_time_calendar" class="box" style="display: none; top: 621px; left: 798.65px;">';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<div class="time-bar">';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<div class="start">';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<label> &nbsp; '+list_msg.data('start-time')+'</label>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<select class="time _hour1" >';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="01">01</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="02">02</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="03">03</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="04">04</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="05">05</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="06">06</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="07">07</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="08">08</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="09">09</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="10">10</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="11">11</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="12">12</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '</select>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<span class="arrowH"></span>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<select class="time mins _minute1" >';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="00">00</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="05">05</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="10">10</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="15">15</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="20">20</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="25">25</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="30">30</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="35">35</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="40">40</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="45">45</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="50">50</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="55">55</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '</select>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<span class="arrowM"></span>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<select class="time ap _ampm1">';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="AM">'+list_msg.data('am')+'</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option selected value="PM">'+list_msg.data('pm')+'</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '</select>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<span class="arrowAP"></span>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<span class="clock"><i class="fa fa-clock-o"></i></span>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '</div>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<div class="end">';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<label> &nbsp; '+list_msg.data('end-time')+'</label>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<select class="time _hour2">';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="--">--</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="01">01</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="02">02</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="03">03</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="04">04</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="05">05</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="06">06</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="07">07</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="08">08</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="09">09</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="10">10</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="11">11</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="12">12</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '</select>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<span class="arrowH"></span>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<select class="time mins _minute2">';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="--">--</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="00">00</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="05">05</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="10">10</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="15">15</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="20">20</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="25">25</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="30">30</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="35">35</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="40">40</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="45">45</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="50">50</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="55">55</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '</select>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<span class="arrowM"></span>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<select class="time ap _ampm2">';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="--">--</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="AM">'+list_msg.data('am')+'</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<option value="PM">'+list_msg.data('pm')+'</option>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '</select>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<span class="arrowAP"></span>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<span class="clock"><i class="fa fa-clock-o"></i></span>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '</div>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '</div>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<div class="popup-btn">';

        if(is_edit === true) {
            calendar_timer_boxAdd = calendar_timer_boxAdd + '<button class="btn-noW" id="add-new-event" type="submit">'+list_msg.data('edit')+'</button>';
            calendar_timer_boxAdd = calendar_timer_boxAdd + '<button class="btn-noW" id="delete-event" type="submit">'+list_msg.data('del')+'</button>';
        }
        else {
            calendar_timer_boxAdd = calendar_timer_boxAdd + '<button class="btn-noW" id="add-new-event" type="submit">'+list_msg.data('add-new')+'</button>';
        }
        calendar_timer_boxAdd = calendar_timer_boxAdd + '</div>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '<div class="closepopup"><img src="' +APL.assert_url+ '/images/close-ico.png"></div>';
        calendar_timer_boxAdd = calendar_timer_boxAdd + '</div>';

        return calendar_timer_boxAdd;
    }



    jQuery.fn._calendar.mydefere = {
        then: function callback(callback) {
            callback();
        }
    };
    (function($) {
        jQuery.fn._calendar.closeModal = function(){
            $(document).on('click', '.light-box', function(e){

                e.stopPropagation();

                if($('.box').length){
                    $('.closepopup').trigger('click');
                }
                if($('.viewmore-box').length){
                    $('.closeviewmore').trigger('click');
                }
                if($('.boxEdit').length){
                    $('.closepopup').trigger('click');
                }
            });
        };

        jQuery.fn._calendar.findPos = function (obj, offsetpclass) {
            var left = 0 , top = 0;

            if (obj.offsetParent) {
                do {
                    left += obj.offsetLeft;
                    top += obj.offsetTop;

                } while ((obj = obj.offsetParent) && !obj.classList.contains(offsetpclass));
            }

            return {left: left, top: top};
        };

        jQuery.fn._calendar.closeModal();
    })(jQuery);


    var app = angular.module("dashboard", []).filter('timeampm', function() {
        return function(input) {
            input = input || '';

            if(input === '')
                return '';

            var ai = input.split(':');
            var out = [];

            if(parseInt(ai[0], 10) < 12 )
            {
                if(ai[0] === '00') {
                    out.push(12);
                }
                else {
                    out.push(ai[0]);
                }

                out.push(ai[1]);
                out.push(list_msg.data('am'));
            }
            else {
                if(ai[0] === '12') {
                    out.push(12);
                }
                else {
                    out.push(ai[0] - 12);
                }

                out.push(ai[1]);
                out.push(list_msg.data('pm'));
            }

            var _strTime = out.pop(2);
            return out.join(':')+ ' '+ _strTime;
        };
    });

    app.controller("calendar", function($scope) {
        $scope.day = moment();
    });


    app.service(
        "eventTimeService",
        function( $http, $q ) {

            // Return public API.
            return({
                getAllEventTime: getAllEventTime,

                createEventTime: createEventTime,
                createEventTimes: createEventTimes,
                editEventTime: editEventTime,
                editEventTimes: editEventTimes,
                deleteEventTime: deleteEventTime,
                deleteEventTimes: deleteEventTimes,
                deleteAllEventCell: deleteAllEventCell,
                getEventDisableCell: getEventDisableCell,
                updateAdditionalTime: updateAdditionalTime,
                restoreEventCell: restoreEventCell,
            });

            // ---
            // PUBLIC METHODS.
            // ---

            function createEventTime(scope, event_id, date_event, time_from, time_to) {
                var request = $http({
                    method: "get",
                    url: APL.ajax_url,
                    headers: aplHeaders,
                    params: {
                        action: "apollo_create_event_time",
                        event_id: event_id,
                        date_event: date_event,
                        time_from: time_from,
                        time_to: time_to
                    }
                });

                return( request.then( function(res) {
                    if(res.data.result == false) {
                        handleError(res);
                        return;
                    }

                    if(scope.type === '1' ) {
                        if(typeof scope.eventtimes[date_event] === 'undefined') {
                            scope.eventtimes[date_event] = [];
                        }
                        
                        scope.eventtimes[date_event].push(res.data.result);
                    }
                    else {

                        try {
                            jQuery.each(res.data.result, function(d, v) {

                                if(typeof scope.eventtimes[d] === 'undefined') {
                                    scope.eventtimes[d] = [];
                                }
                                scope.eventtimes[d].push(v[0]);
                            });
                        } catch(e) {}
                    }


                }, handleError ) );
            }

            // special create
            function createEventTimes(scope, event_id, date_event1, date_event2, time_from, time_to, nonce) {
                var request = $http({
                    method: "get",
                    url: APL.ajax_url,
                    headers: aplHeaders,
                    params: {
                        action: "apollo_create_event_times",
                        event_id: event_id,
                        date_event1: date_event1,
                        date_event2: date_event2,
                        time_from: time_from,
                        time_to: time_to,
                        _aplAddEventNonceField: nonce
                    }
                });

                return( request.then( function(res) {
               
                    if(res.data.result == false) {
                        handleError(res);
                        return;
                    }

                    jQuery.each(res.data.result, function(k, v) {
                        if(typeof scope.eventtimes[k] === 'undefined') {
                            scope.eventtimes[k] = [];
                        }
                        if(!scope.checkExistEvent(scope.eventtimes[k], v[0])) {
                            var date = v[0].date_event;
                            //TriLM
                            //not apply all for disable cell
                            if(!jQuery('div[data-date="'+date+'"]').hasClass('disable')){
                                scope.eventtimes[k].push(v[0]);
                            }
                        }
                    });

                }, handleError ) );
            }

            function getAllEventTime(scope, event_id) {
                var _blockUI = {
                    message: APL.resource.loading.message,
                    css: jQuery.parseJSON(APL.resource.loading.image_style)
                };
                var $body = jQuery('body');
                jQuery(window).block(_blockUI);
                var request = $http({
                    method: "get",
                    url: APL.ajax_url,
                    headers: aplHeaders,
                    params: {
                        action: "apollo_get_all_event_time",
                        event_id: event_id
                    }

                });

                return( request.then( function(res) {
                    scope.eventtimes = res.data.result;
                    //TriLM: init disable event cell
                    getEventDisableCell(scope.eid);
                    $body.unblock();
                }, handleError ) );
            }

            //TriLM
            function getEventDisableCell(eventID){
                var request = $http({
                    method: "get",
                    url: APL.ajax_url,
                    headers: aplHeaders,
                    params: {
                        action: "apollo_get_disable_cell_event",
                        event_id: eventID
                    }
                });
                return( request.then( function(res) {
                    if(res.data.result) {
                        jQuery.each(res.data.result,function(d,v){
                            jQuery('div[data-date="'+v+'"]').addClass('disable');
                            jQuery('div[data-date="'+v+'"]').addClass('inactive');
                        });
                    }
                }, handleError ) );
            }

            function updateAdditionalTime(eventID, str ){
                var request = $http({
                    method: "get",
                    url: APL.ajax_url,
                    headers: aplHeaders,
                    params: {
                        action: "apollo_update_additional_time",
                        event_id: eventID,
                        content: str,
                    }
                });
                return( request.then( function(res) {

                }, handleError ) );
            }
            //end TriLM

            function editEventTime($scope, date_event, $otime_from, $otime_to, $time_from, $time_to) {
                var request = $http({
                    method: "get",
                    url: APL.ajax_url,
                    headers: aplHeaders,
                    params: {
                        action: "apollo_edit_event_time",
                        date_event: date_event,
                        event_id: $scope.eid,
                        otime_from: $otime_from,
                        otime_to: $otime_to,
                        time_from: $time_from,
                        time_to: $time_to
                    }
                });

                return( request.then( function(res) {
                    if(res.data.result === false) {
                        handleError(res);
                        return false;
                    }

                    if(res.data.result === 0) {
                        return false;
                    }

                    if(res.data.result.error === true) {
                        alert(res.data.result.msg);
                        return false;
                    }

                    //@todo here
                    var key = parseInt(jQuery("#box_time_calendar").data('key-time'), 10);
                    if($scope.type === '1') {
                        /**
                         * vulh sort array datetime for each date again before edit or add new
                         * */
                        try {
                            $scope.eventtimes[date_event].sort(function(a, b) {
                                return parseInt(a.time_from) > parseInt(b.time_from);
                            });
                        }catch(e) {}

                        $scope.eventtimes[date_event][key] = {
                            event_id: $scope.eid,
                            date_event: date_event,
                            time_from: $time_from,
                            time_to: $time_to
                        };

                    }
                    else {
                        jQuery.each(res.data.result.data, function(_, d) {

                            /**
                             * vulh sort array datetime for each date again before edit or add new
                             * */
                            try {
                                $scope.eventtimes[d].sort(function(a, b) {
                                    return parseInt(a.time_from) > parseInt(b.time_from);
                                });
                            }catch(e) {}

                            $scope.eventtimes[d][key] = {
                                event_id: $scope.eid,
                                date_event: date_event,
                                time_from: $time_from,
                                time_to: $time_to
                            };
                        });
                    }

                }, handleError ) );
            }

            function editEventTimes($scope, $ids, $time_from, $time_to) {
                var request = $http({
                    method: "get",
                    url: APL.ajax_url,
                    headers: aplHeaders,
                    params: {
                        action: "apollo_edit_event_times",
                        id: $ids,
                        time_from: $time_from,
                        time_to: $time_to
                    }
                });

                return( request.then( handleSuccess, handleError ) );
            }

            function deleteEventTime($scope, date_event, time_from, time_to) {

                // validate

                var request = $http({
                    method: "get",
                    url: APL.ajax_url,
                    headers: aplHeaders,
                    params: {
                        action: "apollo_delete_event_time",
                        event_id: $scope.eid,
                        date_event:  date_event,
                        time_from: time_from,
                        time_to: time_to,
                        is_front: 1,
                    }
                });

                return( request.then( function(res) {

                    if(res.data.result === false) {
                        alert(list_msg.data('missing-error'));
                        return false;
                    }

                    if(typeof res.data.result.error !== 'undefined' && res.data.result.error === true) {
                        if(alert(res.data.result.msg)) {
                            if(res.result.require === 'refresh') {
                                window.location.href=window.location.redirect();
                                return;
                            }
                        }
                    }

                    var key = parseInt(jQuery("#box_time_calendar").data('key-time'), 10);
                    if($scope.type === '1') {
                        try {
                            $scope.eventtimes[date_event].sort(function(a, b) {
                                return parseInt(a.time_from) > parseInt(b.time_from);
                            });
                        }catch(e) {}
                        $scope.eventtimes[date_event].splice(key, 1);
                    }
                    else {
                        jQuery.each(res.data.result.data, function(_, d) {
                            try {
                                $scope.eventtimes[d].sort(function(a, b) {
                                    return parseInt(a.time_from) > parseInt(b.time_from);
                                });
                            }catch(e) {}
                            try {
                                $scope.eventtimes[d].splice(key, 1);
                            } catch(e) {}

                        });
                    }

                }, handleError ) );
            }

            function deleteEventTimes($scope, $eid, $date_from, $date_to, nonce) {

                // validate
                var current_remove = [];

                if($date_from === 'all' && $date_to === 'all') {
                    current_remove = 'all';
                }
                else {
                    // Check type 1

                    if($scope.type === '1') {
                        var d1 = moment($date_from, 'YYYY-MM-DD');
                        var d2 = moment($date_to, 'YYYY-MM-DD');

                        var dta = d2.diff(d1, 'day');

                        for(var i = 0; i <= dta; i++) {
                            var iday = d1.clone();
                            iday.add(i, 'days');

                            var _sdate =  iday.format('YYYY-MM-DD');
                            if(typeof $scope.eventtimes[_sdate] !== 'undefined') {
                                current_remove.push(_sdate);
                            }

                        }

                        if(jQuery.isEmptyObject(current_remove)) {
                            return  jQuery.fn._calendar.mydefere;
                        }
                    }
                    else {
                        current_remove.push($date_from);
                        current_remove.push($date_to);
                    }

                }

                var _sdate_event = 'all';
                if(current_remove !== 'all') {
                    _sdate_event = current_remove.join(',');
                }
                var request = $http({
                    method: "get",
                    url: APL.ajax_url,
                    headers: aplHeaders,
                    params: {
                        action: "apollo_delete_event_times",
                        eid: $eid,
                        date_event: _sdate_event,
                        is_front: 1,
                        _aplAddEventNonceField:nonce
                    }
                });

                return( request.then( function(res) {

                    if(res.data.result === false) {
                        if (res.data.error_type == 'nonce') {
                            alert(res.data.message);
                        } else {
                            alert(list_msg.data('missing-error'));
                        }
                        return false;
                    }

                    if(typeof res.data.result.error !== 'undefined' && res.data.result.error === true) {
                        if(alert(res.data.result.msg)) {
                            if(res.result.require === 'refresh') {
                                window.location.href=window.location.redirect();
                                return;
                            }
                        }
                    }

                    if(current_remove === 'all') {
                        $scope.eventtimes = [];
                        return;
                    }

                    if($scope.type === '1'){
                        jQuery.each(current_remove, function(d, v) {
                            delete $scope.eventtimes[v];
                        });
                    }
                    else {
                        jQuery.each(res.data.result.data, function(d, v) {
                            delete $scope.eventtimes[v];
                        });
                    }

                }, handleError ) );
            }

            //TriLM Delete all event in calendar cell
            function deleteAllEventCell($scope,$eid, date_event) {
                var request = $http({
                    method: "get",
                    url: APL.ajax_url,
                    headers: aplHeaders,
                    params: {
                        action: "apollo_delete_all_event_in_cell",
                        eid: $eid,
                        date_event: date_event,
                        is_front: 1
                    }
                });
                return( request.then( function(res) {
                    delete $scope.eventtimes[date_event];
                    jQuery(window).unblock(jQuery.apl.blockUI);
                }, handleError ) );
            }
            //TriLM restore event time
            function restoreEventCell($scope,$eid, date_event){
                var request = $http({
                    method: "get",
                    url: APL.ajax_url,
                    headers: aplHeaders,
                    params: {
                        action: "apollo_restore_event_time_cell",
                        eid: $eid,
                        date_event: date_event
                    }
                });
                return( request.then( function(res) {

                }, handleError ) );
            }


            // ---
            // PRIVATE METHODS.
            // ---


            // I transform the error response, unwrapping the application dta from
            // the API response payload.
            function handleError( response ) {

                // The API response from the server should be returned in a
                // nomralized format. However, if the request was not handled by the
                // server (or what not handles properly - ex. server error), then we
                // may have to normalize it on our end, as best we can.
                if (
                    ! angular.isObject( response.data ) ||
                    ! response.data.message
                ) {

                    return( $q.reject(list_msg.data('unknow-error')) );

                }

                if (response.data.error_type == 'nonce') {
                    alert(response.data.message);
                }

                // Otherwise, use expected error message.
                return( $q.reject( response.data.message ) );

            }


            // I transform the successful response, unwrapping the application data
            // from the API response payload.
            function handleSuccess( response ) {

                return( response.data );

            }

        }
    );


    app.directive('resize', function($window) {
        return {
            link: function(scope) {
                angular.element($window).on('resize', function(e) {
                    // Namespacing events with name of directive + event to avoid collisions
                    scope.$broadcast('resize::resize');
                });
            }
        }
    });


    app.directive("calendar", ['$compile', 'eventTimeService', function($compile, eventTimeService) {

        return {
            restrict: "E",

            scope: {
                selected: "=",
                eid: "@eid",
                sd: "@sd",
                ed: "@ed",
                dws: "@dws"
            },
            link: function(scope, elem, attrs) {
                if(attrs.type === '2') {
                    var days_of_week = scope.dws.split(',');
                    var arrEnableDay = [];
                    var dow = ['SUN','MON','TUE','WED','THU','FRI','SAT'];
                    elem.replaceWith($compile(calendar_2)(scope));
                    scope.type = '2';
                    scope.specialc = [];
                    scope.specialc.days = { 'SUN' : list_msg.data('sun'),
                                            'MON' : list_msg.data('mon'),
                                            'TUE' : list_msg.data('tue'),
                                            'WED' : list_msg.data('wed'),
                                            'THU' : list_msg.data('thu'),
                                            'FRI' : list_msg.data('fri'),
                                            'SAT' : list_msg.data('sat') };
                    scope.specialc.eventtimes = [];
                    jQuery.each(days_of_week, function( index, value ) {
                        arrEnableDay[index] = dow[value];
                    });
                    scope.enableDays =  arrEnableDay;
                }
                else {
                    elem.replaceWith($compile(calendar_t)(scope));
                    scope.type = '1';
                }

                scope.eventtimes = [];

                scope.sdm = moment(scope.sd, "YYYY-MM-DD");
                scope.sdm_month = scope.sdm.format('MMMM');
                scope.edm = moment(scope.ed, "YYYY-MM-DD");
                scope.selected = scope.sdm;
                scope.month = scope.selected.clone();
                scope.checkExistEvent = function (eventtimes, v2) {
                    var isexist = false;
                    jQuery.each(eventtimes , function(_, v) {
                        if(v.time_from == v2.time_from && v.time_to == v2.time_to) {
                            isexist = true;
                            return false;
                        }
                    });

                    return isexist;
                };

                scope.moreThanThree = function(eventtimes) {
                    return typeof eventtimes !== "undefined" && (eventtimes.length > 3);
                };

                scope.orderTime = function(eventtime) {

                    return eventtime.time_from;
                };

                scope.sameYear = function() {
                    return scope.sdm.format("YYYY") === scope.edm.format("YYYY");
                };

                scope.checkUnnormalEventTime = function(date_from, date_to, time_from, time_to) {

                    var d1 = moment(date_from, 'YYYY-MM-DD');
                    var d2 = moment(date_to, 'YYYY-MM-DD');

                    var dta = d2.diff(d1, 'day');

                    var et = '';
                    var arr_error = [];
                    for(var i = 0; i <= dta; i++) {
                        var iday = d1.clone();
                        iday.add(i, 'days');

                        var _sdate =  iday.format('YYYY-MM-DD');

                        if(typeof scope.eventtimes[_sdate] === 'undefined')
                            continue;

                        et = scope.eventtimes[_sdate];

                        jQuery.each(et, function(_, v) {

                            if(v.time_from >= time_from && v.time_to <= time_to ) {
                                arr_error.push(_sdate);
                                return false;
                            }
                        });

                    }


                    return arr_error;
                };

                scope.haveInValid = function(_sdate) {

                    if(typeof scope.eventtimes !== 'undefined' && typeof scope.eventtimes[_sdate] === 'undefined')
                        return false;
                    var et = scope.eventtimes[_sdate];

                    if(et.length === 1) return false;

                    var isVaid = false;
                    for(var i =0 ;i < et.length; i++) {
                        for(var j = i+1; j < et.length; j++) {

                            if(et[i].time_from >= et[j].time_from && et[i].time_to <= et[j].time_to || et[i].time_from <= et[j].time_from && et[i].time_to >= et[j].time_to) {
                                return true;
                            }
                        }
                    }

                    return isVaid;
                };

                scope.mbUpdateSpecial = function() {

                    if(scope.type === '2') {
                        var d1 = scope.sdm.clone().add('1', 'w');
                        for(var i = 0; i < 7; i++) {
                            var n = d1.clone().add(i, 'days')
                                , w = n.format('ddd').toUpperCase()
                                , sn = n.format('YYYY-MM-DD')
                                ;
                            scope.specialc.eventtimes[w] = scope.eventtimes[sn];

                        }
                    }
                };

                eventTimeService.getAllEventTime(scope, scope.eid).then(function() {renderT(scope, jQuery)}).then(function() {
                    scope.mbUpdateSpecial();
                });


                scope.clone = function (template) {
                    var linkFn = $compile(template);
                    return linkFn(scope);
                };
                
                scope.startTimeValidator = function() {
                    console.log(this.eventtimes, this.month)
                };
            }
        };


        function getPrevClosestSundayInWeek(md) {
            var _md = md.clone();
            var _n = _md.format("dd").substring(0, 2).toUpperCase();
            if(_n === 'SU') {
                return md;
            }

            return getPrevClosestSundayInWeek(_md.add(-1, 'day'));
        }

        function getNextClosestSaturdayInWeek(md) {
            var _md = md.clone();
            var _n = _md.format("dd").substring(0, 2).toUpperCase();
            if(_n === 'SA') {
                return md;
            }

            return getNextClosestSaturdayInWeek(_md.add(1, 'day'));
        }

        function renderT(scope, $) {

            var start = getPrevClosestSundayInWeek(scope.sdm);
            var end = getNextClosestSaturdayInWeek(scope.edm);

            _buildMonth(scope, start, end);

            scope.select = clickListener;
            scope.disableDayInCa2 = disableListener;

            scope.viewmore = function(events, $e) {

                var $calendar_container = jQuery('.calendar-container');
                var $body = $('body');
                var $this = jQuery($e.target);
                var _pos = jQuery.fn._calendar.findPos($this[0], 'calendar-container');
                var _cur_top = _pos.top + jQuery('.calendar-container').position().top;
                var _cur_left = _pos.left + jQuery('.calendar-container').position().left;

                $body.append(_lB);

                var $vmb = $('.viewmore-box');
                $vmb.css('display','block');
                $vmb.fadeIn(1000, 'linear');
                $vmb.css('top',_cur_top + $this.height() + 9);
                $vmb.css('left',_cur_left - ($this.width()/2 ));

                if(typeof scope.current_edit_date !== 'undefined') {
                    // move back
                    jQuery('[data-date="' + scope.current_edit_date + '"]').find('.contain-vm-small').append(scope._eventList);
                }

                scope._eventList = $this.parents('.date-cell').find('.contain-vm-small .cl-event-list');

                scope.current_edit_date = $this.parents(".date-cell").data('date');
                $('.viewmore-box .cl-event-list').remove();
                $vmb.append(scope._eventList);
            };

            scope.isnulltt = function(time_to) {
                return time_to === "";
            };

            scope.edittime = function(edittime, index, e) {
                var $body = jQuery('body');

                var isEditFromViewMore = false;
                if(jQuery('.light-box').length > 0) {
                    isEditFromViewMore = true;
                }
                else {
                    $body.append(_lB);
                }

                var _cl_timer_box = get_calendar_timer(true);

                var _ang_timer_box = $compile(_cl_timer_box)(scope);
                $body.append(_ang_timer_box);

                var $box = jQuery('.box');
                $box.css('display','block');
                $box.fadeIn(1000, 'linear');


                var $this = $(e.target);

                var $date_cell = $this.parents('.date-cell');
                $date_cell.addClass('add-event');

                var _pos = $this.offset();
                var _cur_top = _pos.top;
                var _cur_left = _pos.left;

                if($(window).width()>480){
                    $box.css('top',_cur_top-30);
                    if(($(window).width()- (_cur_left+30))>290){
                        $box.css('left',_cur_left+30);
                    }else{
                        $box.css('left',_cur_left);
                    }
                }else{
                    $box.css('top',_cur_top-30);
                    $box.css('left',($(window).width()/2)-140);
                }

                // hide view more
                if(isEditFromViewMore)  {
                    $('.viewmore-box').css('display','none');
                }

                var arrt1 = $.apl.converTimeStr(edittime.time_from);

                var arrt2 = ['--', '--', '--'];

                if(edittime.time_to !== '') {
                    arrt2 = $.apl.converTimeStr(edittime.time_to);
                }

                $("#box_time_calendar ._hour1").val(arrt1[0]);
                $("#box_time_calendar ._minute1").val(arrt1[1]);
                $("#box_time_calendar ._ampm1").val(arrt1[2]);
                $("#box_time_calendar ._hour2").val(arrt2[0]);
                $("#box_time_calendar ._minute2").val(arrt2[1]);
                $("#box_time_calendar ._ampm2").val(arrt2[2]);

                var $boxtimer = $("#box_time_calendar");

                $boxtimer.data('key-time', index);
                $boxtimer.data('otime_from', edittime.time_from);
                $boxtimer.data('otime_to', edittime.time_to);
            };

            scope.next = function() {

                if(jQuery("#_apl_ca_next").hasClass('disable')) {
                    return;
                }

                var next = scope.month.clone();
                _removeTime(next.month(next.month()+1).date(1));

                scope.month.month(scope.month.month()+1);
                _buildMonth(scope, next, scope.month);

                jQuery.fn._calendar.sstart = " ";
                jQuery.fn._calendar.send = " ";

            };

            scope.previous = function() {

                if(jQuery("#_apl_ca_prev").hasClass('disable')) {
                    return;
                }

                var previous = scope.month.clone();
                _removeTime(previous.month(previous.month()-1).date(1));


                scope.month.month(scope.month.month()-1);
                _buildMonth(scope, previous, scope.month);
            };

            //TriLM customize delete all event in calendar cell
            scope.deleteSelect = function(day,$event){
                var date = day.date_value;

                $.apl.confirm(list_msg.data('remove-date'),
                    function() {
                        jQuery(window).block(jQuery.apl.blockUI);

                        //delete all event here
                        eventTimeService.deleteAllEventCell(scope,scope.eid,date);
                        $('div[data-date="'+date+'"]').addClass('disable');
                        $('div[data-date="'+date+'"]').addClass('inactive');
                        scope.mbUpdateSpecial();
                    },
                    function() {

                    }
                );

            };
            //TriLM undo delete
            scope.undoDeleteSelect = function(day,$event){
                var date = day.date_value;

                //$.apl.confirm(jQuery.apl.__("Are you sure? This action will delete all times in this date."),
                // function() {
                var $body = jQuery('body');
                var _blockUI = {
                    message: APL.resource.loading.message,
                    css: jQuery.parseJSON(APL.resource.loading.image_style)
                };
                $(window).block(_blockUI);
                //undo delete all event here
                eventTimeService.restoreEventCell(scope,scope.eid,date);
                $('div[data-date="'+date+'"]').removeClass('disable');
                $('div[data-date="'+date+'"]').removeClass('inactive');
                $body.unblock();
                //},
                //function() {
                //
                //}
                //);

            };

            (function($) {
                var _blockUI = {
                    message: APL.resource.loading.message,
                    css: $.parseJSON(APL.resource.loading.image_style)
                };

                var $body = $('body');

                $.apl.changeTime = function(hour, minute, ampm) {
                    hour = parseInt(hour, 10);
                    minute = parseInt(minute, 10);
                    ampm  = ampm.toUpperCase();

                    if(ampm === 'AM'){
                        if(hour === 12) {
                            return '00:'+ ("0" + minute).slice(-2);
                        }
                        else {
                            return ("0" + hour).slice(-2) + ':' +  ("0" + minute).slice(-2);
                        }
                    }

                    if(ampm === 'PM'){
                        if(hour === 12) {
                            return '12:'+ ("0" + minute).slice(-2);
                        }
                        else {
                            return ("0" + (hour  + 12)).slice(-2) + ':' +  ("0" + minute).slice(-2);
                        }
                    }

                };

                $.apl.converTimeStr = function(strtime) {
                    if(strtime ==='') {
                        return ['--', '--', '--'];
                    }
                    var arr = strtime.split(':');
                    var hour = parseInt(arr[0], 10);
                    var minute = parseInt(arr[1], 10);

                    if(hour < 12) {
                        return hour ===0 ? [12, ("0" +minute).slice(-2), 'AM'] : [("0" + hour).slice(-2) , ("0" +minute).slice(-2), 'AM'];
                    }else {
                        return hour === 12 ? [12, ("0" +minute).slice(-2), 'PM'] : [("0" + (hour - 12 )).slice(-2), ("0" +minute).slice(-2), 'PM'];
                    }
                };

                /* put other event to change the scope */
                var isValidTime = function(time_from, time_to) {
                    var arr1 = time_from.split(':');
                    var arr2 = time_to.split(':');

                    var t1 = $.apl.changeTime(arr1[0],arr1[1], arr1[2]);
                    var t2 = $.apl.changeTime(arr2[0],arr2[1], arr2[2]);

                    return t2 > t1;

                };

                // @todo workhere
                $("#calendar_choice_all").on('click', function() {

                    var hour1 = $("#add-event-calendar ._hour1").val()
                        , minute1 =   $("#add-event-calendar ._minute1").val()
                        , ampm1 = $("#add-event-calendar ._ampm1").val()

                        , hour2 = $("#add-event-calendar ._hour2").val()
                        , minute2 =   $("#add-event-calendar ._minute2").val()
                        , ampm2 = $("#add-event-calendar ._ampm2").val()
                        ;



                    var time_to = '';

                    var time_from = $.apl.changeTime(hour1, minute1, ampm1);

                    var nonce = $("#_aplAddEventNonceField").val();

                    if(hour2 !== '--' && minute2 !== '--' && ampm2 !== '--') {
                        time_to = $.apl.changeTime(hour2, minute2, ampm2);
                    }
                    else if(hour2 === '--' && minute2 === '--' && ampm2 === '--') {
                        time_to = '';
                    }
                    else if([hour2, minute2, ampm2].indexOf('--') > -1) {
                        alert(list_msg.data('invalid-time'));
                        return false;
                    }

                    if(time_to <= time_from && (time_to !== '')) {
                        alert(list_msg.data('end-larger-start'));
                        return false;
                    }

                    // CHECK ALL PREVENT UNWANTED EVENT
                    //                if(scope.type === '1') {
                    //                    var cal_error = scope.checkUnnormalEventTime(scope.sd, scope.ed, time_from, time_to);
                    //                    if(!$.isEmptyObject(cal_error)) {
                    //                        var $calendar_err = $("#calendar_err");
                    //
                    //                        $.each(cal_error, function(_, v) {
                    //                            $calendar_err.append('<li> Warning:  Date ' + v + ' have event is invalid </li>')
                    //                        });
                    //                    }
                    //                }

                    $_select_date = $(".date-cell.shift-selected");

                    var _dta = [];
                    if($_select_date.length === 0){
                        _dta[0] = scope.sd;
                        _dta[1] = scope.ed;
                    }
                    else {
                        _dta[0] = $_select_date.first().data('date');
                        _dta[1] = $_select_date.last().data('date');
                    }

                    $(window).block(_blockUI);
                    eventTimeService.createEventTimes(scope, scope.eid, _dta[0], _dta[1], time_from, time_to, nonce).then(function() {
                        scope.mbUpdateSpecial();
                        $body.unblock();
                    });

                });


                $('#calendar_clear_all').on('click', function(e) {
                    var $_select_date = $(".date-cell.shift-selected");

                    var _dta = [];
                    if($_select_date.length === 0){
                        _dta[0] = 'all';
                        _dta[1] = 'all';
                    }
                    else {
                        _dta[0] = $_select_date.first().data('date');
                        _dta[1] = $_select_date.last().data('date');

                    }
                    var nonce = $("#_aplAddEventNonceField").val();

                    if(_dta[0] === 'all' && _dta[1] === 'all') {
                        if(jQuery.isEmptyObject(scope.eventtimes)) {
                            return  jQuery.fn._calendar.mydefere;
                        }

                        $.apl.confirm(list_msg.data('remove-date-events'),
                            function() {

                                $(window).block(_blockUI);
                                eventTimeService.deleteEventTimes(scope, scope.eid, _dta[0], _dta[1], nonce).then(function() {
                                    scope.mbUpdateSpecial();
                                    $body.unblock();
                                });

                            },
                            function() {
                                return jQuery.fn._calendar.mydefere;
                            }
                        );
                    }
                    else {
                        $(window).block(_blockUI);
                        eventTimeService.deleteEventTimes(scope, scope.eid, _dta[0], _dta[1], nonce).then(function() {
                            scope.mbUpdateSpecial();
                            $body.unblock();
                        });
                    }
                });

                // CANCEL
                $(window).on('click', function(e) {

                    var $slt = $(".calendar-container .date-cell.shift-selected");
                    $slt.length && $slt.removeClass('shift-selected');
                    jQuery.fn._calendar.sstart = " ";
                    jQuery.fn._calendar.send = " ";

                });

                $body.on('click', '#add-event-calendar', function(e) {
                    if (!$(e.target).hasClass('wp-switch-editor')) {
                        e.stopPropagation();
                    }
                });

                $body.on('change', '#box_time_calendar ._hour2', function(e) {
                    var $minute2 = $("#box_time_calendar ._minute2");
                    var $ampm2 = $("#box_time_calendar ._ampm2");
                    if($(this).val() === '--') {
                        //                    $("#box_time_calendar ._hour2").val('--');
                        $minute2.val('--');
                        $ampm2.val('--');
                        return;
                    }

                    if($minute2.val() === '--'){
                        $minute2.val('00');
                    }

                    if( $ampm2.val() === '--' ) {
                        $ampm2.val('PM');
                    }
                });

                $body.on('change', '#box_time_calendar ._minute2', function(e) {
                    if($(this).val() === '--') {
                        $("#box_time_calendar ._hour2").val('--');
                        //                    $("#box_time_calendar ._minute2").val('--');
                        $("#box_time_calendar ._ampm2").val('--');
                    }
                });
                $body.on('change', '#box_time_calendar ._ampm2', function(e) {
                    if($(this).val() === '--') {
                        $("#box_time_calendar ._hour2").val('--');
                        $("#box_time_calendar ._minute2").val('--');
                        //                    $("#box_time_calendar ._ampm2").val('--');
                    }
                });

                $body.on('change', '#add-event-calendar ._hour2', function(e) {
                    var $minute2 = $("#add-event-calendar ._minute2");
                    var $ampm2 = $("#add-event-calendar ._ampm2");

                    if($(this).val() === '--') {
                        //                    $("#box_time_calendar ._hour2").val('--');
                        $minute2.val('--');
                        $ampm2.val('--');
                        return;
                    }

                    if($minute2.val() === '--'){
                        $minute2.val('00');
                    }

                    if( $ampm2.val() === '--' ) {
                        $ampm2.val('PM');
                    }
                });

                $body.on('change', '#add-event-calendar ._minute2', function(e) {
                    if($(this).val() === '--') {
                        $("#add-event-calendar ._hour2").val('--');
                        //                    $("#box_time_calendar ._minute2").val('--');
                        $("#add-event-calendar ._ampm2").val('--');
                    }
                });
                $body.on('change', '#add-event-calendar ._ampm2', function(e) {
                    if($(this).val() === '--') {
                        $("#add-event-calendar ._hour2").val('--');
                        $("#add-event-calendar ._minute2").val('--');
                        //                    $("#box_time_calendar ._ampm2").val('--');
                    }
                });


                $(document).on('click', '.box', function(e) {
                    e.stopPropagation();
                });

                $('.clear-all').click(function(e){
                    e.preventDefault();
                    $('.date-cell').removeClass('shift-selected');
                    jQuery.fn._calendar.sstart = " ";
                    jQuery.fn._calendar.send = " ";
                });
                
                $body.on('click', '#next-to-step3', function(e){
                    var dates = $('.date-week .date-cell:not(.disable)'),
                        error = false;
                   
                    if (dates.length == 0) {
                        dates = $('.cl-date .date-cell:not(.disable)');
                    }    
                   
                    $.each(dates, function(i, v) {
                        if (!$(v).find('.cl-event-list li').length) {
                            $(v).addClass('cell-error');
                            error = true;
                        }
                    });
                    if (error) {
                        alert($(this).data().alert);
                    } else {
                        //window.location.href = $(this).data().href;
                    }
                });
                
                $body.on('click', '#box_time_calendar #add-new-event', function(e){

                    var $boxtime = $("#box_time_calendar");

                    var hour1 = $("#box_time_calendar ._hour1").val()
                        , minute1 =   $("#box_time_calendar ._minute1").val()
                        , ampm1 = $("#box_time_calendar ._ampm1").val()

                        , hour2 = $("#box_time_calendar ._hour2").val()
                        , minute2 =   $("#box_time_calendar ._minute2").val()
                        , ampm2 = $("#box_time_calendar ._ampm2").val()
                        ;

                    var time_from = $.apl.changeTime(hour1, minute1, ampm1);

                    var time_to = '';
                    if(hour2 !== '--' && minute2 !== '--' && ampm2 !== '--') {
                        time_to = $.apl.changeTime(hour2, minute2, ampm2)
                    }
                    else if(hour2 === '--' && minute2 === '--' && ampm2 === '--') {
                        time_to = '';
                    }
                    else if([hour2, minute2, ampm2].indexOf('--')) {
                        alert(list_msg.data('invalid-time'));
                        return false;
                    }

                    if(time_to <= time_from && (time_to !== '')) {
                        alert(list_msg.data('end-larger-start'));
                        return false;
                    }

                    var $_select_date = $(".date-cell.add-event");

                    var date_event = $_select_date.data('date');

                    if(date_event === undefined) {
                        date_event = scope.current_edit_date;
                    }

                    $(window).block(_blockUI);

                    var key_time = $boxtime.data('key-time');
                    if(key_time === undefined) {
                        eventTimeService.createEventTime(scope, scope.eid, date_event, time_from, time_to).then(function() {

                            $boxtime.remove();
                            $('.light-box').remove();
                            $('.date-cell').removeClass('add-event');

                            scope.mbUpdateSpecial();
                            $body.unblock();

                        });
                    }
                    else {

                        var $otime_from = $boxtime.data('otime_from');
                        var $otime_to = $boxtime.data('otime_to');

                        eventTimeService.editEventTime(scope, date_event, $otime_from, $otime_to, time_from, time_to).then(function() {

                            $boxtime.remove();
                            $('.light-box').remove();
                            $('.date-cell').removeClass('add-event');
                            $('.viewmore-box').css('display','none');

                            scope.mbUpdateSpecial();
                            $body.unblock();

                        });
                    }

                });

                $body.on('click', '#box_time_calendar #delete-event', function(e){

                    var $boxtime = $("#box_time_calendar");

                    var $_select_date = $(".date-cell.add-event");

                    var date_event = $_select_date.data('date');

                    if(date_event === undefined) { // edit in viewmore
                        date_event = scope.current_edit_date;
                    }

                    $(window).block(_blockUI);

                    var $otime_from = $boxtime.data('otime_from');
                    var $otime_to = $boxtime.data('otime_to');

                    eventTimeService.deleteEventTime(scope, date_event, $otime_from, $otime_to).then(function() {

                        $boxtime.remove();
                        $('.light-box').remove();
                        $('.date-cell').removeClass('add-event');
                        $('.viewmore-box').css('display','none');

                        scope.mbUpdateSpecial();
                        $body.unblock();
                    });
                });

                $body.on('click', '.closepopup', function(e){
                    var $parent = $(this).parent();
                    $parent.remove();
                    $('.light-box').remove();
                    $('.date-cell').removeClass('add-event');
                });

                $body.on('click', '.closeviewmore', function(){
                    $('.viewmore-box').css('display','none');
                    $('.light-box').remove();
                    $('.date-cell').removeClass('add-event');
                });

                if( !_.isEmpty(tinyMCE) && !_.isEmpty(tinyMCE.editors)){
                    tinyMCE.editors[0].on('change', function() {
                        var val = tinyMCE.editors[0].getContent();
                        console.log(val);
                        eventTimeService.updateAdditionalTime(scope.eid,val);
                    });
                }


                ///*TriLM add delete event cell*/
                //$body.on('click', '.delete-calendar-cell', function(){
                //    console.log()
                //});
                ///*end delete event cell*/

                $(window).on('resize', function(e) {
                    if(typeof scope.current_edit_date !== 'undefined') {
                        // move back
                        jQuery('[data-date="' + scope.current_edit_date + '"]').find('.contain-vm-small').append(scope._eventList);
                    }
                });
            })(jQuery);
        }

        function _removeTime(date) {
            return date.day(0).hour(0).minute(0).second(0).millisecond(0);
        }

        function _buildMonth(scope, start, end) {
            scope.weeks = [];
            var done = false, date = start.clone(), monthIndex = date.month();
            while (!done) {
                scope.weeks.push({ days: _buildWeek(date.clone(), monthIndex, scope) });
                date.add(1, "w");
                monthIndex = date.month();

                done = date.diff(end) >= 0;
            }
        }


        function _buildWeek(date, month, scope) {
            var days = [];
            var days_of_week = scope.dws.split(',');
            for (var i = 0; i < 7; i++) {
                var number = date.date();

                if(date.date() === 1 || date.diff(scope.sdm) === 0) {
                    number = date.format('MMM') + ' ' + number;
                }

                //            else if(date.diff(scope.edm) === 0) {
                //                number = number + " / " + (date.month() + 1);
                //            }

                days.push({
                    name: date.format("dd").substring(0, 2).toUpperCase(),
                    number: number,
                    isCurrentMonth: date.month() === month,
                    isToday: date.isSame(new Date(), "day"),
                    date: date,
                    outrange: (date.isBefore(scope.sdm) || date.isAfter(scope.edm)) || days_of_week.indexOf(date.day().toString()) == -1,
                    date_value: date.format("YYYY-MM-DD")
                });
                date = date.clone();
                date.add(1, "d");
            }
            return days;
        }



        function clickListener(day, e) {
            (function($) {
                var $this = $(e.target), $body = $('body');
                var _eleTarget = e.target.className;

                if(_eleTarget.indexOf('date-cell') === -1 && (_eleTarget.indexOf("num") > -1 || _eleTarget.indexOf("sun") > -1 || _eleTarget.indexOf("dName sun") > -1 || _eleTarget.indexOf("dName") > -1 || _eleTarget.indexOf("day")> -1 || _eleTarget.indexOf("event")> -1)){
                    $this = $this.parents('.date-cell');
                    _eleTarget = $this.attr('class');
                }

                if($this.hasClass('disable') || $this.hasClass('shift-selected')) return;

                if(!e.shiftKey){

                    _selectedDay = $(e.target);

                    var _cur_left = $this.offset().left;
                    var _cur_top = $this.offset().top;


                    if(_eleTarget.indexOf("date-cell") > -1 || _eleTarget.indexOf("num") > -1 || _eleTarget.indexOf("sun") > -1 || _eleTarget.indexOf("dName sun") > -1 || _eleTarget.indexOf("dName") > -1){
                        $this.addClass('add-event');
                        $body.append(_lB);

                        $body.append(get_calendar_timer(false));

                        var $box = $('.box');
                        $box.css('display','block');
                        $box.fadeIn(1000, 'linear');
                        if($(window).width()>480){
                            $box.css('top',_cur_top-30);
                            if(($(window).width()- (_cur_left+30))>290){
                                $box.css('left',_cur_left+30);
                            }else{
                                $box.css('left',_cur_left);
                            }
                        }else{
                            $box.css('top',_cur_top-30);
                            $box.css('left',($(window).width()/2)-140);
                        }

                    }
                }else if(e.shiftKey){

                    var _start = jQuery.fn._calendar.sstart;
                    var _end = jQuery.fn._calendar.send;

                    if(_eleTarget.indexOf('viewmore') !== -1 || _eleTarget.indexOf('date-cell') === -1 ){
                        $this = $this.parents('.date-cell');
                    }

                    var week_index = $this.parent('.date-week').index();

                    var shift_press = $this.index() + week_index * 7;
                    if(jQuery.fn._calendar.sstart === " "){
                        _start = shift_press + 1;
                    }else{
                        _end = shift_press + 1;
                    }

                    $this.addClass('shift-selected');

                    if((_start !==" ") && (_end !==" ")){

                        if((_end - _start) > 0){
                            for(var i = _start ; i < _end; i ++ ){
                                $('.date-cell:eq('+i+')').addClass('shift-selected');
                            }

                        }else if(_end !== " "){
                            for(var j = _start ; j > _end; j--){
                                $('.date-cell:eq('+(j-1)+')').addClass('shift-selected');
                            }
                        }
                    }

                    jQuery.fn._calendar.sstart = _start;
                    jQuery.fn._calendar.send = _end;
                }
            })(jQuery);
        }

        function disableListener(day,enableDays){
            if(jQuery.inArray( day, enableDays ) != -1){
                return false;
            }else{
                return true;
            }
        }
    }]);
}

