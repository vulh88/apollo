(function($) {

    $(function() {

        $(document).ready(function () {
            $('#apl-fe-save-draft').off('click').on('click', function (e) {
                e.preventDefault();
                var $form = $('#admin-frm-step1');
                $form.append('<input type="hidden" name="apl-fe-save-draft" value="1"/>');
                $form.validationEngine('detach');
                $form.submit();
            });

            $('#apl-event-save-date-draft').off('click').on('click', function (e) {
                e.preventDefault();
                var $form = $('#admin-frm-step1');
                $form.append('<input type="hidden" name="apl-fe-save-draft" value="1"/>');
                var caseAction = $form.find('#case_action');
                if (caseAction.length > 0) {
                    caseAction.val('submit_event_dates');
                }
                $form.validationEngine('detach');
                $form.submit();
            });

            /** Preventing any parent handlers from being executed */
            $('body.home .apl-link-detail-page').off('click', '.add-it-btn').on('click', '.add-it-btn', function(e) {
                e.stopPropagation();
            });

            /** @Ticket #16482 Apply logic neighborhood to FE event search widget */
            $('#search-event').off('change', '#event-city-select').on('change', '#event-city-select', function (e) {
                eventNeighborhood(e);
            });
            $('#search-event-m-t').off('change', '#event-city-select').on('change', '#event-city-select', function (e) {
                eventNeighborhood(e);
            });
        });

        var eventNeighborhood = function(e){
            var current = $(e.currentTarget);
            var loading = current.closest('form');
            var neighborhood = current.closest('form').find('#event-neighborhood-select');
            var neighborhoodSelected = '';
            if (neighborhood.length > 0) {
                neighborhoodSelected = neighborhood.data('selected');
            }
            if (current.val() == '' || current.val() == 0 || typeof current.val() === 'undefined' || current.val() == null) {
                neighborhood.val('');
                neighborhood.html('');
                neighborhood.closest('.el-blk').addClass('hidden');
            } else {
                var currentItem = current.find('option[value="'+current.val()+'"]');
                if (currentItem.length > 0) {
                    $.ajax({
                        url: APL.ajax_url,
                        method: 'get',
                        dataType: 'json',
                        data:{
                            action : 'apollo_render_neighborhood_for_event_search_widget',
                            state: currentItem.data('state'),
                            city: current.val(),
                            neighborhood: neighborhoodSelected
                        },

                        beforeSend: function () {
                            loading.block($.apl.blockUI);
                        },

                        success : function (res) {
                            neighborhood = $(neighborhood);
                            if (res.data != '') {
                                neighborhood.closest('.el-blk').removeClass('hidden');
                                neighborhood.html(res.data);
                            } else {
                                neighborhood.closest('.el-blk').addClass('hidden');
                                neighborhood.html('');
                                neighborhood.val('');
                            }
                            $(loading).unblock();
                        }
                    });
                }
            }
        };

        $(window).on('load', function() {
            var $form = $('[data-ride="log"]');
            if ($form.length) {
                $.ajax({
                    url: APL.ajax_url,
                    type: "POST",
                    data: $form.serialize(),

                    success : function (res) {
                        $('body').unblock();
                        if(!_.isEmpty(res) && res.success === "TRUE"){
                            callback();
                        } else {
                            alert(res.error);
                        }
                    }
                });
            }
            /** @Ticket #14884 */
            $('.apl-others-event-load-more a[data-ride="ap-more"]').trigger('click');
            $(document).ajaxSuccess(function(event, xhr, settings) {
                if (settings.url.indexOf('apollo_category_show_more_other_event') >= 0 ){
                    var loadOthersEvent = $('.apl-others-event-load-more');
                    if (typeof xhr.responseJSON.have_more !== 'undefined' && !xhr.responseJSON.have_more) {
                        if (!loadOthersEvent.hasClass('hidden')) {
                            loadOthersEvent.addClass('hidden');
                        }
                    } else {
                        if (loadOthersEvent.hasClass('hidden')) {
                            loadOthersEvent.removeClass('hidden');
                        }
                    }
                }
            });


            $('.wrap-load-more-artist-for-event #event-artist-load-more').trigger('click');
        })



        /**
         * Check user add an object to his/her profile
         * */
        /**
         * Check user add an object to his/her profile
         * */

        $(window).on('load', function() {
            eventLists = $('.apl-check-user-added');
            var ids = [];


            eventLists.each(function (i, v) {
                var d = $(v).data();
                ids.push(d.objectId);


                $(v).children('span').text(d.addText);
                $(v).removeClass('bookmark_highlight');
                $(v).attr('data-addit', 1);

            })

            if (!ids.length) {
                return false;
            }


            $.ajax({
                url: APL.ajax_url,
                type: "POST",
                data:{
                    action : 'apollo_check_user_bookmark_object',
                    ids: ids.join(','),
                    module: 'event',
                    post_type: 'event'
                },

                success : function (res) {
                    if (res.status == false) {
                        return;
                    }

                    eventLists.each(function (i, v) {
                        var $v = $(v);
                        var d = $v.data();
                        var id = d.objectId.toString();
                        if (res.result && res.result.length && res.result.indexOf(id) != -1) {
                            $v.children('span').text(d.addedText);
                            $v.addClass('bookmark_highlight');
                            $v.attr('data-addit', 0);
                        }
                        else {
                            $v.children('span').text(d.addText);
                            $v.removeClass('bookmark_highlight');
                            $v.attr('data-addit', 1);
                        }
                    })

                }
            });
        });

        /* @ticket #16609:  Merge the 'Add to Outlook' and 'Add to Google Calendar' icons into one popup window. */
        $('.show-calendar-popup').on('click',function(e){
            var $this = $(this);
            $( '#_popup_choose_event .modal-content' ).css( 'z-index', 20 );
            var _pid = $this.attr('target');
            var $_popup = $(_pid);
            var _height_popup = $_popup.outerHeight();

            $_popup.addClass($_popup.data('active')).css({
                'margin-top': -1*(_height_popup/2)
            });
        });

        $('#google-calendar-action-button').on('click',function(e){
            $('#export_ics_frm').attr('action','http://www.google.com/calendar/event');
            $('#export_ics_frm').attr('method','GET');
            $('#export_ics_frm').attr('target','_blank');
            $('#export_ics_frm').submit();
        });

        $('#outlook-action-button').on('click',function(e){
            $('#export_ics_frm').attr('action','/export_calendar/event/');
            $('#export_ics_frm').attr('method','POST');
            $('#export_ics_frm').submit();
        });

        /*@ticket: #17283 */
        $('#discount-quick-search').off('click').on('click', function (e) {
            $(this).toggleClass("active");

            var checked = $(this).hasClass('active');

            $('.top-search-row input[name="is_discount"]').prop('checked', checked);
        });

        /*@ticket: #17283 */
        $('#today-quick-search').off('click').on('click', function (e) {
            quickSearchButtonAction(e, 'today');
        });

        /*@ticket: #17283 */
        $('#weekend-quick-search').off('click').on('click', function (e) {
            quickSearchButtonAction(e, 'weekend');
        });

        /** @Ticket #19484 */
        $('.list-quicksearch').off('click', '#tomorrow-quick-search').on('click', '#tomorrow-quick-search', function(e){
            quickSearchButtonAction(e, 'tomorrow');
        });

        function quickSearchButtonAction(e, button) {
            var current = $(e.currentTarget);
            var parent = current.closest('.list-quicksearch');
            current.toggleClass('active');
            var searchForm = $('#search-event-m-t');
            var buttonTrigger = '.top-search-row #date-range-top a[data-attr="'+button+'"]';
            buttonTrigger = $(buttonTrigger);
            if (current.hasClass('active')) {
                parent.find('a.quick-search-date').removeClass('active');
                current.addClass('active');
                buttonTrigger.trigger('click');
            } else {
                buttonTrigger.removeClass('active');
                searchForm.find('input[name=start_date]').val('');
                searchForm.find('input[name=end_date]').val('');
            }
        }

        $('.add-to-calendar-button').off('click').on('click', function(e) {
            if($('.show-calendar-popup').length){
                $('.show-calendar-popup').click();
            }
            else{
                $('.share-calendar').click();
            }
        });

    });

}) (jQuery);