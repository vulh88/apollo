<?php

require __DIR__.'/apollo-command.php';

class Apollo_Statistic_Command extends Apollo_Command
{
    public function __construct()
    {
        parent::__construct();
    }

    public function statisticVisitAllBlog()
    {
        $arr_blogid = Apollo_Command_Util::getAllPublicBlog();

        foreach($arr_blogid as $objblog) {
            $blogId = intval($objblog->blog_id);

            switch_to_blog($objblog->blog_id);

            if (!Apollo_App::is_avaiable_module(Apollo_DB_Schema::_EVENT_PT)) {
                continue;
            }

            $arr    = Apollo_Statistic_System::exportTopVisitEventForDomain($blogId, 'json');
            /* show error for easy track */
            if($arr['error']) {
                echo $arr['msg']."\n";
            }

            $arrSolrSync = Apollo_Statistic_System::exportSolrEventDataForDomain($blogId);
            if ($arrSolrSync['error']) {
                echo $arrSolrSync['msg'] . PHP_EOL;
            }

            $this->clearSyndicationFiles();
        }

        switch_to_blog(1);
    }

    /**
     * Clear syndication files
     * @author @vulh
     * @return mixed
     */
    public function clearSyndicationFiles()
    {
        if (!Apollo_App::is_avaiable_module(Apollo_DB_Schema::_SYNDICATION_PT)) {
            return false;
        }

        require_once APOLLO_ADMIN_SYNDICATE_DIR. '/class-apollo-syndication-caching-handler.php';
        Apollo_Syndicate_Caching_Handler::clearCache();
    }
}

$statistic = new Apollo_Statistic_Command();

$action = isset($_GET['action']) ? $_GET['action'] : '';
$statistic->statisticVisitAllBlog();


