<?php

require_once APOLLO_TEMPLATES_DIR. '/pages/lib/class-apollo-page-module.php';

/**
 * Created by PhpStorm.
 * User: pc92-vu
 * Date: 20/04/2016
 * Time: 17:14
 */
class APL_Lib_Page_Author extends Apollo_Page_Module
{
    public function __construct() {

        if ( ! Apollo_App::is_avaiable_module('post') ) {
            wp_safe_redirect( '/' );
        }

        parent::__construct();
    }

    public function search() {
        global $wpdb;

        $offset = $this->page == 1 ? 0 : $this->getAjaxOnePageOffset();

        $sql = "SELECT SQL_CALC_FOUND_ROWS DISTINCT post_author, COUNT(p.ID) AS count
                FROM $wpdb->posts p
                INNER JOIN $wpdb->users u ON u.ID = p.post_author
                WHERE " . get_private_posts_cap_sql( 'post' ) . " GROUP BY post_author LIMIT $offset, $this->pagesize";
       
        $this->result = $wpdb->get_results($sql);

        $this->total =$wpdb->get_var("SELECT FOUND_ROWS()");
        $this->total_pages = ceil($this->total = $this->total / $this->pagesize);

        $this->template = APOLLO_TEMPLATES_DIR.'/author/listing-page/' . $this->_get_template_name();
    }
}