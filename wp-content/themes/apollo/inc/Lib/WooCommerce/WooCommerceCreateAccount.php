<?php

class APL_Lib_WooCommerce_Create_Account
{

    /**
     * Update meta data
     * @param $customer_id
     * @param $new_customer_data
     * @param $password_generated
     */
    public function wooCommerceCustomCreateAccount ($customer_id, $new_customer_data, $password_generated) {

        global $wpdb;
        if ($customer_id) {
            $blogUserTbl = Apollo_App::getBlogUserTable();
            $wpdb->insert($blogUserTbl, array(
                'blog_id' => intval($GLOBALS['blog_id']),
                'user_id' => $customer_id,
            ));

            /* add user blog */
            $blog_id = $GLOBALS['blog_id'];
            add_user_to_blog( $blog_id, $customer_id, 'subscriber' );
            update_user_meta( $customer_id, 'primary_blog', $blog_id );
            $details = get_blog_details($blog_id);
            update_user_meta($customer_id, 'source_domain', $details->domain);
        }
    }

    /**
     * Redirect to user/dashboard page
     */
    public function wooCommerceAutoLogin() {
        $linkDashboard = get_home_url($GLOBALS['blog_id']).'/user/dashboard';
        wp_send_json(array(
            'redirect' => $linkDashboard,
            'result' => __('success', 'apollo')
        ));
    }

}

