<?php

/**
 * Class ReportAbstract
 *
 * @ticket #11252
 */
abstract class ReportAbstract
{
    public $collectionName;
    public $collection;

    /**
     * Store MongoDB connection error message
     * @var string
     */
    public $connectionErrorMessage = '';

    /**
     * Set collection name
     *
     * @return string
     */
    abstract function setCollectionName();

    public function __construct()
    {
        $this->collectionName = $this->setCollectionName();
        $this->connectToDB();
    }

    /**
     * Connect to mongoDB, set the object database to collection
     *
     * @return mixed
     */
    public function connectToDB()
    {
        // set up connection
        $host       = defined('MONGODB_HOST') ? MONGODB_HOST : '127.0.0.1';
        $port       = defined('MONGODB_PORT') ? MONGODB_PORT : 27017;
        $connection = sprintf('mongodb://%s:%d', $host, $port);

        /**
         * MongoDB does not enable access control by default.
         * So, if you are not configure username and password, just leave it empty.
         * Ref: https://docs.mongodb.com/v3.4/core/authorization/#enable-access-control
         */
        $uriOptions = array();
        $username   = defined('MONGODB_USERNAME') ? MONGODB_USERNAME : '';
        $password   = defined('MONGODB_PASSWORD') ? MONGODB_PASSWORD : '';
        if ( !empty($username) && !empty($password) ) {
            $uriOptions = array(
                'username' => $username,
                'password' => $password,
            );
        }

        try {
            $blogInfo = get_blog_details();
            $domain   = str_replace(['.', '-'], '_', $blogInfo->domain);

            // db name pattern: apollo_mongo_report_{BlogID}_{BlogDomain}
            $dbName           = 'apollo_mongo_report_' . get_current_blog_id() . '_' . $domain;
            $this->collection = (new MongoDB\Client($connection, $uriOptions))->{$dbName}->{$this->collectionName};
        } catch (Exception $ex) {
            $this->connectionErrorMessage = $ex->getMessage();
        }
    }
}
