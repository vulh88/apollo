<?php

require_once APOLLO_INCLUDES_VERDOR. '/autoload.php'; // include Composer's autoloader
require_once APOLLO_INCLUDES_DIR. '/Lib/Report/Contracts/ReportInterface.php';
require_once APOLLO_INCLUDES_DIR. '/Lib/Report/Abstracts/ReportAbstract.php';

class APL_Lib_Report_Event extends ReportAbstract implements ReportInterface
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add new / Update a record
     *
     * @param array $data
     *
     * @return mixed
    */
    public function save($data)
    {
        // find event based on ID
        $event         = $this->collection->findOne(array('item_id' => $data['item_id']));
        $clickedButton = $this->getClickedButton($data['activity']);

        // update event clicks
        if ( $event ) {
            $this->collection->updateOne(
                array('item_id' => $data['item_id']),
                array('$set' => array(
                    'ip'           => $data['ip'],
                    'updated_date' => date('Y-m-d H:i:s'),
                    'org_id' => $data['org_id'],
                    'org_title' => $data['org_title'],
                    'title' => $data['title'],
                    $clickedButton => $event->{$clickedButton} + 1,
                ))
            );
        } else {
            // insert new event to tracking clicks
            $buttonClicks = array(
                'official_web_clicks'    => 0,
                'buy_ticket_clicks'      => 0,
                'discount_ticket_clicks' => 0,
            );
            $buttonClicks[$clickedButton] = 1;

            $this->collection->insertOne(array_merge(array(
                'org_id'       => $data['org_id'],
                'org_title'       => $data['org_title'],
                'ip'           => $data['ip'],
                'item_id'      => $data['item_id'],
                'title'        => $data['title'],
                'url'          => $data['url'],
                'created_date' => date('Y-m-d H:i:s'),
                'updated_date' => date('Y-m-d H:i:s'),
                'start_date'   => isset($data['start_date']) ? $data['start_date'] : '',
                'end_date'     => isset($data['end_date'])   ? $data['end_date']   : '',
            ), $buttonClicks));
        }
    }

    /**
     * Get click button key of MongoDB
     *
     * @param $activity
     *
     * @return string
     */
    private function getClickedButton($activity)
    {
        switch ( $activity )
        {
            case Apollo_Activity_System::CLICK_OFFICIAL_WEBSITE:
                return 'official_web_clicks';
            case Apollo_Activity_System::CLICK_BUY_TICKET:
                return 'buy_ticket_clicks';
            case Apollo_Activity_System::CLICK_DISCOUNT:
                return 'discount_ticket_clicks';
            default:
                return '';
        }
    }

    /**
     * Set collection name
     * @return string
     */
    public function setCollectionName()
    {
        return 'eventReport';
    }

    /**
     * @return string
     */
    public function getConnectErrorMessage()
    {
        return $this->connectionErrorMessage;
    }
}
