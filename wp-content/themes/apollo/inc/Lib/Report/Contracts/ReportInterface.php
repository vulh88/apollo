<?php


interface ReportInterface
{
    /**
     * Add new / Update a record
     * @param @array $data
     */
    public function save($data);
}