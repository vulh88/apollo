<?php

abstract class ApolloComponentAbstract {

    abstract function render($args);
}