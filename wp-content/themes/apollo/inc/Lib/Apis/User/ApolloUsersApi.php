<?php

apl_require_once(APOLLO_API_DIR. '/ApolloAPI.php');
apl_require_once(APOLLO_INCLUDES_DIR. '/Lib/Helpers/SyncUsers.php');
class APL_Lib_Apis_User_ApolloUsersApi extends Apollo_API
{
    private $token = false;

    public function __construct()
    {
        $this->allowMethod = array('get', 'post');
        parent::__construct();
        $this->action = !empty($this->request['action']) ? $this->request['action'] : '';
        add_filter( 'http_request_timeout', 'aplRequestTimeout' );

    }

    function aplRequestTimeout( )
    {
        // Default timeout is 5
        return 100;
    }

    public function filterRequest()
    {
        $headers = getallheaders();

        if (!empty($headers['Token'])) {
            $this->token = $headers['Token'];
        }

        if (!$this->allowMethod || !in_array(strtolower($this->method), $this->allowMethod)) {
            exit($this->_response(
                array(
                    'error' => sprintf("Bad Request -  Only accepts %s requests", implode(', ', $this->allowMethod)),
                    'code'  => 1009,
                ), 400));
        }

        if (!empty($headers['token'])) {
            $this->token = $headers['token'];
        }

        if (!isset($_GET['from-id'])) {

            if (! $this->token) {
                exit($this->_response(
                    array(
                        'error' => 'Bad Request - No API Key provided',
                        'code'  => 1003,
                    ), 400)
                );
            }
            else if (!$this->verify()) {
                exit($this->_response(
                    array(
                        'error' => 'Bad Request - Invalid API Key',
                        'code'  => 1004,
                    ), 400)
                );
            }
        }

    }

    protected function getUser() {
        $result = array();
        if ($this->method == 'GET' && isset($this->request['from-id'])) {
            $siteID = isset($this->request['blog-id']) ? $this->request['blog-id'] : '';
            $result = APL_Lib_Helpers_Sync_Users::getUserFromUserID($this->request['from-id'], $siteID);
        }

        return $this->response(array('users' => $result));
    }

    protected function addUser() {
        if (!empty($this->request['userInfo'])) {
            return $this->response(array('result' => APL_Lib_Helpers_Sync_Users::addUser($this->request['userInfo']) ));
        }

        return $this->response(array('result' => false), __('Empty data.'));
    }

    protected function verify() {

        if (! $this->token) return false;


        return APL_USER_API_TOKEN == $this->token;
    }
}