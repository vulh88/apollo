<?php

/**
 * Created by PhpStorm.
 * User: pc92-vu
 * Date: 14/06/2016
 * Time: 14:10
 */
abstract class Apollo_API
{
    /**
     * Property: method
     * The HTTP method this request was made in, either GET, POST, PUT or DELETE
     */
    protected $method = '';

    /**
     * Property: action
     */
    protected $action = '';

    /**
     * Property: args
     * Any additional URI components after the endpoint and verb have been removed, in our
     * case, an integer ID for the resource. eg: /<endpoint>/<verb>/<arg0>/<arg1>
     * or /<endpoint>/<arg0>
     */
    protected $args = Array();

    protected $apiClass = '';

    protected $output;

    protected $cNodeName = '';

    protected $pNodeName = '';

    protected $outputFields = array('xml', 'json');

    protected $allowMethod = array();

    protected $validAction = true;
    
    abstract public function filterRequest();
    /**
     * Constructor: __construct
     * Allow for CORS, assemble and pre-process the data
     */
    public function __construct($action = false) {

        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->action = $action;
        if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
            if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
                $this->method = 'DELETE';
            } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
                $this->method = 'PUT';
            } else {
                throw new Exception("Unexpected Header");
            }
        }

        switch($this->method) {
            case 'DELETE':
            case 'POST':
                $this->request = $this->_cleanInputs($_POST);
                if (empty($this->request)) {
                    $this->request = json_decode(file_get_contents('php://input'), true);
                }
                break;
            case 'GET':
                $this->request = $this->_cleanInputs($_GET);

                if ($eID = get_query_var('_apl_event_id')) {
                    $this->request['event_id'] = $this->_cleanInputs($eID);
                }

                break;
            case 'PUT':
                $this->request = $this->_cleanInputs($_GET);
                $this->file = file_get_contents("php://input");
                break;
            case 'OPTIONS':
                $this->request = $this->_cleanInputs($_REQUEST);
                $this->file = file_get_contents("php://input");
                break;
            default:
                $this->_response('Invalid Method', 405);
                break;
        }

        if (!empty($this->request['output'])) $this->output = $this->request['output'];
        if (!$this->outputFields || !in_array($this->output, $this->outputFields)) {
            $this->output = 'json';
        }

        header(sprintf("Content-Type: application/%s", $this->output));
        $this->filterRequest();
    }

    public function processAPI() {

        if (method_exists($this, $this->action)) {
            return $this->_response($this->{$this->action}($this->args));
        }
        return $this->_response("No Action: $this->action", 404);
    }

    protected function _response($data, $status = 200) {
        header("HTTP/1.1 " . $status . " " . $this->_requestStatus($status));

        if ($this->output == 'xml' && !empty($this->pNodeName) && $this->cNodeName) {
            return $this->xmlOutput($data, $this->pNodeName, $this->cNodeName);
        }

        return json_encode($data);
    }

    private function _cleanInputs($data) {
        $clean_input = Array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->_cleanInputs($v);
            }
        } else {
            $clean_input = trim(strip_tags($data));
        }
        return $clean_input;
    }

    private function _requestStatus($code) {
        $status = array(
            200 => 'OK',
            400 => 'Bad Request – Your request format is bad.',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error',
        );
        return ($status[$code]) ? $status[$code] : $status[500];
    }

    function xmlOutput($arrData, $pNode = 'events', $cNode = 'event' ) {
        //creating object of SimpleXMLElement
        apl_require_once(APOLLO_HELPER_DIR. '/XML.php');

        if (!isset($xmlUserInfo))
            $xmlUserInfo = false;

        APL_Lib_Helpers_XML::arrayToXml($arrData, $xmlUserInfo, $pNode, $cNode);
        return $xmlUserInfo->asXML();
    }

    public function response($_response, $msg = 'Success') {

        $basicResponse = $msg ? array(
            'msg'   => $msg,
        ) : array();

        if ($_response) {
            return array_merge($_response, $basicResponse);
        }

        return $basicResponse;
    }
}