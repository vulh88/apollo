<?php

/**
 * Created by PhpStorm.
 * User: pc92-vu
 * Date: 15/06/2016
 * Time: 18:51
 */
apl_require_once(APOLLO_API_DIR. '/Syndication/Syndication.php');

class APL_Lib_Apis_Syndication_Categories extends Syndication
{
    protected $limit = 10;
    protected $page = 1;
    protected $total = 0;

    public function __construct() {
        $this->sortFields = array('name');
        $this->params = array('sort', 'output');
        $this->pNodeName = 'categories';
        $this->cNodeName = 'category';
        $this->allowMethod = array('get', 'options');

        parent::__construct();
    }

    /**
     * Example of an Endpoint
     */
    protected function get() {


        if ($this->method == 'GET') {

            $types = get_post_meta($this->apId, 'meta-type');
            $types = $types[0];

            $args = array(
                'orderby' => $this->orderBy,
                'order' => $this->order,
                'hide_empty' => 0,
            );

            if ($types) {
                $args['include'] = $types;
            }

            $terms = get_terms('event-type', $args);

            // Auto add parent category to list event type if it was not selected
            if ($terms) {
                $childs = array();
                $parents = array();
                foreach ($terms as $term) {

                    if (!$term->parent) {
                        $parents[$term->term_id] = array(
                            'id' => $term->term_id, 'name'   => $term->name,
                            'active' => $this->_isActive($term->term_id, $types),
                            'childs'   => ''
                        );
                        continue;
                    }
                    else if (empty($parents[$term->parent])) {
                        $p = get_term($term->parent, 'event-type');
                        $parents[$p->term_id] = array(
                            'id' => $p->term_id,
                            'name'   => $p->name,
                            'active' => $this->_isActive($p->term_id, $types),
                            'childs'   => '');

                    }

                    if (!isset($childs[$term->parent])) {
                        $childs[$term->parent] = array(
                            array(
                                'id' => $term->term_id,
                                'name'=> $term->name,
                                'active' => $this->_isActive($term->term_id, $types)
                            )
                        );
                    } else {
                        $childs[$term->parent][] = array(
                            'id' => $term->term_id,
                            'name' => $term->name,
                            'active' =>  $this->_isActive($term->term_id, $types)
                        );
                    }
                }

                usort($parents, function($a, $b) {

                    if ($this->order == 'ASC')  {
                        return strcasecmp($a['name'] , $b['name']) > 0;
                    }
                    return strcasecmp($a['name'] , $b['name']) < 0;
                });

                foreach($parents as $k => $p) {
                    if (!empty($childs[$p['id']])) {
                        $parents[$k]['childs'] = $childs[$p['id']];
                    }
                }
            }

            return $this->response(array(
                'data'  => array_values($parents),
            ));
        }
    }

    private function _isActive($termId, $types) {
        return is_array($types) && in_array($termId, $types) ? 1 : 0;
    }
}