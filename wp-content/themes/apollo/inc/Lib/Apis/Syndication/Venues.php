<?php

/**
 * Created by PhpStorm.
 * User: pc92-vu
 * Date: 14/06/2016
 * Time: 14:13
 */

apl_require_once(APOLLO_API_DIR. '/Syndication/Syndication.php');

class APL_Lib_Apis_Syndication_Venues extends Syndication
{
    protected $limit = 10;
    protected $page = 1;
    protected $total = 0;


    public function __construct() {
        $this->sortFields = array('title');
        $this->pNodeName = 'venues';
        $this->cNodeName = 'venue';
        $this->params = array('page', 'limit', 'sort', 'output');
        $this->allowMethod = array('get', 'options');
        parent::__construct();
    }

    /**
     * Example of an Endpoint
     */
    protected function get() {

        if ($this->method == 'GET') {
            $page = isset($this->request['page']) ? $this->request['page'] : 1;
            $limit = isset($this->request['limit']) ? $this->request['limit'] : 10;
            $ids =  get_post_meta( $this->apId, 'meta-venues');

            if (!$this->orderBy) {
                $this->orderBy = 'title';
            }

            $args = array(
                'page' => $page,
                'limit' => $limit,
                'post_type' => 'venue',
                'order' => $this->order,
                'orderBy'   => $this->orderBy,
                'post_status'   => 'publish'
            );

            if ($ids && !empty($ids[0])) {
                $args['selected_item'] = $ids[0];
            }

            $venues = Apollo_App::getDDLItemsBySearchKeyword($args);

            $data = array();
            if ($venues['items']) {
                foreach($venues['items'] as $v) {
                    $venueAddress = maybe_unserialize(Apollo_App::apollo_get_meta_data($v->ID, Apollo_DB_Schema::_APL_VENUE_ADDRESS));
                    $venueMetadata = maybe_unserialize(Apollo_App::apollo_get_meta_data($v->ID, Apollo_DB_Schema::_APL_VENUE_DATA));
                    $data[] = array(
                        "ID" => $v->ID,
                        "post_title" => $v->post_title,
                        "post_excerpt" => $v->post_excerpt,
                        "post_content" => $v->post_content,
                        "post_name" => $v->post_name,
                        "address1" => isset($venueAddress[Apollo_DB_Schema::_VENUE_ADDRESS1]) ? $venueAddress[Apollo_DB_Schema::_VENUE_ADDRESS1] : '',
                        "address2" => isset($venueAddress[Apollo_DB_Schema::_VENUE_ADDRESS2]) ? $venueAddress[Apollo_DB_Schema::_VENUE_ADDRESS2] : '',
                        "city" => isset($venueAddress[Apollo_DB_Schema::_VENUE_CITY]) ? $venueAddress[Apollo_DB_Schema::_VENUE_CITY] : '',
                        "state" => isset($venueAddress[Apollo_DB_Schema::_VENUE_STATE]) ? $venueAddress[Apollo_DB_Schema::_VENUE_STATE] : '',
                        "zip" => isset($venueAddress[Apollo_DB_Schema::_VENUE_ZIP]) ? $venueAddress[Apollo_DB_Schema::_VENUE_ZIP] : '',
                        "phone" => isset($venueMetadata[Apollo_DB_Schema::_VENUE_PHONE]) ? $venueMetadata[Apollo_DB_Schema::_VENUE_PHONE] : '',
                        "email" => isset($venueMetadata[Apollo_DB_Schema::_VENUE_EMAIL]) ? $venueMetadata[Apollo_DB_Schema::_VENUE_EMAIL] : '',
                        "website_url" => isset($venueMetadata[Apollo_DB_Schema::_VENUE_WEBSITE_URL]) ? $venueMetadata[Apollo_DB_Schema::_VENUE_WEBSITE_URL] : '',
                    );
                }
            }

            return $this->response(array(
                'data'  => $data,
                'hasMore'  => $venues && $venues['haveMore'] ? 'true' : 'false',
                'count'	=> $venues ? $venues['total'] :  0
            ));
        }
    }

}