<?php

/**
 * Created by PhpStorm.
 * User: pc92-vu
 * Date: 14/06/2016
 * Time: 15:35
 */
apl_require_once(APOLLO_API_DIR. '/ApolloAPI.php');

class Syndication extends Apollo_API
{
    protected $token = false;
    protected $apId = false;
    protected $order = 'ASC';
    protected $orderBy = '';
    protected $sortFields = array();
    protected $params = array();

    public function __construct()
    {
        parent::__construct('get');
    }

    public function filterRequest() {
        $headers = getallheaders();

        if (!empty($headers['Token'])) {
            $this->token = $headers['Token'];
        }

        if (!$this->allowMethod || !in_array(strtolower($this->method), $this->allowMethod)) {
            exit($this->_response(
                array(
                    'error' => sprintf("Bad Request -  Only accepts %s requests", implode(', ', $this->allowMethod)),
                    'code'  => 1009,
                ), 400));
        }

        if (!empty($headers['token'])) {
            $this->token = $headers['token'];
        }

        if (!$this->token) {
            $this->token = isset($_GET['token']) ? $_GET['token']: '';
        }

        if (!$this->token) {
            $this->token = isset($_POST['token']) ? $_POST['token']: '';
        }

        $tokenData = explode('_', $this->token);
        $this->apId = isset($tokenData[1]) ? $tokenData[1] : '';

        if (isset($this->request['sort'])) {

            if (empty($this->sortFields)) {
                exit($this->_response(
                    array(
                        'error' => 'Bad Request - This API does not support sorting',
                        'code'  => 1002,
                    ), 400)
                );
            }

            $order = substr($this->request['sort'], 0, 1);
            if ($order == '-') {
                $this->order = 'DESC';
                $this->orderBy = substr($this->request['sort'], 1);
            } else {
                $this->orderBy = $this->request['sort'];
            }

            if (!in_array($this->orderBy, $this->sortFields)) {
                exit($this->_response(
                    array(
                        'error' => sprintf("Bad Request - This API does only support sorting by '%s'", implode(', ', $this->sortFields)),
                        'code'  => 1007,
                    ), 400)
                );
            }
        }

        if (isset($this->request['output']) && !in_array($this->request['output'], $this->outputFields)) {
            exit($this->_response(
                array(
                    'error' => sprintf("Bad Request - This API does only support one of output types %s", strtoupper(implode(', ', $this->outputFields))),
                    'code'  => 1008,
                ), 400)
            );
        }

        if (! $this->token) {
            exit($this->_response(
                array(
                    'error' => 'Bad Request - No API Key provided',
                    'code'  => 1003,
                ), 400)
            );
        }
        else if (!$this->verify()) {
            exit($this->_response(
                array(
                    'error' => 'Bad Request - Invalid API Key',
                    'code'  => 1004,
                ), 400)
            );
        }
    }

    protected function verify() {

        if (! $this->token) return false;

        $result = get_post_meta($this->apId, 'meta-token-key');

        if (is_array($result) && $result) {
            $result = $result[0];
        }

        return $result == $this->token;
    }
}