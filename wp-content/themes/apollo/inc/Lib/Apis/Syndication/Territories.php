<?php

apl_require_once(APOLLO_API_DIR. '/Syndication/Syndication.php');

class APL_Lib_Apis_Syndication_Territories extends Syndication
{

    public function __construct() {
        $this->sortFields = array();
        $this->pNodeName = 'territories';
        $this->cNodeName = 'territory';
        $this->params = array();
        $this->allowMethod = array('get', 'options');
        $this->outputFields = array('json');
        parent::__construct();
    }

    /**
     * Example of an Endpoint
     */
    protected function get() {

        if ($this->method == 'GET') {

            $cache = aplc_instance('APLC_Inc_Files_Territory', 'json');
            if ($data = $cache->get()) {
                return $data;
            } else {
                $result = $this->getTerritories();
                $data = $this->response(array(
                    'post_code' => "90210",
                    'country' => "United States",
                    'country_abbreviation' => "US",
                    'states'  => $result
                ), '');
                $cache->save($data);
                return $data;
            }
        }
    }

    private function getTerritories() {
        $terrData = of_get_option(Apollo_DB_Schema::_TERR_DATA);
        $result = array();
        $states = array();
        if ($terrData) {
            $territory = array();
            foreach ($terrData as $stateCode => $stateData) {
                $terr = array(
                    'name' => '',
                    'abbreviation' => $stateCode
                );
                $states[] = $stateCode;
                if ($stateData) {
                    foreach ($stateData as $cityName => $cityData) {
                        $city = array(
                            'name' => $cityName,
                            'zipcodes' => array()
                        );
                        if ($cityData) {
                            foreach ($cityData as $zip => $zipValue) {
                                $city['zipcodes'][] = $zip;
                            }
                        }
                        $terr['cities'][] = $city;
                    }
                    $territory[$stateCode] = $terr;
                }
            }
            $stateName = $this->getStateNameByCode($states);
            if ($stateName) {
                foreach ($stateName as $item) {
                    if (!empty($territory[$item->code])) {
                        $territory[$item->code]['name'] = $item->name;
                        $result[] = $territory[$item->code];
                    }
                }
            }
        }
        return $result;
    }
    private function getStateNameByCode( $stateCodes = array()) {
        global $wpdb;
        if (!empty($stateCodes)) {
            $strState = "'" . implode("','", $stateCodes) . "'";
            $privateStates = Apollo_App::get_network_manage_states_cities();
            $terrTable = ($privateStates ? $wpdb->prefix : $wpdb->base_prefix). Apollo_Tables::_APL_STATE_ZIP;
            $aplQuery = new Apl_Query($terrTable, true);
            return $aplQuery->get_where("type='state' and (parent is NULL OR parent = 0) AND code IN ({$strState})", 'code, name', ' ORDER BY name ');
        }
        return array();

    }

}