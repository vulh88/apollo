<?php

/**
 * Created by PhpStorm.
 * User: pc92-vu
 * Date: 17/06/2016
 * Time: 14:24
 */
apl_require_once(APOLLO_API_DIR. '/Syndication/Syndication.php');

class APL_Lib_Apis_Syndication_EventDatetimes extends Syndication
{
    public function __construct() {
        $this->params = array('eventId', 'page', 'limit', 'output');
        $this->pNodeName = 'datetimes';
        $this->cNodeName = 'datetime';
        $this->allowMethod = array('get', 'options');
        parent::__construct();
    }

    public function get() {

        if (empty($this->request['eventId'])) {
            exit($this->_response(
                array(
                    'error' => 'The eventId parameter is required',
                    'code'  => 1005,
                ), 400)
            );
        }

        apl_require_once(APOLLO_API_DIR. '/Inc/EventDateTime.php');
        $eventsObj = new APL_Lib_Apis_Inc_EventDateTime($this->request['eventId'], $this->request, $this->apId);

        if (! $eventsObj->validEvent()) {
            exit($this->_response(
                array(
                    'error' => 'Event is invalid',
                    'code'  => 1006,
                ), 400)
            );
        }

        if ($this->method == 'GET') {
            return $this->response($eventsObj->getData());
        }
    }
}