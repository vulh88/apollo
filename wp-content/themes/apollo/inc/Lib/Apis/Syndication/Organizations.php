<?php

apl_require_once(APOLLO_API_DIR. '/Syndication/Syndication.php');

class APL_Lib_Apis_Syndication_Organizations extends Syndication
{
    protected $limit = 10;
    protected $page = 1;
    protected $total = 0;


    public function __construct() {
        $this->sortFields = array('title');
        $this->pNodeName = 'organizations';
        $this->cNodeName = 'organization';
        $this->params = array('page', 'limit', 'sort', 'output');
        $this->allowMethod = array('get', 'options');
        parent::__construct();
    }

    /**
     * Example of an Endpoint
     */
    protected function get() {

        if ($this->method == 'GET') {
            $page = isset($this->request['page']) ? $this->request['page'] : 1;
            $limit = isset($this->request['limit']) ? $this->request['limit'] : 10;
            $ids =  get_post_meta( $this->apId, 'meta-orgs');

            if (!$this->orderBy) {
                $this->orderBy = 'title';
            }

            $args = array(
                'page' => $page,
                'limit' => $limit,
                'post_type' => Apollo_DB_Schema::_ORGANIZATION_PT,
                'order' => $this->order,
                'orderBy'   => $this->orderBy,
                'post_status'   => 'publish'
            );

            if ($ids && !empty($ids[0])) {
                $args['selected_item'] = $ids[0];
            }

            $orgs = Apollo_App::getDDLItemsBySearchKeyword($args);

            $data = array();
            if ($orgs['items']) {
                foreach($orgs['items'] as $v) {
                    $orgAddress = maybe_unserialize(Apollo_App::apollo_get_meta_data($v->ID, Apollo_DB_Schema::_APL_ORG_ADDRESS));
                    $orgMetadata = maybe_unserialize(Apollo_App::apollo_get_meta_data($v->ID, Apollo_DB_Schema::_APL_ORG_DATA));
                    $data[] = array(
                        "ID" => $v->ID,
                        "post_title" => $v->post_title,
                        "post_excerpt" => $v->post_excerpt,
                        "post_content" => $v->post_content,
                        "post_name" => $v->post_name,
                        "address1" => isset($orgAddress[Apollo_DB_Schema::_ORG_ADDRESS1]) ? $orgAddress[Apollo_DB_Schema::_ORG_ADDRESS1] : '',
                        "address2" => isset($orgAddress[Apollo_DB_Schema::_ORG_ADDRESS2]) ? $orgAddress[Apollo_DB_Schema::_ORG_ADDRESS2] : '',
                        "city" => isset($orgAddress[Apollo_DB_Schema::_ORG_CITY]) ? $orgAddress[Apollo_DB_Schema::_ORG_CITY] : '',
                        "state" => isset($orgAddress[Apollo_DB_Schema::_ORG_STATE]) ? $orgAddress[Apollo_DB_Schema::_ORG_STATE] : '',
                        "zip" => isset($orgAddress[Apollo_DB_Schema::_ORG_ZIP]) ? $orgAddress[Apollo_DB_Schema::_ORG_ZIP] : '',
                        "phone" => isset($orgMetadata[Apollo_DB_Schema::_ORG_PHONE]) ? $orgMetadata[Apollo_DB_Schema::_ORG_PHONE] : '',
                        "email" => isset($orgMetadata[Apollo_DB_Schema::_ORG_EMAIL]) ? $orgMetadata[Apollo_DB_Schema::_ORG_EMAIL] : '',
                        "website_url" => isset($orgMetadata[Apollo_DB_Schema::_ORG_WEBSITE_URL]) ? $orgMetadata[Apollo_DB_Schema::_ORG_WEBSITE_URL] : '',
                    );
                }
            }

            return $this->response(array(
                'data'  => $data,
                'hasMore'  => $orgs && $orgs['haveMore'] ? 'true' : 'false',
                'count'	=> $orgs ? $orgs['total'] :  0
            ));
        }
    }

}