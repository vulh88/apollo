<?php

/**
 * Created by PhpStorm.
 * User: pc92-vu
 * Date: 14/06/2016
 * Time: 14:13
 */
apl_require_once(APOLLO_API_DIR. '/Syndication/Syndication.php');
class APL_Lib_Apis_Syndication_Events extends Syndication
{
    public function __construct() {
        $this->params = array('page', 'limit', 'keyword', 'categories', 'venues', 'startDate', 'endDate', 'output', 'event_id');
        $this->pNodeName = 'events';
        $this->cNodeName = 'event';
        $this->allowMethod = array('get', 'options');
        parent::__construct();
    }

    /**
     * Example of an Endpoint
     */
    protected function get() {

        apl_require_once(APOLLO_API_DIR. '/Inc/Event.php');

        $eventsObj = new APL_Lib_Apis_Inc_Event($this->request, $this->apId);
        $eventsObj->hasPagination = true;
        if ($this->method == 'GET') {
            if (!empty($this->request['event_id'])){
                $this->pNodeName = "event";
                return $this->response($eventsObj->process_wp_request_event(false, false, $this->request['event_id']), '');
            }
            return $this->response($eventsObj->process_wp_request_event(false, false));
        }
    }
}