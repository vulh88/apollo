<?php
if (!empty($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], 'api/v1/')) {
    add_filter('wp_headers','apl_add_origin');
}

function apl_add_origin($headers) {
    if( !empty( $_SERVER['HTTP_REFERER'] ) ) {
        $ref = $_SERVER['HTTP_REFERER'];

        $aplAllowDomains = get_option('apl_cross_domain_settings', '');
        $headers['Access-Control-Allow-Headers'] = 'Accept-Encoding, X-Apl-Frontend';
        if ( $aplAllowDomains ) {
            if ($aplAllowDomains == '*') {
                $headers['Access-Control-Allow-Origin'] = '*';
                return $headers;
            } else {
                $sites = explode(",", $aplAllowDomains);
                foreach ($sites as $site) {
                    $pos = strpos($ref, trim($site) );
                    if ( $pos !== false ) {
                        $headers['Access-Control-Allow-Origin'] = trim($site);
                        return $headers;
                    }
                }
            }
        }
    }
    return $headers;
}


function cross_domain_settings_api_init() {
    add_settings_section('apl_cross_domain_setting_section',
        '<h1>' . __('Cross Domain Settings', 'apollo') . '</h1>',
        'apl_cross_domain_output',
        'general');

    add_settings_field('apl_cross_domain_settings',
        __('Allowed Domains', 'apollo'),
        'apl_cross_domain_callback',
        'general',
        'apl_cross_domain_setting_section');

    register_setting('general','apl_cross_domain_settings');
}

add_action('admin_init', 'cross_domain_settings_api_init');


function apl_cross_domain_output() {
    echo "<p>". __("Enter a comma-separated list of the domains you'd like to allow to access API system", "apollo"). "<br><br> ".__("Or enter an asterisk ( * ) to allow any site to make cross-domain AJAX requests to this site.", "apollo")."</p>";
}

function apl_cross_domain_callback() {
    echo '<input name="apl_cross_domain_settings" class="regular-text" id="apl_cross_domain_settings" type="text" value="' . get_option('apl_cross_domain_settings', '') . '" class="code" /> <p class="description">(eg. http://example.com, http://example1.com) - Without slash as the end a domain</p>';
}

?>
