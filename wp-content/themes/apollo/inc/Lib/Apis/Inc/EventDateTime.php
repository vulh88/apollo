<?php

/**
 * Created by PhpStorm.
 * User: pc92-vu
 * Date: 17/06/2016
 * Time: 14:46
 */
apl_require_once(APOLLO_ADMIN_SYNDICATE_DIR .'/class-apollo-syndicate-abstract.php');
apl_require_once(APOLLO_ADMIN_SYNDICATE_DIR .'/class-apollo-export-wp.php');

class APL_Lib_Apis_Inc_EventDateTime extends Apollo_Syndicate_Export_WP
{
    private $offset = false;
    private $page = 1;
    private $limit = 10;
    private $request;
    protected $eventId;

    public function __construct($eventId, $request, $apId)
    {
        $this->eventId = $eventId;
        $this->request = $request;

        if (!empty($request['page'])) {
            $this->page = $request['page'];
        }

        if (!empty($request['limit'])) {
            $this->limit = $request['limit'];
        }

        $this->output = !empty($request['output']) ? $request['output'] : 'json';

        $this->_setOffset();

        parent::__construct();

        $this->output = !empty($request['output']) ? $request['output'] : 'json';
        $this->apID = $apId;
    }

    function getData() {

        $syndicationMeta = get_post_meta($this->apID);
        $emStartDate = self::getMetaDataByKey('meta-dateStart', $syndicationMeta);
        $emEndDate = self::getMetaDataByKey('meta-dateEnd', $syndicationMeta);
        $emDateRange = self::getMetaDataByKey('meta-range', $syndicationMeta);
        $emFilterDate = self::getEventStartDateEndDateForQuery($emDateRange, $emStartDate, $emEndDate);
        $endDate = self::getValByKey($emFilterDate,'end-date','');

        $result = $this->get_dates_times($this->eventId, $endDate, $this->offset, $this->limit, $this->output);

        return array(
            'data'  => $result,
            'hasMore'  => $this->_haveMore() ? 'true' : 'false',
            'count' => $this->totalDateTime
        );
    }

    private function _haveMore() {
        return ($this->page) * $this->limit < intval( $this->totalDateTime );
    }

    private function _setOffset() {
        $this->offset = $this->limit * ($this->page - 1);
    }

    public function validEvent() {
        return $this->getEventsByFilters(array(
            'eIDs' => array($this->eventId),
        ));
    }

    /**
     * Abstract whether set store transient cache or not
     * @author vulh
     * @return boolean
     */
    protected function setEnableStoreTransientCache()
    {
        return false;
    }
}