<?php

/**
 * Created by PhpStorm.
 * User: pc92-vu
 * Date: 13/06/2016
 * Time: 12:04
 */

apl_require_once(APOLLO_ADMIN_SYNDICATE_DIR .'/class-apollo-syndicate-abstract.php');
apl_require_once(APOLLO_ADMIN_SYNDICATE_DIR .'/class-apollo-export-wp.php');

class APL_Lib_Apis_Inc_Event extends Apollo_Syndicate_Export_WP
{
    public function __construct($request, $apId)
    {
        parent::__construct();

        $this->searchParams = $request;
        $this->output = !empty($request['output']) ? $request['output'] : 'json';
        $this->apID = $apId;
    }

    /**
     * Abstract whether set store transient cache or not
     * @author vulh
     * @return boolean
     */
    protected function setEnableStoreTransientCache()
    {
        return false;
    }
}