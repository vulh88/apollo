<?php

/**
 * Created by PhpStorm.
 * User: pc92-vu
 * Date: 14/06/2016
 * Time: 14:37
 */
apl_require_once(APOLLO_API_DIR. '/ApolloAPI.php');

class Route extends Apollo_API
{
    protected $action;
    protected $allowActions = array(
        'events', 'categories', 'venues', 'event-datetimes', 'apollo-users-api', 'territories', 'organizations'
    );

    function __construct($action) {
        parent::__construct($action);
        $this->action = $action;
        $this->execute();
    }

    function execute() {

        if (!array_key_exists('HTTP_ORIGIN', $_SERVER)) {
            $_SERVER['HTTP_ORIGIN'] = $_SERVER['SERVER_NAME'];
        }

        if ($this->action && in_array($this->action, $this->allowActions)) {

            try {
                $this->action = str_replace(' ', '', ucwords(str_replace('-', ' ', $this->action)));
                switch ($this->action) {
                    case 'ApolloUsersApi' :
                        $api = apl_instance(sprintf('APL_Lib_Apis_User_%s', $this->action));
                        break;
                    default :
                        $api = apl_instance(sprintf('APL_Lib_Apis_Syndication_%s', $this->action));
                }
                die($api->processAPI());
            }
            catch (\Exception $e) {

                exit($this->_response(
                    array(
                        'error' => $e->getMessage()
                    ), 500)
                );

            }
        }

        header("Content-Type: application/json");
        exit($this->_response(
            array(
                'error' => 'Bad Request - Invalid action',
                'code'  => 1000,
            ), 400)
        );
    }

    public function filterRequest()
    {
        // TODO: Implement filterRequest() method.
    }
}
$action = get_query_var('_apl_action');
new Route($action);