<?php

/**
 * Created by PhpStorm.
 * User: pc92-vu
 * Date: 25/05/2016
 * Time: 14:26
 */
class APL_Lib_Helpers_PostTypeListing
{
    protected $postType = '';
    protected $page = 1;
    protected $limit = 20;
    protected $total;
    protected $data;

    function __construct(Array $properties=array()) {

        extract($properties);

        $this->postType = isset($postType) ? $postType : '';
        if (isset($page)) {
            $this->page = $page;
        }
    }

    function get($args = array()){

        $postType = $this->postType;
        $selectItem= isset($args['selected_item']) ? $args['selected_item'] : '';

        $result = array(
            'total' => 0,
            'items' => array()
        );

        if (empty($postType)){
            return $result;
        }

        $params = array(
            'post_type'         => $postType,
            'orderby'           => 'title',
            'order'             => 'ASC',
            'post_status'       => 'publish',
            'posts_per_page'    => $this->limit,
            'paged'             => $this->page
        );

        if(!empty($selectItem) && intval($selectItem) > 0){
            $params['post__in'] = array(intval($selectItem));
        }

        $this->data = query_posts( $params );

        global $wp_query;
        $this->total = $wp_query->found_posts;

        wp_reset_query();

        $result['items'] = $this->data;
        return $this->data;
    }

    function hasMore() {
        return ($this->page) * $this->limit < intval( $this->total );
    }

    function renderMultipleCheckbox($selected, $name = 'meta-orgs') {
        $this->get();
        if (!$this->data) return '';

        ob_start();
        foreach($this->data as $d):
        ?>
        <p class="form-field ">
            <input <?php echo is_array($selected) && in_array($d->ID, $selected) ? 'checked' : ''  ?> type="checkbox" class="checkbox "
                      name="<?php echo $name ?>[]" id="<?php echo $name ?>-<?php echo $d->ID ?>" value="<?php echo $d->ID ?>">
            <label for="<?php echo $name ?>-<?php echo $d->ID ?>"><?php echo $d->post_title ?></label></p>
        <?php
        endforeach;

        return ob_get_clean();
    }

    public function setLimit($limit = 20){
         $this->limit = intval($limit);
    }
}