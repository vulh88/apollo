<?php

/**
 * Created by PhpStorm.
 * User: pc92-vu
 * Date: 15/06/2016
 * Time: 18:30
 */

class ApolloSimpleXMLExtended extends SimpleXMLElement {

    public function addCData($cdata_text) {
        $node = dom_import_simplexml($this);
        $no   = $node->ownerDocument;
        $node->appendChild($no->createCDATASection($cdata_text));
    }

    public function addChildWithCDATA($name, $value = NULL) {
        $new_child = $this->addChild($name);

        if ($new_child !== NULL) {
            $node = dom_import_simplexml($new_child);
            $no   = $node->ownerDocument;
            $node->appendChild($no->createCDATASection($value));
        }

        return $new_child;
    }
}

class APL_Lib_Helpers_XML {

    public static function arrayToXml($data, &$xmlData = false, $rootNode = 'events', $childNode = 'event') {

        if ( $xmlData === false ) {
            //creating object of SimpleXMLElement
            $xmlData = new ApolloSimpleXMLExtended("<?xml version=\"1.0\" encoding=\"UTF-8\"?><$rootNode></$rootNode>");
        }

        foreach( $data as $key => $value) {

            if (is_object($value)) $value = (array) $value;

            if ( is_array($value) ) {
                if ( !is_numeric($key) ) {
                    $subnode = $xmlData->addChild("$key");
                    self::arrayToXml($value, $subnode, $rootNode, $childNode);
                } else {
                    $subnode = $xmlData->addChild($childNode);
                    self::arrayToXml($value, $subnode, $rootNode, $childNode);
                }
            } else {
                $xmlData->addChildWithCDATA( "$key", "$value" );
            }
        }
    }
}
