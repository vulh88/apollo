<?php
/**
 * Created by PhpStorm.
 * User: pc92-vu
 * Date: 27/04/2016
 * Time: 15:21
 */

class APL_Lib_Helpers_Select2
{
    public $enableRemote = false;
    public $post_type;
    public $selected_item;

    public function __construct(Array $properties=array())
    {
        extract($properties);

        $this->post_type = isset($post_type) ? $post_type : '';
        $this->selected_item = isset($selected_item) ? $selected_item : '';
        $this->setEnableRemote();
    }

    public function setEnableRemote($args = array()){
        $this->enableRemote = 1;
//        global $wpdb;
//        $postType = $this->post_type;
//        if(empty($postType)) return false;
//        $result = $wpdb->get_results("SELECT COUNT(*) as total FROM ".$wpdb->posts." WHERE post_type='".$postType."' AND post_status IN ('publish')");
//        $this->enableRemote = (intval($result[0]->total) > APL_Select2_Remote_Data::_APL_NUMBER_POST_TO_APPLY_REMOTE_DATA_SELECT2);
    }

    public function getEnableRemote() {
        return $this->enableRemote;
    }

    public function getItemsInPostTypeToDropDownList( $args = array() ) {

        $post_type = $this->post_type;
        $need_check_available = isset($args['need_check_available']) ? $args['need_check_available'] : true;
        $post_status = isset($args['post_status']) ? $args['post_status'] : array('publish');

        if (isset($args['selected_item'])) {
            $this->selected_item = $args['selected_item'];
        }

        if ($need_check_available) {
            $checkPostType = $post_type;
            if ($post_type == Apollo_DB_Schema::_EDUCATOR_PT ) {
                $checkPostType = Apollo_DB_Schema::_EDUCATION;
            }

            if (! Apollo_App::is_avaiable_module( $checkPostType )) {
                return array();
            }

        }

        if( $this->getEnableRemote() ){
            // mode enable remote data is enabled => return empty array for auto-changing
            // select2 box to kind of remote data via ajax request on searching by keyword.
            if(!empty($this->selected_item) && intval($this->selected_item) > 0){
                $selectedData = get_post($this->selected_item);
                return !empty($selectedData) ? array($selectedData) : array();
            }
            return array();
        }

        $args = array(
            'post_type'         => $post_type,
            'orderby'           => 'title',
            'order'             => 'ASC',
            'posts_per_page'    => -1,
            'post_status'       => $post_status,
        );
        $listPost = query_posts( $args );
        wp_reset_query();
        return $listPost;
    }

}