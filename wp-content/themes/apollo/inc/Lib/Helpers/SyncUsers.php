<?php

class APL_Lib_Helpers_Sync_Users
{

    public static function getUserFromUserID( $userID = 0, $siteID = '' ) {
        global $wpdb;
        $result = array();
        $table = $wpdb->base_prefix . 'users';
        $toUserID = defined('APL_USER_API_GET_TO_USER_ID') ? APL_USER_API_GET_TO_USER_ID : false;
        if (empty($siteID)) {
            $siteID = defined('APL_USER_API_MIGRATE_SITE_ID') ? APL_USER_API_MIGRATE_SITE_ID : false;
        }

        $join = '';
        if ($siteID) {
            $join = "
            INNER JOIN wp_apollo_blog_user bu ON bu.user_id = u.ID AND bu.blog_id = $siteID
        ";
        }

        if ($toUserID) {
            $toUserID = intval($toUserID);
            $users = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table u $join WHERE u.ID > %d AND u.ID <= %d  ", $userID, $toUserID), ARRAY_A);
        } else {
            $users = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table u $join WHERE u.ID > %d ", $userID), ARRAY_A);
        }
        if ($users) {
            foreach ($users as $user) {
                $result[$user['ID']]['ID'] = $user['ID'];
                $result[$user['ID']]['user_login'] = $user['user_login'];
                $result[$user['ID']]['user_nicename'] = $user['user_nicename'];
                $result[$user['ID']]['user_email'] = $user['user_email'];
                $result[$user['ID']]['user_pass'] = $user['user_pass'];
                $result[$user['ID']]['user_meta'] = get_user_meta($user['ID']);
                $result[$user['ID']]['blog_user'] = self::getUserBlog($user['ID']);
                $result[$user['ID']]['edit_link'] = add_query_arg( 'user_id', $user['ID'], self_admin_url( 'user-edit.php' ) );

            }
        }
        return $result;
    }

    /**
     * Get all blog by user_id
     * @param $userID
     * @return array|null|object
     */
    private static function getUserBlog( $userID ) {
        global $wpdb;
        $blogUserTbl = Apollo_App::getBlogUserTable();
        $result = $wpdb->get_results( $wpdb->prepare( 'SELECT * FROM '.$blogUserTbl.'
            WHERE user_id = %d ', $userID ), ARRAY_A );

        return $result;
    }

    public static function addUser($userInfo) {

        /** Create user with password default */
        $userID = wp_create_user($userInfo['user_login'], 'DefaultPassword', $userInfo['user_email']);
        if ($userID) {
            global $wpdb;
            $table = $wpdb->base_prefix . 'users';
            $tableMeta = $wpdb->base_prefix . 'usermeta';

            /** Update ID, user_pass from reference server */
            $wpdb->update($table, array('user_pass' => $userInfo['user_pass'], 'ID' => $userInfo['ID']), array('ID' => $userID));
            $wpdb->delete($tableMeta, array('user_id' => $userID));
            $userID = $userInfo['ID'];
            if (!empty($userInfo['user_meta'])) {
                foreach ($userInfo['user_meta'] as $key => $value) {
                    $metaValue = isset($value[0]) ? $value[0] : '';
                    update_user_meta($userID, $key, maybe_unserialize( $metaValue ) );
                }
            }
            if ($userInfo['blog_user']) {
                foreach ($userInfo['blog_user'] as $userBlog) {
                    self::addToBlogUser($userBlog);
                }
            }
        }
        return $userID;
    }

    private static function addToBlogUser ( $blogInfo ) {
        global $wpdb;
        $table = Apollo_App::getBlogUserTable();
        $sqlQuery = "SELECT blog_id FROM " . $table . " WHERE blog_id = %d AND user_id = %d ";
        $checkExist = $wpdb->get_col($wpdb->prepare($sqlQuery, intval($blogInfo['blog_id']), intval($blogInfo['user_id'])));
        if (empty($checkExist)) {
            return $wpdb->insert($table, array(
                'blog_id' => $blogInfo['blog_id'],
                'user_id' => $blogInfo['user_id'],
                'artist_id' => $blogInfo['artist_id'],
                'org_id' => $blogInfo['org_id'],
                'venue_id' => $blogInfo['venue_id'],
                'agency_id' => $blogInfo['agency_id'],
                'educator_id' => $blogInfo['educator_id']
            ));
        }
        return false;
    }

    public static function transferUser ( $userInfo) {
        if ( !$userInfo['is_current'] ) {
            return self::addUser( $userInfo );
        } else {
            try{
                $apiCall = wp_remote_post( APL_USER_API_BASE_URL, array(
                    'headers' => array(
                        'Content-Type' => 'application/json',
                        'token' => APL_USER_API_TOKEN
                    ),
                    'body' => json_encode(
                        array(
                            'action' => 'addUser',
                            'userInfo' => $userInfo
                        )
                    )
                ));
                return $apiCall;
            } catch (Exception $e) {
                return false;
            }

        }
    }

    /**
     * Get users only exist on current server or from the reference site
     * @param int $userID
     * @param int $limit
     * @param int $offset
     * @param string $usersFilter
     * @param string $blogId
     * @return array
     */
    public static function getUsersData( $userID = 0, $limit = 10, $offset = 0, $usersFilter = 'current_site', $blogId = '' ) {

        $apiUrl = APL_USER_API_BASE_URL . '?action=getUser&from-id=' . $userID . '&blog-id=' . $blogId;
        $apiCall = wp_remote_get($apiUrl,
                                array('headers' => array(
                                    'Content-Type' => 'application/json',
                                    'token' => APL_USER_API_TOKEN
                                )));
        $userFromServer = array();
        if (is_array($apiCall) && !empty( $apiCall['body']) ) {
            $dataFromServer = json_decode( $apiCall['body'], true );
            $userFromServer = !empty( $dataFromServer['users'] ) ? $dataFromServer['users'] : array();
        }

        $currentUsers = self::getUserFromUserID( $userID, $blogId );
        switch ( $usersFilter ) {
            case 'current_site' :

                $migratedSites = defined('APOLLO_MIGRATED_SITES') ? explode(',', APOLLO_MIGRATED_SITES) : false;
                $result = self::getUsersDifference($currentUsers, $userFromServer, $limit, $offset, true, $migratedSites);
                break;
            case 'reference_site' :
                $result = self::getUsersDifference($userFromServer, $currentUsers, $limit, $offset, false);
                break;
            case 'all_site' :
                $result = self::getUsersAllSite( $currentUsers, $userFromServer, $limit, $offset);
                break;
            default :
                $result = self::getUsersDifference($currentUsers, $userFromServer, $limit, $offset);
                break;
        }
        return $result;

    }

    /**
     * Get users only exist on current server
     * @param array $userCurrentServer
     * @param array $userReferenceServer
     * @param int $limit
     * @param int $offset
     * @param bool $current
     * @param $migratedSites
     * @return array
     */
    private static function getUsersDifference( $userCurrentServer = array(), $userReferenceServer = array(), $limit = 10, $offset = 0, $current = true, $migratedSites = false) {
        $result = array(
            'users' => array(),
            'total' => 0
        );
        if (!empty($userCurrentServer)) {
            $index = 0;
            $total = 0;
            $result['users'] = array();
            foreach ($userCurrentServer as $key => $value) {

                if (!empty($value['blog_user'][0]['blog_id']) && $migratedSites && in_array($value['blog_user'][0]['blog_id'], $migratedSites)) {
                    continue;
                }

                if (!isset($userReferenceServer[$key])) {
                    $index += 1;
                    $total += 1;
                    if ( ($index > $offset &&  sizeof($result['users']) < $limit)  ) {
                        if ($current) {
                            if ( defined('APL_USER_API_REFERENCE_SERVER_NAME') && !empty(APL_USER_API_REFERENCE_SERVER_NAME)) {
                                $value['move_to'] = APL_USER_API_REFERENCE_SERVER_NAME;
                            } else {
                                $value['move_to'] = __( 'Reference server', 'apollo' );
                            }
                            $value['is_current'] = true;
                        } else {
                            $value['move_to'] = __( 'Current server', 'apollo' );
                            $value['is_current'] = false;
                        }
                        $result['users'][$key] = $value;
                    }
                }
            }
            $result['total'] = $total;
        }
        return $result;
    }

    /**
     * Get users conflict
     * @param array $userCurrentServer
     * @param array $userReferenceServer
     * @param int $limit
     * @param int $offset
     * @return array
     */
    private static function getUsersAllSite( $userCurrentServer = array(), $userReferenceServer = array(), $limit = 10, $offset = 0 ) {
        $result = array(
            'users' => array(),
            'total' => 0
        );

        if ( empty($userCurrentServer) || empty($userReferenceServer) ) {
            return $result;
        }
        $index = 0;
        $total = 0;
        foreach ( $userCurrentServer as $key => $value ) {
            if (!isset($userReferenceServer[$key])
                || !isset($value['user_meta']['primary_blog'][0])
                || !isset($userReferenceServer[$key]['user_meta']['primary_blog'][0])
                || $value['user_meta']['primary_blog'][0] == $userReferenceServer[$key]['user_meta']['primary_blog'][0]) {
                continue;
            }
            $index += 1;
            $total += 1;
            if ($index > $offset && sizeof($result['users']) < $limit ) {
                $value['move_to'] = __( 'Conflict', 'apollo' );
                $value['reference_site_id'] = $userReferenceServer[$key]['user_meta']['primary_blog'][0];
                $value['reference_source_domain'] = $userReferenceServer[$key]['user_meta']['source_domain'][0];
                $value['reference_email'] = $userReferenceServer[$key]['user_email'];
                $result['users'][$key] = $value;

            }
        }
        $result['total'] = $total;

        return $result;
    }

    public static function getAllBlogIds() {
        global $wpdb;
        $blogUserTbl = Apollo_App::getBlogUserTable();
        $result = $wpdb->get_results( "SELECT blog_id FROM {$blogUserTbl} GROUP BY blog_id ", ARRAY_A );

        return $result;
    }
}