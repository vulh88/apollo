<?php
/** @Ticket #19481
 * Blog and News module
 * check post with post_title
 * insert/update post data
 * insert/update post metadata
 * insert/update cate and tag
 * insert/update image
 */

class APL_Lib_Helpers_Sync_Blog_News_Data
{

    public function copyDataToDestinationSite($postId, $siteId) {
        if (empty($siteId)) {
            return;
        }
        $postType = get_post_type($postId);
        $categoryIds = array();
        $tagIds = array();
        $taxonomy = 'category';
        $tagType = 'post_tag';
        if ($postType == 'news') {
            $taxonomy = 'news-type';
            $tagType = 'news_tag';
        }
        $categories = wp_get_post_terms($postId, $taxonomy);
        $tags = wp_get_post_tags($postId);
        $tagsOfficial = array();
        $currentPost = get_post($postId);
        $postMeta = get_post_meta($postId);
        $officialTags = get_apollo_meta($currentPost->ID, Apollo_DB_Schema::_APL_POST_OFFICIAL_TAGS,  true);
        $officialTags = explode('e', $officialTags);
        if ($tags) {
            foreach ($tags as $tagMeta) {
                $tagsOfficial[$tagMeta->term_id] = get_apollo_term_meta($tagMeta->term_id, Apollo_DB_Schema::_APL_POST_TAG_OFFICIAL, true);
            }
        }

        $featuredImageUrl = '';
        if (has_post_thumbnail($postId)) {
            $featuredImageUrl = wp_get_attachment_url(get_post_thumbnail_id($postId));
        }

        /** switch to destination site */
        switch_to_blog($siteId);

        /** Post */
        $desPost = $this->checkPostBySlug($currentPost->post_name, $postType);
        if (!empty($desPost)) {
            wp_update_post(array(
                'ID' => $currentPost->ID,
                'post_title' => $currentPost->post_title,
                'post_content' => $currentPost->post_content,
                'post_excerpt'   => $currentPost->post_excerpt,
                'comment_status' => $currentPost->comment_status,
            ));
        } else {
            $desPost = wp_insert_post(array(
                'post_title' => $currentPost->post_title,
                'post_name'  => $currentPost->post_name,
                'post_content' => $currentPost->post_content,
                'post_excerpt'   => $currentPost->post_excerpt,
                'post_status'  => $currentPost->post_status,
                'post_type'    => $postType,
                'comment_status' => $currentPost->comment_status,
            ));
        }
        $this->updateExcerpt($desPost, $currentPost->post_excerpt);
        /** Post meta */
        if ($desPost) {
            if (!empty($postMeta)) {
                foreach ($postMeta as $metaKey => $metaValue) {
                    $this->updatePostMeta($desPost, $metaKey, $metaValue);
                }
            }

            /** Categories */
            foreach ($categories as $cat) {
                $desCatId = $this->checkCatTagBySlug($cat->name, $taxonomy);
                if (!empty($desCatId)) {
                    $categoryIds[] = $desCatId;
                    /** update data */
                    wp_update_term($desCatId, $taxonomy, array(
                        'name' => $cat->name,
                        'description' => $cat->description
                    ));
                } else {
                    /** */
                    $insertCat = wp_insert_term($cat->name, $taxonomy, array(
                        'description' => $cat->description,
                        'parent'  => $cat->parent_id,
                        'slug' => $cat->slug
                    ));
                    if (!empty($insertCat['term_id'])) {
                        $categoryIds[] = $insertCat['term_id'];
                    }
                }
            }
            wp_set_post_terms($desPost, $categoryIds, $taxonomy);

            /** Tags */
            $newOfficialTag = [];
            foreach ($tags as $tag) {
                $desTagId = $this->checkCatTagBySlug($tag->name, $tagType);
                if (!empty($desTagId)) {
                    $tagIds[] = $desTagId;
                    /** update data */
                    wp_update_term($tag->term_id, $tagType, array(
                        'name' => $tag->name,
                        'description' => $tag->description
                    ));
                } else {
                    /** */
                    $insertTag = wp_insert_term($tag->name, $tagType, array(
                        'description' => $tag->description,
                        'parent'  => $tag->parent_id,
                        'slug' => $tag->slug
                    ));
                    if (!empty($insertTag['term_id'])) {
                        $tagsIds[] = $tag->slug;
                        $desTagId = $insertTag['term_id'];
                    }
                }
                if (in_array($tag->term_id, $officialTags)){
                    $newOfficialTag[] = $desTagId;
                }
                update_apollo_term_meta($tag->name, Apollo_DB_Schema::_APL_POST_TAG_OFFICIAL, $tagsOfficial[$tag->term_id]);
            }
            wp_set_post_terms($desPost, $tagIds, $tagType);

            /** Featured image */
            if (!empty($featuredImageUrl)) {
                $this->updatePostAttachment($featuredImageUrl, $desPost);
            }

            /* Official tag */
            $officialTags = '';
            if (!empty($newOfficialTag)) {
                $officialTags = 'e' . implode('e,e', $newOfficialTag) . 'e';
            }
            update_apollo_meta($desPost, Apollo_DB_Schema::_APL_POST_OFFICIAL_TAGS, $officialTags);
        }

        restore_current_blog();
        return $postId;

    }

    /**
     * Check post exists on the destination site
     * @param $slug
     * @param $postType
     * @return string
     */
    public function checkPostBySlug($slug, $postType) {
        $result = '';
        $args = array(
            'name'   =>  $slug,
            'post_type' => $postType,
            'numberposts' => 1
        );

        $data = get_posts($args);
        if (!empty($data[0]->ID)) {
            $result = $data[0]->ID;
        }
        return $result;
    }

    /**
     * Check category and tag exists on the destination site
     * @param $name
     * @param $taxonomy
     * @return int|string
     */
    public function checkCatTagBySlug($name, $taxonomy) {
        $result = '';
        $category = get_term_by('name', $name, $taxonomy);


        if ($category) {
            $result = $category->term_id;
        }
        return $result;
    }

    public static function getAttachmentIDByName($attachment_name){
        $args = array(
            'post_type' => 'attachment',
            'name' => sanitize_title($attachment_name),
            'posts_per_page' => 1,
            'post_status' => 'inherit'
        );
        $queryPosts = get_posts( $args );
        $att = $queryPosts ? array_pop($queryPosts) : null;
        return $att ? $att->ID : 0;
    }

    private function getAttachmentNiceName($image_url){
        if(!empty($image_url)){
            $filename_parts = explode('/', $image_url);
            $filename = end($filename_parts);
            $attachment_name = explode('.', $filename);
            $nice_name = $filename;
            if(sizeof($attachment_name) > 0){
                $nice_name = '';
                for ($i = 0; $i < sizeof($attachment_name) - 1; $i++){
                    $nice_name.= $attachment_name[$i];
                }
            }
            return $nice_name;
        }
        return '';
    }

    public function get_attachment_id_by_src ($image_src) {
        global $wpdb;
        $table = $wpdb->prefix. 'posts';
        $query = "SELECT ID FROM $table WHERE guid='$image_src'";
        $id = $wpdb->get_var($query);
        return $id;

    }

    public function updatePostAttachment($image_url, $postId){
        if($image_url){
            $filename_parts = explode('/', $image_url);
            $filename = end($filename_parts);
            $attachment_name = $this->getAttachmentNiceName($image_url);
            if(empty($attachment_name)) return 0;
            $attach_id = $this->getAttachmentIDByName($attachment_name);
            if(!$attach_id){
                require_once(ABSPATH . 'wp-admin/includes/media.php');
                require_once(ABSPATH . 'wp-admin/includes/file.php');
                require_once(ABSPATH . 'wp-admin/includes/image.php');
                $result = media_sideload_image($image_url, $postId, null, 'src');
                while (preg_match('/--/',$filename)){
                    $filename = preg_replace('/--/','-',$filename);
                }

                if($result && is_string($result)){
                    $attach_id = $this->get_attachment_id_by_src($result);
                }
            }
            update_post_meta($postId, '_thumbnail_id', $attach_id);

        }
    }

    public function updatePostMeta($postID, $metaKey, $metaValue) {
        global $wpdb;
        $metaValue = (isset($metaValue[0])) ? $metaValue[0] : '';
        $sqlWhere = "SELECT meta_id FROM {$wpdb->postmeta} WHERE post_id = {$postID} and meta_key = '{$metaKey}'";
        $result = $wpdb->get_col($sqlWhere);
        if (empty($result)) {
            $wpdb->insert($wpdb->postmeta, array('post_id' => $postID, 'meta_key' => $metaKey, 'meta_value' => $metaValue));
        } else {
            $wpdb->update($wpdb->postmeta, array('meta_value' => $metaValue), array('post_id' => $postID, 'meta_key' => $metaKey));
        }
    }

    public function updateExcerpt($postId, $excerpt) {
        global $wpdb;
        $wpdb->update($wpdb->posts, array('post_excerpt' => $excerpt), array('ID' => $postId));
    }

}