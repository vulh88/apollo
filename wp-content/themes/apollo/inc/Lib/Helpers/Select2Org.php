<?php
/**
 * Author: TriLm
 * extend and override Select2 class
 */
@include('Select2.php');
class APL_Lib_Helpers_Select2Org extends APL_Lib_Helpers_Select2{

    public function __construct(Array $properties=array()){
        parent::__construct($properties);
    }

    public function getItemsInPostTypeToDropDownList( $args = array() ) {
        global $wpdb;
        $strSql = $this->getSqlStr('*', '','', '', 'post_title', 'ASC');
        return $wpdb->get_results($strSql, OBJECT);
    }

    public function setEnableRemote($args = array()){
        $this->enableRemote = 1;
        global $wpdb;
        $postType = $this->post_type;
        if(empty($postType)) return false;
        $sqlStr = $this->getSqlStr('COUNT(*) as total');
        $result = $wpdb->get_results($sqlStr);
        $this->enableRemote = (intval($result[0]->total) > APL_Select2_Remote_Data::_APL_NUMBER_POST_TO_APPLY_REMOTE_DATA_SELECT2);
    }

    protected function getSqlStr($select = 'COUNT(*) as total',$where = '',$limit = '', $offset = '', $orderby = '', $order = ''){
        global $wpdb;
        $post_type = $this->post_type;
        $need_check_available = isset($args['need_check_available']) ? $args['need_check_available'] : true;
        if ( $need_check_available && ! Apollo_App::is_avaiable_module( $post_type ) ) {
            return array();
        }
        $orgIdKey = 'apollo_organization_id';
        $orgMetaTable = $wpdb->{Apollo_Tables::_APL_ORG_META};
        $metaKey = Apollo_DB_Schema::_APL_ORG_DATA;
        $activedMemberSelectSQl = '';
        if(of_get_option(Apollo_DB_Schema::_ORGANIZATION_ACTIVE_MEMBER_FIELD)){
            $activedMemberSelectSQl = "
            AND p.ID IN (
              SELECT $orgIdKey
              FROM  $orgMetaTable mt
              WHERE mt.meta_key = '$metaKey'
                  AND mt.meta_value LIKE '%\"_org_member_only\";s:3:\"yes\"%'
          )
        ";
        }

        $strQuery = "
          SELECT $select  FROM {$wpdb->posts} p
          WHERE p.post_type='$post_type' 
          $where
          AND p.post_status IN ('publish') $activedMemberSelectSQl ";

        $orderby = !empty($orderby) ? $orderby : 'post_title';
        $order = !empty($order) ? $order : 'ASC';
        $strQuery  .=  " ORDER BY ".$orderby . " ". $order . " ";


        if($limit  !== ''){
            $strQuery  .=  " LIMIT ".$limit;
            if($offset !== ''){
                 $strQuery  .= " OFFSET $offset";
            }
        }

        return $strQuery;
    }

    public  function getMemberOrg($args = array()){
        global $wpdb,$wp_query;
        $where = '';
        $limit = isset($args['limit'])?$args['limit']:'';
        $key = isset($args['s'])?$args['s']:'';
        $page = isset($args['page'])?$args['page']:1;
        $offset = ($page - 1)  * $limit;
        if($key != ''){
            $where = "AND p.post_title LIKE '%$key%'" ;
        }
        $sqlStr = $this->getSqlStr(' * ',$where,$limit,$offset);
        $sqlStrTotal = $this->getSqlStr(' COUNT(*) AS count ',$where);
        $listPost = $wpdb->get_results($sqlStr);
        $total = $wpdb->get_results($sqlStrTotal);
        $total = $total[0]->count;
        $return = array(
            'total' => $total,
            'items' => $listPost,
            'haveMore' => $page * $limit < $total,
        );
        return $return;
    }


}