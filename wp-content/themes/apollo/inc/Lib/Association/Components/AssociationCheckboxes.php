<?php

apl_require_once(APOLLO_INCLUDES_DIR. '/Lib/Abstracts/ApolloComponentAbstract.php');

class APL_Lib_Association_Components_AssociationCheckboxes extends ApolloComponentAbstract {

    public function render($configs) {

        $wrapListItems = 'apollo-association-list-'.$configs['post_type'];

        if ($configs['admin_type'] == Apollo_DB_Schema::_AGENCY_PT) {
            /** For Agency */
            $main_id = $configs['agency_id'];
            $classUserAssociation = '';
            if ($configs['post_type'] == 'user') {
                $listOptions = ApolloAssociationFunction::getUsersForAgency($configs['agency_id']);
            } else {
                $listOptions = ApolloAssociationFunction::getAssociationModuleForAgency($configs['agency_id'], $configs['post_type']);
            }
            $ajaxUrl = admin_url('admin-ajax.php?action=apollo_agency_get_more_association_options&post_type='.$configs['post_type'].'&offset=' . $listOptions['offset']. '&agency_id=' .$configs['agency_id'] );
        } else {
            /** For User */
            $classUserAssociation = 'user-wrap-association-list';
            $main_id = $configs['user_id'];
            if ($configs['post_type'] == Apollo_DB_Schema::_AGENCY_PT) {
                $classUserAssociation = '';
                $currentAgency = 'data-current-agency="'. (!empty($configs['agency_list']) ? implode(',', $configs['agency_list']) : "") .'"';
            }
            $listOptions = ApolloAssociationFunction::getAssociationModuleForUser($configs['user_id'], $configs['post_type']);
            $ajaxUrl = admin_url('admin-ajax.php?action=apollo_user_get_more_association_options&post_type='.$configs['post_type'].'&offset=' . $listOptions['offset']. '&user_id=' .$configs['user_id'] );
        }

        ?>
        <div class="postbox apl-custom-postbox <?php echo $configs['wrap_class']; ?> <?php echo $classUserAssociation; ?>">
            <div class="handlediv" title="<?php _e( 'Click to toggle' ) ?>"><br></div>
            <h3 class="hndle apl-custom-title"><?php echo $configs['label'] ?></h3>

            <div id="message-<?php echo $configs['post_type'] ?>" class="error below-h2 hidden">
                <p><?php echo sprintf( __( 'There are some %s are not published.', 'apollo' ), strtolower( $configs['label'] ) ) ?></p>
            </div>
            <div class="section wrap-multiselect">
                <div class="association-list"
                     data-user-id="<?php echo $main_id;  ?>"
                     data-post-type="<?php echo $configs['post_type']; ?>"
                    <?php echo (isset($currentAgency) ? $currentAgency : ''); ?>
                >
                    <div class="group-search">
                        <input class="search-association" data-admin-type="<?php echo $configs['admin_type']; ?>" placeholder="<?php _e('Search by name', 'apollo')?>"/>
                        <span class="remove-search-text empty-search hidden"><i class="fa fa-times"></i></span>
                    </div>
                    <ul id="<?php echo $wrapListItems; ?>" class="apl-user-association-list <?php echo $wrapListItems; ?>">
                        <?php echo (Apollo_App::userRenderAssociationItems($listOptions['data'])); ?>
                    </ul>
                    <input type="hidden" name="<?php echo $configs['associated_id'];?>-checked" class="apl-association-checked-list" value=""/>
                    <input type="hidden" name="<?php echo $configs['associated_id'];?>-unchecked" class="apl-association-unchecked-list" value=""/>
                </div>
                <div class=" association-load-more <?php echo $listOptions['have_more'] ? '' : 'hidden' ?>">
                    <div class="load-more b-btn wp-ct-event">
                        <a href="javascript:void(0);"
                           data-container="#<?php echo $wrapListItems; ?>"
                           data-ride="ap-more"
                           data-holder=".<?php echo $wrapListItems; ?>>:last"
                           data-sourcetype="url"
                           data-sourcedata="<?php echo $ajaxUrl; ?>"
                           data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
                           data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
                           class="btn-b arw ct-btn-sm-event"><?php _e('SHOW MORE', 'apollo') ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    public function renderSingleEducator($configs) {
        $classUserAssociation = 'user-wrap-association-list';
        if ($configs['post_type'] == Apollo_DB_Schema::_AGENCY_PT) {
            $classUserAssociation = '';
        }

        ?>
        <div class="postbox apl-custom-postbox <?php echo $configs['wrap_class']; ?> <?php echo $classUserAssociation; ?>">
            <div class="handlediv" title="<?php _e( 'Click to toggle' ) ?>"><br></div>
            <h3 class="hndle apl-custom-title"><?php echo $configs['label'] ?></h3>

            <div id="message-<?php echo $configs['post_type'] ?>" class="error below-h2 hidden">
                <p><?php echo sprintf( __( 'There are some %s are not published.', 'apollo' ), strtolower( $configs['label'] ) ) ?></p>
            </div>
            <div class="section wrap-multiselect">
                <select
                        data-value="<?php echo $configs['id'] ?>"
                        data-enable-remote="1"
                        data-post-type="<?php echo $configs['post_type']; ?>"
                        data-source-url="apollo_get_remote_associate_data_to_select2_box"
                        data-status="publish, pending"
                        id="educator_id"
                        class="w-200 select apl_select2"
                        name="educator_id"
                >
                    <option value=""><?php _e( '-- Select --', 'apollo' ) ?></option>
                    <?php

                    $id = !empty($configs['data']) ? $configs['data']->ID : '';

                    if ($id) {
                        $name = $configs['data']->post_title;

                        if ($configs['data']->post_status == 'pending') {
                            $name .= ' (*)';
                        }
                        echo "<option selected  value='{$id}'>{$name}</option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <?php
    }
}