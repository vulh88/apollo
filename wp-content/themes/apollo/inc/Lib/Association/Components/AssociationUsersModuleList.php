<?php

apl_require_once(APOLLO_INCLUDES_DIR. '/Lib/Abstracts/ApolloComponentAbstract.php');

class APL_Lib_Association_Components_AssociationUsersModuleList extends ApolloComponentAbstract {

    /**
     * Render associated users
     *
     * @param $configs
     *
     * @return mixed|void
     */
    public function render($configs)
    {
        $objectID     = isset($configs['id'])            ? $configs['id']            : '';
        $postType     = isset($configs['post_type'])     ? $configs['post_type']     : '';
        $page         = isset($configs['page'])          ? $configs['page']          : 'list';
        $displayTitle = isset($configs['display_title']) ? $configs['display_title'] : true;
        $limit    = ($page == 'list')
                  ? Apollo_Display_Config::APL_ASSOCIATED_USERS_LIST_PAGE_LIMIT
                  : Apollo_Display_Config::APL_ASSOCIATED_USERS_DETAIL_PAGE_LIMIT;

        // get list of associated users
        $result = ApolloAssociationFunction::getSelectedAssociationUsers($objectID, $postType, $limit);
        $usersHtml = ApolloAssociationFunction::renderAssociatedUsers($result['users']);

        return ($page == 'list')
               ? $this->_renderForModuleListPage($objectID, $usersHtml, $result['hasMore'])
               : $this->_renderForModuleDetailPage($objectID, $postType, $usersHtml, $result['hasMore'], $displayTitle);
    }

    /**
     * Render associated users for list page
     *
     * @param int   $postId
     * @param array $usersHtml
     * @param bool  $hasMore
     */
    private function _renderForModuleListPage($postId, $usersHtml, $hasMore = false)
    {
        if (count($usersHtml) > 0 && $hasMore) {
            $usersHtml[] = '<a href="' . esc_url(get_edit_post_link($postId)) .'" target="_blank">&nbsp;...</a>';
        }

        echo implode(', ', $usersHtml);
    }

    /**
     * Render associated users for detail page
     *
     * @param int    $postId
     * @param string $postType
     * @param array  $usersHtml
     * @param bool   $hasMore
     * @param bool   $displayTitle
     *
     * @return mixed
     */
    private function _renderForModuleDetailPage($postId, $postType, $usersHtml, $hasMore = false, $displayTitle = true)
    {
        if (count($usersHtml) <= 0) {
            return;
        }

        $ajaxUrl = admin_url('admin-ajax.php?action=apollo_get_more_associated_users&post_id=' . $postId . '&post_type=' . $postType . '&offset=' . Apollo_Display_Config::APL_ASSOCIATED_USERS_DETAIL_PAGE_LIMIT);

        ?>

        <div class="options_group event-box">
            <p>
                <?php
                if ($displayTitle) {
                    ?>
                    <label> <?php echo __('Associated user(s)', 'apollo'); ?></label>
                    <br/>
                    <span class="description"></span>
                    <?php
                }
                ?>
                <div id="associated-users-list-detail-page" class="associated-users-list-detail-page-more">
                    <?php echo implode('<p class="form-field">', $usersHtml); ?>
                </div>
            </p>

            <div class="associated-users-list-load-more <?php echo $hasMore ? '' : 'hidden' ?>">
                <div class="load-more b-btn wp-ct-event">
                    <a href="javascript:void(0);"
                       data-container="#associated-users-list-detail-page"
                       data-ride="ap-more"
                       data-holder=".associated-users-list-detail-page-more>:last"
                       data-sourcetype="url"
                       data-sourcedata="<?php echo $ajaxUrl; ?>"
                       data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
                       data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
                       class="btn-b arw ct-btn-sm-event taxonomy-add-new"><?php _e('Show more', 'apollo') ?>
                    </a>
                </div>
            </div>
        </div>

        <?php
    }
}
