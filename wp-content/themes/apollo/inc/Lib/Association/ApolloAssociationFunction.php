<?php

class ApolloAssociationFunction
{
    /**
     * @Ticket #14988 - Get multiple users for Agency
     * @param $agencyID
     * @param int $offset
     * @param string $textSearch
     * @return array
     */
    public static function getUsersForAgency($agencyID, $offset = 0, $textSearch = '') {
        global $wpdb;

        $userTable = $wpdb->base_prefix.'users';
        $userModulesTable = $wpdb->prefix . Apollo_Tables::_APL_USER_MODULES;
        $userBlogTbl = Apollo_App::getBlogUserTable();
        $post_type = Apollo_DB_Schema::_AGENCY_PT;
        $blogID = get_current_blog_id();
        $sql = " SELECT SQL_CALC_FOUND_ROWS u.ID, u.user_nicename AS post_title, umt.user_id AS item_selected, umt.post_id AS agency_id FROM {$userTable} u
                INNER JOIN {$userBlogTbl} ubl ON ubl.user_id = u.ID AND ubl.blog_id = {$blogID}
                LEFT JOIN {$userModulesTable} umt ON umt.user_id = u.ID AND umt.post_id = {$agencyID} AND umt.post_type = '{$post_type}'
                WHERE 1=1 ";

        if (!empty($textSearch)) {
            $sql .= " AND (u.user_nicename LIKE '%{$textSearch}%' )";
        }
        $sql .= " ORDER BY item_selected DESC, post_title ASC
                     LIMIT " . Apollo_Display_Config::APL_USER_ASSOCIATION_LIMIT .
            " OFFSET " . intval($offset);

        $data = $wpdb->get_results( $sql );
        $count_result = $wpdb->get_var("SELECT FOUND_ROWS()");

        return array(
            'data' => $data,
            'total' => $count_result,
            'offset' => count($data) + $offset,
            'have_more' => count($data) + $offset < $count_result,
        );

    }

    /**
     * @Ticket #14988 - Get items for Agency by post_type
     * @param $agencyID
     * @param $postType
     * @param int $offset
     * @param string $textSearch
     * @return array
     */
    public static function getAssociationModuleForAgency($agencyID, $postType, $offset = 0, $textSearch = '') {
        if(empty($postType)){
            return array();
        }
        global $wpdb;
        $sql = "SELECT SQL_CALC_FOUND_ROWS p.ID, p.post_title, p.post_status";
        $postTable = $wpdb->prefix . 'posts';
        switch ($postType) {
            case Apollo_DB_Schema::_EDUCATOR_PT :
                $tableMeta = Apollo_Tables::_APL_AGENCY_EDUCATOR;
                $keyMeta = 'am.edu_id';
                break;
            case Apollo_DB_Schema::_ORGANIZATION_PT :
                $tableMeta = Apollo_Tables::_APL_AGENCY_ORG;
                $keyMeta = 'am.org_id';
                break;
            case Apollo_DB_Schema::_VENUE_PT :
                $tableMeta = Apollo_Tables::_APL_AGENCY_VENUE;
                $keyMeta = 'am.venue_id';
                break;
            case Apollo_DB_Schema::_ARTIST_PT :
                $tableMeta = Apollo_Tables::_APL_AGENCY_ARTIST;
                $keyMeta = 'am.artist_id';
                break;
            default :
                $tableMeta = '';
                $keyMeta = '';
                break;
        }
        if (!empty($keyMeta) && !empty($tableMeta)) {
            $tableMeta = $wpdb->prefix . $tableMeta;
            $sql .= ", {$keyMeta} AS item_selected FROM {$postTable} p
                    LEFT JOIN {$tableMeta} am ON p.ID = {$keyMeta} AND am.agency_id = {$agencyID}
                    WHERE p.post_type = '{$postType}' AND p.post_status IN ('publish', 'pending') ";
            if (!empty($textSearch)) {
                $sql .= " AND (p.post_title LIKE '%{$textSearch}%' )";
            }
            $sql .= " ORDER BY item_selected DESC, post_title ASC
                     LIMIT " . Apollo_Display_Config::APL_USER_ASSOCIATION_LIMIT .
                " OFFSET " . intval($offset);

            $data = $wpdb->get_results( $sql );
            $count_result = $wpdb->get_var("SELECT FOUND_ROWS()");

            return array(
                'data' => $data,
                'total' => $count_result,
                'offset' => count($data) + $offset,
                'have_more' => count($data) + $offset < $count_result,
            );
        }

        return array(
            'data' => array(),
            'total' => 0,
            'offset' => 0,
            'have_more' => false,
        );
    }

    /**
     * Get more association by post_type for User
     * @param $userID
     * @param string $post_type
     * @param int $offset
     * @param string $textSearch
     * @return array
     */
    public static function getAssociationModuleForUser($userID, $post_type = '', $offset = 0, $textSearch = ''){
        if(empty($post_type)){
            return array();
        }
        global $wpdb;

        $associationTable = $wpdb->prefix . Apollo_Tables::_APL_USER_MODULES;
        $keySearch = 'p.post_title';
        if ($post_type == Apollo_DB_Schema::_AGENCY_PT) {
            $sql = "SELECT SQL_CALC_FOUND_ROWS p.agencyID AS ID, p.name AS post_title, am.user_id AS item_selected
                    FROM {$wpdb->{Apollo_Tables::_APL_AGENCY}} p
                    LEFT JOIN {$associationTable} am ON am.post_id = p.agencyID AND am.user_id = '{$userID}' AND am.post_type ='".$post_type."'
                    WHERE 1=1
                    ";
            $keySearch = 'p.name';
        } else {
            $sql = "SELECT SQL_CALC_FOUND_ROWS p.ID, p.post_title, am.user_id AS item_selected, IF(p.post_author = {$userID}, 1, 0) AS is_author, p.post_status, p.post_author
                    FROM {$wpdb->posts} p
                    LEFT JOIN {$associationTable} am ON am.post_id = p.ID AND am.user_id = '{$userID}' AND am.post_type ='".$post_type."'
                    WHERE p.post_type = '". $post_type ."'
                    AND p.post_status IN ('publish', 'pending') " ;
        }
        $limit = Apollo_Display_Config::APL_USER_ASSOCIATION_LIMIT;

        if(!empty($textSearch)) {
            $sql .= " AND ({$keySearch} LIKE '%{$textSearch}%' )";
        }

        $sql .= " ORDER BY item_selected DESC, post_title ASC ";

        $sql .= "
            LIMIT " . $limit . "
            OFFSET " . intval($offset);

        $data = $wpdb->get_results( $sql );
        $count_result = $wpdb->get_var("SELECT FOUND_ROWS()");

        return array(
            'data' => $data,
            'total' => $count_result,
            'offset' => count($data) + $offset,
            'have_more' => count($data) + $offset < $count_result,
        );

    }

    /**
     * Get selected association users
     *
     * @param int    $objectID
     * @param string $postType (in these types: artist, organization, venue, agency, educator)
     * @param int    $limit
     * @param int    $offset
     *
     * @return array
     * @ticker #15165 - [CF] 20180126 - [2049#c12311] Selected association users on the artist list
     */
    public static function getSelectedAssociationUsers($objectID, $postType = 'artist', $limit = Apollo_Display_Config::APL_ASSOCIATED_USERS_LIST_PAGE_LIMIT, $offset = 0)
    {
        global $wpdb;

        $limit    = 'LIMIT ' . $limit . ' OFFSET ' . intval($offset);
        $postTable = $wpdb->prefix . 'posts';
        //do not show authors below #10
        $query = sprintf("SELECT SQL_CALC_FOUND_ROWS user_id FROM %s WHERE 1=1 AND post_id = '%d' AND post_type ='%s' UNION SELECT post_author from %s p WHERE ID = '%s' AND post_author > 10 $limit",$wpdb->prefix . Apollo_Tables::_APL_USER_MODULES, $objectID, $postType,$postTable,$objectID);
        $result = $wpdb->get_results($query);
        $total    = (int) $wpdb->get_var("SELECT FOUND_ROWS()");
        return array(
            'users'   => $result,
            'total'   => $total,
            'offset'  => count($result) + $offset,
            'hasMore' => count($result) + $offset < $total,
        );
    }

    /**
     * Get selected association agencies
     *
     * @param int    $postId
     * @param string $postType (in these types: artist, organization, venue, agency, educator)
     * @param int    $limit
     * @param int    $offset
     *
     * @return array
     * @ticket #15165 - [CF] 20180126 - [2049#c12311] Selected association users on the artist list
     * @ticket #15222 - [CF] 20180126 - [2049#c12311] Selected association users on the organization, venue, educator list
     */
    public static function getSelectedAssociationAgencies($postId, $postType = 'artist', $limit = Apollo_Display_Config::APL_ASSOCIATED_USERS_LIST_PAGE_LIMIT, $offset = 0)
    {
        global $wpdb;

        if (!Apollo_App::is_avaiable_module(Apollo_DB_Schema::_AGENCY_PT)) {
            return array();
        }

        switch($postType) {
            case 'artist':
                $table = $wpdb->prefix . Apollo_Tables::_APL_AGENCY_ARTIST;
                $module = 'artist_id';
                break;
            case 'organization':
                $table = $wpdb->prefix . Apollo_Tables::_APL_AGENCY_ORG;
                $module = 'org_id';
                break;
            case 'venue':
                $table = $wpdb->prefix . Apollo_Tables::_APL_AGENCY_VENUE;
                $module = 'venue_id';
                break;
            case 'educator':
                $table = $wpdb->prefix . Apollo_Tables::_APL_AGENCY_EDUCATOR;
                $module = 'edu_id';
                break;
        }

        $aplQuery = new Apl_Query($table);
        $where    = "$module = $postId";
        $agencies = $aplQuery->get_where($where, 'agency_id', 'LIMIT ' . $limit . ' OFFSET ' . intval($offset));
        $total    = $aplQuery->get_total($where);

        return array(
            'agencies' => $agencies,
            'total'    => $total,
            'offset'   => count($agencies) + $offset,
            'hasMore'  => count($agencies) + $offset < $total,
        );
    }

    /**
     * Render each associated user to an anchor link
     *
     * @param array $data
     * @param bool  $isAjax
     *
     * @return string
     * @ticket #15226 - [CF] 20180126 - [2049#c12311][Admin] Selected association users on the organization, venue, educator, artist detail
     */
    public static function renderAssociatedUsers($data = array(), $isAjax = false)
    {
        if (empty($data)) {
            return array();
        }

        $html = array();
        foreach ($data as $user) {
            $user = get_user_by('id' , $user->user_id);
            if ($user) {
                $html[] = '<a href="' . get_edit_user_link($user->ID) . '" target="_blank" style="text-decoration: none;">' . $user->user_nicename . '</a>';
            }
        }

        return $isAjax ? implode('<p class="form-field">', $html) : $html;
    }

    /**
     * Render each associated agencies to an anchor link
     *
     * @param array $data
     * @param bool  $isAjax
     *
     * @return string
     * @ticket #15226 - [CF] 20180126 - [2049#c12311][Admin] Selected association users on the organization, venue, educator, artist detail
     */
    public static function renderAssociatedAgencies($data = array(), $isAjax = false)
    {
        if (empty($data)) {
            return array();
        }

        global $wpdb;
        $html = array();
        $apl_query = new Apl_Query($wpdb->prefix . Apollo_Tables::_APL_AGENCY);
        foreach ($data as $agency) {
            $row = $apl_query->get_row("agencyID = $agency->agency_id");
            if ($row) {

                $html[] = '<a href="'. admin_url() . 'admin.php?page=admin-apollo-agency-form&action=edit&id=' . $agency->agency_id . '" target="_blank" style="text-decoration: none;">'. $row->name . '</a>';
            }
        }

        return $isAjax ? implode('<p class="form-field">', $html) : $html;
    }

    /**
     * Get associated modules based on user ID and post type
     *
     * @param int    $userId
     * @param string $postType
     * @param bool   $isCount
     *
     * @return array
     * @ticket #15252 - [CF] 20180309 - Manage Artist: The artist doesn't show although it's assigned
     */
    public static function getAssociatedModules($userId, $postType = 'artist', $isCount = false)
    {
        if (empty($userId)) {
            return array();
        }

        global $wpdb;

        $table  = $wpdb->prefix . Apollo_Tables::_APL_USER_MODULES;
        if ($isCount) {
            $query = sprintf("SELECT count(*) FROM %s WHERE user_id = %d AND post_type = '%s'", $table, $userId, $postType);
            return (int) $wpdb->get_var($query);
        }

        $query  = sprintf("SELECT post_id FROM %s WHERE user_id = %d AND post_type = '%s'", $table, $userId, $postType);
        $result = $wpdb->get_results($query, ARRAY_A);

        return array_map('current', $result); // convert to multidimensional array to simple array
    }

    /**
     * @param $table
     * @param $postIdName
     * @ticket: 15971
     * @return array
     */
    public static function getAssociatedItemsByAgency($table, $postIdName){
        global $wpdb;

        $agencyIds = self::getAssociatedModules(get_current_user_id(), Apollo_DB_Schema::_AGENCY_PT);
        if (count($agencyIds) <= 0) {
            return array();
        }

        $apl_query = new Apl_Query($wpdb->prefix . $table);

        $posts   = $apl_query->get_where("agency_id IN (" . implode(', ', $agencyIds) . " )", "DISTINCT $postIdName", " LIMIT 15 ");

        return $posts;
    }
}
