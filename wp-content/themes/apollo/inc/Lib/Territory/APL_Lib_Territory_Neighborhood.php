<?php

class APL_Lib_Territory_Neighborhood
{

    /***
     * Get current territory table
     * @return string
     */
    private static function getTerritoryTable() {
        global $wpdb;
        if (Apollo_App::get_network_manage_states_cities()) {
            return $wpdb->prefix . Apollo_Tables::_APL_STATE_ZIP;
        } else {
            return $wpdb->base_prefix . Apollo_Tables::_APL_STATE_ZIP;
        }
    }

    /**
     * Insert, update Neighborhood
     * @param array $data
     * @return array
     */
    public static function save($data = array()) {
        global $wpdb;
        $table = $wpdb->prefix . Apollo_Tables::_APL_NEIGHBORHOOD;
        $result = array(
            'status' => true,
            'message' => __( 'Success!', 'apollo' )
        );
        if ($data['id'] != 0) {
            //update
            $save_id = $wpdb->update($table,
                array(
                    'name' => $data['name'],
                    'parent_id' => $data['parent_id']
                ),
                array(
                    'id' => $data['id']
                )
            );
        } else {
            //insert
            $save_id = $wpdb->insert($table, array(
                'name' => $data['name'],
                'parent_id' => $data['parent_id']
            ));
        }
        if (!$save_id) {
            $result['status'] = false;
            $result['message'] = __( 'Cannot save data. Some error occurred!', 'apollo' );
        }

        return $result;
    }

    /**
     * @param $id
     * @return false|int
     */
    public static function delete( $id ) {
        global $wpdb;
        $table = $wpdb->prefix . Apollo_Tables::_APL_NEIGHBORHOOD;
        return $wpdb->delete($table, array('id' => intval($id)));

    }

    /**
     * get by city code
     * @param $parent_code
     * @param string $state
     * @return array|null|object
     */
    private static function getByParentCode( $parent_code, $state= '' ) {
        global $wpdb;
        if (!empty($parent_code) && !empty($state)) {
            $n_table = $wpdb->prefix . Apollo_Tables::_APL_NEIGHBORHOOD;
            $terr_table = self::getTerritoryTable();
            $state_id = self::getStateIdByCode($state);
            if ($state_id) {
                $sql_query = "SELECT n.id, n.name FROM $n_table n INNER JOIN $terr_table t ON n.parent_id=t.id WHERE t.code='%s' AND t.parent=%d ORDER BY n.name ASC ";
                return $wpdb->get_results($wpdb->prepare($sql_query, $parent_code, $state_id), ARRAY_A);
            } else {
                return array();
            }
        }
        return array();
    }

    /**
     * @Ticket #14143
     * get all Neighborhood
     * @return array
     */
    public static function getAllNeighborhood() {
        global $wpdb;
        $table = $wpdb->prefix . Apollo_Tables::_APL_NEIGHBORHOOD;
        $sql = "SELECT id, name, parent_id FROM $table WHERE type='city' ORDER BY name ASC";
        $data = $wpdb->get_results($sql, ARRAY_A);
        $result = array();
        if (!empty($data)) {
            foreach ($data as $item) {
                $result[$item['id']] = $item['name'];
            }
        }
        return $result;
    }

    /***
     * Render Neighborhood options
     * @param string $city
     * @param string $state
     * @param bool $required
     * @param string $selectedValue
     * @return string
     */
    public static function renderNeighborhoodOptions($city = '', $state = '', $required = false, $selectedValue = '') {
        $neighborhoodData = array();
        if (!empty($city) && !empty($state)) {
            $neighborhoodData = APL_Lib_Territory_Neighborhood::getNeighborhoodByCity($city, $state);
        }
        $optionRender = '';
        if (!empty($neighborhoodData)) {
            $optionRender = '<option value="">' . (sprintf(__('Select Neighborhood%s', 'apollo'), $required ? ' (*)' : '')) . '</option>';
            foreach ($neighborhoodData as $key => $value) {
                if (!empty($selectedValue) && $selectedValue == $key) {
                    $optionRender .= '<option value="'.$key.'" selected >'.$value.'</option>';
                } else {
                    $optionRender .= '<option value="'.$key.'">'.$value.'</option>';
                }
            }
        }
        return $optionRender;
    }

    /**
     * @Ticket #16482 - Render neighborhood on event search widget
     * @param $listCityState
     * @param $selectedCity
     * @param $selectedNeighborhood
     * @return string
     */
    public static function renderNeighborhoodOnEventSearch( $listCityState, $selectedCity, $selectedNeighborhood = '') {
        if(empty($listCityState)){
            return '';
        }
        $cities = array();
        foreach($listCityState as $k => $cityItem){
            if(!empty($cityItem)){
                $cities[$k] = $cityItem;
            }
        }
        $neighborhoodData = array();
        if(sizeof($cities) > 2){
            foreach($cities as $k => $cityItem){

                if ($cityItem && is_array($cityItem)){
                    foreach ( $cityItem as $city ){
                        if(!empty($selectedCity) && $selectedCity ==  $city){
                            $neighborhoodData = APL_Lib_Territory_Neighborhood::getNeighborhoodByCity($city, $k);
                        }
                    }
                }
            }
        }else{
            foreach($cities as $k => $cityItem){

                if ($cityItem && is_array($cityItem)){
                    foreach ( $cityItem as $city ){
                        if(!empty($selectedCity) && $city ==  $selectedCity){
                            $neighborhoodData = APL_Lib_Territory_Neighborhood::getNeighborhoodByCity($city, $k);
                        }
                    }
                }
            }
        }
        $optionRender = '';
        if (!empty($neighborhoodData)) {
            $optionRender = '<option value="">' . __('Select Neighborhood', 'apollo') . '</option>';
            foreach ($neighborhoodData as $key => $value) {
                if ($key == $selectedNeighborhood) {
                    $optionRender .= '<option value="'.$key.'" selected >'.$value.'</option>';
                } else {
                    $optionRender .= '<option value="'.$key.'">'.$value.'</option>';
                }
            }
        }
        return $optionRender;
    }

    /**
     * get by cities code
     * @param $city_name
     * @param string $state
     * @return array
     */
    public static function getNeighborhoodByCity( $city_name, $state= '' ) {
        $result = array();
        $neighborhood = self::getByParentCode($city_name, $state );
        if ($neighborhood) {
            foreach ( $neighborhood as $item ) {
                $result[$item['id']] = $item['name'];
            }
        }
        return $result;
    }

    /**
     * Get data for datatable
     * @param array $data
     * @return array
     */
    public static function getDataToDatatables($data = array()) {
        global $wpdb;
        $table = $wpdb->prefix . Apollo_Tables::_APL_NEIGHBORHOOD;
        $cityTable = self::getTerritoryTable();
        $sqlData = " FROM $table n INNER JOIN $cityTable c ON n.parent_id=c.id WHERE 1=1 ";
        if (!empty($data['search'])) {
            $sqlData .= $wpdb->prepare(" AND n.name LIKE '%s' ", "%". $data['search'] ."%");
        }
        if (!empty($data['parent_id']) && $data['parent_id'] != 0) {
            $sqlData .= $wpdb->prepare(" AND parent_id=%d ", intval($data['parent_id']));
        }

        $arr_city = self::getCityIdsByTerritory();
        if (!empty($arr_city)) {
            $sqlData .= " AND parent_id IN ( " . implode(',', $arr_city) ." )";
        } else {
            return array(
                'total' => 0,
                'data' => array()
            );
        }

        $sqlTotal = "SELECT count(*) AS total " . $sqlData;

        if ($data['order_column']) {
            $sqlData .= " ORDER BY ".$data['order_column']." " . $data['sort_order'] . " ";
        } else {
            $sqlData .= " ORDER BY n.id DESC ";
        }

        if (isset($data['limit'])) {
            $sqlData .= $wpdb->prepare(" LIMIT %d, %d", intval($data['limit']), intval($data['offset']));
        }
        $sqlPage = "SELECT n.id, n.name, c.code, c.id as city_id" . $sqlData ;
        $resultTotal = $wpdb->get_col($sqlTotal);
        $resultPage = $wpdb->get_results($sqlPage, ARRAY_A);
        $dataRender = array();
        if ($resultPage) {
            foreach ($resultPage as $item) {
                $dataRender[] = array(
                    'name' => $item['name'],
                    'code' => $item['code'],
                    'actions' => array(
                        'id' => $item['id'],
                        'name' => $item['name'],
                        'code' => $item['code'],
                        'city_id' => $item['city_id']
                    )
                );
            }
        }
        return array(
            'total' => $resultTotal ? $resultTotal[0] : 0,
            'data' => $dataRender
        );
    }

    /**
     * #13856
     * Get data for city select box
     * @param bool $default_option
     * @return array
     */
    public static function getCityByTerritoryV1( $default_option = true ){
        $_arr = of_get_option(Apollo_DB_Schema::_TERR_DATA);
        $array_state = Apollo_App::getListState();
        // Get cities for specific state
        $stateCities = array();
        if ($default_option) {
            $stateCities[''] = __( 'Select City', 'apollo');
        }
        if ( $_arr ) {

            foreach($_arr as $key => $val) {
                if (!$val || !in_array($key, $array_state)) continue;
                $cities = self::getCitiesByState(array_keys($val), $key);
                $stateCities[$key] = $cities[$key];
            }
        }

        return $stateCities;

    }

    /**
     * Get all city in territory options
     * @return array
     */
    private static function getCityIdsByTerritory() {
        $_arr = of_get_option(Apollo_DB_Schema::_TERR_DATA);
        $array_state = Apollo_App::getListState();
        $array_city = array();
        if ($_arr) {
            foreach ($_arr as $key => $val) {
                if (!$val || !in_array($key, $array_state)) {
                    continue;
                }
                $array_city = array_merge($array_city, self::getCityIds(array_keys($val), $key));
            }
        }
        return  $array_city;
    }

    /**
     * Get city id by city code
     * @param array $cities
     * @param $state
     * @return array
     */
    private static function getCityIds($cities = array(), $state) {
        $result = array();
        if (empty($cities) || empty($state)) {
            return $result;
        }
        global $wpdb;
        $state_id = self::getStateIdByCode($state);
        if ($state_id) {
            $list_city = "'" . implode("','", $cities) . "'";
            $sql = "SELECT id FROM ". self::getTerritoryTable() ." WHERE code IN (  $list_city ) AND parent=$state_id AND type='city' ORDER BY code";
            $city_info = $wpdb->get_results($sql, ARRAY_A);
            if ($city_info) {
                foreach ($city_info as $item) {
                    array_push($result, $item['id']);
                }
            }
        }
        return $result;
    }

    /***
     * Render city checkbox in territory option
     * @param array $listCityState
     * @return string
     */
    public static function renderCities( $listCityState = array() ) {
        if(empty($listCityState)){
            return '';
        }
        $cities = array();
        foreach($listCityState as $k => $cityItem){
            if(!empty($cityItem)){
                $cities[$k] = $cityItem;
            }
        }
        $result = '';
        if(sizeof($cities) > 2){
            foreach($cities as $k => $cityItem){
                $result .= '<optgroup ' . ($k ? 'label="'.$k.'"' : '') . '>';
                if (!$k){
                    $result .= '<option value="0">' . $cityItem . '</option>';
                }

                if ($cityItem && is_array($cityItem)){
                    foreach ( $cityItem as $id => $city ){
                        $result .= '<option  value="' . $id . '">' . $city .'</option>';
                    }
                }
                $result .= '</optgroup>';
            }
        } else {
            foreach($cities as $k => $cityItem){
                if (!$k){
                    $result .= '<option value="0">' . $cityItem . '</option>';
                }

                if ($cityItem && is_array($cityItem)){
                    foreach ( $cityItem as $id => $city ){
                        $result .= '<option value="' . $id . '">' . $city . '</option>';
                    }
                }
            }
        }
        return $result;
    }

    /**
     * #13856
     * get cities by state code
     * @param array $cities
     * @param $state
     * @return array|null|object
     */
    private static function getCitiesByState( $cities = array(), $state) {
        $result = array();
        if (empty($cities) || empty($state)) {
            return $result;
        }
        global $wpdb;
        $table = self::getTerritoryTable();
        $state_id = self::getStateIdByCode($state);
        if (($state_id)) {
            $list_city = "'" . implode("','", $cities) . "'";
            $sql = "SELECT id, code FROM $table WHERE code IN (  $list_city ) AND parent=$state_id ORDER BY code";
            $city_info = $wpdb->get_results($sql, ARRAY_A);
            if ($city_info) {
                $city_arr = array();
                foreach ($city_info as $item) {
                    $city_arr[$item['id']] = $item['code'];
                }
                $result[$state] = $city_arr;
            }
        }
        return $result;

    }

    private static function getStateIdByCode ( $state ) {
        if (empty($state)) {
            return '';
        }
        global $wpdb;
        $table = self::getTerritoryTable();
        $sql = "SELECT id FROM $table WHERE code=%s AND type='state' ";
        $state_id = $wpdb->get_col($wpdb->prepare($sql, $state));
        if (!empty($state_id)) {
            return $state_id[0];
        } else {
            return '';
        }
    }

    public static function getNameById( $id ) {
        global $wpdb;
        $table = $wpdb->prefix . Apollo_Tables::_APL_NEIGHBORHOOD;
        $sql_query = "SELECT name FROM $table WHERE id=%d ";
        $result = $wpdb->get_col($wpdb->prepare($sql_query, intval($id)));
        if (!empty($result)) {
            return $result[0];
        } else {
            return '';
        }
    }
}