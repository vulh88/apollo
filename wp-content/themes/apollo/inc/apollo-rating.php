<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}


class Apollo_Rating {

    public static function updateRating($eid, $type)
    {
        global $wpdb;
        $tblname = $wpdb->{Apollo_Tables::_APL_EVENT_RATING};

        $sql = "insert into {$tblname} (item_id, item_type, count) values ";
        $arrP = array(
            $eid,
            $type,
            1
        );

        $sp = '(' . implode(", ",array_fill(0, count($arrP), '%s')) .')';
        $sql .= $sp .' on duplicate key update count = count + 1 ;';

        $r = $wpdb->query($wpdb->prepare($sql, $arrP));

        return $r;
    }

    public static function getRating($eid, $type)
    {
        global $wpdb;
        $tblname = $wpdb->{Apollo_Tables::_APL_EVENT_RATING};

        $sql = "select count from $tblname where item_id = '%s' and item_type = '%s' limit 1";

        return $wpdb->get_var($wpdb->prepare($sql, array(
                    $eid, $type
                )));
    }

    public static function getRatings($arr_uids)
    {
        global $wpdb;
        $tblname = $wpdb->{Apollo_Tables::_APL_EVENT_RATING};

        $where= "(" .implode(" OR ", array_fill(0, count($arr_uids), "item_id = '%s' and item_type = '%s'")) . ")";
        $arrP = array();
        foreach($arr_uids as $_ => $v) {
            $_arr = explode(":", $v);
            $id = intval($_arr[0]);
            $type = sanitize_text_field($_arr[1]);

            $arrP = array_merge($arrP, array($id, $type));
        }

        $sql = "select item_id as i, item_type as t, count as c from $tblname where $where";

        return $wpdb->get_results($wpdb->prepare($sql, $arrP));
    }
}