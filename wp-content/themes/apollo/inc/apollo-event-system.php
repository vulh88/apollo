<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class Apollo_Event_System {

    const  ACTIVIY = 'activity';
    const  VISITEVENT = 'visit';

    protected static $_callback;

    public static function registerCallback($eventName, $callback) {
        if(!is_callable($callback)) {
            throw new Exception("Call back function is invalid!");
        }

        self::$_callback[$eventName][] = $callback;
    }

    public static function trigger($eventName, $data) {
        if(!isset(self::$_callback[$eventName])) {
            self::$_callback[$eventName] = array(
                array(__CLASS__, 'defaultCallback')
            );
        }

        foreach(self::$_callback[$eventName] as $callback) {
            call_user_func_array($callback, array($data));
        }

    }

    // Currently we don't have any log about information.
    //So just put raw information in one place.

    public static function defaultCallback($data)
    {
//        $blog = get_blog_details($GLOBALS['blog_id']);
//
//        $logfile = WP_CONTENT_DIR.'/uploads/sites/'.$blog->domain.'/common.log';
//
//        $qcontent = str_pad('Event name: ',20, ' ') .'"'. str_pad($data['event_name'], 40, ' ') . '"' . " fired!      with data: \n";
//        $qcontent .= print_r($data, true) . "\n";
//
//        @file_put_contents($logfile, $qcontent, FILE_APPEND);
    }
}