<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * This filter will effect all system
 *
 * Class Apollo_Filter
 */
class Apollo_Filter {
    public function __construct() {

        $arr_filter = array(
            'lostpassword_url'          => array( $this, 'lostpassword_url' ),
            'show_admin_bar'            => array( $this, 'show_admin_bar' ),
            'single_template'           => array( $this, 'handle_single_template' ),
            'site_details'              => array($this, 'apollo_blog_details')
            //'pre_option_category_base'  => array( $this, 'change_category_base' )
        );


        //Increase max upload file size
        add_filter('upload_size_limit', array($this, 'upload_size_limit'));

        foreach($arr_filter as $filter_name => $handler) {
            add_filter($filter_name, $handler, 10, 2);
        }
        
    }

    /**
     * Override upload max size
    */
    public function upload_size_limit() {
        return Apollo_App::maxUploadFileSize('b', is_admin());
    }

    public function show_admin_bar($show_admin_bar)
    {
        if($this->_isFrontEnd()) {
            if($this->_isHaveAdminCapa()) {
                return true;
            }
            return false;
        }

        return $show_admin_bar;
    }

    private function _isHaveAdminCapa()
    {
        $sadmin_role = 'administrator';
        $current_user = wp_get_current_user();

        if($current_user instanceof WP_User) {
            return in_array($sadmin_role, $current_user->roles);
        }

        return false;
    }

    private function _isFrontEnd() {
        return !is_admin();
    }


    public function lostpassword_url($lostpassword_url, $redirect ){

        if(is_admin()) {
            return $lostpassword_url;
        }

        // return $lostpassword_url;
        return Apollo_App::getFELostPasswordUrl();
    }
    
    /**
     * Handle the single post type template
     * @param string $single_template asb Template dir
     * @return string asb template dir
     */
    public function handle_single_template( $single_template ) {
        global $post;
       
        if ( $post->post_type != 'event' && Apollo_App::check_avaiable_module( $post->post_type ) ) {
            $single_template = dirname(__FILE__).'/../templates/content-single/single-post-type.php';
        }
        
        return $single_template;
    }
    
    /**
     * Try to change the category taxonomy to another name, due to 
     * we use 'category' keyword as slug of event type
     * @return string new name of default category taxonomy
     */
    function change_category_base( $value ) {
   
        // let's change our category slug to rantings
        // this will change the permalink to http://wpdreamer.com/rantings/wordpress/
        return 'catalogue';
     }

    public function apollo_blog_details($details) {

        Apollo_App::setBlogDetail($details->blog_id, $details);
        return $details;
    }
}

new Apollo_Filter();

