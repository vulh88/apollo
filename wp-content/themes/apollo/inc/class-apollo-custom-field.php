<?php

class Apollo_Custom_Field {
    
    public static function types() {
        return array(
            'text'              => __( 'Text', 'apollo' ),
            'textarea'          => __( 'Textarea', 'apollo' ),
            'select'            => __( 'Dropdown', 'apollo' ),
            'checkbox'          => __( 'Checkbox', 'apollo' ),
            'multi_checkbox'    => __( 'Multi Checkbox', 'apollo' ),
            'radio'             => __( 'Radio', 'apollo' ),
            'wysiwyg'           => __( 'Wysiwyg Editor', 'apollo' ),
            'gallery'           => __( 'Gallery', 'apollo' ),
        );
    }
    
    public static function get_group_fields( $post_type ) {
        $apl_query = new Apl_Query( Apollo_Tables::_APL_CUSTOM_FIELD );
        $groups = $apl_query->get_where( " post_type = '$post_type' AND parent IS NULL ", '*', '' );
        $results = array();
        if ( $groups ) {
            foreach( $groups as $k =>$group ) {
                $results[$k]['group'] = $group;
                $results[$k]['fields'] = self::get_fields($group->id);
            }
        }
        return $results;
    }
    
    public static function get_fields($group_id) {
        $apl_query = new Apl_Query( Apollo_Tables::_APL_CUSTOM_FIELD );
        return $apl_query->get_where( "parent = $group_id", '*', '', ' ORDER BY cf_order ASC' );
    }
    
    public static function get_required_fields_by_type($post_type) {
        $apl_query = new Apl_Query( Apollo_Tables::_APL_CUSTOM_FIELD );
        return $apl_query->get_where( "parent IS NOT NULL AND cf_type <> 'checkbox' AND post_type = '$post_type' AND ( required = 1 OR meta_data LIKE '%validation%' ) ", '*', '', ' ORDER BY label ASC' );
    }
    
    public static function get_all_post_fields($post_type) {
        $apl_query = new Apl_Query( Apollo_Tables::_APL_CUSTOM_FIELD );
        return $apl_query->get_where( "parent IS NOT NULL AND post_type = '$post_type'", '*', '', ' ORDER BY label ASC' );
    }
    
    public static function get_field_by_location( $post_type, $location = 'rsb' ) {
        $apl_query = new Apl_Query( Apollo_Tables::_APL_CUSTOM_FIELD );
        return $apl_query->get_where( "location LIKE '%".$location."%' AND parent IS NOT NULL AND post_type = '$post_type'", '*', '', ' ORDER BY label ASC' );
    }
    
    public static function get_fields_by_type( $post_type, $type = 'wysiwyg' ) {
        $apl_query = new Apl_Query( Apollo_Tables::_APL_CUSTOM_FIELD );
        return $apl_query->get_where( "cf_type='".$type."' AND parent IS NOT NULL AND post_type = '$post_type'", '*', '', ' ORDER BY label ASC' );
    }
    
    public static function get_field_by_name( $name, $post_type ) {
        $apl_query = new Apl_Query( Apollo_Tables::_APL_CUSTOM_FIELD );
        return $apl_query->get_row( "name ='$name' AND parent IS NOT NULL AND post_type = '$post_type'", '*', '', ' ORDER BY label ASC' );
    }

    public static function get_field_parent_by_name( $name, $post_type ) {
        $apl_query = new Apl_Query( Apollo_Tables::_APL_CUSTOM_FIELD );
        return $apl_query->get_row( "name = '$name' AND post_type = '$post_type'", '*', '', ' ORDER BY label ASC' );
    }
    
    public static function get_field_by_id( $id, $post_type ) {
        $apl_query = new Apl_Query( Apollo_Tables::_APL_CUSTOM_FIELD );
        return $apl_query->get_row( "id ='$id' AND parent IS NOT NULL AND post_type = '$post_type'", '*', '', ' ORDER BY label ASC' );
    }
    
    public static function get_choice( $field ) {
        $meta = maybe_unserialize( $field->meta_data );
  
        $results = array();
        
        if ( isset( $meta['data_source'] ) && $meta['data_source'] ) {
            
            // Get terms
            if ( $meta['data_source'] == 'school' || $meta['data_source'] == 'district' ) {
                $terms = get_terms('district-school', array( 'hide_empty' => false ));
                
                foreach( $terms as $term ) {
                    if ( ($meta['data_source'] == 'school' && ! $term->parent) 
                        || ( $meta['data_source'] == 'district' && $term->parent ) ) continue;
                    $results[$term->term_id] = $term->name;
                }
                
                return $results;
            }
            
            // Get posts
            $data = get_posts(array(
                'post_type'         => $meta['data_source'],
                'posts_per_page'    => -1,
                'order'             => 'asc',
                'orderby'           => 'post_title',
                'post_status'       => 'publish'
            ));
            if ( $data ) {
                foreach( $data as $d ) {
                    $results[$d->ID] = $d->post_title;
                }
            }
            return $results;
        }
        //TriLM fix admin choice error in admin
        $metaChoice = isset($meta['choice'])?$meta['choice']:'';
        $choices = array_filter( explode( '<br />' , nl2br( $metaChoice ) ) );
       
        if ( $choices ) {
            foreach( $choices as $choice ) {
                $_arr_choice = explode( ' : ', $choice);
                $key = trim($_arr_choice[0]);
                $results[$key] =count($_arr_choice) == 2 ?  trim( $_arr_choice[1] ) : $key;
            }
        }
        
        return $results;
    }

    public static function get_alternate_choice( $meta ) {
        $results = array();
        //TriLM fix admin choice error in admin
        $metaChoice = isset($meta['alternate_values'])?$meta['alternate_values']:'';
        $choices = array_filter( explode( '<br />' , nl2br( $metaChoice ) ) );

        if ( $choices ) {
            foreach( $choices as $choice ) {
                $_arr_choice = explode( ' : ', $choice);
                $key = trim($_arr_choice[0]);
                $results[$key] =count($_arr_choice) == 2 ?  trim( $_arr_choice[1] ) : $key;
            }
        }

        return $results;
    }

    /**
     * @Ticket #14105 - Get all Alternate fields.
     * @return array
     */
    public static function getAlternateFields ( ) {
        $result = array();
        $groupFields = Apollo_Custom_Field::get_group_fields( Apollo_DB_Schema::_EVENT_PT );
        $synFields = array('multi_checkbox', 'select');
        if (is_array($groupFields) && !empty($groupFields)) {
            foreach ($groupFields as $group ) {
                if (!empty($group['fields'])) {
                    foreach ($group['fields'] as $field) {
                        if (in_array($field->cf_type, $synFields)) {
                            $metaData = maybe_unserialize($field->meta_data);
                            if (!empty($metaData['alternate_values'])) {
                                $alternateField = self::compareAlternateFields($field, $metaData);
                                $result[$field->name] = array(
                                    'syn_field_name' => $metaData['syn_field_name'],
                                    'alternate_field' => $alternateField
                                );
                            }
                        }
                    }
                }
            }
        }
        return $result;
    }

    /**
     * @Ticket #14105 - Compare Alternate field.
     * @param $field
     * @param $metaData
     * @return array
     */
    public static function compareAlternateFields($field, $metaData) {
        if (empty($field) || empty($metaData)) {
            return array();
        }
        $result = array();
        $choiceData = self::get_choice($field);
        $alternateData = self::get_alternate_choice($metaData);
        if (!empty($choiceData)) {
            foreach ($choiceData as $key => $item) {
                if (!empty($alternateData[$key]) || (isset($alternateData[$key]) && $alternateData[$key] == 0)) {
                    $result[$key] = $alternateData[$key];
                }
            }
        }
        return $result;
    }

    public static function get_validation( $field ) {
        $meta = maybe_unserialize( $field->meta_data );
      
        if ( ! isset($meta['validation']) ) return false;
        
        return $meta['validation'];
    }
    
    public static function get_character_limit( $field ) {
        $meta = maybe_unserialize( $field->meta_data );
      
        if ( ! isset($meta['character_limit']) ) return false;
        
        return $meta['character_limit'];
    }
    
    public static function get_default_value_choice( $meta ) {
        if ( ! isset( $meta['default_value'] ) ) return false;
        $default_values = array_filter( explode( '<br />' , nl2br( $meta['default_value'] ) ) );
        $results = array();
        foreach( $default_values as $df ) {
            $v = trim($df);
            $results[$v] = $v;
        }
        return $results;
    }
    
    public static function get_row( $field ) {
        $meta = maybe_unserialize( $field->meta_data );
        return isset( $meta['row'] )? $meta['row'] : FALSE;
    }
    
    public static function can_display( $field, $l = 'ff' ) {
        $locations = maybe_unserialize( $field->location );
        return is_array( $locations ) && in_array( $l , $locations );
    }
    
    public static function has_explain_field( $field ) {
        $meta = maybe_unserialize( $field->meta_data );
      
        if ( ! isset($meta['explain']) ) return false;
        
        return $meta['explain'];
    }
    
    public static function get_refer_to( $field ) {
        $meta = maybe_unserialize( $field->meta_data );
      
        if ( ! isset($meta['refer_to']) ) return false;
        
        return $meta['refer_to'];
    }
    
    public static function get_data_source( $field ) {
        
        if ( ! $field ) return false;
        
        $meta = maybe_unserialize( $field->meta_data );
      
        if ( ! isset($meta['data_source']) ) return false;
        
        return $meta['data_source'];
    }
    
    public static function get_data_manual( $field ) {
        
        if ( ! $field ) return false;
        
        $meta = maybe_unserialize( $field->meta_data );

        if ( ! isset($meta['data_manual']) ) return false;
        
        return $meta['data_manual'] ? $meta['data_manual'] : 'choice';
    }
    
    public static function has_other_choice( $field ) {
        $meta = maybe_unserialize( $field->meta_data );
        
        if ( ! isset($meta['other_choice']) ) return false;
        
        return $meta['other_choice'];
    }
    
    public static function get_group_meta($group) {
        if (!$group) return false;
        $meta = maybe_unserialize( $group->meta_data );
        return isset($meta['group']) ? $meta['group'] : false;
    }
    
    public static function get_prepend_group($group, $default) {
        $meta = self::get_group_meta($group);
        return $meta && isset($meta['prepend']) && $meta['prepend'] ? htmlspecialchars_decode($meta['prepend']) : $default;
    }
    
    public static function get_append_group($group, $default) {
        $meta = self::get_group_meta($group);
        return $meta && isset($meta['append']) && $meta['append'] ? htmlspecialchars_decode($meta['append']) : $default;
    }
    
    public static function is_none_bg_group($group) {
        $meta = self::get_group_meta($group);
        return $meta && isset($meta['none_bg']) ? $meta['none_bg'] : false;
    }
    
    public static function get_display_style($field) {
        $meta = maybe_unserialize( $field->meta_data );
        return isset( $meta['display_style'] )? $meta['display_style'] : FALSE;
    }
    
    public static function get_desc($field) {
        $meta = maybe_unserialize( $field->meta_data );
        return isset( $meta['desc'] )? $meta['desc'] : FALSE;
    }
    
    public static function get_action_refer_to( $sourceField, $post_type ) {
        
        // Get refer to data source
        $referToName        = Apollo_Custom_Field::get_refer_to($sourceField);
        $referField         = Apollo_Custom_Field::get_field_by_name( $referToName, $post_type );
        $referDataSource    = Apollo_Custom_Field::get_data_source($referField);
        
        // Get current data source
        $curDataSource = Apollo_Custom_Field::get_data_source($sourceField);
        
        return $referDataSource && $curDataSource ? sprintf( 'apollo_get_%ss_%s', $referDataSource, $curDataSource ) : '';
        
    }
}