<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class APL {
    public static function renderTemplateWithData($arrdata, $templates, $is_single = false)
    {
        ob_start();

        include $templates;

        return ob_get_clean();
    }

    /**
     * @expected string
     */
    public static function dateUnionDateShort($start_date = NULL, $end_date = NULL)
    {
        $show_date_type = 'none';

        $only_date =  $start_date;
        $unix_start = null;
        $unix_end =  null;

        if((!empty($start_date) && $start_date === $end_date)
            || (!empty($start_date) && empty($end_date))
            || (empty($start_date) && !empty($end_date))

        ) {
            $show_date_type = 'one';
            $start_date = !empty($start_date) ? $start_date : $end_date;
            $unix_start = strtotime($start_date);
        }
        elseif(!empty($start_date) && ($start_date !== $end_date) ) {

            /*same month and year still be one*/
            $unix_start = strtotime($start_date);
            $unix_end = strtotime($end_date);
            $show_date_type = 'two';
        }

        $_arr_show_date  = array(
            'type' => $show_date_type,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'unix_start' => $unix_start,
            'unix_end' => $unix_end,
        );

        $return = '';

        if($_arr_show_date['type'] !== 'none') {

           if($_arr_show_date['type'] === "two") {
               if(date("Y", $unix_start) === date('Y', $unix_end)) {
                   $return = Apollo_App::apl_date("M d", $unix_start).' - '.Apollo_App::apl_date("M d", $unix_end).', '.date('Y', $unix_start);
               }
               else {
                   $return = Apollo_App::apl_date("M d, Y", $unix_start).' - '.Apollo_App::apl_date("M d, Y", $unix_end);
               }

           }
           else {
               $return = Apollo_App::apl_date('M d, Y', $unix_start);
           }
        }

        return $return;
    }

    public static function convertToUserTime($time_string){
        $string = $time_string;
        $tz = get_option( 'timezone_string' );
        $timeformat = get_option('date_format').' '.get_option('time_format');

        if ( $tz ) {
            $datetime = new DateTime($string);

            $datetime = $datetime->setTimezone(new DateTimeZone($tz));
            $return = $datetime->format($timeformat);

        } else {
            if ( preg_match( '#([0-9]{1,4})-([0-9]{1,2})-([0-9]{1,2}) ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})#', $string, $matches ) ) {
                $string_time = gmmktime( $matches[4], $matches[5], $matches[6], $matches[2], $matches[3], $matches[1] );
                $return = Apollo_App::apl_date( $timeformat, $string_time + get_option( 'gmt_offset' ) * HOUR_IN_SECONDS );
            }
            else {
                $return = Apollo_App::apl_date($timeformat, 0);
            }
        
        }

        return $return;
    }
}