<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class Apollo_Activity_System  {

    /**
     *
    You bookmark on abc.com
    you share something at facebook.com  on event abc[link]
    you twitter soemthing at twitter.com on event abc[link]
    you click buy ticket of event abc[link]
    you click discount of event abc[link]
    you comment 'abc' on event abc[link]
     */
    const LIKE = 'like';
    const BOOKMARK = 'bookmark';
    const UNBOOKMARK = 'unbookmark';
    const SHARE_FB = 'share-fb';
    const SHARE_LK = 'share-lk';
    const SHARE_PIN = 'share-pin';
    const TWEET = 'tweet';
    const SHARE_INSTA = 'share-instagram';
    const CLICK_DISCOUNT = 'click-discount';
    const CLICK_BUY_TICKET = 'click-buy-ticket';
    const CLICK_OFFICIAL_WEBSITE = 'click-official-website';
    const COMMENT = 'comment';

    public static function parseMsg($data)
    {
        $arrMsgByAction = array(
            /* Boomark */
            self::BOOKMARK => __('You bookmark %s %s on %s', 'apollo'), //YOu bm event abc on November 19, 2014
            self::UNBOOKMARK => __('You unbookmark %s %s on %s', 'apollo'), //YOu unbm event abc on November 19, 2014

            /* Share */
            self::SHARE_FB => __('You share %s %s on Facebook on %s', 'apollo'), //YOu like event abc on  on November 19, 2014
            self::SHARE_LK => __('You share %s %s on Linked In on %s', 'apollo'), //YOu like event abc on November 19, 2014
            self::TWEET => __('You tweet %s %s on Twitter on %s', 'apollo'), //YOu tweet event abc on November 19, 2014
            self::SHARE_INSTA => __('You share %s %s on Instagram on %s', 'apollo'), //YOu instagram event abc on November 19, 2014
            self::SHARE_PIN => __('You share %s %s on Pinterest on %s', 'apollo'),

            /* click */
            self::CLICK_DISCOUNT => __('You click discount of %s %s on %s', 'apollo'), //YOu click discount url of event abc on November 19, 2014
            self::CLICK_BUY_TICKET => __('You click buy ticket of %s %s on %s', 'apollo'), //YOu click buy ticket of event abc on November 19, 2014

            /* Comment */
            self::COMMENT => __('You comment "%s" on %s %s on %s', 'apollo'), //YOu comment "abc" on event abc on November 19, 2014

            /* No have yet */
            self::LIKE => __('You like %s %s on %s', 'apollo'), //YOu like event abc on November 19, 2014
        );

        $activity = $data['activity'];
        $main_event = 'post';

        if($activity === self::COMMENT) {
            $main_event = 'comment';
        }

        /* POST */
        if($main_event === 'post') {
            $item_type = $data['item_type'];
            $main_item_a = '<a class="tt" href="' . $data['url'] . '">' . $data['title'] . '</a>';
            $timestamp = $data['timestamp']; // Process timezone later
        }

        /* COMMENT */
        elseif($main_event === 'comment') {

            $exdata = unserialize($data['exdata']);

            $content   = $exdata['content'];
            $item_type = $exdata['main_type'];
            $main_item_a = '<a class="tt" href="' . $exdata['main_url'].'#event_comment_block' . '">' . $exdata['main_title'] . '</a>';
            $timestamp = $data['timestamp']; // Process timezone later
        }
        $timestamp = APL::convertToUserTime($timestamp);

        switch($activity) {
            case self::BOOKMARK:
            case self::UNBOOKMARK:
                return sprintf($arrMsgByAction[$activity], ucfirst($item_type) , $main_item_a, $timestamp);

            case self::CLICK_BUY_TICKET:
            case self::CLICK_DISCOUNT:
                return sprintf($arrMsgByAction[$activity], ucfirst($item_type) , $main_item_a, $timestamp);

            case self::SHARE_FB:
            case self::SHARE_LK:
            case self::TWEET:
            case self::SHARE_INSTA:
            case self::SHARE_PIN:
                return sprintf($arrMsgByAction[$activity], ucfirst($item_type) , $main_item_a, $timestamp);

            case self::COMMENT:
                return sprintf($arrMsgByAction[$activity], $content,  ucfirst($item_type) , $main_item_a, $timestamp);
        }
    }

    public static function getShareGroup()
    {
        return array(
            self::SHARE_FB,
            self::SHARE_LK,
            self::TWEET,
            self::SHARE_INSTA,
            self::SHARE_PIN,
        );
    }

    public static function getClickGroup()
    {
        return array(
            self::CLICK_BUY_TICKET,
            self::CLICK_DISCOUNT,
        );
    }

    /**
     * Tracking clicks for these 3 buttons
     *
     * @ticket: #11232
     * @return array
     */
    public static function getClickEventGroup()
    {
        return array(
            self::CLICK_OFFICIAL_WEBSITE,
            self::CLICK_BUY_TICKET,
            self::CLICK_DISCOUNT,
        );
    }
}