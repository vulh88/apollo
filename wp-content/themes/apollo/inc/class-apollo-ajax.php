<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
/**
 * Apollo Apollo_AJAX
 *
 * AJAX Event Handler
 *
 * @class 		Apollo_AJAX
 * @package		inc/Classes
 * @category	Class
 * @author 		vulh
 */
class Apollo_AJAX
{


    const MAX_IMPORTED_CSV_FILE_SIZE = 20; // MB

    /**
     * Hook into ajax events
     */
    public function __construct()
    {

        if (isset($_GET['action']) && $_GET['action'] == 'apollo_get_remote_data_to_select2_box') {
            add_filter('posts_search', array($this, 'search_by_title_only'), 500, 2);
        }

        // apollo => nopriv
        $ajax_events = array(
            'show_more_upcomming_org_event' => true,
            'category_show_more_other_event' => true,
            'add_org_in_event' => true,
            'add_venue_in_event' => true,
            'get_more_search' => true,
            'get_more_search_artist' => true,
            'get_more_search_author' => true,
            'get_more_search_org' => true,
            'get_more_search_business' => true,
            'get_more_search_event' => true,
            'get_more_search_program' => true,
            'get_more_search_educator' => true,
            'get_more_search_venue' => true,
            'get_more_search_classified' => true,
            'get_more_search_public_art' => true,
            'get_more_event_time' => true,
            'show_more_upcomming_artist_event' => true,
            'show_more_upcoming_org_event' => true,
            'show_more_associated_orgs' => true,
            'show_more_associated_venues' => true,
            'show_more_nearby_business_event' => true,
            'show_more_upcoming_venue_event' => true,
            'show_more_past_org_event' => true,
            'get_schools_district' => true,
            'get_programs_educator' => true,
            'show_more_past_user_event' => true,
            'show_more_org_classified' => true,
            'show_more_artist_public_art' => true,

            /* DASHBOARD ACCOUNT */
            'get_bookmark' => false,
            'remove_bookmark' => false,
            'set_bookmark_for_myfriend' => isset($_REQUEST['is_single']) && $_REQUEST['is_single'] == 'true',
            'get_user_activity' => false,

            /* DASHBOARD ORGANIZATION */
            'get_multiple_post_dashboard_page' => false,

            'log_click_activity' => true,
            'log_share_activity' => false,

            /* DASHBOARD VENUE - @Ticket #13856 */
            'get_neighborhood_by_city' => false,


            /* DASHBOARD EVENT */
            'check_process_upload_gallery' => false,

            /* GLOBAL SEARCH */
            'global_search_get_total_items' => true,

            'get_program_by_educator_id' => true,

            /* Add event */
            'is_add_tmp' => false,
            'add_cor_org' => false,

            // Autocomplete orgs
            'get_orgs' => false,
            'add_cor_org' => false,

            //create:
            'create_event_time' => false,
            'create_event_times' => false,
            //get:
            'get_all_event_time' => false,
            //delete:
            'delete_event_time' => false,
            'delete_event_times' => false,
            'delete_event_times_in_range' => false,
            //edit:
            'edit_event_time' => false,
            'edit_event_times' => false,
            'delete_all_event_in_cell' => false,
            'restore_event_time_cell' => false,
            'get_disable_cell_event' => false,
            'delete_all_disable_cell_event' => false,
            'update_additional_time' => false,
            'update_additional_time_content_in_step2b' => false,
            'set_cookie_protected_pass' => true,

            // rating system
            'update_rating' => true,
            'get_ratings' => true,

            // save coordinate by user session
            'save_coordinate' => false,

            /*reuse shortcode upload feature image and gallery*/
            'upload_and_drop' => false,
            'upload_and_drop_actually' => false,
            'upload_and_drop_remove' => false,

            'upload_gallery' => false,
            'remove_gallery' => false,
//            'save_artist' => false,

            // Upload pdf
            'upload_pdf_artist' => false,

            'load_edit_classified_html' => false,

            //org
//            'save_venue' => false,

            'init_captcha' => true,

            'get_public_art_around_you' => true,
            'get_all_public_art' => true,

            'get_cities_state' => true,
            'get_zipcodes_city' => true,

            'get_more_offer_dates_times' => true,

            // Import
            'import_save_event' => true,
            'import_save_event_for_later' => true,
            'load_event_import_data' => true,
            'import_event_by_csv_file' => true,

            // What near by
            'get_what_near_by' => true,

            // CHeck thumbnail url youtube video
            'check_thumbnail_youtube' => true,

            'import_get_taxes' => true,
            'get_home_featured_events_by_category' => true,

            'update_associated_user_event' => true,

            //TriLm - solr search
            'solr_search' => true,
            'get_remote_associate_data_to_select2_box' => true,
            'get_user_asscoc_list' => true,

            // Get more past events in venue detail
            'show_more_past_venue_events' => true,
            // Thienld: define function to handle ajax-requests from select2 remote-data
            'get_remote_data_to_select2_box' => true,
            'check_apply_remote_data_for_select2_box' => true,
            'syndication_api' => true,
            'get_refresh_public_art_map' => true,
            'system_log'    => true,

            'sendmail_tell_a_friend' => true,
            'get_video_thumbnail' => true,
            'agency_get_association_remote_data_to_select2_box' => true,
            'render_neighborhood_for_event_search_widget' => true,
            'get_event_for_map_view' => true
        );

        /* ThienLd : sql injection avoiding handler */
        $currentAJAXAction = '';
        if (isset($_GET['action'])) {
            $currentAJAXAction = $_GET['action'];
        } elseif (isset($_POST['action'])) {
            $currentAJAXAction = $_POST['action'];
        }
        if (!empty($currentAJAXAction) && in_array(str_replace('apollo_', '', $currentAJAXAction), array_keys($ajax_events))) {
            //Apollo_App::sqlInjectionHandler();
        }

        foreach ($ajax_events as $ajax_event => $nopriv) {
            add_action('wp_ajax_apollo_' . $ajax_event, array($this, $ajax_event));

            if ($nopriv)
                add_action('wp_ajax_nopriv_apollo_' . $ajax_event, array($this, $ajax_event));
        }

        // special case: add ajax action for this case because function save_org moved to Apollo_Submit_Form class
        add_action('wp_ajax_apollo_save_featured_and_gallery_image', array('Apollo_Submit_Form', 'save_featured_and_gallery_image'));

        // Thienld : hook filter admin_url to integrate with WPML - Multilinguage Plugin
        add_filter('admin_url', 'Apollo_App::handleAdminUrlIntegrateWPML', 99, 3);

        if (Apollo_App::hasWPLM()) {
            global $sitepress;
            if (method_exists($sitepress, 'switch_lang') && isset($_GET['lang']) && $_GET['lang'] !== $sitepress->get_default_language()) {
                $sitepress->switch_lang($_GET['lang'], true);
            }
        }
    }

    /**
     * System logs
     * @return json
     *
     */
    function system_log() {

        // Verify
        if ( ! isset( $_POST['nonce_field'] )  || ! wp_verify_nonce( $_POST['nonce_field'], 'system_log' ) ) {
            wp_send_json(array(
                'status' => 'Sorry, your nonce did not verify.'
            ));
        }

        // Valid data
        if (!($id = $_POST['id']) || !($postType = $_POST['post_type'])) {
            wp_send_json(array(
                'status' => 'Error'
            ));
        }

        $dataLogView = array(
            'item_id' => $id,
            'item_type' => $postType,
        );

        /* Allow log or not */
        $is_allow_log = true;
        $log_view_sn = Apollo_SESSION::LOG_VISIT. $id;

        if(isset($_SESSION['apollo'][$log_view_sn])){ // view time
            $last_time = $_SESSION['apollo'][$log_view_sn];
            if(time() - $last_time < MINUTE_IN_SECONDS) {
                $is_allow_log = false;
            }
        }

        if($is_allow_log) {
            Apollo_Event_System::trigger(Apollo_Event_System::VISITEVENT, $dataLogView);
            $_SESSION['apollo'][$log_view_sn] = time();

            wp_send_json(array(
                'status' => 'OK'
            ));
        }

        wp_send_json(array(
            'status' => 'No log'
        ));
    }

    /**
     * @Ticket #16073 - Get all programs by educator id
     */
    function get_program_by_educator_id() {
        $data = '<option value="">'.__('Select Program', 'apollo').'</option>';
        if (isset($_GET['edu_id'])) {
            $edu = get_educator( $_GET['edu_id'] );
            $listProgram = $edu->get_programs_does_not_expired();
            if ($listProgram) {
                foreach ($listProgram as $item) {
                    $pro = get_post( $item->prog_id );

                    if ( ! $pro || ($pro && $pro->post_status != 'publish') ) continue;
                    $data .= '<option value="'.$pro->ID.'">'.$pro->post_title.'</option>';
                }
            }
            wp_send_json(array(
                'data' => $data
            ));
        }
        wp_send_json(array(
            'data' => array(),
        ));
    }

    function get_event_for_map_view() {
        include_once APOLLO_INCLUDES_DIR . '/src/event/inc/class-event-search.php';

        $search_obj = new Apollo_Event_Page();
        $search_obj->search();



        /* CREATE NEW REQUEST URL */
        $page = $search_obj->get_current_page();
        $page++;

        $data = array();
        $results = $search_obj->get_results();

        /*@ticket #17348: 0002410: wpdev55 - Requirements part2 - [Event] Display event discount text*/
        $discountConfig = AplEventFunction::getConfigDiscountIcon();
        /*@ticket #17349 */
        $enableBlockIcon = of_get_option(Apollo_DB_Schema::_APOLLO_EVENT_ENABLE_CLOCK_ICON, true);
        $icon_fields = maybe_unserialize( get_option( Apollo_DB_Schema::_APL_EVENT_ICONS ) );

        if (!empty($results)) {
            foreach ($results as $item) {
                $event = get_event($item->ID);
                $venue = $event->getVenueEvent();
                $venueLat = '';
                $venueLng = '';

                ob_start();
                include APOLLO_TEMPLATES_DIR . '/events/listing-page/map-event-item.php';
                $eventHtml = ob_get_clean();

                if (!empty($venue)) {
                    $venueLat = $venue->__get(Apollo_DB_Schema::_VENUE_LATITUDE);
                    $venueLng = $venue->__get(Apollo_DB_Schema::_VENUE_LONGITUDE);
                }
                $data[] = array(
                    'eventHtml' => $eventHtml,
                    'venueLat' => $venueLat,
                    'venueLng' => $venueLng,
                    'venueName' => $venue->get_title()
                );
            }
        }
        if ($search_obj->get_current_page() == 1 && empty($results)) {
            $textPagination = __('We did not find any events near your location.', 'apollo');
        } else {

            $firstPage = $search_obj->get_current_page() == 1 || !$search_obj->get_current_page();
            $numberFrom =  $firstPage ? $search_obj->getOffset() : $search_obj->getAjaxOnePageOffset();

            if ($firstPage) {
                $pageSize = of_get_option(Apollo_DB_Schema::_APL_NUMBER_ITEMS_FIRST_PAGE, Apollo_Display_Config::APL_DEFAULT_NUMBER_ITEMS_FIRST_PAGE);
            }
            else {
                $pageSize = $search_obj->getPagesize();
            }


            $numberFrom++;

            $numberTo = $pageSize + $numberFrom  - 1;
            $numberTo = min($numberTo, $search_obj->getTotal());

            $textPagination = sprintf(__('%d - %d of %d events found', 'apollo'),$numberFrom, $numberTo, $search_obj->getTotal());
        }

        /* SEND DATA */
        wp_send_json(array(
            'page' => $page,
            'data' => $data,
            'iconSrc' => of_get_option(Apollo_DB_Schema::_GOOGLE_MAP_MARKER_ICON, '/wp-content/themes/apollo/assets/images/icon-location.png'),
            'have_more' => $search_obj->have_more_on_viewmore(),
            'total' => $search_obj->getTotal(),
            'textPagination' => $textPagination
        ));
    }

    /**
     * @Ticket #15646
     * Get total items by post type
     */
    function global_search_get_total_items() {
        require_once APOLLO_TEMPLATES_DIR . '/search/class-apollo-search.php';
        $globalSearch = new Apollo_Search();
        $arrayQS = $globalSearch::apolloGetAllQueryStringToArray($_SERVER['QUERY_STRING']);
        $keyword = isset($arrayQS['s']) ? $arrayQS['s'] : '';
        $currentPostType = isset($_GET['currentPostType']) ? $_GET['currentPostType'] : '';
        $globalSearch->_get_total_item_all_post_type($keyword, $currentPostType);
        wp_send_json(array(
            'data' => $globalSearch->arrObjTotal
        ));
    }

    function get_neighborhood_by_city() {
        $requiredNeighborhood = of_get_option(APL_Theme_Option_Site_Config_SubTab::_VENUE_ENABLE_NEIGHBORHOOD_REQUIREMENT, 0);
        $optionRender = APL_Lib_Territory_Neighborhood::renderNeighborhoodOptions($_POST['parent_code'], $_POST['state'], $requiredNeighborhood);
        wp_send_json(array(
            'status' => true,
            'data' => $optionRender
        ));
        exit;

    }

    function render_neighborhood_for_event_search_widget() {
        if (empty($_GET['city']) || empty($_GET['state'])) {
            wp_send_json(array(
                'data' => ''
            ));
        }
        $optionRender = APL_Lib_Territory_Neighborhood::renderNeighborhoodOptions($_GET['city'], $_GET['state'], false, $_GET['neighborhood']);
        wp_send_json(array(
            'data' => $optionRender
        ));
        exit;
    }


    function search_by_title_only( $search, &$wp_query ) {
    global $wpdb;
    if(empty($search)) {
            return $search; // skip processing - no search term in query
        }
        $q = $wp_query->query_vars;
        $n = !empty($q['exact']) ? '' : '%';
        $search =
        $searchand = '';
        foreach ((array)$q['search_terms'] as $term) {
            $term = esc_sql($wpdb->esc_like($term));
            $search .= "{$searchand}($wpdb->posts.post_title LIKE '{$n}{$term}{$n}')";
            $searchand = ' AND ';
        }
        if (!empty($search)) {
            $search = " AND ({$search}) ";
            if (!is_user_logged_in())
                $search .= " AND ($wpdb->posts.post_password = '') ";
        }
        return $search;
    }

    /**
     * @Ticket #15306 - Agency load more associated items (artist, venue, educator, organization).
     */
    public function agency_get_association_remote_data_to_select2_box() {
        global $wpdb;
        $agencyIds = ApolloAssociationFunction::getAssociatedModules(get_current_user_id(), Apollo_DB_Schema::_AGENCY_PT);

        // get all published artists
        switch ($_GET['post_type']) {
            case Apollo_DB_Schema::_EDUCATOR_PT :
                $apl_query = new Apl_Query($wpdb->prefix . Apollo_Tables::_APL_AGENCY_EDUCATOR);
                $artists   = $apl_query->get_where("agency_id IN (" . implode(', ', $agencyIds) . " )", "DISTINCT edu_id");
                break;
            case Apollo_DB_Schema::_ORGANIZATION_PT :
                $apl_query = new Apl_Query($wpdb->prefix . Apollo_Tables::_APL_AGENCY_ORG);
                $artists   = $apl_query->get_where("agency_id IN (" . implode(', ', $agencyIds) . " )", "DISTINCT org_id");
                break;
            case Apollo_DB_Schema::_VENUE_PT :
                $apl_query = new Apl_Query($wpdb->prefix . Apollo_Tables::_APL_AGENCY_VENUE);
                $artists   = $apl_query->get_where("agency_id IN (" . implode(', ', $agencyIds) . " )", "DISTINCT venue_id");
                break;
            case Apollo_DB_Schema::_ARTIST_PT :
                $apl_query = new Apl_Query($wpdb->prefix . Apollo_Tables::_APL_AGENCY_ARTIST);
                $artists   = $apl_query->get_where("agency_id IN (" . implode(', ', $agencyIds) . " )", "DISTINCT artist_id");
                break;
            default :
                $apl_query = new Apl_Query($wpdb->prefix . Apollo_Tables::_APL_AGENCY_ARTIST);
                $artists   = $apl_query->get_where("agency_id IN (" . implode(', ', $agencyIds) . " )", "DISTINCT artist_id");
                break;
        }

        $total = 0;
        $incompleteResults = true;
        $keyword = isset($_GET['q']) ? $_GET['q'] : '';

        $page = isset($_GET['page']) ? intval($_GET['page']) : 1;
        $postType = isset($_GET['post_type']) ? $_GET['post_type'] : '';

        $results = array();

        if ($page == 1) {
            $text = __('Select', 'apollo');
            if ($postType == Apollo_DB_Schema::_ORGANIZATION_PT) {
                $text = __('Select Organization', 'apollo');
            }
            else if($postType == Apollo_DB_Schema::_VENUE_PT) {
                $text = __('Select Venue', 'apollo');
            }
            else if($postType == Apollo_DB_Schema::_ARTIST_PT) {
                $text = __('Select Artist', 'apollo');
            }
            else if($postType == Apollo_DB_Schema::_EDUCATOR_PT) {
                $text = __('Select Educator', 'apollo');
            }
        }

        $postIDs = array();
        if ( !empty($artists) ) {
            foreach ($artists as $value) {
                if ($postType == Apollo_DB_Schema::_ORGANIZATION_PT) {
                    $postIDs[] = $value->org_id;
                }
                else if($postType == Apollo_DB_Schema::_VENUE_PT) {
                    $postIDs[] = $value->venue_id;
                }
                else if($postType == Apollo_DB_Schema::_ARTIST_PT) {
                    $postIDs[] = $value->artist_id;
                }
                else {
                    $postIDs[] = $value->edu_id;
                }
            }
        }
        $loadedData = Apollo_App::agencyGetItemBySearchKeyword(array(
            's' => $keyword,
            'page' => $page,
            'limit' => 15,
            'post_type' => $_GET['post_type'],
            'post_in'  => empty($postIDs) ? array(-1) : $postIDs
        ));

        $results[] = array(
            'id'    => '',
            'text'  => $text
        );
        if(!empty($loadedData['items'])){
            foreach($loadedData['items'] as $p) {
                $results[] = array(
                    'id' => $p->ID,
                    'text'  => $p->post_title,
                    'edit_link' => get_edit_post_link($p->ID, '')
                );
            }
            $total = $loadedData['total'];
            $incompleteResults = false;
        }

        wp_send_json(array(
            'items'     => $results,
            'total_count'   => $total,
            'incomplete_results' => $incompleteResults
        ));

    }

    /**
    ** Thienld: handle get data for select2 ajax-request remote data
     */
    public function check_apply_remote_data_for_select2_box(){
        $enabled = false;
        $selectedID = '';
        $selectedText = '';

        if(isset($_POST['selected_item']) && intval($_POST['selected_item']) > 0){
            $selectedItem = get_post(intval($_POST['selected_item']));
            if(!empty($selectedItem)){
                $selectedID = $selectedItem->ID;
                $selectedText = $selectedItem->post_title;
            }
        }

        wp_send_json(array(
            'enabled' => $enabled ? "TRUE" : "FALSE",
            'selected_id' => $selectedID,
            'selected_text' => $selectedText
            ));
    }

    public function get_remote_data_to_select2_box(){
        global $wpdb;
        $total = 0;
        $incompleteResults = true;
        $keyword = isset($_GET['q']) ? $_GET['q'] : '';

        //$wpdb->escape_by_ref($keyword); // Find out the way to search keyword with apostrophe point

        $page = isset($_GET['page']) ? intval($_GET['page']) : 1;
        $postType = isset($_GET['post_type']) ? $_GET['post_type'] : '';
        $loadedData = Apollo_App::getDDLItemsBySearchKeyword(array(
            's' => $keyword,
            'page' => $page,
            'limit' => 15,
            'post_type' => $postType
            ));

        $results = array();

        $text = __('Select', 'apollo');
        if ($postType == Apollo_DB_Schema::_ORGANIZATION_PT) {
                $text = $page == 1 ? !empty($_GET['default_option']) ? $_GET['default_option'] : __('Select Organization', 'apollo') : '';
        } else if($postType == Apollo_DB_Schema::_VENUE_PT) {
            $text = $page == 1 ? !empty($_GET['default_option']) ? $_GET['default_option'] : __('Select Venue', 'apollo') : '';
        } else if($postType == Apollo_DB_Schema::_EDUCATOR_PT) {
            $text = $page == 1 ? !empty($_GET['default_option']) ? $_GET['default_option'] : __('Select An Educator', 'apollo') : '';
        }


        if($text){
            $results[] = array(
                'id'    => 0,
                'text'  => $text
            );
        }


        if(!empty($loadedData['items'])){
            foreach($loadedData['items'] as $p) {
                $results[] = array(
                    'id' => $p->ID,
                    'text'  => $p->post_title,
                    'edit_link' => get_edit_post_link($p->ID, '')
                    );
            }
            $total = $loadedData['total'];
            $incompleteResults = false;
        }

        wp_send_json(array(
            'items'     => $results,
            'total_count'   => $total,
            'incomplete_results' => $incompleteResults
            ));
    }

    /**
     * @author Trilm
     */
    public function get_remote_associate_data_to_select2_box(){
        global $wpdb;
        $total = 0;
        $incompleteResults = true;
        $keyword = isset($_GET['q']) ? $_GET['q'] : '';

        //$wpdb->escape_by_ref($keyword); // Find out the way to search keyword with apostrophe point

        $page = isset($_GET['page']) ? intval($_GET['page']) : 1;
        $postType = isset($_GET['post_type']) ? $_GET['post_type'] : '';

        $results = array();

        if ($page == 1) {
            $text = __('Select', 'apollo');
            if ($postType == Apollo_DB_Schema::_ORGANIZATION_PT) {
                $text = __('Select Organization', 'apollo');
            }
            else if($postType == Apollo_DB_Schema::_VENUE_PT) {
                $text = __('Select Venue', 'apollo');
            }
            else if($postType == Apollo_DB_Schema::_ARTIST_PT) {
                $text = __('Select Artist', 'apollo');
            }
            else if($postType == Apollo_DB_Schema::_AGENCY_PT) {
                $text = __('Select Agency', 'apollo');
            }
        }
        $status = isset($_GET['post_status']) ? $_GET['post_status'] : 'publish';
        $status = explode(',', $status);
        /** @Ticket #14800 */
        $loadedData = Apollo_App::getDDLItemsBySearchKeyword(array(
            's' => $keyword,
            'page' => $page,
            'limit' => 15,
            'post_type' => $postType,
            'post_status' => $status
        ));

        $results[] = array(
            'id'    => '',
            'text'  => $text
            );
        if(!empty($loadedData['items'])){
            foreach($loadedData['items'] as $p) {
                $results[] = array(
                    'id' => $p->ID,
                    'text'  => $p->post_status == 'pending' ? ($p->post_title . ' (*)') : $p->post_title ,
                    'edit_link' => get_edit_post_link($p->ID, '')
                    );
            }
            $total = $loadedData['total'];
            $incompleteResults = false;
        }

        wp_send_json(array(
            'items'     => $results,
            'total_count'   => $total,
            'incomplete_results' => $incompleteResults
            ));
    }

    public function import_get_taxes() {

        $perPage = 15;
        $page = $_GET['page'];
        $offset = $page == 1? 0:($page-1)*$perPage;
        $s = $_GET['q'];
        $postType = $_GET['post_type'];

        $args = array(
            'post_type'         => $postType,
            'orderby'           => 'title',
            'order'             => 'ASC',
            'post_status'       => 'publish',
            'posts_per_page'    => $perPage,
            'offset'    => $offset,
            );

        if ($s) {
            $args['s'] = $s;
        }

        $listPost = query_posts( $args );
        global $wp_query;
        $total = $wp_query->found_posts;
        wp_reset_query();

        $results = array();
        foreach($listPost as $d) {
            $results[] = array(
                'id' => $d->ID,
                'text'  => $d->post_title
                );
        }

        echo json_encode(array(
            'items'     => $results,
            'total_count'   => $total,
            'hasMore' => ($page * $perPage) < $total
            ));
        die;
    }

    /**
     * Feature a event from admin
     */
    public function home_feature_event()
    {

        // check user can edit event
        $this->_can_edit_event();

        if (!check_admin_referer('apollo-feature-event')) {
            wp_die(__('You have taken too long. Please go back and retry.', 'apollo'));
        }

        $post_id = Apollo_App::clean_data_request($_REQUEST['event_id']);
        $featured = get_apollo_meta($post_id, Apollo_DB_Schema::_APOLLO_HOME_EVENT_FEATURE, true);
        update_apollo_meta($post_id, Apollo_DB_Schema::_APOLLO_HOME_EVENT_FEATURE, $featured === 'yes' ? 'no' : 'yes');
        wp_safe_redirect(remove_query_arg(array('trashed', 'untrashed', 'deleted', 'ids'), wp_get_referer()));

        die();
    }

    /**
     * Get featured events from specific category in Home page
     *
     * @ticket #10738
     */
    public function get_home_featured_events_by_category()
    {
        include_once APOLLO_TEMPLATES_DIR . '/events/list-events.php';
        $html                   = '';
        $featured_category      = $_REQUEST['featured_category'];
        $page                   = isset($_REQUEST['page']) ? max(1, intval($_REQUEST['page'])) : 1;
        $category_page_size     = of_get_option( Apollo_DB_Schema::_NUMBER_FEATURED_EVENTS, Apollo_Display_Config::PAGE_SIZE );
        $adapter_cat            = new List_Event_Adapter($page, $category_page_size);
        $adapter_cat->is_topten = false;
        $adapter_cat->get_home_featured_category_event($featured_category);

        if ( !$adapter_cat->isEmpty() ) {
            $html = $adapter_cat->getHtml();
        }

        if ( $adapter_cat->totalPost < $adapter_cat->pageSize ) {
            $adapter_cat->get_auto_fill_tab_upcomming_events($featured_category, $adapter_cat->pageSize - $adapter_cat->totalPost);
            if (!$adapter_cat->isEmpty()) {
                $html .= $adapter_cat->getHtml();
            }
        }

        if ( empty($html) ) {
            $html = sprintf('<span>%s</span>', __('Data not found!', 'apollo'));
        }

        echo json_encode(array( 'success' => true, 'html' => $html ));
        die();
    }

    /**
     * Check valid category
     *
     * @access private
     * @return bool
     */
    private function _valid_post_category($post_id, $cat_id)
    {

        // Check valid category
        $event = get_event(get_post($post_id));
        $categories = $event->get_categories();

        if (!$categories) return false;

        foreach ($categories as $cat):
            if ($cat->term_id == $cat_id) return true;
        endforeach;
        return false;
    }

    /**
     * Check user can edit event
     *
     * @access private
     * @return bool
     */
    private function _can_edit_event()
    {
        $post_id = isset($_REQUEST['event_id']) ? Apollo_App::clean_data_request($_REQUEST['event_id']) : '';
        if (!current_user_can('edit_events')
            || !$post_id
            || get_post_type($post_id) !== 'event'
            || !Apollo_App::is_avaiable_module('event')
            ) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'apollo'));
        return false;
    }
    return true;
}

    /**
     * Show more upcomming events association with its registered Org ID
     * @access public
     */
    public function show_more_upcomming_org_event()
    {

        include_once APOLLO_TEMPLATES_DIR . '/events/list-events.php';
        $page = isset($_REQUEST['page']) ? max(1, intval($_REQUEST['page'])) : 1;
        $org_id = isset($_REQUEST['org_id']) ? $_REQUEST['org_id'] : '';
        $current_event_id = isset($_REQUEST['current_event_id']) ? $_REQUEST['current_event_id'] : '';
        $adapter = new List_Event_Adapter($page, $this->get_pagesize_view_more());
        $page++;
        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_show_more_upcomming_org_event&page=' . $page . '&org_id=' . $org_id . '&current_event_id=' . $current_event_id),
            'html' => $adapter->get_html_org_upcomming_events($org_id, $current_event_id, true),
            'have_more' => $adapter->isShowMoreEvents(),
        ));
    }

    /**
     * Show more upcomming events association current user
     * @access public
     */
    public function show_more_upcomming_artist_event()
    {

        include_once APOLLO_TEMPLATES_DIR . '/events/list-events.php';
        $page = isset($_REQUEST['page']) ? max(1, intval($_REQUEST['page'])) : 1;
        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : '';

        $adapter = new List_Event_Adapter($page, $this->get_pagesize_view_more());
        $adapter->get_artist_upcomming_event($user_id);
        $page++;
        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_show_more_upcomming_artist_event&page=' . $page . '&user_id=' . $user_id),
            'html' => $adapter->render_html(APOLLO_TEMPLATES_DIR . '/events/_org-events.php'),
            'have_more' => $adapter->isShowMore(),

            ));
    }

    /**
     * Show more upcomming events association current user
     * @access public
     */
    public function show_more_past_user_event()
    {

        include_once APOLLO_TEMPLATES_DIR . '/events/list-events.php';
        $page = isset($_REQUEST['page']) ? max(1, intval($_REQUEST['page'])) : 1;
        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : '';

        $adapter = new List_Event_Adapter($page, $this->get_pagesize_view_more());
        $adapter->get_past_user_events($user_id, TRUE);
        $page++;
        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_show_more_past_user_event&page=' . $page . '&user_id=' . $user_id),
            'html' => $adapter->render_html(APOLLO_TEMPLATES_DIR . '/events/_org-events.php'),
            'have_more' => $adapter->isShowMore(),

            ));
    }

    /**
     * Tri LM
     * Show more org event
     * @return bool|int
     *
     */
    public function show_more_upcoming_org_event()
    {
        include_once APOLLO_TEMPLATES_DIR . '/events/list-events.php';
        $page = isset($_REQUEST['page']) ? max(1, intval($_REQUEST['page'])) : 1;
        $orgId = isset($_REQUEST['current_org_id']) ? $_REQUEST['current_org_id'] : '';
        $adapter = new List_Event_Adapter($page, of_get_option(Apollo_DB_Schema::_NUM_VIEW_MORE,Apollo_Display_Config::PAGESIZE_UPCOM));
        $html = $adapter->get_org_up_comming_events($orgId);

        $page++;
        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_show_more_upcoming_org_event&page=' . $page . '&current_org_id=' . $orgId),
            'html' => $html,
            'have_more' => $adapter->isShowMore(),

            ));
    }

    /**
     * Show more associated organizations in front end via AJAX
     *
     * @ticket #11493
     * @return json
     */
    public function show_more_associated_orgs()
    {
        // get query string parameters
        $page   = isset($_REQUEST['page'])    ? max(1, intval($_REQUEST['page'])) : 1;
        $termId = isset($_REQUEST['term_id']) ? intval($_REQUEST['term_id'])      : 0;

        // load associated organizations were configured in Event Theme Tool page
        include_once APOLLO_INCLUDES_DIR . '/src/org/inc/Apollo_List_Associated_Org.php';
        $list = new Apollo_List_Associated_Org();
        $list->currentPage($page);
        $list->getThemeToolAssociatedOrgs($termId);

        $page++;
        wp_send_json(array(
            'url'       => admin_url('admin-ajax.php?action=apollo_show_more_associated_orgs&page=' . $page . '&term_id=' . $termId),
            'html'      => $list->renderOrgHtml(),
            'have_more' => $list->isShowMore(),
        ));
    }

    /**
     * Show more associated venues in front end via AJAX
     *
     * @ticket #11493
     * @return json
     */
    public function show_more_associated_venues()
    {
        // get query string parameters
        $page   = isset($_REQUEST['page'])    ? max(1, intval($_REQUEST['page'])) : 1;
        $termId = isset($_REQUEST['term_id']) ? intval($_REQUEST['term_id'])      : 0;

        // load associated venues were configured in Event Theme Tool page
        /** @Ticket #13448 */
        include_once APOLLO_INCLUDES_DIR . '/src/venue/inc/Apollo_List_Associated_Venue.php';
        $list = new Apollo_List_Associated_Venue();
        $list->currentPage($page);
        $list->getThemeToolAssociatedVenues($termId);

        $page++;
        wp_send_json(array(
            'url'       => admin_url('admin-ajax.php?action=apollo_show_more_associated_venues&page=' . $page . '&term_id=' . $termId),
            'html'      => $list->renderVenueHtml(),
            'have_more' => $list->isShowMore(),
        ));
    }

    public function show_more_nearby_business_event()
    {
        include_once APOLLO_TEMPLATES_DIR . '/events/list-events.php';
        $page = isset($_REQUEST['page']) ? max(1, intval($_REQUEST['page'])) : 1;
        $orgId = isset($_REQUEST['current_org_id']) ? $_REQUEST['current_org_id'] : '';
        $adapter = new List_Event_Adapter($page, of_get_option(Apollo_DB_Schema::_NUM_VIEW_MORE,Apollo_Display_Config::PAGESIZE_UPCOM));
        $html = $adapter->get_business_nearby_event($orgId);

        $page++;
        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_show_more_nearby_business_event&page=' . $page . '&current_org_id=' . $orgId),
            'html' => $html,
            'have_more' => $adapter->isShowMore(),

            ));
    }

    /**
     * Tri LM
     * Show more org event
     * @return bool|int
     *
     */
    public function show_more_upcoming_venue_event()
    {
        include_once APOLLO_TEMPLATES_DIR . '/events/list-events.php';
        $page = isset($_REQUEST['page']) ? max(1, intval($_REQUEST['page'])) : 1;
        $venueId = isset($_REQUEST['current_venue_id']) ? $_REQUEST['current_venue_id'] : '';
        $adapter = new List_Event_Adapter($page, of_get_option(Apollo_DB_Schema::_NUM_VIEW_MORE,Apollo_Display_Config::PAGESIZE_UPCOM));
        $html = $adapter->get_venue_up_comming_events($venueId);

        $page++;
        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_show_more_upcoming_venue_event&page=' . $page . '&current_venue_id=' . $venueId),
            'html' => $html,
            'have_more' => $adapter->isShowMore(),

            ));
    }

    /**
     * Vu LH
     * Show more org event
     * @return bool|int
     *
     */
    public function show_more_past_venue_events()
    {
        include_once APOLLO_TEMPLATES_DIR . '/events/list-events.php';
        $page = isset($_REQUEST['page']) ? max(1, intval($_REQUEST['page'])) : 1;
        $venueId = isset($_REQUEST['current_venue_id']) ? $_REQUEST['current_venue_id'] : '';
        $adapter = new List_Event_Adapter($page, of_get_option(Apollo_DB_Schema::_NUM_VIEW_MORE,Apollo_Display_Config::PAGESIZE_UPCOM));
        $html = $adapter->get_venue_past_events($venueId);

        $page++;
        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_show_more_past_venue_events&page=' . $page . '&current_venue_id=' . $venueId),
            'html' => $html,
            'have_more' => $adapter->isShowMore(),

            ));
    }

    public function show_more_past_org_event()
    {

        include_once APOLLO_TEMPLATES_DIR . '/events/list-events.php';
        $page = isset($_REQUEST['page']) ? max(1, intval($_REQUEST['page'])) : 1;
        $orgId = isset($_REQUEST['current_org_id']) ? $_REQUEST['current_org_id'] : '';

        $adapter = new List_Event_Adapter($page, $this->get_pagesize_view_more());
        $html = $adapter->get_org_past_event($orgId);
        $page++;
        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_show_more_past_org_event&page=' . $page . '&current_org_id=' . $orgId),
            'html' => $html,
            'have_more' => $adapter->isShowMore(),

            ));
    }


    public function get_pagesize_view_more()
    {
        return ($num = of_get_option(Apollo_DB_Schema::_NUM_VIEW_MORE)) ? $num : Apollo_Display_Config::NUM_VIEW_MORE;
    }

    public function category_show_more_other_event()
    {
        require APOLLO_TEMPLATES_DIR . '/taxonomy/inc/single.php';

        $start = isset($_REQUEST['start']) ? max(0, intval($_REQUEST['start'])) : 0;
        $term_id = isset($_REQUEST['term_id']) ? (int)$_REQUEST['term_id'] : 0;
        /** @Ticket #14884 */
        if ($start == 0 ) {
            $pagesize = Apollo_Display_Config::SINGLE_TAXONOMY_OTHER_EVENT_SHOW;
        } else {
            $pagesize = $this->get_pagesize_view_more();
        }
        $displayed_ids = isset($_REQUEST['displayed_ids']) ? $_REQUEST['displayed_ids'] : '';

        $hander_page = new Apollo_Single_Category($term_id);
        $hander_page->set_displayed_event(explode(',', $displayed_ids));

        $others = $hander_page->getOthersEvent(array('pagesize' => $pagesize, 'start' => $start), $term_id);


        $currentTheme = function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '';
        if ( $currentTheme == APL_SM_DIR_THEME_NAME) {
            $html = $hander_page->renderWithData(SONOMA_MODULES_DIR. '/event/templates/taxonomy/html/others-event.php',  $others['datas']);
        } else {
            $html = $hander_page->renderWithData(APOLLO_TEMPLATES_DIR . '/taxonomy/html/others-event.php', $others['datas']);
        }

        // Repair next link url
        $nextStart = $start + $pagesize;

        $lastDisplayedIds = implode(',', $hander_page->get_displayed_events());

        // Set cookie pagesize: if go to the event detail then come back to the category listing page, the previous results will be displayed again
        setcookie(Apollo_Const::_APL_TAX_OTHER_EVENTS_DISPLAYED.$term_id, $others['displayed'], time() + 1800, COOKIEPATH, COOKIE_DOMAIN);

        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_category_show_more_other_event&start=' . $nextStart . '&pagesize=' . $pagesize . '&term_id=' . $term_id. '&displayed_ids=' . $lastDisplayedIds),
            'html' => $html,
            'have_more' => $hander_page->isHaveMoreOtherEvent(),
            'displayed' => $others['displayed']
            ));
    }

    public function get_more_search()
    {

        include APOLLO_TEMPLATES_DIR . '/search/class-apollo-search.php';
        $s = isset($_REQUEST['s']) ? $_REQUEST['s'] : '';
        $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : '';
        $type = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';
        $temp = isset($_REQUEST['view']) ? $_REQUEST['view'] : '';
        $search_obj = new Apollo_Search($page);

        $search_obj->type = $type;
        $search_obj->search($s);
        $html = $search_obj->render_html();

        /* CREATE NEW REQUEST URL */
        $page++;

        /* SEND DATA */
        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_get_more_search&page=' . $page . '&s=' . $s . '&type=' . $type . '&view=' . $temp),
            'html' => $html,
            'have_more' => $search_obj->have_more_on_viewmore(),
            ));
    }

    public function get_more_search_artist()
    {

        include_once APOLLO_SRC_DIR. '/artist/inc/factory/ArtistsFactory.php';

        $artistActiveMemberField = of_get_option(Apollo_DB_Schema::_ARTIST_ACTIVE_MEMBER_FIELD, false);
        $factory = new ArtistsFactory();

        if($artistActiveMemberField){
            $artistMemberType = !empty($_GET['artist_member']) ? $_GET['artist_member'] : 'premium';
            $search_obj = $factory->get($artistMemberType);
        } else {
            $search_obj = $factory->get();
        }

        $html = $search_obj->render_html();

        /* CREATE NEW REQUEST URL */
        $page = $search_obj->get_current_page();
        $page++;

        $old_request = add_query_arg('page', $page);
        $old_request = remove_query_arg('action', $old_request);
        $qs_pattern = '|\?.*?$|';
        $query_string = '';
        if (preg_match($qs_pattern, $old_request, $arr_matched)) {
            $query_string = $arr_matched[0];
            $query_string = ltrim($query_string, '?');
        }

        /* SEND DATA */
        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_get_more_search_artist' . '&' . $query_string),
            'html' => $html,
            'have_more' => $search_obj->have_more_on_viewmore(),
            ));
    }

    public function get_more_search_author()
    {

        $search_obj = apl_instance('APL_Lib_Page_Author');

        $search_obj->search();
        $html = $search_obj->render_html();

        /* CREATE NEW REQUEST URL */
        $page = $search_obj->get_current_page();
        $page++;

        $old_request = add_query_arg('page', $page);
        $old_request = remove_query_arg('action', $old_request);
        $qs_pattern = '|\?.*?$|';
        $query_string = '';
        if (preg_match($qs_pattern, $old_request, $arr_matched)) {
            $query_string = $arr_matched[0];
            $query_string = ltrim($query_string, '?');
        }

        /* SEND DATA */
        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_get_more_search_author' . '&' . $query_string),
            'html' => $html,
            'have_more' => $search_obj->have_more_on_viewmore(),
            ));
    }

    public function get_more_search_org()
    {

        include_once APOLLO_INCLUDES_DIR . '/src/org/inc/class-org-search.php';

        $search_obj = new Apollo_Org_Page();

        $search_obj->search();
        $html = $search_obj->render_html();

        /* CREATE NEW REQUEST URL */
        $page = $search_obj->get_current_page();
        $page++;

        $old_request = add_query_arg('page', $page);
        $old_request = remove_query_arg('action', $old_request);
        $qs_pattern = '|\?.*?$|';
        $query_string = '';
        if (preg_match($qs_pattern, $old_request, $arr_matched)) {
            $query_string = $arr_matched[0];
            $query_string = ltrim($query_string, '?');
        }

        /* SEND DATA */
        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_get_more_search_org' . '&' . $query_string),
            'html' => $html,
            'have_more' => $search_obj->have_more_on_viewmore(),
            ));
    }

    /**
     * Get more business
     *
     * @return mixed
     */
    public function get_more_search_business()
    {

        include APOLLO_SRC_DIR. '/business/inc/class-business-search.php';

        $search_obj = new Apollo_Business_Page();

        $search_obj->search();
        $html = $search_obj->render_html();

        /* CREATE NEW REQUEST URL */
        $page = $search_obj->get_current_page();
        $page++;

        $old_request = add_query_arg('page', $page);
        $old_request = remove_query_arg('action', $old_request);
        $qs_pattern = '|\?.*?$|';
        $query_string = '';
        if (preg_match($qs_pattern, $old_request, $arr_matched)) {
            $query_string = $arr_matched[0];
            $query_string = ltrim($query_string, '?');
        }

        /* SEND DATA */
        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_get_more_search_business' . '&' . $query_string),
            'html' => $html,
            'have_more' => $search_obj->have_more_on_viewmore(),
        ));
    }

    public function get_more_search_event()
    {

        include_once APOLLO_INCLUDES_DIR . '/src/event/inc/class-event-search.php';

        $search_obj = new Apollo_Event_Page();

        $search_obj->search();
        $html = $search_obj->render_html();

        /* CREATE NEW REQUEST URL */
        $page = $search_obj->get_current_page();
        $page++;

        $old_request = add_query_arg('page', $page);
        $old_request = remove_query_arg('action', $old_request);
        $qs_pattern = '|\?.*?$|';
        $query_string = '';
        if (preg_match($qs_pattern, $old_request, $arr_matched)) {
            $query_string = $arr_matched[0];
            $query_string = ltrim($query_string, '?');
        }

        /* SEND DATA */
        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_get_more_search_event' . '&' . $query_string),
            'html' => $html,
            'have_more' => $search_obj->have_more_on_viewmore(),
            ));
    }

    public function get_more_search_program()
    {
        include_once APOLLO_INCLUDES_DIR . '/src/program/inc/class-program-search.php';//

        $search_obj = new Apollo_Program_Page();

        $search_obj->search();
        $html = $search_obj->render_html();

        /* CREATE NEW REQUEST URL */
        $page = $search_obj->get_current_page();
        $page++;

        $old_request = add_query_arg('page', $page);
        $old_request = remove_query_arg('action', $old_request);
        $qs_pattern = '|\?.*?$|';
        $query_string = '';
        if (preg_match($qs_pattern, $old_request, $arr_matched)) {
            $query_string = $arr_matched[0];
            $query_string = ltrim($query_string, '?');
        }

        /* SEND DATA */
        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_get_more_search_program' . '&' . $query_string),
            'html' => $html,
            'have_more' => $search_obj->have_more_on_viewmore(),
            ));
    }

    public function get_more_search_educator()
    {
        include_once APOLLO_INCLUDES_DIR . '/src/educator/inc/class-educator-search.php';
        $search_obj = new Apollo_Educator_Page();
        $search_obj->search();
        $html = $search_obj->render_html();

        $page = $search_obj->get_current_page();
        $page++;

        $old_request = add_query_arg('page', $page);
        $old_request = remove_query_arg('action', $old_request);
        $qs_pattern = '|\?.*?$|';
        $query_string = '';
        if (preg_match($qs_pattern, $old_request, $arr_matched)) {
            $query_string = $arr_matched[0];
            $query_string = ltrim($query_string, '?');
        }

        /* SEND DATA */
        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_get_more_search_educator' . '&' . $query_string),
            'html' => $html,
            'have_more' => $search_obj->have_more_on_viewmore(),
            ));
    }

    public function get_more_search_venue()
    {

        include_once APOLLO_INCLUDES_DIR . '/src/venue/inc/class-venue-search.php';

        $search_obj = new Apollo_Venue_Page();

        $search_obj->search();
        $html = $search_obj->render_html();

        /* CREATE NEW REQUEST URL */
        $page = $search_obj->get_current_page();
        $page++;

        $old_request = add_query_arg('page', $page);
        $old_request = remove_query_arg('action', $old_request);
        $qs_pattern = '|\?.*?$|';
        $query_string = '';
        if (preg_match($qs_pattern, $old_request, $arr_matched)) {
            $query_string = $arr_matched[0];
            $query_string = ltrim($query_string, '?');
        }

        /* SEND DATA */
        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_get_more_search_venue' . '&' . $query_string),
            'html' => $html,
            'have_more' => $search_obj->have_more_on_viewmore(),
            ));
    }

    public function get_more_search_classified()
    {

        include_once APOLLO_SRC_DIR . '/classified/inc/class-classified-search.php';

        $search_obj = new Apollo_Classified_Page();

        $search_obj->search();
        $html = $search_obj->render_html();

        /* CREATE NEW REQUEST URL */
        $page = $search_obj->get_current_page();
        $page++;

        $old_request = add_query_arg('page', $page);
        $old_request = remove_query_arg('action', $old_request);
        $qs_pattern = '|\?.*?$|';
        $query_string = '';
        if (preg_match($qs_pattern, $old_request, $arr_matched)) {
            $query_string = $arr_matched[0];
            $query_string = ltrim($query_string, '?');
        }

        /* SEND DATA */
        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_get_more_search_classified' . '&' . $query_string),
            'html' => $html,
            'have_more' => $search_obj->have_more_on_viewmore(),
            ));
    }

    public function get_more_search_public_art()
    {

        include_once APOLLO_SRC_DIR . '/public-art/inc/class-public-art-search.php';

        $search_obj = new Apollo_Public_Art_Page();

        $search_obj->search();
        $html = $search_obj->render_html();

        /* CREATE NEW REQUEST URL */
        $page = $search_obj->get_current_page();
        $page++;

        $old_request = add_query_arg('page', $page);
        $old_request = remove_query_arg('action', $old_request);
        $qs_pattern = '|\?.*?$|';
        $query_string = '';
        if (preg_match($qs_pattern, $old_request, $arr_matched)) {
            $query_string = $arr_matched[0];
            $query_string = ltrim($query_string, '?');
        }

        /* SEND DATA */
        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_get_more_search_public_art' . '&' . $query_string),
            'html' => $html,
            'have_more' => $search_obj->have_more_on_viewmore(),
            ));
    }

    public function get_multiple_post_dashboard_page()
    {
        $arrdata ='';
        require APOLLO_TEMPLATES_DIR . '/pages/dashboard/inc/apollo-account-dashboard-dataprovider.php';
        $page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $pagesize = isset($_REQUEST['pagesize']) ? (int)$_REQUEST['pagesize'] : 1;
        $post_type = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';
        $start = ($page - 1) * $pagesize;
        $user_id = get_current_user_id();

        $eventListing = array('upcomming_event', 'past_event', 'draft_event', 'private_event', 'unconfirmed_event');
        if (in_array($post_type, $eventListing)) {
            include_once APOLLO_TEMPLATES_DIR.'/events/list-events.php';
            $list_events = new List_Event_Adapter(1);

            switch ($post_type){
                case "upcomming_event":
                    $events = $list_events->get_upcomming_user_events_for_dashboard('', " LIMIT $start, $pagesize ");
                    break;
                case "draft_event":
                    $events = $list_events->get_draft_user_events_for_dashboard('', " LIMIT $start, $pagesize ");
                    break;
                case "private_event":
                    /**
                     * @ticket #18932: Display 'private' events on FE user dashboard
                     */
                    $events = $list_events->get_private_user_events_for_dashboard('', " LIMIT $start, $pagesize ");
                    break;
                case "unconfirmed_event":
                    $events = $list_events->get_unconfirmed_user_events_for_dashboard('', " LIMIT $start, $pagesize ");
                    break;
                default:
                    //past event:
                    $events = $list_events->get_past_user_events_for_dashboard('', " LIMIT $start, $pagesize ");
                    break;
            }

            $arrdata = array(
                'datas' => $events,
                //'hideCopy' => $post_type == 'past_event'
                );
        }
        elseif ($post_type == 'upcomming_program' || $post_type == 'expired_program')
        {
            include_once APOLLO_TEMPLATES_DIR.'/educator/list-programs.php';
            $list_programs = new List_Program_Adapter(1);

            $events = $post_type == 'upcomming_program' ?
            $list_programs->get_upcomming_programs('', " LIMIT $start, $pagesize ", ' ORDER BY post_title ASC')
            : $list_programs->get_expired_programs('', " LIMIT $start, $pagesize ", ' ORDER BY post_title ASC');

            $arrdata = array(
                'datas' => $events,
                //'hideCopy' => $post_type == 'past_event'
                );
        }
        else {
            $arrdata = Apollo_Account_Dashboard_Provider::getListPostsByCurrentUser(array(
                'pagesize' => $pagesize,
                'start' => $start,
                'post_type' => array(
                    $post_type,
                    ),
                'user_id' => $user_id,
                'have_paging' => true,
                ));
        }

        if (count($arrdata['datas']) > 0) {
            $template = '';
            if($post_type == Apollo_DB_Schema::_ORGANIZATION_PT){
                /* @Ticket #13470 **/
                $template = APOLLO_TEMPLATES_DIR . '/org/partial/list-org.php';
            }
            if($post_type == Apollo_DB_Schema::_VENUE_PT){
                $template = APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/partial/_venue/list-venue.php';
            }
            if ($post_type == Apollo_DB_Schema::_ARTIST_PT) {
                $template = APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/partial/_artist/list-artist.php';
            }
            if (in_array($post_type, $eventListing)) {
                $template = APOLLO_TEMPLATES_DIR. '/pages/dashboard/html/partial/_event-dashboard/events.php';
            }
            if ($post_type == 'upcomming_program' || $post_type == 'expired_program') {
                $template = APOLLO_TEMPLATES_DIR. '/pages/dashboard/html/partial/_program/program_published.php';
            }

            if(!empty($template)){
                wp_send_json(array(
                    'html' => APL::renderTemplateWithData($arrdata, $template),
                    ));
            }
        }

        wp_send_json(array(
            'html' => ''
            ));
    }

    public function get_bookmark()
    {
        require APOLLO_TEMPLATES_DIR . '/pages/dashboard/inc/apollo-account-dashboard-dataprovider.php';
        $page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $pagesize = isset($_REQUEST['pagesize']) ? (int)$_REQUEST['pagesize'] : 1;
        $post_type = isset($_REQUEST['type']) ? $_REQUEST['type'] : Apollo_DB_Schema::_EVENT_PT;

        if (!in_array($post_type, array(
            Apollo_DB_Schema::_EVENT_PT,
            Apollo_DB_Schema::_ORGANIZATION_PT,
            Apollo_DB_Schema::_VENUE_PT,
            Apollo_DB_Schema::_ARTIST_PT,
            Apollo_DB_Schema::_CLASSIFIED,
            Apollo_DB_Schema::_PUBLIC_ART_PT
            ))
            )
            $post_type = Apollo_DB_Schema::_EVENT_PT;

        $start = ($page - 1) * $pagesize;
        $user_id = get_current_user_id();

        $arrdata = Apollo_Account_Dashboard_Provider::getBookmark(array(
            'pagesize' => $pagesize,
            'start' => $start,
            'post_type' => array(
                $post_type,
                ),
            'user_id' => $user_id,
            'have_paging' => true,
            ));

        if (count($arrdata['datas']) > 0) {
            $template = APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/partial/_bookmark/' . $post_type . '.php';
            wp_send_json(array(
                'html' => APL::renderTemplateWithData($arrdata, $template),
                ));
        }

        wp_send_json(array(
            'html' => ''
            ));
    }

    public function get_user_activity()
    {
        require APOLLO_TEMPLATES_DIR . '/pages/dashboard/inc/apollo-account-dashboard-dataprovider.php';
        $page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $pagesize = isset($_REQUEST['pagesize']) ? (int)$_REQUEST['pagesize'] : 1;


        $start = ($page - 1) * $pagesize;
        $user_id = get_current_user_id();

        $arrdata = Apollo_Account_Dashboard_Provider::getUserActivity(array(
            'pagesize' => $pagesize,
            'start' => $start,
            'user_id' => $user_id,
            'have_paging' => true,
            ));

        $template_name = '_activity';
        if (count($arrdata['datas']) > 0) {
            $template = APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/partial/' . $template_name . '.php';
            wp_send_json(array(
                'html' => APL::renderTemplateWithData($arrdata, $template),
                ));
        }

        wp_send_json(array(
            'html' => ''
            ));
    }

    public function remove_bookmark()
    {
        $s_event_id = isset($_REQUEST['ids']) ? $_REQUEST['ids'] : '';

        $a_event_id = explode(",", $s_event_id);
        $a_event_id = array_map('intval', $a_event_id);

        if (empty($a_event_id)) {
            wp_send_json(array(
                'status' => 0
                ));
        }

        /* delete bookmark */
        global $wpdb;

        $sin = "('" . implode("','", $a_event_id) . "')";
        $user_id = get_current_user_id();

        $tbl_bookmark = $wpdb->{Apollo_Tables::_BOOKMARK};
        $sql = "DELETE FROM $tbl_bookmark WHERE (bookmark_id IN $sin) AND user_id = $user_id";
        $status = $wpdb->query($sql);
        wp_send_json(array(
            'status' => $status,
            ));
    }

    public function set_bookmark_for_myfriend()
    {
        $s_event_id = isset($_REQUEST['ids']) ? $_REQUEST['ids'] : '';
        $is_single = isset($_REQUEST['is_single']) && $_REQUEST['is_single'] ? $_REQUEST['is_single'] : '';
        $a_event_id = explode(",", $s_event_id);
        $a_event_id = array_map('intval', $a_event_id);

        if (empty($a_event_id)) {
            wp_send_json(array(
                'status' => 0
                ));
        }

        /* Build content popup */
        $html = APL::renderTemplateWithData($a_event_id, APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/partial/_bookmark/tellfriend.php', $is_single);

        wp_send_json(array(
            'status' => 1,
            'html' => $html,
            ));
    }

    public function add_org_in_event()
    {

        $user_id = get_current_user_id();
        $event_id = $_REQUEST['post'];
        $event = get_event(get_post($event_id));

        // Not have event
        if (!$_REQUEST['name'] || !$event->id) return false;

        // Create post object
        $post = array(
            'post_title' => wp_strip_all_tags($_REQUEST['name']),
            'post_status' => 'publish',
            'post_author' => $user_id,
            'post_type' => Apollo_DB_Schema::_ORGANIZATION_PT,
            );

        // Insert the post into the database
        $post_ID = wp_insert_post($post);
        wp_send_json(array(
            'id' => $post_ID,
            'edit_link' => get_edit_post_link($post_ID, '')
        ));
        exit;
    }

    public function log_click_activity()
    {
        if (!isset($_GET['item_id']) || !isset($_GET['activity'])) {
            // return; Do nothing
        }

        $activity        = sanitize_text_field($_GET['activity']);
        $start           = isset($_GET['start']) ? sanitize_text_field($_GET['start']) : '';
        $end             = isset($_GET['end'])   ? sanitize_text_field($_GET['end'])   : '';
        $registeredOrgID = isset($_GET['orgId']) ? intval($_GET['orgId'])              : '';

        if (!in_array($activity, Apollo_Activity_System::getClickGroup())) {
            // return; Do nothing
        }

        $item_id = intval($_GET['item_id']);
        $post    = get_post($item_id);

        $dataForLog = array(
            'user_id'    => get_current_blog_id(),
            'item_id'    => $item_id,
            'activity'   => $activity,
            'item_type'  => $post->post_type,
            'url'        => get_permalink($item_id),
            'title'      => $post->post_title,
            'timestamp'  => time(),
            'org_id'     => $registeredOrgID,
            'org_title'  => get_the_title($registeredOrgID),
            'start_date' => $start,
            'end_date'   => $end,
        );
        Apollo_Event_System::trigger(Apollo_Event_System::ACTIVIY, $dataForLog);
    }

    public function update_rating()
    {
        //check maintenance mode
        if(Apollo_App::isMaintenanceMode()) {
            wp_send_json(
                array(
                    'error' => true,
                    'data' => array(
                        'msg' => __( 'Sorry for the inconvenience.Our website is currently undergoing scheduled maintenance')
                    ),
                    'error_code' => '02'
                )
            );
            return;
        }

        if (!isset($_GET['item_id']) || !isset($_GET['item_type'])) {
            // return; Do nothing
        }

        $item_id = intval($_GET['item_id']);
        $item_type = sanitize_text_field($_GET['item_type']);
        $msg = isset($_GET['item_msg']) && $_GET['item_msg'] ? $_GET['item_msg'] : '';

        $is_allow_log = true;
        $log_view_sn = Apollo_SESSION::LOG_EVENT_RATING . $item_id . ':' . $item_type;

        if (isset($_SESSION['apollo'][$log_view_sn])) { // view time
            $last_time = $_SESSION['apollo'][$log_view_sn];
            if (time() - $last_time < MINUTE_IN_SECONDS) {
                $is_allow_log = false;
            }
        }

        if (!$is_allow_log)
            wp_send_json(array(
                'error' => true,
                'data' => array(
                    'msg' => $msg ? $msg : __('Thank you! You have rated already', 'apollo'),
                    )
                ));

        $_SESSION['apollo'][$log_view_sn] = time();

        $r = Apollo_Rating::updateRating($item_id, $item_type);

        $arr = array(
            'error' => false,
            'data' => array(
                'msg' => ''
                )
            );

        if ($r !== false) {
            $arr['data']['count'] = intval($r);
            wp_send_json($arr);
        }

        $arr['error'] = true;
        wp_send_json($arr);
    }

    public function get_ratings()
    {
        if (!isset($_GET['uids'])) {
            // return; Do nothing
        }

        $arr = explode(",", $_GET['uids']);

        $r = Apollo_Rating::getRatings($arr);

        $arr = array(
            'error' => false,
            'data' => array(
                'msg' => ''
                )
            );

        if ($r !== false) {
            $arr['data']['counts'] = $r;
            wp_send_json($arr);
        }

        $arr['error'] = true;
        wp_send_json($arr);
    }

    public function log_share_activity()
    {
        if (!isset($_GET['item_id']) || !isset($_GET['activity'])) {
            // return; Do nothing
        }

        $item_id = intval($_GET['item_id']);
        $post = get_post($item_id);
        $activity = sanitize_text_field($_GET['activity']);


        if (!in_array($activity, Apollo_Activity_System::getShareGroup())) {
            // return; Do nothing
        }

        $dataForLog = array(
            'user_id' => get_current_blog_id(),
            'item_id' => $item_id,
            'activity' => $activity,
            'item_type' => $post->post_type,
            'url' => get_permalink($item_id),
            'title' => $post->post_title,
            'timestamp' => time(),
            );
        Apollo_Event_System::trigger(Apollo_Event_System::ACTIVIY, $dataForLog);
    }

    public function add_venue_in_event()
    {

        if (!Apollo_App::is_avaiable_module(Apollo_DB_Schema::_VENUE_PT)) return false;

        $user_id = get_current_user_id();
        $event_id = $_REQUEST['post'];
        $event = get_event(get_post($event_id));

        // Not have event
        if (!$_REQUEST['name'] || !$event->id) return false;

        // Create post object
        $post = array(
            'post_title' => wp_strip_all_tags($_REQUEST['name']),
            'post_status' => 'publish',
            'post_author' => $user_id,
            'post_type' => Apollo_DB_Schema::_VENUE_PT,
            );

        $url = isset($_REQUEST['url']) && filter_var($_REQUEST['url'], FILTER_VALIDATE_URL) ? Apollo_App::clean_data_request($_REQUEST['url']) : '';

        /** @Ticket #13874 - Apply Neighborhood logic*/
        $address = array(
            Apollo_DB_Schema::_VENUE_ADDRESS1 => Apollo_App::clean_data_request($_REQUEST['address1']),
            Apollo_DB_Schema::_VENUE_ADDRESS2 => Apollo_App::clean_data_request($_REQUEST['address2']),
            Apollo_DB_Schema::_VENUE_CITY => Apollo_App::clean_data_request($_REQUEST['city']),
            Apollo_DB_Schema::_VENUE_NEIGHBORHOOD => Apollo_App::clean_data_request($_REQUEST['neighborhood']),
            Apollo_DB_Schema::_VENUE_STATE => Apollo_App::clean_data_request($_REQUEST['state']),
            Apollo_DB_Schema::_VENUE_ZIP => Apollo_App::clean_data_request($_REQUEST['zip']),
            Apollo_DB_Schema::_VENUE_REGION => Apollo_App::clean_data_request($_REQUEST['region']),
            );

        $venueData = array(
            Apollo_DB_Schema::_VENUE_WEBSITE_URL => $url
            );

        // Insert the post into the database
        $post_ID = wp_insert_post($post);

        // Update venue meta data
        update_apollo_meta($post_ID, Apollo_DB_Schema::_APL_VENUE_ADDRESS, serialize($address));
        update_apollo_meta($post_ID, Apollo_DB_Schema::_APL_VENUE_DATA, serialize($venueData));

        // Update for searching
        update_apollo_meta( $post_ID, Apollo_DB_Schema::_VENUE_CITY, isset($_REQUEST['city']) ? $_REQUEST['city'] : '' );
        /** @Ticket #13874 */
        update_apollo_meta( $post_ID, Apollo_DB_Schema::_VENUE_NEIGHBORHOOD, isset($_REQUEST['neighborhood']) ? $_REQUEST['neighborhood'] : '' );
        update_apollo_meta( $post_ID, Apollo_DB_Schema::_VENUE_ZIP, isset($_REQUEST['zip']) ? $_REQUEST['zip'] : '' );
        update_apollo_meta( $post_ID, Apollo_DB_Schema::_VENUE_REGION, isset($_REQUEST['region']) ? $_REQUEST['region'] : '' );
        update_apollo_meta( $post_ID, Apollo_DB_Schema::_VENUE_STATE, isset($_REQUEST['state']) ? $_REQUEST['state'] : '' );

        /*@ticket #16898: [CF] 20180730 - [Event admin] Please add latitude/longitude data to the 'temp' venue - Item 4*/
        update_apollo_meta( $post_ID, Apollo_DB_Schema::_VENUE_LATITUDE, Apollo_App::clean_data_request($_REQUEST['lat']) );
        update_apollo_meta( $post_ID, Apollo_DB_Schema::_VENUE_LONGITUDE, Apollo_App::clean_data_request($_REQUEST['long']) );

        //vandd get edit post link.
        wp_send_json(array(
            'id' => $post_ID,
            'edit_link' => get_edit_post_link($post_ID, '')
        ));
//        echo $post_ID;
        exit;
    }

    public function upload_pdf_artist()
    {
        require_once(ABSPATH . "wp-admin" . '/includes/image.php');
        require_once(ABSPATH . "wp-admin" . '/includes/file.php');
        require_once(ABSPATH . "wp-admin" . '/includes/media.php');

        if (!$_FILES) {
            wp_die(''); // Do nothing
        }

        $pid = 0;
        $uid = sanitize_text_field($_POST['uid']);

        // check max allowed
        $max_upload_pdf_artist = of_get_option(Apollo_DB_Schema::_MAX_UPLOAD_PDF_ARTIST, Apollo_Display_Config::_MAX_UPLOAD_PDF_DEFAULT);
        $group_name = 'pdf' . Apollo_SESSION::SUFFIX_ADD_PDF_ARTIST;

        unset($_SESSION['apollo'][$group_name][$pid]);
        if (isset($_SESSION['apollo'][$group_name][$pid]) && count($_SESSION['apollo'][$group_name][$pid]) > $max_upload_pdf_artist) {
            wp_die('Max file limit'); // do nothing
        }

        $FILE = end($_FILES);

        if ($FILE['error'] !== UPLOAD_ERR_OK) {
            wp_send_json(array(
                'error' => true,
                'data' => array(
                    'msg' => 'Error upload file. Please try again',
                    'uid' => $uid,
                    ),
                ));

        }

        // check type here @todo check here
        $type = 'pdf';

        if ($type !== 'pdf') {
            wp_send_json(array(
                'error' => true,
                'data' => array(
                    'msg' => 'Unsupport that file',
                    'uid' => $uid,
                    ),
                ));
        }

        // copy to upload directory
        if (!(($uploads = wp_upload_dir()) && false === $uploads['error'])) {
            wp_send_json(array(
                'error' => true,
                'data' => array(
                    'uid' => $uid,
                    'msg' => 'Error upload file: ' . $FILE['name']
                    ),
                ));
        }

        $oname = $FILE['name'];

        $tmpname = $oname;
        $fname = wp_unique_filename($uploads['path'], $tmpname);

        // Move the file to the uploads dir.
        $new_file = $uploads['path'] . '/' . $fname;
        $url = $uploads['url'] . '/' . $fname;

        $move_new_file = @ move_uploaded_file($FILE['tmp_name'], $new_file);

        if (false === $move_new_file) {
            wp_send_json(array(
                'error' => true,
                'data' => array(
                    'uid' => $uid,
                    'msg' => __('Upload error', 'apollo')
                    ),
                ));
        }

        $_SESSION['apollo'][$group_name][$pid][$uid] = array(
            'file' => $new_file,
            'oname' => $oname,
            'type' => $type,
            'url' => $url,
            );

        wp_send_json(array(
            'error' => false,
            'data' => array(
                'msg' => 'Upload successfully',
                'gallerys' => array(
                    'error' => false,
                    'url' => $url,
                    'uid' => $uid,
                    ),
                )
            ));
    }


    public function upload_and_drop()
    {

        check_ajax_referer('upload_and_drop', 'nonce');

        require_once(ABSPATH . "wp-admin" . '/includes/image.php');
        require_once(ABSPATH . "wp-admin" . '/includes/file.php');
        require_once(ABSPATH . "wp-admin" . '/includes/media.php');

        $pid = intval($_GET['pid']);
        $target = sanitize_text_field($_GET['target']);

        if (!$_FILES) {
            wp_die(''); // Do nothing
        }

        // Just process one image currently
        $endFile = end($_FILES);
        /*@ticket #17020: [CF] 20180803 - [FE Forms] - Add the option to increase the file size upload - Item 1*/
        $configUploadFileSize = Apollo_App::maxUploadFileSize('kb');
        $configUploadFileSizeMB = round($configUploadFileSize / 1024,2);
        if ($endFile['error'] !== UPLOAD_ERR_OK) {
            /** @Ticket #15077 - Exceeded file size limit.*/
            switch ($endFile['error']) {
                case UPLOAD_ERR_NO_FILE :
                    $msg = __("No file sent.","apollo");
                    break;
                case UPLOAD_ERR_INI_SIZE :
                case UPLOAD_ERR_FORM_SIZE :
                    $msg = sprintf(__("Image file size is too large. Your image cannot be larger than  %sMB","apollo"),$configUploadFileSizeMB);
                    break;
                default :
                    $msg = sprintf(__("Invalid Uploaded File '%s'","apollo"),$endFile['name']);
                    break;
            }
            wp_send_json(array(
                'error' => true,
                'data' => array(
                    'msg' => $msg
                    ),
                ));
        }

        /*Validate filesize with value in networking configuration */
        $uploadingFileSize = intval($endFile['size']) / 1024;
        if($uploadingFileSize > $configUploadFileSize){
            wp_send_json(array(
                'error' => true,
                'data' => array(
                    'msg' => sprintf(__("File '%s' is too large. Maximum size allow is %sMB","apollo"),$endFile['name'],$configUploadFileSizeMB)
                    ),
                ));
        }
        /*End validate filesize with value in networking configuration */

        $wp_filetype = wp_check_filetype_and_ext($endFile['tmp_name'], $endFile['name']);
        $ext = empty($wp_filetype['ext']) ? '' : $wp_filetype['ext'];
        $type = empty($wp_filetype['type']) ? '' : $wp_filetype['type'];


        if ((!$type || !$ext) && !current_user_can('unfiltered_upload')) {
            wp_send_json(array(
                'error' => true,
                'data' => array(
                    'msg' => __('Sorry, this file type is not permitted for security reasons.', 'apollo')
                    )
                ));
        }

        $meta = getimagesize($endFile['tmp_name']);
        $arr_size = Apollo_App::getMinFeatureImgUpload($target);
        if ($arr_size['w'] !== 'auto' && $arr_size['h'] !== 'auto') {
            $min_w = $arr_size['w'];
            $min_h = $arr_size['h'];

            if ($meta[0] < $min_w || $meta[1] < $min_h) {
                wp_send_json(array(
                    'error' => true,
                    'data' => array(
                        'msg' => sprintf(__("Image must have width > %s px and height > %s px", 'apollo'), $min_w, $min_h)
                        )
                    ));
            }
        }

        // copy to upload directory
        if (!(($uploads = wp_upload_dir()) && false === $uploads['error'])) {
            wp_send_json(array(
                'error' => true,
                'data' => array(
                    'msg' => sprintf(__('Error upload file: %s','apollo'),$endFile['name'])
                    ),
                ));
        }

        $oname = $endFile['name'];
        $fname = wp_unique_filename($uploads['path'], $oname);

        // Move the file to the uploads dir.
        $new_file = $uploads['path'] . '/' . $fname;
        $url = $uploads['url'] . '/' . $fname;

        $move_new_file = @ move_uploaded_file($endFile['tmp_name'], $new_file);

        if (false === $move_new_file) {
            wp_send_json(array(
                'error' => true,
                'data' => array(
                    'msg' => sprintf(__( 'Cannot move to directory: %s', 'apollo'), $uploads['path'])
                    ),
                ));
        }

        wp_send_json(array(
            'error' => false,
            'data' => array(
                'file' => array(
                    'url' => $url,
                    'meta' => array(
                        'width' => $meta[0],
                        'height' => $meta[1],
                        ),
                    'file' => $new_file,
                    'oname' => $fname,
                    'type' => $type
                    )
                )
            ));

    }


    public function upload_and_drop_actually()
    {
        $changes = json_decode(wp_unslash($_REQUEST['history']));
        $sel = $changes[0]->c;

        if ($sel->w === 0 || $sel->h === 0) return;

        $pid = intval($_REQUEST['pid']);
        $target = sanitize_text_field($_GET['target']);

        /** @Ticket #12729 */
        $info = $_GET['featured'];

        require ABSPATH . 'wp-admin/includes/image-edit.php';

        $path = $info['file'];
        $type = $info['type'];
        $oname = $info['oname'];

        $img = wp_get_image_editor($path);

        if (is_wp_error($img)) {
            wp_send_json(array(
                'error' => true,
                'data' => array(
                    'msg' => __('Create image from path error', 'apollo')
                    )
                ));
        }
        /* Drop here */

        $img->crop($sel->x, $sel->y, $sel->w, $sel->h);

        /* clear exists infomation */
        if ($pid === 0) {
            @unlink($path); /* remove old path */
        }

        /* generate image for server */
        $uploads = wp_upload_dir();
        $fname = wp_unique_filename($uploads['path'], $oname);
        $filename = $uploads['path'] . '/' . $fname;
        $img->save($filename, $type);

        $url = $uploads['url'] . "/$fname";

        // update new info session

        $metafile = $img->get_size();
        wp_send_json(array(
            'error' => false,
            'data' => array(
                'file' => array(
                    'url' => $url,
                    'meta' => $metafile,
                    'file' => $filename,
                    'oname' => $fname,
                    'type' => $type
                    )
                )
            ));

    }

    public function upload_and_drop_remove()
    {
        $pid = intval($_GET['pid']);
        $target = sanitize_text_field($_GET['target']);

        $info = (isset($_SESSION['apollo'][$target . Apollo_SESSION::SUFFIX_ADD_PIMAGE][$pid])) ? $_SESSION['apollo'][$target . Apollo_SESSION::SUFFIX_ADD_PIMAGE][$pid] : array();
        unset($_SESSION['apollo'][$target . Apollo_SESSION::SUFFIX_ADD_PIMAGE]);

        if (empty($info)) return;

        if ($pid === 0) {
            @unlink($info['file']);
        }
    }

    public function is_add_tmp()
    {

        $btn = isset($_REQUEST['btn']) ? $_REQUEST['btn'] : '';
        $mod = isset($_REQUEST['mod']) ? $_REQUEST['mod'] : '';

        if (!$btn) $btn = 'add';
        $_SESSION['apl_add_' . $mod . '_btn'] = $btn;
    }

    public function check_process_upload_gallery()
    {
        if (isset($_GET['progress_key'])) {
            $status = apc_fetch('upload_' . $_GET['progress_key']);
            wp_send_json($status);
        }
    }

    public function upload_gallery()
    {

        require_once(ABSPATH . "wp-admin" . '/includes/image.php');
        require_once(ABSPATH . "wp-admin" . '/includes/file.php');
        require_once(ABSPATH . "wp-admin" . '/includes/media.php');

        if (!$_FILES) {
            wp_send_json(array(
                'error' => true,
                'data' => array(
                    'msg' => 'Error upload file. Please try again'
                ),
            ));
        }

        $target = sanitize_text_field($_GET['target']);
        $uid = sanitize_text_field($_POST['uid']);

        // Just process one image currently

        $FILE = end($_FILES);

        if ($FILE['error'] !== UPLOAD_ERR_OK) {


            wp_send_json(array(
                'error' => true,
                'data' => array(
                    'msg' => 'Error upload file. Please try again',
                    'uid' => $uid,
                    ),
                ));

        }

        $wp_filetype = wp_check_filetype_and_ext($FILE['tmp_name'], $FILE['name']);
        $ext = empty($wp_filetype['ext']) ? '' : $wp_filetype['ext'];
        $type = empty($wp_filetype['type']) ? '' : $wp_filetype['type'];

        if ((!$type || !$ext) && !current_user_can('unfiltered_upload')) {
            wp_send_json(array(
                'error' => true,
                'data' => array(
                    'msg' => 'Unsupport that file',
                    'uid' => $uid,
                    ),
                ));
        }

        $meta = getimagesize($FILE['tmp_name']);
        $arr_size = Apollo_App::getMinGalleryImgUpload($target);

        if ($arr_size['w'] !== 'auto' && $arr_size['h'] !== 'auto') {
            if ($meta[0] < $arr_size['w'] || $meta[1] < $arr_size['h']) {

                wp_send_json(array(
                    'error' => true,
                    'data' => array(
                        'uid' => $uid,
                        'msg' => sprintf(__("Image must have width > %s px and height > %s px", 'apollo'), $arr_size['w'], $arr_size['h'])
                        ),
                    ));
            }
        }

        // copy to upload directory
        if (!(($uploads = wp_upload_dir()) && false === $uploads['error'])) {
            wp_send_json(array(
                'error' => true,
                'data' => array(
                    'uid' => $uid,
                    'msg' => 'Error upload file: ' . $FILE['name']
                    ),
                ));
        }

        $oname = $FILE['name'];
        $tmpname = $oname;
        $fname = wp_unique_filename($uploads['path'], $tmpname);

        // Move the file to the uploads dir.
        $new_file = $uploads['path'] . '/' . $fname;
        $url = $uploads['url'] . '/' . $fname;

        $move_new_file = @ move_uploaded_file($FILE['tmp_name'], $new_file);

        if (false === $move_new_file) {
            wp_send_json(array(
                'error' => true,
                'data' => array(
                    'uid' => $uid,
                    'msg' => __('Upload error', 'apollo')
                    ),
                ));
        }

        $input_hidden = '
            <div class="gallery-item" img-id="'. $uid .'" data-url="'. $url .'">
                <input type="hidden" class="_gallerys _apl_file" name="_gallerys[file][]" value="'. $new_file .'" />
                <input type="hidden" class="_gallerys _apl_oname" name="_gallerys[oname][]" value="'. $oname .'" />
                <input type="hidden" class="_gallerys _apl_type" name="_gallerys[type][]" value="'. $type .'" />
                <input type="hidden" class="_gallerys _apl_url" name="_gallerys[url][]" value="'. $url .'" />
                <input type="hidden" class="_gallerys _apl_img_id" name="_gallerys[img_id][]" value="'. $uid .'" />
            </div>
        ';
        wp_send_json(array(
            'error' => false,
            'data' => array(
                'msg' => 'Upload successfully',
                'gallerys' => array(
                    'error' => false,
                    'url' => $url,
                    'uid' => $uid,
                    'file' => $new_file,
                    'oname' => $oname,
                    'type' => $type,
                    'input_hidden' => $input_hidden
                    ),
                )
            ));
    }

    public function remove_gallery()
    {
        $uid = '';
        if (isset($_GET['uid'])) {
            $uid = $_GET['uid'];
        }

        $pid = intval($_GET['pid']);
        $target = sanitize_text_field($_GET['target']);
        $meta_gallery = Apollo_App::getMetaKeyGalleryImgByTarget($target);

        if (empty($uid)) {

            if ($pid === 0) {
                foreach ($_SESSION['apollo'][$target . Apollo_SESSION::SUFFIX_ADD_GALLERY][$pid] as $key => $info) {
                    @unlink($info['file']);
                }
            }

            unset($_SESSION['apollo'][$target . Apollo_SESSION::SUFFIX_ADD_GALLERY][$pid]);
        } else if (isset($_SESSION['apollo'][$target . Apollo_SESSION::SUFFIX_ADD_GALLERY][$pid][$uid])) {
            if ($pid === 0) {
                @unlink($_SESSION['apollo'][$target . Apollo_SESSION::SUFFIX_ADD_GALLERY][$pid][$uid]['file']);
            }

            unset($_SESSION['apollo'][$target . Apollo_SESSION::SUFFIX_ADD_GALLERY][$pid][$uid]);
        }

        wp_send_json(array(
            'error' => false,
            'data' => array(
                'msg' => 'Successfully delete',
                )
            ));
    }

    /**
     * Ticket #14684
     * set expire for protected page cookie.
    */
    public function set_cookie_protected_pass() {
        $result = false;
        if (!empty($_POST['apl_cookiehash']) ) {
            if (isset( $_COOKIE['wp-postpass_'. $_POST['apl_cookiehash']])) {
                setcookie('wp-postpass_' . $_POST['apl_cookiehash'], '', 0, COOKIEPATH, COOKIE_DOMAIN);
                $result = true;
            }
        }
        wp_send_json(array('error' => false, 'data' => $result));
    }

    public function remove_pdf_artist()
    {
        $uid = '';
        if (isset($_GET['uid'])) {
            $uid = $_GET['uid'];
        }

        $pid = intval($_GET['pid']);
        $group_name = 'pdf' . Apollo_SESSION::SUFFIX_ADD_PDF_ARTIST;

        if (empty($uid)) {

            if ($pid === 0) {
                foreach ($_SESSION['apollo'][$group_name][$pid] as $key => $info) {
                    @unlink($info['file']);
                }
            }

            unset($_SESSION['apollo'][$group_name][$pid]);
        } else if (isset($_SESSION['apollo'][$group_name][$pid][$uid])) {
            if ($pid === 0) {
                @unlink($_SESSION['apollo'][$group_name][$pid][$uid]['file']);
            }

            unset($_SESSION['apollo'][$group_name][$pid][$uid]);
        }

        wp_send_json(array(
            'error' => false,
            'data' => array(
                'msg' => 'Successfully delete'
                )
            ));
    }

    //create:
    public function      create_event_time()
    {
        $date = $_GET['date_event'];

        $time_from = $_GET['time_from'];
        $time_to = isset($_GET['time_to']) ? $_GET['time_to'] : '';
        $event_id = intval($_GET['event_id']);


        $_p = implode("|", Apollo_Calendar::$arrWeeks);

        if (preg_match('/' . $_p . '/i', $date) > 0) {// type 2

            $dates = Apollo_Calendar::getStepsDateByDay($event_id, $date, $date);
            $r = Apollo_Calendar::createEvent($event_id, $dates, $time_from, $time_to);

            if ($r) {
                $arr = array();

                foreach ($dates as $k => $v) {
                    $arr[$v][] = array(
                        'event_id' => $event_id,
                        'date_event' => $v,
                        'time_from' => $time_from,
                        'time_to' => $time_to,
                        );
                }

                wp_send_json(array(
                    'result' => $arr
                    ));
            } else {
                wp_send_json(array(
                    'result' => $r
                    ));
            }
        } else {
            wp_send_json(array(
                'result' => Apollo_Calendar::createEvent($event_id, $date, $time_from, $time_to)
                ));
        }


    }

    public function create_event_times()
    {
        $date1 = $_GET['date_event1'];// Sun
        $date2 = $_GET['date_event2'];// Mon
        $time_from = $_GET['time_from'];
        $time_to = isset($_GET['time_to']) ? $_GET['time_to'] : '';
        $event_id = intval($_GET['event_id']);

        if (in_array($date1, Apollo_Calendar::$arrWeeks)) {
            $dates = Apollo_Calendar::getStepsDateByDay($event_id, $date1, $date2);
        } else {
            $dates = Apollo_Calendar::getStepsDateByDate($event_id, $date1, $date2);
        }


        /**
         *  TriLM:get disable calendar event cells
         *  Before insert event time we check this date is not disable
         */
        $disableCellDate = Apollo_App::apollo_get_meta_data($event_id, Apollo_DB_Schema::_APL_EVENT_DISABLE_CALENDAR_CELL);
        $insertDate = array();
        if (is_array($disableCellDate) && count($disableCellDate) > 0) {
            foreach ($dates as $date) {
                if (!in_array($date, $disableCellDate)) {
                    $insertDate[] = $date;
                }
            }
        } else {
            $insertDate = $dates;
        }

        $r = Apollo_Calendar::createEvent($event_id, $insertDate, $time_from, $time_to);
        $arr = array();

        if ($r) {
            foreach ($dates as $k => $v) {
                $arr[$v][] = array(
                    'event_id' => $event_id,
                    'date_event' => $v,
                    'time_from' => $time_from,
                    'time_to' => $time_to,
                    );
            }

            wp_send_json(array(
                'result' => $arr
                ));
        }

        wp_send_json(array(
            'result' => $r
            ));

    }

    //get:
    public function get_all_event_time()
    {
        $event_id = $_GET['event_id'];
        $data = Apollo_Calendar::static_get_all_event($event_id);
        wp_send_json($data);
    }

    //delete:
    public function      delete_event_time()
    {
        $is_front = isset($_GET['is_front']);
        $date_event = $_GET['date_event'];
        $event_id = $_GET['event_id'];
        $time_from = $_GET['time_from'];
        $time_to = isset($_GET['time_to']) ? $_GET['time_to'] : '';

        $_p = implode("|", Apollo_Calendar::$arrWeeks);

        if (preg_match('/' . $_p . '/i', $date_event) >= 1) {
            $arrdate_event = Apollo_Calendar::getStepsDateByDay($event_id, $date_event, $date_event);
        } else {
            $arrdate_event = (array)$date_event;
        }
        $r = Apollo_Calendar::deleteEventByListDateAndTime($event_id, $arrdate_event, $time_from, $time_to);

        $logObj = new Apollo_Logs("Delete the event date/time of event_id = $event_id, time_from = $time_from, time_to = $time_to", __FUNCTION__, $is_front);
        $logObj->logs();

        $m_error = error_get_last();

        if ($r) {
            wp_send_json(array(
                'result' => array(
                    'error' => false,
                    'data' => $arrdate_event
                    )
                ));
        } else {
            wp_send_json(array(
                'result' => array(
                    'error' => true,
                    'data' => array(
                        'msg' => isset($m_error['message']) ? $m_error['message'] : '',
                        )
                    )
                ));

        }


    }

    public function delete_event_times()
    {
        $eid = $_GET['eid'];

        $date_event = $_GET['date_event'];
        $is_front = isset($_GET['is_front']);
        $_pa = implode("|", Apollo_Calendar::$arrWeeks);
        if (preg_match('/' . $_pa . '/i', $date_event) > 0) {
            wp_send_json(array(
                'result' => Apollo_Calendar::specialDelete($eid, $date_event)
                ));

            $logObj = new Apollo_Logs("(specialDelete) Delete the event date/time of event_id = $eid, date_event = $date_event", __FUNCTION__, $is_front);
            $logObj->logs();

        }

        $logObj = new Apollo_Logs("(deleteEventByListDate) Delete the event date/time of event_id = $eid, date_event = $date_event", __FUNCTION__, $is_front);
        $logObj->logs();

        wp_send_json(array(
            'result' => Apollo_Calendar::deleteEventByListDate($eid, $date_event)
            ));
    }

    //edit:
    public function      edit_event_time()
    {

        $date_event = $_GET['date_event'];
        $event_id = $_GET['event_id'];
        $otime_from = $_GET['otime_from'];
        $otime_to = isset($_GET['otime_to']) ? $_GET['otime_to'] : '';
        $time_from = $_GET['time_from'];
        $time_to = isset($_GET['time_to']) ? $_GET['time_to'] : '';


        $_p = implode("|", Apollo_Calendar::$arrWeeks);

        if (preg_match('/' . $_p . '/i', $date_event) >= 1) {
            $arrdate_event = Apollo_Calendar::getStepsDateByDay($event_id, $date_event, $date_event);
            $r = Apollo_Calendar::editEvent($arrdate_event, $event_id, $otime_from, $otime_to, $time_from, $time_to);

            if ($r) {
                wp_send_json(array(
                    'result' => array(
                        'error' => false,
                        'data' => $arrdate_event
                        )
                    ));
            }

        } else {
            $r = Apollo_Calendar::editEvent($date_event, $event_id, $otime_from, $otime_to, $time_from, $time_to);
        }

        if ($r) {
            wp_send_json(array(
                'result' => true,
                ));
        } else {
            wp_send_json(array(
                'result' => $r
                ));
        }

    }

    /**
     * Get list org for the adding event
     */
    public function get_orgs()
    {
        global $wpdb;
        $s = Apollo_App::clean_data_request($_GET['s']);
        $sql = "
        SELECT p.post_title, p.ID FROM $wpdb->posts p
        WHERE 1 = 1 AND p.post_title LIKE '%" . $s . "%'
        AND p.post_type = '" . Apollo_DB_Schema::_ORGANIZATION_PT . "'
        AND p.post_status = 'publish'
        ORDER BY p.post_title DESC
        LIMIT 40
        ";

        wp_send_json($wpdb->get_results($sql));
    }

    /**
     * Get more invidual date time event
     */

    /**
     * TriLM: delete all event in calendar cell, get event disabled cell, delete all disabled cell
     *
     */
    public function  delete_all_event_in_cell()
    {

        $date_event = $_GET['date_event'];
        $is_front = isset($_GET['is_front']);
        $event_id = $_GET['eid'];
        $r = Apollo_Calendar::deleteAllEventInCell($event_id, $date_event);

        $logObj = new Apollo_Logs("Delete all the event date/time event_id = $event_id and date_event = $date_event", __FUNCTION__, $is_front);
        $logObj->logs();

        //TriLM: update disable cell for event
        $meta = Apollo_App::apollo_get_meta_data($event_id, Apollo_DB_Schema::_APL_EVENT_DISABLE_CALENDAR_CELL);
        if (!is_array($meta) || count($meta) == 0) {
            $meta = array($date_event);
        } else {
            if (!in_array($date_event, $meta)) {
                $meta[] = $date_event;
            }
        }
        //update disable cell for this event
        update_apollo_meta($event_id,
            Apollo_DB_Schema::_APL_EVENT_DISABLE_CALENDAR_CELL, $meta);
        //end update disable cell for event


        if ($r) {
            wp_send_json(array(
                'result' => true,
                ));
        } else {
            wp_send_json(array(
                'result' => $r
                ));
        }

    }

    public function  get_disable_cell_event()
    {
        $event_id = $_GET['event_id'];
        $data = Apollo_Calendar::static_get_disable_cell($event_id);
        wp_send_json($data);
    }

    public function  delete_all_disable_cell_event()
    {
        $event_id = $_GET['event_id'];
        update_apollo_meta($event_id, Apollo_DB_Schema::_APL_EVENT_DISABLE_CALENDAR_CELL, array());
        wp_send_json(array(
            'result' => 1
            ));
    }


    /**
    ** Author : ThienLD
     * Description : implement function allow save event additional time content on AJAX saving Request in Step 2b
     * Created Date : 2016-11-08
     */
    public function  update_additional_time_content_in_step2b()
    {
        try{
            $eID = isset($_POST['event_id']) ? intval($_POST['event_id']) : 0;
            $content = isset($_POST['content']) ? $_POST['content'] : "";
            $metaSerialize = Apollo_App::apollo_get_meta_data($eID, Apollo_DB_Schema::_APOLLO_EVENT_DATA);
            $meta = maybe_unserialize($metaSerialize);
            $meta[Apollo_DB_Schema::_APL_EVENT_ADDITIONAL_TIME] = Apollo_App::convertCkEditorToTinyCME(wp_unslash($content));

            // Force event status is 'pending' in case of saving additional time content on saving new event, in editing event and has already status = 'publish' => SKIP
            $eventData = get_post($eID);
            if (!empty($eventData)) {
                if (isset($_POST['apl-fe-save-draft']) && $_POST['apl-fe-save-draft']) {
                    /** save draft*/
                    if ($eventData->post_status != 'draft') {
                        $eventData->post_status = 'draft';
                        wp_update_post($eventData);
                    }
                } else if ($eventData->post_status == 'draft') {
                    /**
                     * @ticket #19007: Modify the Draft labels in event
                     */
                    $eventData->post_status = Apollo_App::bypassEventPendingApproveLoggedinUser() ? 'publish' : 'unconfirmed';
                    $meta[Apollo_DB_Schema::_E_USER_DRAFT] = '';
                    wp_update_post($eventData);
                }
            }

            update_apollo_meta($eID, Apollo_DB_Schema::_APOLLO_EVENT_DATA, $meta);

            wp_send_json(array(
                'success' => "TRUE",
                "error" => ""
            ));
        } catch (Exception $ex) {
            wp_send_json(array(
                'success' => "FALSE",
                "error" => $ex->getMessage()
            ));
        }
        exit;
    }

    public function  update_additional_time()
    {
        $eID = $_GET['event_id'];
        $content = $_GET['content'];
        $metaSerialize = Apollo_App::apollo_get_meta_data($eID, Apollo_DB_Schema::_APOLLO_EVENT_DATA);
        $meta = maybe_unserialize($metaSerialize);
        $meta[Apollo_DB_Schema::_APL_EVENT_ADDITIONAL_TIME] = Apollo_App::convertCkEditorToTinyCME(wp_unslash($content));
        update_apollo_meta($eID, Apollo_DB_Schema::_APOLLO_EVENT_DATA, $meta);
        wp_send_json(array(
            'result' => 1
            ));
    }

    public function  restore_event_time_cell()
    {
        $date_event = $_GET['date_event'];
        $event_id = $_GET['eid'];
        $meta = Apollo_App::apollo_get_meta_data($event_id, Apollo_DB_Schema::_APL_EVENT_DISABLE_CALENDAR_CELL);

        if (is_array($meta) && count($meta) > 0) {
            if (in_array($date_event, $meta)) {
                $index = array_search($date_event, $meta);
                unset($meta[$index]);
            }
        }
        //update disable cell for this event
        update_apollo_meta($event_id,
            Apollo_DB_Schema::_APL_EVENT_DISABLE_CALENDAR_CELL, $meta);
        //end update disable cell for event

        wp_send_json(array(
            'result' => true,
            ));

    }

    public function delete_event_times_in_range()
    {
        $is_front = isset($_GET['is_front']);
        $eventID = isset($_GET['event_id']) ? $_GET['event_id'] : '';
        $startDateNew = isset($_GET['time_start_new']) ? $_GET['time_start_new'] : '';
        $endDateNew = isset($_GET['time_end_new']) ? $_GET['time_end_new'] : '';
        $startDateOld = isset($_GET['time_start_old']) ? $_GET['time_start_old'] : '';
        $endDateOld = isset($_GET['time_end_old']) ? $_GET['time_end_old'] : '';
        if (empty($eventID)) {
            wp_send_json(array(
                'result' => false,
                ));
        }
        if (empty($startDateNew) || empty($endDateNew) || empty($startDateOld) || empty($endDateOld)) {
            wp_send_json(array(
                'result' => false,
                ));
        }
        $res = Apollo_Calendar::deleteInRange($eventID, $startDateNew, $endDateNew, $startDateOld, $endDateOld);

        $logObj = new Apollo_Logs("Delete in range event_id = $eventID, start_date_new = $startDateNew, "
            . "end_date_new = $endDateNew, start_date_new = $startDateOld, end_date_new = $endDateOld", __FUNCTION__, $is_front);
        $logObj->logs();

        wp_send_json(array(
            'result' => $res,
            ));
    }

    /**
     * End delete cell event  calendar
     *
     */

    public function get_more_event_time()
    {
        $event_id = Apollo_App::clean_data_request($_REQUEST['event_id']);
        $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 2;
        $event = get_event(get_post($event_id));

        if (!$event->post) die ();

        $pagesize = Apollo_Display_Config::MAX_DATETIME;
        $offset = ($page - 1) * $pagesize;
        $arr_data = $event->get_periods_time(" LIMIT $offset, $pagesize ");

        /* CREATE NEW REQUEST URL */
        $page++;

        /* SEND DATA */
        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_get_more_event_time&page=' . $page . '&event_id=' . $event_id),
            'html' => $event->render_periods_time($arr_data),
            'have_more' => $event->have_more_periods_time($offset),
            ));

        wp_send_json(array());
    }

    public function save_coordinate()
    {

        if (!wp_verify_nonce($_GET['nonce'], 'save-coor'))
            wp_send_json(array(
                'status' => 'FAIL',
                'error' => WP_DEBUG ? 'check nonce fail' : '',
                ));

        $lat = isset($_GET['lat']) ? $_GET['lat'] : '';
        $lng = isset($_GET['lng']) ? $_GET['lng'] : '';
        $address = isset($_GET['addr']) ? sanitize_text_field($_GET['addr']) : '';

        if (empty($lat) || empty($lng) || empty($address)) {
            wp_die('');
        }

        $r = Apollo_Google_Coor_Cache::saveCoorToLocalDB($lat, $lng, $address);

        if (!$r instanceof WP_Error) {
            wp_send_json(array(
                'status' => 'OK'
                ));
        }

        wp_send_json(array(
            'status' => 'FAIL',
            'error' => WP_DEBUG ? print_r($r->get_error_messages(), true) : '',
            ));
    }

    public function get_schools_district()
    {
        $parentID = Apollo_App::clean_data_request($_GET['id']);
        $schools = get_terms('district-school', array('order' => 'ASC', 'orderby' => 'name', 'hide_empty' => false, 'parent' => $parentID));
        if (!$schools) {
            wp_send_json(array('html' => ''));
            exit;
        }
        $html = '';
        foreach ($schools as $school) {
            $html .= '<option value="' . $school->term_id . '">' . $school->name . '</option>';
        }
        wp_send_json(array('html' => $html));
    }

    public function get_programs_educator()
    {
        $edu_id = Apollo_App::clean_data_request($_GET['id']);
        $eduObj = get_educator($edu_id);
        if (!$eduObj) {
            wp_send_json(array('html' => ''));
            exit;
        }
        $progs = $eduObj->get_programs();
        if (!$progs) {
            wp_send_json(array('html' => ''));
            exit;
        }
        $html = '';
        foreach ($progs as $prog) {
            $prog = get_program($prog->prog_id);
            $html .= '<option value="' . $prog->post->ID . '">' . $prog->get_title() . '</option>';
        }
        wp_send_json(array('html' => $html));
    }

    function init_captcha()
    {
        if (!isset($_SESSION['_APOLLO_CAPTCHA_COMMENT'])) {
            $captcha = new Apollo_Captcha();
            $commenter = wp_get_current_commenter();    /* get captcha again */
            $commenter['captcha_image_path'] = $captcha->getImagePath();
            wp_send_json(array('src' => $captcha->getImagePath()));
        }
    }

    public function load_edit_classified_html()
    {
        ob_start();
        include APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/partial/_classified/_edit_selectbox.php';
        $content = ob_get_clean();

        wp_send_json(array(
            'html' => $content,
            ));
    }

    public function show_more_org_classified()
    {

        include_once APOLLO_TEMPLATES_DIR . '/classified/list-classifieds.php';
        $page = isset($_REQUEST['page']) ? max(1, intval($_REQUEST['page'])) : 1;
        $orgId = isset($_REQUEST['current_org_id']) ? $_REQUEST['current_org_id'] : '';

        $adapter = new List_Classified_Adapter($page, $this->get_pagesize_view_more());
        $html = $adapter->get_org_classified($orgId);
        $page++;
        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_show_more_org_classified&page=' . $page . '&current_org_id=' . $orgId),
            'html' => $html,
            'have_more' => $adapter->isShowMore(),

            ));
    }

    public function show_more_artist_public_art()
    {

        include_once APOLLO_TEMPLATES_DIR . '/public-art/list-public-arts.php';
        $page = isset($_REQUEST['page']) ? max(1, intval($_REQUEST['page'])) : 1;
        $artistId = isset($_REQUEST['current_artist_id']) ? $_REQUEST['current_artist_id'] : '';

        $adapter = new List_Public_Art_Adapter($page, $this->get_pagesize_view_more());
        $html = $adapter->get_artist_public_art($artistId);
        $page++;
        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_show_more_artist_public_art&page=' . $page . '&current_artist_id=' . $artistId),
            'html' => $html,
            'have_more' => $adapter->isShowMore(),

            ));
    }

    public function get_public_art_around_you()
    {
        global $wpdb;
        $pubArtMetaTbl = $wpdb->{Apollo_Tables::_APL_PUBLIC_ART_META};
        $postTbl = $wpdb->posts;
        $mapData = isset($_REQUEST['mapData'])?$_REQUEST['mapData']:array();
        $minLat = isset($mapData['min_lat']) ? $mapData['min_lat'] : -99999;
        $maxLat = isset($mapData['min_lat']) ? $mapData['max_lat'] : 99999;
        $minLng = isset($mapData['min_lng']) ? $mapData['min_lng'] : -99999;
        $maxLng = isset($mapData['max_lng']) ? $mapData['max_lng'] : 99999;

        $currentLat = isset($mapData['my_pos_lat']) ? $mapData['my_pos_lat'] : 0;
        $currentLng = isset($mapData['my_pos_lng']) ? $mapData['my_pos_lng'] : 0;

        $sql = "
        SELECT
        p.post_title,
        p.post_content,
        mt1.meta_value as lat,
        mt2.meta_value as lng,
        p.ID as id
        FROM $postTbl p "
        . "INNER JOIN $pubArtMetaTbl mt1 ON mt1.apollo_public_art_id = p.ID"
        . " AND mt1.meta_key = '" . Apollo_DB_Schema::_APL_PUBLIC_ART_LATITUDE . "' AND mt1.meta_value >= $minLat AND mt1.meta_value <= $maxLat "
        . "INNER JOIN $pubArtMetaTbl mt2 ON mt2.apollo_public_art_id = p.ID"
        . " AND mt2.meta_key = '" . Apollo_DB_Schema::_APL_PUBLIC_ART_LONGITUDE . "' AND mt2.meta_value >= $minLng AND mt2.meta_value <= $maxLng "
        . "WHERE p.post_status = 'publish'
        ORDER BY p.post_title   LIMIT 1000 ";

        $result = $wpdb->get_results($sql);
        $data =  $this->ajaxResault($result);
        $radius = 1.609344; //miles

        $data = Apollo_Map_Calc::queryByRadius($data, $currentLat, $currentLng, $radius);

        wp_send_json(array(
            'data' => $data
            ));
    }

    public function get_all_public_art()
    {
        $radius = '';
        $currentLat= '';
        $currentLng= '';
        $limit = of_get_option(Apollo_DB_Schema::_PUBLIC_ART_SETTING_LIMIT, Apollo_Display_Config::_PUBLIC_ART_SETTING_LIMIT_STD);
        $arr_params = array(
            'post_type'         => Apollo_DB_Schema::_PUBLIC_ART_PT,
            'post_status'       => array('publish'),
            'posts_per_page'    => $limit,
            'order'             => 'asc',
            'orderby'           => 'post_title'
            );

        if(isset($_GET['keyword']) && !empty($_GET['keyword'])) {
            $arr_params['s'] =  $_GET['keyword'];
        }
        if(isset($_GET['term']) && !empty($_GET['term'])) {
            $arr_tax_query[] = array(
                'taxonomy'=> Apollo_DB_Schema::_PUBLIC_ART_PT.'-type',
                'terms' => array($_GET['term']),
                );
            $arr_params['tax_query'] = $arr_tax_query;
        }
        if(isset($_GET['collection']) && !empty($_GET['collection'])) {
            $arr_tax_query[] = array(
                'taxonomy'=> Apollo_DB_Schema::_PUBLIC_ART_COL,
                'terms' => array($_GET['collection']),
                );
            $arr_params['tax_query'] = $arr_tax_query;
        }
        if(isset($_GET['location']) && !empty($_GET['location'])) {
            $arr_tax_query[] = array(
                'taxonomy'=> Apollo_DB_Schema::_PUBLIC_ART_LOC,
                'terms' => array($_GET['location']),
                );
            $arr_params['tax_query'] = $arr_tax_query;
        }
        if(isset($_GET['medium']) && !empty($_GET['medium'])) {
            $arr_tax_query[] = array(
                'taxonomy'=> Apollo_DB_Schema::_PUBLIC_ART_MED,
                'terms' => array($_GET['medium']),
                );
            $arr_params['tax_query'] = $arr_tax_query;
        }
        if(isset($_GET['radius']) && !empty($_GET['radius'])) {
            $radius = $_GET['radius'];
        }
        if(isset($_GET['currentLat']) && !empty($_GET['currentLat'])) {
            $currentLat = $_GET['currentLat'];
        }
        if(isset($_GET['currentLng']) && !empty($_GET['currentLng'])) {
            $currentLng= $_GET['currentLng'];
        }

        $result = query_posts($arr_params);
        wp_reset_query();
        $data = $this->ajaxResault($result);
        //disable outside marker here
        $data = Apollo_Map_Calc::queryByRadius($data, $currentLat, $currentLng, $radius);

        wp_send_json(array(
            'data' => $data
            ));

    }

    protected function  ajaxResault($result)
    {
        $dataReturn = array();
        if($result){
            foreach ($result as $r) {
                $id = isset($r->ID)? $r->ID: $r->id;
                $public_art = get_public_art($r);
                $data = $public_art->getDataDetail();
                $address = $str_address_all = $public_art->get_full_address();
                $defaultImage = $public_art->get_thumb_image_url();
                $dataReturn[] = array(
                    'lat' => floatval($data['latitude']),
                    'lng' => floatval($data['longitude']),
                    'post_title' => $r->post_title,
                    'link' => isset($data['web'])?$data['web']:get_permalink($id),
                    'id' => $id,
                    'thumbnail' => $defaultImage,
                    'address' => $address,
                    'description' => wp_trim_words($r->post_content, 50),
                    'title' => wp_trim_words($r->post_title, 3,'...'),
                    'sitelink' => get_permalink($id),
                    'marker_icon' => isset($data['marker_icon'])?$data['marker_icon']:''
                );
            }
        }
        return $dataReturn;
    }

    function get_cities_state() {
        $state = isset($_REQUEST['state']) ? $_REQUEST['state'] : '';
        if (!$state) {
            wp_send_json(array(
                'html' => '',
                'msg'   => 'State can not empty'
                ));
        }

        $html = '';
        if ( $cities = Apollo_App::getCityByTerritory(false, $state, false) ) {

            if($cities) {
                foreach( $cities as $key => $city ) {
                    $html .= '<option value="'.$key.'">'.$city.'</option>';
                }
            }

            wp_send_json(array(
                'html' => $html,
                'msg'   => 'Susscess'
                ));
        }
        wp_send_json(array(
            'html' => '',
            'msg'   => 'No data'
            ));
    }

    function get_zipcodes_city() {
        $state = isset($_REQUEST['state']) ? $_REQUEST['state'] : '';
        $city = isset($_REQUEST['city']) ? $_REQUEST['city'] : '';
        $html = '';

        if (!$state || !$city) {
            wp_send_json(array(
                'html' => '',
                'msg'   => 'city or state can not empty'
                ));
        }

        if ( $zipcodes = Apollo_App::getZipByTerritory(false, $state, $city, false) ) {

            if($zipcodes) {
                foreach( $zipcodes as $zipcode ) {
                    $html .= '<option value="'.$zipcode.'">'.$zipcode.'</option>';
                }
            }

            wp_send_json(array(
                'html' => $html,
                'msg'   => 'Susscess'
                ));
        }
        wp_send_json(array(
            'html' => $html,
            'msg'   => 'No data'
            ));
    }

    function get_more_offer_dates_times() {
        /* REPAIR DATA TO RETURN */
        $page = isset($_REQUEST['page']) ? max(1, intval($_REQUEST['page'])) : 1;
        $event_id = isset($_REQUEST['event_id']) ? max(1, intval($_REQUEST['event_id'])) : 1;

        $page_size = of_get_option(Apollo_Display_Config::_NUMBER_OFFER_DATE_TIME, 5);

        /* CREATE NEW REQUEST URL */

        include APOLLO_WIDGETS_DIR. '/offer-dates-times/class-apollo-event-offer-dates-times.php';

        $offerObj = new Apollo_Event_Offer_Dates_Time($event_id, $page);
        $offerObj->getOffers();
        if ($offerObj->isEmpty()) return;

        $htmlItems = $offerObj->getHtmlItem();
        $page++;

        /* SEND DATA */
        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_get_more_offer_dates_times&page=' . $page. '&event_id='. $event_id),
            'html' => $htmlItems,
            'have_more' => $offerObj->isShowMore(),
            ));
    }

    function import_save_event() {
        $events = isset($_REQUEST['events']) ? $_REQUEST['events'] : false;
        $filename = $_REQUEST['filename'];
        include APOLLO_INCLUDES_DIR. '/scripts/class-apollo-event-import.php';
        //$events = Apollo_Event_Import::extractPassImportEventData($events);
        $import = new Apollo_Event_Import($events, $filename);
        $result = $import->saveImport();

        if ($result) {
            wp_send_json(array(
                'msg'   => $result ? __('Events imported successfully.', 'apollo') : __('Error', 'apollo'),
                'success'   => true,
                ));
        } else {
            wp_send_json(array(
                'msg'   => __('There are some errors in your submitted data. Please check them again !', 'apollo'),
                'success' => false,
                ));
        }

    }

    function import_save_event_for_later() {
        $events = isset($_REQUEST['events']) ? $_REQUEST['events'] : false;
        $filename = $_REQUEST['filename'];

        $tmpFile = isset($_REQUEST['tmpFile']) ? $_REQUEST['tmpFile'] : '';

        include APOLLO_INCLUDES_DIR. '/scripts/class-apollo-event-import.php';

        //$events = Apollo_Event_Import::extractPassImportEventData($events, $filename);

        // Add to tmp file
        if ($events && is_array($events)) {

            if ($tmpFile == 'false') {
                $tmpFile = Apollo_Event_Import::getTmpFullPath();
            }

            $eventsTmpFile = [];
            if (file_exists($tmpFile)) {
                $eventsTmpFile = json_decode(file_get_contents($tmpFile));
            }

            if ($eventsTmpFile == null || !$eventsTmpFile) $eventsTmpFile = [];

            @file_put_contents($tmpFile, json_encode(array_merge($eventsTmpFile, $events)));

            wp_send_json(array(
                'tmpFile'      => $tmpFile,
                'isMerging'   => true
                ));
            exit;
        }

        $isEdit = false;

        // Get data from tmp file
        $events = ($eventsTmpFile = @file_get_contents($tmpFile)) ? json_decode($eventsTmpFile) : [];

        $import = new Apollo_Event_Import($events, $filename);

        // Remove tmp file
        @unlink($tmpFile);

        if ( $events ) {
            foreach ($events as $i => &$event) {
                $event = (array) $event;
                if (!empty($event['eventname'])) {
                    $event['eventname'] = Apollo_App::clean_data_request($event['eventname']);
                }

                if (!empty($event['description'])) {
                    $event['description'] = Apollo_App::clean_data_request($event['description']);
                }

                if (! $import->validate($event)) {
                    unset($events[$i]);
                }
            }
        }

        // New events
        $import->setEvents($events);

        // Insert events to xml file
        try {

            $xml = false;
            $import->arrayToXml($events, $xml);
            $save = $import->xmlToFile($xml);

            if ($save) {
                $isEdit = $import->getIsEdit();
                if ($isEdit) {
                    $import->updateImportForLater();
                } else {
                    $import->insertImportForLater();
                }
            }
            $xmlUrl = $import->getXmlUrl();
            $msg = $save ? __('Successfully saved for later.', 'apollo') : __('Could not save file, please check the right of destination directory', 'apollo');
            wp_send_json(array(
                'msg'       =>  $msg,
                'success'   => $save === true,
                'fileurl'      => $xmlUrl,
                'isEdit'    => $isEdit
                ));

        } catch (Exception $ex) {

            wp_send_json(array(
                'msg'   => __('Could not save file, please check the right of destination directory', 'apollo'),
                'success'   =>  false
                ));
        }

    }

    private function _checkAllowedImportFileType($filename){
        $filename = basename($filename);
        $fileArr = explode('.', $filename);
        $ext = !empty($fileArr) ? end($fileArr) : '';
        return strtolower($ext) == 'csv';
    }

    public function import_event_by_csv_file(){
        if(!isset($_FILES['file'])
            || empty($_FILES['file'])
            || intval($_FILES['file']['error']) !== 0
            ){
            wp_send_json(array(
                'msg'   => __('Import process failed. please try again !', 'apollo'),
                'success'   =>  false
                ));
    }

    if( !$this->_checkAllowedImportFileType($_FILES['file']['name']) ){
        wp_send_json(array(
            'msg'   => __('Incorrect import file type. It allows only CSV file type.', 'apollo'),
            'success'   =>  false
            ));
    }

    if( intval($_FILES['file']['size']) > (self::MAX_IMPORTED_CSV_FILE_SIZE * 1024 * 1024) ){
        wp_send_json(array(
            'msg'   => __('The uploaded file exceeds max upload file size. Max uploaded file size is '. self::MAX_IMPORTED_CSV_FILE_SIZE .' !', 'apollo'),
            'success'   =>  false
            ));
    }
    require_once APOLLO_INCLUDES_DIR. '/scripts/class-apollo-event-import.php';
    $import = new Apollo_Event_Import();

    $data = $import->importEventByCSVFileHandler($_FILES['file']);

    if ($data && isset($data['success']) && $data['success'] === true) {
        $importedData = $data['data'];
        foreach($importedData as $i => $item) {
            if (!empty($item['venuename'])) {
                $p = get_post($item['venuename']);
                $importedData[$i]['_venuename'] = $p ? $p->post_title : $item['venuename'];
            }

            if (!empty($item['orgname'])) {
                $p = get_post($item['orgname']);
                $importedData[$i]['_orgname'] = $p ? $p->post_title : $item['orgname'];
            }
        }
        wp_send_json(array(
            'msg'   => __('Done', 'apollo'),
            'success'   =>  true,
            'data'  => $importedData
            ));
    }

    wp_send_json($data);
}

    function load_event_import_data() {
        $filename = $_GET['filename'];
        include APOLLO_INCLUDES_DIR. '/scripts/class-apollo-event-import.php';
        $import = new Apollo_Event_Import(array(), $filename);

        $data = $import->xmlToArray();
        if ($data) {
            foreach($data as $i => $item) {
                if (!empty($item['venuename'])) {
                    $p = get_post($item['venuename']);
                    $data[$i]['_venuename'] = $p ? $p->post_title : $item['venuename'];
                }

                if (!empty($item['orgname'])) {
                    $p = get_post($item['orgname']);
                    $data[$i]['_orgname'] = $p ? $p->post_title : $item['orgname'];
                }
            }
        }

        if ($data) {
            wp_send_json(array(
                'msg'   => __('Done', 'apollo'),
                'success'   =>  true,
                'data'  => $data
                ));
        }
    }

    function update_associated_user_event() {
        $userId = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : '';
        $postId = isset($_REQUEST['post_id']) ? $_REQUEST['post_id'] : '';
        if($userId == '' || $postId == ''){
            echo 'false';
            exit;
        }
        $my_post = array(
            'ID'           => $postId,
            'post_author'   => $userId,
            );
        $result = wp_update_post( $my_post );

        if ( $result ) {
         echo 'true';
     } else {
         echo 'false';
     }
     exit;
    }

    function get_what_near_by() {
        $minLat = $_REQUEST['min_lat'];
        $maxLat = $_REQUEST['max_lat'];
        $minLng = $_REQUEST['min_lng'];
        $maxLng = $_REQUEST['max_lng'];

        $data = array();

        $activeBusTypes = of_get_option(APL_Business_Module_Theme_Option::DISPLAY_BUSINESS_TYPES);

        $defaultMarkers = Apollo_App::getDefaultMarkersBusIcons();

        if ($activeBusTypes) {

            foreach($activeBusTypes as $activeType => $val) {

                if (!$val) continue;

                $selectObj = array(
                    'text'  => of_get_option($activeType.'_text'),
                    'marker'  => of_get_option($activeType.'_marker'),
                    'val'  => of_get_option($activeType),
                    );

                if (!$selectObj['val']) continue;
                $diningData = $this->getBusinessByType($selectObj['val'], $minLat, $maxLat, $minLng, $maxLng);
                $data[$activeType] = array(
                    'data' => $diningData,
                    'iconSrc' => $selectObj['marker'] ? $selectObj['marker'] : $defaultMarkers[$activeType]['marker'],
                    'text' => $selectObj['text']
                    );
            }
        }


        wp_send_json(array(
            'msg'   => __('Done', 'apollo'),
            'success'   =>  true,
            'data'  => $data
            ));

    }
    public function check_thumbnail_youtube(){
        $url = $_REQUEST['url'];
        $headers = @get_headers($url);
        if( strpos($headers[0],'200') === false) {
            echo  'false';
        }
        else {
            echo 'true';
        }
    }

    /**
     * @Ticket #14857
     * Get thumbnail for video
     */
    public function get_video_thumbnail() {
        $videos = isset($_POST['videos']) ? $_POST['videos'] : array();
        $dataResponse = array();
        if (!empty($videos)) {
            require_once APOLLO_INCLUDES_DIR . '/class-apollo-video.php';
            foreach ($videos as $video) {
                $thumb = new Apollo_Video($video['embed']);
                $dataResponse[] = array(
                    'code'  => $video['code'],
                    'thumb' => $thumb->getThumb()
                );
            }
        }
        wp_send_json(array(
            'data' => $dataResponse
        ));
    }

    private function getBusinessByType($termID, $minLat, $maxLat, $minLng, $maxLng) {
        global $wpdb;

        $mtTbl = $wpdb->{Apollo_Tables::_APOLLO_BUSINESS_META};
        $termID = intval($termID);
        $enableBussiness = Apollo_App::checkEnableBusiness();
        $sql = "
        SELECT p.*, mtLat.meta_value as lat, mtLng.meta_value as lng  FROM {$wpdb->posts} p

        INNER JOIN $wpdb->term_relationships  tr ON p.ID = tr.object_id
        INNER JOIN $wpdb->term_taxonomy tt ON tr.term_taxonomy_id = tt.term_taxonomy_id AND tt.term_id = '$termID'

        INNER JOIN $mtTbl mtLat ON mtLat.apollo_business_id = p.ID AND
        (mtLat.meta_key = '".Apollo_DB_Schema::_BUSINESS_LATITUDE."'
        AND mtLat.meta_value >= $minLat AND mtLat.meta_value <= $maxLat
        )
        INNER JOIN $mtTbl mtLng ON mtLng.apollo_business_id = p.ID AND
        (mtLng.meta_key = '".Apollo_DB_Schema::_BUSINESS_LONGITUDE."'
        AND mtLng.meta_value >= $minLng AND mtLng.meta_value <= $maxLng
        )

        WHERE p.post_type = '".Apollo_DB_Schema::_BUSINESS_PT."'
        AND p.post_status = 'publish'
        AND p.ID IN ( {$enableBussiness} )
        ORDER BY p.post_title
        ";

        $results = $wpdb->get_results($sql);
        $output = array();
        if ($results) {
            $websiteText = __('Website', 'apollo');
            foreach( $results as $v ) {
                $b = get_business($v->ID);
                $orgID = $b->get_meta_data(Apollo_DB_Schema::_APL_BUSINESS_ORG);
                $orgObj = $orgID ? get_org($orgID) : false;
                $output[] = array(
                    'post_title'    => $v->post_title,
                    'url'       => $b->get_permalink(),
                    'lat'   => $v->lat,
                    'lng'   => $v->lng,
                    'address'   => $orgObj ? $orgObj->get_meta_data(Apollo_DB_Schema::_ORG_ADDRESS1, Apollo_DB_Schema::_APL_ORG_ADDRESS) : '',
                    'phone' => $orgObj ? $orgObj->get_meta_data(Apollo_DB_Schema::_ORG_PHONE, Apollo_DB_Schema::_APL_ORG_DATA) : '',
                    'website_url' => $orgObj ? $orgObj->get_meta_data(Apollo_DB_Schema::_ORG_WEBSITE_URL, Apollo_DB_Schema::_APL_ORG_DATA) : '',
                    'website_text'  => $websiteText,
                    );
            }
        }
        return $output;
    }

    /**
     * @author: TriLM
     * solr search
     *
     */
    public function solr_search(){
        $term = isset($_GET['term'])?$_GET['term']:'';
        $postType = isset($_GET['post_type'])?$_GET['post_type']:'event';
        if(!empty($term)){
            $searchModule = new Apollo_Solr_Search();
            $term = str_replace(':', '', $term);

            $data = $searchModule->search(array(
                'title' =>  $term,
                'category'  => $postType
            ));
            wp_send_json($data);
            exit;
        }
        wp_send_json(array());
        exit;
    }

    public function syndication_api() {
        apl_instance('APL_Lib_Apis_Syndication');
    }

    public function get_user_asscoc_list(){
        $currentBlogID = get_current_blog_id();
        $args = array(
            'blog_id'      => $currentBlogID,
            'orderby'      => 'user_nicename',
            'order'        => 'ASC',
            );
        $listUsers = array();
        $users = get_users( $args );

        if(!empty($users)){
            /** @Ticket #14602 */
            foreach ($users as $value){
                $listUsers[] = array(
                    'id' => $value->ID,
                    'user_nicename' => $value->user_nicename
                );
            }
        }

        wp_send_json(
          array('list' => $listUsers)
          );
        exit;
    }

    public function sendmail_tell_a_friend() {
        $email  = Apollo_App::clean_data_request($_REQUEST['email']);
        $link_share  = Apollo_App::clean_data_request($_REQUEST['link_share']);
        $class = 'Apollo_Email_'. Apollo_App::processClassAction('share_post_with_friend');
        $instance = new $class();
        $result = $instance->trigger($email, $link_share);
        wp_send_json(array(
            'success'=> $result,
        ));
    }
}

new Apollo_AJAX();
