<?php

/**
 * TriLM: customize upload dir
 *
 */
class Class_Upload_Path_Custom
{
    const UPLOAD_PATH = 'upload_path';
    const UPLOAD_URL_PATH = 'upload_url_path';
    const SITE_URL = 'siteurl';
    const DOMAIN_UPLOAD_SITE = 'domain_upload_site';
    const GLOBAL_UPLOAD_DIR = '/uploads/sites/';

    const UPLOAD_SYNDICATION_DIR = 'syndication';

    public static function getConfig($isCustomConfig = false)
    {
        //$domainUploadSite = '/uploads/sites/'. Apollo_App::apollo_get_current_domain();
        $configUpload = self::GLOBAL_UPLOAD_DIR;
        $domainUploadSite = $configUpload . Apollo_App::apollo_get_current_domain();

        //old
        $contentUploadDir = WP_CONTENT_DIR;
        $contentUploadUlr = WP_CONTENT_URL;
    
        //use config in wp_config
        if (self::isValidConfig()) {
            $contentUploadDir = UPLOADS;
            $contentUploadDir = self::uploadDirRule($contentUploadDir);

            $contentUploadUlr = get_option('siteurl') . '/' . UPLOADS;
            $domainUploadSite = '/sites/' . Apollo_App::apollo_get_current_domain();
        }
        $configArray = array(
            self::UPLOAD_PATH => $contentUploadDir,
            self::UPLOAD_URL_PATH => $contentUploadUlr,
            self::DOMAIN_UPLOAD_SITE => $domainUploadSite
        );

        //new
        if ($isCustomConfig) {
            $contentUploadDir = ABSPATH;
            $contentUploadUlr = get_option(self::SITE_URL);
            $domainUploadSite = '/sites/' . Apollo_App::apollo_get_current_domain();
        }
        $uploadDir = get_option(self::UPLOAD_PATH, false);
        $uploadUrl = get_option(self::UPLOAD_URL_PATH, false);

        if (!empty($uploadDir)) {
            $contentUploadDir = self::uploadDirRule($uploadDir);
            $contentUploadUlr = get_option('siteurl') . '/' . $uploadDir;
            $domainUploadSite = '/sites/' . Apollo_App::apollo_get_current_domain();
        }

        if (!empty($uploadUrl)) {
            //internal domain
            if (self::checkInternalDomain($uploadUrl)) {
                $contentUploadUlr = $uploadUrl . '/' . $uploadDir;
            } //external domain
            else {
                $contentUploadUlr = $uploadUrl;
            }
        }

        if (!empty($uploadDir) && !empty($uploadUrl)) {
            $domainUploadSite = '/sites/' . Apollo_App::apollo_get_current_domain();
        }

        $configArray[self::UPLOAD_PATH] = $contentUploadDir;
        $configArray[self::UPLOAD_URL_PATH] = $contentUploadUlr;
        $configArray[self::DOMAIN_UPLOAD_SITE] = $domainUploadSite;
       
        return $configArray;
    }

    public static function getLastPartConfig()
    {
        $upload = UPLOADS;
        if (self::isValidConfig()) {
            return self::getLastPart($upload);
        }
        return '';
    }

    public static function getLastPart($uploadPath)
    {
        $arrayPart = explode('/', $uploadPath);
        if (is_array($arrayPart) && count($arrayPart) > 0) {
            for ($i = count($arrayPart) - 1; $i >= 0; $i--) {
                if (!empty($arrayPart[$i])) {
                    return $arrayPart[$i];
                }
            }
        }
        return $uploadPath;
    }

    public static function isValidConfig()
    {
        if (defined('UPLOADS')) {
                $upload = UPLOADS;
            
            // Defined upload dir outside of source code
            $domainUpload = (strpos($upload, '/') === 0 ? '' : ABSPATH). 
                $upload. '/sites/' . Apollo_App::apollo_get_current_domain();
          
            if (isset($upload) && !empty($upload) && $upload != 'UPLOADS' && is_writeable($domainUpload)) {
                return true;
            }
            return false;
        }
        return false;
    }

    public static function uploadDirRule($uploadDir)
    {
        $currentSiteDir = ABSPATH;
        //wp-content
        if (
            strrpos('wp-content', $uploadDir) !== false
            || strrpos($uploadDir, 'wp-content') !== false
        ) {
            return $currentSiteDir . $uploadDir;
        } //root
        elseif ($uploadDir[0] === '/') {
            return $uploadDir;
        }
        //project dir
        return $currentSiteDir . $uploadDir;
    }

    public static function checkInternalDomain($urlPath)
    {
        $siteUrl = get_option('siteurl');
        if (strrpos($siteUrl, $urlPath) !== false
            || strrpos($urlPath, $siteUrl) !== false
        ) {

            return true;
        }
        return false;
    }

    /**
     * Get upload dir
     * @param bool $uploadPathCustom
     * @author vulh
     * @return string
     */
    public static function getUploadDir($uploadPathCustom = false)
    {
        if (!$uploadPathCustom) {
            $uploadPathCustom = self::getConfig();
        }

        $domain_upload = isset($uploadPathCustom[Class_Upload_Path_Custom::DOMAIN_UPLOAD_SITE]) ?
            $uploadPathCustom[Class_Upload_Path_Custom::DOMAIN_UPLOAD_SITE] : '';

        return $uploadPathCustom[Class_Upload_Path_Custom::UPLOAD_PATH]. $domain_upload;
    }

    /**
     * Get upload url
     * @param bool $uploadPathCustom
     * @author vulh
     * @return string
     */
    public static function getUploadURL($uploadPathCustom = false)
    {
        if (!$uploadPathCustom) {
            $uploadPathCustom = self::getConfig();
        }

        $domain_upload = isset($uploadPathCustom[Class_Upload_Path_Custom::DOMAIN_UPLOAD_SITE]) ?
            $uploadPathCustom[Class_Upload_Path_Custom::DOMAIN_UPLOAD_SITE] : '';

        return $uploadPathCustom[Class_Upload_Path_Custom::UPLOAD_URL_PATH]. $domain_upload;
    }
}