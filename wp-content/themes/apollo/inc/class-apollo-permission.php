<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


class Apollo_Permission {


    public function __construct() {

        $arr_action = array(

        );

        if(!empty($arr_action)) {
            foreach($arr_action as $action_name => $handler) {
                add_action($action_name, $handler, 10, 2);
            }
        }


        // Add filter specific for custome resources
        $arr_filter = array(

        );

        foreach($arr_filter as $filter_name => $handler) {
            add_filter($filter_name, $handler, 10, 2);
        }
    }
}

new Apollo_Permission();