<?php

if (!defined('ABSPATH')) {
    exit; // Silent is golden
}

/*
 * Apollo user for wordpress
 */

class Apollo_User {

    public function __construct() {
        // Hook ajax function for add bookmark
        add_action('wp_ajax_add_bookmark', array($this, 'prefix_ajax_add_bookmark'));
        add_action( 'wp_ajax_nopriv_add_bookmark', array( $this, 'prefix_ajax_add_bookmark' ) );
        
        add_action( 'init', array( $this, 'add_bookmark_table' ) );
        add_action( 'deleted_user', array( $this, 'apollo_delete_user' ) );
    }

    public function addToTotalInfo($meta_key) {
        
    }
    public function apollo_delete_user($user_id) {
        global $wpdb;

        $tbl_blog_user = Apollo_App::getBlogUserTable();

        $r = $wpdb->delete($tbl_blog_user, array(
                'user_id' => $user_id
        ));

        if($r === false) {
            Apollo_Event_System::trigger('delete-user-fail in tblbloguser', array($user_id, 'event_name' => 'delete-user-fail in tblbloguser'));
        }
    }
    
    function prefix_ajax_add_bookmark() {

        if ( Apollo_App::isMaintenanceMode() ) {
            return wp_send_json(
                array(
                    'action' => 'not_login',
                    'message' => __('Sorry for the inconvenience.Our website is currently undergoing scheduled maintenance', 'apollo')
                )
            );
        }
        
    if ( ! is_user_logged_in() || ! Apollo_App::isEnableAddItBtn() ) {
            return wp_send_json( array('action' => 'not_login', 'message' => __('You must login to bookmark!', 'apollo')) );
        }
            
        $user_id    = get_current_user_id();
        $post_id    = Apollo_App::clean_data_request( $_POST['id'] );
        $post_type  = Apollo_App::clean_data_request( $_POST['post_type'] );

        if ( get_post_status($post_id) !== 'publish' ) {
            return wp_send_json( array('action' => 'not_login', 'message' => __('This event does not exist', 'apollo')) );
        }
        
        if ( self::has_bookmark( $post_id ) ) {
            self::remove_bookmark( $user_id, $post_id );
            $action = 'remove';
        } else {
            $issuccess = self::insert_bookmark(
                array(
                    'user_id'       => $user_id,
                    'post_id'       => $post_id,
                    'post_type'     => $post_type,
                    'bookmark_date' => date('Y-m-d'),
                )
            );
            $action = 'add';
        }

        // LOG FUNC
        $dataForLog =  array(
            'user_id' => $user_id,
            'item_id' => $post_id,
            'activity' => $action === 'add' ? Apollo_Activity_System::BOOKMARK : Apollo_Activity_System::UNBOOKMARK,

            'item_type' => $post_type,
            'url' => get_permalink($post_id),
            'title' => get_the_title($post_id),
            'timestamp' => time(),
        );
        Apollo_Event_System::trigger(Apollo_Event_System::ACTIVIY, $dataForLog);

        wp_send_json( array( 'action' => $action ) );
    }
    
    protected static function insert_bookmark( $data ) {


        global $wpdb;
        return $wpdb->insert( $wpdb->{Apollo_Tables::_BOOKMARK}, $data );
    }
    
    protected static function remove_bookmark( $user_id, $post_id ) {
        global $wpdb;
        $wpdb->query( $wpdb->prepare( "DELETE FROM {$wpdb->{Apollo_Tables::_BOOKMARK}} WHERE user_id = %d AND post_id = %d ", $user_id, $post_id ) );
    }
    
    public static function has_bookmark( $post_id ) {
        if( ! $post_id || !is_user_logged_in() ) return false;
        
        $user_id = get_current_user_id();
        
        global $wpdb;
        return $wpdb->query( $wpdb->prepare( "SELECT bookmark_id FROM {$wpdb->{Apollo_Tables::_BOOKMARK}} WHERE user_id = %d AND post_id = %d", $user_id, $post_id ) );
    }
    
    public function add_bookmark_table() {
        global $wpdb;
        $bookmark_table = Apollo_Tables::_BOOKMARK;
        $wpdb->tables[] = $bookmark_table;
        $wpdb->{$bookmark_table} = $wpdb->prefix . $bookmark_table;
    }

    public static function _rmuserFromDB($blog_id, $user_id) {
        global $wpdb;
        $tbl_blog_user = Apollo_App::getBlogUserTable();
        $sql = "DELETE FROM $tbl_blog_user WHERE blog_id = %s and user_id = %s";
        $naffected_rows = $wpdb->query($wpdb->prepare($sql, $blog_id, $user_id));

        if($naffected_rows  >= 1) {
            // delete from wpdb->users
            $sql = "DELETE FROM $wpdb->users WHERE ID = %s";
            $naffected_rows = $wpdb->query($wpdb->prepare($sql, $user_id));

            if($naffected_rows >=1 ) {
                // delete all from wpdb->usermeta
                $sql = "DELETE FROM $wpdb->usermeta WHERE user_id = %s";
                $naffected_rows = $wpdb->query($wpdb->prepare($sql, $user_id));
            }
        }
    }

    public static function getListSuperAdmin()
    {
        global $wpdb;
        $super_admins = get_site_option( 'site_admins', array( 'admin' ) );

        $qsa = '("' . implode("\",\"", $super_admins) . '")';

        $sql = "SELECT * FROM $wpdb->users WHERE user_login in $qsa; ";
        $arruser = $wpdb->get_results($sql);

        return $arruser;
    }

    public static function getListEmailSuperAdmin() {
        $auser = self::getListSuperAdmin();

        $aE = array();

        foreach($auser as $_ => $v) {
            $aE[] = $v->user_email;
        }

        return $aE;
    }

    /**
     * Get current submitting associated id by post_type for form (first item from wp_{site_id}_apollo_user_modules or from query var)
     * @param $postType
     * @param string $userID
     * @return int|string
     */
    public static function getSubmittingAssociatedID( $postType, $userID = '' ) {
        $id = '';
        if (Apollo_App::isInputMultiplePostMode() && $postType != Apollo_DB_Schema::_EDUCATOR_PT) {
            if (get_query_var( '_edit_post_id', 0) !== 0) {
                $id = intval(get_query_var( '_edit_post_id'));
            }
        } else {

            if ( get_query_var( '_is_agency_educator_page' ) == '1' && get_query_var( '_apollo_educator_id' ) ) {
                $id = get_query_var( '_apollo_educator_id' );
            }
            else {
                $id = self::getCurrentAssociatedID($postType, $userID );
            }
        }
        return $id;
    }


    /**
     * Get current association id by post_type
     * @param $postType
     * @param string $userID
     * @param array $status
     * @return int|string
     */
    public static function getCurrentAssociatedID( $postType, $userID = '', $status = array()) {
        global $wpdb;

        $id = '';
        $status = self::_getStatus($status);

        if (!$userID) {
            $userID = get_current_user_id();
        }

        // Get assigned items not agency by admin
        $result = self::getAssignedItems($postType, $userID, " LIMIT 1 ", $status);

        // Get by author
        if (empty($result[0]->post_id)) {
            $statusStr = self::_convertStatusToStr($status);
            $lastName = Apollo_DB_Schema::_APL_ARTIST_LNAME;

            if ($postType == Apollo_DB_Schema::_ARTIST_PT) {
                $sql = " SELECT p.ID as post_id FROM $wpdb->posts p 
                     INNER JOIN {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}} am ON p.ID = am.apollo_artist_id AND am.meta_key = '{$lastName}' 
                     WHERE p.post_type = '{$postType}' AND p.post_status IN ($statusStr) AND post_author = {$userID} 
                     ORDER BY am.meta_value ASC 
                     LIMIT 1 ";
            }
            else {
                $sql = " SELECT ID as post_id FROM $wpdb->posts WHERE post_type = '{$postType}' AND post_status IN ($statusStr) AND post_author = {$userID} ORDER BY post_title LIMIT 1";
            }
            $result = $wpdb->get_results($sql);
        }
        // Get by Agency
        if (empty($result[0]->post_id)) {
            $agencies = self::getAgenciesForUser();
            $result = self::getAssignedItemsForAgenciesUser($postType, $agencies, " LIMIT 1 ", $status);
        }

        if ($result && isset($result[0])) {
            $id = $result[0]->post_id;
        }

        return $id;
    }


    /**
     * Get user id based on the module
     *
     * @param int    $postId
     * @param string $postType (artist, organization, venue, agency, educator)
     *
     * @return array
     * @ticket #15165 - [CF] 20180126 - [2049#c12311] Selected association users on the artist list
     */
    public static function getUserIdBasedOnModule($postId, $postType = 'artist')
    {
        global $wpdb;
        $table  = Apollo_App::getBlogUserTable();
        $module = ($postType == Apollo_DB_Schema::_ORGANIZATION_PT) ? 'org_id' : $postType . '_id';

        $user = $wpdb->get_results($wpdb->prepare(
            "SELECT user_id FROM $table WHERE $module = %d AND blog_id = %d LIMIT 1 ",
            $postId,
            get_current_blog_id()
        ));

        return $user;
    }

    /**
     * Check the list IDs are associated items (are assigned to manage )
     * @param $arrayID (array or int)
     * @param string $postType
     * @return integer
     */
    public static function isAssociatedItems($arrayID, $postType = 'artist') {

        if (!is_array($arrayID)) {
            $arrayID = array($arrayID);
        }
        $strIDs = implode(',', $arrayID);

        // Author
        $postQuery = new Apl_Query('posts');
        $userID = get_current_user_id();
        $ownedTotalPost = $postQuery->get_total(" post_author = $userID AND ID IN ($strIDs) ");
        if ($ownedTotalPost == count($arrayID)) {
            return true;
        }

        // was assigned
        $aplQuery = new Apl_Query(Apollo_Tables::_APL_USER_MODULES);
        if ($aplQuery->get_total(" post_id IN ($strIDs) AND post_type = '$postType' AND user_id = {$userID} ")) {
            return true;
        }

        // Agency
        $agencies = self::getAgenciesForUser($userID);
        $agencyAssociatedIds = self::checkAssociatedByAgency($arrayID, $postType, $agencies);
        /** @Ticket #15971 */
        if (isset($agencyAssociatedIds[0]) && $agencyAssociatedIds[0]->post_id) {
            return true;
        }

        return false;
    }

    /**
     * Check agency has associated
     * @param $arrayID
     * @param $postType
     * @param array $userAgency
     * @return array|bool|null|object
     */
    public static function checkAssociatedByAgency($arrayID, $postType, $userAgency = array())
    {
        global $wpdb;

        if (empty($userAgency) || empty($arrayID)) {
            return false;
        }

        $agencyIds = array_map('current', $userAgency);

        switch ($postType) {
            case Apollo_DB_Schema::_ARTIST_PT :
                $tableName = Apollo_Tables::_APL_AGENCY_ARTIST;
                $fieldName = 'artist_id';
                break;
            case Apollo_DB_Schema::_EDUCATOR_PT :
                $tableName = Apollo_Tables::_APL_AGENCY_EDUCATOR;
                $fieldName = 'edu_id';
                break;
            case Apollo_DB_Schema::_ORGANIZATION_PT :
                $tableName = Apollo_Tables::_APL_AGENCY_ORG;
                $fieldName = 'org_id';
                break;
            case Apollo_DB_Schema::_VENUE_PT :
                $tableName = Apollo_Tables::_APL_AGENCY_VENUE;
                $fieldName = 'venue_id';
                break;
            default :
                $tableName = Apollo_Tables::_APL_AGENCY_ORG;
                $fieldName = 'org_id';
                break;
        }
        $agencyIds = implode(',', $agencyIds);
        $strIDs = implode(',', $arrayID);
        $tbl = $wpdb->{$tableName};
        $sql = "
            SELECT COUNT(*) as post_id FROM $tbl agency_post 
            WHERE agency_post.agency_id IN ({$agencyIds}) AND {$fieldName} IN ($strIDs) 
        ";

        return $wpdb->get_results($sql);
    }

    /**
     * count associated by agency
     * @param $postType
     * @param string $userID
     * @return bool|int
     */
    public static function countAssociatedItemsByAgency($postType, $userID = '') {
        global $wpdb;
        if (!Apollo_App::is_avaiable_module($postType)) {
            return 0;
        }
        if (!$userID) {
            $userID = get_current_user_id();
        }
        $agencies = self::getAgenciesForUser($userID);
        if ($agencies) {
            $agencies = array_map('current', $agencies);
            $postType = ($postType == Apollo_DB_Schema::_EDUCATION) ? Apollo_DB_Schema::_EDUCATOR_PT : $postType;
            switch ($postType) {
                case Apollo_DB_Schema::_ARTIST_PT :
                    $tableName = Apollo_Tables::_APL_AGENCY_ARTIST;
                    $fieldName = 'artist_id';
                    break;
                case Apollo_DB_Schema::_EDUCATOR_PT :
                    $tableName = Apollo_Tables::_APL_AGENCY_EDUCATOR;
                    $fieldName = 'edu_id';
                    break;
                case Apollo_DB_Schema::_ORGANIZATION_PT :
                    $tableName = Apollo_Tables::_APL_AGENCY_ORG;
                    $fieldName = 'org_id';
                    break;
                case Apollo_DB_Schema::_VENUE_PT :
                    $tableName = Apollo_Tables::_APL_AGENCY_VENUE;
                    $fieldName = 'venue_id';
                    break;
                default :
                    $tableName = Apollo_Tables::_APL_AGENCY_ORG;
                    $fieldName = 'org_id';
                    break;
            }
            $agencyIds = implode(",", $agencies);
            $tbl = $wpdb->{$tableName};
            $status = self::_getStatus();
            $statusStr = self::_convertStatusToStr($status);
            $sql = "
                SELECT 1 FROM $tbl md
                INNER JOIN {$wpdb->posts} p ON md.{$fieldName} = p.ID 
                WHERE agency_id IN ({$agencyIds}) AND p.post_status IN ({$statusStr})  
                LIMIT 1 
            ";
            return (int) $wpdb->query($sql);
        }
        return 0;
    }

    /**
     * Get assigned and owner items
     * @param $postType
     * @param string $userID
     * @return array|null|object
     */
    public static function getUserItemIDs($postType = Apollo_DB_Schema::_ORGANIZATION_PT, $userID = '') {

        if (!$userID) {
            $userID = get_current_user_id();
        }

        $result = self::getAssignedItems($postType, $userID);

        /** Get association by userID */
        global $wpdb;
        $ownerItems = $wpdb->get_results( " SELECT ID as post_id FROM {$wpdb->posts} WHERE post_type = '{$postType}' AND post_author = {$userID} AND post_status IN ('publish', 'pending') " );

        $result = array_merge($result, $ownerItems);
        $assignedIds = array();
        if ($result) {
            foreach ($result as $item) {
                $assignedIds[] = $item->post_id;
            }
        }
        // get by agency
        $agencies = self::getAgenciesForUser($userID);
        $agencyAssociatedIds = self::getAssignedItemsForAgenciesUser($postType, $agencies);
        if (!empty($agencyAssociatedIds)) {
            $agencyAssociatedIds = array_map('current', $agencyAssociatedIds);
            $assignedIds = array_merge($assignedIds, $agencyAssociatedIds);
        }

        return $assignedIds;
    }

    /**
     * Only get assigned items
     * @param $postType
     * @param string $userID
     * @param bool $limit
     * @param array $status
     * @return array|null|object
     */
    public static function getAssignedItems($postType, $userID = '', $limit = false, $status = array()) {

        global $wpdb;

        if (!$userID) {
            $userID = get_current_user_id();
        }

        $status = self::_getStatus($status);

        $statusStr = self::_convertStatusToStr($status);
        $aplUserModTbl = $wpdb->{Apollo_Tables::_APL_USER_MODULES};

        if ($postType == Apollo_DB_Schema::_ARTIST_PT) {
            $lastName = Apollo_DB_Schema::_APL_ARTIST_LNAME;
            $sql = " SELECT um.post_id FROM {$aplUserModTbl} um
            INNER JOIN {$wpdb->posts} p ON p.ID =  um.post_id
            INNER JOIN {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}} am ON um.post_id = am.apollo_artist_id AND am.meta_key = '{$lastName}' 
            WHERE um.user_id = {$userID} AND um.post_type = '{$postType}' AND p.post_status IN ($statusStr) 
            ORDER BY am.meta_value ASC 
            $limit ";
        }
        else {
            $sql = "
                SELECT um.post_id FROM {$aplUserModTbl} um 
                INNER JOIN {$wpdb->posts} p ON p.ID = um.post_id 
                WHERE um.post_type = '{$postType}' AND p.post_status IN ($statusStr) AND um.user_id = {$userID}
                $limit
            ";
        }
        $result = $wpdb->get_results($sql);

        return $result;
    }

    /**
     * Get associated item for
     * @param $postType
     * @param array $userAgency
     * @param bool $limit
     * @param bool $status
     * @return array|null|object
     */
    public static function getAssignedItemsForAgenciesUser($postType, $userAgency = array(), $limit = false, $status = false)
    {
        global $wpdb;

        if (empty($userAgency)) {
            return false;
        }

        $agencyIds = array();
        foreach ($userAgency as $item) {
            $agencyIds[] = $item->post_id;
        }

        switch ($postType) {
            case Apollo_DB_Schema::_ARTIST_PT :
                $tableName = Apollo_Tables::_APL_AGENCY_ARTIST;
                $fieldName = 'artist_id';
                break;
            case Apollo_DB_Schema::_EDUCATOR_PT :
                $tableName = Apollo_Tables::_APL_AGENCY_EDUCATOR;
                $fieldName = 'edu_id';
                break;
            case Apollo_DB_Schema::_ORGANIZATION_PT :
                $tableName = Apollo_Tables::_APL_AGENCY_ORG;
                $fieldName = 'org_id';
                break;
            case Apollo_DB_Schema::_VENUE_PT :
                $tableName = Apollo_Tables::_APL_AGENCY_VENUE;
                $fieldName = 'venue_id';
                break;
            default :
                $tableName = Apollo_Tables::_APL_AGENCY_ORG;
                $fieldName = 'org_id';
                break;
        }
        $agencyIds = implode(',', $agencyIds);


        $status = self::_getStatus($status);
        $statusStr = self::_convertStatusToStr($status);

        if ($postType == Apollo_DB_Schema::_ARTIST_PT)
        {
            $agencyArtist = $wpdb->{$tableName};
            $mtKey = Apollo_DB_Schema::_APL_ARTIST_LNAME;
            $sql = "SELECT DISTINCT({$fieldName}) as post_id FROM $agencyArtist aa
            INNER JOIN {$wpdb->posts} p ON p.ID = aa.artist_id
            INNER JOIN {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}} am ON am.apollo_artist_id = aa.artist_id
            WHERE agency_id IN ({$agencyIds}) AND am.meta_key = '$mtKey' AND p.post_status IN ($statusStr)
            ORDER BY am.meta_value ASC 
            $limit
            ";
            return $wpdb->get_results($sql);
        }

        $tbl = $wpdb->{$tableName};
        $sql = "
            SELECT DISTINCT({$fieldName}) as post_id FROM $tbl agency_post 
            INNER JOIN {$wpdb->posts} p ON p.ID = agency_post.{$fieldName}
            WHERE agency_post.agency_id IN ({$agencyIds}) AND p.post_status IN ($statusStr)
            $limit
              
        ";

        return $wpdb->get_results($sql);
    }

    /**
     * get assigned agencies
     * @param string $userID
     * @return array|null|object
     */
    public static function getAgenciesForUser($userID = '') {
        if (!Apollo_App::is_avaiable_module(Apollo_DB_Schema::_AGENCY_PT)) {
            return array();
        }
        if (!$userID) {
            $userID = get_current_user_id();
        }
        $agencyPT = Apollo_DB_Schema::_AGENCY_PT;
        $aplQuery = new Apl_Query(Apollo_Tables::_APL_USER_MODULES);
        return $aplQuery->get_where(" user_id={$userID} AND post_type='{$agencyPT}' ", " post_id ");
    }

    private static function _getStatus($status = array())
    {
        if (empty($status)) {
            $status = array('publish', 'pending');
        }
        return $status;
    }

    private static function _convertStatusToStr($status)
    {
        return "'".implode("','", $status). "'";
    }
}

new Apollo_User();
