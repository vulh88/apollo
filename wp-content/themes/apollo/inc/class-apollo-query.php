<?php

if ( !defined( 'ABSPATH' ) ) exit;

class Apl_Query {
    
    public $table = '';
    
    public function __construct( $_table, $force_table = false ) {
        global $wpdb;
      
        if ( $force_table ) {
            $this->table = $_table;
        } else {
            $this->table = isset( $wpdb->{$_table} ) ? $wpdb->{$_table} : $_table;
        }
        
    }
    
    public function get_row( $where, $select = "*" ) {
        global $wpdb;
        return $wpdb->get_row( " SELECT {$select} FROM {$this->table} WHERE 1=1 AND {$where}  " );
    }
    
    public function get_where( $where = '1=1', $select = "*", $limit = '', $order = '' ) {
        global $wpdb;
        return $wpdb->get_results( " SELECT {$select} FROM {$this->table} WHERE 1=1 AND {$where}  $order $limit" );
    }

    public function get_total( $where = '1=1' ) {
        global $wpdb;
        $total_data = $wpdb->get_row( " SELECT count(*) as total FROM {$this->table} WHERE 1=1 AND {$where} " );
        return $total_data ? intval( $total_data->total ) : 0;
    }

    /**
     * Get found rows from MySQL select, please note this param SQL_CALC_FOUND_ROWS should be
     * injected into the select clause
     *
     * @author vulh
     * @return integer
     */
    public static function get_found_rows() {
        global $wpdb;
        return $wpdb->get_var("SELECT FOUND_ROWS()");
    }
    
    public function insert( $data ) {
        global $wpdb;
        return $wpdb->insert( $this->table, $data );
    }
    
    public function update( $data, $where ) {
        global $wpdb;
        return $wpdb->update( $this->table, $data, $where );
    }
    
    public function delete( $where, $echo = false ) {
        global $wpdb;
        $sql =  " DELETE FROM {$this->table} WHERE 1=1 AND {$where}  " ;

        if ($echo) {
            echo $sql. '-------';
        }

        return $wpdb->query($sql);
    }

    public function deleteJoinedTable( $join = '', $where,  $echo = false ){
    	global $wpdb;
	    if( empty($join) ){
	    	return ;
	    }
	    $sql = " DELETE t FROM {$this->table} t
							{$join}
							WHERE {$where} ";

	    if ($echo) {
		    echo $sql. '-------';
	    }

	    return $wpdb->query($sql);

    }
    
    public function get_bottom( $field = '', $where = '1=1' ) {
        global $wpdb;
        if ( ! $field ) $field = 'id';
        $result = $wpdb->get_row( " SELECT MAX(".$field.") as max_value FROM {$this->table} WHERE {$where} " );
        return $result ? $result->max_value : false;
    }
}

