<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

require_once APOLLO_SHORTCODE_DIR. '/class-apollo-slide-shortcode.php';

require_once APOLLO_SHORTCODE_DIR. '/class-apollo-user-shortcode.php';

require_once APOLLO_SHORTCODE_DIR. '/class-apollo-taxonomy-shortcode.php';

require_once APOLLO_SHORTCODE_DIR. '/class-apollo-search-shortcode.php';

require_once APOLLO_SHORTCODE_DIR. '/class-apollo-images-shortcode.php';

require_once APOLLO_SHORTCODE_DIR. '/class-apollo-offer-dates-times.php';

require_once APOLLO_SHORTCODE_DIR. '/class-apollo-form-shortcode.php';

require_once APOLLO_SHORTCODE_DIR. '/class-apollo-get-content-shortcode.php';

require_once APOLLO_SHORTCODE_DIR. '/class-apollo-blog-shortcode.php';

/** @Ticket - #13001 */
require_once APOLLO_SHORTCODE_DIR. '/class-apollo-network-footer-shortcode.php';

require_once APOLLO_SHORTCODE_DIR. '/class-apollo-topten-shortcode.php';

require_once APOLLO_SHORTCODE_DIR. '/class-apollo-photo-slider-shortcode.php';

/** @Ticket #13552 */
require_once APOLLO_SHORTCODE_DIR. '/class-apollo-home-spotlight-shortcode.php';