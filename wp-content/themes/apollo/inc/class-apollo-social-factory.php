<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class SocialFactory {

    public static  $FBSHARERLINK = 'https://www.facebook.com/sharer/sharer.php'; // ?u=www.google.com

    /**
     * <a href="https://twitter.com/share?
        url=https%3A%2F%2Fdev.twitter.com%2Fweb%2Ftweet-button&
        via=twitterdev&
        related=twitterapi%2Ctwitter&
        hashtags=example%2Cdemo&
        text=custom%20share%20text">
        Tweet
        </a>
     * @var string
     */
    public  static  $TWSHAERLINK = 'https://twitter.com/share';

    /**
     * http://www.linkedin.com/shareArticle?
     *      mini=true
     *      &url=http%3A//developer.linkedin.com
     *      &title=LinkedIn%20Developer%20Network
     *      &summary=My%20favorite%20developer%20program
     *      &source=LinkedIn
     */
    public static  $LKSHARERLINK = 'https://www.linkedin.com/shareArticle';//?mini=true&url=abc.com&title=&summary=&source=

    /**
     * https://www.pinterest.com/pin/create/button/
        ?url=http%3A%2F%2Fwww.flickr.com%2Fphotos%2Fkentbrew%2F6851755809%2F
        &media=http%3A%2F%2Ffarm8.staticflickr.com%2F7027%2F6851755809_df5b2051c9_z.jpg
        &description=Next%20stop%3A%20Pinterest
     */
    
    public static $PINLINK = 'https://www.pinterest.com/pin/create/button/';

    public static $INSTLINK = 'https://www.instagram.com/';


    /**
     *
     * FB not support parameter for sharer.php anymore. Just use og: on header of url that you want to share
     * @expect string
     */
    public static function getPinSharerLink($arrInfo) {

        $link =  self::$PINLINK;

        if(!isset($arrInfo ['url'])) {
            return false;
        }

        $url = trim($arrInfo ['url']);

        if(empty($url)) {
            return false;
        }

        $link .= '?url='.urlencode($arrInfo['url']);
        
        $media = isset( $arrInfo ['media'] ) ? trim($arrInfo ['media']) : '';
        if ( $media ) {
            $link .= '&amp;media='.urlencode($media);
        }

        $summary = isset($arrInfo['summary']) ? $arrInfo['summary'] : '';
        $summary = trim($summary);
        if(!empty($summary)) {
            $link .= '&amp;description='.urlencode($summary);
        }
        
        return $link;
    }
    
    /**
     *
     * FB not support parameter for sharer.php anymore. Just use og: on header of url that you want to share
     * @expect string
     */
    public static function getFacebookSharerLink($arrInfo) {

        $link =  self::$FBSHARERLINK;

        if(!isset($arrInfo ['url'])) {
            return false;
        }

        $url = trim($arrInfo ['url']);

        if(empty($url)) {
            return false;
        }

        $link .= '?u='.urlencode($arrInfo['url']);

        return $link;
    }

    /**
     *
     * @expect string
     */
    public static function getTwitterSharerLink($arrInfo) {
        $link =  self::$TWSHAERLINK;

        if(!isset($arrInfo ['url'])) {
            return false;
        }

        $url = trim($arrInfo ['url']);

        if(empty($url)) {
            return false;
        }

        $link .= '?url='.urlencode($arrInfo['url']);

        $via = isset($arrInfo['via']) ? $arrInfo['via']: '';
        $via = trim($via);

        if(!empty($via)) {
            $link .= '&amp;via='.urlencode($via);
        }

//        $summary = isset($arrInfo['summary']) ? $arrInfo['summary'] : '';
//        $summary = trim($summary);
        $title = trim(isset($arrInfo['title']) ? $arrInfo['title'] : '');

        if(!empty($title)) {
            $link .= '&amp;text='.urlencode($title);
        }

        //@todo Add more if necessary: related, hashtags

        return $link;
    }

    /**
     *
     * @expect String
     */
    public static function getLinkedinSharerLink ($arrInfo) {
        $link = self::$LKSHARERLINK;

        if(!isset($arrInfo ['url'])) {
            return false;
        }

        $url = trim($arrInfo ['url']);

        if(empty($url)) {
            return false;
        }

        $link .='?mini=true&amp;url='.urlencode($url);

        //summary, source
        $title = isset($arrInfo['title']) ? $arrInfo['title']: '';
        $title = trim($title);
        
        if(!empty($title)) {
            $link .= '&amp;title='.urlencode($title);
        }

        $summary = isset($arrInfo['summary']) ? $arrInfo['summary']: '';
        $summary = trim($summary);

        if(!empty($summary)) {
            $link .= '&amp;summary='.urlencode($summary);
        }

        $source = isset($arrInfo['source']) ? $arrInfo['source']: '';
        $source = trim($source);

        if(!empty($source)) {
            $link .= '&amp;source='.urlencode($source);
        }

        return $link;

    }

    public static function getInstagramSharerLink ($arrInfo) {
        $link = self::$INSTLINK;
        return $link;
    }

        /**
     * Get sharable link
     * @param array $args
     * @return string
     * @Author: ThienLD
     */
    public static function getDefaultSharableLink($args = []){
        $postType = getArrayValueByKey('post_type',$args,'');
        $extraQSVals = getArrayValueByKey('extra_qs_values', $args,'');
        $extraQSVals = !empty($extraQSVals) ? '/' . $extraQSVals : "";
        $printObjID = getArrayValueByKey('object_id',$args,0);

        /**
         * @author vulh
         * @ticket #19027 - 0002522: wpdev54 Customization - Custom venue slug
         */
        $printSlug = Apollo_App::getCustomSlugByModuleName($postType);

        return home_url('/print/' . $printSlug . '/' . $printObjID . $extraQSVals);
    }
    
    public static function social_btns( $share_datas, $display_lb = true) {
        do_action( 'apollo_social_factory' );
        ?>
        <?php
            $eventId = isset($share_datas['id']) ? $share_datas['id'] : '';
            $bookmarkIcon = '';
            echo apply_filters('apl_add_bookmark_icon_to_social_btn', $bookmarkIcon, $eventId);
            $iconStyleClass = '';
            $iconStyleClass = apply_filters('apl_social_icon_style', $iconStyleClass);
        ?>
        <?php
            if ( $display_lb ):
                /** @Ticket #13138 */
                /** @Ticket #13041 - add the word "share" after the share icon */
        ?>
            <label>&nbsp;<span class=""><?php _e( 'Share', 'apollo' ); ?></span></label>
        <?php endif; ?>
        <a href="<?php echo self::getFacebookSharerLink( $share_datas['info'] ) ?>" target="_blank" class="sc fb <?php echo $iconStyleClass; ?>"
            data-ride="ap-logclick"
            data-action="apollo_log_share_activity"
            data-activity="<?php echo Apollo_Activity_System::SHARE_FB ?>"
            data-item_id="<?php echo $share_datas['id'] ?>"
             ><i class="fa fa-facebook"></i><span class="hidden"><?php _e( 'Facebook', 'apollo' ); ?></span></a>

        <a href="<?php echo self::getTwitterSharerLink( $share_datas['info'] ) ?>" target="_blank" class="sc tw <?php echo $iconStyleClass; ?>"
            data-ride="ap-logclick"
            data-action="apollo_log_share_activity"
            data-activity="<?php echo Apollo_Activity_System::TWEET ?>"
            data-item_id="<?php echo $share_datas['id'] ?>"
        > <i class="fa fa-twitter"></i><span class="hidden"><?php _e( 'Twitter', 'apollo' ); ?></span></a>

        <?php
            $instagram = '';
            echo apply_filters('apl_render_more_socials', $instagram, $share_datas);
        ?>
        <a href="<?php echo self::getLinkedinSharerLink( $share_datas['info'] );  ?>" target="_blank" class="sc lk <?php echo $iconStyleClass; ?>"
            data-ride="ap-logclick"
            data-action="apollo_log_share_activity"
            data-activity="<?php echo Apollo_Activity_System::SHARE_LK ?>"
            data-item_id="<?php echo $share_datas['id'] ?>"
             > <i class="fa fa-linkedin"></i><span class="hidden"><?php _e( 'Linkedin', 'apollo' ); ?></span></a>

        <?php
        /*@ticket: #17303 0002410: wpdev55 - Requirements part1 - [Page 5] Move Bookmark (save) icon after other share icons*/
        $eventId = isset($share_datas['id']) ? $share_datas['id'] : '';
        $bookmarkIcon = '';
        echo apply_filters('apl_add_event_bookmark_icon_after_social_btn', $bookmarkIcon, $eventId);
        ?>

        <?php if ( isset( $share_datas['data_btns'] ) && in_array( 'print' , $share_datas['data_btns'] )
                    && get_post_type() !== Apollo_DB_Schema::_BUSINESS_PT):

            if ( isset( $share_datas['prog_id'] ) ) {
                $prog_url = $share_datas['prog_id'] ? 'program/'. $share_datas['prog_id'] : 'program/0';
            } else {
                $prog_url = '';
            }
            /** @Ticket #16161 */
            global $taxonomy;
            $psType = '';
            if ($taxonomy) {
                $psType = str_replace('-type', '', $taxonomy);
            }
        ?>
        <a  rel="nofollow" class="pt print hidden" title="<?php _e('Print') ?>" target="_blank" href="<?php echo self::getDefaultSharableLink([
                'post_type' => $psType ? $psType : get_post_type(),
                'extra_qs_values' => $prog_url,
                'object_id' => $share_datas['id']
        ]); ?>">
            <i class="fa fa-print"></i>
            <span class="hidden"><?php _e( 'Print', 'apollo' ); ?></span>
        </a>
        <?php endif; ?>

        <?php if ( isset( $share_datas['data_btns'] ) && in_array( 'event-print' , $share_datas['data_btns'] ) ):

            if ( isset( $share_datas['event_print_url'] ) ) {
                $event_ptr_url = $share_datas['event_print_url'];
            } else {
                $event_ptr_url = '';
            }
            ?>
            <a rel="nofollow" class="pt print hidden" title="<?php _e('Print') ?>" target="_blank" href="<?php echo $event_ptr_url; ?>">
                <i class="fa fa-print"></i>
            </a>
        <?php endif; ?>

        <?php if ( isset( $share_datas['data_btns'] ) &&  in_array( 'sendmail' , $share_datas['data_btns'] ) ):
            /** @Ticket #13525 */
            $emailTextHover = of_get_option(Apollo_DB_Schema::_EMAIL_TEXT_HOVER, __('Email', 'apollo'));
            ?>
        <a  class="pt email hidden"
            data-ride="ap-tellfriendbylink"
            data-action_after="open:popup"
            data-single="true"
            data-url="<?php echo admin_url('admin-ajax.php?action=apollo_set_bookmark_for_myfriend') ?>"
            data-data_source_selector="[name^='eventid']"
            data-data="ids:value"
            title="<?php echo $emailTextHover; ?>"
            ><i class="fa fa-envelope-o"></i></a>
        <?php
        endif;

        /* @ticket #16609:  Merge the 'Add to Outlook' and 'Add to Google Calendar' icons into one popup window. */
        if (of_get_option('separation_group','two_icon') === "two_icon") {
            //TriLM add ics, googleCalendar
            //ics
            if (isset($share_datas['data_btns']) && in_array('ics', $share_datas['data_btns'])):

                if (isset($share_datas['prog_id'])) {
                    $prog_url = $share_datas['prog_id'] ? 'program/' . $share_datas['prog_id'] : 'program/0';
                } else {
                    $prog_url = '';
                }

                ?>
                <a class="pt calendar_btn " title="<?php _e('Save Outlook/iCal file') ?>" target="#_popup_choose_event">
                    <i class="fa fa-file"></i>
                </a>
            <?php
            endif;
            //google calendar
            if (isset($share_datas['data_btns']) && in_array('googleCalendar', $share_datas['data_btns'])):

                if (isset($share_datas['prog_id'])) {
                    $prog_url = $share_datas['prog_id'] ? 'program/' . $share_datas['prog_id'] : 'program/0';
                } else {
                    $prog_url = '';
                }

                ?>
                <a class="pt share-calendar " title="<?php _e('Post to Google Calendar', 'apollo') ?>"
                   target="#_popup_choose_event">
                    <i class="fa fa-calendar"></i>
                </a>
            <?php
            endif;
        }
        else{
            if (isset($share_datas['data_btns']) && in_array('googleCalendar', $share_datas['data_btns'])
                && in_array('ics', $share_datas['data_btns']) ):
                ?>
                <a class="pt show-calendar-popup" target="#_popup_choose_event"><i class="fa fa-calendar"></i></a>
            <?php
            endif;
        }
    }
}