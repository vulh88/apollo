<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Apollo Event Functions
 *
 * Functions for event specific things.
 *
 * @author 		vulh
 * @category 	Core
 * @package 	inc/Functions
 */

/**
 * Main function for returning events
 *
 * @param mixed $the_event Post object or post ID of the event.
 * @param array $args (default: array()) Contains all arguments to be used to get this event.
 * @return Apollo_Event
 */
function get_event( $the_event = false ) {
	return new Apollo_Event( $the_event, Apollo_DB_Schema::_EVENT_PT );
}

/**
 * Get the placeholder image
 * @param $size string
 * @param $aw
 * @param $ah
 * @param $thumbnail_size
 * @param $title
 * @access public
 * @return string
 */
function apollo_event_placeholder_img( $size = 'shop_thumbnail', $aw = false, $ah= false, $thumbnail_size = '' , $title = 'Placeholder') {

    if($thumbnail_size === '') $thumbnail_size = $size;

    if($aw && $ah) {
        $simg = '<img src="' . apollo_event_placeholder_img_src($thumbnail_size) . '" alt="'.$title.'" />';
        return apply_filters('apollo_event_placeholder_img',  $simg);
    }

	$dimensions = apollo_event_image_size( $size );

    $simg = '<img src="' . apollo_event_placeholder_img_src($thumbnail_size) . '" alt="'.$title.'"';
    if(!$aw){
        $simg .= ' width='.esc_attr( $dimensions['width'] );
    }

    if(!$ah) {
        $simg .= ' height='.esc_attr( $dimensions['height'] );
    }

    $simg .= ' class="wp-post-image" />';

	return apply_filters('apollo_event_placeholder_img',  $simg);
}

/**
 * Get an image size.
 *
 * Variable is filtered by apollo_get_image_size_{image_size}
 *
 * @param string $image_size
 * @return array
 */
function apollo_event_image_size( $image_size ) {
	if ( in_array( $image_size, array( 'shop_thumbnail', 'shop_catalog', 'shop_single' ) ) ) {
		$size           = get_option( $image_size . '_image_size', array() );
		$size['width']  = isset( $size['width'] ) ? $size['width'] : '300';
		$size['height'] = isset( $size['height'] ) ? $size['height'] : '300';
		$size['crop']   = isset( $size['crop'] ) ? $size['crop'] : 1;
	} elseif(in_array( $image_size, array( 'fixed','thumbnail','print-event-img-size' ))) {
		$size = array(
			'width'  => '150',
			'height' => '150',
			'crop'   => 1
		);
	} else {
        $size = array(
            'width'  => '300',
            'height' => '300',
            'crop'   => 1
        );
    }

	return apply_filters( 'apollo_event_get_image_size_' . $image_size, $size );
}


/**
 * Get the placeholder image URL for events etc
 *
 * @access public
 * @return string
 */
function apollo_event_placeholder_img_src($type='normal') {
    $arr = array(
        'normal' => APOLLO_ADMIN_IMAGES_URL. '/placeholder.png',
        'medium' => APOLLO_ADMIN_IMAGES_URL. '/placeholder-1.png',
    );

    if(!in_array($type, array('normal', 'medium'))) {
        $type = 'normal';
    }

	return apply_filters( 'apollo_event_placeholder_img_src',  $arr[$type] );
}


function apollo_get_post($post_id) {
    global $wpdb;

    $_post = wp_cache_get( $post_id, 'posts' );

    if ( ! $_post ) {
        $_post = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $wpdb->posts WHERE ID = %d LIMIT 1", $post_id ) );

        if ( ! $_post )
            return false;

        $_post = sanitize_post( $_post, 'raw' );
        wp_cache_add( $_post->ID, $_post, 'posts' );
    } elseif ( empty( $_post->filter ) ) {
        $_post = sanitize_post( $_post, 'raw' );
    }

    return new WP_Post( $_post );
}
