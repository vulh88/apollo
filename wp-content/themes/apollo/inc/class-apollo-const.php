<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

class Apollo_Tables {
    /* Blog Table */

    const _APOLLO_EVENT_VIEW = 'apollo_event_view';
    const _BOOKMARK = 'apollo_user_bookmark';
    const _POST_TERM = 'apollo_post_term';
    const _APOLLO_EVENT_META = 'apollo_eventmeta';
    const _APOLLO_BUSINESS_META = 'apollo_businessmeta';
    const _APOLLO_ARTIST_META = 'apollo_artistmeta';
    const _APOLLO_TERM_META = 'apollo_termmeta';
    const _APOLLO_TERM_RELATIONSHIPS = 'term_relationships';
    const _APOLLO_TERM_TAXONOMY = 'term_taxonomy';
    const _APL_VENUE_META = 'apollo_venuemeta';
    const _APL_ORG_META = 'apollo_organizationmeta';
    const _APL_IFRAME_SEARCH_WIDGET_META = 'apollo_iframeswmeta';
    const _APL_PHOTO_SLIDER_META = 'apollo_photo_slider';
    const _APL_EDU_EVAL_META = 'apollo_edu_evaluationmeta';
    const _APL_GRANT_EDU_META = 'apollo_grant_educationmeta';
    const _APL_EDUCATOR_META = 'apollo_educatormeta';
    const _APL_PROGRAM_META = 'apollo_programmeta';
    const _APL_CLASSIFIED_META = 'apollo_classifiedmeta';
    const _APL_PUBLIC_ART_META = 'apollo_public_artmeta';
    const _APL_NEIGHBORHOOD = 'apollo_neighborhood';

    /* Network Table */
    const _APL_BLOGUSER = 'apollo_blog_user';
    const _APL_TOPTEN = 'apollo_top_ten';
    const _APL_SITES = 'blogs';


    /* Program Educator */
    const _APL_PROGRAM_EDUCATOR = 'apollo_program_educator';
    const _APL_AGENCY_EDUCATOR = 'apollo_agency_educator';
    const _APL_AGENCY_ORG = 'apollo_agency_org';
    const _APL_AGENCY_VENUE = 'apollo_agency_venue';
    const _APL_AGENCY_ARTIST = 'apollo_agency_artist';

    const _APL_COOR_CACHE = 'apollo_coordinate_cache';

    /* artist public art table */
    const _APL_ARTIST_PUBLIC_ART = 'apollo_artist_public_art';

    /* artist event table */
    const _APL_ARTIST_EVENT = 'apollo_artist_event';

    /** apollo user modules */
    const _APL_USER_MODULES = 'apollo_user_modules';

    /* user enable/disable event import tool */
    const _APL_TOGGLE_EVENT_IMPORT_TOOL = 'apollo_toggle_event_import_tool';
    /* user bypass pending approval via using import event tool */
    const _APL_BYPASS_PENDING_APPROVAL = 'apollo_bypass_pending_approval';

    /* event org table */
    const _APL_EVENT_ORG = 'apollo_event_org';

    const _APL_USER_ACTIVITY = 'apollo_user_activity';

    //event_rating
    const _APL_EVENT_RATING = 'apollo_event_rating';

    const _APL_USER_VISIT = 'apollo_user_visit';

    const _APL_TIMELINE = 'apollo_timeline';

    const _APL_CUSTOM_FIELD = 'apollo_custom_field';

    const _APL_THEME_TOOL = 'apollo_theme_tool';

    const _APL_STATE_ZIP = 'apollo_state_zip_v1';

    const _APL_EVENT_IMPORT = 'apollo_event_import';

    const _APL_DUPLICATES = 'apollo_duplicates';

    const _APL_EVENT_CALENDAR = 'apollo_event_calendar';

    const _APL_AGENCY = 'apollo_agency';

    const _APL_EVENT_VIEW_FOR_EXPORTING = 'apollo_event_view_for_exporting';

    public static function networkTables()
    {
        return array(
            self::_APL_BLOGUSER
        );
    }

    public static function blogTables() {
        return array(
            self::_APL_USER_VISIT,
            self::_APL_USER_ACTIVITY,
            self::_APL_EVENT_CALENDAR,
            self::_APL_EVENT_RATING,
            self::_APL_TIMELINE,
        );
    }
}

class Apollo_Const {
    // Taxonomy cookies key
    const _APL_TAX_OTHER_EVENTS_DISPLAYED = '_apl_tax_other_events_displayed_';

    // nonce names
    const _APL_NONCE_NAME                    = '_aplNonceField';
    const _APL_ORGANIZATION_NONCE_NAME       = '_aplOrganizationNonceField';
    const _APL_CLASSIFIED_NONCE_NAME         = '_aplClassifiedNonceField';
    const _APL_EDUCATION_NONCE_NAME          = '_aplEducationNonceField';
    const _APL_TEACHER_EVALUATION_NONCE_NAME = '_aplTeacherEvaluationNonceField';
    const _APL_GRANT_EDUCATION_NONCE_NAME    = '_aplGrantEducationNonceField';
    const _APL_ADD_EVENT_NONCE_NAME          = '_aplAddEventNonceField';
    const _APL_ADD_CLASSIFIED_NONCE_NAME          = '_aplAddClassifiedNonceField';
    const _APL_ARTIST_NONCE_NAME       = '_aplArtistNonceField';

    // nonce actions
    const _APL_NONCE_ACTION_ARTIST_PROFILE_PAGE        = 'submitArtistProfileAction';
    const _APL_NONCE_ACTION_ARTIST_VIDEO_PAGE          = 'submitArtistVideoAction';
    const _APL_NONCE_ACTION_ARTIST_AUDIO_PAGE          = 'submitArtistAudioAction';
    const _APL_NONCE_ACTION_ARTIST_EVENT_PAGE          = 'submitArtistEventAction';
    const _APL_NONCE_ACTION_ARTIST_GALLERY_PAGE        = 'submitArtistGalleryAction';
    const _APL_NONCE_ACTION_ORGANIZATION_PROFILE_PAGE  = 'submitOrganizationProfileAction';
    const _APL_NONCE_ACTION_ORGANIZATION_PHOTO_PAGE    = 'submitOrganizationPhotoAction';
    const _APL_NONCE_ACTION_ORGANIZATION_VIDEO_PAGE    = 'submitOrganizationVideoAction';
    const _APL_NONCE_ACTION_ORGANIZATION_AUDIO_PAGE    = 'submitOrganizationAudioAction';
    const _APL_NONCE_ACTION_VENUE_PROFILE_PAGE         = 'submitVenueProfileAction';
    const _APL_NONCE_ACTION_VENUE_PHOTO_PAGE           = 'submitVenuePhotoAction';
    const _APL_NONCE_ACTION_VENUE_VIDEO_PAGE           = 'submitVenueVideoAction';
    const _APL_NONCE_ACTION_CLASSIFIED_PAGE            = 'submitClassifiedAction';
    const _APL_NONCE_ACTION_EDUCATION_PROFILE_PAGE     = 'submitEducationProfileAction';
    const _APL_NONCE_ACTION_EDUCATION_ADD_PROGRAM_PAGE = 'submitEducationAddProgramAction';
    const _APL_NONCE_ACTION_TEACHER_EVALUATION_PAGE    = 'submitTeacherEvaluationAction';
    const _APL_NONCE_ACTION_GRANT_EDUCATION_PAGE       = 'submitGrantEducationAction';
    const _APL_NONCE_ACTION_ADD_EVENT_PAGE             = 'submitAddEventAction';
    const _APL_NONCE_ACTION_ADD_CLASSIFIED_PAGE             = 'submitAddClassifiedAction';

    const _APL_ADMIN_METABOX_RENDER_TYPE_DEFAULT = 1;
    const _APL_ADMIN_METABOX_RENDER_TYPE_IN_OTHER_MODULE = 2;
}

class Apollo_DB_Schema {

    const _APL_RE_ACTIVATION_THEME_VERSION = '_apollo_re_activation_theme_version';

    // Event Theme - Events Displaying Limitation
    const _APL_MAX_EVENT_THEMES_PER_PAGE = 250;

    // Event theme - Organization displaying limitation
    const _APL_MAX_ORG_PER_PAGE = 250;

    // IFrame Search widget - Venues Displaying Limitation
    const _APL_MAX_VENUE_PER_PAGE = 250;
    // Options value
    const _APL_ARTIST_OPTIONS = '_apl_artist_options';

    const _APL_UPGRADE_KEY = '123Whatever';


    const _APL_AGENCY_ORG_FIELD_AGENCY_ID = 'agency_id';
    const _APL_AGENCY_ORG_FIELD_ORG_ID = 'org_id';


    /**
     * Custom fields
     */
    const _APL_ARTIST_CUSTOM_FIELDS = '_apl_artist_custom_fields';
    const _APL_EDUCATOR_CUSTOM_FIELDS = '_apl_educator_custom_fields';
    const _APL_PROGRAM_CUSTOM_FIELDS = '_apl_educator_program_fields';
    const _APL_CF_GROUP = '_apl_cf_group';
    const _APL_POST_TYPE_CF_DATA = '_apl_post_type_cf_data';
    const _APL_POST_TYPE_CF_SEARCH = '_apl_post_type_cf_search';
    const _APL_CLASSIFIED_CUSTOM_FIELDS = '_apl_classified_custom_fields';
    const _APL_PUBLIC_ART_CUSTOM_FIELDS = '_apl_public_art_custom_fields';
    const _APL_TAXONOMY_DISPLAY_PUBLIC_FIELD = '_apl_taxonomy_display_public_field';
    const _APL_TAXONOMY_DO_NOT_DISPLAY_ON_FE = '_apl_taxonomy_do_not_display_on_fe';
    // End custom fields

    const _SIDEBAR_PREFIX = 'apl-sidebar';
    const _APL_EDUCATORS_ICONS = '_apl_educator_icons';
    const _APL_ARTISTS_ICONS = '_apl_artist_icons';

    // Apollo theme options
    const _APOLLO_THEME_OPTION = '_apollo_theme_options';
    const _HEADER = '_header';
    const _PAGE_404 = '_page_404';
    const _TOP_HEADER = '_top_header';
    const _ENABLE_TOP_HEADER = '_enable_top_header';
    const _HEADER_TRACKING_SCRIPT = '_header_tracking_script';
    const _SCRIPT_IN_BODY_TAG = '_script_in_body_tag';
    const _ENABLE_TWO_ROW_NAVIGATION = '_enable_two_row_navigation';
    const _NAVIGATION_BAR_STYLE = '_navigation_bar_style';
    const _SCROLL_WITH_PAGE = '_scroll_with_page';
    const _TOP_TEN_HOURS_AGO = 'top_ten_hours_ago';

    const _APOLLO_PROCESSING_IMPORT_SETTINGS = 'apollo_processing_import_settings';

    const _FOOTER1 = '_footer1';
    const _FOOTER2 = '_footer2';
    const _FOOTER3 = '_site_info';
    const _FOOTER4 = '_footer';
    const _FOOTER5 = '_footer5';
    const _ADDITIONAL_JS = '_apl_additional';

    const _EVENT_SOCIAL_MEDIA_LOCATION_DETAIL = '_event_social_media_location_detail';
    const _FEATURED_BLOG_TOP_CONTENT = '_featured_blog_top_content';
    const _BOTTOM_SUB_CONTAIN = '_bottom_sub_contain';
    const _TOP_SUB_CONTAIN = '_top_sub_contain';
    const _OVERRIDE_CSS = '_override_css';
    const _OVERRIDE_CSS_VERSION = '_override_css_version';
    const _DEFAULT_HOME_LAYOUT = '_default_home_layout';
    const _PRIMARY_COLOR = '_primary_color';
    const _HOVERING_SEARCH_CONTENT_COLOR = '_hovering_search_content_color';

    const _ORGANIZATION_PHOTO_FORM_LOCATION = '_organization_photo_form_location';
    const _ORGANIZATION_ACTIVE_MEMBER_FIELD = '_organization_active_member_field';
    const _ORGANIZATION_CLONE_TO_VENUE = '_organization_clone_to_venue';
    const _ORGANIZATION_DEFAULT_VIEW_TYPE = '_organization_default_view';
    const _ORGANIZATION_NUM_ITEMS_LISTING_PAGE = '_organization_num_items_listing_page';
    const _ORGANIZATION_ENABLE_REQUIRED_PRIMARY_IMAGE = '_enable_requirement_organization_primary_image';
    const _APL_ORGANIZATION_CHECK_DISCOUNTS_TEXT = '_apl_organization_check_discounts_text';
    const _ORGANIZATION_ENABLE_ADDRESS_REQUIREMENT = '_organization_enable_address_requirement';
    const _ORGANIZATION_ENABLE_COUNTY_REQUIREMENT = '_organization_enable_county_requirement';
    const _ORGANIZATION_ENABLE_CITY_REQUIREMENT = '_organization_enable_city_requirement';
    const _ORGANIZATION_ENABLE_STATE_REQUIREMENT = '_organization_enable_state_requirement';
    const _ORGANIZATION_ENABLE_ZIP_REQUIREMENT = '_organization_enable_zip_requirement';
    const _ORGANIZATION_ENABLE_EMAIL_REQUIREMENT = '_organization_enable_email_requirement';
    const _ORGANIZATION_ENABLE_PHONE_REQUIREMENT = '_organization_enable_phone_requirement';
    const _ORGANIZATION_ENABLE_TYPES = '_organization_enable_types';
    const _ORGANIZATION_TYPES_LABEL = '_organization_types_label';
    const _ORGANIZATION_FE_ENABLE_OTHER_STATE = '_organization_fe_enable_other_state';
    const _ORGANIZATION_FE_ENABLE_OTHER_CITY = '_organization_fe_enable_other_city';
    const _ORGANIZATION_FE_ENABLE_OTHER_ZIP = '_organization_fe_enable_other_zip';



    const _NAV_SUB_L1_HOVER_COLOR = '_nav_sub_level1_hover_color';
    const _NAV_SUB_L2_HOVER_COLOR = '_nav_sub_level2_hover_color';

    const _NAV_SUB_L1_COLOR = '_nav_sub_level_1_color';
    const _NAV_SUB_L2_COLOR = '_nav_sub_level_2_color';

    const _NAV_SUB_L1_HOVER_BG_COLOR = '_nav_sub_l1_hover_bg_color';
    const _NAV_TWO_TIER_MAIN_HOVER_BG_COLOR = '_nav_tow_tier_main_hover_bg_color';
    const _NAV_SUB_L2_HOVER_BG_COLOR = '_nav_sub_l2_hover_bg_color';

    const _NAV_SUB_L1_BG_COLOR = '_nav_sub_level1_bg_color';
    const _NAV_SUB_L2_BG_COLOR = '_nav_sub_level2_bg_color';

    const _DEFAULT_NO_IMAGE = '_default_no_image';
    const _SPOT_TYPE = '_spot_type';
    const _SPOT_HEIGHT = '_spot_height';
    const _APL_HOME_SLIDER_DISPLAY_FULL = '_apl_home_slider_display_full';
    const _SPOT_TYPE_BUSINESS = '_spot_type_business';
    const _PRIMARY_IMAGE_TYPE_BUSINESS = '_primary_image_type_business';
    const _BLOCK_LABEL_STYLE_BUSINESS = '_block_label_style_business';
    const _BUSINESS_SHOW_ICON_POSITION = '_business_show_icon_position';
    const _FEATURE_BUSINESS = '_feature_business';
    const _HOME_FEATURED_TYPE = '_home_featured_type';
    const _HOME_FEATURED_DATA_TYPE = '_home_featured_data_type';
    const _HOME_SEMI_TRANSPARENT_TYPE = '_home_semi_transparent_type';
    const _HOME_SEMI_TRANSPARENT_TYPE_BACKGROUND_COLOR = '_home_semi_transparent_type_background_color';
    //const _HOME_FEATURED_TAB_TRANSFORM = '_home_featured_tab_transform';
    const _HOME_FEATURED_DATE_OPTION = '_home_featured_date_opt';
    const _EVENT_CATEGORY_SPOTLIGHT_STYLE = '_event_category_spotlight_style';
    const _SLIDER_BACKGROUND = '_slider_background';
    const _EVENT_SPOTLIGHT_BG_IMG = '_event_spotlight_bg_img';
    const _ROTATION_BAR_BACKGROUND = '_rotation_bar_background';
    const _COLOR_SELECTION_FONT = '_color_selection_font';
    const _COLOR_SELECTION_FONT_DATE = '_color_selection_date';
    const _COLOR_SELECTION_FONT_TITLE = '_color_selection_font_title';
    const _COLOR_SELECTION_META_AUTHOR = '_color_selection_meta_author';
    const _COLOR_SELECTION_META_AUTHOR_LINK = '_color_selection_meta_author_link';
    const _COLOR_SELECTION_VIEW_MORE= '_color_selection_view_more';
    const _ACTIVATE_TOPTEN_TAB = '_activate_topten_tab';
    const _HOME_CMS_PAGE_EVENT_FEATURED = '_home_cms_page_event_featured';
    const _HOME_CMS_PAGE_BLOG_FEATURED = '_home_cms_page_blog_featured';
    const _FEATURED_EVENTS_LABEL = '_featured_events_label';
    const _TOPTEN_LABEL_TAB = '_topten_label_tab';
    const _TOPTEN_CACHE_FILENAME_DEFAULT_TPL = '_topten_cache_default_tpl';
    const _TOPTEN_CACHE_FILENAME_HOR_VER_TPL = '_topten_cache_hor_ver_tpl';
    const _TOPTEN_CACHE_FLAG_DETECTION = '_topten_flag_detection';
    const _ACTIVATE_OVERRIDE_CSS = '_activate_override_css';
    const _COLLAPSEALLCHILDRENTMOBILE = '_collapse_all_children_on_mobile';
    const _FACEBOOK = '_facebook';
    const _LINKEDIN = '_linkedin';
    const _TWITTER = '_twitter';
    const _YOUTUBE = '_youtube';
    const _VIMEO = '_vimeo';
    const _INSTAGRAM = '_oc_instagram';

    const _EMAIL_TEXT_HOVER = '_email_text_hover';
    const _NUMBER_FEATURED_EVENTS = '_number_featured_events';
    const _FB_SHARING_LOGO = '_facebook_sharing_logo';
    const _EVENT_SETTING_ORDER = '_event_setting_order';
    const _EVENT_ADMIN_DISPLAY_ORDER= '_event_admin_display_order';
    const _EVENT_ENABLE_IMPORT_TAB_FOR_ALL_USERS= '_event_enable_import_tab_for_all_users';
    const _EVENT_SEARCH_METHOD = '_event_search_method';
    const _EVENT_OFFER_WIDGET_FILTER_TYPE = '_event_offer_widget_filter_type';
    const _ENABLE_ADD_IT_BTN = '_enable_add_it_btn';
    const _ENABLE_DISCOUNT_OFFER_CHBOX = '_enable_discount_offer_chbox';
    const _EVENT_DISCOUNT_CHECKBOX_FILTER_TYPE = '_event_discount_checkbox_filter_type';
    const _TEXT_OF_DISCOUNT_OFFER = '_text_of_discount_offer';
    const _FEATURE_BLOCK_TITLE_STYLE = '_feature_block_title_style';
    const _ROW_TITLE_STYLE = '_row_title_style';
    const _HOME_SPOTLIGHT_CUSTOM_CONTAIN = '_home_spotlight_custom_contain';
    const _HOME_FEATURE_EVENT_ITEM_PER_PAGE = '_home_feature_event_item_per_page';
    const _MOBILE_MENU_STYLE = '_mobile_menu_style';
    const _MOBILE_NAVIGATION_STYLE = '_mobile_navigation_style';
    const _MASTHEAD_GROUP_BUTTONS_STYLE = '_masthead_group_buttons_style';

    const _ENABLE_ENVENT_BY_MY_LOCATION_CHECKBOX = '_enable_event_by_my_location_checkbox';

    const _APL_GRAPHIC_TYPE = '_apl_graphic_type';
    const _TEXT_OF_ADD_IT = '_text_of_add_it';
    const _TEXT_OF_ADDED_IT = '_text_of_added_it';
    const _EVENT_SEARCH_TYPE    = '_event_search_type';
    const _HOR_EVENT_SEARCH_LOC    = '_hor_event_search_loc';
    const _ENABLE_EVENT_SEARCH_VENUE    = '_enable_event_search_venue';
    const _ENABLE_EVENT_SEARCH_ACCESSIBILITY    = '_enable_event_search_accessibility';
    const _ENABLE_EVENT_SEARCH_ORG    = '_enable_event_search_org';
    const _ENABLE_EVENT_SEARCH_CITY    = '_enable_event_search_city';
    const _ENABLE_EVENT_SEARCH_NEIGHBORHOOD    = '_enable_event_search_neighborhood';
    const _ENABLE_EVENT_SEARCH_REGION    = '_enable_event_search_region';
    const _ENABLE_EVENT_SEARCH_FREE_EVENT    = '_enable_event_search_free_event';
    const _ENABLE_VENUE_ACCESSIBILITY_MODE    = '_enable_venue_accessibility_mode';
    const _EVENT_ENABLE_FREE_EVENT    = '_event_enable_free_event';
    const _ENABLE_EVENT_SEARCH_DATE = '_enable_event_seach_date';
    const _ENABLE_REQUIREMENT_EVENT_URL_FIELD = '_enable_requirement_event_fe_url_field';
    const _ENABLE_EVENT_PRIMARY_IMG_SQUARE = '_enable_event_primary_img_square';
    const _ENABLE_REQUIREMENT_EVENT_PRIMARY_IMAGE = '_enable_requirement_event_primary_image';
    const _ENABLE_REQUIREMENT_EVENT_SUMMARY = '_enable_requirement_event_summary';
    const _ENABLE_DISPLAY_EDIT_BUTTON= 'enable_display_edit_button';
    const _ENABLE_DISPLAY_DELETE_BUTTON = '_enable_display_delete_button';
    const _ENABLE_REQUIRED_ORG_PROFILE = '_enable_required_org_profile';
    const DISABLE_REQUIREMENT_EVENT_DATE_TIME = '_enable_requirement_event_date_time';
    const _EVENT_FORM_ENABLE_DISCOUNT_TEXT = '_event_form_enable_discount_text';
    const _ENABLE_RETINA_DISPLAY = '_enable_retina_display';
    const _ACTIVATE_HOVER_EFFECT_IN_GRID = '_activate_hover_effect_in_grid';
    const _ENABLE_THE_TEXT_DESCRIPTION = '_enable_the_text_description';
    const _APL_EVENT_HOME_FEATURED_CHARACTERS_DESCRIPTION = '_apl_event_home_featured_characters_description';
    const _ENABLE_COMMENT = '_enable_comment';
    const _ENABLE_THUMBS_UP = '_enable_thumbs_up';
    const _ENABLE_PLACEHOLDER_IMG = '_enable_placeholder_img';
    /*@ticket #17262: wpdev19 - Suppress the 'zip code' field in the new account registration form*/
    const _ENABLE_REGISTRATION_ZIP_FIELD = '_enable_registration_zip_field';
    const _ENABLE_REQUIREMENT_REGISTRATION_ZIP_FIELD = '_enable_requirement_registration_zip_field';
    const _ROUTING_FOR_404_ERROR = '_routing_for_404_error';

    const _EVENT_SHOW_ICON_POSITION = 'event_show_icon_positition';
    const _EVENT_SHOW_THUMBS_UP_POSITION = 'event_show_thumbs_up_positition';
    const _ORGANIZATION_SHOW_ICON_POSITION = 'organization_show_icon_positition';

    const _HOME_FEATURED_ARTICLES_TITLE = '_home_featured_articles_title';
    const _ACTIVE_HOME_FEATURED_ARTICLES = '_active_home_featured_articles';
    const _HOME_FEATURED_BLOG_POST_LARGE_LEFT = '_home_featured_blog_post_large_left';
    const _HOME_FEATURED_BLOG_POST_LARGE_RIGHT = '_home_featured_blog_post_large_right';
    const _HOME_FEATURED_BLOG_POST_RIGHT_TOP = '_home_featured_blog_post_right_top';
    const _HOME_FEATURED_BLOG_POST_RIGHT_MIDDLE = '_home_featured_blog_post_right_middle';
    const _HOME_FEATURED_BLOG_POST_RIGHT_BOTTOM = '_home_featured_blog_post_right_bottom';
    const _HOME_FEATURED_BLOG_POST_TYPE = '_home_featured_blog_post_type';
    const _HOME_FEATURED_BLOG_POST_POSTION = '_home_featured_blog_post_postion';
        const _HOME_FEATURED_BLOG_POST_POSTION_TOP = 'top';
        const _HOME_FEATURED_BLOG_POST_POSTION_BOTTOM = 'bottom';


    const _ENABLE_EVENT_BYPASS_APPROVAL_PROCESS = '_enable_event_bypass_approval_process';
    const _ENABLE_EVENT_BYPASS_APPROVAL_METHOD = '_enable_event_bypass_approval_method';
    const _ENABLE_INDIVIDUAL_DATE_TIME = '_enable_individual_date_time';
    const _ENABLE_MAP_IT_BUTTON = '_enable_map_it_button';
    const _ENABLE_ADD_TO_CALENDAR_BUTTON = '_enable_add_to_calendar_button';
    const _ENABLE_DISPLAY_WITHOUT_VIEW_MORE = '_enable_display_without_view_mor';
    const _ENABLE_CATEGORY_PAGE_SPOTLIGHT = '_enable_category_page_spotlight';
    const _CATEGORY_PAGE_DATE_DISPLAY_OPTIONS = '_category_page_date_display_options';
    const _EVENT_DETAIL_PAGE_DATE_DISPLAY_OPTIONS = '_event_detail_page_date_display_options';
    const _EVENT_DEFAULT_VIEW_TYPE = '_event_default_view_type';
    const _EVENT_NUM_ITEMS_LISTING_PAGE = '_event_num_items_listing_page';
    const _EVENT_ENABLE_TOP_SPOTLIGHT = '_event_enable_spotlight';
    const _EVENT_CATE_ENABLE_TOP_SPOTLIGHT = '_event_cate_enable_spotlight';
    const _EVENT_TOP_SPOTLIGHT_TYPE = '_event_top_spotlight_type';
    const _EVENT_TOP_SPOTLIGHT_NUM = '_event_top_spotlight_num';
    const _EVENT_CATE_TOP_SPOTLIGHT_NUM = '_event_cate_top_spotlight_num';
    const _EVENT_TOP_SPOTLIGHT_SEMI_TRANSPARENT_TYPE = '_event_top_spotlight_semi_transparent_type';
    const _EVENT_TOP_SPOTLIGHT_SEMI_TRANSPARENT_TYPE_BG_COLOR = '_event_top_spotlight_semi_transparent_type_background_color';
    const _EVENT_ENABLE_DISCOUNT_DESCRIPTION = '_event_enable_discount_description';
    const _EVENT_DISCOUNT_DESCRIPTION_TEXT = '_event_discount_description_text';
    const _EVENT_DISCOUNT_DESCRIPTION_IMAGE = '_event_discount_description_image';
    const _EVENT_ENABLE_DISCOUNT_DESCRIPTION_FE = '_event_enable_discount_description_fe';
    const _EVENT_INCLUDE_DISCOUNT_URL = '_event_include_discount_url';
    const _EVENT_DISCOUNT_DESCRIPTION_FE_STYLE = '_event_discount_description_fe_style';

    const _HEADER_PRINTING_SEARCH_EVENT = '_header_printing_search_event';
    const _FOOTER_PRINTING_SEARCH_EVENT = '_footer_printing_search_event';
    const _MAXIMUM_EVENT_DISPLAY_IN_PRIMARY_CATEGORY = '_max_event_in_pr_cate';
    const _MAXIMUM_EVENT_DISPLAY_IN_EVENT_PRINT = '_max_event_in_event_print';


    const _BLOG_ENDABLE_DATE = '_apl_blog_enable_date';
    const _BLOG_ENABLE_AUTHOR = '_apl_blog_enable_author';
    const _BLOG_ENABLE_CATEGORY = '_apl_blog_enable_category';
    const _BLOG_ENDABLE_THUMB_UP = '_apl_blog_enable_thumb_up';
    const _BLOG_ENDABLE_COMMENT = '_apl_blog_enable_comment';
    const _BLOG_NUM_OF_CHAR = '_apl_blog_num_of_char';
    const _BLOG_DISPLAY_STYLE = '_apl_blog_displau_styler';
    const _BLOG_CATEGORY_PAGE_ORDER = '_apl_blog_category_page_order';
    const _BLOG_CATEGORY_TYPES = '_apl_blog_category_types';
    const _BLOG_CUSTOM_SLUG = '_apl_blog_rename_module';
    const _BLOG_CUSTOM_LABEL = '_apl_blog_custom_label';
    const _BLOG_LABEL_RELATED_ARTICLES = '_apl_blog_label_related_articles';
    const _BLOG_NUM_OF_HEADLINE = '_apl_blog_num_of_headline';
    const _BLOG_ENABLE_MSWP = '_blog_enable_mswp';
    const _BLOG_MSWP_TARGET_POST_META = '_blog_mswp_target_post_meta';
    const _BLOG_MSWP_LOC_POST_META = '_blog_mswp_loc_post_meta';
    const _BLOG_ENABLE_OVERRIDE_SEARCH_GLOBAL_CUSTOM = '_blog_enable_override_search_global_custom';
    const _BLOG_OVERRIDE_SEARCH_GLOBAL_CUSTOM_DIRECT_MODULE = '_blog_override_search_global_custom_direct_module';
    const _BLOG_FEATURED_IMAGE_MAX_HEIGHT = '_blog_featured_image_max_height';

    const _BLOG_LISTING_TYPE = '_apl_blog_listing_type';
    /* const for public art settings */
    const _PUBLIC_ART_SETTING_LATITUDE = '_apl_public_art_latitude';
    const _PUBLIC_ART_SETTING_LONGITUDE = '_apl_public_art_longitude';
    const _PUBLIC_ART_SETTING_ZOOM_MAGNIFICATION = '_apl_public_art_magnification';
    const _PUBLIC_ART_SETTING_LIMIT = '_public_art_setting_limit';
    const _PUBLIC_ART_DEFAULT_VIEW_TYPE = '_public_art_default_view_type';
    const _PUBLIC_ART_NUM_ITEMS_LISTING_PAGE = '_public_art_num_items_listing_page';
    const _PUBLIC_ART_MAP_REFRESH = '_public_art_map_refresh';
    const _APL_PUBLIC_ART_WIDGET_SEARCH_ENABLE_ZIP = '_enable_public_art_widget_search_zip';
    const _PUBLIC_ART_MAP_REFRESH_TIME = '_public_art_map_refresh_time';
    /* end const for public art settings */

    // Const for classified settings
    const _CLASSIFIED_DEFAULT_VIEW_TYPE = '_classified_default_view_type';
    const _CLASSIFIED_NUM_ITEMS_LISTING_PAGE = '_classified_num_items_listing_page';
    const _CLASSIFIED_ENABLE_PRIMARY_IMAGE = '_classified_enable_primary_image';
    const _CLASSIFIED_ENABLE_REQUIREMENTS_CITY = '_classified_enable_requirements_city';
    const _CLASSIFIED_ENABLE_REQUIREMENTS_STATE = '_classified_enable_requirements_state';
    const _CLASSIFIED_ENABLE_REQUIREMENTS_ZIP = '_classified_enable_requirements_zip';
    const _CLASSIFIED_ENABLE_REQUIREMENTS_EMAIL = '_classified_enable_requirements_email';
    const _CLASSIFIED_CUSTOM_SLUG = '_classified_custom_slug';
    const _CLASSIFIED_CUSTOM_LABEL = '_classified_custom_label';
    const _CLASSIFIED_FE_ENABLE_OTHER_STATE = '_classified_fe_enable_other_state';
    const _CLASSIFIED_FE_ENABLE_OTHER_CITY = '_classified_fe_enable_other_city';
    const _CLASSIFIED_FE_ENABLE_OTHER_ZIP = '_classified_fe_enable_other_zip';

    //const for venue settings
    const  _VENUE_DEFAULT_VIEW_TYPE = '_venue_default_view_type';
    const  _VENUE_NUM_ITEMS_LISTING_PAGE = '_venue_num_items_listing_page';

    //const for programs settings
    const  _PROGRAMS_DEFAULT_VIEW_TYPE = '_programs_default_view_type';
    const  _PROGRAM_NUM_ITEMS_LISTING_PAGE = '_program_num_items_listing_page';
    const _PROGRAM_ENABLE_MSWP = '_program_enable_mswp';
    const _PROGRAM_MSWP_LOC_POST_META = '_program_mswp_loc_post_meta';
    const _PROGRAM_MSWP_TARGET_POST_META = '_program_mswp_target_post_meta';
    const _PROGRAM_MSWP_THEME_LOC_OPTION = '_program_mswp_theme_loc_option';

    const _FB_ENABLE = '_fb_enable';
    const _FB_APP_ID = '_fb_app_id';
    const _FB_API_KEY = '_fb_api_key';
    const _FB_SECRET_KEY = '_fb_secret_key';
    const _NUM_HOME_SPOTLIGHT_EVENT = '_num_home_spotlight_event';
    const _NUM_BUSINESS_SPOTLIGHT = '_num_business_spotlight';
    const _APL_BUSINESS_SEMI_TRANSPARENT_TYPE = 'business_semi_transparent_type';
    const _APL_BUSINESS_SEMI_TRANSPARENT_TYPE_BACKGROUND_COLOR = 'business_semi_transparent_type_background_color';
    const _APL_BUSINESS_ENABLE_DISCOUNT_BUTTON = 'business_enable_discount_button';
    const _NUM_CHARS_SPOTLIGHT_EVENT_DESC = '_num_chars_spotlights_event_desc';
    const _HOME_CAT_FEATURES = '_home_cat_features'; // Hidden field save home categories feature
    const _MAX_UPLOAD_GALLERY_IMG = '_max_upload_gallery_img';
    const _MAX_UPLOAD_PDF_ARTIST = '_max_upload_pdf_artist';
    const _MAX_UPLOAD_PDF_PROGRAM = '_max_upload_pdf_program';
    const _MAX_UPLOAD_PDF_CLASSIFIED = '_max_upload_pdf_classified';//@ticket #16639
    const _NUM_VIEW_MORE = '_num_view_more';
    const _SUPPORT_EMAIL = '_support_email';
    const _IS_ENABLE_SUPPORT = '_is_enable_support';
    const _PROGRAM_EDU_ID = '_prog_edu_id';
    const EVENT_VIDEO_TEXT = '_event_video_text';
    const EVENT_GALLERY_TEXT = '_event_gallery_text';

    const _GOOGLE_API_KEY_SERVER = '_google_api_access_server_key';
    const _GOOGLE_API_KEY_BROWSER = '_google_api_access_browser_key';
    const _GOOGLE_ANALYTICS_KEY = '_google_analytics';
    const _SITE_SEO_KEYWORDS = '_site_seo_keywords';
    const _SITE_SEO_DESCRIPTION = '_site_seo_description';
    const _US_STATES = '_us_states';
    const _REGIONS = '_regions';
    const _TEXT_OF_NEARBY = 'whats_nearby_text';
    const _TEXT_OF_NEARBY_DESCRIPTION = 'whats_nearby_description';


    const _TEMP_ARTIST_MORE_ABOUT = '_temp_artist_more_about';
    const _ARTIST_ENABLE_COUNTY = '_artist_enable_county';
    const _ARTIST_ENABLE_COUNTY_REQUIREMENT = '_artist_enable_county_requirement';
    const _ARTIST_ENABLE_ADDRESS_REQUIREMENT = '_artist_enable_address_requirement';
    const _ARTIST_ENABLE_CITY_REQUIREMENT = '_artist_enable_city_requirement';
    const _ARTIST_ENABLE_STATE_REQUIREMENT = '_artist_enable_state_requirement';
    const _ARTIST_ENABLE_ZIP_REQUIREMENT = '_artist_enable_zip_requirement';
    const _ARTIST_ENABLE_EMAIL_REQUIREMENT = '_artist_enable_email_requirement';
    const _ARTIST_ENABLE_PHONE_REQUIREMENT = '_artist_enable_phone_requirement';
    const _ARTIST_DEFAULT_VIEW_TYPE = '_artist_default_view_type';
    const _ARTIST_NUM_ITEMS_LISTING_PAGE = '_artist_num_items_listing_page';
    const _ARTIST_LISTING_ORDER = '_artist_listing_order';
    const _ARTIST_ENABLE_REQUIRED_PRIMARY_IMAGE = '_enable_requirement_artist_primary_image';
    const _ARTIST_FE_ENABLE_REQUIRED_DESCRIPTION = '_artist_fe_enable_requirement_description';
    const _ARTIST_FE_ENABLE_REQUIRED_TYPE = '_artist_fe_enable_requirement_type';
    const _ARTIST_CUSTOM_SLUG = "_artist_custom_slug";
    const _ARTIST_CUSTOM_LABEL = '_artist_custom_label';
    const _ARTIST_CUSTOM_SINGLE_LABEL = '_artist_custom_single_label';
    const _ARTIST_FE_ENABLE_OTHER_STATE = '_artist_fe_enable_other_state';
    const _ARTIST_FE_ENABLE_OTHER_CITY = '_artist_fe_enable_other_city';
    const _ARTIST_FE_ENABLE_OTHER_ZIP = '_artist_fe_enable_other_zip';

    const _TEMP_ARTIST_DOCUMENTS = '_temp_artist_documents';
    const _EDUCATOR_TEACHERS_TITLE = '_educator_teachers_title';
    const _EDUCATOR_ENABLE_TEACHERS_BTN = '_educator_enable_teachers_btn';
    const _EDUCATOR_ENABLE_POPULATION_SERVED_DROP = '_educator_enable_population_served_drop';
    const _EDUCATOR_ENABLE_SUBJECT_DROP = '_educator_enable_subject_drop';
    const _EDUCATOR_ENABLE_ARTISTIC_DISCIPLINE_DROP = '_educator_enable_artistic_discipline_drop';
    const _EDUCATOR_ENABLE_ARTISTIC_DISCIPLINE_LABEL = '_educator_enable_artistic_discipline_label';
    const _EDUCATOR_ENABLE_CULTURAL_ORIGIN_DROP = '_educator_enable_cultural_origin_drop';
    const _EDUCATOR_ENABLE_PROGRAM_BILINGUAL = '_educator_enable_program_bilingual';
    const _EDUCATOR_ENABLE_PROGRAM_FREE = '_educator_enable_program_free';
    const _EDUCATOR_ENABLE_PROGRAM_ESSENTIALS_FIELD = '_educator_enable_program_essentials_field';
    const _EDUCATOR_ENABLE_PROGRAM_CORE_FIELD = '_educator_enable_program_core_field';
    const _EDUCATOR_PROGRAM_CORE_LABEL= '_educator_program_core_label';
    const _EDUCATOR_PROGRAM_ESSENTIALS_LABEL= '_educator_program_essentials_label';
    const _EDUCATOR_PROGRAM_STANDARD_LABEL= '_educator_program_standard_label';
    const _EDUCATOR_DEFAULT_VIEW_TYPE = '_educator_default_view_type';
    const _EDUCATOR_ENABLE_MSWP = '_educator_enable_mswp';
    const _EDUCATOR_MSWP_LOC_POST_META = '_educator_mswp_loc_post_meta';
    const _EDUCATOR_MSWP_TARGET_POST_META = '_educator_mswp_target_post_meta';
    const _EDUCATOR_MSWP_THEME_LOC_OPTION = '_educator_mswp_theme_loc_option';
    const _EDUCATOR_NUM_ITEMS_LISTING_PAGE = '_educator_num_items_listing_page';
    const _EDUCATOR_ENABLE_SEARCH_GLOBAL_CUSTOM = '_educator_enable_search_global_custom';
    const _EDUCATOR_FE_ENABLE_OTHER_STATE = '_educator_fe_enable_other_state';
    const _EDUCATOR_FE_ENABLE_OTHER_CITY = '_educator_fe_enable_other_city';
    const _EDUCATOR_FE_ENABLE_OTHER_ZIP = '_educator_fe_enable_other_zip';

    const _APL_EVENT_CHARACTERS_DESCRIPTION = '_apl_event_characters_description';
    const _APL_EVENT_ONGOING_DATE_RANGE = '_apl_event_ongoing_date_range';
    const _APL_EVENT_CHARACTERS_SUM = '_apl_event_characters_sum';
    const _APL_EVENT_BUY_TICKET_TEXT = '_apl_event_buy_tickets_text';
    const _APL_EVENT_CHECK_DISCOUNTS_TEXT = '_apl_event_check_discounts';
    const _APL_EVENT_BOOTH_OVERRIDE = '_apl_event_booth_override';
    const _APL_EVENT_BOOTH_OVERRIDE_URL = '_apl_event_booth_override_url';
    const _APL_LOAD_MORE_TXT_OFFER_DATESTIME_WIDGET = '_apl_load_more_text_offer_datestimes_wg';
    const _APL_EVENT_SUMMARY_FIELD = '_apl_event_summary_field';
    const _APL_EVENT_DESCRIPTION_FIELD = '_apl_event_description_field';
    const _APL_EVENT_ARTISTS_FIELD = '_apl_event_artists_field';
    const _APL_EVENT_REVIEW_EDIT = '_apl_event_review_edit';

    const _GOOGLE_MAP_MARKER_ICON = '_map_marker_icon';
    const _GOOGLE_MAP_MY_POSITION_MARKER_ICON = '_map_my_position_marker_icon';
    const _GOOGLE_MAP_DEFAULT_ADDRESS= '_google_map_default_address';
    const _GOOGLE_MAP_MARKER_ICON_WIDTH = '_map_marker_icon_width';
    const _GOOGLE_MAP_MARKER_ICON_HEIGHT= '_map_marker_icon_height';

    const _GEO_LOCATION_ENABLE = '_geo_location_enable';

    const _APL_DEFAULT_STATE= '_apl_default_state';
    const _APL_DEFAULT_CITY= '_apl_default_city';

    /**
     * Territory settings
    */
    const _TERR_STATES = '_apl_terr_states';
    const _TERR_CITIES = '_apl_terr_cities';
    const _TERR_ZIPCODE = '_apl_terr_zipcode';
    const _TERR_DATA = '_apl_terr_data';
    const _TERR_DATA_FULL_PASS = '_apl_pass_full_terr_data';
    const _TERR_STATES_FILTER_BUTTON = '_apl_terr_filter_button';

    /**
     * Email Settings
     */
    const _ES_FROM_NAME = '_es_from_name';
    const _ES_FROM_EMAIL = '_es_from_email';
    const _ES_EMAIL_HEADING = '_es_email_heading';
    const _ES_FROM_LOGO = '_es_from_logo';
    const _ES_EMAIL_FOOTER_TEXT = '_es_email_footer_text';
    const _ES_EMAIL_SUBJECT = '_es_email_subject';
    const _ES_EMAIL_TYPE = '_es_email_type';
    const _ES_EMAIL_RECIPIENT = '_es_email_recipient';
    const _ES_EMAIL_HTML_TEMP = '_es_email_html_temp';
    const _ES_EMAIL_CONFIRMATION_FEATURE = '_es_email_confirmation_feature';

    // End * Email Settings

    // Apollo Facebook user
    const _APOLLO_INCODE = '_apollo_incode';
    const _APOLLO_FB_USER = '_apollo_fb_user';
    const _FB_ID = '_fb_id';
    const _FB_NAME = '_fb_name';

    // Apollo Event
    const _APOLLO_EVENT_START_DATE = '_apollo_event_start_date';
    const _APOLLO_EVENT_DAYS_OF_WEEK = '_apollo_event_days_of_week';
    const _APOLLO_EVENT_END_DATE = '_apollo_event_end_date';
    const _APOLLO_EVENT_SOURCE = '_apollo_event_source'; // Add new or import tool
    const _APOLLO_HOME_EVENT_FEATURE = '_apollo_home_event_feature';
    const _APOLLO_EVENT_IMAGE_GALLERY = '_apollo_event_image_gallery';
    const _APOLLO_EVENT_ASSOCIATED_USER = '_apollo_event_associated_user';
    const _APL_EVENT_MINI_SITE = '_apl_event_mini_site';

    const _APOLLO_EVENT_CUSTOM_ACCESSIBILITY = '_custom_accessibility';
    const _APOLLO_EVENT_DATA = '_apollo_event_data';
        const _EMBED_VIDEO = '_embed_video';
        const _VIDEO_DESCRIPTION = '_video_description';
        const _WEBSITE_URL = '_website_url';
        const _ADMISSION_DETAIL = '_admission_detail';
        const _ADMISSION_PHONE = '_admission_phone';
        const _ADMISSION_TICKET_URL = '_admission_ticket_url';
        const _ADMISSION_TICKET_EMAIL = '_admission_ticket_email';
        const _ADMISSION_DISCOUNT_URL = '_admission_discount_url';
        const _ADMISSION_DISCOUNT_TEXT = '_admission_discount_text';
        const _ADMISSION_TICKET_INFO = '_admission_ticket_info';
        const _VIDEO = '_video';
        const _E_CONFIRM = '_e_confirm';
        const _E_USER_DRAFT = '_e_user_draft';

        const _E_CONTACT_NAME = '_contact_name';
        const _E_CONTACT_EMAIL = '_contact_email';
        const _E_CONTACT_PHONE = '_contact_phone';
        const _E_CUS_ACB = '_custom_accessibility';
        const _E_ACB_INFO = '_accessibility_info';
        const _E_PRIVATE = '_event_private';
        const _E_PREFERRED_SEARCH = '_event_preferred_search';

    const _APOLLO_EVENT_ORGANIZATION = '_apollo_event_organization';
    const _APOLLO_EVENT_VENUE = '_apollo_event_venue';
    const _APOLLO_EVENT_ARTIST = '_apollo_event_artist';

    const _APOLLO_EVENT_HOME_SPOTLIGHT = '_apollo_event_home_spotlight';
    const _APOLLO_EVENT_HOME_SPOTLIGHT_IMAGE = '_apollo_event_home_spotlight_image';
    const _APOLLO_EVENT_FREE_ADMISSION = '_apollo_event_free_admission';
    const _APL_EVENT_TMP_ORG = '_apl_event_tmp_org';
    const _APL_EVENT_TMP_VENUE = '_apl_event_tmp_venue';
    const _APL_EVENT_TERM_PRIMARY_ID = '_apl_event_term_primary_id';
    const _APL_EVENT_DISABLE_CALENDAR_CELL = '_apl_event_disable_calendar_cell';
    const _APL_EVENT_NUMBER_MONTH_CHANGE_MODE_CALENDAR = 6;
    const _APL_EVENT_MAXIMUM_YEAR = 2; //2year
    const _APL_EVENT_ADDITIONAL_TIME  = '_apl_event_additional_time';
    /*@ticket #17235 - [Admin] Official tag for event and post */
    const _APL_EVENT_OFFICIAL_TAGS  = '_apl_event_official_tags';
    const _APL_POST_OFFICIAL_TAGS  = '_apl_post_official_tags';


    const _APL_EVENT_TAX_HIDE = '_apl_event_tax_hide';
    const _APL_EVENT_TAX_HIDE_EXCEPT_USER_IDS = '_apl_event_tax_hide_except_user_ids';
    const _APL_EVENT_TAX_HIDE_SPOT = '_apl_event_tax_hide_spotlight';

    /*@ticket #17234 0002398 - Recommendation Widgets - [Admin] Official tag for event and post*/
    const _APL_EVENT_TAG_OFFICIAL = '_apl_event_tag_official';
    const _APL_POST_TAG_OFFICIAL = '_apl_post_tag_official';

    const _ENABLE_PHOTO_EVENT = '_enable_photo_event';

    const _ENABLE_VIDEO_EVENT = '_enable_video_event';
    const _ENABLE_TAGS_SECTION_FE_FORM = '_enable_tags_section_fe_form';
    const _EVENT_ENABLE_ARTISTS_SECTION_FE_FORM = '_event_enable_artists_section_fe_form';

    const _APL_BUSINESS_SPOT_TYPES_PRIMARY_ID = '_apl_business_spot_types_primary_id';
    const _APL_EVENT_SPOT_TYPES_PRIMARY_ID = '_apl_event_spot_types_primary_id';

    /**
     * Apollo Business
     */
    const _APOLLO_BS_FEATURES_META_KEY = '_apl_bs_features';
    const _BUSINESS_FEATURES_GROUP_FIELD = 'business_features_group_field';
    const _APOLLO_BUSINESS_DATA = '_apollo_business_data';
    const _BUSINESS_LATITUDE = '_apl_business_lat';
    const _BUSINESS_LONGITUDE = '_apl_business_lng';
    const _BUSINESS_CITY = '_apl_business_city';
    const _BUSINESS_ZIP = '_apl_business_zip';
    const _BUSINESS_REGION = '_apl_business_region';
    const _APL_BUSINESS_ID = '_apl_business_id';
    const _APL_BUSINESS_TITLE = '_apl_business_title';
    const _APL_BUSINESS_DESCRIPTION = '_apl_business_description';
    const _APL_BUSINESS_CATEGORIES = '_apl_business_categories';
    const _APL_BUSINESS_LOCATION_INFO = '_apl_business_location_info';
    const _APL_BUSINESS_HOURS = '_apl_business_hours';
    const _APL_BUSINESS_RESERVATION = '_apl_business_reservation';
    const _APL_BUSINESS_OTHER_INFO = '_apl_business_other_info';
    const _APL_BUSINESS_CUSTOM_FIELD_SEARCH = '_apl_business_custom_field_search';
    const _APL_BUSINESS_DATA = '_apl_business_data';
    const _APL_BUSINESS_ORG = '_apl_business_org';
    // constants for rendering form inside organization
    const _APL_BUSINESS_TYPE_DDL = '_apl_bs_type_ddl';
    const _ENABLE_BUSINESS_SEARCH_CITY    = '_enable_business_search_city';
    const _ENABLE_BUSINESS_SEARCH_REGION    = '_enable_business_search_region';
    const _APL_BUSINESS_SEARCH_WIDGET_LABEL_FEATURES    = '_apl_business_search_widget_label_features';
    const _APL_BUSINESS_SEARCH_KEYWORD_PLACEHOLDER    = '_apl_business_search_keyword_placeholder';
    const _APL_BUSINESS_SEARCH_WIDGET_OPEN_LIST_FEATURES    = '_apl_business_search_widget_open_list_features';


    /**
     * Apollo Venue meta
     */
    const _APL_VENUE_DATA = '_apl_venue_data';
    const _VENUE_PHONE = '_venue_phone';
    const _VENUE_FAX = '_venue_fax';
    const _VENUE_EMAIL = '_venue_email';
    const _VENUE_WEBSITE_URL = '_venue_website_url';

    const _APL_VENUE_ADDRESS = '_apl_venue_address';
    const _VENUE_NAME = '_venue_name';
    const _VENUE_ADDRESS1 = '_venue_address1';
    const _VENUE_ADDRESS2 = '_venue_address2';
    const _VENUE_CITY = '_venue_city';
    const _VENUE_NEIGHBORHOOD = '_venue_neighborhood';
    const _VENUE_STATE = '_venue_state';
    const _VENUE_ZIP = '_venue_zip';
    const _VENUE_REGION = '_venue_region';
    const _VENUE_LATITUDE = '_venue_lat';
    const _VENUE_LONGITUDE = '_venue_lng';
    const _VENUE_ENABLE_ADD_IT_BTN = 'venue_enable_add_it_btn';
    const _VENUE_TEXT_OF_ADD_IT = 'venue_text_of_add_it';
    const _VENUE_TEXT_OF_ADDED_IT = 'venue_text_of_added_it';

    const _VENUE_ENABLE_ADDRESS_REQUIREMENT = '_venue_enable_address_requirement';
    const _VENUE_ENABLE_COUNTY_REQUIREMENT = '_venue_enable_county_requirement';
    const _VENUE_ENABLE_CITY_REQUIREMENT = '_venue_enable_city_requirement';
    const _VENUE_ENABLE_STATE_REQUIREMENT = '_venue_enable_state_requirement';
    const _VENUE_ENABLE_ZIP_REQUIREMENT = '_venue_enable_zip_requirement';
    const _VENUE_ENABLE_EMAIL_REQUIREMENT = '_venue_enable_email_requirement';
    const _VENUE_ENABLE_PHONE_REQUIREMENT = '_venue_enable_phone_requirement';

    //TriLM add new field for venue
    const _VENUE_ENABLE_REQUIRED_PRIMARY_IMAGE = '_enable_requirement_venue_primary_image';
    const _VENUE_PARKING_INFO = '_venue_parking_info';
    const _VENUE_TYPES = '_venue_types';
    const _VENUE_PUBLIC_HOURS = '_venue_public_hours';
    const _VENUE_PUBLIC_ADMISSION_FEEDS = '_venue_public_admission_fees';
    const _VENUE_ACCESSIBILITY  = '_venue_accessibility ';
    const _VENUE_PUBLIC_BLOG_URL = '_venue_blog_url';
    const _VENUE_INSTAGRAM_URL = '_venue_instagram_url';
    const _VENUE_PINTEREST_URL = '_venue_pinterest_url';
    const _VENUE_FACEBOOK_URL = '_venue_facebook_url';
    const _VENUE_TWITTER_URL = '_venue_twitter_url';
    const _VENUE_LINKEDIN_URL = '_venue_linkedin_url';
    const _VENUE_DONATE = '_venue_donate';
    const _VENUE_CONTACT_NAME = '_venue_contact_name';
    const _VENUE_CONTACT_EMAIL = '_venue_contact_email';
    const _VENUE_CONTACT_PHONE = '_venue_contact_phone';
    const _APL_VENUE_IMAGE_GALLERY = '_apl_venue_image_gallery';
    const _APL_VENUE_VIDEOS = '_apl_venue_videos';
    const _APL_VENUE_ICONS = '_apl_venue_icons';
    const _APL_CLASSIFIED_ICONS = '_apl_classified_icons';
    const _APL_VENUE_POST_ICONS = '_apl_venue_post_icons';
    const _APL_PUBLIC_ART_ICONS = '_apl_public_art_icons';
    const _APL_EVENT_ICONS = '_apl_event_icons';
    const _APL_EVENT_POST_ICONS = '_apl_event_post_icons';

    /**
     * Apollo org meta
     */

    const _APL_ORG_TERM = '_apl_org_term';
    const _APL_ORG_ADDRESS = '_apl_org_address';
    const _ORG_ADDRESS1 = '_org_address1';
    const _ORG_ADDRESS2 = '_org_address2';
    const _ORG_CITY = '_org_city';
    const _ORG_STATE = '_org_state';
    const _ORG_ZIP = '_org_zip';
    /*begin @ticket #18223: Turn on the secondary org address */
    const _APL_ENABLE_SECONDARY_ORG_ADDRESS = '_apl_enable_secondary_org_address';
    const _APL_SECONDARY_ORG_ADDRESS = '_apl_secondary_org_address';
    const _SECONDARY_ORG_ADDRESS1 = '_secondary_org_address1';
    const _SECONDARY_ORG_ADDRESS2 = '_secondary_org_address2';
    const _SECONDARY_ORG_CITY = '_secondary_org_city';
    const _SECONDARY_ORG_STATE = '_secondary_org_state';
    const _SECONDARY_ORG_ZIP = '_secondary_org_zip';
    /*end*/
    const _ORG_REGION = '_org_region';
    const _ORG_LATITUDE = '_apl_org_lat';
    const _ORG_LONGITUDE = '_apl_oorg_lng';
    const _ORG_ENABLE_ADD_IT_BTN = 'organization_enable_add_it_btn';
    const _ORG_TEXT_OF_ADD_IT = 'organization_text_of_add_it';
    const _ORG_TEXT_OF_ADDED_IT = 'organization_text_of_added_it';
    const _ORG_AUTO_REDIRECT_TO_BUSINESS_DETAIL_URL = '_org_auto_redirect_to_business_detail_url';
    const _APL_ORG_TMP_STATE = '_apl_org_tmp_state';
    const _APL_ORG_TMP_CITY = '_apl_org_tmp_city';
    const _APL_ORG_TMP_ZIP = '_apl_org_tmp_zip';

    const _APL_ORG_DATA = '_apl_org_data';
    const _APL_ORG_BUSINESS = '_apollo_org_business';
    const _APL_ORG_VENUE = '_apollo_org_venue';

    const _ORG_PHONE = '_org_phone';
    const _ORG_FAX = '_org_fax';
    const _ORG_EMAIL = '_org_email';
    const _ORG_DISCOUNT_URL = '_org_discount_url';
    const _ORG_WEBSITE_URL = '_org_website_url';
    const _ORG_BLOG_URL = '_org_blog_url';
    const _ORG_INSTAGRAM_URL = '_org_instagram_url';
    const _ORG_PINTEREST_URL = '_org_pinterest_url';
    const _ORG_FACEBOOK_URL = '_org_facebook_url';
    const _ORG_TWITTER_URL = '_org_twitter_url';
    const _ORG_LINKED_IN_URL = '_org_linked_in_url';
    const _ORG_DONATE_URL = '_org_donate_url';
    const _ORG_CONTACT_NAME = '_org_contact_name';
    const _ORG_CONTACT_PHONE = '_org_contact_phone';
    const _ORG_CONTACT_EMAIL = '_org_contact_email';
    const _ORG_BUSINESS = '_org_business';
    const _ORG_DO_NOT_DISPLAY = '_org_do_not_display';
    const _ORG_MEMBER_ONLY = '_org_member_only';
    const _ORG_RESTAURANT = '_org_restaurant';
    const _APL_ORG_IMAGE_GALLERY = '_apl_org_image_gallery';
    const _APL_ORG_VIDEO = '_apl_org_video';
    const _APL_ORG_AUDIO = '_apl_org_audio';
    const _APL_ORG_ICONS = '_apl_org_icons';
    const _APL_ORG_POST_ICONS = '_apl_org_post_icons';
    const _APL_PRI_STATE = '_apl_pri_state';
    const _APL_PRI_CITY = '_apl_pri_city';
    const _APL_PRI_ZIP = '_apl_pri_zip';

	/**
     * Apollo Classified meta
     */
    const _APL_CLASSIFIED_TERM = '_apl_classified_term';
    const _APL_CLASSIFIED_DATA = '_apl_classified_data';
		const _CLASSIFIED_PHONE = '_classified_phone';
	    const _CLASSIFIED_FAX = '_classified_fax';
	    const _CLASSIFIED_EMAIL = '_classified_email';
	    const _CLASSIFIED_WEBSITE_URL = '_classified_website_url';
		const _CLASSIFIED_NAME = '_classified_name';
		const _CLASSIFIED_CONTACT_NAME = '_classified_contact_name';
	    const _CLASSIFIED_CONTACT_PHONE = '_classified_contact_phone';
	    const _CLASSIFIED_CONTACT_EMAIL = '_classified_contact_email';
		const _CLASSIFIED_EXP_DATE = '_classified_exp_date';
		const _CLASSIFIED_DEADLINE_DATE = '_classified_deadline_date';

	const _APOLLO_CLASSIFIED_ORGANIZATION = '_apl_classified_organization';
	const _APL_CLASSIFIED_TMP_ORG = '_apl_classified_tmp_org';

    const _APL_CLASSIFIED_EXP_DATE = '_apl_classified_exp_date';
    const _APL_CLASSIFIED_DEADLINE_DATE = '_apl_classified_deadline_date';

    const _APL_CLASSIFIED_ADDRESS = '_apl_classified_address';
		const _CLASSIFIED_ADDRESS = '_classified_address';
	    const _CLASSIFIED_CITY = '_classified_city';
	    const _CLASSIFIED_ZIP = '_classified_zip';
	    const _CLASSIFIED_TMP_CITY = '_classified_tmp_city';
	    const _CLASSIFIED_TMP_ZIP = '_classified_tmp_zip';
	    const _CLASSIFIED_STATE = '_classified_state';
        const _CLASSIFIED_TMP_STATE = '_classified_tmp_state';
        const _CLASSIFIED_REGION = '_classified_region';
    const _APL_CLASSIFIED_CITY = '_apl_classified_city';
    const _APL_CLASSIFIED_STATE = '_apl_classified_state';

	const _APL_CLASSIFIED_IMAGE_GALLERY = '_apl_classified_image_gallery';
    const _APL_CLASSIFIED_POST_ICONS = '_apl_classified_post_icons';
    const _CLASSIFIED_ENABLE_ADD_IT_BTN = 'classified_enable_add_it_btn';
    const _CLASSIFIED_TEXT_OF_ADD_IT = 'classified_text_of_add_it';
    const _CLASSIFIED_TEXT_OF_ADDED_IT = 'classified_text_of_added_it';

    /**
     * Apollo Public art meta
     */
    const _APL_PUBLIC_ART_POST_ICONS = '_apl_public_art_post_icons';
    const _APL_PUBLIC_ART_DATA = '_apl_public_art_data';
        const _PUBLIC_ART_DIMENSION = '_public_art_dimension';
        const _PUBLIC_ART_WEBSITE_URL = '_public_art_website_url';
        const _PUBLIC_ART_CONTACT_NAME = '_public_art_contact_name';
        const _PUBLIC_ART_CONTACT_PHONE = '_public_art_contact_phone';
        const _PUBLIC_ART_CONTACT_EMAIL = '_public_art_contact_email';
        const _PUBLIC_ART_CREATED_DATE = '_public_art_created_date';
        const _PUBLIC_ART_LATITUDE = '_public_art_latitude';
        const _PUBLIC_ART_LONGITUDE = '_public_art_longitude';
        const _APL_PUBLIC_ART_GOOGLE_ICON = '_apl_public_art_google_icon';
    const _APL_PUBLIC_ART_VIDEO = '_apl_public_art_video';
    const _APL_PUBLIC_ART_ADDRESS = '_apl_public_art_address';
        const _PUBLIC_ART_ADDRESS = '_public_art_address';
        const _PUBLIC_ART_CITY = '_public_art_city';
        const _PUBLIC_ART_STATE = '_public_art_state';
        const _PUBLIC_ART_ZIP = '_public_art_zip';
        const _PUBLIC_ART_REGION = '_public_art_region';
    const _APL_PUBLIC_ART_LATITUDE = '_apl_public_art_latitude';
    const _APL_PUBLIC_ART_LONGITUDE = '_apl_public_art_longitude';
    const _APL_PUBLIC_ART_ARTIST = '_apl_public_art_artist';
    const _APL_PUBLIC_ART_ARTIST_SEARCHING = '_apl_public_art_artist_searching';

    const _APL_PUBLIC_ART_IMAGE_GALLERY = '_apl_public_art_image_gallery';
    const _PUBLIC_ART_ENABLE_ADD_IT_BTN = 'public-art_enable_add_it_btn';
    const _PUBLIC_ART_TEXT_OF_ADD_IT = 'public-art_text_of_add_it';
    const _PUBLIC_ART_TEXT_OF_ADDED_IT = 'public-art_text_of_added_it';

    // spotlight
    const _APL_SPOT_LINK = '_spot_link';
    const _APL_SPOT_LINK_OPEN_STYLE = '_spot_link_open_style';
    const _APL_SPOT_LIGHT_GALLERY= '_spot_light_gallery';
    const _APL_SPOT_LIGHT_COLOR_LEFT_ARROW= '_spot_light_color_left_arrow';
    const _APL_SPOT_LIGHT_COLOR_RIGHT_ARROW= '_spot_light_color_right_arrow';
    const _APL_SPOT_LIGHT_NUM_DISPLAY_SLIDER= '_spot_light_num_displays_slider';
    const _APL_SPOT_LIGHT_START_DATE= '_spot_light_start_date';
    const _APL_SPOT_LIGHT_END_DATE= '_spot_light_end_date';
    const _APL_SPOT_LIGHT_DISPLAY_TITLE = 'spot_light_display_title';
    const _APL_SPOT_LIGHT_DISPLAY_SEMI_TRANSPARENT = 'spot_light_display_semi_transparent';
    const _APL_SPOT_LIGHT_DISPLAY_ACTION_BUTTON = 'spot_light_display_action_button';

    const _APL_SPOT_LIGHT_DISPLAY_SETTING = 'spot_light_display_setting';
    const _APL_SPOT_LIGHT_SETTING_TEXT = 'spot_light_setting_text';
    const _APL_SPOT_LIGHT_SETTING_TRANSPARENT_LEVEL = 'spot_light_setting_transparent_level';
    const _APL_SPOT_LIGHT_SETTING_BACKGROUND = 'spot_light_setting_background';
    const _APL_SPOT_LIGHT_SETTING_WIDTH = 'spot_light_setting_width';
    const _APL_SPOT_LIGHT_SETTING_HEIGHT = 'spot_light_setting_height';


    // business spotlight
    const _APL_BUSINESS_SPOTLIGHT_LINK            = '_business_spotlight_link';
    const _APL_BUSINESS_SPOTLIGHT_LINK_OPEN_STYLE = '_business_spotlight_link_open_style';

    // event spotlight
    const _APL_EVENT_SPOTLIGHT_LINK            = '_event_spotlight_link';
    const _APL_EVENT_SPOTLIGHT_LINK_OPEN_STYLE = '_event_spotlight_link_open_style';

    /*@ticket #18320: 0002504: Arts Education Customizations - add hyperlinks to the landing page slider images (Education spotlights) - item 4*/
    const _APL_EDUCATION_SPOTLIGHT_LINK            = '_education_spotlight_link';
    const _APL_EDUCATION_SPOTLIGHT_LINK_OPEN_NEW_TAB = '_education_spotlight_link_open_new_tab';
    /*@ticket #18320: 0002504: Arts Education Customizations - add hyperlinks to the landing page slider images (Education spotlights) - item 4*/
    const _EDUCATION_SPOTLIGHT_PT   = 'education-spotlight';

    const _APL_USER_ASSOCIATION_AGENCY_MODE_FIELD = 'apollo_user_association_agency_mode';

    // Theme activation
    const _APOLLO_THEME_ACTIVATION_OPTIONS = '_apollo_theme_activation_options';
    const _CREATE_FRONT_PAGE = '_create_front_page';
    const _CHANGE_PERMALINK_STRUCTURE = '_change_permalink_structure';
    const _CREATE_NAVIGATION_MENUS = '_create_navigation_menus';
    const _ADD_PAGES_TO_PRIMARY_NAVIGATION = '_add_pages_to_primary_navigation';
    const _FLAG_SETUP_FIRST_SIZE = '_flag_setup_first_size';

    // Event Post Type
    const _ORGANIZATION_PT = 'organization';
    const _VENUE_PT = 'venue';
    const _EVENT_PT = 'event';
    const _ARTIST_PT = 'artist';
    const _SPOT_PT = 'spotlight';
    const _CLASSIFIED = 'classified';
    const _BLOG_POST_PT = 'post';
    const _PUBLIC_ART_PT = 'public-art';
    const _SYNDICATION_PT = 'syndication';
    const _IFRAME_SEARCH_WIDGET_PT = 'iframesw';
    const _PHOTO_SLIDER_PT = 'photo-slider';
    const _NEWS_PT = 'news';
    const _SYNDICATION_ARTIST_PT = 'syndication-artist';
    const _SYNDICATION_ORG_PT = 'syndication-org';

    const _EVENT_SEARCH = 'event-search';

    const _EDUCATION = 'education';
    const _EDU_EVALUATION = 'edu_evaluation';
    const _GRANT_EDUCATION = 'grant_education';
    const _EDUCATOR_PT = 'educator';
    const _EDUCATOR_QUALIFICATION_GROUP_FIELD = 'educator_qualification_group_field';
    const _PROGRAM_PT = 'program';
    const _AGENCY_PT = 'agency';

    const _BUSINESS_PT = 'business';
    const _BUSINESS_TYPE = 'business-type';
    const _PUBLIC_ART_COL = 'public-art-collection';
    const _PUBLIC_ART_LOC = 'public-art-location';
    const _PUBLIC_ART_MED = 'public-art-medium';

    /**
     * Create new custom post type "Business-Spotlight" when Business module is active
     * @ticket #11186
     */
    const _BUSINESS_SPOTLIGHT_PT   = 'business_spotlight';
    const _BUSINESS_SPOTLIGHT_TYPE = 'business_spotlight_type';

    /** @Ticket #13565 */
    const _EVENT_SPOTLIGHT_PT   = 'event_spotlight';
    const _EVENT_SPOTLIGHT_TYPE = 'event_spotlight_type';

    /* Taxonomy */
    const _ARTIST_MEDIUM_TAX = 'artist-medium';
    const _ARTIST_STYLE_TAX = 'artist-style';
    const _ARTIST_TYPE = 'artist-type';
    const _EVENT_TAG = 'event-tag';

    // Wp post term
    const _IS_CATEGORY_SPOTLIGHT = '_is_category_spotlight';
    const _IS_CATEGORY_FEATURE = '_is_category_feature';
    const _IS_HOME_SPOTLIGHT = '_is_home_spotlight';
    const _TAB_LOCATION = '_tab_location';
    const _TERM_LANGUAGE = '_language';

    // Event org
    const _APL_E_ORG_ID = 'org_id';
    const _APL_E_VENUE_ID = 'venue_id';
    const _APL_E_ORG_ORDERING = 'org_ordering';
    const _APL_E_ORG_IS_MAIN = 'is_main';

    /* Artist */
    const _APL_ARTIST_DATA = '_apl_artist_data';
    const _APL_ARTIST_FNAME = '_apl_artist_fname';
    const _APL_ARTIST_LNAME = '_apl_artist_lname';
    const _APL_ARTIST_PHONE = '_apl_artist_phone';
    const _APL_ARTIST_EMAIL = '_apl_artist_email';
    const _APL_ARTIST_WEBURL = '_apl_artist_weburl';
    const _APL_ARTIST_BLOGURL = '_apl_artist_blogurl';
    const _APL_ARTIST_DISPLAY_EMAIL = '_apl_artist_display_email';
    const _APL_ARTIST_DISPLAY_PHONE = '_apl_artist_display_phone';
    const _APL_ARTIST_DISPLAY_ADDRESS = '_apl_artist_display_address';

    const _APL_ARTIST_DISPLAY_PUBLIC = '_apl_artist_display_public';
    const _APL_ARTIST_DO_NOT_LINKED = '_apl_artist_do_not_linked';

    const _APL_ARTIST_ANOTHER_CAT = '_apl_artist_another_cat';
    const _APL_ARTIST_ANOTHER_STYLE = '_apl_artist_another_style';
    const _APL_ARTIST_ANOTHER_MEDIUM = '_apl_artist_another_medium';
    const _APL_ARTIST_FB = '_apl_artist_fb';
    const _APL_ARTIST_TW = '_apl_artist_tw';
    const _APL_ARTIST_INS = '_apl_artist_ins'; //Instagram
    const _APL_ARTIST_LK = '_apl_artist_lk'; //LinkedIn
    const _APL_ARTIST_PR = '_apl_artist_pr'; //Pinterest
    const _APL_ARTIST_VIDEO = '_apl_artist_video'; //Pinterest
    const _APL_ARTIST_TIMELINE = '_apl_artist_timeline'; //Pinterest
    const _APL_ARTIST_POST_ICONS = '_apl_artist_post_icons';
    const _APL_ARTIST_AUDIO = '_apl_artist_audio';

    const _APL_ARTIST_QUESTIONS = '_apl_artist_questions'; //Pinterest

    const _APL_ARTIST_ADDRESS = '_apl_artist_address';
    const _APL_ARTIST_STREET = '_apl_artist_street';
    const _APL_ARTIST_CITY = '_apl_artist_city';
    const _APL_ARTIST_STATE = '_apl_artist_state';
    const _APL_ARTIST_ZIP = '_apl_artist_zip';
    const _APL_ARTIST_COUNTRY = '_apl_artist_country';
    const _APL_ARTIST_REGION = '_apl_artist_region';
    const _APL_ARTIST_TMP_STATE = '_apl_artist_tmp_state';
    const _APL_ARTIST_TMP_CITY = '_apl_artist_tmp_city';
    const _APL_ARTIST_TMP_ZIP = '_apl_artist_tmp_zip';
    const _APL_ARTIST_ASSOCIATION_USER_NICENAME = '_apl_artist_association_user_nicename';
    const _APL_ARTIST_ASSOCIATION_USER_ID = '_apl_artist_association_user_id';

    const _APL_ARTIST_IMAGE_GALLERY = '_apl_artist_image_gallery';
    const _APL_ARTIST_YEAR = '_apl_artist_year';
    const _ARTIST_ENABLE_ADD_IT_BTN = 'artist_enable_add_it_btn';
    const _ARTIST_TEXT_OF_ADD_IT = 'artist_text_of_add_it';
    const _ARTIST_TEXT_OF_ADDED_IT = 'artist_text_of_added_it';

    /**
     * Apollo Educator
     */
    const _APL_EDUCATOR_CUSTOM_FIELD_SEARCH = '_apl_educator_custom_field_search';
    const _APL_EDUCATOR_DATA = '_apl_educator_data';
    const _APL_EDUCATOR_ADD1 = '_apl_educator_add1';
    const _APL_EDUCATOR_ADD2 = '_apl_educator_add2';
    const _APL_EDUCATOR_REGION = '_apl_educator_region';
    const _APL_EDUCATOR_CITY = '_apl_educator_city';
    const _APL_EDUCATOR_STATE = '_apl_educator_state';
    const _APL_EDUCATOR_ZIP = '_apl_educator_zip';
    const _APL_EDUCATOR_COUNTY = '_apl_educator_county';
    const _APL_EDUCATOR_PHONE1 = '_apl_educator_phone1';
    const _APL_EDUCATOR_EMAIL = '_apl_educator_email';
    const _APL_EDUCATOR_URL = '_apl_educator_url';
    const _APL_EDUCATOR_BLOG = '_apl_educator_blog';
    const _APL_EDUCATOR_FAX = '_apl_educator_fax';
    const _APL_EDUCATOR_FB = '_apl_educator_fb';
    const _APL_EDUCATOR_TW = '_apl_educator_tw';
    const _APL_EDUCATOR_YT = '_apl_educator_yt';
    const _APL_EDUCATOR_INS = '_apl_educator_ins';
    const _APL_EDUCATOR_LK = '_apl_educator_lk';
    const _APL_EDUCATOR_PTR = '_apl_educator_ptr';
    const _APL_EDUCATOR_FLI = '_apl_educator_fli';
    const _APL_EDUCATOR_POST_ICONS = '_apl_educator_post_icons';
    const _APL_EDUCATOR_TMP_STATE = '_apl_educator_tmp_state';
    const _APL_EDUCATOR_TMP_CITY = '_apl_educator_tmp_city';
    const _APL_EDUCATOR_TMP_ZIP = '_apl_educator_tmp_zip';


    /**
     * Program
     */
    const _APL_PROGRAM_DATA = '_apl_program_data';
    const _APL_PROGRAM_CNAME = '_apl_program_cname';
    const _APL_PROGRAM_PHONE = '_apl_program_phone';
    const _APL_PROGRAM_EMAIL = '_apl_program_email';
    const _APL_PROGRAM_URL = '_apl_program_url';
    const _APL_PROGRAM_IMAGE_GALLERY = '_apl_program_images';
    const _APL_PROGRAM_IS_BILINGUAL = '_apl_program_isbilingual';
    const _APL_PROGRAM_IS_FREE = '_apl_program_isfree';
    const _APL_PROGRAM_VIDEO = '_apl_program_video';
    const _APL_PROGRAM_VIDEO_EMBED = '_apl_program_video_embed';
    const _APL_PROGRAM_VIDEO_DESC = '_apl_program_video_desc';

    const _APL_PROGRAM_AVAIABLE_DATE = '_apl_program_available_date';
    const _APL_PROGRAM_AVAIABLE_TIME = '_apl_program_available_time';
    const _APL_PROGRAM_SPACE_TECHNICAL = '_apl_program_space_technical';
    const _APL_PROGRAM_LOCATION = '_apl_program_location';
    const _APL_PROGRAM_FEE = '_apl_program_fee';
    const _APL_PROGRAM_PROGRAM_CORE = '_apl_program_program_core';
    const _APL_PROGRAM_PROGRAM_ESSENTIAL = '_apl_program_program_essential';
    const _APL_PROGRAM_CANCEL_POLICY = '_apl_program_cancel_policy';
    const _APL_PROGRAM_REFERENCES = '_apl_program_references';
    const _APL_PROGRAM_TEKS_GUIDELINES = '_apl_program_teks_guidelines';
    const _APL_PROGRAM_MAX_STUDENTS = '_apl_program_max_students';
    const _APL_PROGRAM_LENGTH_PROGRAM = '_apl_program_length_program';

    const _APL_PROGRAM_STARTD = '_apl_program_start_date';
    const _APL_PROGRAM_ENDD = '_apl_program_end_date';
    const _APL_PROGRAM_EXPIRATIOND = '_apl_program_expiration_date';

    // Program taxs
    const _APL_PROG_TAX_ART_DESC = 'artistic-discipline';
    const _APL_PROG_TAX_CUL_ORIGIN = 'cultural-origin';
    const _APL_PROG_TAX_POP_SER = 'population-served';
    const _APL_PROG_TAX_SUBJECT = 'subject';
    const _APL_EDU_TYPE = 'educator-type';


    // User meta
    const _APL_USER_ARTIST_EVENTS = '_apl_user_artist_events';

    // Apollo Edu Evaluation and Apollo Grant Education
    const _APL_EDU_EVAL_EDUCATOR = '_apl_edu_eval_edu';
    const _APL_EDU_EVAL_PROG = '_apl_edu_eval_prog';

    // Artist public art
    const _APL_A_ART_ID = 'artist_id';
    const _APL_A_ORDERING = 'public_art_ordering';


    // Network table fields
    const _APL_NETWORK_FIELD_BLOG_ID = 'blog_id';

    // Pagination
    const _APL_NUMBER_ITEMS_FIRST_PAGE = '_apl_number_items_first_page';
    const _APL_NUMBER_ITEMS_VIEW_MORE = '_apl_number_items_view_more';
    const _APL_SELECT_REGION = 'select_region';
    const _APL_FILTERING_BY_REGION = 'filtering_by_region';
    const _APL_FILTERING_BY_REGION_ZIP_LIST = 'filtering_by_region_zip_list';
    const _APL_FILTERING_BY_REGION_DEFAULT = 0;
    const _APL_FILTERING_BY_REGION_LISTING = 1;

    /**
     * @author: HieuLuong
     * search
     */
    const _APL_SEARCH_TYPE_VIEW= '_apl_search_type_view';
    const _APL_SEARCH_TYPE_VIEW_DEFAULT= 'thumb';

    /**
     * @author: TriLM
     *Solr search
     */
    const _ENABLE_SOLR_SEARCH = '_enable_solr_search';
    const _SOLR_SEARCH_HOST = '_solr_search_host';
    const _SOLR_SEARCH_PORT = '_solr_search_port';
    const _SOLR_SEARCH_USER_NAME = '_solr_search_user_name';
    const _SOLR_SEARCH_PASSWORD = '_solr_search_password';
    const _SOLR_SEARCH_PATH = '_solr_search_path';
    const _SOLR_SEARCH_TIME_OUT = '_solr_search_time_out';
    const _SOLR_MAX_QUERY_ROW = '_solr_max_query_row';
    const _SOLR_MAX_QUERY_ROW_PER_POST_TYPE = '_solr_max_query_row_per_post_type';
    const _SYNC_DATA_BUTTON = '_sync_data_button';
    const _SOLR_POST_TYPE_LIST = '_solr_post_type_list';

    const _IS_GLOBAL_PAGE = 'is_global_page';

    // Syndication
    const _SYNDICATION_EXPIRED_TIME = '_syndication_expired_time';
    const _SYNDICATION_ALL_ORGANIZATION = '_syndication_all_organization';
    const _SYNDICATION_SELECTED_ORGANIZATION = '_syndication_selected_organization';
    const _SYNDICATION_ORGANIZATION_LIMIT = '_syndication_organization_limit';
    const _SYNDICATION_ORGANIZATION_ORDERBY = '_syndication_organization_orderby';
    const _SYNDICATION_ORGANIZATION_ORDER = '_syndication_organization_order';

    //trilm: search session key
    const _SEARCH_SESSION_KEY = 'search_post';
    const _SEARCH_PREFERRED_EVENT = 'search_preferred_event';

    //Trilm Bypass aproval Org, Venue, Artist, Classified
    const _ENABLE_MODULE_TYPE_BYPASS_APPROVAL_PROCESS_TEMPLATE = '_enable_%s_bypass_approval_process';
    const _ENABLE_ORG_BYPASS_APPROVAL_PROCESS = '_enable_organization_bypass_approval_process';
    const _ENABLE_VENUE_BYPASS_APPROVAL_PROCESS = '_enable_venue_bypass_approval_process';
    const _APL_VENUE_WIDGET_SEARCH_ENABLE_REGION = '_apl_widget_search_enable_region';
    const _APL_VENUE_WIDGET_SEARCH_ENABLE_STATE = '_apl_widget_search_enable_state';
    const _APL_VENUE_WIDGET_SEARCH_ENABLE_ZIP = '_apl_widget_search_enable_zip';
    const _APL_ENABLE_ORGANIZATION_WIDGET_SEARCH_REGION = '_enable_organization_widget_search_region';
    const _APL_ENABLE_ORGANIZATION_WIDGET_SEARCH_STATE = '_enable_organization_widget_search_state';
    const _APL_ENABLE_ORGANIZATION_WIDGET_SEARCH_ZIP = '_enable_organization_widget_search_zip';
    const _APL_ENABLE_ARTIST_WIDGET_SEARCH_ZIP = '_enable_artist_widget_search_zip';
    const _APL_ENABLE_ARTIST_WIDGET_SEARCH_CITY = '_enable_artist_widget_search_city';
    const _ENABLE_ARTIST_BYPASS_APPROVAL_PROCESS = '_enable_artist_bypass_approval_process';
    const _ENABLE_CLASSIFIED_BYPASS_APPROVAL_PROCESS = '_enable_classified_bypass_approval_process';
    const _ARTIST_ACTIVE_MEMBER_FIELD = '_artist_active_member_field';
    const _ARTIST_MEMBER_ONLY = '_artist_member_only';
    const _ARTIST_FEATURED_LABEL = 'artist_featured_label';
    const _ARTIST_NORMAL_LABEL  = "artist_normal_label";
    const _FILTER_MORE_ARTIST_MEMBER_LABEL  = "filter_more_artist_member";
    const _ARTIST_IS_NEW = '_new_artist';

    //const label for FE Form artist
    const _ARTIST_MENU_LABEL = 'artist_menu_label';
    const _ARTIST_PROFILE_TITLE = 'artist_profile_title';
    const _ARTIST_EMAIL_LABEL = 'artist_email_label';
    const _ARTIST_PHONE_LABEL = 'artist_phone_label';
    const _ARTIST_ADDRESS_LABEL = 'artist_address_label';
    const _ARTIST_FORM_PDF_LABEL = 'artist_form_pdf_label';
    const _ARTIST_BIO_STATEMENT_TYPE = 'artist_bio_statement_type';


    const _APL_RESET_PERMARK_LINK = '_reset_permark_link';
    const _APL_UPDATED_PERMARK_LINK_VER = 'v1';

    const _APOLLO_BOOKMARK_BUTTON_LOCATION = '_apollo_bookmark_button_location';
    const _APOLLO_BOOKMARK_BUTTON_TYPE = '_apollo_bookmark_button_type';
    const _APOLLO_EVENT_ENABLE_CLOCK_ICON = '_apollo_event_enable_clock_icon';

    /**
     * @ticket #18515: Add the options allow control the transition speed of the slides.
     */
    const _APL_SLIDESHOW_SPEED_DEFAULT = 5000;
    const _APL_HOME_SLIDESHOW_SPEED = '_apl_home_slideshow_speed';
    const _APL_EVENT_SLIDESHOW_SPEED = '_apl_event_slideshow_speed';
    const _APL_BUSINESS_SLIDESHOW_SPEED = '_apl_business_slideshow_speed';
    const _APL_EDUCATION_SLIDESHOW_SPEED = '_apl_education_slideshow_speed';
}

class Apollo_Display_Config {

    // Default pagination
    const APL_DEFAULT_NUMBER_ITEMS_FIRST_PAGE = 24;
    const APL_DEFAULT_NUMBER_ITEMS_VIEW_MORE = 24;
    const APL_SYNDICATION_EXPIRED_TIME = 24;
    const APL_PUBLIC_ART_ARTISTS_LIMIT = 10;
    const APL_ARTISTS_EVENT_LIMIT = 10;
    const APL_USER_MANAGEMENT_LIMIT = 20;
    const APL_USER_ASSOCIATION_LIMIT = 10;
    const APL_ASSOCIATED_USERS_LIST_PAGE_LIMIT = 3;
    const APL_ASSOCIATED_USERS_DETAIL_PAGE_LIMIT = 10;


    /* datetime format pattern */
    const _APL_DATETIME_FORMAT_PATTERN_FOR_FILENAME = 'M, d, Y h:i A';

    const PAGE_SIZE = 10;
    const PAGESIZE_UPCOM = 5;
    const NUM_HOME_SPOTLIGHT = 5;
    const NUM_BUSINESS_SPOTLIGHT = 10;
    const _EVENT_TOP_SPOTLIGHT_NUM = 10;
    const NUM_TOPTEN_PAGESIZE = 10;
    const MAX_UPLOAD_GALLERY_IMG = 5;
    const MAX_UPLOAD_PDF_ARTIST = 5;
    const TOP_TEN_LABEL = 'Top Ten';
    const DEFAULT_FEATURED_EVENTS_LABEL = 'FEATURED EVENTS';
    const TOP_TEN_HOURS_AGO = 48;
    const _NUM_VIEW_MORE = 2;
    const _NUMBER_OFFER_DATE_TIME = 5;

    /* Taxonomy single page */
    const SINGLE_TAXONOMY_SPOTLIGHT_EVENT_SHOW = 1;
    const SINGLE_TAXONOMY_FEATURES_EVENT_SHOW = 7;
    const SINGLE_TAXONOMY_OTHER_EVENT_SHOW = 10;
    const SINGLE_TAXONOMY_OTHER_EVENT_SHOW_STEP = 2;
    const MIN_NUM_FEATURED_EVENT = 7;
    const LISTING_TITLE_MAX_TRIM_WORDS = 50;

    /* Summary text number character */
    const EVENT_OVERVIEW_NUMBER_CHAR = 100;
    const HOME_SPOTLIGHT_EVENT_OVERVIEW_NUMBER_CHAR = 200;
    const HOME_SMALL_SPOTLIGHT_EVENT_OVERVIEW_NUMBER_CHAR = 200;

    const DASHBOARD_ACCOUNT_BOOKMARK_PAGESIZE = 10;

    const DASHBOARD_ACCOUNT_ACTIVITY_PAGESIZE = 10;

    const DASHBOARD_ARTIST_EVENTS_PAGESIZE = 10;

    const DASHBOARD_EVENTS = 50;

    const PUBLISHED_PROGRAMS = 20;

    const MAX_DATETIME = 10;
    const MAX_DATETIME_SEARCH = 7;

    const MAX_SHORT_DESC = 20;
    const NUM_VIEW_MORE = 2;

    const TREE_MAX_LEVEL = 5;

    const _BLOG_NUM_OF_CHAR = 350;

    const _PUBLIC_ART_DF_SETTING_LATITUDE = '';
    const _PUBLIC_ART_DF_SETTING_LONGITUDE = '';
    const _PUBLIC_ART_DF_SETTING_ZOOM_MAGNIFICATION = 8;
    const _PUBLIC_ART_SETTING_LIMIT_STD = 500;


    // @ticket #11430
    const _APL_FE_EVENT_FORM_SUMMARY_FIELD     = 'Summary: A short, promotional summary of your event (1 sentence)';
    const _APL_FE_EVENT_FORM_DESCRIPTION_FIELD = 'Description: A more in-depth explanation of your event (1-2 paragraphs)';

    const EVENT_VIDEO_TEXT = 'Video';

    const EVENT_GALLERY_TEXT = 'Gallery';
    const _TEXT_OF_DISCOUNT_OFFER = 'Include Discount Offers';

    /* @ticket #12232 */
    const _PRIMARY_IMAGE_TYPE_MEDIUM_WIDTH_BUSINESS = 620;

    /* @ticket #14466 */
    const _APL_EVENT_ARTISTS_PAGE_SIZE = 10;
    /** @Ticket #16287 */
    const _MAX_UPLOAD_PDF_DEFAULT = 10;

}

class Apollo_State {
    const REMOVE_USER = 'RM_USER';
}

class Apollo_SESSION {
    const BOOKMARK = '_apollo_bookmark';

    const LOG_VISIT = 'l:v:';
    const EVENT_ADD_PIMAGE = 'event:add:pimg:';
    const EVENT_ADD_GALLERY = 'event:add:galerry:';


    const LOG_EVENT_RATING = 'l:event:rating:';

    const SUFFIX_ADD_PIMAGE = ':add:pimg:';
    const SUFFIX_ADD_GALLERY = ':add:galerry:';

    const SUFFIX_ADD_PDF_ARTIST = ':artist:add:galerry:';

    const SAVE_EVENT_OPTIONS = 'event_options_data';
    const FLAG_FOR_SAVING_EVENT_OPTIONS = 'is_save_event_options_action';
}

/* Administrator have all capabiities of roles */
class Apollo_Role {
    public static  function getRoles() {
        return array(
            'administrator' => array(
                'event_manager',
                'organization_manager',
                'venue_manager',
                'artist_manager'
            )
        );
    }
    public static  function getCapabilities() {
        return array(
            'manage_user_accounts' => array(
                'list_users' => true,
                'create_users' => true,
                'edit_users' => true,
                'delete_users' => true,
                'promote_users' => true,
                'remove_users' => true,
            )
        );
    }
}

class Apollo_Type_Manager {
    public static function get_apollo_type() {
        return array(
            Apollo_DB_Schema::_ORGANIZATION_PT,
            Apollo_DB_Schema::_VENUE_PT,
            Apollo_DB_Schema::_EVENT_PT,
            Apollo_DB_Schema::_ARTIST_PT,
            Apollo_DB_Schema::_SPOT_PT,
            Apollo_DB_Schema::_CLASSIFIED
        );
    }
}

class Apollo_Sort_Manager {

    public static function getSortOneFieldForPostType($type='event', $eventForce = false) {

        if($type === 'post') {
            return array(
                'order_by' => 'post_date',
                'order' => 'DESC',
                'type_sort' => 'post',
            );
        }

        if($type === 'artist') {
            return array(
                'order_by' => 'meta_value',
                'order' => 'ASC',
                'type_sort' => 'apollo_meta',
                'metakey_name' => Apollo_DB_Schema::_APL_ARTIST_LNAME,
            );
        }

        if($type === 'classified' || $type === 'public_art' || $type === 'program' ) {
            return array(
                'order_by' => 'post_date',
                'order' => 'DESC',
                'type_sort' => 'post',
            );
        }

        if($type !== 'event') {
            return array(
                'order_by' => 'post_title',
                'order' => 'ASC',
                'type_sort' => 'post',
            );
        }


        $order =  $eventForce ? $eventForce : of_get_option( Apollo_DB_Schema::_EVENT_SETTING_ORDER );
        switch ( $order ) {
            case 'ALPHABETICAL_ASC':
                return array(
                    'order_by' => 'post_title',
                    'order' => 'ASC',
                    'type_sort' => 'post',
                );
                break;

            case 'ALPHABETICAL_DESC':
                return array(
                    'order_by' => 'post_title',
                    'order' => 'DESC',
                    'type_sort' => 'post',
                );

            case 'END_DATE_ASC':
                return array(
                    'order_by' => 'meta_value',
                    'order' => 'ASC',
                    'type_sort' => 'apollo_meta',
                    'metakey_name' => Apollo_DB_Schema::_APOLLO_EVENT_END_DATE,
                );
                break;

            case 'START_DATE_ASC':
            default:
                return array(
                    'order_by' => 'meta_value',
                    'order' => 'ASC',
                    'type_sort' => 'apollo_meta',
                    'metakey_name' => Apollo_DB_Schema::_APOLLO_EVENT_START_DATE,
                );
                break;
        }
    }
}

class Apollo_Taxonomy_Template {
    const _APOLLO_TEMPLATE_CUSTOM_TAXONOMY_CLASSIFIED = 'classified';
    const _APOLLO_TEMPLATE_CUSTOM_TAXONOMY_ARTIST = 'artists';
    const _APOLLO_TEMPLATE_CUSTOM_TAXONOMY_EDUCATORS = 'educators';
    const _APOLLO_TEMPLATE_CUSTOM_TAXONOMY_ORGANIZATION = 'org';
    const _APOLLO_TEMPLATE_CUSTOM_TAXONOMY_PUBLIC_ART = 'public-art';
    const _APOLLO_TEMPLATE_CUSTOM_TAXONOMY_VENUE = 'venue';
    const _APOLLO_TEMPLATE_CUSTOM_TAXONOMY_PROGRAMS = 'programs';
    const _APOLLO_TEMPLATE_CUSTOM_TAXONOMY_BUSINESS = 'business';
    const _APOLLO_TEMPLATE_CUSTOM_TAXONOMY_NEWS = 'news';
}

class Apollo_Event_Import_File {
    const _APOLLO_EVENT_IMPORT_TYPE = 'event-import';
    const _APOLLO_IEF_COLUMN_ID = 'id';
    const _APOLLO_IEF_COLUMN_FILENAME = 'filename';
    const _APOLLO_IEF_COLUMN_USERNAME = 'username';
    const _APOLLO_IEF_COLUMN_IMPORTED_DATE = 'imported_date';
    const _APOLLO_IEF_COLUMN_IS_IMPORTED = 'is_imported';
    const _APOLLO_IEF_NUM_NUM_PER_PAGE = 5;
    const _APOLLO_IEF_COLUMN_USER_ID = 'user_id';
    const _APOLLO_IEF_COLUMN_REMOVE_ACTION = 'remove_action';

    const _APOLLO_IEF_DB_COLUMN_ID = 'id';
    const _APOLLO_IEF_DB_COLUMN_FILENAME = 'file_name';
    const _APOLLO_IEF_DB_COLUMN_USER_ID = 'user_id';
    const _APOLLO_IEF_DB_COLUMN_IMPORTED_DATE = 'import_date';
    const _APOLLO_IEF_DB_COLUMN_IS_IMPORTED = 'imported';
}

class APL_Theme_Option_Site_Config_SubTab {
    const GENERAL_SETTING = 'general-settings-sub-tab';
    const EVENT_SETTING = 'event-settings-sub-tab';
    const BLOG_SETTING = 'blog-settings-sub-tab';
    const ORGANIZATION_SETTING = 'organization-settings-sub-tab';
    const PUBLIC_ART_SETTING = 'public-art-settings-sub-tab';
    const ARTIST_SETTING = 'artist-settings-sub-tab';
    const EDUCATION_SETTING = 'education-settings-sub-tab';
    const CLASSIFIED_SETTING = 'classified-settings-sub-tab';
    const VENUE_SETTING = 'venue-settings-sub-tab';
    const PROGRAMS_SETTING = 'programs-settings-sub-tab';
    const SEARCH_SETTING = 'search-settings-sub-tab';
    const BUSINESS_SETTING = 'business-settings-sub-tab';
    const SYNDICATION_SETTING = 'syndication-settings-sub-tab';
    const GLOBAL_SEARCH_TABS = 'global_search_tab';
    const NEWS_SETTING = 'news-settings-sub-tab';
    const PLACE_GLOBAL_SEARCH = 'place_global_search';

    const ARTIST_TYPE_SEARCH_WIDGET_LABEL = 'artist_type_search_widget_label';
    const ARTIST_STYLE_SEARCH_WIDGET_LABEL = 'artist_style_search_widget_label';
    const ARTIST_MEDIUM_SEARCH_WIDGET_LABEL = 'artist_medium_search_widget_label';

    const ARTIST_FORM_MAX_OTHER_TYPE = 'artist_form_max_other_type';
    const ARTIST_FORM_MAX_OTHER_STYLE = 'artist_form_max_other_style';
    const ARTIST_FORM_MAX_OTHER_MEDIUM = 'artist_form_max_other_medium';
    const ARTIST_FORM_MAX_DESCRIPTION_FIELD = "artist_form_max_description_field";
    const ARTIST_FORM_MAX_OTHER_DEFAULT = 100;
    const ARTIST_FORM_MAX_DESCRIPTION_DEFAULT = 1500;

    const ARTIST_ENABLE_TYPE_SEARCH_WIDGET = 'artist_enable_type_search_widget';
    const ARTIST_ENABLE_STYLE_SEARCH_WIDGET = 'artist_enable_style_search_widget';
    const ARTIST_ENABLE_MEDIUM_SEARCH_WIDGET = 'artist_enable_medium_search_widget';
    const _ARTIST_ENABLE_OPTION_NEW_ARTIST = '_enable_option_new_artist';
    const _ARTIST_PHOTO_FORM_LOCATION = '_artist_photo_form_location';


    const _APL_EVENT_TICKET_REGISTRATION_DESCRIPTION_CHARACTER_LIMIT = '_apl_event_ticket_registration_description_character_limit';
    const _APL_ENABLE_EVENT_SUMMARY_FIELD = '_enable_event_summary_field';
    const _APL_ENABLE_EVENT_REGISTERED_ORGANIZATIONS_FIELD = '_enable_event_registered_organizations_field';
    const _APL_ENABLE_EVENT_ACCESSIBILITY_FIELD = '_enable_event_accessbility_field';
    const _APL_ENABLE_EVENT_TIMEINFO_FIELD = '_enable_event_timeinfo_field';

    const _APL_EVENT_DESCRIPTION_CHARACTER_LIMIT = '_apl_event_description_character_limit';
    const _APL_EVENT_TEXT_OF_KEYWORD_SEARCH = '_apl_event_text_of_keyword_search';

    /**
     * @ticket #19185: [CF] 20190215 - Add a character limit field for the event title field
     */
    const _APL_EVENT_TITLE_CHARACTERS_LIMIT = '_apl_event_title_character_limit';
    const _APL_EVENT_TITLE_CHARACTERS_LIMIT_DEFAULT = 150;

    const _APL_SHARE_EMAIL_WITH_FRIENT_METHOD = '_apl_share_post_with_friend_method';

    const _VENUE_ENABLE_NEIGHBORHOOD_REQUIREMENT = '_venue_enable_neighborhood_requirement';

    /* @ticket #14597 */
    const _APL_ICON_SIZE = '_apl_icon_size';
    /** @Ticket #15733 */
    const _ENABLE_RESET_BUTTON_ON_HORIZONTAL_TAB    = '_enable_reset_button_on_horizontal_tab';

    /** @Ticket #16589 #16603 */
    // const for geolocation option
    const _ENABLE_ENVENT_BY_MY_LOCATION_CHECKBOX = '_enable_event_by_my_location_checkbox';
    const _OC_GEO_LOCATION_ENABLE = '_oc_geo_location_enable';

    const _EVENT_DETAIL_DISPLAY_ALL_ICON = '_oc_event_detail_display_all_icon';
    const _EVENT_HOME_SPOTLIGHT_AUTO_FILL = '_event_home_spotlight_auto_fill';

    const _EDUCATOR_PROGRAM_ENABLE_STANDARDS = '_educator_program_enable_standards';
    const _EDUCATOR_COMMENT_FIELD = '_educator_comment_field';
    /**
     * @ticket #18497: 0002504: Arts Education Customizations - Change home link on all page custom arts ed masthead
     */
    const _EDUCATOR_ENABLE_OVERRIDE_BREADCRUMB_LINK  = '_educator_enable_override_home_url';
    const _EDUCATOR_BREADCRUMB_LINK = '_educator_breadcrumb_link';
    const _BLOG_ENABLE_OVERRIDE_BREADCRUMB_LINK  = '_blog_enable_override_breadcrumb_link';
    const _BLOG_BREADCRUMB_LINK  = '_blog_breadcrumb_link';

    const _BLOG_ENABLE_DEFAULT_IMAGE = '_blog_enable_default_image';
    const _BLOG_ENABLE_EXCERPT = '_blog_enable_excerpt';
    const _BLOG_ENABLE_THUMBS_UP = '_blog_enable_thumbs_up';

    const _PROGRAM_ENABLE_REQUIREMENT_MAX_STUDENTS = '_program_enable_requirement_max_students';
    const _PROGRAM_ENABLE_DISPLAY_COPY_BUTTON = '_program_enable_display_copy_button';
    const _PROGRAM_ENABLE_DISPLAY_DELETE_BUTTON = '_program_enable_display_delete_button';

    const _BUSINESS_TYPE_CUSTOM_SLUG = '_business_type_custom_slug';

    /**
     * @author vulh
     * @ticket #19027 - 0002522: wpdev54 Customization - Custom venue slug
     */
    const VENUE_CUSTOM_SLUG = '_apl_venue_custom_slug';
    const VENUE_TYPE_CUSTOM_SLUG = '_apl_venue_type_slug';
    const VENUE_CUSTOM_LABEL = '_apl_venue_custom_label';
    const VENUE_WIDGET_SEARCH_VENUE_TYPE_LABEL = '_apl_widget_search_venue_type_label';
}

class APL_Theme_Option_Email_Engine_SubTab {
    const EMAIL_OPTION = 'email-options-sub-tab';
    const EMAIL_TEMPLATE = 'email-template-sub-tab';
}

class APL_Business_Module_Theme_Option {
    const TAB_NAME_DEFAULT_VALUE = 'Make it A Night';
    const TAB_NAME = 'business_tab_name';

    const INTRODUCTION = 'business_introduction';

    const DINING_SELECT = 'business_ddl_dining';

    const ACCOMMODATION_SELECT = 'business_ddl_accommodation';

    const BARS_CLUBS_SELECT = 'business_ddl_bars_clubs';

    const LOCAL_BUSINESS_SELECT = 'business_ddl_local_business';

    const DISPLAY_BUSINESS_TYPES = 'business_displayed_business_type';

    const DISPLAY_BS_TYPE_WITHIN = 'business_displayed_bs_type_within';

    const WHATS_NEARBY_MAP_ACTIVE = 'business_whats_nearby_map_active';
    const WHATS_NEARBY_STATIC = 'business_whats_nearby_static';
    const _BUSINESS_DEFAULT_VIEW_TYPE = '_business_default_view_type';
    const _BUSINESS_VIEW_TYPE = '_business_view_type';
    const _BUSINESS_NUM_ITEMS_LISTING_PAGE = '_business_num_items_listing_page';
    const _ENABLE_BUSINESS_TAXONOMY_TITLE    = '_enable_business_taxonomy_title';
    const _BUSINESS_ENABLE_FEATURE_SEC = '_business_enable_feature_sec';
    const _BUSINESS_ENABLE_SPOT = '_business_enable_spot';
    const _BUSINESS_ENABLE_UPCOMING_EVENTS = '_business_enable_upcoming_events';
    const _BUSINESS_ENABLE_NEARBY_EVENTS = '_business_enable_nearby_events';
    const _BUSINESS_FE_DISPLAY_ALL_FIELDS = '_business_fe_display_all_fields';
    const _BUSINESS_SERVICES_LABEL = '_business_services_label';

    const GOOGLE_MAP_ICON = 'google_map_icon';

    const YOUR_LOCATION_ICON = 'your_location_icon';


}

class APL_Dashboard_Hor_Tab_Options {
    const ORG_MANAGE_LIST_URL = 'user/org/manage-listing';
    const ORG_ADD_NEW_URL = 'user/org/add-new';
    const ORG_PROFILE_URL = 'user/org/profile';
    const ORG_PHOTOS_URL = 'user/org/photos';
    const ORG_AUDIO_URL = 'user/org/audio';
    const ORG_VIDEO_URL = 'user/org/video';

    const VENUE_MANAGE_LIST_URL = 'user/venue/manage-listing';
    const VENUE_ADD_NEW_URL = 'user/venue/add-new';
    const VENUE_PROFILE_URL = 'user/venue/profile';
    const VENUE_PHOTOS_URL = 'user/venue/photos';
    const VENUE_VIDEO_URL = 'user/venue/video';

    /* @Ticket #15207 Artist */
    const ARTIST_MANAGE_LIST_URL = 'user/artist/manage-listing';
    const ARTIST_ADD_NEW_URL = 'user/artist/add-new';
    const ARTIST_PROFILE_URL = 'user/artist/profile';
    const ARTIST_PHOTOS_URL = 'user/artist/photos';
    const ARTIST_VIDEO_URL = 'user/artist/video';
    const ARTIST_AUDIO_URL = 'user/artist/audio';
    const ARTIST_EVENTS_URL = 'user/artist/events';

    /* Agency - Org */
    const AGENCY_ORG_PROFILE_URL = 'user/agency/org';
    const AGENCY_ORG_PHOTOS_URL = 'user/agency/org/photo';
    const AGENCY_ORG_AUDIO_URL = 'user/agency/org/audio';
    const AGENCY_ORG_VIDEO_URL = 'user/agency/org/video';

    /* Agency - Venue */
    const AGENCY_VENUE_PROFILE_URL = 'user/agency/venue';
    const AGENCY_VENUE_PHOTOS_URL = 'user/agency/venue/photo';
    const AGENCY_VENUE_VIDEO_URL = 'user/agency/venue/video';

    /* Agency - Artist - @ticket #15268 */
    const AGENCY_ARTIST_PROFILE_URL = 'user/agency/artist';
    const AGENCY_ARTIST_PHOTOS_URL  = 'user/agency/artist/photos';
    const AGENCY_ARTIST_VIDEO_URL   = 'user/agency/artist/video';
    const AGENCY_ARTIST_AUDIO_URL   = 'user/agency/artist/audio';
    const AGENCY_ARTIST_EVENTS_URL  = 'user/agency/artist/events';

    /* Event */
    const EVENT_PROFILE_URL = 'user/add-event/step-1';
    const EVENT_DELETE_URL = 'user/event/delete';
    const EVENT_COPY_URL = 'user/copy-event/step-1';

    /* Program*/
    const PROGRAM_PROFILE_URL = 'user/education/add-program';
    const PROGRAM_COPY_URL = 'user/copy-program/add-program';

}

class APL_Syndication_Const{
    const META_SFACTIVE = 'meta-sfactive';
    const META_ANALYTICS = 'meta-analytics';
    const META_DESC = 'meta-desc';
    const META_TOKEN_KEY = 'meta-token-key';
    const META_STATUS = 'meta-status';
    const META_INCLUDE_PRI_CATE = 'meta-include-primary-category';
    const META_MODE = 'meta-mode';
    const META_TYPE = 'meta-type';
    const META_BLACKLIST = 'meta-blacklist';
    const META_DATE_START = 'meta-dateStart';
    const META_DATE_END = 'meta-dateEnd';
    const META_RANGE = 'meta-range';
    const META_CITY_ZIP_OPTION = 'meta-cityzip';
    const META_ORGS = 'meta-orgs';
    const META_VENUE = 'meta-venues';
    const META_CITIES = 'meta-cities';
    const META_ZIPS = 'meta-zips';
    const META_ALLOW_DISPLAY_FIELD = 'meta-allown';
    const META_OWNSEL = 'meta-ownsel';
    const META_IFRAME = 'meta-iframe';
    const ENABLE_FILE_CACHING = 'enable-file-caching';
    const NUM_EVENTS = 'syndication_num_events';
    const META_LANDING = 'meta-landing';
    const META_SEARCH = 'meta-search';
    const META_SUBMIT = 'meta-submit';
    const META_ENABLE_EVENT_IMAGE_FOR_IFRAME_WIDGET = 'meta-enable-event-image-for-iframe-widget';
    const META_CUSTOM_URL = 'meta-custom-url';
    const META_ORG_COL = 'meta-orgcol';
    const META_LOCATION_COL = 'meta-loccol';
    const META_LINK_DESTINATION = 'meta-linkdest';
    const META_IMAGE = 'meta-image';
    const META_WID_UPPER = 'meta-widupper';
    const META_WID_DESC = 'meta-widdesc';
    const META_WID_LOWER = 'meta-widlower';
    const META_WID_CSS = 'meta-widcss';
    const META_CACHE = 'meta-cache';
    const META_SORT = 'meta-sort';
    const META_EVENT_IDS = 'meta-eventID';
    const META_EXCLUDE_PRIVATE_EVENTS = 'meta-exclude-private-events';

    // Displayed fields of showing result
    const DF_EVENT_DESCRIPTION = 'eventDescription';
    const DF_EVENT_DATETIME = 'eventDatesTimes';
    const DF_ORG_NAME = 'orgName';
    const DF_ORG_IMAGE = 'orgImage';
    const DF_VENUE_NAME = 'venue';
    const DF_EVENT_SPOTLIGHT = 'spotlight';
    const DF_EVENT_FEATURED = 'featured';
    const DF_SUB_CATE_NAME = 'secondaryType';
    const DF_EVENT_TICKET_INFO = 'eventTicketInfo';
    const DF_CONTACT_PHONE_NUMBER = 'eventPhone1';
    const DF_CONTACT_EMAIL = 'eventEmail';
    const DF_TICKET_URL = 'eventTicketUrl';
    const DF_DISCOUNT_URL = 'discountUrl';
    const DF_EVENT_START_TIME = 'eventStartTime';
    const DF_EVENT_CREATED_DATE = 'datePosted';
    const DF_EVENT_LINK = 'eventLink';
    const DF_EVENT_GU_ID = 'eventGuid';


    /*@ticket 18228:  Artist Directory Plugin > Admin > Syndication - Artist configuration*/
    const _SYNDICATION_ALL_ARTIST= '_syndication_all_artist';
    const _SYNDICATION_SELECTED_ARTIST = '_syndication_selected_artist';
    const _SYNDICATION_ARTIST_LIMIT = '_syndication_artist_limit';
    const _SYNDICATION_ARTIST_ENABLE_CACHE = '_syndication_artist_enable_cache';
    const _SYNDICATION_ARTIST_ORDERBY = '_syndication_artist_orderby';
    const _SYNDICATION_ARTIST_ORDER = '_syndication_artist_order';

    const _SA_META_ARTIST_ALL = '_meta_artist_all';
    const _SA_META_ARTIST_SELECTED = '_meta_artist_selected';
    const _SA_META_ARTIST_LIMIT ='_meta_artist_limit';
    const _SA_META_ARTIST_ENABLE_CACHE = '_meta_artist_enable_cache';
    const _SA_META_ARTIST_ORDER = '_meta_artist_order';
    const _SA_META_ARTIST_ORDER_BY = '_meta_artist_order_by';

    /**
     * @ticket #19069:  [Organization] Syndication form
     */
    const _SO_META_ORG_ALL = '_meta_org_all';
    const _SO_META_ORG_SELECTED = '_meta_org_selected';
    const _SO_META_ORG_LIMIT ='_meta_org_limit';
    const _SO_META_ORG_ORDER = '_meta_org_order';
    const _SO_META_ORG_ORDER_BY = '_meta_org_order_by';

}
//
//class APL_Listing_Pages_Slug{
//    static $pagesSlug = array(
//        Apollo_DB_Schema::
//    );
//}

class APL_Select2_Remote_Data{
    // Select2 - Remote Data - Limitation - Backend
    const _APL_NUMBER_POST_TO_APPLY_REMOTE_DATA_SELECT2 = 5;

}

class APL_Iframe_Search_Widget_Const {
    const META_COLOR = 'meta_color';
    const META_COLOR_BEFORE_SAVE = 'meta_color_before_save';
    const META_IMAGE = 'meta_image';
    const META_HTML_AFTER = 'meta_html_after';
    const META_HTML_BEFORE = 'meta_html_before';
    const META_ENABLE_CITIES = 'meta_enable_cities';
    const META_ENABLE_VENUE = 'meta_enable_venue';
    const META_ENABLE_CATEGORY = 'meta_enable_category';
    const META_ENABLE_REGION = 'meta_enable_region';
    const META_CITIES = 'meta_cities';
    const META_DEFAULT_CITY = 'meta_default_city';
    const META_VENUE = 'meta_venues';
    const META_VENUES = 'meta_venue_list';
    const META_CATEGORY = 'meta_categories';
    const META_REGION = 'meta_regions';
    const META_IFRAME_URL = 'meta-iframe-url';
    const META_IFRAME_CODE = 'meta-iframe-code';

}

class APL_Photo_Slider_Const{
    const NUMBER_IMAGES_ORG = 'number_images_organization';
    const NUMBER_IMAGES_ARTIST = 'number_images_artist';
    const NUMBER_IMAGES_PUBLIC_ART = 'number_images_public-art';
    const NUMBER_IMAGES_PUBLIC_EVENT = 'number_images_event';
    const NUMBER_IMAGES_PUBLIC_VENUE = 'number_images_venue';
    const NUMBER_IMAGES_PUBLIC_CLASSIFIED = 'number_images_classified';
    const NUMBER_IMAGES_PUBLIC_EDUCATION = 'number_images_education';
    const PHOTO_SLIDER_SHORT_CODE = 'photo_slider_short_code';
    const ENABLE_TITLE_SLIDER = 'enable_title_slider';
    const ENABLE_DESC_SLIDER = 'enable_desc_slider';
    const ENABLE_TITLE_SLIDER_ITEMS = 'enable_title_slider_items';
}

class APL_Event_CSV_Import {
    static $arrayEventKeyToColIndex = array(
        'eventname' => 0,
        'orgname' => 1,
        'venuename' => 2,
        'description' => 3,
        'event-category' => 4,
        'event-sub-category' => 5,
        'event-url' => 6,
        'event-phone' => 7,
        'event-email' => 8,
        'ticket-info' => 9,
        'ticket-url' => 10,
        'start-date' => 11,
        'end-date' => 12,
        'day1' => 13,
        'day2' => 14,
        'day3' => 15,
        'day4' => 16,
        'day5' => 17,
        'day6' => 18,
        'day0' => 19,
        'image' => 20,
        'contactname' => 21,
        'contactphone' => 22,
        'contactemail' => 23
    );
}

class Apollo_Theme_Options_Default_Template{
    static $themeOptsDefaultTemplates = array(
        Apollo_DB_Schema::_HEADER => APOLLO_THEME_HEADER ,
        Apollo_DB_Schema::_TOP_HEADER => APOLLO_THEME_TOP_HEADER,
        Apollo_DB_Schema::_FOOTER1 => APOLLO_THEME_FOOTER1,
        Apollo_DB_Schema::_FOOTER2 => APOLLO_THEME_FOOTER2,
        Apollo_DB_Schema::_FOOTER3 => APOLLO_THEME_FOOTER3,
        Apollo_DB_Schema::_FOOTER4 => APOLLO_THEME_FOOTER4,
        Apollo_DB_Schema::_FOOTER5 => APOLLO_THEME_FOOTER5,
        Apollo_DB_Schema::_FOOTER5 => APOLLO_THEME_FOOTER5,
        Apollo_DB_Schema::_OVERRIDE_CSS => APOLLO_THEME_OVERRIDE_CSS,
        Apollo_DB_Schema::_TOP_SUB_CONTAIN => APOLLO_THEME_SUB_CONTAIN_TOP,
        Apollo_DB_Schema::_BOTTOM_SUB_CONTAIN => APOLLO_THEME_SUB_CONTAIN_BOTTOM
    );
}

class APL_News_Config{
    /*@ticket 18127: News Module*/
    const _NEWS_NUM_OF_CHAR = '_apl_news_num_of_char';
    const _NEWS_CUSTOM_SLUG = '_apl_news_rename_module';
    const _NEWS_CUSTOM_LABEL = '_apl_news_custom_label';
    const _NEWS_LISTING_TYPE = '_apl_news_listing_type';
    const _NEWS_DISPLAY_STYLE = '_apl_news_display_styler';
    const _NEWS_CATEGORY_TYPES = '_apl_news_category_types';
    const _NEWS_CATEGORY_PAGE_ORDER = '_apl_news_category_page_order';
    const _NEWS_NUM_OF_CHAR_DEFAULT = 350;
}