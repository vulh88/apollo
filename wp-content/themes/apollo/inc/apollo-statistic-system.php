<?php
class Apollo_Statistic_System {

    private  static $statisticPath = '';
    private static $visitFile = 'visit';
    private static $limit = 10;
    private static $currentDate = '';


    /**
     * Get table prefix
     * @param boolean $isSingleSite
     * @param integer $blog_id
     * @return integer
    */
    public static function getPrefixTbl($isSingleSite, $blog_id) {
        global $wpdb;

        if ($isSingleSite) {
            return $wpdb->prefix;
        }

        return BLOG_ID_CURRENT_SITE === $blog_id ? $wpdb->prefix : $wpdb->prefix. $blog_id. '_';

    }

    public static function statisticVisitEventForBlog($blog_id, $arrCondition = array(), $echoQuery = true, $isSingleSite = false)
    {
        global $wpdb;

        $default = array(
            'start' => 0,
            'pagesize' => Apollo_Display_Config::NUM_TOPTEN_PAGESIZE,
        );

        $arrCondition = wp_parse_args($arrCondition, $default);
        $prefix = $wpdb->prefix;


        $tblvisit = $wpdb->base_prefix . Apollo_Tables::_APL_TOPTEN;
        $tbleventmeta = $prefix . Apollo_Tables::_APOLLO_EVENT_META;
        $tblpost = $prefix . 'posts';

        if(php_sapi_name() === 'cli') {

            if(BLOG_ID_CURRENT_SITE === $blog_id) {
                $tblvisit = $wpdb->base_prefix . Apollo_Tables::_APL_TOPTEN;
                $tbleventmeta = $wpdb->base_prefix . Apollo_Tables::_APOLLO_EVENT_META;
                $tblpost = $wpdb->base_prefix . 'posts';
            }
        }

        $meta_key_end_date = Apollo_DB_Schema::_APOLLO_EVENT_END_DATE;
        $meta_key_start_date = Apollo_DB_Schema::_APOLLO_EVENT_START_DATE;

        // UTC Time
        self::$currentDate = date('Y-m-d H:i:s', time());


        $limitOngoing = Apollo_App::orderDateRange();

        $comparedTime = self::getPastTimeAtRunningCron($blog_id);

        $sql = "
            SELECT item_id, post_title, end_date, count, start_date
            FROM (
                SELECT  wuv.item_id, wp.post_title, we.meta_value as end_date, wuv.count as count, startDate.meta_value as start_date
                FROM $tblvisit AS wuv
                INNER JOIN $tbleventmeta we ON we.apollo_event_id = wuv.item_id AND we.meta_key = '{$meta_key_end_date}' AND we.meta_value > '".self::$currentDate."'
                INNER JOIN $tblpost wp ON wp.ID = wuv.item_id AND wp.post_status = 'publish'
                INNER JOIN $tbleventmeta startDate ON startDate.apollo_event_id = wuv.item_id AND startDate.meta_key = '$meta_key_start_date'
                WHERE blog_id = '$blog_id' AND latest_visitation >= '$comparedTime' AND DATEDIFF( we.meta_value, startDate.meta_value ) < '$limitOngoing'
                AND startDate.meta_value <  DATE_ADD(CURDATE(), INTERVAL 2 MONTH)
				AND DATEDIFF(
				we.meta_value,
				startDate.meta_value
				) < 31
                GROUP BY wuv.item_id
                ORDER BY wuv.count DESC
                LIMIT ".self::$limit."
            ) as countTbl

            ORDER BY end_date ASC
        ";

        /*Run cron jon by manual*/
        if ($echoQuery) {
            echo $sql. '<br/>';
        }

        /**
         * Get 10 first events
         * * @Ticket #15340
         * */
        if ($data = $wpdb->get_results($sql)) {
            $data = array_slice($data, 0, self::$limit);
        }

        // Auto fill out more data to make sure there are at least 10 events in the list
        $fillOutNumber = self::$limit - count($data);

        if ($data && $fillOutNumber > 0) {

            $existedArr = array();

            foreach ($data as $item) {
                $existedArr[] = $item->item_id;
            }

            $existeParseStr = implode(',', $existedArr);
            $sql = "
                SELECT (case when (DATEDIFF( end_date, startDate.meta_value ) >= '$limitOngoing') THEN 1000 ELSE 0 END) AS date_range, end_date, item_id, post_title, startDate.meta_value as start_date
                FROM (
                    SELECT  wuv.item_id, wp.post_title, we.meta_value as end_date
                    FROM $tblvisit AS wuv
                    INNER JOIN $tbleventmeta we ON we.apollo_event_id = wuv.item_id AND we.meta_key = '{$meta_key_end_date}' AND we.meta_value > '".self::$currentDate."'
                    INNER JOIN $tblpost wp ON wp.ID = wuv.item_id AND wp.post_status = 'publish'
                    WHERE blog_id = '$blog_id' AND wuv.item_id NOT IN ($existeParseStr)
                    GROUP BY wuv.item_id
                    ORDER BY wuv.count DESC, latest_visitation DESC
                    LIMIT $fillOutNumber
                ) as countTbl

                INNER JOIN $tbleventmeta startDate ON startDate.apollo_event_id = countTbl.item_id AND startDate.meta_key = '$meta_key_start_date'
                ORDER BY date_range ASC, end_date ASC
            ";

            $fillOutData = $wpdb->get_results($sql);
        }

        return isset($fillOutData) && $fillOutData ? array_merge($fillOutData, $data) : $data;
    }

    public static function getPastTimeAtRunningCron($blogId) {

        $table = $blogId == 1 ? 'wp_options' : sprintf("wp_%s_options", $blogId);
        $aplQuery = new Apl_Query($table, true);
        $options = $aplQuery->get_row("option_name = '_apollo_theme_options'");

        $v = $options ? maybe_unserialize($options->option_value) : '';
        $configHours = $v && isset($v['top_ten_hours_ago']) ? $v['top_ten_hours_ago'] : 48;

        return date("Y-m-d H:i:s", strtotime("-$configHours hours"));
    }

    public static function exportTopVisitEventForDomain($blog_id, $type, $showQuery = true, $isSingle = false, $cleanData = true)
    {
        $data = self::statisticVisitEventForBlog($blog_id, array(), $showQuery, $isSingle);

        if (!$isSingle) {
            aplDebug($data);
        }

        if ($cleanData) {
            // Remove all top ten data of current site
            global $wpdb;
            $comparedTime = self::getPastTimeAtRunningCron($blog_id);

            $mtTbl = ($blog_id == 1 ? $wpdb->base_prefix : $wpdb->base_prefix.$blog_id.'_'). Apollo_Tables::_APOLLO_EVENT_META;
            $topTenTbl = $wpdb->base_prefix. Apollo_Tables::_APL_TOPTEN;


            if(self::canDelete($blog_id, $comparedTime, $isSingle)) {
                $sql = "
                    DELETE FROM $topTenTbl
                    WHERE item_id IN (
                        SELECT tmp.item_id FROM (
                            SELECT tt.item_id
                            FROM $topTenTbl tt
                            INNER JOIN $mtTbl mt ON tt.item_id = mt.apollo_event_id
                            WHERE blog_id = '$blog_id' AND item_type = 'event' AND ((mt.meta_key = '%s' and mt.meta_value < '%s') OR latest_visitation < '%s')
                        ) as tmp
                    )
                ";
                $sql = $wpdb->prepare($sql, Apollo_DB_Schema::_APOLLO_EVENT_END_DATE, self::$currentDate, $comparedTime);

                $wpdb->query($sql);
            }
        }

        /*Run cron jon by manual*/
        if (isset($_GET['crontab'])) {
            var_dump($blog_id);
            aplDebug($data);
            echo '------------------------------<br/>';
        }

        if(!$data) {
            return array(
                'error' => true,
                'msg' => 'No data found for blog: '.$blog_id,
            );
        }

        $blog = get_blog_details($blog_id);

        $path = WP_CONTENT_DIR.'/uploads/sites/'.$blog->domain;

        wp_mkdir_p($path);

        $path = $path.'/'.self::$visitFile;
        $resultExport = self::exportTo($path, $data, $type);

        if ($resultExport !== true) {
            return array(
                'error' => true,
                'msg' => $resultExport
            );
        }

        // Vu fix update option for each site
        update_blog_option($blog_id, Apollo_DB_Schema::_TOPTEN_CACHE_FLAG_DETECTION, 1);

        // Thienld : Turn On flag for alert that have new file was wrote from cron job system.
        //update_site_option(Apollo_DB_Schema::_TOPTEN_CACHE_FLAG_DETECTION,1);


        return array(
            'error' => false,
            'msg' => __('Refresh Top Ten data successully', 'apollo')
        );
    }


    /**
     * Check can delete top ten data.
     *
     * @param integer $blog_id
     * @param datetime $compareTime
     * @return boolean
    */
    public static function canDelete($blog_id, $comparedTime, $isSingleSite) {
        global $wpdb;
        $mtTbl = ($blog_id == 1 ? $wpdb->base_prefix : $wpdb->base_prefix.$blog_id.'_'). Apollo_Tables::_APOLLO_EVENT_META;
        $topTenTbl = $wpdb->base_prefix. Apollo_Tables::_APL_TOPTEN;
        $prefix = $wpdb->prefix;
        $tblpost = $prefix . 'posts';
        $sql = "SELECT count(*) as total
                FROM $topTenTbl tt
                INNER JOIN $mtTbl mt ON tt.item_id = mt.apollo_event_id
                INNER JOIN $tblpost wp ON wp.ID = tt.item_id AND wp.post_status = 'publish'
                WHERE blog_id = '$blog_id' AND item_type = 'event' AND ((mt.meta_key = '%s' and mt.meta_value >= '%s') AND latest_visitation >= '%s')
        ";

        $sql = $wpdb->prepare($sql, Apollo_DB_Schema::_APOLLO_EVENT_END_DATE, self::$currentDate, $comparedTime);

        $total = $wpdb->get_var($sql);
        return $total && $total > self::$limit;
    }

    public static function getStorePath($blog_id) {
        $blog = get_blog_details($blog_id);

        $uploadPath = Class_Upload_Path_Custom::getConfig(true);
        $storePathRoot = isset($uploadPath['upload_path'])?$uploadPath['upload_path']:WP_CONTENT_DIR;

        $currentSiteUrl = get_option(Class_Upload_Path_Custom::SITE_URL);

        if($storePathRoot === WP_CONTENT_DIR || empty($uploadPath['upload_url_path'] ) || $uploadPath['upload_url_path'] == $currentSiteUrl){
            return WP_CONTENT_DIR.'/uploads/sites/'.$blog->domain;
        }
        return $storePathRoot.'/sites/'.$blog->domain;
    }

    public static function getVisitFilePath($blog_id, $type = 'json') {
        return self::getStorePath($blog_id).'/'.self::$visitFile.'.'.$type;
    }

    /**
     * @param        $path
     * @param        $data
     * @param string $type
     * @return boolean or string
     */
    private  static function exportTo($path, $data, $type = 'json')
    {
        switch($type) {
            case 'json':
                $path .= '.json';
                if(file_exists($path) && !is_writable($path)) {
                    return 'Permission deny! Please set writable for file '. $path;
                    break;
                }

                file_put_contents($path, json_encode($data), LOCK_EX);
                break;
        }
        return true;
    }

    public static function sortByAlphaDESC($a, $b) {
        return $a['post_title'] < $b['post_title'];
    }

    public static function sortByAlphaASC($a, $b) {
        return $a['post_title'] > $b['post_title'];
    }

    public static function sortByEnddateASC($a, $b) {
        return $a['end_date'] > $b['end_date'];
    }

    public static function sortByStartdateASC($a, $b) {
        return $a['start_date'] > $b['start_date'];
    }

    public static function getTopVisitEventForDomain($blog_id)
    {

        $type = 'json';
        $path = Apollo_Statistic_System::getVisitFilePath($blog_id, $type);

        if(!file_exists($path)) {
            return false;
        }

        $content = json_decode(file_get_contents($path), true);

//        $order = of_get_option( Apollo_DB_Schema::_EVENT_SETTING_ORDER );
//
//        switch ( $order ) {
//            case 'ALPHABETICAL_DESC':
//                usort($content, array(__CLASS__, 'sortByAlphaDESC'));
//                break;
//            case 'ALPHABETICAL_ASC':
//                usort($content, array(__CLASS__, 'sortByAlphaASC'));
//                break;
//
//            case 'END_DATE_ASC':
//                usort($content, array(__CLASS__, 'sortByEnddateASC'));
//                break;
//
//            case 'START_DATE_ASC':
//            default:
//                usort($content, array(__CLASS__, 'sortByStartdateASC'));
//                break;
//        }



        return $content;
    }

    /**
     * Sync events data to Solr server for each site
     *
     * @param integer $blogId
     *
     * @ticket #11018
     * @return array contains success or error information
     */
    public static function exportSolrEventDataForDomain($blogId)
    {
        try {
            // invalid blog ID
            if ( empty($blogId) || !is_numeric($blogId) ) {
                return array('error' => true, 'msg' => sprintf('Invalid blog ID: %d', $blogId));
            }

            // current blog hasn't enabled for Solr module
            if ( !Apollo_App::is_enable_solr_search($blogId) ) {
                return array('error' => true, 'msg' => sprintf('Solr is not enabled for blog ID: %d', $blogId));
            }

            // set appropriate Solr configuration for current blog
            $solr = new Apollo_Solr_Search();
            $solr->delete(); // remove old data
            $isSync = $solr->insertPost();

            if ( !$isSync ) {
                return array('error' => true, 'msg' => sprintf('Could not sync new events data for blog ID: %d', $blogId));
            }
            return array('error' => true, 'msg' => sprintf('Sync Solr events data successfully for blog ID: %d.', $blogId));
        } catch (Exception $ex) {
            return array('error' => true, 'msg' => sprintf('%s for Blog ID: %d', $ex->getMessage(), $blogId));
        }
    }
}