<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Apollo_User_Shortcode {

    const SC_LOGIN = 'apollo_login';
    const SC_REGIS = 'apollo_registration';
    const SC_LOGIN_FB = 'apollo_login_fb';
    const SC_RESET_PASS = 'apollo_reset_password';
    const SC_MY_ACCOUNT = 'apollo_my_account';
    const SC_LOGOUT = 'apollo_logout';
    const SC_CHANGE_MY_PASS = 'apollo_change_my_password';

    private $_loginHandler = null;
    private $_forgotPassHandler = null;
    private $_changeMyPassHandler = null;
    private $_registerHandler = null;
   
    public function __construct() {

        // Hook ajax function for login via facebook
        add_action('wp_ajax_nopriv_login_fb', array( $this, 'prefix_ajax_login_fb' ));
        add_action('wp_ajax_login_fb', array( $this, 'prefix_ajax_login_fb' ));
        
        $arr_shortcode = array(
            self::SC_REGIS => array( $this, 'handle_shortcode_registration' ),
            self::SC_LOGIN => array( $this, 'handle_shortcode_login' ),
            self::SC_LOGIN_FB => array( $this, 'handle_shortcode_login_fb' ),
            self::SC_RESET_PASS => array( $this, 'handle_shortcode_reset_password' ),
            self::SC_MY_ACCOUNT => array( $this, 'handle_shortcode_my_account' ),
            self::SC_LOGOUT => array( $this, 'handle_shortcode_logout' ),
            self::SC_CHANGE_MY_PASS => array( $this, 'handle_shortcode_change_my_pass' ),
        );

        foreach($arr_shortcode as $shorcode => $handler) {

            // shortcode hanler
            add_shortcode($shorcode, $handler);

            // preprocess shortcode
            $preprocess_func = array($handler[0], $handler[1].'_preprocess');

            if(method_exists($preprocess_func[0], $preprocess_func[1])) {
                add_action('template_redirect', $preprocess_func, 1);
            }

        }
    }

    public function handle_shortcode_change_my_pass_preprocess() {
        require_once __DIR__.'/users/lib/class-apollo-change-my-password-handler.php';

        if (!is_singular()) return;
        global $post;
        if (!empty($post->post_content)) {
            $regex = get_shortcode_regex();
            preg_match_all('/'.$regex.'/',$post->post_content,$matches);
            if (!empty($matches[2]) && in_array(self::SC_CHANGE_MY_PASS,$matches[2])) {
                $this->_changeMyPassHandler = new Apollo_Change_My_Password_Handler();

                /* Check type of change */
                $this->_changeMyPassHandler->process();
            }
        }

    }

    public function handle_shortcode_change_my_pass() {
        return $this->_changeMyPassHandler->renderTemplate();
    }

    public function handle_shortcode_my_account_preprocess()
    {
        if (!is_singular()) return;
        global $post;
        if (!empty($post->post_content)) {
            $regex = get_shortcode_regex();
            preg_match_all('/'.$regex.'/',$post->post_content,$matches);
            if (!empty($matches[2]) && in_array(self::SC_MY_ACCOUNT,$matches[2])) {
                if(!is_user_logged_in()) {
                    wp_redirect(get_home_url());
                }
            }
        }
    }

    public function handle_shortcode_registration_preprocess()
    {
        if (!is_singular()) return;
        global $post;
        if (!empty($post->post_content)) {
            $regex = get_shortcode_regex();
            preg_match_all('/'.$regex.'/',$post->post_content,$matches);
            if (!empty($matches[2]) && in_array(self::SC_REGIS,$matches[2])) {
                if(is_user_logged_in()) {
                    wp_redirect(get_home_url());
                }
            }
        }
    }

    public function handle_shortcode_my_account()
    {
        return '';
    }

    public function handle_shortcode_reset_password_preprocess() {
        require_once __DIR__.'/users/lib/class-apollo-forgot-password-handler.php';

        if (!is_singular()) return;
        global $post;
        if (!empty($post->post_content)) {
            $regex = get_shortcode_regex();
            preg_match_all('/'.$regex.'/',$post->post_content,$matches);
            if (!empty($matches[2]) && in_array(self::SC_RESET_PASS,$matches[2])) {
                $this->_forgotPassHandler = new Apollo_User_Forgot_Password_Hanler();

                $this->_forgotPassHandler->filter();

                if($this->_forgotPassHandler->isPost()) {
                    $this->_forgotPassHandler->resetPass();
                }
            }
        }
    }

    public function handle_shortcode_reset_password() {
        return $this->_forgotPassHandler->renderTemplate();
    }

    public function handle_shortcode_logout_preprocess()
    {
        if (!is_singular()) return;
        global $post;
        if (!empty($post->post_content)) {
            $regex = get_shortcode_regex();
            preg_match_all('/'.$regex.'/',$post->post_content,$matches);
            if (!empty($matches[2]) && in_array(self::SC_LOGOUT,$matches[2])) {
                wp_logout();
                wp_safe_redirect(get_home_url(get_current_blog_id()));
            }
        }
    }

    public function handle_shortcode_logout() {
        return '';
    }

    public function handle_shortcode_login_preprocess() {
        require_once __DIR__.'/users/lib/class-apollo-login-handler.php';

        if (!is_singular()) return;
        global $post;
        if (!empty($post->post_content)) {
            $regex = get_shortcode_regex();
            preg_match_all('/'.$regex.'/',$post->post_content,$matches);
            if (!empty($matches[2]) && in_array(self::SC_LOGIN,$matches[2])) {
                $this->_loginHandler = new Apollo_User_Login_Handler();

                $this->_loginHandler->filter();

                if($this->_loginHandler->isPost()) {
                    if($this->_loginHandler->validate()){
                        global $wp_rewrite;
                        $wp_rewrite->init();
                        $wp_rewrite->flush_rules();
                        $redirectTo = isset($_GET['redirect_to']) ? urldecode($_GET['redirect_to']) : get_home_url($GLOBALS['blog_id']).'/user/dashboard';
                        $this->_loginHandler->redirect($redirectTo);
                    }
                }
            }
        }
    }

    public function handle_shortcode_login() {
        return $this->_loginHandler->renderTemplate();
    }

    public function handle_shortcode_login_fb() {
        return $this->_loginHandler->renderTemplate(true);
    }

    public function handle_shortcode_registration() {
        require_once __DIR__.'/users/lib/class-apollo-register-handler.php';

        global $wpdb;
        $this->_registerHandler = new Apollo_User_Register_Handler();

        if ( $this->_registerHandler->isPost() )  {

            $this->_registerHandler->validate();
            
            if ( !$this->_registerHandler->hasError()) {
                
                $first_name = sanitize_text_field( $_POST['first_name'] );
                $last_name  = sanitize_text_field( $_POST['last_name'] );
                $username   = sanitize_user( $_POST['username'] );
                $password   = esc_attr( $_POST['password'] );
                $email      = sanitize_email( $_POST['email'] )  ;
                $zipcode    = sanitize_text_field( $_POST['zipcode'] )  ;
                $country    = isset($_POST['country']) ? sanitize_text_field( $_POST['country'] ) : ''  ;

                /* Role depend on config of wordpress */
                $userdata = array(
                    'first_name'    =>   $first_name,
                    'last_name'     =>   $last_name,
                    'user_login'    =>   $username,
                    'user_email'    =>   $email,
                    'user_pass'     =>   $password,
                    'zip_code'       =>   $zipcode,
                    'country'       =>   $country,
                );

                $user_id = wp_insert_user( $userdata);
                if($user_id instanceof WP_Error) {
                    $this->_registerHandler->setError($user_id);
                }

                else {
                    wp_localize_script( 'apollo-js-function' , 'apollo_registered_page', 'yes' );

                    if ( $user_id ) {
                     
                        do_action( 'apollo_email_registration', $first_name, $last_name, $username, $_POST['password'], $email);
                        
                        // Add user meta
                        add_user_meta( $user_id, 'zip_code', $zipcode );

                        if(!empty($country)) {
                            /**
                             * @ticket #18744
                             * Add country for user meta
                             */
                            add_user_meta($user_id, 'country', $country);
                        }

                        /* don't delete !!!important
                        although it also call in add_user_to_blog. but add_user_to_blog in this context will be error */
                        $blogUserTbl = Apollo_App::getBlogUserTable();
                        $wpdb->insert($blogUserTbl, array(
                            'blog_id' => intval($GLOBALS['blog_id']),
                            'user_id' => $user_id,
                        ));

                        /* add user blog */
                        $blog_id = $GLOBALS['blog_id'];
                        add_user_to_blog( $blog_id, $user_id, 'subscriber' );
                        update_user_meta( $user_id, 'primary_blog', $blog_id );
                        $details = get_blog_details($blog_id);
                        update_user_meta($user_id, 'source_domain', $details->domain);

                        /** @Ticket #15963 Redirect into the dashboard page */
                        require_once __DIR__.'/users/lib/class-apollo-login-handler.php';
                        $this->_loginHandler = new Apollo_User_Login_Handler();
                        if($this->_loginHandler->validate()){
                            global $wp_rewrite;
                            $wp_rewrite->init();
                            $wp_rewrite->flush_rules();
                            $this->_loginHandler->redirect(get_home_url($GLOBALS['blog_id']).'/user/dashboard');
                        }
                    }
                }

            }
        }

        return $this->_registerHandler->renderTemplate(); /* replace short code */
    }
    
    private function _register_form( $input, $success ) {
        global $apollo_reg_error;
        
        ob_start();
        include dirname(__FILE__) . '/users/templates/register-template.php';
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
        
    public function apollo_handle_filter_the_register_content( $content ) {
       
        if ( strpos( $content, '[apollo_registration]' ) && ! is_user_logged_in() ) {
            return '[apollo_registration]';
        } else {
            return $content;
        }
    }
    
    public function prefix_ajax_login_fb() {
        global $wpdb;
        $blog_id = $GLOBALS['blog_id'];

        $userData = $_POST['data'];
        /** @Ticket #13912 */
        if (empty($userData['email'])) {
            $domainUrl = get_site_url();
            $userData['email'] = $userData['id'] . "@" . str_replace(["http://", "https://", "www"], [""], $domainUrl);
        }
        $email = sanitize_email($userData['email']);
        $userInfo = get_user_by('email', $email);

        if (empty($userInfo)) { // check user exist, if user not exist, create user and send mail for user with email address of facebook
            // add user info into user table
            /** @Ticket #13912 */
            $first_name = isset($userData['first_name']) ? (sanitize_text_field($userData['first_name'])) : '';
            $last_name = isset($userData['last_name']) ? (sanitize_text_field($userData['last_name'])) : '';
            $username = $email;
            if (!empty($userData['name'])) {
                $display_name = $userData['name'];
            } else {
                $display_name = $email;
            }

            $password = wp_generate_password();

            $userdata = array(
                'user_login' => $username,
                'user_pass' => $password,
                'user_nicename' => $display_name,
                'user_email' => $email,
                'user_url' => '#',
                'user_regristerd' => date('c'),
                'display_name' => $display_name,
                'first_name' => $first_name,
                'last_name' => $last_name,
            );
            $user_id = wp_insert_user($userdata);

            if (!($user_id instanceof WP_Error)) {
                
                // send email wellcome (username,password) for user
                do_action( 'apollo_email_registration', $first_name, $last_name, $username, $password, $email );
                
                // add user info into usermeta table
                $apollo_fb_user_data = array( Apollo_DB_Schema::_FB_ID => $userData['id'], Apollo_DB_Schema::_FB_NAME => $userData['name']);
                $apollo_fb_user_data = json_encode($apollo_fb_user_data);
                add_user_meta($user_id, Apollo_DB_Schema::_APOLLO_FB_USER, $apollo_fb_user_data);

                /* all information we want to save */
                $blogUserTbl = Apollo_App::getBlogUserTable();
                $wpdb->insert($blogUserTbl, array(
                        'blog_id' => $blog_id,
                        'user_id' => $user_id,
                    ));

                /* add user blog */
                add_user_to_blog( $blog_id, $user_id, 'subscriber' );
                update_user_meta( $user_id, 'primary_blog', $blog_id );
                $details = get_blog_details($blog_id);
                update_user_meta($user_id, 'source_domain', $details->domain);

                wp_set_auth_cookie($user_id);
            }
            else {
                $return = array('mess' => __( '* Cannot create user', 'apollo' ));
                wp_send_json($return);
            }

        } else {
            wp_set_auth_cookie($userInfo->ID);
        }
    }
    
}

new Apollo_User_Shortcode();
