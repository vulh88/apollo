<?php
$loadingOverlay = '  <div class="main-slider-overlay">
         <div class="loading" style="display: none;"><a><i class="fa fa-spinner fa-spin fa-3x"></i></a></div>
     </div>';
// large slider


if (! Apollo_App::isDashboardPage() && (Apollo_App::is_homepage() || is_front_page() ) && (of_get_option( Apollo_DB_Schema::_SPOT_TYPE ) == 'large'  || of_get_option( Apollo_DB_Schema::_SPOT_TYPE ) == 'small' )):

    $hasEvtSeaWidInsideAltSpot = of_get_option(Apollo_DB_Schema::_HOR_EVENT_SEARCH_LOC) == 'ins_spot';
    ?>
        <?php
        if ($hasEvtSeaWidInsideAltSpot) {
            global $aplTopEventWidget;
            echo isset($aplTopEventWidget) ? $aplTopEventWidget : '';
        }
        ?>
    <?php

    require_once APOLLO_INCLUDES_DIR. '/admin/tools/cache/Inc/File.php';
    $cacheFileClass = aplc_instance('APLC_Inc_Files_HomeSpotlight');
    $html = $cacheFileClass->get();

    if (!$html):

        /** @Ticket #13274 */
        $spots = Apollo_App::getHomeSpotlights();

        ob_flush();
        ob_start();
        $semi_trans = of_get_option(Apollo_DB_Schema::_HOME_SEMI_TRANSPARENT_TYPE, 'def');
        $spot_type = of_get_option( Apollo_DB_Schema::_SPOT_TYPE );
        $semi_class_large = ($semi_trans === 'def') ? '' : 'semi-trans-large';
        $semi_class_small = ($semi_trans === 'def') ? '' : 'semi-trans-small';
        $bg_semi_trans = of_get_option( Apollo_DB_Schema::_HOME_SEMI_TRANSPARENT_TYPE_BACKGROUND_COLOR );
        ?>
        <section class="section-slider <?php echo $hasEvtSeaWidInsideAltSpot ? 'has-search-widget' : '' ?>">
        <?php
        if ($bg_semi_trans):
            ?>
            <style>
                .main-slider .wrap-inner {
                    background-color:  <?php echo $bg_semi_trans ?> !important;
                }
                .i-slider .i-caption {
                    background-color: <?php echo $bg_semi_trans ?> !important;
                    opacity: 0.7;
                }
            </style>
        <?php endif;
        if ( $spots &&  of_get_option( Apollo_DB_Schema::_SPOT_TYPE ) === 'large'):
        ?>
            <div class="main-slider full <?php echo $semi_class_large; ?>">
                <?php echo $loadingOverlay; ?>
                <div class="flexslider" data-speed="<?php echo Apollo_App::getSlideShowSpeed('home') ?>">
                    <ul class="slides">
                        <?php foreach ( $spots as $spt ):
                            $spt = get_spot( $spt );

                        $l = $spt->get_post_meta_data( Apollo_DB_Schema::_APL_SPOT_LINK );
                        $l = trim($l);
                        $_l = !empty($l) ? $l : 'javascript:void(0);';

                        $open_style = $spt->get_post_meta_data( Apollo_DB_Schema::_APL_SPOT_LINK_OPEN_STYLE );
                        $target = $open_style ? 'target = "blank"' : '';

                        $img_id = $spt->get_image_id();

                        $src_img = wp_get_attachment_image_src( $img_id, 'large');
                        $_o_summary = $spt->get_summary(Apollo_Display_Config::HOME_SPOTLIGHT_EVENT_OVERVIEW_NUMBER_CHAR);

                        $enableTitle = get_post_meta($spt->id , Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_TITLE ,true);

                        if ( ! $src_img[0] ) continue;
                        ?>

                        <li>
                            <?php if($semi_trans === 'def') : ?>
                                <div class="i-slider">
                                    <?php echo apply_filters('apl_render_cta_button', '', $spt->id, $target, $_l ); ?>
                                    <div class="inner">
                                        <?php if ( !empty(trim($_o_summary['text']))):
                                            ?>
                                        <div class="i-caption">
                                            <?php if($enableTitle === 'on' || !$enableTitle){ ?>
                                                <h4><a <?php echo $target ?> href="<?php echo $_l; ?>"><?php echo $spt->get_title(); ?></a></h4>
                                            <?php }?>
                                            <p>
                                                <?php echo $_o_summary['text']. ( $_o_summary['have_more'] ? '...' : '' ); ?>
                                            </p>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                    <a <?php echo $target ?> href="<?php echo $_l; ?>">
                                        <img src="<?php echo isset( $src_img[0] ) ? $src_img[0] : ''; ?>" alt="<?php echo $spt->get_title() ?>">
                                    </a>
                                </div>
                            <?php else : ?>
                                <?php echo apply_filters('apl_render_cta_button', '', $spt->id, $target, $_l ); ?>
                                <div class="i-slider">
                                    <a <?php echo $target ?> href="<?php echo $_l; ?>">
                                        <img src="<?php echo isset( $src_img[0] ) ? $src_img[0] : ''; ?>" alt="<?php echo $spt->get_title() ?>">
                                    </a>
                                </div>
                                <div class="wrap-inner">
                                    <div class="inner">
                                        <?php if ( !empty(trim($_o_summary['text'])) ):
                                            ?>
                                            <div class="i-caption">
                                                <?php if($enableTitle === 'on' || !$enableTitle){ ?>
                                                    <h4><a <?php echo $target ?> href="<?php echo $_l; ?>"><?php echo $spt->get_title(); ?></a></h4>
                                                <?php }?>
                                                <p>
                                                    <?php
                                                    echo $_o_summary['text']. ( $_o_summary['have_more'] ? '...' : '' );
                                                    ?></p>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </li>
                        <?php  endforeach; ?>
                    </ul>
                </div>
            </div>

            <?php elseif($spots): ?>
             <div class="inner">
                 <div class="main-slider mini <?php echo $semi_class_small; ?>">
                     <?php echo $loadingOverlay; ?>
                     <div class="flexslider" data-speed="<?php echo Apollo_App::getSlideShowSpeed('home') ?>">
                         <ul class="slides">
                             <?php foreach ( $spots as $spt ):
                                 $spt = get_spot( $spt );

                                 $l = $spt->get_post_meta_data( Apollo_DB_Schema::_APL_SPOT_LINK );
                                 $l = trim($l);
                                 $_l = !empty($l) ? $l : 'javascript:void(0);';

                                 $open_style = $spt->get_post_meta_data( Apollo_DB_Schema::_APL_SPOT_LINK_OPEN_STYLE );
                                 $target = $open_style ? 'target = "blank"' : '';

                                 $img_id = $spt->get_image_id();

                                 $src_img = wp_get_attachment_image_src( $img_id, 'large');
                                 $_o_summary = $spt->get_summary(Apollo_Display_Config::HOME_SPOTLIGHT_EVENT_OVERVIEW_NUMBER_CHAR);

                                 $enableTitle = get_post_meta($spt->id , Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_TITLE ,true);

                                 if ( ! $src_img[0] ) continue;
                                 ?>
                                 <li>
                                     <?php if($spot_type === 'small' && $semi_trans == 'def') : ?>
                                         <div class="i-slider">
                                             <div class="inner">
                                                 <?php if ( !empty(trim($_o_summary['text'])) ):
                                                     ?>
                                                     <div class="i-caption">
                                                         <?php if($enableTitle === 'on' || !$enableTitle){ ?>
                                                             <h4><a <?php echo $target ?> href="<?php echo $_l; ?>"><?php echo $spt->get_title(); ?></a></h4>
                                                         <?php }?>
                                                         <p>
                                                             <?php
                                                             echo $_o_summary['text']. ( $_o_summary['have_more'] ? '...' : '' );
                                                             ?></p>
                                                     </div>
                                                 <?php endif; ?>
                                             </div>
                                             <a <?php echo $target ?> href="<?php echo $_l; ?>">
                                                 <img src="<?php echo isset( $src_img[0] ) ? $src_img[0] : ''; ?>" alt="<?php echo $spt->get_title() ?>">
                                             </a>
                                         </div>
                                     <?php else : ?>
                                         <div class="i-slider">
                                             <a <?php echo $target ?> href="<?php echo $_l; ?>">
                                                 <img src="<?php echo isset( $src_img[0] ) ? $src_img[0] : ''; ?>" alt="<?php echo $spt->get_title() ?>">
                                             </a>
                                         </div>
                                         <div class="wrap-inner">
                                             <div class="inner">
                                                 <?php if ( !empty(trim($_o_summary['text'])) ):
                                                     ?>
                                                     <div class="i-caption">
                                                         <?php if($enableTitle === 'on' || !$enableTitle){ ?>
                                                             <h4><a <?php echo $target ?> href="<?php echo $_l; ?>"><?php echo $spt->get_title(); ?></a></h4>
                                                         <?php }?>
                                                         <p>
                                                             <?php
                                                             echo $_o_summary['text']. ( $_o_summary['have_more'] ? '...' : '' );
                                                             ?></p>
                                                     </div>
                                                 <?php endif; ?>
                                             </div>
                                         </div>
                                     <?php endif; ?>
                                 </li>
                             <?php endforeach; ?>
                         </ul>
                     </div>
                 </div>
             </div>

 <?php
        endif; ?>
        </section>
        <?php
        // Save cache
        $html = ob_get_contents();
        ob_end_clean();
        $cacheFileClass->save($html);

    endif;

    echo $html;
    ?>

<?php endif;


