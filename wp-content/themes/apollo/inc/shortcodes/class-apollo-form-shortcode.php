<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of class-apollo-form-shortcode
 *
 * @author vulh
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Apollo_Form_Shortcode {
    
    const SC_EDU_EVAL_FORM = 'apollo-teacher-evaluation-form';
    const SC_GRANT_EDU_FORM = 'apollo-grant-education-form';
    
    public function __construct() {
        $arr_shortcode = array(
            self::SC_EDU_EVAL_FORM => array($this, 'handle_shortcode_teacher_evaluation_form'),
            self::SC_GRANT_EDU_FORM => array($this, 'handle_shortcode_grant_education_form'),
        );
        foreach($arr_shortcode as $shorcode => $handler) {
            // shortcode hanler
            add_shortcode($shorcode, $handler);
        }
    }
    
    public function handle_shortcode_teacher_evaluation_form() {
        
        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EDUCATION ) ) {
            wp_safe_redirect('/');
        }
        
        ob_start();
        require_once __DIR__. '/form/teacher-evaluation.php';
        return ob_get_clean();
    }
    
    public function handle_shortcode_grant_education_form() {
        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EDUCATION ) ) {
            wp_safe_redirect('/');
        }
        
        ob_start();
        require_once __DIR__. '/form/grant-education.php';
        return ob_get_clean();
    }
}
new Apollo_Form_Shortcode();