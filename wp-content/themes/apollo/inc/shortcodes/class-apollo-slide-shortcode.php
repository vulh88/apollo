<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class Apollo_SlideShow_Shortcode
{
    public function __construct() {

        add_shortcode('apollo_slideshow', array($this, 'shortcodeSlideShow'));
    }

    /**
     * generate SlideShow from shortcode
     * @param type $atts
     * @author mynv
     */
    function shortcodeSlideShow($atts) {

        $ids = isset($atts['ids']) ? $atts['ids'] : "";
        $height = isset($atts['height']) ? $atts['height'] : '100%';
        $width = isset($atts['width']) ? $atts['width'] : '100%';
        $caption = isset($atts['caption']) ? $atts['caption'] : 'true';
        $ids = explode(",", $ids);
        $strImg = "";
        $style = "<style>.wg-slider .flex-direction-nav a {top: 95px;}</style>";
        $html = '<div class="r-blk-ct">
                <div class="wg-slider">
                  <div class="flexslider">
                    <ul class="slides">';
        foreach ($ids as $id) {
            $imgUrl = wp_get_attachment_url($id);
            $contentCaption = strip_tags(get_post($id)->post_excerpt);
            if (strlen($contentCaption) > 120) {
                $stringCut = substr($contentCaption, 0, 120);
                $contentCaption = substr($stringCut, 0, strrpos($stringCut, ' ')) . ' ...';
            }
            $strCaption = ($caption == 'true') ? '<div class="wg-caption" style="padding: 10px 10px 10px 15px; background: #f9f9f9; border-left: 1px solid #ddd;
        border-right: 1px solid #ddd; border-bottom: 1px solid #ddd; height: 44px; line-height: 18px;">' . $contentCaption . '</div>' : "";
            $strImg = '
        <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" class="">
            <div>
                <div class="wg-pic" style="background: black; width: ' . $width . '; height: ' . $height . '"> <img style="width: auto; margin: 0 auto;" alt="r 1" src="' . $imgUrl . '" draggable="false"></div>' . $strCaption . '
            </div>
        </li>';
            $html .= $strImg;
        }
        $html .= '     </ul>
               <ol class="flex-control-nav flex-control-paging"><li><a class="">1</a></li><li><a class="flex-active">2</a></li><li><a class="">3</a></li></ol><ul class="flex-direction-nav"><li><a href="#" class="flex-prev">Previous</a></li><li><a href="#" class="flex-next">NextSlide</a></li></ul></div>
            </div>
         </div>
         ';
        $html .= $style;
        return $html;
    }

}

new Apollo_SlideShow_Shortcode();