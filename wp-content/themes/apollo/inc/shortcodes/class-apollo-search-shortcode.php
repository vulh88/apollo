<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Apollo_Search_Shortcode {
    
    public function __construct() {
       
        add_shortcode( 'apollo_search', array( $this, 'handle_shortcode' ) );
    }
    
    public function handle_shortcode( $atts ) {
        
        $keyword = $_GET['keyword'];
        
        global $wpdb;
        
        $sql = "
            SELECT p.post_title, p.post_type FROM {$wpdb->posts} p
            WHERE 
            p.post_title LIKE '%".$keyword."%' 
            AND p.post_status = 'publish'    
            AND p.post_type IN ( 'event',  'venue',  'organization' )
            ORDER BY p.post_type
        "; 
        $result = $wpdb->get_results( $sql );
        
        return $this->_render();
    }
    
    private function _render() {
       
        ob_start();
        include dirname( __FILE__ )   . '/search/default-template.php';
        return ob_get_clean();
    }
}

new Apollo_Search_Shortcode();