<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Apollo_Home_Spotlight_Shortcode {
    
    public function __construct() {
       
        add_shortcode( 'apollo_home_spotlight', array( $this, 'apollo_home_spotlight' ) );
    }
    
    public function apollo_home_spotlight(  ) {

        /*@ticket #17821: 0002466: Promo video homepage - New option for the home spotlight*/
        ob_start();
        if(of_get_option(Apollo_DB_Schema::_SPOT_TYPE) == 'customize_html'){
            include APOLLO_SHORTCODE_DIR. '/home-spotlight/custom-spotlight.php';
        }else{
            include APOLLO_SHORTCODE_DIR. '/home-spotlight/slider.php';
        }

        return ob_get_clean();
    }

}

new Apollo_Home_Spotlight_Shortcode();