<input type="hidden" id="cptchar_enable" value="<?php
if(get_current_user_id()){ echo 0; }else { echo 1;}
?>" />
<?php
if(!get_current_user_id()){
    $captcha = new Apollo_Captcha();
    $aria_req = '';
    $req = true;
    $captchaError  = '
            <div class="_comment_err_container">
                <span class="error" id="_comment_captcha_err">
                    <span id="required_err" class="_required hidden">* Captcha is required!</span>
                    <span id="captcha_err" class="_captcha hidden">* Captcha not matched, Please try again</span>
                </span>
            </div>';

    echo '<div class="cm-frm">';
    echo $captchaError;
    echo '<div class="comment-form">';
    echo '<div>';
    echo '<div class="inp-half ">' . '<label class="grant-capt-char custom" for="_captcha">' . __( 'CAPTCHA Code' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
        '<input
                                data-error_holder="#_comment_captcha_err"
                                data-captcha_image="#respond #_captcha_image"
                                data-error_hide="hidden"
                                data-check="required,captcha"
                                data-owner_form="#commentform"
                                data-url_check_captcha="'.$captcha->getUrlCheckCaptcha().'"
                                id="_captcha" name="captcha_code" class="inp inp-txt" type="text" value="" size="30"' . $aria_req . ' /></div>';
    echo '<div class="inp-half custom-cpt-image">' .'<img id="_captcha_image" src="'.$captcha->getImagePath().'" />'. '</div>';
    echo '</div>';
    echo '</div>';
    echo '</div>';
}