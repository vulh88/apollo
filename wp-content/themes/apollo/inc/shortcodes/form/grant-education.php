<?php 

    $submitObj = new Apollo_Submit_Grant_Education();

    $arrRuleFail = array();
    $isSaveSuccess = false;
    $isSubmitFail = false;
    $arrCFData = '';
    $method = $submitObj->method;
    
    $case_action = isset($_POST['case_action']) ? $_POST['case_action'] : '';
  
    if($method === 'post' && $case_action == 'submit_frm') {
        
        $arrData = $submitObj->getDataFromRequest();
        
        $arrCFAndSearchData = $submitObj->getCustomFieldsFromRequest( FALSE );
        $arrCFData = $arrCFAndSearchData[0];
        $isSubmitFail = true;
        if($submitObj->isValidAll(array_merge( $arrData, $arrCFData ))) {
            // save here!
            $id            = $submitObj->save($arrData, $arrCFData);
            $isSaveSuccess = $id;
            $isSubmitFail  = !$id;

            $arrData['id'] = $id;
        }
    }
    
    // Get form data
    $educators = get_posts( array( 'post_type'  => Apollo_DB_Schema::_EDUCATOR_PT, 'order'  => 'ASC', 'orderby' => 'post_title', 'posts_per_page' => -1 ) );
    
    $educator_id = isset($arrData[Apollo_DB_Schema::_APL_EDU_EVAL_EDUCATOR]) ? $arrData[Apollo_DB_Schema::_APL_EDU_EVAL_EDUCATOR] : '';
    
    $fullProg = true;
    if($educator_id && get_post($educator_id)) {
        $eduObj = get_educator($educator_id);
        $programs = $eduObj->get_programs();
        $fullProg = false;
    } else {
        $programs = get_posts( array( 'post_type'  => Apollo_DB_Schema::_PROGRAM_PT, 'order'  => 'ASC', 'orderby' => 'post_title', 'posts_per_page' => -1 ) );
    }
?>

<form id="teacher-frm" method="POST" action="">
    <?php wp_nonce_field($submitObj->nonceAction, $submitObj->nonceName, false); ?>
    <input type="hidden" name="case_action" id="case_action" value="submit_frm" />


    <div class="blog-bkl">
        <?php
        if ( $submitObj->isNonceFailed ) {
            echo '<span class="error">' . __("Cannot save data. Some error occurred !", "apollo") . '</span>';
        } else {
            echo $submitObj->error_form();
        }
        ?>
    </div>
  
    <?php $submitObj->custom_fields_form($arrCFData) ?>

    <?php
    /** @Ticket #14783 Hidden captcha if plugin Spamshield is active*/
    include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    if (!is_plugin_active('wp-spamshield/wp-spamshield.php')) {
        //TriLm add capt-char
        include('grant-capchar.php');
        //end TriLm capt-char
    }
    ?>

    <div class="blog-bkl">
        <?php
        if (!is_plugin_active('wp-spamshield/wp-spamshield.php')) { ?>
            <a href="javascript:apollo_submit_form( 'teacher-frm', 'submit_frm' )" class="btn-form-submit"><?php _e('Submit application', 'apollo') ?></a>
        <?php
        } else { ?>
            <button type="submit" name="submit" class="grant-application-submit btn-form-submit"><?php _e('Submit application', 'apollo') ?></button>
        <?php } ?>


    </div>
</form>