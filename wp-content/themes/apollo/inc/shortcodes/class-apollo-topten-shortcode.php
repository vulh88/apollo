<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Apollo_TopTen_Shortcode {
    
    public function __construct() {
       
        add_shortcode( 'apollo_topten_viewed', array( $this, 'apollo_topten' ) );
    }
    
    public function apollo_topten( $atts ) {
      
        $catData = isset($atts['cats']) && $atts['cats'] ? explode(',', str_replace(' ', '', $atts['cats'])) : '';
        
        ob_start();
        if (!empty($atts['is_widget']) && $atts['is_widget'] == 1) {
            include dirname(__FILE__). '/topten/widget-right-sidebar.php';
        } else {
            include dirname(__FILE__). '/topten/default-template.php';
        }
        
        return ob_get_clean();
    }

}

new Apollo_TopTen_Shortcode();