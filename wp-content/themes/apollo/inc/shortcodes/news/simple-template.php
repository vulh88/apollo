<div class="blog-bkl" style="margin-top: 20px;">
    <nav class="blog-list">
        <?php if (have_posts()):
            $character = of_get_option(APL_News_Config::_NEWS_NUM_OF_CHAR, APL_News_Config::_NEWS_NUM_OF_CHAR_DEFAULT);

            add_filter('excerpt_length', function () {
                return of_get_option(APL_News_Config::_NEWS_NUM_OF_CHAR, APL_News_Config::_NEWS_NUM_OF_CHAR_DEFAULT);
            }, 999);

            while (have_posts()) : the_post();

                add_filter('excerpt_more', function () {
                    return '';
                });
                $post_id = get_the_ID();
                $arrShareInfo = array(
                    'url' => get_permalink(),
                    'summary' => Apollo_App::my_cut_excerpt(get_the_excerpt(), $character),
                    'media' => wp_get_attachment_thumb_url(get_post_thumbnail_id($post_id))
                );

                ?>

                <li>
                    <div class="blog-content no-margin">
                        <div class="blog-pic-detail allow-max-height">
                            <div class="apl-blog-simple-contain-image">
                                <?php
                                /** @Ticket #15735 get large image url */
                                $imgUrl = '';
                                if (has_post_thumbnail()) :
                                    $src = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'large');
                                    if (isset($src[0])) :
                                        $imgUrl = $src[0];
                                    endif;
                                else :
                                    $imgUrl = Apollo_App::defaultBlogImage('large', true);
                                endif;
                                if (!empty($imgUrl)): ?>
                                    <span class="apl-simple-image">
                                    <a href="<?php echo get_permalink($post_id); ?>"><img src="<?php echo $imgUrl ?>"
                                                                                          class="detail"></a>
                                        <?php if (function_exists('the_media_credit_html')) { ?>
                                            <span class="media-credit not-detail"><?php the_media_credit_html(get_post_thumbnail_id($post_id)); ?></span>
                                        <?php } ?>
                                </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="blog-category-list">
                        <?php
                        if (!is_single()) {
                            ?>
                            <div class="cate-blk apl-blog-cat">
                                <?php Apollo_App::apl_get_news_categories($post_id) ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="blog-tt blog-category-list">
                        <div class="blog-name">
                            <?php
                            if (is_single()) {
                                ?>
                                <div class="namedetail-txt blog-date parent-not-underline-hover">
                                    <a><?php echo get_the_title(); ?></a></div>
                                <?php
                            } else {
                                ?>
                                <div class="namedetail-txt blog-date parent-not-underline-hover">
                                    <a href="<?php echo get_permalink($post_id); ?>">
                                        <?php echo get_the_title(); ?>
                                    </a>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <?php if (!get_query_var('author') && get_the_author_meta('ID')) { ?>
                        <div class=" blog-category-list author-blog-listing">
                            <div class="cate-blk parent-not-underline-hover apl-blog-author listing-post-author">
                                <?php echo _e('by ', 'apollo') . the_author_posts_link() ?>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="blog-text center">
                        <?php $more = '&nbsp;<a class="vmore" href="' . get_the_permalink($post_id) . '">' . __('View more', 'apollo') . '</a>'; ?>
                        <p><?php
                            $summary = get_post_meta($post_id, 'post-summary', true);
                            echo Apollo_App::my_cut_excerpt($summary, $character, $more); ?></p>
                    </div>


                </li>
            <?php endwhile; ?>
        <?php else: ?>
            <div><?php _e("Don't have any post !", 'apollo'); ?></div>
        <?php endif; ?>
    </nav>
</div>
<?php
$next_link = get_next_posts_link();
$prev_link = get_previous_posts_link();

if ($next_link || $prev_link):
    ?>
    <div class="blog-bkl">
        <div class="blk-paging">
        <span class="pg-tt"><?php _e('Pages', 'apollo') ?>:
            <?php apollo_blog_paging_nav() ?>
        </span>
        </div>
    </div>

<?php endif; ?>