<?php
$newsOrder = of_get_option(APL_News_Config::_NEWS_CATEGORY_PAGE_ORDER, 'title-asc');
global $apl_current_event_term;
switch ($newsOrder) {
    case ('title-asc'):
        $orderBy = 'post_title';
        $order = 'ASC';
        break;
    case ('title-desc'):
        $orderBy = 'post_title';
        $order = 'DESC';
        break;
    case ('date-asc'):
        $orderBy = 'post_date';
        $order = 'ASC';
        break;
    case ('date-desc'):
        $orderBy = 'post_date';
        $order = 'DESC';
        break;
    case ('order-asc'):
        $orderBy = 'menu_order';
        $order = 'ASC';
        break;
    case ('order-desc'):
        $orderBy = 'menu_order';
        $order = 'DESC';
        break;
    default:
        $orderBy = 'post_title';
        $order = 'ASC';
        break;
}

$args = array(
    'post_type' => array('news'),
    'post_status' => 'publish',
    'showposts' => get_option('posts_per_page'),
    'paged' => get_query_var('paged'),

);

$args['orderby'] = $orderBy;
$args['order'] = $order;

if (!empty($apl_current_event_term)) {
    $arr_tax_query[] = array(
        'taxonomy' => Apollo_DB_Schema::_NEWS_PT . '-type',
        'terms' => array($apl_current_event_term),
    );
    $args['tax_query'] = $arr_tax_query;
}

if ($author = get_query_var('author')) {
    $args['author'] = $author;
}

$tag = get_query_var('tag');
if ($tag != '') {
    $args['tag_slug__in'] = array($tag);
}

query_posts($args);

if (of_get_option(APL_News_Config::_NEWS_LISTING_TYPE, 'default') == 'simple') {
    include APOLLO_SHORTCODE_DIR . '/news/simple-template.php';
} else {
    include APOLLO_SHORTCODE_DIR . '/news/default-template.php';
}