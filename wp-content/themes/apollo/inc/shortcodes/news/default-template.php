<div class="blog-bkl">
    <nav class="blog-list">
        <?php if (have_posts()):
            $character = of_get_option(APL_News_Config::_NEWS_NUM_OF_CHAR, APL_News_Config::_NEWS_NUM_OF_CHAR_DEFAULT);

            add_filter('excerpt_length', function () {
                return of_get_option(APL_News_Config::_NEWS_NUM_OF_CHAR, APL_News_Config::_NEWS_NUM_OF_CHAR_DEFAULT);
            }, 999);

            while (have_posts()) : the_post();

                add_filter('excerpt_more', function () {
                    return '';
                });
                $post_id = get_the_ID();
                $arrShareInfo = array(
                    'url' => get_permalink(),
                    'summary' => Apollo_App::my_cut_excerpt(get_the_excerpt(), $character),
                    'media' => wp_get_attachment_thumb_url(get_post_thumbnail_id($post_id))
                );
                ?>

                <li>
                    <?php
                    include(APOLLO_TEMPLATES_DIR . '/content-single/news/header.php');
                    ?>
                    <div class="blog-content">
                        <div class="blog-pic apl-blog-default">
                            <a class="apl-blog-default-image"
                               href="<?php echo get_permalink($post_id); ?>"><?php echo has_post_thumbnail() ? get_the_post_thumbnail($post_id, 'medium') : Apollo_App::defaultBlogImage('medium', false, 'auto'); ?></a>
                            <?php if (function_exists('the_media_credit_html')) { ?>
                                <span class="media-credit "><?php the_media_credit_html(get_post_thumbnail_id($post_id)); ?></span>
                            <?php } ?>
                        </div>
                        <div class="blog-text">
                            <?php $more = '&nbsp;<a class="vmore" href="' . get_permalink($post_id) . '">' . __('View more', 'apollo') . '</a>'; ?>
                            <p><?php
                                echo Apollo_App::my_cut_excerpt(get_the_excerpt(), $character, $more)
                                ?>
                            </p>
                        </div>
                    </div>

                    <?php
                    $tags = get_the_tag_list('', ', ');

                    if ($tags):
                        ?>
                        <div class="blog-tags">
                            <label><?php _e('Tags', 'apollo') ?>:</label>
                            <?php echo $tags ?>
                        </div>
                    <?php endif; ?>

                </li>
            <?php endwhile; ?>
        <?php else: ?>
            <div><?php _e("Don't have any post !", 'apollo'); ?></div>
        <?php endif; ?>
    </nav>
</div>
<?php
$next_link = get_next_posts_link();
$prev_link = get_previous_posts_link();

if ($next_link || $prev_link):
    ?>
    <div class="blog-bkl">
        <div class="blk-paging">
        <span class="pg-tt"><?php _e('Pages', 'apollo') ?>:
            <?php apollo_blog_paging_nav() ?>
        </span>
        </div>
    </div>

<?php endif; ?>
