<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Apollo_Photo_Slider_ShortCode {
    
    public function __construct() {
       
        add_shortcode( 'apollo_photo_slider', array( $this, 'apollo_photo_slider' ) );
    }
    
    public function apollo_photo_slider( $atts ) {
        if ( !Apollo_App::is_avaiable_module(Apollo_DB_Schema::_PHOTO_SLIDER_PT) ){
            return;
        }
        $postId = isset($atts['photo_slider_id']) && $atts['photo_slider_id'] ? $atts['photo_slider_id'] : 0;
        $post = get_post($postId);
        $post_content = !empty($post) ? Apollo_App::the_content($post->post_content) : '';
        $post_title = !empty($post) ? $post->post_title : '';
        $enableTitleSlider = Apollo_App::apollo_get_meta_data($postId, APL_Photo_Slider_Const::ENABLE_TITLE_SLIDER );
        $enableDescSlider = Apollo_App::apollo_get_meta_data($postId, APL_Photo_Slider_Const::ENABLE_DESC_SLIDER );
        $enableTitleSliderItems = Apollo_App::apollo_get_meta_data($postId, APL_Photo_Slider_Const::ENABLE_TITLE_SLIDER_ITEMS );

        $available_modules = Apollo_App::getModulesForPhotoSlider();
        $sliderItems = array();
        foreach ($available_modules as $module => $index) {
            $mdSliderPhotos = Apollo_App::getDataFromModulePulled($module,$postId);
            if ( !empty($mdSliderPhotos) ){
                $sliderItems = array_merge($sliderItems,$mdSliderPhotos);   
            }
        }

        $resultHTML = Apollo_App::getTemplatePartCustom(dirname(__FILE__). '/photo-slider/template.php',array(
            'ps_content' => $post_content,
            'ps_title' => $post_title,
            'ps_is_enabled_title_slider' => $enableTitleSlider,
            'ps_is_enabled_description_slider' => $enableDescSlider,
            'ps_is_enabled_title_slider_items' => $enableTitleSliderItems,
            'ps_slider_photos' => $sliderItems
        ));

        return $resultHTML;
    }

}

new Apollo_Photo_Slider_ShortCode();