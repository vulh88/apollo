<?php
    $psContent = isset($template_args['ps_content']) ? $template_args['ps_content'] : '';
    $psTitle = isset($template_args['ps_title']) ? $template_args['ps_title'] : '';
    $psIsEnabledTitleSlider = isset($template_args['ps_is_enabled_title_slider']) ? $template_args['ps_is_enabled_title_slider'] : '';
    $psIsEnabledDescriptionSlider = isset($template_args['ps_is_enabled_description_slider']) ? $template_args['ps_is_enabled_description_slider'] : '';
    $psIsEnabledTitleSliderItems = isset($template_args['ps_is_enabled_title_slider_items']) ? $template_args['ps_is_enabled_title_slider_items'] : '';
    $psSliderPhotos = isset($template_args['ps_slider_photos']) ? $template_args['ps_slider_photos'] : array();
?>
<?php if(!empty($psSliderPhotos)) : ?>
    <div class="_js-btm-slider-wrap">
        <div class="slider-description rich-txt">
            <?php if ( $psIsEnabledTitleSlider == 'ON' ) { ?>
                <h2><?php echo $psTitle; ?></h2>
            <?php }
            if ( $psIsEnabledDescriptionSlider == 'ON' ) { ?>
                <div class="custom-block-class">
                    <?php echo $psContent; ?>
                </div>
            <?php } ?>
        </div>
        <div class="bottom-slider">
            <ul class="slides">
                <?php
                foreach ($psSliderPhotos as $sItem ) :
                    $sItem = is_object($sItem) ? get_object_vars($sItem) : $sItem;
                    $sIThumbnail = has_post_thumbnail($sItem['ID']) ? get_the_post_thumbnail( $sItem['ID'], 'medium' ) : Apollo_App::defaultBlogImage('medium', false);
                    $sIPermalink = get_permalink($sItem['ID']);
                    $sITitle = $sItem['post_title'];
                ?>
                    <li>
                        <?php echo $sIThumbnail; ?>
                        <div class="hover-content">
                            <a href="<?php echo $sIPermalink; ?>" class="link"></a>
                            <div class="wrapper-middle">
                                <?php  if ( $psIsEnabledTitleSliderItems == 'ON' ) {?>
                                    <h1 class="content-title"><?php echo $sITitle; ?> </h1>
                                <?php } ?>
                                <h2 class="content-subtitle"></h2>
                            </div>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
<?php endif; ?>
