<?php
/*@ticket #17126 */
$pageId = Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ARTIST_MEMBER_REQUIREMENT);
$artistMemberRequirement = $pageId ? get_post($pageId) : false;

if (!empty($artistMemberRequirement)) { ?>
    <div class = "artist-member-requirement">
        <h4><?php echo $artistMemberRequirement->post_title ?></h4>
        <div>
            <p><?php echo $artistMemberRequirement->post_content ?></p>
        </div>
    </div>

<?php } else { ?>

<div class = "artist-member-requirement">
    <h4><?php echo __('Membership Required', 'apollo'); ?></h4>
    <div>
        <p><?php echo __('You must be a member to access this feature.', 'apollo'); ?></p>
    </div>
</div>

<?php }