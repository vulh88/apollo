<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Apollo_User_Register_Handler {

    const RT_NORMAL = 'normal';
    const RT_EXIST_NETWORK = 'exists_network';

    private $errors = null;
    private $user = null;
    private $type_register = self::RT_NORMAL;

    public function __construct() {
        $this->errors = new WP_Error();
    }

    /**
     * @return boolean
     */
    public function isGet() {
        return $_SERVER['REQUEST_METHOD'] === 'GET';
    }

    /**
     * @return boolean
     */
    public function isPost()
    {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }

    public function hasError() {
        return count($this->errors->get_error_codes());
    }

    /**
     * @param WP_Error $error
     */
    public function setError($errors) {
        $this->errors = $errors;
    }

    public function validate()
    {
        $input = $_POST;

        $require_fields = array( 
            'username'          => __( 'Username', 'apollo' ), 
            'password'          => __( 'Password', 'apollo' ), 
            'email'             => __( 'Email', 'apollo' ), 
            'confirm_password'  => __( 'Confirm Password', 'apollo' ), 
           // 'zipcode'           => __( 'Zip code', 'apollo' ),
            'first_name'        => __( 'First name', 'apollo' ), 
            'last_name'         => __( 'Last name', 'apollo' ),
            'country'           => __( 'Country', 'apollo' )
        );
        /*@ticket #17262: wpdev19 - Suppress the 'zip code' field in the new account registration form*/
        if (of_get_option(Apollo_DB_Schema::_ENABLE_REGISTRATION_ZIP_FIELD, 1) && of_get_option(Apollo_DB_Schema::_ENABLE_REQUIREMENT_REGISTRATION_ZIP_FIELD, 0)){
            $require_fields['zipcode'] = __( 'Zip code', 'apollo' );
        }

        foreach ( $require_fields as $f => $label ) {
            if ( empty( $input[$f] ) ) {
                $this->errors->add( $f, sprintf( __( "* You can't leave %s empty", 'apollo' ), $label ) );
            }
        }

        if ( isset( $input['username'] ) ) {

            $input['username'] = trim($input['username']);
            
            if ( preg_match( '/[^a-z0-9]/', $input['username'] ) ) {
                $this->errors->add( 'username', __( '* Only lowercase letters (a-z) and numbers are allowed', 'apollo' ) );
            }
            
            if ( strlen( $input['username'] ) < 3 ) {
                $this->errors->add( 'username', __( '* Username has at least 4 characters', 'apollo' ) );
            }

            if ( ! validate_username( $input['username'] ) ) {
                $this->errors->add( 'username', __( '* The username you entered is not valid', 'apollo' ) );
            }

            /* Don't allow user name is same name with super admin */
            $arr_super_admin = get_super_admins();

            if(in_array($input['username'], $arr_super_admin)) {
                $this->errors->add('username', __( '* This username already exists', 'apollo' ) );
            }

            if ( username_exists( $input['username'] ) ) {
                $this->errors->add('username', __( '* This username already exists', 'apollo' ) );
            }
        }

        if ( isset( $input['password'] ) ) {
            if ( strlen( $input['password'] ) < 5 ) {
                $this->errors->add( 'password', __( '* Password must be greater than 5 characters', 'apollo' ) );
            }

            if ( $input['password'] != $input['confirm_password'] ) {
                $this->errors->add( 'password', __( '* Password does not match', 'apollo' ) );
            }
        }

        if ( isset( $input['password'] ) ) {
            if ( $input['email'] && ! is_email( $input['email'] ) ) {
                $this->errors->add( 'email', __( '* Email is not valid', 'apollo' ) );
            }

            /* check this email belong to this blog and has exist */
            $user = get_user_by('email', sanitize_email($input['email']));
            if($user) {
                $this->errors->add( 'email', __( '* Email address is already used', 'apollo' ) );
            }
        }

        if ( isset( $input['first_name'] ) && strlen( $input['first_name'] ) > 100 ) {
            $this->errors->add( 'first_name', __( '* First name must be less than 100 characters', 'apollo' ) );
        }

        if ( isset( $input['last_name'] ) && strlen( $input['last_name'] ) > 100 ) {
            $this->errors->add( 'last_name', __( '* Last name must be less than 100 characters', 'apollo' ) );
        }

        if ( isset( $input['zipcode'] ) && strlen( $input['zipcode'] ) > 30 ) {
            $this->errors->add( 'zipcode', __( '* Zip code must be less than 100 characters', 'apollo' ) );
        }
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function renderTemplate() {
        ob_start();
        include  __DIR__. '/../templates/register-template.php';
        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }

    public function redirect($url)
    {
        wp_redirect($url);
    }

}