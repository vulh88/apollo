<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Apollo_User_Login_Handler {

    private $errors = null;
    private $user = null;
    private $request = null;

    public function __construct() {
        $this->setRequest();

    }

    public function setRequest() {
        $this->request = array(
            'username' => isset($_POST['username']) ? $_POST['username'] : '',
        );
    }

    public function getRequest() {
        return $this->request;
    }
    /**
     * @return boolean
     */
    public function isGet() {
        return $_SERVER['REQUEST_METHOD'] === 'GET';
    }

    /**
     * @return boolean
     */
    public function isPost()
    {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }

    public function validate()
    {

        $secure_cookie = '';

        $user = $this->loginByUserNameOrEmail($_POST);
        /* user is accepted - but don't belong this blog */
        if(!$user){
            $this->errors = new WP_Error('invalid_username_password', __('Invalid Username, Email or Password','apollo'));
            return false;
        }

        if ( !empty($_POST['username']) && !force_ssl_admin() ) {
            if ( $user ) {
                if ( get_user_option('use_ssl', $user->ID) ) {
                    $secure_cookie = true;
                    force_ssl_admin(true);
                }
            }
        }

        $credentials['user_password'] = '';
        $credentials['user_login'] = '';

        if ( ! empty($_POST['username']) )
            $credentials['user_login'] = $_POST['username'];
        if($user)
            $credentials['user_login'] = $user->user_login;
        if ( ! empty($_POST['password']) )
            $credentials['user_password'] = $_POST['password'];
        if ( ! empty($_POST['rememberme']) )
            $credentials['remember'] = $_POST['rememberme'];

        if ( !empty($credentials['remember']) )
            $credentials['remember'] = true;
        else
            $credentials['remember'] = false;
        $user = wp_authenticate($credentials['user_login'], $credentials['user_password']);

        if ( is_wp_error($user) ) {
            if ( $user->get_error_codes() == array('empty_username', 'empty_password') ) {
                $user = new WP_Error('empty_username_password', __('Empty user name and password','apollo'));
            }
        }
        else {
            wp_set_auth_cookie($user->ID, $credentials['remember'], $secure_cookie);
        }

        if(is_wp_error($user)) {
            $this->errors = $user;
            return false;
        }

        $this->user = $user;
        return true;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function filter()
    {
        if ( is_user_logged_in() ) {
            wp_safe_redirect( get_home_url($GLOBALS['blog_id']).'/user/dashboard' );
            return;
        }
    }

    public function renderTemplate($render_login_fb = false) {
        ob_start();
        if($render_login_fb){
            include __DIR__ . '/../templates/login-fb-template.php';
        } else {
            include __DIR__ . '/../templates/login-template.php';
        }
        return ob_get_clean();
    }

    public function redirect($url)
    {
        wp_redirect($url);
    }

    public function loginByUserNameOrEmail($data){
        $user_name = sanitize_user($data['username']);
        $user = apl_get_data_by('login', $user_name, $GLOBALS['blog_id']);
        if(!$user){

            $user = apl_get_data_by('email', $user_name, $GLOBALS['blog_id']);

        }
        return $user;
    }


}
