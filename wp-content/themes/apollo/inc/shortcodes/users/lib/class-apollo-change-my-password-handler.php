<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Apollo_Change_My_Password_Handler {

    private $errors = null;
    private $user = null;
    private $request = null;

    public function __construct() {

        $this->setRequest();
    }

    public function setRequest() {

    }

    public function getRequest() {
        return $this->request;
    }
    /**
     * @return boolean
     */
    public function isGet() {
        return $_SERVER['REQUEST_METHOD'] === 'GET';
    }

    /**
     * @return boolean
     */
    public function isPost()
    {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }

    public function validate()
    {

    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function process()
    {
        $this->errors = new WP_Error();

        if(isset($_REQUEST['error']) && $_REQUEST['error'] === 'invalidkey') {
            $this->errors->add( 'invalidkey', __( 'Sorry, that key does not appear to be valid.' ) );
            return;
        }
            

        if(isset($_REQUEST['error']) && $_REQUEST['error'] === 'expiredkey') {
            $this->errors->add( 'expiredkey', __( 'Sorry, that key has expired. Please try again.' ) );
            return;
        }
            

        list( $rp_path ) = explode( '?', wp_unslash( $_SERVER['REQUEST_URI'] ) );
        $rp_cookie = 'wp-resetpass-' . COOKIEHASH;
        if ( isset( $_GET['key'] ) ) {
            $value = sprintf( '%s:%s', wp_unslash( $_GET['login'] ), wp_unslash( $_GET['key'] ) );
            setcookie( $rp_cookie, $value, 0, $rp_path, COOKIE_DOMAIN, is_ssl(), true );
            wp_safe_redirect( remove_query_arg( array( 'key', 'login' ) ) );
            exit;
        }

        if ( isset( $_COOKIE[ $rp_cookie ] ) && 0 < strpos( $_COOKIE[ $rp_cookie ], ':' ) ) {
            list( $rp_login, $rp_key ) = explode( ':', wp_unslash( $_COOKIE[ $rp_cookie ] ), 2 );
            $user = apl_check_password_reset_key( $rp_key, $rp_login );
        } else {
            $user = false;
        }

        if ( ! $user || is_wp_error( $user ) ) {
            setcookie( $rp_cookie, ' ', time() - YEAR_IN_SECONDS, $rp_path, COOKIE_DOMAIN, is_ssl(), true );
            if ( $user && $user->get_error_code() === 'expired_key' )
                wp_redirect( get_permalink(Apollo_App::getChangeMyPassIdPage()).'?error=expiredkey' );
            else
                wp_redirect( get_permalink(Apollo_App::getChangeMyPassIdPage()).'?error=invalidkey' );
            exit;
        }

        if(isset($_POST['password']) && strlen( $_POST['password'] ) < 5) {
            $this->errors->add( 'password_err', __( 'Password must be greater than 5 characters.', 'apollo' ) );
            return;
        }

        if ( isset($_POST['password']) && $_POST['confirm_password'] != $_POST['password'] ) {
            $this->errors->add( 'confirm_password_err', __( 'The passwords do not match.' ) );
            return;
        }
            

        do_action( 'validate_password_reset', $this->errors, $user );

        if ( ( ! $this->errors->get_error_code() ) && isset( $_POST['password'] ) && !empty( $_POST['password'] ) ) {
            reset_password($user, $_POST['password']);
            setcookie( $rp_cookie, ' ', time() - YEAR_IN_SECONDS, $rp_path, COOKIE_DOMAIN, is_ssl(), true );
        }
    }

    public function renderTemplate() {
        ob_start();
        include __DIR__ . '/../templates/change-my-password-template.php';
        return ob_get_clean();
    }

    public function redirect($url)
    {
        wp_redirect($url);
    }

}