<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Apollo_User_Forgot_Password_Hanler {

    private $errors = null;
    private $data = null;
    private $request = null;

    public function __construct(  ) {
        $this->setRequest();
    }

    /**
     *
     */
    public function setRequest() {
        $user_login = isset($_POST['user_login']) ? $_POST['user_login'] : '';

        $this->request = array(
            'user_login' => $user_login,
        );
    }

    public function getRequest() {
        return $this->request;
    }

    /**
     * @return boolean
     */
    public function isGet() {
        return $_SERVER['REQUEST_METHOD'] === 'GET';
    }

    /**
     * @return boolean
     */
    public function isPost()
    {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }

    public function resetPass()
    {
        $this->errors = $this->_retrieve_password();
        if ( !is_wp_error($this->errors) ) {
            // Write the message to seesion to notify one mail has send successfull to their account
            // Continue running and set the message 
        }
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getData()
    {
        return $this->data;
    }

    public function filter()
    {
        if ( is_user_logged_in() ) {
            wp_safe_redirect( get_home_url(get_current_blog_id()));
            return;
        }
    }

    public function renderTemplate() {
        ob_start();
        include __DIR__ . '/../templates/forgot-password-template.php';
        return ob_get_clean();
    }

    public function redirect($url)
    {
        wp_safe_redirect($url);
    }

    /**
     * Copy from core wordpress
     *
     * @uses $wpdb WordPress Database object
     *
     * @return bool|WP_Error True: when finish. WP_Error on error
     */
    private function _retrieve_password() {
        global $wpdb, $wp_hasher;

        $errors = new WP_Error();

        if ( empty( $_POST['user_login'] ) ) {
            $errors->add('empty_username', __('Enter a username or e-mail address.'));
        } else if ( strpos( $_POST['user_login'], '@' ) ) {
            $user_data = get_user_by( 'email', trim( $_POST['user_login'] ) );
            if ( empty( $user_data ) )
                $errors->add('invalid_email', __('There is no user registered with that email address.'));
        } else {
            $login = trim($_POST['user_login']);
            $user_data = get_user_by('login', $login);
        }

        /**
         * Fires before errors are returned from a password reset request.
         */
        do_action( 'lostpassword_post' );

        if ( $errors->get_error_code() )
            return $errors;

        if ( !$user_data ) {
            $errors->add('invalidcombo', __('Invalid username or e-mail.'));
            return $errors;
        }

        // Redefining user_login ensures we return the right case in the email.
        $user_login = $user_data->user_login;
        $user_email = $user_data->user_email;

        /**
         * Fires before a new password is retrieved.
         *
         * @param string $user_login The user login name.
         */
        do_action( 'retreive_password', $user_login );

        /**
         * Fires before a new password is retrieved.
         * @param string $user_login The user login name.
         */
        do_action( 'retrieve_password', $user_login );

        /**
         * Filter whether to allow a password to be reset.
         *
         * @param bool true           Whether to allow the password to be reset. Default true.
         * @param int  $user_data->ID The ID of the user attempting to reset a password.
         */
        $allow = apply_filters( 'allow_password_reset', true, $user_data->ID );

        if ( ! $allow )
            return new WP_Error('no_password_reset', __('Password reset is not allowed for this user'));
        else if ( is_wp_error($allow) )
            return $allow;

        // Generate something random for a password reset key.
        $key = wp_generate_password( 20, false );

        /**
         * Fires when a password reset key is generated.
         *
         * @param string $user_login The username for the user.
         * @param string $key        The generated password reset key.
         */
        do_action( 'retrieve_password_key', $user_login, $key );

        // Now insert the key, hashed, into the DB.
        if ( empty( $wp_hasher ) ) {
            require_once ABSPATH . WPINC . '/class-phpass.php';
            $wp_hasher = new PasswordHash( 8, true );
        }
        $hashed = $wp_hasher->HashPassword( $key );
        $wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user_login ) );

        $reset_link = get_permalink(Apollo_App::getChangeMyPassIdPage())."?key=$key&login=".rawurlencode($user_login);
            
        do_action( 'apollo_email_forgot_password', $user_login, $reset_link, $user_email );    
        //wp_die( __('The e-mail could not be sent.') . "<br />\n" . __('Possible reason: your host may have disabled the mail() function.') );

        return true;
    }
}