<?php if (of_get_option( Apollo_DB_Schema::_FB_ENABLE )) { ?>
<div class="or-line"><span>or</span></div>
<div class="log-fb">
    <div class="log-fb-ct">
        <?php if (of_get_option( Apollo_DB_Schema::_FB_ENABLE )) { ?>
            <div id="fb_error" style="color: #fa2805;margin-bottom: 10px;">
                <span class="error"></span>
            </div>
            <p><?php _e('Sign in using your Facebook account', 'apollo'); ?></p>
            <fb:login-button  size="large" scope="public_profile,email" onlogin="checkLoginState();"><?php _e('SIGN IN WITH FACEBOOK'); ?></fb:login-button>
        <?php } else {
            _e('Facebook login has disabled', 'apollo');
        } ?>
    </div>
</div>
<script>
    function statusChangeCallback(response) {
        var statusElement = document.getElementById('status');
        if (response.status === 'connected') {
            // Logged into your app and Facebook.
            apolo_fb_API();
        } else if (response.status === 'not_authorized') {
            // The person is logged into Facebook, but not your app.
            if (statusElement != null) {
                statusElement.innerHTML = '<?php _e('Please log into this app.','apollo');?>';
            }

            jQuery(window).unblock();
        } else {
            // The person is not logged into Facebook, so we're not sure if
            // they are logged into this app or not.
            if (statusElement != null) {
                statusElement.innerHTML = '<?php _e('Please log into Facebook.','apollo');?>';
            }

            jQuery(window).unblock();
        }
    }
    // This function is called when someone finishes with the Login
    // Button.  See the onlogin handler attached to it in the sample
    // code below.
    function checkLoginState() {
        jQuery(window).block(jQuery.apl.blockUI);
        FB.getLoginStatus(function (response) {
            statusChangeCallback(response);
        });
    }

    function apolo_fb_API() {
        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
        var homepage = '<?php echo get_home_url(); ?>';
        FB.api('/me', {fields: 'id, name, email, first_name, last_name'}, function (response) {
            jQuery.post(
                ajaxurl,
                {
                    'action': 'login_fb',
                    'data': response
                },
                function (data) {
                    if(typeof(data.mess) == "undefined") {
                        window.location = homepage;
                    } else {
                        jQuery("#fb_error span").text(data.mess);
                    }

                    jQuery(window).unblock();
                }
            );

        });
    }
</script>
<?php } ?>