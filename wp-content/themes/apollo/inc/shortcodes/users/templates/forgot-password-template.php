<?php

$post_link = get_the_permalink();
$request = $this->getRequest();
$is_error = false;
$smessage = '';
$amessages = array();
?>

<?php if(is_wp_error($this->errors)): ?>
        <?php

        $is_error = true;
        foreach($this->errors->errors as $err_code => $err ):
            $amessages[] = $err[0];
        endforeach;
        ?>
    <div class="error-wrap">
        <?php foreach($amessages as $msg ): ?>
            <span class="error">* <?php echo $msg ?></span>
        <?php endforeach; ?>
    </div>
    
<?php else: ?>
    <?php 
    // Mean don't have error and is post. mean send email success fully
    if ($this->isPost()): ?>
        <div class="_apollo_success">
            <i class="fa fa-check"></i>
            <?php _e('Email has sent successfully! Please check your e-mail for the confirmation link.', 'apollo') ?>
        </div>
    <?php endif; ?>
<?php endif; ?>


<div class="regis-wrp">
    <form id="login-frm" method="post" action="<?php echo $post_link ?>">
        <div class="el-blk full">
            <input type="text" name="user_login" value="<?php echo isset( $request['user_login'] ) ? $request['user_login'] : '' ?>" placeholder="<?php _e( 'Email address', 'apollo' ) ?> (*)" class="inp inp-txt <?php echo $is_error ? 'inp-error' : '' ?>">
            <div class="show-tip"><?php _e( 'Your email', 'apollo' ) ?></div>
        </div>
        <div class="b-btn">
            <button type="submit" name="submit" class="btn btn-b"><?php _e( 'RESET PASSWORD', 'apollo' ) ?></button>
        </div>
    </form>
</div>