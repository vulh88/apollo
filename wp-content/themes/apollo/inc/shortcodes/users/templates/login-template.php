<?php

$post_link = get_the_permalink(). (isset($_GET['redirect_to']) && $_GET['redirect_to'] ? '?redirect_to='.$_GET['redirect_to'] : '');

$forget_url = get_the_permalink(Apollo_App::getForgetPasswordIdPage());
$amesssges = array();
$request = $this->getRequest();
$is_error_pass = false;
$is_error_user = false;
?>

<?php if(is_wp_error($this->errors)): ?>
    <div id="_login_error">
        <?php
        $is_check_this = false;
        foreach($this->errors->errors as $err_code => $err ):
            if(strrpos($err_code, 'password')) {
                $is_error_pass = true;
            }
            if(strpos($err_code, 'username')) {
                $is_error_user = true;
            }

            if($err_code === 'invalid_username' || $err_code === 'incorrect_password') {
                if($is_check_this) continue;
                $is_check_this = true;
                $err[0] = __('Invalid username or password.', 'apollo');
            }
            ?>
            <span class="error">* <?php echo str_replace("<strong>ERROR</strong>: ", "",$err[0]) ?></span>
         <?php endforeach; ?>
    </div>
<?php endif; ?>



<form id="login-frm" method="POST" action="<?php echo $post_link; ?>">
    <div class="el-blk full">
        <input type="text" placeholder="<?php _e('Username or Email', 'apollo') ?>" name="username" value="<?php echo isset($request['username']) ? $request['username']: '' ?>" class="inp inp-txt <?php echo $is_error_user  ? 'inp-error' : '' ?>">
        <div class="show-tip"><?php _e('Username or Email', 'apollo') ?></div>
    </div>

    <div class="el-blk full">
        <input type="password" placeholder="<?php _e('Password', 'apollo') ?>" name="password" class="inp inp-txt <?php echo $is_error_pass  ? 'inp-error' : '' ?>">
        <div class="show-tip"><?php _e('Password', 'apollo') ?></div>
    </div>

    <div class="b-btn">
        <button type="submit" class="btn btn-b"><?php _e('LOGIN', 'apollo') ?>   </button><a href="<?php echo $forget_url; ?>"><?php _e('Forgot your password?', 'apollo') ?>                                 </a>
    </div>
</form>