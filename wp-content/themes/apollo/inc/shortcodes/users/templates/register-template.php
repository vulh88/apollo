<?php

    $apollo_reg_error = $this->errors;
    $input = Apollo_App::clean_array_data($_POST);

    $arr_codes = array(
        'first_name', 'last_name', 'username', 'password' , 'email', 'confirm_password', 'zipcode', 'country'
    );

    foreach($arr_codes as $code) {
        ${$code.'_errors'} = $apollo_reg_error->get_error_messages( $code );
    }

    // SOme other error.  check later
    $order_error = array();
    $required = of_get_option(Apollo_DB_Schema::_ENABLE_REQUIREMENT_REGISTRATION_ZIP_FIELD) ? '(*)' : '';
    /*@ticket #17262: wpdev19 - Suppress the 'zip code' field in the new account registration form*/
    $enableZipCodeField = of_get_option(Apollo_DB_Schema::_ENABLE_REGISTRATION_ZIP_FIELD, 1);
    /** @Ticket #18744 */
    $countriesList = Apollo_App::getAllCountries();

?>

<?php if ( $this->isGet() || $this->isPost() && $this->hasError() ): ?>
    <div class="regis-wrp">
        <form id="login-frm" method="post" action="">
            <input type="hidden" name="case_action" id="case_action" value="apl_register">
            <div class="el-blk full">
                <input type="text" name="first_name" value="<?php echo isset( $input['first_name'] ) ? esc_attr($input['first_name']) : '' ?>" placeholder="<?php _e( 'First name', 'apollo' ) ?> (*)" class="inp inp-txt <?php echo ! empty( $first_name_errors ) ? 'inp-error' : '' ?>">
                <span <?php echo empty( $first_name_errors ) ? 'style="display: none;"' : ''  ?> class="error"><?php echo ! empty( $first_name_errors ) ? $first_name_errors[0] : '' ?></span>                
                <div class="show-tip"><?php _e('First name', 'apollo') ?></div>
            </div>
            <div class="el-blk full">
                <input type="text" name="last_name" value="<?php echo isset( $input['last_name'] ) ? esc_attr($input['last_name']) : '' ?>" placeholder="<?php _e( 'Last name', 'apollo' ) ?> (*)" class="inp inp-txt <?php echo ! empty( $last_name_errors ) ? 'inp-error' : '' ?>">
                <span <?php echo empty( $last_name_errors ) ? 'style="display: none;"' : ''  ?> class="error"><?php echo ! empty( $last_name_errors ) ? $last_name_errors[0] : '' ?></span>
                <div class="show-tip"><?php _e('Last name', 'apollo') ?></div>
            </div>
            <div class="el-blk full">
                <input name="email" value="<?php echo isset( $input['email'] ) ? $input['email'] : '' ?>" type="text" placeholder="<?php _e( 'Email', 'apollo' ) ?> (*)" class="inp inp-txt <?php echo ! empty( $email_errors ) ? 'inp-error' : '' ?>">
                <span <?php echo empty( $email_errors ) ? 'style="display: none;"' : ''  ?>  class="error"><?php echo ! empty( $email_errors ) ? $email_errors[0] : '' ?></span>
                <div class="show-tip"><?php _e('Email', 'apollo') ?></div>
            </div>
            <?php
                if (!empty($countriesList)) : ?>
                    <div class="el-blk full">
                        <?php echo Apollo_App::renderCountryHtml($countriesList, $input['country'] ? $input['country'] : '');?>
                        <span <?php echo empty( $country_errors ) ? 'style="display: none;"' : ''  ?>  class="error"><?php echo ! empty( $country_errors ) ? $country_errors[0] : '' ?></span>
                        <div class="show-tip"><?php _e('Country (*)', 'apollo') ?></div>
                    </div>
                <?php endif; ?>
            <?php if($enableZipCodeField) { ?>
            <div class="el-blk full">
                <input type="text" name="zipcode" value="<?php echo isset( $input['zipcode'] ) ? $input['zipcode'] : '' ?>" placeholder="<?php _e( 'Zip code '.$required, 'apollo' ) ?>" class="inp inp-txt  <?php echo ! empty( $zipcode_errors    ) ? 'inp-error' : '' ?>">
                <span <?php echo empty( $zipcode_errors ) ? 'style="display: none;"' : ''  ?>  class="error"><?php echo ! empty( $zipcode_errors ) ? $zipcode_errors[0] : '' ?></span>
                <div class="show-tip"><?php _e('Zip code', 'apollo') ?></div>
            </div>
            <?php } ?>
            <div class="el-blk full">
                <input name="username" value="<?php echo isset( $input['username'] ) ? $input['username'] : '' ?>" type="text" placeholder="<?php _e( 'Username', 'apollo' ) ?> (*)" class="inp inp-txt <?php echo ! empty( $username_errors ) ? 'inp-error' : '' ?>">
                <span <?php echo empty( $username_errors ) ? 'style="display: none;"' : ''  ?> class="error"><?php echo ! empty( $username_errors ) ? $username_errors[0] : '' ?></span>
                <div class="show-tip"><?php _e('Username', 'apollo') ?></div>
            </div>
            <div class="el-blk full">
                <input name="password" type="password" placeholder="<?php _e( 'Password', 'apollo' ) ?> (*)" class="inp inp-txt <?php echo ! empty( $password_errors ) ? 'inp-error' : '' ?>">
                <span <?php echo empty( $password_errors ) ? 'style="display: none;"' : ''  ?>  class="error"><?php echo ! empty( $password_errors ) ? $password_errors[0] : '' ?></span>
                <div class="show-tip"><?php _e('Password', 'apollo') ?></div>
            </div>
            <div class="el-blk full">
                <input type="password" name="confirm_password" placeholder="<?php _e( 'Confirm password', 'apollo' ) ?> (*)" class="inp inp-txt <?php echo ! empty( $confirm_password_errors ) || ! empty( $password_errors ) ? 'inp-error' : '' ?>">
                <span <?php echo empty( $confirm_password_errors ) ? 'style="display: none;"' : ''  ?>   class="error"><?php echo ! empty( $confirm_password_errors ) ? $confirm_password_errors[0] : '' ?></span>
                <div class="show-tip"><?php _e('Confirm password', 'apollo') ?></div>
            </div>
            <?php
            /**
             * @ticket #18933: Apply reCaptcha to the FE registration form
             */
            include_once APOLLO_SRC_DIR . '/common/captcha/captcha.php'
            ?>
            <div class="b-btn">   
                <button type="submit" name="apl_register_submit" class="btn btn-b"><?php _e( 'SIGN UP', 'apollo' ) ?></button>
            </div>
            <input id="apollo-register-page" type="hidden" value="1" />
        </form>
    </div>
<?php else: ?>    
    <div id="apollo-registered-page"><?php _e( 'Thanks for your registration, please check your email and', 'apollo' ) ?> 
        <a href="<?php echo get_bloginfo('url') ?>/login"><?php _e( 'click here', 'apollo' ) ?></a> <?php _e( 'to login', 'apollo' ) ?>.</div>
<?php endif; ?>