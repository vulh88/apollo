<?php

$post_link = get_the_permalink();
$request = $this->getRequest();
$is_error = false;
$smessage = '';
$amessages = array();
?>

<?php if(is_wp_error($this->errors) && count($this->errors->errors) >= 1): ?>

    <div class="error-wrap">
        <?php foreach($this->errors->errors as $msg ): ?>
            <span class="error">* <?php echo $msg[0] ?></span>
        <?php endforeach; ?>
    </div>

    <?php else: ?>
    <?php
    // Mean don't have error and is post. mean send email success fully\

    if ($this->isPost()): ?>
        <div class="_apollo_success">
            <i class="fa fa-check"></i>
            <?php echo sprintf(__('Password reset successfully. Please %s again.', 'apollo'), '<a href="'.get_permalink(Apollo_App::getLoginIdPage()).'">Login</a>') ?>
        </div>
    <?php endif; ?>
<?php endif; ?>


<div class="regis-wrp">
    <form id="login-frm" method="post" action="<?php echo $post_link ?>">
        <p class="f-row">
            <input name="password" type="password" placeholder="<?php _e( 'Password', 'apollo' ) ?> (*)" class="inp inp-txt <?php echo  isset($this->errors->errors['password_err'])  ? 'inp-error' : '' ?>">
        </p>
        <p class="f-row">
            <input type="password" name="confirm_password" placeholder="<?php _e( 'Confirm password', 'apollo' ) ?> (*)" class="inp inp-txt <?php echo isset($this->errors->errors['confirm_password_err']) ? 'inp-error' : '' ?>">
        </p>

        <div class="b-btn">   
                <button type="submit" name="submit" class="btn btn-b"><?php _e( 'SUBMIT', 'apollo' ) ?></button>
            </div>  
    </form>
</div>