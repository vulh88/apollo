<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Apollo_Images_Shortcode {
    
    public function __construct() {
        $arr_shortcode = array(
            'apollo_upload_and_drop',
            'apollo_upload_gallery',
        );

        foreach($arr_shortcode as $sn) {
            add_shortcode( $sn, array( $this, $sn ) );
        }
    }

    private function  _getPid($target) {
        $pid = 0;
        switch ($target) {
            case 'event':
                $pid =  $GLOBALS['_apl_pid'];
                break;
            case 'artist':
                $pid = Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_ARTIST_PT);
                wp_reset_query();
                return $pid;
                break;
            case 'educator':
                $pid = Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_EDUCATOR_PT);
                return $pid;
                break;
            case 'program':
                $pid = get_query_var( '_apollo_program_id', 0);
                return $pid  !== 0 ? $pid : (isset($_SESSION['program']['id']) ? $_SESSION['program']['id'] : 0);
            case 'organization':
                $pid = Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_ORGANIZATION_PT);
                return $pid;
            case 'venue':
                $pid = Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_VENUE_PT);
                return $pid;
			case 'classified':
                $pid = get_query_var( '_apollo_classified_id', 0);
                return $pid  !== 0 ? $pid : (isset($_SESSION['classified']['id']) ? $_SESSION['classified']['id'] : 0);
        }

        return $pid;
    }
    
    public function apollo_upload_and_drop( $atts ) {

        if(!isset($atts['target'])) return __('target attribute is required', 'apollo');
        $target = $atts['target'];

        $min_size = Apollo_App::getMinFeatureImgUpload($target);

        extract(shortcode_atts(array(
                    'min_size_w' => $min_size['w'],
                    'min_size_h' => $min_size['h'],
                ), $atts));

        $pid = $this->_getPid($target);

        /**
         * ThienLD: handle ticket #12725
         */
        $isRequiredPrimaryImage = Apollo_App::isEnableRequiredPrimaryImageByModuleName($target) == 1;


        wp_enqueue_script('plupload');
        $suffix = '';
        wp_enqueue_script('jquery.jsrop', APOLLO_FRONTEND_ASSETS_URI."/js/plugins/jquery.Jcrop$suffix.js", array('jquery'), '0.9.12', true);
        wp_enqueue_style('jquery.jsrop', APOLLO_FRONTEND_ASSETS_URI."/css/plugins/jquery.Jcrop$suffix.css", array(), '0.9.12');

        return $this->_render(__DIR__. '/images/template/'. __FUNCTION__ .'.php', array(
                'pid' => $pid,
                'target' => $target,
                'min_size_w' => $min_size_w,
                'min_size_h' => $min_size_h,
                'is_required_primary_image' => $isRequiredPrimaryImage
            ));
    }

    public function _extraPolicyCheck( $target, $pid ) {
        /**
         * @ticket #18931: Can not load the artist profile when the artist profile is empty
         * Only redirect to the Artist Profile when the artist profile is empty and the photo section is sub tab
         */
        $locationPhotoForm = of_get_option(APL_Theme_Option_Site_Config_SubTab::_ARTIST_PHOTO_FORM_LOCATION, 1);
        if(empty($pid) && $target === 'artist' && !Apollo_App::isInputMultiplePostMode() && $locationPhotoForm == 1) {
            Apollo_App::safeRedirect(home_url(). '/user/artist/profile?warning');
        }
    }


    public function apollo_upload_gallery( $atts ) {
        if(!isset($atts['target'])) return __('target attribute is required', 'apollo');
        $target = $atts['target'];

        $min_size = Apollo_App::getMinGalleryImgUpload($target);
        /*@ticket #17020: [CF] 20180803 - [FE Forms] - Add the option to increase the file size upload - Item 1*/
        $maxfilesize = Apollo_App::maxUploadFileSize('b');

        extract(shortcode_atts(array(
                    'min_size_w' => $min_size['w'],
                    'min_size_h' => $min_size['h'],
                    'maxitem' => of_get_option(Apollo_DB_Schema::_MAX_UPLOAD_GALLERY_IMG),
                    'maxw' => 'auto',
                    'maxh' => 'auto',
                    'ext'  => 'jpg,png,jpeg',
                    'maxfilesize' => $maxfilesize .'b'
                ), $atts));

        $pid = $this->_getPid($target);

        $this->_extraPolicyCheck($target, $pid);


        wp_enqueue_script('plupload');
        $suffix = WP_DEBUG ? '' : '.min';
        wp_enqueue_script('jquery.jsrop', APOLLO_FRONTEND_ASSETS_URI."/js/plugins/jquery.Jcrop$suffix.js", array('jquery'), '0.9.12', true);
        wp_enqueue_style('jquery.jsrop', APOLLO_FRONTEND_ASSETS_URI."/css/plugins/jquery.Jcrop$suffix.css", array(), '0.9.12');

        return $this->_render(__DIR__. '/images/template/' .__FUNCTION__. '.php', array(
                'pid' => $pid,
                'target' => $target,
                'min_size_w' => $min_size_w,
                'min_size_h' => $min_size_h,
                'maxitem' => $maxitem,
                'maxw' => $maxw,
                'maxh' => $maxh,
                'ext' => $ext,
                'maxfilesize' => $maxfilesize
            ));
    }
    
    private function _render($template, $arr) {

        extract($arr);

        ob_start();
        include $template;
//        include dirname( __FILE__ )   . '/search/default-template.php';
        return ob_get_clean();
    }
}

new Apollo_Images_Shortcode();