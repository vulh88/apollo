<?php 
    if ( ! empty( $terms ) ) :
        echo '<div id="apollo-taxonomy-container">';
        foreach ( $terms as $term ):
            echo '<div class="apollo-taxonomy-row">';
            echo "<h3><a href='". home_url(). '/'. $term->taxonomy . '/' . $term->slug ."'>". $term->name ."</a></h3>";
            echo "<p>". $term->description ."</p>";
            echo "</div> <br/>";            
        endforeach;
        echo '</div>';
    else:
        _e( "Don't have any post !", 'apollo' );
    endif;
    
?>
<?php 
$total = $this->get_terms( 0, true );

// It mean ajax request    
if ( ! isset( $_REQUEST['num_displayed'] ) ): ?>
<p style="cursor: pointer; <?php echo $this->_per_page >= $total ? ' display: none;' : '' ?> " 
    name="apollo-see-more-taxonomy" id="apollo-see-more-taxonomy" taxonomy="<?php echo $this->_taxonomy ?>"
    total="<?php echo $total; ?>" per-page="<?php echo $this->_per_page ?>"><?php _e( 'See more', 'apollo' ) ?></p>
<?php endif; ?>
