<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Apollo_Network_Footer_Shortcode {
    
    public function __construct() {
       
        add_shortcode( 'apollo_network_footer', array( $this, 'apollo_network_footer' ) );
    }
    
    public function apollo_network_footer( $atts ) {

        ob_start();
        echo Apollo_App::convertContentEditorToHtml(get_site_option( '_apollo_network_footer'));
        return ob_get_clean();
    }

}

new Apollo_Network_Footer_Shortcode();