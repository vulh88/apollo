<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Apollo_Blog_Shortcode {
    
    public function __construct() {

        add_shortcode( 'apollo_blog', array( $this, 'apollo_blog' ) );
    }
    
    public function apollo_blog( $atts ) {

        $catID = isset($atts['cats']) && $atts['cats'] ? explode(',', str_replace(' ', '', $atts['cats'])) : '';

        ob_start();
        include APOLLO_SHORTCODE_DIR . '/blog/template.php';
        return ob_get_clean();
    }

}

new Apollo_Blog_Shortcode();