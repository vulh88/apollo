<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Apollo_Artist_Member_Requirement_Shortcode {

    public function __construct() {
        add_shortcode( 'apollo_artist_member_requirement_short_code', array( $this, 'artist_member_requirement' ) );
    }

    public function artist_member_requirement() {

        ob_start();
        include APOLLO_SHORTCODE_DIR . '/artist/member-requirement.php';
        return ob_get_clean();
    }

}
new Apollo_Artist_Member_Requirement_Shortcode();