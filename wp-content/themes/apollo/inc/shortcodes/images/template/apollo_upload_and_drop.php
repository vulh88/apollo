<div data-target="1" class="gallery-bkl" id="upload_and_drop_container">
    <?php
    // repair data if it exists

    $url = isset($_REQUEST['_url']) ? $_REQUEST['_url'] : '';
    $size = array(
        0, 0
    );
    /** @Ticket 14138 */
    $oname = isset($_REQUEST['_oname']) ? $_REQUEST['_oname'] : '';
    $type = isset($_REQUEST['_type']) ? $_REQUEST['_type'] : '';
    $file = isset($_REQUEST['_file']) ? $_REQUEST['_file'] : '';
    $is_delete = isset($_REQUEST['_is_deleted']) ? $_REQUEST['_is_deleted'] : '';
    $img_id = isset($_REQUEST['img_id']) ? $_REQUEST['img_id'] : '';

    $pid = intval($pid);
    $p = get_post($pid);

    $postStatus = array( 'publish','pending','draft');

    /**
     * @ticket #19007: Modify the Draft labels in event
     */
    if($target == Apollo_DB_Schema::_EVENT_PT ){
        $postStatus[] = 'unconfirmed';
    }

    $pid = $pid && $p && in_array($p->post_status, $postStatus) ? $pid : 0;
    /**
     * ThienLD: handle ticket #12725
    */
    $classRequiredPrimaryImage = isset($is_required_primary_image) && $is_required_primary_image === true ? 'requirement' : '';

    if($pid != 0 && strtolower($_SERVER['REQUEST_METHOD']) === 'get') {
        $img_id = get_post_thumbnail_id( $pid );
        $attachment = wp_get_attachment_image_src($img_id, 'full');
        $url = isset($attachment[0]) ? $attachment[0] : '';
        $size = array(isset($attachment[1]) ? $attachment[1] : 0, isset($attachment[2]) ? $attachment[2] : 0);
        $type = get_post_mime_type( $img_id );
        $file = get_attached_file( $img_id );
        $oname = basename( $file );
    }

    $isAddingNewOrModifyingImg = isset($_REQUEST['_is_new_or_modify']) ? $_REQUEST['_is_new_or_modify'] : 0;
    ?>

    <?php
        /*
        * @Ticket 15203
        * generate select tags
        */

        $tags_html_default = '';
        if(has_filter('apl_pbm_admin_render_select_tags')) {
            $tags_html_default = apply_filters('apl_pbm_admin_render_select_tags', [
                'post_id' => $pid,
                'tags_name' => 'apl_pbm_fe_selected_feature_image_tags',
                'type' => 'photo',
                'media_id' => $img_id,
                'wrap_html' => array(
                    'first' => "<div class='pbm-feature-image-select-tags'>",
                    'last' => '</div>'
                )
            ]);
        }

        echo $tags_html_default;

    ?>
    <div class="upload-img <?php echo !empty($url) ? 'hidden': '' ?>">
        <button data-square="<?php echo of_get_option(Apollo_DB_Schema::_ENABLE_EVENT_PRIMARY_IMG_SQUARE, 0) ?>" id="select-image" class="btn-noW"><?php _e('UPLOAD IMAGE', 'apollo') ?></button>
    </div>
    <div class="crop-img <?php echo empty($url) ? 'hidden': '' ?>">
        <div class="upload-bar">
            <div class="upl-tt"><a>
                    <div class="l-outer">
                        <div class="l-inner"></div>
                    </div>
                    <div class="r-outer">
                        <div class="r-inner"></div>
                    </div><span>Crop Photos</span></a></div>
            <div class="upl-btn">
                <button id="crop-btn" class="btn-noW" ><?php _e('CROP', 'apollo') ?></button>
                <button id="cancel-crop" class="btn-noW"><?php _e('REMOVE', 'apollo') ?></button>
            </div>
        </div>
        <div class="img-crop">
            <img src="<?php echo $url ?>" id="_primary_img"
                                   data-o_width="<?php echo !empty($size) ? $size[0] : ''; ?>"
                                   data-o_height="<?php echo !empty($size) ? $size[1] : ''; ?>">
        </div>
    </div>
</div>

<?php
/**
 * ThienLD: handle ticket #12725
 */
?>
<input type="hidden" class="<?php echo $classRequiredPrimaryImage; ?>" id="check_primary_image" />
<div class="required-image" style="display: none;">
    <p style="color: red"> <?php echo __('A primary image is required.','apollo');?> </p>
</div>


<div class="hidden apl-upload-crop" data-message-error="<?php _e('Image file type is not allowed. Your image must be .jpg, .png or .jpeg', 'apollo');?>">
    <input type="hidden" name="min_size_w" value="<?php echo isset($min_size_w) ? $min_size_w : 0; ?>" />
    <input type="hidden" name="min_size_h" value="<?php echo isset($min_size_h) ? $min_size_h : 0; ?>" />
    <input type="hidden" name="pid" value="<?php echo $pid ?>" />
    <input type="hidden" name="target" value="<?php echo $target  ?>" />
    <input type="hidden" name="nonce" value="<?php echo wp_create_nonce('upload_and_drop') ?>" />

    <input type="hidden" id="_featured_file" name="_file" value="<?php echo $file; ?>" />
    <input type="hidden" id="_featured_oname" name="_oname" value="<?php echo $oname; ?>" />
    <input type="hidden" id="_featured_type" name="_type" value="<?php echo $type; ?>" />
    <input type="hidden" id="_featured_url" name="_url" value="<?php echo $url; ?>" />
    <input type="hidden" id="_featured_is_deleted" name="_is_deleted" value="<?php echo $is_delete; ?>" autocomplete="off"/>
    <input type="hidden" id="_featured_is_new_or_modify" name="_is_new_or_modify" value="<?php echo $isAddingNewOrModifyingImg ?>" />
    <input type="hidden" id="_featured_img_id" name="img_id" value="<?php echo $img_id ?>" />
</div>


<input id="apl-translate" type="hidden"
       data-select-msg="<?php _e('Please select area to drop','apollo');?>"
       data-drop-min-height="<?php _e('Image\'s height must larger than %s px', 'apollo') ?>"
       data-drop-min-width="<?php _e('Image\'s width must larger than %s px', 'apollo') ?>"
       data-upload-error-msg="<?php _e('Your file %s1 does not meet our minimum size requirement. Images must be at least %s2 x %s3 pixels.') ?>"
       data-upload-event-must-square="<?php _e('The primary event image must be a perfect SQUARE (%s2 x %s3 pixels)', 'apollo'); ?>"
       data-image-file ="<?php _e('Image files', 'apollo') ?>"
       data-one-file ="<?php _e('Just upload one. Please!', 'apollo') ?>"
       data-max-file-gallery ="<?php _e('No more than %s file(s)', 'apollo') ?>"
       data-max-upload-gallery ="<?php _e('Maximize image allow uploaded is %s file(s)', 'apollo') ?>"
       data-max-size-gallery="<?php _e('File %s1 too large. Maximum size allow is %s2 MB', 'apollo') ?>"
       data-gallery-min-resolution-msg="<?php _e('Your file %s1 does not meet our minimum size requirement. Images must be at least %s2 x %s3 pixels.','apollo') ?>"
       data-gallery-max-resolution-msg="<?php _e('Your file %s1 does not meet our maximum size requirement. Images must be at least %s2 x %s3 pixels.','apollo') ?>"
    />