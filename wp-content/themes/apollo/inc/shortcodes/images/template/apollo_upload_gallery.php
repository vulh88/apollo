<?php
/*
* @Ticket 15203
* generate select tags
*/

$tags_html_default = '';
if(has_filter('apl_pbm_admin_render_select_tags')) {
    $tags_html_default = apply_filters('apl_pbm_admin_render_select_tags', [
        'post_id' => $pid,
        'tags_name' => 'apl_pbm_fe_selected_gallery_tags[{i}]',
        'type' => 'photo',
        'media_id' => -1,
        'wrap_html' => array(
            'first' => "<div class='pbm-gallery-select-tags'>",
            'last' => '</div>'
        )
    ]);
}

/*@ticket #17126 */
if ($target === Apollo_DB_Schema::_ARTIST_PT){
    $isArtistMember = APL_Artist_Function::checkArtistMember($pid);

    include_once APOLLO_SHORTCODE_DIR. '/images/template/upload_gallery_content.php';

    if (!$isArtistMember) {

        echo  '<style>
                    #apollo_upload_gallery {
                        display: none !important;
                    }
                </style>';

        echo do_shortcode('[apollo_artist_member_requirement_short_code]');
    }
}
else{
    include_once APOLLO_SHORTCODE_DIR. '/images/template/upload_gallery_content.php';
}
?>


