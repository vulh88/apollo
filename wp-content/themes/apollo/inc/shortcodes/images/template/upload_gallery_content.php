<div data-target="2" class="gallery-bkl <?php echo !empty($tags_html_default) ? 'pbm-gallery-bkl' : ''?>" id="apollo_upload_gallery">
    <?php
    // repair data
    $arrGallery = array();
    $pid = intval($pid);

    $p = get_post($pid);
    $postStatus = array( 'publish','pending','draft');

    /**
     * @ticket #18620: Missing the uploaded images when "Return to Edit"
     * Avoid lost image when add new program and click "Return to Edit" step
     */
    if($target == Apollo_DB_Schema::_PROGRAM_PT || $target == Apollo_DB_Schema::_EVENT_PT ){
        $postStatus[] = 'unconfirmed';
    }

    $pid = $pid && $p && in_array($p->post_status, $postStatus) ? $pid : 0;

    /** Remove session */
    $gallerys_is_deleted = (isset($_REQUEST['_gallerys_is_deleted'])) ? $_REQUEST['_gallerys_is_deleted'] : '';
    $rm_imgs = !empty($gallerys_is_deleted) ? explode(',', $gallerys_is_deleted) : array();
    $org_gallery = array();
    $caption_gallerys = array();
    $meta_key = Apollo_App::getMetaKeyGalleryImgByTarget($p->post_type);
    $sids = get_apollo_meta($pid, $meta_key, true);
    $arrId = array();
    if (!empty($sids)) {
        $arrId = explode(",", $sids);
        foreach($arrId as $_ => $id) {
            if (in_array($id, $rm_imgs)) {
                continue;
            }
            $image_array = wp_get_attachment_image_src( $id, 'full' );
            $url = $image_array[0];
            $size = array( $image_array[1], $image_array[2] );
            $image = get_attached_file($id);
            $_meta_data = get_post($id);
            $caption_gallerys[] = $_meta_data ? $_meta_data->post_excerpt : '';

            $org_gallery[] = array(
                'file' => $image,
                'oname' => basename( $image ),
                'type' => get_post_mime_type( $id ),
                'url' => $url,
                'attachment_id' => $_meta_data->ID
            );
        }
    }

    /** @Ticket #14138 */
    if (!empty($_REQUEST['_gallerys']) && !empty($_REQUEST['_gallerys']['file'])) {
        foreach ($_REQUEST['_gallerys']['file'] as $fileKey => $fileValue) {
            if (isset($_REQUEST['_gallerys']['img_id'][$fileKey]) && in_array($_REQUEST['_gallerys']['img_id'][$fileKey], $arrId)) {
                continue;
            }
            $org_gallery[] = array(
                'file' => $fileValue,
                'oname' => (isset($_REQUEST['_gallerys']['oname'][$fileKey])) ? $_REQUEST['_gallerys']['oname'][$fileKey] : '',
                'type' => (isset($_REQUEST['_gallerys']['type'][$fileKey])) ? $_REQUEST['_gallerys']['type'][$fileKey] : '',
                'url' => (isset($_REQUEST['_gallerys']['url'][$fileKey])) ? $_REQUEST['_gallerys']['url'][$fileKey] : '',
                'attachment_id' => (isset($_REQUEST['_gallerys']['img_id'][$fileKey])) ? $_REQUEST['_gallerys']['img_id'][$fileKey] : ''
            );
            $caption_gallerys[] = (isset($_REQUEST['caption_gallerys'][$fileKey])) ? $_REQUEST['caption_gallerys'][$fileKey] : '';
        }
    }

    $arrGallery = $org_gallery;

    $gallerys_is_deleted = (isset($_REQUEST['_gallerys_is_deleted'])) ? $_REQUEST['_gallerys_is_deleted'] : '';

    ?>
    <div class="upload" id="_upload_gallery">
        <div class="upload-bar">
            <div class="upl-tt">
                <div class="upload"><a>
                        <div class="up-img">
                            <div class="triangle-up"></div>
                            <div class="line-up"></div>
                            <div class="mask-up"></div>
                            <div class="bottom-up"></div>
                        </div>
                        <span><?php _e('Upload Files', 'apollo') ?></span>
                    </a></div>
            </div>
            <div class="upl-btn">
                <?php
                if ( ! $arrGallery  ) $arrGallery = array();
                $_arr_val = array_values( $arrGallery );
                $is_cancel_btn = !empty($_arr_val) && $_arr_val[0]['url'] ?>
                <button type="button" id="_upload_gallery_btn" class="btn-noW <?php echo ! $is_cancel_btn ?: 'hidden' ?>" ><?php _e('Upload', 'apollo') ?></button>


            </div>
        </div>

        <div id="_gallery_console"></div>

        <script type="a-tmp" id="_box_template">
            <li id="_gallery_box_{i}">
                <div class="box_info">
                    <img class="overview hidden">
                    <i class="fa fa-arrow-circle-up up-ico"></i>
                    <div class="tt"><span>{filename} ({filesize})</span></div>
                </div>
                <div class="status"><span></span></div>
            </li>
        </script>

        <script type="a-tmp" id="_list_template">
            <li class="_list_">
                <a href="{href}">
                    <img src="{src}" />
                    <div class="del-gallery">
                        <img class="rm_img" data-id="{i}" src="<?php echo APOLLO_FRONTEND_ASSETS_URI ?>/images/ico-del.png" width="20" height="20">
                    </div>
                 </a>
                 <div class="gal-caption">
                        <input type="text" maxlength="100" name="caption_gallerys[]" placeholder="<?php _e('Enter caption here', 'apollo'); ?>" class="gal-cap">
                </div>
                <?php
            /*
            * @Ticket 15203
            * generate select tags
            */
            echo $tags_html_default;
            ?>
            </li>

        </script>

        <ul class="uploading-list" id="_gallery_box">
        </ul>
        <ul class="gallery-list" id="_galler_list">
            <?php if(!empty($arrGallery)):
                $i = 0;
                ?>

                <?php foreach($arrGallery as $key => $info ):
                if ( ! $info['url'] ) continue;
                ?>
                <li class="_list_">
                    <a href="javascript:void(0);">
                        <img src="<?php echo $info['url'] ?>">
                        <div class="del-gallery"><img class="rm_img" data-id="<?php echo $key ?>" data-img-id="<?php echo $info['attachment_id']; ?>" src="<?php echo APOLLO_FRONTEND_ASSETS_URI ?>/images/ico-del.png" width="20" height="20"></div>
                    </a>

                    <div class="gal-caption">
                        <input type="text" maxlength="100" name="caption_gallerys[]" value="<?php echo isset($caption_gallerys[$i]) ? htmlentities($caption_gallerys[$i]) : ''  ?>" placeholder="Enter caption here" class="gal-cap">
                    </div>

                    <?php

                    /*
                    * @Ticket 15203
                    * generate select tags
                    */
                    $tags_html = '';
                    if(has_filter('apl_pbm_admin_render_select_tags')) {
                        $tags_html = apply_filters('apl_pbm_admin_render_select_tags', [
                            'post_id' => $pid,
                            'tags_name' => 'apl_pbm_fe_selected_gallery_tags[' . $info['attachment_id'] . ']',
                            'type' => 'photo',
                            'media_id' => $info['attachment_id'],
                            'wrap_html' => array(
                                'first' => "<div class='pbm-gallery-select-tags'>",
                                'last' => '</div>'
                            )
                        ]);
                    }

                    echo $tags_html;

                    ?>

                </li>
                <?php
                $i++;
            endforeach; ?>
            <?php endif ?>

        </ul>
    </div>
</div>
<?php $p = get_post($pid); ?>
<div class="hidden apl-gallery-upload">
    <input type="hidden" name="min_size_w" value="<?php echo isset($min_size_w) ? $min_size_w : 0; ?>" />
    <input type="hidden" name="min_size_h" value="<?php echo isset($min_size_h) ? $min_size_h : 0; ?>" />
    <input type="hidden" name="maxw" value="<?php echo isset($maxw) ? $maxw : 0; ?>" />
    <input type="hidden" name="maxh" value="<?php echo isset($maxh) ? $maxh : 0; ?>" />
    <input type="hidden" name="maxitem" value="<?php echo isset($maxitem) ? $maxitem : 0; ?>" />

    <input type="hidden" name="ext" value="<?php echo isset($ext) ? $ext : ''; ?>" />

    <input type="hidden" name="pid" value="<?php echo $pid  ?>" />
    <input type="hidden" name="target" value="<?php echo isset($target) ? $target : ''; ?>" />
    <input type="hidden" name="maxfilesize" value="<?php echo strpos($maxfilesize, 'MB') !== false ? (intval($maxfilesize) * 1024 * 1024).'b' : intval($maxfilesize).'b'; ?>" />
    <?php wp_nonce_field(Apollo_Const::_APL_NONCE_ACTION_ARTIST_GALLERY_PAGE, Apollo_Const::_APL_NONCE_NAME, false); ?>

    <div id="_apl_gallerys">
        <?php if(isset($org_gallery)) :
            foreach ($org_gallery as $item) :
                ?>
                <div class="gallery-item" img-id="<?php echo $item['attachment_id']; ?>" data-url="<?php echo $item['url']; ?>">
                    <input type="hidden" class="_gallerys _apl_file" name="_gallerys[file][]" value='<?php echo $item['file']; ?>' />
                    <input type="hidden" class="_gallerys _apl_oname" name="_gallerys[oname][]" value='<?php echo $item['oname']; ?>' />
                    <input type="hidden" class="_gallerys _apl_type" name="_gallerys[type][]" value='<?php echo $item['type']; ?>' />
                    <input type="hidden" class="_gallerys _apl_url" name="_gallerys[url][]" value='<?php echo $item['url']; ?>' />
                    <input type="hidden" class="_gallerys _apl_img_id" name="_gallerys[img_id][]" value='<?php echo $item['attachment_id']; ?>' />
                </div>
            <?php endforeach; endif; ?>
        <input type="hidden" id="_gallerys_is_deleted" name="_gallerys_is_deleted" value="" />
    </div>

</div>