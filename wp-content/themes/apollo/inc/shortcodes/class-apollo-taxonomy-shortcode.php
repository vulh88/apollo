<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Apollo_Taxonomy_Shortcode {
    
    private $_taxonomy = 'organization-type';
    private $_per_page = 10;
    
    public function __construct() {
       
        add_shortcode( 'apollo_taxonomy', array( $this, 'handle_shortcode' ) );
        
        // Register an action for ajax
        add_action( 'wp_ajax_nopriv_apollo_taxonomy_get_more', array( $this, 'apollo_taxonomy_get_more' ));
        add_action('wp_ajax_apollo_taxonomy_get_more', array( $this, 'apollo_taxonomy_get_more' ) );

    }
    
    public function handle_shortcode( $atts ) {
        
        if ( $atts ) {
            if ( isset( $atts['taxonomy'] ) && $atts['taxonomy'] ) {
                $this->_taxonomy = $atts['taxonomy'];
            }
            
            if ( isset( $atts['per_page'] ) && $atts['per_page'] ) {
                $this->_per_page = $atts['per_page'];
            }
        }
        
        $type = str_replace( 'type' , '', $this->_taxonomy);
        $type = str_replace( '-' , '', $type);
        if ( ! Apollo_App::is_avaiable_module( $type ) ) {
            global $wp_query;
            
            _e( "This type not exist or you don't have permission for accessing" );
            
            return;
        }
        
        return $this->_render( $this->get_terms( 0 ) );
    }
    
    private function _render( $terms ) {
       
        ob_start();
        include dirname( __FILE__ )   . '/taxonomies/default-template.php';
        return ob_get_clean();
    }
    
    public function get_terms( $offset = 0, $count = FALSE ) {
       
        if ( ! taxonomy_exists( $this->_taxonomy ) ) {
            return false;
        }
        
        $terms = get_terms( $this->_taxonomy, array(
            'hide_empty' => 0
        ) );
        
        if ( $offset >=0 && ! $count ) {
                $terms = array_slice( $terms, $offset, $this->_per_page );
            }
            
        return $count ? count( $terms ) : $terms;
    }
    
    public function apollo_taxonomy_get_more() {
        
        $num_displayed   = intval( $_REQUEST['num_displayed'] );
        $per_page        = intval( $_REQUEST['per_page'] );
        $total           = intval( $_REQUEST['total'] );
        $this->_taxonomy = $_REQUEST['taxonomy'];
        
        $this->_per_page = intval( $per_page );
        $terms = $this->get_terms( $num_displayed );
        
        echo json_encode( array( 
            'html'          => $this->_render( $terms ),
            'display_btn'   => $total > $num_displayed + $per_page
        ) );
        exit;
    }
    
    
}

new Apollo_Taxonomy_Shortcode();