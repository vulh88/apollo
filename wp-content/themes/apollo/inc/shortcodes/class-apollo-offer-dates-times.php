<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class Apollo_Offers_Dates_Times_Shortcode {

    public function __construct() {

        add_shortcode( 'apollo_offer_dates_times', array( $this, 'apollo_offer_dates_times' ) );
    }

    public function apollo_offer_dates_times() {
        global $post;

        include_once APOLLO_WIDGETS_DIR. '/offer-dates-times/class-apollo-event-offer-dates-times.php';

        $offerObj = new Apollo_Event_Offer_Dates_Time($post->ID);
        return $offerObj->renderDateTime('lft-wrapper');
    }

}

new Apollo_Offers_Dates_Times_Shortcode();