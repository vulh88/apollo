<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Apollo_Get_Content_Shortcode {
    
    public function __construct() {
       
        add_shortcode( 'apollo_content', array( $this, 'apollo_content' ) );
    }
    
    public function apollo_content( $atts ) {
        global $wpdb;
        $type   = isset( $atts['type'] ) && !empty( $atts['type'] ) ?  $atts['type'] : 'page';
        $id     = $atts['id'];
        $result = NULL;
        switch ($type) {
            case 'page':
            default:
                $sql = "
                SELECT p.post_content FROM {$wpdb->posts} p
                WHERE
                p.post_type = '$type'
                AND p.ID = '$id'
                AND p.post_status = 'publish'
                ";
                $result = $wpdb->get_var($sql);
                if ($result) {
                    $result = do_shortcode($result);
                }
        }
        return $result;
    }

}

new Apollo_Get_Content_Shortcode();