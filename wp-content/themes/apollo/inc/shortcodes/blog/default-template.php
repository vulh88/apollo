<div class="blog-bkl">
    <nav class="blog-list">
        <?php if ( have_posts() ):
            $character = of_get_option(Apollo_DB_Schema::_BLOG_NUM_OF_CHAR,Apollo_Display_Config::_BLOG_NUM_OF_CHAR) ;

            add_filter( 'excerpt_length', function() {
                return of_get_option(Apollo_DB_Schema::_BLOG_NUM_OF_CHAR,Apollo_Display_Config::_BLOG_NUM_OF_CHAR);
            }, 999 );

            while (have_posts() ) : the_post();

                add_filter( 'excerpt_more', function () {return '';});
                $post_id = get_the_ID();
                $arrShareInfo = array(
                    'url' => get_permalink(),
                    'summary' => Apollo_App::my_cut_excerpt(get_the_excerpt(),$character),
                    'media' => wp_get_attachment_thumb_url( get_post_thumbnail_id( $post_id ) )
                );

            /*@ticket #16768: 0002309: BLOG - Sponsored Content of the blog*/
		    $sponsoredContent = get_post_meta($post_id,'post-sponsored-content',true);
            /*@ticket #177560:  BLOG - Sponsored Content - New Page checkbox and URL field textbox*/
		    $blogPost = get_apl_blog($post_id);
            $urlField = $blogPost->renderURLBlog();

            /*@ticket #18343 0002504: Arts Education Customizations - Add Arts Ed masthead logo and footers to Blog listing and detail pages.*/
            $imageDefault = of_get_option( APL_Theme_Option_Site_Config_SubTab::_BLOG_ENABLE_DEFAULT_IMAGE, 1) ? Apollo_App::defaultBlogImage('medium', false, 'auto') : '';
                ?>

            <li>

                <?php
                    $enableGoogleIcon = true;
                    $enableThumbUp = true;
                    include(APOLLO_TEMPLATES_DIR.'/content-single/post/header.php');
                ?>
                <div class="blog-content">
                    <div class="blog-pic apl-blog-default">
                        <?php
                        /*@ticket 17547 */
                        if ($sponsoredContent && $sponsoredContent === 'on'){
                            echo "<div  class='sponsored-content-text'>" .  __('SPONSORED CONTENT', 'apollo') . "</div>";
                        }
                        ?>
                        <a class="apl-blog-default-image" <?php echo $urlField ?>><?php echo has_post_thumbnail() ? get_the_post_thumbnail( $post_id, 'medium' ) : $imageDefault; ?></a>
                        <?php if (  function_exists( 'the_media_credit_html' ) ) { ?>
                            <span class="media-credit "><?php the_media_credit_html( get_post_thumbnail_id( $post_id ) );?></span>
                        <?php } ?>
                    </div>
                    <div class="blog-text">
                        <?php $more =  '&nbsp;<a class="vmore" '. $urlField . '>' . __('View more', 'apollo') . '</a>';?>
                        <p><?php
                            $summary = get_post_meta($post_id,'post-summary',true);
                            echo Apollo_App::my_cut_excerpt($summary,$character,$more); ?></p>
                    </div>
                </div>

                <?php
                    $tags = get_the_tag_list( '', ', ' );

                    if ( $tags ):
                ?>
                <div class="blog-tags">
                    <label><?php _e( 'Tags', 'apollo' ) ?>:</label>
                    <?php echo $tags ?>
                </div>
                <?php endif; ?>

            </li>
        <?php endwhile; ?>
        <?php else: ?>
            <div><?php _e( "Don't have any post !", 'apollo' ); ?></div>
        <?php endif; ?>
    </nav>
</div>
<?php
    $next_link = get_next_posts_link();
    $prev_link = get_previous_posts_link();

    if ( $next_link || $prev_link ):
?>
<div class="blog-bkl">
    <div class="blk-paging">
        <span class="pg-tt"><?php _e( 'Pages', 'apollo' ) ?>:
        <?php apollo_blog_paging_nav() ?>
        </span>
    </div>
</div>

<?php endif; ?>
