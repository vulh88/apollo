<?php

$postsOrder = of_get_option(Apollo_DB_Schema::_BLOG_CATEGORY_PAGE_ORDER,'title-asc');
switch($postsOrder){
    case ('title-asc'):
        $orderBy = 'post_title';
        $order = 'ASC';
        break;
    case ('title-desc'):
        $orderBy = 'post_title';
        $order = 'DESC';
        break;
    case ('date-asc'):
        $orderBy = 'post_date';
        $order = 'ASC';
        break;
    case ('date-desc'):
        $orderBy = 'post_date';
        $order = 'DESC';
        break;
    case ('order-asc'):
        $orderBy = 'menu_order';
        $order = 'ASC';
        break;
    case ('order-desc'):
        $orderBy = 'menu_order';
        $order = 'DESC';
        break;
    default:
        $orderBy = 'post_title';
        $order = 'ASC';
        break;
}

$args = array(
    'post_type' => array('post'),
    'post_status' => 'publish',
    'showposts' => get_option('posts_per_page'),
    'paged' => get_query_var('paged'),

);

if ( !get_query_var( '_apollo_page_blog' )){
    $args['orderby'] = $orderBy;
    $args['order']    = $order;
}

if (empty($catID)) {
    $catID = get_query_var( 'cat' );
}

if ($catID) {
    $args['category__in'] = $catID;
}

if( $author = get_query_var('author')){
    $args['author'] = $author;
}

$tag =  get_query_var('tag');
if($tag != ''){
    $args['tag_slug__in'] = array($tag);
}

query_posts($args);

if (of_get_option(Apollo_DB_Schema::_BLOG_LISTING_TYPE, 'default') == 'simple') {
    include APOLLO_SHORTCODE_DIR . '/blog/simple-template.php';
} else {
    include APOLLO_SHORTCODE_DIR . '/blog/default-template.php';
}