<?php
require_once APOLLO_INCLUDES_DIR. '/admin/tools/cache/Inc/File.php';
$cacheFileClass = aplc_instance('APLC_Inc_Files_HomeRightTopTenWidget');
$html = $cacheFileClass->get();

if (!$html):
    ob_flush();
    ob_start();
    include_once APOLLO_TEMPLATES_DIR . '/events/list-events.php';
    $currentBlogID = get_current_blog_id();
    $topTenEvents = List_Event_Adapter::getTopTenEventsByBlogID($currentBlogID, 10);

    if (!empty($topTenEvents)) {
        ?>
        <div class="r-blk-ct topten">
            <div class="topten-list">
                <?php
                $length_summary = Apollo_Display_Config::EVENT_OVERVIEW_NUMBER_CHAR;
                foreach($topTenEvents as $data):
                    $_a_event = get_event($data);

                    /// STARTDATE-ENDDATE
                    $_arr_show_date = $_a_event->getShowTypeDateTime('picture');
                    $length_summary = Apollo_Display_Config::EVENT_OVERVIEW_NUMBER_CHAR;

                    ?>

                    <div class="topten-item">
                        <div class="pic">
                            <a href="<?php echo $_a_event->get_permalink() ?>"><?php echo $_a_event->get_image('thumbnail', array(), array(
                                    'aw' => true,
                                    'ah' => true,
                                )) ?></a>
                        </div>
                        <div class="info">
                            <h2><a href="<?php echo $_a_event->get_permalink() ?>"> <?php echo $_a_event->get_title(true) ?></a></h2>
                            <p class="meta auth"><?php echo $_a_event->renderOrgVenueHtml() ?></p>
                            <p><?php $_a_event->render_sch_date(); ?></p>
                        </div>
                    </div>

                <?php endforeach; ?>
            </div>
        </div>
        <?php
    } else {
        $adapter->the_notice_empty();
    }
    // Save cache
    $html = ob_get_contents();
    ob_end_clean();
    $cacheFileClass->save($html);
endif;
echo $html;
