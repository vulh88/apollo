<?php
include_once APOLLO_TEMPLATES_DIR . '/events/list-events.php';
$currentBlogID = get_current_blog_id();
$topTenEvents = List_Event_Adapter::getTopTenEventsByBlogID($currentBlogID, 10);
if (!empty($topTenEvents)) {
?>
    <section class="list-category">

        <?php
            $length_summary = Apollo_Display_Config::EVENT_OVERVIEW_NUMBER_CHAR;
            foreach($topTenEvents as $eventID) :
            $_a_event = get_event($eventID);
            $arrShareInfo = $_a_event->getShareInfo();

            /// STARTDATE-ENDDATE
            $_arr_show_date = $_a_event->getShowTypeDateTime('picture');

        ?>
        <article class="category-itm">
            <?php  $_a_event->render_circle_date(); ?>

            <div class="pic"><a href="<?php echo $_a_event->get_permalink() ?>"><?php echo $_a_event->get_image('thumbnail', array(), array(
                            'aw' => true,
                            'ah' => true,
                        )) ?></a></div>
            <div class="category-t">
                <h2 class="category-ttl"> <a href="<?php echo $_a_event->get_permalink() ?>"> <?php echo $_a_event->get_title(true) ?></a></h2>
                <p class="meta auth">
                    <?php echo $_a_event->renderOrgVenueHtml() ?>
                </p>
                <?php if( of_get_option(Apollo_DB_Schema::_ENABLE_THUMBS_UP,1)) : ?>
                <div class="cat-rating-box rating-action">
                    <div class="like" id="_event_rating_<?php echo $_a_event->id . '_'. 'event' ?>" data-ride="ap-event_rating" data-i="<?php echo $_a_event->id ?>" data-t="event"> <a href="javascript:void(0);"><span class="_count">&nbsp;</span></a></div>
                </div>
                <?php endif ?>
                <p class="desc">
                    <?php
                    $summary =$_a_event->get_summary($length_summary);
                    echo $summary['text']; ?><?php if($_a_event->is_have_more_summary($length_summary)) echo '...'; ?>
                </p>

                <?php if($_a_event->is_have_more_summary($length_summary)): ?>
                    <a href="<?php echo $_a_event->get_permalink() ?>" class="vmore"><?php _e("View more", 'apollo') ?></a>
                <?php endif; ?>

                <div class="b-btn category">

                    <?php
                        echo $_a_event->renderTickerBtn(array(
                                'class' => 'btn btn-category'
                            ));
                        echo $_a_event->renderCheckDCBtn(array(
                            'class' => 'btn btn-category'
                        ));
                        echo $_a_event->renderBookmarkBtn(array(
                                'class' => 'btn btn-category btn-bm'
                            ));
                    ?>
                </div>
            </div>
        </article>
        <?php endforeach; ?>
    </section>
    <?php
} else {
    $adapter->the_notice_empty();
}


