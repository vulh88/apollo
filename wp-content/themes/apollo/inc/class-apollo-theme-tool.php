<?php

/* 
 * Process theme tool features of event category module
 */

class Apollo_Theme_Tool {
    
    private $themeTool = false;
    private $filterBy = false;
    private $filterData = false;
    private $themeToolCatSlugs = array();
    
    public function __construct( $termId = false ) {
        if ($termId) {
            $this->setThemeTool($termId);
            $this->setThemeToolDataFilter();
        }
    }
    
    public function getAllThemeTools($getSlug = false) {
        $aplQuery = new Apl_Query(Apollo_Tables::_APL_THEME_TOOL);
        $results = $aplQuery->get_where("leave_as_normal <> 1");
        $output = array();
        if ($results) {
            foreach($results as $result) {
                $output[] = $result->cat_id;
                if ($getSlug) {
                    $term = get_term_by('term_id', $result->cat_id, 'event-type');
                    $this->themeToolCatSlugs[] = $term->slug;
                }
            }
        }
        // root cause for using array_merge at here
        // it's not a bug of php function array_unique
        // This is bug of wordpress function's when using wp_localize_script to pass array to javascript.
        // Specify as :
        // array_unique(array(1,2,4,5)) is parsed to javascript through wp_localize_script is normal -> it will be ARRAY data type.
        // array_unique(array(1,1,44,44,22,22,65,5,65,102,20,30,20,2,4,5)) is parsed to javascript through wp_localize_script had big problem -> it will be OBJECT data type
        // -> the bug is we couldn't use js function themeTools.indexOf with themeTools data type is Object.
        $output = array_merge(array(),array_unique($output));
        return $output;
    }
    
    public function isThemeTool() {
        return $this->themeTool && $this->themeTool->leave_as_normal == 0;
    }

    public function getThemeToolData() {
        return $this->themeTool;
    }
    /**
     * Check in case individual events
     * @access public
     * @return boolean
     * 
     */
    public function isForceEvents() {
        return $this->themeTool && $this->themeTool->entity_type == 'IND';
    }
    
    public function setThemeToolDataFilter() {
        if ( ! $this->themeTool ) return false;
        
        $filter = false;
        $data = false;
        if ( $this->themeTool->entity_type == 'ALL' ) {
            $filter = $this->themeTool->location_type == 0 ? 'zip' : 'city';
            $data = maybe_unserialize($this->themeTool->location_data);
        }
        
        if ( $this->themeTool->entity_type == 'IND' ) {
            $filter = 'event';
            $data = maybe_unserialize($this->themeTool->events);
        }
        $this->setFilterBy($filter);
        $this->setFilterData($data);
    }
    
    public function setFilterBy($filterBy) {
        $this->filterBy = $filterBy;
    }
    
    public function setFilterData($filterData) {
        $this->filterData = $filterData;
    }
    
    public function setThemeTool($termId) {
        $aplQuery = new Apl_Query(Apollo_Tables::_APL_THEME_TOOL);
        $this->themeTool = $aplQuery->get_row("cat_id=".$termId."");
    }

    public static function getVenueIDsByFilterData($locationType = '', $locationData = array()){
        if(empty($locationType) || empty($locationData)) return array();
        global $wpdb;
        $venueMetaTbl = $wpdb->{Apollo_Tables::_APL_VENUE_META};
        $sql = "SELECT vm.apollo_venue_id as venue_id
                FROM $venueMetaTbl vm
                WHERE vm.meta_key = '".$locationType."'
                AND vm.meta_value IN ('".implode("','", $locationData). "')";
        $result = $wpdb->get_results($sql,ARRAY_A);
        if(empty($result)) return array();
        $finalResults = array();
        foreach($result as $item){
            $finalResults[] = $item['venue_id'];
        }
        return $finalResults;
    }

    private function _getEventsHaveVenueByZipOrCity($venueType){
        global $wpdb;
        $eventMetaTbl = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};
        $venueIDs = self::getVenueIDsByFilterData($venueType, $this->filterData);
        $venueIDs = empty($venueIDs) ? array(-1) : $venueIDs;
        $sql = "SELECT mt.apollo_event_id as event_id
                FROM $eventMetaTbl mt
                AND mt.meta_key = '".Apollo_DB_Schema::_APOLLO_EVENT_VENUE."'
                AND mt.meta_value IN ( ".implode(',',$venueIDs)." )";
        $results = $wpdb->get_results($sql,ARRAY_A);
        if(empty($results)) return array();
        $finalResults = array();
        foreach($results as $item){
            $finalResults[] = $item['event_id'];
        }
        return $finalResults;
    }

    public function getSelectedEventIDsByThemeTool($args=array()){
        $default = array('outputType' => 'array',
            'selectType' => 'event',
            'eventAlias' => 'p'
        );
        $latestArgs = array_replace($default, $args);
        $outputType = $latestArgs['outputType'];
        $selectType = $latestArgs['selectType'];
        $eventAlias = $latestArgs['eventAlias'];
        if($outputType == 'array'){
            $finalResult = array();
            switch($this->filterBy){
                case 'event':
                    $finalResult = $this->filterData;
                    break;
                case 'zip':
                    $finalResult = $this->_getEventsHaveVenueByZipOrCity(Apollo_DB_Schema::_VENUE_ZIP);
                    break;
                case 'city':
                    $finalResult = $this->_getEventsHaveVenueByZipOrCity(Apollo_DB_Schema::_VENUE_CITY);
                    break;
                default:
                    break;
            }
        } else {
            $finalResult = "";
            if($selectType == 'event' && $this->filterBy == 'event'){
                $finalResult .= " AND ".$eventAlias.".ID IN (".implode(',',$this->filterData).") ";
            } elseif($selectType == 'venue' && in_array($this->filterBy, array('zip','city'))){
                $finalResult .= $this->getVenueJoin($eventAlias);
            }
        }
        return $finalResult;
    }

    /**
     * Get join location
     * @access public 
     * @return string
     * 
     */
    public function getVenueJoin($mainAlias = '') {

        if (!$this->isThemeTool()) return '';

        global $wpdb;
        $join = '';
        
        if (! $mainAlias) {
            $mainAlias = $wpdb->posts;
        }
        
        $venueMetaTbl = $wpdb->{Apollo_Tables::_APL_VENUE_META};
        $locationData = $this->filterData ? "('".implode("','", $this->filterData). "')" : "('-1')";
        $eventMetaTbl = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};
        //Todo: improve query, don't use In (select statement)
        //Filter by zip if has theme tool exists
        if ( $this->filterBy == 'zip' ) {
            
            $join = " INNER JOIN $eventMetaTbl mt ON mt.apollo_event_id = $mainAlias.ID "
                . " AND mt.meta_key = '".Apollo_DB_Schema::_APOLLO_EVENT_VENUE."'"
                . " AND mt.meta_value IN ( SELECT vm.apollo_venue_id FROM $venueMetaTbl vm WHERE vm.meta_key = '".Apollo_DB_Schema::_VENUE_ZIP."' AND vm.meta_value IN $locationData ) ";
        }

        // Filter by city
        if ( $this->filterBy == 'city' ) {
           
            $join = " INNER JOIN $eventMetaTbl mt ON mt.apollo_event_id = $mainAlias.ID "
                . " AND mt.meta_key = '".Apollo_DB_Schema::_APOLLO_EVENT_VENUE."'"
                . " AND mt.meta_value IN ( SELECT vm.apollo_venue_id FROM $venueMetaTbl vm WHERE vm.meta_key = '".Apollo_DB_Schema::_VENUE_CITY."' AND vm.meta_value IN $locationData ) ";
        }
        
        return $join;
    }

    public function getEventIDsInVenue($mainAlias = '') {

        if (! $this->isThemeTool()) {
            return '';
        }

        global $wpdb;
        $where = '';

        if (! $mainAlias) {
            $mainAlias = $wpdb->posts;
        }

        $venueMetaTbl = $wpdb->{Apollo_Tables::_APL_VENUE_META};
        $eventMetaTbl = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};
        $locationData = $this->filterData ? "('".implode("','", $this->filterData). "')" : "('-1')";

        // Query venues
        if ($this->filterBy) {
            $sqlVenue = '';
            if ( $this->filterBy == 'zip' ) {
                $sqlVenue = "SELECT vm.apollo_venue_id FROM $venueMetaTbl vm WHERE vm.meta_key = '".Apollo_DB_Schema::_VENUE_ZIP."' AND vm.meta_value IN $locationData";
            } else if ($this->filterBy == 'city') {
                $sqlVenue = "SELECT vm.apollo_venue_id FROM $venueMetaTbl vm WHERE vm.meta_key = '".Apollo_DB_Schema::_VENUE_CITY."' AND vm.meta_value IN $locationData";
            }


            $venueResult = $wpdb->get_results($sqlVenue, 'ARRAY_N');
            $arrVenueIDs = array();
            if ($venueResult) {
                foreach($venueResult as $vn) {
                    $arrVenueIDs[] = $vn[0];
                }
            }
        }


        //Filter by zip if has theme tool exists
        if (isset($arrVenueIDs) && $arrVenueIDs) {
            $where = " $mainAlias.ID IN(
                SELECT apollo_event_id FROM $eventMetaTbl mtEventVenue WHERE mtEventVenue.meta_key = '".Apollo_DB_Schema::_APOLLO_EVENT_VENUE."' AND mtEventVenue.meta_value IN (
                    ".implode(",", $arrVenueIDs)."
                )
            ) ";
        }
       
        return $where ? $where. ' AND ' : '';
    }
    
    /**
     * Get Where in of events in case individual 
     * @access public
     * @return string
     */
    public function getWhereInEventIds($mainAlias = '', $logicAfter = '') {

        $where = ' 1=1 ';
        if ($this->isThemeTool()) {
            global $wpdb;

            if (! $mainAlias) {
                $mainAlias = $wpdb->posts;
            }

            if ( $this->filterBy == 'event' ) {
                $inEvents = $this->filterData ? "('".implode("','", $this->filterData). "')" : '';
                $where =  $inEvents ? " $mainAlias.ID IN $inEvents " : '1=2';
            }
        }

        return sprintf(" $where %s", $logicAfter);
    }
    
    public function getOrder() {
        return $this->themeTool ? $this->themeTool->tt_order : false;
    }
    
    public function getId() {
        return $this->themeTool ? $this->themeTool->id : false;
    }
    
    public function getCatSlugs() {
        return $this->themeToolCatSlugs;
    }
    
    public function getStorageData($themeTools) {
        
        $events = array();
        $cities = array();
        $zips   = array();
        
        if ($themeTools) {
            foreach ( $themeTools as $tt ) {
                if ( $tt->entity_type == 'IND' ) {
                    $events = @array_merge( $events, unserialize( $tt->events ) );
                } else if ( $tt->location_type == 1 ) {
                    $cities = @array_merge( $cities, unserialize( $tt->location_data ) );
                } else if ( $tt->location_type == 0 ) {
                    $zips = @array_merge( $zips, unserialize( $tt->location_data ) );
                }
            }
        }
       
        return array(
            'events'    => $events,
            'cities'    => $cities,
            'zips'      => $zips,
        );
        
    }
}