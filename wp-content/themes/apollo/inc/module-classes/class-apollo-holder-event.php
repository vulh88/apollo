<?php
/**
 * Abstract Event Class
 *
 * The Apollo event class handles individual event data.
 *
 * @class 		Apollo_Event
 * @package		inc/Abstracts
 * @category	Abstract Class
 * @author 		elitext
 */
class Apollo_Holder_Event {

	/** @var int The event (post) ID. */
	private  $id;

	/** @var object The actual post object. */
	private $post = false;

	/** @var string The event's type (simple, variable etc). */
	private  $event_type = null;

    private $ameta = array();

    private $permalink = '';


	/**
	 * __construct function.
	 *
	 * @access public
	 * @param mixed $event
	 */
	public function __construct( $event ) {
        
		if ( $event instanceof stdClass) {
            $this->id   = absint( isset($event->id) ? $event->id : $event->ID ) ;
            $this->post = $event;
		} elseif(intval($event) > 0) {
			$this->id   = absint( $event );
			$this->post = apollo_get_post( $this->id );
		}
        
	}

    public function isEmpty() {
        return $this->post === false;
    }

	public function isPublish() {
		return $this->post->post_status == 'publish';
	}

	/**
	 * __isset function.
	 *
	 * @access public
	 * @param mixed $key
	 * @return bool
	 */
	public function __isset( $key ) {
        //@todo Optimize later
        return metadata_exists( 'apollo_event', $this->id, $key );
	}

	/**
	 * __get function.
	 *
	 * @access public
	 * @param mixed $key
	 * @return mixed
	 */
	public function __get( $key ) {
        if(isset($this->ameta[$key]))
            return $this->ameta[$key];

        return $this->ameta[$key] = get_apollo_meta( $this->id, $key, true );
	}

	/**
	 * Get the event's post data.
	 *
	 * @access public
	 * @return object
	 */
	public function get_post_data() {
		return $this->post;
	}


	/**
	 * Wrapper for get_permalink
	 * @return string
	 */
	public function get_permalink() {
        if($this->permalink !== '')
                return $this->permalink;
		return $this->permalink =  get_permalink( $this->id );
	}

		/**
	 * Get the title of the post.
	 *
	 * @access public
	 * @return string
	 */
	public function get_title() {
		return $this->post->post_title;
	}

	/**
	 * Get the parent of the post.
	 *
	 * @access public
	 * @return int
	 */
	public function get_parent() {
		return $this->post->post_parent;
	}

    public function getPostTypeLinkHtml() {
        return '<a href="'. home_url() . '/' . $this->post->post_type. '/' . $this->post->post_name . '">' . $this->post->post_title . '</a>' ;
    }

    public function getTitle() {
        return $this->post->post_title;
    }
    

}
