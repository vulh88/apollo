<?php
/**
 * Abstract Event Class
 *
 * The Apollo event class handles individual event data.
 *
 * @class 		Apollo_Event
 * @package		inc/Abstracts
 * @category	Abstract Class
 * @author 		elinext
 */
class Apollo_Event extends Apollo_ABS_Module {

    /** @var array The event's type (simple, variable etc). */
    public $event_type = null;

    private $vote = null;

    private $_org_event = null;
    private $_venu_event = null;

    private $_arr_address = array();
    private $_tax;
    public $total_datetime = 0;

    protected $metaMainData = '';
    
    protected $eventOrgs = false;
    
    protected $periodTimes = false;

    protected $isPrivate = false;

    /**
     * @return boolean
     */
    public function isIsPrivate()
    {
        return $this->isPrivate;
    }

    /**
     * @param boolean $isPrivate
     */
    public function setIsPrivate($isPrivate)
    {
        $this->isPrivate = $isPrivate;
    }



    /**
    * __construct function.
    *
    * @access public
    * @param mixed $event
    */
    public function __construct( $event, $type ) {
        $this->iconField = Apollo_DB_Schema::_APL_EVENT_ICONS;
        parent::__construct( $event, $type );
        $this->_tax = $this->type. '-type';
        $this->setMetaMainData();
        $this->isPrivateEvent();


        /**
         * @ticket #19307: TF - 01032019 - Optimize code: Load options for event detail page
         */
        require_once APOLLO_INCLUDES_DIR . '/src/event/inc/theme-option/options.php';
        $this->themeOption = ThemeOptionEventDetailPage::getInstance();
    }

	/**
	 * __get function.
	 *
	 * @access public
	 * @param mixed $key
	 * @return mixed
	 */
	public function __get( $key ) {

		// Get values or default if not set
		if ( in_array( $key, array( Apollo_DB_Schema::_APOLLO_HOME_EVENT_FEATURE ) ) ) {
			$value = ( $value = get_apollo_meta( $this->id, $key, true ) ) ? $value : 'no';
		} else {
			$value = get_apollo_meta( $this->id, $key, true );
		}
        
		return $value;
	}

    /**
    * Returns whether or not the event is featured.
    *
    * @access public
    * @return bool
    */
    public function is_home_featured() {

        return $this->{Apollo_DB_Schema::_APOLLO_HOME_EVENT_FEATURE} === 'yes';
    }
    
    public function getMetaInMainData( $key ) {
        return isset($this->metaMainData[$key]) ? $this->metaMainData[$key] : '';
    }
    
    /**
     * Keep main meta data
     * 
     * @access public
     * @return none
     */
    public function setMetaMainData() {
        $this->metaMainData = maybe_unserialize($this->get_meta_data(Apollo_DB_Schema::_APOLLO_EVENT_DATA));
    }
    
    /**
     * Returns whether or not the event is featured in category
     * 
     * @access public
     * @return bool
     */
    public function is_featured_category( $cat_slug ) {
        
        $term = get_term_by( 'slug' , $cat_slug, 'event-type' );
        
        if ( ! $term ) return false;
        
        $post_term = Apollo_App::has_post_term( $this->id, $term->term_id );
       
        return $post_term && $post_term->{Apollo_DB_Schema::_IS_CATEGORY_FEATURE} === 'yes';
    }
    
    /**
     * Return whether or not the event is tab feature in home page
     * 
     * @access public
     * @return boolean
     */
    public function is_tab_feature() {
        global $wpdb;
        $sql = " SELECT ".Apollo_DB_Schema::_TAB_LOCATION." FROM ". $wpdb->{Apollo_Tables::_POST_TERM} ."
            WHERE post_id = $this->id
            AND ".Apollo_DB_Schema::_TAB_LOCATION." IS NOT NULL AND ".Apollo_DB_Schema::_TAB_LOCATION." <> '' ";
        return $wpdb->get_row( $sql );
    }
    
    /**
	 * Returns whether or not the event is spotlight.
	 *
	 * @access public
	 * @return bool
	 */
	public function is_category_spotlight( $cat_slug ) {
        $term = get_term_by( 'slug' , $cat_slug, 'event-type' );
        
        if ( ! $term ) return false;
        
        $post_term = Apollo_App::has_post_term( $this->id, $term->term_id );
        
        return $post_term && $post_term->{Apollo_DB_Schema::_IS_CATEGORY_SPOTLIGHT} === 'yes';
	}
   
    /**
	 * Returns whether or not the event is home spotlight.
	 *
	 * @access public
	 * @return bool
	 */
	public function is_home_spotlight() {
        
        global $wpdb;
        $sql = " SELECT ".Apollo_DB_Schema::_TAB_LOCATION." FROM ". $wpdb->{Apollo_Tables::_POST_TERM} ."
            WHERE post_id = $this->id
            AND ".Apollo_DB_Schema::_IS_HOME_SPOTLIGHT." = 'yes'
            GROUP BY post_id ";
        return $wpdb->get_row( $sql );
	}

	/**
	 * Returns the event categories.
	 *
	 * @access public
	 * @param string $sep (default: ')
	 * @param mixed '
	 * @param string $before (default: '')
	 * @param string $after (default: '')
	 * @return string
	 */
	public function the_categories( $sep = ', ', $before = '', $after = '' ) {
		return get_the_term_list( $this->id, 'event-type', $before, $sep, $after );
	}
    
     /**
	 * Get the primary term link
	 *
	 * @access public
	 * @return int
	 */
    public function get_primary_term_link( $start_el = '', $end_el = '' ) {
        
        global $apollo_modules;
        $primary_id = $this->get_meta_data( Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID );
        $term = get_term( $primary_id, $this->_tax );
        $event_m = $apollo_modules[$this->type];
     
        if ( ! $term || empty($term->slug) || empty($term->name)) return false;

        return $start_el.'<a href="'.  home_url() . '/' .$event_m['taxonomy_slug']. '/'.$term->slug.' ">'.$term->name.'</a>'. $end_el;
    }

	/**
	 * @expected array
	 */
	public function getShowTypeDateTime($format= 'picture', $delimiter = '/') {

		switch($format) {
			case 'picture':
                $start_date = trim($this->{Apollo_DB_Schema::_APOLLO_EVENT_START_DATE});
                $end_date   = trim($this->{Apollo_DB_Schema::_APOLLO_EVENT_END_DATE});
				$show_date_type = 'none';

				$only_date =  $start_date;

				if((!empty($start_date) && $start_date === $end_date) || (!empty($start_date) && empty($end_date)) || (empty($start_date) && !empty($end_date)) ) {
					$show_date_type = 'one';
					$start_date = !empty($start_date) ? $start_date : $end_date;
				}
				elseif(!empty($start_date) && ($start_date !== $end_date) ) {
					$show_date_type = 'two';
				}
                
				return array(
						'type' => $show_date_type,
						'start_date' => $start_date,
						'end_date' => $end_date,
                        'unix_start' => strtotime( $start_date ),
                        'unix_end' => strtotime( $end_date ),
					);
            case 'short-m-y':

                $start_date = trim($this->{Apollo_DB_Schema::_APOLLO_EVENT_START_DATE});
                $end_date   = trim($this->{Apollo_DB_Schema::_APOLLO_EVENT_END_DATE});
                $show_date_type = 'none';

                $only_date =  $start_date;
                $unix_start = null;
                $unix_end =  null;

                if((!empty($start_date) && $start_date === $end_date)
                    || (!empty($start_date) && empty($end_date))
                    || (empty($start_date) && !empty($end_date))

                ) {
                    $show_date_type = 'one';
                    $start_date = !empty($start_date) ? $start_date : $end_date;
                    $unix_start = strtotime($start_date);
                }
                elseif(!empty($start_date) && ($start_date !== $end_date) ) {

                    /*same month and year still be one*/
                    $unix_start = strtotime($start_date);
                    $unix_end = strtotime($end_date);
                    if(date("m", $unix_start) === date('m', $unix_end)) {
                        $show_date_type = 'one';
                    }
                    else {
                        $show_date_type = 'two';
                    }


                }

                return array(
                    'type' => $show_date_type,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'unix_start' => $unix_start,
                    'unix_end' => $unix_end,
                );
                
            case 'full_string':
                $start_date = trim($this->{Apollo_DB_Schema::_APOLLO_EVENT_START_DATE});
                $end_date   = trim($this->{Apollo_DB_Schema::_APOLLO_EVENT_END_DATE});
                return array(
                        'start_date' => str_replace( '-' , '/', $start_date ),
                    'end_date'   => str_replace( '-' , '/', $end_date )
                );
                
		}

        return array(); 
	}
       
    /**
     * @expect array
     */
    public function getParentCategory()
    {

    }

    /**
     * Get string address
     * @param array $fields Location field
     * @param boolean $is_get_full_address
     * @return string Address
     */ 
    public function get_location_data( $fields , $is_get_full_address = false) {
        
        if ( empty( $fields ) ) {
            return array();
        }    
        
        $venue_id = $this->get_meta_data( Apollo_DB_Schema::_APOLLO_EVENT_VENUE );
        $zip = '';
        /** @Ticket #16483 - Get Neighborhood name */
        $neighborhood = '';
        $city = '';
        if ( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EVENT_PT ) && $venue_id ) {     
            $venue = get_venue( get_post( $venue_id ) ); 
            if ( !$venue ) return false;
        } else {
            $venue = @unserialize( $this->get_meta_data( Apollo_DB_Schema::_APL_EVENT_TMP_VENUE ) );
        } 
       
        $arr_address = array();
        $_is_register_venue = $venue instanceof stdClass || $venue instanceof WP_Post || $venue instanceof Apollo_ABS_Module;

        $is_have_addr = false;
        foreach($fields as $_field) {
            
            if ( $_is_register_venue ) {
                if ( $_field == Apollo_DB_Schema::_VENUE_NAME ) {
                    $arr_address[] = $venue->get_title();
                    continue;
                }

                /* If have address */
                if($is_have_addr && $_field === Apollo_DB_Schema::_VENUE_ADDRESS2) {
                    continue;
                }

                /* meta data */
                if ( $val = $venue->get_meta_data( $_field ,Apollo_DB_Schema::_APL_VENUE_ADDRESS ) ) {
                    if ($_field === Apollo_DB_Schema::_VENUE_ZIP){
                        $zip = $val;
                        continue;
                    }
                    if ($_field == Apollo_DB_Schema::_VENUE_NEIGHBORHOOD) {
                        $neighborhood = $val;
                        continue;
                    }
                    if ($_field == Apollo_DB_Schema::_VENUE_CITY) {
                        $city = $val;
                    }
                    $arr_address[] = $val; // _venue_address2

                    if($_field == Apollo_DB_Schema::_VENUE_ADDRESS1 && $is_get_full_address == false ) {
                        $is_have_addr = true;
                    }
                }

                continue;
            } 
            
            if ( isset( $venue[$_field] ) && $venue[$_field] ) {
                if ($_field == Apollo_DB_Schema::_VENUE_ZIP){
                    $zip = $venue[$_field];
                    continue;
                }
                if ($_field == Apollo_DB_Schema::_VENUE_NEIGHBORHOOD) {
                    $neighborhood = $venue[$_field];
                    continue;
                }
                if ($_field == Apollo_DB_Schema::_VENUE_CITY) {
                    $city = $venue[$_field];
                }
                $arr_address[] = $venue[$_field];
            }
        }
        if (!empty($neighborhood)) {
            /** @Ticket #16483 - Neighborhood hyperlink */
            $neighborhoodName = APL_Lib_Territory_Neighborhood::getNameById($neighborhood);
            $searchLink =  home_url().'/event/?event_city='.$city.'&neighborhood='.$neighborhoodName.'&event_neighborhood='.$neighborhood;
            $neighborhoodHyperLink = '<a href="'.$searchLink.'" rel="nofollow">'.$neighborhoodName.'</a>';
            $neighborhood = '</br>' . sprintf(__('(Neighborhood: %s)', 'apollo'), $neighborhoodHyperLink);
        }
        return implode( ", ", $arr_address ).' '.$zip. $neighborhood;
    }
    
    /**
     * Get Venue name
     *@access public
     * @return string
     */ 
    public function get_location_name() {
         $venue_id = $this->get_meta_data( Apollo_DB_Schema::_APOLLO_EVENT_VENUE );
        if ( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EVENT_PT ) && $venue_id ) {     
            $venue = get_venue( get_post( $venue_id ) ); 
        } else {
            $venue = unserialize( $this->get_meta_data( Apollo_DB_Schema::_APL_EVENT_TMP_VENUE ) );
        } 
       
        if ( $venue instanceof stdClass || $venue instanceof WP_Post || $venue instanceof Apollo_ABS_Module ) {
            return $venue->get_title();
        } else {
            return isset( $venue[Apollo_DB_Schema::_VENUE_NAME] ) ? $venue[Apollo_DB_Schema::_VENUE_NAME] : '';
        }
        return false;    
    }
    
    /**
     *
     */
    public function getStrAddress($is_get_venue_name = false) {
        if(isset($this->_arr_address[$is_get_venue_name])) {
            return $this->_arr_address[$is_get_venue_name];
        }

        $arr = array();
        if($is_get_venue_name) {
            $arr = array_merge($arr, array(Apollo_DB_Schema::_VENUE_NAME));
        }
        $arr = array_merge($arr, array(
                Apollo_DB_Schema::_VENUE_ADDRESS1,
                Apollo_DB_Schema::_VENUE_ADDRESS2,
                Apollo_DB_Schema::_VENUE_CITY,
                Apollo_DB_Schema::_VENUE_STATE,
                Apollo_DB_Schema::_VENUE_ZIP,
            ));

        return $this->_arr_address[$is_get_venue_name] = $this->get_location_data($arr);
    }

    public function getFullAddress( $is_get_full_address = true ){
        $arr = array();
        $arr = array_merge($arr, array(
            Apollo_DB_Schema::_VENUE_ADDRESS1,
            Apollo_DB_Schema::_VENUE_ADDRESS2,
            Apollo_DB_Schema::_VENUE_CITY,
            Apollo_DB_Schema::_VENUE_STATE,
            Apollo_DB_Schema::_VENUE_ZIP,
            Apollo_DB_Schema::_VENUE_NEIGHBORHOOD,
        ));

        return  $this->get_location_data($arr, $is_get_full_address);
    }

    /**
     * @param array $arr_params
     * @return string
     */
    public function renderTickerBtn($arr_params = array()){

        // get event's organization
        $registeredOrg   = $this->get_register_org(); // = NULL if organization is TMP ORG
        $registeredOrgID = empty($registeredOrg) ? '' : $registeredOrg->org_id;

        $default = array(
            'class'         => 'btn btn-cat-detail',
            'data-ride'     => "ap-logclick",
            'data-action'   => 'apollo_log_click_activity',
            'data-activity' => Apollo_Activity_System::CLICK_BUY_TICKET,
            'data-item_id'  => $this->id,
            'data-start'    => $this->{Apollo_DB_Schema::_APOLLO_EVENT_START_DATE},
            'data-end'      => $this->{Apollo_DB_Schema::_APOLLO_EVENT_END_DATE},
            'data-org-id'   => $registeredOrgID,
        );

        $arr_params = wp_parse_args($arr_params, $default);
     
        $sattr = '';
        foreach($arr_params as $attn => $value) {
            $sattr .= $attn . ' = "'. $value.'"';
        }

        $ticket_url     = $this->getMetaInMainData( Apollo_DB_Schema::_ADMISSION_TICKET_URL );
        if(!empty($ticket_url)) {
            ob_start();
            $buyTicketText = of_get_option(Apollo_DB_Schema::_APL_EVENT_BUY_TICKET_TEXT, __('BUY TICKETS', 'apollo'));
            ?>
            <a href="<?php echo $ticket_url ?>" <?php echo $sattr ?> target="_blank" ><?php echo $buyTicketText ?></a>
            <?php

            return ob_get_clean();
        }
        else {
            return '';
        }
    }

    /**
     * @param array $arr_params
     * @return string
     */
    public function renderCheckDCBtn($arr_params = array()){

        // get event's organization
        $registeredOrg   = $this->get_register_org(); // = NULL if organization is TMP ORG
        $registeredOrgID = empty($registeredOrg) ? '' : $registeredOrg->org_id;

        $default = array(
            'class'         => 'btn btn-cat-detail',
            'data-ride'     => "ap-logclick",
            'data-action'   => 'apollo_log_click_activity',
            'data-activity' => Apollo_Activity_System::CLICK_DISCOUNT,
            'data-item_id'  => $this->id,
            'data-start'    => $this->{Apollo_DB_Schema::_APOLLO_EVENT_START_DATE},
            'data-end'      => $this->{Apollo_DB_Schema::_APOLLO_EVENT_END_DATE},
            'data-org-id'   => $registeredOrgID,
        );

        $arr_params = wp_parse_args($arr_params, $default);
        $sattr = '';
        foreach($arr_params as $attn => $value) {
            $sattr .= $attn . ' = "'. $value.'"';
        }

        $discount_url     = $this->getMetaInMainData( Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL );
        if(!empty($discount_url)) {
            ob_start();
            $checkD = of_get_option(Apollo_DB_Schema::_APL_EVENT_CHECK_DISCOUNTS_TEXT, __('CHECK DISCOUNTS', 'apollo'));
            ?>
            <a href="<?php echo $discount_url ?>" target="_blank" <?php echo $sattr ?>><?php echo $checkD ?></a>
            <?php

            return ob_get_clean();
        }
        else {
            return '';
        }
    }

    public function getVote()
    {
        if($this->vote !== null) {
            return $this->vote;
        }

        return $this->vote = 1;
    }

    public function getOrganizationEvent() {
        if(!is_null($this->_org_event)) {
            return $this->_org_event;
        }

        if(Apollo_App::is_avaiable_module(Apollo_DB_Schema::_ORGANIZATION_PT ) && $this->{Apollo_DB_Schema::_APOLLO_EVENT_ORGANIZATION}!== "") {
            return $this->_org_event = new Apollo_Holder_Event($this->{Apollo_DB_Schema::_APOLLO_EVENT_ORGANIZATION}); /* On event just belong to one organization */
        }

        return $this->_org_event = new Apollo_Holder_Event(NULL);
    }

    public function getVenueEvent() {
        if(!is_null($this->_venu_event)) {
            return $this->_venu_event;
        }

        if(Apollo_App::is_avaiable_module(Apollo_DB_Schema::_VENUE_PT) && $this->{Apollo_DB_Schema::_APOLLO_EVENT_VENUE} !== "") {
            return $this->_venu_event = new Apollo_Holder_Event($this->{Apollo_DB_Schema::_APOLLO_EVENT_VENUE}); /* On event just belong to one venue */
        }
        
        return $this->_venu_event = new Apollo_Holder_Event(NULL);

    }


    public function renderOrgVenueHtml($showVenueAddress = false, $onlyOrg = false, $showFullAdress = false) {

        $og_link = $this->event_org_links();

        if($onlyOrg) {
            $text = __('Presented by %s ', 'apollo');
            return $og_link != 'Unknown' ? sprintf($text, $og_link) : sprintf($text, __(' Unknown', 'apollo'));
        }

        $text = __('Presented by %s <span class="venue-event"> at %s</span>%s', 'apollo');
        $textWithoutOrg = __(' <span class="venue-event"> at %s</span>%s', 'apollo');
        $vn_link  = __( 'Unknown', 'apollo' );

        $_venu_event = $this->getVenueEvent();

        $venueMeta = array();
        if( ! $_venu_event->isEmpty() && $_venu_event->isPublish()) {
            $vn_link = $_venu_event->getPostTypeLinkHtml();
            $venueMeta = get_apollo_meta($_venu_event->get_post_data()->ID, Apollo_DB_Schema::_APL_VENUE_ADDRESS);
            $venueMeta = !empty($venueMeta) ? maybe_unserialize($venueMeta[0]) : $venueMeta;
        } else if ( $tmp =  @unserialize( $this->get_meta_data(Apollo_DB_Schema::_APL_EVENT_TMP_VENUE  ) ) ) {

            if ( $tmp && isset( $tmp[Apollo_DB_Schema::_VENUE_NAME] ) && $tmp[Apollo_DB_Schema::_VENUE_NAME] ) {
                $vn_link = $tmp[Apollo_DB_Schema::_VENUE_NAME];
            }
            $venueMeta = $tmp;
        }
        /** @Ticket #15483 */
        $address = '';
        if ($showVenueAddress)
        {
            $city = empty($venueMeta[Apollo_DB_Schema::_VENUE_CITY]) ? '' : $venueMeta[Apollo_DB_Schema::_VENUE_CITY];
            $state = empty($venueMeta[Apollo_DB_Schema::_VENUE_STATE]) ? '' : $venueMeta[Apollo_DB_Schema::_VENUE_STATE];

            if ($city || $state) {
                $address = ', ' . $city . ' ' . $state;
                $address = '<span class="event-venue-address">'. $address .'</span>';
            }
        }

        if($showFullAdress){
            $address = $this->getFullAddress();
            $address = trim($address) ? ", " . $address : " ";
        }

        return $og_link != 'Unknown' ? sprintf($text, $og_link, $vn_link, $address) : sprintf($textWithoutOrg, $vn_link, $address);
    }

    public function getNumberStar() {
        return rand(0,5);
    }

    public function renderStarBox()
    {
        $star = $this->getNumberStar();
        $ai = array_fill(0,5, '<i class="fa fa-star"></i>');

        for($i = 0; $i< $star; $i++) {
            $ai[$i] = '<i class="fa fa-star active"></i>';
        }

        return implode('', $ai);
    }

    //include pending
    public function all_event_orgs() {
        
        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ORGANIZATION_PT ) ) return false;
        
        global $wpdb;
        $sql = "
            SELECT p.*, e_org.".Apollo_DB_Schema::_APL_E_ORG_IS_MAIN." FROM {$wpdb->posts} p
            INNER JOIN {$wpdb->{Apollo_Tables::_APL_EVENT_ORG}} e_org ON e_org.". Apollo_DB_Schema::_APL_E_ORG_ID ."= p.ID    
            WHERE p.post_type = '".Apollo_DB_Schema::_ORGANIZATION_PT."'
            AND p.post_status IN ('publish','pending')
            AND e_org.post_id = {$this->id}
            GROUP BY e_org.".Apollo_DB_Schema::_APL_E_ORG_ID." 
            ORDER BY e_org.".Apollo_DB_Schema::_APL_E_ORG_ORDERING."     
        ";
        return $wpdb->get_results( $sql );    
    }

    /**
     * Get event ORGs. Please use the eventORGs function
     *
     * @deprecated Use eventOrgs()
     *
     * @return collection ORGs
     */
    public function event_orgs() {

        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ORGANIZATION_PT ) ) return false;
        
        
        if ($this->eventOrgs) {
            return $this->eventOrgs;
        }
      
        global $wpdb;
        $sql = "
            SELECT p.*, e_org.".Apollo_DB_Schema::_APL_E_ORG_IS_MAIN." FROM {$wpdb->posts} p
            INNER JOIN {$wpdb->{Apollo_Tables::_APL_EVENT_ORG}} e_org ON e_org.". Apollo_DB_Schema::_APL_E_ORG_ID ."= p.ID
            WHERE p.post_type = '".Apollo_DB_Schema::_ORGANIZATION_PT."'
            AND p.post_status = 'publish'
            AND e_org.post_id = {$this->id}
            GROUP BY e_org.".Apollo_DB_Schema::_APL_E_ORG_ID."
            ORDER BY e_org.".Apollo_DB_Schema::_APL_E_ORG_ORDERING."
        ";
        $this->eventOrgs = $wpdb->get_results( $sql );    
        return $this->eventOrgs;
    }

    /**
     * Get event ORGs
     *
     * @return collection ORGs
     */
    public function eventOrgs() {

        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ORGANIZATION_PT ) ) return false;


        if ($this->eventOrgs) {
            return $this->eventOrgs;
        }

        global $wpdb;
        $sql = "
            SELECT p.ID, p.post_title, p.post_type, p.post_content, p.post_name, e_org.".Apollo_DB_Schema::_APL_E_ORG_IS_MAIN." FROM {$wpdb->posts} p
            INNER JOIN {$wpdb->{Apollo_Tables::_APL_EVENT_ORG}} e_org ON e_org.". Apollo_DB_Schema::_APL_E_ORG_ID ."= p.ID
            WHERE p.post_type = '".Apollo_DB_Schema::_ORGANIZATION_PT."'
            AND p.post_status = 'publish'
            AND e_org.post_id = {$this->id}
            GROUP BY e_org.".Apollo_DB_Schema::_APL_E_ORG_ID."
            ORDER BY e_org.".Apollo_DB_Schema::_APL_E_ORG_ORDERING."
        ";
        $this->eventOrgs = $wpdb->get_results( $sql );

        return $this->eventOrgs;
    }

    /**
     * Get all org name
     * @Ticket #13437
     * @return string
     */
    public function getEventOrgName() {
        $result = '';
        $eventOrgs = $this->eventOrgs();
        if ($eventOrgs) {
            $orgs = array();
            foreach ($eventOrgs as $e_org) {
                array_push($orgs, $e_org->post_title);
            }
            $result = implode(', ', $orgs);
        }
        if (empty($result)) {
            $tmp_org = $this->get_meta_data( Apollo_DB_Schema::_APL_EVENT_TMP_ORG );
            $result = $tmp_org ? $tmp_org : '';
        }

        return $result;
    }

    public function event_venue_link() {
        $registeredVenueID = $this->get_meta_data(Apollo_DB_Schema::_APOLLO_EVENT_VENUE);
        if(!empty($registeredVenueID)){
            $registeredVenue = get_posts(array(
                'post_type'         => Apollo_DB_Schema::_VENUE_PT,
                'post_status'       => array('publish','pending'),
                'p'          => intval($registeredVenueID)
            ));
        } else {
            $registeredVenue = '';
        }
        $venueName = '';
        $venueLink = '';
        if(!empty($registeredVenue)){
            $venueName = $registeredVenue[0]->post_title;
            $venueLink = get_edit_post_link($registeredVenue[0]->ID);
        } else {
            $tmpVenue = $this->get_meta_data( Apollo_DB_Schema::_APL_EVENT_TMP_VENUE );
            $tmpVenue = !empty($tmpVenue) ? maybe_unserialize($tmpVenue) : '';
            if(!empty($tmpVenue)){
                $venueName = isset($tmpVenue['_venue_name']) ? $tmpVenue['_venue_name'] : '';
            }
        }

        $venueName = !empty($venueName) ? $venueName : __( 'Unknown', 'apollo' );
        if(!empty($venueLink)){
            $return = '<a href="'.$venueLink.'" target="_blank">'.$venueName.'</a>';
        } else {
            $return = $venueName;
        }
        return  $return;
    }

    public function event_org_links( $delimiter = 'and' ) {
        $orgs = $this->eventOrgs();

        if ( $delimiter == 'and' ) {
            $delimiter = __( ' and ', 'apollo' );
        }
        
        $_arr = array();
        
        if ( $orgs ):
            foreach( $orgs as $org ):
                $_arr[] = Apollo_App::the_post_type_link( $org );
            endforeach;
        endif;
        
        $tmp_org = $this->get_meta_data( Apollo_DB_Schema::_APL_EVENT_TMP_ORG );
        return  $_arr ? implode( $delimiter , $_arr) : ($tmp_org ? $tmp_org : __( 'Unknown', 'apollo' ));
    }
    
    public function get_register_org() {
        global $wpdb;
        $query = new Apl_Query( Apollo_Tables::_APL_EVENT_ORG );
        $org_e_id = Apollo_DB_Schema::_APL_E_ORG_ID;
        $org_e_id_main  = Apollo_DB_Schema::_APL_E_ORG_IS_MAIN;
        $org_type  = Apollo_DB_Schema::_ORGANIZATION_PT;
        $strQuery = "
                post_id = {$this->id} 
                AND {$org_e_id_main} = 'yes'
                AND {$org_e_id} IN (
                    SELECT p.ID
                    FROM {$wpdb->posts} p
                    WHERE p.post_status = 'publish'
                          AND p.post_type = '{$org_type}'
                    GROUP BY p.ID      
                )
            ";

        return $query->get_row( $strQuery, $org_e_id );
    }

    public function isEmpty()
    {
        return $this->post === false;
    }

    public function getType()
    {
        return (!$this->isEmpty()) ? $this->post->post_type : '';
    }

    public function render_date_strip_top() {


        $_arr_show_date = $this->getShowTypeDateTime('picture');

        $time_start = $_arr_show_date['unix_start'];
        $time_end = $_arr_show_date['unix_end'];
        $mStart = Apollo_App::apl_date('M', $time_start);
        $mEnd   = Apollo_App::apl_date('M', $time_end);
        $dStart = date_i18n('d', $time_start);
        $dEnd   = date_i18n('d', $time_end);
        if($_arr_show_date['type'] !== 'none'):
            ?>
            <div class="event-time top-pos rec_above_image">
                <div class="md">
                <?php if (($time_end - $time_start) / (24 * 3600 * 30) < Apollo_App::configDateRange()) {
                    $mEnd = $mStart == $mEnd ? '' : $mEnd. '&nbsp;';
                    echo $mStart, '&nbsp;', $dStart;
                    echo $_arr_show_date['type'] == 'two' ?  '&nbsp;-&nbsp;'. $mEnd. $dEnd : '';
                 } else { ?>
                    <div class="month ongoing"><?php _e('Ongoing', 'apollo'); ?></div>
                    <?php
                }
                ?>
                </div>
            </div>
            <?php
        endif;
    }
    public function render_circle_date() {
        
        $_arr_show_date = $this->getShowTypeDateTime('picture');
        
        $time_start = $_arr_show_date['unix_start'];
        $time_end = $_arr_show_date['unix_end'];
        $mStart = Apollo_App::apl_date('M', $time_start);
        $mEnd   = Apollo_App::apl_date('M', $time_end);
        $dStart = date_i18n('d', $time_start);
        $dEnd   = date_i18n('d', $time_end);
        $htDateOpts = of_get_option(Apollo_DB_Schema::_HOME_FEATURED_DATE_OPTION,'');
        if($_arr_show_date['type'] !== 'none'):
        ?>
            <div class="left-event-time <?php echo $htDateOpts == 'flags' ? 'nw-evt-blue' : 'evt-date-bubble'; ?>">
                <?php if (($time_end - $time_start) / (24 * 3600 * 30) < Apollo_App::configDateRange()) {
                    if ($mStart != $mEnd) {
                        ?>
                        <div class="month">
                            <span><?php echo $mStart ?></span>
                            <span><?php echo $dStart ?></span>
                        </div>
                        <?php if ($_arr_show_date['type'] === 'two'): ?>
                            <div class="th">
                                <?php _e('-', 'apollo'); ?>
                            </div>
                            <div class="date">
                                <span><?php echo $mEnd ?></span>
                                <span><?php echo $dEnd ?></span>
                            </div>
                        <?php endif; ?>
                    <?php } else { ?>
                        <div class="month">
                            <span><?php echo $mStart ?></span>
                            <span><?php echo $_arr_show_date['type'] == 'two' ? $dStart . '&nbsp;' .  '-' . '&nbsp;' . $dEnd : $dStart ?></span>
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <div class="month ongoing"><?php _e('Ongoing', 'apollo'); ?></div>
                    <?php
                }
                ?>
                <?php
                    // These html is implementing for date bubble displaying as flag
                    if($htDateOpts == 'flags'):
                ?>
                        <div class="evt-arr">
                            <svg id="Layer_1" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="9px" height="40px" viewbox="0 0 9 40" enable-background="new 0 0 9 40" xml:space="preserve">
                                <polyline fill="<?php echo of_get_option( Apollo_DB_Schema::_PRIMARY_COLOR, '#283473' ); ?>" points="0,0 9,0 2.954,20.023 9,40 0,40 "></polyline>
                              </svg>
                        </div>
                <?php endif; ?>
            </div>
        <?php
        endif;
    }
    
    public function renderCircleTopLeftDateTime($dateBoxLocation, $color) {
      
        if ( $dateBoxLocation && !in_array($dateBoxLocation,array('circle_tl','flags', 'below_tile')) ) return '';
        
        $_arr_show_date = $this->getShowTypeDateTime('picture');
        ob_start();
        require APOLLO_TEMPLATES_DIR. '/events/html/home-featured/items/datetime/top-left-circle.php';
        return ob_get_clean();
    }
    
    public function renderRectangleDateTime($dateBoxLocation, $color = '') {
        
        if ( in_array($dateBoxLocation,array('circle_tl','flags', 'below_tile')) || ! $dateBoxLocation ) return '';

        $_arr_show_date = $this->getShowTypeDateTime('picture');

        $arrDateClass = array(
            'rec_t'   => 'top-pos',
            'rec_b'   => 'bottom-pos',
            'rec_br'  => 'right-pos',
        );

        if ($dateBoxLocation == 'rec_above_image') {
            $dateClass = 'top-pos rec_above_image';
        } else {
            $dateClass = isset($arrDateClass[$dateBoxLocation]) ? $arrDateClass[$dateBoxLocation] : 'top-pos';
        }


        ob_start();
        require APOLLO_TEMPLATES_DIR. '/events/html/home-featured/items/datetime/rectangle.php';
        return ob_get_clean();
    }
    
    public function get_periods_time( $limit = "" ) {
        global $wpdb;
        
        if ($this->periodTimes) {
            return $this->periodTimes;
        }
        
        $apl_query = new Apl_Query( Apollo_Tables::_APL_EVENT_CALENDAR );
        $current = current_time('Y-m-d');
     
        $data = $apl_query->get_where( " event_id = $this->id AND CAST(date_event AS DATE) >= '$current' ", "SQL_CALC_FOUND_ROWS *", " $limit " );
        $this->periodTimes = $data;
        $this->total_datetime = $wpdb->get_var("SELECT FOUND_ROWS()");
        return $data;
    }
    
    public function render_periods_time( $arr_data = array(), $start_elm = "<li>", $end_elm = "</li>" ) {
       
        $render = "";
        if ( $arr_data ): 
            foreach( $arr_data as $e ): 

                $time_from = strtotime( $e->date_event. ' '. $e->time_from );

                $time_to = '';
                if($e->time_to !== '') {
                    $time_to = strtotime( $e->date_event. ' '. $e->time_to );
                }

            $end_string = $time_from != $time_to && $time_to !== '' ?  " - ". date_i18n( 'h:i a ', $time_to ) : '';

            $D = date_i18n(' (D)', $time_from );

            $render .= $start_elm. Apollo_App::apl_date( 'M j, Y ', $time_from ).__( 'at', 'apollo' ).  date_i18n( ' h:i a', $time_from ).$end_string.$D. $end_elm;

            endforeach;
            
        endif;
        return $render;
    }  
    
    public function have_more_periods_time( $offset = 0 ) {
        return $this->total_datetime > $offset + Apollo_Display_Config::MAX_DATETIME;
    }
    
    public function render_sch_date() {
        $_arr_show_date = $this->getShowTypeDateTime();
        if($_arr_show_date['type'] !== 'none') {
            $us = $_arr_show_date['unix_start'];
            $ue = $_arr_show_date['unix_end'];

            if ( ( $ue - $us ) / (24*3600*30) < Apollo_App::configDateRange() ) {
                if($_arr_show_date['type'] === 'one'){
                    echo Apollo_App::apl_date('M d', $us).', <span>'.date('Y', $us).'</span>';
                }
                else if($_arr_show_date['type'] === 'two') {
                    // same year
                    if(date('Y', $ue) === date('Y', $us)) {
                        echo Apollo_App::apl_date('M d', $us).' - '.Apollo_App::apl_date('M d', $ue).', <span>'.date('Y', $us).'</span>';
                    }
                    else {
                        echo Apollo_App::apl_date('M d', $us).', <span>'.date('Y', $us).'</span>'.' - '.Apollo_App::apl_date('M d', $ue).', <span>'.date('Y', $ue).'</span>';
                    }
                }
            } else {
                _e( 'Ongoing', 'apollo' );
            }

        } 
    }

    public function getVenue(){
        $venue = '';
        $venue_id = $this->get_meta_data( Apollo_DB_Schema::_APOLLO_EVENT_VENUE );
        $venue  = get_post($venue_id);
        $venue  = get_venue($venue_id);
        if($venue)
            return $venue;
        return null;
    }

    /***
     * Fix event image rule
     * -1: if event had thumbnail image: use it
     * -2: else
     * -2.1: use org event image
     * -2.2: if org event image no have image use default image
     *
     */
    public function get_image( $size = 'thumbnail', $attr = array(), $attrholder = array(), $thumbnail_size = '', $isDefault = true ) {
        $default_holder = array(
            'aw' => false,
            'ah' => false,
        ) ;

        /** @Ticket - #12942 */
        if (!isset($attr['alt'])) {
            $attr['alt'] = $this->get_title();
        }

        //use event post thumbnail
        if ( has_post_thumbnail( $this->id ) ) {
            $image = get_the_post_thumbnail( $this->id, $size, $attr );

            return $this->lazyImage($image);
        }

        $org = $this->eventOrgs();

        //use org image
        if(isset($org[0])) {

            if ( has_post_thumbnail( $org[0]->ID ) ) {
                $orgThumb = get_the_post_thumbnail($org[0]->ID, $size, $attr);
                if ($orgThumb) {
                    return $this->lazyImage($orgThumb);
                }
            }
        }

        $defaultImage = Apollo_ABS_Module::apollo_get_default_img($size, $this->get_title());
        if (!empty($defaultImage)) {
            return $defaultImage;
        }
        if(!$isDefault){
            return '';
        }
        $attrholder = wp_parse_args($attrholder, $default_holder);
        return apollo_event_placeholder_img( $size, $attrholder['aw'], $attrholder['ah'] , $thumbnail_size, $this->get_title());
    }

    function lazyImage($image) {
        $outputImg = str_replace('<img', '', $image);

        preg_match( '@src="([^"]+)"@' , $image, $match );
        $src = array_pop($match);
        
        return sprintf('<img data-original="%s" %s', $src, $outputImg);        
    }
    
    public function get_title($forceReturn = false) {
        
        if ( $forceReturn ) return $this->post ? $this->post->post_title : '';
        
        if ( ! $this->post ) return false;
        return $this->title_filter($this->post->post_title);
    }
    
    function getSliderImage() {
        
        $src_img = wp_get_attachment_image_src( $this->get_image_id(), 'large');
        
        if ( $src_img[0] ) return $src_img[0];
        
        
        $org = $this->eventOrgs();
        //use org image
        if(isset($org[0])) {
            $src_img = wp_get_attachment_image_src( get_post_thumbnail_id( $org[0]->ID ), 'large');
            if ( $src_img[0] ) return $src_img[0];
        }
        
        return false;
    }

    public function getImageLink($size='full'){
        $image = $this->get_image($size);
        $doc = new DOMDocument();
        $doc->loadHTML($image);
        $imageTags = $doc->getElementsByTagName('img');
        $link = '';
        foreach($imageTags as $tag) {
            $link = $tag->getAttribute('src');
        }
        return $link;
    }

    public function isPrivateEvent(){
        $this->isPrivate = $this->get_meta_data(Apollo_DB_Schema::_E_PRIVATE);
        $isPrivateData = $this->get_meta_data(Apollo_DB_Schema::_E_PRIVATE,Apollo_DB_Schema::_APOLLO_EVENT_DATA);
        if(
            (   $this->isPrivate == 'no'
                || $this->isPrivate == ''
            ) && ($isPrivateData == 'no' || $isPrivateData == '')
        )
            {
                $this->isPrivate = false;
        }
        else{
            $this->isPrivate = true;
        }
        return $this->isPrivate;
    }

    /**
     * check event  has expired ?
     * @param $even_end_date
     * @return bool
     * @author Truonghn <truong.hoangnhat@elinext.com>
     */

    public function isHasExpiredEvent( $even_end_date ) {

        if ( empty($even_end_date)){
            return false;
        }

        return $even_end_date < current_time('Y-m-d') ;

    }

    /**
     * Get associated artists
     * @param bool $limit
     * @param int $page
     * @return array
     */
    public function getAssociatedArtist($limit = false, $page = 1)
    {
        global $wpdb;
        if ( !$this->id || ! Apollo_App::is_avaiable_module(Apollo_DB_Schema::_ARTIST_PT) )
            return false;

        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.ID, p.post_title, post_status FROM {$wpdb->posts} p
            INNER JOIN {$wpdb->{Apollo_Tables::_APL_ARTIST_EVENT}} as apollo_artist_event 
              ON apollo_artist_event.artist_id = p.ID
            WHERE p.post_type = '".Apollo_DB_Schema::_ARTIST_PT."'
            AND p.post_status = 'publish'
            AND apollo_artist_event.event_id = " . $this->id . "
            ORDER BY p.post_title ASC
            ";

        if ($limit) {
            $pageSize = Apollo_Display_Config::_APL_EVENT_ARTISTS_PAGE_SIZE;
            if ($page >= 2) {
                $_offset = $pageSize * ($page - 1);
            } else {
                $_offset = 0;
            }
            $sql .= " limit $pageSize offset $_offset";
        }

        $artists = $wpdb->get_results($sql);

        if(!$limit) {
            return $artists;
        } else {
            $count_result = $wpdb->get_var("SELECT FOUND_ROWS()");
            return array(
                'data' => $artists,
                'total' => $count_result,
                'have_more' => count($artists) + (($page - 1) * $pageSize) < $count_result,
            );
        }
    }

    /**
     * Get associated ids artists
     * @return array
     */
    public function getAssociatedArtistIDs($artists = false)
    {
        if (!$artists) {
            $artists = $this->getAssociatedArtist();
        }

        $artistIdsEvent = array();
        if($artists) {
            foreach ($artists as $e) {
                $artistIdsEvent[] = $e->ID;
            }
        }
        return $artistIdsEvent;
    }

    /**
     * @ticket #17348: 0002410: wpdev55 - Requirements part2 - [Event] Display event discount text
     * @param $includeDiscountURL
     */
    public function generateDiscountText($includeDiscountURL){
        $discountText = $this->getMetaInMainData( Apollo_DB_Schema::_ADMISSION_DISCOUNT_TEXT );
        $discountURL = $this->getMetaInMainData( Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL );
        $discountLink = $includeDiscountURL ? "href='$discountURL' target='_blank'" : '';

        if(!empty($discountText)){
                echo "<p class='apl-event-discount-description-fe'>
                    <a $discountLink> $discountText</a>
                </p>";
        }
    }

    /**
     * @ticket #18181: 0002410: wpdev55 - Modify the position of the 'discount' icon
     * @param $displayAllIcon: Displays all icons once an event has a discount URL
     * @param $icon_fields: is icon field of event
     * @return array|mixed|string
     */
    public function getEventDataIcons($displayAllIcon, $icon_fields){
        $discountURL = $this->getMetaInMainData( Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL );
        if ($displayAllIcon && !empty($discountURL)) {
            return array_keys($icon_fields);
        }
        else {
            return Apollo_App::apollo_get_meta_data($this->id ,Apollo_DB_Schema::_APL_EVENT_POST_ICONS,Apollo_DB_Schema::_APOLLO_EVENT_DATA);
        }
    }

    /**
     * @ticket #19217: 0002596: Sonoma Theme - Build event detail page following the artist detail page
     * @param string $position
     */
    public function renderIconsPosition($position = '', $even_dataIcons = array()) {
        $icon_position = $this->themeOption->getIconPosition();
        $icon_fields = $this->themeOption->getEventIcons();

        $even_dataIcons = !empty($even_dataIcons) ? $even_dataIcons : Apollo_App::apollo_get_meta_data(
            $this->id ,
            Apollo_DB_Schema::_APL_EVENT_POST_ICONS,
            Apollo_DB_Schema::_APOLLO_EVENT_DATA
        );

        if($icon_position == $position) {

            switch ($position):
                case 'after_title':
                case 'before_discount_text':
                    $this->renderIcons($icon_fields,$even_dataIcons, false);
                    break;
                case 'after_thumbs_up':
                default:
                    ?>
                    <div class="icons-list-after-thumbsup">
                        <?php echo $this->renderIcons($icon_fields,$even_dataIcons, false); ?>
                    </div>
                <?php

            endswitch;
        }
    }

    /**
     * @ticket #19217: 0002596: Sonoma Theme - Build event detail page following the artist detail page
     * @param string $position
     */
    public function getSharingInfo($position = 'above_title')
    {
        $listPosition = array('above_title' => 1, 'under_present_by' => 2);

        $socialMediaButtonsLocation = $this->themeOption->getPositionSocial();

        /**
         * if $position == 'top': always show social icon
         */
        if ((isset($listPosition[$position]) && $socialMediaButtonsLocation == $listPosition[$position]) || $position == "top") {

            $checkTime = $this->get_periods_time(" LIMIT 0, " . Apollo_Display_Config::MAX_DATETIME);
            $arraySocial = array(
                'info' => $this->getShareInfo(),
                'id' => $this->id,
                'data_btns' =>
                    array('print',
                        'sendmail',
                    )
            );

            if (is_array($checkTime) && count($checkTime) > 0) {
                $arraySocial['data_btns'][] = 'ics';
                $arraySocial['data_btns'][] = 'googleCalendar';
            }

            SocialFactory::social_btns($arraySocial);
        }
    }

    /**
     * @ticket #19217: 0002596: Sonoma Theme - Build event detail page following the artist detail page
     * @param $position
     */
    public function getDateDisplay($position)
    {
        $dateDisplayType = $this->themeOption->getDateDisplay();

        if ($dateDisplayType == $position) {

            switch ($position){
                case 'strip-top':
                    $this->render_date_strip_top();
                    break;
                case 'plain-text':
                    ob_start();
                    $this->render_sch_date();
                    $str = ob_get_clean();
                    echo '<p class="sch-date">' . $str. '</p>';
                    break;
                case 'default':
                default:
                    $this->render_circle_date();
            }
        }
    }

    /**
     * @ticket #19217: 0002596: Sonoma Theme - Build event detail page following the artist detail page
     * @param $position
     */
    public function getThumbsUpPosition($position)
    {
        $thumbsUpEnable = $this->themeOption->getThumbsUpEnable();
        $thumbsUpPosition = $this->themeOption->getThumbsUpPosition();

        if ($thumbsUpEnable && $thumbsUpPosition == $position) {
            echo '<div class="like" id="_event_rating_' . $this->id . '_event' . '" data-ride="ap-event_rating" data-i="' . $this->id . '" data-t="event"> <a href="javascript:void(0);"><span class="_count">&nbsp;</span></a></div>';

        }
    }

    /**
     * @ticket #19217: 0002596: Sonoma Theme - Build event detail page following the artist detail page
     * @return array
     */
    public function getAccessibility($venue = false)
    {
        global $apollo_event_access;
        $listFullACB = $apollo_event_access;
        ksort($listFullACB);

        $venue = $venue ? $venue : get_venue($this->{Apollo_DB_Schema::_APOLLO_EVENT_VENUE});
        $venue_access_mode = of_get_option(Apollo_DB_Schema::_ENABLE_VENUE_ACCESSIBILITY_MODE, 1);

        if (!empty($venue->id) && $venue_access_mode) {
            $venueData = Apollo_App::unserialize(Apollo_App::apollo_get_meta_data($venue->id, Apollo_DB_Schema::_APL_VENUE_DATA));
            $eventListACB = isset($venueData[Apollo_DB_Schema::_E_CUS_ACB]) ? $venueData[Apollo_DB_Schema::_E_CUS_ACB] : array();
            $eventACBInfo = isset($venueData[Apollo_DB_Schema::_E_ACB_INFO]) ? $venueData[Apollo_DB_Schema::_E_ACB_INFO] : '';
        } else {
            $eventListACB = $this->getMetaInMainData(Apollo_DB_Schema::_E_CUS_ACB);
            $eventACBInfo = $this->getMetaInMainData(Apollo_DB_Schema::_E_ACB_INFO);
        }

        return array(
            'list_acb' => $eventListACB,
            'acb_info' => $eventACBInfo,
            'list_full_acb' => $listFullACB
        );
    }

    /**
     * @ticket #19217: 0002596: Sonoma Theme - Build event detail page following the artist detail page
     * @return string
     */
    public function getCoordinates($venue  = false)
    {
        $str_address_all = $this->getStrAddress();
        $latVn = $lngVn = false;
        $venue = $venue ? $venue : get_venue($this->{Apollo_DB_Schema::_APOLLO_EVENT_VENUE});

        if ($venue) {
            $latVn = $venue->get_meta_data(Apollo_DB_Schema::_VENUE_LATITUDE);
            $lngVn = $venue->get_meta_data(Apollo_DB_Schema::_VENUE_LONGITUDE);
        }

        if ($latVn && $lngVn) {
            $arrCoor = array($latVn, $lngVn);
        } else {
            $arrCoor = Apollo_Google_Coor_Cache::getCoordinateByAddressInLocalDB($str_address_all);
        }

        return $arrCoor ? implode(",", $arrCoor) : '';
    }
}
