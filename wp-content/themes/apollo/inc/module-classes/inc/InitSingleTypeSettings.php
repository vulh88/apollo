<?php

class InitSingleTypeSettings {
    private static $instance;
    private $type;

    private function __construct($data)
    {
        $module = isset($data['module']) ? $data['module'] : '';
        $this->type = Apollo_App::getCustomSlugByModuleName($module);
    }

    public static function getInstance($data) {
        if (self::$instance == null) {
            self::$instance = new InitSingleTypeSettings($data);
        }
        return self::$instance;
    }

    public function getType() {
        return $this->type;
    }
}