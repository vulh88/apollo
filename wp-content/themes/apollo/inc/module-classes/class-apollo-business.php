<?php

/**
 * Created by PhpStorm.
 * User: elisoft-php
 * Date: 18/11/2015
 * Time: 18:45
 */
class Apollo_Business extends Apollo_ABS_Module
{

    private $locationInfo;
    protected $featuresMetaKey;

    public function __construct($data, $type) {
        parent::__construct($data, $type);

        $locationInfo = maybe_unserialize(get_apollo_meta($this->post->ID, Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO, true));
        $this->setLocationInfo($locationInfo);

        $featuredMeta = maybe_unserialize(get_apollo_meta($this->post->ID, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA, true));
        if (is_array($featuredMeta) && array_key_exists(Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY, $featuredMeta)) {
            $this->setFeaturesMetaKey($featuredMeta[Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY]);
        }
    }

    /**
     * @ticket #18087: [CF] 20181030 - [ORG][Admin form] Preview Changes button for the business - item 4, 5
     * @return mixed
     */
    public function getLocationInfo()
    {
        return $this->locationInfo;
    }

    /**
     * @ticket #18087: [CF] 20181030 - [ORG][Admin form] Preview Changes button for the business - item 4, 5
     * @param mixed $locationInfo
     */
    public function setLocationInfo($locationInfo)
    {
        $this->locationInfo = $locationInfo;
    }

    /**
     * @ticket #18087: [CF] 20181030 - [ORG][Admin form] Preview Changes button for the business - item 4, 5
     * @return mixed
     */
    public function getFeaturesMetaKey()
    {
        return $this->featuresMetaKey;
    }

    /**
     * @ticket #18087: [CF] 20181030 - [ORG][Admin form] Preview Changes button for the business - item 4, 5
     * @param mixed $featuresMetaKey
     */
    public function setFeaturesMetaKey($featuresMetaKey)
    {
        $this->featuresMetaKey = $featuresMetaKey;
    }

    public function get_org() {

        $orgID = $this->get_meta_data(Apollo_DB_Schema::_APL_BUSINESS_ORG);
        return get_post($orgID);
    }


    /**
     * @return string
     */
    public function get_post_content(){
        return isset($this->post) && $this->post instanceof WP_Post ? $this->post->post_content : "";
    }

    /**
     * @return string
     */
    public function get_id(){
        return isset($this->post) && $this->post instanceof WP_Post ? (int) $this->post->ID : -1;
    }

    /**
     * @Ticket #13422
     * @param bool $replace
     * @return bool|mixed|string
     */
    public function get_title($replace = false)
    {
        if ( ! $this->post ) return false;
        /** @Ticket #13694 */
        $result = $this->post->post_title;
        if ($replace) {
            $result = str_replace(array("'s Restaurant", "'s restaurant", "'S RESTAURANT"), "", $result);
        }
        return $result;
    }

    public static function outputCustomFieldOrg($post) {
        global $thepostid, $pagenow;


        $thepostid = $post->ID;

        $group_fields = Apollo_Custom_Field::get_group_fields($post->post_type);
        $a = '';
        $services = maybe_unserialize(Apollo_App::apollo_get_meta_data($thepostid, Apollo_DB_Schema::_APL_POST_TYPE_CF_SEARCH));

        if ($group_fields):
            foreach ($group_fields as $gf):
                foreach ($gf['fields'] as $field):
                    switch ($field->cf_type):
                        case 'checkbox':
                            if (in_array($field->name, $services)) {
                                $a .= ' ' . $field->label . ',';
                            }
                            break;
                    endswitch;
                endforeach; // End loop fields
            endforeach;
        endif;
        return $a;
    }

    public function getDisplayBusinessType($hasLink = true, $isSearchLink = false){
        $result = array();
        $list = wp_get_post_terms($this->id,$this->type . '-type');
        if($list){
            foreach($list as $item){
                if($hasLink && $isSearchLink){
                    $result[] = '<a class="link" target="_blank" href="'.site_url('/'.$this->type . '/?' . 'term=' . $item->term_id).'">'.$item->name.'</a>';
                } elseif ($hasLink){
                    $termLink = term_exists($item->term_id,$this->type . '-type') !== null ? get_term_link($item->term_id,$this->type . '-type') : '';
                    $result[] = '<a class="link" target="_blank" href="'.$termLink.'">'.$item->name.'</a>';
                } else {
                    $result[] = $item->name;
                }
            }
        }
        return implode("; ",$result);
    }

    public static function getFieldBSServicesMetaData($moduleName){
        $groupFields = Apollo_Custom_Field::get_group_fields($moduleName);
        if($groupFields) {
            foreach ($groupFields as $gf):
                foreach ($gf['fields'] as $field):
                    if($field->cf_type == 'multi_checkbox' && $field->name == Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY){
                        return Apollo_Custom_Field::get_choice( $field );
                    }
                endforeach;
            endforeach;
        }
        return array();
    }

    public function getDisplayBusinessService($isSearchLink = false){
        $result = array();
        $services = maybe_unserialize(Apollo_App::apollo_get_meta_data($this->id, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA));
        $serviceMetaData = self::getFieldBSServicesMetaData($this->type);
        if( $services
            && isset($services[Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY])
            && !empty($services[Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY])
            && $serviceMetaData ){
            foreach($services[Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY] as $sv){
                if($isSearchLink){
                    $result[] = '<a class="link" target="_blank" href="'.site_url('/'.$this->type . '/?' . 'business_service=' . $sv).'">'.$serviceMetaData[$sv].'</a>';
                } else {
                    $result[] = $serviceMetaData[$sv];
                }
            }
        }
        return implode("; ",$result);
    }

    public function getOrgInfo($thepostid = "" ,$meta_key = "", $arr = array(),$meta_key_break = "",$plus =""){

        $metaAddr = get_apollo_meta($thepostid, $meta_key, true);
        if(!is_array($metaAddr)) {
            $metaAddr = unserialize($metaAddr);
        }
        if (is_array($metaAddr)) {

            foreach ($metaAddr as $key => $value) {
                if($arr && $plus){
                    if(!in_array($key,$arr)) continue;
                } elseif($arr){
                    if(in_array($key,$arr)) continue;
                }
                if($meta_key_break){
                    if($key == $meta_key_break) break;
                }
                $key = str_replace(array("org","_"), " ",$key);
                if(preg_match('#[0-9]#',$key)){
                    $lastCharacter = substr($key,-1);
                    $key = str_replace(substr($key,-1), " ",$key);
                    $key  .= $lastCharacter;
                }
                $key = ucwords($key);
                echo '<div class="options_group event-box org-text-fields">';
                echo '<div> <label for="' . esc_attr($value) . '">' . __($key, 'apollo') . '</label> </div>';
                echo '<div class = text-org> ' . $value . ' </div>';
                echo '</div>';
            }
        }
    }

    /* Thienld : get list of business types */
    public static function getBusinessTypes($displayDefaultOption = true,$required = false){
        $args = array(
            'orderby'           => 'name',
            'order'             => 'ASC',
            'hide_empty'        => false,
            'exclude'           => array(),
            'exclude_tree'      => array(),
            'include'           => array(),
            'number'            => '',
            'fields'            => 'all',
            'slug'              => '',
            'parent'            => '',
            'hierarchical'      => true,
            'child_of'          => 0,
            'childless'         => false,
            'get'               => '',
            'name__like'        => '',
            'description__like' => '',
            'pad_counts'        => false,
            'offset'            => '',
            'search'            => '',
            'cache_domain'      => 'core'
        );
        $bsCates = get_terms('business-type', $args);
        $result = array();
        if(empty($bsCates)) return $result;
        foreach($bsCates as $bs){
            $result[$bs->term_id] = $bs->name;
        }
        $arrKeyBSTypes = array_keys($result);

        if($displayDefaultOption){
            array_unshift($arrKeyBSTypes, 0);
            array_unshift($result, sprintf( __( '-- Select a business type %s --', 'apollo' ), $required ? '(*)' : '' ));
        }
        return array_combine( $arrKeyBSTypes, $result);

    }

    /**
     * Get Fully list of business categories which defined by Admin > Business Categories section
     * @author ThienLD
     * @return array
     */
    public function getListOfBusinessTypesMultiLevels()
    {
        $resultsData = array();
        $args = array(
            'orderby' => 'name',
            'order' => 'ASC',
            'hide_empty' => false,
            'fields' => 'all',
            'hierarchical' => true,
            'parent' => 0,
        );
        $businessTypes = get_terms(Apollo_DB_Schema::_BUSINESS_PT . '-type', $args);
        if (!empty($businessTypes)) {
            foreach ($businessTypes as $itemParent) {
                $resultsData[$itemParent->term_id] = array(
                    'name' => $itemParent->name,
                    'cat-level' => 0);
                $termsChild = get_terms(Apollo_DB_Schema::_BUSINESS_PT . '-type', array(
                    'orderby' => 'name',
                    'order' => 'ASC',
                    'hide_empty' => false,
                    'fields' => 'all',
                    'hierarchical' => true,
                    'parent' => $itemParent->term_id,
                ));
                if (!empty($termsChild)) {
                    foreach ($termsChild as $tc) {
                        $resultsData[$tc->term_id] = array(
                            'name' => '—' . $tc->name,
                            'cat-level' => 1);
                    }
                }
            }
        }
        return $resultsData;
    }

    /**
     * Get list of selected business categories of the current instance
     * @author ThienLD
     * @return array|WP_Error
     */
    public function getListOfSelectedBusinessTypes(){
        $list = wp_get_post_terms($this->id, $this->type . '-type');
        $bsTypeArrayIDs = array();
        if(!empty($list)){
            foreach ($list as $item) {
                $bsTypeArrayIDs[] = (int) $item->term_id;
            }
        }
        return $bsTypeArrayIDs;
    }

    /**
     * @ticket #18087: [CF] 20181030 - [ORG][Admin form] Preview Changes button for the business - item 4, 5
     * @return string
     */
    public function getHours() {
        $mt = $this->getLocationInfo();
        return isset($mt[Apollo_DB_Schema::_APL_BUSINESS_HOURS]) ? $mt[Apollo_DB_Schema::_APL_BUSINESS_HOURS] : "";
    }

    /**
     * @ticket #18087: [CF] 20181030 - [ORG][Admin form] Preview Changes button for the business - item 4, 5
     * @return string
     */
    public function getReservations() {
        $mt = $this->getLocationInfo();
        return isset($mt[Apollo_DB_Schema::_APL_BUSINESS_RESERVATION]) ? $mt[Apollo_DB_Schema::_APL_BUSINESS_RESERVATION] : "";
    }

    /**
     * @ticket #18087: [CF] 20181030 - [ORG][Admin form] Preview Changes button for the business - item 4, 5
     * @return string
     */
    public function getOtherInfo() {
        $mt = $this->getLocationInfo();
        return isset($mt[Apollo_DB_Schema::_APL_BUSINESS_OTHER_INFO]) ? $mt[Apollo_DB_Schema::_APL_BUSINESS_OTHER_INFO] : "";
    }
}
