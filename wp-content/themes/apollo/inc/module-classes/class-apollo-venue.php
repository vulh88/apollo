<?php

class Apollo_Venue extends Apollo_ABS_Module {

    private $artist_image_gallery = '';

    public function __construct($data, $type) {
        $this->iconField = Apollo_DB_Schema::_APL_VENUE_ICONS;
        parent::__construct($data, $type);
    }

    public function get_gallery_attachment_ids() {
        if ( ! isset( $this->artist_image_gallery ) ) {
            // Backwards compat
            $attachment_ids = get_posts( 'post_parent=' . $this->id . '&numberposts=-1&post_type=attachment&orderby=menu_order&order=ASC&post_mime_type=image&fields=ids&meta_key=_apollo_artist_exclude_image&meta_value=0' );
            $attachment_ids = array_diff( $attachment_ids, array( get_post_thumbnail_id() ) );
            $this->artist_image_gallery = implode( ',', $attachment_ids );
        }
        return apply_filters( 'apollo_artist_gallery_attachment_ids', array_filter( (array) explode( ',', $this->artist_image_gallery ) ), $this );
    }

    public function get_title() {

        return parent::get_title();
    }

    public function get_styles() {
        return get_the_terms( $this->id , $this->type.'-style' );
    }

    public function get_mediums() {
        return get_the_terms( $this->id , $this->type.'-medium' );
    }




    public function getListImages(){
        $gallery = explode( ',' , $this->get_meta_data( Apollo_DB_Schema::_APL_VENUE_IMAGE_GALLERY ) );
        return $gallery;
    }

    public function getListVideo(){
        $videos = @unserialize($this->get_meta_data( Apollo_DB_Schema::_APL_VENUE_VIDEOS));
        $returnArray = array();
        if(is_array($videos)){
            foreach($videos as $video){
                $item = array(
                    'embed' => isset($video['video_embed'])?$video['video_embed']:'',
                    'desc' => isset($video['video_desc'])?$video['video_desc']:'',
                );
                $returnArray[] = $item;
            }
        }
        return $returnArray;
    }



    public function render_html( $template = '' ) {
        ob_start();
        include $template ? $template : $this->_template;
        return ob_get_clean();
    }

    public function getPastEvent($offset = false){
        return $this->getUpcommingEvent('<','AND',$offset);
    }

    public function get_full_address( $config = array() ) {
        $address = Apollo_App::apollo_get_meta_data($this->id ,Apollo_DB_Schema::_APL_VENUE_ADDRESS);
        $zip ='';
        if(!is_array($address)){
            $address = maybe_unserialize($address);
        }

        $config = array(
            Apollo_DB_Schema::_VENUE_ADDRESS1,
            Apollo_DB_Schema::_VENUE_ADDRESS2,
            Apollo_DB_Schema::_VENUE_CITY,
            Apollo_DB_Schema::_VENUE_STATE,
            Apollo_DB_Schema::_VENUE_ZIP,
        );


        if ( ! $address ) return false;

        $_add_arr = array();
        foreach ( $config as $c ) {
            if ( isset($address[$c]) && !empty($address[$c]) ) {
                if ( $c == Apollo_DB_Schema::_VENUE_ZIP ){
                    $zip = $address[$c];
                    continue;
                }
                $_add_arr[] = $address[$c] ;
            }
        }

        return $_add_arr ? implode( ', ' , $_add_arr ).' '.$zip : false;
    }



    public static function get_list_org_address($metaKey =  Apollo_DB_Schema::_ORG_ZIP) {
        global $wpdb;
        $arrayZip = array();
        $sql = "SELECT DISTINCT (meta_value) FROM {$wpdb->{Apollo_Tables::_APL_ORG_META}} WHERE meta_key = '%s' AND meta_value <> '' ORDER BY meta_value ASC ;";
        $results = $wpdb->get_results($wpdb->prepare($sql, Apollo_DB_Schema::_APL_ORG_ADDRESS), ARRAY_A);
        if($results !== null) {
            $i = 1;
            foreach($results as $result){
                $meta = maybe_unserialize($result['meta_value']);
                //array
                if(is_array($meta)){
                    if(isset($meta[$metaKey]) && !empty($meta[$metaKey]))
                    {
                        if(!in_array($meta[$metaKey],$arrayZip))
                            $arrayZip[$i] = $meta[$metaKey];
                    }
                }
                //serialize string
                elseif(is_string($meta)){
                    $meta = maybe_unserialize($meta);
                    if(isset($meta[$metaKey]) && !empty($meta[$metaKey]) ){
                        if(!in_array($meta[$metaKey],$arrayZip))
                            $arrayZip[$i] = $meta[$metaKey];
                    }
                }
                $i++;
            }
            return $arrayZip;
        }
        else {
            return array();
        }
    }

    public function getVenueData(){
        $address = Apollo_App::apollo_get_meta_data($this->id ,Apollo_DB_Schema::_APL_VENUE_DATA);
        if(!is_array($address)){
            $address = unserialize($address);
        }
        $data = Apollo_App::apollo_get_meta_data($this->id ,Apollo_DB_Schema::_APL_VENUE_DATA);
        $dataAddress = Apollo_App::apollo_get_meta_data($this->id ,Apollo_DB_Schema::_APL_VENUE_ADDRESS);
        $dataAddress = maybe_unserialize($dataAddress);

        if (Apollo_App::enableMappingRegionZipSelection() && isset($dataAddress[Apollo_DB_Schema::_VENUE_REGION]) ) {
            unset($dataAddress[Apollo_DB_Schema::_VENUE_REGION]);
        }

        if(!is_array($data)){
            $data = unserialize($data);
        }
        
        $icon_fields = maybe_unserialize( get_option( Apollo_DB_Schema::_APL_VENUE_ICONS ) );
        $icons_in_post = $this->get_meta_data( Apollo_DB_Schema::_APL_VENUE_POST_ICONS, Apollo_DB_Schema::_APL_VENUE_DATA );
        
        $returnArray = array(
            'address' => $this->get_full_address(),
            'address1'=>  isset($dataAddress[Apollo_DB_Schema::_VENUE_ADDRESS1])?$dataAddress[Apollo_DB_Schema::_VENUE_ADDRESS1]:'',
            'city' =>  isset($dataAddress[Apollo_DB_Schema::_VENUE_CITY])?$dataAddress[Apollo_DB_Schema::_VENUE_CITY]:'',
            'state' =>  isset($dataAddress[Apollo_DB_Schema::_VENUE_STATE])?$dataAddress[Apollo_DB_Schema::_VENUE_STATE]:'',
            'zip' =>  isset($dataAddress[Apollo_DB_Schema::_VENUE_ZIP])?$dataAddress[Apollo_DB_Schema::_VENUE_ZIP]:'',
            'phone' => isset($data[Apollo_DB_Schema::_VENUE_PHONE])?$data[Apollo_DB_Schema::_VENUE_PHONE]:'',
            'fax' => isset($data[Apollo_DB_Schema::_VENUE_FAX])?$data[Apollo_DB_Schema::_VENUE_FAX]:'',
            'email' => isset($data[Apollo_DB_Schema::_VENUE_EMAIL])?$data[Apollo_DB_Schema::_VENUE_EMAIL]:'',
            'web' => isset($data[Apollo_DB_Schema::_VENUE_WEBSITE_URL])?$data[Apollo_DB_Schema::_VENUE_WEBSITE_URL]:'',
            'blog' => isset($data[Apollo_DB_Schema::_VENUE_PUBLIC_BLOG_URL])?$data[Apollo_DB_Schema::_VENUE_PUBLIC_BLOG_URL]:'',
            'facebook' => isset($data[Apollo_DB_Schema::_VENUE_FACEBOOK_URL])?$data[Apollo_DB_Schema::_VENUE_FACEBOOK_URL]:'',
            'twitter' => isset($data[Apollo_DB_Schema::_VENUE_TWITTER_URL])?$data[Apollo_DB_Schema::_VENUE_TWITTER_URL]:'',
            'inst' => isset($data[Apollo_DB_Schema::_VENUE_INSTAGRAM_URL])?$data[Apollo_DB_Schema::_VENUE_INSTAGRAM_URL]:'',
            'linked' => isset($data[Apollo_DB_Schema::_VENUE_LINKEDIN_URL])?$data[Apollo_DB_Schema::_VENUE_LINKEDIN_URL]:'',
            'icons' => $icons_in_post,
            'icon_fields'   => $icon_fields,
            'pinterest' =>  isset($data[Apollo_DB_Schema::_VENUE_PINTEREST_URL])?$data[Apollo_DB_Schema::_VENUE_PINTEREST_URL]:'',
            'add_fields' => $this->getPrintrAdditionFields(),
            'str_address' => $this->getStrAddress(),
            'data_parking' => array(
                'parking_info' => isset($data[Apollo_DB_Schema::_VENUE_PARKING_INFO])?$data[Apollo_DB_Schema::_VENUE_PARKING_INFO]:'',
                'public_hours' => isset($data[Apollo_DB_Schema::_VENUE_PUBLIC_HOURS])?$data[Apollo_DB_Schema::_VENUE_PUBLIC_HOURS]:'',
                'public_admission_fees' => isset($data[Apollo_DB_Schema::_VENUE_PUBLIC_ADMISSION_FEEDS])?$data[Apollo_DB_Schema::_VENUE_PUBLIC_ADMISSION_FEEDS]:'',
            ),
            'data_access' => $this->getAccessibilityInformation($data),
            'accessibility_info' => isset($data[Apollo_DB_Schema::_E_ACB_INFO])?$data[Apollo_DB_Schema::_E_ACB_INFO]:''
        );

        return $returnArray;
    }



    public function getStrAddress($is_get_venue_name = false) {
        if(isset($this->_arr_address[$is_get_venue_name])) {
            return $this->_arr_address[$is_get_venue_name];
        }

        $arr = array();
        if($is_get_venue_name) {
            $arr = array_merge($arr, array(Apollo_DB_Schema::_VENUE_NAME));
        }
        $arr = array_merge($arr, array(
            Apollo_DB_Schema::_VENUE_ADDRESS1,
            Apollo_DB_Schema::_VENUE_ADDRESS2,
            Apollo_DB_Schema::_VENUE_CITY,
            Apollo_DB_Schema::_VENUE_STATE,
            Apollo_DB_Schema::_VENUE_ZIP,
        ));

        return $this->get_location_data($arr);
    }


    public function get_location_data( $fields ) {

        if ( empty( $fields ) ) {
            return array();
        }

        $venue_id = $this->id;
        if ( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EVENT_PT ) && $venue_id ) {
            $venue = get_venue( get_post( $venue_id ) );
            if ( !$venue ) return false;
        } else {
            $venue = @unserialize( $this->get_meta_data( Apollo_DB_Schema::_APL_EVENT_TMP_VENUE ) );
        }

        $arr_address = array();
        $_is_register_venue = $venue instanceof stdClass || $venue instanceof WP_Post || $venue instanceof Apollo_ABS_Module;

        $is_have_addr = false;
        foreach($fields as $_field) {

            if ( $_is_register_venue ) {
                if ( $_field == Apollo_DB_Schema::_VENUE_NAME ) {
                    $arr_address[] = $venue->get_title();
                    continue;
                }

                /* If have address */
                if($is_have_addr && $_field === Apollo_DB_Schema::_VENUE_ADDRESS2) {
                    continue;
                }

                /* meta data */
                if ( $val = $venue->get_meta_data( $_field ,Apollo_DB_Schema::_APL_VENUE_ADDRESS ) ) {
                    $arr_address[] = $val; // _venue_address2

                    if($_field == Apollo_DB_Schema::_VENUE_ADDRESS1) {
                        $is_have_addr = true;
                    }
                }

                continue;
            }

            if ( isset( $venue[$_field] ) && $venue[$_field] ) {
                $arr_address[] = $venue[$_field];
            }
        }
        return implode( ", ", $arr_address );
    }


    public function get_location_name() {
        $venue_id = $this->id;
        if ( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EVENT_PT ) && $venue_id ) {
            $venue = get_venue( get_post( $venue_id ) );
        } else {
            $venue = unserialize( $this->get_meta_data( Apollo_DB_Schema::_APL_EVENT_TMP_VENUE ) );
        }

        if ( $venue instanceof stdClass || $venue instanceof WP_Post || $venue instanceof Apollo_ABS_Module ) {
            return $venue->get_title();
        } else {
            return isset( $venue[Apollo_DB_Schema::_VENUE_NAME] ) ? $venue[Apollo_DB_Schema::_VENUE_NAME] : '';
        }
        return false;
    }

    public function getAccessibilityInformation($data){
        $customAccess = isset($data[Apollo_DB_Schema::_E_CUS_ACB])?$data[Apollo_DB_Schema::_E_CUS_ACB]:'';
        global $apollo_event_access;
        if(is_array($customAccess) && count($customAccess) > 0){
            $res = array();
            $i = 1;
            $step = 6;
            if(count($customAccess) < count($apollo_event_access)){
                $step = count($customAccess)/2;
                $step = round($step);
            }


            if(is_array($apollo_event_access) && count($apollo_event_access) > 0){
                foreach($apollo_event_access as $image => $label){
                    if(in_array($image,$customAccess)){
                        $list = 'right';
                        if($i <= $step  ){
                            $list = 'left';
                        }
                        $res[$list][$image] = array(
                            'link' =>  get_template_directory_uri().'/assets/images/event-accessibility/2x/'.$image.'.png',
                            'label' => $label,
                        );
                        $i++;
                    }

                }
            }
            return $res;
        }
        return array();
    }

    public function get_venue_up_comming_events(){

    }

    public function get_event_in_venue(){
        $and = 'OR';
        $query = '>=';
        $venue_id = $this->id;
        global $wpdb, $post;
        $this->pageSize = of_get_option(Apollo_DB_Schema::_NUM_VIEW_MORE,Apollo_Display_Config::_NUM_VIEW_MORE);
        if ( ! Apollo_App::is_avaiable_module( 'venue' ) || !$this->id ) {
            return false;
        }

        $filterEventByVenueStr  = ' p.ID IN (
                    SELECT em.apollo_event_id
                    FROM '.$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}.' em
                    WHERE
                         em.meta_value = "'.$venue_id.'"
                        AND em.meta_key = "'.Apollo_DB_Schema::_APOLLO_EVENT_VENUE.'"
                )';



        $apollo_meta_table   = $wpdb->apollo_eventmeta;

        $arr_order = Apollo_Sort_Manager::getSortOneFieldForPostType('event');
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.ID, p.post_title, p.post_content, p.post_excerpt, p.post_name, p.post_status, p.post_type FROM {$wpdb->posts} p
            INNER JOIN {$apollo_meta_table} mt_start_d ON p.ID = mt_start_d.apollo_event_id AND mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
            INNER JOIN {$apollo_meta_table} mt_end_d ON p.ID   = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
        ";
        if(
            $arr_order['type_sort'] === 'apollo_meta'
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE )
        { // sort by meta key
            $sql .= " INNER JOIN {$apollo_meta_table} tblSortMeta ON tblSortMeta.apollo_event_id = p.ID AND tblSortMeta.meta_key = '{$arr_order["metakey_name"]}'";
        }
        $sql .=   " WHERE 1=1 AND p.post_status = 'publish' AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."'  AND ".$filterEventByVenueStr;
        $sql .= " GROUP BY p.ID ";
        if($arr_order['type_sort'] === 'apollo_meta') {
            if(
                $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
                && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE
            ) {
                $sql .= " ORDER BY tblSortMeta.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_START_DATE) {
                $sql .= " ORDER BY mt_start_d.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_END_DATE) {
                $sql .= " ORDER BY mt_end_d.meta_value {$arr_order['order']} ";
            }
        }
        else if($arr_order['type_sort'] === 'post') {
            $sql .= " ORDER BY p.{$arr_order['order_by']} {$arr_order['order']} ";
        }

        
       $eventList       = $wpdb->get_results($sql);

        return $eventList;

    }

    public static function getVenueTerm(){

    }

    public static function updateAllVenueForSearch(){
        //update venue for search
        $option = get_option('venue_accessbility');
        if($option!=1){
            $arr_params = array(
                'post_type'         => Apollo_DB_Schema::_VENUE_PT,
                'posts_per_page'    => -1,

            );
            $venues = query_posts($arr_params);
            foreach($venues as $venue){
                $aceessinfo = Apollo_App::apollo_get_meta_data($venue->ID,Apollo_DB_Schema::_E_CUS_ACB, Apollo_DB_Schema::_APL_VENUE_DATA);

                update_apollo_meta($venue->ID,Apollo_DB_Schema::_VENUE_ACCESSIBILITY,$aceessinfo);
            }
            wp_reset_query();
            //end update venue
            update_option('venue_accessbility',1, 'no');
        }
    }
}
