<?php
/**
 * Abstract Module Class
 *
 * The Apollo event class handles individual event data.
 *
 * @class 		Apollo_ABS_Module
 * @package		inc/Abstracts
 * @category	Abstract Class
 * @author 		vulh
 */
class Apollo_ABS_Module {

	/** @var int The module (post) ID. */
	public $id;

	/** @var object The actual post object. */
	public $post;

    public $type = '';

    public $asummary = array();

    public $permalink = '';

    public $iconField = '';

    private $catIDs = [];

    /**
     * Theme option configuration
     */
    protected $themeOption;

    /**
     * @return array
     */
    public function getCatIDs()
    {
        return $this->catIDs;
    }

	/**
	 * __construct function.
	 *
	 * @access public
	 * @param mixed $data
	 */
	public function __construct( $data, $type) {

        $this->type = $type;

		if ( $data instanceof stdClass || $data instanceof WP_Post || $data instanceof Apollo_ABS_Module ) {
            $this->id   = absint( isset($data->id) ? $data->id : $data->ID ) ;
            $this->post = $data;
		} elseif(intval($data) > 0){
			$this->id   = absint( $data );
			$this->post = apollo_get_post( $this->id );
		}

	}

    /**
     * @return mixed
     */
    public function getThemeOption()
    {
        return $this->themeOption;
    }

	/**
	 * __isset function.
	 *
	 * @access public
	 * @param mixed $key
	 * @return bool
	 */
	public function __isset( $key ) {
        return metadata_exists( 'apollo_'. $this->type, $this->id, $key );
	}

	/**
	 * __get function.
	 *
	 * @access public
	 * @param mixed $key
	 * @return mixed
	 */
	public function __get( $key ) {
        return get_apollo_meta( $this->id, $key, true );
	}

	/**
	 * Get the module post data.
	 *
	 * @access public
	 * @return object
	 */
	public function get_post_data() {
		return $this->post;
	}

    /**
     * Get the module post data.
     *
     * @access public
     * @return object
     */
    public function get_post_author() {
        return $this->exists() ? $this->post->post_author : '';
    }

	/**
	 * Wrapper for get_permalink
	 * @return string
	 */
	public function get_permalink() {
        if($this->permalink !== '')
                return $this->permalink;
		return $this->permalink =  get_permalink( $this->id );
	}

	/**
	 * Returns whether or not the module post exists.
	 *
	 * @access public
	 * @return bool
	 */
	public function exists() {
		return empty( $this->post ) ? false : true;
	}

    public function get_full_title(){
        if ( ! $this->post ) return false;
        return $this->post->post_title;
    }

	/**
	 * Get the title of the post.
	 *
	 * @access public
	 * @return string
	 */
	public function get_title() {
        if ( ! $this->post ) return false;
        return $this->title_filter($this->post->post_title);
	}
    //title rule
    public  function title_filter($title){
        //we apply for listing page
        if(!is_single()){
        $maxWords = Apollo_Display_Config::LISTING_TITLE_MAX_TRIM_WORDS;
            if(strlen ($title) >= $maxWords){
                return substr($title,0,$maxWords).'...';
            }else{
                return $title;
            }
        }
        return $title;
    }

	/**
	 * Get the parent of the post.
	 *
	 * @access public
	 * @return int
	 */
	public function get_parent() {
		return apply_filters('apollo_'.$this->type.'_parent', $this->post->post_parent, $this);
	}

	/**
	 * Returns the module categories.
	 *
	 * @access public
	 * @param string $sep (default: ')
	 * @param mixed '
	 * @param string $before (default: '')
	 * @param string $after (default: '')
	 * @return string
	 */
	public function the_categories( $sep = ', ', $before = '', $after = '' ) {
		return get_the_term_list( $this->id, $this->type. '-type', $before, $sep, $after );
	}

    public function the_terms_list( $tax, $sep = ', ', $before = '', $after = '' ) {
		return get_the_term_list( $this->id, $tax, $before, $sep, $after );
	}

    /**
     * Return the module categories
     *
     * @access public
     * @return array array terms
     *
     */
    public function get_categories() {
        return get_the_terms( $this->id , $this->type.'-type' );
    }

    public function get_term_childrens( $term_id, $tax ) {
        return get_terms(  $tax,  array( 'child_of' => $term_id ) );
    }

    public function renderCategories() {
        $cats = $this->get_categories();

        if ( $cats ) {
            $arr_cat = array();
            foreach( $cats as $c ):
                $arr_cat[] = $c->name;
            endforeach;
        }

        if ( $this->type == Apollo_DB_Schema::_ARTIST_PT ) {
            $other_cat_name = $this->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_ANOTHER_CAT, Apollo_DB_Schema::_APL_ARTIST_DATA );
            if ( $other_cat_name ) $arr_cat[] = $other_cat_name;
        }

        return isset($arr_cat) ? implode( ' - ' , $arr_cat ) : '';
    }

    /**
     * Gets the main module image ID.
     * @return int
     */
    public function get_image_id() {
    	if ( has_post_thumbnail( $this->id ) ) {
			$image_id = get_post_thumbnail_id( $this->id );
		} elseif ( ( $parent_id = wp_get_post_parent_id( $this->id ) ) && has_post_thumbnail( $parent_id ) ) {
			$image_id = get_post_thumbnail_id( $parent_id );
		} else {
			$image_id = 0;
		}
		return $image_id;
    }

	/**
	 * Returns the main module image
	 *
	 * @access public
	 * @param string $size (default: 'thumbnail')
	 * @return string
	 */
	public function get_image( $size = 'thumbnail', $attr = array(), $attrholder = array(), $thumbnail_size = '' ) {
		$image = '';

        $default_holder = array(
           'aw' => false,
            'ah' => false,
        ) ;

        /** @Ticket - #12942 */
        if (!isset($attr['alt'])) {
            $attr['alt'] = $this->get_title();
        }

		if ( has_post_thumbnail( $this->id ) ) {
			$image = get_the_post_thumbnail( $this->id, $size, $attr );

		} elseif ( ( $parent_id = wp_get_post_parent_id( $this->id ) ) && has_post_thumbnail( $parent_id ) ) {
			$image = get_the_post_thumbnail( $parent_id, $size, $attr );
		}

        if ($image) return $image;

        $defaultImage = Apollo_ABS_Module::apollo_get_default_img($size, $this->get_title());
        if (!empty($defaultImage)) {
            return $defaultImage;
        }

        $attrholder = wp_parse_args($attrholder, $default_holder);
        return apollo_event_placeholder_img( $size, $attrholder['aw'], $attrholder['ah'] , $thumbnail_size, $this->get_title());
	}

    /**
     * Returns the main module image
     * @param string $size
     * @param array $attr
     * @param array $attrholder
     * @param string $thumbnail_size
     * @param string $isEnablePlaceholder
     * @return mixed|string
     */
    public function get_image_with_option_placeholder( $size = 'thumbnail', $attr = array(), $attrholder = array(), $thumbnail_size = '', $isEnablePlaceholder = '' ) {
        $image = '';

        $default_holder = array(
            'aw' => false,
            'ah' => false,
        ) ;

        /** @Ticket - #12942 */
        if (!isset($attr['alt'])) {
            $attr['alt'] = $this->get_title();
        }

        if ( has_post_thumbnail( $this->id ) ) {
            $image = get_the_post_thumbnail( $this->id, $size, $attr );

        } elseif ( ( $parent_id = wp_get_post_parent_id( $this->id ) ) && has_post_thumbnail( $parent_id ) ) {
            $image = get_the_post_thumbnail( $parent_id, $size, $attr );
        }

        if ($image) return $image;

        // Check include placeholder
        if ($isEnablePlaceholder === '') {
            $isEnablePlaceholder = of_get_option(Apollo_DB_Schema::_ENABLE_PLACEHOLDER_IMG, 1);
        }
        if (!$isEnablePlaceholder) {
            return '';
        }


        $defaultImage = Apollo_ABS_Module::apollo_get_default_img($size, $this->get_title());
        if (!empty($defaultImage)) {
            return $defaultImage;
        }

        $attrholder = wp_parse_args($attrholder, $default_holder);
        return apollo_event_placeholder_img( $size, $attrholder['aw'], $attrholder['ah'] , $thumbnail_size, $this->get_title());
    }

    /**
     * Get default image
     *
     * @param string $image_style
     * @param string $title
     * @return mixed|string
     * @author Sang Nguyen
     */
    public static function apollo_get_default_img($image_style = 'shop_thumbnail', $title = 'Placeholder')
    {
        $default_image = of_get_option(Apollo_DB_Schema::_DEFAULT_NO_IMAGE);
        //no have org image + thumbnail use admin default image
        if (empty($default_image)) {
            return '';
        }

        $dimensions = apollo_event_image_size($image_style);

        $image = '<img src="' . $default_image . '" alt="'.$title.'"' . ' width=' . esc_attr($dimensions['width']) . ' height=' . esc_attr($dimensions['height']) . ' class="wp-post-image" />';

        return apply_filters('apollo_get_default_img', $image);
    }

    /**
     * @expected string
     */
    public function get_thumb_image_url() {
        $thumbnail_url = wp_get_attachment_thumb_url($this->get_image_id());

        return $thumbnail_url !== false ? $thumbnail_url :   apollo_event_placeholder_img_src();
    }

	/**
	 * @param $max_length
	 *
	 * @return array
	 */
	public function get_summary($max_length = 0, $stripData = true) {
        if($max_length <=0) $max_length = 0;

        if(isset($this->asummary[$max_length]))
            return $this->asummary[$max_length];

        $str =  $this->post ? strip_tags($this->post->post_content) : '';
        $data = Apollo_App::getStringByLength( $str, $max_length );
        if ($data && $stripData) {
            $data['text'] = strip_tags($data['text']);
        }
        return $this->asummary[$max_length] = $data;
	}

    public function get_meta_data( $key, $parent_key = '', $default_value = false ) {
        $val = Apollo_App::apollo_get_meta_data( $this->id , $key, $parent_key);
        return $val ? $val : $default_value;
    }

    public function get_cf_meta_data() {
        return maybe_unserialize( Apollo_App::apollo_get_meta_data( $this->id , Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA) );
    }

    public function get_cf_meta_search() {
        return maybe_unserialize( Apollo_App::apollo_get_meta_data( $this->id , Apollo_DB_Schema::_APL_POST_TYPE_CF_SEARCH) );
    }

    /**
     * Get the value of additional field
     * @param $f_data data of custom field
     * @param $parent_key the key contain the $k
     * @return bool false this field will be not displayed and then the field is displayed
     */
    public function get_custom_field_value( $f_data, $parent_key = Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA ) {

        $locations = maybe_unserialize( $f_data->location );
        if ( $locations && in_array( 'rsb' , $locations ) ) {
            $parent_key = Apollo_DB_Schema::_APL_POST_TYPE_CF_SEARCH;
        }

        $_type = $f_data->cf_type;

        $val = $this->get_meta_data( $f_data->name, $parent_key );

        if (in_array( $_type , array( 'multi_checkbox', 'radio', 'select' )) ) {
            $choices = Apollo_Custom_Field::get_choice($f_data);
        }

        switch( $_type ) {
            case 'text':
                if ( ! $val ) return false;
                break;
            case 'radio':
                if ( Apollo_Custom_Field::has_other_choice($f_data) && $val == 'other_choice') {
                    $val = $this->get_meta_data( $f_data->name.'_other_choice', $parent_key );
                } else {
                    if ( ! $val ) return false;
                    $val = isset( $choices[$val] ) ? $choices[$val] : FALSE;
                }
                break;
            case 'select':
                if ( ! $val ) return false;
                $val = isset( $choices[$val] ) ? $choices[$val] : FALSE;
                break;
            case 'checkbox':
                if ( ! $val ) return false;
                $val = __('Yes', 'apollo');
                break;
            case 'textarea':
                if ( ! $val ) return false;
                break;
            case 'multi_checkbox':
                $val_mul = array();
                if ( $choices ) {
                    foreach ( $choices as $k1 => $v1 ) {
                        if ( is_array( $val ) && in_array( $k1, $val ) ) {
                            $val_mul[] = $v1;
                        }
                    }

                    /*@ticket #18395: 0002504: Arts Education Customizations - On the FE Program detail page's IDOE Standards section please list each selected type on separate lines*/
                    if($f_data->post_type && $f_data->post_type === Apollo_DB_Schema::_PROGRAM_PT && !empty($val_mul)){
                        $val = '<br>' . implode( '<br> ' , $val_mul);
                    }
                    else{
                        $val = implode( ', ' , $val_mul);
                    }
                }

                if ( ! $val ) return false;
                break;
        }



        return $val;
    }

    public function get_post_meta_data( $key ) {
        $data = get_post_meta( $this->id, $key );
        return $data && $data[0] ? $data[0] : '';
    }

    public function is_empty() {
        return $this->post === false;
    }

    public function get_type()
    {
        return (!$this->isEmpty()) ? $this->post->post_type : '';
    }

    public function get_excerpt( $max_length = null ) {
        $str =  $this->post ? strip_tags($this->post->post_excerpt) : '';
		return $this->getStringByLength($str, $max_length);
    }

    public function get_excerpt_blog( $max_length = null, $is_filter = false ) {
        $str =  $this->post ? strip_tags($this->post->post_excerpt) : '';
        return $this->getStringByLength($str, $max_length, $is_filter);
    }
    public function get_summary_blog( $max_length = null, $is_filter = false ) {
        $str = strip_tags(get_post_meta($this->id,'post-summary',true));
        return $this->getStringByLength($str, $max_length, $is_filter);
    }

    public function the_get_author_link() {
        $user = $user = get_userdata($this->post->post_author);
        ?>
        <a href="<?php echo get_author_posts_url($this->post->post_author) ?>"><?php echo sprintf(__('By %s'), $user->data->display_name) ?></a>
        <?php
    }

    public function getStringByLength($str, $max_length, $is_filter = false) {
		return Apollo_App::getStringByLength($str, $max_length, $is_filter);
	}

    public function get_content($max_length = null, $is_filter = false) {
		$str =  $this->post->post_content;
		return $this->getStringByLength($str, $max_length, $is_filter);
	}

    public function get_full_content() {
        $content = $this->post ? $this->post->post_content : '';
        $content = apply_filters( 'the_content', $content );
        return str_replace( ']]>', ']]&gt;', $content );
    }

    public function get_full_excerpt() {
        $content = $this->post ? $this->post->post_excerpt : '';
        $content = apply_filters( 'the_excerpt', $content );
        return str_replace( ']]>', ']]&gt;', $content );
    }

    public function is_have_more_summary($max_length = 0) {
        $str = $this->get_summary($max_length);
        return $str['have_more'];
    }

    public function is_have_more_excerpt($max_length = 0) {
        $str = $this->get_content($max_length);
        return $str['have_more'];
    }

    public function getShareInfo() {
        $sum = $this->get_summary();
        $title = $this->get_full_title();
        return array(
            'url' => $this->get_permalink(),
            'summary' => $sum['text'],
            'title' => $title,
            'media' => $this->get_thumb_image_url(),
        );
    }

    /**
     * @param array $arr_params
     * @param bool $isIcon
     * @return string
     */
    public function renderBookmarkBtn($arr_params = array(), $isIcon = false){

        /**
         * Only display if this value empty or equal to 0
         *
         */

        if ( ! Apollo_App::isEnableAddItBtn() ) return '';

        $default = array(
            'class' => 'btn btn-category',
        );

        $arr_params = wp_parse_args($arr_params, $default);

        /* Is have bookmark */
        $has_bookmark = Apollo_User::has_bookmark( $this->id );
        if($has_bookmark) {
            $arr_params['class'] .= ' bookmark_highlight';
        }

        $sattr = '';
        foreach($arr_params as $attn => $value) {
            $sattr .= $attn . ' = "'. $value.'"';
        }

        ob_start();

        $addTxt = of_get_option(Apollo_DB_Schema::_TEXT_OF_ADD_IT, __('ADD IT', 'apollo'));
        $addedTxt = of_get_option(Apollo_DB_Schema::_TEXT_OF_ADDED_IT, __('ADDED', 'apollo'));

        $additTipText = __( 'Click to bookmark this listing', 'apollo' );
        /** @Ticket #16751 - Render bookmark icon  near the Comment and 'thumbs up' icons. */
        if ($isIcon) :
        ?>
            <div class="bok-mk bok-mk--clickable bok-mk--item">
                <a href="javascript:void(0);"
                   onclick="ajaxAddBookmark('<?php echo admin_url("admin-ajax.php"); ?>','<?php echo $this->id ?>','<?php echo $this->post->post_type ?>', this)"
                   data-added-text="<?php echo $addedTxt ?>"
                   data-add-text="<?php echo $addTxt ?>"
                   data-object-id="<?php echo $this->id ?>"
                   data-post-type="<?php echo $this->post->post_type ?>"
                   data-addit="<?php echo ! $has_bookmark ? '1' : 0 ?>"
                   title="<?php echo $has_bookmark ? $addedTxt : $addTxt; ?>"
                    <?php echo $sattr ?>
                >
                    <?php
                    if(of_get_option(Apollo_DB_Schema::_APOLLO_BOOKMARK_BUTTON_TYPE, 'icon_text') === 'icon_text'){
                        _e('Save', 'apollo');
                    }
                    ?>
                </a>
            </div>
        <?php
        else :
        ?>
            <a href="javascript:void(0);"
               onclick="ajaxAddBookmark('<?php echo admin_url("admin-ajax.php"); ?>','<?php echo $this->id ?>','<?php echo $this->post->post_type ?>', this)"
               data-added-text="<?php echo $addedTxt ?>"
               data-add-text="<?php echo $addTxt ?>"
               data-object-id="<?php echo $this->id ?>"
               data-post-type="<?php echo $this->post->post_type ?>"
               data-addit="<?php echo ! $has_bookmark ? '1' : 0 ?>"
              <?php echo $sattr ?>>
                <span><?php echo $has_bookmark ? $addedTxt : $addTxt ?></span>
                <div class="show-tip"><?php echo $additTipText ?></div>
            </a>
        <?php
        endif;
        return ob_get_clean();

    }

    /**
     * Add bookmark icon
     * @param array $arr_params
     * @return string
     */
    public function renderBookmarkIcon($arr_params = array()){

        /**
         * Only display if this value empty or equal to 0
         *
         */

        if ( ! Apollo_App::isEnableAddItBtn() ) return '';

        $default = array(
            'class' => 'btn btn-category',
        );

        $arr_params = wp_parse_args($arr_params, $default);

        /* Is have bookmark */
        $has_bookmark = Apollo_User::has_bookmark( $this->id );
        if($has_bookmark) {
            $arr_params['class'] .= ' bookmark_highlight';
        }

        $sattr = '';
        foreach($arr_params as $attn => $value) {
            $sattr .= $attn . ' = "'. $value.'"';
        }

        ob_start();

        $addTxt = of_get_option(Apollo_DB_Schema::_TEXT_OF_ADD_IT, __('ADD IT', 'apollo'));
        $addedTxt = of_get_option(Apollo_DB_Schema::_TEXT_OF_ADDED_IT, __('ADDED', 'apollo'));

        ?>
        <a href="javascript:void(0);"
           onclick="ajaxAddBookmark('<?php echo admin_url("admin-ajax.php"); ?>','<?php echo $this->id ?>','<?php echo $this->post->post_type ?>', this)"
           data-added-text="<?php echo $addedTxt ?>"
           data-add-text="<?php echo $addTxt ?>"
           data-object-id="<?php echo $this->id ?>"
           data-post-type="<?php echo $this->post->post_type ?>"
           data-addit="<?php echo ! $has_bookmark ? '1' : 0 ?>"
           title="<?php echo $has_bookmark ? $addedTxt : $addTxt; ?>"
          <?php echo $sattr ?>
        >
            <i class="fa fa-bookmark-o"></i>
        </a>
        <?php

        return ob_get_clean();

    }

    public function the_short_desc( $short, $full, $short_elm, $full_elm ) {

            if ( $short['text'] ):
        ?>
            <div class="a-txt-fea" id="<?php echo $short_elm ?>">
                <?php echo $short['text']; ?>
                <?php if($short['have_more'] === true): ?>
                    <a href="javascript:void(0);" data-type="vmore" class="vmore" data-target="#<?php echo $full_elm ?>"
                       data-own='#<?php echo $short_elm ?>'><?php _e('View more', 'apollo') ?></a>
                <?php endif; ?>
            </div>
            <div class="a-txt-fea hidden" id="_ed_sum_full">
                <?php echo $full; ?>
                <a href="javascript:void(0);" data-type="vmore" class="vmore" data-target="#<?php echo $short_elm ?>"
                   data-own='#<?php echo $full_elm ?>'><?php _e('View less', 'apollo') ?></a>
            </div>
        <?php endif; ?>
        <?php
    }

    public function isEmpty()
    {
        return $this->post === false;
    }

    /**
     * should be removed and improve usage places
     * @todo this function has been moved to the class-apollo-app-function
     * @param $items
     * @param string $parent_key
     * @param string $self_id
     * @return array|mixed
     */
    public static function build_tree_category($items, $parent_key='parent', $self_id = 'term_id') {

        if(empty($items))
            return array();

        $childs = array();

        foreach($items as &$item) {
            $childs[$item->{$parent_key}][] = &$item;
            unset($item);
        }


        foreach($items as &$item)  {
            if (isset($childs[$item->{$self_id}])) {
                $item->childs = $childs[$item->{$self_id}];
            }
        }

        return $childs[0];
    }

    public static function build_option_tree($trees, $default_value, $max_level = Apollo_Display_Config::TREE_MAX_LEVEL, $tax = '', $hiddenCatIDs = array()) {

        if($max_level <= 0) return;

        foreach($trees as $tree ):

            if ( $tax == 'event-type' && is_array($hiddenCatIDs) && in_array($tree->term_id, $hiddenCatIDs) ) {
                continue;
            }
            ?>
            <option value="<?php echo $tree->term_id ?>" <?php selected($tree->term_id, $default_value) ?>><?php echo str_repeat('--', (Apollo_Display_Config::TREE_MAX_LEVEL - $max_level)).' ' . $tree->name ?></option>
        <?php

        if(isset($tree->childs) && !empty($tree->childs)):
            self::build_option_tree($tree->childs, $default_value, $max_level-1, $tax, $hiddenCatIDs);
        endif;
        endforeach;
    }

    public static function get_custom_keys($only_cf_data =  false, $post_type = Apollo_DB_Schema::_EDUCATOR_PT ) {
        $custom_fields = Apollo_Custom_Field::get_all_post_fields( $post_type );

        $cf_data = array();
        $cf_search = array();

        if ( $custom_fields ) {
            foreach( $custom_fields as $field ) {
                $location = maybe_unserialize( $field->location );

                if ( $location && in_array( 'rsb' , $location ) ) {
                    $cf_search[$field->name] = array();
                }
                $validation = Apollo_Custom_Field::get_validation($field);
                $validateObj = array();
                if ( $field->cf_type != 'checkbox' && $field->required == 1 ) {
                    $validateObj[] = 'required';
                }

                if ( $validation ) {
                    $validateObj[] = $validation;
                }

                $cf_data[$field->name] = $validateObj;
            }
        }

        if ($only_cf_data) return $cf_data;

        return array( $cf_data, $cf_search );
    }


    public static function get_post_mod( $post ) {
        $the_post_mod = false;
        switch( $post->post_type ) {
            case Apollo_DB_Schema::_EVENT_PT:
                $the_post_mod = get_event( $post );
                break;
            case Apollo_DB_Schema::_EDUCATOR_PT:
                $the_post_mod = get_educator( $post );
                break;
            case Apollo_DB_Schema::_PROGRAM_PT:
                $the_post_mod = get_program( $post );
                break;
            case Apollo_DB_Schema::_ORGANIZATION_PT:
                $the_post_mod = get_org( $post );
                break;
            case Apollo_DB_Schema::_VENUE_PT:
                $the_post_mod = get_venue( $post );
                break;
            case Apollo_DB_Schema::_ARTIST_PT:
                $the_post_mod = get_artist( $post );
                break;
            case Apollo_DB_Schema::_CLASSIFIED:
                /**
                 * @ticket #18492: 0002504: Arts Education Customizations - Share a Classified listing with Facebook it displays the Arts Ed logo - item 3
                 */
                $the_post_mod = get_classified( $post );
                break;
            case 'post':
                $the_post_mod = get_apl_blog($post);
                break;
        }
        return $the_post_mod;
    }

    /**
     * @expected string
     */
    public function get_article_image_url() {
        $source_url = wp_get_attachment_image_src($this->get_image_id(), 'full');

        return $source_url !== false ? $source_url[0] :   apollo_event_placeholder_img_src();
    }

    public function render_cf_detail($unexpectedField = '') {
        $end_result = '';
        $custom_fields = Apollo_Custom_Field::get_group_fields( $this->type );
        /** Vandd @ticket #12197*/
        $blockLabelStyle = '';
        if ($this->type == Apollo_DB_Schema::_BUSINESS_PT) {
            $blockLabelStyle = of_get_option(Apollo_DB_Schema::_BLOCK_LABEL_STYLE_BUSINESS, 'default') == 'default' ? '' : 'label-style';
        }
        if ( $custom_fields ):

            foreach ( $custom_fields as $g ):
                if ( $g['fields'] ):
                    $q_str = '';
                    /** @Ticket #18558 Display control field */
                    $groupData = Apollo_Custom_Field::get_group_meta($g['group']);
                    foreach ( $g['fields'] as $field ):
                        /** @Ticket #13237 */
                        if ( ! Apollo_Custom_Field::can_display( $field, 'dp' ) ||
                            (!empty($unexpectedField) && $field->name == $unexpectedField)) continue;

                        $_cf_value = $this->get_custom_field_value( $field );
                        if (!empty($groupData['display_control'])
                            && $groupData['display_control'] == $field->name){
                            if (!empty($_cf_value)) {
                                continue;
                            } else {
                                break;
                            }
                        }
                        if (is_array( $_cf_value ) ) {

                            $_cf_value = implode( ', ', $_cf_value );
                        }

                        if ( ! $_cf_value ) continue;


                        $_cf_value = $field->cf_type == 'wysiwyg' ? Apollo_App::convertContentEditorToHtml($_cf_value) : $_cf_value;
                        $before = $after = '';
                        if ( $field->cf_type == 'textarea'  ) {
                            $before = '<p>';
                            $after  = '</p>';
                        } else if ( $field->cf_type == 'text' ) {
                            $before = '<span>';
                            $after  = '</span>';
                        }
                        if( $this->type == Apollo_DB_Schema::_EDUCATOR_PT){
                            $q_str .= '<div class="item '.($field->cf_type == 'wysiwyg' ? "editor" : "").'"><span>'.sprintf( '%s:', $field->label ).' </span>'.$before .$_cf_value. $after.'</div>';
                        } else {
                            $q_str .= '<div class="item '.($field->cf_type == 'wysiwyg' ? "editor" : "").'"><span><strong>'.sprintf( '%s:', $field->label ).' </strong></span>'.$before .$_cf_value. $after.'</div>';
                        }

                    endforeach;

                endif;

                if ( $q_str ) {
                    if ( $g['group'] ) {
                        if (
                            $this->type == Apollo_DB_Schema::_ARTIST_PT ||
                            $this->type == Apollo_DB_Schema::_ORGANIZATION_PT ||
                            $this->type == Apollo_DB_Schema::_VENUE_PT ||
                            $this->type == Apollo_DB_Schema::_CLASSIFIED ||
                            $this->type == Apollo_DB_Schema::_PUBLIC_ART_PT
                        ) {

                            $sectionLabel =  Apollo_App::renderSectionLabels($g['group']->label);

                            $end_result .= '<div class="blog-bkl more-info '. $blockLabelStyle .'">'
                                . '<div class="a-block">'
                                    . $sectionLabel
                                    . '<div class="des">'.$q_str. '</div>'
                                . '</div></div>';
                        } elseif (  $this->type == Apollo_DB_Schema::_BUSINESS_PT){

                            $sectionLabel =  Apollo_App::renderSectionLabels($g['group']->label);

                            $end_result .= '<div class="blog-bkl more-info feature-business '. $blockLabelStyle . (!empty($unexpectedField) ? ' custom-field' : '') .'">'
                                . '<div class="a-block">'
                                . $sectionLabel
                                . '<div class="des">'.$q_str. '</div>'
                                . '</div></div>';
                        }elseif(  $this->type == Apollo_DB_Schema::_EDUCATOR_PT) {
                            $end_result .= '<div class="pg-bkl educator-content">'
                                . '<div class="arrow-right"></div>'
                                . '<h4>'.$g['group']->label.'</h4><div class="des">'.$q_str. '</div></div>';
                        }
                        else {
                            $end_result .= '<div class="pg-bkl more-info">'
                            . '<div class="arrow-right"></div>'
                            . '<h4>'.$g['group']->label.'</h4><div class="des">'.$q_str. '</div></div>';
                        }

                    } else {
                        $end_result .= '<div class="des">'.$q_str . '</div>';
                    }
                }

            endforeach;
        endif;
        echo $end_result;
    }

    public function render_cf_email() {
        $end_result = '';
        $custom_fields = Apollo_Custom_Field::get_group_fields( $this->type );
        if ( $custom_fields ):

            foreach ( $custom_fields as $g ):

                if ( $g['fields'] ):
                        $q_str = '';
                        foreach ( $g['fields'] as $field ):

                            $_cf_value = $this->get_custom_field_value( $field );

                            if (is_array( $_cf_value ) ) {

                                $_cf_value = implode( ', ', $_cf_value );
                            }

                            if ( ! $_cf_value ) continue;


                            $_cf_value = $field->cf_type == 'wysiwyg' ? Apollo_App::convertContentEditorToHtml($_cf_value) : $_cf_value;

                            $q_str .= '<tr>
                            <td class="container-padding header" align="left">
                                <!--[if mso]>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr><td width="50%" valign="top"><![endif]-->
                                    <table width="200" border="0" cellpadding="0" cellspacing="0" align="left" class="force-row">
                                        <tr>
                                            <td class="col" valign="top" style="font-weight:bold;font-size:12px;font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif; padding-bottom:10px;padding-right: 10px;">
                                                '.$field->label.'
                                            </td>
                                        </tr>
                                    </table>
                                    <!--[if mso]></td><td width="50%" valign="top"><![endif]-->
                                    <table width="400" border="0" cellpadding="0" cellspacing="0" align="right" class="force-row">
                                        <tr>
                                            <td class="col" valign="top" align="left" style="font-size:12px;font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;">
                                                '.$_cf_value.'
                                            </td>
                                        </tr>
                                    </table>
                                <!--[if mso]></td></tr></table><![endif]-->
                            </td>
                        </tr><tr>
                            <td height="10" style="font-size:0;line-height:0">&nbsp;</td>
                        </tr>';

                        endforeach;

                endif;

                $end_result .= $q_str;

            endforeach;
        endif;

        return $end_result;
    }

    //Tri LM: add new global print additional fileds
    public function getPrintrAdditionFields(){
        $arrayGroupField = array();
        $custom_fields = Apollo_Custom_Field::get_group_fields($this->type );
        if ( $custom_fields ){
            $i = 0;
            foreach ( $custom_fields as $g ){
                $arrayItem = array();
                $j =0;
                foreach ( $g['fields'] as $field ){
                    if (  Apollo_Custom_Field::can_display( $field, 'dp' ) ){
                        $value = $this->get_custom_field_value( $field );
                        $type = $field->cf_type;
                        switch ($type){
                            case 'wysiwyg' :
                                $value = Apollo_App::convertContentEditorToHtml($value);
                                break;
                            case 'text':
                                break;
                            case 'textarea':
                                break;
                        }
                        if($value){
                            $arrayItem[$j]['label'] = $field->label;
                            $arrayItem[$j]['value'] = $value;
                            $arrayItem[$j]['type'] = $type;
                        }

                    }
                    $j++;
                }
                if(count($arrayItem)>0){
                    $arrayGroupField[$i]['label'] = $g['group']->label;
                    $arrayGroupField[$i]['items'] = $arrayItem;
                }
                $i ++;
            }
        }
        return $arrayGroupField;
    }
    //end add
    // Tri LM -- get audio list

    public function getListAudios($param = '' ){
        $audioList = array();
        $audios = @unserialize($this->get_meta_data( $param ));
        if(is_array($audios)){
            foreach($audios as $audio){
                $link = isset($audio['embed'])?base64_decode($audio['embed']):'';
                $audioItem  = array(
                    'link' => $link,
                    'desc' => isset($audio['desc'])?base64_decode($audio['desc']):'',
                    'type' => $this->checkAudioLinkType($link)
                );
                $audioList[] = $audioItem;
            }
        }
        return $audioList;
    }

    public function checkAudioLinkType($str){
        $embebCheck = new Embed();
        $isFileLink = $embebCheck->checkMimeType($str);
        if($isFileLink)
            return 'file';
        $isIframe = $embebCheck->checkIframeObject($str);
        if($isIframe)
            return 'iframe';
        $isEmbed = $embebCheck->checkEmbed($str);
        if($isEmbed)
            return 'embed';
        $isAudio = $embebCheck->checkAudio($str);
        if($isAudio)
            return 'audio';
        $isObject = $embebCheck->checkObject($str);
        if($isObject)
            return 'object';
    }
    //end audio

    public static function get_tree_event_style($postType = Apollo_DB_Schema::_EVENT_PT, $suppressEmpty = false){
        $terms = get_terms( $postType. '-type', array('hide_empty' => true, 'orderby' => 'name', 'order' => 'ASC'));

        if ($suppressEmpty && !$terms) return '';

        return self::build_tree_category($terms);
    }

    /**
     * Get events category
     * @since wordpress 4.6.1
     * @author vulh
     */
    public static function getTreeEventCategories($suppressEmpty = false) {
        $terms = get_terms( 'event-type', array(
            'hide_empty' => $suppressEmpty,
            'orderby' => 'name',
            'order' => 'ASC')
        );

        if (!$terms) return '';

        return self::build_tree_category($terms);
    }


    /**
     * Render icon of detail page
     * @return string
     * @access public
     *
     */
    function renderIcons($icon_fields, $icons) {

        if ( $icons ) {
            $icon_size = of_get_option(APL_Theme_Option_Site_Config_SubTab::_APL_ICON_SIZE, 1);
            echo '<div class="icons-list ' . ($icon_size ? 'icons-list-free-size' : '') . '">';
            foreach ( $icons as $key => $icon_id ) {
                if ( !isset( $icon_fields[$icon_id] ) )                            continue;
                $icon_src_data = wp_get_attachment_image_src( $icon_fields[$icon_id]['icon_id'], 'full' );

                $desc = isset( $icon_fields[$icon_id]['icon_desc'] ) && $icon_fields[$icon_id]['icon_desc'] ? $icon_fields[$icon_id]['icon_desc'] : '';
                $url = isset( $icon_fields[$icon_id]['icon_url'] ) && $icon_fields[$icon_id]['icon_url'] ? $icon_fields[$icon_id]['icon_url'] : '';
                $open_new_window = isset( $icon_fields[$icon_id]['open_new_window'] ) && $icon_fields[$icon_id]['open_new_window'] ? $icon_fields[$icon_id]['open_new_window'] : '';

                $target = '_parent';
                if($open_new_window) {
                    $target = '_blank';
                }

                if($url) {
                    echo '<span><a class="ref-img-icon" href="' . $url . '" target="' . $target . '"><img class="img-icon" src="' . $icon_src_data[0] . '"/></a>';
                } else {
                    echo '<span><img class="img-icon" src="'.$icon_src_data[0].'" ."/>';
                }

                if ( $desc ) {
                    $primaryColor = of_get_option(Apollo_DB_Schema::_PRIMARY_COLOR);
                    echo '<i class="fa fa-caret-down ttip" style="color:'.$primaryColor.'"></i>';
                    echo '<div class="show-tip" >' . html_entity_decode( htmlentities( stripslashes($desc))) . '</div>';
                }
                echo '</span>';

            }
            echo '</div>';
        }
    }

    public function get_term_ids( $terms ) {
        $output = array();
        if ( $terms ):
            foreach ( $terms as $c ) {
                $output[] = $c->term_id;
            }
        endif;
        return $output;
    }

    public function renderAddFieldPrintDetailPage($fields) {

        if(is_array($fields) && count($fields) > 0){
                foreach($fields as $field){

                $fieldLabel = isset($field['label'])?$field['label']:'';
                $items = isset($field['items'])?$field['items']:array();

            ?>
            <div class="block-detail">
                <a><i class="fa fa-info-circle fa-2x"></i></a>
                <span class="question"><?php echo $fieldLabel; ?></span>
                <!--loop -->
                <?php
                    if(is_array($items) && count($items)>0){
                        foreach($items as $item){
                            $itemLabel = isset($item['label'])?$item['label']:'';
                            $itemValue = isset($item['value'])?$item['value']:'';
                ?>
                <div class="el-blk">
                    <p class="custom"> <strong><?php echo $itemLabel ?> :</strong></p>
                    <p class="custom">
                        <?php echo $itemValue; ?>
                    </p>
                </div><?php }} ?>
                <!--end loop -->

            </div>
        <?php } }
    }

    //TriLM remove trash code
    public function getCat(){
        $cats = $this->get_categories();
        if ( ! $cats ) return false;
        return $cats;
    }


    public function generate_categories($isPrintPage = false, $onlyParentTerm = false){
        $type = $this->type.'-type';
        $page = $this->type;
        return $this->render_tax_links($this->getCat(), $type, 'term', true, $isPrintPage,$page, $onlyParentTerm);
    }

    public function render_tax_links($cats, $tax, $query_var, $has_link,$is_printer = false,$page = Apollo_DB_Schema::_ORGANIZATION_PT, $onlyParentTerm = false) {

        if($cats === false)
            return '';
        $arr_cat = array();
        $arr_cat_ids = $this->get_term_ids( $cats );
        $businessCateIds = [];
        if ( $cats ) {

            foreach( $cats as $c ):
                if ( $c->parent ) continue;

                $this->catIDs[] = $c->term_id;

                /** @Ticket #17906 */
                if ($tax == (Apollo_DB_Schema::_BUSINESS_PT . '-type')) {
                    $businessCateIds[] = $c->term_id;
                }
                $termLink = get_term_link($c->term_id,$tax);
                $termLink = empty($termLink) ? "/" : $termLink;
                $parent = $has_link ? '<a class="link" href="'.(string)$termLink.'">'.$c->name. '</a>' : $c->name;
                if($is_printer == true){
                    $parent = $c->name;
                }

                $child_terms = $this->get_term_childrens( $c->term_id, $tax );
                if( $child_terms && !$onlyParentTerm) {
                    $_arr_childs = array();

                    foreach ( $child_terms as $c ) {
                        $term = get_term( $c, $tax );

                        if ( $term && in_array( $term->term_id , $arr_cat_ids ) ) {
                            if ($tax == (Apollo_DB_Schema::_BUSINESS_PT . '-type')) {
                                $businessCateIds[] = $term->term_id;
                            }

                            $this->catIDs[] = $term->term_id;

                            if ($is_printer == true) {
                                $_arr_childs[] = $term->name;
                            } else {
                                $childTermLink = get_term_link($term->term_id, $term->taxonomy);
                                $childTermLink = empty($childTermLink) ? "/" : $childTermLink;
                                $_arr_childs[] = $has_link ? '<a   class="link" href="' . (string)$childTermLink . '">' . $term->name . '</a>' : $term->name;
                            }
                        }
                    }

                    if ( $_arr_childs ) {
                        if ( $has_link ) {

                            if($is_printer == true){
                                $parent .=  ': '.implode( ', ', $_arr_childs );
                            }else{
                                $parent .= '<a>:&nbsp;</a>'. implode( '<a>,&nbsp;</a>', $_arr_childs );
                            }
                        } else {
                            $parent .= ': '. implode( ', ', $_arr_childs );
                        }

                    }

                    $arr_cat[] = $parent;

                    continue;
                }
                $arr_cat[] = $parent;

            endforeach;
        }
        $desl = ';&nbsp;';
        $businessDining = '';
        if (!empty($businessCateIds)) {
            $businessDining = '<span class="hidden single-business-cate-ids" data-ids="'.implode(",", $businessCateIds).'"></span>';
        }
        if($is_printer == true){
            $desl = ' - ';
            $delimiter = $has_link ? ''.$desl.' ' : '; ';
            return implode( $delimiter , $arr_cat ) . $businessDining ;

        }else{
            $delimiter = $has_link ? '<a>'.$desl.' </a>' : '; ';
            return implode( $delimiter , $arr_cat ) . $businessDining ;
        }
        return false;



    }

    /**
     * @ticket #17756: 0002309: BLOG - Sponsored Content - New Page checkbox and URL field textbox
     * @return string
     */
    public function renderURLBlog(){
        $urlField =  get_post_meta($this->id,'post-new-url',true);
        $newPage = get_post_meta($this->id,'post-new-page',true);
        $target = ($newPage && $newPage == 'on' && $urlField) ? "target='blank'" : '';
        $urlPost = $urlField ? $urlField : get_permalink($this->id);

        return "href='$urlPost' $target";
    }
}
