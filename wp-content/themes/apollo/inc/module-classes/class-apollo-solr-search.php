<?php
/**
 * @author: TriLM.
 * Class solr search
 *
 */

class Apollo_Solr_Search{
    //properties
    protected $config;
    protected $solrClient;
    protected $listPostType;
    protected $filePath = '/solr-search/vendor/autoload.php';
    protected $limtItemInCategory;

    public function __construct(){
        $this->limtItemInCategory = of_get_option(Apollo_DB_Schema::_SOLR_MAX_QUERY_ROW_PER_POST_TYPE);
        //post type

        $this->getPostTypeConfig();

        include(APOLLO_INCLUDES_DIR.$this->filePath);
        //config array
        $config = array(
            'endpoint' => array(
                'localhost' => $this->settingSearch()
            )
        );
        $this->config = $config;
        $this->solrClient = new Solarium\Client($config);
    }

    /***
     *
     * @author Trilm
     * @return array
     *
     */
    public function getPostTypeConfig(){
        $listPostType = of_get_option(Apollo_DB_Schema::_SOLR_POST_TYPE_LIST);

        $listPostTypeArray = array();
        if(is_array($listPostType) && count($listPostType) > 0){
            foreach($listPostType as $k => $value){
                if($value == 1){
                    $listPostTypeArray[] = $k;
                }
            }
        }

        $this->listPostType = $listPostTypeArray;
    }

    public function settingSearch(){

        $blog = get_blog_details(get_current_blog_id());
        $coreName = Apollo_App::formatSolrCoreName($blog->domain);

        $host = defined('APL_SOLR_HOST') && APL_SOLR_HOST ? APL_SOLR_HOST : '127.0.0.1';
        $port = defined('APL_SOLR_PORT') && APL_SOLR_PORT ? APL_SOLR_PORT : '   8983';
        $path = '/solr/'. $coreName;

        $userName = of_get_option(Apollo_DB_Schema::_SOLR_SEARCH_USER_NAME,false);
        $pass = of_get_option(Apollo_DB_Schema::_SOLR_SEARCH_PASSWORD,false);
        $timeout = of_get_option(Apollo_DB_Schema::_SOLR_SEARCH_TIME_OUT,true);

        $config = array(
            'host' => $host,
            'port' =>  $port,
            'path' => $path,
            'timeout'  => $timeout,
            'username' => $userName,
            'password' => $pass,
        );

        if(empty($userName)){
            unset($config['username']);
        }
        if(empty($pass)){
            unset($config['password']);
        }

        return $config;
    }

    /**
     *@author: TriLM
     * insert data to solr server
     */
    public function insertPost(){
        if(!Apollo_Solr_Search::isDisable()){
            $client = $this->solrClient;
            //add content to solr
            $update = $client->createUpdate();
            //can loop doc here
            $itemArray = $this->getListPost($update);
            $update->addDocuments($itemArray);
            $update->addCommit();
            $client->update($update);
            return true;
        }
        return false;
    }

    /**
     * @authot: TriLM
     * get list post for insert
     */
    protected function getListPost($update){

        $itemArray = array();
        foreach($this->listPostType as $postTypeTable) {
            if ($postTypeTable == Apollo_DB_Schema::_EVENT_PT) {
                $this->getEvents($itemArray, $update);
            }
            else {
                $this->getPosts($postTypeTable,$itemArray,$update);
            }

        }
        return $itemArray;
    }

    protected function getEvents(&$itemArray,$update) {
        //query post of table here
        global $wpdb;
        require_once APOLLO_INCLUDES_DIR. '/src/event/inc/class-event-search.php';
        $searchObject = new Apollo_Event_Page();
        $searchObject->search(-1, true);
        $data = $searchObject->result;

        if(is_array($data) && count($data) > 0){
            foreach($data as $k => $item){
                $doc = $update->createDocument();
                $doc->id = $item->ID;
                $doc->category = 'event';
                $doc->title = $item->post_title;
                $itemArray[] = $doc;
            }
        }
    }

    /**
     * @author: TriLm
     * get single post.
     */
    protected function getPosts($postType,&$itemArray,$update){
        //query post of table here
        global $wpdb;
        $select = 'SELECT p.*';
        $from =  "FROM {$wpdb->posts} p";
        $where = "WHERE p.post_type = \"{$postType}\" AND post_status = \"publish\" ";
        $query = sprintf(' %s %s %s  ',$select,$from,$where) ;
        $data = $wpdb->get_results($query);
        if(is_array($data) && count($data) > 0){
            foreach($data as $k => $item){
                $doc = $update->createDocument();
                $doc->id = $item->ID;
                $doc->category = $postType;
                $doc->title = $item->post_title;
                $itemArray[] = $doc;
            }
        }

    }

    /**
     * @author: TriLm
     * Check disable solr
     */
    public static function isDisable(){
        global $blog_id;
        $isEnable =  Apollo_App::is_enable_solr_search($blog_id);
        if(!$isEnable)
        {
            return true;
        }
        return false;
    }

    /**
     * @author: TriLM
     * query post
     */
    public function query($field = '*',$value = '*'){
        if(!Apollo_Solr_Search::isDisable()){
            $queryStr = sprintf('%s:"%s"',$field,$value);
            $query = $this->solrClient->createSelect();
            $query->setQuery($queryStr);
            $query->addSort('category','asc');
            $query->setRows(of_get_option(Apollo_DB_Schema::_SOLR_MAX_QUERY_ROW,true));
            $resultSet = $this->solrClient->select($query);
            return $resultSet->getDocuments();
        }
        return false;
    }

    /**
     * @author: TriLM
     * query post
     */
    public function queryV1($searchFields){
        if(!Apollo_Solr_Search::isDisable()){

            $queryArr = array();

            foreach($searchFields as $field => $value) {
                $queryArr[] = sprintf('%s:*%s*', $field, $value);
            }

            $queryStr = implode(' AND ', $queryArr);
            $query = $this->solrClient->createSelect();
            $query->setQuery($queryStr);

            $query->setRows(of_get_option(Apollo_DB_Schema::_SOLR_MAX_QUERY_ROW,true));
            $resultSet = $this->solrClient->select($query);
            return $resultSet->getDocuments();

        }
        return false;
    }


    /**
     * @author: TriLM
     */
    public function delete($field = '*',$value = '*'){
        if(!Apollo_Solr_Search::isDisable()){
            $queryStr = sprintf('%s:%s',$field,$value);
            $client = $this->solrClient;
            $update = $client->createUpdate();
            $update->addDeleteQuery($queryStr);
            $update->addCommit();
            $client->update($update);
            return true;
        }
        return false;
    }

    public function update(){

    }

    public function search($searchfields){
        if(!Apollo_Solr_Search::isDisable()){
            $data = $this->queryV1($searchfields);

            if(! $data) {
                return '';
            }

            $counterCategory = array();
            $postTypeList = $this->listPostType;
            foreach($postTypeList as $postType){
                $counterCategory[$postType] = 0;
            }
            $returnData= array();
            foreach($data as $dataTempItem){

                $returnData[] =  array(
                    'id' => $dataTempItem->id,
                    'label' => isset($dataTempItem->title[0])?$dataTempItem->title[0]:"",
                    'category' => $dataTempItem->category,
                    'href' => get_the_permalink($dataTempItem->id),
                );
                $counterCategory[$dataTempItem->category] ++;

            }
            return $returnData;

        }
        return '';
    }


    /**
     * @author TriLm
     * query then format list
     *
     */
    public function queryPosts($field = '*',$value = '*'){
        if(!Apollo_Solr_Search::isDisable()){
            $data = $this->query($field,$value);
            if($data !== false){
                $counterCategory = array();
                $postTypeList = $this->listPostType;
                foreach($postTypeList as $postType){
                    $counterCategory[$postType] = 0;
                }
                $returnData= array();
                foreach($data as $dataTempItem){
                        //check available postType
                        if(in_array($dataTempItem->category,$this->listPostType)){
                            //limit item in category
                            $itemInCategory =    $counterCategory[$dataTempItem->category];
                            if($itemInCategory < $this->limtItemInCategory){
                                $returnData[] =  array(
                                    'id' => $dataTempItem->id,
                                    'label' => isset($dataTempItem->title[0])?$dataTempItem->title[0]:"",
                                    'category' => $dataTempItem->category,
                                    'href' => get_the_permalink($dataTempItem->id),
                                );
                                $counterCategory[$dataTempItem->category] ++;
                            }
                        }
                }
                return $returnData;
            }
            return '';
        }
        return '';
    }

    /**
     * @author Trilm
     * Compare new config with old config
     */
    public function compareConfig($newConfig){
        $oldConfig = of_get_option(Apollo_DB_Schema::_SOLR_POST_TYPE_LIST,true);
        $newConfig = json_decode(str_replace('\\','',$newConfig));
        $newConfig =  $this->convertConfigToCompare($newConfig);
        if(!is_array($oldConfig) || count($oldConfig) <= 0){
            if(is_array($newConfig) && count($newConfig) > 0){
                return false;
            }
            return true;

        }
        if($oldConfig == $newConfig){
            return true;
        }
        return false;
    }

    /**
     *
     * @author TriLm
     * @param $newConfig
     * @return array
     *
     */
    protected function convertConfigToCompare($newConfig){
        $newConfigReturn = array();
        foreach($newConfig as $newConfigItem){
            if($newConfigItem->value){
                $newConfigReturn[$newConfigItem->key] = $newConfigItem->value;
            }
        }
        return $newConfigReturn;
    }
}
