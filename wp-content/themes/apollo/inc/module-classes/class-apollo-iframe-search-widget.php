<?php

class Apollo_IFrame_Search_Widget extends Apollo_ABS_Module
{


    public function __construct($data, $type)
    {
        parent::__construct($data, $type);
    }

    public static function getMetaData($post_id = 0, $key)
    {
        $result = '';
        $val = Apollo_App::apollo_get_meta_data($post_id, $key);
        if ($val)
            $result = $val;
        return $result;
    }

    public static function getEventCategories($post_id)
    {
        $eventTypes = self::getMetaData($post_id, APL_Iframe_Search_Widget_Const::META_CATEGORY);
        return !empty($eventTypes) ? Apollo_ABS_Module::build_tree_category(get_terms(Apollo_DB_Schema::_EVENT_PT . '-type',
            array('hide_empty' => true,
                'orderby' => 'name',
                'order' => 'ASC',
                'include' => $eventTypes
            ))) : array();
    }

    public static function getEventVenues($post_id)
    {
        $venueTypes = self::getMetaData($post_id, APL_Iframe_Search_Widget_Const::META_VENUE);
        if(empty($venueTypes)){
            return array();
        }
        $args = array(
            'post_type' => Apollo_DB_Schema::_VENUE_PT,
            'orderby' => 'title',
            'order' => 'ASC',
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'post__in' => $venueTypes
        );
        $listPost = query_posts($args);
        wp_reset_query();
        return $listPost;
    }

    public static function getCity($post_id)
    {
        $city = self::getMetaData($post_id, APL_Iframe_Search_Widget_Const::META_CITIES);
        if(empty($city)){
            return array();
        }
        $_arr = of_get_option(Apollo_DB_Schema::_TERR_DATA);

        $array_state = Apollo_App::getListState();
        sort($array_state);

        // Get cities for specific state

        $cityState = array();
        if ( $array_state ) {
            foreach($array_state as $key) {
                $val = isset($_arr[$key]) && $_arr[$key] ? $_arr[$key] : false;
                if (!$val || !in_array($key, $array_state)) continue;

                $cities = array_keys($val);
                sort($cities);
                foreach( $cities as $k => $ci){
                    if(!in_array($ci,$city)){
                        unset($cities[$k]);
                    }
                }
                $cityState[$key] = $cities;
            }
        }
        array_unshift($cityState, sprintf( __( ' Select City ', 'apollo' )));

        return $cityState;
    }

    public static function getRegions($post_id)
    {
        $regions = self::getMetaData($post_id, APL_Iframe_Search_Widget_Const::META_REGION);

        if (!$regions) return false;

        $array_region = $regions;
        $array_region = array_map('trim', $array_region);

        return $array_region;
    }

    public static function getAllInfoIFrameSearchWidget($post_id)
    {
        $result = array();
        $infoArray = array(APL_Iframe_Search_Widget_Const::META_COLOR,
            APL_Iframe_Search_Widget_Const::META_HTML_AFTER,
            APL_Iframe_Search_Widget_Const::META_HTML_BEFORE,
            APL_Iframe_Search_Widget_Const::META_ENABLE_VENUE,
            APL_Iframe_Search_Widget_Const::META_ENABLE_CITIES,
            APL_Iframe_Search_Widget_Const::META_DEFAULT_CITY,
            APL_Iframe_Search_Widget_Const::META_ENABLE_CATEGORY,
            APL_Iframe_Search_Widget_Const::META_ENABLE_REGION,
            APL_Iframe_Search_Widget_Const::META_IFRAME_CODE,
            APL_Iframe_Search_Widget_Const::META_IFRAME_URL,);
        foreach ($infoArray as $info) {
            $temp = self::getMetaData($post_id, $info);
            if ($temp) {
                $result[$info] = $temp;
            } else {
                $result[$info] = '';
            }
        }

        $listArray = array(APL_Iframe_Search_Widget_Const::META_CATEGORY => self::getEventCategories($post_id),
            APL_Iframe_Search_Widget_Const::META_CITIES => self::getCity($post_id),
            APL_Iframe_Search_Widget_Const::META_REGION => self::getRegions($post_id),
            APL_Iframe_Search_Widget_Const::META_VENUE => self::getEventVenues($post_id),
        );
        return array_merge($result, $listArray);
    }


}
