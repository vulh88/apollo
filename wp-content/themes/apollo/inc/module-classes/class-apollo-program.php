<?php

class Apollo_Program extends Apollo_ABS_Module {

    public function __construct($data, $type) {
        parent::__construct($data, $type);
    }

    public function getData($key = '') {

        if ( ! $this->post ) return false;

        if(empty($this->dataStorage))  {
            $arrBunchData = maybe_unserialize(get_apollo_meta( $this->id, Apollo_DB_Schema::_APL_PROGRAM_DATA, true ));
            $video_embed = maybe_unserialize(get_apollo_meta( $this->id, Apollo_DB_Schema::_APL_PROGRAM_VIDEO_EMBED, true ));
            $video_desc = maybe_unserialize(get_apollo_meta( $this->id, Apollo_DB_Schema::_APL_PROGRAM_VIDEO_DESC, true ));

            if ( ! $arrBunchData ) $arrBunchData = array();

            $this->dataStorage = array_merge(array(
                    'id'         => $this->id,
                    'post_title' => $this->post->post_title,
                    'post_content' => $this->post->post_content,
                    'post_status' => $this->post->post_status,
                ),

                array(
                    'program_type' => wp_get_post_terms($this->id, 'program-type', array( 'fields' => 'ids' )),
                    'population_served' => wp_get_post_terms($this->id, 'population-served', array( 'fields' => 'ids' )),
                    'subject' => wp_get_post_terms($this->id, 'subject', array( 'fields' => 'ids' )),
                    'artistic_discipline' => wp_get_post_terms($this->id, 'artistic-discipline', array( 'fields' => 'ids' )),
                    'cultural_origin' => wp_get_post_terms($this->id, 'cultural-origin', array( 'fields' => 'ids' )),

                ),

                array(
                    'video_embed' => !empty($video_embed) ? $video_embed : array(),
                    'video_desc' => !empty($video_desc) ? $video_desc : array(),
                    Apollo_DB_Schema::_APL_PROGRAM_STARTD => get_apollo_meta( $this->id, Apollo_DB_Schema::_APL_PROGRAM_STARTD, true ),
                    Apollo_DB_Schema::_APL_PROGRAM_ENDD => get_apollo_meta( $this->id, Apollo_DB_Schema::_APL_PROGRAM_ENDD, true ),
                    Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND => get_apollo_meta( $this->id, Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND, true ),
                    Apollo_DB_Schema::_APL_PROGRAM_IS_BILINGUAL => get_apollo_meta( $this->id, Apollo_DB_Schema::_APL_PROGRAM_IS_BILINGUAL, true ),
                    Apollo_DB_Schema::_APL_PROGRAM_IS_FREE => get_apollo_meta( $this->id, Apollo_DB_Schema::_APL_PROGRAM_IS_FREE, true ),

                ),

                $arrBunchData

            );
        }

        if($key === '') { // get all
            return $this->dataStorage;
        }

        if(isset($this->dataStorage[$key]) ) {
            return $this->dataStorage[$key];
        }

        throw new Exception("Key ". $key .' is not exists!');
    }

    public static function getAllProgram($arrParam) {

        return get_posts(array(
                'post_type' => Apollo_DB_Schema::_PROGRAM_PT,
                'post_status' => isset($arrParam['post_status']) ? $arrParam['post_status'] : 'any',
                'orderby' => isset($arrParam['orderby']) ? $arrParam['orderby'] : 'title',
                'posts_per_page' => isset($arrParam['posts_per_page']) ? $arrParam['posts_per_page'] : '-1',
            ));
    }

    public function generate_type_string( $arr_type, $delimiter = ' -' ) {
        if ( ! $arr_type ) {
            return false;
        }

        $arr_cat = array();

        foreach( $arr_type as $c ):
            $arr_cat[] = is_object( $c ) ?  $c->name : $c;
        endforeach;

        return implode( $delimiter , $arr_cat );
    }

    public function get_the_tax( $tax, $sep = ', ', $before = '', $after = '' ) {
		return get_the_term_list( $this->id, $tax, $before, $sep, $after );
	}

    public function get_population_service() {
        global $wpdb;
        $terms = wp_get_post_terms( $this->id, 'population-served', array('fields' => 'ids') );

        if ( ! $terms ) return false;

        $term_mt = Apollo_Tables::_APOLLO_TERM_META;

        $sortedTerms =  $wpdb->get_results("SELECT apollo_term_id AS term_id FROM {$wpdb->{$term_mt}} WHERE apollo_term_id IN (".  implode(',', $terms).") AND meta_key = 'population_served_order' ORDER BY ABS(meta_value) ASC");

        $results = array();
        if ($sortedTerms) {
            foreach ($sortedTerms as $sortedTerm) {
                $results[] = (int) $sortedTerm->term_id;
            }
        }

        $notSortedYet = array_diff($terms, $results);
        $results = array_merge($results, $notSortedYet);

        if (empty($results)) {
            return $terms;
        }

        return $results;

    }

    public function get_population_service_str() {
        $terms = $this->get_population_service();
        if ( ! $terms ) return false;
        $_arr_str = array();

        foreach( $terms as $termID ) {
            $term = get_term( $termID , 'population-served' );
            $_arr_str[] = $term->name;
        }
        return implode(', ', $_arr_str);
    }

    public function get_yn( $key, $parent = '' ) {
        $val = $this->get_meta_data( $key, $parent );
        return $val === 'yes' ? __( 'Yes', 'apollo' ) : __( 'No', 'apollo' );
    }

    public function has_educator() {
        $apl_query = new Apl_Query( Apollo_Tables::_APL_PROGRAM_EDUCATOR );
        return $apl_query->get_where( "prog_id = '$this->id' AND edu_id <> '' " );
    }

    public function process_videos_data() {
        $video_embeds = $this->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_VIDEO_EMBED );
        $video_descs = $this->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_VIDEO_DESC );
        $video_data = array();
        if ( $video_embeds ) {
            foreach( $video_embeds as $i => $embed ) {
                if ( ! $embed ) continue;
                $video_data[] = array( 'embed'  => $embed, 'desc'   => isset( $video_descs[$i] ) ? $video_descs[$i] : '' );
            }
        }
        return $video_data;
    }

    public function get_image($size = 'thumbnail', $attr = array(), $attrholder = array(), $thumbnail_size = '') {

        /** @Ticket - #12942 */
        if (!isset($attr['alt'])) {
            $attr['alt'] = $this->get_title();
        }

        $gallerys = $this->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_IMAGE_GALLERY );
        $_arr_gallery = explode( ',' , $gallerys );
        if ( $_arr_gallery && $_arr_gallery[0] ) {
            $image = wp_get_attachment_image( $_arr_gallery[0], $size );
        } else {
            $edu_id = $this->get_educator_id();
            if ( $edu_id && has_post_thumbnail( $edu_id ) ) {
                $image = get_the_post_thumbnail( $edu_id, $size, $attr );
            } else {
                $aw = !empty($attrholder['aw']) ? $attrholder['aw'] : false;
                $ah = !empty($attrholder['ah']) ? $attrholder['ah'] : false;
                $image = apollo_event_placeholder_img( $size, $aw, $ah , $thumbnail_size, $this->get_title());
            }
        }
        return $image;
    }

    public function get_educator_id() {
        $apl_query = new Apl_Query( Apollo_Tables::_APL_PROGRAM_EDUCATOR );
        $prog_edu = $apl_query->get_row( "prog_id = $this->id" );
        return $prog_edu ? $prog_edu->edu_id : false;
    }

    public static function get_tree_terms( $tax_key ) {

        return self::build_tree_category( get_terms( $tax_key, array('hide_empty' => 0, 'orderby' => 'name', 'order' => 'ASC') ) );
    }

    public function get_permalink() {
        $edu_id = $this->get_meta_data( Apollo_DB_Schema::_PROGRAM_EDU_ID, Apollo_DB_Schema::_APL_PROGRAM_DATA );

        if ( ! $edu_id ) {
            $edu_id = $this->get_educator_id ();
        }
        return sprintf( '%s%s', get_permalink($edu_id), $this->id );
    }

    /**
     * @param $files
     * @return string
     */
    public function getAttachFiles($files){
        /*@ticket #18391: 0002504: Arts Education Customizations - FE Program Form - The PDF files and text do NOT display on the admin program form nor the FE program detail page - item 1, 2*/
        $_all_attach_url = '';
        foreach( $files as  $attach_pdf_id ){
            $_attach_title = get_the_title( $attach_pdf_id );
            $_attach_url = wp_get_attachment_url( $attach_pdf_id );
            $attachDescription = has_excerpt($attach_pdf_id) ? get_the_excerpt($attach_pdf_id) : '';
            $_all_attach_url .= $_attach_url;
            if ( ! $_attach_url )                        continue;

            $_attach_title = $_attach_title ? $_attach_title : $_attach_url;
            echo "
            <div class='support-materials'>
                <p>$attachDescription</p>
                <i class='fa fa-file-pdf-o'></i><a href='$_attach_url' target='_blank' class='vmore'>$_attach_title</a>
            </div> ";
        }

        return $_all_attach_url;
    }
}
