<?php

class Apollo_Classified extends Apollo_ABS_Module
{

    private $_arr_address = array();

    public $dataStorage;

    public function __construct($data, $type)
    {
        $this->iconField = Apollo_DB_Schema::_APL_CLASSIFIED_ICONS;
        parent::__construct($data, $type);
    }

    public function getData($key = '')
    {

        if (!$this->post) return false;

        if (empty($this->dataStorage)) {
            $arrBunchData = maybe_unserialize(get_apollo_meta($this->id, Apollo_DB_Schema::_APL_CLASSIFIED_DATA, true));
            $arrBunchAddress = maybe_unserialize(get_apollo_meta($this->id, Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS, true));

            $this->dataStorage = array_merge(array(
                'id' => $this->id,
                'post_title' => $this->post->post_title,
                'post_content' => $this->post->post_content,
                'post_status' => $this->post->post_status,
                'post_cat' => $this->get_term_ids($this->get_categories()),
            ),
                array_merge($arrBunchData, $arrBunchAddress)
            );
        }

        if ($key === '') { // get all
            return $this->dataStorage;
        }

        if (isset($this->dataStorage[$key])) {
            return $this->dataStorage[$key];
        }

        throw new Exception("Key " . $key . ' is not exists!');
    }

    public static function get_user_classifieds($user_id)
    {
        $apl_query = new Apl_Query('posts');
        return $apl_query->get_where(" post_author = '$user_id' AND post_status = 'publish' AND post_type = '" . Apollo_DB_Schema::_CLASSIFIED . "'");
    }

    public function render_tax_links($cats, $tax, $query_var, $has_link, $is_printer = false, $page = Apollo_DB_Schema::_CLASSIFIED, $onlyParentTerm = false)
    {
        $arr_cat = array();
        $arr_cat_ids = $this->get_term_ids($cats);
        if ($cats) {

            foreach ($cats as $c):
                if ($c->parent) continue;

                $parent = $has_link ? '<a class="link" href="' . Apollo_App::getCustomUrlByModuleName(Apollo_DB_Schema::_CLASSIFIED) . '/?' . $query_var . '=' . $c->term_id . '">' . $c->name . '</a>' : $c->name;
                if ($is_printer == true) {
                    $parent = $c->name;
                }

                $child_terms = $this->get_term_childrens($c->term_id, $tax);
                if ($child_terms) {
                    $_arr_childs = array();

                    foreach ($child_terms as $c) {
                        $term = get_term($c, $tax);

                        if ($term && has_term($c, $tax, $this->id) && in_array($term->term_id, $arr_cat_ids))

                            if ($is_printer == true) {
                                $_arr_childs[] = $term->name;
                            } else {
                                $_arr_childs[] = $has_link ? '<a   class="link" href="' . Apollo_App::getCustomUrlByModuleName(Apollo_DB_Schema::_CLASSIFIED) . '/?' . $query_var . '=' . $term->term_id . '">' . $term->name . '</a>' : $term->name;
                            }
                    }

                    if ($_arr_childs) {
                        if ($has_link) {

                            if ($is_printer == true) {
                                $parent .= ': ' . implode(', ', $_arr_childs);
                            } else {
                                $parent .= '<a>: </a>' . implode('<a>, </a>', $_arr_childs);
                            }
                        } else {
                            $parent .= ': ' . implode(', ', $_arr_childs);
                        }

                    }

                    $arr_cat[] = $parent;

                    continue;
                }
                $arr_cat[] = $parent;

            endforeach;
        }
        $desl = ';';

        if ($is_printer == true) {
            $desl = ' - ';
            $delimiter = $has_link ? '' . $desl . ' ' : '; ';
            return implode($delimiter, $arr_cat);

        } else {
            $delimiter = $has_link ? '<a>' . $desl . ' </a>' : '; ';
            return implode($delimiter, $arr_cat);
        }
        return false;

    }

    public function getDataDetail()
    {
        $address = Apollo_App::apollo_get_meta_data($this->id, Apollo_DB_Schema::_APL_CLASSIFIED_DATA);
        if (!is_array($address)) {
            $address = unserialize($address);
        }
        $data = Apollo_App::apollo_get_meta_data($this->id, Apollo_DB_Schema::_APL_CLASSIFIED_DATA);
        $dataAddress = Apollo_App::apollo_get_meta_data($this->id, Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS);
        $dataAddress = maybe_unserialize($dataAddress);
        $deadlineDate = Apollo_App::apollo_get_meta_data($this->id, Apollo_DB_Schema::_APL_CLASSIFIED_DEADLINE_DATE);

        if (!is_array($data)) {
            $data = unserialize($data);
        }

        $cityTerrVal = isset($dataAddress[Apollo_DB_Schema::_CLASSIFIED_CITY]) ? $dataAddress[Apollo_DB_Schema::_CLASSIFIED_CITY] : '';
        $cityTmpVal = isset($dataAddress[Apollo_DB_Schema::_CLASSIFIED_TMP_CITY]) ? $dataAddress[Apollo_DB_Schema::_CLASSIFIED_TMP_CITY] : '';
        $cityVal = !empty($cityTerrVal) && !is_int($cityTerrVal) ? $cityTerrVal : trim($cityTmpVal);
        $cityVal = !empty($cityVal) ? $cityVal : '';

        $icon_fields = maybe_unserialize(get_option(Apollo_DB_Schema::_APL_CLASSIFIED_ICONS));
        $icons_in_post = $this->get_meta_data(Apollo_DB_Schema::_APL_CLASSIFIED_POST_ICONS, Apollo_DB_Schema::_APL_CLASSIFIED_DATA);

        $returnArray = array(
            'address' => $this->get_full_address(),
            'city' => $cityVal,
            'state' => isset($dataAddress[Apollo_DB_Schema::_CLASSIFIED_STATE]) ? $dataAddress[Apollo_DB_Schema::_CLASSIFIED_STATE] : '',
            'phone' => isset($data[Apollo_DB_Schema::_CLASSIFIED_PHONE]) ? $data[Apollo_DB_Schema::_CLASSIFIED_PHONE] : '',
            'fax' => isset($data[Apollo_DB_Schema::_CLASSIFIED_FAX]) ? $data[Apollo_DB_Schema::_CLASSIFIED_FAX] : '',
            'email' => isset($data[Apollo_DB_Schema::_CLASSIFIED_EMAIL]) ? $data[Apollo_DB_Schema::_CLASSIFIED_EMAIL] : '',
            'web' => isset($data[Apollo_DB_Schema::_CLASSIFIED_WEBSITE_URL]) ? $data[Apollo_DB_Schema::_CLASSIFIED_WEBSITE_URL] : '',
            'name' => isset($data[Apollo_DB_Schema::_CLASSIFIED_NAME]) ? $data[Apollo_DB_Schema::_CLASSIFIED_NAME] : '',
            'deadline_date' => $deadlineDate,
            'icons' => $icons_in_post,
            'icon_fields' => $icon_fields,
            'add_fields' => $this->getPrintrAdditionFields(),
            'org' => $this->classifiedOrg(),
        );

        return $returnArray;
    }

    public function get_full_address($config = array())
    {
        $address = Apollo_App::apollo_get_meta_data($this->id, Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS);

        if (!is_array($address)) {
            $address = maybe_unserialize($address);
        }

        $config = array(
            Apollo_DB_Schema::_CLASSIFIED_ADDRESS,
            Apollo_DB_Schema::_CLASSIFIED_CITY,
            Apollo_DB_Schema::_CLASSIFIED_STATE,
        );


        if (!$address) return false;
        if (empty($address[Apollo_DB_Schema::_CLASSIFIED_ADDRESS])) unset($config[0]);

        // Thienld : get custom data classified city
        $cityTerrVal = isset($address[Apollo_DB_Schema::_CLASSIFIED_CITY]) ? $address[Apollo_DB_Schema::_CLASSIFIED_CITY] : '';
        $cityTmpVal = isset($address[Apollo_DB_Schema::_CLASSIFIED_TMP_CITY]) ? $address[Apollo_DB_Schema::_CLASSIFIED_TMP_CITY] : '';
        $cityVal = !empty($cityTerrVal) && !is_int($cityTerrVal) ? $cityTerrVal : trim($cityTmpVal);
        $cityVal = !empty($cityVal) ? $cityVal : __("Unknown", "apollo");

        // Truonghn : get custom data classified state
        $stateTerrVal = isset($address[Apollo_DB_Schema::_CLASSIFIED_STATE]) ? $address[Apollo_DB_Schema::_CLASSIFIED_STATE] : '';
        $stateTmpVal = isset($address[Apollo_DB_Schema::_CLASSIFIED_TMP_STATE]) ? $address[Apollo_DB_Schema::_CLASSIFIED_TMP_STATE] : '';
        $stateVal = !empty($stateTerrVal) && !is_int($stateTerrVal) ? $stateTerrVal : trim($stateTmpVal);
        $stateVal = !empty($stateVal) ? $stateVal : __("Unknown", "apollo");

        $address[Apollo_DB_Schema::_CLASSIFIED_STATE] = $stateVal;
        $address[Apollo_DB_Schema::_CLASSIFIED_CITY] = $cityVal;

        $_add_arr = array();
        foreach ($config as $c) {
            if (isset($address[$c])) {
                $_add_arr[] = $address[$c];
            }
        }

        return $_add_arr ? implode(', ', $_add_arr) : false;
    }

    public function getListImages()
    {
        $gallery = explode(',', $this->get_meta_data(Apollo_DB_Schema::_APL_CLASSIFIED_IMAGE_GALLERY));
        if ($gallery && count($gallery) == 1 && $gallery[0] == '') return false;
        return $gallery;
    }


    public function getStrAddress()
    {
        if (!empty($this->_arr_address)) {
            return $this->_arr_address;
        }

        $meta_key_address = $this->get_meta_data(Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS);

        if (!empty($meta_key_address)) {
            $_arr_address = maybe_unserialize($meta_key_address);

            return implode(", ", array_values($_arr_address));
        }

        return '';
    }

    public function renderOrgHtml()
    {

        $text = __('Posted by %s');
        $vn_link = $empty_link = __('Unknown', 'apollo');
        $og_link = $this->classified_org_links();
        return !empty($og_link) ? sprintf($text, $og_link, $vn_link) : '';
    }

    public function classifiedOrg()
    {

        if (!Apollo_App::is_avaiable_module(Apollo_DB_Schema::_ORGANIZATION_PT)) return false;

        global $wpdb;
        $sql = "
            SELECT p.* FROM {$wpdb->posts} p
            INNER JOIN {$wpdb->{Apollo_Tables::_APL_CLASSIFIED_META}} mt_classified ON mt_classified.meta_key ='" . Apollo_DB_Schema::_APOLLO_CLASSIFIED_ORGANIZATION . "' AND mt_classified.meta_value = p.ID
            WHERE p.post_type = '" . Apollo_DB_Schema::_ORGANIZATION_PT . "'
            AND p.post_status = 'publish'
            AND mt_classified.apollo_classified_id = '$this->id'
        ";
        return $wpdb->get_row($sql);
    }

    public function classified_org_links()
    {
        $org = $this->classifiedOrg();
        $tmp_org = $this->get_meta_data(Apollo_DB_Schema::_APL_CLASSIFIED_TMP_ORG);
        return $org ? Apollo_App::the_post_type_link($org) : ($tmp_org ? $tmp_org : '');
    }

    public function classified_org_edit_post_link()
    {
        $org = $this->classifiedOrg();
        $tmp_org = $this->get_meta_data(Apollo_DB_Schema::_APL_CLASSIFIED_TMP_ORG);
        return $org ? '<a href="' . get_edit_post_link($org) . '">' . $org->post_title . '</a>' : ($tmp_org ? $tmp_org : __('Unknown', 'apollo'));
    }

    function lazyImage($image)
    {
        $outputImg = str_replace('<img', '', $image);

        preg_match('@src="([^"]+)"@', $image, $match);
        $src = array_pop($match);

        return sprintf('<img data-original="%s" %s', $src, $outputImg);
    }

    /**
     * @param string $size
     * @param array $attr
     * @param array $attrholder
     * @param string $thumbnail_size
     * @param string $isEnablePlaceholder
     * @return mixed|string
     */
    public function get_image($size = 'thumbnail', $attr = array(), $attrholder = array(), $thumbnail_size = '', $isEnablePlaceholder = '')
    {
        $default_holder = array(
            'aw' => false,
            'ah' => false,
        );

        /** @Ticket - #12942 */
        if (!isset($attr['alt'])) {
            $attr['alt'] = $this->get_title();
        }

        $attrholder = wp_parse_args($attrholder, $default_holder);
        $org = $this->classifiedOrg();

        //use classified post thumbnail
        if (has_post_thumbnail($this->id)) {
            $image = get_the_post_thumbnail($this->id, $size, $attr);

            return $this->lazyImage($image);
        }
        //use org image
        if (!empty($org)) {

            if (has_post_thumbnail($org->ID)) {
                $orgThumb = get_the_post_thumbnail($org->ID, $size, $attr);
                if ($orgThumb) {
                    return $this->lazyImage($orgThumb);
                }
            }
        }

        /** @Ticket #13137 */
        // Check include placeholder
        if ($isEnablePlaceholder === '') {
            $isEnablePlaceholder = of_get_option(Apollo_DB_Schema::_ENABLE_PLACEHOLDER_IMG, 1);
        }
        if (!$isEnablePlaceholder) {
            return '';
        }

        $defaultImage = Apollo_ABS_Module::apollo_get_default_img($size, $this->get_title());
        if (!empty($defaultImage)) {
            return $defaultImage;
        }

        return apollo_event_placeholder_img($size, $attrholder['aw'], $attrholder['ah'], $thumbnail_size, $this->get_title());
    }

    public function get_title_listing_page()
    {
        if (!$this->post) return false;
        return $this->post->post_title;
    }
}