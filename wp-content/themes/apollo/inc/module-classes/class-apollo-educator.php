<?php

class Apollo_Educator extends Apollo_ABS_Module {

    private $dataStorage = array();
    public $rsb_keys = array();
    
    public function __construct($data, $type) {
        $this->iconField = Apollo_DB_Schema::_APL_EDUCATORS_ICONS;
        parent::__construct($data, $type);
    }

    public function getData($key = '') {

        if ( ! $this->post ) return false;
        
        if(empty($this->dataStorage))  {
            $arrBunchData = maybe_unserialize(get_apollo_meta( $this->id, Apollo_DB_Schema::_APL_EDUCATOR_DATA, true ));

            $this->dataStorage = array_merge(array(
                    'id'         => $this->id,
                    'post_title' => $this->post->post_title,
                    'post_content' => $this->post->post_content,
                    'post_status' => $this->post->post_status,
                ),
                $arrBunchData
            );
        }

        if($key === '') { // get all
            return $this->dataStorage;
        }

        if(isset($this->dataStorage[$key]) ) {
            return $this->dataStorage[$key];
        }

        throw new Exception("Key ". $key .' is not exists!');
    }

    public function get_term_ids( $terms ) {
        $output = array();
        if ( $terms ):
            foreach ( $terms as $c ) {
                $output[] = $c->term_id;
            }
        endif;
        return $output;
    }
    
    public function generate_type_string( $arr_type, $delimiter = ' - ' ) {
        if ( ! $arr_type ) {
            return false;
        }
        
        $arr_cat = array();
        
        foreach( $arr_type as $c ):
            $arr_cat[] = is_object( $c ) ?  $c->name : $c;
        endforeach;
        
        return implode( $delimiter , $arr_cat );
        
    }
    
    public function get_programs() {
        $apl_query = new Apl_Query( Apollo_Tables::_APL_PROGRAM_EDUCATOR );
        return $apl_query->get_where( " edu_id = '$this->id' AND prog_id <> '' " );
    }

    /* quocpt - extra method for getting list of programs does not expired */
    public function get_programs_does_not_expired() {
        global $wpdb;
        $program_educator = $wpdb->prefix.Apollo_Tables::_APL_PROGRAM_EDUCATOR;
        $programmeta = $wpdb->prefix.Apollo_Tables::_APL_PROGRAM_META;
        $currentDate = current_time('Y-m-d');

        /** @Ticket #12980 sort Program name */
        $query = " SELECT pe.* FROM ".$program_educator." AS pe
                   LEFT JOIN ".$programmeta." AS mt_exd ON pe.prog_id = mt_exd.apollo_program_id AND mt_exd.meta_Key = '".Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND."'
                   INNER JOIN ".$wpdb->posts." AS p ON  pe.prog_id = p.ID
                   WHERE pe.edu_id='".$this->id."'
                   AND (
                        (CAST('$currentDate' AS DATE) <= CAST(mt_exd.meta_value AS DATE) OR mt_exd.meta_value IS NULL
                            OR mt_exd.meta_value = ''
                        )
                    )
                    ORDER BY p.post_title";
        return $wpdb->get_results( $query );
    }

    public function is_program_belong_to_educator( $pro_id = 0 ) {
        $apl_query = new Apl_Query( Apollo_Tables::_APL_PROGRAM_EDUCATOR );
        return $apl_query->get_where( " edu_id = '$this->id' AND prog_id = '$pro_id' " );
    }
    
    public static function get_user_programs($educator_id) {
        $apl_query = new Apl_Query( Apollo_Tables::_APL_PROGRAM_EDUCATOR );
        return $apl_query->get_where( " edu_id = '$educator_id' " );
    }

    /**
     * @return bool|string
     */
    public function get_full_address(){

        $config = array(
            Apollo_DB_Schema::_APL_EDUCATOR_ADD1,
            Apollo_DB_Schema::_APL_EDUCATOR_ADD2,
            Apollo_DB_Schema::_APL_EDUCATOR_CITY,
            Apollo_DB_Schema::_APL_EDUCATOR_STATE,
        );

        $fullAddress = array();
        foreach ($config as $address) {
            $meta = $this->get_meta_data($address, Apollo_DB_Schema::_APL_EDUCATOR_DATA);
            if (!empty($meta)) {
                $fullAddress[] = $meta;
            }
        }

        $zip = $this->get_meta_data(Apollo_DB_Schema::_APL_EDUCATOR_ZIP, Apollo_DB_Schema::_APL_EDUCATOR_DATA);
        $zip = !empty($zip) ? $zip : '';

        return !empty($fullAddress) ? implode(', ', $fullAddress) . ' ' . $zip : false;
    }
}
