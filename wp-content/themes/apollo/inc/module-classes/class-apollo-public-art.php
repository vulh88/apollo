<?php

class Apollo_Public_Art extends Apollo_ABS_Module {

    private $_arr_address = array();

    public $dataStorage;
    private $colections, $locations, $medium;
    
    public function __construct($data, $type) {
        $this->iconField = Apollo_DB_Schema::_APL_PUBLIC_ART_ICONS;
        parent::__construct($data, $type);
    }

    public static function get_user_classifieds($user_id) {
        $apl_query = new Apl_Query( 'posts' );
        return $apl_query->get_where( " post_author = '$user_id' AND post_status = 'publish' AND post_type = '". Apollo_DB_Schema::_CLASSIFIED ."'" );
    }

    public function render_tax_links($cats, $tax, $query_var, $has_link, $is_printer = false, $page = Apollo_DB_Schema::_PUBLIC_ART_PT, $onlyParentTerm = false) {
        $arr_cat = array();
        $arr_cat_ids = $this->get_term_ids( $cats );
        if ( $cats ) {
            foreach( $cats as $c ):
                if ( $c->parent ) continue;

                $parent = $has_link ? '<a class="link" href="'.home_url().'/public-art/?'.$query_var.'='.$c->term_id.'">'.$c->name. '</a>' : $c->name;
                if($is_printer == true){
                    $parent = $c->name;
                }

                $child_terms = $this->get_term_childrens( $c->term_id, $tax );
                if( $child_terms ) {
                    $_arr_childs = array();

                    foreach ( $child_terms as $c ) {
                        $term = get_term( $c, $tax );

                        if ( $term && has_term( $c, $tax, $this->id ) && in_array( $term->term_id , $arr_cat_ids ) )

                        if($is_printer == true){
                            $_arr_childs[] = $term->name;
                        }else{
                            $_arr_childs[] = $has_link ? '<a   class="link" href="'.home_url().'/public-art/?'.$query_var.'='.$term->term_id.'">'.$term->name. '</a>' : $term->name;
                        }
                    }

                    if ( $_arr_childs ) {
                        if ( $has_link ) {

                            if($is_printer == true){
                                $parent .=  ': '.implode( ', ', $_arr_childs );
                            }else{
                                $parent .= '<a>: </a>'. implode( '<a>, </a>', $_arr_childs );
                            }
                        } else {
                            $parent .= ': '. implode( ', ', $_arr_childs );
                        }

                    }

                    $arr_cat[] = $parent;

                    continue;
                }
                $arr_cat[] = $parent;

            endforeach;
        }
        $desl = ';';

        if($is_printer == true){
            $desl = ' - ';
            $delimiter = $has_link ? ''.$desl.' ' : '; ';
            return implode( $delimiter , $arr_cat ) ;

        }else{
            $delimiter = $has_link ? '<a>'.$desl.' </a>' : '; ';
            return implode( $delimiter , $arr_cat ) ;
        }
        return false;

    }
    
    public function getDataDetail(){
        $address = Apollo_App::apollo_get_meta_data($this->id ,Apollo_DB_Schema::_APL_PUBLIC_ART_DATA);
        if(!is_array($address)){
            $address = unserialize($address);
        }
        $markerID = Apollo_App::apollo_get_meta_data($this->id,Apollo_DB_Schema::_APL_PUBLIC_ART_GOOGLE_ICON);
        $markerLink = '';
        if($markerID){
            $markerLink = wp_get_attachment_url( $markerID, 'thumbnail' ) ;
        }

        $data = Apollo_App::apollo_get_meta_data($this->id ,Apollo_DB_Schema::_APL_PUBLIC_ART_DATA);
        $dataAddress = Apollo_App::apollo_get_meta_data($this->id ,Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS);
        $dataAddress = maybe_unserialize($dataAddress);
        $lat = Apollo_App::apollo_get_meta_data($this->id ,Apollo_DB_Schema::_APL_PUBLIC_ART_LATITUDE);
        $long = Apollo_App::apollo_get_meta_data($this->id ,Apollo_DB_Schema::_APL_PUBLIC_ART_LONGITUDE);
        if(!is_array($data)){
            $data = unserialize($data);
        }

        $icon_fields = maybe_unserialize( get_option( Apollo_DB_Schema::_APL_PUBLIC_ART_ICONS ) );
        $icons_in_post = $this->get_meta_data( Apollo_DB_Schema::_APL_PUBLIC_ART_POST_ICONS, Apollo_DB_Schema::_APL_PUBLIC_ART_DATA );

        $returnArray = array(
            '_address' => isset($dataAddress[Apollo_DB_Schema::_PUBLIC_ART_ADDRESS]) ? $dataAddress[Apollo_DB_Schema::_PUBLIC_ART_ADDRESS]:'',
            'address' => $this->get_full_address(),
            'city' =>  isset($dataAddress[Apollo_DB_Schema::_PUBLIC_ART_CITY]) ? $dataAddress[Apollo_DB_Schema::_PUBLIC_ART_CITY]:'',
            'state' =>  isset($dataAddress[Apollo_DB_Schema::_PUBLIC_ART_STATE])?$dataAddress[Apollo_DB_Schema::_PUBLIC_ART_STATE]:'',
            'phone' => isset($data[Apollo_DB_Schema::_PUBLIC_ART_CONTACT_PHONE])?$data[Apollo_DB_Schema::_PUBLIC_ART_CONTACT_PHONE]:'',
            'email' => isset($data[Apollo_DB_Schema::_PUBLIC_ART_CONTACT_EMAIL])?$data[Apollo_DB_Schema::_PUBLIC_ART_CONTACT_EMAIL]:'',
            'web' => isset($data[Apollo_DB_Schema::_PUBLIC_ART_WEBSITE_URL])?$data[Apollo_DB_Schema::_PUBLIC_ART_WEBSITE_URL]:'',
            'icons' => $icons_in_post,
            'icon_fields'   => $icon_fields,
            'add_fields' => $this->getPrintrAdditionFields(),
            'date_created'   => isset($data[Apollo_DB_Schema::_PUBLIC_ART_CREATED_DATE]) ? $data[Apollo_DB_Schema::_PUBLIC_ART_CREATED_DATE]:'',
            'dimension' => isset($data[Apollo_DB_Schema::_PUBLIC_ART_DIMENSION]) ? $data[Apollo_DB_Schema::_PUBLIC_ART_DIMENSION]:'',
            'latitude' => $lat,
            'longitude'   => $long,
            'marker_icon' =>$markerLink
        );

        return $returnArray;
    }
    
    public function get_full_address( $config = array() ) {
        $address = Apollo_App::apollo_get_meta_data($this->id ,Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS);

        if(!is_array($address)){
            $address = maybe_unserialize($address);
        }

        $config = array(
            Apollo_DB_Schema::_PUBLIC_ART_ADDRESS,
            Apollo_DB_Schema::_PUBLIC_ART_CITY,
            Apollo_DB_Schema::_PUBLIC_ART_STATE,
            Apollo_DB_Schema::_PUBLIC_ART_ZIP,
        );


        if ( ! $address ) return false;

        $_add_arr = array();
        foreach ( $config as $c ) {
            if ( isset($address[$c]) && !empty($address[$c]) ) {
                $_add_arr[] = $address[$c] ;
            }
        }

        return $_add_arr ? implode( ', ' , $_add_arr ) : false;
    }

    public function getArtistPublicArt(){
        global $wpdb;
        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ARTIST_PT ) ) return false;
        $sql = "
            SELECT p.* FROM {$wpdb->posts} p
            INNER JOIN {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}} mt_artisrt ON mt_artisrt.meta_key ='". Apollo_DB_Schema::_APL_ARTIST_DISPLAY_PUBLIC ."' AND mt_artisrt.apollo_artist_id = p.ID
             INNER JOIN {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}} mt_lname ON mt_lname.meta_key ='". Apollo_DB_Schema::_APL_ARTIST_LNAME ."' AND mt_lname.apollo_artist_id = p.ID
            WHERE p.post_type = '".Apollo_DB_Schema::_ARTIST_PT."'
            AND p.post_status = 'publish'
            AND mt_artisrt.meta_value = 'yes'
            GROUP BY p.ID
            ORDER BY mt_lname.meta_value ASC
        ";
        return $wpdb->get_results( $sql );
    }

    public function getListArtists(){
        global $wpdb;
        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ARTIST_PT ) ) return false;
        @$artists = implode(',', $this->get_meta_data( Apollo_DB_Schema::_APL_PUBLIC_ART_ARTIST ));

        if($artists){

            $sql = "
            SELECT p.* FROM {$wpdb->posts} p
            INNER JOIN {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}} mt_display ON mt_display.meta_key ='". Apollo_DB_Schema::_APL_ARTIST_DISPLAY_PUBLIC ."' AND mt_display.apollo_artist_id = p.ID
            WHERE p.post_type = '".Apollo_DB_Schema::_ARTIST_PT."'
            AND p.post_status = 'publish'
            AND p.ID IN ( $artists )
            AND mt_display.meta_value = 'yes'
            ORDER BY p.post_title ASC
            ";
            return $wpdb->get_results( $sql );
        }
        return false;
    }

    public function getListImages(){
        $gallery = explode( ',' , $this->get_meta_data( Apollo_DB_Schema::_APL_PUBLIC_ART_IMAGE_GALLERY ) );
        return $gallery;
    }

    public function getListVideo(){
        $videos = @unserialize($this->get_meta_data( Apollo_DB_Schema::_APL_PUBLIC_ART_VIDEO));
        $returnArray = array();
        if(is_array($videos)){
            foreach($videos as $video){
                $item = array(
                    'embed' => isset($video['video_embed'])?$video['video_embed']:'',
                    'desc' => isset($video['video_desc'])?$video['video_desc']:'',
                );
                $returnArray[] = $item;
            }
        }
        return $returnArray;
    }
    

    public function getStrAddress() {
        if(!empty($this->_arr_address)) {
            return $this->_arr_address;
        }

        $meta_key_address = $this->get_meta_data(Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS);

        if(!empty($meta_key_address)) {
            $_arr_address = maybe_unserialize($meta_key_address);

            return implode(", ", array_values($_arr_address));
        }

        return '';
    }

    public function get_mediums() {
        return get_the_terms( $this->id , $this->type.'-medium' );
    }

    public function generate_public_art_mediums( $isPrintPage = false ) {
        $cats = $this->get_mediums();
        if ( ! $cats ) return false;
        return $this->render_tax_links($cats, 'public-art-medium','term', true, $isPrintPage);
    }

    public function get_collections() {
        return get_the_terms( $this->id , $this->type.'-collection' );
    }

    public function generate_public_art_collections( $isPrintPage = false ) {
        $cats = $this->get_collections();
        if ( ! $cats ) return false;
        return $this->render_tax_links($cats, 'public-art-collection','collection', true, $isPrintPage);
    }

    public function get_public_art_locations() {
        return get_the_terms( $this->id , $this->type.'-location' );
    }

    public function generate_public_art_locations( $isPrintPage = false ) {
        $cats = $this->get_public_art_locations();
        if ( ! $cats ) return false;
        return $this->render_tax_links($cats, 'public-art-location','location', true, $isPrintPage);
    }
    
    public function generate_locations($isPrintPage = false){
        $term = Apollo_DB_Schema::_PUBLIC_ART_LOC;
        $cat = get_the_terms( $this->id , $term );
        return $this->render_tax_links($cat, $term, 'term', true, $isPrintPage, $term);
    }
    
    public function generate_colections($isPrintPage = false){
        $term = Apollo_DB_Schema::_PUBLIC_ART_COL;
        $cat = get_the_terms( $this->id , $term );
        return $this->render_tax_links($cat, $term, 'term', true, $isPrintPage, $term);
    }
    
    public static function get_tree_medium($suppressEmpty = false) {
        $terms = get_terms(Apollo_DB_Schema::_PUBLIC_ART_MED, array('hide_empty' => 0, 'orderby' => 'name', 'order' => 'ASC'));
        if ($suppressEmpty && !$terms) return '';
        return self::build_tree_category($terms);
    }
    
    public static function get_tree_location($suppressEmpty = false) {
        $terms = get_terms(Apollo_DB_Schema::_PUBLIC_ART_LOC, array('hide_empty' => 0, 'orderby' => 'name', 'order' => 'ASC'));
        if ($suppressEmpty && !$terms) return '';
        return self::build_tree_category($terms);
    }
    
    public static function get_tree_collection($suppressEmpty = false) {
        $terms = get_terms(Apollo_DB_Schema::_PUBLIC_ART_COL, array('hide_empty' => 0, 'orderby' => 'name', 'order' => 'ASC'));
        if ($suppressEmpty && !$terms) return '';
        return self::build_tree_category($terms);
    }
    
    public static function get_list_cities() {
        global $wpdb;
        $sql = "SELECT DISTINCT (meta_value) FROM {$wpdb->{Apollo_Tables::_APL_PUBLIC_ART_META}} WHERE meta_key = '%s' AND meta_value <> '' ORDER BY meta_value ASC ;";
      
        $result = $wpdb->get_results($wpdb->prepare($sql, Apollo_DB_Schema::_PUBLIC_ART_CITY), ARRAY_A);

        if($result !== null) {
            return $result;
        }
        else {
            return array();
        }
    }
    
    public static function get_list_artnames() {
        global $wpdb;
        $sql = "SELECT DISTINCT (post_title) FROM {$wpdb->posts} WHERE post_type = '%s' AND post_status = '%s' ORDER BY post_title ASC ;";
      
        $result = $wpdb->get_results($wpdb->prepare($sql, Apollo_DB_Schema::_PUBLIC_ART_PT, 'publish'), ARRAY_A);

        if($result !== null) {
            return $result;
        }

        return array();
    }
    
    public static function get_list_zips() {
        global $wpdb;
        $sql = "SELECT DISTINCT (meta_value) FROM {$wpdb->{Apollo_Tables::_APL_PUBLIC_ART_META}} WHERE meta_key = '%s' AND meta_value <> '' ORDER BY meta_value ASC ;";

        $result = $wpdb->get_results($wpdb->prepare($sql, Apollo_DB_Schema::_PUBLIC_ART_ZIP), ARRAY_A);

        if($result !== null) {
            return $result;
        }
        else {
            return array();
        }
    }

    /**
     * Get associated artists
     * @return array
     */
    public function getAssociatedArtist()
    {
        global $wpdb;
        if ( !$this->id || ! Apollo_App::is_avaiable_module(Apollo_DB_Schema::_ARTIST_PT) )
            return false;

        $sql = "
            SELECT p.ID, p.post_title, post_status FROM {$wpdb->posts} p
            INNER JOIN {$wpdb->{Apollo_Tables::_APL_ARTIST_PUBLIC_ART}} as apollo_artist_event 
              ON apollo_artist_event.artist_id = p.ID
            WHERE p.post_type = '".Apollo_DB_Schema::_ARTIST_PT."'
            AND p.post_status = 'publish'
            AND apollo_artist_event.post_id = " . $this->id . "
            ORDER BY p.post_title ASC
            ";
        return $wpdb->get_results($sql);
    }

    /**
     * Get associated ids artists
     * @return array
     */
    public function getAssociatedArtistIDs($artists = false)
    {
        if (!$artists) {
            $artists = $this->getAssociatedArtist();
        }

        $artistIdsEvent = array();
        if($artists) {
            foreach ($artists as $e) {
                $artistIdsEvent[] = $e->ID;
            }
        }
        return $artistIdsEvent;
    }
}