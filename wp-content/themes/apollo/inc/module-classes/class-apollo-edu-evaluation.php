<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of class-apollo-edu-evaluation
 *
 * @author vulh
 */
class Apollo_Edu_Evaluation extends Apollo_ABS_Module {
    
    public function __construct($data, $type) {
        parent::__construct($data, $type);
    }
    
    public static function getDataFromRequest() {
        return array(
            Apollo_DB_Schema::_APL_EDU_EVAL_EDUCATOR => isset( $_POST[Apollo_DB_Schema::_APL_EDU_EVAL_EDUCATOR] ) ? sanitize_text_field($_POST[Apollo_DB_Schema::_APL_EDU_EVAL_EDUCATOR]) : '',
            Apollo_DB_Schema::_APL_EDU_EVAL_PROG => isset( $_POST[Apollo_DB_Schema::_APL_EDU_EVAL_PROG] ) ? sanitize_text_field($_POST[Apollo_DB_Schema::_APL_EDU_EVAL_PROG]) : '',
            'district'  => isset( $_POST['district'] ) ? $_POST['district'] : '',
            'school'  => isset( $_POST['school'] ) ? $_POST['school'] : '',
        );
    }
    
    public static function save($arrData, $arrCFData) {
        $post = array(
            'ID'            => '',
            'post_title'    => __('No title', 'apollo'),
            'post_author'   => get_current_user_id(),
            'post_type'     => Apollo_DB_Schema::_EDU_EVALUATION,
            'post_status'   => 'publish',
        );
        
        // UPDATE POST
        $id = wp_insert_post( $post );
        
        // Insert meta data
        update_apollo_meta($id, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA, Apollo_App::clean_array_data($arrCFData));
        
        update_apollo_meta($id, Apollo_DB_Schema::_APL_EDU_EVAL_EDUCATOR, $arrData[Apollo_DB_Schema::_APL_EDU_EVAL_EDUCATOR]);
        update_apollo_meta($id, Apollo_DB_Schema::_APL_EDU_EVAL_PROG, $arrData[Apollo_DB_Schema::_APL_EDU_EVAL_PROG]);
        
        /**
         * Save terms
         */ 
        $_arr_term = array();
        if ( isset( $arrData['district'] ) && $arrData['district'] ) {
            $_arr_term[] = Apollo_App::clean_data_request($arrData['district']);
        }
        
        if ( isset( $arrData['school'] ) && $arrData['school'] ) {
            $_arr_term[] = Apollo_App::clean_data_request($arrData['school']);
        }
        
        wp_set_post_terms( $id, $_arr_term , 'district-school' );
        
    }
    
    public static function getRule( $key = '' ) {
        $rules = array(
            Apollo_DB_Schema::_APL_EDU_EVAL_EDUCATOR => array('required'),
            Apollo_DB_Schema::_APL_EDU_EVAL_PROG => array('required'),
            'district'  => array('required'),
            'school'  => array('required'),
        );
        
        if($key === '') return $rules;

        $custom_keys = self::get_custom_keys(true, Apollo_DB_Schema::_EDU_EVALUATION);
      
        if ( $custom_keys ) {
            $rules = array_merge( $rules , $custom_keys );
        }
       
        if(isset($rules[$key])) return $rules[$key];

        throw new Exception("Key ". $key .' is not exists!');
    }
    
    public static function isValid($key, $value, $dataHolder = array()) {
        $arrRules = self::getRule($key);
      
        $arrRuleError = array();
        foreach($arrRules as $rule) {
            if ( ! $rule ) continue;
            $param = '';
            
            if(strpos($rule, ':') !== false) {
                list($rule, $param) = explode(":", $rule);
            }
            
            $className = ucfirst($rule);
            $c = new $className($param);

            $isValid = $c->isValid($value, $param, $dataHolder);
            if($isValid === false) {
                $arrRuleError[] = $rule;
            }
        }

        return $arrRuleError;
    }
    
    public static function isValidAll($arrData) {
        foreach($arrData as $key => $value) {
            $arrRuleFail = self::isValid($key, $value, $arrData);
            if(!empty($arrRuleFail))
            {
                return false;
            }
        }

        return true;
    }
    
    public static function getMessageValidate($rule, $label, $type = 'text', $arrExtraParam = array()) {
        $pattern = array(
            'required' => __('Please enter %s', 'apollo'),
            'email'    => __('The %s is invalid', 'apollo'),
            'url'      => __('The %s is invalid', 'apollo'),
            'largerThan' => __('The %s must larger than %s', 'apollo'),
            'youtubeUrl' => __('The %s is not valid. ', 'apollo'),
            'endateLargerThanStartdate' => __('End date must larger than start date', 'apollo'),
        );

        $patternForChoice = array(
            'required' => __('Please choice %s', 'apollo'),
        );


        if(!isset($pattern[$rule])) {
            throw new Exception("Please set message for this rule");
        }

        if($rule === 'largerThan') {
            return sprintf($pattern[$rule], $label, $arrExtraParam['largerThan']['number']);
        }

        if($type === 'choice') {
            $pattern = array_merge($pattern, $patternForChoice);
        }

        return sprintf($pattern[$rule], $label);

    }
    
    public function get_school() {
        $terms = get_the_terms($this->id, 'district-school');
        $term = false;
        if ( $terms ) {
            foreach( $terms as $term ) {
                if ( $term->parent ) return $term;
            }
        }
        return false;
    }
    
    public function get_district() {
        $terms = get_the_terms($this->id, 'district-school');
        $term = false;
        if ( $terms ) {
            foreach( $terms as $term ) {
                if ( ! $term->parent ) return $term;
            }
        }
        return false;
    }
}
