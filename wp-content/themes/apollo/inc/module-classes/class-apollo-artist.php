<?php

class Apollo_Artist extends Apollo_ABS_Module
{

    private $artist_image_gallery = '';

    private $get_personal_data = array();

    private $_arr_address = array();

    public $dataStorage;

    public function __construct($data, $type)
    {
        $this->iconField = Apollo_DB_Schema::_APL_ARTISTS_ICONS;
        parent::__construct($data, $type);
    }

    public function getData($key = '')
    {

        if (!$this->post) return false;

        if (empty($this->dataStorage)) {
            $arrBunchData = Apollo_App::unserialize(get_apollo_meta($this->id, Apollo_DB_Schema::_APL_ARTIST_DATA, true));
            $arrBunchAddress = Apollo_App::unserialize(get_apollo_meta($this->id, Apollo_DB_Schema::_APL_ARTIST_ADDRESS, true));
            $arrBunchQuestion = Apollo_App::unserialize(get_apollo_meta($this->id, Apollo_DB_Schema::_APL_ARTIST_QUESTIONS, true));

            $this->dataStorage = array_merge(array(
                'id' => $this->id,
                'post_title' => $this->post->post_title,
                'post_content' => $this->post->post_content,
                'post_status' => $this->post->post_status,
                'post_cat' => $this->get_term_ids($this->get_categories()),
                'post_style' => $this->get_term_ids($this->get_styles()),
                'post_medium' => $this->get_term_ids($this->get_mediums()),
                'questions' => $arrBunchQuestion,
            ),
                array_merge($arrBunchData, $arrBunchAddress)
            );
        }

        if ($key === '') { // get all
            return $this->dataStorage;
        }

        if (isset($this->dataStorage[$key])) {
            return $this->dataStorage[$key];
        }

        throw new Exception("Key " . $key . ' is not exists!');
    }

    public function is_public()
    {
        $display = get_apollo_meta($this->id, Apollo_DB_Schema::_APL_ARTIST_DISPLAY_PUBLIC, true);
        if ($display && $display == 'yes') {
            return true;
        } else {
            return false;
        }
    }

    public function is_linked()
    {
        $not_linked = get_apollo_meta($this->id, Apollo_DB_Schema::_APL_ARTIST_DO_NOT_LINKED, true);
        if ($not_linked && $not_linked == 'yes') {
            return false;
        } else {
            return true;
        }
    }

    /**
     * get_gallery_attachment_ids function.
     *
     * @access public
     * @return array
     */
    public function get_gallery_attachment_ids()
    {
        if (!isset($this->artist_image_gallery)) {
            // Backwards compat
            $attachment_ids = get_posts('post_parent=' . $this->id . '&numberposts=-1&post_type=attachment&orderby=menu_order&order=ASC&post_mime_type=image&fields=ids&meta_key=_apollo_artist_exclude_image&meta_value=0');
            $attachment_ids = array_diff($attachment_ids, array(get_post_thumbnail_id()));
            $this->artist_image_gallery = implode(',', $attachment_ids);
        }
        return apply_filters('apollo_artist_gallery_attachment_ids', array_filter((array)explode(',', $this->artist_image_gallery)), $this);
    }

    public function get_career_info()
    {
        $arr_key = array(
            __('Phone', 'apollo') => Apollo_DB_Schema::_APL_ARTIST_PHONE,
            __('Email', 'apollo') => Apollo_DB_Schema::_APL_ARTIST_EMAIL,
            __('Blog Url', 'apollo') => Apollo_DB_Schema::_APL_ARTIST_BLOGURL,
        );

        $arr_info = array();

        foreach ($arr_key as $key => $field) {
            $val = $this->get_personal_data($field);
            if ($val) {
                $arr_info[] = $key . ': ' . $val;
            }
        }

        return implode("<br/>", $arr_info);
    }

    public function get_personal_data($field = '_apl_artist_lname')
    {

        if (isset($this->get_personal_data[$field])) {
            return $this->get_personal_data[$field];
        }

        $arr_artist_data = unserialize(get_apollo_meta($this->id, Apollo_DB_Schema::_APL_ARTIST_DATA, true));

        if (isset($arr_artist_data[$field])) {
            return $this->get_personal_data[$field] = $arr_artist_data[$field];
        }

        return '';

    }

    /**
     * Get the title
     * @param bool $arr_artist_data
     * @return string
     */
    public function get_title($arr_artist_data = false)
    {
        if (!$arr_artist_data) {
            $arr_artist_data = Apollo_App::unserialize(get_apollo_meta($this->id, Apollo_DB_Schema::_APL_ARTIST_DATA, true));
        }

        return $this->title_filter( (isset($arr_artist_data['_apl_artist_fname']) ? $arr_artist_data['_apl_artist_fname'] : '') . ' ' . (isset($arr_artist_data['_apl_artist_lname']) ? $arr_artist_data['_apl_artist_lname'] : ''));
    }

    public static function get_list_artist_city()
    {
        global $wpdb;
        $sql = "SELECT DISTINCT (meta_value) FROM {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}} WHERE meta_key = '%s' AND meta_value <> '' ORDER BY meta_value ASC ;";

        $result = $wpdb->get_results($wpdb->prepare($sql, Apollo_DB_Schema::_APL_ARTIST_CITY), ARRAY_A);

        if ($result !== null) {
            return $result;
        } else {
            return array();
        }
    }

    public static function get_list_artist_zipcode()
    {
        global $wpdb;
        $sql = "SELECT DISTINCT (meta_value) FROM {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}} WHERE meta_key = '%s' AND meta_value <> '' ORDER BY meta_value ASC ;";

        $result = $wpdb->get_results($wpdb->prepare($sql, Apollo_DB_Schema::_APL_ARTIST_ZIP), ARRAY_A);

        if ($result !== null) {
            return $result;
        } else {
            return array();
        }
    }

    public static function get_list_artist_medium()
    {
        return get_terms(Apollo_DB_Schema::_ARTIST_MEDIUM_TAX);
    }

    public static function get_tree_artist_medium()
    {
        return self::build_tree_category(get_terms(Apollo_DB_Schema::_ARTIST_MEDIUM_TAX, array('hide_empty' => true, 'orderby' => 'name', 'order' => 'ASC')));
    }

    public static function get_list_artist_type()
    {
        return get_terms(Apollo_DB_Schema::_ARTIST_TYPE);
    }

    public static function get_tree_artist_type()
    {
        return self::build_tree_category(get_terms(Apollo_DB_Schema::_ARTIST_TYPE, array('hide_empty' => true, 'orderby' => 'name', 'order' => 'ASC')));
    }

    public static function get_list_artist_style()
    {
        return get_terms(Apollo_DB_Schema::_ARTIST_STYLE_TAX);
    }

    public static function get_tree_artist_style()
    {
        return self::build_tree_category(get_terms(Apollo_DB_Schema::_ARTIST_STYLE_TAX, array('hide_empty' => true, 'orderby' => 'name', 'order' => 'ASC')));
    }


    public function get_full_address($config = array())
    {

        $config = array(
            Apollo_DB_Schema::_APL_ARTIST_STREET,
            Apollo_DB_Schema::_APL_ARTIST_CITY,
            Apollo_DB_Schema::_APL_ARTIST_STATE,
            Apollo_DB_Schema::_APL_ARTIST_ZIP,
        );

        $address = @unserialize($this->get_meta_data(Apollo_DB_Schema::_APL_ARTIST_ADDRESS));

        if (!$address) return false;

        $_add_arr = array();
        foreach ($config as $c) {
            if ($v = $this->get_meta_data($c, Apollo_DB_Schema::_APL_ARTIST_ADDRESS)) {
                $_add_arr[] = $v;
            }
        }

        return $_add_arr ? implode(', ', $_add_arr) : false;
    }

    public function getStrAddress()
    {
        if (!empty($this->_arr_address)) {
            return $this->_arr_address;
        }

        $meta_key_address = $this->get_meta_data(Apollo_DB_Schema::_APL_ARTIST_ADDRESS);

        if (!empty($meta_key_address)) {
            $_arr_address = maybe_unserialize($meta_key_address);

            return implode(", ", array_values($_arr_address));
        }

        return '';
    }

    /**
     * Return the module styles
     *
     * @access public
     * @return array array terms
     *
     */
    public function get_styles()
    {
        return get_the_terms($this->id, $this->type . '-style');
    }

    /**
     * Return the module styles
     *
     * @access public
     * @return array array terms
     *
     */
    public function get_mediums()
    {
        return get_the_terms($this->id, $this->type . '-medium');
    }


    public function get_term_ids($terms)
    {
        $output = array();
        if ($terms):
            foreach ($terms as $c) {
                $output[] = $c->term_id;
            }
        endif;
        return $output;
    }

    public function generate_type_string($arr_type, $delimiter = ' - ')
    {
        if (!$arr_type) {
            return false;
        }

        $arr_cat = array();

        foreach ($arr_type as $c):
            $arr_cat[] = is_object($c) ? $c->name : $c;
        endforeach;

        return implode($delimiter, $arr_cat);

    }

    public function generate_artist_categories($has_link = false)
    {
        $cats = $this->get_categories();

        $other_cat = $this->get_meta_data(Apollo_DB_Schema::_APL_ARTIST_ANOTHER_CAT, Apollo_DB_Schema::_APL_ARTIST_DATA);

        if (!$cats) return false;
        return $this->render_tax_links($cats, 'artist-type', $other_cat, 'term', $has_link);
    }

    public function generate_artist_styles($has_link = false)
    {
        $cats = $this->get_styles();

        $other_cat = $this->get_meta_data(Apollo_DB_Schema::_APL_ARTIST_ANOTHER_STYLE, Apollo_DB_Schema::_APL_ARTIST_DATA);

        if (!$cats) return false;
        return $this->render_tax_links($cats, 'artist-style', $other_cat, 'artist_style', $has_link);
    }

    public function generate_artist_mediums($has_link = false)
    {
        $cats = $this->get_mediums();

        $other_cat = $this->get_meta_data(Apollo_DB_Schema::_APL_ARTIST_ANOTHER_MEDIUM, Apollo_DB_Schema::_APL_ARTIST_DATA);

        if (!$cats) return false;
        return $this->render_tax_links($cats, 'artist-medium', $other_cat, 'artist_medium', $has_link);
    }

    public function render_tax_links($cats, $tax, $other_cat, $query_var, $has_link = false, $is_printer = false, $page = Apollo_DB_Schema::_ARTIST_PT)
    {
        $arr_cat = array();
        $arr_cat_ids = $this->get_term_ids($cats);
        $modName = Apollo_App::getCustomUrlByModuleName(Apollo_DB_Schema::_ARTIST_PT);
        if ($cats) {
            foreach ($cats as $c):
                if ($c->parent) continue;
                $parent = $has_link ? '<a class="link" href="' . $modName . '/?' . $query_var . '=' . $c->term_id . '">' . $c->name . '</a>' : $c->name;

                $child_terms = $this->get_term_childrens($c->term_id, $tax);
                if ($child_terms) {
                    $_arr_childs = array();

                    foreach ($child_terms as $c) {
                        $term = get_term($c, $tax);

                        if ($term && has_term($c, $tax, $this->id) && in_array($term->term_id, $arr_cat_ids))
                            $_arr_childs[] = $has_link ? '<a   class="link" href="' . $modName . '/?' . $query_var . '=' . $term->term_id . '">' . $term->name . '</a>' : $term->name;
                    }

                    if ($_arr_childs) {
                        if ($has_link) {
                            $parent .= '<a>: </a>' . implode('<a>, </a>', $_arr_childs);
                        } else {
                            $parent .= ': ' . implode(', ', $_arr_childs);
                        }

                    }

                    $arr_cat[] = $parent;

                    continue;
                }
                $arr_cat[] = $parent;

            endforeach;
        }

        if ($other_cat) $arr_cat[] = $has_link ? '<a  class="link" href="' . home_url() . '/'.$modName.'/?keyword=' . $other_cat . '">' . $other_cat . '</a>' : $other_cat;
        $delimiter = $has_link ? '<a>; </a>' : '; ';
        return implode($delimiter, $arr_cat);
    }

    /**
     * Get associated artists
     * @param bool $limit
     * @param int $page
     * @return array
     */
    public function getAssociatedEvents($limit = false, $page = 1, $pageSize = 0)
    {
        global $wpdb;
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.ID, p.post_title, post_status 
            FROM {$wpdb->posts} p
            INNER JOIN {$wpdb->{Apollo_Tables::_APL_ARTIST_EVENT}} as apollo_artist_event 
              ON apollo_artist_event.event_id = p.ID
            WHERE p.post_type = '".Apollo_DB_Schema::_EVENT_PT."'
            AND (p.post_status = 'publish' OR p.post_status = 'pending')
            AND apollo_artist_event.artist_id = " . $this->id . "
            ORDER BY p.post_title ASC
            ";

        if ($limit) {
            if(!$pageSize) {
                $pageSize = Apollo_Display_Config::DASHBOARD_ARTIST_EVENTS_PAGESIZE;
            }
            if ($page >= 2) {
                $_offset = $pageSize * ($page - 1);
            } else {
                $_offset = 0;
            }
            $sql .= " limit $pageSize offset $_offset";
        }

        $artists = $wpdb->get_results($sql);

        if(!$limit) {
            return $artists;
        } else {
            $count_result = $wpdb->get_var("SELECT FOUND_ROWS()");
            return array(
                'data' => $artists,
                'total' => $count_result,
                'have_more' => count($artists) + (($page - 1) * $pageSize) < $count_result,
            );
        }
    }


    /**
     * Assign event to artist
     * @ticket #14485
     * @param $event_id
     */
    public function assignEvent($event_id){
        $apl_query  = new Apl_Query( Apollo_Tables::_APL_ARTIST_EVENT );
        if(!$apl_query->get_row( " event_id = {$event_id} and artist_id = {$this->id} ", 'id')) {
            $apl_query->insert( array(
                'artist_id' => $this->id,
                'event_id'  => $event_id,
                'artist_ordering' => 0,
            ));
        }
    }

    /**
     * Remove event list associated with artist
     * @ticket #14485
     * @param $event_ids
     */
    public function unAssignEvent($event_ids){
        $apl_query  = new Apl_Query( Apollo_Tables::_APL_ARTIST_EVENT );
        $apl_query->delete( " event_id in($event_ids) and artist_id = {$this->id} ");
    }

    /**
     * Get upcoming/past events
     * @param int $page
     * @param bool $isUpComingEvents
     * @param bool $userID
     * @return array
     */
    public function getAssociatedUpPastEvents($page = 1, $isUpComingEvents = true, $userID = false)
    {

        global $wpdb;
        $current_date = current_time( 'Y-m-d' );
        $apollo_meta_table = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};


        $pageSize = Apollo_Display_Config::PAGESIZE_UPCOM  ;
        $_offset = ($page-1) * $pageSize;
        $limit = "LIMIT $_offset, $pageSize";


        $arr_order = Apollo_Sort_Manager::getSortOneFieldForPostType('event');


        $artistEventTbl = $wpdb->{Apollo_Tables::_APL_ARTIST_EVENT};
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.ID, p.post_title, p.post_content, p.post_name, p.post_type, p.post_excerpt, p.post_author, p.post_status FROM {$wpdb->posts} p
            INNER JOIN {$artistEventTbl} av ON p.ID = av.event_id AND av.artist_id = {$this->id}
            INNER JOIN {$apollo_meta_table} mt_start_d ON p.ID = mt_start_d.apollo_event_id AND mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
            INNER JOIN {$apollo_meta_table} mt_end_d ON p.ID   = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
        ";

        if(
            $arr_order['type_sort'] === 'apollo_meta'
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE )
        { // sort by meta key
            $sql .= " INNER JOIN {$apollo_meta_table} tblSortMeta ON tblSortMeta.apollo_event_id = p.ID AND tblSortMeta.meta_key = '{$arr_order["metakey_name"]}'";
        }

        $where_status = "p.post_status = 'publish'";


        if(get_current_user_id() == $userID) {
            $where_status .= " or p.post_status = 'pending'";
        }

        $sql .= " WHERE ( $where_status ) AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."' ";

        if ($isUpComingEvents) {
            $sql .=   " AND (
                        ( mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
                        AND CAST( mt_start_d.meta_value AS DATE ) >= '{$current_date}' )
                    OR
                        ( mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
                        AND CAST( mt_end_d.meta_value AS DATE ) >= '{$current_date}' )
                    ) ";
        }
        else {
            $sql .=   " AND 
                    ( mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
                        AND CAST( mt_end_d.meta_value AS DATE ) < '{$current_date}' )";
        }

        $sql = Apollo_App::hidePrivateEvent($sql,'p');
        $sql .= " GROUP BY p.ID ";


        if($arr_order['type_sort'] === 'apollo_meta') {
            if(
                $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
                && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE
            ) {
                $sql .= " ORDER BY tblSortMeta.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_START_DATE) {
                $sql .= " ORDER BY mt_start_d.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_END_DATE) {
                $sql .= " ORDER BY mt_end_d.meta_value {$arr_order['order']} ";
            }
        }
        else if($arr_order['type_sort'] === 'post') {
            $sql .= " ORDER BY p.{$arr_order['order_by']} {$arr_order['order']} ";
        }


        $sql .= " $limit ";

        $result = $wpdb->get_results($sql);
        $total = $wpdb->get_var("SELECT FOUND_ROWS()");

        return array(
            'data'  => $result,
            'total' => $total,
            'have_more' => Apollo_App::hasMore($page, $pageSize, $total)
        );
    }
}
