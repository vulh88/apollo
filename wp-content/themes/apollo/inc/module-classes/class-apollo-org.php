<?php

class Apollo_Org extends Apollo_ABS_Module {

    private $artist_image_gallery = '';

    public function __construct($data, $type) {
        $this->iconField = Apollo_DB_Schema::_APL_ORG_ICONS;
        parent::__construct($data, $type);
    }

    public function get_gallery_attachment_ids() {
        if ( ! isset( $this->artist_image_gallery ) ) {
            // Backwards compat
            $attachment_ids = get_posts( 'post_parent=' . $this->id . '&numberposts=-1&post_type=attachment&orderby=menu_order&order=ASC&post_mime_type=image&fields=ids&meta_key=_apollo_artist_exclude_image&meta_value=0' );
            $attachment_ids = array_diff( $attachment_ids, array( get_post_thumbnail_id() ) );
            $this->artist_image_gallery = implode( ',', $attachment_ids );
        }
        return apply_filters( 'apollo_artist_gallery_attachment_ids', array_filter( (array) explode( ',', $this->artist_image_gallery ) ), $this );
    }

    public function get_title() {
        return parent::get_title();
    }

    public function get_styles() {
        return get_the_terms( $this->id , $this->type.'-style' );
    }

    public function get_mediums() {
        return get_the_terms( $this->id , $this->type.'-medium' );
    }



    public function getOrgData(){
        $address = Apollo_App::apollo_get_meta_data($this->id ,Apollo_DB_Schema::_APL_ORG_ADDRESS);
        if(!is_array($address)){
            $address = unserialize($address);
        }

        if (Apollo_App::enableMappingRegionZipSelection() && isset($address[Apollo_DB_Schema::_ORG_REGION])) {
            unset($address[Apollo_DB_Schema::_ORG_REGION]);
        }

        $data = Apollo_App::apollo_get_meta_data($this->id ,Apollo_DB_Schema::_APL_ORG_DATA);
        if(!is_array($data)){
            $data = unserialize($data);
        }

        /** @Ticket #13517 */


        $icon_fields = maybe_unserialize( get_option( Apollo_DB_Schema::_APL_ORG_ICONS ) );
        $icons_in_post = $this->get_meta_data( Apollo_DB_Schema::_APL_ORG_POST_ICONS, Apollo_DB_Schema::_APL_ORG_DATA );
        
        $returnArray = array(
            'address' => $this->get_full_address(),
            'phone' => isset($data[Apollo_DB_Schema::_ORG_PHONE])?$data[Apollo_DB_Schema::_ORG_PHONE]:'',
            'fax' => isset($data[Apollo_DB_Schema::_ORG_FAX])?$data[Apollo_DB_Schema::_ORG_FAX]:'',
            'email' => isset($data[Apollo_DB_Schema::_ORG_EMAIL])?$data[Apollo_DB_Schema::_ORG_EMAIL]:'',
            'web' => isset($data[Apollo_DB_Schema::_ORG_WEBSITE_URL])?$data[Apollo_DB_Schema::_ORG_WEBSITE_URL]:'',
            'blog' => isset($data[Apollo_DB_Schema::_ORG_BLOG_URL])?$data[Apollo_DB_Schema::_ORG_BLOG_URL]:'',
            'facebook' => isset($data[Apollo_DB_Schema::_ORG_FACEBOOK_URL])?$data[Apollo_DB_Schema::_ORG_FACEBOOK_URL]:'',
            'twitter' => isset($data[Apollo_DB_Schema::_ORG_TWITTER_URL])?$data[Apollo_DB_Schema::_ORG_TWITTER_URL]:'',
            'inst' => isset($data[Apollo_DB_Schema::_ORG_INSTAGRAM_URL])?$data[Apollo_DB_Schema::_ORG_INSTAGRAM_URL]:'',
            'linked' => isset($data[Apollo_DB_Schema::_ORG_LINKED_IN_URL])?$data[Apollo_DB_Schema::_ORG_LINKED_IN_URL]:'',
            'donate' => isset($data[Apollo_DB_Schema::_ORG_DONATE_URL])?$data[Apollo_DB_Schema::_ORG_DONATE_URL]:'',
            'icon_fields' => $icon_fields,
            'icons' => $icons_in_post,
            'pinterest' =>  isset($data[Apollo_DB_Schema::_ORG_PINTEREST_URL])?$data[Apollo_DB_Schema::_ORG_PINTEREST_URL]:'',
            'add_fields' => $this->getPrintrAdditionFields(),
            Apollo_DB_Schema::_ORG_DISCOUNT_URL => isset($data[Apollo_DB_Schema::_ORG_DISCOUNT_URL]) ? $data[Apollo_DB_Schema::_ORG_DISCOUNT_URL] : '',
            Apollo_DB_Schema::_ORG_BUSINESS => isset($data[Apollo_DB_Schema::_ORG_BUSINESS]) && $data[Apollo_DB_Schema::_ORG_BUSINESS] == 'yes',
            Apollo_DB_Schema::_APL_ORG_BUSINESS => Apollo_App::apollo_get_meta_data($this->id,Apollo_DB_Schema::_APL_ORG_BUSINESS)
        );

        return $returnArray;
    }

    public function getListImages(){
        $gallery = explode( ',' , $this->get_meta_data( Apollo_DB_Schema::_APL_ORG_IMAGE_GALLERY ) );
        return $gallery;
    }

    public function getListVideo(){
        $videos = @unserialize($this->get_meta_data( Apollo_DB_Schema::_APL_ORG_VIDEO));
        $returnArray = array();
        if(is_array($videos)){
            foreach($videos as $video){
                $item = array(
                    'embed' => isset($video['video_embed'])?$video['video_embed']:'',
                    'desc' => isset($video['video_desc'])?$video['video_desc']:'',
                );
                $returnArray[] = $item;
            }
        }
        return $returnArray;
    }

    public function getUpcommingEvent($query = '>=',$and = 'OR',$offset = true){

        $org_id = $this->id;
        if(empty($this->pageSize)) {
            $this->pageSize = of_get_option(Apollo_DB_Schema::_NUM_VIEW_MORE,Apollo_Display_Config::PAGESIZE_UPCOM);
        }
        if ( ! Apollo_App::is_avaiable_module( 'organization' ) ) {
            return false;
        }
        global $wpdb, $post;
        $apollo_meta_table   = $wpdb->apollo_eventmeta;
        $_start_date_field  = Apollo_DB_Schema::_APOLLO_EVENT_START_DATE;
        $_end_date_field    = Apollo_DB_Schema::_APOLLO_EVENT_END_DATE;
        $_current_date      = date( 'Y-m-d' );
        $_offset = $offset ? ($this->page-1) * $this->pageSize. ', ' : '';
        $arr_order = Apollo_Sort_Manager::getSortOneFieldForPostType('event');
        $sql = "
            SELECT SQL_CALC_FOUND_ROWS p.* FROM {$wpdb->posts} p
            INNER JOIN {$apollo_meta_table} mt_start_d ON p.ID = mt_start_d.apollo_event_id AND mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
            INNER JOIN {$apollo_meta_table} mt_end_d ON p.ID   = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
            INNER JOIN {$wpdb->{Apollo_Tables::_APL_EVENT_ORG}} e_org ON e_org.post_id = p.ID AND e_org." . Apollo_DB_Schema::_APL_E_ORG_ID . " = '$org_id'
        ";
        if(
            $arr_order['type_sort'] === 'apollo_meta'
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
            && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE )
        { // sort by meta key
            $sql .= " INNER JOIN {$apollo_meta_table} tblSortMeta ON tblSortMeta.apollo_event_id = p.ID AND tblSortMeta.meta_key = '{$arr_order["metakey_name"]}'";
        }
        $sql .=   " WHERE 1=1 AND p.post_status = 'publish' AND p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."' AND (
                        ( mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
                        AND CAST( mt_start_d.meta_value AS DATE )  ".$query." '{$_current_date}' )
                    ".$and."
                        ( mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
                        AND CAST( mt_end_d.meta_value AS DATE ) ".$query." '{$_current_date}' )
                    ) ";
        $sql .= " GROUP BY p.ID ";
        if($arr_order['type_sort'] === 'apollo_meta') {
            if(
                $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
                && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE
            ) {
                $sql .= " ORDER BY tblSortMeta.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_START_DATE) {
                $sql .= " ORDER BY mt_start_d.meta_value {$arr_order['order']} ";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_END_DATE) {
                $sql .= " ORDER BY mt_end_d.meta_value {$arr_order['order']} ";
            }
        }
        else if($arr_order['type_sort'] === 'post') {
            $sql .= " ORDER BY p.{$arr_order['order_by']} {$arr_order['order']} ";
        }
        $sql .= " LIMIT $_offset $this->pageSize ";
        $this->events       = $wpdb->get_results($sql);
        $this->totalPost    = $wpdb->get_var("SELECT FOUND_ROWS()");
        $this->_template = APOLLO_PARENT_DIR.'/templates/events/_org-events.php';
        return $this->render_html($this->_template);
    }

    public function render_html( $template = '' ) {
        ob_start();
        include $template ? $template : $this->_template;
        return ob_get_clean();
    }

    public function getPastEvent($offset = false){
        return $this->getUpcommingEvent('<','AND',$offset);
    }

    /**
     * @param array $config
     * @param bool $isSecondaryAddress: @ticket #18672
     * @return bool|string
     */
    public function get_full_address($config = array(), $isSecondaryAddress = false) {

        $address = $isSecondaryAddress ? Apollo_App::apollo_get_meta_data($this->id ,Apollo_DB_Schema::_APL_SECONDARY_ORG_ADDRESS)
            :Apollo_App::apollo_get_meta_data($this->id ,Apollo_DB_Schema::_APL_ORG_ADDRESS);

        $zip = '';

        if(!is_array($address)){
            $address = maybe_unserialize($address);
        }

        if (empty($config)) {
            $config = array(
                Apollo_DB_Schema::_ORG_ADDRESS1,
                Apollo_DB_Schema::_ORG_ADDRESS2,
                Apollo_DB_Schema::_ORG_CITY,
                Apollo_DB_Schema::_ORG_STATE,
                Apollo_DB_Schema::_ORG_ZIP,
            );
        }

        if ( ! $address ) return false;

        $_add_arr = array();
        foreach ( $config as $c ) {
            if ( isset($address[$c]) && !empty( $address[$c] ) ) {
                if ( $c == Apollo_DB_Schema::_ORG_ZIP || $c == Apollo_DB_Schema::_SECONDARY_ORG_ZIP) {
                    $zip = $address[$c];
                    continue;
                }
                $_add_arr[] = $address[$c] ;
            }
        }

        return $_add_arr ? implode( ', ' , $_add_arr ).' '.$zip : false;
    }

    public static function get_list_org_address($metaKey =  Apollo_DB_Schema::_ORG_ZIP, $table = Apollo_Tables::_APL_ORG_META,$parrent = Apollo_DB_Schema::_APL_ORG_ADDRESS) {
        global $wpdb;
        $arrayZip = array();
        $sql = "SELECT DISTINCT (meta_value) FROM {$wpdb->{$table}} WHERE meta_key = '%s' AND meta_value <> '' ORDER BY meta_value ASC ;";
        $results = $wpdb->get_results($wpdb->prepare($sql, $parrent), ARRAY_A);
        if($results !== null) {
            $i = 1;
            foreach($results as $result){
                $meta = maybe_unserialize($result['meta_value']);
                //array
                if(is_array($meta)){
                    if(isset($meta[$metaKey]) && !empty($meta[$metaKey]))
                    {
                        if(!in_array($meta[$metaKey],$arrayZip))
                            $arrayZip[$i] = $meta[$metaKey];
                    }
                }
                //serialize string
                elseif(is_string($meta)){
                    $meta = maybe_unserialize($meta);
                    if(isset($meta[$metaKey]) && !empty($meta[$metaKey]) ){
                        if(!in_array($meta[$metaKey],$arrayZip))
                            $arrayZip[$i] = $meta[$metaKey];
                    }
                }
                $i++;
            }
            return $arrayZip;
        }
        else {
            return array();
        }
    }

    public  static function getBusinessType($business_id){
        global $wpdb;
        $term_categories_table = $wpdb->prefix.''.Apollo_Tables::_APOLLO_TERM_RELATIONSHIPS;
        $sql = " SELECT term_id FROM ".$wpdb->{Apollo_Tables::_APOLLO_TERM_TAXONOMY}." WHERE  term_taxonomy_id
                IN ( SELECT  term_taxonomy_id FROM {$term_categories_table}  WHERE object_id = '".$business_id."' )";
        $result = $wpdb->get_row($sql);

        if(empty($result))
            return false;
        return $result->term_id;
    }
}
