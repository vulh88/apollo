<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * This action's obj will effect all system
 * + We will use session from here. Because this is large system. So start session is necessary.
 * Class Apollo_Filter
 */
class Apollo_Action {
    public function __construct() {

        $arr_action = array(
            'init' => array(__CLASS__, 'apollo_init'),
            'wp_logout' => array(__CLASS__, 'apollo_destruct'),

            /* Administrator */
            'edit_user_profile_update' => array($this, 'apollo_edit_user_profile_update'),
            'personal_options_update' => array($this, 'apollo_personal_options_update'),
            'personal_options' => array($this, 'personal_options'),

            /* Site Options */
            'wpmu_options' => array(__CLASS__, 'apollo_add_site_options'),
            'update_wpmu_options' => array(__CLASS__, 'apollo_update_wpmu_options'),

            /* user functions */
            'user_new_form' => array(__CLASS__, 'apollo_hidden_add_existing_user'),

            /* activate page */
            'activate_header' => array(__CLASS__, 'apollo_header_activate_page'),

            /* Log comment */
            'wp_insert_comment' => array(__CLASS__, 'apollo_wp_insert_comment'),

            'wp_head' => array(__CLASS__, 'fbHomeSharingInfoInjection'),
            
        );

        foreach($arr_action as $action_name => $handler) {
            add_action($action_name, $handler, 10, 2);
        }

        /** @Ticket #16107 */
        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        if(is_plugin_active('woocommerce/woocommerce.php')){
            /** @Ticket #16013 */
            require_once APOLLO_INCLUDES_DIR . '/Lib/WooCommerce/WooCommerceCreateAccount.php';
            $aplWoo = new APL_Lib_WooCommerce_Create_Account();
            add_action( 'woocommerce_created_customer', array($aplWoo, 'wooCommerceCustomCreateAccount'), 10, 3);
        }


        if(is_plugin_active('ninja-forms/ninja-forms.php')){
            add_filter( 'ninja_forms_render_options', array($this, 'aplCustomRenderOptionNinjaForm'), 10, 2);
            add_filter( 'ninja_forms_merge_tag_value_listselect', array($this, 'aplCustomMessageSendMail'), 10, 2);
            add_filter( 'ninja_forms_submission_actions', array($this, 'aplCustomEmailTo'), 10, 3);

            /*@ticket #17157 */
            add_filter('ninja_forms_subs_export_field_value_listselect', array($this, 'aplCustomExportFieldValue'), 10, 2);
        }
    }

    /**
     * @ticket #17157: [CF] 20150815 - Display Educator and Program name in the CSV file export.
     * @param $field_value
     * @param $field
     * @return string
     */
    public function aplCustomExportFieldValue($field_value, $field){

        if ($field->get_settings()['key'] == 'apl_ninja_select_educator'
            || $field->get_settings()['key'] == 'apl_ninja_select_program'){

            $title = $field_value ? html_entity_decode(get_the_title($field_value)) : '';

            return $title;
        }else{
            if(isset($field->get_settings()['options'])){
                $listOptions = $field->get_settings()['options'];
                foreach($listOptions as $option){
                    if(isset($option['value']) && $option['value'] === $field_value){
                        return $option['label'];
                    }
                }
            }

            return $field_value;
        }
    }


    /**
     * @Ticket #17080 - Custom two fields Education/Program id to name.
     * @param $value
     * @param $field
     * @return string
     */
    public function aplCustomMessageSendMail($value, $field) {
        if ($field['key'] == 'apl_ninja_select_educator'
            || $field['key'] == 'apl_ninja_select_program') {
            if (!empty($field['settings']['value'])) {
                return get_the_title(intval($field['settings']['value']));
            }
            return '';
        }
        return $value;
    }

    /**
     * @Ticket #17110 - Add Program email to recipient list.
     * @param $actions
     * @param $formCache
     * @param $formData
     * @return mixed
     */
    public function aplCustomEmailTo($actions, $formCache, $formData) {
        if (!empty($actions)) {
            foreach ($actions as $key => $action) {
                if (!empty($action['settings']['type']) && $action['settings']['type'] == 'email') {
                    $fields = Ninja_Forms()->form($formData['id'])->get_fields();
                    if ($fields) {
                        foreach ($fields as $field) {
                            $settings = $field->get_settings();
                            if ($settings['key'] == 'apl_ninja_select_program') {
                                if (!empty($formData['fields'][$field->get_id()]['value'])) {
                                    $programData = maybe_unserialize(get_apollo_meta($formData['fields'][$field->get_id()]['value'], Apollo_DB_Schema::_APL_PROGRAM_DATA, true));
                                    if (!empty($programData[Apollo_DB_Schema::_APL_PROGRAM_EMAIL])) {
                                        $actions[$key]['settings']['to'] = $programData[Apollo_DB_Schema::_APL_PROGRAM_EMAIL]. ',' . $action['settings']['to'];
                                    }
                                }
                                break;
                            }
                        }
                    }
                    break;
                }
            }
        }
        return $actions;
    }

    public static  function apollo_init() {
        
        ob_start();
        
        self::checkHelperExists();
        self::apollo_start_session();
        self::add_tables();

        /* all action at frontend only */
        if(!is_admin() || (defined('DOING_AJAX') && DOING_AJAX === true)) {
            /**/
            Apollo_Log_System::registerLogActivity();
            Apollo_Log_System::registerLogVisitEvent();
        }
        
        self::update_data();
    }

    public function aplCustomRenderOptionNinjaForm($options,$settings){
        if( $settings['key'] == 'apl_ninja_select_educator' ){

            $params = array(
                'post_type'         => Apollo_DB_Schema::_EDUCATOR_PT,
                'post_status'       => 'publish',
                'posts_per_page'    => -1,
                'order'             => 'ASC',
                'orderby'           => 'post_title'
            );
            /** Get all Educators */
            $listEdu = query_posts( $params );
            wp_reset_query();
            if (empty($options)) {
                if (is_admin()) {
                    $options[] = array('label' => __('Select Cultural Organization'), 'value' => 'ninja-form-admin-custom-educator-element');
                } else {
                    $options[] = array('label' => __('Select Cultural Organization'), 'value' => '');
                }
            }
            if ($listEdu) {
                foreach ($listEdu as $eduItem) {
                    $option = array('label' => $eduItem->post_title, 'value' => $eduItem->ID);
                    if (isset($_GET['edu']) && ($_GET['edu'] == $eduItem->post_name || $_GET['edu'] == $eduItem->ID)) {
                        $option['selected'] = true;
                    }
                    $options[] = $option;
                }
            }

        }
        if ( $settings['key'] == 'apl_ninja_select_program') {
            if (empty($options)) {
                if (is_admin()) {
                    $options[] = array('label' => __('Select Program'), 'value' => 'ninja-form-admin-custom-program-element');
                } else {
                    $options[] = array('label' => __('Select Program'), 'value' => '');
                }
            }
            if (is_admin() && isset($_GET['post'])) {
                $formData = Ninja_Forms()->form()->get_sub($_GET['post']);
                $eduValue = $formData->get_field_value('apl_ninja_select_educator');
            } else {
                $eduValue = !empty($_GET['edu']) ? $_GET['edu'] : '';
            }
            if (!empty($eduValue)) {

                if (intval($eduValue)) {
                    $educator = get_post($eduValue);
                } else {
                    $args = array(
                        'name' => $eduValue,
                        'post_type' => Apollo_DB_Schema::_EDUCATOR_PT,
                        'post_status' => 'publish',
                        'posts_per_page' => 1
                    );
                    $results = get_posts($args);
                    if (isset($results[0])) {
                        $educator = $results[0];
                    }
                }
                if (!empty($educator)) {
                    $edu = get_educator( $educator );
                    $listProgram = $edu->get_programs_does_not_expired();
                    if ($listProgram) {
                        foreach ($listProgram as $item) {
                            $pro = get_post( $item->prog_id );

                            if ( ! $pro || ($pro && $pro->post_status != 'publish') ) continue;
                            $proOption = array('label' => $pro->post_title, 'value' => $pro->ID);
                            $options[] = $proOption;
                        }
                    }
                }
            }
        }
        return $options;
    }

    /**
     * Facebook home page sharing injection
     *
     */
    public static function fbHomeSharingInfoInjection()
    {
        if (!is_front_page()) return false;
        $logo = of_get_option(Apollo_DB_Schema::_FB_SHARING_LOGO);
        if ($logo) {
            echo '
            <meta property="og:type" content="website" />
            <meta property="og:url" content="'.site_url().'" />
            <meta property="og:image" content="'.$logo.'"/>';
        }
    }

    public static function checkHelperExists()
    {
        if(!file_exists(WPMU_PLUGIN_DIR.'/apollo-helper.php')) {
            $path = WPMU_PLUGIN_DIR;
            wp_die(__("The Apollo Helper file don't exists. Please add apollo-helper.php file to {$path} directory"));
        }
    }

    public static function apollo_start_session() {
        if(!session_id()) { // be polite
            @session_start();
        }
    }

    public static function apollo_destruct(){
        self::apollo_destroy_session();
    }

    public static function apollo_destroy_session()
    {
        session_destroy();
    }

    /* Save blog info */
    public function apollo_personal_options_update($user_id) {
        $this->_save_blog_id_meta($user_id);
    }


    public function apollo_edit_user_profile_update($user_id) {
        $this->_save_blog_id_meta($user_id);
    }

    public function personal_options()
    {
        $this->_hide_role();
    }

    private function _hide_role() {
        ?>
        <script>
            jQuery(function() {
                jQuery("#role option[value='artist_manager']").hide();
                jQuery("#role option[value='venue_manager']").hide();
                jQuery("#role option[value='organization_manager']").hide();
                jQuery("#role option[value='event_manager']").hide();
            });

        </script>
    <?php
    }

    private function _save_blog_id_meta($user_id) {
        global $wpdb;

        $arr_blog = get_blogs_of_user($user_id);
        $blogUserTbl = Apollo_App::getBlogUserTable();
        foreach ($arr_blog as $arrb) {
            $wpdb->insert($blogUserTbl, array(
                    'blog_id' => $arrb->userblog_id,
                    'user_id' => $user_id,
                ));
        }
    }

    public static function apollo_add_site_options()
    {

        $arr_email_config = include_once __DIR__.'/emails/templates/default-template.php';
        ?>
        <h3><?php _e('Apollo Site Settings'); ?></h3>
        <table class="form-table">

            <tr>
                <th scope="row"><label for="_apollo_reset_user_pass"><?php _e( 'Reset User Pass' ) ?></label></th>
                <td>
                    <textarea name="_apollo_reset_user_pass" id="_apollo_reset_user_pass" rows="5" cols="45" class="large-text"><?php echo esc_textarea( get_site_option( '_apollo_reset_user_pass', $arr_email_config['forgot_pass']) ) ?></textarea>
                    <p class="description">
                        <?php _e( 'The reset user\'s pass sent to users who forgot their password.' ) ?>
                    </p>
                </td>
            </tr>

            <tr class="form-table">
                <th scope="row"><label for="_apollo_network_footer"><?php _e( 'Network footer' ); ?></label></th>
                <td>
                    <?php
                    $editor_settings = array(
                        'textarea_name' => '_apollo_network_footer',
                        'media_buttons' => true,
                        'editor_height' => 150,
                    );
                    wp_editor( get_site_option( '_apollo_network_footer'), '_apollo_network_footer', $editor_settings );
                    ?>
                </td>
            </tr>

        </table>
        <?php
    }

    public static function apollo_update_wpmu_options()
    {
        $options = array(
            '_apollo_reset_user_pass',
            '_apollo_network_footer'
        );

        foreach ( $options as $option_name ) {
            if ( ! isset($_POST[$option_name]) )
                continue;
            $value = wp_unslash( $_POST[$option_name] );
            update_site_option( $option_name, $value );
        }
    }
    
    public static function add_tables() {
        
        global $wpdb;

        /* Network table */
        $tables = Apollo_Tables::networkTables();
        foreach($tables as $k => $v) {
            $wpdb->global_tables[] = $v;
            $wpdb->{$v} = $wpdb->base_prefix . $v;
        }

        /* Blog table */
        $tables = Apollo_Tables::blogTables();
        foreach($tables as $k => $v) {
            $wpdb->tables[] = $v;
            $wpdb->{$v} = $wpdb->prefix . $v;
        }

        /* Optimize */
        $table = Apollo_Tables::_POST_TERM;
        $wpdb->tables[] = $table;
        $wpdb->{$table} = $wpdb->prefix . $table;
       
        /* Event org table */
        $org_tbl            = Apollo_Tables::_APL_EVENT_ORG;
        $wpdb->tables[]     = $org_tbl;
        $wpdb->{$org_tbl}   = $wpdb->prefix. $org_tbl;

        /* Artist Public Art table */
        $art_pub_art_tbl            = Apollo_Tables::_APL_ARTIST_PUBLIC_ART;
        $wpdb->tables[]     = $art_pub_art_tbl;
        $wpdb->{$art_pub_art_tbl}   = $wpdb->prefix. $art_pub_art_tbl;

        /* Artist Event table */
        $artist_event_tbl           = Apollo_Tables::_APL_ARTIST_EVENT;
        $wpdb->tables[]             = $artist_event_tbl;
        $wpdb->{$artist_event_tbl}  = $wpdb->prefix. $artist_event_tbl;

        /* Agency table */
        $agency_tbl            = Apollo_Tables::_APL_AGENCY;
        $wpdb->tables[]     = $agency_tbl;
        $wpdb->{$agency_tbl}   = $wpdb->prefix. $agency_tbl;
        
        /* Program Educator table */
        $pro_edu_tbl = Apollo_Tables::_APL_PROGRAM_EDUCATOR;
        $wpdb->tables[] = $pro_edu_tbl;
        $wpdb->{$pro_edu_tbl} = $wpdb->prefix. $pro_edu_tbl;
        
        /* Agency Artist table */
        $agency_artist_tbl = Apollo_Tables::_APL_AGENCY_ARTIST;
        $wpdb->tables[] = $agency_artist_tbl;
        $wpdb->{$agency_artist_tbl} = $wpdb->prefix. $agency_artist_tbl;

        /* Agency Educator table */
        $agency_educator_tbl = Apollo_Tables::_APL_AGENCY_EDUCATOR;
        $wpdb->tables[] = $agency_educator_tbl;
        $wpdb->{$agency_educator_tbl} = $wpdb->prefix. $agency_educator_tbl;
        
        /* Agency Org table */
        $agency_org_tbl = Apollo_Tables::_APL_AGENCY_ORG;
        $wpdb->tables[] = $agency_org_tbl;
        $wpdb->{$agency_org_tbl} = $wpdb->prefix. $agency_org_tbl;
        
        /* Agency Venue table */
        $agency_venue_tbl = Apollo_Tables::_APL_AGENCY_VENUE;
        $wpdb->tables[] = $agency_venue_tbl;
        $wpdb->{$agency_venue_tbl} = $wpdb->prefix. $agency_venue_tbl;
        
        $custom_field_tbl = Apollo_Tables::_APL_CUSTOM_FIELD;
        $wpdb->tables[] = $custom_field_tbl;
        $wpdb->{$custom_field_tbl} = $wpdb->prefix. $custom_field_tbl;
        
        $apl_theme_tool = Apollo_Tables::_APL_THEME_TOOL;
        $wpdb->tables[] = $apl_theme_tool;
        $wpdb->{$apl_theme_tool} = $wpdb->prefix. $apl_theme_tool;

        $apl_state_city = Apollo_Tables::_APL_STATE_ZIP;
        $wpdb->tables[] = $apl_state_city;
        $wpdb->{$apl_state_city} = $wpdb->prefix. $apl_state_city;

        $importEvent = Apollo_Tables::_APL_EVENT_IMPORT;
        $wpdb->tables[] = $importEvent;
        $wpdb->{$importEvent} = $wpdb->prefix. $importEvent;
        
        $meta_data_tbls = array(
            Apollo_Tables::_APOLLO_ARTIST_META,
            Apollo_Tables::_APL_PROGRAM_META,
            Apollo_Tables::_APL_EDUCATOR_META,
            Apollo_Tables::_APL_USER_MODULES
        );
        foreach ( $meta_data_tbls as $meta_tbl ) {
            $wpdb->tables[] = $meta_tbl;
            $wpdb->{$meta_tbl} = $wpdb->prefix. $meta_tbl;
        }
          
    }

    public static function apollo_hidden_add_existing_user()
    {
        // Hiden create existing user form another blog
        ?>
        <script>
            var $h3_eu = jQuery('#add-existing-user');
            jQuery($h3_eu.next().next()).hide();
            jQuery($h3_eu.next()).hide();
            jQuery($h3_eu).hide();
        </script>
        <?php
    }

    public static function apollo_header_activate_page()
    {
        get_template_part('templates/head');
    }

    public static function apollo_wp_insert_comment($id, $comment)
    {
        $mainitem_id = $comment->comment_post_ID;
        $post = get_post($mainitem_id);

        $dataForLog =  array(
            'user_id' => get_current_blog_id(),
            'item_id' => $id,
            'activity' => Apollo_Activity_System::COMMENT,
            'item_type' => 'comment',
            'url' => 'none',
            'title' => 'none', /* Comment don't have title */
            'timestamp' => time(),
            'exdata' => array(
                'main_type' => $post->post_type,
                'main_id' => $comment->comment_post_ID,
                'main_url' => get_permalink($mainitem_id),
                'main_title' => $post->post_title,
                'content' => $comment->comment_content,
            )
        );

        Apollo_Event_System::trigger(Apollo_Event_System::ACTIVIY, $dataForLog);
    }
    
    public static function update_data() {
        if ( isset($_REQUEST['updated_ver']) && ! get_option('updated_ver') && 1==2 ) {
            /**
             * Backup option keys
             */ 
            $old_opts = array(
                Apollo_DB_Schema::_APL_ARTIST_CUSTOM_FIELDS  => array( 
                    'post_type'     => Apollo_DB_Schema::_ARTIST_PT,
                    'parent_name'   => __( 'More Info', 'apollo' ),
                ),
                Apollo_DB_Schema::_APL_PROGRAM_CUSTOM_FIELDS  => array( 
                    'post_type'     => Apollo_DB_Schema::_PROGRAM_PT,
                    'parent_name'   => __( 'More Info', 'apollo' ),
                ),
                Apollo_DB_Schema::_APL_EDUCATOR_CUSTOM_FIELDS  => array( 
                    'post_type'     => Apollo_DB_Schema::_EDUCATOR_PT,
                    'parent_name'   => __( 'Educator Qualifications', 'apollo' ),
                ),
                Apollo_DB_Schema::_APL_CLASSIFIED_CUSTOM_FIELDS  => array(
                    'post_type'     => Apollo_DB_Schema::_CLASSIFIED,
                    'parent_name'   => __( 'More Info', 'apollo' ),
                ),
                Apollo_DB_Schema::_APL_PUBLIC_ART_CUSTOM_FIELDS  => array(
                    'post_type'     => Apollo_DB_Schema::_PUBLIC_ART_PT,
                    'parent_name'   => __( 'More Info', 'apollo' ),
                ),
             
            );
            $apl_query = new Apl_Query( Apollo_Tables::_APL_CUSTOM_FIELD );
            $order_parent = 1;
            foreach( $old_opts as $op => $data ) {
                $op_val = maybe_unserialize( get_option($op) );
                
                $apl_query->insert( array(
                    'label' => $data['parent_name'],
                    'cf_order'  => $order_parent,
                    'post_type' => $data['post_type'],
                ) );

                $parent_id = $apl_query->get_bottom();
                if ( ! $parent_id ) {
                    throw new Exception('parent not exist');
                }
                
                
                if ( $op_val ) {
                    $order = 1;
                    foreach( $op_val as $key => $field_data ) {
                       
                        $key = isset( $field_data['key'] ) && $field_data['key'] ? $field_data['key'] : $key;

                        if ( isset( $field_data['type'] ) && $field_data['type'] ) {
                            $type = $field_data['type'];
                        } else {
                            $arr_key = explode( '_' , $key);
                            $type = isset( $arr_key[0] ) && $arr_key[0] ? $arr_key[0] : 'text';
                        }

                        $apl_query->insert( array(
                            'name'      => $key,
                            'label'     => $field_data['label'],
                            'cf_order'  => $order,
                            'cf_type'   => $type,
                            'location'  => maybe_serialize(explode( '_' , $field_data['location'] ) ),
                            'parent'    => $parent_id,
                            'required'  => $field_data['require'] == 'yes' ? 1 : 0,
                            'post_type' => $data['post_type'],
                            'meta_data' => $type == 'radio' ? maybe_serialize( array('choice'=> 'yes:Yes
no:No') ) : '',
                        ) );
                        $order++;
                    }
                }
                $order_parent++;
            }

            // End * Backup option keys
            
            /**
            * Backup old meta data key to new meta data
            */
           $old_meta_data = array(
               Apollo_DB_Schema::_APL_ARTIST_DATA  => array( 
                   'post_type'     => Apollo_DB_Schema::_ARTIST_PT,
                   'meta_table'    => Apollo_Tables::_APOLLO_ARTIST_META
               ),
             
               Apollo_DB_Schema::_APL_PROGRAM_DATA  => array( 
                   'post_type'     => Apollo_DB_Schema::_PROGRAM_PT,
                   'meta_table'    => Apollo_Tables::_APL_PROGRAM_META
               ),
               Apollo_DB_Schema::_APL_EDUCATOR_DATA  => array( 
                   'post_type'     => Apollo_DB_Schema::_EDUCATOR_PT,
                   'meta_table'    => Apollo_Tables::_APL_EDUCATOR_META
               ),
               Apollo_DB_Schema::_APL_EDUCATOR_CUSTOM_FIELD_SEARCH  => array( 
                   'post_type'     => Apollo_DB_Schema::_EDUCATOR_PT,
                   'meta_table'    => Apollo_Tables::_APL_EDUCATOR_META,
                   'meta_key_update' => Apollo_DB_Schema::_APL_POST_TYPE_CF_SEARCH,   
               ),
           );

           global $wpdb;
           foreach( $old_meta_data as $meta_key => $data ) {
               $post_type = $data['post_type'];
               $meta_tbl = $wpdb->{$data['meta_table']};

               $results = $wpdb->get_results("
                   SELECT * FROM {$meta_tbl} pm
                   INNER JOIN {$wpdb->posts} p ON p.ID = pm.apollo_".$post_type."_id
                   WHERE p.post_type = '$post_type' AND pm.meta_key = '".$meta_key."'   
               ");

               if ( $results ) {
                   foreach ( $results as $result ) {
                        $id = 'apollo_'.$post_type.'_id';
                        $meta_value = $result->meta_value;
                        
                        $wpdb->insert( $meta_tbl, array(
                            $id             => $result->{$id},
                            'meta_key'      => isset( $data['meta_key_update'] ) ? $data['meta_key_update'] : Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA,
                            'meta_value'    => $meta_value,
                        ) );
                   }
               }
               
           }
           // End * Backup old meta data key to new meta data


           /**
            * Update _APL_EDUCATOR_CUSTOM_FIELD_SEARCH of educator
            */ 
            $results = $wpdb->get_results("
                SELECT * FROM {$wpdb->{Apollo_Tables::_APL_EDUCATOR_META}} pm
                INNER JOIN {$wpdb->posts} p ON p.ID = pm.apollo_educator_id
                WHERE p.post_type = '".Apollo_DB_Schema::_EDUCATOR_PT."' AND pm.meta_key = '".Apollo_DB_Schema::_APL_EDUCATOR_CUSTOM_FIELD_SEARCH."'   
            ");
            if ( $results ) {
                foreach ( $results as $result ) {
                    // Update new meta data    
                    $wpdb->insert( $meta_tbl, array(
                        'apollo_educator_id' => $result->apollo_educator_id,
                        'meta_key'           => Apollo_DB_Schema::_APL_POST_TYPE_CF_SEARCH,
                        'meta_value'           => $result->meta_value,
                    ) );
                }
            } // End * Update _APL_EDUCATOR_CUSTOM_FIELD_SEARCH of educator
           
            
            update_option( 'updated_ver' , 1, 'no');
            
        } // End * isset($_REQUEST['updated_ver'])
        
        
        
    }
}

new Apollo_Action();

