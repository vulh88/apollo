<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/25/2015
 * Time: 5:59 PM
 */
class Apollo_Form_Static {
    //form message here
    const _FAIL_MESSAGE = 'Cannot save data. Some error occurred!';
    const _SUCCESS_MESSAGE = 'Update successful!'; // for update
    const _SUCCESS_ADD_NEW_MESSAGE = 'Added successful!'; // for update
    const _SUCCESS_INSERT_NEW_MESSAGE = 'Change profile info successfully!'; //for insert new
    
    const _SUCCESS_ADD_CLASSIFIED_MESSAGE = 'Add Classified successful!'; // for insert classified
    const _FAIL_ADD_CLASSIFIED_MESSAGE = 'Add Classified fail!'; // for insert classified

    public static function renderElement($element_item,$validateClassObject=null,$formRequestMethod=array()){

        $type = isset($element_item['type'])?$element_item['type']:'';
        $name = isset($element_item['name'])?$element_item['name']:'';
        $placeHolder = isset($element_item['place_holder'])?$element_item['place_holder']:'';
        $value =  isset($element_item['value'])?$element_item['value']:'';
        $class =  isset($element_item['class'])?$element_item['class']:'';
        $title = isset($element_item['title'])?$element_item['title']:'';
        $id = isset($element_item['id'])?$element_item['id']:'';
        $data = isset($element_item['data'])?$element_item['data']:'';
        $dataAttributes = isset($element_item['data_attributes']) ? $element_item['data_attributes'] : array();
        $display = isset($element_item['display'])?$element_item['display']:true;
        $func = $type;

        // get nonce action and nonce name
        $nonceAction = isset($element_item['nonce_action']) ? $element_item['nonce_action'] : '';
        $nonceName   = isset($element_item['nonce_name'])   ? $element_item['nonce_name']   : '';

        if(!$display) return;
        if(!empty($func)){
            $conatainerClass =   isset($element_item['container'])?$element_item['container']:'el-blk full';
            $element = new $func($name,$placeHolder,$value,$class,$title,$id,$data,'get',$dataAttributes);
            switch($type){
                case 'Category_Group' :
                    $postType = isset($element_item['post_type'])?$element_item['post_type']:'post';
                    $element->setPostType($postType);
                    $element->setValidateClass($validateClassObject);
                    $tax = isset($element_item['tax'])?$element_item['tax']:'';
                    $element->setTax($tax);
                break;
                case 'Access_Group':
                    $postType = isset($element_item['post_type'])?$element_item['post_type']:'post';
                    $element->setPostType($postType);
                    $element->setValidateClass($validateClassObject);
                    break;
                case 'Loop':
                    $children = isset($element_item['children'])?$element_item['children']:'';
                    $controlButton = isset($element_item['control_button'])?$element_item['control_button']:'';
                    $element->setChildItem($children);
                    $element->setControlButton($controlButton);
                    break;
                case 'Tab':
                    $children = isset($element_item['children'])?$element_item['children']:'';
                    $element->setChildItem($children);
                    break;
                case 'Button':
                    $noContainer = isset($element_item['no_container'])?$element_item['no_container']:false;
                    $element->setNocontainer($noContainer);
                    break;
                case 'Submit':
                    $noContainer = isset($element_item['no_container'])?$element_item['no_container']:false;
                    $element->setNocontainer($noContainer);
                    break;
                case 'ShortCode':
                    $target = isset($element_item['target'])?$element_item['target']:'';
                    $element->setTarget($target);
                    break;
                case 'Nonce':
                    $element->setENonceAction(esc_attr(Apollo_App::clean_data_request($nonceAction)));
                    $element->setENonceName(esc_attr(Apollo_App::clean_data_request($nonceName)));
                    $conatainerClass = '';
                    break;
                case 'Wysiwyg':
                    /**
                     * @ticket #18431 0002504: Arts Education Customizations - Remove a restriction that requires the end date to be longer than the start date and modify toolbar editor on FE Program form - item 1,2
                     */
                    $editorSetting = isset($element_item['editor_settings'])   ? $element_item['editor_settings']   : '';
                    $element->setEditorSetting($editorSetting);
                    break;
                case 'Div':
                case 'Hidden':
                case 'CloseDiv':
                    $conatainerClass = '';
                    break;
            }


            if(isset($element_item['validate']['rule']) && $element_item['validate']['rule']){
                $element->setEValidate(true);
                $element->setEValidKey($name);
                $element->setEValidValue($value);
                if($validateClassObject != null){
                    $element->setValidateClass($validateClassObject);
                }
            }
            if($func == 'Loop'){
                if(isset($element_item['children']) && is_array($element_item['children'])){
                    $childrenItem = $element_item['children'];
                    foreach($childrenItem as $k => $childItemArray){
                        foreach($childItemArray as $key=>$childItem){
                            if (isset($childItem['validate']['rule']) && $childItem['validate']['rule']) {
                                $element->setValidateClass($validateClassObject);
                            }
                        }
                   }
                }
            }

            if($func == 'Option' || $func == 'Additional_fields' ){
                $element->setValidateClass($validateClassObject);
            }
            if($func == 'Access_Group' ){
                $groupName = isset($element_item['group_name'])?$element_item['group_name']:'';
                if(!empty($groupName)){
                    $element->setGroupName($groupName);
                }
            }
            if($func == 'Title'){
                $conatainerClass = '';
            }

            if(!empty($conatainerClass)){
                echo '<div class="'.$conatainerClass.'">';
            }
            $element->setERequestedMethod($formRequestMethod);
            $element->render();
            if(!empty($conatainerClass)) {
                echo '</div>';
            }
        }
    }
    public static function renderCroup($childItem,$validateClassObject = null, $requestMethod = 'get'){
        $i = 0;
        foreach($childItem as $k => $childGroups){
            self::renderItemInGroup($childGroups,$i,$validateClassObject,$requestMethod);
            $i++;
        }
    }
    public static function renderItemInGroup($childGroups,$pos = 0,$validateClassObject = null,$requestMethod = 'get'){
        echo '<div class="video-item group-item" id="video-group'.$pos.'">';
        echo '<div class="divider"></div>';
        foreach($childGroups as $key => $item){
            Apollo_Form_Static::renderElement($item,$validateClassObject,$requestMethod);
        }
        echo '<div class="delVideo delGroup">
                <i class="fa fa-times"></i>
            </div>';
        echo '</div>';
    }
    public static function AddMoreButtonLabel($data,$key = 'embed'){
        $name = __('ADD VIDEO LINK','apollo');
        if(is_array($data)){
            if(isset($data[$key])){
                $name = __('ADD MORE LINKS','apollo');
            }
        }
        return $name;
    }
}