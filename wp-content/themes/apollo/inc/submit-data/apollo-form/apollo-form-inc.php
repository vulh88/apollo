<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/6/2015
 * Time: 5:52 PM
 */

define('_APL_INPUT_ERROR','inp-error');

include_once('class-apollo-form-static.php');
include_once('class-apollo-form.php');
//org form
/** @Ticket #15971 */
include_once(APOLLO_INCLUDES_DIR. '/src/org/forms/class-apollo-organization-base-form.php');
/** @Ticket #13470 */
include_once(APOLLO_INCLUDES_DIR. '/src/org/forms/class-apollo-organization-form.php');
include_once(APOLLO_INCLUDES_DIR. '/src/org/forms/class-apollo-organization-photo-form.php');
include_once(APOLLO_INCLUDES_DIR. '/src/org/forms/class-apollo-organization-video-form.php');
include_once(APOLLO_INCLUDES_DIR. '/src/org/forms/class-apollo-organization-audio-form.php');
//educator form
/** @Ticket #13482 */
include_once(APOLLO_INCLUDES_DIR. '/src/educator/forms/class-apollo-educator-form.php');
//program form
include_once(APOLLO_INCLUDES_DIR. '/src/program/forms/class-apollo-program-form.php');
//artist form
/** @Ticket #13364 */
include_once(APOLLO_INCLUDES_DIR. '/src/artist/forms/class-apollo-artist-base-form.php');
include_once(APOLLO_INCLUDES_DIR. '/src/artist/forms/class-apollo-artist-audio-form.php');
include_once(APOLLO_INCLUDES_DIR. '/src/artist/forms/class-apollo-artist-video-form.php');
include_once(APOLLO_INCLUDES_DIR. '/src/artist/forms/class-apollo-artist-form.php');
include_once(APOLLO_INCLUDES_DIR. '/src/artist/forms/class-apollo-artist-photo-form.php');
include_once(APOLLO_INCLUDES_DIR. '/src/artist/forms/class-apollo-artist-event-form.php');

//venue
/** @Ticket #15971 */
include_once(APOLLO_INCLUDES_DIR. '/src/venue/forms/class-apollo-venue-base-form.php');
/** @Ticket #13448 */
include_once( APOLLO_INCLUDES_DIR. '/src/venue/forms/class-apollo-venue-form.php');
include_once( APOLLO_INCLUDES_DIR. '/src/venue/forms/class-apollo-venue-photo-form.php');
include_once( APOLLO_INCLUDES_DIR. '/src/venue/forms/class-apollo-venue-video-form.php');


//classified form
include_once('apollo-forms/classified/class-apollo-classified-form.php');
//element
include_once('apollo-form-elements/apollo-class-form-element.php');
include_once('apollo-form-elements/check-box.php');
include_once('apollo-form-elements/onoff-switch-check-box.php');
include_once('apollo-form-elements/check-box-multi.php');
include_once('apollo-form-elements/radio.php');
include_once('apollo-form-elements/text-area.php');
include_once('apollo-form-elements/wysiwyg.php');
include_once('apollo-form-elements/text-area-audio.php');
include_once('apollo-form-elements/div.php');
include_once('apollo-form-elements/access-group.php');



include_once('apollo-form-elements/nonce.php');
include_once('apollo-form-elements/text.php');
include_once('apollo-form-elements/button.php');
include_once('apollo-form-elements/apollo-class-child-elements/button/submit.php');
include_once('apollo-form-elements/apollo-class-child-elements/button/reset.php');

include_once('apollo-form-elements/select.php');
include_once('apollo-form-elements/option.php');
include_once('apollo-form-elements/additional-fields.php');
include_once('apollo-form-elements/classified-additional-fields.php');
include_once('apollo-form-elements/title.php');
include_once('apollo-form-elements/sub-title.php');
include_once('apollo-form-elements/category-group.php');
include_once('apollo-form-elements/document-group.php');
include_once('apollo-form-elements/apollo-class-child-elements/text/hidden.php');
include_once('apollo-form-elements/apollo-class-child-elements/text/youtube-url.php');
include_once('apollo-form-elements/apl-form-icon.php');

include_once('apollo-form-elements/tab.php');
include_once('apollo-form-elements/loop.php');
include_once('apollo-form-elements/short-code.php');
