<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/6/2015
 * Time: 5:24 PM
 */
abstract class Apollo_Form{
    const _FORM_REQUIRED = 'required';
    const _FORM_EXISTS = 'exists';
    const _FORM_EMAIL = 'email';
    const _FORM_URL = 'url';
    const _FORM_YOUTUBE_LINK = 'youtubeUrl';
    const _FORM_EMBED = 'embed';
	const _FORM_NUMBER = 'Number';
	const _FORM_DATE_LARGER_NOW = 'dateLargerNow';

    protected $formName;
    protected $formMethod;
    protected $formAction;
    protected $elements;
    protected $formId;
    protected $formRequestMethod;
    protected $formData;
    protected $isSaveSuccess;
    protected $isSubmitFail;
    protected $optionValidateClass;
    protected $currentPage;
    protected $validateClassObject;
    protected $headerContent;
    protected $footerContent;
    protected  $successMessage;
    protected  $failMessage;
    protected $formDescription;
    protected  $id;
    protected $activatedTab;

    protected $nonceAction = '';
    protected $nonceName   = '';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * @return mixed
     */
    public function getFormDescription()
    {
        return $this->formDescription;
    }

    /**
     * @param mixed $formDescription
     */
    public function setFormDescription($formDescription)
    {
        $this->formDescription = $formDescription;
    }



    /**
     * @return string
     */
    public function getFormName()
    {
        return $this->formName;
    }

    /**
     * @param string $formName
     */
    public function setFormName($formName)
    {
        $this->formName = $formName;
    }

    /**
     * @return string
     */
    public function getFormMethod()
    {
        return $this->formMethod;
    }

    /**
     * @param string $formMethod
     */
    public function setFormMethod($formMethod)
    {
        $this->formMethod = $formMethod;
    }

    /**
     * @return string
     */
    public function getFormAction()
    {
        return $this->formAction;
    }

    /**
     * @param string $formAction
     */
    public function setFormAction($formAction)
    {
        $this->formAction = $formAction;
    }

    /**
     * @return mixed
     */
    public function getElements()
    {
        return $this->elements;
    }

    /**
     * @param mixed $elements
     */
    public function setElements($elements)
    {
        $this->elements = $elements;
    }

    /**
     * @return string
     */
    public function getFormId()
    {
        return $this->formId;
    }

    /**
     * @param string $formId
     */
    public function setFormId($formId)
    {
        $this->formId = $formId;
    }

    /**
     * @return mixed
     */
    public function getIsSubmitFail()
    {
        return $this->isSubmitFail;
    }

    /**
     * @param mixed $isSubmitFail
     */
    public function setIsSubmitFail($isSubmitFail)
    {
        $this->isSubmitFail = $isSubmitFail;
    }

    /**
     * @return mixed
     */
    public function getIsSaveSuccess()
    {
        return $this->isSaveSuccess;
    }

    /**
     * @param mixed $isSaveSuccess
     */
    public function setIsSaveSuccess($isSaveSuccess)
    {
        $this->isSaveSuccess = $isSaveSuccess;
    }

    /**
     * @return string
     */
    public function getFormRequestMethod()
    {
        return $this->formRequestMethod;
    }

    /**
     * @param string $formRequestMethod
     */
    public function setFormRequestMethod($formRequestMethod)
    {
        $this->formRequestMethod = $formRequestMethod;
    }

    /**
     * @return mixed
     */
    public function getFormData()
    {
        return $this->formData;
    }

    /**
     * @param mixed $formData
     */
    public function setFormData($formData)
    {
        $this->formData = $formData;
    }

    /**
     * @return mixed
     */
    public function getOptionValidateClass()
    {
        return $this->optionValidateClass;
    }

    /**
     * @param mixed $optionValidateClass
     */
    public function setOptionValidateClass($optionValidateClass)
    {
        $this->optionValidateClass = $optionValidateClass;
    }

    /**
     * @return mixed
     */
    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    /**
     * @return mixed
     */
    public function getValidateClassObject()
    {
        return $this->validateClassObject;
    }

    /**
     * @param mixed $validateClassObject
     */
    public function setValidateClassObject($validateClassObject)
    {
        $this->validateClassObject = $validateClassObject;
    }

    /**
     * @return mixed
     */
    public function getHeaderContent()
    {
        return $this->headerContent;
    }

    /**
     * @param mixed $headerContent
     */
    public function setHeaderContent($headerContent)
    {
        $this->headerContent = $headerContent;
    }

    /**
     * @return mixed
     */
    public function getFooterContent()
    {
        return $this->footerContent;
    }

    /**
     * @param mixed $footerContent
     */
    public function setFooterContent($footerContent)
    {
        $this->footerContent = $footerContent;
    }

    /**
     * @param mixed $currentPage
     */



    public function setCurrentPage($currentPage)
    {
        $this->currentPage = $currentPage;
    }



    public function __construct($formId = '',$fName = '',$fMethod = '',$fAction = '', $formElement = array(),$requestMethod = ''){
        $this->formName = $fName;
        $this->formMethod = $fMethod;
        $this->formAction = $fAction;
        $this->elements = $formElement;
        $this->formId = $formId;
        $this->formRequestMethod =strtolower($requestMethod);
        $this->successMessage = __(Apollo_Form_Static::_SUCCESS_MESSAGE,'apollo');
        $this->failMessage = __(Apollo_Form_Static::_FAIL_MESSAGE,'apollo');

        // Handle submit permission
        add_action('apollo_submit_form', array($this, 'handleSubmitPermission'), 10);

    }
    //form header

    /**
     * Handle submit form permission
     * @ticket #15847
     * @return mixed
     */
    public function handleSubmitPermission() {
        if (!$this->authorize()) {
            $permissionDeniesPage = get_page_by_path(Apollo_Page_Creator::ID_PERMISSION_DENIES);

            if (!empty($permissionDeniesPage)){
                $permissionDeniesPage = Apollo_Page_Creator::getSlugByPageID(Apollo_Page_Creator::ID_PERMISSION_DENIES);
                wp_redirect(site_url() . '/' . $permissionDeniesPage);
            }

            exit();
        }
    }

    protected function formHeader(){
        
        $this->formAction = str_replace('?warning', '', $this->formAction);
        /* Thienld : if any form has override function of 'renderHorizontalTab' then it's rendered as well */
        if(Apollo_App::isInputMultiplePostMode()){
            echo $this->renderHorizontalTab();
        }
        echo '  <form enctype="multipart/form-data" id="'.$this->formId.'" method="'.$this->formMethod.'" action="'.$this->formAction.'" name="'.$this->formName.'" >';
        echo '<div class="">';

    }
    //form footer
    protected function formFooter(){
        echo '</div>';
        echo '</form>';
    }
    public function  renderForm(){

        // ticket #10927: set nonce info before init form
        $this->setNonceInfo();

        //get form data again not validate
        $this->getMethod();
        if($_POST)
            $this->formData = Apollo_App::clean_array_data($_POST);

        $this->formInit();

        // ticket #10927: add nonce field to form
        $this->elements = array_merge($this->getNonceField(), $this->elements);

        $this->validateClassObject = new $this->optionValidateClass($this->getRule());
        $this->validateClassObject->setRules($this->getRule());
        $this->formSubmitProcess();
        echo $this->formDescription;
        //render header for this form
        
        // vulh hack code to init form data again, this code should be optimized
//        if($_POST)
//            $this->formData = Apollo_App::clean_array_data($_POST);
//        $this->formInit();
    //    $this->validateClassObject->setRules($this->getRule());


        if(count($this->elements) > 0){
            foreach($this->elements as $element_item){
                
                if ( isset($element_item['name'] ) && $element_item['name'] == 'ID' && $this->id ) {
                    $element_item['value'] = $this->id;
                }

                Apollo_Form_Static::renderElement($element_item,$this->validateClassObject,$this->formRequestMethod);
            }
        }
        //render fooer for this form
        $this->formFooter();
    }
    //Form submit action process here
    public function formSubmitProcess(){
        $this->formSubmitAction();
        $this->formHeader();
        $this->successMessage();
    }

    public function formSubmitAction()
    {

        // Form submit
        do_action('apollo_submit_form');

        $this->isSaveSuccess = false;
        $this->isSubmitFail = false;
        //post method
        if($this->formRequestMethod == 'post'){
            $this->isSubmitFail = true;
            if( $this->validateClassObject->isValidAll($this->mergerPostRule())){
                $this->formData = $_POST;

                $this->cleanPost();

                //save data
                $this->id = $this->postMethod();
                //end save date
                $this->isSaveSuccess = true;
                $this->isSubmitFail = false;
            }
        }
    }

    //abstract method
    //get all rule of all element in form
    protected function getRule(){
        $rule = array();
        foreach($this->elements as $k => $element){
            $rule[$k] = isset($element['validate']['rule'])?$element['validate']['rule']:array();
            if(isset($element['children'][0]) && is_array($element['children'][0])){
                foreach($element['children'][0] as $kc => $childElement){
                    $rule[$kc] = isset($childElement['validate']['rule'])?$childElement['validate']['rule']:array();
                }
            }
            //rule additional form
            if($element['type'] == 'Option'){
                $dataPostType = isset($element['value']['post_type'])?$element['value']['post_type']:'';
                if(!empty($dataPostType)){
                    $group_fields = Apollo_Custom_Field::get_group_fields( Apollo_DB_Schema::_ORGANIZATION_PT );
                    foreach($group_fields as $group){
                        if(isset($group['fields']) && is_array($group['fields'])){
                            foreach($group['fields'] as $field){
                                $elementRule = array();
                                if($field->required){
                                    $elementRule[] =  Apollo_Form::_FORM_REQUIRED;
                                }
                                $fieldMetaData = $field->meta_data;
                                $fieldMetaData = @unserialize($fieldMetaData);
                                if(isset($fieldMetaData['validation']) && !empty($fieldMetaData['validation'])){
                                    switch($fieldMetaData['validation']){
                                        case 'url' :
                                            $elementRule[] =  Apollo_Form::_FORM_URL;
                                            break;
                                        case 'email' :
                                            $elementRule[] =  Apollo_Form::_FORM_EMAIL;
                                            break;
                                    }
                                }
                                $rule[$field->name] = $elementRule;
                            }
                        }
                    }

                }
            }
            //end rule additional form
        }
        return $rule;
    }

    public function renderHorizontalTab(){
        $this->setCurDataHorizontalTab();
        return $this->renderTabUI();
    }

    public function getTabLinks(){
        return array();
    }

    public function renderTabUI(){
        $htmlTab = "";
        $tabLinks = $this->getTabLinks();
        if(empty($tabLinks)) return $htmlTab;
        $htmlTab .= '<div class="wc-f">
                        <div class="search-bkl">
                            <nav class="nav-tab">
                                <ul class="tab-list-search">';
        foreach($tabLinks as $tabUrl => $tabTitle){
            $selected = $tabUrl === $this->activatedTab ? 'selected' : '';
            $dataUrl = $selected ? '#' : site_url($tabUrl);
            $dataUrl .= '/'.intval(get_query_var( '_edit_post_id'));
            $selectedTabStyle = $selected ? 'style="cursor:default;"' : '';
            $htmlTab .= '<li class="'.$selected.'"><a data-href="'.$dataUrl.'" '.$selectedTabStyle.' href="javascript:void(0);" >'.$tabTitle.'</a></li>';
        }
        $htmlTab .= '</ul></nav></div></div>';
        return $htmlTab;
    }

    public function setCurDataHorizontalTab(){
        // to do if have anything is general
    }

    public abstract function postMethod();
    public abstract function getMethod();
    public abstract function formInit();
    public function formSubmitFail(){

        echo '<div>';
        echo '<span class="error">';
        echo $this->failMessage;
        echo '</span>';
        echo '</div>';
    }
    public function formSubmitSuccess(){
        echo ' <div class="_apollo_success">
                    <i class="fa fa-check"></i>';
        echo $this->successMessage;
        echo '</div>';
    }

    public function formSubmitSuccessRedirect($message) {
        echo ' <div class="_apollo_success">
                    <i class="fa fa-check"></i>';
        echo $message;
        echo '</div>';
    }

    protected function mergerPostRule(){
        $rule = $this->getRule();
        $data = array();
        foreach($rule as $k => $r){
            $data[$k] = isset($_POST[$k])?$_POST[$k]:'';
        }
        return $data;
    }
    public function successMessage(){
        if($this->isSaveSuccess){
            $this->formSubmitSuccess();
        }
        if($this->isSubmitFail){
            $this->formSubmitFail();
        }
    }

    public function cleanPost() {
        if($this->formData) {
            foreach($this->formData as $k => $v) {

                //$this->formData[$k] = Apollo_App::clean_data_request($v);
                $this->formData[$k] = $v;
            }
        }
    }

    /**
     * Set nonce info
     *
     * @return void
     */
    public abstract function setNonceInfo();

    /**
     * Get nonce field element for Form
     * @return array
     */
    public function getNonceField()
    {
        return array(
            'nonce' => array(
                'type'         => 'Nonce',
                'nonce_name'   => $this->nonceName,
                'nonce_action' => $this->nonceAction,
            )
        );
    }

    /**
     * Validate nonce value for Front End forms
     *
     * @ticket   #10927
     * @document https://codex.wordpress.org/Function_Reference/wp_nonce_field
     * @return   boolean
     */
    public function validateNonce()
    {
        if ( !isset($_POST[$this->nonceName]) || !wp_verify_nonce($_POST[$this->nonceName], $this->nonceAction) ) {
            $this->isSaveSuccess = false;
            $this->isSubmitFail  = true;
            return false;
        }
        return true;
    }
}
