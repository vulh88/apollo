<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/6/2015
 * Time: 5:30 PM
 */

class Apollo_Form_Element{

    protected $eName;
    protected $eClass;
    protected $ePlaceHolder;
    protected $eValue;
    protected $eTitle;
    protected $eId;
    protected $eRequestedMethod;
    protected $eErrorClass;
    protected $eValidKey;
    protected $eValidValue;
    protected $eMessage;
    protected $eValidate;
    protected $validateClass;
    protected $arrRuleFail;
    protected $arrData;
    protected $attr;
    protected $dataAttributes;

    /**
     * @return mixed
     */
    public function getDataAttributes()
    {
        return !empty($this->dataAttributes) && is_array($this->dataAttributes) ? implode(' ', $this->dataAttributes) : '';
    }

    /**
     * @param mixed $dataAttributes
     */
    public function setDataAttributes($dataAttributes)
    {
        if(empty($this->dataAttributes)){
            $this->dataAttributes = array($dataAttributes);
        } else {
            $this->dataAttributes[] = $dataAttributes;
        }
    }
    /**
     * @return mixed
     */
    public function getEName()
    {
        return $this->eName;
    }

    /**
     * @param mixed $eName
     */
    public function setEName($eName)
    {
        $this->eName = $eName;
    }

    /**
     * @return mixed
     */
    public function getEClass()
    {
        return $this->eClass;
    }

    /**
     * @param mixed $eClass
     */
    public function setEClass($eClass)
    {
        $this->eClass = $eClass;
    }

    /**
     * @return mixed
     */
    public function getEPlaceHolder()
    {
        return $this->ePlaceHolder;
    }

    /**
     * @param mixed $ePlaceHolder
     */
    public function setEPlaceHolder($ePlaceHolder)
    {
        $this->ePlaceHolder = $ePlaceHolder;
    }

    /**
     * @return mixed
     */
    public function getEValue()
    {
        return $this->eValue;
    }
    
    /**
     * @param mixed $eValue
     */
    public function setEValue($eValue)
    {
        $this->eValue = $eValue;
    }

    /**
     * @return mixed
     */
    public function getETitle()
    {
        return $this->eTitle;
    }

    /**
     * @param mixed $eTitle
     */
    public function setETitle($eTitle)
    {
        $this->eTitle = $eTitle;
    }

    /**
     * @return mixed
     */
    public function getERequestedMethod()
    {
        return $this->eRequestedMethod;
    }

    /**
     * @param mixed $eRequestedMethod
     */
    public function setERequestedMethod($eRequestedMethod)
    {
        $this->eRequestedMethod = $eRequestedMethod;
    }

    /**
     * @return mixed
     */
    public function getEId()
    {
        return $this->eId;
    }

    /**
     * @param mixed $eId
     */
    public function setEId($eId)
    {
        $this->eId = $eId;
    }

    /**
     * @return mixed
     */
    public function getEErrorClass()
    {
        return $this->eErrorClass;
    }

    /**
     * @param mixed $eErrorClass
     */
    public function setEErrorClass($eErrorClass)
    {
        $this->eErrorClass = $eErrorClass;
    }

    /**
     * @return mixed
     */
    public function getEValidKey()
    {
        return $this->eValidKey;
    }

    /**
     * @param mixed $eValidKey
     */
    public function setEValidKey($eValidKey)
    {
        $this->eValidKey = $eValidKey;
    }

    /**
     * @return mixed
     */
    public function getEValidValue()
    {
        return $this->eValidValue;
    }

    /**
     * @param mixed $eValidValue
     */
    public function setEValidValue($eValidValue)
    {
        $this->eValidValue = $eValidValue;
    }

    /**
     * @return mixed
     */
    public function getEMessage()
    {
        return $this->eMessage;
    }

    /**
     * @param mixed $eMessage
     */
    public function setEMessage($eMessage)
    {
        $this->eMessage = $eMessage;
    }

    /**
     * @return mixed
     */
    public function getEValidate()
    {
        return $this->eValidate;
    }

    /**
     * @param mixed $eValidate
     */
    public function setEValidate($eValidate)
    {
        $this->eValidate = $eValidate;
    }

    /**
     * @return mixed
     */


    /**
     * @return mixed
     */
    public function getValidateClass()
    {
        return $this->validateClass;
    }

    /**
     * @param mixed $validateClass
     */
    public function setValidateClass($validateClass)
    {
        $this->validateClass = $validateClass;
    }

    /**
     * @return mixed
     */
    public function getArrRuleFail()
    {
        return $this->arrRuleFail;
    }

    /**
     * @param mixed $arrRuleFail
     */
    public function setArrRuleFail($arrRuleFail)
    {
        $this->arrRuleFail = $arrRuleFail;
    }

    /**
     * @return mixed
     */
    public function getArrData()
    {
        return $this->arrData;
    }

    /**
     * @param mixed $arrRuleFail
     */
    public function setArrData($arrData)
    {
        $this->arrData = $arrData;
    }


    public function __construct($name = '', $placeHolder  = '', $value  = '', $class  = '',$title  = '',$id='',$data= array(),$requestedMethod='get', $dataAttributes = array()){

        $this->eName = $name;
        $this->ePlaceHolder = $placeHolder;
        $this->eValue =  $value;
        $this->eClass = $class;
        $this->eTitle = $title;
        $this->eId = $id;
        $this->eRequestedMethod = $requestedMethod;
        $this->eErrorClass = '';
        $this->eMessage = '';
        $this->arrData = $data;
        $this->dataAttributes = $dataAttributes;
    }

    public function render(){
        $this->validate();
    }

    public function validate(){
        $this->validateProcess();
    }

    public function customValidateProcess(){
        $this->arrRuleFail = array();
        //get  NAME not NAME[];
        $key = str_replace(array('[',']'),'',$this->eValidKey);
        if($this->eRequestedMethod == 'post'){
            if($this->eValidate){
                $processClass = $this->validateClass;
                $this->arrRuleFail = $processClass->isValid($key,$this->eValidValue,array());
                $this->eErrorClass = !empty( $this->arrRuleFail ) ? _APL_INPUT_ERROR : '';
            }
        }
    }
    public function validateProcess(){

        $this->customValidateProcess();
        $message = '';
        if( is_array($this->arrRuleFail) && count( $this->arrRuleFail)>0){
                foreach( $this->arrRuleFail as $rule) {
                    $validate = call_user_func_array(
                        array($this->validateClass,'getMessageValidate'),
                        array($rule,$this->eTitle)
                    );
                    $message  .='<span class="error" >*'.$validate.'</span >';
                }
                $this->eMessage = $message;
            }
    }
}