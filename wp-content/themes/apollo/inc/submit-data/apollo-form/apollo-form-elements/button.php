<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/25/2015
 * Time: 4:58 PM
 */

class Button extends  Apollo_Form_Element{

    protected $type = 'button';
    protected $noContainer = false;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return boolean
     */
    public function isNoContainer()
    {
        return $this->noContainer;
    }

    /**
     * @param boolean $noContainer
     */
    public function setNoContainer($noContainer)
    {
        $this->noContainer = $noContainer;
    }



    public function render(){
        if($this->noContainer == false){
             echo '<div class="submit-blk">';
             echo '<button name="'.$this->eName.'"  type="'.$this->type.'" id="'.$this->eId.'" class="'.$this->eClass.'"';
            if($this->arrData){
                foreach($this->arrData as $k => $v){
                    echo $k,'=','"'.$v.'"';
                }
            }
             echo '>'.$this->eTitle.'</button>';
             echo '</div>';
        }else{
            echo '<button name="'.$this->eName.'"  type="'.$this->type.'" id="'.$this->eId.'" class="'.$this->eClass.'"';
            if($this->arrData){
                foreach($this->arrData as $k => $v){
                    echo $k,'=','"'.$v.'"';
                }
            }
            echo '>'.$this->eTitle.'</button>';
        }


    }
}