<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/6/2015
 * Time: 5:30 PM
 */
class Select extends  Apollo_Form_Element{
    public function render(){
        parent::render();
        $dataAttributes = $this->getDataAttributes();
        echo '<select '.$dataAttributes.' name="' . $this->eName . '" id="' . $this->eId . '" class="' . $this->eClass . ' ' . $this->eErrorClass . '">';
        echo $this->getOptions();
        echo '</select>';
        echo '   <div class="show-tip">' . $this->eTitle . '</div>';
        echo $this->eMessage;
   }
    private function getOptions(){
        $value = $this->eValue;

        if(isset($value['null_option'])){
            $output = '';

            if (!empty($value['null_option'])) {
                $output  .= '<option>'.$value['null_option'].'</option>';
            }
            else {
                $output  .= '<option>'.__( '-- Select a business type (*)--', 'apollo' ).'</option>';
            }

            if (!empty($value['list_item'])) $output .= $value['list_item'];


            return $output;
        }
        else {
            $listItem = isset($value['list_item'])?$value['list_item']:array();
            $selectedItem = isset($value['selected_item'])?$value['selected_item']:'';
            $list = '';

            if (!empty($this->eTitle)) {
                $list .= '<option value="">'.$this->eTitle.'</option>';
            }

            if (isset($value['is_post_type_list']) && $value['is_post_type_list']) {
                foreach($listItem as $item){
                    $selected = '';
                    if($item->ID == strtolower($selectedItem))
                        $selected = 'selected';
                    $list .= '<option '.$selected.' value="'.$item->ID.'">'.$item->post_title.'</option>';
                }
            } else {
                foreach($listItem as $k => $item){
                    $selected = '';
                    if(strtolower($k) == strtolower($selectedItem))
                        $selected = 'selected';
                    $list .= '<option '.$selected.' value="'.$k.'">'.$item.'</option>';
                }
            }

            return $list;
        }

    }
    //because value of select different other type
    public function customValidateProcess(){
        $this->arrRuleFail = array();
        if($this->eRequestedMethod == 'post'){
            if($this->eValidate){
                $processClass = $this->validateClass;
                $this->arrRuleFail = $processClass->isValid($this->eValidKey,$this->eValidValue['selected_item'],array());
                $this->eErrorClass = !empty($this->arrRuleFail) ? _APL_INPUT_ERROR : '';
            }
        }
    }
}