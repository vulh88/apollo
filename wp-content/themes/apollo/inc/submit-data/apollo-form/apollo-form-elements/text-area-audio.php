<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/6/2015
 * Time: 5:30 PM
 */
class TextAreaAudio extends  Apollo_Form_Element{

    public function render(){
        parent::render();
        $value = esc_attr($this->eValue);
        echo '<textarea placeholder=\''.$this->ePlaceHolder.'\' name=\''.$this->eName.'\' id=\''.$this->eId.'\' rows=\'5\' class=\''.$this->eClass.' '.$this->eErrorClass. '\'>'.$value.'</textarea>';
        echo '  <div class=\'show-tip\'>'.$this->eTitle.'</div>';
        echo $this->eMessage;
    }
}