<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/6/2015
 * Time: 5:30 PM
 */
class Checkbox extends  Apollo_Form_Element{

    protected  $type = 'checkbox';
    protected  $listValue;
    protected  $listSelected;

    public function render(){
        parent::render();


        $this->listValue = isset($this->eValue['list_item'])?$this->eValue['list_item']:'';
        $this->listSelected = isset($this->eValue['selected_item'])?$this->eValue['selected_item']:'';
        $selected = $this->listValue == $this->listSelected ? 'checked' : '';
        $disable = isset($this->eValue['disable'])?$this->eValue['disable']:'';
        echo '<div class="art-email-check">';
            echo ' <input class="art-email-chk" type=\''.$this->type.'\' id=\''.$this->eId.'\'  '.$disable.' name=\''.$this->eName.'\' placeholder=\''.$this->ePlaceHolder.'\' '.$this->listSelected.' '.$selected.' value=\''. $this->listValue.'\' class=\''.$this->eErrorClass. '\'>';
            echo '<p class="art-email-label">'.$this->eTitle.'</p>';
            echo $this->eMessage;
        echo '</div>';
    }

    public function customValidateProcess(){

    }
}