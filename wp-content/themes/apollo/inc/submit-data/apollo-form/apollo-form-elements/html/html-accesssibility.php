<p><?php _e('To the best of your knowledge, please tell us which (if any) accessibility features your event offers to patrons.','apollo'); ?></p>
<div <?php echo $none_style; ?> class="access-list">
    <ul class="access-listing">
        <?php
        $i = 0;
        $total = count( $data_access );
        $step = 6;

        foreach ( $data_access as $img => $label ):
            if ( $i == 0 || $i == $step ) echo '<li><ul class="ACLlist">';
            $checked = '';
            if(isset($postData[Apollo_DB_Schema::_E_CUS_ACB])){
                if(in_array( $img, $postData[Apollo_DB_Schema::_E_CUS_ACB] )){
                    $checked = 'checked';
                }
            }

            ?>
            <li>
                <input value="<?php echo $img ?>" <?php echo $checked; ?>
                       name="<?php echo Apollo_DB_Schema::_E_CUS_ACB ?>[]" type="checkbox">
                <img src="<?php echo get_template_directory_uri() ?>/assets/images/event-accessibility/2x/<?php echo $img ?>.png">
                <label><?php echo $label ?></label>
            </li>

            <?php if ( $i == $step - 1 || $i == $total - 1 ) echo '</ul></li>' ?>

            <?php $i++; endforeach; ?>

    </ul>
    <?php $key = Apollo_DB_Schema::_E_ACB_INFO; ?>
    <div class="el-blk full">
        <?php
        /** @Ticket #14178 */
        wp_editor(isset($postData[Apollo_DB_Schema::_E_ACB_INFO])?$postData['_accessibility_info']:'', $key,
            array(
                'editor_height' => 250,
                'editor_class'  => $submit_obj->the_error_class( $key ) . ' inp-desc-event',
                'textarea_name' => $key
            )
        );
        ?>
        <?php  //echo $submit_obj->the_field_error( $key ); ?>
        <div class="show-tip"><?php  _e( 'Additional accessibility info', 'apollo' ) ?></div>
    </div>
</div>