<ul class="cat-listing tbl<?php echo $k ?> <?php echo isset($extraClasses) && !empty($extraClasses) ? $extraClasses : ''; ?>" <?php echo isset($dataAttributes) && !empty($dataAttributes) ? $dataAttributes : ""; ?> >
    <?php
    $total = count( $primarys );
    if ( $total > 2 ) {
        $columns = array(
            array_slice( $primarys , 0, ceil( $total / 2 ) ),
            array_slice( $primarys , ceil( $total / 2 ), $total - 1 )
        );
    } else {
        $columns = array(
            $primarys,
        );
    }

  if((is_array($cats) && count($cats) == 0) || !is_array($cats))
      $cats = array();

    foreach ( $columns as $k => $column ):
        ?>
        <li>
            <ul class="Llist c<?php echo $k ?>">
                <?php
                foreach ( $column as $p ):
                    $displayFEFrom = get_apollo_term_meta( $p->term_id, Apollo_DB_Schema::_APL_TAXONOMY_DO_NOT_DISPLAY_ON_FE, true );
                    $class = $displayFEFrom == 1 ? 'hidden' : '';
                    ?>
                    <li class="<?php echo $class ?>">
                        <input class="<?php echo $class ?>" <?php echo $cats && in_array( $p->term_id, $cats ) ? 'checked' : '' ?>
                            name="<?php echo $input_name ?>" type="checkbox" value="<?php echo $p->term_id ?>"  >
                        <label><?php echo $p->name ?></label>
                        <?php
                        $childs = get_terms( $type, array( 'parent' => $p->term_id, 'hide_empty' => false ) );
                        if ( $childs ):
                            /*@ticket #18084: [CF] 20181030 - [Business] Collapse/expand the business sub-category types by adding an 'arrow' next to each parent type - item 1*/
                            echo apply_filters('apl_add_collapse_expand_icon', '');
                            ?>
                            <ul>
                                <?php foreach( $childs as $c ):
                                    $displayChildFEFrom = get_apollo_term_meta(  $c->term_id, Apollo_DB_Schema::_APL_TAXONOMY_DO_NOT_DISPLAY_ON_FE, true );
                                    $hidden = $displayChildFEFrom == 1 ? 'hidden' : '';?>
                                    <li class="<?php echo $hidden?>">
                                        <input class="<?php echo $hidden?>" <?php echo $cats && in_array( $c->term_id, $cats ) ? 'checked' : '' ?>
                                            name="<?php echo $input_name ?>" type="checkbox" value="<?php echo $c->term_id ?>" >
                                        <label><?php echo $c->name ?></label>
                                    </li>
                                <?php  endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </li>
    <?php endforeach; ?>
</ul>
