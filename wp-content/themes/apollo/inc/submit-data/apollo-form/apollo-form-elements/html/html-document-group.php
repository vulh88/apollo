<div class="artist-blk">
    <?php if(!$hide_header) : ?>
    <div class="title-bar-blk"><?php _e( 'Documents (PDF)', 'apollo' ) ?></div>
    <?php endif; ?>
    <div id="<?php echo $document_id; ?>" >
        <div class="upload_pdf-wrapper">
            <?php
            // DEFAULT DATA
            $arr_items = array(
            );

            ?>

            <script type="text/plain" id="upload_pdf-default-template">

                        <div class="count apollo-artist-upload_pdf clearboth">
                            <div class="divider-shadow"></div>
                            <div class="pos-relative clearboth">

                                <div class="el-blk full">
                                    <input style="display: none" type="file" name="upload_pdf<?php echo $suffix; ?>[]" data-ride="upload_file"
                                            data-alert="<?php _e('File too large. Maximum size allow is %s MB','apollo') ?>"
                                           data-maxfilesize="<?php echo Apollo_App::maxUploadFileSize('b') ?>"
                                           accept=".pdf"
                                        />
                                    <button class="apollo-btn-upload-pdf" type="button" data-role="file">
                                     <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                                      <?php _e('Upload PDF', 'apollo') ?>
                                  </button>
                                    <input type="text" placeholder="<?php _e('No file chosen', 'apollo') ?>" class="input-file" readonly="true"/>
                                </div>

                                <div class="el-blk full">
                                    <input class="desc-pdf"  name="upload_pdf_label<?php echo $suffix; ?>[]" placeholder="<?php _e( 'Description', 'apollo' ) ?>" rows="3" cols="20" />
                                    <div class="show-tip"><?php _e( 'Description', 'apollo' ) ?></div>
                                </div>
                                <div style="display: block;" data-target=".apollo-artist-upload_pdf" data-mainwrapper="#upload_pdf-event-data" data-ride="remove-relation"  data-confirm="<?php _e( 'Are you sure to remove this record ?', 'apollo' ); ?>" class="right_corner_abs cred"><i class="fa fa-times"></i></div>
                            </div>
                        </div>
                    </script>

            <span id="_center_info_html" data-max-upload-file="<?php echo of_get_option(Apollo_DB_Schema::_MAX_UPLOAD_PDF_ARTIST, Apollo_Display_Config::_MAX_UPLOAD_PDF_DEFAULT) ?>" data-max_upload_size="<?php wp_max_upload_size(); ?> data-uploaded="0" />


            <div class="apollo-artist-upload_pdf-list">

                <?php

                // GET DATA - override $arr_award
                global $wpdb;

                $arr_upload_pdf_info_id = maybe_unserialize(get_apollo_meta($thepostid, $metadata_key, true));

                if(!empty($arr_upload_pdf_info_id)) {

                    // build $arr_items
                    foreach($arr_upload_pdf_info_id as $id) {
                        array_push($arr_items, get_post($id)) ;
                    }
                }

                // FETCH DATA
                if(!empty($arr_items)):
                    $i = 0;
                    foreach($arr_items as $i =>  $item):

                        if ( ! $item )                                continue;
                        ?>

                        <div class="count apollo-artist-upload_pdf clearboth">
                            <div class="divider-shadow"></div>

                            <div class="pos-relative clearboth">
                                <div class="el-blk full pdf-title">
                                    <span class="mr-15"><i class="fa fa-file-pdf-o"></i></span> <?php echo $item->post_title ?>
                                </div>

                                <div class="el-blk full">
                                    <input class="desc-pdf"  name="upload_pdf_label_old<?php echo $suffix; ?>[]" placeholder="<?php _e( 'Description', 'apollo' ) ?>" rows="3" cols="20" value="<?php echo esc_attr($item->post_excerpt); ?>" />
                                    <div class="show-tip"><?php _e( 'Description', 'apollo' ) ?></div>
                                </div>
                                <div style="display: block;" data-ride="remove-relation" data-target=".apollo-artist-upload_pdf"
                                     data-action="apollo_remove_upload_pdf"
                                     data-mainwrapper="#upload_pdf-event-data"
                                     data-action_data = '<?php echo $thepostid ?>, <?php echo $item->ID ?>'
                                     data-confirm="Are you sure to remove this record ?" class="right_corner_abs cred"><i class="fa fa-times"></i></div>
                            </div>


                        </div>
                        <?php
                        $i++;
                    endforeach;
                else:
                    ?>


                    <div class="count apollo-artist-upload_pdf clearboth">
                        <div class="divider-shadow"></div>

                        <div class="pos-relative clearboth">
                            <div class="el-blk full">
                                <input style="display: none" type="file" name="upload_pdf<?php echo $suffix; ?>[]" data-ride="upload_file"
                                       data-alert="<?php _e('File too large. Maximum size allow is %s MB','apollo') ?>"
                                       data-maxfilesize="<?php echo Apollo_App::maxUploadFileSize('b'); ?>"
                                       accept=".pdf"
                                    />
                                
                                <button class="apollo-btn-upload-pdf" type="button" data-role="file">
                                    <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                                    <?php _e('Upload PDF', 'apollo') ?>
                                </button>
                                <input placeholder="<?php _e('No file chosen', 'apollo') ?>" type="text" class="input-file" readonly="true"/>
                                    
                            </div>

                            <div class="el-blk full">
                                <input type="text" class="desc-pdf"  name="upload_pdf_label<?php echo $suffix; ?>[]" placeholder="<?php _e( 'Description', 'apollo' ) ?>" rows="3" cols="20" />
                                <div class="show-tip"><?php _e( 'Description', 'apollo' ) ?></div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <input type="hidden" class="apl-remove-old-pdf" name="apl-remove-old-pdf" value=""/>
            </div><!-- end apollo-artist-upload_pdf-list -->
        </div>

        <input type="button" class="btn-noW"
               data-ride="shadow-man"
               data-template="#upload_pdf-default-template"
               data-mainwrapper="#<?php echo $document_id; ?>"
               data-append-to = ".apollo-artist-upload_pdf-list:append"
               data-maxtimes="<?php echo $maxUploadPDF; ?>"
               data-bornname=".apollo-artist-upload_pdf"
               data-alert = "<?php _e('Max %s item allow!','apollo') ?>"

               value="<?php _e( 'Add More', 'apollo' ) ?>" />

        <br/><br/>
    </div>
</div>