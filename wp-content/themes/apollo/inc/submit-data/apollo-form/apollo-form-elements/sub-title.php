<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/22/2015
 * Time: 2:15 PM
 */
class Subtitle extends Apollo_Form_Element{
    public function render(){
        $content = '<label class="form-subtitle '.$this->eClass.'">'.$this->eValue.'</label>';
        echo $content;
    }
    public function validateProcess(){
        return true;
    }
}