<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/26/2015
 * Time: 4:28 PM
 */
class Tab extends Apollo_Form_Element{
    protected $childItem;
    public function getChildItem()
    {
        return $this->childItem;
    }
    public function setChildItem($childItem)
    {
        $this->childItem = $childItem;
    }
    public function render(){
        parent::render();
        $this->renderTabHeader();
        $this->renderTabContent();
    }
    private function renderTabHeader(){
        $i = 1;
        echo '<nav class="nav-tab" style="width: 100%;">';
        echo '    <ul class="tab-image-list">';
        foreach($this->childItem as $child){
            $title = isset($child['title'])?$child['title']:'';
            if($child['name'] === 'primary_photo' && !empty($this->eErrorClass)){
                $requiredTextStyle = 'style="color:red;"';
            } else {
                $requiredTextStyle = '';
            }
            echo ' <li>
                    <a '.$requiredTextStyle.' data-id="'.$i.'" href="javascript:void(0);">'.$title.'</a>
            </li>';
            $i++;
        }
        echo '    </ul>';
        echo '</nav><p></p><br/>';
    }
    private function renderTabContent(){
        $i = 1;
        foreach($this->childItem as $child) {
            echo '<div data-target="'.$i.'" class="" id="upload_and_drop_container" style="position: relative;">';
                Apollo_Form_Static::renderElement($child,$this->validateClass);
            echo   '</div>';
            $i++;
        }
    }


    public function customValidateProcess(){
        $this->arrRuleFail = array();
        if($this->eRequestedMethod == 'post'){
            if($this->eValidate){
                $isValidTabField = $this->isValidTabField($this->eValidValue);
                $this->arrRuleFail = !$isValidTabField ? array(Apollo_Form::_FORM_REQUIRED) : array();
                $this->eErrorClass = !$isValidTabField ? _APL_INPUT_ERROR : '';
            }
        }
    }

    public function isValidTabField($pID){
        $pid = $pID;
        $target = Apollo_DB_Schema::_ORGANIZATION_PT;
        $isExistingFImagePidInSession = isset($_SESSION['apollo'][$target . Apollo_SESSION::SUFFIX_ADD_PIMAGE][$pid]);
        $isNotExistingFImagePidInSession = isset($_SESSION['apollo'][$target . Apollo_SESSION::SUFFIX_ADD_PIMAGE][0]);
        if(!$isExistingFImagePidInSession && !$isNotExistingFImagePidInSession){
            return false;
        }
        return true;
    }

}