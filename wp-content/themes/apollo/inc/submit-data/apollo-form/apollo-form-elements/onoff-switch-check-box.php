<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/6/2015
 * Time: 5:30 PM
 */
class OnOffSwitchCheckbox extends  Apollo_Form_Element{

    protected  $type = 'checkbox';
    protected  $listValue;
    protected  $listSelected;

    public function render(){
        parent::render();


        $this->listValue = isset($this->eValue['list_item'])?$this->eValue['list_item']:'';
        $this->listSelected = isset($this->eValue['selected_item'])?$this->eValue['selected_item']:'';
        $selected = $this->listValue == $this->listSelected ? 'checked' : '';
        $disable = isset($this->eValue['disable'])?$this->eValue['disable']:'';
        echo '<div class="evt-blk" ><span class="title">'.$this->eTitle.'</span>';
            echo '<div class="onoffswitch">';
                echo ' <input class="onoffswitch-checkbox" type=\''.$this->type.'\' id=\''.$this->eId.'\'  '.$disable.' name=\''.$this->eName.'\' placeholder=\''.$this->ePlaceHolder.'\' '.$this->listSelected.' '.$selected.' value=\''. $this->listValue.'\' class=\''.$this->eErrorClass. '\'>';
                echo '<label for="'. $this->eId .'" class="onoffswitch-label"><span class="onoffswitch-inner"></span><span class="onoffswitch-switch"></span></label>';
                echo $this->eMessage;
            echo '</div>';
        echo '</div>';
    }

    public function customValidateProcess(){

    }
}