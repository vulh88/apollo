<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/6/2015
 * Time: 5:30 PM
 */
class Additional_fields extends  Apollo_Form_Element{
    protected $postType = '';
    protected $postData = '';
    protected $postId = '';
    public function render(){
        parent::render();
        $this->postType = isset($this->eValue['post_type'])?$this->eValue['post_type']:'';
        $this->postData = isset($this->eValue['post_data'])?$this->eValue['post_data']:'';
        $this->postId = isset($this->eValue['post_id'])?$this->eValue['post_id']:'';
        if(isset($_POST) && !empty($_POST)){
            $data = $this->postData;
        } else {
            $data = maybe_unserialize( Apollo_App::apollo_get_meta_data( $this->postId , Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA) );
        }

        $submitForm = new Apollo_Submit_Form($this->postType,array());

        // Thienld : get value of enabled_validation to enable or disable validation for all fields which are belong to this additional field.
        $enabledValidation = isset($this->eValue['enabled_validation']) ? $this->eValue['enabled_validation'] : true;
        $submitForm->custom_fields_form($data, $enabledValidation);
    }
}