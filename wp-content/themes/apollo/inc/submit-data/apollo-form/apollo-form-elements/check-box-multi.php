<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/6/2015
 * Time: 5:30 PM
 */
class Multi_checkbox extends  Apollo_Form_Element{

    protected  $type = 'checkbox';
    protected  $listValue;
    protected  $listSelected;

    public function render(){
        parent::render();

        $this->listValue = isset($this->eValue['list_item'])?$this->eValue['list_item']:'';
        $this->listSelected = isset($this->eValue['selected_item'])?$this->eValue['selected_item']:array();

        echo '<p>'.$this->eTitle.'</p>';
        echo '<ul class="radio-listing">';
        if(is_array($this->listValue)){
            foreach($this->listValue as $k => $item){
                echo '<li class="Llist ">';
                $checked = '';
                if(is_array($this->listSelected)){
                    if(in_array($k, $this->listSelected))
                        $checked = 'checked';
                }
                echo '<input name="'.$this->eName.'[]" type ="'.$this->type.'" '.$checked.' value="'.$k.'"  ><label>'.$item.'</label>';
                echo '</li>';
            }
            echo '</ul>';

            echo $this->eMessage;
        }
    }

    public function customValidateProcess(){
        $this->arrRuleFail = array();
        $value = isset($this->eValidValue['selected_item'])?$this->eValidValue['selected_item']:'';
        if($this->eRequestedMethod == 'post'){
            if($this->eValidate){
                $processClass = $this->validateClass;
                $this->arrRuleFail = $processClass->isValid($this->eValidKey,$value,array());
                $this->eErrorClass = !empty($this->arrRuleFail) ? _APL_INPUT_ERROR : '';
            }
        }
    }
}