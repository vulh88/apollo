<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/6/2015
 * Time: 5:30 PM
 */
class Radio extends  Apollo_Form_Element{

    protected  $type = 'radio';
    protected  $listValue;
    protected  $listSelected;

    public function render(){
        parent::render();
        
        $this->listValue = isset($this->eValue['list_item'])?$this->eValue['list_item']:'';
        $this->listSelected = isset($this->eValue['selected_item'])?$this->eValue['selected_item']:'';
     
        echo '<p>'.$this->eTitle.'</p>';
        echo '<ul class="radio-listing">';
        if(is_array($this->listValue)){
            foreach($this->listValue as $k => $item){
                echo '<li class="Llist ">';
                $checked = '';
                if($this->listSelected == $k)
                    $checked = 'checked';
                echo '<input name="'.$this->eName.'" type ="'.$this->type.'" '.$checked.' value="'.$k.'"  ><label>'.$item.'</label>';
                echo '</li>';
            }
            echo $this->eMessage;
            echo '</ul>';
        }
    }
}