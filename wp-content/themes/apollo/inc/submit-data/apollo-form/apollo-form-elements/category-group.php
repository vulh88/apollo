<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/22/2015
 * Time: 3:58 PM
 */

class Category_Group extends  Apollo_Form_Element{
    protected $postType;
    protected $type;
    protected $tax;
    public function getPostType()
    {
        return $this->postType;
    }
    public function setPostType($postType)
    {
        $this->postType = $postType;
    }
    public function getTax()
    {
        return $this->tax;
    }
    public function setTax($tax)
    {
        if ( !$tax ) $tax = $this->postType. '-type';
        $this->tax = $tax;
    }
    private function renderColumn($primarys,$k){
        $cats = isset($this->eValue)?$this->eValue:array();
        $type = $this->tax;
        $input_name = $this->eName.'[]';
        $html = '';
        $dataAttributes = $this->getDataAttributes();
        $extraClasses = $this->eClass;
        include('html/html-category-group.php');
    }
    public function render(){
        parent::render();
        $primarys = get_terms( $this->tax, array( 'parent' => false, 'hide_empty' => false ) );
        if($primarys){
            $k = 0;
            $this->renderColumn($primarys,$k);
        }
        echo $this->eMessage;
    }

    public function customValidateProcess(){
        $this->arrRuleFail = array();
        $value = isset($this->eValidValue)?$this->eValidValue:'';
        if($this->eRequestedMethod == 'post'){
            if($this->eValidate){
                $processClass = $this->validateClass;
                $this->arrRuleFail = $processClass->isValid($this->eValidKey,$value,array());
                $this->eErrorClass = !empty($this->arrRuleFail) ? _APL_INPUT_ERROR : '';
            }
        }
    }

}