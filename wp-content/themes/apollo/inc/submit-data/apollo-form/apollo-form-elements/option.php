<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/6/2015
 * Time: 5:30 PM
 */
class Option extends  Apollo_Form_Element{
    protected $postType = '';
    protected $postData = '';
    protected $postId = '';
    public function render(){
        parent::render();
        $this->postType = isset($this->eValue['post_type'])?$this->eValue['post_type']:'';
        $this->postData = isset($this->eValue['post_data'])?$this->eValue['post_data']:'';
        $this->postId = isset($this->eValue['post_id'])?$this->eValue['post_id']:'';
        if(!empty($this->postType)){
            $group_fields = Apollo_Custom_Field::get_group_fields( Apollo_DB_Schema::_ORGANIZATION_PT );
            $this->renderFieldItems($group_fields);
        }

    }

    protected function renderFieldItems($fieldGroup){
        //render group
        foreach($fieldGroup as $group){
            $groupName = $group['group']->label;
            //render group title here
            $this->renderTitle($groupName);
            //end render group title
            $fields = isset($group['fields'])?$group['fields']:array();
            //render fields in group
            foreach($fields as $field){
                //check field available (set in admin)
                if ( ! Apollo_Custom_Field::can_display( $field ) )
                    continue;
                //render field
                $this->renderFieldItemType($field);
                //end render field
                $has_fields = TRUE;
            }
        }
    }
    //validate function
    public function validateProcess(){}

    //render functions
    protected function renderFieldItemType($field){


        $type = $field->cf_type;
        $className = $this->getClassNameElement($type);

        //get data for field
        $methodPostValue = '';
        if($this->eRequestedMethod != 'post'){
            $methodPostValue =  $value = Apollo_App::apollo_get_meta_data( $this->postId , $field->name, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA);
        }
        else{
            if(isset($this->postData[$field->name])){
                $methodPostValue = $value = $this->postData[$field->name];
            }
        }


        $fieldMetaData = @unserialize($field->meta_data);
        //end get data

        //set rule for custom field
        $elementRule = array();
        if($field->required){
            $elementRule[] =  Apollo_Form::_FORM_REQUIRED;
        }
        if(isset($fieldMetaData['validation']) && !empty($fieldMetaData['validation'])){
            switch($fieldMetaData['validation']){
                case 'url' :
                    $elementRule[] =  Apollo_Form::_FORM_URL;
                    break;
                case 'email' :
                    $elementRule[] =  Apollo_Form::_FORM_EMAIL;
                    break;
            }
        }
        //end set field for custom field
        $id = '';
        $classCss = 'inp inp-txt';



        if($className == 'Textarea' ){
            $classCss = 'inp-desc-event';

        }

        $valueSelect = '';
        if($className == 'Select'  || $className == 'Radio' ){
            $selectArray = Apollo_Custom_Field::get_choice( $field );
            $selectItem = '';
            $itemArray = array();
            foreach($selectArray as $k => $val){
                if($k == $methodPostValue)
                    $selectItem = $k;
            }
            $valueSelect = array(
                'list_item' =>    $selectArray,
                'selected_item' => $selectItem
            );
            $classCss .= ' chosen';
        }
        if($className == 'Multi_checkbox' ){
            $selectArray = Apollo_Custom_Field::get_choice( $field );
            $valueSelect = array(
                'list_item' =>    $selectArray,
                'selected_item' => $methodPostValue
            );
        }
        if($className == 'Checkbox'){
            $checked = '';
            if($field->name == $methodPostValue){
                $checked = 'checked';
            }
            $value = array(
                'list_item' =>    $field->name,
                'selected_item' => $checked
            );
        }
        if(is_array($valueSelect)){
            $value = $valueSelect;
        }

        if($className == 'Wysiwyg'){
            $classCss = 'inp-desc-event ckeditor ';
            $className = 'Wysiwyg';
            $id = $field->name;
            $value = Apollo_App::convertTinyCMEToCkEditor($value);
        }

        $element = array(
            'class' => $classCss,
            'value' => $value,
            'label' => $field->label,
            'place_holder' => $field->label,
            'type' => $className,
            'title' => $field->label,
            'name' => $field->name,
            'validate' => array(
                'rule' => $elementRule
            ),
            'id' => $id

        );
        Apollo_Form_Static::renderElement($element,$this->validateClass,$this->eRequestedMethod);
    }

    public function getClassNameElement($name){
        if(is_string($name)){
            $first = strtoupper($name[0]);
            $rest = substr($name, 1);
            return $first.$rest;
        }
    }
    protected function renderTitle($title){
        $titleObj = new Title();
        $titleObj->setEValue(__($title,'apollo'));
        $titleObj->setEClass('custom-label');
        $titleObj->render();
    }
}