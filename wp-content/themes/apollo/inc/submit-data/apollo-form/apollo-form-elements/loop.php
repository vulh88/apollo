<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/25/2015
 * Time: 4:44 PM
 */
class Loop extends Apollo_Form_Element{
    protected $controlButton;
    protected $childItem;
    /**
     * @return mixed
     */
    public function getChildItem()
    {
        return $this->childItem;
    }
    /**
     * @param mixed $childItem
     */
    public function setChildItem($childItem)
    {
        $this->childItem = $childItem;
    }
    public function getControlButton()
    {
        return $this->controlButton;
    }
    public function setControlButton($controlButton)
    {
        $this->controlButton = $controlButton;
    }
    public function render(){
        parent::render();
        $this->renderItem();
        $this->renderButtonItem();
        echo $this->eMessage;
    }
    public function renderItem(){
        if(isset($this->childItem[0]) && is_array($this->childItem[0])){
            $groupContent = $this->childItem[0];
        }
        $validateClass = $this->validateClass;
        include_once('script/loop-script.php');
        $this->clearFormData();
        echo '<div class="des-list" data-html="'. $groupContentHtml.'" >';
        //loop render group item
        for($i = 0; $i < $this->countGroup();$i++){
            //set value for item
           foreach($this->eValue  as $k => $value){
               //mapping value + element in group
               $this->childItem[0][$k]['value'] = $value[$i];
           }
            Apollo_Form_Static::renderCroup($this->childItem,$this->validateClass,$this->eRequestedMethod);
        }
        echo '</div>';
    }
    public function renderButtonItem(){

        foreach($this->controlButton as $k => $controlButton){
            foreach($controlButton as $key => $item){
                echo '<div class="video-btn">';
                Apollo_Form_Static::renderElement($item);
                echo '</div>';
              //  Apollo_Form_Static::renderElement($item);
            }
        }
    }
    public function clearFormData(){
        foreach($this->eValue as $k => $value){
            if(!is_array($value)){
                unset($this->eValue[$k]);
            }
        }
    }
    public function countGroup(){
        if(!is_array($this->eValue))
            return 0;
        $firstKey = '';
        foreach($this->eValue as $k => $val){
            if(!empty($k)){
                $firstKey = $k;
                break;
            }
        }
        if(is_array($this->eValue)){
            if(isset($this->eValue[$firstKey]) && is_array($this->eValue[$firstKey])){
                $max = count($this->eValue[$firstKey]);
                foreach($this->eValue as $val){
                    if(count($val) > $max)
                        $max = count($val);
                }
                return $max;
            }

        }
        return 0;

    }
    public function customValidateProcess(){

    }
}