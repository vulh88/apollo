<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/22/2015
 * Time: 3:58 PM
 */

class Access_Group extends  Apollo_Form_Element{
    protected $postType;
    protected $groupName;
    /**
     * @return mixed
     */
    public function getGroupName()
    {
        return $this->groupName;
    }
    /**
     * @param mixed $groupName
     */
    public function setGroupName($groupName)
    {
        $this->groupName = $groupName;
    }

    public function getPostType()
    {
        return $this->postType;
    }
    public function setPostType($postType)
    {
        $this->postType = $postType;
    }
    private function renderColumn($primarys,$k){
        $data_access = $this->eValue['list_item'];
        $postData = $this->eValue['selected_item'];
        $data_action = '';
        $none_style = '';
        $submit_obj = $this->validateClass;
        $postType = $this->postType;
        $groupName = $this->groupName;
        $postTitle = $this->eTitle;
       include('html/html-accesssibility.php');
    }
    public function render(){
        parent::render();
        $primarys = get_terms( $this->postType.'-type', array( 'parent' => false, 'hide_empty' => false ) );
        if($primarys){
            $k = 0;
            $this->renderColumn($primarys,$k);
        }
        echo $this->eMessage;
    }

    public function customValidateProcess(){
        $this->arrRuleFail = array();
        $value = isset($this->eValidValue)?$this->eValidValue:'';
        if($this->eRequestedMethod == 'post'){
            if($this->eValidate){
                $processClass = $this->validateClass;
                $this->arrRuleFail = $processClass->isValid($this->eValidKey,$value,array());
                $this->eErrorClass = !empty($this->arrRuleFail) ? _APL_INPUT_ERROR : '';
            }
        }
    }

}