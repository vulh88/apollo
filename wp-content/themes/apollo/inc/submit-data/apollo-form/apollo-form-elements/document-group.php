<?php

class Document_Group extends  Apollo_Form_Element{
    protected $postType;
    public function getPostType()
    {
        return $this->postType;
    }
    public function setPostType($postType)
    {
        $this->postType = $postType;
    }

    public function render(){
        parent::render();
        $thepostid = $this->eValue['post_id'];
        $hide_header = isset($this->eValue['hide_header']) ? $this->eValue['hide_header'] : false;
        $document_id = isset($this->eValue['document_id']) ? $this->eValue['document_id'] : 'upload_pdf-event-data';
        $metadata_key = isset($this->eValue['metadata_key']) ? $this->eValue['metadata_key'] : 'upload_pdf';
        $suffix = isset($this->eValue['suffix']) ? $this->eValue['suffix'] : '';
        /** @Ticket #16287 */
        $maxUploadPDF = isset($this->eValue['max_upload_pdf']) ? $this->eValue['max_upload_pdf'] : Apollo_Display_Config::_MAX_UPLOAD_PDF_DEFAULT;
        include('html/html-document-group.php');
    }
}