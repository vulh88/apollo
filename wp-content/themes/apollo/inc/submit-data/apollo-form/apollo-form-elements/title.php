<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/22/2015
 * Time: 2:15 PM
 */
class Title extends Apollo_Form_Element{
    public function render(){
        $content = '<div class="title-bar-blk '.$this->eClass.'">'.$this->eValue.'</div>';
        echo $content;
    }
    public function validateProcess(){
        return true;
    }
}