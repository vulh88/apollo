<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/6/2015
 * Time: 5:30 PM
 */
class Text extends  Apollo_Form_Element{
    protected $type = 'text';
    public function render(){
        parent::render();
        $value = esc_attr(Apollo_App::clean_data_request($this->eValue));
        $dataAttributes = $this->getDataAttributes();
        if ($this->type == 'hidden') {
            echo ' <input '.$dataAttributes.' type=\''.$this->type.'\' id=\''.$this->eId.'\' name=\''.$this->eName.'\' placeholder=\''.$this->ePlaceHolder.'\' value=\''.$value.'\' class=\''.$this->eClass.' '.$this->eErrorClass. '\'>';
            return;
        }
        
        if (! $this->ePlaceHolder) {
            echo '<p>'.$this->eTitle.'</p>';
        }

        echo ' <input '.$dataAttributes.' type=\''.$this->type.'\' id=\''.$this->eId.'\' name=\''.$this->eName.'\' placeholder=\''.$this->ePlaceHolder.'\' value=\''.$value.'\' class=\''.$this->eClass.' '.$this->eErrorClass. '\'>
        <div class="show-tip">'.$this->eTitle.'</div>';
        echo $this->eMessage;
    }
}