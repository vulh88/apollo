<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/26/2015
 * Time: 2:59 PM
 */
class ShortCode extends Apollo_Form_Element{
    protected $target;
    protected $translateMessage;
    public function getTarget()
    {
        return $this->target;
    }
    public function setTarget($target)
    {
        $this->target = $target;
    }

    public function getTranslateMessage() {
        return $this->translateMessage;
    }

    public function setTranslateMessage($translateMessage) {
        $this->translateMessage = $translateMessage;
    }

    public function render(){
        echo do_shortcode('['.$this->eValue.' target ="'.$this->target.'"]');
        if (isset($this->dataAttributes['translate']) && $this->dataAttributes['translate']) {
             echo ('<input id="apl-translate" type="hidden"
                   data-select-msg="'. __("Please select area to drop","apollo").'"
                   data-drop-min-height="'. __( "Image's height must larger than % s px", "apollo") .'"
                   data-drop-min-width="'. __( "Image's width must larger than % s px", "apollo") .'"
                   data-upload-error-msg="'. __( "Your file %s1 does not meet our minimum size requirement. Images must be at least %s2 x %s3 pixels.") .'"
                   data-upload-event-must-square="'. __( "The primary event image must be a perfect SQUARE (%s2 x %s3 pixels)", "apollo").'"
                   data-image-file ="'. __( "Image files", "apollo") .'"
                   data-one-file ="'. __( "Just upload one. Please!", "apollo").'"
                   data-max-file-gallery ="'. __( "No more than %s file(s)", "apollo").'"
                   data-max-upload-gallery ="'. __( "Maximize image allow uploaded is %s file(s)", "apollo").'"
                   data-max-size-gallery="'. __( "File %s1 too large. Maximum size allow is %s2 MB", "apollo").'"
                   data-gallery-min-resolution-msg="'. __( "Your file %s1 does not meet our minimum size requirement. Images must be at least %s2 x %s3 pixels.", "apollo").'"
                   data-gallery-max-resolution-msg="'. __( "Your file %s1 does not meet our maximum size requirement. Images must be at least %s2 x %s3 pixels.", "apollo").'"
                />'
             );
        }
    }

}