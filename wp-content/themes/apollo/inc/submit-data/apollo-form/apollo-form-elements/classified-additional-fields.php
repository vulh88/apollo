<?php

class Classified_Additional_fields extends  Apollo_Form_Element{
    protected $postType = '';
    protected $postData = '';
    protected $postId = '';
    public function render(){
        parent::render();
        $this->postType = isset($this->eValue['post_type'])?$this->eValue['post_type']:'';
        $this->postData = isset($this->eValue['post_data'])?$this->eValue['post_data']:'';
        $this->postId = isset($this->eValue['post_id'])?$this->eValue['post_id']:'';

        $data = maybe_unserialize( Apollo_App::apollo_get_meta_data( $this->postId , Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA) );

        if($this->eRequestedMethod == 'post'){
            $data = $_POST;
        }
        $submitForm = new Apollo_Submit_Form($this->postType,array());
        $submitForm->method = $this->eRequestedMethod;
        $data = Apollo_App::clean_array_data($data);
        $submitForm->custom_fields_form($data);


    }
}