<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/6/2015
 * Time: 5:30 PM
 */
class YoutubeLink extends  Text{
    protected $type = 'text';

    public function validateProcess(){
        $this->customValidateProcess();
        $message = '';
        if(count( $this->arrRuleFail)>0){
            foreach( $this->arrRuleFail as $rule) {
                $validate = call_user_func_array(
                    array($this->validateClass,'getMessageValidate'),
                    array($rule,$this->eTitle)
                );
                if($rule == Apollo_Form::_FORM_REQUIRED){
                    $validate = __('This youtube link is empty','apollo');
                }
                $message  .='<span class="error" >*'.$validate.'</span >';
            }
            $this->eMessage = $message;
        }
    }
}