<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/6/2015
 * Time: 5:30 PM
 */
class Submit extends  Button{

    public function render(){
        $this->type = 'submit';
        parent::render();
    }
}