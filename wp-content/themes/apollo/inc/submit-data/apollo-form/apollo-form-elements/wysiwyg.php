<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/6/2015
 * Time: 5:30 PM
 */
class Wysiwyg extends  Apollo_Form_Element{

    protected $editorSetting = array();

    /**
     * @param mixed $editorSetting
     */
    public function setEditorSetting($editorSetting = '')
    {
        /**
         * @ticket #18431 0002504: Arts Education Customizations - Remove a restriction that requires the end date to be longer than the start date and modify toolbar editor on FE Program form - item 1,2
         */
        $this->editorSetting = $editorSetting;
    }


    public function render(){
        parent::render();
        $value = $this->eValue;
        $dataAttributes = $this->getDataAttributes();
        if ($this->eTitle) {
            echo '<p>'.$this->eTitle.'</p>';
            echo $this->eMessage;
        }

        if ($this->eMessage && ! $this->eErrorClass) {
            $this->eErrorClass = 'error';
        }

        /**
         * @ticket #18431 0002504: Arts Education Customizations - Remove a restriction that requires the end date to be longer than the start date and modify toolbar editor on FE Program form - item 1,2
         */
        $setting = array(
            'editor_height' => 250,
            'editor_class'  => $this->eClass.' '.$this->eErrorClass
        );

        if(!empty($this->editorSetting)){
            $setting = $this->editorSetting;
        }

        wp_editor($value, $this->eName, $setting);
        //echo '<textarea '.$dataAttributes.' placeholder=\''.$this->ePlaceHolder.'\' name=\''.$this->eName.'\' id=\''.$this->eId.'\' rows=\'5\' class=\' ckeditor '.$this->eClass.' '.$this->eErrorClass. '\'>'.$value.'</textarea>';
        echo '  <div class=\'show-tip\'>'.$this->eTitle.'</div>';
    }
}