<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/6/2015
 * Time: 5:30 PM
 */
class Div extends  Apollo_Form_Element{
    public function render(){
        $dataAtributes =  $this->getDataAttributes();
        echo '<div ' . $dataAtributes .' class="'.$this->eClass.'">';
   }
}
class CloseDiv extends  Apollo_Form_Element{
    public function render(){
        echo '</div>';
    }
}