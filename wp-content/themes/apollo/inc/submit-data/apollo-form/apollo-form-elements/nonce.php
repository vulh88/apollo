<?php

class Nonce extends Apollo_Form_Element
{
    protected $type         = 'nonce';
    protected $eNonceAction = '';
    protected $eNonceName   = '';

    /**
     * @return string
     */
    public function getENonceAction()
    {
        return $this->eNonceAction;
    }

    /**
     * @param string $eNonceAction
     */
    public function setENonceAction($eNonceAction)
    {
        $this->eNonceAction = $eNonceAction;
    }

    /**
     * @return string
     */
    public function getENonceName()
    {
        return $this->eNonceName;
    }

    /**
     * @param string $eNonceName
     */
    public function setENonceName($eNonceName)
    {
        $this->eNonceName = $eNonceName;
    }

    /**
     * Render hidden wp_nonce field
     *
     * @ticket: #10927
     * @doc: https://codex.wordpress.org/Function_Reference/wp_nonce_field
     */
    public function render()
    {
        parent::render();
        wp_nonce_field($this->eNonceAction, $this->eNonceName, false);
    }
}
