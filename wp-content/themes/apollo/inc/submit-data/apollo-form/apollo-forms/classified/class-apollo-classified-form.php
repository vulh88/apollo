<?php

class Apollo_Classified_Form extends Apollo_Form
{
    /* Init form element then set data for them */ 
    public function formInit()
	{
		if($this->isSaveSuccess){
			$this->formRequestMethod = 'get';
			$this->formData = NULL;
			//for addition fields
			$_POST = NULL;
		}
		$arrData = $this->formData;
		$stateVal = '';
		$cityVal = '';
		if( $arrData){
			$stateVal = isset($arrData[Apollo_DB_Schema::_CLASSIFIED_STATE]) ? $arrData[Apollo_DB_Schema::_CLASSIFIED_STATE] : '';
			$cityVal = isset($arrData[Apollo_DB_Schema::_CLASSIFIED_CITY]) ? $arrData[Apollo_DB_Schema::_CLASSIFIED_CITY]: '';
		} else {
			$list_states = Apollo_App::getListState();
			$default_states =  of_get_option(Apollo_DB_Schema::_APL_DEFAULT_STATE);
			$cityVal = of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY);
			if( $default_states && in_array($default_states,$list_states) ) {
				$stateVal = $default_states;
			} else if(count($list_states) == 1) {
				$stateVal = key($list_states);
			}
		}

		$selectedOrg = $this -> selectIdOrg($arrData);

		$orgSelect2 = apl_instance('APL_Lib_Helpers_Select2', array(
			'post_type' => Apollo_DB_Schema::_ORGANIZATION_PT,
			'selected_item' => $selectedOrg,
		));
		$orgs = $orgSelect2->getItemsInPostTypeToDropDownList(array(
			'post_status' => array('publish','pending'),
			'selected_item'	=> $selectedOrg,
		));

		$orgValues = array(
			'list_item' => $orgs,
			'selected_item' => $selectedOrg,
			'is_post_type_list'	=> true,
		);

		$requirementState = of_get_option(Apollo_DB_Schema::_CLASSIFIED_ENABLE_REQUIREMENTS_STATE, 1);
		$requirementCity = of_get_option(Apollo_DB_Schema::_CLASSIFIED_ENABLE_REQUIREMENTS_CITY, 1);
		$requirementZip = of_get_option(Apollo_DB_Schema::_CLASSIFIED_ENABLE_REQUIREMENTS_ZIP, 0);
		$enableOtherState = of_get_option(Apollo_DB_Schema::_CLASSIFIED_FE_ENABLE_OTHER_STATE, 0);
		$enableOtherCity = of_get_option(Apollo_DB_Schema::_CLASSIFIED_FE_ENABLE_OTHER_CITY, 0);
		$enableOtherZip = of_get_option(Apollo_DB_Schema::_CLASSIFIED_FE_ENABLE_OTHER_ZIP, 0);

        /**
         * @ticket #19642: [CF] 20190402 - Auto fill the 'contact information' name and email - Item 6
         */
        $contactName = isset($arrData[Apollo_DB_Schema::_CLASSIFIED_CONTACT_NAME]) ? $arrData[Apollo_DB_Schema::_CLASSIFIED_CONTACT_NAME] : '';
        $current_user = wp_get_current_user();
        if (!$contactName) {
            $contactName = !empty($current_user->display_name) ? $current_user->display_name : '';
        }

        $contactEmail = isset($arrData[Apollo_DB_Schema::_CLASSIFIED_CONTACT_EMAIL]) ? $arrData[Apollo_DB_Schema::_CLASSIFIED_CONTACT_EMAIL] : '';
        if (!$contactEmail) {
            $contactEmail = !empty($current_user->user_email) ? $current_user->user_email : '';
        }

		$formElementItems = array(
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

            'ID' => array(
                'type' => 'Hidden',
                'name' => 'ID',
                'place_holder' => __('Name', 'apollo'),
                'value' => isset($arrData['ID']) ? $arrData['ID'] : '',
                'class' => 'inp inp-txt',
                'title' => __('Name', 'apollo'),
                'validate' => true,
                'container' => '',
            ),

            'post_status' => array(
                'type' => 'Hidden',
                'name' => 'post_status',
                'place_holder' => __('Post status', 'apollo'),
                'value' => isset($arrData['post_status']) ? $arrData['post_status'] : 'pending',
                'class' => '',
                'title' => __('Post status', 'apollo'),
                'validate' => true,
            ),

            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'el-blk full',
                'container' => '',
            ),

            'post_title' => array(
                'type' => 'Text',
                'name' => 'post_title',
                'place_holder' => __('Classified Name (*)', 'apollo'),
                'value' => isset($arrData['post_title']) ? $arrData['post_title'] : '',
                'class' => 'inp inp-txt validate[required]',
                'data_attributes' => array(
                    'data-errormessage-value-missing="'.__('Classified name is required','apollo').'"'
                ),
                'title' => __('Classified Name (*)', 'apollo'),
                'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_REQUIRED
                        )
                ),
                'container' =>'',
            ),

            array(
            'type' => 'CloseDiv',
            'name' => 'title',
            'value' => '',
            'class' => '',
            'container' => '',
            ),

            array(
            'type' => 'Div',
            'name' => 'title',
            'value' => '',
            'class' => 'el-blk full',
            'container' => '',
            ),

            Apollo_DB_Schema::_APOLLO_CLASSIFIED_ORGANIZATION => array(
                'type' => 'Select',
                'name' => Apollo_DB_Schema::_APOLLO_CLASSIFIED_ORGANIZATION,
                'class' => 'event chosen',
                'title' => __('Select Organization/Business', 'apollo'),
                'container' => '',
                'value' => $orgValues,
                'id' => Apollo_DB_Schema::_APOLLO_CLASSIFIED_ORGANIZATION,
                'data_attributes' => array(
                    'data-post-type="'.Apollo_DB_Schema::_ORGANIZATION_PT.'"',
                    'data-default-option="'.__('Select Organization/Business', 'apollo').'""',
                    'data-enable-remote="'.($orgSelect2->getEnableRemote() ? 1 : 0).'"',
                    'data-source-url="apollo_get_remote_data_to_select2_box"',
                ),
            ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'el-blk full',
                'container' => '',
            ),

            Apollo_DB_Schema::_APL_CLASSIFIED_TMP_ORG => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_APL_CLASSIFIED_TMP_ORG,
                'place_holder' => __('Other Organization/Business', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_APL_CLASSIFIED_TMP_ORG]) ? $arrData[Apollo_DB_Schema::_APL_CLASSIFIED_TMP_ORG] : '',
                'class' => 'inp inp-txt',
                'container' => '',
                'title' => sprintf('%s (%s)', __('Other Organization/Business', 'apollo'), __('Enter other organization/business name if not available in above drop menu', 'apollo')),
            ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'el-blk full',
                'container' => '',
            ),

            Apollo_DB_Schema::_CLASSIFIED_ADDRESS => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_CLASSIFIED_ADDRESS,
                'place_holder' => __('Address', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_CLASSIFIED_ADDRESS]) ? $arrData[Apollo_DB_Schema::_CLASSIFIED_ADDRESS] : '',
                'class' => 'inp inp-txt',
                'container' => '',
                'title' => __('Address', 'apollo'),

            ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'el-blk',
                'container' => '',
            ),


            Apollo_DB_Schema::_CLASSIFIED_STATE => array(
                'type' => 'Select',
                'name' => Apollo_DB_Schema::_CLASSIFIED_STATE,
                'place_holder' => __('State (*)', 'apollo'),
                'value' => array(
                    'list_item' => Apollo_App::getStateByTerritory($requirementState ? true : false, false),
                    'selected_item' => $stateVal
                ),
                'class' => 'event apl-territory-state custom-validate-field',
                'data_attributes' => $requirementState ? array(
                    'data-custom-validate="select2"',
                    'data-error-message="'.__('State is required','apollo').'"'
                ) : '',
                'title' => sprintf( __( 'Select State %s ', 'apollo' ), $requirementState ? '(*)' : '' ),
                'id' => 'apl-us-states',
                'validate' => array(
                    'rule' => array(
                        $requirementState ? Apollo_Form::_FORM_REQUIRED : ''
                    )
                ),
            ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'el-blk',
                'container' => '',
            ),

            Apollo_DB_Schema::_CLASSIFIED_CITY => array(
                'type' => 'Select',
                'name' => Apollo_DB_Schema::_CLASSIFIED_CITY,
                'place_holder' => __('City (*)', 'apollo'),
                'class' => 'event apl-territory-city custom-validate-field',
                'data_attributes' => $requirementCity ? array(
                    'data-custom-validate="select2"',
                    'data-error-message="'.__('City is required','apollo').'"'
                ) : '',
                'title' => sprintf( __( 'Select City %s', 'apollo' ), $requirementCity ? '(*)' : '' ),
                'container' => 'hafl fl',
                'id' => 'apl-us-cities',
                'value' => array(
                    'list_item' => Apollo_App::getCityByTerritory($requirementCity ? true : false, $stateVal, false),
                    'selected_item' => $cityVal,
                ),
                'validate' => array(
                    'rule' => array(
                        $requirementCity ? Apollo_Form::_FORM_REQUIRED : ''
                    )
                ),
            ),

            Apollo_DB_Schema::_CLASSIFIED_ZIP => array(
                'type' => 'Select',
                'name' => Apollo_DB_Schema::_CLASSIFIED_ZIP,
                'place_holder' => $requirementZip ? __('Select Zip Code (*)', 'apollo') : __('Select Zip Code', 'apollo'),
                'value' => array(
                    'list_item' => Apollo_App::getZipByTerritory(true, $stateVal, $cityVal, false),
                    'selected_item' => isset($arrData[Apollo_DB_Schema::_CLASSIFIED_ZIP]) ? $arrData[Apollo_DB_Schema::_CLASSIFIED_ZIP] : ''
                ),
                'id' => 'apl-us-zip',
                'class' => sprintf('event apl-territory-zipcode %s', $requirementZip ? 'custom-validate-field' : ''),
                'data_attributes' => array(
                    'data-custom-validate="select2"',
                    'data-error-message="'.__('Zip Code is required','apollo').'"'
                ),
                'title' => $requirementZip ? __('Select Zip Code (*)', 'apollo') : __('Select Zip Code', 'apollo'),
                'validate' => array(
                    'rule' => array(
                        $requirementZip ? Apollo_Form::_FORM_REQUIRED : ''
                    )
                ),
                'container' => 'hafl fr',
            ),
        );

        /** @ticket #19687
         * [CF] 20190408 - Add text to create more separation between the address drop menus and the other address fields
         */
        if ($enableOtherState || $enableOtherCity || $enableOtherZip) {
            $formElementItems = array_merge($formElementItems, array(
                array(
                    'type' => 'Subtitle',
                    'value' => __("If your city, state and/or zip are not listed above, please fill in the fields below.", 'apollo'),
                    'class' => 'apl-subtitle-bold',
                )
            ));
        }

        /** @Ticket #19640 */
        if ($enableOtherState) {
            $formElementItems = array_merge($formElementItems, array(
                Apollo_DB_Schema::_CLASSIFIED_TMP_STATE => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_CLASSIFIED_TMP_STATE,
                    'place_holder' => __('Other State', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_CLASSIFIED_TMP_STATE]) ? $arrData[Apollo_DB_Schema::_CLASSIFIED_TMP_STATE] : '',
                    'class' => 'inp inp-txt',
                    'title' => __('Other State', 'apollo'),

                )
            ));
        }

        if ($enableOtherCity) {
            $formElementItems = array_merge($formElementItems, array(
                Apollo_DB_Schema::_CLASSIFIED_TMP_CITY => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_CLASSIFIED_TMP_CITY,
                    'place_holder' => __('Other City', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_CLASSIFIED_TMP_CITY]) ? $arrData[Apollo_DB_Schema::_CLASSIFIED_TMP_CITY] : '',
                    'class' => 'inp inp-txt',
                    'title' => __('Other City', 'apollo'),

                )
            ));
        }

        if ($enableOtherZip) {
            $formElementItems = array_merge($formElementItems, array(
                Apollo_DB_Schema::_CLASSIFIED_TMP_ZIP => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_CLASSIFIED_TMP_ZIP,
                    'place_holder' => __('Other Zip Code', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_CLASSIFIED_TMP_ZIP]) ? $arrData[Apollo_DB_Schema::_CLASSIFIED_TMP_ZIP] : '',
                    'class' => 'inp inp-txt',
                    'title' => __('Other Zip Code', 'apollo'),

                )
            ));
        }

        $moreClassifiedElement = array(

            Apollo_DB_Schema::_CLASSIFIED_REGION => array(
                'type' => 'Select',
                'name' => Apollo_DB_Schema::_CLASSIFIED_REGION,
                'class' => 'event',
                'value' => array(
                    'list_item' => Apollo_App::get_regions(),
                    'selected_item' => isset($arrData[Apollo_DB_Schema::_CLASSIFIED_REGION]) ? $arrData[Apollo_DB_Schema::_CLASSIFIED_REGION] : ''
                ),
                'id' => 'apl-regions',
                'display' => Apollo_App::showRegion(),
                'title' => __('Select Region', 'apollo'),
            ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            array(
                'type' => 'Subtitle',
                'value' => __("Classified Contact Data", 'apollo'),
                'class' => 'apl-subtitle-bold',
            ),

            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'el-blk full',
                'container' => '',
            ),

            Apollo_DB_Schema::_CLASSIFIED_NAME => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_CLASSIFIED_NAME,
                'place_holder' => __('Name', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_CLASSIFIED_NAME]) ? $arrData[Apollo_DB_Schema::_CLASSIFIED_NAME] : '',
                'class' => 'inp inp-txt',
                'container' => '',
                'title' => __('Name', 'apollo'),
            ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'el-blk full',
                'container' => '',
            ),
            Apollo_DB_Schema::_CLASSIFIED_EMAIL => $this->renderEmail(),
            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            array(
            'type' => 'Div',
            'name' => 'title',
            'value' => '',
            'class' => 'el-blk full',
            'container' => '',
            ),

            Apollo_DB_Schema::_CLASSIFIED_WEBSITE_URL => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_CLASSIFIED_WEBSITE_URL,
                'place_holder' => __('Website URL', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_CLASSIFIED_WEBSITE_URL]) ? $arrData[Apollo_DB_Schema::_CLASSIFIED_WEBSITE_URL] : '',
                'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]]',
                'container' => '',
                'title' => __('Website URL', 'apollo'),
                'validate' => array(
                    'rule' => array(
                        Apollo_Form::_FORM_URL
                    )
                ),
            ),
            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'el-blk',
                'container' => '',
            ),


            Apollo_DB_Schema::_CLASSIFIED_PHONE => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_CLASSIFIED_PHONE,
                'place_holder' => __('Phone', 'apollo'),
                'container' => 'hafl fl',
                'value' => isset($arrData[Apollo_DB_Schema::_CLASSIFIED_PHONE]) ? $arrData[Apollo_DB_Schema::_CLASSIFIED_PHONE] : '',
                'class' => 'inp inp-txt',
                'title' => __('Phone', 'apollo'),

            ),

            Apollo_DB_Schema::_CLASSIFIED_FAX => array(
                'type' => 'text',
                'name' => Apollo_DB_Schema::_CLASSIFIED_FAX,
                'place_holder' => __('Fax', 'apollo'),
                'class' => 'inp inp-txt',
                'title' => __('Fax', 'apollo'),
                'container' => 'hafl fr',
                'value' => isset($arrData[Apollo_DB_Schema::_CLASSIFIED_FAX]) ? $arrData[Apollo_DB_Schema::_CLASSIFIED_FAX] : '',
            ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'el-blk full',
                'container' => '',
            ),

            Apollo_DB_Schema::_CLASSIFIED_EXP_DATE => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_CLASSIFIED_EXP_DATE,
                'place_holder' => __('Expiration date  (date your classified will be removed from website) (*)', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_CLASSIFIED_EXP_DATE]) ? $arrData[Apollo_DB_Schema::_CLASSIFIED_EXP_DATE] : '',
                'class' => 'inp inp-txt  apl-validation-datepicker validate[required]',
                'data_attributes' => array(
                    'data-errormessage-value-missing="'.__('Expiration Date is required (The date of your classified will be removed from website.)','apollo').'"'
                ),
                'id' =>Apollo_DB_Schema::_CLASSIFIED_EXP_DATE,
                'container' => 'date-picker',
                'title' => __('Expiration date  (date your classified will be removed from website) (*)', 'apollo'),
                'validate' => array(
                    'rule' => array(
                        Apollo_Form::_FORM_REQUIRED,
                        Apollo_Form::_FORM_DATE_LARGER_NOW,
                    )
                ),
            ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'el-blk full',
                'container' => '',
            ),

            Apollo_DB_Schema::_CLASSIFIED_DEADLINE_DATE => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_CLASSIFIED_DEADLINE_DATE,
                'place_holder' => __('Deadline date  ', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_CLASSIFIED_DEADLINE_DATE]) ? $arrData[Apollo_DB_Schema::_CLASSIFIED_DEADLINE_DATE] : '',
                'class' => 'inp inp-txt  apl-validation-datepicker ',
                'id' =>Apollo_DB_Schema::_CLASSIFIED_DEADLINE_DATE,
                'container' => 'date-picker',
                'title' => __('Deadline date  ', 'apollo'),
                'validate' => array(
                    'rule' => array(
                        Apollo_Form::_FORM_DATE_LARGER_NOW,
                    )
                ),
            ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            'post_content' => array(
                'type' => 'Wysiwyg',
                'name' => 'post_content',
                'place_holder' => __('Description/Bio (*)', 'apollo'),
                'value' => Apollo_App::convertTinyCMEToCkEditor(isset($arrData['post_content']) ? $arrData['post_content'] : ''),
                'class' => 'inp-desc-event custom-validate-field',
                'id' => 'biography-description',
                'data_attributes' => array(
                    'data-custom-validate="wysiwyg"',
                    'data-error-message="'.__('Description/Bio is required','apollo').'"',
                    'data-id="biography-description"'
                ),
                'title' => __('Description/Bio (*)', 'apollo'),
                'validate' => array(
                    'rule' => array(
                        Apollo_Form::_FORM_REQUIRED
                    )
                ),
            ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            //category
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

            //title
            array(
                'type' => 'Title',
                'name' => 'title',
                'value' => __('Classified types (*)', 'apollo'),
                'class' => 'title-bar-blk',
            ),
            //end title

            Apollo_DB_Schema::_APL_CLASSIFIED_TERM => array(
                'title' => __('Classified types (*)', 'apollo'),
                'type' => 'Category_Group',
                'name' => Apollo_DB_Schema::_APL_CLASSIFIED_TERM,
                'class' => 'access-listing custom-validate-field',
                'data_attributes' => array(
                    'data-custom-validate="multi-checkbox"',
                    'data-error-message="'.__('Classified type is required','apollo').'"'
                ),
                'value' => isset($arrData[Apollo_DB_Schema::_APL_CLASSIFIED_TERM]) ? $arrData[Apollo_DB_Schema::_APL_CLASSIFIED_TERM] : '',
                'post_type' => Apollo_DB_Schema::_CLASSIFIED,
                'container' => 'access-list artist',
            ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            //contact info
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

            //title
            array(
                'type' => 'Title',
                'name' => 'title',
                'value' => __('Contact Info', 'apollo'),
                'class' => 'title-bar-blk ',
            ),
            //end title

            Apollo_DB_Schema::_CLASSIFIED_CONTACT_NAME => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_CLASSIFIED_CONTACT_NAME,
                'place_holder' => __('Contact name (*)', 'apollo'),
                'value' => trim($contactName),
                'class' => 'inp inp-txt validate[required]',
                'data_attributes' => array(
                    'data-errormessage-value-missing="'.__('Contact Name is required','apollo').'"'
                ),
                'title' => __('Contact name (*)', 'apollo'),
                'validate' => array(
                    'rule' => array(
                        Apollo_Form::_FORM_REQUIRED
                    )
                ),
            ),

            Apollo_DB_Schema::_CLASSIFIED_CONTACT_EMAIL => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_CLASSIFIED_CONTACT_EMAIL,
                'place_holder' => __('Contact email (*)', 'apollo'),
                'value' => $contactEmail,
                'class' => 'inp inp-txt validate[funcCall[disableSpace], required,custom[email]]',
                'data_attributes' => array(
                    'data-errormessage-value-missing="'.__('Contact Email is required','apollo').'"'
                ),
                'title' => __('Contact email (*)', 'apollo'),
                'validate' => array(
                    'rule' => array(
                        Apollo_Form::_FORM_REQUIRED,
                        Apollo_Form::_FORM_EMAIL
                    )
                ),
            ),

            Apollo_DB_Schema::_CLASSIFIED_CONTACT_PHONE => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_CLASSIFIED_CONTACT_PHONE,
                'place_holder' => __('Contact phone', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_CLASSIFIED_CONTACT_PHONE]) ? $arrData[Apollo_DB_Schema::_CLASSIFIED_CONTACT_PHONE] : '',
                'class' => 'inp inp-txt',
                'title' => __('Contact phone', 'apollo'),
            ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            //addition cf
            'Additional_Form' => array(
                'type' => 'Classified_Additional_fields',
                'name' => '',
                'class' => 'full-custom',
                'id' => '',
                'container' => 'artist-blk',
                'value' => array(
                    'post_type' => Apollo_DB_Schema::_CLASSIFIED,
                    'post_data' => $arrData,
                    'post_id' => isset($arrData['ID']) ? $arrData['ID'] : '',
                )
            ),

            //images & photos
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

            //title
            array(
                'type' => 'Title',
                'name' => 'title',
                'value' => __('Images', 'apollo'),
                'class' => 'title-bar-blk ',
            ),
            //end title
				
            'photo_tab' => array(
                'type' => 'Tab',
                'place_holder' =>__('Name','apollo'),
                'value' =>  isset($arrData['ID']) ? $arrData['ID'] : '',
                'container' => 'el-blk',
                'children' => array(
                    'primary_photo' => array(
                        'type' => 'ShortCode',
                        'name' => 'primary_photo',
                        'title' => __('PRIMARY IMAGE','apollo'),
                        'target' => Apollo_DB_Schema::_CLASSIFIED,
                        'value' =>  'apollo_upload_and_drop ',
                    ),
                    'gallery_photo' => array(
                        'type' => 'ShortCode',
                        'name' => 'gallery_photo',
                        'title' => __('GALLERY','apollo'),
                        'target' => Apollo_DB_Schema::_CLASSIFIED,
                        'value' =>  'apollo_upload_gallery ',
                    ),
                )
            ),
            
			array(
					'type' => 'CloseDiv',
					'name' => 'title',
					'value' => '',
					'class' => '',
					'container' => '',
				),

        );

        $formElementItems = array_merge($formElementItems, $moreClassifiedElement);

        /*
        * @Ticket 15203
        * generate select tags
        */
        if(has_filter("apl_pbm_fe_render_select_tags_section")) {
            $formElementItems = apply_filters('apl_pbm_fe_render_select_tags_section', [
                'post_id' => $arrData['ID'],
                'form'    => $formElementItems
            ]);
        }

        /*@ticket #16639:  Upload PDF files to the FE and Admin Classified forms.*/
        $documentUpload = array(
            'document' => array(
                'type' => 'Document_Group',
                'container' => 'classified-document-upload',
                'value' => array(
                    'post_id' => $arrData['ID'],
                    'max_upload_pdf' => of_get_option(Apollo_DB_Schema::_MAX_UPLOAD_PDF_CLASSIFIED, Apollo_Display_Config::_MAX_UPLOAD_PDF_DEFAULT)
                )
            ),
        );
        $formElementItems = array_merge($formElementItems, $documentUpload);

        $formElementItems['Submit_btn'] = array(
					'type' => 'Submit',
					'class' => 'submit-btn submit-form-with-validation-engine',
					'title' => __('SUBMIT RECORD', 'apollo')
				);
        $formElementItems[] =
				array(
					'type' => 'CloseDiv',
					'name' => 'title',
					'value' => '',
					'class' => 'artist-blk',
					'container' => '',
				);
		$this->elements = $formElementItems;
	}

	public function formHeader()
	{


		$id = 0;
		if(intval( get_query_var( '_apollo_classified_id' ) )){
			$id = get_query_var( '_apollo_classified_id' );
		}
		?>
			<div class="dsb-welc">
				<h1 ><?php
					if ($id == 0) {
						_e('Add Classified', 'apollo');
					} else {
						_e('Update Classified', 'apollo');
					}
					?>
					<?php if (!empty($this->formData['ID']) && isset($this->formData['post_status']) &&
						$this->formData['post_status'] == 'publish' && $this->formData
					): ?>
						<a class="view-page-link" target="_blank"
						   href="<?php echo get_the_permalink($this->formData['ID']) ?>">(<?php _e('View Page', 'apollo') ?>
							)</a>
					<?php endif; ?>
				</h1>
			</div>
        <?php
        parent::formHeader();
    }

    /*Process form submit*/
    public function postMethod()
    {
		// validate nonce
		if ( $this->validateNonce() ) {
			$this->validateClassObject->save($this->formData);
		}
    }

    /*process form get method*/ 
    public function getMethod()
	{

		$this->successMessage = __(Apollo_Form_Static::_SUCCESS_ADD_CLASSIFIED_MESSAGE, 'apollo');
		$this->failMessage = __(Apollo_Form_Static::_FAIL_ADD_CLASSIFIED_MESSAGE, 'apollo');

		if($this->formRequestMethod == 'get') {
			if (isset($_SESSION['apollo'][Apollo_DB_Schema::_CLASSIFIED . Apollo_SESSION::SUFFIX_ADD_GALLERY])) {
				unset($_SESSION['apollo'][Apollo_DB_Schema::_CLASSIFIED . Apollo_SESSION::SUFFIX_ADD_GALLERY]);
			}
			if (isset($_SESSION['apollo'][Apollo_DB_Schema::_CLASSIFIED . Apollo_SESSION::SUFFIX_ADD_PIMAGE])) {
				unset($_SESSION['apollo'][Apollo_DB_Schema::_CLASSIFIED . Apollo_SESSION::SUFFIX_ADD_PIMAGE]);
			}
		}

		//for edit
		if(intval( get_query_var( '_apollo_classified_id' ) )){
			$id = get_query_var( '_apollo_classified_id' );
		}
	
		if (isset($id) && !empty($id)) {
			$postStatus = get_post_status($id);
			if($postStatus == 'publish' ||  $postStatus == 'pending' || $postStatus == 'draft'){
				$postInfo = get_post($id, ARRAY_A);
				$status = $postInfo['post_status'];
				$id = $postInfo['ID'];
				//get all meta;
				$metaAddr 	= get_apollo_meta($id, Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS, true);
				$metaData 	= get_apollo_meta($id, Apollo_DB_Schema::_APL_CLASSIFIED_DATA, true);
				$metaOrg  	= get_apollo_meta($id, Apollo_DB_Schema::_APOLLO_CLASSIFIED_ORGANIZATION, true);
				$metaTmpOrg	= get_apollo_meta($id, Apollo_DB_Schema::_APL_CLASSIFIED_TMP_ORG, true);
				if (!is_array($metaAddr) && !empty($metaAddr))
					$metaAddr = unserialize($metaAddr);
				if (!is_array($metaData) && !empty($metaData))
					$metaData = unserialize($metaData);
			
				if (is_array($metaAddr)) {
					foreach ($metaAddr as $key => $value) {
						$postInfo[$key] = $value;
					}
				}
				if (is_array($metaAddr)) {
					foreach ($metaData as $key => $value) {
						$postInfo[$key] = $value;
					}
				}
				$postInfo[Apollo_DB_Schema::_APOLLO_CLASSIFIED_ORGANIZATION] = $metaOrg;
				$postInfo[Apollo_DB_Schema::_APL_CLASSIFIED_TMP_ORG] = $metaTmpOrg;
				$postInfo[Apollo_DB_Schema::_APL_CLASSIFIED_TERM] = wp_get_post_terms($id, Apollo_DB_Schema::_CLASSIFIED . '-type', array('fields' => 'ids'));
				$postInfo['ID'] = $id;
                /*@ticket #16639:  Upload PDF files to the FE and Admin Classified forms.*/
                $postInfo['upload_pdf'] = get_apollo_meta($id, 'upload_pdf');
				$this->successMessage = __(Apollo_Form_Static::_SUCCESS_MESSAGE, 'apollo');
				$this->failMessage = __(Apollo_Form_Static::_FAIL_MESSAGE, 'apollo');
				$this->formData = $postInfo;
			}
	
		}
		
}
    //Show message Fail Submit

	//override parent for additional field
	public function formSubmitAction()
	{
		$addFieldsValidateClass = new Apollo_Submit_Form(Apollo_DB_Schema::_CLASSIFIED, array());
		$this->isSaveSuccess = false;
		$this->isSubmitFail = false;
		//post method

		if ($this->formRequestMethod == 'post') {
			$this->cleanPost();
			$this->isSubmitFail = true;
			if ($this->validateClassObject->isValidAll($this->mergerPostRule())) {
				$this->isSaveSuccess = true;
				$this->isSubmitFail = false;
			}
			if (!$addFieldsValidateClass->isValidAll($this->mergePostAFields())) {
				$this->isSaveSuccess = false;
				$this->isSubmitFail = true;
			}
			//save data
			if ($this->isSaveSuccess){
				$this->id = $this->postMethod();
			}

			//end save data
		}
	}

	public function mergePostAFields()
	{
		$data = array();
		$group_fields = Apollo_Custom_Field::get_group_fields(Apollo_DB_Schema::_CLASSIFIED);
		foreach ($group_fields as $group) {
			$fields = isset($group['fields']) ? $group['fields'] : array();
			if (count($fields) > 0) {
				foreach ($fields as $field) {
					$data[$field->name] = isset($_POST[$field->name]) ? $_POST[$field->name] : '';
				}
			}
		}
		return $data;
	}
    
	//Hong return id org selected
	public function selectIdOrg($arrData){

		if ( isset($arrData[Apollo_DB_Schema::_APOLLO_CLASSIFIED_ORGANIZATION]) ) {
			$idOrg = $arrData[Apollo_DB_Schema::_APOLLO_CLASSIFIED_ORGANIZATION];
		} else {
            $idOrg = Apollo_User::getCurrentAssociatedID(Apollo_DB_Schema::_ORGANIZATION_PT);
		}

		$org = get_post($idOrg);

		return $org ? $idOrg : 0;
	}
	
	//clear data after post success
	public function formSubmitProcess(){
        parent::formSubmitProcess();
		$id = 0;
		if(intval( get_query_var( '_apollo_classified_id' ) )){
			$id = get_query_var( '_apollo_classified_id' );
		}
		//if is not update case,init form again to clear data post
		if($id == 0){
			if($this->isSaveSuccess){
				$this->formInit();
			}
		}

    }

	/**
	 * Set nonce info
	 *
	 * @return void
	 */
	public function setNonceInfo()
	{
		$this->nonceName   = Apollo_Const::_APL_CLASSIFIED_NONCE_NAME;
		$this->nonceAction = Apollo_Const::_APL_NONCE_ACTION_CLASSIFIED_PAGE;
	}

	/**
     * Render email field
     * @return array
     */
	private function renderEmail(){
	    if(of_get_option(Apollo_DB_Schema::_CLASSIFIED_ENABLE_REQUIREMENTS_EMAIL)){
	        return array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_CLASSIFIED_EMAIL,
                'place_holder' => __('Email (*)', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_CLASSIFIED_EMAIL]) ? $arrData[Apollo_DB_Schema::_CLASSIFIED_EMAIL] : '',
                'class' => 'inp inp-txt validate[funcCall[disableSpace],required,custom[email]]',
                'data_attributes' => array(
                    'data-errormessage-value-missing="'.__('Email is required','apollo').'"'
                ),
                'container' => '',
                'title' => __('Email (*)', 'apollo'),
                'validate' => array(
                    'rule' => array(
                        Apollo_Form::_FORM_REQUIRED,
                        Apollo_Form::_FORM_EMAIL,
                    )
                )
            );
        }else{
	        return array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_CLASSIFIED_EMAIL,
                'place_holder' => __('Email', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_CLASSIFIED_EMAIL]) ? $arrData[Apollo_DB_Schema::_CLASSIFIED_EMAIL] : '',
                'class' => 'inp inp-txt validate[funcCall[disableSpace],custom[email]]',
                'data_attributes' => array(
                    'data-errormessage-value-missing="'.__('Email is required','apollo').'"'
                ),
                'container' => '',
                'title' => __('Email', 'apollo'),
                'validate' => array(
                    'rule' => array(
                        Apollo_Form::_FORM_EMAIL,
                    )
                )
            );
        }
    }
}