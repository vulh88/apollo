<?php

class Apollo_Submit_Grant_Education extends Apollo_Submit_Form {

    public $nonceName     = '';
    public $nonceAction   = '';
    public $isNonceFailed = false;

    public function __construct() {
        $post_type         = Apollo_DB_Schema::_GRANT_EDUCATION;
        $this->nonceName   = Apollo_Const::_APL_GRANT_EDUCATION_NONCE_NAME;
        $this->nonceAction = Apollo_Const::_APL_NONCE_ACTION_GRANT_EDUCATION_PAGE;

        $rules = array(
           
        );
        parent::__construct($post_type, $rules);
    }
    
    public function save($arrData, $arrCFData)
    {
        // ticket #11047: validate nonce field
        if ( !isset($_POST[$this->nonceName]) || !wp_verify_nonce($_POST[$this->nonceName], $this->nonceAction) ) {
            $this->isNonceFailed = true;
            return false;
        }

        $post = array(
            'ID'            => '',
            'post_title'    => __('No title', 'apollo'),
            'post_author'   => get_current_user_id(),
            'post_type'     => $this->post_type,
            'post_status'   => 'publish',
        );
        
        // UPDATE POST
        $id = wp_insert_post( $post );
        
        // Insert meta data
        update_apollo_meta($id, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA, Apollo_App::clean_array_data($arrCFData));
        
        $page_link = get_permalink(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_SUBMIT_GRANT_EDUCATION_DONE));
        
        do_action( 'apollo_email_grant_education', $id, get_option('admin_email') );
        
        // Go to the successful register page
        if ( $page_link ) {
            $_SESSION['apollo_grant_edu'] = 1;
            wp_safe_redirect($page_link);
        }
        
    }
    
    public function getDataFromRequest() {
        return array(
//            
        );
    }
    
}