<?php

class Apollo_Submit_Classified extends Apollo_Submit_Form {
    public function __construct($rule) {
        $this->post_type = Apollo_DB_Schema::_CLASSIFIED;
        $this->rules = $rule;
        $this->nonceName   = Apollo_Const::_APL_ADD_CLASSIFIED_NONCE_NAME;
        $this->nonceAction = Apollo_Const::_APL_NONCE_ACTION_ADD_CLASSIFIED_PAGE;
    }
    public function save($arrData, $arrCustomField = array(), $arrCFSearch = array()) {

        if(isset($arrData['ID']) && $arrData['ID'] != ''){
            Apollo_Permission_System::processPermission(array(
                'type' => 'access',
                'user_id' => get_current_user_id(),
                'event_id' => $arrData['ID'],
                'fail_action' => 'redirect',
                'fail_data' => array(

                ),
                'post_type' => Apollo_DB_Schema::_CLASSIFIED
            ));
        }
        
		$pid = 0;
        $post = array(
            'ID'            => isset($arrData['ID'])?$arrData['ID']:'',
            'post_title'    => Apollo_App::clean_data_request( $arrData['post_title'] ),
            'post_content'  => Apollo_App::convertCkEditorToTinyCME($arrData['post_content']),
            'post_author'   => get_current_user_id(),
            'post_type'     => Apollo_DB_Schema::_CLASSIFIED,
        );
        $postId = $post['ID'];

        if(isset($post['ID']) && !empty($post['ID'])){
            unset($post['post_author']);

            // Do not change the post status when editing a classified
            if (isset($post['post_status'])) {
                unset($post['post_status']);
            }

            wp_update_post($post);
            $pid = $postId;
            
            // Keep icons due to it not exist in the front end form
            $arrData[Apollo_DB_Schema::_APL_CLASSIFIED_POST_ICONS] = Apollo_App::apollo_get_meta_data($postId, Apollo_DB_Schema::_APL_CLASSIFIED_POST_ICONS, Apollo_DB_Schema::_APL_CLASSIFIED_DATA );
        }
        else{
            /** @Ticket #13795 - this status will be updated in the preview listing after user submits the listing */
            $post['status'] = 'draft';

            $postId = wp_insert_post($post);
        }
        
        if(isset($arrData[Apollo_DB_Schema::_APL_CLASSIFIED_TERM])){
            wp_set_post_terms( $postId, $arrData[Apollo_DB_Schema::_APL_CLASSIFIED_TERM],Apollo_DB_Schema::_CLASSIFIED.'-type' );
        }
        if(isset($arrData[Apollo_DB_Schema::_CLASSIFIED_CITY]) && !empty($arrData[Apollo_DB_Schema::_CLASSIFIED_CITY])){
            update_apollo_meta($postId, Apollo_DB_Schema::_APL_CLASSIFIED_CITY, $arrData[Apollo_DB_Schema::_CLASSIFIED_CITY]);
        }

        unset($arrData[Apollo_DB_Schema::_APL_CLASSIFIED_TERM]);
        $arrayMetaGroup = $this->getMetaGroup($arrData,$postId);
        //$arrayMetaGroup[Apollo_DB_Schema::_APL_ORG_DATA][ Apollo_DB_Schema::_APL_ORG_POST_ICONS] = $icons;
        foreach($arrayMetaGroup as $group => $value){
            if($group != Apollo_DB_Schema::_APL_CLASSIFIED_TERM){
                //meta
                update_apollo_meta($postId, $group, $value);
            }
        }

        //update custom field
        $group_fields = Apollo_Custom_Field::get_group_fields( Apollo_DB_Schema::_CLASSIFIED );
        $customFieldValue = array();
        if(is_array($group_fields) && !empty($group_fields)){
            foreach($group_fields as $group){
                if(isset($group['fields']) && !empty($group['fields'])){
                    foreach($group['fields'] as $field){
                        if(isset($arrData[$field->name])){
                            if(is_array( $arrData[$field->name])){
                                $customFieldValue[$field->name] = $arrData[$field->name];
                            }
                            elseif($field->cf_type == 'wysiwyg'){
                                $customFieldValue[$field->name] = Apollo_App::convertCkEditorToTinyCME($arrData[$field->name]);
                            }
                            else{
                                $customFieldValue[$field->name] = Apollo_App::clean_data_request($arrData[$field->name]);
                            }

                            if(Apollo_Custom_Field::has_explain_field($field)){
                                $exFieldName = $field->name.'_explain';
                                $customFieldValue[$exFieldName] = Apollo_App::clean_data_request($arrData[$exFieldName]);
                            }

                            if(Apollo_Custom_Field::has_other_choice($field)){
                                $exFieldName = $field->name.'_other_choice';
                                $customFieldValue[$exFieldName] = Apollo_App::clean_data_request($arrData[$exFieldName]);
                            }

                        } else if ($postId) {
                            $customFieldValue[$field->name] = Apollo_App::apollo_get_meta_data($postId, $field->name, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA);
                        }
                    }
                }
            }
        }
        update_apollo_meta($postId, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA, serialize($customFieldValue));

        // SAVE FEATURE IMAGE

        /** @Ticket #12729 - Remove session */
        self::saveFeaturedImageHandler($postId, Apollo_DB_Schema::_CLASSIFIED);


        /// SAVE GALLERY
        self::saveGalleryHandler($postId, Apollo_DB_Schema::_CLASSIFIED);

        /*@ticket #16639:  Upload PDF files to the FE and Admin Classified forms.*/
        $this->_process_upload_pdf($postId);

        /*
        * @Ticket 15203
         * page builder module
         * ave post tags
        */
        if(has_action('pbm_fe_save_post_tags')) {
            do_action('pbm_fe_save_post_tags', $postId, array('data' => $_POST));
        }

        wp_redirect('/user/classified/'.$postId.'/preview');

        return $postId;
    }

    /**
     * @ticket #13795
     * Submit listing
     * @param $id
     *
     * @return bool or mixed
     */
    public function submitListing($id)
    {
        if ( !isset($_POST[$this->nonceName]) || !wp_verify_nonce($_POST[$this->nonceName], $this->nonceAction) ) {
            return false;
        }

        if ($id) {
            $p = get_post( $id );

            $post = array(
                'ID' => $id,
            );

            // Insert case
            if ($isAddingEvent = $p->post_status == 'draft') {
                $post['post_status'] = Apollo_App::insertStatusRule(Apollo_DB_Schema::_CLASSIFIED);
            }

            wp_update_post($post);

            /**
             * Thienld : send email to confirm admin the new post is created
             * Update from @ticket #13795
             **/
            if(has_action('apollo_email_confirm_new_item_created') && $isAddingEvent){
                do_action('apollo_email_confirm_new_item_created', $id, Apollo_DB_Schema::_CLASSIFIED,false);
            }

        }
        wp_redirect(home_url(). '/user/classified/submit-success' );
    }
    
    private function getMetaGroup($arrData,$postId){
    	Apollo_App::clean_array_data($arrData);

    	if (isset($arrData[Apollo_DB_Schema::_CLASSIFIED_TMP_STATE])) {
    	    $otherState = sanitize_text_field($arrData[Apollo_DB_Schema::_CLASSIFIED_TMP_STATE]);
        } else {
            $otherState = Apollo_App::apollo_get_meta_data($postId, Apollo_DB_Schema::_CLASSIFIED_TMP_STATE, Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS);
        }

        if (isset($arrData[Apollo_DB_Schema::_CLASSIFIED_TMP_CITY])) {
    	    $otherCity = sanitize_text_field($arrData[Apollo_DB_Schema::_CLASSIFIED_TMP_CITY]);
        } else {
            $otherCity = Apollo_App::apollo_get_meta_data($postId, Apollo_DB_Schema::_CLASSIFIED_TMP_CITY, Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS);
        }

        if (isset($arrData[Apollo_DB_Schema::_CLASSIFIED_TMP_ZIP])) {
    	    $otherZip = sanitize_text_field($arrData[Apollo_DB_Schema::_CLASSIFIED_TMP_ZIP]);
        } else {
            $otherZip = Apollo_App::apollo_get_meta_data($postId, Apollo_DB_Schema::_CLASSIFIED_TMP_ZIP, Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS);
        }

        $arrayMetaGroup[Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS] = array(
            Apollo_DB_Schema::_CLASSIFIED_TMP_STATE => $otherState,
            Apollo_DB_Schema::_CLASSIFIED_TMP_CITY => $otherCity,
            Apollo_DB_Schema::_CLASSIFIED_TMP_ZIP => $otherZip,
            Apollo_DB_Schema::_CLASSIFIED_ADDRESS => $arrData[ Apollo_DB_Schema::_CLASSIFIED_ADDRESS ],
            Apollo_DB_Schema::_CLASSIFIED_CITY => $arrData[Apollo_DB_Schema::_CLASSIFIED_CITY],
            Apollo_DB_Schema::_CLASSIFIED_STATE => $arrData[Apollo_DB_Schema::_CLASSIFIED_STATE],
            Apollo_DB_Schema::_CLASSIFIED_ZIP => $arrData[Apollo_DB_Schema::_CLASSIFIED_ZIP],
            Apollo_DB_Schema::_CLASSIFIED_REGION => isset($arrData[Apollo_DB_Schema::_CLASSIFIED_REGION])? $arrData[Apollo_DB_Schema::_CLASSIFIED_REGION] : Apollo_App::apollo_get_meta_data($postId, Apollo_DB_Schema::_CLASSIFIED_REGION, Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS ),

        );
		$arrayMetaGroup[Apollo_DB_Schema::_APOLLO_CLASSIFIED_ORGANIZATION] =  $arrData[ Apollo_DB_Schema::_APOLLO_CLASSIFIED_ORGANIZATION ];
        $arrayMetaGroup[Apollo_DB_Schema::_APL_CLASSIFIED_TMP_ORG] =  $arrData[ Apollo_DB_Schema::_APL_CLASSIFIED_TMP_ORG ];
        $arrayMetaGroup[Apollo_DB_Schema::_APL_CLASSIFIED_EXP_DATE] =  $arrData[ Apollo_DB_Schema::_CLASSIFIED_EXP_DATE ];
        $arrayMetaGroup[Apollo_DB_Schema::_APL_CLASSIFIED_DEADLINE_DATE] =  $arrData[ Apollo_DB_Schema::_CLASSIFIED_DEADLINE_DATE ];
        unset(
            $arrData['id'],
            $arrData['post_title'],
            $arrData['post_content'],
            $arrData['post_author'],
            $arrData['post_type'],
            $arrData['post_status'],
            $arrData[Apollo_DB_Schema::_CLASSIFIED_ADDRESS ],
            $arrData[Apollo_DB_Schema::_CLASSIFIED_CITY ],
            $arrData[Apollo_DB_Schema::_CLASSIFIED_STATE],
            $arrData[Apollo_DB_Schema::_CLASSIFIED_TMP_STATE],
            $arrData[Apollo_DB_Schema::_CLASSIFIED_TMP_CITY],
            $arrData[Apollo_DB_Schema::_CLASSIFIED_TMP_ZIP],
            $arrData[ Apollo_DB_Schema::_APOLLO_CLASSIFIED_ORGANIZATION ],
            $arrData[ Apollo_DB_Schema::_APL_CLASSIFIED_TMP_ORG ],
            $arrData[ Apollo_DB_Schema::_CLASSIFIED_REGION ]
        );
        $arrayMetaGroup[Apollo_DB_Schema::_APL_CLASSIFIED_DATA] = $arrData;
        /** @Ticket #13028 */
        $arrayMetaGroup[Apollo_DB_Schema::_CLASSIFIED_PHONE] = $arrData[Apollo_DB_Schema::_CLASSIFIED_PHONE];
        $arrayMetaGroup[Apollo_DB_Schema::_CLASSIFIED_NAME] = $arrData[Apollo_DB_Schema::_CLASSIFIED_NAME];

        // Update state field
        if (!empty($arrayMetaGroup[Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS][Apollo_DB_Schema::_CLASSIFIED_STATE])) {
            $state = $arrayMetaGroup[Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS][Apollo_DB_Schema::_CLASSIFIED_STATE];
        }
        else {
            $state = $otherState;
        }
        $arrayMetaGroup[Apollo_DB_Schema::_APL_CLASSIFIED_STATE] = $state;

        // Update city field
        if (!empty($arrayMetaGroup[Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS][Apollo_DB_Schema::_CLASSIFIED_CITY])) {
            $city = $arrayMetaGroup[Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS][Apollo_DB_Schema::_CLASSIFIED_CITY];
        }
        else {
            $city = $otherCity;
        }
        $arrayMetaGroup[Apollo_DB_Schema::_APL_CLASSIFIED_CITY] = $city;

        return $arrayMetaGroup;

    }

    public function deleteClassified($id)
    {
        if (
            ! isset( $_POST['_aplNonceField'] )
            || ! wp_verify_nonce( $_POST['_aplNonceField'], 'deleteClassifiedAction' )
        ) {
            return false;
        }

        $classified = get_classified($id);
        if (!$classified->post || $classified->post->post_author != get_current_user_id()) return false;

        wp_trash_post($id);
        return true;
    }
	
    /*
    public function saveVideo($arrData, $arrCustomField = array(), $arrCFSearch = array()) {
            $data = array();
            if(isset($arrData['video_embed']) && is_array($arrData['video_desc']) ){
                foreach($arrData['video_embed'] as $k => $val){
                    $arrDataItem = array(
                        'video_embed' => $val,
                        'video_desc' => base64_encode(Apollo_App::clean_data_request($arrData['video_desc'][$k]))
                    );
                    $data[] = $arrDataItem;
                }
            }
            update_apollo_meta( $arrData['ID'], Apollo_DB_Schema::_APL_ORG_VIDEO, serialize( $data ) );
    
        }
        public function saveAudio($arrData, $arrCustomField = array(), $arrCFSearch = array()) {
            $data = array();
            if(isset($arrData['embed']) && isset($arrData['desc']) && is_array($arrData['desc']) ){
                foreach($arrData['embed'] as $k => $val){
                    $val = str_replace('\\','',$val);
                    $arrDataItem = array(
                        'embed' => base64_encode($val),
                        'desc' => base64_encode(Apollo_App::clean_data_request($arrData['desc'][$k]))
                    );
                    $data[] = $arrDataItem;
                }
            }
            $dataSerialize = serialize($data);
            update_apollo_meta( $arrData['ID'], Apollo_DB_Schema::_APL_ORG_AUDIO, $dataSerialize );
    
        }*/
    

    public function isValidAll($arrData){

           return parent::isValidAll($arrData);
 	   }

    /**
     * @param $id
     * @ticket #16639:  Upload PDF files to the FE and Admin Classified forms.
     */
    private function _process_upload_pdf($id) {
        $arr_attachment_ids = array();
        $post_id = $id;

        $_POST['upload_pdf_label'] = isset($_POST['upload_pdf_label']) ? Apollo_App::clean_array_data($_POST['upload_pdf_label']) : array();
        $arr_description = array_map('sanitize_text_field', $_POST['upload_pdf_label']);
        $old_attachment_ids = maybe_unserialize(get_apollo_meta($post_id, 'upload_pdf', true));
        if(empty($old_attachment_ids)) $old_attachment_ids = array();
        $arrError = array();
        /*@ticket #17020: [CF] 20180803 - [FE Forms] - Add the option to increase the file size upload - Item 1*/
        $maxUploadSize = Apollo_App::maxUploadFileSize('b');

        if(!empty($_FILES['upload_pdf']['name'])) {
            foreach($_FILES['upload_pdf']['name'] as $i => $name) {

                if(empty($name)) continue; /* Silent and continue */

                if ($_FILES['upload_pdf']['error'][$i] !== UPLOAD_ERR_OK) {
                    $arrError[] = array(
                        'message' => 'Error upload file: ' . $name,
                        'error' => true
                    );
                    continue;

                }

                // check filesize
                if($_FILES['upload_pdf']['size'][$i] > $maxUploadSize) {
                    $arrError[] = array(
                        'message' => 'File too large. Max upload size allowed is ' . number_format($maxUploadSize/ (1024 * 1024), 2) . ' MB',
                        'error' => true
                    );
                    continue;
                }

                // copy to upload directory
                if ( ! ( ( $uploads = wp_upload_dir(  ) ) && false === $uploads['error'] ) ) {

                    $arrError[] = array(
                        'message' => 'Missing upload dir for file ' . $name,
                        'error' => true
                    );
                    continue;
                }
                $wp_filetype = wp_check_filetype_and_ext($_FILES['upload_pdf']['tmp_name'][$i], $name );
                $type = empty( $wp_filetype['type'] ) ? '' : $wp_filetype['type'];

                if($type !== 'application/pdf') {
                    $arrError[] = array(
                        'message' => 'File  '. $name .' is invalid. Please upload pdf file only!',
                        'error' => true
                    );
                    continue;
                }

                $unique_name = wp_unique_filename( $uploads['path'], $name );
                $filename = $uploads['path']. '/' . $unique_name;

                $move_new_file = @ move_uploaded_file( $_FILES['upload_pdf']['tmp_name'][$i], $filename );

                if ( false === $move_new_file ) {
                    $arrError[] = array(
                        'message' => 'Cannot move to directory: ' . $uploads['path'],
                        'error' => true
                    );
                    continue;
                }


                $url = $uploads['url'] .'/'. $unique_name;

                /* Save into post attachment */
                $attachment = array(
                    'post_mime_type' => $type,
                    'guid' => $url,
                    'post_title' => $unique_name,
                    'post_content' => '',
                    'post_excerpt' => isset($arr_description[$i]) ? Apollo_App::clean_data_request($arr_description[$i]) : '',
                );

                $attachment_id = wp_insert_attachment($attachment, $filename, $post_id);
                if ( !is_wp_error($attachment_id) ) {
                    array_push($arr_attachment_ids, $attachment_id);
                }
            }
        }

        /* Error mechasin */
        $_SESSION['messages']['classified-upload_pdf'] = $arrError;

        /* Update old part */
        $_POST['upload_pdf_label_old'] = isset($_POST['upload_pdf_label_old']) ? Apollo_App::clean_array_data($_POST['upload_pdf_label_old']) : array();
        $arr_description_old = array_map('sanitize_text_field', $_POST['upload_pdf_label_old']);
        $actual_old_attachment_ids = array();
        $removeAttachmentIds = isset($_POST['apl-remove-old-pdf']) ? explode(',', $_POST['apl-remove-old-pdf']) : array();
        $index = 0;
        foreach($old_attachment_ids as $id) {
            if (!in_array($id, $removeAttachmentIds)) {
                $pdf_post = array(
                    'ID'           => $id,
                    'post_excerpt' => isset($arr_description_old[$index]) ?  $arr_description_old[$index] : '',
                );
                if (isset($arr_description_old[$index])) {
                    array_push($actual_old_attachment_ids, $id);
                }
                wp_update_post( $pdf_post );
                $index += 1;
            }
        }

        // Merge upload result
        $arr_attachment_ids = array_merge($actual_old_attachment_ids, $arr_attachment_ids);

        update_apollo_meta($post_id, 'upload_pdf', $arr_attachment_ids);
    }
}