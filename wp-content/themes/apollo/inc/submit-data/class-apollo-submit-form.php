<?php

class Apollo_Submit_Form {

    public $post_type = '';
    protected $rules;
    const  ERROR_CLASS_STR = 'inp-error';
    public $method = '';
    public $arrRuleFail = '';
    public $nonceName   = '';
    public $nonceAction = '';

    /**
     * @return string
     */
    public function getPostType()
    {
        return $this->post_type;
    }

    /**
     * @param string $post_type
     */
    public function setPostType($post_type)
    {
        $this->post_type = $post_type;
    }

    /**
     * @return mixed
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * @param mixed $rules
     */
    public function setRules($rules)
    {
        $this->rules = $rules;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @return string
     */
    public function getArrRuleFail()
    {
        return $this->arrRuleFail;
    }

    /**
     * @param string $arrRuleFail
     */
    public function setArrRuleFail($arrRuleFail)
    {
        $this->arrRuleFail = $arrRuleFail;
    }

    public function __construct($post_type, $rules) {
        /* ThienLd : sql injection avoiding handler */
        Apollo_App::sqlInjectionHandler();
        $this->post_type    = $post_type;
        $this->rules        = $rules;
        $this->method       = strtolower($_SERVER['REQUEST_METHOD']);
    }

    public function get_custom_keys($only_cf_data =  false ) {

        $custom_fields = Apollo_Custom_Field::get_all_post_fields( $this->post_type );

        $cf_data = array();
        $cf_search = array();

        if ( $custom_fields ) {
            foreach( $custom_fields as $field ) {

                if ( !Apollo_Custom_Field::can_display($field) ) continue;

                $location = maybe_unserialize( $field->location );

                if ( $location && in_array( 'rsb' , $location ) ) {
                    $cf_search[$field->name] = array();
                }
                $validation = Apollo_Custom_Field::get_validation($field);
                $validateObj = array();
                if ( $field->cf_type != 'checkbox' && $field->required == 1 ) {
                    $validateObj[] = 'required';
                }

                if ( $validation ) {
                    $validateObj[] = $validation;
                }

                $cf_data[$field->name] = $validateObj;

                if ( Apollo_Custom_Field::has_explain_field($field) ) {
                    $cf_data[$field->name. '_explain'] = array();
                }

                if ( Apollo_Custom_Field::has_other_choice($field) ) {
                    $cf_data[$field->name. '_other_choice'] = array();
                }
            }
        }

        if ($only_cf_data) return $cf_data;

        return array( $cf_data, $cf_search );
    }

    public function getRule( $key = '' ) {

        if($key === '') return $rules;

        $custom_keys = $this->get_custom_keys(TRUE);
        $rules = $custom_keys ? array_merge( $this->rules , $custom_keys ) : $this->rules;

        if(isset($rules[$key])) return $rules[$key];

        return false;
    }

    public function isValid($key, $value, $dataHolder = array()) {
        $arrRules = $this->getRule($key);
        $arrRuleError = array();
        if ( ! $arrRules ) return '';
        foreach($arrRules as $rule) {

            if ( ! $rule ) continue;
            $param = '';

            if(strpos($rule, ':') !== false) {
                list($rule, $param) = explode(":", $rule);
            }

            $className = ucfirst($rule);

            if (!class_exists($className)) continue;

            $c = new $className($param);

            $isValid = $c->isValid($value, $param, $dataHolder);


            if($isValid === false) {
                $arrRuleError[] = $rule;
            }
        }
        return $arrRuleError;
    }

    public function isValidAll($arrData) {
        foreach($arrData as $key => $value) {
            $this->arrRuleFail = $this->isValid($key, $value, $arrData);
            if(!empty($this->arrRuleFail))
            {
                return false;
            }
        }

        return true;
    }

    public function getMessageValidate($rule, $label, $type = 'text', $arrExtraParam = array()) {
        $pattern = array(
            'required' => __('Please enter %s', 'apollo'),
            'email'    => __('The %s is invalid. It must contain a valid email value', 'apollo'),
//            'url'      => __('The %s is invalid. It must contain a valid link value that includes "http://" or "https://', 'apollo'),
            'url'      => __('The %s is invalid. ','apollo'),
            'number'      => __('The %s is invalid. It must contain a valid numeric value', 'apollo'),
            'largerThan' => __('The %s must larger than %s', 'apollo'),
            'youtubeUrl' => __('The %s is not valid. It must contain a valid youtube link value', 'apollo'),
            'dateLargerNow' => __('Date must larger than today', 'apollo'),
            'endateLargerThanStartdate' => __('End date must larger than start date', 'apollo'),
            'embed' => __('The embed or link file is not valid. ', 'apollo'),
            'exists'    => __('This %s has already existed', 'apollo'),
        );

        $patternForChoice = array(
            'required' => __('Please choice %s', 'apollo'),
        );


        if(!isset($pattern[$rule])) {
            throw new Exception("Please set message for this rule");
        }

        if($rule === 'largerThan') {
            return sprintf($pattern[$rule], $label, isset($arrExtraParam['largerThan']['number']) ? $arrExtraParam['largerThan']['number'] : '');
        }

        if($type === 'choice') {
            $pattern = array_merge($pattern, $patternForChoice);
        }

        return sprintf($pattern[$rule], $label);

    }

    private function get_wrapper_class_field() {
        if ($this->post_type == Apollo_DB_Schema::_EDU_EVALUATION || $this->post_type == Apollo_DB_Schema::_GRANT_EDUCATION) {
            echo 'blog-bkl';
        } else {
            echo 'artist-blk';
        }
    }

    private function get_bold_label_field() {
        if ($this->post_type == Apollo_DB_Schema::_GRANT_EDUCATION) {
            echo 'sl';
        } else {
            echo '';
        }
    }

    // Thienld : $enabledValidation always = true if additional fields don't pass this parameter.
    public function custom_fields_form($arrCFData, $enabledValidation = true) {

        $group_fields = Apollo_Custom_Field::get_group_fields( $this->post_type );

        // Get custom fields post data

        $custom_fields_form = '';
        if ( $group_fields ):
            $i = 0;
            foreach( $group_fields as $gf ):
                /** Check display control field */
                $groupData = Apollo_Custom_Field::get_group_meta($gf['group']);
                if (!empty($groupData['display_control'])){
                    if (empty($arrCFData[$groupData['display_control']])){
                        continue;
                    }
                }

                $prepend = Apollo_Custom_Field::get_prepend_group($gf['group'], '<div class="title-bar-blk">');
                $append  = Apollo_Custom_Field::get_append_group($gf['group'], '</div>');

                if ( ! $gf['fields'] ) continue;
                    ?>
                <div class="custom-fields artist-blk">
                  
                <?php
                if ( $gf['group']->label ) {
                    echo $prepend. $gf['group']->label. $append;
                }

                foreach( $gf['fields'] as $field ):
                    $k = $field->name;
                    $_cf_val = isset( $arrCFData[$k] ) ? $arrCFData[$k] : '';

                    $type = $field->cf_type;

                    if ( ! Apollo_Custom_Field::can_display( $field ) ) continue;

                    $display_type = Apollo_Custom_Field::get_display_style($field);

                    $star = $field->required == 1 ? '(*)' : '';

                    if($enabledValidation){
                        $error_msgs = $this->error_msgs($k, $field->label, $_cf_val);
                    } else {
                        $error_msgs = '';
                    }

                    $_desc = Apollo_Custom_Field::get_desc($field);
                    $desc = $_desc ? '<div class="note">'.$_desc.'</div>' : '';

                    /* Handle for validation form on client site*/
                    $classValidation = "";
                    $classCustomValidation = "";
                    $dataAttributes = "";
                    $cfErrorMessage = "";
                    if($field->required == 1){
                        $classValidation = ' validate[required] ';
                        $classCustomValidation = "custom-validate-field";
                        $cfErrorMessage = sprintf(__('%s is required', 'apollo'),$field->label);
                        $dataAttributes = ' data-errormessage-value-missing="'.$cfErrorMessage.'"';
                    }
                    /* End of Handle for validation form on client site*/


                    switch( $type ):

                        case 'text':
                            $max_length = Apollo_Custom_Field::get_character_limit($field);
                            $validation = Apollo_Custom_Field::get_validation($field);
                            /** @Ticket #14086 */
                            switch ($validation) {
                                case 'email':
                                    $customValidate = 'custom[email]';
                                    break;
                                case 'url':
                                    $customValidate = 'custom[url]';
                                    break;
                                case 'number':
                                    $customValidate = 'custom[number]';
                                    break;
                                default:
                                    $customValidate = '';
                                    break;
                            }
                            if (!empty($customValidate) ) {
                                if (empty($classValidation)) {
                                    $classValidation = ' validate[ ' . $customValidate . ']';
                                } else {
                                    $classValidation = ' validate[required, ' . $customValidate . ']';
                                }
                            }
                            ?>
                            <div class="el-blk full">
                                <?php if ( $validation == 'datepicker' ) echo '<div class="date-picker">'; ?>

                                <input name="<?php echo $k ?>" type="text" placeholder="<?php echo $field->label ?> <?php echo $star ?>"
                                       value="<?php echo esc_attr($_cf_val) ?>"
                                       <?php echo $max_length ? 'maxlength="'.$max_length.'"' : '' ?>
                                       class=" <?php echo $classValidation; ?> inp inp-txt <?php $this->error_class() ?> apl-validation-<?php echo $validation ?>"
                                       id="<?php echo $field->name ?>"
                                       <?php echo $dataAttributes; ?>
                                    >

                                <?php if ( $validation == 'datepicker' ) echo '</div>'; ?>


                                <div class="show-tip"><?php echo $field->label ?></div>
                                <?php echo $desc; ?>
                                <?php echo $error_msgs; ?>

                            </div>
                            <?php
                        break;

                        case 'textarea':
                            $max_length = Apollo_Custom_Field::get_character_limit($field);
                            if ( ! $display_type || $display_type == 'no_label' ) {
                            ?>
                            <div class="el-blk full">
                                <textarea name="<?php echo $k ?>" placeholder="<?php echo $field->label ?> <?php echo $star ?>"
                                       <?php echo $max_length ? 'maxlength="'.$max_length.'"' : '' ?>
                                       class=" <?php echo $classValidation; ?> inp-desc-event <?php $this->error_class() ?>"
                                        <?php echo $dataAttributes; ?>
                                    ><?php echo $_cf_val ?></textarea>

                                <div class="show-tip"><?php echo $field->label ?></div>
                                <?php echo $desc,$error_msgs ?>
                            </div>
                            <?php
                            } else { ?>
                            <div class="el-blk">
                                <div class="question-sec-full"><?php echo $field->label ?> <?php echo $star ?></div>
                                <?php echo $desc; ?>
                                <textarea name="<?php echo $k ?>"
                                          <?php echo $max_length ? 'maxlength="'.$max_length.'"' : '' ?>
                                          class=" <?php echo $classValidation; ?> inp inp-txt inp-desc <?php $this->error_class() ?>"
                                            <?php echo $dataAttributes; ?>
                                    ><?php echo $_cf_val ?></textarea>
                                <?php echo $error_msgs ?>
                            </div>
                            <?php
                            } // ! $display_type || $display_type == ''

                        break;

                        case 'wysiwyg':
                            ?>
                            
                            <div class="el-blk full wysiwwyg">
                                <p class="title"><?php echo $field->label ?> <?php echo $star ?></p>
                                <?php if ( $error_msgs || $desc ): ?>
                                <div class="title"><?php echo $desc, $error_msgs; ?></div>
                                <?php endif; ?>

                            <?php

                            wp_editor($_cf_val, $field->name, array(
                                'editor_height' => 250,
                                'editor_class'  => $this->error_class(false)
                            )); ?>
                            <!--<textarea data-custom-validate="wysiwyg" data-error-message="<?php /*echo $cfErrorMessage; */?>" data-id="<?php /*echo $field->name */?>" name="<?php /*echo $k */?>" placeholder="<?php /*echo $field->label */?> <?php /*echo $star */?>"
                                       class="<?php /*echo $classCustomValidation; */?> apl-editor inp-desc-event <?php /*$this->error_class() */?>"><?php /*echo $v */?></textarea>-->

                            </div>
                       
                            <?php
                        break;

                        case 'checkbox':
                            /** Hidden display control field */
                            $hiddenDisplayControl = '';
                            if (!empty($groupData['display_control'])
                                &&!empty($arrCFData[$groupData['display_control']])
                                && $arrCFData[$groupData['display_control']] == $field->name){
                                $hiddenDisplayControl = 'hidden';
                            }
                            $checked = $field->name == $_cf_val ? 'checked' : '';
                            ?>
                                <ul class="qualifi-list <?php echo $hiddenDisplayControl; ?>">
                                <li>
                                    <div class="quali-ct"><span class="<?php $this->get_bold_label_field() ?>"><?php echo $field->label ?> <?php echo $star ?></span></div>
                                    <?php echo $desc; ?>
                                    <div class="quali-check">
                                        <div class="onoffswitch">
                                            <input <?php echo $checked ?> value="<?php echo esc_attr($field->name) ?>"
                                                                type="checkbox" id="qualionoffswitch<?php echo $i ?>"
                                                                name="<?php echo $k ?>" class="onoffswitch-checkbox <?php echo $classCustomValidation; ?>"
                                                                data-custom-validate="checkbox" data-error-message="<?php echo $cfErrorMessage; ?>"
                                                >
                                            <label for="qualionoffswitch<?php echo $i ?>" class="onoffswitch-label">
                                                <span class="onoffswitch-inner"></span><span class="onoffswitch-switch"></span>
                                            </label>
                                        </div>
                                    </div>
                                </li>
                                </ul>
                            <?php
                        break;

                        case 'radio':
                            if($field->required == 1){
                                $classValidation = ' validate[groupRequired['.$k.']] ';
                            }
                            $meta = maybe_unserialize( $field->meta_data );
                            $options = Apollo_Custom_Field::get_choice( $field );

                            $has_other_choice = Apollo_Custom_Field::has_other_choice($field);

                            ?>
                            <div class="el-blk" >
                                
                                <p class="title"><?php echo $field->label ?> <?php echo $star ?></p>
                                <?php 
                                if ( $error_msgs || $desc ): ?>
                                <div class="title"><?php echo $desc ,$error_msgs; ?></div>
                                <?php endif; ?>
                                
                                <ul class="grant-sl apl-radio-choice" <?php echo $dataAttributes; ?>>
                                    <?php
                                    foreach( $options as $k_c => $v_c ):
                                        echo '<li><input '.( $_cf_val == $k_c ? 'checked="checked"' : '' ).' name="'.$k.'" value="'.  esc_attr($k_c).'" type="radio" class="'.$classValidation.' apollo-radio"> <label>'.$v_c.'</label></li>';
                                    endforeach;

                                    if ( $has_other_choice ) {

                                        $_val_other = isset( $arrCFData[$k. '_other_choice'] ) ? $arrCFData[$k. '_other_choice'] : '';
                                        echo '<li><input '.( $_cf_val == 'other_choice' ? 'checked="checked"' : '' ).' name="'.$k.'" value="other_choice" type="radio" class=" '.$classValidation.' apollo-radio"> <label>'.__("Other (describe)", "apollo").'</label>';
                                        echo '<textarea '.($_cf_val !== 'other_choice' ? 'style="display: none"' : '').' name="'.$k.'_other_choice" class="inp inp-txt inp-desc other-choice-ipt"/>'.$_val_other.'</textarea>';
                                        echo '</li>';
                                    }
                                    ?>
                                </ul>
                            </div>
                            <?php
                        break;

                        case 'multi_checkbox':
                            if($field->required == 1){
                                $classValidation = ' validate[groupRequired['.$k.']] ';
                            }
                            $meta = maybe_unserialize( $field->meta_data );
                            $options = Apollo_Custom_Field::get_choice( $field );

                            /*@ticket #18221: 0002504: Arts Education Customizations - Add a checkbox option for "Add Menu Arrow" - item 1*/
                            $classExpanderParent = '';
                            $dataToggleClass = '';
                            $expanderElement = '';
                            if(isset($meta["add_menu_arrow"]) && $meta['add_menu_arrow']){
                                $classExpanderParent = 'expander-parent';
                                $dataToggleClass = ' data-toggle-class="show"';
                                $expanderElement = '<span class="expander"></span>';
                            }
                            ?>
                            <div class="el-blk radio-box <?php echo $k; ?>" >
                                <p class="title <?php echo $classExpanderParent?>" <?php echo $dataToggleClass?>>
                                    <?php echo $field->label ?> <?php echo $star ?>
                                    <?php echo $expanderElement ?>
                                </p>
                                <?php if ( $error_msgs || $desc ): ?>
                                <div class="title"><?php echo  $desc , $error_msgs; ?></div>
                                <?php endif; ?>

                                <ul class="vertical" <?php echo $dataAttributes; ?>>
                                    <?php
                                    if ( $options ) {
                                        foreach( $options as $k_c => $v_c ):
                                            /** @Ticket #18222 */
                                            if (strpos($v_c, '[label]') > -1) : ?>
                                                <li>
                                                    <label class="apl-item-none-checkbox"><?php echo trim(str_replace('[label]', '', $v_c)); ?></label>
                                                </li>
                                            <?php
                                            else :
                                                echo '<li><input '.( is_array( $_cf_val ) && in_array( $k_c , $_cf_val ) ? 'checked="checked"' : '' ).' name="'.$k.'[]" value="'.  esc_attr($k_c).'" type="checkbox" class="select short '.$classValidation.' apollo-multi-checkbox " autocomplete="off"> <span onClick="checkCBBefore(this)">'.$v_c.'</span></li>';
                                            endif;
                                        endforeach;
                                    }
                                    ?>
                                </ul>
                            </div>
                            <?php
                        break;

                        case 'select':

                            $meta = maybe_unserialize( $field->meta_data );
                            $options = Apollo_Custom_Field::get_choice( $field );
                           
                            $display_type = Apollo_Custom_Field::get_display_style($field);

                            if ( ! $options ) continue;

                            $option_str = '';
                            foreach ( $options as $k_o => $v_o ) {
                                $option_str .= '<option '.  selected( $k_o, $_cf_val, FALSE  ).' value="'.  esc_attr($k_o).'">'.$v_o.'</option>';
                            }

                            // Explain field
                            $explain = '';
                            if ( Apollo_Custom_Field::has_explain_field($field) ) {
                                $explain = '<div class="el-blk full"><textarea name="'.$k.'_explain" class="inp inp-txt inp-desc">'.(isset( $arrCFData[$k.'_explain'] ) ? $arrCFData[$k.'_explain'] : '').'</textarea></div>';
                            }

                            $data_attr = '';
                            if ( $actionReference = Apollo_Custom_Field::get_action_refer_to($field, $this->post_type) ) {
                                $data_attr = sprintf( 'data-action="'.$actionReference.'" data-ride="ap-refer-select" data-target="#%s"', Apollo_Custom_Field::get_refer_to($field));
                            } 
                            
                            if (! $display_type || $display_type == 'full_width') {
                            ?>
                            <div class="el-blk full explain-full-field">
                                
                                <p class="title"><?php echo $field->label ?></p>

                                <div class="title"><?php echo $desc,$error_msgs; ?></div>

                                
                                <select data-value="<?php echo $_cf_val ?>" <?php echo $data_attr ?>
                                        name="<?php echo $k ?>"
                                        id="<?php echo $k ?>"
                                        class="event apl-cus-select <?php $this->error_class() ?> <?php echo $classCustomValidation; ?>"
                                        data-custom-validate="select2"
                                        data-error-message="<?php echo $cfErrorMessage; ?>">

                                    <option value="0"><?php echo $field->label ?> <?php echo $star ?></option>
                                    <?php echo $option_str ?>
                                </select>
                                <?php echo $explain ?>
                            </div>
                            <?php } else { ?>
                            <div class="el-blk">
                                <ul class="agree-list">
                                    <li>
                                        <div class="question-sec"><?php echo $field->label ?>

                                            <?php echo $desc,$error_msgs;  ?></div>
                                        <div class="answer-sec">
                                            <div class="select-bkl">
                                                <select data-value="<?php echo $_cf_val ?>" <?php echo $data_attr ?> name="<?php echo $k ?>" id="<?php echo $k ?>" class="<?php $this->error_class() ?>">
                                                    <option value="0"><?php _e('Select', 'apollo') ?></option>
                                                    <?php echo $option_str ?>
                                                </select>
                                                <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <?php echo $explain ?>
                              </div>
                            <?php
                            }

                        break;

                    endswitch;
                    $i++;
                endforeach;
                ?>
            </div>
            <?php
            endforeach;
        endif;
    }

    public function error_msgs($k, $label, $value, $dataHolder = '') {

        if ( isset($_POST['case_action']) && $_POST['case_action'] == 'reset' ) return false;

        $custom_fields_form = '';
        if ( $this->method == 'post' ):
            $this->arrRuleFail = $this->isValid( $k, $value, $dataHolder);

            if ( ! empty($this->arrRuleFail) && is_array($this->arrRuleFail) ) {

                foreach($this->arrRuleFail as $rule ) {
                    if ( $rule == 'largerThan' ) {
                        $custom_fields_form .= '<span class="error">* '.$this->getMessageValidate( $rule, $label, '', array(
                                            'largerThan' => array('number' => 'zero')
                                        ) ).'</span>';

                    } else {
                        $custom_fields_form .= '<span class="error">* '.$this->getMessageValidate( $rule, $label ).'</span>';
                    }

                }
            }
        endif;
        return $custom_fields_form;
    }

    public function error_class($echo = TRUE) {

        if ( $this->method !== 'post' ) return false;

        $error_class = !empty($this->arrRuleFail) ? self::ERROR_CLASS_STR : '';
        if ( ! $echo ) return $error_class;

        if ($echo) echo $error_class;
        return $error_class;
    }

    public function error_form() {

        if ( $this->method !== 'post' ) return false;

        if ( $this->arrRuleFail )
            return '<span class="error form">'.__("* There are some errors in your submitted data. Please check them again !", "apollo").'</span>';
        return false;
    }

    public function getCustomFieldsFromRequest( $only_cf_data =  false) {

        $cf_data = array();
        $cf_search = array();

        $_arr_cf = $this->get_custom_keys($only_cf_data);

        $_cf_data_fields = array_keys( $_arr_cf[0] );
        $_cf_search_fields = array_keys( $_arr_cf[1] );

        if ( $_cf_data_fields ) {
            foreach ( $_cf_data_fields as $k ) {

                if ( isset( $_REQUEST[$k] ) ) {
                    $cf_data[$k] = is_array( $_REQUEST[$k] ) ? Apollo_App::clean_array_data( $_REQUEST[$k] ) :  $_REQUEST[$k];
                } else {
                    $cf_data[$k] = '';
                }
            }
        }

        if ( $_cf_search_fields ) {
            foreach ( $_cf_search_fields as $k ) {
                $cf_search[$k] = isset( $_REQUEST[$k] ) ? sanitize_text_field( $_REQUEST[$k] ) : '';
            }
        }

        return Apollo_App::clean_array_data(array( $cf_data, $cf_search ));
    }
    
    function savePrivateStateCity($postId) {
        
        $state = isset($_POST[Apollo_DB_Schema::_APL_PRI_STATE]) ? $_POST[Apollo_DB_Schema::_APL_PRI_STATE] : '';
        $city  = isset($_POST[Apollo_DB_Schema::_APL_PRI_CITY]) ? $_POST[Apollo_DB_Schema::_APL_PRI_CITY] : '';
        $zip   = isset($_POST[Apollo_DB_Schema::_APL_PRI_ZIP]) ? $_POST[Apollo_DB_Schema::_APL_PRI_ZIP] : '';

        $aplQuery = new Apl_Query(Apollo_Tables::_APL_STATE_ZIP);

        if ( ! $state || ! $city || ! $zip ) {
            return false;
        }
        
        // Check exist state
        $checkState = $aplQuery->get_row("code='$state' AND type='state'");
         
        if (!$checkState) {
            // Add state
            $aplQuery->insert(array(
                'name'   => $state,
                'code'   => $state,
                'type'   => 'state',
                'source' => $postId   
            ));
            $stateId = $aplQuery->get_bottom( 'id', "code='$state' AND type = 'state'");
        } else {
            $stateId = $checkState->id;
        }
            
        
        // Add city
        if ($stateId) {
            
            // Check exist city
            $checkCity = $aplQuery->get_row("code='$city' AND type='city' AND parent=$stateId");

            if (!$checkCity) {
            
                $aplQuery->insert(array(
                    'name'   => $city,
                    'code'   => $city,
                    'type'   => 'city',
                    'parent' => $stateId,
                    'source' => $postId
                ));
                // Add zip
                $cityId = $aplQuery->get_bottom( 'id', "code='$city' AND type = 'city' AND parent=$stateId");
            } else {
                $cityId = $checkCity->id;
            }
            
            // Check exist city
            $checkZip = $aplQuery->get_row("code='$zip' AND type='zip' AND parent=$cityId");
            if (! $checkZip) {
                $aplQuery->insert(array(
                    'name'   => $zip,
                    'code'   => $zip,
                    'type'   => 'zip',
                    'parent' => $cityId,
                    'source' => $postId
                ));
            }
            
            // Update new territory data
            $terrData = of_get_option(Apollo_DB_Schema::_TERR_DATA, array());
            $oldThemeOpt = get_option('_apollo_theme_options');
            $terrData[$state][$city][$zip] = 'on';
           
            if ( isset($terrData[0]) ) unset ($terrData[0]);
            
            $oldThemeOpt[Apollo_DB_Schema::_TERR_DATA] = $terrData;
            $oldThemeOpt[Apollo_DB_Schema::_TERR_STATES][$state] = 1;
            update_option('_apollo_theme_options', $oldThemeOpt);
         
        }
       
       
    }

    public static function saveFeaturedImageHandler($pid, $post_type, $ajax_request = false){
        // Thienld: This function only save featured image data for existing organization
        if (!$ajax_request) {
            $imageData = array(
                'img_id' => isset($_POST['img_id']) ? $_POST['img_id'] : '',
                'file' => isset($_POST['_file']) ? $_POST['_file'] : '',
                'oname' => isset($_POST['_oname']) ? $_POST['_oname'] : '',
                'type' => isset($_POST['_type']) ? $_POST['_type'] : '',
                'url' => isset($_POST['_url']) ? $_POST['_url'] : '',
                'is_deleted' => isset($_POST['_is_deleted']) ? $_POST['_is_deleted'] : '',
                '_is_new_or_modify' => isset($_POST['_is_new_or_modify']) ? $_POST['_is_new_or_modify'] : '',
            );
        } else {
            $imageData = array(
                'img_id' => isset($_GET['featured']['img_id']) ? $_GET['featured']['img_id'] : '',
                'file' => isset($_GET['featured']['file']) ? $_GET['featured']['file'] : '',
                'oname' => isset($_GET['featured']['oname']) ? $_GET['featured']['oname'] : '',
                'type' => isset($_GET['featured']['type']) ? $_GET['featured']['type'] : '',
                'url' => isset($_GET['featured']['url']) ? $_GET['featured']['url'] : '',
                'is_deleted' => isset($_GET['featured']['is_deleted']) ? $_GET['featured']['is_deleted'] : '',
                '_is_new_or_modify' => isset($_GET['featured']['_is_new_or_modify']) ? $_GET['featured']['_is_new_or_modify'] : '',
            );
        }
        $imageData['target'] = $post_type;
        $imageData['current_user_display_name'] = $GLOBALS['current_user']->display_name;

        // Modifying image
        $isModifying = isset($imageData['_is_new_or_modify']) && $imageData['_is_new_or_modify'] == 1;

        if ($imageData['is_deleted']) {
            delete_post_thumbnail($pid);

            // Delete image from media
            if (!empty($imageData['img_id'])) {
                wp_delete_attachment($imageData['img_id']);
            }

            if (!$isModifying) return true;
        }
        if (empty($imageData['file']) || !file_exists($imageData['file'])) {
            return false;
        }
        if(!empty($imageData) && $isModifying){
            $target = sanitize_text_field($imageData['target']);
            $user_display_name = $imageData['current_user_display_name'];
            $pid = intval($pid);
            $featureInfo = $imageData;
            $type = $featureInfo['type'];
            $url = $featureInfo['url'];
            $fname = $featureInfo['oname'];
            $filename = $featureInfo['file'];
            $_arr_ext_img = explode('/', $type);
            $ext_img = $_arr_ext_img && isset($_arr_ext_img[1]) && $_arr_ext_img[1] ? '.' . $_arr_ext_img[1] : '';

            /* rename filename */
            $upload_path = Apollo_App::getUploadBaseInfo('path');
            $upload_url = Apollo_App::getUploadBaseInfo('url');

            $safe_title = preg_replace('/[^a-z0-9A-Z]/', '-', $user_display_name);
            $new_fname = $target . '-featured-' . $safe_title . '-' . time();
            $full_file_name = $new_fname . $ext_img;
            $url= $upload_url . '/' . $full_file_name;
            $filename2 = $upload_path . '/' . $full_file_name;
            rename($filename, $filename2);

            $attachment = array(
                'post_mime_type' => $type,
                'guid' => $url,
                'post_title' => $new_fname,
                'post_content' => '',
            );

            $image_id = wp_insert_attachment($attachment, $filename2, $pid);
            if (!is_wp_error($image_id)) {
                require_once(ABSPATH . "wp-admin" . '/includes/image.php');
                wp_update_attachment_metadata($image_id, wp_generate_attachment_metadata($image_id, $filename2));
                set_post_thumbnail($pid, $image_id);
            }
        }
        return true;
    }

    public static function saveGalleryHandler($pid, $post_type, $ajax_request = false)
    {
        // Thienld : this function only save gallery data for existing org id
        if(empty($pid)) return false;
        $galleryData = array();

        //page_builder @15085
        $arrIdAttachment = array();

        if (!$ajax_request) {
            if(isset($_POST['_gallerys']['file'])){
                for ($i = 0; $i < sizeof($_POST['_gallerys']['img_id']); $i++) {
                    $galleryData['galleriesStorageOnSession'][$_POST['_gallerys']['img_id'][$i]] = array(
                        'file' => isset($_POST['_gallerys']['file'][$i]) ? $_POST['_gallerys']['file'][$i] : '',
                        'oname' => isset($_POST['_gallerys']['file'][$i]) ? $_POST['_gallerys']['file'][$i] : '',
                        'type' => isset($_POST['_gallerys']['type'][$i]) ? $_POST['_gallerys']['type'][$i] : '',
                        'url' => isset($_POST['_gallerys']['url'][$i]) ? $_POST['_gallerys']['url'][$i] : ''
                    );
                }
                $caption_gallery = isset($_POST['caption_gallerys']) ? $_POST['caption_gallerys'] : array();
            }
        } else {
            $galleryData['galleriesStorageOnSession'] = isset($_GET['gallery']) ? $_GET['gallery'] : array();
            $galleryData['current_user_display_name'] = $GLOBALS['current_user']->display_name;
            $galleryData['target'] = $post_type;
            $caption_gallery = isset($_REQUEST['caption_gallerys']) ? $_REQUEST['caption_gallerys'] : array();
        }

        if(!empty($galleryData['galleriesStorageOnSession'])){
            $pid = intval($pid);
            $target = $post_type;
            $user_display_name = $GLOBALS['current_user']->display_name;
            $meta_key_gallery = Apollo_App::getMetaKeyGalleryImgByTarget($target);

            $s_old_img = get_apollo_meta($pid, $meta_key_gallery, true);
            $arrOImg = explode(",", $s_old_img);

            $arrId = array();
            $i = 0;

            if(empty($galleryData['galleriesStorageOnSession'])) {
                //page_builder @15085
                if(has_action('pbm_fe_save_media_tags')) {
                    do_action('pbm_fe_save_media_tags', $pid, array(
                        'ajax_request' => $ajax_request,
                        'arrId' => $arrId,
                        'arrIdAttachment' => $arrIdAttachment,
                    ));
                }
                return false;
            }

            $galleriesStorageOnSession = $galleryData['galleriesStorageOnSession'];
            foreach ($galleriesStorageOnSession as $img_id => $info) { // img_id maybe uid
                $oldCaption = '';
                $is_img_id_exists = false;
                $post_img = array();

                if (is_numeric($img_id)) {
                    $post_img = get_post($img_id);
                    $oldCaption = $post_img->post_excerpt;
                }

                //@Ticket @15085
                $img_id_flag = $img_id;
                $arrIdAttachment[$img_id_flag] = $img_id_flag;


                /**
                 * If you add some new images gallery and not refresh the page,
                 * the img_id is a string, not the real image, the $info['new_img_id'] will
                 * tell you it is the image you've already saved, so don't need to update it anymore
                 */
                $is_img_id_exists = in_array($img_id, $arrOImg) || isset($info['new_img_id']);

                if (isset($info['new_img_id']) && $info['new_img_id']) {
                    $img_id = $info['new_img_id'];
                }

                if ($is_img_id_exists === false && file_exists($info['file'])) {
                    $type = $info['type'];
                    $url = $info['url'];
                    $fname = $info['oname'];
                    $filename = $info['file'];

                    $upload_path = Apollo_App::getUploadBaseInfo('path');
                    $upload_url = Apollo_App::getUploadBaseInfo('url');

                    $_arr_ext_img = explode('/', $type);
                    $ext_img = $_arr_ext_img && isset($_arr_ext_img[1]) && $_arr_ext_img[1] ? '.' . $_arr_ext_img[1] : '';

                    $safe_title = preg_replace('/[^a-z0-9A-Z]/', '-', $user_display_name);
                    $new_fname = $target . '-gallery-' . $i . '-' . $safe_title . '-' . time();
                    $full_name = $new_fname . $ext_img;
                    $url= $upload_url . '/' . $full_name;
                    $filename2 = $upload_path . '/' . $full_name;
                    $filename2Url = $upload_url . '/' . $full_name;

                    rename($filename, $filename2);
                    // replace filename url when uploaded file is move to wordpress upload folder.
                    $info['url'] = $filename2Url;

                    $attachment = array(
                        'post_mime_type' => $type,
                        'guid' => $url,
                        'post_title' => $new_fname,
                        'post_content' => '',
                        'post_excerpt' => isset($caption_gallery[$i]) && !empty($caption_gallery[$i]) ? Apollo_App::clean_data_request($caption_gallery[$i]) : '',
                    );

                    $img_id = wp_insert_attachment($attachment, $filename2);

                    /**
                     * Keep the image real image id for the next updating,
                     * because if you not refresh the page the new image id is a string,
                     * it not the real img_id, so it will be break the data when you update data
                     */
                    $info['new_img_id'] = $img_id;

                    if (!$img_id instanceof WP_Error) {
                        require_once(ABSPATH . "wp-admin" . '/includes/image.php');
                        wp_update_attachment_metadata($img_id, wp_generate_attachment_metadata($img_id, $filename2));
                    }
                } elseif (!empty($post_img) && isset($caption_gallery[$i]) && ($oldCaption !== $caption_gallery[$i])) {// update caption only
                    $post_img->post_excerpt = Apollo_App::clean_data_request($caption_gallery[$i]);
                    wp_update_post($post_img);
                }

                //@Ticket 15085
                $arrIdAttachment[$img_id_flag] = $img_id;

                $arrId[] = $img_id;
                $i++;
            }

            update_apollo_meta($pid, $meta_key_gallery, implode(',', array_unique($arrId)));
        } else {
            $meta_key_gallery = Apollo_App::getMetaKeyGalleryImgByTarget($post_type);
            update_apollo_meta($pid, $meta_key_gallery, '');
        }

        //page_builder @15085

        if(has_action('pbm_fe_save_media_tags')) {
            do_action('pbm_fe_save_media_tags', $pid, array(
                'ajax_request' => $ajax_request,
                'arrId' => $arrId,
                'arrIdAttachment' => $arrIdAttachment,
            ));
        }

        return true;
    }

    public static function save_featured_and_gallery_image()
    {
        // check pid belong to this user first
        $pid = isset($_GET['pid']) ? $_GET['pid'] : '';

        if ($pid === '') {
            wp_die();
        }
        $pid = intval($pid);
        $p = get_post($pid);

        if ( !isset($_REQUEST['_aplNonceField']) || !wp_verify_nonce($_REQUEST['_aplNonceField'], self::getNonceActionByType($p->post_type)) ) {
            wp_send_json(array(
                'status' => 'failed',
            ));
        }

        // Thienld : handling for add new featured image and galleries from storage data on session.
        // $pidOnSessionStorage : in this case is only for existing organization profile.
        // Because this case is called when org profile is created completely.

        /** @Ticket: #12729 */

        self::saveFeaturedImageHandler($pid, $p->post_type, true);

        /** get gallery info */

        self::saveGalleryHandler($pid, $p->post_type, true);

        if(!isset($_GET['synchronous_save_data'])){
            wp_send_json(array(
                'status' => 'ok',
            ));
        }
    }

    private static function getNonceActionByType($type){
        switch ($type) {
            case Apollo_DB_Schema::_ORGANIZATION_PT :
                return Apollo_Const::_APL_NONCE_ACTION_ORGANIZATION_PHOTO_PAGE;
            case Apollo_DB_Schema::_ARTIST_PT :
                return Apollo_Const::_APL_NONCE_ACTION_ARTIST_GALLERY_PAGE;
            case Apollo_DB_Schema::_VENUE_PT :
                return Apollo_Const::_APL_NONCE_ACTION_VENUE_PHOTO_PAGE;
            default:
                return Apollo_Const::_APL_NONCE_ACTION_ORGANIZATION_PHOTO_PAGE;
        }
    }
}



