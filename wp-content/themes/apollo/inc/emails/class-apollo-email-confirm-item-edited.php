<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Description of Apollo_Email_Confirm_Item_Edited
 *
 * @author Truonghn
 */
class Apollo_Email_Confirm_Item_Edited extends Apollo_ABS_Email {

    var $username,
        $site_url,
        $edit_post_link,
        $item_name,
        $site_name,
        $post_title;

    const numberParam = 6;

    private $email_send_test;


    public function __construct() {

        $this->id = 'confirm_item_edited';

        parent::__construct();

        add_action( 'apollo_email_confirm_item_edited', array($this, 'trigger'), 10, 3 );
    }

    function getItemData($post_type, $post_id){
        $result = null;
        try{
            switch ($post_type){
                case Apollo_DB_Schema::_ORGANIZATION_PT:
                    $result = get_org($post_id);
                    break;
                case Apollo_DB_Schema::_EVENT_PT:
                    $result = get_event($post_id);
                    break;
                case Apollo_DB_Schema::_ARTIST_PT:
                    $result = get_artist($post_id);
                    break;
                case Apollo_DB_Schema::_VENUE_PT:
                    $result = get_venue($post_id);
                    break;
                case Apollo_DB_Schema::_CLASSIFIED:
                    $result = get_classified($post_id);
                    break;
                default:
                    break;
            }
        }catch (Exception $ex){
            aplDebugFile($ex->getMessage(),'Apollo_Email_Confirm_Item_Edited:getItemData');
        }
        return $result;
    }

    function trigger($created_id, $post_type = '', $send_test_email = false) {
        global $current_site;
        $this->setDefaultData();
        try{
            if(!$send_test_email) {
                $this->created_item = $this->getItemData($post_type, $created_id);
                if(empty($this->created_item)){
                    aplDebugFile("The '".$post_type."' is not exist. (ID : '".$created_id."')",'Apollo_Email_Confirm_Item_Edited:trigger send email');
                    return false;
                }
                $this->username         = $this->created_item->get_post_author();
                $this->item_name         = $this->created_item->post->post_title;
                $this->edit_post_link        = get_permalink($created_id);
                $this->post_title  = $this->getPostTitle($this->created_item->post->post_type);
                $this->recipient        = get_option('admin_email');
            } else {
                $this->username         = __("Post's  author name.","apollo");
                $this->item_name         = __("The post title.","apollo");
                $this->edit_post_link        = '#';
                $this->post_title = __("post","apollo");
                $this->recipient = $this->email_send_test;
            }

            $this->site_name        = get_option('blogname',__("Apollo Multisite","apollo"));
            $this->site_url         = home_url();
            if( ! $this->enabled_email_confirmation) {
                // stop sending email to confirm to admin when this feature is disabled.
                return false;
            }

            return $this->send( $this->get_recipient(), $this->get_subject(), $this->wrap_message($this->get_content(), $this->heading), $this->get_headers() );
        }catch (Exception $ex){
            aplDebugFile($ex->getMessage(),'Apollo_Email_Confirm_Item_Edited:trigger send email');
        }
    }

    function sendTest($email = '') {
        $this->subject = $this->subject. ' - '. __('Testing Email Confirm Post Edited', 'apollo');
        $this->email_send_test = $email;
        return $this->trigger(0,'',true);
    }

    function getPostTitle($post_type = ''){
        $result = '';
        switch ($post_type){
            case Apollo_DB_Schema::_ORGANIZATION_PT:
                $result = __("organization","apollo");
                break;
            case Apollo_DB_Schema::_EVENT_PT:
                $result = __("event","apollo");
                break;
            case Apollo_DB_Schema::_ARTIST_PT:
                $result = __("artist","apollo");
                break;
            case Apollo_DB_Schema::_VENUE_PT:
                $result = __("venue","apollo");
                break;
            case Apollo_DB_Schema::_CLASSIFIED:
                $result = __("classified","apollo");
                break;
            default:
                break;
        }
        return $result;
    }

    function get_content( $preview = FALSE ) {

        if ( $preview ) {
            $this->setPreview();
        }

        return $this->get_template_file( array(
            '{username}'        => $this->username,
            '{site_name}'       => $this->site_name,
            '{post_name}'       => $this->item_name,
            '{new_post_title}'  => $this->post_title,
            '{edit_post_link}'  => $this->edit_post_link,
            '{site_url}'        => $this->site_url
        ) );
    }

    /**
     * Set the preview data in admin preview template
     */
    function setPreview() {
        global $current_site;
        $this->username         = __("The org author name.","apollo");
        $this->item_name         = __("The org title.","apollo");
        $this->edit_post_link        = '#';
        $this->site_name        = $current_site->site_name;
        $this->post_title = __("post","apollo");
    }

    /**
     * Get from email address
     *
     * @access public
     * @return string
     */
    function get_from_email_address($from_email) {
        $aplFromEmail = sanitize_email(of_get_option( Apollo_DB_Schema::_ES_FROM_EMAIL ) );
        $fromEmailVal = get_site_option('mail_from',$aplFromEmail); // override from email by WP-Mail-SMTP
        return !empty($fromEmailVal) ? $fromEmailVal : 'apldefaultadmin@gmail.com';
    }

    /**
     * Get from name
     *
     * @access public
     * @return string
     */
    function get_from_name() {
        $aplFromName = of_get_option( Apollo_DB_Schema::_ES_FROM_NAME );
        if ( ! $aplFromName ) {
            global $apollo_email_option;
            $aplFromName = $apollo_email_option['from_name'];
        }
        $fromNameVal = get_site_option('mail_from_name',$aplFromName); // override from email name by WP-Mail-SMTP
        return wp_specialchars_decode(esc_html(str_replace( '{site_title}', get_bloginfo(), $fromNameVal) ), ENT_QUOTES );
    }
}

new Apollo_Email_Confirm_Item_Edited();
