<?php

if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Description of Apollo_Email_Grant_Education
 *
 * @author vulh
 */
class Apollo_Email_Grant_Education extends Apollo_ABS_Email {
    var $additional_fields;  
    var $object_id = '';
    var $isSendTest = false;
    
    const numberParam = 1;
    
    public function __construct() {
        
        $this->id = 'grant_education';
        
        parent::__construct();
        
        add_action( 'apollo_email_grant_education', array($this, 'trigger'), 10, 2 );
        
    }
    
    function trigger( $object_id, $email ) {
        $this->setDefaultData();
        $this->object_id = $object_id;
        
        if ( $this->isSendTest ) {
            $configEmail = $email;
        } else {
            $configEmail = explode( '<br />', nl2br(of_get_option( $this->id. '_'. Apollo_DB_Schema::_ES_EMAIL_RECIPIENT )) );
            $configEmail[] = $email;
        }
        
        $this->recipient = $configEmail;
        
        $recipient = is_array( $this->recipient ) ? $this->recipient : $this->get_recipient();
        return $this->send( $recipient, $this->get_subject(), $this->wrap_message($this->get_content(), $this->heading) , $this->get_headers() );
    }
 
    function sendTest($email) {
        $this->subject = $this->subject. ' - '. __('Testing Email', 'apollo');
        $this->setTest();
        return $this->trigger($this->object_id, $email);
    }
    
    function get_content() {
        
        if ( !$this->object_id ) {
            $this->setTest();
        }
        
        if ( ! $this->isSendTest ) {
            $grantObj = get_grant_education($this->object_id);
            $this->additional_fields = $grantObj->render_cf_email();
        }
        
        if ( ! $this->additional_fields ) {
            $this->additional_fields = __('Grant Education Email Testing', 'apollo');
        }
        
        return $this->get_template_file( array( 
                '{additional_fields}'     => $this->additional_fields,
            ) 
        );
    }
    
    /**
     * Set test data email
     * 
     * @access private
     * 
     */
    private function setTest() {
        $this->isSendTest = true;
        $grants = get_posts(array('post_type' => Apollo_DB_Schema::_GRANT_EDUCATION, 'posts_per_page' => 1));
        if ($grants) {
            $this->object_id = $grants[0]->ID;
            $grantObj = get_grant_education($this->object_id);
            $this->additional_fields = $grantObj->render_cf_email();
        }
    }
}

new Apollo_Email_Grant_Education();
