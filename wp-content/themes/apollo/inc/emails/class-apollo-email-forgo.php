<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Description of Apollo_Email_Forgo
 *
 * @author vulh
 */
class Apollo_Email_Forgo extends Apollo_ABS_Email {
    var $reset_link;
    var $username;
    
    const numberParam = 2;
    
    public function __construct() {
        
        $this->id = 'forgo';
        
        parent::__construct();
        
        add_action( 'apollo_email_forgot_password', array($this, 'trigger'), 10, 3 );
    }
    
    function trigger($username, $resetLink, $email) {
        $this->setDefaultData();
        $this->username = $username;
        $this->reset_link = $resetLink;
        $this->recipient = $email;
        
        return $this->send( $this->get_recipient(), $this->get_subject(), $this->wrap_message($this->get_content(), $this->heading) , $this->get_headers() );
    }
    
    function sendTest($email) {
        $this->subject = $this->subject. ' - '. __('Testing Email', 'apollo');
        return $this->trigger( 'User', '#', $email);
    }
    
    function get_content( $preview = FALSE ) {
        
        if ( $preview ) {
            $this->setPreview();
        }
        
        return $this->get_template_file( array(
            '{username}'      => $this->username,
            '{reset_link}'    => $this->reset_link,
        ) );
    }
    
    /**
     * Set the preview data in admin preview template
     */
    function setPreview() {
        $this->username = __('User', 'apollo');
        $this->reset_link = '#';    
    }
    
}

new Apollo_Email_Forgo();
