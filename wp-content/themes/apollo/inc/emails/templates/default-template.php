<?php

$forgot_pass_content = <<<EC
Someone requested that the password be reset for the following account:

Username: [USERNAME]

If this was a mistake, just ignore this email and nothing will happen.

To reset your password, visit the following address:

<<a href="[RESET_LINK]">[RESET_LINK]</a>>
EC;

return array(
    'forgot_pass' => $forgot_pass_content,
);
