<?php

if ( ! defined( 'ABSPATH' ) ) exit;

class Apollo_Email_Confirm_Rejected_Item_To_Owner extends Apollo_ABS_Email {
    var $username,
        $site_name,
        $event_title,
        $support_email,
        $reasons,
        $reason_detail,
        $userEmail;

    const numberParam = 7;

    private $email_send_test;

    private $rejected_item;

    public function __construct() {

        $this->id = 'confirm_rejected_item_to_owner';
        
        parent::__construct();
        $this->support_email = of_get_option(Apollo_DB_Schema::_SUPPORT_EMAIL, '');
        
        add_action( 'apollo_email_confirm_rejected_item_to_owner', array($this, 'trigger'), 10, 4 );
    }

    function getItemData($post_type, $post_id){
        $result = null;
        try{
            switch ($post_type){
                case Apollo_DB_Schema::_EVENT_PT:
                    $result = get_event($post_id);
                    break;
                default:
                    break;
            }
        }catch (Exception $ex){
            aplDebugFile($ex->getMessage(),'Apollo_Email_Confirm_Rejected_Item_To_Owner:getItemData');
        }
        return $result;
    }

    function trigger($rejected_id, $post_type = '', $reasons = array(), $reasonDetail = '', $send_test_email = false) {
        $this->setDefaultData();
        try{
            if(!$send_test_email) {
                $this->rejected_item = $this->getItemData($post_type, $rejected_id);
                if(empty($this->rejected_item)){
                    aplDebugFile("The '".$post_type."' is not exist. (ID : '".$rejected_id."')",'Apollo_Email_Confirm_Rejected_Item_To_Owner:trigger send email');
                    return false;
                }
                $email = Apollo_App::apollo_get_meta_data($rejected_id,Apollo_DB_Schema::_E_CONTACT_EMAIL, Apollo_DB_Schema::_APOLLO_EVENT_DATA);
                $this->username = Apollo_App::apollo_get_meta_data($rejected_id, Apollo_DB_Schema::_E_CONTACT_NAME, Apollo_DB_Schema::_APOLLO_EVENT_DATA);
                $this->event_title         = $this->rejected_item->post->post_title;
                $this->recipient        = get_option('admin_email');
                $this->reasons = !empty($reasons) ? implode(' <br/> ', $reasons) : '';
                $this->reason_detail = $reasonDetail;
            }
            else {
                $this->username         = __("New post author name.","apollo");
                $this->userEmail = $this->email_send_test;
                $this->event_title = __("Reject test","apollo");
                $this->recipient = $this->email_send_test;
                $email = $send_test_email;
            }

            $this->site_name        = get_option('blogname',__("Apollo Multisite","apollo"));

            $emailContent = $this->wrap_message($this->get_content(), $this->heading);
            return $this->send( $email, $this->get_subject(), $emailContent , $this->get_headers() );

        }catch (Exception $ex){
            aplDebugFile($ex->getMessage(),'Apollo_Email_Confirm_Rejected_Item_To_Owner:trigger send email');
        }
    }
    
    function sendTest($email = '') {
        $this->subject = $this->subject. ' - '. __('Testing Email Confirm Reject Event', 'apollo');
        $this->email_send_test = $email;
        return $this->trigger(0,'', array(), '',$email);
    }

    function getNewPostTitle($post_type = ''){
        $result = '';
        switch ($post_type){
            case Apollo_DB_Schema::_EVENT_PT:
                $result = __("event","apollo");
                break;
            default:
                break;
        }
        return $result;
    }

    function get_content( $preview = FALSE ) {
        
        if ( $preview ) {
            $this->setPreview();
        }
        return $this->get_template_file( array(
            '{username}'        => $this->username,
            '{site_name}'       => $this->site_name,
            '{event_title}'     => $this->event_title,
            '{support_email}'   => $this->support_email,
            '{reasons}'         => $this->reasons,
            '{reason_detail}'   => $this->reason_detail
        ) );
    }
    
    /**
     * Set the preview data in admin preview template
     */
    function setPreview() {
        global $current_site;
        $this->username         = __("New event author name","apollo");
        $this->event_title      = __("Reject Event","apollo");
        $this->site_name        = $current_site->site_name;
        $this->reasons          = '';
        $this->reason_detail    = '';
    }

    /**
     * Get from email address
     *
     * @access public
     * @return string
     */
    function get_from_email_address($from_email) {
        $aplFromEmail = sanitize_email(of_get_option( Apollo_DB_Schema::_ES_FROM_EMAIL ) );
        $fromEmailVal = get_site_option('mail_from',$aplFromEmail); // override from email by WP-Mail-SMTP
        return !empty($fromEmailVal) ? $fromEmailVal : 'apldefaultadmin@gmail.com';
    }

    /**
     * Get from name
     *
     * @access public
     * @return string
     */
    function get_from_name() {
        $aplFromName = of_get_option( Apollo_DB_Schema::_ES_FROM_NAME );
        if ( ! $aplFromName ) {
            global $apollo_email_option;
            $aplFromName = $apollo_email_option['from_name'];
        }
        $fromNameVal = get_site_option('mail_from_name',$aplFromName); // override from email name by WP-Mail-SMTP
        return wp_specialchars_decode(esc_html(str_replace( '{site_title}', get_bloginfo(), $fromNameVal) ), ENT_QUOTES );
    }
}

new Apollo_Email_Confirm_Rejected_Item_To_Owner();
