<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Description of Apollo_Email_Published_New_Event
 *
 * @author vulh
 */
class Apollo_Email_Share_Post_With_Friend extends Apollo_ABS_Email {

    var $email, $site_url, $link_share;

    const numberParam = 3;

    public function __construct() {

        $this->id = 'share_post_with_friend';

        parent::__construct();

        add_action( 'apollo_email_share_post_with_friend', array($this, 'trigger'), 10, 4 );
    }

    function trigger($email, $link_share) {
        $this->setDefaultData();
        $this->email     = $email;
        $this->link_share  = $link_share;
        $this->site_url  = home_url();
        $this->recipient = $email;
        return $this->send( $this->get_recipient(), $this->get_subject(), $this->wrap_message($this->get_content(), $this->heading) , $this->get_headers() );
    }

    function sendTest($email) {
        $this->subject = $this->subject. ' - '. __('Testing Email', 'apollo');
        return $this->trigger( $email, home_url());
    }

    function get_content( $preview = FALSE ) {
        if ( $preview ) {
            $this->setPreview();
        }
        return $this->get_template_file( array(
            '{email}'      => $this->email,
            '{link_share}'    => $this->link_share,
            '{site_url}' => site_url(),
        ) );
    }

    /**
     * override function
     * @param array $arg
     * @return mixed
     */
    function get_template_file($arg = array()) {

        $fileContent =  Apollo_App::get_static_html_content($this->id, FALSE, 'html');
        if(empty($fileContent)) {
            $fileContent = render_php_to_string(APOLLO_ADMIN_DIR. '/theme-options/default-template/email/' . $this->id. '.php');
        }
        if ( isset($_GET['preview_email_template']) ) {
            if (isset($_SESSION['apl_preview_email_content'])) {
                $tmpFile = Apollo_App::getUploadBaseInfo( 'html_dir' ). '/'. $_SESSION['apl_preview_email_content'];

                $fileContent = file_get_contents( $tmpFile );
                unlink($tmpFile);
                unset($_SESSION['apl_preview_email_content']);
            } else {
                wp_safe_redirect(admin_url());
            }

        }

        $searchObj      = array_keys( $arg );
        $replaceObj     = array_values( $arg );
        $searchObj[]    = '{primary_color}';
        $replaceObj[]   = of_get_option( Apollo_DB_Schema::_PRIMARY_COLOR );
        return str_replace( $searchObj , $replaceObj, $fileContent);
    }

    /**
     * Set the preview data in admin preview template
     */
    function setPreview() {
        $this->email = __('Email', 'apollo');
    }

    /**
     * Get from name
     *
     * @access public
     * @return string
     */
    function get_from_name() {
        $aplFromName = of_get_option( Apollo_DB_Schema::_ES_FROM_NAME );
        if ( ! $aplFromName ) {
            global $apollo_email_option;
            $aplFromName = $apollo_email_option['from_name'];
        }
        $fromNameVal = get_site_option('mail_from_name',$aplFromName); // override from email name by WP-Mail-SMTP

        return wp_specialchars_decode(esc_html(str_replace( '{site_title}', get_bloginfo(), $fromNameVal) ), ENT_QUOTES );
    }

}

new Apollo_Email_Share_Post_With_Friend();