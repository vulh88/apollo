<?php

if ( ! defined( 'ABSPATH' ) ) exit;

abstract class Apollo_ABS_Email {
    
    var $id;
    
    var $title;
    
    /** @var string html template path */
    var $template_html;
    
    /** @var string template path */
    var $template_base;
    
    /** @var string recipients for the email */
	var $recipient;
  
	/** @var string subject for the email */
	var $subject;
    
    /** @var  string Email type */
    var $email_type;
    
    /** @var  string Email heading */
    var $heading;
    
    /** @var  string Email footer */
    var $footer_text;

    /** @var  boolean Email Confirmation Feature */
    var $enabled_email_confirmation;

    /** @var array strings to find in subjects/headings */
	var $find;
    
    /** @var array strings to replace in subjects/headings */
	var $replace;
    
    function __construct() {
        add_filter('apollo_email_subject_'. $this->id, array($this, 'change_subject'));
    }
    
    function change_subject($subject) {
        $blog = get_blog_details();
        return str_replace( array( '{site_domain}' ) , array( $blog->domain ), $subject);
    }
    
    function setDefaultData() {
        global $apl_email_templates, $apollo_email_option;
       
        $this->email_type = of_get_option( $this->id. '_'. Apollo_DB_Schema::_ES_EMAIL_TYPE );
        if ( ! $this->email_type ) {
            $this->email_type = $apl_email_templates[$this->id]['data']['type'];
        }
        
        $this->subject =  of_get_option( $this->id. '_'. Apollo_DB_Schema::_ES_EMAIL_SUBJECT );
        if ( ! $this->subject ) {
            $this->subject = $apl_email_templates[$this->id]['data']['subject'];
        }
        
        $this->heading =  of_get_option( $this->id. '_'. Apollo_DB_Schema::_ES_EMAIL_HEADING );
        if ( ! $this->heading ) {
            $this->heading = $apl_email_templates[$this->id]['data']['heading'];
        }
        
        $this->footer_text =  of_get_option( $this->id. '_'. Apollo_DB_Schema::_ES_EMAIL_FOOTER_TEXT );
        if ( ! $this->footer_text ) {
            $this->footer_text = $apollo_email_option['footer_text'];
        }

        // get option to detect email confirmation on or off
        $enabled_email_confirmation = of_get_option( $this->id. '_'. Apollo_DB_Schema::_ES_EMAIL_CONFIRMATION_FEATURE );
        if ( ! $enabled_email_confirmation ) {
            $enabled_email_confirmation = of_get_option( Apollo_DB_Schema::_ES_EMAIL_CONFIRMATION_FEATURE );
        }
        $this->enabled_email_confirmation = intval($enabled_email_confirmation) === 1;
    }
    
    /**
	 * Send the email.
	 *
	 * @access public
	 * @param mixed $to
	 * @param mixed $subject
	 * @param mixed $message
	 * @param string $headers
	 * @param string $attachments
	 * @return bool
	 */
	function send( $to, $subject, $message, $headers, $attachments = '' ) {
       
		add_filter( 'wp_mail_from', array( $this, 'get_from_email_address' ), 99, 1 );
		add_filter( 'wp_mail_from_name', array( $this, 'get_from_name' ) );
		add_filter( 'wp_mail_content_type', array( $this, 'get_content_type' ) );

		$return = wp_mail( $to, $subject, $message, $headers, $attachments );

		remove_filter( 'wp_mail_from', array( $this, 'get_from_email_address' ) );
		remove_filter( 'wp_mail_from_name', array( $this, 'get_from_name' ) );
		remove_filter( 'wp_mail_content_type', array( $this, 'get_content_type' ) );

		return $return;
	}
    
    /**
     * Get from email address
     * 
     * @access public
     * @return string
     */
    function get_from_email_address($from_email) {
        $val = sanitize_email(of_get_option( Apollo_DB_Schema::_ES_FROM_EMAIL, 'apldefaultadmin@gmail.com' ) );
        return $val;
    }
    
    /**
     * Get from name
     * 
     * @access public
     * @return string
     */
    function get_from_name() {
        $from = of_get_option( Apollo_DB_Schema::_ES_FROM_NAME );
        if ( ! $from ) {
            global $apollo_email_option;
            $from = $apollo_email_option['from_name'];
        }
        
        return wp_specialchars_decode(esc_html(str_replace( '{site_title}', get_bloginfo(), $from) ), ENT_QUOTES );
    }
    
    /**
	 * get_type function.
	 *
	 * @access public
	 * @return string
	 */
	function get_email_type() {
        return 'html';
		//return $this->email_type ? $this->email_type : 'plain';
	}

	/**
	 * get_content_type function.
	 *
	 * @access public
	 * @return string
	 */
	function get_content_type() {
		switch ( $this->get_email_type() ) {
			case "html" :
				return 'text/html';
			case "multipart" :
				return 'multipart/alternative';
			default :
				return 'text/plain';
		}
	}
    
    /**
	 * get_recipient function.
	 *
	 * @access public
	 * @return string
	 */
	function get_recipient() {
		return apply_filters( 'apollo_email_recipient_' . $this->id, $this->recipient );
	}
    
    /**
	 * get_subject function.
	 *
	 * @access public
	 * @return string
	 */
	function get_subject() {
		return apply_filters( 'apollo_email_subject_' . $this->id, $this->subject );
	}
  
    /**
     * Get email content
     * 
     * @access public
     * @return string
     */
    function get_content() {}
    
    /**
	 * get_headers function.
	 *
	 * @access public
	 * @return string
	 */
	function get_headers() {
		return apply_filters( 'apollo_email_headers', "Content-Type: " . $this->get_content_type() . "\r\n" );
	}
   
    /**
	 * Wraps a message in the  mail template.
	 *
	 * @access public
	 * @param mixed $email_heading
	 * @param mixed $message
	 * @return string
	 */
	function wrap_message( $message, $email_heading = '') {
		// Buffer
        ob_start();
        $this->email_header($email_heading);
        echo $message;
        $this->email_footer();
		// Get contents
        $message = ob_get_contents();
        ob_end_clean();

		return $message;
	}
    
    function email_header($email_heading = '') {
        $this->heading = str_replace('{site_domain}', '<a style="color: '.of_get_option( Apollo_DB_Schema::_PRIMARY_COLOR ).'; text-decoration: none;" href="'.get_home_url().'">'. get_bloginfo(). '</a>', $email_heading);
        require(APOLLO_EMAILS_DIR. '/templates/email-header.php');
    }
    
    function email_footer() {
        require(APOLLO_EMAILS_DIR. '/templates/email-footer.php');
    }
    
    function get_template_file($arg = array()) {
        
        $fileContent =  Apollo_App::get_static_html_content($this->id, FALSE, 'html');
        /** @Ticket #15877 - Get default email template by id */
        if (empty($fileContent)) {
            $file = APOLLO_DEFAULT_EMAIL_TEMPLATES_DIR . '/' . $this->id . '.php';
            $fileContent = @file_get_contents($file);
        }

        if ( isset($_GET['preview_email_template']) ) {
            if (isset($_SESSION['apl_preview_email_content'])) {
                $tmpFile = Apollo_App::getUploadBaseInfo( 'html_dir' ). '/'. $_SESSION['apl_preview_email_content'];
                $fileContent = file_get_contents( $tmpFile );
                unlink($tmpFile);
                unset($_SESSION['apl_preview_email_content']);
            } else {
                wp_safe_redirect(admin_url());
            }
            
        }
        
        $searchObj      = array_keys( $arg );
        $replaceObj     = array_values( $arg );
        $searchObj[]    = '{primary_color}';
        $replaceObj[]   = of_get_option( Apollo_DB_Schema::_PRIMARY_COLOR );
        return str_replace( $searchObj , $replaceObj, $fileContent);
    }
    
    function numberParams() {}
}