<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Description of Apollo_Email_Forgo
 *
 * @author vulh
 */
class Apollo_Email_Confirm_Regis_Email extends Apollo_ABS_Email {
    var $username,
        $site_url,
        $activate_link,
        $site_name;
    
    const numberParam = 4;
    
    public function __construct() {
        
        $this->id = 'confirm_regis_email';
        
        parent::__construct();
        
        add_action( 'apollo_email_confirm_regis_email', array($this, 'trigger'), 10, 4 );
    }
    
    function trigger( $username, $activateKey, $email ) {
        global $current_site;
        $this->setDefaultData();
        $this->username         = $username;
        $this->activate_link    = home_url(). '/wp-activate.php?key='. $activateKey;
        $this->recipient        = $email;
        $this->site_name        = get_option('blogname',__("Apollo Multisite","apollo"));
        $this->site_url         = home_url();
        
        return $this->send( $this->get_recipient(), $this->get_subject(), $this->wrap_message($this->get_content(), $this->heading) , $this->get_headers() );
    }
    
    function sendTest($email) {
        $this->subject = $this->subject. ' - '. __('Testing Email', 'apollo');
        return $this->trigger( 'User', '', $email);
    }
    
    function get_content( $preview = FALSE ) {
        
        if ( $preview ) {
            $this->setPreview();
        }
        
        return $this->get_template_file( array(
            '{username}'        => $this->username,
            '{activate_link}'   => $this->activate_link,
            '{site_name}'       => $this->site_name,
            '{site_url}'        => $this->site_url,
        ) );
    }
    
    /**
     * Set the preview data in admin preview template
     */
    function setPreview() {
        global $current_site;
        $this->username         = 'User';
        $this->activate_link    = "#";
        $this->site_name        = $current_site->site_name;
        $this->site_url         = home_url();       
    }
    
}

new Apollo_Email_Confirm_Regis_Email();
