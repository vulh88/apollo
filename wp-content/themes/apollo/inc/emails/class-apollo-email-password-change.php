<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Description of Apollo_Email_Password_Change
 *
 * @author vulh
 */
class Apollo_Email_Password_Change extends Apollo_ABS_Email {

    var $username;
    var $admin_email;

    const numberParam = 3;

    public function __construct() {

        $this->id = 'password_change';

        parent::__construct();

        add_action( 'apollo_email_password_change', array($this, 'trigger'), 10, 3 );
    }

    function trigger($username, $email, $admin_email) {
        $this->setDefaultData();
        $this->username = $username;
        $this->recipient = $email;
        $this->admin_email = $admin_email;

        return $this->send( $this->get_recipient(), $this->get_subject(), $this->wrap_message($this->get_content(), $this->heading) , $this->get_headers() );
    }

    function sendTest($email) {
        $this->subject = $this->subject. ' - '. __('Testing Email', 'apollo');
        return $this->trigger( 'User', $email, '#');
    }

    function get_content( $preview = FALSE ) {

        if ( $preview ) {
            $this->setPreview();
        }

        return $this->get_template_file( array(
            '{username}'      => $this->username,
            '{site_name}'    => get_bloginfo( 'name' ),
            '{site_url}' => site_url(),
            '{admin_email}' => get_bloginfo('admin_email'),
        ) );
    }

    /**
     * Set the preview data in admin preview template
     */
    function setPreview() {
        $this->username = __('User', 'apollo');
    }

    /**
     * Get from name
     *
     * @access public
     * @return string
     */
    function get_from_name() {
        $aplFromName = of_get_option( Apollo_DB_Schema::_ES_FROM_NAME );
        if ( ! $aplFromName ) {
            global $apollo_email_option;
            $aplFromName = $apollo_email_option['from_name'];
        }
        $fromNameVal = get_site_option('mail_from_name',$aplFromName); // override from email name by WP-Mail-SMTP

        return wp_specialchars_decode(esc_html(str_replace( '{site_title}', get_bloginfo(), $fromNameVal) ), ENT_QUOTES );
    }

}

new Apollo_Email_Password_Change();
