<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Description of Apollo_Email_Published_New_Event
 *
 * @author vulh
 */
class Apollo_Email_Published_New_Event extends Apollo_ABS_Email {

    var $username;
    var $eventTitle;
    var $eventUrl;

    const numberParam = 2;

    public function __construct() {

        $this->id = 'published_new_event';

        parent::__construct();

        add_action( 'apollo_email_published_new_event', array($this, 'trigger'), 10, 4 );
    }

    function trigger($username, $postTitle, $postUrl, $email) {
        $this->setDefaultData();
        $this->username = $username;
        $this->eventTitle = $postTitle;
        $this->eventUrl = $postUrl;
        $this->recipient = $email;

        return $this->send( $this->get_recipient(), $this->get_subject(), $this->wrap_message($this->get_content(), $this->heading) , $this->get_headers() );
    }

    function sendTest($email) {
        $this->subject = $this->subject. ' - '. __('Testing Email', 'apollo');
        return $this->trigger( 'User', __('Event title', 'apollo'), '#',$email);
    }

    function get_content( $preview = FALSE ) {

        if ( $preview ) {
            $this->setPreview();
        }

        return $this->get_template_file( array(
            '{username}'      => $this->username,
            '{event_title}'    => $this->eventTitle,
            '{event_url}'    => $this->eventUrl,
            '{site_name}'    => get_bloginfo( 'name' ),
            '{site_url}' => site_url()
        ) );
    }

    /**
     * Set the preview data in admin preview template
     */
    function setPreview() {
        $this->username = __('User', 'apollo');
        $this->eventTitle = __('Event title', 'apollo');
        $this->eventUrl = '#';
    }

    /**
     * Get from name
     *
     * @access public
     * @return string
     */
    function get_from_name() {
        $aplFromName = of_get_option( Apollo_DB_Schema::_ES_FROM_NAME );
        if ( ! $aplFromName ) {
            global $apollo_email_option;
            $aplFromName = $apollo_email_option['from_name'];
        }
        $fromNameVal = get_site_option('mail_from_name',$aplFromName); // override from email name by WP-Mail-SMTP

        return wp_specialchars_decode(esc_html(str_replace( '{site_title}', get_bloginfo(), $fromNameVal) ), ENT_QUOTES );
    }

}

new Apollo_Email_Published_New_Event();
