<?php

if ( ! defined( 'ABSPATH' ) ) exit;

class Apollo_Email_Regis extends Apollo_ABS_Email {
    var $username, $site_name, $password, $login_link, $fullname;
    const numberParam = 5;
    
    public function __construct() {
        
        $this->id = 'regis';
        
        parent::__construct();
        
        add_action( 'apollo_email_registration', array($this, 'trigger'), 10, self::numberParam );
    }
    
    function trigger($firstname, $lastname, $username, $password, $email) {
        global $current_site;
        $this->setDefaultData();
        $this->username     = $username;
        $this->login_link   = get_permalink(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_LOGIN_PAGE));
        $this->password     = sanitize_text_field( $password );
        $this->fullname     = Apollo_App::clean_data_request($firstname). ' '. Apollo_App::clean_data_request($lastname);
        $this->site_name    = get_option('blogname',__("Apollo Multisite","apollo"));
        $this->recipient    = $email;
            
        
        return $this->send( $this->get_recipient(), $this->get_subject(), $this->wrap_message($this->get_content(), $this->heading) , $this->get_headers() );
    }
    
    function sendTest($email) {
        $this->subject = $this->subject. ' - '. __('Testing Email', 'apollo');
        return $this->trigger('User', '', 'test_user', 'test password', $email);
    }
    
    function get_content( $preview = FALSE ) {
        
        if ( $preview ) {
            $this->setPreview();
        }
        
        return $this->get_template_file( array(
            '{username}'      => $this->username,
            '{site_name}'     => $this->site_name,
            '{fullname}'      => $this->fullname,
            '{password}'      => $this->password,
            '{login_link}'    => $this->login_link
        ) );
    }
    
    /**
     * Set the preview data in admin preview template
     */
    function setPreview() {
        global $current_site;
        $this->username = __('User', 'apollo');
        $this->fullname = __('full name', 'apollo');
        $this->site_name = $current_site->site_name;
        $this->password = __('your password', 'apollo');
        $this->login_link = '#';    
    }
    
}

new Apollo_Email_Regis();