<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}


class Apollo_Page_Creator  {

    /* GROUP */
    const ID_GROUP_PAGE = 'group_page';

    /* Thienld: The new page must follow the rule as below :
       value of constants ID must equal by page title
     */

    /*ELEMENT*/
    const ID_REGISTER_PAGE                  = 'register';
    const ID_FORGOT_PASS_PAGE               = 'forgot-password';
    const ID_LOGIN_PAGE                     = 'login';
    const ID_LOGOUT_PAGE                    = 'logout';
    const ID_WELCOME_DASHBOARD_PAGE         = 'welcome-dashboard';
    const ID_CHANGE_MY_PASS                 = 'change-my-password';
    const ID_ORG_PROFILE                    = 'organization-profile';
    const ID_ORG_PROFILE_VIDEOS             = 'organization-videos';
    const ID_ORG_PROFILE_PHOTO              = 'organization-photos';
    const ID_ORG_PROFILE_AUDIO              = 'organization-audios';
    const ID_VENUE_PROFILE                  = 'venue-profile';
    const ID_VENUE_PROFILE_PHOTO            = 'venue-profile-photo';
    const ID_VENUE_PROFILE_VIDEO            = 'venue-profile-video';
    const ID_ADD_EVENT                      = 'add-event';
    const ID_IMPORT_EVENT                   = 'import-event';
    const ID_UPDATE_EVENT                   = 'update-event-page';
    const ID_COPY_EVENT                     = 'copy-event';
    const ID_ARTIST_PROFILE                 = 'artist-profile';
    const ID_ARTIST_VIDEO                   = 'artist-video';
    const ID_ARTIST_AUDIO                   = 'artist-audio';
    const ID_ARTIST_EVENT                   = 'artist-event';
    const ID_EDUCATION_PROFILE              = 'education-profile';
    const ID_EDUCATION_PROGRAM              = 'education-program';
    const ID_BUSINESS_PROFILE               = 'business-profile';
    const ID_ADD_CLASSIFIED                 = 'add-classified';
    const ID_PUBLISHED_CLASSIFIEDS          = 'published-classifieds';
    const ID_BLOG                           = 'blog';
    const ID_AUTHORS                        = 'authors';
    const ID_ARTIST_PHOTO                   = 'artist-photo';
    const ID_ARTIST_PHOTO_FOR_MULTIPLE      = 'artist-photo-for-multiple';
    const ID_DONE_REGISTER                  = 'registration-success';
    const ID_SUBMIT_EVALUATION_DONE         = 'submit-teacher-evaluation-success';
    const ID_TEACHER_EVALUATION_FORM        = 'teacher-evaluation';
    const ID_GRANT_EDUCATION_FORM           = 'grant-education';
    const ID_SUBMIT_GRANT_EDUCATION_DONE    = 'submit-grant-education-success';

    const ID_ADD_PROGRAM                    = 'add-program';
    const ID_PUBLISHED_PROGRAMS             = 'published-programs';

    const ID_EVENT_PHOTO                    = 'event-photo';
    /** @Ticket #14118 */
    const ID_EVENT_FORM_PRIMARY_TYPE        = 'event-form-primary-type';
    const ID_IMPORT_EVENT_INSTRUCTIONS      = 'import-event-tool-instruction';

    const ID_SUBMIT_EVENT_SUCCESS           = 'submit-event-success';
    const ID_SUBMIT_CLASSIFIED_SUCCESS           = 'submit-classified-success';

    const ID_ORGANIZATION_HEADER            = 'organization-header';
    const ID_VENUE_HEADER                   = 'venue-header';
    const ID_ARTIST_HEADER                  = 'artist-header';
    const ID_PUBLIC_ART_HEADER              = 'public-art-header';
    const ID_CLASSIFIED_HEADER              = 'classified-header';
    const ID_EDUCATOR_HEADER                = 'educator-header';
    const ID_PROGRAM_HEADER                 = 'program-header';
    const ID_TAF_POPUP                      = 'taf-popup';

    const ID_ACCOUNT_INFO                      = 'account-info';
    const ID_ACCOUNT_MY_LIST                   = 'account-my-list';
    const ID_ACCOUNT_ACTIVITY                  = 'account-activity';

    const ID_ORG_LISTING                    = 'org-listing';

    const ID_AGENCY_EDUCATOR                = 'agency-educator';
    const ID_AGENCY_ORG                     = 'agency-org';
    const ID_AGENCY_VENUES                  = 'agency-venues';
    const ID_AGENCY_ARTISTS                 = 'agency-artists';

    const ID_EVENTS                         = 'event-listing';

    const ID_BLOG_HOME_FEATURED             = 'blog-home-featured';
    const ID_EVENTS_HOME_FEATURED           = 'events-home-featured';

    //@ticket #15847
    const ID_PERMISSION_DENIES              = 'permission-denies';

    //@ticket #17126
    const ID_ARTIST_MEMBER_REQUIREMENT              = 'membership-required';

    /** @Ticket #18432 */
    const ID_SUBMIT_PROGRAM_SUCCESS           = 'submit-program-success';
    const ID_SUBMIT_PROGRAM_PREVIEW           = 'submit-program-preview';

    /* OPTIONS PAGE */
    public static $options = array();



    public static function getPageId($pageid)
    {

        if(empty(self::$options)) {
            self::$options = get_option(Apollo_App::getIncodeOptionName());
        }
        return isset(self::$options[self::ID_GROUP_PAGE], self::$options[self::ID_GROUP_PAGE][$pageid]) ? self::$options[self::ID_GROUP_PAGE][$pageid]['id'] : false;
    }

    public static function createDefaultPage()
    {
        $arr = self::getPagesDefinition();

        /* Try to create raw insert for performance purpose */
        foreach($arr as $id => $a) {

            $arr_page_pr = array(
                'post_title'    => $a['title'],
                'post_content'  => @include __DIR__ .'/admin/_templates/'.$id.'.php',
                'post_status'   => $a['status'],
                'post_type'     => 'page',
            );

            if(isset($a['page_template'])) {
                $arr_page_pr['page_template'] = $a['page_template'];
            }
            $page = get_page_by_path( $id );
            if (! $page) {
                $page_id = wp_insert_post( $arr_page_pr);
            } else {
                $page_id = $page->ID;
            }
            if($page_id) {
                self::$options[self::ID_GROUP_PAGE][$id] = array('id' => $page_id);
            }
            else {
                throw new ErrorException("Some page cannot insert correctly");
            }
        }

        /* Update one time */
        update_option(Apollo_App::getIncodeOptionName(), self::$options);
    }

    public static function getPagesDefinition()
    {
        $arrPages = array(

            self::ID_REGISTER_PAGE => array(
                'title' => 'Register',
                'status' => 'publish',
            ),

            self::ID_FORGOT_PASS_PAGE => array(
                'title' => 'Forgot Password',
                'status' => 'publish',
            ),
            self::ID_LOGIN_PAGE => array(
                'title' => 'Login',
                'status' => 'publish',
            ),

            self::ID_LOGOUT_PAGE => array(
                'title' => 'Logout',
                'status' => 'publish',
            ),

            self::ID_WELCOME_DASHBOARD_PAGE => array(
                'title' => 'Welcome Dashboard',
                'status' => 'publish',
            ),

            self::ID_CHANGE_MY_PASS => array(
                'title' => 'Change My Password',
                'status' => 'publish',
            ),

            self::ID_ORG_PROFILE => array(
                'title' => 'Organization Profile',
                'status' => 'publish',
            ),

            self::ID_VENUE_PROFILE => array(
                'title' => 'Venue Profile',
                'status' => 'publish',
            ),
            self::ID_VENUE_PROFILE_PHOTO => array(
                'title' => 'Venue Profile Photo',
                'status' => 'publish',
            ),
            self::ID_VENUE_PROFILE_VIDEO => array(
                'title' => 'Venue Profile Video',
                'status' => 'publish',
            ),

            self::ID_ADD_EVENT=> array(
                'title' => 'Add Event',
                'status' => 'publish',
            ),

            self::ID_COPY_EVENT=> array(
                'title' => 'Copy Event',
                'status' => 'publish',
            ),

            self::ID_IMPORT_EVENT=> array(
                'title' => 'Import Event',
                'status' => 'publish',
            ),

            self::ID_UPDATE_EVENT=> array(
                'title' => 'Update Event Page',
                'status' => 'publish',
            ),

            self::ID_ARTIST_PROFILE=> array(
                'title' => 'Artist Profile',
                'status' => 'publish',
            ),

            self::ID_ARTIST_VIDEO=> array(
                'title' => 'Artist Video',
                'status' => 'publish',
            ),

            self::ID_ARTIST_AUDIO => array(
                'title' => 'Artist Audio',
                'status' => 'publish',
            ),

            self::ID_ARTIST_EVENT => array(
                'title' => 'Artist Event',
                'status' => 'publish',
            ),

            self::ID_EDUCATION_PROFILE=> array(
                'title' => 'Education Profile',
                'status' => 'publish',
            ),

            self::ID_EDUCATION_PROGRAM=> array(
                'title' => 'Education Program',
                'status' => 'publish',
            ),

            self::ID_BUSINESS_PROFILE=> array(
                'title' => 'Business Profile',
                'status' => 'publish',
            ),

            self::ID_ADD_CLASSIFIED=> array(
                'title' => 'Add Classified',
                'status' => 'publish',
            ),

            self::ID_PUBLISHED_CLASSIFIEDS => array(
                'title' => 'Published Classifieds',
                'status' => 'publish',
            ),

            self::ID_BLOG=> array(
                'title' => 'Blog',
                'status' => 'publish',
            ),

            self::ID_AUTHORS=> array(
                'title' => 'Authors',
                'status' => 'publish',
            ),

            self::ID_ARTIST_PHOTO => array(
                'title' => 'Artist Photo',
                'status' => 'publish',
            ),

            self::ID_ARTIST_PHOTO_FOR_MULTIPLE => array(
                'title' => 'Artist Photo For Multiple',
                'status' => 'publish',
            ),

            self::ID_DONE_REGISTER => array(
                'title' => 'Registration Success',
                'status' => 'publish',
            ),

            self::ID_TEACHER_EVALUATION_FORM => array(
                'title'     => 'Teacher Evaluation',
                'status'    => 'publish',
            ),

            self::ID_SUBMIT_EVALUATION_DONE => array(
                'title'     => 'Submit Teacher Evaluation Success',
                'status'    => 'publish',
            ),

            self::ID_GRANT_EDUCATION_FORM => array(
                'title'     => 'Grant Education',
                'status'    => 'publish',
            ),

            self::ID_SUBMIT_GRANT_EDUCATION_DONE => array(
                'title'     => 'Submit Grant Education Success',
                'status'    => 'publish',
            ),

            self::ID_ORG_PROFILE_PHOTO => array(
                'title' => 'Organization Photos',
                'status' => 'publish',
            ),
            self::ID_ORG_PROFILE_VIDEOS => array(
                'title' => 'Organization Videos',
                'status' => 'publish',
            ),
            self::ID_ORG_PROFILE_AUDIO=> array(
                'title' => 'Organization Audios',
                'status' => 'publish',
            ),

            self::ID_ADD_PROGRAM => array(
                'title' => 'Add Program',
                'status' => 'publish',
            ),

            self::ID_ARTIST_EVENT => array(
                'title' => 'Artist Event',
                'status' => 'publish',
            ),

            self::ID_PUBLISHED_PROGRAMS => array(
                'title' => 'Published Programs',
                'status' => 'publish',
            ),

            self::ID_EVENT_PHOTO => array(
                'title' => 'Event Photo',
                'status' => 'publish',
            ),

            self::ID_EVENT_FORM_PRIMARY_TYPE => array(
                'title' => 'Event Form Primary Type',
                'status' => 'publish',
            ),

            self::ID_IMPORT_EVENT_INSTRUCTIONS => array(
                'title' => 'Import Event Tool Instruction',
                'status' => 'publish',
            ),

            self::ID_SUBMIT_EVENT_SUCCESS => array(
                'title' => 'Submit Event Success',
                'status' => 'publish',
            ),
            self::ID_SUBMIT_CLASSIFIED_SUCCESS => array(
                'title' => 'Submit Classified Success',
                'status' => 'publish',
            ),
            self::ID_ORGANIZATION_HEADER => array(
                'title' => 'Organization Header',
                'status' => 'publish',
            ),
            self::ID_VENUE_HEADER => array(
                'title' => 'Venue Header',
                'status' => 'publish',
            ),
            self::ID_ARTIST_HEADER => array(
                'title' => 'Artist Header',
                'status' => 'publish',
            ),
            self::ID_PUBLIC_ART_HEADER => array(
                'title' => 'Public Art Header',
                'status' => 'publish',
            ),
            self::ID_CLASSIFIED_HEADER => array(
                'title' => 'Classified Header',
                'status' => 'publish',
            ),
            self::ID_EDUCATOR_HEADER => array(
                'title' => 'Educator Header',
                'status' => 'publish',
            ),
            self::ID_PROGRAM_HEADER => array(
                'title' => 'Program Header',
                'status' => 'publish',
            ),

            //TAF - page
            self::ID_TAF_POPUP => array(
                'title' => 'TAF Popup',
                'status' => 'publish',
            ),

            // Trilm Dashboard account page
            self::ID_ACCOUNT_INFO => array(
                'title' => 'Account Info',
                'status' => 'publish',
            ),

            self::ID_ACCOUNT_ACTIVITY => array(
                'title' => 'Account Activity',
                'status' => 'publish',
            ),

            self::ID_ACCOUNT_MY_LIST => array(
                'title' => 'Account My List',
                'status' => 'publish',
            ),

            self::ID_ORG_LISTING => array(
                'title' => 'ORG Listing',
                'status' => 'publish',
            ),

            self::ID_AGENCY_EDUCATOR => array(
                'title' => 'Agency Educator',
                'status' => 'publish',
            ),

            self::ID_AGENCY_ORG => array(
                'title' => 'Agency ORG',
                'status' => 'publish',
            ),
            self::ID_AGENCY_VENUES => array(
                'title' => 'Agency Venues',
                'status' => 'publish',
            ),
            self::ID_AGENCY_ARTISTS => array(
                'title' => 'Agency Artists',
                'status' => 'publish',
            ),

            self::ID_EVENTS => array(
                'title' => 'Event Listing',
                'status' => 'publish',
            ),
            self::ID_BLOG_HOME_FEATURED => array(
                'title' => 'Blog Home Featured',
                'status' => 'publish',
            ),
            self::ID_EVENTS_HOME_FEATURED => array(
                'title' => 'Events Home Featured',
                'status' => 'publish',
            ),
            self::ID_PERMISSION_DENIES => array(
                'title' => 'Permission Denies',
                'status' => 'publish',
            ),

            self::ID_ARTIST_MEMBER_REQUIREMENT => array(
                'title' => 'Membership Required',
                'status' => 'publish',
            ),
            self::ID_SUBMIT_PROGRAM_SUCCESS => array(
                'title' => 'Submit Program Success',
                'status' => 'publish',
            ),
            self::ID_SUBMIT_PROGRAM_PREVIEW => array(
                'title' => 'Submit Program Preview',
                'status' => 'publish',
            ),
        );

        return $arrPages;
    }

    public static function getSlugByPageID($id)
    {
        $post = get_post(self::getPageId($id));
        return  $post ? $post->post_name : '';
    }
}
