<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class Apollo_Permission_System  {
    public static function processPermission($arrData)
    {
        $type = $arrData['type']; // access-event
        $user_id = $arrData['user_id'];
        $postType = !empty($arrData['post_type']) ? $arrData['post_type'] : '';

        $isallow = false;
        switch($type) {
            case 'access-event':
                $event_id = $arrData['event_id'];
                $isallow = self::checkUserHavePerAccessEvent($event_id, $user_id);
                break;
            case 'access':
            default:
                $event_id = $arrData['event_id'];
                $isallow = self::checkUserHavePerAccess($event_id, $user_id, $postType);
        }

        if($isallow) return true;

        $action = $arrData['fail_action'];
        switch($action) {
            case 'redirect':
                $url = home_url();
                if(isset($arrData['fail_data']['url'])) {
                    $url = $arrData['fail_data']['url'];
                }
                Apollo_App::safeRedirect($url);
                exit();
                break;

            default:
                wp_die(__('Permission deny!', 'apollo'));
        }

    }

    private static function checkUserHavePerAccessEvent($event_id, $user_id) {

        global $wpdb;

        $orgArrID = Apollo_User::getUserItemIDs(Apollo_DB_Schema::_ORGANIZATION_PT);
        $orgIDStr = implode(',', $orgArrID);

        $apollo_meta_table = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};
        $apollo_event_org_table = $wpdb->{Apollo_Tables::_APL_EVENT_ORG};

        $additionalWhere = array(
            "p.post_author= $user_id"
        );

        if ($orgIDStr) {
            $additionalWhere[] = "event_org.org_id IN ($orgIDStr)";
        }

        $addWhereStr = implode(' OR ', $additionalWhere);
        $query = "select 1 from {$wpdb->posts} p
        INNER JOIN {$apollo_meta_table} mt_venue_id ON p.ID   = mt_venue_id.apollo_event_id AND mt_venue_id.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_VENUE ."'
        INNER JOIN {$apollo_event_org_table} event_org ON p.ID   = event_org.post_id
        where p.ID = '%s' and ($addWhereStr)
";

        //prevent error: wpdb::prepare was called incorrectly.
        $wpdb->query($wpdb->prepare($query, $event_id));

        return $wpdb->last_query;
    }

    private static function checkUserHavePerAccess($event_id, $user_id, $postType) {
        global $wpdb;
        $query = "select 1 from {$wpdb->posts} where ID = '%s' and post_author= '%s'";
        $result = $wpdb->query($wpdb->prepare($query, $event_id, $user_id));

        if($result) return true;

        /**
         * @ticket #19641: [CF] 20190402 - [Classified] Add the Associated User(s) field to main admin list and detail form - Item 5
         * Get associate post_id
         */
        $table  = $wpdb->prefix . Apollo_Tables::_APL_USER_MODULES;
        $query = "select 1 from $table where post_id = '%s' and user_id= '%s' and post_type = '%s'";
        $result = $wpdb->query($wpdb->prepare($query, $event_id, $user_id, $postType));

        return $result;
    }
}