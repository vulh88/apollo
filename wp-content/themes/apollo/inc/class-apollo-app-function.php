<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class Apollo_App {

    const INDIVIDUAL_EVENTS_PAGESIZE_DEFAULT = 10;

    public static $uploadBaseDir = array();
    public static $uploadBaseUrl = array();

    public static $apl_state = array();

    private static $currentBlog = array();

    /**
     * Render Template
     * @param $template
     * @param array $data
     * @return string
     */
    public static function renderHTML($template, $data = array()) {

        ob_start();
        include $template;
        return ob_get_clean();
    }

    /**
     * Require a template part
     * @param $file
     * @param $model
     * @param bool $return
     * @return false|string
     */
    public static function getSimpleTemplatePart( $file, $model, $return = false ) {

        $fileBasename = basename($file);

        if ( !file_exists($file) && file_exists( get_stylesheet_directory() . '/' . $fileBasename . '.php' ) ) {
            $file = get_stylesheet_directory() . '/' . $fileBasename . '.php';
        }elseif ( !file_exists($file) && file_exists( get_template_directory() . '/' . $fileBasename . '.php' ) ){
            $file = get_template_directory() . '/' . $fileBasename . '.php';
        }
        ob_start();
        require( $file );
        $data = ob_get_clean();

        if ( $return ){
            return $data;
        }
        echo $data;
    }

    /**
     * Modules data with keys and labels
     * @return array
     */
    public static function modulesData()
    {
        return array(
            Apollo_DB_Schema::_EVENT_PT => __( 'Events', 'apollo' ),
            'post' => __( 'Blog ', 'apollo' ),
            Apollo_DB_Schema::_ORGANIZATION_PT => __( 'Orgs', 'apollo' ),
            Apollo_DB_Schema::_VENUE_PT => __( 'Venues', 'apollo' ),
            Apollo_DB_Schema::_CLASSIFIED => __( 'Classifieds', 'apollo' ),
            Apollo_DB_Schema::_ARTIST_PT => __( 'Artists', 'apollo' ),
            Apollo_DB_Schema::_PUBLIC_ART_PT => __( 'Public Arts', 'apollo' ),
            Apollo_DB_Schema::_PROGRAM_PT => __( 'Programs', 'apollo' ),
            Apollo_DB_Schema::_BUSINESS_PT => __( 'Business', 'apollo' ),
            Apollo_DB_Schema::_NEWS_PT => __('News', 'apollo'),
            'page' => __( 'Pages', 'apollo' ),
        );
    }

    /**
     * @param $postID
     * @param $metaData
     * @return boolean
     */
    public static function storeMultipleMetaData($postID, $metaData) {
        if (empty($metaData)) {
            return false;
        }

        foreach ($metaData as $key => $value) {
            update_apollo_meta( $postID, $key, $value );
        }
        return true;
    }

    /**
     * Render tabs for theme options
     * @author vulh
     * @param $selected
     * @return string
     */
    public static function render_tabs_for_theme_options($selected) {


        $ac_mods = self::get_avaiable_modules();

        if ( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EDUCATION ) ) {
            $ac_mods[] = Apollo_DB_Schema::_PROGRAM_PT;
            $ac_mods = array_values( array_diff( $ac_mods , array( Apollo_DB_Schema::_EDUCATION ) ) );
        }

        if (!$ac_mods) {
            $ac_mods = array();
        }

        $result = '';

        $mods = self::modulesData();

        foreach ( $mods as $k => $v ):

            if ( ! in_array( $k , $ac_mods ) && $k !== 'page') continue;
            $selectedOption = is_array($selected) && in_array($k, $selected) ? 'selected="selected"' : '';
            $result .= '<option '.$selectedOption.' class="level-0" value="'.$k.'" >'.$v.'</option>';
        endforeach;

        return $result;
    }

    /**
     * Get bypass pending approve for a current user
     * @return boolean
     */
    public static function bypassEventPendingApproveLoggedinUser()
    {
        // Check Global bypass config first
        if (!of_get_option(Apollo_DB_Schema::_ENABLE_EVENT_BYPASS_APPROVAL_PROCESS, false)) {
            return false;
        }


        // Allow to all users
        if (of_get_option(Apollo_DB_Schema::_ENABLE_EVENT_BYPASS_APPROVAL_METHOD, 1) == 1) {
            return true;
        }


        $listOfUserCan = get_site_option(Apollo_Tables::_APL_BYPASS_PENDING_APPROVAL, array());
        $currentUserId = get_current_user_id() ? get_current_user_id() : -1;
        return in_array($currentUserId, $listOfUserCan);
    }

    /**
     * @Ticket #13274
     * @Ticket #13436 - order by spotlights
     * @return array|null|object
     */
    public static function getHomeSpotlights() {
        global $wpdb;
        $num = ($num_opt = of_get_option( Apollo_DB_Schema::_NUM_HOME_SPOTLIGHT_EVENT )) ? $num_opt
            : Apollo_Display_Config::NUM_HOME_SPOTLIGHT;
        $str_query = "SELECT p.ID, p.post_title, p.post_content FROM ". $wpdb->posts ." p   
                      LEFT JOIN ". $wpdb->postmeta ." pms ON p.ID=pms.post_id AND pms.meta_key='". Apollo_DB_Schema::_APL_SPOT_LIGHT_START_DATE ."'
                      LEFT JOIN ". $wpdb->postmeta ." pme ON p.ID=pme.post_id AND pme.meta_key='". Apollo_DB_Schema::_APL_SPOT_LIGHT_END_DATE ."'
                      WHERE p.post_status='publish' AND p.post_type='". Apollo_DB_Schema::_SPOT_PT ."' AND (
                      ((pms.meta_value IS NULL OR pms.meta_value='') AND (pme.meta_value IS NULL OR pme.meta_value=''))
                      OR ((pms.meta_value IS NULL OR pms.meta_value='') AND pme.meta_value>='". current_time('Y-m-d') ."')
                      OR ((pme.meta_value IS NULL OR pme.meta_value='' OR pme.meta_value>='". current_time('Y-m-d') ."') AND ((pms.meta_value<='". current_time('Y-m-d') ."' OR pms.meta_value IS NULL OR pms.meta_value='')))
                      ) ORDER BY p.menu_order ASC LIMIT 0, ". $num;
        return $wpdb->get_results($str_query);
    }

    public static function renderCities($listCityState=array(), $selectedValue = ''){
        if(empty($listCityState)){
            return '';
        }
        $cities = array();
        foreach($listCityState as $k => $cityItem){
            if(!empty($cityItem)){
                $cities[$k] = $cityItem;
            }
        }
        $result = '';
        if(sizeof($cities) > 2){
            foreach($cities as $k => $cityItem){
                $result .= '<optgroup ' . ($k ? 'label="'.$k.'"' : '') . '>';
                    if (!$k){
                        $result .= '<option value="0">' . $cityItem . '</option>';
                    }

                    if ($cityItem && is_array($cityItem)){
                        foreach ( $cityItem as $city ){
                            $selected = '';
                            if(!empty($selectedValue) && $city ==  $selectedValue){
                                $selected = 'selected';
                            }
                            $result .= '<option ' . $selected . ' value="' . $city . '" data-state="'.$k.'">' . $city .'</option>';
                        }
                    }
                $result .= '</optgroup>';
            }
        }else{
            foreach($cities as $k => $cityItem){
                if (!$k){
                    $result .= '<option value="0">' . $cityItem . '</option>';
                }

                if ($cityItem && is_array($cityItem)){
                    foreach ( $cityItem as $city ){
                        $selected = '';
                        if(!empty($selectedValue) && $city ==  $selectedValue){
                            $selected = 'selected';
                        }
                        $result .= '<option ' . $selected . ' value="' . $city . '" data-state="'.$k.'">' . $city . '</option>';
                    }
                }
            }
        }
        return $result;
    }

    /**
     * @Description get custom url by module name with existing configuration custom slug OR not
     * @param string $module
     * @Author ThienLD
     * @return string
     */
    public static function getCustomUrlByModuleName($module = ''){
        $slug = $module ? self::getCustomSlugByModuleName($module) : '';
        return home_url($slug);
    }

    /**
     * Get custom slug by module name
     * @author vulh
     * @param string $module
     * @ticket c
     * @return string
     */
    public static function getCustomSlugByModuleName($module = ''){
        switch ($module) {
            case Apollo_DB_Schema::_CLASSIFIED:
                $slug = of_get_option(Apollo_DB_Schema::_CLASSIFIED_CUSTOM_SLUG,'classified');
                break;
            case Apollo_DB_Schema::_BLOG_POST_PT:
                $slug = of_get_option(Apollo_DB_Schema::_BLOG_CUSTOM_SLUG,'blog');
                break;
            case Apollo_DB_Schema::_ARTIST_PT:
                $slug = of_get_option(Apollo_DB_Schema::_ARTIST_CUSTOM_SLUG,'artist');
                break;
            case Apollo_DB_Schema::_NEWS_PT:
                $slug = of_get_option(APL_News_Config::_NEWS_CUSTOM_SLUG,'news');
                break;
            case Apollo_DB_Schema::_VENUE_PT:
                $slug = of_get_option(APL_Theme_Option_Site_Config_SubTab::VENUE_CUSTOM_SLUG,'venue');
                break;
            default:
                $slug = $module;
                break;
        }

        return $slug;
    }

    /**
     * Custom tax slug by module name
     * @param string $module
     * @Author vulh
     * @return string
     */
    public static function getTaxSlugByModuleName($module = ''){
        switch ($module) {
            case Apollo_DB_Schema::_BUSINESS_PT:
                $slug = of_get_option( APL_Theme_Option_Site_Config_SubTab::_BUSINESS_TYPE_CUSTOM_SLUG, Apollo_DB_Schema::_BUSINESS_TYPE);
                break;
            case Apollo_DB_Schema::_VENUE_PT:
                $slug = of_get_option( APL_Theme_Option_Site_Config_SubTab::VENUE_TYPE_CUSTOM_SLUG, 'venue-type');
                break;
            case Apollo_DB_Schema::_EVENT_PT:
                $slug = 'categories';
                break;
            case 'post':
                $slug = 'category';
                break;
            default:
                $slug = $module.'-type';
                break;
        }
        return $slug;
    }

    /**
     * @Description get custom label by module name with existing configuration custom label OR not
     * @param string $module
     * @Author ThienLD
     * @return string
     */
    public static function getCustomLabelByModuleName($module = ''){
        switch ($module) {
            case Apollo_DB_Schema::_CLASSIFIED:
                $label = of_get_option(Apollo_DB_Schema::_CLASSIFIED_CUSTOM_LABEL,'classified');
                break;
            case Apollo_DB_Schema::_BLOG_POST_PT:
                $label = of_get_option(Apollo_DB_Schema::_BLOG_CUSTOM_LABEL,'blog');
                break;
            case Apollo_DB_Schema::_ARTIST_PT:
                $label = of_get_option(Apollo_DB_Schema::_ARTIST_CUSTOM_LABEL,'artist');
                break;
            case Apollo_DB_Schema::_NEWS_PT:
                $label = of_get_option(APL_News_Config::_NEWS_CUSTOM_LABEL,'news');
                break;
            case Apollo_DB_Schema::_VENUE_PT:
                $label = of_get_option(APL_Theme_Option_Site_Config_SubTab::VENUE_CUSTOM_LABEL,'venue');
                break;
            default:
                $label = '';
                break;
        }
        return ucfirst($label);
    }

    /**
     * @ticket #17123: Modify some styles, labels, the artist slug for the artist directory page
     * @param string $module
     * @return bool|string
     */
    public static function getSingleLabelByModuleName($module = ''){
        switch ($module) {
            case Apollo_DB_Schema::_ARTIST_PT:
                $label = of_get_option(Apollo_DB_Schema::_ARTIST_CUSTOM_SINGLE_LABEL, 'artist');
                break;
            default:
                $label = '';
                break;
        }
        return $label;
    }

    public static function hiddenCategoryIDs() {

        $aplQuery = new Apl_Query(Apollo_Tables::_APOLLO_TERM_META);
        $metas = $aplQuery->get_where(sprintf(" meta_key = '%s' AND meta_value = 1 ", Apollo_DB_Schema::_APL_EVENT_TAX_HIDE));
        if (!$metas) return array();

        $result = array();
        $currentUserId = get_current_user_id();

        $categoryForUsers = self::_categoryOnlyForUsers();

        foreach($metas as $meta ) {

            if (!isset($categoryForUsers[$meta->apollo_term_id])) {
                $result[] = $meta->apollo_term_id;
                continue;
            }

            // User IDs can see this category
            $onlyForUsers = $categoryForUsers[$meta->apollo_term_id];


            if ($onlyForUsers && $currentUserId && in_array($currentUserId, $onlyForUsers)) {
                continue;
            }
            $result[] = $meta->apollo_term_id;
        }

        return $result;
    }

    /**
     * Vandd @ticket #12232
     * Get information about available image sizes
     * @param string $size
     * @return array|bool|mixed
     */
    public static function get_image_sizes( $size = '' ) {

        global $_wp_additional_image_sizes;

        $sizes = array();
        $get_intermediate_image_sizes = get_intermediate_image_sizes();

        // Create the full array with sizes and crop info
        foreach( $get_intermediate_image_sizes as $_size ) {
            if ( in_array( $_size, array( 'thumbnail', 'medium', 'large' ) ) ) {
                $sizes[ $_size ]['width'] = get_option( $_size . '_size_w' );
                $sizes[ $_size ]['height'] = get_option( $_size . '_size_h' );
                $sizes[ $_size ]['crop'] = (bool) get_option( $_size . '_crop' );
            } elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
                $sizes[ $_size ] = array(
                    'width' => $_wp_additional_image_sizes[ $_size ]['width'],
                    'height' => $_wp_additional_image_sizes[ $_size ]['height'],
                    'crop' =>  $_wp_additional_image_sizes[ $_size ]['crop']
                );
            }
        }

        // Get only 1 size if found
        if ( $size ) {
            if( isset( $sizes[ $size ] ) ) {
                return $sizes[ $size ];
            } else {
                return false;
            }
        }
        return $sizes;
    }

    /**Vandd @ticket #12148
     * @param $type
     * @return array
     */
    public static function renderOptionsSwapperMenu($type){
        $result = array();
        $theme_locations = get_registered_nav_menus();
        if(sizeof($theme_locations) > 0){
            if($type === 'target'){
                $result['none'] = __('None (Swapper will not affect menu)', 'apollo');
                $result['all'] = __('Any/All (will affect all menus)', 'apollo');
            }
            foreach ($theme_locations as $slug => $name){
                $result[$slug] = $name;
            }
        }
        return $result;
    }

    /**
     * check has more
     *
     * @param $page integer
     * @param $limit integer
     * @param $total integer
     *
     * @return boolean
     */
    public static function hasMore($page, $limit, $total) {
        return $page * $limit < intval($total);
    }

    /**
     * @param $artists
     * @param $metaArt
     *
     * @return string
     */
    public static function renderArtists($artists = array(), $metaArt = array(), $frontend = false){
        if(empty($artists)){
            return '';
        }
        $result = '';
        foreach ( $artists as $key => $val ) {
            if(empty($metaArt)){
                $checked = '';
            }else{
                $checked = in_array($key, $metaArt) ? 'checked' : '';
            }
            $post_edit_link = get_edit_post_link($key, '');
            if($post_edit_link != null){
                $post_edit_link = '<a class="edit-link" target="_blank" href="' . $post_edit_link . '">' . __("Edit", "apollo") . '</a>';
            }else{
                $post_edit_link = '';
            }
            $result .= '<li>
            <input ' . $checked . ' type="checkbox" class="artist-checked" value="' . $key . '" />
             <span>' . $val . '</span> ' . (!$frontend ? $post_edit_link : '') . '</li>';
        }
        return $result;
    }

    /**
     * render multiple checkboxes
     * @param array $associationData
     * @return string
     */
    public static function userRenderAssociationItems($associationData = array()) {
        if(empty($associationData)){
            return '';
        }
        $result = '';
        foreach ( $associationData as $val ) {
            $isAuthor = isset($val->is_author) ? (bool) $val->is_author : false;

            $checked = ($val->item_selected || $isAuthor || (!empty($val->agency_id)) ) ? 'checked' : '';
            if (isset($val->post_author) && !is_super_admin($val->post_author) && !user_can($val->post_author, 'manage_options')) {
                $disabled = $isAuthor ? 'disabled' : '';
                $ownerText = $isAuthor ? __(' (owner)', 'apollo') : '';
            } else {
                if ($isAuthor && !$val->item_selected) {
                    $checked = '';
                }
                $disabled = '';
                $ownerText = '';
            }

            $result .= '<li>
            <input ' . $checked . ' ' . $disabled . ' type="checkbox" class="association-checked" value="' . $val->ID . '" />
            <span>' . $val->post_title . $ownerText . ((isset($val->post_status) && $val->post_status == 'pending') ? ' (*)' : '') . '</span></li>';
        }
        return $result;
    }

    /**
     * Get meta artists
     * @param $ids
     *
     * @return array
     */
    public  static function getMetaArtists($ids = array()){
        if(empty($ids)){
            return array();
        }
        global $wpdb;
        $sql = "
            SELECT p.* FROM {$wpdb->posts} p
            INNER JOIN {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}} mt_artisrt ON mt_artisrt.meta_key ='". Apollo_DB_Schema::_APL_ARTIST_DISPLAY_PUBLIC ."' AND mt_artisrt.apollo_artist_id = p.ID
            INNER JOIN {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}} mt_lname ON mt_lname.meta_key ='". Apollo_DB_Schema::_APL_ARTIST_LNAME ."' AND mt_lname.apollo_artist_id = p.ID
            WHERE p.post_type = '".Apollo_DB_Schema::_ARTIST_PT."'
            AND p.post_status = 'publish'
            AND mt_artisrt.meta_value = 'yes'
            AND (p.ID IN (". join(',', $ids) ."))
            ORDER BY mt_lname.meta_value ASC "
        ;
        $artist = $wpdb->get_results( $sql );

        $_arr = array();
        if ( $artist ) {
            foreach ( $artist as $e ) {
                $_arr[$e->ID] = $e->post_title;
            }
        }
        return $_arr;
    }

    /**
     * @param $artists
     *
     * @return string
     */
    public static function renderMetaArtists($artists = array()){
        if(empty($artists)){
            return '';
        }
        $result = array();
        foreach ( $artists as $key => $val ) {
            array_push($result,'<a class="artist-default" target="_blank" href="' . get_edit_post_link($key, '') . '">' . $val . '</a>');
        }
        if(!empty($result)){
            return join(", ", $result);
        }else{
            return '';
        }
    }

    /**
     * Get category only for user
     *
     */
    private static function _categoryOnlyForUsers() {

        $aplQuery = new Apl_Query(Apollo_Tables::_APOLLO_TERM_META);
        $metas = $aplQuery->get_where(sprintf(" meta_key = '%s' AND meta_value IS NOT NULL AND meta_value <> ''  ", Apollo_DB_Schema::_APL_EVENT_TAX_HIDE_EXCEPT_USER_IDS));
        if (!$metas) return array();

        $result = array();
        foreach($metas as $meta ) {

            if (!$forUsers = $meta->meta_value) continue;

            // Do not have any logged in user or not for current user
            $result[$meta->apollo_term_id] = explode(',', $forUsers);
        }

        return $result;
    }

    /**
     * Serialize to base 64 encode
     * @param $data
     * @return string
     */
    public static function serialize($data) {
        return base64_encode(maybe_serialize($data));
    }

    /**
     * Unserialize support base 64 encode
     * @param $data
     * @return mixed|string
     */
    public static function unserialize($data) {

        if (is_array($data)) return $data;

        if (self::is_base64_encoded($data)) {
            $data = base64_decode($data);
        }

        $result = maybe_unserialize($data);
        if (! is_array($result)) return maybe_unserialize(maybe_unserialize($data));
        return $result;
    }

    /**
     * Check is base 64 encode
     * @param $data
     * @return int
     */
    public static function is_base64_encoded($data)
    {
        return preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $data);
    }

    /**
     * vulh: Check whether string is html or not
     * @return boolean
     **/
    public static function isHtmlString($string) {
        return preg_match('#(?<=<)\w+(?=[^<]*?>)#', $string);
    }

    /**
     * vulh: remove CDATA tags
     * @return string
     **/
    static function removeCDATATag($content) {
        $content = str_replace( '// <![CDATA[', '', $content );
        $content = str_replace( '// ]]>', '', $content );
        return $content;
    }

    /**
     * Thienld: check if request is iframe search widget
     * @return boolean
    **/
    public static function isSearchWidgetRequest(){
        return isset($_GET[_APL_IFRAME_SEARCH_WIDGET_FLAG]) && isset($_GET['ifswid']);
    }

    /**
     * filter content , which contain short-code
     * @author : TruongHN
     * @param $content
     * @return mixed|string|void
     */
    public static function the_content($content)
    {
        if ($content == '') {
            return '';
        } else {
            $content = apply_filters('the_content', $content);
            $content = str_replace(']]>', ']]&gt;', $content);
            return $content;
        }

    }

    /**
     * get number of images pulled from module in Photo Slider Short COde
     * @author Truonghn
     * @param $post_id
     * @param string $module
     * @return mixed|string|void
     */
    public static function getNumberImagePulledFromModule( $post_id, $module = ''){

        if ( empty($module) || empty($post_id)){
            return;

        }
        $isEnable = self::apollo_get_meta_data($post_id, 'enable_images_'.$module ) == 'ON';
        $numberImages = self::apollo_get_meta_data($post_id, 'number_images_'.$module );
        if ( $isEnable){
            return $numberImages ;
        }
        return '';

    }

    public static function getModulesForPhotoSlider(){
        $apollo_modules = array_flip(self::get_avaiable_modules());
        if ( !empty($apollo_modules) ) {
            unset($apollo_modules['agency'],$apollo_modules['business'],$apollo_modules['syndication'],$apollo_modules['iframesw']);
            unset($apollo_modules['photo-slider'],$apollo_modules['post']);
            if ( self::is_avaiable_module(Apollo_DB_Schema::_EDUCATION) ){
                unset( $apollo_modules[Apollo_DB_Schema::_EDUCATION]);
                $apollo_modules[Apollo_DB_Schema::_EDUCATOR_PT]  = Apollo_DB_Schema::_EDUCATOR_PT;
            }
        }
        return $apollo_modules;

    }

    public static function formatModuleTitle($module = ''){
        if ( empty( $module )) {
            return;
        }
        $title = ucfirst( $module );
        $title = str_replace('-', ' ', $title);
        return $title;
    }


    public static function getDataFromModulePulled($module = '', $postId , $checkHasData = false){
        if( empty($module )|| empty($postId) ){
            return;
        }
        $numberImages = !$checkHasData ?  self::getNumberImagePulledFromModule($postId, $module ) : -1 ;
        if(intval($numberImages) === 0){
            // in case of number of slider items in module's setting is ZERO => do not dispaly any thing in here.
            return array();
        }
        if($module == Apollo_DB_Schema::_CLASSIFIED ){
            add_filter('posts_where', array(__CLASS__, 'photoSliderFilterWhereOnModuleClassified'), 10, 1);
        }
        $data = query_posts(array(
            'orderby' => 'rand' ,
            'post_type' => $module,
            'post_status' => 'publish',
            'showposts' => intval($numberImages),
            'meta_query' => array( array('key' => '_thumbnail_id') )
        ));
        if($module == Apollo_DB_Schema::_CLASSIFIED ){
            remove_filter('posts_where', array(__CLASS__, 'photoSliderFilterWhereOnModuleClassified'), 10);
        }
        wp_reset_postdata();
        wp_reset_query();
        return $data;
    }

    public static function photoSliderFilterWhereOnModuleClassified ($where)
    {
        global $wpdb;
        $_current_date = current_time('Y-m-d');
        $sqlString = "AND  " . $wpdb->posts . ".ID IN (
                    SELECT em.apollo_classified_id
                    FROM " . $wpdb->{Apollo_Tables::_APL_CLASSIFIED_META} . " em
                    WHERE em.apollo_classified_id  = " . $wpdb->posts . ".ID
                        AND em.meta_key = '" . Apollo_DB_Schema::_APL_CLASSIFIED_EXP_DATE . "'
                        AND em.meta_value >= '{$_current_date}'
                )";
        return $where .= $sqlString;
    }

    public static  function getMetaDataForGallery( $postId = 0 , $postType ='', $key = '') {

        if ( intval($postId) < 1 || empty($postType) ){
            return;
        }
        if ( in_array($postType, array(Apollo_DB_Schema::_BLOG_POST_PT, 'page')) ) {
            $data = get_post_meta($postId, $key, true );
        } else {
            $data = get_apollo_meta($postId, $key, true );

        }
        return $data;
    }

    public static  function saveMetaDataForGallery( $postId = 0 , $postType ='', $data = '', $key = '') {

        if ( intval($postId) < 1 || empty($postType)  ){
            return;
        }
        if ( in_array($postType, array(Apollo_DB_Schema::_BLOG_POST_PT, 'page')) ) {
             update_post_meta($postId, $key, $data );
        } else {
            update_apollo_meta($postId, $key, $data);

        }
    }


    public static function defaultBlogImage($size = 'medium', $onlyUrl = true, $auto = true) {
        if (! $defaultImage = of_get_option(Apollo_DB_Schema::_DEFAULT_NO_IMAGE)) {
            $defaultImage = apollo_event_placeholder_img_src($size, '', $auto);
        }

        if ($onlyUrl) {
            return $defaultImage;
        }

        return '<img src="'.$defaultImage.'" />';
    }

    /**
     ** Thienld: Functionality handles for remote data on select2 ajax request auto-completion
     */
    public static function getDDLItemsBySearchKeyword($args = array()){
        $postType = isset($args['post_type']) ? $args['post_type'] : '';
        $return = '';
        if(
            $postType == Apollo_DB_Schema::_ORGANIZATION_PT
            && of_get_option(Apollo_DB_Schema::_ORGANIZATION_ACTIVE_MEMBER_FIELD)
        ){
            $return = self::queryOrgPost($args);
        }
        else{
            $return = self::queryPost($args);
        }
        return $return;
    }

    /**
     * @Ticket #15306 - Get data load more
     * @param array $args
     * @return array
     */
    public static function agencyGetItemBySearchKeyword($args = array ()) {
        $postType = isset($args['post_type']) ? $args['post_type'] : '';

        $sKeyword = isset($args['s']) ? $args['s'] : '';
        $page = isset($args['page']) ? intval($args['page']) : 1;
        $limit = isset($args['limit']) ? $args['limit'] : -1;
        $order = isset($args['order']) ? $args['order'] : 'ASC';
        $orderBy = isset($args['orderBy']) ? $args['orderBy'] : 'title';
        /** @Ticket #14800 */
        $postIn = isset($args['post_in']) ? $args['post_in'] : array();

        $result = array(
            'total' => 0,
            'items' => array()
        );
        if(empty($postType)){
            return $result;
        }
        $params = array(
            'post_type'         => $postType,
            'orderby'           => $orderBy,
            'order'             => $order,
            'posts_per_page'    => $limit,
            'paged'             => $page,
            'post__in'           => $postIn,
            'sentence'          => 1, // Exactly search
        );
        if ($sKeyword) {
            $params['s'] = $sKeyword;
        }

        $listPost = query_posts( $params );
        global $wp_query;
        $total = $wp_query->found_posts;
        wp_reset_query();
        $result['total'] = $total;
        $result['items'] = $listPost;
        $result['haveMore'] = $page * $limit < $total;

        return $result;
    }

    /**
     * @author Trilm
     * @param bool $is_get_total
     * @param int $page
     * @param int $pagesize
     * @return string
     */
    public static function queryPost($args){
        $sKeyword = isset($args['s']) ? $args['s'] : '';
        $page = isset($args['page']) ? intval($args['page']) : 1;
        $limit = isset($args['limit']) ? $args['limit'] : -1;
        $postType = isset($args['post_type']) ? $args['post_type'] : '';
        $selectItem = isset($args['selected_item']) ? $args['selected_item'] : '';
        $order = isset($args['order']) ? $args['order'] : 'ASC';
        $orderBy = isset($args['orderBy']) ? $args['orderBy'] : 'title';
        /** @Ticket #14800 */
        $postNotIn = isset($args['post_not_in']) ? $args['post_not_in'] : array();

        $result = array(
            'total' => 0,
            'items' => array()
        );
        if(empty($postType)){
            return $result;
        }
        $params = array(
            'post_type'         => $postType,
            'orderby'           => $orderBy,
            'order'             => $order,
            'post_status'       => isset($args['post_status']) ? $args['post_status'] : 'publish',
            'posts_per_page'    => $limit,
            'paged'             => $page,
            'post__not_in'      => $postNotIn,
            'sentence'          => 1, // Exactly search
        );
        if(!empty($selectItem)){
            if (is_array($selectItem)) {
                $params['post__in'] = $selectItem;
            } else if (intval($selectItem) > 0) {
                $params['post__in'] = array(intval($selectItem));
            }
        }
        if ($sKeyword) {
            $params['s'] = $sKeyword;
        }

        $listPost = query_posts( $params );
        global $wp_query;
        $total = $wp_query->found_posts;
        wp_reset_query();
        $result['total'] = $total;
        $result['items'] = $listPost;
        $result['haveMore'] = $page * $limit < $total;
        return $result;
    }

    /**
     * @author Trilm
     * @param $args
     * @return string
     */
    public static function queryOrgPost($args){
        $result = '';
        $orgSelect2 = apl_instance('APL_Lib_Helpers_Select2Org', array(
            'post_type' => Apollo_DB_Schema::_ORGANIZATION_PT,
            'selected_item' => array(),
        ));
        $orgs = $orgSelect2->getMemberOrg($args);
        return  $orgs;
    }

    public static function getSQLIndividualEvents($is_get_total = false, $page = 1, $pagesize = self::INDIVIDUAL_EVENTS_PAGESIZE_DEFAULT, $orderBy = 'date', $sortDefault = 'ASC')
    {
        global $wpdb;
        $currentDate = current_time('Y-m-d');
        $postStatus = array('publish');
        $nextPageIndex = (intval($page) - 1) * $pagesize;
        $select = $is_get_total ? " COUNT(p.ID) AS total " : " p.* ";
        $paging = $is_get_total ? "" : "LIMIT ".$nextPageIndex.",".$pagesize."";
        $order = ($orderBy == 'date') ? 'mt_end_d.meta_value' : 'post_title';

        $sql = "
			SELECT ".$select."
			FROM ".$wpdb->posts." p
			INNER JOIN ".$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}." mt_end_d
				ON p.ID = mt_end_d.apollo_event_id
				AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
				AND ( CAST( mt_end_d.meta_value AS DATE )  >= '{$currentDate}' OR mt_end_d.meta_value  = '' )
			WHERE p.post_type = '".Apollo_DB_Schema::_EVENT_PT."'
			AND p.post_status IN ('".implode("','",$postStatus)."')
			ORDER BY " . $order . " " . $sortDefault . "
			".$paging."
		";
        return $sql;
    }

    /**
     * Get sql for individual orgs
     *
     * @param int  $page
     * @param int  $pageSize
     *
     * @ticket #11493
     * @return string
     */
    public static function getSQLIndividualOrgs($page = 1, $pageSize = self::INDIVIDUAL_EVENTS_PAGESIZE_DEFAULT)
    {
        global $wpdb;

        $postStatus    = array('publish');
        $nextPageIndex = (intval($page) - 1) * $pageSize;
        $select        = " p.ID, p.post_title";
        $paging        = "LIMIT " . $nextPageIndex . "," . $pageSize . "";

        $sql = "
			SELECT SQL_CALC_FOUND_ROWS " . $select . "
			FROM " . $wpdb->posts . " p
			WHERE p.post_type = '" . Apollo_DB_Schema::_ORGANIZATION_PT . "'
			AND p.post_status IN ('" . implode("','", $postStatus). "')
			ORDER BY p.post_title
			" . $paging . "
		";

        return $sql;
    }

    /**
     * Get sql for individual venues
     *
     * @param int  $page
     * @param int  $pageSize
     *
     * @ticket #11493
     * @return string
     */
    public static function getSQLIndividualVenues($page = 1, $pageSize = self::INDIVIDUAL_EVENTS_PAGESIZE_DEFAULT)
    {
        global $wpdb;

        $postStatus    = array('publish');
        $nextPageIndex = (intval($page) - 1) * $pageSize;
        $select        = " p.ID, p.post_title";
        $paging        = "LIMIT " . $nextPageIndex . "," . $pageSize . "";

        $sql = "
			SELECT SQL_CALC_FOUND_ROWS " . $select . "
			FROM " . $wpdb->posts . " p
			WHERE p.post_type = '" . Apollo_DB_Schema::_VENUE_PT . "'
			AND p.post_status IN ('" . implode("','", $postStatus). "')
			ORDER BY p.post_title
			" . $paging . "
		";

        return $sql;
    }

    public static function state($state = null, $value = null) {

        if(!is_null($state) && !is_null($value)) {
            return self::$apl_state[$state][] = $value;
        }

        if(is_null($value) & !is_null($state)) {
            return isset(self::$apl_state[$state]) ? self::$apl_state[$state] : '';
        }

        return self::$apl_state;
    }

    public static function destruct(){
        $astate = self::state();

        foreach($astate as $state => $manycall) {
            switch($state) {
                case Apollo_State::REMOVE_USER:
                    foreach($manycall as $params) {
                        Apollo_User::_rmuserFromDB($params[0], $params[1]);
                    }
                    break;
            }
        }
    }

    public static function autload($class) {
        // Try to get class automatically
        $class = strtolower($class);
        $file_path = APOLLO_INCLUDES_DIR.DIRECTORY_SEPARATOR.str_replace('_', '-', $class).'.php';

        if(file_exists($file_path)) {
            require_once $file_path;
        }
    }

    public static function handleAdminUrlIntegrateWPML( $url, $path = '', $blog_id = 0){
        if ( Apollo_App::hasWPLM() ) {
            global $sitepress;
            $langFormatUrlType = $sitepress->get_setting('language_negotiation_type');
            if( ! in_array(intval($langFormatUrlType), array(2,3)) ){
                return $url;
            }
            // Only handle custom admin_ajax url for Language Negotiation Type = 'A different domain per language'
            $urlData = parse_url($url);
            if(isset($urlData['path']) && strpos($urlData['path'],'admin-ajax.php') !== false){
                $defaultLang = $sitepress->get_default_language();
                if(ICL_LANGUAGE_CODE != $defaultLang){
                    $urlLangCode = ICL_LANGUAGE_CODE . '.';
                    $argLang = ICL_LANGUAGE_CODE;
                } elseif(isset($_GET['lang']) && $_GET['lang'] != $defaultLang){
                    $urlLangCode = $_GET['lang'] . '.';
                    $argLang = $_GET['lang'];
                }else {
                    // default language
                    $urlLangCode = '';
                    $argLang = '';
                }
                $scheme = isset($urlData['scheme']) ? $urlData['scheme'] : '';
                $host = isset($urlData['host']) ? $urlData['host'] : '';
                $urlPath = isset($urlData['path']) ? $urlData['path'] : '';
                $qsString = isset($urlData['query']) ? $urlData['query'] : '';
                if(!empty($argLang)){
                    $qsString = !empty($qsString) ? $qsString . '&lang=' . $argLang : 'lang=' . $argLang;
                }
                if(intval($langFormatUrlType) === 2){
                    // subdomain url : http://lang.domain_name
                    $wpmlAdminUrl = $scheme . '://' . $urlLangCode . $host . $urlPath . '?' . $qsString;
                } elseif(intval($langFormatUrlType) === 3){
                    // Language name added as a parameter (http://domain_name?lang=es)
                    $wpmlAdminUrl = $scheme . '://' . $host . $urlPath . '?' . $qsString;
                } else {
                    $wpmlAdminUrl = $url;
                }
                return $wpmlAdminUrl;
            }
        }
        return $url;
    }

    public static function apl_date_spanish($format, $timestamp = null) {

        $param_M = array('', __('Jan', 'apollo'), __('Feb', 'apollo'),
            __('Mar', 'apollo'), __('Apr', 'apollo'), __('May', 'apollo'), __('Jun', 'apollo'),
            __('Jul', 'apollo'), __('Aug', 'apollo'), __('Sep', 'apollo'), __('Oct', 'apollo'),
            __('Nov', 'apollo'), __('Dec', 'apollo'));

        $return = '';

        if ( is_null($timestamp) ) {
            $timestamp = mktime();
        }

        $len = strlen($format);
        for($i = 0; $i < $len; $i++) {
            switch($format[$i]) {
                case '\\' : // fix.slashes
                    $i++;
                    $return .= isset($format[$i]) ? $format[$i] : '';
                    break;
//                case 'D' :
//                    $return .= $param_D[date('N', $timestamp)];
//                    break;
//                case 'l' :
//                    $return .= $param_l[date('N', $timestamp)];
//                    break;
//                case 'F' :
//                    $return .= $param_F[date('n', $timestamp)];
//                    break;
                case 'M' :
                    $return .= $param_M[date('n', $timestamp)];
                    break;
                default :
                    $return .= date_i18n($format[$i], $timestamp);
                    break;
            }
        }
        return $return;
    }

    public static function apl_date($format, $timestamp = null) {
        if (Apollo_App::hasWPLM()) {
            return self::apl_date_spanish($format, $timestamp);
        }
        return date_i18n($format, $timestamp);
    }

    public static function wplmRenderCSSTextTrans () {
        // WPLM Text translation
        global $apollo_theme_wplm;

        if ($apollo_theme_wplm && Apollo_App::hasWPLM()) {
            global $sitepress;
            $curtLang = $sitepress->get_current_language();
            $languages = $sitepress ? $sitepress->get_active_languages() : false;
            echo '<style>';
            if ($languages) {
                foreach($languages as $lang) {
                    if ( isset($lang['code']) && $lang['code'] == $curtLang) continue;
                    echo 'wplm_'.$lang['code'] . '{display: none;}';
                }
            }
            echo '</style>';
        }
    }

    public static function hasWPLM() {
        return defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != 'ICL_LANGUAGE_CODE';
    }

    public static function checkExistedTermLangCol(){
        global $wpdb;
        $sql = "
            SELECT *
            FROM information_schema.COLUMNS
            WHERE
                TABLE_SCHEMA = '".$wpdb->dbname."'
            AND TABLE_NAME = '".$wpdb->{Apollo_Tables::_POST_TERM}."'
            AND COLUMN_NAME = '".Apollo_DB_Schema::_TERM_LANGUAGE."'
        ";
        $result = $wpdb->get_results($sql);
        return !empty($result);
    }

    public static function updatePostTermLang($post_id = ''){
        try{
            if(!empty($post_id)){
                global $wpdb;
                $currentLang = self::getLanguageCodeByPostID($post_id);
                $wpdb->update( $wpdb->{Apollo_Tables::_POST_TERM}, array(
                    Apollo_DB_Schema::_TERM_LANGUAGE => $currentLang,
                ), array(
                    'post_id'      => $post_id
                ));
            }
        }catch(Exception $ex){

        }
    }

    public static function getLanguageCodeByPostID($post_id) {
        try{
            if(!self::hasWPLM()){
                return NULL;
            }
            global $wpdb;
            $query = $wpdb->prepare('SELECT language_code FROM ' . $wpdb->prefix . 'icl_translations WHERE element_id="%d"', $post_id);
            $query_exec = $wpdb->get_row($query);
            return $query_exec ? $query_exec->language_code : NULL;
        }catch (Exception $ex){}
        return NULL;
    }


        /**
     * Get hidden event categories
     * @access public
     * @return array list id
     *
     */
    public static function getHiddenEventCats() {
        $aplQuery = new Apl_Query( Apollo_Tables::_APOLLO_TERM_META );
        $data = $aplQuery->get_where('meta_key="_apl_event_tax_hide" AND meta_value=1');
        if ( ! $data ) return array();
        $result = array();
        foreach( $data as $item ) {
            $result[] = $item->apollo_term_id;
        }
        return $result;
    }

    /**
     * @return string
     * @throws Exception
     */
    public static function getUploadBaseInfo($key = null ) {

        if(empty(self::$uploadBaseDir))
        {
            self::$uploadBaseDir = wp_upload_dir();
            if(empty(self::$uploadBaseDir))
                throw new Exception(__("Uploads folder have not created! Please create!", 'apollo'));
        }

        return is_null($key) ? self::$uploadBaseDir : self::$uploadBaseDir[$key];
    }

    public static function getCurrentBLog($blog_id)
    {
        if(!isset(self::$currentBlog[$blog_id])) {
            self::$currentBlog[$blog_id] = get_blog_details($blog_id);
        }

        return self::$currentBlog[$blog_id];
    }

    /**
     * Get the blog user table name
     *
     * @return string
     */
    public static function getBlogUserTable()
    {
        global $wpdb;

        return $wpdb->base_prefix . Apollo_Tables::_APL_BLOGUSER;
    }

    public static function setBlogDetail($blog_id, $details)
    {
        self::$currentBlog[$blog_id] = $details;
    }

    public static function apollo_get_current_domain() {

        if ( is_multisite() ) {
            $site = self::getCurrentBLog(get_current_blog_id());

            // If site has been deleted
            if($site)
                return $site->domain;
            return '';
        }

        return str_replace( array( 'http://', 'https://' ), array( '', '' ),  get_home_url() );
    }

    /**
     * Format a price with Currency Locale settings
     * @param  string $value
     * @return string
     */
    public static function apollo_format_localized_price( $value ) {
        return str_replace( '.', get_option( 'apollo_price_decimal_sep' ), strval( $value ) );
    }

    /**
     * Format a decimal with PHP Locale settings
     * @param  string $value
     * @return string
     */
    public static function apollo_format_localized_decimal( $value ) {
        $locale = localeconv();
        return str_replace( '.', $locale['decimal_point'], strval( $value ) );
    }

    /**
     * is_ajax - Returns true when the page is loaded via ajax.
     *
     * @access public
     * @return bool
     */
    public static function is_ajax() {
        return defined( 'DOING_AJAX' );
    }

    public static function isQuickEdit() {
        return isset($_POST['action']) && $_POST['action'] == 'inline-save' && self::is_ajax();
    }

    /**
     * Is theme Option or not
     *
     * @return boolean
    */
    public static function isThemeOption() {
        return (isset($_GET['page']) && ($_GET['page'] == 'options-framework' || $_GET['page'] == 're-active-theme')) || (isset($_POST['option_page']) && $_POST['option_page'] == 'optionsframework');
    }

    /**
     * @expect array
     */
    public static function jsCommunityWithClient() {

        /*@ticket #17020: [CF] 20180803 - [FE Forms] - Add the option to increase the file size upload - Item 1*/
        $max_upload_size = Apollo_App::maxUploadFileSize('b');
        $text_search = isset( $_GET['s'] ) ? Apollo_App::clean_data_request( $_GET['s'] ) : '';
        $jsTrans = array();

        $homeUrl = home_url();
        $ajaxUrl = admin_url('admin-ajax.php');

        $currentLang = '';
        if ( self::hasWPLM() ) {
            $jsTrans = aplJsTranslate();
            global $sitepress;
            $currentLang = 'en';
            if ($sitepress) {
                $currentLang = $sitepress->get_current_language();
            }

            if ( !is_admin() && function_exists('icl_get_home_url')) {
                $homeUrl = icl_get_home_url();
                $ajaxUrl = $homeUrl. '/wp-admin/admin-ajax.php';
            }

        }

        $isLogin = is_user_logged_in();

        $data = array(
            'config' => array(
                'layout' => array(
                    'front_page' => of_get_option( Apollo_DB_Schema::_DEFAULT_HOME_LAYOUT, 'right_sidebar_one_column' )
                ),
                'social_links' => array(
                    'fb' => of_get_option( Apollo_DB_Schema::_FACEBOOK ),
                    'lk' => of_get_option( Apollo_DB_Schema::_LINKEDIN ),
                    'tu' => of_get_option( Apollo_DB_Schema::_TWITTER ),
                    'yt' => of_get_option( Apollo_DB_Schema::_YOUTUBE ),
                    'vm' => of_get_option( Apollo_DB_Schema::_VIMEO ),
                    'insta' =>  of_get_option( Apollo_DB_Schema::_INSTAGRAM )
                ),
                'enable_cache' => intval(Apollo_App::get_network_local_cache(get_current_blog_id())) == 1 ? 1 : 0
            ),

            'is_singular[event]' => is_singular(Apollo_DB_Schema::_EVENT_PT),
            'is_singular[artist]' => is_singular(Apollo_DB_Schema::_ARTIST_PT),
            'is_singular[educator]' => is_singular(Apollo_DB_Schema::_EDUCATOR_PT),
            'is_singular[organization]' => is_singular(Apollo_DB_Schema::_ORGANIZATION_PT),
            'is_singular[post]' => is_singular('post'),
            'is_singular[venue]' => is_singular(Apollo_DB_Schema::_VENUE_PT),
            'is_singular[classified]' => is_singular(Apollo_DB_Schema::_CLASSIFIED),
            'is_singular[public_art]' => is_singular(Apollo_DB_Schema::_PUBLIC_ART_PT),
            'is_singular[business]' => is_singular(Apollo_DB_Schema::_BUSINESS_PT),
            'is_listing[post_type]' => self::isBlogPage() || is_category() || is_archive(),
            'is_singular[news]' => is_singular(Apollo_DB_Schema::_NEWS_PT),
            'is_news_listing' => get_query_var('_apollo_news_search') == 'true',
            'is_page' => is_page(),
            'is_home_page' => is_home(),
            'is_front_page' => is_front_page(),
            'site_url'  => $homeUrl,
            'ajax_url' => $ajaxUrl,
            'assert_url' => APOLLO_FRONTEND_ASSETS_URI,
            'flash_swf_url'       => includes_url( 'js/plupload/plupload.flash.swf' ),
            'silverlight_xap_url' => includes_url( 'js/plupload/plupload.silverlight.xap' ),
            'google_calendar_client_id' => '162304385314-q57btpggaj4a71mr7tbgh4u64abjmk0l.apps.googleusercontent.com',
            'google_calendar_scopes' => "https://www.googleapis.com/auth/calendar",
            'google_calendar_api_key' => "AIzaSyBDZ_oir98f1qYgdRnzJkg3SlftSWhyYoY",
            'upload_image' => array(
                'filters' => array(
                    'max_file_size'   => $max_upload_size . 'b',
                    'max_upload_gal_img' => of_get_option(Apollo_DB_Schema::_MAX_UPLOAD_GALLERY_IMG),
                    'max_upload_pdf' => of_get_option(Apollo_DB_Schema::_MAX_UPLOAD_GALLERY_IMG)
                )
            ),

            // Task login
            'urls' => array(
                'login_url' => !$isLogin ? get_permalink(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_LOGIN_PAGE)) : '',
                'register_url' => !$isLogin ? get_permalink(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_REGISTER_PAGE)) : '',
                'forgot_password_url' => !$isLogin ? get_permalink(self::getForgetPasswordIdPage()) : '',
                'my_account_url' => $isLogin ? self::getMyAccountLink() : '',
                'logout_url' => $isLogin ? get_permalink(self::getLogoutIdPage()) : '',
            ),
            'is_login' => $isLogin,
            'is_register_page' => !$isLogin ? self::isRegisterPage() : '',
            'is_login_page' => !$isLogin ? self::isLoginPage() : '',
            'is_registration_succeed'   => !$isLogin ? self::isRegistrationSucceedPage() : '',
            'is_submit_evaluation_success'  => self::isSubmitEvaluationSuccess(),
            'is_forget_password_page' => !$isLogin ? self::isForgetPasswordPage() : '',
            'is_global_page' => self::isGlobalPage(),
            'is_my_account_page' => $isLogin ? self::isMyAccountPage() : '',
            'resource' => array(
                'loading' => array(
                    'message' => '<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>',
                    'image_style' => self::getLoadingStyle(),
                )

            ),

            /* User dashboard */
            'is_dashboard' => self::isDashboardPage(),
            'dashboard_menu_active_info' => self::getCurrentIdMenuDashboardActive(),
            'text_search' => $text_search,
            'is_print_event_page' => get_query_var( '_apollo_print_event_page' ),

            'google' => array(
                'apikey' => array(
                    'browser' => of_get_option(Apollo_DB_Schema::_GOOGLE_API_KEY_BROWSER, ''),
                )
            ),
            'is_search_program' => get_query_var( '_apollo_program_search' ) == 'true',
            'need_captcha' => self::needCaptcha(),

            //map setting
            'google_map_settings' =>    Apollo_Map_Calc::getMapOption(),

            'terrData' => of_get_option(Apollo_DB_Schema::_TERR_DATA),

            'currentLang'   => $currentLang == 'en' ? '' : $currentLang,
            'jsTrans'       => $jsTrans,
            'hasTrans'      => self::hasWPLM(),
            'is_disable_solr' => Apollo_Solr_Search::isDisable(),
            'event_maximum_year' => Apollo_DB_Schema::_APL_EVENT_MAXIMUM_YEAR,
            /**
             * Author : ThienLD
             * [START] Description: Apollo - Child Theme globalized variables
            **/
            'current_activated_theme' => function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '',
            'apl_sonoma_dir_theme_name' => APL_SM_DIR_THEME_NAME,
            /**
             * Author : ThienLD
             * [END] Description: Apollo - Child Theme globalized variables
             **/

            /** @Ticket #12941 */
            'venue_access_mode' => of_get_option(Apollo_DB_Schema::_ENABLE_VENUE_ACCESSIBILITY_MODE, 1)
        );

        // Get event data for import
        if (get_query_var('_apollo_import_event_page') == 1 ) {
            $priCats = get_terms( 'event-type', array( 'parent' => false, 'hide_empty' => false ) );
            $subsCat = array();
            $priCatsArr = array();

            if ( $priCats ) {
                foreach ( $priCats as $cat ) {

                    $ttObj = new Apollo_Theme_Tool($cat->term_id);
                    if (!$ttObj->isThemeTool()) {
                        $cat->name = html_entity_decode($cat->name);
                        $priCatsArr[] = $cat;
                    }

                    $_subsCat = get_term_children($cat->term_id, 'event-type');
                    if ($_subsCat) {
                        $_sub = array();
                        foreach ($_subsCat as $scId ) {

                            $ttcObj = new Apollo_Theme_Tool($scId);

                            if ($ttcObj->isThemeTool()) continue;

                            $term = get_term($scId, 'event-type');
                            $term->name = '-- '. html_entity_decode($term->name);
                            $_sub[] = $term;
                        }

                        if (!empty($_sub)) {
                            $subsCat[] = $cat;
                            $subsCat = array_merge($subsCat, $_sub);
                        }
                    }
                }

            }

            $data['event_import'] = array(
                'pri_cats'  => $priCatsArr,
                'sub_cats'  => $subsCat,
            );
        }

        return $data;
    }

    public static function admin_js_communicate_client() {

        // Errors for Event
        $is_event_display_management = isset( $_REQUEST['dm'] ) && $_REQUEST['dm'] ? 1 : 0;
        global $taxnow;

        $themeTools = array();
        $themeToolCatSlugs = array();
        global $typenow;
        $currentPage =  isset($_GET['page']) ? $_GET['page'] : '';
        if($typenow == Apollo_DB_Schema::_EVENT_PT && $currentPage != 'event_themes') {
            $themeToolClass = new Apollo_Theme_Tool();
            $themeTools = $themeToolClass->getAllThemeTools(true);
            $themeToolCatSlugs = $themeToolClass->getCatSlugs();
        }

        $state_page_root = is_network_admin() ? '/wp-admin/network/settings.php?page=apl-manage-states-cities#/states'
            : '/wp-admin/options-general.php?page=apl-manage-states-cities#/states';

        return array(
            'event_display_management_url'  => admin_url(). 'edit.php?post_type='. Apollo_DB_Schema::_EVENT_PT .'&dm=true',
            'event_display_management_text' => __( 'Display Management', 'apollo' ),

            'advanced_search_text' => __( 'Advanced Search', 'apollo' ),
            'advanced_search_url'  => admin_url(). 'edit.php?post_type='. Apollo_DB_Schema::_EVENT_PT .'&advanced=true',
            'is_advanced_search' => isset( $_REQUEST['advanced'] ) && $_REQUEST['advanced'] ? 1 : 0,

            'is_event_display_management_page' => $is_event_display_management,
            'event_number_error'            =>  __( 'Please enter number', 'apollo' ),
            'event_url_error'               =>  __( 'This should be a URL format', 'apollo' ),
            'youtube_url_error'             =>  __( 'This should be a youtube URL format', 'apollo' ),
            'event_email_error'             =>  __( 'This should be an email format', 'apollo' ),
            'calendar_url'                  => APOLLO_ADMIN_IMAGES_URL. '/date-button.gif',
            'ajax_url'                      => admin_url('admin-ajax.php'),
            'typenow'                       => get_post_type(),
            'taxnow'                        => $taxnow,
            'admin_url'                     => admin_url(),
            'theme_tools'                   => $themeTools,
            'theme_tools_cat_slug'          => $themeToolCatSlugs,
            'state_root_page'               => $state_page_root,

            'classified_search_url'  => admin_url(). 'edit.php?post_type='. Apollo_DB_Schema::_CLASSIFIED .'&advanced=true',
            'program_advanced_search_url'  => admin_url(). 'edit.php?post_type='. Apollo_DB_Schema::_PROGRAM_PT .'&advanced=true',
            'event_maximum_year' => Apollo_DB_Schema::_APL_EVENT_MAXIMUM_YEAR,
            'isEnableBusinessSpot' => of_get_option(APL_Business_Module_Theme_Option::_BUSINESS_ENABLE_SPOT, 0),
            /** @Ticket #12941 */
            'venue_access_mode' => of_get_option(Apollo_DB_Schema::_ENABLE_VENUE_ACCESSIBILITY_MODE, 1)
        );
    }

    public static function isRegisterPage()
    {
        return is_page(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_REGISTER_PAGE));
    }

    public static function isBlogPage()
    {
        return is_page(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_BLOG));
    }

    public static function isLoginPage()
    {
        return is_page(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_LOGIN_PAGE));
    }

    public static function isRegistrationSucceedPage() {
        return is_page(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_DONE_REGISTER));
    }

    public static function isSubmitEvaluationSuccess() {
        return is_page(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_SUBMIT_EVALUATION_DONE));
    }

    public static function isSubmitGrantEducationSuccess() {
        return is_page(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_SUBMIT_GRANT_EDUCATION_DONE));
    }

    public static function isForgetPasswordPage()
    {
        return is_page(self::getForgetPasswordIdPage());
    }

    public static function isSubmitFormPage() {

        $evalID = Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_TEACHER_EVALUATION_FORM);
        if ($evalID && is_page($evalID)) {
            return true;
        }

        $grantID = Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_GRANT_EDUCATION_FORM);
        if ($grantID && is_page($grantID)) {
            return true;
        }

        return false;
    }

    public static function isGlobalPage() {
        return self::isDashboardPage() || self::isLoginPage() || self::isRegisterPage()
        || self::isForgetPasswordPage() || self::isMyAccountPage() || self::isRegistrationSucceedPage()
        || self::isSubmitEvaluationSuccess() || self::isSubmitFormPage() || self::isSubmitGrantEducationSuccess();
    }

    public static function needCaptcha() {

        return (self::isSubmitFormPage() || is_single()) && ! is_user_logged_in();
    }

    public static function isMyAccountPage()
    {
        return is_page(self::getWelcomeIdPage());
    }

    public static function isMaintenanceMode($_blog_id = '', $return_content = false) {
        global $blog_id;

        if (!$_blog_id) {
            $_blog_id = $blog_id;
        }

        if ( !function_exists( 'get_blog_option' ) ) return false;

        $mm_content = get_blog_option( $_blog_id,  '_apollo_mm_content');
        $mm_active =  get_blog_option( $_blog_id,  '_apollo_mm_active');

        if (!empty($mm_active) || $return_content) {
            return [
                'content' => $mm_content,
                'active' => $mm_active,
            ];
        }

        return false;
    }

    /**
     * @param string $unit
     * @param bool $forAdmin
     * @param string $_blog_id
     * @return float|int|mixed
     */
    public static function maxUploadFileSize($unit = "mb", $forAdmin = false, $_blog_id = '') {
        global $blog_id;

        if (!$_blog_id) {
            $_blog_id = $blog_id;
        }

        $maxUploadFileSize = $forAdmin ? get_blog_option( $_blog_id,  '_apollo_admin_max_upload_file_size')
                                        :get_blog_option( $_blog_id,  '_apollo_frontend_max_upload_file_size');

        if(!is_numeric($maxUploadFileSize)) {
            $configUploadFileSize = intval(get_site_option('fileupload_maxk',2048));
            $maxUploadFileSize = $configUploadFileSize/1024;
        }

        $default = (int)(ini_get('upload_max_filesize'));

        $maxUploadFileSize = min($maxUploadFileSize, $default);

        if($unit == 'b'){
            return $maxUploadFileSize * 1024 * 1024;
        }
        else if ($unit == 'kb'){
            return $maxUploadFileSize * 1024;
        }

        return $maxUploadFileSize;
    }

    public static function forcePageMaintenanceMode($_blog_id = '') {
        $isMM = self::isMaintenanceMode($_blog_id);

        if ($isMM && (self::isLoginPage() || self::isRegisterPage() || self::isDashboardPage() )) {
            return $isMM;
        }

        return false;
    }

    /**
     * Breadcrumb Renders
     *
     * @access public
     * @return void
     */
    public static function the_breadcrumb() {
        global $post;
        $postType = $post && $post->post_type  ? $post->post_type : '';
        if ( !is_home() ) {

            echo '<div class="breadcrumbs">
              <ul class="nav">
                <li><a href="'.  home_url().'">'. __( 'Home', 'apollo' ) .'</a></li>';

            if ( is_category() ) {

                the_category('title_li=');
                echo '<span>';
                the_title();
                echo '</span>';

            } else if( is_single() ) {

                $url = home_url(). '/'. $postType;

                if ( $name = self::get_post_type_name() ) {
                    echo "<li><span><a href='{$url}'>{$name}</a></span></li>";
                }
                echo '<li><span>';
                the_title();
                echo '</span</li>';

            } elseif ( is_page() ) {

                echo '<li><span>';
                the_title();
                echo '</span></li>';

            } elseif ( is_tax() ) {

                $the_tax = get_taxonomy( get_query_var( 'taxonomy' ) );
                //$slug = isset( $the_tax->rewrite['slug'] ) ? $the_tax->rewrite['slug'] : get_query_var( 'taxonomy' );
                //echo "<li><span><a href='". home_url() ."/" .$slug. "'>{$the_tax->labels->name}</a></span></li>";
//                echo "<li><span><a href='". home_url() ."/" .get_query_var( 'taxonomy' ). "'>{$the_tax->labels->name}</a></span></li>";

                /*@ticket #18344: 0002504: Arts Education Customizations - Add category type to breadcrumb (program)*/
                $url = home_url($postType);
                if ( $name = self::get_post_type_name($postType) ) {
                    echo "<li><a href='{$url}'><span>{$name}</span></a></li>";
                }
                echo "<li><span>". get_queried_object()->name ."</span></li>";

            } else if ( is_post_type_archive() ) {

                global $apollo_modules;
                if ( $name = self::get_post_type_name() ) {
                    echo "<li><span>{$name}</span></li>";
                }
            }

            echo ' </ul></div>';
        }
    }

    public static function getIncodeOptionName(){
        // Create option meta key saved all information page we have created
        return Apollo_DB_Schema::_APOLLO_INCODE;//_apollo_incode
    }

    public static function getRegisterIdPage(){
        return Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_REGISTER_PAGE);
    }

    public static function getMyAccountLink(){
        $home_url = get_home_url();
        return parse_url($home_url, PHP_URL_SCHEME).'://'.parse_url($home_url, PHP_URL_HOST).'/user/dashboard';
    }

    public static function createPermalink($request){
        $home_url = get_home_url();
        return parse_url($home_url, PHP_URL_SCHEME).'://'.parse_url($home_url, PHP_URL_HOST).'/'.$request;
    }

    public static function getWelcomeIdPage(){
        return Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_WELCOME_DASHBOARD_PAGE);
    }
    public static function getChangeMyPassIdPage(){
        return Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_CHANGE_MY_PASS);
    }

    public static function getLoginIdPage(){
        return Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_LOGIN_PAGE);
    }

    public static function getLogoutIdPage(){
        return Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_LOGOUT_PAGE);
    }

    public static function getSuccessfulRegistration(){
        return Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_DONE_REGISTER);
    }
    public static function getSuccessPage(){
        return Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_SUBMIT_EVENT_SUCCESS);
    }
    public static function getClassifiedSuccessPage(){
        return Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_SUBMIT_CLASSIFIED_SUCCESS);
    }
    public static function getOrgHeaderPage(){
        return Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ORGANIZATION_HEADER);
    }
    public static function getVenueHeaderPage(){
        return Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_VENUE_HEADER);
    }
    public static function getArtistHeaderPage(){
        return Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ARTIST_HEADER);
    }
    public static function getPublicArtHeaderPage(){
        return Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_PUBLIC_ART_HEADER);
    }
    public static function getClassifiedHeaderPage(){
        return Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_CLASSIFIED_HEADER);
    }
    public static function getEducatorHeaderPage(){
        return Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_EDUCATOR_HEADER);
    }
    public static function getProgramHeaderPage(){
        return Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_PROGRAM_HEADER);
    }
    public static function getAuthorsHeaderPage(){
        return Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_AUTHORS);
    }

    public static function getForgetPasswordIdPage(){
        return Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_FORGOT_PASS_PAGE);
    }

    public static function getFELostPasswordUrl() {
        $login_page_id = Apollo_App::getForgetPasswordIdPage();
        return get_permalink($login_page_id);
    }

    /**
     * Remove html tags, correct the string
     * @param $text
     * @return mixed|string
     */
    public static function correctExecString($text) {
        $strExact = strip_tags($text); /** Remove html tags */
        /** Remove special html characters */
        $strExact = preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', ' ', $strExact);
        return trim($strExact);
    }

    public static function truncate($text, $length = 100, $ending = '', $exact = false, $considerHtml = true) {
        if ($considerHtml) {
            // if the plain text is shorter than the maximum length, return the whole text
            if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
                return $text;
            }
            // splits all html-tags to scanable lines
            preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);
            $total_length = strlen($ending);
            $open_tags = array();
            $truncate = '';
            foreach ($lines as $line_matchings) {
                // if there is any html-tag in this line, handle it and add it (uncounted) to the output
                if (!empty($line_matchings[1])) {
                    // if it's an "empty element" with or without xhtml-conform closing slash
                    if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
                        // do nothing
                        // if tag is a closing tag
                    } else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
                        // delete tag from $open_tags list
                        $pos = array_search($tag_matchings[1], $open_tags);
                        if ($pos !== false) {
                            unset($open_tags[$pos]);
                        }
                        // if tag is an opening tag
                    } else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
                        // add tag to the beginning of $open_tags list
                        array_unshift($open_tags, strtolower($tag_matchings[1]));
                    }
                    // add html-tag to $truncate'd text
                    $truncate .= $line_matchings[1];
                }
                // calculate the length of the plain text part of the line; handle entities as one character
                $content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
                if ($total_length+$content_length> $length) {
                    // the number of characters which are left
                    $left = $length - $total_length;
                    $entities_length = 0;
                    // search for html entities
                    if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
                        // calculate the real length of all entities in the legal range
                        foreach ($entities[0] as $entity) {
                            if ($entity[1]+1-$entities_length <= $left) {
                                $left--;
                                $entities_length += strlen($entity[0]);
                            } else {
                                // no more characters left
                                break;
                            }
                        }
                    }
                    $truncate .= substr($line_matchings[2], 0, $left+$entities_length);
                    // maximum lenght is reached, so get off the loop
                    break;
                } else {
                    $truncate .= $line_matchings[2];
                    $total_length += $content_length;
                }
                // if the maximum length is reached, get off the loop
                if($total_length>= $length) {
                    break;
                }
            }
        } else {
            if (strlen($text) <= $length) {
                return $text;
            } else {
                $truncate = substr($text, 0, $length - strlen($ending));
            }
        }
        // if the words shouldn't be cut in the middle...
        if (!$exact) {
            // ...search the last occurance of a space...
            $spacepos = strrpos($truncate, ' ');
            if (isset($spacepos)) {
                // ...and cut the text in this position
                $truncate = substr($truncate, 0, $spacepos);
            }
        }
        // add the defined ending to the text
        $truncate .= $ending;
        if($considerHtml) {
            // close all unclosed html-tags
            foreach ($open_tags as $tag) {
                $truncate .= '</' . $tag . '>';
            }
        }
        return $truncate;
    }


    /**
     * @param $str
     * @param $max_length
     * @param bool $is_filter
     * @param bool $exact - if the words shouldn't be cut in the middle...
     * @return array
     */
    public static function getStringByLength($str, $max_length, $is_filter = false, $exact = false) {

        $str = self::convertContentEditorToHtml($str);

        if($max_length === null || $max_length === 0) { // get all
            if($is_filter === true) {
                $str = apply_filters( 'the_content', $str );
                $str =  str_replace( ']]>', ']]&gt;', $str );
            }

            return array(
                'text' => self::fixUnclosedTags($str),
                'have_more' => false,
            );
        }

        // Remove html tags, correct the string to count the exactly length
        $strExact= self::correctExecString($str);
        if(strlen($strExact) <= $max_length) {
            if($is_filter === true) {
                $str = apply_filters( 'the_content', $str );
                $str =  str_replace( ']]>', ']]&gt;', $str );
            }

            return array(
                'have_more' => false,
                'text' => self::fixUnclosedTags($str),
            );
        }

        $str = self::truncate($str, $max_length, '', $exact);

        if($is_filter === true) {
            $str = apply_filters( 'the_content', $str . '...' );
            $str =  str_replace( ']]>', ']]&gt;', $str );
        }

        return array(
            'have_more' => true,
            'text' => self::fixUnclosedTags($str),

        );
    }

    /**
     * Fix missing encloded tag
     * @param $html
     * @return string
     */
    public static function fixUnclosedTags ($html) {
        #put all opened tags into an array
        preg_match_all ( "#<([a-z]+)( .*)?(?!/)>#iU", $html, $result );
        $openedtags = $result[1];
        #put all closed tags into an array
        preg_match_all ( "#</([a-z]+)>#iU", $html, $result );
        $closedtags = $result[1];
        $len_opened = count ( $openedtags );

        # all tags are closed
        if( count ( $closedtags ) == $len_opened ) {
            return $html;
        }
        $openedtags = array_reverse ( $openedtags );
        # close tags
        for( $i = 0; $i < $len_opened; $i++ ) {

            if ($openedtags[$i] == 'br') continue;

            if ( !in_array ( $openedtags[$i], $closedtags ) ) {
                $html .= "</" . $openedtags[$i] . ">";
            }
            else {
                unset ( $closedtags[array_search ( $openedtags[$i], $closedtags)] );
            }
        }

        return $html;
    }

    /**
     * Display list links of taxonomy
     * @param $post_type_item
     * @return String Return html
     * @internal param object $terms Term data
     */
    public static function the_post_type_link( $post_type_item ) {

        $return = '';
        if ( $post_type_item ) {
            $return =  '<a href="'. home_url() . '/' . $post_type_item->post_type. '/' . $post_type_item->post_name . '">' . $post_type_item->post_title . '</a>' ;
        }

        return $return ? $return : __( 'Unknown', 'apollo' );

    }

    public static function get_post_type_item( $post_id ) {

        if ( ! intval( $post_id ) ) {
            return '';
        }

        $post = get_post( $post_id );
        return $post && self::is_avaiable_module( $post->post_type ) ? $post : false;
    }

    public static function apollo_get_meta_data( $thepostid, $key, $parent_key = '' ) {

        if ( self::apollo_is_complex_key( $key ) ) {

            $key_data   = explode( '[+]' , str_replace( '[', '[+]', str_replace( ']', '', $key ) ) );
            $key        = isset( $key_data[1] ) ? $key_data[1] : '';
            $parent_key = isset( $key_data[0] ) ? $key_data[0] : '';
        }

        if ( $parent_key ) {

            $value = get_apollo_meta( $thepostid, $parent_key, true );
            $meta_data = Apollo_App::unserialize($value);

            if (!is_array($meta_data)) {
                $meta_data = maybe_unserialize($value);
            }

            return isset( $meta_data[ $key ] ) ? $meta_data[ $key ] : '';
        } else {
            return get_apollo_meta( $thepostid, $key, true );
        }

    }

    public static function apollo_is_complex_key( $key ) {
        return strpos( $key, '[' ) && strpos( $key, ']' );
    }

    public static function get_static_html_content( $filename, $check_exist = TRUE, $ext = 'html' ) {
        $file =  self::getUploadBaseInfo( 'html_dir' ). '/'. $filename. '.'. $ext;
        $content = @file_get_contents( $file );

        if ( $content === false && $check_exist ) {
            if (WP_DEBUG) {
                echo sprintf( __( '%s not exists. '
                    . 'Please check the permission of parent directory. Do not forget restore theme option or active your theme again', 'apollo' ), $file  );
                exit;
            }

            return "";
        }
        return do_shortcode(self::replaceTextHeader($content));
    }

    public static function getImageLoading() {
        return '<img src="'.APOLLO_FRONTEND_ASSETS_URI.'/images/loading-quote.gif'.'"/>';
    }

    public static function getLoadingStyle() {
        return '{ "border": "none", "background": "none" }';
    }

    public static function getActiveSiteUrl($user_id, $path = '', $scheme = null )
    {
        if (!is_multisite())
            return site_url($path, $scheme);

        $active_site = get_active_blog_for_user($user_id);

        if ('relative' == $scheme)
            $url = $active_site->path;
        else
            $url = set_url_scheme('http://' . $active_site->domain . $active_site->path, $scheme);

        if ($path && is_string($path))
            $url .= ltrim($path, '/');

        /**
         * Filter the network site URL.
         *
         * @since 3.0.0
         *
         * @param string $url The complete network site URL including scheme and path.
         * @param string $path Path relative to the network site URL. Blank string if
         *                            no path is specified.
         * @param string|null $scheme Scheme to give the URL context. Accepts 'http', 'https',
         *                            'relative' or null.
         */
        return apply_filters('network_site_url', $url, $path, $scheme);
    }

    /**
     * Check module is avaiable
     * @param string $module_name
     * @return boolean
     */
    public static function check_avaiable_module( $module_name ) {

        global $blog_id, $apollo_modules;
        $avaiable_module = Apollo_App::get_avaiable_modules( $blog_id );
        return $avaiable_module && in_array( $module_name , $avaiable_module ) && in_array( $module_name , array_keys( $apollo_modules ) );
    }

    /**
     * Get avaiable modules
     * @param int $blog_id
     */
    public static function get_avaiable_modules( $_blog_id = '' ) {
        global $blog_id;

        if ( ! $_blog_id ) {
            $_blog_id = $blog_id;
        }

        if ( !function_exists( 'get_blog_option' ) ) return false;

        return unserialize( get_blog_option( $_blog_id,  '_apollo_module' ) );
    }

    /**
     * Get avaiable modules
     * @param int $blog_id
     */
    public static function get_disabled_plugins( $_blog_id = '' ) {
        global $blog_id;

        if ( ! $_blog_id ) {
            $_blog_id = $blog_id;
        }

        if ( !function_exists( 'get_blog_option' ) ) return false;

        return unserialize( get_blog_option( $_blog_id,  '_apollo_plugins' ) );
    }

    /**
     * Get manage states & cities right
     * @param int $blog_id
     */
    public static function get_network_manage_states_cities( $_blog_id = '' ) {
        global $blog_id;

        if ( ! $_blog_id ) {
            $_blog_id = $blog_id;
        }

        if ( !function_exists( 'get_blog_option' ) ) return false;

        return get_blog_option( $_blog_id,  '_apollo_manage_states_cities' );
    }

    /**
     * Get Enable/disable event import tool
     * @param int $blog_id
     */
    public static function get_network_event_import_tool( $_blog_id = '' ) {
        global $blog_id;

        if ( ! $_blog_id ) {
            $_blog_id = $blog_id;
        }

        if ( !function_exists( 'get_blog_option' ) ) return false;

        return get_blog_option( $_blog_id,  '_apollo_enable_event_import_tool' );
    }

    /**
     * Get Enable/disable local cache
     * @param string $_blog_id
     * @return bool|mixed
     */
    public static function get_network_local_cache( $_blog_id = '') {
        global $blog_id;

        if ( ! $_blog_id ) {
            $_blog_id = $blog_id;
        }

        if ( !function_exists( 'get_blog_option' ) ) return false;

        return get_blog_option( $_blog_id,  '_apollo_enable_local_cache' );
    }

    /**
     * Get enable/disable local reports
     *
     * @param string $_blog_id
     *
     * @ticket #11307
     * @return bool|mixed
     */
    public static function get_network_enable_reports($_blog_id = '') {
        if ( ! $_blog_id ) {
            $_blog_id = get_current_blog_id();
        }

        if ( !function_exists( 'get_blog_option' ) ) return false;

        return get_blog_option( $_blog_id,  '_apollo_enable_reports' );
    }

    /**
     * @author TriLM
     * @return bool
     * check  solr_search enable/disable
     */
    public static function is_enable_solr_search( $_blog_id = '') {
        global $blog_id;

        if ( ! $_blog_id ) {
            $_blog_id = $blog_id;
        }

        if ( !function_exists( 'get_blog_option' ) ) return false;

        return get_blog_option( $_blog_id,  '_apollo_enable_solr_search' );
    }


    public static function formatSolrCoreName($corename) {
        return str_replace('-', '', $corename);
    }

    public static function user_can_import_events() {
        $listOfUserCan = get_site_option(Apollo_Tables::_APL_TOGGLE_EVENT_IMPORT_TOOL, array());
        return in_array(get_current_user_id(), $listOfUserCan) && Apollo_App::get_network_event_import_tool() == 1;
    }

    /**
     * Get post type name
     * @param int $post_type
     * @return string
     */
    public static function get_post_type_name( $post_type = '' ) {

        if ( ! $post_type ) {
            $post_type = get_query_var( 'post_type' );
        }

        global $apollo_modules;

        return $apollo_modules && isset( $apollo_modules[$post_type] ) && $apollo_modules[$post_type]['sing'] ?
            $apollo_modules[$post_type]['sing'] : '';
    }

    /**
     * Check avaiable module
     * @param string $module
     * @return boolean
     */
    public static function is_avaiable_module( $module, $avaialbe_module = array() ) {
        global $apollo_modules;
        $_apollo_modules = array_keys( $apollo_modules );
        if ( ! $avaialbe_module ) {
            $avaialbe_module = self::get_avaiable_modules();
        }

        if ( $module == 'post' || $module == Apollo_DB_Schema::_AGENCY_PT ) return $avaialbe_module && in_array( $module, $avaialbe_module );

        return  $_apollo_modules
        && in_array( $module, $_apollo_modules)
        && $avaialbe_module
        && in_array( $module, $avaialbe_module );

    }

    public static function get_list_post_type_items( $post_type, $need_check_avaiable = true ,$post_status = array('publish'),$posts_per_page = -1, $paged = 1  ) {

        if ( $need_check_avaiable && ! self::is_avaiable_module( $post_type ) ) {
            return array();
        }

        $args = array(
            'post_type'         => $post_type,
            'orderby'           => 'title',
            'order'             => 'ASC',
            'posts_per_page'    => -1,
            'post_status'        => $post_status,
        );
        $listPost = query_posts( $args );
        wp_reset_query();
        return $listPost;
    }

    /**
     *  The function is output aray of custom post type items.
     * @author TruongHN
     * @param $post_type
     * @param bool $need_check_avaiable
     * @return array
     */
    public static function get_array_post_type_items($post_type, $need_check_avaiable = true ){
        if ( $need_check_avaiable && ! self::is_avaiable_module( $post_type ) ) {
            return array();
        }
        $resultsData = array();
        $posts = self::get_list_post_type_items($post_type);
        if ($posts) {
            foreach ($posts as $v ){
                $resultsData[$v->ID] = $v->post_title;
            }
        }
        if(empty($resultsData))
            return array();
        return $resultsData;
    }


    /**
     ** The function is output list of custom post type items have pagination option
     * @param $args array
     * @return mixed
     * default arguments
     * array(
     *  'post_type' => '',
     *  'need_check_available' => true,
     *  'post_status' => array('publish'),
     *  'posts_per_page' => =1,
     *  'page' => 1
     * )
     */
    public static function getListPostItemWithPagination( $args = array()  ) {

        $post_type = isset($args['post_type']) ? $args['post_type'] : '';
        $need_check_available = isset($args['need_check_available']) ? $args['need_check_available'] : true;
        $post_status = isset($args['post_status']) ? $args['post_status'] : array('publish');
        $posts_per_page = isset($args['posts_per_page']) ? $args['posts_per_page'] : -1;
        $orderBy = isset($args['order_by']) ? $args['order_by'] : 'title';
        $order = isset($args['order']) ? $args['order'] : 'ASC';
        $paged = isset($args['page']) ? $args['page'] : 1;

        if ( $need_check_available && ! self::is_avaiable_module( $post_type ) ) {
            return array();
        }
        $args = array(
            'post_type'         => $post_type,
            'orderby'           => $orderBy,
            'order'             => $order,
            'paged'             => $paged,
            'posts_per_page'    => $posts_per_page,
            'post_status'        => $post_status,
        );
        $listPost = query_posts( $args );
        wp_reset_query();
        return $listPost;
    }

    public static function getArrRewriteDashboard()
    {

        /* Slugs of page which we have track */
        $post = get_post(Apollo_App::getWelcomeIdPage());
        $post_accout_info = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ACCOUNT_INFO));
        $post_accout_activity = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ACCOUNT_ACTIVITY));
        $post_accout_mylist = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ACCOUNT_MY_LIST));

        $account_info_slug =isset($post_accout_info->post_name) ? $post_accout_info->post_name : '';
        $account_activity_slug = isset($post_accout_activity->post_name) ? $post_accout_activity->post_name : '';
        $account_mylist_slug =isset($post_accout_mylist->post_name) ? $post_accout_mylist->post_name : '';

        // Check if user not login
        if ($post && !is_user_logged_in()) {
            $loginPost = get_post(Apollo_App::getLoginIdPage());
            if ($loginPost) {
                $currentUrl = home_url(). $_SERVER['REQUEST_URI'];
                wp_safe_redirect(home_url().'/'. $loginPost->post_name. '?redirect_to='. urlencode($currentUrl));
            }
        }

        $slug_account = $post ? $post->post_name : '';

        // Check has WPML
        if (self::hasWPLM()) {
            $originalID = icl_object_id( $post->ID, 'post', false, ICL_LANGUAGE_CODE );
            $originalPost = get_post($originalID);
            $slug_account = $originalPost ? $originalPost->post_name : '';
        }

        /**
         * ACCOUNT INFO
        BOOKMARKS
        ACTIVITY
        REVIEWS
        DISCOUNTS
         */

        $tree_menu =  array(
            'user/dashboard' => array(
                'is_static' => false,
                'source' => 'index.php?_apollo_dashboard=true&pagename='.$slug_account,
                'source_code' => '',
                'text' => __('DASHBOARD', 'apollo'),
                'attr' => array(
                    'class' => 'fa fa-home fa-2x',
                ),
            ),

            'account' => array(
                'text' => __('ACCOUNT', 'apollo'),
                'id'   => '_apollo_user_dashboard_account_main',
                'attr' => array(
                    'class' => 'fa fa-user fa-2x',
                ),
                'childs' => array(

                    'user/account' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$account_info_slug,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/pages/dashboard/html/account/info.php',
                        'text' => __('ACCOUNT INFO', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_account_main',
                            'id' => '_apollo_user_dashboard_account'
                        )
                    ),

                    'user/my-list' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$account_mylist_slug,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/pages/dashboard/html/account/my-bookmark-list.php',
                        'text' => __('MY LIST ', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_account_main',
                            'id' => '_apollo_user_dashboard_account_bookmark'
                        )
                    ),

                    'user/activity' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$account_activity_slug,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/pages/dashboard/html/account/activity.php',
                        'text' => __('ACTIVITY', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_account_main',
                            'id' => '_apollo_user_dashboard_account_activity'
                        )
                    ),
                ),

            ),
        );

        /**
         * Not display bookmark buttin if it not activate in admin panel
         */
        if ( ! self::isEnableAddItBtn() ) {
            unset($tree_menu['account']['childs']['user/my-list']);
        }

        // Depend on moudle active add more menu here
        $array_map_functions = array(
            'organization' => array(__CLASS__, 'getOrganization'),
            'event' => array(__CLASS__, 'getEvent'),
            'venue' => array(__CLASS__, 'getVenue'),
            'artist' => array(__CLASS__, 'getArtist'),
            'classified' => array(__CLASS__, 'getClassified'),
            'education' => array(__CLASS__, 'getEducation'),
            /* Thienld : remove business menu-item because it was included in org profile */
            // 'business' => array(__CLASS__, 'getBusiness'),
        );


        foreach($array_map_functions as $mn => $func) {
            if(self::is_avaiable_module($mn)) {
                $tree_menu = array_merge($tree_menu, call_user_func($func));
            }
        }
        if(is_array( self::getAgency()))
            $tree_menu['agency'] =  self::getAgency();
        return $tree_menu;

    }

    public static function getOrganizationByAgencyID($agencyID = ''){
        if(empty($agencyID) || intval($agencyID) === 0) return array();
        $result = array();
        $aplQuery = new Apl_Query( Apollo_Tables::_APL_AGENCY_ORG );
        $agencyOrgs = $aplQuery->get_where(Apollo_DB_Schema::_APL_AGENCY_ORG_FIELD_AGENCY_ID . "='".$agencyID."'",Apollo_DB_Schema::_APL_AGENCY_ORG_FIELD_ORG_ID);
        if(!empty($agencyOrgs)){
            foreach($agencyOrgs as $org){
                $org = is_object($org) ? get_object_vars($org) : $org;
                $result[] = $org[Apollo_DB_Schema::_APL_AGENCY_ORG_FIELD_ORG_ID];
            }
        }
        return $result;
    }

    public static function isInputMultiplePostMode(){
        /* ThienLD: check current activated theme to allow mode multiple editing posts in UserDashboard pages */
        $template = function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '';
        return self::is_avaiable_module( Apollo_DB_Schema::_AGENCY_PT ) || $template == APL_SM_DIR_THEME_NAME;
    }

    public static function getOrganization()
    {
        // Thienld: if input multiple mode = true => custom left org navigation on dashboard page.
        if(self::isInputMultiplePostMode()){
            return self::getOrganizationMultiple();
        }
        $locationOrgPhotoForm = of_get_option(Apollo_DB_Schema::_ORGANIZATION_PHOTO_FORM_LOCATION, 1);
        $orgAddNew = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ORG_PROFILE));
        $orgAddNewVideo = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ORG_PROFILE_VIDEOS));
        $orgAddNewAudio = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ORG_PROFILE_AUDIO));
        $orgAddNewPhoto = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ORG_PROFILE_PHOTO));

        $result = array(
            'organization' => array(
                'text' => __('ORGANIZATION/ BUSINESS', 'apollo'),
                'id'   => '_apollo_user_dashboard_organization_main',
                'attr' => array(
                    'class' => 'fa fa-users fa-2x',
                    'text_class'    => 'long-ttl',
                ),
                'childs' => array(
                    'user/org/profile' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$orgAddNew->post_name,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/org/dashboard-form/profile.php',
                        'text' => __('PROFILE', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_organization_main',
                            'id' => '_apollo_user_dashboard_organization_reports'
                        )
                    ),
                    'user/org/photos' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$orgAddNewPhoto->post_name,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/org/dashboard-form/photos.php',
                        'text' => __('PHOTOS', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_organization_main',
                            'id' => '_apollo_user_dashboard_organization_photos'
                        )
                    ),

                    'user/org/video' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$orgAddNewVideo->post_name,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/org/dashboard-form/video.php',
                        'text' => __('VIDEO', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_organization_main',
                            'id' => '_apollo_user_dashboard_organization_video'
                        )
                    ),

                    'user/org/audio' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$orgAddNewAudio->post_name,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/org/dashboard-form/audio.php',
                        'text' => __('AUDIO', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_organization_main',
                            'id' => '_apollo_user_dashboard_organization_audio'
                        )
                    ),
                ),

            ),
        );
        if($locationOrgPhotoForm != 1){
            unset($result['organization']['childs']['user/org/photos']);
        }
        return $result;
    }

    public static function getOrganizationMultiple()
    {
        $post = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ORG_PROFILE));
        $slug_profile = $post ? $post->post_name : '';
        $locationOrgPhotoForm = of_get_option(Apollo_DB_Schema::_ORGANIZATION_PHOTO_FORM_LOCATION, 1);
        $orgAddNew = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ORG_PROFILE));
        $orgAddNewVideo = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ORG_PROFILE_VIDEOS));
        $orgAddNewAudio = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ORG_PROFILE_AUDIO));
        $orgAddNewPhoto = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ORG_PROFILE_PHOTO));

        $result = array(
            'organization' => array(
                'text' => __('ORGANIZATION/ BUSINESS', 'apollo'),
                'id'   => '_apollo_user_dashboard_organization_main',
                'attr' => array(
                    'class' => 'fa fa-users fa-2x',
                    'text_class'    => 'long-ttl',
                ),
                'childs' => array(
                    APL_Dashboard_Hor_Tab_Options::ORG_ADD_NEW_URL => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$orgAddNew->post_name,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/org/dashboard-form/profile.php',
                        'text' => __('ADD NEW', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_organization_main',
                            'id' => '_apollo_user_dashboard_organization_reports'
                        )
                    ),
                    APL_Dashboard_Hor_Tab_Options::ORG_PROFILE_URL . '/(\d+)' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&_edit_post_id=$matches[1]&pagename='.$orgAddNew->post_name,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/org/dashboard-form/profile.php',
                        'text' => __('PROFILE', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_organization_main',
                            'id' => '_apollo_user_dashboard_organization_reports',
                            'class' => ' hidden '
                        )
                    ),
                    APL_Dashboard_Hor_Tab_Options::ORG_MANAGE_LIST_URL => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$slug_profile,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/org/dashboard-form/manage-list-org.php',
                        'text' => __('MANAGE', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_organization_main',
                            'id' => '_apollo_user_dashboard_organization_photos'
                        )
                    ),
                    APL_Dashboard_Hor_Tab_Options::ORG_PHOTOS_URL . '/(\d+)' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&_edit_post_id=$matches[1]&pagename='.$orgAddNewPhoto->post_name,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/org/dashboard-form/photos.php',
                        'text' => __('PHOTOS', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_organization_main',
                            'id' => '_apollo_user_dashboard_organization_photos',
                            'class' => ' hidden '
                        )
                    ),
                    APL_Dashboard_Hor_Tab_Options::ORG_VIDEO_URL . '/(\d+)' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&_edit_post_id=$matches[1]&pagename='.$orgAddNewVideo->post_name,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/org/dashboard-form/video.php',
                        'text' => __('VIDEO', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_organization_main',
                            'id' => '_apollo_user_dashboard_organization_video',
                            'class' => ' hidden '
                        )
                    ),
                    APL_Dashboard_Hor_Tab_Options::ORG_AUDIO_URL . '/(\d+)' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&_edit_post_id=$matches[1]&pagename='.$orgAddNewAudio->post_name,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/org/dashboard-form/audio.php',
                        'text' => __('AUDIO', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_organization_main',
                            'id' => '_apollo_user_dashboard_organization_audio',
                            'class' => ' hidden '
                        )
                    ),
                ),

            ),
        );
        if($locationOrgPhotoForm != 1){
            unset($result['user/org/photos']);
        }
        return $result;
    }

    public static function getVenueMultiple()
    {

        $post = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_VENUE_PROFILE));
        $post_photo = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_VENUE_PROFILE_PHOTO));
        $post_video= get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_VENUE_PROFILE_VIDEO));
        $slug_profile = $post ? $post->post_name : '';
        $slug_profile_photo = $post ? $post_photo->post_name : '';
        $slug_profile_video= $post ? $post_video->post_name : '';

        $result = array(
            'venue' => array(
                'text' => __('VENUE', 'apollo'),
                'id'   => '_apollo_user_dashboard_venue_main',
                'attr' => array(
                    'class' => 'fa fa-university fa-2x',
                ),
                'childs' => array(
                    APL_Dashboard_Hor_Tab_Options::VENUE_ADD_NEW_URL => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$slug_profile,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/venue/dashboard-form/profile.php',
                        'text' => __('ADD NEW', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_venue_main',
                            'id' => '_apollo_user_dashboard_venue_add_new'
                        )
                    ),
                    APL_Dashboard_Hor_Tab_Options::VENUE_MANAGE_LIST_URL => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$slug_profile,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/venue/dashboard-form/manage-list-venue.php',
                        'text' => __('MANAGE VENUE', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_venue_main',
                            'id' => '_apollo_user_dashboard_venue_manage_list'
                        )
                    ),
                    APL_Dashboard_Hor_Tab_Options::VENUE_PROFILE_URL . '/(\d+)' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&_edit_post_id=$matches[1]&pagename='.$slug_profile,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/venue/dashboard-form/profile.php',
                        'text' => __('PROFILE', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_venue_main',
                            'id' => '_apollo_user_dashboard_venue_reports',
                            'class' => ' hidden '
                        )
                    ),
                    APL_Dashboard_Hor_Tab_Options::VENUE_PHOTOS_URL . '/(\d+)' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&_edit_post_id=$matches[1]&pagename='.$slug_profile_photo,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/venue/dashboard-form/photos.php',
                        'text' => __('PHOTOS', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_venue_main',
                            'id' => '_apollo_user_dashboard_venue_photos',
                            'class' => ' hidden '
                        )
                    ),
                    APL_Dashboard_Hor_Tab_Options::VENUE_VIDEO_URL . '/(\d+)' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&_edit_post_id=$matches[1]&pagename='.$slug_profile_video,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/venue/dashboard-form/videos.php',
                        'text' => __('VIDEO', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_venue_main',
                            'id' => '_apollo_user_dashboard_venue_video',
                            'class' => ' hidden '
                        )
                    ),
                ),

            ),
        );
        return $result;
    }

    public static function getVenue()
    {
        // Thienld: if input multiple mode = true => custom left venue navigation on dashboard page.
        if(self::isInputMultiplePostMode()){
            return self::getVenueMultiple();
        }
        $post = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_VENUE_PROFILE));
        $post_photo = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_VENUE_PROFILE_PHOTO));
        $post_video= get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_VENUE_PROFILE_VIDEO));
        $slug_profile = $post ? $post->post_name : '';
        $slug_profile_photo = $post ? $post_photo->post_name : '';
        $slug_profile_video= $post ? $post_video->post_name : '';
        return array(
            'venue' => array(
                'text' => __('VENUE', 'apollo'),
                'id'   => '_apollo_user_dashboard_venue_main',
                'attr' => array(
                    'class' => 'fa fa-university fa-2x',
                ),
                'childs' => array(

                    'user/venue/profile' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$slug_profile,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/venue/dashboard-form/profile.php',
                        'text' => __('PROFILE', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_venue_main',
                            'id' => '_apollo_user_dashboard_venue_reports'
                        )
                    ),

                    'user/venue/photos' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$slug_profile_photo,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/venue/dashboard-form/photos.php',
                        'text' => __('PHOTOS', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_venue_main',
                            'id' => '_apollo_user_dashboard_venue_photos'
                        )
                    ),

                    'user/venue/video' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$slug_profile_video,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/venue/dashboard-form/videos.php',
                        'text' => __('VIDEO', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_venue_main',
                            'id' => '_apollo_user_dashboard_venue_video'
                        )
                    ),
                ),

            ),
        );
    }

    public static function getArtistMultiple()
    {
        $post = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ARTIST_PROFILE));
        $post_photo = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ARTIST_PHOTO));
        $post_video= get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ARTIST_VIDEO));
        $post_audio= get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ARTIST_AUDIO));
        $post_event = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ARTIST_EVENT));
        $slug_profile = $post ? $post->post_name : '';
        $slug_profile_photo = $post ? $post_photo->post_name : '';
        $slug_profile_video= $post ? $post_video->post_name : '';
        $slug_profile_audio= $post ? $post_audio->post_name : '';
        $slug_profile_event = $post_event ? $post_event->post_name : '';

        $result = array(
            'aritst' => array(
                    'text' => of_get_option( Apollo_DB_Schema::_ARTIST_MENU_LABEL,__('ARTIST', 'apollo')),
                'id'   => '_apollo_user_dashboard_artist_main',
                'attr' => array(
                    'class' => 'fa fa-paint-brush fa-2x',
                ),
                'childs' => array(
                    APL_Dashboard_Hor_Tab_Options::ARTIST_ADD_NEW_URL => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$slug_profile,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/artists/dashboard-form/profile.php',
                        'text' => __('ADD NEW', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_artist_main',
                            'id' => '_apollo_user_dashboard_venue_add_new'
                        )
                    ),
                    APL_Dashboard_Hor_Tab_Options::ARTIST_MANAGE_LIST_URL => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$slug_profile,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/artists/dashboard-form/manage-list-artist.php',
                        'text' => __('MANAGE ARTIST', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_artist_main',
                            'id' => '_apollo_user_dashboard_artist_manage_list'
                        )
                    ),
                    APL_Dashboard_Hor_Tab_Options::ARTIST_PROFILE_URL . '/(\d+)' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&_edit_post_id=$matches[1]&pagename='.$slug_profile,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/artists/dashboard-form/profile.php',
                        'text' => __('PROFILE', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_artist_main',
                            'id' => '_apollo_user_dashboard_artist_reports',
                            'class' => ' hidden '
                        )
                    ),
                    APL_Dashboard_Hor_Tab_Options::ARTIST_PHOTOS_URL . '/(\d+)' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&_edit_post_id=$matches[1]&pagename='.$slug_profile_photo,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/artists/dashboard-form/multiple/photos.php',
                        'text' => __('PHOTOS', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_artist_main',
                            'id' => '_apollo_user_dashboard_artist_photos',
                            'class' => ' hidden '
                        )
                    ),
                    APL_Dashboard_Hor_Tab_Options::ARTIST_VIDEO_URL . '/(\d+)' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&_edit_post_id=$matches[1]&pagename='.$slug_profile_video,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/artists/dashboard-form/video.php',
                        'text' => __('VIDEO', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_artist_main',
                            'id' => '_apollo_user_dashboard_artist_video',
                            'class' => ' hidden '
                        )
                    ),
                    APL_Dashboard_Hor_Tab_Options::ARTIST_AUDIO_URL . '/(\d+)' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&_edit_post_id=$matches[1]&pagename='.$slug_profile_audio,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/artists/dashboard-form/audio.php',
                        'text' => __('AUDIO', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_artist_main',
                            'id' => '_apollo_user_dashboard_artist_audio',
                            'class' => ' hidden '
                        )
                    ),
                    APL_Dashboard_Hor_Tab_Options::ARTIST_EVENTS_URL . '/(\d+)' => array(
                        'is_static'   => true,
                        'source'      => 'index.php?_apollo_dashboard=true&_edit_post_id=$matches[1]&pagename=' . $slug_profile_event,
                        'source_code' => APOLLO_TEMPLATES_DIR . '/artists/dashboard-form/multiple/events.php',
                        'text'        => __('EVENTS', 'apollo'),
                        'attr'        => array (
                            'data-acitve-by' => '_apollo_user_dashboard_artist_main',
                            'id'             => '_apollo_user_dashboard_artist_events',
                            'class'          => ' hidden ',
                        ),
                    ),
                ),

            ),
        );
        return $result;
    }

    public static function getArtist()
    {
        if(self::isInputMultiplePostMode()){
            return self::getArtistMultiple();
        }

        $video = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ARTIST_VIDEO));
        $post = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ARTIST_PROFILE));
        $audio = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ARTIST_AUDIO));
        $event = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ARTIST_EVENT));
        $profile_slug = $post ? $post->post_name : '';
        $video_slug = $video ? $video->post_name : '';
        $audio_slug = $audio ? $audio->post_name : '';
        $event_slug = $event ? $event->post_name : '';
        $event_key = 'user/artist/events';
        $locationPhotoForm = of_get_option(APL_Theme_Option_Site_Config_SubTab::_ARTIST_PHOTO_FORM_LOCATION, 1);

        $artist_menu = array(
            'artist' => array(
                'text' => of_get_option( Apollo_DB_Schema::_ARTIST_MENU_LABEL, __('ARTIST', 'apollo')),
                'id'   => '_apollo_user_dashboard_artist_main',
                'attr' => array(
                    'class' => 'fa fa-paint-brush fa-2x',
                ),
                'childs' => array(
                    'user/artist/profile' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$profile_slug,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/artists/dashboard-form/profile.php',
                        'text' => __('PROFILE', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_artist_main',
                            'id' => '_apollo_user_dashboard_artist_profile'
                        )
                    ),
                    'user/artist/photos' => array(
                        'is_static' => false,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.Apollo_Page_Creator::getSlugByPageID(Apollo_Page_Creator::ID_ARTIST_PHOTO),
                        'source_code' => '',
                        'text' => __('PHOTOS', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_artist_main',
                            'id' => '_apollo_user_dashboard_artist_photos'
                        )
                    ),
                    'user/artist/video' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$video_slug,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/artists/dashboard-form/video.php',
                        'text' => __('VIDEOS', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_artist_main',
                            'id' => '_apollo_user_dashboard_artist_video'
                        )
                    ),
                    'user/artist/audio' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$audio_slug,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/artists/dashboard-form/audio.php',
                        'text' => __('AUDIO', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_artist_main',
                            'id' => '_apollo_user_dashboard_artist_audio'
                        )
                    ),
                    $event_key => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$event_slug,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/artists/dashboard-form/events.php',
                        'text' => __('EVENTS', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_artist_main',
                            'id' => '_apollo_user_dashboard_artist_events'
                        )
                    ),
                ),

            ),
        );

        /**
         * @ticket #18695: [CF] 20181221 - Add the option for image upload to the main artist profile form.
         */
        if($locationPhotoForm == 2){
            unset($artist_menu['artist']['childs']['user/artist/photos']);
        }

        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EVENT_PT ) ) {
            unset($artist_menu['artist']['childs'][$event_key]);
        }

        return $artist_menu;
    }


    public static function getClassified()
    {
        $add_slug = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ADD_CLASSIFIED))->post_name;
        $public_slug = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_PUBLISHED_CLASSIFIEDS))->post_name;

        return array(
            'classified' => array(
                'text' => __('CLASSIFIED', 'apollo'),
                'id'   => '_apollo_user_dashboard_classified_main',
                'attr' => array(
                    'class' => 'fa fa-file-text-o fa-2x',
                ),
                'childs' => array(
                    'user/add-classified' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$add_slug,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/pages/dashboard/html/classified/add-classified.php',
                        'text' => __('ADD CLASSIFIED', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_classified_main',
                            'id' => '_apollo_user_dashboard_classified_add_classified'
                        )
                    ),
                    'user/classified/preview' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true',
                        'source_code' => APOLLO_TEMPLATES_DIR.'/pages/dashboard/html/classified/preview-add-classified.php',
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_classified_main',
                            'id' => '_apollo_user_dashboard_classified_add_classified',
                            'class' => 'hidden',
                        ),
                    ),
                    'user/classified/?([0-9]{1,})/?/preview$' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&_apollo_classified_id=$matches[1]',
                        'source_code' => APOLLO_TEMPLATES_DIR.'/pages/dashboard/html/classified/preview-add-classified.php',
                        'text' => '',
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_classified_main',
                            'id' => '_apollo_user_dashboard_classified_add_classified',
                            'class' => 'hidden'
                        )
                    ),
                    'user/classified/submit-success' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true',
                        'source_code' => APOLLO_TEMPLATES_DIR.'/pages/dashboard/html/classified/success-add-classified.php',
                        'text' => '',
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_classified_main',
                            'id' => '_apollo_user_dashboard_classified_add_classified',
                            'class' => 'hidden'
                        )
                    ),
                    'user/published-classifieds' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$public_slug,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/pages/dashboard/html/classified/published-classifieds.php',
                        'text' => __('PUBLISHED CLASSIFIEDS', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_classified_main',
                            'id' => '_apollo_user_dashboard_classified_published_classifieds'
                        )
                    ),
                    'user/edit-classified/(\d+)' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$public_slug.'&_apollo_classified_id=$matches[1]',
                        'source_code' => APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/classified/published-classifieds.php',
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_classified_main',
                            'id' => '_apollo_user_dashboard_classified_add_classified',
                            'class' => 'hidden'
                        )
                    ),
                    'user/published-classifieds/(\d+)' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$add_slug.'&_apollo_classified_id=$matches[1]&pagename='.$public_slug,
                        'source_code' => APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/classified/published-classifieds.php',
                        'text' => __('PUBLISHED CLASSIFIEDS', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_classified_main',
                            'id' => '_apollo_user_dashboard_classified_published_classifieds',
                            'class' => 'hidden',
                        )
                    ),
                ),

            ),
        );

    }

    public static function getEducation()
    {
        $profile_slug = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_EDUCATION_PROFILE))->post_name;
        $program_slug = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_EDUCATION_PROGRAM))->post_name;
        $public_program = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_EDUCATION_PROGRAM))->post_name;

        $education = array(
            'education' => array(
                'text' => __('EDUCATION', 'apollo'),
                'id' => '_apollo_user_dashboard_education_main',
                'attr' => array(
                    'class' => 'fa fa-graduation-cap fa-2x',
                ),
                'childs' => array(

                    'user/education/profile' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$profile_slug,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/educator/dashboard-form/profile.php',
                        'text' => __('PROFILE', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_education_main',
                            'id' => '_apollo_user_dashboard_education_profile'
                        )
                    ),

                    'user/education/add-program' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$program_slug,
                        'source_code' => APOLLO_TEMPLATES_DIR.'/educator/dashboard-form/add-program-1.php',
                        'text' => __('ADD PROGRAM', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_education_main',
                            'id' => '_apollo_user_dashboard_education_add_program'
                        )
                    ),
                    'user/education/add-program/(\d+)' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$program_slug.'&_apollo_program_id=$matches[1]',
                        'source_code' => APOLLO_TEMPLATES_DIR.'/educator/dashboard-form/add-program-1.php',
                        'text' => __('ADD PROGRAM', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_education_main',
                            'id' => '_apollo_user_dashboard_education_add_program',
                            'class' => 'hidden',
                        )
                    ),
                    /**
                     * @ticket #19655: [CF] 20190403 - FE Program dashboard - Add Copy/Delete Buttons
                     */
                    'user/copy-program/add-program/(\d+)' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$program_slug.'&_apollo_program_id=$matches[1]&_apollo_copy_program=1',
                        'source_code' => APOLLO_TEMPLATES_DIR.'/educator/dashboard-form/add-program-1.php',
                        'text' => __('ADD PROGRAM', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_education_main',
                            'id' => '_apollo_user_dashboard_education_copy_program',
                            'class' => 'hidden',
                        )
                    ),

                    'user/education/published-programs' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$public_program,
                        'source_code' => APOLLO_TEMPLATES_DIR . '/educator/dashboard-form/published-programs.php',
                        'text' => __('PUBLISHED PROGRAMS', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_education_main',
                            'id' => '_apollo_user_dashboard_education_published_programs'
                        )
                    ),
                    'user/education/published-programs/(\d+)' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$program_slug.'&_apollo_program_id=$matches[1]',
                        'source_code' => APOLLO_TEMPLATES_DIR . '/educator/dashboard-form/published-programs.php',
                        'text' => __('PUBLISHED PROGRAMS', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_education_main',
                            'id' => '_apollo_user_dashboard_education_published_programs',
                            'class' => 'hidden',
                        )
                    ),
                    /**
                     * @Ticket #18432- Add two steps: Preview and 'Success' page when submitted program form - item 3
                     */
                    'user/education/program/preview/?([0-9]{1,})/?$' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&_apollo_program_id=$matches[1]',
                        'source_code' => APOLLO_TEMPLATES_DIR . '/educator/dashboard-form/preview-program.php',
                        'text' => '',
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_education_main',
                            'id' => '_apollo_user_dashboard_education_preview_program',
                            'class' => 'hidden'
                        )
                    ),
                    'user/education/program/submit-success' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true',
                        'source_code' => APOLLO_TEMPLATES_DIR . '/educator/dashboard-form/submit-program-success.php',
                        'text' => '',
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_education_main',
                            'id' => '_apollo_user_dashboard_education_program_submit',
                            'class' => 'hidden'
                        )
                    ),
                ),

            ),
        );
        return $education;
    }

    //TriLm agency menu
    public static function getAgency(){
        $showAgency = false;
        if(!self::is_avaiable_module(Apollo_DB_Schema::_AGENCY_PT)) {
            return $showAgency;
        }
        $program_slug = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_EDUCATION_PROGRAM))->post_name;
        $venue_slug = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_AGENCY_VENUES ))->post_name;
        $org_slug = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_AGENCY_ORG ))->post_name;
        $artist_slug = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_AGENCY_ARTISTS ))->post_name;

        /* @ticket #15312 */
        $hasAgencies = ApolloAssociationFunction::getAssociatedModules(get_current_user_id(), Apollo_DB_Schema::_AGENCY_PT, true);
        $hasArtist = Apollo_User::countAssociatedItemsByAgency(Apollo_DB_Schema::_ARTIST_PT);
        $hasEducator = Apollo_User::countAssociatedItemsByAgency(Apollo_DB_Schema::_EDUCATION);
        $hasOrg = Apollo_User::countAssociatedItemsByAgency(Apollo_DB_Schema::_ORGANIZATION_PT);
        $hasVenue = Apollo_User::countAssociatedItemsByAgency(Apollo_DB_Schema::_VENUE_PT);
        $agency = array();
        if ($hasAgencies && ($hasArtist || $hasEducator || $hasOrg || $hasVenue)) {
            $agency = array(
                'text' => __('AGENCY', 'apollo'),
                'id' => '_apollo_user_dashboard_agency_main',
                'attr' => array(
                    'class' => 'fa fa-sitemap fa-2x',
                ),
            );
            //add module to agency menu
            if(self::is_avaiable_module( Apollo_DB_Schema::_EDUCATION)){
                $agency['childs']['user/agency/educator'] = array(
                    'is_static' => true,
                    'source' => 'index.php?_apollo_dashboard=true&pagename='.$program_slug,
                    'source_code' => APOLLO_TEMPLATES_DIR.'/pages/dashboard/html/agency/educators.php',
                    'text' => __('EDUCATORS', 'apollo'),
                    'attr' => array(
                        'data-acitve-by' => '_apollo_user_dashboard_agency_main',
                        'id' => '_apollo_user_dashboard_agency_selected_educator',
                        'class' => $hasEducator ? '' : 'hidden'
                    )
                );
                $agency['childs'][ 'user/agency/educator/?([0-9]{1,})/?$'] = array(
                    'is_static' => true,
                    'source' => 'index.php?_apollo_dashboard=true&pagename='.$program_slug. '&_apollo_educator_id=$matches[1]&_is_agency_educator_page=1',
                    'source_code' => APOLLO_TEMPLATES_DIR.'/pages/dashboard/html/agency/educators.php',
                    'text' => __('EDUCATORS', 'apollo'),
                    'attr' => array(
                        'data-acitve-by' => '_apollo_user_dashboard_agency_main',
                        'id' => '_apollo_user_dashboard_agency_selected_educator',
                        'class' => 'hidden',
                    )
                );
                $showAgency = true;
            }
            if(self::is_avaiable_module( Apollo_DB_Schema::_ORGANIZATION_PT )){
                $agency['childs']['user/agency/org'] = array(
                    'is_static' => true,
                    'source' => 'index.php?_apollo_dashboard=true&pagename='.$org_slug,
                    'source_code' => APOLLO_TEMPLATES_DIR.'/pages/dashboard/html/agency/org.php',
                    'text' => __('ORGANIZATIONS', 'apollo'),
                    'attr' => array(
                        'data-acitve-by' => '_apollo_user_dashboard_agency_main',
                        'id' => '_apollo_user_dashboard_agency_selected_org',
                        'class' => $hasOrg ? '' : 'hidden'
                    )
                );
                $agency['childs'][APL_Dashboard_Hor_Tab_Options::AGENCY_ORG_PROFILE_URL . '/?([0-9]{1,})/?$'] = array(
                    'is_static' => true,
                    'source' => 'index.php?_apollo_dashboard=true&pagename='.$org_slug. '&_apollo_org_id=$matches[1]&_edit_post_id=$matches[1]&_is_agency_educator_page=1&_agency_org_tab=profile',
                    'source_code' => APOLLO_TEMPLATES_DIR.'/pages/dashboard/html/agency/org.php',
                    'text' => __('ORGANIZATIONS', 'apollo'),
                    'attr' => array(
                        'data-acitve-by' => '_apollo_user_dashboard_agency_main',
                        'id' => '_apollo_user_dashboard_agency_selectedorg',
                        'class' => 'hidden',
                    )
                );
                $agency['childs'][APL_Dashboard_Hor_Tab_Options::AGENCY_ORG_PHOTOS_URL . '/?([0-9]{1,})/?$'] = array(
                    'is_static' => true,
                    'source' => 'index.php?_apollo_dashboard=true&pagename='.$org_slug. '&_apollo_org_id=$matches[1]&_edit_post_id=$matches[1]&_is_agency_educator_page=1&_agency_org_tab=photo',
                    'source_code' => APOLLO_TEMPLATES_DIR.'/pages/dashboard/html/agency/org.php',
                    'text' => __('ORGANIZATIONS', 'apollo'),
                    'attr' => array(
                        'data-acitve-by' => '_apollo_user_dashboard_agency_main',
                        'id' => '_apollo_user_dashboard_agency_selectedorg',
                        'class' => 'hidden',
                    )
                );
                $agency['childs'][APL_Dashboard_Hor_Tab_Options::AGENCY_ORG_AUDIO_URL . '/?([0-9]{1,})/?$'] = array(
                    'is_static' => true,
                    'source' => 'index.php?_apollo_dashboard=true&pagename='.$org_slug. '&_apollo_org_id=$matches[1]&_edit_post_id=$matches[1]&_is_agency_educator_page=1&_agency_org_tab=audio',
                    'source_code' => APOLLO_TEMPLATES_DIR.'/pages/dashboard/html/agency/org.php',
                    'text' => __('ORGANIZATIONS', 'apollo'),
                    'attr' => array(
                        'data-acitve-by' => '_apollo_user_dashboard_agency_main',
                        'id' => '_apollo_user_dashboard_agency_selectedorg',
                        'class' => 'hidden',
                    )
                );
                $agency['childs'][APL_Dashboard_Hor_Tab_Options::AGENCY_ORG_VIDEO_URL . '/?([0-9]{1,})/?$'] = array(
                    'is_static' => true,
                    'source' => 'index.php?_apollo_dashboard=true&pagename='.$org_slug. '&_apollo_org_id=$matches[1]&_edit_post_id=$matches[1]&_is_agency_educator_page=1&_agency_org_tab=video',
                    'source_code' => APOLLO_TEMPLATES_DIR.'/pages/dashboard/html/agency/org.php',
                    'text' => __('ORGANIZATIONS', 'apollo'),
                    'attr' => array(
                        'data-acitve-by' => '_apollo_user_dashboard_agency_main',
                        'id' => '_apollo_user_dashboard_agency_selectedorg',
                        'class' => 'hidden',
                    )
                );
                $showAgency = true;
            }
            if(self::is_avaiable_module( Apollo_DB_Schema::_VENUE_PT )){
                $agency['childs']['user/agency/venue'] = array(
                    'is_static' => true,
                    'source' => 'index.php?_apollo_dashboard=true&pagename='.$venue_slug,
                    'source_code' => APOLLO_TEMPLATES_DIR.'/pages/dashboard/html/agency/venues.php',
                    'text' => __('VENUES', 'apollo'),
                    'attr' => array(
                        'data-acitve-by' => '_apollo_user_dashboard_agency_main',
                        'id' => '_apollo_user_dashboard_agency_selected_venue',
                        'class' => $hasVenue ? '' : 'hidden'
                    )
                );
                $agency['childs'][APL_Dashboard_Hor_Tab_Options::AGENCY_VENUE_PROFILE_URL . '/?([0-9]{1,})/?$'] = array(
                    'is_static' => true,
                    'source' => 'index.php?_apollo_dashboard=true&pagename='.$venue_slug. '&_apollo_venue_id=$matches[1]&_edit_post_id=$matches[1]&_is_agency_educator_page=1&_agency_venue_tab=profile',
                    'source_code' => APOLLO_TEMPLATES_DIR.'/pages/dashboard/html/agency/venues.php',
                    'text' => __('VENUES', 'apollo'),
                    'attr' => array(
                        'data-acitve-by' => '_apollo_user_dashboard_agency_main',
                        'id' => '_apollo_user_dashboard_agency_selected_venue',
                        'class' => 'hidden',
                    )
                );
                $agency['childs'][APL_Dashboard_Hor_Tab_Options::AGENCY_VENUE_PHOTOS_URL . '/?([0-9]{1,})/?$'] = array(
                    'is_static' => true,
                    'source' => 'index.php?_apollo_dashboard=true&pagename='.$venue_slug. '&_apollo_venue_id=$matches[1]&_edit_post_id=$matches[1]&_is_agency_educator_page=1&_agency_venue_tab=photo',
                    'source_code' => APOLLO_TEMPLATES_DIR.'/pages/dashboard/html/agency/venues.php',
                    'text' => __('VENUES', 'apollo'),
                    'attr' => array(
                        'data-acitve-by' => '_apollo_user_dashboard_agency_main',
                        'id' => '_apollo_user_dashboard_agency_selected_venue',
                        'class' => 'hidden',
                    )
                );
                $agency['childs'][APL_Dashboard_Hor_Tab_Options::AGENCY_VENUE_VIDEO_URL . '/?([0-9]{1,})/?$'] = array(
                    'is_static' => true,
                    'source' => 'index.php?_apollo_dashboard=true&pagename='.$venue_slug. '&_apollo_venue_id=$matches[1]&_edit_post_id=$matches[1]&_is_agency_educator_page=1&_agency_venue_tab=video',
                    'source_code' => APOLLO_TEMPLATES_DIR.'/pages/dashboard/html/agency/venues.php',
                    'text' => __('VENUES', 'apollo'),
                    'attr' => array(
                        'data-acitve-by' => '_apollo_user_dashboard_agency_main',
                        'id' => '_apollo_user_dashboard_agency_selected_venue',
                        'class' => 'hidden',
                    )
                );
                $showAgency = true;
            }

            /* @ticket #15268 - [CF] 20180312 - [Artist] Add artist menu under the Agency module */
            if (self::is_avaiable_module( Apollo_DB_Schema::_ARTIST_PT )) {
                $agency['childs']['user/agency/artist'] = array(
                    'is_static'   => true,
                    'source'      => 'index.php?_apollo_dashboard=true&pagename=' . $artist_slug,
                    'source_code' => APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/agency/artists.php',
                    'text'        => __('ARTISTS', 'apollo'),
                    'attr'        => array (
                        'data-acitve-by' => '_apollo_user_dashboard_agency_main',
                        'id'             => '_apollo_user_dashboard_agency_selected_artist',
                        'class'          => $hasArtist ? '' : 'hidden'
                    ),
                );

                $agency['childs'][APL_Dashboard_Hor_Tab_Options::AGENCY_ARTIST_PROFILE_URL . '/?([0-9]{1,})/?$'] = array(
                    'is_static'   => true,
                    'source'      => 'index.php?_apollo_dashboard=true&pagename=' . $artist_slug . '&_apollo_artist_id=$matches[1]&_edit_post_id=$matches[1]&_is_agency_educator_page=1&_agency_artist_tab=profile',
                    'source_code' => APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/agency/artists.php',
                    'text'        => __('ARTISTS', 'apollo'),
                    'attr'        => array (
                        'data-acitve-by' => '_apollo_user_dashboard_agency_main',
                        'id'             => '_apollo_user_dashboard_agency_selected_artist',
                        'class'          => 'hidden',
                    ),
                );

                $agency['childs'][APL_Dashboard_Hor_Tab_Options::AGENCY_ARTIST_PHOTOS_URL . '/?([0-9]{1,})/?$'] = array(
                    'is_static'   => true,
                    'source'      => 'index.php?_apollo_dashboard=true&pagename=' . $artist_slug . '&_apollo_artist_id=$matches[1]&_edit_post_id=$matches[1]&_is_agency_educator_page=1&_agency_artist_tab=photo',
                    'source_code' => APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/agency/artists.php',
                    'text'        => __('ARTISTS', 'apollo'),
                    'attr'        => array (
                        'data-acitve-by' => '_apollo_user_dashboard_agency_main',
                        'id'             => '_apollo_user_dashboard_agency_selected_artist',
                        'class'          => 'hidden',
                    ),
                );

                $agency['childs'][APL_Dashboard_Hor_Tab_Options::AGENCY_ARTIST_VIDEO_URL . '/?([0-9]{1,})/?$'] = array(
                    'is_static'   => true,
                    'source'      => 'index.php?_apollo_dashboard=true&pagename=' . $artist_slug . '&_apollo_artist_id=$matches[1]&_edit_post_id=$matches[1]&_is_agency_educator_page=1&_agency_artist_tab=video',
                    'source_code' => APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/agency/artists.php',
                    'text'        => __('ARTISTS', 'apollo'),
                    'attr'        => array (
                        'data-acitve-by' => '_apollo_user_dashboard_agency_main',
                        'id'             => '_apollo_user_dashboard_agency_selected_artist',
                        'class'          => 'hidden',
                    ),
                );

                $agency['childs'][APL_Dashboard_Hor_Tab_Options::AGENCY_ARTIST_AUDIO_URL . '/?([0-9]{1,})/?$'] = array(
                    'is_static'   => true,
                    'source'      => 'index.php?_apollo_dashboard=true&pagename=' . $artist_slug . '&_apollo_artist_id=$matches[1]&_edit_post_id=$matches[1]&_is_agency_educator_page=1&_agency_artist_tab=audio',
                    'source_code' => APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/agency/artists.php',
                    'text'        => __('ARTISTS', 'apollo'),
                    'attr'        => array (
                        'data-acitve-by' => '_apollo_user_dashboard_agency_main',
                        'id'             => '_apollo_user_dashboard_agency_selected_artist',
                        'class'          => 'hidden',
                    ),
                );

                $agency['childs'][APL_Dashboard_Hor_Tab_Options::AGENCY_ARTIST_EVENTS_URL . '/?([0-9]{1,})/?$'] = array(
                    'is_static'   => true,
                    'source'      => 'index.php?_apollo_dashboard=true&pagename=' . $artist_slug . '&_apollo_artist_id=$matches[1]&_edit_post_id=$matches[1]&_is_agency_educator_page=1&_agency_artist_tab=events',
                    'source_code' => APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/agency/artists.php',
                    'text'        => __('ARTISTS', 'apollo'),
                    'attr'        => array (
                        'data-acitve-by' => '_apollo_user_dashboard_agency_main',
                        'id'             => '_apollo_user_dashboard_agency_selected_artist',
                        'class'          => 'hidden',
                    ),
                );

                $showAgency = true;
            }
        }
        if($showAgency == false)
            return false;
        return $agency;
    }

    public static function getBusiness()
    {
        $profile_slug = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_BUSINESS_PROFILE))->post_name;

        return array(
            'business' => array(
                'text' => __('BUSINESS', 'apollo'),
                'id' => '_apollo_user_dashboard_business_main',
                'attr' => array(
                    'class' => 'fa fa-suitcase fa-2x',
                ),
                'childs' => array(

                    'user/business' => array(
                        'is_static' => false,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$profile_slug,
                        'source_code' => APOLLO_TEMPLATES_DIR . '/pages/dashboard/account2.php',
                        'text' => __('PROFILE', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_business_main',
                            'id' => '_apollo_user_dashboard_business_profile'
                        )
                    ),

                    'user/business/photos' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true',
                        'source_code' => APOLLO_TEMPLATES_DIR . '/pages/dashboard/account2.php',
                        'text' => __('PHOTOS', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_business_main',
                            'id' => '_apollo_user_dashboard_business_photos'
                        )
                    ),

                    'user/business/video' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true',
                        'source_code' => APOLLO_TEMPLATES_DIR . '/pages/dashboard/account2.php',
                        'text' => __('VIDEO', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_business_main',
                            'id' => '_apollo_user_dashboard_business_video'
                        )
                    ),
                ),

            ),
        );
    }

    public static function getEvent()
    {

        $post = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_EVENTS));
        $addNew = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_ADD_EVENT));
        $importEvent = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_IMPORT_EVENT));
        $updateEvent = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_UPDATE_EVENT));
        $copyEvent = get_post(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_COPY_EVENT));
        $add_event_slug = $addNew ? $addNew->post_name : '';
        $events = $post ? $post->post_name : '';

        $eventRoutes = array(
            'event' => array(
                'text' => __('EVENTS', 'apollo'),
                'id' => '_apollo_user_dashboard_event_main',
                'attr' => array(
                    'class' => 'fa fa-calendar fa-2x',
                ),
                'childs' => array(

                    'user/events' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$events,
                        'source_code' => APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/event/dashboard.php',
                        'text' => __('Events', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_event_main',
                            'id' => '_apollo_user_dashboard_add_event',
                        )
                    ),

                    'user/add-event/step-1' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$add_event_slug,
                        'source_code' => APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/event/add-step1.php',
                        'text' => '',
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_event_main',
                            'id' => '_apollo_user_dashboard_add_event',
                            'class' => 'hidden'
                        )
                    ),

                    // ticket #11077: custom URL for submit new event at step 1
                    'user/add-event/step-1/?([A-Za-z0-9-]{1,})/?$' => array(
                        'is_static'   => true,
                        'source'      => 'index.php?_apollo_dashboard=true&pagename=' . $add_event_slug . '&_apollo_event_org_slug=$matches[1]&_apollo_event_id=$matches[1]',
                        'source_code' => APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/event/add-step1.php',
                        'text'        => '',
                        'attr'        => array(
                            'data-acitve-by' => '_apollo_user_dashboard_event_main',
                            'id'             => '_apollo_user_dashboard_add_event',
                            'class'          => 'hidden',
                        ),
                    ),

                    'user/add-event/step-1/?([0-9]{1,})/?$' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&_apollo_event_id=$matches[1]&pagename='.$updateEvent->post_name,
                        'source_code' => APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/event/add-step1.php',
                        'text' => '',
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_event_main',
                            'id' => '_apollo_user_dashboard_add_event',
                            'class' => 'hidden'
                        )
                    ),

                    'user/copy-event/step-1/?([0-9]{1,})/?$' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&_apollo_copy_event=true&_from_id=$matches[1]&pagename='.$copyEvent->post_name,
                        'source_code' => APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/event/add-step1.php',
                        'text' => '',
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_event_main',
                            'id' => '_apollo_user_dashboard_add_event',
                            'class' => 'hidden'
                        )
                    ),

                    'user/add-event/step-2a' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$add_event_slug,
                        'source_code' => APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/event/add-step2.php',
                        'text' => '',
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_event_main',
                            'id' => '_apollo_user_dashboard_add_event',
                            'class' => 'hidden'
                        )
                    ),

                    'user/add-event/step-2a/?([0-9]{1,})/?$' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&_apollo_event_id=$matches[1]&pagename='.$updateEvent->post_name,
                        'source_code' => APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/event/add-step2.php',
                        'text' => '',
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_event_main',
                            'id' => '_apollo_user_dashboard_add_event',
                            'class' => 'hidden'
                        )
                    ),

                    'user/add-event/step-2b' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_step2a=true&_apollo_dashboard=true&pagename='.$add_event_slug,
                        'source_code' => APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/event/add-step2.php',
                        'text' => '',
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_event_main',
                            'id' => '_apollo_user_dashboard_add_event',
                            'class' => 'hidden'
                        )
                    ),

                    'user/add-event/step-2b/?([0-9]{1,})/?$' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_step2a=true&_apollo_dashboard=true&_apollo_event_id=$matches[1]&pagename='.$updateEvent->post_name,
                        'source_code' => APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/event/add-step2.php',
                        'text' => '',
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_event_main',
                            'id' => '_apollo_user_dashboard_add_event',
                            'class' => 'hidden'
                        )
                    ),

                    'user/add-event/step-3' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$add_event_slug,
                        'source_code' => APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/event/add-step3.php',
                        'text' => '',
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_event_main',
                            'id' => '_apollo_user_dashboard_add_event',
                            'class' => 'hidden'
                        )
                    ),
                    'user/submit-event-success' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&pagename='.$add_event_slug,
                        'source_code' => APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/event/submit-event-success.php',
                        'text' => '',
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_event_main',
                            'id' => '_apollo_user_dashboard_add_event',
                            'class' => 'hidden'
                        )
                    ),


                    'user/add-event/step-3/?([0-9]{1,})/?$' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&_apollo_event_id=$matches[1]&pagename='.$updateEvent->post_name,
                        'source_code' => APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/event/add-step3.php',
                        'text' => '',
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_event_main',
                            'id' => '_apollo_user_dashboard_add_event',
                            'class' => 'hidden'
                        )
                    ),

                    'user/import-events' => array(
                        'is_static' => true,
                        'source' => 'index.php?_apollo_dashboard=true&_apollo_import_event_page=1&pagename='.$importEvent->post_name,
                        'source_code' => APOLLO_TEMPLATES_DIR . '/pages/dashboard/html/event/import.php',
                        'text' => __('IMPORT', 'apollo'),
                        'attr' => array(
                            'data-acitve-by' => '_apollo_user_dashboard_event_main',
                            'id' => '_apollo_user_dashboard_import'
                        )
                    ),
                ),

            ),
        );

            // check current user can use import event tool
            /** Vandd - @Ticket #12435 */
            $importToolOptions = of_get_option(Apollo_DB_Schema::_EVENT_ENABLE_IMPORT_TAB_FOR_ALL_USERS, false);
            $listOfUserCan = get_site_option(Apollo_Tables::_APL_TOGGLE_EVENT_IMPORT_TOOL, array());
            $networkImportTool = self::get_network_event_import_tool();
            $userCanUpdate = in_array(get_current_user_id(), $listOfUserCan);
            if($networkImportTool != 1 || (!$userCanUpdate && !$importToolOptions)){
                unset($eventRoutes['event']['childs']['user/import-events']);
            }
        return $eventRoutes;
    }

    public static function getMapBetweenUriAndSrc() {
        $tree_menus = self::getArrRewriteDashboard();

        $arr = array();

        foreach($tree_menus as $m => $a) {
            if(isset($a['source'])) {
                $arr['^'.$m.'/?$'] = array(
                    'src' => $a['source_code'],
                    'attr' => $a['attr'],
                    'is_static' => $a['is_static'],
                );
            }

            if(isset($a['childs'])) {
                foreach($a['childs'] as $_m => $_a) {
                    if(isset($_a['source'])) {
                        $arr['^'.$_m.'/?$'] = array(
                            'src' => $_a['source_code'],
                            'attr' => $_a['attr'],
                            'is_static' => $_a['is_static'],
                        );
                    }
                }
            }
        }

        return $arr;
    }

    public static function getCurrentIdMenuDashboardActive(){
        if(get_query_var('_apollo_dashboard') === "true") {
            $info = self::getInfoADCurrentHandle();
            return isset($info['attr']) ? $info['attr'] : '';
        }

        return false;
    }

    public static function getInfoADCurrentHandle()
    {
        $arr_map_static_and_src = self::getMapBetweenUriAndSrc();
        return array_key_exists($GLOBALS['wp']->matched_rule, $arr_map_static_and_src) ? $arr_map_static_and_src[$GLOBALS['wp']->matched_rule] : '';
    }

    public static function isDashboardPage(){

        if(get_query_var('_apollo_dashboard') === "true" && is_user_logged_in()) {
            return true;
        }

        return false;
    }

    /* WILL MOVE LATER - THIS FUNCTION IS NOT BELONG HERE */
    public static function getNewColorFromDefault($hash_source, $hash_dest, $hash_need_change)
    {
        // Source
        $hex1 = str_replace('#', '', $hash_source);

        if (strlen($hex1) == 3) {
            $hex1 = str_repeat(substr($hex1,0,1), 2).str_repeat(substr($hex1,1,1), 2).str_repeat(substr($hex1,2,1), 2);
        }

        // Get decimal values
        $r1 = hexdec(substr($hex1,0,2));
        $g1 = hexdec(substr($hex1,2,2));
        $b1 = hexdec(substr($hex1,4,2));

        // Dest
        $hex2 = str_replace('#', '', $hash_dest);

        if (strlen($hex2) == 3) {
            $hex2 = str_repeat(substr($hex2,0,1), 2).str_repeat(substr($hex2,1,1), 2).str_repeat(substr($hex2,2,1), 2);
        }

        // Get decimal values
        $r2 = hexdec(substr($hex2,0,2));
        $g2 = hexdec(substr($hex2,2,2));
        $b2 = hexdec(substr($hex2,4,2));

        //==> Delta
        $rd = $r2 - $r1;
        $gd = $g2 - $g1;
        $bd = $b2 - $b1;

        // Need change
        $hex3 = str_replace('#', '', $hash_need_change);

        if (strlen($hex3) == 3) {
            $hex3 = str_repeat(substr($hex3,0,1), 2).str_repeat(substr($hex3,1,1), 2).str_repeat(substr($hex3,2,1), 2);
        }

        // Get decimal values
        $r3 = hexdec(substr($hex3,0,2));
        $g3 = hexdec(substr($hex3,2,2));
        $b3 = hexdec(substr($hex3,4,2));


        // Adjust number of steps and keep it inside 0 to 255
        $r = max(0,min(255,$r3 + $rd));
        $g = max(0,min(255,$g3 + $gd));
        $b = max(0,min(255,$b3 + $bd));

        $r_hex = str_pad(dechex($r), 2, '0', STR_PAD_LEFT);
        $g_hex = str_pad(dechex($g), 2, '0', STR_PAD_LEFT);
        $b_hex = str_pad(dechex($b), 2, '0', STR_PAD_LEFT);

        return '#'. $r_hex.$g_hex.$b_hex;
    }

    public static function filterDisplay($str) {
        return stripslashes( sanitize_text_field( $str ) );
    }

    public static function clean_data_request( $str, $convertToHtml = false ) {

        $result =  wp_unslash( sanitize_text_field( $str ) );

        if ($convertToHtml) {
            $result = htmlspecialchars($result);
        }
        return $result;
    }

    public static function clean_array_data( $arr, $post_type = '' ) {
        if ( ! $arr || !is_array($arr) ) return false;

        foreach ( $arr as $k => $v ) {
            $arr[$k] = wp_unslash( $v );
        }
        return stripslashes_deep( $arr );
    }

    public static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function has_post_term( $post_id, $term_id ) {
        global $wpdb;
        $sql = " SELECT * FROM ". $wpdb->{Apollo_Tables::_POST_TERM} ." "
            . " WHERE post_id = $post_id AND term_id = $term_id ";

        return $wpdb->get_row( $sql );
    }

    public static function is_valid_event_date( $_start, $_end, $format = 'Y-m-d' ) {
        $_time_start = strtotime($_start);
        $_time_end  = strtotime($_end);
        $_time_today = strtotime(current_time('Y-m-d'));
        return $_time_start <= $_time_end && ( $_time_start >= $_time_today || $_time_end >= $_time_today );
    }

    public static function is_valid_date( $_date, $format = "Y-m-d" ) {
        $_date_valid = DateTime::createFromFormat($format, $_date);
        return $_date_valid !== false ;

    }

    public static function yt_exists( $link ) {

        if ( ! $link ) return false;

        $code_data = explode( 'watch?v=', $link );
        if ( ! isset( $code_data[1] ) ) {
            $code_data = explode( 'youtu.be/', $link );
        }

        if ( ! isset( $code_data[1] ) ) {
            $code_data = explode( 'youtube.com/embed/', $link );
        }

        $format_yt_link = 'https://www.youtube.com/watch?v='. ( isset( $code_data[1] ) ? $code_data[1] : '' );
        $the_url = "http://www.youtube.com/oembed?url=".$format_yt_link."&format=json";


        $headers = @get_headers( $the_url );

        if ( substr( $headers[0], 9, 3) !== "404" ) {
            return true;
        }
        return false;
    }

    public static function is_youtube_url($url) {
        return preg_match("/^(https\:\/\/)?(www\.)?(youtube\.com|youtu\.be)\/watch\?v\=[\w-]+$/", $url)
        || preg_match( "/^(https\:\/\/)?(www\.)?(youtube\.com|youtu\.be)\/embed\/[\w-]+$/" , $url)
        || preg_match( "/^(https\:\/\/)?(www\.)?(youtu\.be)\/[\w-]+$/" , $url)
        || preg_match("/^(http\:\/\/)?(www\.)?(youtube\.com|youtu\.be)\/watch\?v\=[\w-]+$/", $url)
        || preg_match( "/^(http\:\/\/)?(www\.)?(youtube\.com|youtu\.be)\/embed\/[\w-]+$/" , $url)
        || preg_match( "/^(http\:\/\/)?(www\.)?(youtu\.be)\/[\w-]+$/" , $url);
    }

    public static function is_vimeo($url) {
        return (preg_match('/.vimeo\.com/i', $url));
    }

    public static function safeRedirect($url)
    {
        if (!headers_sent()) {
            wp_redirect($url);
        } else {
            echo "<script type='text/javascript'> window.location.href='". $url ."' </script>";
            exit;
        }
    }

    /**
     * Check option REQUIRED PRIMARY IMAGE is ENABLED or NOT in each setting tab in ThemeOption
     * @param string $module
     * @return bool
     */
    public static function isEnableRequiredPrimaryImageByModuleName($module = '') {
        switch ($module) {
            case Apollo_DB_Schema::_ORGANIZATION_PT:
                $isEnabled = of_get_option(Apollo_DB_Schema::_ORGANIZATION_ENABLE_REQUIRED_PRIMARY_IMAGE);
                break;
            case Apollo_DB_Schema::_ARTIST_PT:
                $isEnabled = of_get_option(Apollo_DB_Schema::_ARTIST_ENABLE_REQUIRED_PRIMARY_IMAGE);
                break;
            case Apollo_DB_Schema::_VENUE_PT:
                $isEnabled = of_get_option(Apollo_DB_Schema::_VENUE_ENABLE_REQUIRED_PRIMARY_IMAGE);
                break;
            case Apollo_DB_Schema::_EVENT_PT:
                $isEnabled = of_get_option(Apollo_DB_Schema::_ENABLE_REQUIREMENT_EVENT_PRIMARY_IMAGE);
                break;
            default:
                $isEnabled = 0;
                break;
        }
        return intval($isEnabled) === 1;
    }

    public static function getMinFeatureImgUpload($target) { // Target mean type, event, artist, venue ...
        $mw = get_option('medium_size_w');
        $mh = get_option('medium_size_h');

        $hasRetina = of_get_option( Apollo_DB_Schema::_ENABLE_RETINA_DISPLAY );
        $duplicateNum = $hasRetina ? 2 : 1;
        switch($target) {
            case 'event':
                return array(
                    'w' => $duplicateNum * $mw,
                    'h' => $duplicateNum * $mh,
                );
            case 'artist':
                return array(
                    'w' => $duplicateNum * $mw,
                    'h' => $duplicateNum * $mh,
                );
            default:
                return array(
                    'w' => $duplicateNum * $mw,
                    'h' => $duplicateNum * $mh,
                );
        }
    }
    public static function getMinGalleryImgUpload($target) { // Target mean type, event, artist, venue ...
        $mw = get_option('medium_size_w');
        $mh = get_option('medium_size_h');

        switch($target) {
            default:
                return array(
                    'w' => 'auto',
                    'h' => 'auto',
                );
        }
    }

    public static function getMetaKeyGalleryImgByTarget($target)
    {
        switch($target) {
            case 'event':
                return Apollo_DB_Schema::_APOLLO_EVENT_IMAGE_GALLERY;
            case 'artist':
                return Apollo_DB_Schema::_APL_ARTIST_IMAGE_GALLERY;
            case 'program':
                return Apollo_DB_Schema::_APL_PROGRAM_IMAGE_GALLERY;
            case 'organization':
                return Apollo_DB_Schema::_APL_ORG_IMAGE_GALLERY;
            case 'venue':
                return Apollo_DB_Schema::_APL_VENUE_IMAGE_GALLERY;
			case 'classified':
                return Apollo_DB_Schema::_APL_CLASSIFIED_IMAGE_GALLERY;
        }
    }

    /**
     * Save data
     */
    public static function convertCkEditorToTinyCME ($content) {
        //for <br/>
        // Right now we use tinymce in the front-end form so do not need to convert data
//        $content = str_replace(array('\r','\n'),'', $content);
//        //for image src, html link
//        $content = str_replace(array('\\\&quot;','\\'),'', $content);
//        $content = str_replace("<br/>", "", $content);

        return $content;
    }

    public static function stripSlash ($content) {
        $content = str_replace(array('\r','\n'),'', $content);
        $content = str_replace(array('\\\&quot;','\\'),'', $content);
        $content = str_replace("<br/>", "", $content);

        return $content;
    }

    /**
     * For front end form
     */
    public static function convertTinyCMEToCkEditor($content) {

        // Right now we use tinymce in the front-end form so do not need to convert data
//        $content =  str_replace( ']]>', ']]&gt;', $content );
//
//        return nl2br($content);


        return $content;
    }

    /**
     * @param $content
     * @param bool $stripslash
     * @param bool $applyFilterContent
     * @return string
     */
    public static function convertContentEditorToHtml($content, $stripslash = false, $applyFilterContent = true) {

        if ($applyFilterContent) {
            $content = apply_filters( 'the_content', $content );
        }
        $content = str_replace( ']]>', ']]&gt;', $content );
        $content = str_replace('\r\n', '', $content);
        return $stripslash ? stripslashes($content) : wpautop($content);
    }

    public static function get_us_states( $required = false ) {
        $states = of_get_option( Apollo_DB_Schema::_US_STATES );

        if ( ! $states ) $states = APOLLO_US_STATES;

        $states = explode( ',' , str_replace( ' ', '', $states ));
        $_key_states = $states;
        array_unshift($_key_states, '');
        array_unshift($states, sprintf( __( 'Select State %s', 'apollo' ), $required ? '(*)' : '' ));

        return array_combine( $_key_states , $states);
    }

    public static function get_regions( $required = false, $displayDefaultOption = true ) {
        $regions = of_get_option( Apollo_DB_Schema::_REGIONS );

        if ( ! $regions ) return false;

        $array_region = explode( ',' , $regions);
        $array_region = array_map('trim', $array_region);

        $_key_regions = $array_region;

        if($displayDefaultOption){
            array_unshift($_key_regions, 0);
            array_unshift($array_region, sprintf( __( 'Select Region %s ', 'apollo' ), $required ? '(*)' : '' ));
        }
        return array_combine( $_key_regions , $array_region);
    }



    public static function getRegionsByRegisteredVenues(){
        global $wpdb;
        $results = NULL;
        $sql = " SELECT vm.meta_value as region_name
                 FROM {$wpdb->{Apollo_Tables::_APL_VENUE_META}} vm
                 WHERE vm.meta_key = '".Apollo_DB_Schema::_VENUE_REGION."'
                 GROUP BY vm.meta_value
                 ORDER BY vm.meta_value ASC
               ";
        $results = $wpdb->get_results($sql,ARRAY_A);
        return empty($results) ? false : $results;
    }

    public static function getRegionsByVenues( $required = false ) {
        $regions = self::getRegionsByRegisteredVenues();
        if ( ! $regions  || !is_array($regions)) return false;
        $finalListRegions = array();
        foreach($regions as $rValue){
            $finalListRegions[] = trim($rValue['region_name']);
        }
        $_key_regions = $finalListRegions;
        array_unshift($_key_regions, '');
        array_unshift($finalListRegions, sprintf( __( '-- Region %s --', 'apollo' ), $required ? '(*)' : '' ));

        return array_combine( $_key_regions , $finalListRegions);
    }

    public static function showRegion(){
        if( Apollo_App::get_regions())
            return true;
        else
            return false;
    }

    public static function render_regions_option( $current_val = '' ) {

        $regions = self::get_regions();
        if ( ! $regions ) return false;
        $str = '';
        foreach( $regions as $k => $v ) {
            $selected = $current_val ? selected( $k, $current_val,false ) : '';
            $str .= '<option '.$selected.' value="'.$k.'">'.$v.'</option>';
        }
        return $str;
    }

    public static function render_us_states_option( $current_val = '' ) {

        $us_states = self::getStateByTerritory();
        if ( ! $us_states ) return false;
        $str = '';
        foreach( $us_states as $k => $v ) {
            $selected = $current_val ? selected( $k, $current_val,false ) : '';
            $str .= '<option '.$selected.' value="'.$k.'">'.$v.'</option>';
        }
        return $str;
    }

    public static function render_us_city_option( $current_val = '',$state = '' ) {
        $us_states = self::getCityByTerritory(false,$state,true);

        if ( ! $us_states ) return false;
        $str = '';
        foreach( $us_states as $k => $v ) {
            $selected = $current_val ? selected( $k, $current_val,false ) : '';
            $str .= '<option '.$selected.' value="'.$k.'">'.$v.'</option>';
        }
        return $str;
    }
    public static function render_us_zip_option( $current_val = '',$state = '' ,$city = '' ) {

        $us_states = self::getZipByTerritory(false,$state,$city);
        if ( ! $us_states ) return false;
        $str = '';
        foreach( $us_states as $k => $v ) {
            $selected = $current_val ? selected( $k, $current_val ) : '';
            $str .= '<option '.$selected.' value="'.$k.'">'.$v.'</option>';
        }
        return $str;
    }

    public static function get_cus_field_group_opt($post_type) {
        return Apollo_DB_Schema::_APL_CF_GROUP. '_'. strtolower(str_replace('-', '_', $post_type));
    }

    public static function add_ads_block( $top = TRUE ) {
        echo '<div id="apl-ads-'.($top ? 'top' : 'bottom').'"></div>';
    }

    public static function getEmailTemplateContent($action_template) {

        if ( ! $val = Apollo_App::get_static_html_content($action_template, FALSE, 'html') ) {
            $val = render_php_to_string( APOLLO_ADMIN_DIR. '/theme-options/default-template/email/'.$action_template. '.php' );
        }
        return $val;
    }

    /**
     * Return formal class name
     *
     * @access public
     * @return string
     *
     */
    public static function processClassAction($action) {
        return str_replace(' ', '_', ucwords(str_replace('_', ' ', $action)));
    }

    /**
     * Get content of global page by path
     */
    public static function getPageContentByPath($pageId) {
        $page = get_page_by_path( $pageId ,'page');
        $content = isset($page->post_content) ? $page->post_content : '';
        if ( strpos( $content , 'Coming soon') !== false ) $content = '';
        echo $content ? '<div class="apl-header-global-page-content">'.$page->post_content .'</div>':'';
    }

    public static function isEnableAddItBtn() {
        return of_get_option( Apollo_DB_Schema::_ENABLE_ADD_IT_BTN ) !== '0';
    }

    public static function getHeaderLogo($fileName){

        $currentTheme = function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '';

        if ( $currentTheme == APL_SM_DIR_THEME_NAME) {
            $file = SONOMA_FRONTEND_ASSETS_URI.'/images/demo/logo_rgb.png';
            return $file;
        } else {
            $htmlHeader = self::get_static_html_content($fileName);
            if($htmlHeader != ''){
                $matches = array();

                /**
                 * @ticket #19431, 19434: add custom logo to blog, educator print page
                 */
                if(get_query_var( '_apollo_educator_id' ) || get_query_var( '_apollo_post_id' )){
                    $logoCustom = preg_match('@<div class="logo art-education-header">(.*?)</div>@is', $htmlHeader, $matches);
                    if (!empty($logoCustom)) {
                        preg_match_all('/src="([^"]+)"/', $matches[1], $matches);
                        if (is_array($matches) && count($matches) > 0) {
                            if (isset($matches[1]) && is_array($matches[1]))
                                return $matches[1];
                        }
                    }
                }

                preg_match('/src="([^"]+)"/', $htmlHeader, $matches);
                if(is_array($matches) && count($matches) > 0){
                    if(isset($matches[1]) && is_string($matches[1]))
                        return $matches[1];
                }
            }
        }

        return '';
    }

    public static function getPrimarycolorCSSContent() {
        return file_get_contents(get_primary_color_css_abs_path());
    }

    /**
     * Check logged in user who is author of post
     * @param string $post_id
     * @param string $post_type
     * @return boolean
     *
     */
    public static function isUserOwnPost($post_id, $post_type) {
        $currentUserID = get_current_user_id();
        $args = array(
            'post_type'         => $post_type,
            'post_status'       => array('publish','pending'),
            'author'            => $currentUserID,
            'p'                 => $post_id
        );
        $listPost = query_posts( $args );
        wp_reset_query();
        return !empty($listPost);
    }

	//Hong get arr org
	public static function getArrOrganization( $title = '' ){
		$org = self::get_list_post_type_items( Apollo_DB_Schema::_ORGANIZATION_PT );
		$arrOrg = array();
		if ($org) {
			$arrOrg[''] = $title;
			foreach ($org as $v ){
				$arrOrg[$v -> ID] = $v -> post_title;
			}
		}
		return $arrOrg;
	}

    public static function isFullTemplatePage($pageTemplate) {
        return !isset( $_GET['s'] ) && $pageTemplate == 'template-full-width.php';
    }

    public static function my_cut_excerpt ($str = '',$character = 0,$more = ''){
        $str = str_replace("\xc2\xa0",' ',html_entity_decode($str,ENT_COMPAT,'UTF-8'));

        /**
         * @ticket #18402: Arts Education Customizations - [Blog listing page] - Suppress 'Author' and 'Cateogry' - Item 3,4
         */
        if(strlen($str) > $character){
            $pos = strpos($str, ' ', $character);
            return substr($str,0,$pos ).$more;
        }

        return $str;
    }

    public static function replaceTextHeader($buffer) {
       $replacements = array(__('LOGIN','apollo'),__('LOGOUT','apollo'),__('REGISTER','apollo'),__('MY ACCOUNT','apollo'));
        $patterns = array('/{Login}/','/{Logout}/','/{Register}/','/{My Account}/');

        return preg_replace($patterns, $replacements, $buffer);
    }

    public static function getListTerritory($arr_key = array()){
        global $wpdb;
        $results = NULL;
        if( $arr_key ) {
            $str_key = implode("', '",$arr_key);
            $sql = " SELECT  DISTINCT state_prefix,state_name FROM wp_apollo_state_zip "
                . " WHERE state_prefix IN ('%s')
                ORDER BY state_name ASC
            ";
            $query = sprintf($sql,$str_key);
            $results =  $wpdb->get_results($query);
        }
        return $results;
    }

    public static function getStateByTerritory($required=false, $hasLabel = true){
        $_arr = of_get_option(Apollo_DB_Schema::_TERR_STATES);
        $states = array();

        if($_arr){
            foreach( $_arr as $key => $terri ){
                $states[$key] = $key;
            }
        }

        sort($states);
        $result = array_combine($states, $states);

        if (!$result) $result = array();

        if ($hasLabel === true) {
            array_unshift($result, sprintf( __( 'Select State %s ', 'apollo' ), $required ? '(*)' : '' ));
        }
        if (isset($_GET['check'])) {var_dump($result);}
        return $result;
    }

    public static function getCityByTerritory($required=false, $state = '', $hasLabel = true, $first_label = false){
        $_arr = of_get_option(Apollo_DB_Schema::_TERR_DATA);
        $array_state = self::getListState();
        // Get cities for specific state
        if ($state && in_array($state, $array_state)) {
             $cities = isset($_arr[$state]) && $_arr[$state] ? array_keys($_arr[$state]) : array();
        } else {
            $cities = array();
            if ( $_arr ) {
                foreach($_arr as $key => $val) {
                    if (!$val || !in_array($key, $array_state)) continue;
                    $cities = array_merge($cities, array_keys($val));
                }
            }
        }

        $cities = Apollo_App::sortOrderForCities($cities);
        if ($hasLabel) {

            $firstLabelOpt = $first_label ? $first_label : __( 'Select City %s', 'apollo' );
            array_unshift($cities, sprintf( $firstLabelOpt, $required ? '(*)' : '' ));
            $cityKeys = $cities;
            $cityKeys[0] = 0;
            return array_combine($cityKeys, $cities);
        }

        return array_combine($cities, $cities);
    }

    public static function getCityStateByTerritory($required=false){
        $_arr = of_get_option(Apollo_DB_Schema::_TERR_DATA);

        $array_state = self::getListState();
        sort($array_state);

        // Get cities for specific state

        $cities = array();
        $cityState = array();
        if ( $array_state ) {
            foreach($array_state as $key) {
                $val = isset($_arr[$key]) && $_arr[$key] ? $_arr[$key] : false;
                if (!$val || !in_array($key, $array_state)) continue;

                $cities = array_keys($val);
                sort($cities);
                $cityState[$key] = $cities;
                $cities = array();
            }
        }

        array_unshift($cityState, sprintf( __( 'Select City %s', 'apollo' ), $required ? '(*)' : '' ));

        return $cityState;
    }

    public static function getZipByTerritory($required=false, $state = false, $city = false, $hasLabel = true){
        $_arr = of_get_option(Apollo_DB_Schema::_TERR_DATA);
        $zipcodes = array();

        // Get zips of the specific city and state
        if ($state && $city) {
            $zipcodes = isset($_arr[$state][$city]) && $_arr[$state][$city] ? array_keys($_arr[$state][$city]) : array();
            asort($zipcodes);
        } else if (!$city && !$state) {

            if ( $_arr ) {
                foreach($_arr as $key => $cities) {
                    if (!$cities) continue;
                    foreach($cities as $val ) {
                        $val = is_array($val) ? array_keys($val) : '';
                        asort($val);
                        if($val) {
                            $zipcodes = array_merge($zipcodes, $val);
                        }
                    }

                }
            }
        } else if( !$state ) {
            if ( $_arr ) {
                foreach($_arr as $key => $cities) {
                    if (!$cities) continue;
                    foreach($cities as $kcity => $val ) {

                        if ( $city != $kcity ) continue;

                        $val = is_array($val) ? array_keys($val) : '';
                        asort($val);
                        if($val) {
                            $zipcodes = array_merge($zipcodes, $val);
                        }
                    }
                }
            }
        } else {
            if ( $_arr ) {
                $cities = isset($_arr[$state]) ? $_arr[$state] : array();
                if($cities){
                    foreach($cities as $kcity => $val ) {
                        $val = is_array($val) ? array_keys($val) : '';
                        asort($val);
                        if($val) {
                            $zipcodes = array_merge($zipcodes, $val);
                        }
                    }
                }
            }
        }

        $zipcodes = Apollo_App::sortOrderForZipCodes($zipcodes);

        if ($hasLabel) {
            array_unshift($zipcodes, sprintf(__('Select Zip %s ', 'apollo'), $required ? '(*)' : '' ));
            $zipcodeKeys = $zipcodes;
            $zipcodeKeys[0] = 0;
            return array_combine($zipcodeKeys, $zipcodes);
        }
        return array_combine($zipcodes, $zipcodes);
    }

    public static function getListState(){
        $_arr = of_get_option(Apollo_DB_Schema::_TERR_STATES);
        $states = array();
        if($_arr){
            foreach( $_arr as $key => $terri ){
                $states[$key] = $key;
            }
        }
        return $states;
    }

    public static function is_homepage(){
        return ($_SERVER["REQUEST_URI"] == '/' || strpos($_SERVER["REQUEST_URI"], '/page') === 0);
    }

    public static function sortOrderForZipCodes($zips, $order = 'ASC'){
        if(empty($zips)) return array();
        $resultSortZips = $zips;
        if($order === 'ASC' ){
            sort($resultSortZips);
        }elseif($order === 'DESC'){
            rsort($resultSortZips);
        }
        return $resultSortZips;
    }

    public static function sortOrderForCities($cities, $order = 'ASC'){
        if(empty($cities)) return array();
        $resultSortCities = $cities;
        if($order === 'ASC' ){
            asort($resultSortCities);
        }elseif($order === 'DESC'){
            arsort($resultSortCities);
        }
        return $resultSortCities;
    }

    public static function apolloBuildHttpQuery( $query, $encodeFlag = true ){

        $query_array = array();

        foreach( $query as $key => $key_value ){
            if($encodeFlag){
                $query_array[] = urlencode( $key ) . '=' . urlencode( $key_value );
            } else {
                $query_array[] = $key . '=' . $key_value;
            }
        }
        return implode( '&', $query_array );
    }


    public static function aplGetTemplatePart($args = array()){
        $file = isset($args['file']) ? $args['file'] : '';
        $template_args = isset($args['template_args']) ? $args['template_args'] : array();
        $return = isset($args['return']) ? $args['return'] : true;
        $pathFileToClassCacheHandler = isset($args['file_to_class_cache_handler']) ? $args['file_to_class_cache_handler'] : '';
        $classNameCacheHandler = isset($args['class_name_cache_handler']) ? $args['class_name_cache_handler'] : '';

        if(empty($file)) return '';

        if(!empty($classNameCacheHandler)
            && file_exists($pathFileToClassCacheHandler)
            && is_file($pathFileToClassCacheHandler)
        ){
            require_once($pathFileToClassCacheHandler);
            if( class_exists($classNameCacheHandler) && method_exists($classNameCacheHandler,'output')){
                $cacheClassInstance = new $classNameCacheHandler($args);
                if($return){
                    return $cacheClassInstance->output();
                } else {
                    echo $cacheClassInstance->output();
                    return true;
                }
            }
        }

        $template_args = wp_parse_args( $template_args );

        $fileBasename = basename($file);

        if ( !file_exists($file) && file_exists( get_stylesheet_directory() . '/' . $fileBasename . '.php' ) ) {
            $file = get_stylesheet_directory() . '/' . $fileBasename . '.php';
        }elseif ( !file_exists($file) && file_exists( get_template_directory() . '/' . $fileBasename . '.php' ) ){
            $file = get_template_directory() . '/' . $fileBasename . '.php';
        }

        if(file_exists($file) && is_file($file)){
            ob_start();
            require( $file );
            $data = ob_get_clean();
        } else {
            $data = '';
        }

        if ( $return )
            return $data;
        else
            echo $data;
    }

    /**
     * Include template with args
     *
     * @param string $file
     * @param string $args
    */
    public static function includeTemplate($file, $args) {
        extract($args);
        include $file;
    }

    public static function getTemplatePartCustom( $file, $template_args = array(), $return = true, $cached = false, $expired_time = '') {
        // Init cache file
        if(file_exists(APOLLO_INCLUDES_DIR. '/admin/tools/cache/Inc/File.php')){
            require_once APOLLO_INCLUDES_DIR. '/admin/tools/cache/Inc/File.php';
        }

        $localSiteCacheEnabled = false;
        $currentBlogID = get_current_blog_id();
        $optionCachedValue = self::get_network_local_cache($currentBlogID);
        if(intval($optionCachedValue) === 1){
            $localSiteCacheEnabled = true;
        }

        if($localSiteCacheEnabled && $cached && class_exists('APLC_Inc_File')){
            $cachedClassIns = new APLC_Inc_File();
        } else {
            $cachedClassIns = null;
        }

        if ($localSiteCacheEnabled && $cached &&  $cachedClassIns !== null ) {
            $cachedData = $cachedClassIns->getCache(basename($file));
            if(!empty($cachedData) && $return) {
                return $cachedData;
            }elseif(!empty($cachedData)){
                echo $cachedData;
                return '';
            }
        }

        $template_args = wp_parse_args( $template_args );

        $fileBasename = basename($file);

        if (!file_exists($file)) {
            if (file_exists( get_stylesheet_directory() . '/' . $fileBasename . '.php' ) ) {
                $file = get_stylesheet_directory() . '/' . $fileBasename . '.php';
            }
            elseif (file_exists( get_template_directory() . '/' . $fileBasename . '.php' ) ){
                $file = get_template_directory() . '/' . $fileBasename . '.php';
            }
            /** @Ticket #14835 */
            else if (file_exists( get_template_directory() . '/' . $file . '.php' ) ) {
                $file = get_template_directory() . '/' . $file . '.php';
            }
        }

        ob_start();
        require( $file );
        $data = ob_get_clean();

        if (isset($customExpiredtime) && $customExpiredtime) $expired_time = $customExpiredtime; // If has custom expired time from the required file

        if ($localSiteCacheEnabled && $cached &&  $cachedClassIns !== null ) {
            $cachedClassIns->addCache($file,$data,$expired_time);
        }

        if ( $return )
            return $data;
        else
            echo $data;
    }

    public static function responseHandler($status = TRUE, $data = array()) {
        return json_encode(array('result' => $status ? "SUCCESS" : "FAILED", 'data' => $data));
    }

    public static function isBlogPostsPage(){
        return ( (is_archive() || is_author() || is_tag() || is_single()) && get_post_type() == 'post')
        || strpos($GLOBALS['wp']->matched_rule, 'blog/') !== false;
    }

    /* Thienld : functions for displaying artist search widget to top on mobile version */
    public static function displaySearchArtistWidgetOnMobile(){
        if ( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ARTIST_PT)
            && (
                strpos($GLOBALS['wp']->matched_rule, 'artist/') !== false
            )
            && !Apollo_App::isDashboardPage()
            && is_active_widget(false,false,'apollo_search_'.Apollo_DB_Schema::_ARTIST_PT.'_widget') !== FALSE
        ) {
            $_GET['top-search-mobile'] = 1;
            include_once APOLLO_WIDGETS_DIR . '/search/templates/artist-top.php' ;
        }
    }

    /* Thienld : functions for displaying artist search widget to top on mobile version */
    public static function displaySearchBusinessWidgetOnMobile(){

        $slugBusinessType = self::getTaxSlugByModuleName('business');
        $slugBusinessType = $slugBusinessType . '/';

        if ( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_BUSINESS_PT)
            && (
                strpos($GLOBALS['wp']->matched_rule, 'business/') !== false
                || strpos($GLOBALS['wp']->matched_rule, $slugBusinessType) !== false
            )
            && !Apollo_App::isDashboardPage()
            && is_active_widget(false,false,'apollo_search_'.Apollo_DB_Schema::_BUSINESS_PT.'_widget') !== FALSE
        ) {
            $_GET['top-search-mobile'] = 1;
            include_once APOLLO_WIDGETS_DIR . '/search/templates/business-top.php' ;
        }
    }

    /* Thienld : functions for displaying classified search widget to top on mobile version */
    public static function displaySearchClassifiedWidgetOnMobile(){
        if ( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_CLASSIFIED)
            && (
                strpos($GLOBALS['wp']->matched_rule, 'classified/') !== false
            )
            && !Apollo_App::isDashboardPage()
            && is_active_widget(false,false,'apollo_search_'.Apollo_DB_Schema::_CLASSIFIED.'_widget') !== FALSE
        ) {
            $_GET['top-search-mobile'] = 1;
            include_once APOLLO_WIDGETS_DIR . '/search/templates/classified-top.php' ;
        }
    }

    /* Thienld : functions for displaying education search widget to top on mobile version */
    public static function displaySearchEducationWidgetOnMobile(){
        if ( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EDUCATION)
            && (
                strpos($GLOBALS['wp']->matched_rule, 'educator/') !== false
                || strpos($GLOBALS['wp']->matched_rule, 'program/') !== false
            )
            && !Apollo_App::isDashboardPage()
            && is_active_widget(false,false,'apollo_search_'.Apollo_DB_Schema::_EDUCATION.'_widget') !== FALSE
        ) {
            $_GET['top-search-mobile'] = 1;
            include_once APOLLO_WIDGETS_DIR . '/search/templates/education-top.php' ;
        }
    }

    /* Thienld : functions for displaying event search form on mode HORIZONTAL desktop version */
    public static function displaySearchEventHorizontalOnDesktop(){
        if (  Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EVENT_PT)
            && (
                self::is_homepage()
                || is_page()
                || is_search()
                || strpos($GLOBALS['wp']->matched_rule, 'event/') !== false
                || strpos($GLOBALS['wp']->matched_rule, 'categories/') !== false
                || self::isBlogPostsPage() // via task : ThienLD - http://redmine.elidev.info/issues/8490
            )
//            && !self::isBlogPage() // via task : ThienLD - http://redmine.elidev.info/issues/8490
            && !self::isSubmitFormPage()
            && !Apollo_App::isDashboardPage()
            && of_get_option( Apollo_DB_Schema::_EVENT_SEARCH_TYPE, 'rgt' ) == 'hor'
        ) {
            $_GET['top-search-mobile-hor'] = 1;
            ob_start();
            include APOLLO_WIDGETS_DIR . '/search/templates/event-top.php' ;
            return ob_get_clean();
        }
    }

    /* Thienld : functions for displaying event search widget to top on mobile version */
    public static function displaySearchEventWidgetOnMobile(){
        if (  Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EVENT_PT)
            && (
                self::is_homepage()
                || is_search()
                || strpos($GLOBALS['wp']->matched_rule, 'event/') !== false
                || strpos($GLOBALS['wp']->matched_rule, 'categories/') !== false
                || self::isBlogPostsPage() // via task : ThienLD - http://redmine.elidev.info/issues/8490
            )
            && !Apollo_App::isDashboardPage()
            && is_active_widget(false,false,'apollo_search_'.Apollo_DB_Schema::_EVENT_PT.'_widget') !== FALSE
            && of_get_option( Apollo_DB_Schema::_EVENT_SEARCH_TYPE, 'rgt' ) == 'rgt'
        ) {
            $_GET['top-search-mobile'] = 1;
            include_once APOLLO_WIDGETS_DIR . '/search/templates/event-top.php' ;
        }
    }

    /* Thienld : functions for displaying organization search widget to top on mobile version */
    public static function displaySearchOrganizationWidgetOnMobile(){
        if ( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ORGANIZATION_PT)
            && (
                strpos($GLOBALS['wp']->matched_rule, 'organization/') !== false
            )
            && !Apollo_App::isDashboardPage()
            && is_active_widget(false,false,'apollo_search_'.Apollo_DB_Schema::_ORGANIZATION_PT.'_widget') !== FALSE
        ) {
            $_GET['top-search-mobile'] = 1;
            include_once APOLLO_WIDGETS_DIR . '/search/templates/organization-top.php' ;
        }
    }

    /* Thienld : functions for displaying public-art search widget to top on mobile version */
    public static function displaySearchPublicArtWidgetOnMobile(){
        if ( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_PUBLIC_ART_PT)
            && (
                strpos($GLOBALS['wp']->matched_rule, 'public-art/') !== false
            )
            && !Apollo_App::isDashboardPage()
            && is_active_widget(false,false,'apollo_search_'.str_replace('-','_',Apollo_DB_Schema::_PUBLIC_ART_PT).'_widget') !== FALSE
        ) {
            $_GET['top-search-mobile'] = 1;
            include_once APOLLO_WIDGETS_DIR . '/search/templates/public-art-top.php' ;
        }
    }

    /* Thienld : functions for displaying venue search widget to top on mobile version */
    public static function displaySearchVenueWidgetOnMobile(){

        /**
         * @author vulh
         * @ticket #19027 - 0002522: wpdev54 Customization - Custom venue slug
         */
        $venueSlug = self::getCustomSlugByModuleName('venue');

        if ( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_VENUE_PT)
            && (
                strpos($GLOBALS['wp']->matched_rule, $venueSlug.'/') !== false
            )
            && !Apollo_App::isDashboardPage()
            && is_active_widget(false,false,'apollo_search_'.Apollo_DB_Schema::_VENUE_PT.'_widget') !== FALSE
        ) {
            $_GET['top-search-mobile'] = 1;
            include_once APOLLO_WIDGETS_DIR . '/search/templates/venue-top.php' ;
        }
    }

    public static function getDefaultMarkersBusIcons() {
        $imagePath = get_template_directory_uri();
        return array(
            APL_Business_Module_Theme_Option::DINING_SELECT => array(
                'filterIcon' => $imagePath . '/assets/images/restaurant.png',
                'marker'    => $imagePath . '/assets/images/restaurant.png'
            ),
            APL_Business_Module_Theme_Option::ACCOMMODATION_SELECT => array(
                'filterIcon'    => $imagePath . '/assets/images/lodging_0star.png',
                'marker'    => $imagePath . '/assets/images/lodging_0star.png',
            ),
            APL_Business_Module_Theme_Option::BARS_CLUBS_SELECT => array(
                'filterIcon'    => $imagePath . '/assets/images/bar_coktail.png',
                'marker'    => $imagePath . '/assets/images/bar_coktail.png',
            ),
            APL_Business_Module_Theme_Option::LOCAL_BUSINESS_SELECT => array(
                'filterIcon'    => $imagePath . '/assets/images/icon-smallbiz.png',
                'marker'    => $imagePath . '/assets/images/icon-smallbiz.png',
            ),
            APL_Business_Module_Theme_Option::GOOGLE_MAP_ICON => array(

                'marker'    => $imagePath . '/assets/images/map-marker-icon.png',
            ),
            APL_Business_Module_Theme_Option::YOUR_LOCATION_ICON => array(

                'marker'    => $imagePath . '/assets/images/map-marker.png',
            ),
        );
    }

    /** Author : ThienLD
     ** Handle avoiding and prevent SQL Injection attack on search form and submitting form data for all pages on frontend area
     * - Things need to do in this task:
     *   + search form protection
     *   + submit form protection
     *   + ajax-call protection
     *   + double-check function get_query_var about avoiding sql injection (protected and prevented by wordpress parse_query function)
     *  - How to protect them
     *   + write function to retrieve all $_POST or $_GET per request to server side.
     *   + with each value -> check it with each data-type and force them to correct those data-type and assign back to value of that $_POST or $_GET
     *   + To escape it -> use the private function definition and call --->  $wpdb->escape_by_ref($string)
     *   + And esc_like for if key = keyword -> serve for searching form.
     **/

    public static function sqlInjectionHandler(){
        if(isset($_POST)){
            self::sqlInjectionHandlerByPostRequest($_POST);
        }

        if(isset($_GET)){
            self::sqlInjectionHandlerByGetRequest($_GET);
        }
    }

    public static function sqlInjectionHandlerByPostRequest(&$post){
        if(!empty($post) && is_array($post)){
            foreach($post as $k => &$v){
                if(is_array($v) && !empty($v)){
                    foreach($v as $ck => &$cv){
                        $cv = self::sqlInjectionSanitizeFieldValue($ck,$cv);
                    }
                } else {
                    $v = self::sqlInjectionSanitizeFieldValue($k,$v);
                }
            }
        }
    }

    public static function sqlInjectionHandlerByGetRequest(&$get){
        if(!empty($get) && is_array($get)){
            foreach($get as $k => &$v){
                if(is_array($v) && !empty($v)){
                    foreach($v as $ck => &$cv){
                        $cv = self::sqlInjectionSanitizeFieldValue($ck,$cv);
                    }
                } else {
                    $v = self::sqlInjectionSanitizeFieldValue($k,$v);
                }
            }
        }
    }

    public static function sqlInjectionSanitizeFieldValue($key, $value){
        global $wpdb;
        if(empty($value)){
            return $value;
        }
        if(is_array($value)){
            return $value;
        }
        if(is_int($value)){
            return intval($value);
        }
        if(is_float($value)){
            return floatval($value);
        }
        if(is_double($value)){
            return doubleval($value);
        }
        if($key == 'keyword'){
            return $wpdb->esc_like($value);
        }
        $wpdb->escape_by_ref($value);
        return $value;
    }


    /**
     * @author TriLM
     * @return int
     */
    public static function configDateRange(){
        $dateRangeOption = of_get_option(Apollo_DB_Schema::_APL_EVENT_ONGOING_DATE_RANGE);
        if(!empty($dateRangeOption)){
           return $dateRangeOption;
        }
        return Apollo_DB_Schema::_APL_EVENT_NUMBER_MONTH_CHANGE_MODE_CALENDAR;
    }

    public static function orderDateRange(){
        return 30*self::configDateRange();
    }

    /**
     * Is on going eveng
     *
     * @param $startDate int
     * @param $endDate int
     *
     * @return boolean
     */
    public static function isOngoing($startDate, $endDate) {
        return (($endDate - $startDate) / (24*3600*30)) >= self::configDateRange();
    }

    public static function enableMappingRegionZipSelection() {
        return of_get_option(Apollo_DB_Schema::_APL_FILTERING_BY_REGION, false);
    }

    /**
     * @param string $post_id
     * @param $blogType
     * @author Truonghn <truong.hoangnhat@elinext.com>
     */

    public static function apl_get_blog_categories($post_id = '', $blogType)
    {
        $allCategories = get_the_terms($post_id, 'category');
        switch($blogType){
            case "parent-only":
                $categoriesHTML = self::apl_get_blog_categories_parent($allCategories);
                break;
            case "child-only":
                $categoriesHTML = self::apl_get_blog_categories_child($allCategories);
                break;
            case "parent-child":
                $categoriesHTML = get_the_category_list( '<a class="coma">,&nbsp;</a>', '', $post_id);
                break;
            default:
                $categoriesHTML = get_the_category_list( '<a class="coma">,&nbsp;</a>', '', $post_id);
                break;
        }
        echo $categoriesHTML;
    }

    /**
     * @param $categories
     * @return string
     * @author Truonghn <truong.hoangnhat@elinext.com>
     */

    public static function apl_get_blog_categories_parent($categories)
    {
        $resultHTML = "";
        if(!empty($categories)){
            $result = array();
            foreach ($categories as $category) {
                if ($category->parent == 0) {
                    $termLink = get_term_link($category,'category');
                    $result[] = ' <a href="' . $termLink . '" title="' . $category->name . '">  ' . $category->name . '</a> ';
                }
            }
            $resultHTML = is_array($result) && !empty($result) ? implode('<a class="coma">,&nbsp;</a> ', $result) : "";
        }
        return $resultHTML;
    }

    /**
     * @param $categories
     * @return string
     * @author Truonghn <truong.hoangnhat@elinext.com>
     */

    public static function apl_get_blog_categories_child($categories)
    {
        $resultHTML = "";
        if(!empty($categories)){
            $result = array();
            foreach ($categories as $category) {
                if ($category->parent > 0) {
                    $termLink = get_term_link($category,'category');
                    $result[] = ' <a href="' . $termLink . '" title="' . $category->name . '">  ' . $category->name . '</a> ';
                }
            }
            $resultHTML = is_array($result) && !empty($result) ? implode('<a class="coma">,&nbsp;</a> ', $result) : "";
            $resultHTML = empty($resultHTML) ? self::apl_get_blog_categories_parent($categories) : $resultHTML;
        }
        return $resultHTML;
    }

    /**
     * @param $min
     * @param $max
     * @return mixed
     * @author Truonghn <truong.hoangnhat@elinext.com>
     */

    public static  function apl_crypto_rand_secure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }

    /**
     * @param int $length
     * @return string
     * @author Truonghn <truong.hoangnhat@elinext.com>
     */

    public  static  function apl_getToken($length = 40 )
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet) - 1;
        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[self::apl_crypto_rand_secure(0, $max)];
        }
        return $token;
    }

    /**
     * @param $default
     * @param string $type
     * @return bool
     * @author Truonghn <truong.hoangnhat@elinext.com>
     */

    public static  function aplGetPageSize(  $default , $type = ""){
        if ( empty( $type)) {
            return $default;
        }
        $sizeDefault = of_get_option(Apollo_DB_Schema::_APL_NUMBER_ITEMS_FIRST_PAGE, $default);
        $size = of_get_option($type, $sizeDefault);

        return $size ;
    }

    /**
     * get classified IDs , which has expired_date unexpired
     * @return array
     * @author Truonghn <truong.hoangnhat@elinext.com>
     */
    public static function getUnexpiredClassifiedIds(){
        $classifiedIds = [];
        global $wpdb;
        $_current_date = current_time('Y-m-d');
        $sql = "   SELECT em.apollo_classified_id
                    FROM " . $wpdb->{Apollo_Tables::_APL_CLASSIFIED_META} . " em
                    WHERE  em.meta_key = '" . Apollo_DB_Schema::_APL_CLASSIFIED_EXP_DATE . "'
                        AND em.meta_value >= '{$_current_date}' ";
        $classifieds =  $wpdb->get_results($sql);
        if ( !empty($classifieds) ) {
            foreach ( $classifieds as $classified ){
                $classifiedIds[] = $classified->apollo_classified_id;
            }
        }

        return $classifiedIds;
    }

    /**
     * @author Trilm
     */
    public static function get_post_info($slug = '',$post_type =  'post'){
        $number_of_post  = 1;
        $the_slug = $slug;
        $args = array(
            'name'        => $the_slug,
            'post_type'   => $post_type,
            'post_status' => 'publish',
            'numberposts' => $number_of_post
        );
        $posts = get_posts($args);
        if(isset($posts[0]))
        {
            return $posts[0];
        }
        return null;
    }

    /**
     * @author Trilm
     * disable event with meta = private
     */
    public static function hidePrivateEvent($sqlString, $tablePrefix = ''){

        global $wpdb, $aplPrivateEventIDs;
        $tableName = $wpdb->posts;
        if($tablePrefix != ''){
            $tableName = $tablePrefix;
        }

        /**
         * @Ticket #15809
        */
        $aplPrivateEventIDs = maybe_unserialize(get_option('apollo_private_events'));
        if (! $aplPrivateEventIDs) {

            // todo: should optimize  \'%_event_private";s:3:"yes"%\'. It will speed down the query
            $sql = '
            SELECT em.apollo_event_id
                    FROM '.$wpdb->{Apollo_Tables::_APOLLO_EVENT_META }.' em
                    WHERE
                        ( em.meta_value =  "yes"  AND em.meta_key = "'.Apollo_DB_Schema::_E_PRIVATE.'" )
                        OR ( em.meta_key = "_apollo_event_data" AND em.meta_value LIKE  \'%_event_private";s:3:"yes"%\' )
                    GROUP BY em.apollo_event_id
            ';

            $eventIdsResult = $wpdb->get_col($sql);
            $eventIdsResult = empty($eventIdsResult) ? array(-1): $eventIdsResult;

            $GLOBALS['aplPrivateEventIDs'] = $eventIdsResult;

            /**
             * @Ticket #15809
             */
            update_option('apollo_private_events', maybe_serialize($aplPrivateEventIDs), 'no');
        }

        if ($aplPrivateEventIDs) {
            $sqlString .= ' AND '.$tableName.'.ID NOT IN ( '.implode(",", $aplPrivateEventIDs).' )';
        }

        return $sqlString;
    }

    /**
     * @author Trilm
     * @return string
     */
    public static function checkEnableBusiness($select  = 'orgMeta.meta_value'){
        global $wpdb;
        $query = '
             SELECT  '.$select.'
                            FROM '.$wpdb->{Apollo_Tables::_APL_ORG_META}.' AS orgMeta
                                LEFT JOIN  '.$wpdb->{Apollo_Tables::_APL_ORG_META}.' AS orgMeta1
                                ON orgMeta.apollo_organization_id =  orgMeta1.apollo_organization_id
                                LEFT JOIN '.$wpdb->posts.' p on orgMeta1.apollo_organization_id = p.ID
                            WHERE
                                orgMeta.meta_key = \'_apollo_org_business\'
                                AND orgMeta1.meta_key = \'_apl_org_data\'
                                AND orgMeta1.meta_value LIKE \'%"_org_business";s:3:"yes"%\'
                                AND p.post_status = "publish"
                            GROUP BY '.$select.'
        ';
        return $query;
    }

    /**
     * @author: Trilm
     * @param $key
     * @param $id
     * @return string
     */
    public static function convertIdToName($key,$id,$postType=''){
        $text = '';
        if($key != 'is_discount' && $key != 'top-search-mobile-hor' && $key != '' && $key != 'page' && $key != 'onepage' && $key != 'save_lst_list'  && $id != '' && $postType !='' && $key != 'do_search'){
            if($key == 'term'){
                $term = get_term_by('id',$id,$postType.'-type');
                $text  = $term->name;
            }
            elseif($key == 'event_location' ||  $key == 'event_org' ){
                $obj = get_post($id);
                $text = $obj->post_title;
            }elseif ($key == 'event_neighborhood') {
                /** @Ticket @16482 - Neighborhood */
                $text = APL_Lib_Territory_Neighborhood::getNameById($id);
            }
            else{
                $text = $id;
            }

        }
        return $text;
    }

    /**
     * @param $access
     * @return string
    */
    public static function convertAccessToText($access) {
        $result = '';
        if (!empty($access)) {
            $text = array();
            global $apollo_event_access;
            foreach ($access as $item) {
                if(isset($apollo_event_access[$item])) {
                    $text[] = $apollo_event_access[$item];
                }
            }
            if(sizeof($text) > 0){
                $result = implode("+", $text);
            }
        }
        return $result;
    }

    /**
     * @author Trim
     *
     */
    public static function insertStatusRule($postType = 'event'){
        $status = 'pending';
        $key = Apollo_DB_Schema::_ENABLE_MODULE_TYPE_BYPASS_APPROVAL_PROCESS_TEMPLATE;
        $key = sprintf($key,$postType);
        if (of_get_option($key, false) == '1')   {
            $status = 'publish';
        }
        return $status;
    }

    /**
     * @author Trim
     * Render tracking script
     */
    public static function renderTrackingScript(){
        $fileTrackingScript = get_html_file_abs_path( Apollo_DB_Schema::_HEADER_TRACKING_SCRIPT, 'js' );
        $trackingScript = render_php_to_string($fileTrackingScript  );
        if(!empty($trackingScript)){
            return $trackingScript;
        }
        return '';
    }

    /**
     * Render script in body tag
     * @Ticket #15370
     * @return string
     */
    public static function renderScriptInBodyTag() {
        $fileScript = get_html_file_abs_path(Apollo_DB_Schema::_SCRIPT_IN_BODY_TAG, 'js');
        $script = render_php_to_string($fileScript);
        return !empty($script) ? $script : '';
    }

    /**
     * Reformat date from m-d-Y to Y-m-d (standard format)
     *
     * @param string $date
     *
     * @ticket #11487
     * @return string
     */
    public static function formatDateFromMDYToYMD($date)
    {
        $startDateItems = explode('-', $date);
        return date('Y-m-d', mktime(0, 0, 0, $startDateItems[0], $startDateItems[1], $startDateItems[2]));
    }

    /**
     * Check value is date
     * @param $date
     * @param string $format
     * @return bool
     */
    public static function checkIsDateTime($date, $format = 'Y-m-d') {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    /**
     * Check current theme need to re-activation or not
     *
     * @ticket #11493
     * @return bool
     */
    public static function checkReactivation()
    {
        return get_option(Apollo_DB_Schema::_APL_RE_ACTIVATION_THEME_VERSION) != APOLLO_CURRENT_REACTIVE_THEME_VERSION;
    }

    /**
     * Check current user is one of Artsopolis network administrators.
     *
     * @ticket #11637
     * @return bool
     */
    public static function checkArtsopolisNetworkAdmin()
    {
        $allowEmails = array(
            'administrator@artsopolis.com',
            'david.dasinger@gmail.com',
            'jeff@artsopolis.com',
            'minhtrius@gmail.com'
        );
        $currentUser = wp_get_current_user();

        if ( WP_ENV == 'production' && $currentUser && in_array($currentUser->user_email, $allowEmails) ) {
            return true;
        }
        return false;
    }

    /**
     * Display re-activation message
     *
     * @ticket #11493
     */
    public static function reActivationMessage()
    {
        if ( self::checkReactivation() && self::checkArtsopolisNetworkAdmin() ) {
            include APOLLO_ADMIN_DIR . '/_templates/partial/_re-activation-message.php';
        }
    }

    /**
     * Get the menu ID of a module
     *
     * @param string $module
     *
     * @return int
     * @ticket #15216 - [CF] 20180305 - [Admin] Adding pending counter items into the left menu like Comments - item 4
     */
    public static function getMenuIdOfAModule($module = '')
    {
        global $menu;

        foreach ($menu as $key => $item) {
            $item = isset($item[2]) ? $item[2] : '';
            if (strpos($item, $module)) {
                return (int) $key;
            }
        }

        return -1;
    }

    /**
     * @param string $sidebarId
     * @param bool $widgetName
     * Get widget id in sidebar area (Example: get ids of appllo_custom_widget in apl-sidebar-post, ...)
     * @return array
     */
    public static function getWidgetId($sidebarId = '', $widgetName = false){
        global $sidebars_widgets;
        $sidebarWidget = isset($sidebars_widgets[$sidebarId]) ? $sidebars_widgets[$sidebarId] : array();
        $widgetIDs = array();

        if (!empty($sidebarWidget)) {
            foreach ($sidebarWidget as $sidebar) {
                if (preg_match("/\b$widgetName\b/", $sidebar)) {
                    $widgetIDs[] = explode("-", $sidebar)[1];
                }
            }
        }

        return $widgetIDs;
    }


    /**
     * Move function from class Apollo_ABS_Module to here
     * Render category tree
     * @param $items
     * @param string $parent_key
     * @param string $self_id
     * @return array|mixed
     */
    public static function build_tree_category($items, $parent_key='parent', $self_id = 'term_id') {

        if(empty($items))
            return array();

        $childs = array();

        foreach($items as &$item) {
            $childs[$item->$parent_key][] = &$item;
            unset($item);
        }


        foreach($items as &$item)  {
            if (isset($childs[$item->$self_id])) {
                $item->childs = $childs[$item->$self_id];
            }
        }

        return $childs[0];
    }

    /**
     * Get Current term
     * @return array|false|mixed|WP_Term
     */
    public static function getCurrentTerm(){
        $currentTerm = get_query_var('term');
        $currentTerm = get_term_by('slug', $currentTerm, get_query_var( 'taxonomy' ));
        $GLOBALS['apl_current_term_object'] = $currentTerm;
        return $currentTerm;
    }

    /**
     * @ticket #18127: News Module
     * @param string $post_id
     */
    public static function apl_get_news_categories($post_id = '')
    {
        $allCategories = get_the_terms($post_id, 'news-type');
        $newsCategoriesType = of_get_option( APL_News_Config::_NEWS_CATEGORY_TYPES, 'parent-only');
        $html = '<a>' . __('Uncategorized', 'apollo') . '</a>';

        if(!empty($allCategories)){
            switch($newsCategoriesType){
                case "parent-only":
                    $terms = array();
                    foreach($allCategories as $term) {
                        if (!$term->parent) {
                            $terms[] = $term;
                        }
                    }
                    break;
                case "child-only":
                    $terms = array();
                    foreach($allCategories as $term) {
                        if ($term->parent) {
                            $terms[] = $term;
                        }
                    }
                    break;
                default:
                    $terms = $allCategories;
                    break;
            }

            $html = '';
            foreach($terms as $index => $term ){
                if($index) $html .= '<a class="coma">,&nbsp;</a>';
                $html .='<a href="'.get_term_link($term->slug, 'news-type').'">'.$term->name.'</a>';
            }
        }

        echo $html;
    }

    /**
     * @ticket #18497: 0002504: Arts Education Customizations - Change home link on all page custom arts ed masthead
     * @param $module
     * @return bool|string
     */
    public static function changeBreadcrumbLink($module){

        switch($module){
            case 'education':
                $enableOverrideBreadcrumb = of_get_option(APL_Theme_Option_Site_Config_SubTab::_EDUCATOR_ENABLE_OVERRIDE_BREADCRUMB_LINK, 0);
                $breadcrumbLink = of_get_option(APL_Theme_Option_Site_Config_SubTab::_EDUCATOR_BREADCRUMB_LINK, '');
                break;
            case 'post':
                $enableOverrideBreadcrumb = of_get_option(APL_Theme_Option_Site_Config_SubTab::_BLOG_ENABLE_OVERRIDE_BREADCRUMB_LINK, 0);
                $breadcrumbLink = of_get_option(APL_Theme_Option_Site_Config_SubTab::_BLOG_BREADCRUMB_LINK, '');
                break;
            default:
                $enableOverrideBreadcrumb = false;
                $breadcrumbLink = '';
        }

        if($enableOverrideBreadcrumb && !empty($breadcrumbLink)){
            return $breadcrumbLink;
        }

        return home_url();
    }

    /**
     * @ticket #18515: Add the options allow control the transition speed of the slides.
     * @param $module
     * @return bool
     */
    public static function getSlideShowSpeed($module){
        $option = "_apl_{$module}_slideshow_speed";

        return of_get_option($option, 5000);
    }

    public static function getAllCountries() {
        return array(
            'AF' => __( 'Afghanistan', 'apollo' ),
            'AL' => __( 'Albania', 'apollo' ),
            'DZ' => __( 'Algeria', 'apollo' ),
            'AS' => __( 'American Samoa', 'apollo' ),
            'AD' => __( 'Andorra', 'apollo' ),
            'AO' => __( 'Angola', 'apollo' ),
            'AI' => __( 'Anguilla', 'apollo' ),
            'AQ' => __( 'Antarctica', 'apollo' ),
            'AG' => __( 'Antigua And Barbuda', 'apollo' ),
            'AR' => __( 'Argentina', 'apollo' ),
            'AM' => __( 'Armenia', 'apollo' ),
            'AW' => __( 'Aruba', 'apollo' ),
            'AU' => __( 'Australia', 'apollo' ),
            'AT' => __( 'Austria', 'apollo' ),
            'AZ' => __( 'Azerbaijan', 'apollo' ),
            'BS' => __( 'Bahamas', 'apollo' ),
            'BH' => __( 'Bahrain', 'apollo' ),
            'BD' => __( 'Bangladesh', 'apollo' ),
            'BB' => __( 'Barbados', 'apollo' ),
            'BY' => __( 'Belarus', 'apollo' ),
            'BE' => __( 'Belgium', 'apollo' ),
            'BZ' => __( 'Belize', 'apollo' ),
            'BJ' => __( 'Benin', 'apollo' ),
            'BM' => __( 'Bermuda', 'apollo' ),
            'BT' => __( 'Bhutan', 'apollo' ),
            'BO' => __( 'Bolivia', 'apollo' ),
            'BA' => __( 'Bosnia And Herzegowina', 'apollo' ),
            'BW' => __( 'Botswana', 'apollo' ),
            'BV' => __( 'Bouvet Island', 'apollo' ),
            'BR' => __( 'Brazil', 'apollo' ),
            'IO' => __( 'British Indian Ocean Territory', 'apollo' ),
            'BN' => __( 'Brunei Darussalam', 'apollo' ),
            'BG' => __( 'Bulgaria', 'apollo' ),
            'BF' => __( 'Burkina Faso', 'apollo' ),
            'BI' => __('Burundi', 'apollo'),
            'KH' => __('Cambodia', 'apollo'),
            'CM' => __('Cameroon', 'apollo'),
            'CA' => __('Canada', 'apollo'),
            'CV' => __('Cape Verde', 'apollo'),
            'KY' => __('Cayman Islands', 'apollo'),
            'CF' => __('Central African Republic', 'apollo'),
            'TD' => __('Chad', 'apollo'),
            'CL' => __('Chile', 'apollo'),
            'CN' => __('China', 'apollo'),
            'CX' => __('Christmas Island', 'apollo'),
            'CC' => __('Cocos (Keeling) Islands', 'apollo'),
            'CO' => __('Colombia', 'apollo'),
            'KM' => __('Comoros', 'apollo'),
            'CG' => __('Congo', 'apollo'),
            'CD' => __('Congo, The Democratic Republic Of The', 'apollo'),
            'CK' => __('Cook Islands', 'apollo'),
            'CR' => __('Costa Rica', 'apollo'),
            'CI' => __('Cote D\'Ivoire', 'apollo'),
            'HR' => __('Croatia (Local Name: Hrvatska)', 'apollo'),
            'CU' => __('Cuba', 'apollo'),
            'CY' => __('Cyprus', 'apollo'),
            'CZ' => __('Czech Republic', 'apollo'),
            'DK' => __('Denmark', 'apollo'),
            'DJ' => __('Djibouti', 'apollo'),
            'DM' => __('Dominica', 'apollo'),
            'DO' => __('Dominican Republic', 'apollo'),
            'TL' => __('Timor-Leste (East Timor)', 'apollo'),
            'EC' => __('Ecuador', 'apollo'),
            'EG' => __('Egypt', 'apollo'),
            'SV' => __('El Salvador', 'apollo'),
            'GQ' => __('Equatorial Guinea', 'apollo'),
            'ER' => __('Eritrea', 'apollo'),
            'EE' => __('Estonia', 'apollo'),
            'ET' => __('Ethiopia', 'apollo'),
            'FK' => __('Falkland Islands (Malvinas)', 'apollo'),
            'FO' => __('Faroe Islands', 'apollo'),
            'FJ' => __('Fiji', 'apollo'),
            'FI' => __('Finland', 'apollo'),
            'FR' => __('France', 'apollo'),
            'FX' => __('France, Metropolitan', 'apollo'),
            'GF' => __('French Guiana', 'apollo'),
            'PF' => __('French Polynesia', 'apollo'),
            'TF' => __('French Southern Territories', 'apollo'),
            'GA' => __('Gabon', 'apollo'),
            'GM' => __('Gambia', 'apollo'),
            'GE' => __('Georgia', 'apollo'),
            'DE' => __('Germany', 'apollo'),
            'GH' => __('Ghana', 'apollo'),
            'GI' => __('Gibraltar', 'apollo'),
            'GR' => __('Greece', 'apollo'),
            'GL' => __('Greenland', 'apollo'),
            'GD' => __('Grenada', 'apollo'),
            'GP' => __('Guadeloupe', 'apollo'),
            'GU' => __('Guam', 'apollo'),
            'GT' => __('Guatemala', 'apollo'),
            'GN' => __('Guinea', 'apollo'),
            'GW' => __('Guinea-Bissau', 'apollo'),
            'GY' => __('Guyana', 'apollo'),
            'HT' => __('Haiti', 'apollo'),
            'HM' => __('Heard And Mc Donald Islands', 'apollo'),
            'VA' => __('Holy See (Vatican City State)', 'apollo'),
            'HN' => __('Honduras', 'apollo'),
            'HK' => __('Hong Kong', 'apollo'),
            'HU' => __('Hungary', 'apollo'),
            'IS' => __('Iceland', 'apollo'),
            'IN' => __('India', 'apollo'),
            'ID' => __('Indonesia', 'apollo'),
            'IR' => __('Iran (Islamic Republic Of)', 'apollo'),
            'IQ' => __('Iraq', 'apollo'),
            'IE' => __('Ireland', 'apollo'),
            'IL' => __('Israel', 'apollo'),
            'IT' => __('Italy', 'apollo'),
            'JM' => __('Jamaica', 'apollo'),
            'JP' => __('Japan', 'apollo'),
            'JO' => __('Jordan', 'apollo'),
            'KZ' => __('Kazakhstan', 'apollo'),
            'KE' => __('Kenya', 'apollo'),
            'KI' => __('Kiribati', 'apollo'),
            'KP' => __('Korea, Democratic People\'s Republic Of', 'apollo'),
            'KR' => __('Korea, Republic Of', 'apollo'),
            'KW' => __('Kuwait', 'apollo'),
            'KG' => __('Kyrgyzstan', 'apollo'),
            'LA' => __('Lao People\'s Democratic Republic', 'apollo'),
            'LV' => __('Latvia', 'apollo'),
            'LB' => __('Lebanon', 'apollo'),
            'LS' => __('Lesotho', 'apollo'),
            'LR' => __('Liberia', 'apollo'),
            'LY' => __('Libyan Arab Jamahiriya', 'apollo'),
            'LI' => __('Liechtenstein', 'apollo'),
            'LT' => __('Lithuania', 'apollo'),
            'LU' => __('Luxembourg', 'apollo'),
            'MO' => __('Macau', 'apollo'),
            'MK' => __('Macedonia, Former Yugoslav Republic Of', 'apollo'),
            'MG' => __('Madagascar', 'apollo'),
            'MW' => __('Malawi', 'apollo'),
            'MY' => __('Malaysia', 'apollo'),
            'MV' => __('Maldives', 'apollo'),
            'ML' => __('Mali', 'apollo'),
            'MT' => __('Malta', 'apollo'),
            'MH' => __('Marshall Islands', 'apollo'),
            'MQ' => __('Martinique', 'apollo'),
            'MR' => __('Mauritania', 'apollo'),
            'MU' => __('Mauritius', 'apollo'),
            'YT' => __('Mayotte', 'apollo'),
            'MX' => __('Mexico', 'apollo'),
            'FM' => __('Micronesia, Federated States Of', 'apollo'),
            'MD' => __('Moldova, Republic Of', 'apollo'),
            'MC' => __('Monaco', 'apollo'),
            'MN' => __('Mongolia', 'apollo'),
            'ME' => __('Montenegro', 'apollo'),
            'MS' => __('Montserrat', 'apollo'),
            'MA' => __('Morocco', 'apollo'),
            'MZ' => __('Mozambique', 'apollo'),
            'MM' => __('Myanmar', 'apollo'),
            'NA' => __('Namibia', 'apollo'),
            'NR' => __('Nauru', 'apollo'),
            'NP' => __('Nepal', 'apollo'),
            'NL' => __('Netherlands', 'apollo'),
            'AN' => __('Netherlands Antilles', 'apollo'),
            'NC' => __('New Caledonia', 'apollo'),
            'NZ' => __('New Zealand', 'apollo'),
            'NI' => __('Nicaragua', 'apollo'),
            'NE' => __('Niger', 'apollo'),
            'NG' => __('Nigeria', 'apollo'),
            'NU' => __('Niue', 'apollo'),
            'NF' => __('Norfolk Island', 'apollo'),
            'MP' => __('Northern Mariana Islands', 'apollo'),
            'NO' => __('Norway', 'apollo'),
            'OM' => __('Oman', 'apollo'),
            'PK' => __('Pakistan', 'apollo'),
            'PW' => __('Palau', 'apollo'),
            'PA' => __('Panama', 'apollo'),
            'PG' => __('Papua New Guinea', 'apollo'),
            'PY' => __('Paraguay', 'apollo'),
            'PE' => __('Peru', 'apollo'),
            'PH' => __('Philippines', 'apollo'),
            'PN' => __('Pitcairn', 'apollo'),
            'PL' => __('Poland', 'apollo'),
            'PT' => __('Portugal', 'apollo'),
            'PR' => __('Puerto Rico', 'apollo'),
            'QA' => __('Qatar', 'apollo'),
            'RE' => __('Reunion', 'apollo'),
            'RO' => __('Romania', 'apollo'),
            'RU' => __('Russian Federation', 'apollo'),
            'RW' => __('Rwanda', 'apollo'),
            'KN' => __('Saint Kitts And Nevis', 'apollo'),
            'LC' => __('Saint Lucia', 'apollo'),
            'VC' => __('Saint Vincent And The Grenadines', 'apollo'),
            'WS' => __('Samoa', 'apollo'),
            'SM' => __('San Marino', 'apollo'),
            'ST' => __('Sao Tome And Principe', 'apollo'),
            'SA' => __('Saudi Arabia', 'apollo'),
            'SN' => __('Senegal', 'apollo'),
            'RS' => __('Serbia', 'apollo'),
            'SC' => __('Seychelles', 'apollo'),
            'SL' => __('Sierra Leone', 'apollo'),
            'SG' => __('Singapore', 'apollo'),
            'SK' => __('Slovakia (Slovak Republic)', 'apollo'),
            'SI' => __('Slovenia', 'apollo'),
            'SB' => __('Solomon Islands', 'apollo'),
            'SO' => __('Somalia', 'apollo'),
            'ZA' => __('South Africa', 'apollo'),
            'GS' => __('South Georgia, South Sandwich Islands', 'apollo'),
            'ES' => __('Spain', 'apollo'),
            'LK' => __('Sri Lanka', 'apollo'),
            'SH' => __('St. Helena', 'apollo'),
            'PM' => __('St. Pierre And Miquelon', 'apollo'),
            'SD' => __('Sudan', 'apollo'),
            'SR' => __('Suriname', 'apollo'),
            'SJ' => __('Svalbard And Jan Mayen Islands', 'apollo'),
            'SZ' => __('Swaziland', 'apollo'),
            'SE' => __('Sweden', 'apollo'),
            'CH' => __('Switzerland', 'apollo'),
            'SY' => __('Syrian Arab Republic', 'apollo'),
            'TW' => __('Taiwan', 'apollo'),
            'TJ' => __('Tajikistan', 'apollo'),
            'TZ' => __('Tanzania, United Republic Of', 'apollo'),
            'TH' => __('Thailand', 'apollo'),
            'TG' => __('Togo', 'apollo'),
            'TK' => __('Tokelau', 'apollo'),
            'TO' => __('Tonga', 'apollo'),
            'TT' => __('Trinidad And Tobago', 'apollo'),
            'TN' => __('Tunisia', 'apollo'),
            'TR' => __('Turkey', 'apollo'),
            'TM' => __('Turkmenistan', 'apollo'),
            'TC' => __('Turks And Caicos Islands', 'apollo'),
            'TV' => __('Tuvalu', 'apollo'),
            'UG' => __('Uganda', 'apollo'),
            'UA' => __('Ukraine', 'apollo'),
            'AE' => __('United Arab Emirates', 'apollo'),
            'GB' => __('United Kingdom', 'apollo'),
            'US' => __('United States', 'apollo'),
            'UM' => __('United States Minor Outlying Islands', 'apollo'),
            'UY' => __('Uruguay', 'apollo'),
            'UZ' => __('Uzbekistan', 'apollo'),
            'VU' => __('Vanuatu', 'apollo'),
            'VE' => __('Venezuela', 'apollo'),
            'VN' => __('Vietnam', 'apollo'),
            'VG' => __('Virgin Islands (British)', 'apollo'),
            'VI' => __('Virgin Islands (U.S.)', 'apollo'),
            'WF' => __('Wallis And Futuna Islands', 'apollo'),
            'EH' => __('Western Sahara', 'apollo'),
            'YE' => __('Yemen', 'apollo'),
            'YU' => __('Yugoslavia', 'apollo'),
            'ZM' => __('Zambia', 'apollo'),
            'ZW' => __('Zimbabwe', 'apollo'),
        );
    }

    public static function renderCountryHtml($countries = array(), $selected = 'US', $name = 'country') {
        $selected = empty($selected) ? 'US' : $selected;
        $html = '<select name="'.$name.'" id="user-country">';
        if (!empty($countries)) {
            foreach ($countries as $key => $value) {
                $countrySelected = '';
                if($selected == $key)
                {
                    $countrySelected = 'selected';
                }
                $html .= '<option value="'.$key.'" '.$countrySelected.'>'. $value .'</option>';
            }
        }

        $html .= '</select>';
        return ($html);
    }

    /**
     * @ticket #19131: Octave Theme - Change all detail page section labels same the section labels on the homepage - item 2
     * @param $text
     * @param bool $icon
     * @return string
     */
    public static function renderSectionLabels($text, $icon = false, $attribute = array()){

        $optLabels = apply_filters('oc_get_opt_section_labels', 'default');

        $attributeText = !empty($attribute) ? implode(' ', $attribute) : '';

        if($optLabels == 'line'){
            $sectionLabel = "<h4 class='apl-detail-title-line' $attributeText><span>$text</span>";
        }
        else{
            $sectionLabel = "<h4 $attributeText>$text";
        }

        $sectionLabel .= $icon ? $icon : '';

        $sectionLabel .= "</h4>";

        return $sectionLabel;
    }

    /**
     * @ticket #19131: Octave Theme - Change all detail page section labels same the section labels on the homepage
     * style nav-tab (upcomming events, pass events)
     * @return string
     */
    public static function getClassSectionLabels(){
        $optLabels = apply_filters('oc_get_opt_section_labels', 'default');
        return $optLabels == 'default' ? '' : 'apl-detail-title-line';
    }

}

function apl_sort_agency_educators_by_alpha($a, $b) {
    $p1 = get_post($a->edu_id);
    $p2 = get_post($b->edu_id);
    return strcasecmp($p1->post_title , $p2->post_title);
}

function aplDebug($value, $stop_process = 0) {
    echo '<pre>';
    print_r($value);
    echo '</pre>';
    if($stop_process == 1){
        die();
    }
}

function aplDebugRedirect() {
    error_reporting(E_ALL | E_WARNING | E_NOTICE);
    ini_set('display_errors', TRUE);
    flush();
}

/**
 * Clear header
 * add_action('init', 'appOutputBuffer');
 */
function appOutputBuffer() {
    ob_start();
} // soi_output_buffer


function aplDebugFile($value = '', $header = ''){
    $date = date('Y/m/d H:i:s'). ': ';
    $location = ($header != '' ? $header.': ' : '');
    $valueToFile = json_encode($value);
    $upload_dir = wp_upload_dir();
    $fp = fopen($upload_dir['basedir'] . '/apollo-theme-debug-file.txt','a');
    fwrite($fp, $date. $location. $valueToFile."\r\n");
    fclose($fp);
}


//TriLM sort org by alpha
function apl_sort_agency_org_by_alpha($a, $b) {
    $p1 = get_post($a->org_id);
    $p2 = get_post($b->org_id);
    return strcasecmp($p1->post_title , $p2->post_title);
}

//TriLM covert php to iCal
class Apollo_Ical_calendar{
    public static function getTimeZone(){
        $timeZone = get_option('timezone_string');
        if(empty($timeZone)){
            $timeZone = get_option('gmt_offset');
            if($timeZone == 0){
                $timeZone = 'UTC';
                return $timeZone;
            }
            return self::generateGMT($timeZone);
        }


        return  $timeZone;
    }
    public static function generateGMT($timeZone,$icsFile = false){
        $vowels = array(":", ".");
        $timeZone = str_replace($vowels,'',$timeZone);
        if($icsFile === false){
            if($timeZone > 0)
                return 'GMT+'.$timeZone.':00';
            return 'GMT'.$timeZone.':00';
        }


        $timeZone = str_replace('GMT','UTC',$timeZone);
        return $timeZone;
    }
    public static function export_list_time_to_ical($item = array(),$eventID = ''){
        $str = '';
        if($eventID != ''){
            $event = get_post($eventID);
            $event = get_event($event);
            $str .= self::export_to_ical($item,$event);
        }
        return  "BEGIN:VCALENDAR\nVERSION:2.0\n".$str."END:VCALENDAR";
    }
    public static function dateToCal($timestamp) {
        $timestamp = strtotime($timestamp." GMT");
        return date('Ymd\THis', $timestamp);
    }
    public static function export_to_ical($item,$event){
        $timeZone = self::getTimeZone();
        $timeZone = self::generateGMT($timeZone,true);

        $item = explode('/',$item);
        $timeFrom = isset($item[0])?$item[0]:0;
        $timeTo = isset($item[1])?$item[1]:0;

        $timeFromText = $timeFrom;
        $timeToText = $timeTo;
        $strTimeTo = '';
        if($timeTo != '')
            $strTimeTo = "DTEND;TZID=".$timeZone.":".$timeToText."\n";
        $dtStart = "DTSTART;TZID=".$timeZone.":".$timeFromText."\n";
        $ical =
            "BEGIN:VEVENT\nUID:calendar@".str_replace(' ','_',get_bloginfo('name'))."\n".$dtStart.$strTimeTo."SUMMARY:".$event->get_title()."\nLOCATION:".$event->getStrAddress()."\nDESCRIPTION:".wp_strip_all_tags($event->get_full_content())."\nPRIORITY:1\nEND:VEVENT\n";
        //set correct content-type-header
        return $ical;
    }
    public static function download_send_headers($filename) {
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }

}

//TriLM apollo map function
class Apollo_Map_Calc{
    public static function calcDistance($pos1 = array(), $pos2 = array()){
        $_latA = $pos1['lat'];
        $_latB = $pos2['lat'];

        $_lngA = $pos1['lng'];
        $_lngB = $pos2['lng'];

        $R = 6371;
        $latA = deg2rad($_latA);
        $latB = deg2rad($_latB);

        $d1 =  deg2rad($_latB-$_latA);
        $d2 =  deg2rad($_lngB-$_lngA);

        $a =    sin($d1/2) * sin($d1/2) +
                cos($latA) *   cos($latB) *
                sin($d2/2) * sin($d2/2);
        $c = 2 * atan2(sqrt($a),sqrt(1-$a));
        return $d = $R * $c;
    }

    public static function getRadiusFilter(){
        $unit = __('miles','Apollo');
        $radiusArray = array(
            '0.5' => '0.5'.$unit,
            '1' => '1'.$unit,
            '2' => '2'.$unit,
            '5' => '5'.$unit,
            '10' => '10'.$unit,
            '25' => '25'.$unit,
            '50' => '50'.$unit,
            '100' => '100'.$unit,
            '150' => '150'.$unit,
            '200' => '200'.$unit,
            '250' => '250'.$unit);
        return $radiusArray;

    }

    public static function getDefaultMarker() {
        $googleMapIcon = of_get_option(Apollo_DB_Schema::_GOOGLE_MAP_MARKER_ICON);
        if(empty($googleMapIcon) ){
            $googleMapIcon = '/wp-content/themes/apollo/assets/images/icon-location.png';
        }
        return $googleMapIcon;
    }

    public static function getMapOption(){
        $googleMapIcon = of_get_option(Apollo_DB_Schema::_GOOGLE_MAP_MARKER_ICON);
        $googleMapMyPositionIcon = of_get_option(Apollo_DB_Schema::_GOOGLE_MAP_MY_POSITION_MARKER_ICON);
        $googleMapIconWidth = of_get_option(Apollo_DB_Schema::_GOOGLE_MAP_MARKER_ICON_WIDTH);
        $googleMapIconHeight= of_get_option(Apollo_DB_Schema::_GOOGLE_MAP_MARKER_ICON_HEIGHT);
        $arrCoor = array();

        if(!isset($googleMapIcon) || empty($googleMapIcon) ){
            $googleMapIcon = '/wp-content/themes/apollo/assets/images/icon-location.png';
        }
        if(!isset($googleMapIconWidth) || empty($googleMapIconWidth) && $googleMapIconHeight!=0 ){
            $googleMapIconWidth = $googleMapIconHeight;
        }
        if(!isset($googleMapIconHeight) || empty($googleMapIconHeight) && $googleMapIconWidth!=0 ){
            $googleMapIconHeight = $googleMapIconWidth;
        }
        if(!isset($googleMapIconHeight) || empty($googleMapIconHeight) && !isset($googleMapIconHeight) || empty($googleMapIconHeight )){
            $googleMapIconHeight = $googleMapIconWidth = '20';
        }

        return array(
            'map_refreshed_time_from_server' => get_option(Apollo_DB_Schema::_PUBLIC_ART_MAP_REFRESH_TIME,false),
            'google_map_icon' => $googleMapIcon,
            'google_map_icon_width' =>  $googleMapIconWidth,
            'google_map_icon_height' =>  $googleMapIconHeight,
            'google_map_my_position_icon' =>  $googleMapMyPositionIcon,
            'google_map_default_address' => array(
                'lat' => isset($arrCoor['lat'])?$arrCoor['lat']:0,
                'lng' => isset($arrCoor['lng'])?$arrCoor['lng']:0,
            )
        );
    }

    public static function queryByRadius($data,$currentLat,$currentLng,$radius){
        if(!empty($radius) && !empty($currentLat) && !empty($currentLng)){
            foreach($data as $k =>  $item){

                $pos1 = array(
                    'lat' => $item['lat'],
                    'lng' => $item['lng'],
                );
                $pos2 = array(
                    'lat' => $currentLat,
                    'lng' => $currentLng,
                );
                $distance = Apollo_Map_Calc::calcDistance($pos1,$pos2);
                if($distance > $radius){
                    unset($data[$k]);
                }
            }
        }
        return $data;
    }

}

//Trilm apollo_static_sidebar
class Apollo_Static_Sidebar{

    public static function get_current_type(){

        if(get_query_var('_apollo_artist_search') === 'true') {
            return Apollo_DB_Schema::_ARTIST_PT;
        }

        if(get_query_var('_apollo_educator_search') === 'true' || get_query_var( '_apollo_program_search' ) === 'true' ) {
            return Apollo_DB_Schema::_EDUCATION;
        }

        //event here
        if ( get_query_var( '_apollo_event_search' ) === 'true'  ) {
            return Apollo_DB_Schema::_EVENT_PT;
        }
        //org here
        if ( get_query_var( '_apollo_org_search' ) === 'true'  ) {
            return Apollo_DB_Schema::_ORGANIZATION_PT;
        }
        //venue here
        if ( get_query_var( '_apollo_venue_search' ) === 'true'  ) {
            return Apollo_DB_Schema::_VENUE_PT;
        }
        //classified here
        if ( get_query_var( '_apollo_classified_search' ) === 'true'  ) {
            return Apollo_DB_Schema::_CLASSIFIED;
        }
        //public art here
        if ( get_query_var( '_apollo_public_art_search' ) === 'true'  ) {
            return Apollo_DB_Schema::_PUBLIC_ART_PT;
        }

        //Tihenld : add widget business here
        if ( get_query_var( '_apollo_business_search' ) === 'true'  ) {
            return Apollo_DB_Schema::_BUSINESS_PT;
        }

        $current_type = isset( $_GET['s'] ) ?  '' : get_post_type();

        // Check relative page of education
        $relative_education = false;
        if (is_page()) {
            global $post;
            $relative_education = $post && $post->post_name
                && ( $post->post_name == Apollo_Page_Creator::ID_TEACHER_EVALUATION_FORM
                    || $post->post_name == Apollo_Page_Creator::ID_SUBMIT_EVALUATION_DONE
                    || $post->post_name == Apollo_Page_Creator::ID_TEACHER_EVALUATION_FORM
                    || $post->post_name == Apollo_Page_Creator::ID_GRANT_EDUCATION_FORM
                );
        }

        $_educations = array(
            Apollo_DB_Schema::_EDUCATOR_PT,
            Apollo_DB_Schema::_PROGRAM_PT,
            Apollo_DB_Schema::_EDU_EVALUATION,
            Apollo_DB_Schema::_GRANT_EDUCATION,
            Apollo_DB_Schema::_APL_PROG_TAX_ART_DESC,
            Apollo_DB_Schema::_APL_PROG_TAX_CUL_ORIGIN,
            Apollo_DB_Schema::_APL_PROG_TAX_POP_SER,
            Apollo_DB_Schema::_APL_PROG_TAX_SUBJECT,
        );

        $_artists = array(
            Apollo_DB_Schema::_ARTIST_MEDIUM_TAX,
            Apollo_DB_Schema::_ARTIST_STYLE_TAX,
        );

        $_public_arts = array(
            Apollo_DB_Schema::_PUBLIC_ART_COL,
            Apollo_DB_Schema::_PUBLIC_ART_LOC,
            Apollo_DB_Schema::_PUBLIC_ART_MED,
        );

        /**
         * @ticket #18888: Missing the right bar widget when the taxonomies are empty item
         */
        global $apl_current_taxonomy_type;
        if (empty($current_type) && !empty($apl_current_taxonomy_type)) {
            $current_type = str_replace('-type', '', $apl_current_taxonomy_type);
        }

        if (in_array( $current_type , $_educations ) || $relative_education ) {
            return Apollo_DB_Schema::_EDUCATION;
        }

        if (in_array( $current_type , $_artists ) ) {
            return Apollo_DB_Schema::_ARTIST_PT;
        }

        if (in_array( $current_type , $_public_arts ) ) {
            return  Apollo_DB_Schema::_PUBLIC_ART_PT;
        }

        if(empty($current_type)) {
            $postType = get_query_var('post_type');
            $current_type = is_array($postType) ? (isset($postType[0]) ? $postType[0] : '') : $postType;
        }

        return $current_type;
    }

    public static function disable_sidebar(){
        $current_type = Apollo_Static_Sidebar::get_current_type();
        if ($current_type != Apollo_DB_Schema::_EVENT_PT){
            $sidebar_id =     Apollo_DB_Schema::_SIDEBAR_PREFIX . '-'.  str_replace('-', '_', $current_type);
        }else{
            $sidebar_id =     Apollo_DB_Schema::_SIDEBAR_PREFIX . '-primary';
        }
        unregister_sidebar($sidebar_id);
    }
}
