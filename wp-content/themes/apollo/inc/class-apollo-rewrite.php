<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


class Apollo_Rewrite {


    public function __construct() {

        $arr_action = array(
        );

        if(!empty($arr_action)) {
            foreach($arr_action as $action_name => $handler) {
                add_action($action_name, $handler, 10, 2);
            }
        }


        // Add filter specific for custome resources
        $arr_filter = array(
            'query_vars' => array(__CLASS__, 'apollo_add_query_vars'),
            'rewrite_rules_array' => array(__CLASS__, 'apollo_rewrite_rules_array'),
        );



        foreach($arr_filter as $filter_name => $handler) {
            add_filter($filter_name, $handler, 10, 2);
        }

        //Trilm add action
        add_action('wp_loaded',array(__CLASS__,'reset_permarklink'));
    }


    public static function reset_permarklink(){
        $last_reset_rule  = get_option(Apollo_DB_Schema::_APL_RESET_PERMARK_LINK);
        $time = Apollo_DB_Schema::_APL_UPDATED_PERMARK_LINK_VER;
        if(empty($last_reset_rule) || $last_reset_rule != $time){
            global $wp_rewrite;
            $wp_rewrite->flush_rules();
            update_option(Apollo_DB_Schema::_APL_RESET_PERMARK_LINK,$time);
        }
    }


    public static function apollo_rewrite_rules_array($old_rules)
    {

        $haystack  = $_SERVER['REQUEST_URI'];
        $needle = 'wp-admin';

        $new_rules = array();

        $blogCustomSlug = Apollo_App::getCustomSlugByModuleName(Apollo_DB_Schema::_BLOG_POST_PT);
        $new_rules['^'.$blogCustomSlug.'/page/?([0-9]{1,})/?$'] = 'index.php?paged=$matches[1]&pagename='.Apollo_Page_Creator::ID_BLOG.'&_apollo_page_blog=true';
        $new_rules['^'.$blogCustomSlug.'/?$'] = 'index.php?pagename='.Apollo_Page_Creator::ID_BLOG.'&_apollo_page_blog=true&paged=1';
        $new_rules['^authors/?$'] = 'index.php?_apollo_author_search=true&paged=1';

        /**
         * @ticket #19408: [CF] 20190311 - [Blog] The print page function does not work
         */
        $new_rules['^print/'.$blogCustomSlug.'/?([0-9]{1,})/?$'] = 'index.php?_apollo_print_post_page=true&_apollo_post_id=$matches[1]';

        /*@ticket #17123 */
        if(Apollo_App::is_avaiable_module(Apollo_DB_Schema::_ARTIST_PT)){
            $artistCustomSlug = Apollo_App::getCustomSlugByModuleName('artist');
            $new_rules['^'.$artistCustomSlug.'/?$'] = 'index.php?_apollo_artist_search=true&paged=1';
            $new_rules['^print/'.$artistCustomSlug.'/?([0-9]{1,})/?$'] = 'index.php?_apollo_print_artist_page=true&_apollo_artist_id=$matches[1]';
            /*@ticket #17123 */
            $new_rules['^('.$artistCustomSlug.')/timeline/?$'] = 'index.php?_apollo_artist_timeline=true&_timeline_type=$matches[1]&_rw_is_one_column=true';
        }

        if(Apollo_App::is_avaiable_module(Apollo_DB_Schema::_EDUCATION)) {
            $new_rules['^educator/?$'] = 'index.php?_apollo_educator_search=true&paged=1';
            $new_rules['^educator/([^/]+)(/[0-9]+)?/([0-9]+)?/?$'] = 'index.php?educator=$matches[1]&_apollo_prog_id=$matches[3]';
            $new_rules['^program/?$'] = 'index.php?_apollo_program_search=true&paged=1';
            $new_rules['^print/educator/?([0-9]{1,})/program/?([0-9]{1,})/?$'] = 'index.php?_apollo_print_educator_page=true&_apollo_educator_id=$matches[1]&_apollo_program_id=$matches[2]';
        }

        if(Apollo_App::is_avaiable_module(Apollo_DB_Schema::_EVENT_PT)) {
            //TriLM add new route for event
            $new_rules['^event/?$'] = 'index.php?_apollo_event_search=true&paged=1';
            $new_rules['^print/event/?([0-9]{1,})/?$'] = 'index.php?_apollo_print_event_page=true&_apollo_event_id=$matches[1]';
            $new_rules['^export_calendar/event'] = 'index.php?_apollo_export_calendar_event_page=true';
            //end route for event
        }

        if(Apollo_App::is_avaiable_module(Apollo_DB_Schema::_ORGANIZATION_PT)) {
            //TriLM add new route for event
            $new_rules['^organization/?$'] = 'index.php?_apollo_org_search=true&paged=1';
            $new_rules['^print/organization/?([0-9]{1,})/?$'] = 'index.php?_apollo_print_org_page=true&_apollo_org_id=$matches[1]';
            //end route for event
        }

        if(Apollo_App::is_avaiable_module(Apollo_DB_Schema::_BUSINESS_PT)){
            //Thienld: add new route for business
            $new_rules['^business/?$'] = 'index.php?_apollo_business_search=true&paged=1';
            $new_rules['^print/business/?([0-9]{1,})/?$'] = 'index.php?_apollo_print_business_page=true&_apollo_business_id=$matches[1]';
            //Thienld: End add new route for business
        }

        if(Apollo_App::is_avaiable_module(Apollo_DB_Schema::_VENUE_PT)) {
            //TriLM add new route for venue

            /**
             * @author vulh
             * @ticket #19027 - 0002522: wpdev54 Customization - Custom venue slug
             */
            $venueCustomSlug = Apollo_App::getCustomSlugByModuleName('venue');
            $new_rules['^'.$venueCustomSlug.'/?$'] = 'index.php?_apollo_venue_search=true&paged=1';
            $new_rules['^print/'.$venueCustomSlug.'/?([0-9]{1,})/?$'] = 'index.php?_apollo_print_venue_page=true&_apollo_venue_id=$matches[1]';
            //end route for venue
        }

        if(Apollo_App::is_avaiable_module(Apollo_DB_Schema::_CLASSIFIED)) {
            //VuLH add new route for classified
            /**
             * ThienLD : handle custom classified slug in ticket #12697
             */
            $classifiedCustomSlug = Apollo_App::getCustomSlugByModuleName('classified');
            $new_rules['^'.$classifiedCustomSlug.'/?$'] = 'index.php?_apollo_classified_search=true&paged=1';
            $new_rules['^print/'.$classifiedCustomSlug.'/?([0-9]{1,})/?$'] = 'index.php?_apollo_print_classified_page=true&_apollo_classified_id=$matches[1]';
            //end route for classified
        }

        if(Apollo_App::is_avaiable_module(Apollo_DB_Schema::_PUBLIC_ART_PT)) {
            //VuLH add new route for public art
            $new_rules['^public-art/?$'] = 'index.php?_apollo_public_art_search=true&paged=1';
            $new_rules['^public-art/map?$'] = 'index.php?_apollo_public_art_map=true&paged=1';
            $new_rules['^print/public-art/?([0-9]{1,})/?$'] = 'index.php?_apollo_print_public_art_page=true&_apollo_public_art_id=$matches[1]';
            //end route for public art
        }

        /*TruongHN : add new rules for Education Feed */
        $new_rules['education/feed/(feed|rdf|rss|rss2|atom)/?$'] = 'index.php?post_type=educator&feed=$matches[1]';
        $new_rules['education/(feed|rdf|rss|rss2|atom)/?$'] = 'index.php?post_type=educator&feed=$matches[1]';
        /*TruongHN : end of adding new rules for Education Feed */

        $new_rules['^api/v1/syndication/?([A-Za-z0-9-]{1,})/?$'] = 'index.php?_apollo_api=true&_apl_action=$matches[1]';
        $new_rules['^api/v1/syndication/events/?([0-9]{1,})/?$'] = 'index.php?_apollo_api=true&_apl_event_id=$matches[1]&_apl_action=events';

        $new_rules['^api/v1/users/?([A-Za-z0-9-]{1,})/?$'] = 'index.php?_apollo_api=true&_apl_action=$matches[1]';

        //
        $new_rules['^clean-form/?$'] = 'index.php?_apollo_clean_form=true&redirect=$matches[1]';

        /*@ticket #18127: News Module */
        if (Apollo_App::is_avaiable_module(Apollo_DB_Schema::_NEWS_PT)) {
            $newsCustomSlug = Apollo_App::getCustomSlugByModuleName('news');
            $new_rules['^'.$newsCustomSlug.'/?$'] = 'index.php?_apollo_news_search=true&paged=1';
            $new_rules['^print/'.$newsCustomSlug.'/?([0-9]{1,})/?$'] = 'index.php?_apollo_print_news_page=true&_apollo_news_id=$matches[1]';
            $new_rules['^'.$newsCustomSlug.'/page/?([0-9]{1,})/?$'] = 'index.php?paged=$matches[1]&_apollo_news_search=true';
        }

        $new_rules['^cron/?([A-Za-z0-9-]{1,})/?$'] = 'index.php?_apollo_cron=$matches[1]';

        if(strpos($haystack, $needle) !== false || !is_user_logged_in()) {
            return array_merge($new_rules, $old_rules);
        }

        /* Rewrite Dahsboard page */
        $tree_menus = Apollo_App::getArrRewriteDashboard();


        foreach($tree_menus as $match => $arr) {

            if(isset($arr['source'])) {
                $new_rules['^'.$match.'/?$'] =  $arr['source'];
            }

            // level 1
            if(isset($arr['childs'])) {
                foreach($arr['childs'] as $m => $a) {
                    if(isset($a['source']))  {
                        $new_rules['^'.$m.'/?$'] = $a['source'];
                    }
                }
            }
        }

        /* add map google */
        $new_rules['^map/?$'] = '';

        /* Other rewrite put here */

        return array_merge($new_rules, $old_rules);
    }

    public static function apollo_add_query_vars($public_query_vars)
    {
        $public_query_vars[] = '_apollo_dashboard';
        $public_query_vars[] = '_apollo_event_id';
        $public_query_vars[] = '_apollo_artist_id';
        $public_query_vars[] = '_apollo_copy_event';
        $public_query_vars[] = '_from_id';
        $public_query_vars[] = '_apollo_print_event_page';
        $public_query_vars[] = '_apollo_print_artist_page';
        $public_query_vars[] = '_apollo_print_educator_page';
        $public_query_vars[] = '_apollo_page_blog';
        $public_query_vars[] = '_apollo_artist_search';
        $public_query_vars[] = '_apollo_artist_timeline';
        $public_query_vars[] = '_rw_is_one_column';
        $public_query_vars[] = '_timeline_type';
        $public_query_vars[] = '_apollo_educator_id';
        $public_query_vars[] = '_apollo_program_id';
        $public_query_vars[] = '_is_agency_educator_page';
        $public_query_vars[] = '_apollo_educator_search';
        $public_query_vars[] = '_apollo_prog_id';
        $public_query_vars[] = '_apollo_program_search';
        $public_query_vars[] = '_apollo_step2a';
        $public_query_vars[] = '_org_id';
        $public_query_vars[] = '_edit_post_id';
        $public_query_vars[] = '_apollo_event_search';
        $public_query_vars[] = '_apollo_author_search';
        $public_query_vars[] = '_apollo_org_search';
        $public_query_vars[] = '_apollo_business_search';
        $public_query_vars[] = '_apollo_print_org_page';
        $public_query_vars[] = '_apollo_org_id';
        $public_query_vars[] = '_apollo_venue_id';
        $public_query_vars[] = '_apollo_venue_search';
        $public_query_vars[] = '_apollo_print_venue_page';
        $public_query_vars[] = '_apollo_print_classified_page';
		$public_query_vars[] = '_apollo_classified_id';
        $public_query_vars[] = '_apollo_classified_search';
        $public_query_vars[] = '_apollo_print_classified_page';
        $public_query_vars[] = '_apollo_public_art_search';
        $public_query_vars[] = '_apollo_print_public_art_page';
        $public_query_vars[] = '_apollo_public_art_map';
		$public_query_vars[] = '_apollo_export_calendar_event_page';
        $public_query_vars[] = '_apollo_public_art_id';
        $public_query_vars[] = '_apollo_print_public_art_page';
        $public_query_vars[] = '_apollo_print_post_page';
        $public_query_vars[] = '_apollo_post_id';
        $public_query_vars[] = '_apollo_import_event_page';
        $public_query_vars[] = '_agency_org_tab';
        $public_query_vars[] = '_agency_venue_tab';
        $public_query_vars[] = '_agency_artist_tab';
        $public_query_vars[] = 'keyword';
        $public_query_vars[] = '_apollo_page_blog_author';
        $public_query_vars[] = '_apollo_api';
        $public_query_vars[] = '_apollo_cron';
        $public_query_vars[] = '_apl_action';
        $public_query_vars[] = '_apl_event_id';
        $public_query_vars[] = '_apollo_event_org_slug';
        $public_query_vars[] = '_apollo_clean_form';
        $public_query_vars[] = '_apollo_news_search';
        $public_query_vars[] = '_apollo_print_news_page';
        $public_query_vars[] = '_apollo_news_id';
        $public_query_vars[] = '_apollo_copy_program';

        return $public_query_vars;
    }


}
new Apollo_Rewrite();

//add_action('generate_rewrite_rules', 'work_list');
//function work_list($wp_rewrite) {
//    $newrules = array();
//
//    $newrules['educator/([^/]+)(/[0-9]+)?/?([0-9]{1,})/?$'] = 'index.php?educator=$matches[1]&_apollo_prog_id=$matches[2]';
//    $wp_rewrite->rules = $newrules + $wp_rewrite->rules;
//}

