<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}


class Apollo_Google_Coor_Cache  {

    /**
     * https://maps.googleapis.com/maps/api/geocode/json?address
     */
    public static function getCoordinateByAddressInLocalDB($address)
    {
        $key_address = hash('sha512', $address);

        global $wpdb;

        $tbl_coordinate = $wpdb->base_prefix.'apollo_coordinate_cache';
        $sql = "select coordinate from $tbl_coordinate where key_address = '%s'";
        $coor = $wpdb->get_var($wpdb->prepare($sql, $key_address));

        if(!empty($coor)) {

            $data = explode(",", $coor);

            if ((isset($data[0]) && $data[0] === 'false') || (isset($data[1]) && $data[1] === 'false')) {
                return false;
            }

            return $data;
        }

        return false;
    }

    private static function getCoorFromGoogleServerAndSave($address)
    {
        // request to server and get coordinate
        $arrCoor = self::_getCoorByAddress($address);

        if(!empty($arrCoor)) {
            return self::saveCoorToLocalDB($arrCoor[0], $arrCoor[1], $address);
        }

        return $arrCoor;
    }

    public static function saveCoorToLocalDB($lat, $lng, $address) {
        global $wpdb;
        $tbl_coordinate = $wpdb->base_prefix.'apollo_coordinate_cache';

        $_key_address = hash('sha512', $address);
        $_scoor = $lat.','.$lng;
        $result = $wpdb->insert($tbl_coordinate, array(
                'key_address' => $_key_address,
                'coordinate' => $_scoor,
            ));

        return $result;
    }

    private static  function _getCoorByAddress($address) {

        $defaultServerKey = defined('APL_GOOGLE_API_KEY_SERVER') ? APL_GOOGLE_API_KEY_SERVER : '';

        $serverKey = of_get_option(Apollo_DB_Schema::_GOOGLE_API_KEY_SERVER, $defaultServerKey);

        $ch = curl_init();
        $link = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($address) . '&key=' . $serverKey;

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_URL, $link);

        $data = json_decode(curl_exec($ch), true);

        if($data['status'] !== 'ZERO_RESULTS') {
            // save to database
            if(isset($data['results'][0]['geometry']['location'])) {
                return $data['results'][0]['geometry']['location'];
            }
        }

        return array();
    }

    //TriLM get Coor
    public static function getCoorForVenue($address){
        return self::_getCoorByAddress($address);
    }
}