<?php
/**
 * @author Trilm
 * Date: 27/10/2016
 * Time: 18:18
 * [domain]?is_reset_option=true&page_id='' or [domain]?is_reset_option=true
 * This class for update default page all sites
 */

class Apollo_Reset_Default_Page{
    protected $request;

    public function  __construct()
    {
        $this->request = $_GET;
        add_action('wp_loaded',array($this,'reset_option'));
    }

    public function reset_option(){

       if( $this->request['is_reset_option'] &&
           $this->request['is_reset_option']==1
       ){
           $message = '';
           $sites = null;
           //wordpress 4.5 and later
           if(function_exists('get_sites')){
               $sites = get_sites();
           }
           //older wordpress version
           elseif (function_exists('wp_get_sites')){
               $sites = wp_get_sites();
           }

           if(is_array($sites) && count($sites) > 0){
               foreach ($sites as $site){
                   if($this->request['is_all_page'] == 1){
                       $message .=  "<br/>======================== {$site->domain} ================= <br/>";
                       $message .= $this->update_all_page($site);
                       $message .=  "<br/><br/>";
                   }else{
                       if(!empty($this->request['page_id'])){
                           $page_id  = $this->request['page_id'];
                           $message =   $this->update_one_page($page_id,$site);
                       }
                   }
               }
               echo $message;
               exit();
           }
       }
    }

    /**
     * @author Trilm
     * @return array
     * Define pages need updated
     */
    public function get_pages(){
        //page id in class-apollo-creator: we no need get all pages, only newest pages.
        $arr = array(
            //Event page
            Apollo_Page_Creator::ID_IMPORT_EVENT,
            Apollo_Page_Creator::ID_UPDATE_EVENT,
            Apollo_Page_Creator::ID_COPY_EVENT,

            //Artist page
            Apollo_Page_Creator::ID_ARTIST_PROFILE,
            Apollo_Page_Creator::ID_ARTIST_VIDEO,
            Apollo_Page_Creator::ID_ARTIST_AUDIO,
            Apollo_Page_Creator::ID_ARTIST_EVENT,

            //Account page
            Apollo_Page_Creator::ID_ACCOUNT_INFO,
            Apollo_Page_Creator::ID_ACCOUNT_MY_LIST,
            Apollo_Page_Creator::ID_ACCOUNT_ACTIVITY,

            //org
            Apollo_Page_Creator::ID_ORG_LISTING,

            //classified
            Apollo_Page_Creator::ID_PUBLISHED_CLASSIFIEDS,

            //agency
            Apollo_Page_Creator::ID_AGENCY_EDUCATOR,
            Apollo_Page_Creator::ID_AGENCY_ORG,
            Apollo_Page_Creator::ID_AGENCY_VENUES,
            Apollo_Page_Creator::ID_AGENCY_ARTISTS,
        );
        return $arr;
    }

    public function get_page($pageID){
        if(empty($pageID)){
            return null;
        }
        $arr_define = Apollo_Page_Creator::getPagesDefinition();
        $arr_page_pr = isset($arr_define[$pageID])?$arr_define[$pageID]:null;
        if(!$arr_page_pr){
            return null;
        }
        return  $arr_page_pr;

    }

    public function update_all_page($site){
        $message = '';
        $pages = $this->get_pages();
        if(is_array($pages) && count($pages) > 0)
        foreach ($pages as $pageId ){
            $message .=  $this->update_one_page($pageId,$site);
        }
        return $message;
    }

    public function update_one_page($page_name,$site){
        $blogId = $site->blog_id;
        $arr_page_pr = $this->get_page($page_name);
        $template =  __DIR__ .'/admin/_templates/'.$page_name.'.php';
        if(file_exists($template)){
            $page_arg  = array(
                'post_title'    => $arr_page_pr['title'],
                'post_content'  => include $template,
                'post_status'   => $arr_page_pr['status'],
                'post_type'     => 'page',
                'post_name'     => $page_name
            );
            $inserted_page_id = 0;
            switch_to_blog($blogId);
            $db_page = get_page_by_path( $page_name );
            if(!empty($db_page) && $db_page->ID){
                $inserted_page_id =  $page_arg['ID'] = $db_page->ID;
                wp_insert_post($page_arg);
            }else{
                $inserted_page_id = wp_insert_post( $page_arg);
            }
            restore_current_blog();
            $inserted_title = $db_page->post_title ? $db_page->post_title : $page_id;
            if($inserted_page_id){
                return "Updated \"{$inserted_title}\" in site \"{$site->domain}\" success <br/>";
            }
            return "Fail update \"{$inserted_title}\" in site \"{$site->domain}\" <br/>";

        }
        return "Fail open \"{$template}\" in site \"{$site->domain}\" <br/>";
    }
}

new Apollo_Reset_Default_Page();
