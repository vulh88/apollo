<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class Apollo_Log_System  {
    public static  function registerLogActivity() {
        Apollo_Event_System::registerCallback(Apollo_Event_System::ACTIVIY,  array(__CLASS__, 'logActivityDb'));
    }

    public static  function registerLogVisitEvent() {
        Apollo_Event_System::registerCallback(Apollo_Event_System::VISITEVENT,  array(__CLASS__, 'logVisitEvent'));
    }

    public static function logActivityDb($data) {

        // Get data for mongo db
        $mongoData = $data;

        /**
         * Save data to MongoDB database
         * Just save for:
         *    + 3 button clicks (official website, buy ticket, discount ticket)
         *    + organization is not TMP ORG
         *    + current site has enabled Report feature in Network configuration
         *
         * @ticket: #11232, #11307
         */
        $enableReport = Apollo_App::get_network_enable_reports( get_current_blog_id() );
        if ( in_array($data['activity'], Apollo_Activity_System::getClickEventGroup()) && !empty($data['org_id']) && intval($enableReport) === 1 ) {
            self::mongDBLogActivity($mongoData);
        }


        // Only allow for logged in user
        if (!is_user_logged_in()) {
            return false;
        }

        if (isset($data['start_date'])) {
            unset($data['start_date']);
        }
        if (isset($data['end_date'])) {
            unset($data['end_date']);
        }


        $defaut = array(
            'ip' => $_SERVER['REMOTE_ADDR'],
            'timestamp' => date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']),
            'exdata' => serialize(array()),
        );

        // Exdata
        $exdata = $defaut['exdata'];
        if(isset($data['exdata'])) {
            $exdata = serialize($data['exdata']);
            unset($data['exdata']);
        }

        $data = array_map("trim", $data);
        $data['exdata'] = $exdata;

        if(!isset($data['item_id']) || empty($data['item_id'])) {
            throw new Exception(__('item_id key is required', 'apollo'));
        }

        if(!isset($data['item_type']) || empty($data['item_type'])) {
            throw new Exception(__('item_type key is required', 'apollo'));
        }

        if(!isset($data['activity']) || empty($data['activity'])) {
            throw new Exception(__('activity key is required', 'apollo'));
        }

        //switch difference environment
        if(!isset($data['url']) || empty($data['url'])) {
            throw new Exception(__('url key is required', 'apollo'));
        }

        if(!isset($data['title']) || empty($data['title'])) {
            throw new Exception(__('title key is required', 'apollo'));
        }

        /* Besure save all with UTC timezone - when show will depend on user to show */
        if(isset($data['timestamp'])) {
            $data['timestamp'] = date('Y-m-d H:i:s', $data['timestamp']);
        }

        $data = wp_parse_args($data, $defaut);

        // save actually to db
        global $wpdb;
        $tblname = $wpdb->prefix.Apollo_Tables::_APL_USER_ACTIVITY;

        // Org is only using for mongo data
        if (isset($data['org_id'])) {
            unset($data['org_id']);
        }

        $wpdb->replace($tblname, $data);

     }

    public static function logVisitEvent($data)
    {
        // Insert into database
        // save actually to db
        global $wpdb;

        $tblname = $wpdb->prefix.Apollo_Tables::_APL_USER_VISIT;

        $topTenTable = $wpdb->base_prefix.Apollo_Tables::_APL_TOPTEN;

        $item_id = intval($data['item_id']);
        $item_type = sanitize_text_field($data['item_type']);

        $wpdb->query("insert into $tblname(item_id, item_type, count) values ('$item_id', '$item_type', 1) on duplicate key update count=count+1");

        $blogId = get_current_blog_id();

        $datetime = date('Y-m-d H:i:s');
        $wpdb->query("insert into $topTenTable(item_id, item_type, count, blog_id, latest_visitation) values ('$item_id', '$item_type', 1, $blogId, '$datetime') on duplicate key update count=count+1,latest_visitation = '$datetime'");
    }

    public static function mongDBLogActivity($data)
    {
        try {
            $ins = apl_instance('APL_Lib_Report_Event');
            $ins->save($data);
        }
        catch(Exception $e) {

        }

    }
}

