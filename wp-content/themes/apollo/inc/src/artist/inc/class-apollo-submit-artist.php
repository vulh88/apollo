<?php

class Apollo_Submit_Artist extends Apollo_Submit_Form {
    private $id = '';
    
    public function __construct() {
      
        $rules =  array(
            'id'   => '',
            Apollo_DB_Schema::_APL_ARTIST_LNAME => array( 'required' ),
            Apollo_DB_Schema::_APL_ARTIST_EMAIL => array( 'email' ),
            Apollo_DB_Schema::_APL_ARTIST_BLOGURL => array( 'url' ),
            Apollo_DB_Schema::_APL_ARTIST_WEBURL => array( 'url' ),
            Apollo_DB_Schema::_APL_ARTIST_INS => array( 'url' ),
            Apollo_DB_Schema::_APL_ARTIST_PR => array( 'url' ),
            Apollo_DB_Schema::_APL_ARTIST_FB => array( 'url' ),
            Apollo_DB_Schema::_APL_ARTIST_LK => array( 'url' ),
            Apollo_DB_Schema::_APL_ARTIST_TW => array( 'url' ),
        );

        /** @Ticket #18747 */
        if (of_get_option(Apollo_DB_Schema::_ARTIST_FE_ENABLE_REQUIRED_TYPE, true)) {
            $rules['post_cat'] = array('required' );
        }
        parent::__construct(Apollo_DB_Schema::_ARTIST_PT, $rules);
    }

    public function isValidAll($arrData){
        return parent::isValidAll($arrData);
    }

    public function save($arrData, $question = '', $arrCustomField = '') {

        $post = array(
            'ID'            => isset($arrData['ID']) ? $arrData['ID'] : '',
            'post_title'    => Apollo_App::clean_data_request( $arrData[Apollo_DB_Schema::_APL_ARTIST_FNAME] ). ' '
                .Apollo_App::clean_data_request( $arrData[Apollo_DB_Schema::_APL_ARTIST_LNAME] ),
            'post_content'  => Apollo_App::convertCkEditorToTinyCME($arrData['post_content']),
            'post_author'   => get_current_user_id(),
            'post_type'     => $this->post_type,
        );
        
        $isNew = true;
        if($arrData['ID']) {
            $post['ID'] = $arrData['ID'];
            $isNew = false;
        }
        
        if ( !$isNew ) {
            /** Vandd - @Ticket: #12283*/
            unset($post['post_author']);
            wp_update_post( $post );
            $this->id = $post['ID'];
        } else {
            $post['post_status'] = Apollo_App::insertStatusRule(Apollo_DB_Schema::_ARTIST_PT);
            $this->id = wp_insert_post( $post );
        }

        if( !Apollo_App::isInputMultiplePostMode()){
            // Update user nice name for sorting
            $artist_user = get_user_by('id', get_current_user_id());
            if (!empty($artist_user)) {
                APL_Artist_Function::update_artists_usermeta($this->id, $artist_user->user_nicename, $artist_user->ID);
            }
        }

        /**
         * Update terms
         */
        wp_set_post_terms( $this->id, isset($arrData[Apollo_DB_Schema::_ARTIST_TYPE]) ? $arrData[Apollo_DB_Schema::_ARTIST_TYPE] : '' , $this->post_type. '-type' );
        wp_set_post_terms( $this->id, isset($arrData[Apollo_DB_Schema::_ARTIST_STYLE_TAX]) ? $arrData[Apollo_DB_Schema::_ARTIST_STYLE_TAX] : '' , $this->post_type. '-style' );
        wp_set_post_terms( $this->id, isset($arrData[Apollo_DB_Schema::_ARTIST_MEDIUM_TAX]) ? $arrData[Apollo_DB_Schema::_ARTIST_MEDIUM_TAX] : '', $this->post_type. '-medium' );

        
        unset($arrData['id'], $arrData['post_title'], $arrData['post_content'], $arrData['post_author'],
            $arrData['post_cat'], $arrData['upload_pdf_label'],
            $arrData['post_style'], $arrData['post_medium'] );


        // Update meta data
        $metaData = $this->getMetaData($arrData, $isNew);
        foreach($metaData as $key => $value){
            if($key != Apollo_DB_Schema::_APL_ORG_TERM){
                //meta
                update_apollo_meta($this->id, $key, $value);
            }
        }

        $this->_process_upload_pdf();

        if(isset($_SESSION['apollo']['search'])) {
            unset($_SESSION['apollo']['search']);
        }

        /**
         * @ticket #18695: [CF] 20181221 - Add the option for image upload to the main artist profile form.
         */
        $locationPhotoForm = of_get_option(APL_Theme_Option_Site_Config_SubTab::_ARTIST_PHOTO_FORM_LOCATION, 1);
        if($locationPhotoForm == 2) {
            self::saveFeaturedImageHandler($this->id, Apollo_DB_Schema::_ARTIST_PT);
            self::saveGalleryHandler($this->id, Apollo_DB_Schema::_ARTIST_PT);
        }

        $this->savePrivateStateCity($this->id);
        /**
         * Thienld : send email to confirm admin the new post is created
         **/
        if(has_action('apollo_email_confirm_new_item_created') && $isNew){
            do_action('apollo_email_confirm_new_item_created',$this->id,Apollo_DB_Schema::_ARTIST_PT,false);
        }

        /**
         * @ticket #18695: [CF] 20181221 - Add the option for image upload to the main artist profile form.
         */
        global $wp;
        $current_url = home_url(). '/'. add_query_arg(array(),$wp->request);
        wp_redirect(home_url() . '/clean-form?redirect=' . urlencode($current_url) . '&post_type='. Apollo_DB_Schema::_ARTIST_PT .'&action=' . ($isNew ? 'add-new' : 'update'));

        return $this->id;
    }

    private function getMetaData($arrData, $isNew){
        $isAddNewStateZip = isset($_POST['add_new_state']) && $_POST['add_new_state'] == 1;
        $cityVal = Apollo_App::clean_data_request($isAddNewStateZip ? $arrData[Apollo_DB_Schema::_APL_PRI_CITY] : $arrData[Apollo_DB_Schema::_APL_ARTIST_CITY]);
        $stateVal = Apollo_App::clean_data_request($isAddNewStateZip ? $arrData[Apollo_DB_Schema::_APL_PRI_STATE] : $arrData[Apollo_DB_Schema::_APL_ARTIST_STATE]);
        $zipVal = Apollo_App::clean_data_request($isAddNewStateZip ? $arrData[Apollo_DB_Schema::_APL_PRI_ZIP] : $arrData[Apollo_DB_Schema::_APL_ARTIST_ZIP]);

        // Update post state/city/zip value in case the client add new private data
        $_POST[Apollo_DB_Schema::_APL_ARTIST_STATE] = $stateVal;
        $_POST[Apollo_DB_Schema::_APL_ARTIST_CITY] = $cityVal;
        $_POST[Apollo_DB_Schema::_APL_ARTIST_ZIP] = $zipVal;

        /** @Ticket #19640 */
        if (isset($arrData[Apollo_DB_Schema::_APL_ARTIST_TMP_STATE])) {
            $otherState = sanitize_text_field($arrData[Apollo_DB_Schema::_APL_ARTIST_TMP_STATE]);
        } else {
            $otherState = Apollo_App::apollo_get_meta_data($this->id, Apollo_DB_Schema::_APL_ARTIST_TMP_STATE, Apollo_DB_Schema::_APL_ARTIST_ADDRESS);
        }

        if (isset($arrData[Apollo_DB_Schema::_APL_ARTIST_TMP_CITY])) {
            $otherCity = sanitize_text_field($arrData[Apollo_DB_Schema::_APL_ARTIST_TMP_CITY]);
        } else {
            $otherCity = Apollo_App::apollo_get_meta_data($this->id, Apollo_DB_Schema::_APL_ARTIST_TMP_CITY, Apollo_DB_Schema::_APL_ARTIST_ADDRESS);
        }

        if (isset($arrData[Apollo_DB_Schema::_APL_ARTIST_TMP_ZIP])) {
            $otherZip = sanitize_text_field($arrData[Apollo_DB_Schema::_APL_ARTIST_TMP_ZIP]);
        } else {
            $otherZip = Apollo_App::apollo_get_meta_data($this->id, Apollo_DB_Schema::_APL_ARTIST_TMP_ZIP, Apollo_DB_Schema::_APL_ARTIST_ADDRESS);
        }

        $arrayMetaGroup[Apollo_DB_Schema::_APL_ARTIST_ADDRESS] = serialize(Apollo_App::clean_array_data(
            array(
            Apollo_DB_Schema::_APL_ARTIST_STREET   => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_STREET]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_STREET] : '',
            Apollo_DB_Schema::_APL_ARTIST_STATE   => $stateVal,
            Apollo_DB_Schema::_APL_ARTIST_CITY   => $cityVal,

            Apollo_DB_Schema::_APL_ARTIST_REGION   => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_REGION]) ? sanitize_text_field($arrData[Apollo_DB_Schema::_APL_ARTIST_REGION]) : Apollo_App::apollo_get_meta_data($this->id, Apollo_DB_Schema::_APL_ARTIST_REGION, Apollo_DB_Schema::_APL_ARTIST_ADDRESS ),

            Apollo_DB_Schema::_APL_ARTIST_ZIP   => $zipVal,
            Apollo_DB_Schema::_APL_ARTIST_COUNTRY   => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_COUNTRY]) ? sanitize_text_field($arrData[Apollo_DB_Schema::_APL_ARTIST_COUNTRY]) : '',
            Apollo_DB_Schema::_APL_ARTIST_TMP_STATE   => $otherState,
            Apollo_DB_Schema::_APL_ARTIST_TMP_CITY   => $otherCity,
            Apollo_DB_Schema::_APL_ARTIST_TMP_ZIP   => $otherZip,
            )
        ));
        $arrayMetaGroup[Apollo_DB_Schema::_APL_ARTIST_QUESTIONS] = isset($arrData[Apollo_DB_Schema::_APL_ARTIST_QUESTIONS]) ? maybe_serialize(Apollo_App::clean_array_data($arrData[Apollo_DB_Schema::_APL_ARTIST_QUESTIONS])) : '';
        $arrayMetaGroup[Apollo_DB_Schema::_APL_ARTIST_LNAME] = $arrData[Apollo_DB_Schema::_APL_ARTIST_LNAME];
        $arrayMetaGroup[Apollo_DB_Schema::_APL_ARTIST_YEAR] = isset($arrData[Apollo_DB_Schema::_APL_ARTIST_YEAR]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_YEAR] : '';

        // Keep icons due to it not on the front end form
        if ( !$isNew ) {
            // Get meta data
            $artistObj = get_artist($this->id);
            $iconData = $artistObj->get_meta_data(Apollo_DB_Schema::_APL_ARTIST_POST_ICONS, Apollo_DB_Schema::_APL_ARTIST_DATA);
            $arrData[Apollo_DB_Schema::_APL_ARTIST_POST_ICONS] = $iconData;

            /**
             * ThienLD : get existing videos data on saving the existing an artist to avoid loosing artist video data
            */
            $videos = $artistObj->get_meta_data(Apollo_DB_Schema::_APL_ARTIST_VIDEO, Apollo_DB_Schema::_APL_ARTIST_DATA, "");
            $arrData[Apollo_DB_Schema::_APL_ARTIST_VIDEO] = $videos;
        }

        $id = $arrData['ID'];

        unset(
            $arrData['ID'],
            $arrData['post_title'],
            $arrData['post_content'],
            $arrData['post_author'],
            $arrData['post_type'],
            $arrData['post_status'],
            $arrData[Apollo_DB_Schema::_APL_ARTIST_ADDRESS ],
            $arrData[Apollo_DB_Schema::_APL_ARTIST_CITY],
            $arrData[Apollo_DB_Schema::_APL_ARTIST_STATE],
            $arrData[Apollo_DB_Schema::_APL_ARTIST_ZIP],
            $arrData[Apollo_DB_Schema::_APL_ARTIST_TMP_STATE],
            $arrData[Apollo_DB_Schema::_APL_ARTIST_TMP_CITY],
            $arrData[Apollo_DB_Schema::_APL_ARTIST_TMP_ZIP],
            $arrData[Apollo_DB_Schema::_APL_ARTIST_REGION],
            $arrData[Apollo_DB_Schema::_APL_ARTIST_QUESTIONS],
            $arrData[Apollo_DB_Schema::_ARTIST_TYPE],
            $arrData[Apollo_DB_Schema::_ARTIST_STYLE_TAX],
            $arrData[Apollo_DB_Schema::_ARTIST_MEDIUM_TAX]
        );

        $arrayMetaGroup[Apollo_DB_Schema::_APL_ARTIST_CITY] = $cityVal;
        $arrayMetaGroup[Apollo_DB_Schema::_APL_ARTIST_ZIP] = $zipVal;

        /** @Ticket #13028 */
        $arrayMetaGroup[Apollo_DB_Schema::_APL_ARTIST_STATE] = $stateVal;

        //custom field
        $group_fields = Apollo_Custom_Field::get_group_fields( Apollo_DB_Schema::_ARTIST_PT );
        $customFieldValue = array();
        if(is_array($group_fields) && !empty($group_fields)){
            foreach($group_fields as $group){
                if(isset($group['fields']) && !empty($group['fields'])){
                    foreach($group['fields'] as $field){
                        if(isset($arrData[$field->name])){
                            if(is_array( $arrData[$field->name])){
                                $customFieldValue[$field->name] = $arrData[$field->name];
                            }
                            elseif($field->cf_type == 'wysiwyg'){
                                $customFieldValue[$field->name] = Apollo_App::convertCkEditorToTinyCME($arrData[$field->name]);
                            }
                            else{
                                $customFieldValue[$field->name] = Apollo_App::clean_data_request($arrData[$field->name]);
                            }

                            if(Apollo_Custom_Field::has_explain_field($field)){
                                $exFieldName = $field->name.'_explain';
                                $customFieldValue[$exFieldName] = Apollo_App::clean_data_request($arrData[$exFieldName]);
                            }
                            unset($arrData[$field->name]);
                        } else if ($id) { // Keep the old data
                            $customFieldValue[$field->name] = Apollo_App::apollo_get_meta_data($id, $field->name, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA);
                        }
                    }
                }
            }
        }
        $arrayMetaGroup[Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA] = maybe_serialize($customFieldValue);

        $arrayMetaGroup[Apollo_DB_Schema::_APL_ARTIST_DATA] = Apollo_App::serialize($arrData) ;

        return $arrayMetaGroup;

    }

    private function _process_upload_pdf() {
        $arr_attachment_ids = array();
        $post_id = $this->id;

        $_POST['upload_pdf_label'] = isset($_POST['upload_pdf_label']) ? Apollo_App::clean_array_data($_POST['upload_pdf_label']) : array();
        $arr_description = array_map('sanitize_text_field', $_POST['upload_pdf_label']);
        $old_attachment_ids = maybe_unserialize(get_apollo_meta($post_id, 'upload_pdf', true));
        if(empty($old_attachment_ids)) $old_attachment_ids = array();
        $arrError = array();
        /*@ticket #17020: [CF] 20180803 - [FE Forms] - Add the option to increase the file size upload - Item 1*/
        $maxUploadSize = Apollo_App::maxUploadFileSize('b');

        if(!empty($_FILES['upload_pdf']['name'])) {
            foreach($_FILES['upload_pdf']['name'] as $i => $name) {

                if(empty($name)) continue; /* Silent and continue */

                if ($_FILES['upload_pdf']['error'][$i] !== UPLOAD_ERR_OK) {
                    $arrError[] = array(
                        'message' => 'Error upload file: ' . $name,
                        'error' => true
                    );
                    continue;

                }

                // check filesize
                if($_FILES['upload_pdf']['size'][$i] > $maxUploadSize) {
                    $arrError[] = array(
                        'message' => 'File too large. Max upload size allowed is ' . number_format($maxUploadSize/ (1024 * 1024), 2) . ' MB',
                        'error' => true
                    );
                    continue;
                }

                // copy to upload directory
                if ( ! ( ( $uploads = wp_upload_dir(  ) ) && false === $uploads['error'] ) ) {

                    $arrError[] = array(
                        'message' => 'Missing upload dir for file ' . $name,
                        'error' => true
                    );
                    continue;
                }
                $wp_filetype = wp_check_filetype_and_ext($_FILES['upload_pdf']['tmp_name'][$i], $name );
                $type = empty( $wp_filetype['type'] ) ? '' : $wp_filetype['type'];

                if($type !== 'application/pdf') {
                    $arrError[] = array(
                        'message' => 'File  '. $name .' is invalid. Please upload pdf file only!',
                        'error' => true
                    );
                    continue;
                }

                $unique_name = wp_unique_filename( $uploads['path'], $name );
                $filename = $uploads['path']. '/' . $unique_name;

                $move_new_file = @ move_uploaded_file( $_FILES['upload_pdf']['tmp_name'][$i], $filename );

                if ( false === $move_new_file ) {
                    $arrError[] = array(
                        'message' => 'Cannot move to directory: ' . $uploads['path'],
                        'error' => true
                    );
                    continue;
                }


                $url = $uploads['url'] .'/'. $unique_name;

                /* Save into post attachment */
                $attachment = array(
                    'post_mime_type' => $type,
                    'guid' => $url,
                    'post_title' => $unique_name,
                    'post_content' => '',
                    'post_excerpt' => isset($arr_description[$i]) ? Apollo_App::clean_data_request($arr_description[$i]) : '',
                );

                $attachment_id = wp_insert_attachment($attachment, $filename, $post_id);
                if ( !is_wp_error($attachment_id) ) {
                    array_push($arr_attachment_ids, $attachment_id);
                }
            }
        } 

        /* Error mechasin */
        $_SESSION['messages']['artist-upload_pdf'] = $arrError;

        /* Update old part */
        $_POST['upload_pdf_label_old'] = isset($_POST['upload_pdf_label_old']) ? Apollo_App::clean_array_data($_POST['upload_pdf_label_old']) : array();
        $arr_description_old = array_map('sanitize_text_field', $_POST['upload_pdf_label_old']);
        $actual_old_attachment_ids = array();
        $removeAttachmentIds = isset($_POST['apl-remove-old-pdf']) ? explode(',', $_POST['apl-remove-old-pdf']) : array();
        $index = 0;
        foreach($old_attachment_ids as $id) {
            if (!in_array($id, $removeAttachmentIds)) {
                $pdf_post = array(
                    'ID'           => $id,
                    'post_excerpt' => isset($arr_description_old[$index]) ?  $arr_description_old[$index] : '',
                );
                if (isset($arr_description_old[$index])) {
                    array_push($actual_old_attachment_ids, $id);
                }
                wp_update_post( $pdf_post );
                $index += 1;
            }
        }

        // Merge upload result
        $arr_attachment_ids = array_merge($actual_old_attachment_ids, $arr_attachment_ids);

        update_apollo_meta($post_id, 'upload_pdf', $arr_attachment_ids);
    }

    
    function getId() {
        return $this->id;
    }

    /**
     * @param $arrData
     */
    public function saveVideo($arrData) {
        $arr_video = array();
        $base64encodedData = get_apollo_meta( $arrData['ID'], Apollo_DB_Schema::_APL_ARTIST_DATA, true );
        $_data = Apollo_App::unserialize($base64encodedData);
        if(isset($arrData['video_embed']) && is_array($arrData['video_desc']) ){
            foreach($arrData['video_embed'] as $k => $val){
                $arrDataItem = array(
                    'embed' => $val,
                    'desc' => isset($arrData['video_desc'][$k]) ? Apollo_App::clean_data_request($arrData['video_desc'][$k]) : ''
                );
                $arr_video[] = $arrDataItem;
            }
        }
        $_data[Apollo_DB_Schema::_APL_ARTIST_VIDEO] = $arr_video;

        update_apollo_meta( $arrData['ID'], Apollo_DB_Schema::_APL_ARTIST_DATA, Apollo_App::serialize( $_data ) );

    }

    public function saveAudio($arrData){
        $data = array();
        if(isset($arrData['embed']) && isset($arrData['desc']) && is_array($arrData['desc']) ){
            foreach($arrData['embed'] as $k => $val){
                $val = str_replace('\\','',$val);
                $arrDataItem = array(
                    'embed' => base64_encode($val),
                    'desc' => base64_encode(Apollo_App::clean_data_request($arrData['desc'][$k]))
                );
                $data[] = $arrDataItem;
            }
        }
        $dataSerialize = serialize($data);
        update_apollo_meta( $arrData['ID'], Apollo_DB_Schema::_APL_ARTIST_AUDIO, $dataSerialize );
    }
}