<?php
include_once APOLLO_SRC_DIR. '/artist/inc/factory/ArtistsInterface.php';
class DefaultArtistsFactory implements ArtistsInterface
{
    public function get()
    {
        include_once APOLLO_INCLUDES_DIR. '/src/artist/inc/class-artist-search.php';
        $search_obj = new Apollo_Artist_Page();
        $search_obj->setArtistMember(ArtistsInterface::ALL);
        return $search_obj;
    }
}