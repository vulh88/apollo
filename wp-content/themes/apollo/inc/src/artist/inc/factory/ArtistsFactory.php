<?php

class ArtistsFactory
{
    public function get($type = '', $isCounting = false) {
        include_once APOLLO_SRC_DIR. '/artist/inc/factory/ArtistsInterface.php';
        include_once APOLLO_SRC_DIR. '/artist/inc/factory/NormalArtistsFactory.php';
        include_once APOLLO_SRC_DIR. '/artist/inc/factory/PremiumArtistsFactory.php';
        include_once APOLLO_SRC_DIR. '/artist/inc/factory/DefaultArtistsFactory.php';

        switch ($type) {
            case ArtistsInterface::PREMIUM:
                $premiumArtistsFactory = new PremiumArtistsFactory();
                $objSearch = $premiumArtistsFactory->get();
                break;

            case ArtistsInterface::NORMAL:
                $normalArtistsFactory = new NormalArtistsFactory();
                $objSearch = $normalArtistsFactory->get();
                break;
            default:
                $defaultArtistFactory = new DefaultArtistsFactory();
                $objSearch = $defaultArtistFactory->get();
        }

        if ($objSearch) {
            $objSearch->setIsCounting($isCounting);
            $objSearch->search();
            return $objSearch;
        }

        return false;
    }
}