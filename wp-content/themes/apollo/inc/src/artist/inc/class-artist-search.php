<?php
/** @Ticket #13364 */
require_once APOLLO_TEMPLATES_DIR. '/pages/lib/class-apollo-page-module.php';

class Apollo_Artist_Page extends Apollo_Page_Module {

    public static $_order_by;
    public static $_order = 'ASC';
    protected $artistMember = '';

    /**
     * @return string
     */
    public function getArtistMember()
    {
        return $this->artistMember;
    }


    public function __construct($isCounting = false) {

        /* @ticket #15646 */
        self::setIsCounting($isCounting);

        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ARTIST_PT ) ) {
            wp_safe_redirect( '/' );
        }

        parent::__construct();

        $order = of_get_option( Apollo_DB_Schema::_ARTIST_LISTING_ORDER,'ALPHABETICAL_ASC');

        switch ( $order ) {
            case 'ALPHABETICAL_ASC':
                Apollo_Artist_Page::$_order_by    = 'tblMetaSort.meta_value';
                Apollo_Artist_Page::$_order = 'ASC';
                break;

            case 'ALPHABETICAL_DESC':
                Apollo_Artist_Page::$_order_by    = 'tblMetaSort.meta_value';
                Apollo_Artist_Page::$_order = 'DESC';
                break;


            default:
                Apollo_Artist_Page::$_order_by    = 'tblMetaSort.meta_value';
                Apollo_Artist_Page::$_order = 'ASC';
                break;
        }
    }

    public function search() {

        $arr_params = array(
            'post_type'         => Apollo_DB_Schema::_ARTIST_PT,
            'posts_per_page'    => self::getIsCounting() ? 1 : Apollo_App::aplGetPageSize($this->pagesize , Apollo_DB_Schema::_ARTIST_NUM_ITEMS_LISTING_PAGE),
            'paged'             => self::getIsCounting() ? 1 : $this->page,
            'post_status'       => array('publish'),
        );

        if($keyword = self::getKeyword()) {
            $arr_params['s'] =  $keyword;
        }

        $arr_tax_query = array();
        if(isset($_GET['term']) && !empty($_GET['term'])) {
            $arr_tax_query[] = array(
                'taxonomy'=>'artist-type',
                'terms' => array($_GET['term']),
            );
        }

        if(isset($_GET['artist_medium']) && !empty($_GET['artist_medium'])) {
            $arr_tax_query[] = array(
                'taxonomy'  =>Apollo_DB_Schema::_ARTIST_MEDIUM_TAX,
                'terms'     => array($_GET['artist_medium']),
            );
        }

        if(isset($_GET['artist_style']) && !empty($_GET['artist_style'])) {
            $arr_tax_query[] = array(
                'taxonomy'  =>Apollo_DB_Schema::_ARTIST_STYLE_TAX,
                'terms'     => array($_GET['artist_style']),
            );
        }

        $arr_params['tax_query'] = $arr_tax_query;

        // Set new offset if this is ajax action
        $this->addOffsetToParams($arr_params);

        add_filter('posts_where', array($this, 'filter_where_artist_tbl'), 10, 1);
        add_filter('posts_join', array(__CLASS__, 'filter_join_artist_tbl'), 10, 1);
        add_filter('posts_orderby', array(__CLASS__, 'filter_order_artist_tbl'), 10, 1);
        add_filter( 'posts_search', array(__CLASS__, 'posts_search'), 10, 1);
        add_filter( 'posts_groupby', array($this, 'filter_groupby'), 10, 1 );

        $this->result = query_posts($arr_params);

        Apollo_Next_Prev::updateSearchResult($GLOBALS['wp_query']->request,Apollo_DB_Schema::_ARTIST_PT);
        remove_filter('posts_orderby', array(__CLASS__, 'filter_order_artist_tbl'), 10);
        remove_filter('posts_join', array(__CLASS__, 'filter_join_artist_tbl'), 10);
        remove_filter('posts_where', array($this, 'filter_where_artist_tbl'), 10);
        remove_filter( 'posts_search', array(__CLASS__, 'posts_search'), 10, 1);
        remove_filter( 'posts_groupby', array($this, 'filter_groupby'), 10, 1 );

        if (isset($_GET['vulh'])) {
            echo $GLOBALS['wp_query']->request;
        }

        $this->setTotal($GLOBALS['wp_query']->found_posts);
        $this->total_pages = ceil($this->total / $this->pagesize);

        $this->template = APOLLO_TEMPLATES_DIR. '/artists/listing-page/'. $this->_get_template_name(of_get_option(Apollo_DB_Schema::_ARTIST_DEFAULT_VIEW_TYPE));

        $this->resetPostData();
    }

    public function filter_where_artist_tbl ($where) {
        global $wpdb;

        if ( $this->getArtistMember() == 'premium'){
            $where .= " AND
            $wpdb->posts.ID IN (
                SELECT apollo_artist_id FROM {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}}
                WHERE meta_key = '".Apollo_DB_Schema::_ARTIST_MEMBER_ONLY."' AND meta_value = 'yes'
            )";
        }

        if ( $this->getArtistMember() == 'normal'){
            $where .= " AND
            $wpdb->posts.ID NOT IN (
                SELECT apollo_artist_id FROM {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}}
                WHERE meta_key = '".Apollo_DB_Schema::_ARTIST_MEMBER_ONLY."' AND meta_value = 'yes'
            )";
        }
        /* end */

        $where .= " AND
            $wpdb->posts.ID NOT IN (
                SELECT apollo_artist_id FROM {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}}
                WHERE meta_key = '".Apollo_DB_Schema::_APL_ARTIST_DO_NOT_LINKED."' AND meta_value = 'yes'
            )
        ";

        // JUST LAST NAME
        if(isset($_GET['last_name']) && !empty($_GET['last_name'])) {

            $last_name = $_GET['last_name'];
            $wpdb->escape_by_ref($last_name);
            return $where ." AND CAST(tblMetaSort.meta_value AS CHAR) LIKE '". $last_name ."%'";
        }

        // OTHER CASE
        if(isset($_GET['city']) && !empty($_GET['city'])) {
            $city = $_GET['city'];

            $wpdb->escape_by_ref($city);
            $where .= " AND CAST(mt_city.meta_value AS CHAR) LIKE '". $city ."%' ";
        }
        
        if(isset($_GET['zip']) && !empty($_GET['zip'])) {
            $zip = $_GET['zip'];

            $wpdb->escape_by_ref($zip);
            $where .= " AND CAST(mt_zip.meta_value AS CHAR) LIKE '". $zip ."%' ";
        }
        
        $where .= " AND {$wpdb->posts}.post_type = '".Apollo_DB_Schema::_ARTIST_PT."' ";
            
        return $where;
    }

    public static function filter_join_artist_tbl ($join) {
        global $wpdb;
        
        // Click on a letter bellow the search box
        if((isset($_GET['last_name']) && !empty($_GET['last_name']))) { // because city need join with meta table
            return $join . " INNER JOIN {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}} as tblMetaSort ON ({$wpdb->posts}.ID = tblMetaSort.apollo_artist_id) AND tblMetaSort.meta_key = '" . Apollo_DB_Schema::_APL_ARTIST_LNAME . "'";
        }

        // NOT LAST NAME
        $join .= " LEFT JOIN {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}} as tblMetaSort ON ({$wpdb->posts}.ID = tblMetaSort.apollo_artist_id) AND tblMetaSort.meta_key = '" . Apollo_DB_Schema::_APL_ARTIST_LNAME . "'";

        if(isset($_GET['city']) && !empty($_GET['city'])) {
            $join .=
                    " INNER JOIN {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}} mt_city ON "
                    . "(mt_city.meta_key = '". Apollo_DB_Schema::_APL_ARTIST_CITY ."' AND {$wpdb->posts}.ID = mt_city.apollo_artist_id)";
        }
        
        if(isset($_GET['zip']) && !empty($_GET['zip'])) {
            $join .=
                    " INNER JOIN {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}} mt_zip ON "
                    . "(mt_zip.meta_key = '". Apollo_DB_Schema::_APL_ARTIST_ZIP ."' AND {$wpdb->posts}.ID = mt_zip.apollo_artist_id)";
        }

        /**
         * @Ticket @13827
         * Render join for custom search fields
        */
        $join .= self::renderJoinCustomFieldSearch(Apollo_Tables::_APOLLO_ARTIST_META,  'apollo_artist_id');

        $join .= self::getJoinTaxByKeyword();
        
        return $join;
    }

    /* last name is not query with city in same query: Good to know that. because we don't need join an extra query to sort by last_name  */
    public static function filter_order_artist_tbl ($order) {

        if (self::getIsCounting()) {
            return false;
        }

        return sprintf("%s  %s", Apollo_Artist_Page::$_order_by, Apollo_Artist_Page::$_order);
    }

    /**
     * @ticket #17079: 0002366: wpdev19 - [Artist] Separate member artists and normal artists in two tabs
     * @param $artistMember
     * @return string
     */
    public function get_template_by_artist_member($artistMember){
        return untrailingslashit(get_bloginfo('url')) . add_query_arg(array(
                'onepage' => $this->is_onepage(),
                'artist_member' => $artistMember,
                'page' => 1
            ));
    }

    /**
     * @ticket #17079: 0002366: wpdev19 - [Artist] Separate member artists and normal artists in two tabs
     * @return string
     */
    public function setArtistMember($member){
        $this->artistMember = $member;
    }
}
