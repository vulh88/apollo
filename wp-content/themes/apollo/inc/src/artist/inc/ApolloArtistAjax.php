<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class ApolloArtistAjax {
    public function __construct()
    {

        $ajax_events = array(
            'assign_event_to_artist' => true,
            'artist_unassign_events_from_selected' => true,
            'get_more_events_assign_to_artist' => true,
            'get_more_events_upcomming_past' => true,
        );


        foreach ($ajax_events as $ajax_event => $nopriv) {
            add_action('wp_ajax_apollo_' . $ajax_event, array($this, $ajax_event));

            if ($nopriv)
                add_action('wp_ajax_nopriv_apollo_' . $ajax_event, array($this, $ajax_event));
        }

        if (Apollo_App::hasWPLM()) {
            global $sitepress;
            if (method_exists($sitepress, 'switch_lang') && isset($_GET['lang']) && $_GET['lang'] !== $sitepress->get_default_language()) {
                $sitepress->switch_lang($_GET['lang'], true);
            }
        }

    }

    /**
     * Ajax - assign event list to artist
     * @ticket #14485
     */
    public function assign_event_to_artist()
    {
        $user_id = get_current_user_id();
        if (!$user_id) {
            wp_send_json(array(
                'msg' => __('Please login to use this feature', 'apollo'),
                'status' => 'failed'
            ));
            exit;
        }

        $event_id = isset($_GET['eid']) ? Apollo_App::clean_data_request($_GET['eid']) : '';
        if (!$event_id) {
            wp_send_json(array(
                'msg' => __('Event not found', 'apollo'),
                'status' => 'failed'
            ));
            exit;
        }

        /* @ticket #15248 */
        $artistID = 0;
        if (isset($_GET['artistId']) && $_GET['artistId'] > 0) {
            $artistID = (int) $_GET['artistId'];
        }


        if (!Apollo_User::isAssociatedItems($artistID, Apollo_DB_Schema::_ARTIST_PT)) {
            wp_send_json(array(
                'msg' => __('Permission denies', 'apollo'),
                'status'    => 'failed',
            ));
            exit;
        }

        if ($artist = get_artist($artistID)) {
            $artist->assignEvent($event_id);
            wp_send_json(array(
                'html' => __('Success', 'apollo'),
            ));
        }
        else {
            wp_send_json(array(
                'html' => __('Artist does not exist', 'apollo'),
                'status' => 'failed'
            ));
        }

    }

    /**
     * Ajax - remove event list associated with artist
     * @ticket #14485
     */
    public function artist_unassign_events_from_selected()
    {
        $user_id = get_current_user_id();
        if (!$user_id) {
            wp_send_json(array(
                'status'    => 'failed',
                'msg' => __('Please login to use this feature', 'apollo'),
            ));
            exit;
        }

        /* @ticket #15248 */
        $artistID = 0;
        if (isset($_GET['artistId']) && $_GET['artistId'] > 0) {
            $artistID = (int) $_GET['artistId'];
        }

        if (!Apollo_User::isAssociatedItems($artistID, Apollo_DB_Schema::_ARTIST_PT)) {
            wp_send_json(array(
                'msg' => __('Permission denies', 'apollo'),
                'status'    => 'failed',
            ));
            exit;
        }


        $event_ids = $_GET['eids'];

        if($artist = get_artist($artistID)) {
            $artist->unAssignEvent($event_ids);
            wp_send_json(array(
                'html' => __('Success', 'apollo'),
            ));
        } else {
            wp_send_json(array(
                'html' => __('Artist does not exist', 'apollo'),
                'status' => 'failed'
            ));
        }
    }

    /**
     * Ajax - get more event associated with artist
     * @ticket #14485
     */
    public function get_more_events_assign_to_artist()
    {
        $page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $pagesize = isset($_REQUEST['pagesize']) ? (int)$_REQUEST['pagesize'] : 1;

        $user_id = get_current_user_id();
        if (!$user_id) {
            wp_send_json(array(
                'html' => '',
            ));
            exit;
        }

        /* @ticket #15248 */
        $artistID = 0;
        if (isset($_GET['artistId']) && $_GET['artistId'] > 0) {
            $artistID = (int) $_GET['artistId'];
        }

        if (!Apollo_User::isAssociatedItems($artistID, Apollo_DB_Schema::_ARTIST_PT)) {
            wp_send_json(array(
                'msg' => __('Permission denies', 'apollo'),
                'status'    => 'failed',
            ));
            exit;
        }

        if (!$user_artist = get_artist($artistID)) {
            wp_send_json(array(
                'html' => __('Artist does not exist', 'apollo'),
                'status' => 'failed'
            ));
            exit;
        }

        $events = $user_artist->getAssociatedEvents(true, $page, $pagesize);
        $html = '';
        if(isset($events['data'])) {
            $listEvent = $events['data'];
            $html = include APOLLO_TEMPLATES_DIR . '/artists/listing-page/selected-events.php';
        }
        wp_send_json(array(
            'html' => $html,
        ));
    }

    /**
     * Ajax - get more event associated with artist
     * @ticket #14485
     */
    public function get_more_events_upcomming_past()
    {
        $page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $artist_id = isset($_GET['artist_id']) ? $_GET['artist_id'] : '';
        $type = isset($_GET['type']) ? $_GET['type'] : '';
        if (!$artist_id || empty($type)) {
            wp_send_json(array(
                'html' => '',
            ));
            exit;
        }

        $artist = get_artist($artist_id);
        $upComingEvents = $artist->getAssociatedUpPastEvents($page, ($type == 'upcomming' ? true : false));
        $html = Apollo_App::renderHTML(APOLLO_TEMPLATES_DIR. '/content-single/artists/partial/artist-events.php', $upComingEvents['data']);
        $page ++;

        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_get_more_events_upcomming_past&type=' . $type. '&module=artist&page=' . $page. '&artist_id=' .
                $artist_id),
            'html' => $html,
            'have_more' => $upComingEvents['have_more']
        ));
    }
}

new ApolloArtistAjax();
