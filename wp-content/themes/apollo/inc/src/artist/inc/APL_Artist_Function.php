<?php

class APL_Artist_Function
{
    /**
     * @Ticket #13029
     * @param $id
     * @param $user_nicename
     * @param $user_id
     */
    public static function update_artists_usermeta($id, $user_nicename, $user_id) {
        global $wpdb;

        $sql_nicename = "DELETE FROM " . $wpdb->{Apollo_Tables::_APOLLO_ARTIST_META} ." WHERE meta_key='" . Apollo_DB_Schema::_APL_ARTIST_ASSOCIATION_USER_NICENAME . "' AND meta_value='" . $user_nicename . "' ; " ;
        $sql_user_id = "DELETE FROM " . $wpdb->{Apollo_Tables::_APOLLO_ARTIST_META} ." WHERE meta_key='" . Apollo_DB_Schema::_APL_ARTIST_ASSOCIATION_USER_ID . "' AND meta_value='" . $user_id . "'" ;
        $wpdb->query($sql_nicename);
        $wpdb->query($sql_user_id);

        if (!empty($id)) {
            update_apollo_meta($id, Apollo_DB_Schema::_APL_ARTIST_ASSOCIATION_USER_ID, $user_id);
            update_apollo_meta($id, Apollo_DB_Schema::_APL_ARTIST_ASSOCIATION_USER_NICENAME, $user_nicename);
        }
    }

    /**
     * Get artists
     * @param int $offset
     * @param string $text_search
     * @param bool $only_show_art_display_public
     * @return array
     *
     */

    public  static function getListArtists($offset = 0, $text_search = '', $only_show_art_display_public = true){
        global $wpdb;
        $sql = "SELECT SQL_CALC_FOUND_ROWS p.ID, p.post_title FROM {$wpdb->posts} p ";

        if($only_show_art_display_public) {
            $sql .= " INNER JOIN {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}} mt_artisrt ON mt_artisrt.meta_key ='". Apollo_DB_Schema::_APL_ARTIST_DISPLAY_PUBLIC ."' AND mt_artisrt.apollo_artist_id = p.ID AND mt_artisrt.meta_value = 'yes' ";
        }
        $sql .= " LEFT JOIN {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}} mt_lname ON mt_lname.meta_key ='". Apollo_DB_Schema::_APL_ARTIST_LNAME ."' AND mt_lname.apollo_artist_id = p.ID
            WHERE p.post_type = '".Apollo_DB_Schema::_ARTIST_PT."'
            AND p.post_status = 'publish' ";

        $limit = Apollo_Display_Config::APL_PUBLIC_ART_ARTISTS_LIMIT;

        if(!empty($text_search)) {
            $sql .= " AND (p.post_title LIKE '%" . $text_search . "%' OR (mt_lname.meta_key = '" . Apollo_DB_Schema::_APL_ARTIST_LNAME . "' AND mt_lname.meta_value LIKE '%" . $text_search . "%') )";
        }

        $sql .= "
            ORDER BY mt_lname.meta_value ASC
            LIMIT " . $limit . "
            OFFSET " . intval($offset);

        $artist = $wpdb->get_results( $sql );
        $_arr = array();
        if ( $artist ) {
            foreach ( $artist as $e ) {
                $_arr[$e->ID] = $e->post_title;
            }
        }
        $count_result = $wpdb->get_var("SELECT FOUND_ROWS()");
        return array(
            'data' => $_arr,
            'total' => $count_result,
            'have_more' => count($artist) + $offset < $count_result,
        );
    }

    /**
     * @Ticket #14774 - Get list artists with pagination.
     * @param int $offset
     * @param string $textSearch
     * @param string $event_id
     * @return array
     */
    public static function getListArtistForEvent( $offset = 0, $textSearch = '', $event_id = '' ) {
        global $wpdb;

        $sql = "SELECT SQL_CALC_FOUND_ROWS p.ID, p.post_title". ($event_id ? ", ae.artist_id " : "") ." FROM {$wpdb->posts} p ";

        if ($event_id) {
            $sql .= " LEFT JOIN {$wpdb->{Apollo_Tables::_APL_ARTIST_EVENT}} ae ON  ae.artist_id = p.ID  AND ae.event_id  = " . $event_id;
        }

        $sql .= " LEFT JOIN {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}} mt_lname ON mt_lname.meta_key ='". Apollo_DB_Schema::_APL_ARTIST_LNAME ."' AND mt_lname.apollo_artist_id = p.ID
            WHERE p.post_type = '".Apollo_DB_Schema::_ARTIST_PT."'
            AND p.post_status = 'publish' ";

        $limit = Apollo_Display_Config::APL_PUBLIC_ART_ARTISTS_LIMIT;

        if(!empty($textSearch)) {
            $sql .= " AND (p.post_title LIKE '%" . $textSearch . "%' OR (mt_lname.meta_key = '" . Apollo_DB_Schema::_APL_ARTIST_LNAME . "' AND mt_lname.meta_value LIKE '%" . $textSearch . "%') )";
        }

        if ($event_id) {
            $sql .= " ORDER BY ae.artist_id DESC, p.post_title ASC ";
        } else {
            $sql .= " ORDER BY p.post_title ASC ";
        }
        $sql .= "
            LIMIT " . $limit . "
            OFFSET " . intval($offset);

        $artist = $wpdb->get_results( $sql, ARRAY_A );
        $count_result = $wpdb->get_var("SELECT FOUND_ROWS()");
        return array(
            'data' => $artist,
            'total' => $count_result,
            'have_more' => count($artist) + $offset < $count_result,
        );


    }

    /**
     * @Ticket #14774 - Render multiple artists for event.
     * @param array $artists
     * @param bool $frontend
     * @return string
     */
    public static function renderArtistsForEvent( $artists = array(), $frontend = false ) {
        if(empty($artists)){
            return '';
        }
        $result = '';
        foreach ( $artists as $val ) {
            $checked = $val['artist_id'] ? 'checked' : '';

            $post_edit_link = get_edit_post_link($val['ID']);
            if($post_edit_link != null && !$frontend){
                $post_edit_link = '<a class="edit-link" target="_blank" href="' . $post_edit_link . '">' . __("Edit", "apollo") . '</a>';
            }else{
                $post_edit_link = '';
            }
            $result .= '<li>
            <input ' . $checked . ' type="checkbox" class="artist-checked" value="' . $val['ID'] . '" />
            <span>' . $val['post_title'] . '</span> ' . $post_edit_link . '</li>';
        }
        return $result;
    }


    /**
     * render artists selected
     * @param $artists
     * @return string
     */
    public static function renderArtistsSelected($artists = array()){
        if(empty($artists)){
            return '';
        }
        $result = array();
        foreach ( $artists as $artist) {
            array_push($result,'<a class="artist-default" target="_blank" href="' . get_edit_post_link($artist->ID, '') . '">' . $artist->post_title . '</a>');
        }
        if(!empty($result)){
            return join(", ", $result);
        }else{
            return '';
        }
    }

    /**
     * render artists selected for frontend
     * @param $artists
     * @return string
     */
    public static function renderArtistsSelectedFrontend($artists = array()){
        if(empty($artists)){
            return '';
        }
        $result = array();
        foreach ( $artists as $artist) {
            array_push($result,'<a class="artist-default" target="_blank" href="' . get_permalink($artist->ID) . '">' . $artist->post_title . '</a>');
        }
        if(!empty($result)){
            return join(", ", $result);
        }else{
            return '';
        }
    }

    /**
     * #@Ticket #14774 - Update multiple artists for event.
     * @param $eventID
     * @param $listChecked
     * @param $listUnchecked
     */
    public static function saveArtistEvent($eventID, $listChecked, $listUnchecked) {
        if (empty($listChecked) && empty($listUnchecked)) {
            return;
        }
        $listUnchecked = !empty($listUnchecked) ? explode(",", $listUnchecked) : array();
        $listChecked = !empty($listChecked) ? explode(",", $listChecked) : array();
        $listRemove = array_merge($listUnchecked, $listChecked);

        $apl_query  = new Apl_Query( Apollo_Tables::_APL_ARTIST_EVENT );


        if (!empty($listRemove)) {
            $listIDS = implode(",", $listRemove);
            $apl_query->delete( " event_id = {$eventID} AND artist_id IN ( {$listIDS} )" );
        }

        $art_field  = Apollo_DB_Schema::_APL_A_ART_ID;

        if (!empty($listChecked)){
            foreach ($listChecked as $k => $v) {
                $apl_query->insert( array(
                    $art_field => $v,
                    'artist_ordering' => $k,
                    'event_id'  => $eventID,
                ));
            }
        }
    }

    /**
     * Check artist member
     * @param $artistId
     * @return bool
     */
    public static function checkArtistMember($artistId) {
        $artistActivateMember = of_get_option(Apollo_DB_Schema::_ARTIST_ACTIVE_MEMBER_FIELD, false);
        if ($artistActivateMember) {
            if (!$artistId) {
                return false;
            }
            $artistMember = Apollo_App::apollo_get_meta_data( $artistId, Apollo_DB_Schema::_ARTIST_MEMBER_ONLY);
            if (empty($artistMember) || $artistMember == 'no') {
                return false;
            }
        }
        return true;
    }
}