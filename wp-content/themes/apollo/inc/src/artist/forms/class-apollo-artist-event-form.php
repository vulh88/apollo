<?php

/**
 * Class Apollo_Artist_Event_Form
 *
 * @ticket #15248 - [CF] 20180307 - [2049#c12311] Manage events assignment for each artist in multiple selections mode
 */
class Apollo_Artist_Event_Form extends Apollo_Artist_Base_Form
{
    public function formInit()
    {
        $arrData = $this->formData;

        $formElementItems = array(
            'ID' =>  array(
                'type'         => 'Hidden',
                'name'         => 'ID',
                'place_holder' => __('Id', 'apollo'),
                'value'        => isset($arrData['ID']) ? $arrData['ID'] : '',
                'class'        => 'inp inp-txt',
                'title'        => __('Name', 'apollo'),
                'validate'     => true,
                'id'           => 'artist_id',
                'container'    => 'ebl',
            ),
            'target' =>  array(
                'type'         => 'Hidden',
                'name'         => 'ID',
                'place_holder' => __('target', 'apollo'),
                'value'        => Apollo_DB_Schema::_ARTIST_PT,
                'class'        => 'inp inp-txt',
                'title'        => __('Name', 'apollo'),
                'validate'     => true,
                'id'           => 'artist_target',
                'container'    => 'ebl',
            ),
        );

        $this->elements = $formElementItems;
    }

    public function getMethod()
    {
        $id = Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_ARTIST_PT);
        if(!empty($id)) {
            $this->formData['ID'] = $id;
        }
    }

    public function postMethod()
    {
    }

    public function setCurDataHorizontalTab()
    {
        $this->activatedTab = APL_Dashboard_Hor_Tab_Options::ARTIST_EVENTS_URL;

        if (intval(get_query_var('_is_agency_educator_page', 0))) {
            $this->activatedTab = APL_Dashboard_Hor_Tab_Options::AGENCY_ARTIST_EVENTS_URL;
        }
    }

    /**
     * Set nonce info
     *
     * @return void
     */
    public function setNonceInfo()
    {
        $this->nonceName   = Apollo_Const::_APL_NONCE_NAME;
        $this->nonceAction = Apollo_Const::_APL_NONCE_ACTION_ARTIST_EVENT_PAGE;
    }
}
