<?php

/**
 * Class Apollo_Artist_Base_Form
 *
 * @ticket #15248 - [CF] 20180307 - [2049#c12311] Manage events assignment for each artist in multiple selections mode
 */


/** @Ticket #17126 */
require_once APOLLO_SHORTCODE_DIR. '/class-apollo-artist-member-requirement-shortcode.php';

class Apollo_Artist_Base_Form extends Apollo_Form
{
    public function formInit()
    {
    }

    public function getMethod()
    {
    }

    public function postMethod()
    {
    }

    /**
     * Set nonce info
     *
     * @return void
     */
    public function setNonceInfo()
    {
    }

    public function getTabData()
    {
        return intval(get_query_var('_is_agency_educator_page', 0))
            ? array(
                'profile' => APL_Dashboard_Hor_Tab_Options::AGENCY_ARTIST_PROFILE_URL,
                'photo'   => APL_Dashboard_Hor_Tab_Options::AGENCY_ARTIST_PHOTOS_URL,
                'video'   => APL_Dashboard_Hor_Tab_Options::AGENCY_ARTIST_VIDEO_URL,
                'audio'   => APL_Dashboard_Hor_Tab_Options::AGENCY_ARTIST_AUDIO_URL,
                'event'   => APL_Dashboard_Hor_Tab_Options::AGENCY_ARTIST_EVENTS_URL,
            )
            : array(
                'profile' => APL_Dashboard_Hor_Tab_Options::ARTIST_PROFILE_URL,
                'photo'   => APL_Dashboard_Hor_Tab_Options::ARTIST_PHOTOS_URL,
                'video'   => APL_Dashboard_Hor_Tab_Options::ARTIST_VIDEO_URL,
                'audio'   => APL_Dashboard_Hor_Tab_Options::ARTIST_AUDIO_URL,
                'event'   => APL_Dashboard_Hor_Tab_Options::ARTIST_EVENTS_URL,
            );
    }

    public function getTabLinks()
    {
        $tabData = $this->getTabData();
        $tabLinks = array(
            $tabData['profile'] => __("PROFILE", "apollo"),
            $tabData['photo']   => __("PHOTO", "apollo"),
            $tabData['video']   => __("VIDEO", "apollo"),
            $tabData['audio']   => __("AUDIO", "apollo"),
            $tabData['event']   => __("EVENT", "apollo"),
        );

        /**
         * @ticket #18695: [CF] 20181221 - Add the option for image upload to the main artist profile form.
         */
        $locationPhotoForm = of_get_option(APL_Theme_Option_Site_Config_SubTab::_ARTIST_PHOTO_FORM_LOCATION, 1);
        if($locationPhotoForm == 2){
            unset($tabLinks[$tabData['photo']]);
        }

        return $tabLinks;
    }

    /**
     * Determine if the user is authorized to make this request.
     * @ticket #15847 - huyenln
     * @return bool
     */
    public function authorize()
    {
        $postId = Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_ARTIST_PT);

        // Prevent security error by updating the value of ID hidden input
        if (!empty($_POST['ID']) && $_POST['ID'] != $postId) {
            return false;
        }

        if (empty($postId)){
           return true;
        }

        return Apollo_User::isAssociatedItems($postId, Apollo_DB_Schema::_ARTIST_PT);
    }

    public function setTitleArtistElementForm($title){
        $postId = Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_ARTIST_PT);

        return APL_Artist_Function::checkArtistMember(isset($postId)? $postId :'') ? $title : '';
    }
}
