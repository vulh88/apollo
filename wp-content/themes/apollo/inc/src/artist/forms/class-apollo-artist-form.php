<?php

/**
 * Created by PhpStorm.
 * User: vulh
 * Date: 5/7/2015
 * Time: 11:41 AM
 */
class Apollo_Artist_Form extends Apollo_Artist_Base_Form
{

    protected $isAddingNewArtist = false;

    /**
     * @return boolean
     */
    public function isIsAddingNewArtist()
    {
        return $this->isAddingNewArtist;
    }

    /**
     * @param boolean $isAddingNewArtist
     */
    public function setIsAddingNewArtist($isAddingNewArtist)
    {
        $this->isAddingNewArtist = $isAddingNewArtist;
    }
    
    /* Init form element then set data for them */
    public function formInit()
    {
        global $wpdb;
        $stateTbl = $wpdb->{Apollo_Tables::_APL_STATE_ZIP};

        $arrData = $this->formData;

        $stateVal = '';
        $cityVal = '';
        if( $arrData ){
            $stateVal = isset($arrData[Apollo_DB_Schema::_APL_ARTIST_STATE]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_STATE] : '';
            $cityVal = isset($arrData[Apollo_DB_Schema::_APL_ARTIST_CITY]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_CITY]: '';
        } else {
            $list_states = Apollo_App::getListState();
            $default_states =  of_get_option(Apollo_DB_Schema::_APL_DEFAULT_STATE);
            $cityVal = of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY);
            if( $default_states && in_array($default_states,$list_states) ) {
                $stateVal = $default_states;
            } else if(count($list_states) == 1) {
                $stateVal = key($list_states);
            }
        }

        $addNewStateType = isset($_POST['add_new_state']) ? $_POST['add_new_state'] : 0;
        if ($this->isSaveSuccess) {
            $addNewStateType = 0;
        }

        $defaultHidden = $addNewStateType == 1 ? 'hidden' : '';
        $requiredPriState = $addNewStateType == 1 ? Apollo_Form::_FORM_REQUIRED : '';

        $defaultBtnText = $addNewStateType == 1 ? __('BACK TO DEFAULT STATE/CITY/ZIP DATA', 'apollo') : __('ADD NEW STATE/CITY/ZIP DATA', 'apollo');

        $addNewHidden = $addNewStateType == 0 ? 'hidden' : '';
        $labelCounty = of_get_option(Apollo_DB_Schema::_ARTIST_ENABLE_COUNTY_REQUIREMENT) ? 'County (*)' : 'County';

        /** @Ticket #18747 */
        $requiredDescription = of_get_option(Apollo_DB_Schema::_ARTIST_FE_ENABLE_REQUIRED_DESCRIPTION, 1);

        // Cat required
        $catRequired = of_get_option(Apollo_DB_Schema::_ARTIST_FE_ENABLE_REQUIRED_TYPE, 1);

        $requiredEmail = of_get_option(Apollo_DB_Schema::_ARTIST_ENABLE_EMAIL_REQUIREMENT, 0);
        $requiredPhone = of_get_option(Apollo_DB_Schema::_ARTIST_ENABLE_PHONE_REQUIREMENT, 0);
        $requiredAddress = of_get_option(Apollo_DB_Schema::_ARTIST_ENABLE_ADDRESS_REQUIREMENT, 0);
        $requiredState = $addNewStateType ? 0 : of_get_option(Apollo_DB_Schema::_ARTIST_ENABLE_STATE_REQUIREMENT, 0);
        $requiredCity = $addNewStateType ? 0 : of_get_option(Apollo_DB_Schema::_ARTIST_ENABLE_CITY_REQUIREMENT, 0);
        $requiredZip = $addNewStateType ? 0 : of_get_option(Apollo_DB_Schema::_ARTIST_ENABLE_ZIP_REQUIREMENT, 0);
        $requiredClass = 'validate[required]';
        /** @Ticket #13364 */
        $enableCounty = of_get_option(Apollo_DB_Schema::_ARTIST_ENABLE_COUNTY, false);

        $enableSearchWdStyle = of_get_option(APL_Theme_Option_Site_Config_SubTab::ARTIST_ENABLE_STYLE_SEARCH_WIDGET,1);
        $enableSearchWdMedium = of_get_option(APL_Theme_Option_Site_Config_SubTab::ARTIST_ENABLE_MEDIUM_SEARCH_WIDGET, 1);

        $searchWdTypeLabel = of_get_option(APL_Theme_Option_Site_Config_SubTab::ARTIST_TYPE_SEARCH_WIDGET_LABEL, __("Artist Categories", "apollo"));
        $searchWdStyleLabel = of_get_option(APL_Theme_Option_Site_Config_SubTab::ARTIST_STYLE_SEARCH_WIDGET_LABEL, __("Artist Style", "apollo"));
        $searchWdMediumLabel = of_get_option(APL_Theme_Option_Site_Config_SubTab::ARTIST_MEDIUM_SEARCH_WIDGET_LABEL, __("Artist Medium", "apollo"));


        /**
         * Maximum characters
         * @ticket #17181
         */
        $maxCharactersOtherTypes = (int) of_get_option(APL_Theme_Option_Site_Config_SubTab::ARTIST_FORM_MAX_OTHER_TYPE, APL_Theme_Option_Site_Config_SubTab::ARTIST_FORM_MAX_OTHER_DEFAULT);
        $maxCharactersOtherStyle = (int) of_get_option(APL_Theme_Option_Site_Config_SubTab::ARTIST_FORM_MAX_OTHER_STYLE, APL_Theme_Option_Site_Config_SubTab::ARTIST_FORM_MAX_OTHER_DEFAULT);
        $maxCharactersOtherMedium = (int) of_get_option(APL_Theme_Option_Site_Config_SubTab::ARTIST_FORM_MAX_OTHER_MEDIUM, APL_Theme_Option_Site_Config_SubTab::ARTIST_FORM_MAX_OTHER_DEFAULT);
        /*@ticket: #17721 */
        $maxCharactersDesciption = (int) of_get_option(APL_Theme_Option_Site_Config_SubTab::ARTIST_FORM_MAX_DESCRIPTION_FIELD, APL_Theme_Option_Site_Config_SubTab::ARTIST_FORM_MAX_DESCRIPTION_DEFAULT);
        $artistMember = APL_Artist_Function::checkArtistMember(isset($arrData['ID']) ? $arrData['ID'] : '');
        $classArtistMember = $artistMember ? '' : 'hidden';

        $cityClass = $requiredCity ? 'event apl-territory-city custom-validate-field' : 'event apl-territory-city';
        $cityClass .= !$artistMember ? ' is-not-member' : '';

        $enableOtherState = of_get_option(Apollo_DB_Schema::_ARTIST_FE_ENABLE_OTHER_STATE, 0);
        $enableOtherCity = of_get_option(Apollo_DB_Schema::_ARTIST_FE_ENABLE_OTHER_CITY, 0);
        $enableOtherZip = of_get_option(Apollo_DB_Schema::_ARTIST_FE_ENABLE_OTHER_ZIP, 0);

        $locationInfoElement = array(
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk ',
                'container' => '',
            ),
            //title
            array(
                'type' => 'Title',
                'name' => 'title',
                'value' => __('Location Info', 'apollo'),
                'class' => '',

            ),

            Apollo_DB_Schema::_APL_ARTIST_STREET => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_APL_ARTIST_STREET,
                'place_holder' => $title = $requiredAddress ? __('Address (*)', 'apollo') : __('Address', 'apollo'),
                'container' => 'el-blk full '. $classArtistMember,
                'data_attributes' => array(
                    'data-errormessage-value-missing="'.__('Address is required','apollo').'"'
                ),
                'value' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_STREET]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_STREET] : '',
                'class' => 'inp inp-txt '. ($requiredAddress ? $requiredClass : ''),
                'title' => $title,
                'validate' => array(
                    'rule' => array(
                        ($requiredAddress && $artistMember) ? Apollo_Form::_FORM_REQUIRED : ''
                    )
                )
            ),

            Apollo_DB_Schema::_APL_ARTIST_REGION => array(
                'type' => 'Select',
                'name' => Apollo_DB_Schema::_APL_ARTIST_REGION,
                'place_holder' => __('Region', 'apollo'),
                'class' => 'event',
                'title' => __( 'Region', 'apollo' ),
                'value' => array(
                    'list_item' => Apollo_App::get_regions(false, false),
                    'selected_item' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_REGION]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_REGION] : ''
                ),
                'container' => 'el-blk full '. $classArtistMember,
                'id' => 'apl-regions',
                'display' => Apollo_App::showRegion(),
            ),

            Apollo_DB_Schema::_APL_ARTIST_COUNTRY => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_APL_ARTIST_COUNTRY,
                'place_holder' => __($labelCounty, 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_COUNTRY]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_COUNTRY] : '',
                'class' => 'inp inp-txt '. (of_get_option(Apollo_DB_Schema::_ARTIST_ENABLE_COUNTY_REQUIREMENT) ? $requiredClass : '') . ($enableCounty ? '' : ' hidden '),
                'title' => __($labelCounty, 'apollo'),
                'container' => 'el-blk full '. $classArtistMember,
                'data_attributes' => array(
                    'data-errormessage-value-missing="'.__('Artist county is required','apollo').'"'
                ),
                'validate' => array(
                    'rule' => array(
                        (of_get_option(Apollo_DB_Schema::_ARTIST_ENABLE_COUNTY_REQUIREMENT) && $artistMember) ? Apollo_Form::_FORM_REQUIRED : ''
                    )
                ),
            ),

            'add_new_state' => array(
                'type' => 'hidden',
                'name' => 'add_new_state',
                'value' => $addNewStateType
            ),

            'add_new_state_btn' => array(
                'type' => 'Button',
                'class' => 'add-new-state-btn btn-noW '. $classArtistMember,
                'title' => $defaultBtnText,
                'no_container' => true,
                'data' => array(
                    'data-prev' => __('ADD NEW STATE/CITY/ZIP DATA', 'apollo'),
                    'data-next' => __('BACK TO DEFAULT STATE/CITY/ZIP DATA', 'apollo'),
                ),
            ),


            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'el-blk state-block-1 '. $defaultHidden,
                'container' => '',
            ),
            Apollo_DB_Schema::_APL_ARTIST_STATE => array(
                'type' => 'Select',
                'container' => 'el-blk full '. $classArtistMember,
                'name' => Apollo_DB_Schema::_APL_ARTIST_STATE,
                'class' => $requiredState ? 'event apl-territory-state custom-validate-field' : 'event apl-territory-state',
                'title' => $requiredState ? __('State (*)', 'apollo') : __('State', 'apollo'),
                'data_attributes' => $requiredState ? array(
                    'data-custom-validate="select2"',
                    'data-errormessage-value-missing="' . __('State is required', 'apollo') . '"'
                ) : '',
                'value' => array(
                    'list_item' => Apollo_App::getStateByTerritory(false, $stateVal),
                    'selected_item' => $stateVal
                ),
                'id' => 'apl-us-states',
                'validate' => array(
                    'rule' => array(
                        ($requiredState && $artistMember) ? Apollo_Form::_FORM_REQUIRED : ''
                    )
                )
            ),


            Apollo_DB_Schema::_APL_ARTIST_CITY => array(
                'type' => 'Select',
                'name' => Apollo_DB_Schema::_APL_ARTIST_CITY,
                'id' => 'apl-us-cities',
                /**
                 * @ticket id 19197
                 * City field is displayed whether this artist is a member field or not. If it is a member field, will display on the left and zip is                    * displayed on the right then city is displayed in full wrapper
                */
                'container' => $artistMember ? ' hafl fl' : 'el-blk full',
                'value' => array(
                        /**
                         * @ticket id 19197
                         * display all cities when it is NOT a member field due to the state is suppressed
                         */
                    'list_item' => Apollo_App::getCityByTerritory(false, $artistMember ? $stateVal : '', false),
                    'selected_item' => $cityVal
                ),
                'data_attributes' => $requiredCity ? array(
                    'data-custom-validate="select2"',
                    'data-error-message="'.__('City is required','apollo').'"'
                ) : '',
                'class' => $cityClass,
                'title' => $requiredCity ? __('City (*)', 'apollo') : __('City', 'apollo'),
                'validate' => array(
                    'rule' => array(
                        $requiredCity ? Apollo_Form::_FORM_REQUIRED : ''
                    )
                )
            ),

            Apollo_DB_Schema::_APL_ARTIST_ZIP => array(
                'type' => 'Select',
                'name' => Apollo_DB_Schema::_APL_ARTIST_ZIP,
                'container' => 'hafl fr '. $classArtistMember,
                'value' => array(
                    'list_item' => Apollo_App::getZipByTerritory(false, $stateVal, $cityVal, false),
                    'selected_item' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_ZIP]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_ZIP] : ''
                ),
                'data_attributes' => $requiredZip ? array(
                    'data-custom-validate="select2"',
                    'data-error-message="' . __('Zip is required', 'apollo') . '"'
                ) : '',
                'id' => 'apl-us-zip',
                'class' => $requiredZip ? 'event apl-territory-zipcode custom-validate-field' : 'event apl-territory-zipcode',
                'title' => $requiredZip ? __('Zip (*)', 'apollo') : __('Zip', 'apollo'),
                'validate' => array(
                    'rule' => array(
                        ($requiredZip && $artistMember) ? Apollo_Form::_FORM_REQUIRED : ''
                    )
                )
            ),
        );

        /** @ticket #19687
         * [CF] 20190408 - Add text to create more separation between the address drop menus and the other address fields
         */
        if ($enableOtherState || $enableOtherCity || $enableOtherZip) {
            $locationInfoElement = array_merge($locationInfoElement, array(
                array(
                    'type' => 'Subtitle',
                    'value' => __("If your city, state and/or zip are not listed above, please fill in the fields below.", 'apollo'),
                    'class' => 'apl-subtitle-bold',
                )
            ));
        }

        /** @Ticket #19640 */
        if ($enableOtherState) {
            $locationInfoElement = array_merge($locationInfoElement, array(
                Apollo_DB_Schema::_APL_ARTIST_TMP_STATE => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_ARTIST_TMP_STATE,
                    'place_holder' => __('Other State', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_TMP_STATE]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_TMP_STATE] : '',
                    'class' => 'inp inp-txt',
                    'title' => __('Other State', 'apollo'),
                )
            ));
        }

        if ($enableOtherCity) {
            $locationInfoElement = array_merge($locationInfoElement, array(
                Apollo_DB_Schema::_APL_ARTIST_TMP_CITY => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_ARTIST_TMP_CITY,
                    'place_holder' => __('Other City', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_TMP_CITY]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_TMP_CITY] : '',
                    'class' => 'inp inp-txt',
                    'title' => __('Other City', 'apollo'),
                )
            ));
        }

        if ($enableOtherZip) {
            $locationInfoElement = array_merge($locationInfoElement, array(
                Apollo_DB_Schema::_APL_ARTIST_TMP_ZIP => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_ARTIST_TMP_ZIP,
                    'place_holder' => __('Other Zip Code', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_TMP_ZIP]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_TMP_ZIP] : '',
                    'class' => 'inp inp-txt',
                    'title' => __('Other Zip Code', 'apollo'),
                )
            ));
        }

        $moreElementItems = array(
            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),


            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'el-blk state-block-2 '. $addNewHidden,
                'container' => '',
            ),
            Apollo_DB_Schema::_APL_PRI_STATE => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_APL_PRI_STATE,
                'place_holder' => __('State (*)', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_APL_PRI_STATE]) ? $arrData[Apollo_DB_Schema::_APL_PRI_STATE] : '',
                'class' => 'inp inp-txt validate[required]',
                'data_attributes' => array(
                    'data-errormessage-value-missing="'.__('State is required','apollo').'"'
                ),
                'title' => __('State (*)', 'apollo'),
                'validate' => array(
                    'rule' => array(
                        $requiredPriState,
                        //$addNewStateType == 1 ? Apollo_Form::_FORM_EXISTS. ':'. $stateTbl. ',code, type="state"' : '',
                    )
                ),
            ),

            Apollo_DB_Schema::_APL_PRI_CITY => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_APL_PRI_CITY,
                'place_holder' => __('City (*)', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_APL_PRI_CITY]) ? $arrData[Apollo_DB_Schema::_APL_PRI_CITY] : '',
                'container' => 'hafl fl',
                'class' => 'inp inp-txt validate[required]',
                'data_attributes' => array(
                    'data-errormessage-value-missing="'.__('City is required','apollo').'"'
                ),
                'title' => __('City (*)', 'apollo'),
                'validate' => array(
                    'rule' => array(
                        $requiredPriState,
                    )
                ),
            ),

            Apollo_DB_Schema::_APL_PRI_ZIP => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_APL_PRI_ZIP,
                'place_holder' => __('Zip (*)', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_APL_PRI_ZIP]) ? $arrData[Apollo_DB_Schema::_APL_PRI_ZIP] : '',
                'container' => 'hafl fr',
                'class' => 'inp inp-txt validate[required]',
                'data_attributes' => array(
                    'data-errormessage-value-missing="'.__('Zip is required','apollo').'"'
                ),
                'title' => __('Zip (*)', 'apollo'),
                'validate' => array(
                    'rule' => array(
                        $requiredPriState
                    )
                ),
            ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            Apollo_DB_Schema::_APL_ARTIST_DISPLAY_ADDRESS => array(
                'type' => 'Checkbox',
                'name' => Apollo_DB_Schema::_APL_ARTIST_DISPLAY_ADDRESS,
                'class' => 'inp inp-txt',
                'container' => 'el-blk full '. $classArtistMember,
                'title' => of_get_option(Apollo_DB_Schema::_ARTIST_ADDRESS_LABEL, __('Check here if you DO NOT want your Address to appear on your Artist Profile page.', 'apollo')),
                'value' => array(
                    'list_item' => 'yes',
                    'selected_item' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_DISPLAY_ADDRESS]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_DISPLAY_ADDRESS] : ''
                ),
            ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),
        );

        $locationInfoElement = array_merge($locationInfoElement, $moreElementItems);

        $socialElement = array(
                array(
                    'type' => 'Div',
                    'name' => 'title',
                    'value' => '',
                    'class' => 'artist-blk',
                    'container' => '',
                ),
                //title
                array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => __('Website, Blog and Social Media Links (optional)', 'apollo'),
                    'class' => '',

                ),

                array(
                    'type' => 'Div',
                    'name' => 'title',
                    'value' => '',
                    'class' => 'el-blk',
                    'container' => '',
                ),
                Apollo_DB_Schema::_APL_ARTIST_WEBURL => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_ARTIST_WEBURL,
                    'place_holder' => __('Website URL', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_WEBURL]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_WEBURL] : '',
                    'container' => $artistMember ? 'hafl fl' : '', //show full textbox once only website url field
                    'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]]',
                    'title' => __('Website URL', 'apollo'),
                    'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_URL,

                        )
                    )
                ),
                Apollo_DB_Schema::_APL_ARTIST_BLOGURL => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_ARTIST_BLOGURL,
                    'place_holder' => __('Blog URL', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_BLOGURL]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_BLOGURL] : '',
                    'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]] ' . $classArtistMember,
                    'title' => __('Blog URL', 'apollo'),
                    'container' => 'hafl fr',
                    'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_URL,

                        )
                    )
                ),
                array(
                    'type' => 'CloseDiv',
                    'name' => 'title',
                    'value' => '',
                    'class' => '',
                    'container' => '',
                ),


                array(
                    'type' => 'Div',
                    'name' => 'title',
                    'value' => '',
                    'class' => 'el-blk ' . $classArtistMember,
                    'container' => '',
                ),
                Apollo_DB_Schema::_APL_ARTIST_INS => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_ARTIST_INS,
                    'place_holder' => __('Instagram URL', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_INS]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_INS] : '',
                    'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]]',
                    'title' => __('Instagram URL', 'apollo'),
                    'container' => 'hafl fl',
                    'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_URL,

                        )
                    )
                ),
                Apollo_DB_Schema::_APL_ARTIST_TW => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_ARTIST_TW,
                    'place_holder' => __('Twitter URL', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_TW]) ?
                        $arrData[Apollo_DB_Schema::_APL_ARTIST_TW] : '',
                    'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]]',
                    'title' => __('Twitter URL', 'apollo'),
                    'container' => 'hafl fr',
                    'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_URL,

                        )
                    )
                ),
                array(
                    'type' => 'CloseDiv',
                    'name' => 'title',
                    'value' => '',
                    'class' => '',
                    'container' => '',
                ),

                array(
                    'type' => 'Div',
                    'name' => 'title',
                    'value' => '',
                    'class' => 'el-blk ' . $classArtistMember,
                    'container' => '',
                ),
                Apollo_DB_Schema::_APL_ARTIST_PR => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_ARTIST_PR,
                    'place_holder' => __('Pinterest URL', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_PR]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_PR] : '',
                    'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]]',
                    'title' => __('Pinterest URL', 'apollo'),
                    'container' => 'hafl fl',
                    'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_URL,

                        )
                    )
                ),
                Apollo_DB_Schema::_APL_ARTIST_FB => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_ARTIST_FB,
                    'place_holder' => __('Facebook URL', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_FB]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_FB] : '',
                    'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]]',
                    'title' => __('Facebook URL', 'apollo'),
                    'container' => 'hafl fr',
                    'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_URL,

                        )
                    )
                ),
                array(
                    'type' => 'CloseDiv',
                    'name' => 'title',
                    'value' => '',
                    'class' => '',
                    'container' => '',
                ),

                array(
                    'type' => 'Div',
                    'name' => 'title',
                    'value' => '',
                    'class' => 'el-blk ' . $classArtistMember,
                    'container' => '',
                ),
                Apollo_DB_Schema::_APL_ARTIST_LK => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_ARTIST_LK,
                    'place_holder' => __('LinkedIn URL', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_LK]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_LK] : '',
                    'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]]',
                    'title' => __('LinkedIn URL', 'apollo'),
                    'container' => 'hafl fl',
                    'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_URL,
                        )
                    )
                ),

                array(
                    'type' => 'CloseDiv',
                    'name' => 'title',
                    'value' => '',
                    'class' => '',
                    'container' => '',
                ),
                array(
                    'type' => 'CloseDiv',
                    'name' => 'title',
                    'value' => '',
                    'class' => '',
                    'container' => '',
                ),
            );

        $categoryElement = array(
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),
            //title
            array(
                'type' => 'Title',
                'name' => 'title',
                'value' => $searchWdTypeLabel . ( $catRequired ? ' (*)' : ''),
                'class' => ''
            ),
            //end title
            'post_cat' => array(
                'title' => __('Artist Categories (*)', 'apollo'),
                'type' => 'Category_Group',
                'name' => Apollo_DB_Schema::_ARTIST_TYPE,
                'class' => 'event ' . ($catRequired ? 'custom-validate-field' : ''),
                'data_attributes' => array(
                    'data-custom-validate="multi-checkbox"',
                    'data-error-message="'.__('Please select at least ONE category type.','apollo').'"'
                ),
                'value' => isset($arrData[Apollo_DB_Schema::_ARTIST_TYPE]) ? $arrData[Apollo_DB_Schema::_ARTIST_TYPE] : '',
                'post_type' => Apollo_DB_Schema::_ARTIST_PT,
                'validate' => array(
                    'rule' => array(
                        $catRequired
                    )
                )
            ),

            Apollo_DB_Schema::_APL_ARTIST_ANOTHER_CAT => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_APL_ARTIST_ANOTHER_CAT,
                'value' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_ANOTHER_CAT]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_ANOTHER_CAT] : '',
                'class' => 'inp inp-txt relation-field'. sprintf(' validate[maxSize[%d]]', $maxCharactersOtherTypes),
                'title' => __('Other type not listed above:', 'apollo'),
            ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),
        );
        $styleElement = array(
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk ' . ($enableSearchWdStyle ? "" : 'hidden') . ' ' . $classArtistMember,
                'container' => '',
            ),
            //title
            array(
                'type' => 'Title',
                'name' => 'title',
                'value' => $searchWdStyleLabel,
                'class' => ''
            ),
            //end title
            'post_style' => array(
                'title' => __('What is your style?', 'apollo'),
                'type' => 'Category_Group',
                'name' => Apollo_DB_Schema::_ARTIST_STYLE_TAX,
                'class' => 'event',
                'value' => isset($arrData[Apollo_DB_Schema::_ARTIST_STYLE_TAX]) ? $arrData[Apollo_DB_Schema::_ARTIST_STYLE_TAX] : '',
                'tax' => 'artist-style',
            ),

            Apollo_DB_Schema::_APL_ARTIST_ANOTHER_STYLE => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_APL_ARTIST_ANOTHER_STYLE,
                'value' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_ANOTHER_STYLE]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_ANOTHER_STYLE] : '',
                'class' => 'inp inp-txt'. sprintf(' validate[maxSize[%d]]', $maxCharactersOtherStyle),
                'title' => __('Other type not listed above:', 'apollo'),
            ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),
        );
        $mediumElement = array(
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk ' . ($enableSearchWdMedium ? "" : 'hidden') . ' ' . $classArtistMember,
                'container' => '',
            ),
            //title
            array(
                'type' => 'Title',
                'name' => 'title',
                'value' => $searchWdMediumLabel,
                'class' => ''
            ),
            //end title
            'post_medium' => array(
                'title' => __('What is your medium?', 'apollo'),
                'type' => 'Category_Group',
                'name' => Apollo_DB_Schema::_ARTIST_MEDIUM_TAX,
                'class' => 'event',
                'value' => isset($arrData[Apollo_DB_Schema::_ARTIST_MEDIUM_TAX]) ? $arrData[Apollo_DB_Schema::_ARTIST_MEDIUM_TAX] : '',
                'tax'   => 'artist-medium',
            ),

            Apollo_DB_Schema::_APL_ARTIST_ANOTHER_MEDIUM => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_APL_ARTIST_ANOTHER_MEDIUM,
                'value' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_ANOTHER_MEDIUM]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_ANOTHER_MEDIUM] : '',
                'class' => 'inp inp-txt'. sprintf(' validate[maxSize[%d]]', $maxCharactersOtherMedium),
                'title' => __('Other type not listed above:', 'apollo'),
            ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),
        );
        $bioElement = array(
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk artist_bio_statement_type',
                'container' => '',
            ),
            //title
            array(
                'type' => 'Title',
                'name' => 'title',
                'value' => __('BIO/STATEMENT', 'apollo'),
                'class' => ''
            ),
            //end title
            array(
                'type' => 'Subtitle',
                'value' => __('Tell us about yourself.', 'apollo'),
                'class' => 'artist-form-label'
            ),
            'post_content' => array(
                'title' => __('Tell us about yourself', 'apollo'),
                'type' => !of_get_option(Apollo_DB_Schema::_ARTIST_BIO_STATEMENT_TYPE, 0) ? 'Wysiwyg' : 'textarea',
                'name' => 'post_content',
                'class' => 'inp-desc-event ' . sprintf(' validate[maxSize[%d]]', $maxCharactersDesciption),
                'value' => Apollo_App::convertTinyCMEToCkEditor(isset($arrData['post_content']) ? $arrData['post_content'] : ''),
                'validate' => array(
                    'rule' => array(
                        $requiredDescription ? Apollo_Form::_FORM_REQUIRED : ''
                    )
                ),
            ),
            array(
                'type' => 'Hidden',
                'value' => $maxCharactersDesciption,
                'class' => 'max_length_artist_description'
            ),
            array(
                'type' => 'Div',
                'value' => '',
                'class' => 'countdown_bio_statement'
            ),
            array(
                'type' => 'CloseDiv',
                'value' => '',
                'class' => '',
                'container' => '',
            ),
            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),
        );
        $formElementItems = array(
            //group1: information
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

                'ID' => array(
                    'type' => 'Hidden',
                    'name' => 'ID',
                    'value' => isset($arrData['ID']) ? $arrData['ID'] : '',
                    'class' => 'inp inp-txt',
                    'container' => '',
                ),
                'post_status' => array(
                    'type' => 'Hidden',
                    'name' => 'post_status',
                    'place_holder' => __('Post status', 'apollo'),
                    'value' => isset($arrData['post_status']) ? $arrData['post_status'] : 'pending',
                    'class' => '',
                    'title' => __('Post status', 'apollo'),
                    'validate' => true,
                ),
                'message_failed' =>  array(
                    'type' => 'Subtitle',
                    'value' =>  __('Your profile form is not complete. One or more errors exist. Please scroll down for more information.', 'apollo'),
                    'class' => 'apl-message-error hidden'
                ),
                Apollo_DB_Schema::_APL_ARTIST_FNAME => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_ARTIST_FNAME,
                    'place_holder' => __('First name', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_FNAME]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_FNAME] : '',
                    'class' => 'inp inp-txt',
                    'title' => __('First Name', 'apollo'),
                ),
                Apollo_DB_Schema::_APL_ARTIST_LNAME => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_ARTIST_LNAME,
                    'place_holder' => __('Last Name (*)', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_LNAME]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_LNAME] : '',
                    'class' => 'inp inp-txt validate[required]',
                    'title' => __('Last Name (*)', 'apollo'),
                    'data_attributes' => array(
                        'data-errormessage-value-missing="'.__('Artist last name is required','apollo').'"'
                    ),
                    'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_REQUIRED
                        )
                    ),
                ),

                Apollo_DB_Schema::_APL_ARTIST_EMAIL => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_ARTIST_EMAIL,
                    'place_holder' => $title = $requiredEmail ? __('Email (*)', 'apollo') : __('Email', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_EMAIL]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_EMAIL] : '',
                    'class' => sprintf('inp inp-txt %s[funcCall[disableSpace],custom[email]]', $requiredEmail ? $requiredClass : ''),
                    'data_attributes' => array(
                        'data-errormessage-value-missing="'.__('Email is required','apollo').'"'
                    ),
                    'title' => $title,
                    'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_EMAIL,
                            $requiredEmail ? Apollo_Form::_FORM_REQUIRED : ''
                        )
                    )
                ),
                Apollo_DB_Schema::_APL_ARTIST_DISPLAY_EMAIL => array(
                    'type' => 'Checkbox',
                    'name' => Apollo_DB_Schema::_APL_ARTIST_DISPLAY_EMAIL,
                    'title' => of_get_option(Apollo_DB_Schema::_ARTIST_EMAIL_LABEL, __('Check here if you DO NOT want your Email to appear on your Artist Profile page.', 'apollo')),
                    'value' => array(
                        'list_item' => 'yes',
                        'selected_item' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_DISPLAY_EMAIL]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_DISPLAY_EMAIL] : ''
                    ),
                ),

                Apollo_DB_Schema::_APL_ARTIST_PHONE => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_ARTIST_PHONE,
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_PHONE]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_PHONE] : '',
                    'class' => 'inp inp-txt '. ($requiredPhone ? $requiredClass : ''),
                    'place_holder' => $title = $requiredPhone ? __('Phone (*)', 'apollo') : __('Phone', 'apollo'),
                    'data_attributes' => array(
                        'data-errormessage-value-missing="'.__('Phone is required','apollo').'"'
                    ),
                    'title' => $title,
                    'validate' => array(
                        'rule' => array(
                            $requiredPhone ? Apollo_Form::_FORM_REQUIRED : ''
                        )
                    )
                ),
                Apollo_DB_Schema::_APL_ARTIST_DISPLAY_PHONE => array(
                    'type' => 'Checkbox',
                    'name' => Apollo_DB_Schema::_APL_ARTIST_DISPLAY_PHONE,
                    'title' => of_get_option(Apollo_DB_Schema::_ARTIST_PHONE_LABEL,__('Check here if you DO NOT want your Phone Number to appear on your Artist Profile page.', 'apollo')),
                    'value' => array(
                        'list_item' => 'yes',
                        'selected_item' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_DISPLAY_PHONE]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_DISPLAY_PHONE] : ''
                    ),
                ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

        );

        $formElementItems = array_merge($formElementItems, $locationInfoElement, $socialElement, $categoryElement, $styleElement, $mediumElement, $bioElement);

        $ques = unserialize(get_option( Apollo_DB_Schema::_APL_ARTIST_OPTIONS ));
        if ($ques):
            $quesFields = array(
                array(
                    'type' => 'Div',
                    'name' => 'title',
                    'value' => '',
                    'class' => 'artist-blk ' . $classArtistMember,
                    'container' => '',
                ),
                //title
                array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => __('Questions', 'apollo'),
                    'class' => ''
                ),
            );
            $i = 0;
            foreach( $ques as $q ):
                if ( ! $q ) continue;
                $pre_label = sprintf( __( 'Q%s. ', 'apollo' ), $i+1 );
                $quesFields[Apollo_DB_Schema::_APL_ARTIST_QUESTIONS.$i] = array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_ARTIST_QUESTIONS. '[]',
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_ARTIST_QUESTIONS][$i]) ? $arrData[Apollo_DB_Schema::_APL_ARTIST_QUESTIONS][$i] : '',
                    'class' => 'inp inp-txt',
                    'title' =>'<strong>'.$pre_label.'</strong>'.$q.'',
                );
                $i++;
            endforeach;

        endif;

        $quesFields[] = array(
            'type' => 'CloseDiv',
            'name' => 'title',
        );


        $formElementItems = array_merge($formElementItems, $quesFields);

        /**
         * @ticket #18695: [CF] 20181221 - Add the option for image upload to the main artist profile form.
         */
        $locationPhotoForm = of_get_option(APL_Theme_Option_Site_Config_SubTab::_ARTIST_PHOTO_FORM_LOCATION, 1);
        if($locationPhotoForm ==  2) {

            $enableRequirePrimaryPhoto = Apollo_App::isEnableRequiredPrimaryImageByModuleName(Apollo_DB_Schema::_ARTIST_PT);
            $primary_title = $enableRequirePrimaryPhoto ? 'PRIMARY IMAGE(*)' : 'PRIMARY IMAGE';

            $photoElement = array(
                'artist_image_gallery_section' => array(
                    'type' => 'Div',
                    'name' => 'title',
                    'value' => '',
                    'class' => 'artist-blk',
                    'container' => '',
                    'data_attributes' => array("id='artist-profile-photo-frm'")
                ),
                'artist_image_gallery_title' => array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => __('Artist Images', 'apollo'),
                    'class' => ''
                ),
                Apollo_DB_Schema::_APL_ARTIST_IMAGE_GALLERY => array(
                    'type' => 'Tab',
                    'value' =>  isset($arrData['ID'])?$arrData['ID']:'',
                    'container' => 'el-blk',
                    'children' => array(
                        'primary_photo' => array(
                            'type' => 'ShortCode',
                            'name' => 'primary_photo',
                            'title' => __($primary_title, 'apollo'),
                            'target' => Apollo_DB_Schema::_ARTIST_PT,
                            'value' => 'apollo_upload_and_drop '
                        ),
                        'gallery_photo' => array(
                            'type' => 'ShortCode',
                            'name' => 'gallery_photo',
                            'title' => __('GALLERY', 'apollo'),
                            'target' => Apollo_DB_Schema::_ARTIST_PT,
                            'value' => 'apollo_upload_gallery ',
                        ),
                    )
                ),
                array(
                    'type' => 'CloseDiv',
                    'value' => '',
                    'class' => '',
                    'container' => '',
                )
            );

            $formElementItems = array_merge($formElementItems, $photoElement);
        }

        $formElementItems['Additional_Form'] = array(
            'type' => 'Additional_fields',
            'name' => '',
            'class' => 'full-custom',
            'title' => '',
            'id' => '',
            'container' => 'artist-blk ' . $classArtistMember,
            'value' => array(
                'post_type' => Apollo_DB_Schema::_ARTIST_PT,
                'post_data' => $arrData,
                'post_id' => $arrData['ID']
            )
        );

        /**
         * Artist documentation
         */

        $documents = array(

            //end title
            'document' => array(
                'type' => 'Document_Group',
                'container' => 'artist-blk document-custom ' . $classArtistMember,
                'value' => array(
                    'hide_header' => true,
                    'post_id' => $arrData['ID'],
                    'max_upload_pdf' => of_get_option(Apollo_DB_Schema::_MAX_UPLOAD_PDF_ARTIST, Apollo_Display_Config::_MAX_UPLOAD_PDF_DEFAULT)
                )
            ),

        );

        /** End artist documentation */
        if (Apollo_App::get_network_manage_states_cities() == 0 && isset($formElementItems['add_new_state_btn'])) {
            unset($formElementItems['add_new_state_btn']);
            unset($formElementItems['add_new_state']);
        }

        /*@ticket #17168 */
        $max_upload_size_in_mb = number_format(Apollo_App::maxUploadFileSize('mb'), 2);
        $pdfLabel = of_get_option(Apollo_DB_Schema::_ARTIST_FORM_PDF_LABEL, __( 'Resume and/or Work Samples (PDF)', 'apollo' ));
        $formElementItems = array_merge($formElementItems, array(
                array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => $pdfLabel,
                    'class' => 'title-bar-blk ' . $classArtistMember,
                ),
                'artist-upload-pdf-desc' => array(
                    'type' => 'Subtitle',
                    'class' => 'artist-form-label ' . $classArtistMember,
                    'value' => sprintf(__("Each PDF must be less than %s MB", "apollo"), $max_upload_size_in_mb)
                )

            )
        );
        $formElementItems = array_merge($formElementItems, $documents);

        /*
       * @Ticket 15203
       * generate select tags
       */
        if(has_filter("apl_pbm_fe_render_select_tags_section")) {
            $formElementItems = apply_filters('apl_pbm_fe_render_select_tags_section', [
                'post_id' => $arrData['ID'],
                'form'    => $formElementItems,
                'hidden_field' => $classArtistMember
            ]);
        }

        //submit button
        $formElementItems['Submit_btn'] = array(
            'type' => 'Submit',
            'container' => 'artist-blk',
            'class' => 'submit-btn submit-form-with-validation-engine',
            'title' => __('UPDATE PROFILE', 'apollo')
        );

        $this->elements = $formElementItems;
    }

    public function formHeader()
    {
        ?>
        <div class="dsb-welc custom">
            <h1 ><?php echo of_get_option(Apollo_DB_Schema::_ARTIST_PROFILE_TITLE ,__('Artist Information', 'apollo')) ?>
                <?php
                $id = $this->id ? $this->id : $this->formData['ID'];
                if ($id && isset($this->formData['post_status']) &&
                    ($this->formData['post_status'] == 'publish' || $this->formData['post_status'] == 'pending') && $this->formData
                ): ?>
                    <a class="view-page-link" target="_blank"
                       href="<?php echo get_the_permalink($id) ?>">(<?php _e('View Page', 'apollo') ?>
                        )</a>
                <?php endif; ?>
            </h1>
            <?php
            /**
             * @ticket #18695: show message save success
             */
            if (isset($_SESSION['save_'. Apollo_DB_Schema::_ARTIST_PT])) {
                if ($_SESSION['save_'. Apollo_DB_Schema::_ARTIST_PT] == 'add-new') {
                    $this->formSubmitSuccessRedirect(__( Apollo_Form_Static::_SUCCESS_ADD_NEW_MESSAGE, 'apollo' ));
                } else {
                    $this->formSubmitSuccessRedirect(__( Apollo_Form_Static::_SUCCESS_MESSAGE, 'apollo'));
                }
                unset($_SESSION['save_'. Apollo_DB_Schema::_ARTIST_PT]);
            }
            else{
                $this->successMessage();
            }
            ?>
        </div>

        <?php
        if ( isset($_GET['warning']) ):
            ?>
            <div><span class="error"><?php _e('Please enter your artist profile first !', 'apollo') ?></span></div>
        <?php endif; ?>

        <?php
        parent::formHeader();
        if(Apollo_App::isInputMultiplePostMode()){
            if(isset($_SESSION['saved-post-success'])){
                $this->successMessage = __(Apollo_Form_Static::_SUCCESS_MESSAGE, 'apollo');
                parent::formSubmitSuccess();
                unset($_SESSION['saved-post-success']);
            }
        }
    }

    /*Process form submit*/
    public function postMethod()
    {
        // validate nonce
        if ( $this->validateNonce() ) {
            return $this->validateClassObject->save($this->formData);
        }
    }

    /*process form get method*/
    public function getMethod()
    {
        $this->successMessage = __(Apollo_Form_Static::_SUCCESS_MESSAGE, 'apollo');
        $this->failMessage = __('Your profile form is not complete. One or more errors exist. Please scroll down for more information.', 'apollo');

        $id = Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_ARTIST_PT);

        if (!empty($id)) {

            if(get_post_status($id) == 'publish' ||  get_post_status($id) == 'pending'){
                $postInfo = get_post($id, ARRAY_A);
                $status = $postInfo['post_status'];
                $id = $postInfo['ID'];
                //get all meta;
                $metaAddr = get_apollo_meta($id, Apollo_DB_Schema::_APL_ARTIST_ADDRESS, true);
                $metaData = get_apollo_meta($id, Apollo_DB_Schema::_APL_ARTIST_DATA, true);

                if (!is_array($metaData) && !empty($metaData))
                    $metaData = Apollo_App::unserialize($metaData);

                if (is_array($metaData)) {
                    foreach ($metaData as $key => $value) {
                        $postInfo[$key] = $value;
                    }
                }

                if (!is_array($metaAddr) && !empty($metaAddr))
                    $metaAddr = unserialize($metaAddr);

                if (is_array($metaAddr)) {
                    foreach ($metaAddr as $key => $value) {
                        $postInfo[$key] = $value;
                    }
                }

                $postInfo[Apollo_DB_Schema::_ARTIST_TYPE] = wp_get_post_terms($id, Apollo_DB_Schema::_ARTIST_PT . '-type', array(
                    'fields' => 'ids'
                ));

                $postInfo[Apollo_DB_Schema::_ARTIST_STYLE_TAX] = wp_get_post_terms($id, Apollo_DB_Schema::_ARTIST_PT . '-style', array(
                    'fields' => 'ids'
                ));

                $postInfo[Apollo_DB_Schema::_ARTIST_MEDIUM_TAX] = wp_get_post_terms($id, Apollo_DB_Schema::_ARTIST_PT . '-medium', array(
                    'fields' => 'ids'
                ));
                
                $postInfo[Apollo_DB_Schema::_APL_ARTIST_QUESTIONS] = maybe_unserialize(get_apollo_meta($id, Apollo_DB_Schema::_APL_ARTIST_QUESTIONS, true));

                $postInfo['ID'] = $id;
                $postInfo['upload_pdf'] = get_apollo_meta($id, 'upload_pdf');

                $this->formData = $postInfo;
                $this->successMessage = Apollo_Form_Static::_SUCCESS_MESSAGE;
            }
        }
    }
    //Show message Fail Submit

    /**
     * Override submit action to override parent for additional field
     */
    public function formSubmitAction()
    {
        // Handle submit form
        do_action('apollo_submit_form');

        $addFieldsValidateClass = new Apollo_Submit_Form(Apollo_DB_Schema::_ARTIST_PT, array());
        $this->isSaveSuccess = false;
        $this->isSubmitFail = false;
        //post method

        if ($this->formRequestMethod == 'post') {
            $this->cleanPost();

            $this->isSubmitFail = true;

            if ($this->validateClassObject->isValidAll($this->mergerPostRule())) {
                $this->isSaveSuccess = true;
                $this->isSubmitFail = false;
            }
            if (APL_Artist_Function::checkArtistMember($this->getFormData()['ID'])){
                if (!$addFieldsValidateClass->isValidAll($this->mergePostAFields())) {
                    $this->isSaveSuccess = false;
                    $this->isSubmitFail = true;
                }
            }
            //save data
            if ($this->isSaveSuccess) {
                $this->id = $this->postMethod();

                /*
                 * @Ticket 15203
                 * page builder module
                 * save tags
                 */
                if(has_action('pbm_fe_save_post_tags')) {
                    do_action('pbm_fe_save_post_tags', $this->id, array('data' => $_POST));
                }
            }
            //end save date
            if(Apollo_App::isInputMultiplePostMode()){
                // Thienld : handle for case submit post of inputting multiple post
                // Take user to the edit post page on dashboard when saved data success (add new / edit action)
                if ($this->isSaveSuccess) {
                    $_SESSION['saved-post-success'] = 1;
                    $redirectUrl = intval(get_query_var('_is_agency_educator_page', 0)) ? APL_Dashboard_Hor_Tab_Options::AGENCY_ARTIST_PROFILE_URL : APL_Dashboard_Hor_Tab_Options::ARTIST_PROFILE_URL;

                    wp_redirect(site_url($redirectUrl) . '/' .$this->id);
                    exit();
                }
            }
        }
    }

    public function mergePostAFields()
    {
        $data = array();
        $group_fields = Apollo_Custom_Field::get_group_fields(Apollo_DB_Schema::_ARTIST_PT);
        foreach ($group_fields as $group) {
            $fields = isset($group['fields']) ? $group['fields'] : array();
            if (count($fields) > 0) {
                foreach ($fields as $field) {
                    $data[$field->name] = isset($_POST[$field->name]) ? $_POST[$field->name] : '';
                }
            }
        }

        return $data;
    }

    public function formSubmitProcess(){
        $this->formSubmitAction();
        $this->formHeader();
    }

    public function setCurDataHorizontalTab()
    {
        $this->activatedTab = APL_Dashboard_Hor_Tab_Options::ARTIST_PROFILE_URL;

        if (intval(get_query_var('_is_agency_educator_page', 0))) {
            $this->activatedTab = APL_Dashboard_Hor_Tab_Options::AGENCY_ARTIST_PROFILE_URL;
        }
    }

    /**
     * Set nonce info
     *
     * @return void
     */
    public function setNonceInfo()
    {
        $this->nonceName   = Apollo_Const::_APL_ARTIST_NONCE_NAME;
        $this->nonceAction = Apollo_Const::_APL_NONCE_ACTION_ARTIST_PROFILE_PAGE;
    }
}
