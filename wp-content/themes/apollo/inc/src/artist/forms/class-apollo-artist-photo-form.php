<?php

class Apollo_Artist_Photo_Form extends Apollo_Artist_Base_Form
{

    public function formInit(){

        $arrData = $this->formData;

        $photoTabContent = array(
            'primary_photo' => array(
                   'type' => 'ShortCode',
                   'name' => 'primary_photo',
                   'title' => __('PRIMARY IMAGE','apollo'),
                   'target' => Apollo_DB_Schema::_ARTIST_PT,
                   'value' =>  'apollo_upload_and_drop ',
            ),
            'gallery_photo' => array(
                   'type' => 'ShortCode',
                   'name' => 'gallery_photo',
                   'title' => __('GALLERY','apollo'),
                   'target' => Apollo_DB_Schema::_ARTIST_PT,
                   'value' =>  'apollo_upload_gallery',

            ),
        );

        $formElementItems = array(
            'ID' =>  array(
                'type' => 'Hidden',
                'name' => 'ID',
                'place_holder' =>__('Id','apollo'),
                'value' =>  isset($arrData['ID'])?$arrData['ID']:'',
                'class' => 'inp inp-txt',
                'title' =>__('Name','apollo'),
                'validate' => true,
                'id' => 'artist_id',
                'container' => 'ebl'
            ),
            'target' =>  array(
                'type' => 'Hidden',
                'name' => 'ID',
                'place_holder' =>__('target','apollo'),
                'value' =>  Apollo_DB_Schema::_ARTIST_PT,
                'class' => 'inp inp-txt',
                'title' =>__('Name','apollo'),
                'validate' => true,
                'id' => 'artist_target',
                'container' => 'ebl'
            ),
            'photo_tab' => array(
                'type' => 'Tab',
                'place_holder' =>__('Name','apollo'),
                'value' =>  $arrData['ID'],
                'container' => 'el-blk',
                'children' => $photoTabContent
            ),
            'Submit_btn' => array(
                'type' => 'Button',
                'class' => 'submit-btn',
                'id' => 'apollo_submit_artist_photo',
                'title' => __('SUBMIT PHOTO','apollo')
            )
        );
        $this->elements = $formElementItems;
    }
    public function getMethod(){
        $id = Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_ARTIST_PT);
        if(!empty($id)) {
            $this->formData['ID'] = $id;
        }
    }

    public function postMethod(){
    }

    //Show message Success Submit


    // Thienld : These functions apply for mode input multiple post
    public function setCurDataHorizontalTab()
    {
        $this->activatedTab = APL_Dashboard_Hor_Tab_Options::ARTIST_PHOTOS_URL;

        if (intval(get_query_var('_is_agency_educator_page', 0))) {
            $this->activatedTab = APL_Dashboard_Hor_Tab_Options::AGENCY_ARTIST_PHOTOS_URL;
        }
    }

    /**
     * Set nonce info
     *
     * @return void
     */
    public function setNonceInfo()
    {
        $this->nonceName   = Apollo_Const::_APL_NONCE_NAME;
        $this->nonceAction = Apollo_Const::_APL_NONCE_ACTION_ARTIST_GALLERY_PAGE;
    }
}
