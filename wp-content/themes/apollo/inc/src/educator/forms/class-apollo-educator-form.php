<?php
/**
 * Created by PhpStorm.
 * User: elisoft-php
 * Date: 19/10/2015
 * Time: 10:02
 */

class Apollo_Educator_Form extends Apollo_Form
{
    /* Init form element then set data for them */

    public function formInit()
    {

        $arrData = $this->formData;

        $stateVal = '';
        $cityVal = '';
        if( $arrData){
            $stateVal = isset($arrData[Apollo_DB_Schema::_APL_EDUCATOR_STATE]) ? $arrData[Apollo_DB_Schema::_APL_EDUCATOR_STATE] : '';
            $cityVal = isset($arrData[Apollo_DB_Schema::_APL_EDUCATOR_CITY]) ? $arrData[Apollo_DB_Schema::_APL_EDUCATOR_CITY]: '';
        } else {
            $list_states = Apollo_App::getListState();
            $default_states =  of_get_option(Apollo_DB_Schema::_APL_DEFAULT_STATE);
            $cityVal = of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY);
            if( $default_states && in_array($default_states,$list_states) ) {
                $stateVal = $default_states;
            } else if(count($list_states) == 1) {
                $stateVal = key($list_states);
            }
        }

        $enableOtherState = of_get_option(Apollo_DB_Schema::_EDUCATOR_FE_ENABLE_OTHER_STATE, 0);
        $enableOtherCity = of_get_option(Apollo_DB_Schema::_EDUCATOR_FE_ENABLE_OTHER_CITY, 0);
        $enableOtherZip = of_get_option(Apollo_DB_Schema::_EDUCATOR_FE_ENABLE_OTHER_ZIP, 0);

        $formElementItems = array(
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),
            'ID' => array(
                'type' => 'Hidden',
                'name' => 'ID',
                'place_holder' => __('Name', 'apollo'),
                'value' => isset($arrData['ID']) ? $arrData['ID'] : '',
                'class' => 'inp inp-txt',
                'title' => __('Name', 'apollo'),
                'validate' => true,
                'container' => '',
            ),
            'post_status' => array(
                'type' => 'Hidden',
                'name' => 'post_status',
                'place_holder' => __('Post status', 'apollo'),
                'value' => isset($arrData['post_status']) ? $arrData['post_status'] : 'pending',
                'class' => '',
                'title' => __('Post status', 'apollo'),
                'validate' => true,
            ),
            'post_title' => array(
                'type' => 'Text',
                'name' => 'post_title',
                'place_holder' => __('Educator Name (*) ', 'apollo'),
                'value' => isset($arrData['post_title']) ? $arrData['post_title'] : '',
                'class' => 'inp inp-txt validate[required]',
                'data_attributes' => array(
                    'data-errormessage-value-missing="'.__('Educator name is required','apollo').'"'
                ),
                'title' => __('Educator Name', 'apollo'),
                'validate' => array(
                    'rule' => array(
                        Apollo_Form::_FORM_REQUIRED
                    )
                ),
            ),
            'post_content' => array(
                'type' => 'Wysiwyg',
                'name' => 'post_content',
                'place_holder' => __('Description/Bio ', 'apollo'),
                'value' => Apollo_App::convertTinyCMEToCkEditor(isset($arrData['post_content']) ? $arrData['post_content'] : ''),
                'class' => 'inp-desc-event',
                'title' => __('Description/Bio ', 'apollo'),
            ),



            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'el-blk',
                'container' => '',
            ),
            Apollo_DB_Schema::_APL_EDUCATOR_ADD1 => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_APL_EDUCATOR_ADD1,
                'place_holder' => __('Address 1 ', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_APL_EDUCATOR_ADD1]) ? $arrData[Apollo_DB_Schema::_APL_EDUCATOR_ADD1] : '',
                'class' => 'inp inp-txt',
                'title' => __('Address 1', 'apollo'),

            ),

            Apollo_DB_Schema::_APL_EDUCATOR_ADD2 => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_APL_EDUCATOR_ADD2,
                'place_holder' => __('Address 2', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_APL_EDUCATOR_ADD2]) ? $arrData[Apollo_DB_Schema::_APL_EDUCATOR_ADD2] : '',
                'class' => 'inp inp-txt',
                'title' => __('Address 2', 'apollo'),
            ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'el-blk',
                'container' => '',
            ),
            Apollo_DB_Schema::_APL_EDUCATOR_STATE => array(
                'type' => 'Select',
                'name' => Apollo_DB_Schema::_APL_EDUCATOR_STATE,
                'class' => 'event apl-territory-state ',
                'title' => __('State ', 'apollo'),
                'value' => array(
                    'list_item' => Apollo_App::getStateByTerritory(false, $stateVal),
                    'selected_item' => $stateVal
                ),
                'id' => 'apl-us-states',
            ),
            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'el-blk',
                'container' => '',
            ),
            Apollo_DB_Schema::_APL_EDUCATOR_CITY => array(
                'type' => 'Select',
                'name' => Apollo_DB_Schema::_APL_EDUCATOR_CITY,
                'place_holder' => __('City ', 'apollo'),
                'id' => 'apl-us-cities',
                'container' => 'hafl fl',
                'value' => array(
                    'list_item' => Apollo_App::getCityByTerritory(false,$stateVal, false),
                    'selected_item' => $cityVal
                ),
                'class' => 'event apl-territory-city',
                'title' => __('City', 'apollo'),

            ),


            Apollo_DB_Schema::_APL_EDUCATOR_ZIP => array(
                'type' => 'Select',
                'name' => Apollo_DB_Schema::_APL_EDUCATOR_ZIP,
                'place_holder' => __('Zip ', 'apollo'),
                'container' => 'hafl fr',
                'value' => array(
                    'list_item' => Apollo_App::getZipByTerritory(false, $stateVal, $cityVal, false),
                    'selected_item' => isset($arrData[Apollo_DB_Schema::_APL_EDUCATOR_ZIP]) ? $arrData[Apollo_DB_Schema::_APL_EDUCATOR_ZIP] : ''
                ),
                'id' => 'apl-us-zip',
                'class' => 'event apl-territory-zipcode',
                'title' => __('Zip', 'apollo'),
            ),


            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            Apollo_DB_Schema::_APL_EDUCATOR_REGION => array(
                'type' => 'Select',
                'name' => Apollo_DB_Schema::_APL_EDUCATOR_REGION,
                'class' => 'event',
                'title' => __('Region ', 'apollo'),
                'value' => array(
                    'list_item' => Apollo_App::get_regions(false, false),
                    'selected_item' => isset($arrData[Apollo_DB_Schema::_APL_EDUCATOR_REGION]) ? $arrData[Apollo_DB_Schema::_APL_EDUCATOR_REGION] : '',
                ),
                'id' => 'apl-regions',
                'display' => Apollo_App::showRegion(),
            ),
            Apollo_DB_Schema::_APL_EDUCATOR_COUNTY => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_APL_EDUCATOR_COUNTY,
                'place_holder' => __('County ', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_APL_EDUCATOR_COUNTY]) ? $arrData[Apollo_DB_Schema::_APL_EDUCATOR_COUNTY] : '',
                'class' => 'inp inp-txt',
                'title' => __('County', 'apollo'),

            ),

        );

        /** @ticket #19687
         * [CF] 20190408 - Add text to create more separation between the address drop menus and the other address fields
         */
        if ($enableOtherState || $enableOtherCity || $enableOtherZip) {
            $formElementItems = array_merge($formElementItems, array(
                array(
                    'type' => 'Subtitle',
                    'value' => __("If your city, state and/or zip are not listed above, please fill in the fields below.", 'apollo'),
                    'class' => 'apl-subtitle-bold',
                )
            ));
        }

        /** @Ticket #19640 */
        if ($enableOtherState) {
            $formElementItems = array_merge($formElementItems, array(
                Apollo_DB_Schema::_APL_EDUCATOR_TMP_STATE => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_EDUCATOR_TMP_STATE,
                    'place_holder' => __('Other State', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_EDUCATOR_TMP_STATE]) ? $arrData[Apollo_DB_Schema::_APL_EDUCATOR_TMP_STATE] : '',
                    'class' => 'inp inp-txt',
                    'title' => __('Other State', 'apollo'),
                )
            ));
        }

        if ($enableOtherCity) {
            $formElementItems = array_merge($formElementItems, array(
                Apollo_DB_Schema::_APL_EDUCATOR_TMP_CITY => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_EDUCATOR_TMP_CITY,
                    'place_holder' => __('Other City', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_EDUCATOR_TMP_CITY]) ? $arrData[Apollo_DB_Schema::_APL_EDUCATOR_TMP_CITY] : '',
                    'class' => 'inp inp-txt',
                    'title' => __('Other City', 'apollo'),
                )
            ));
        }

        if ($enableOtherZip) {
            $formElementItems = array_merge($formElementItems, array(
                Apollo_DB_Schema::_APL_EDUCATOR_TMP_ZIP => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_EDUCATOR_TMP_ZIP,
                    'place_holder' => __('Other Zip Code', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_EDUCATOR_TMP_ZIP]) ? $arrData[Apollo_DB_Schema::_APL_EDUCATOR_TMP_ZIP] : '',
                    'class' => 'inp inp-txt',
                    'title' => __('Other Zip Code', 'apollo'),
                )
            ));
        }

        $moreElementItems = array(
            array(
                'type' => 'Subtitle',
                'value' => __("Educator Contact Data", 'apollo'),
                'class' => 'apl-subtitle-bold',
            ),
            Apollo_DB_Schema::_APL_EDUCATOR_PHONE1 => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_APL_EDUCATOR_PHONE1,
                'place_holder' => __('Phone  ', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_APL_EDUCATOR_PHONE1]) ? $arrData[Apollo_DB_Schema::_APL_EDUCATOR_PHONE1] : '',
                'class' => 'inp inp-txt',
                'title' => __('Phone', 'apollo'),

            ),
            Apollo_DB_Schema::_APL_EDUCATOR_FAX => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_APL_EDUCATOR_FAX,
                'place_holder' => __('Fax', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_APL_EDUCATOR_FAX]) ? $arrData[Apollo_DB_Schema::_APL_EDUCATOR_FAX] : '',
                'class' => 'inp inp-txt',
                'title' => __('Fax', 'apollo'),
            ),
            Apollo_DB_Schema::_APL_EDUCATOR_EMAIL => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_APL_EDUCATOR_EMAIL,
                'place_holder' => __('Contact Email ', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_APL_EDUCATOR_EMAIL]) ? $arrData[Apollo_DB_Schema::_APL_EDUCATOR_EMAIL] : '',
                'class' => 'inp inp-txt validate[funcCall[disableSpace],custom[email]]',
                'title' => __('Contact Email', 'apollo'),
                'validate' => array(
                    'rule' => array(
                        Apollo_Form::_FORM_EMAIL,
                    )
                )
            ),
            Apollo_DB_Schema::_APL_EDUCATOR_URL => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_APL_EDUCATOR_URL,
                'place_holder' => __('URL', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_APL_EDUCATOR_URL]) ? $arrData[Apollo_DB_Schema::_APL_EDUCATOR_URL] : '',
                'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]]',
                'title' => __('URL', 'apollo'),
                'validate' => array(
                    'rule' => array(
                        Apollo_Form::_FORM_URL,

                    )
                )
            ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            /*@ticket #18400: 0002504: Arts Education Customizations - Add educator category type field to front end Educator profile form - Item 1*/
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

            array(
                'type' => 'Title',
                'name' => 'title',
                'value' => __( 'Educator type', 'apollo' ),
                'class' => '',
                'container' => 'title-bar-blk',
            ),

            Apollo_DB_Schema::_APL_EDU_TYPE => array(
                'title' => __('Educator type', 'apollo'),
                'type' => 'Category_Group',
                'name' => Apollo_DB_Schema::_APL_EDU_TYPE,
                'class' => 'access-listing custom-validate-field',
                'value' => isset($arrData[Apollo_DB_Schema::_APL_EDU_TYPE]) ? $arrData[Apollo_DB_Schema::_APL_EDU_TYPE] : '',
                'post_type' => Apollo_DB_Schema::_EDUCATOR_PT,
                'tax' => Apollo_DB_Schema::_APL_EDU_TYPE,
                'container' => 'access-list artist',
            ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            /**
             * onwer website
             *
             **/
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),
            //title
            array(
                'type' => 'Title',
                'name' => 'title',
                'value' => __('PRIMARY IMAGE', 'apollo'),
                'class' => '',

            ),
            //end title


            //IMAGE
            'primary_photo' => array(
                'type' => 'ShortCode',
                'name' => 'primary_photo',
                'target' => Apollo_DB_Schema::_EDUCATOR_PT,
                'value' =>  'apollo_upload_and_drop',
            ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),


            'Additional_Form' => array(
                'type' => 'Additional_fields',
                'name' => '',
                'class' => 'full-custom',
                'title' => '',
                'id' => '',
                'container' => 'artist-blk',
                'value' => array(
                    'post_type' => Apollo_DB_Schema::_EDUCATOR_PT,
                    'post_data' => $arrData,
                    'post_id' => $arrData['ID']
                )
            ),

            //submit button
            'Submit_btn' => array(
                'type' => 'Submit',
                'class' => 'submit-btn submit-form-with-validation-engine',
                'title' => __('SUBMIT RECORD', 'apollo')
            ),
            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

        );


        $this->elements = array_merge($formElementItems, $moreElementItems);

    }

    public function formHeader()
    {
        ?>
        <div class="dsb-welc custom">
            <h1 ><?php _e('Educator Profile', 'apollo') ?>
                <?php
                $id = $this->id ? $this->id : $this->formData['ID'];
                if ( $id && isset($this->formData['post_status']) &&
                    ($this->formData['post_status'] == 'publish' || $this->formData['post_status'] == 'pending') && $this->formData
                ): ?>
                    <a class="view-page-link" target="_blank"
                       href="<?php echo get_the_permalink($id) ?>">(<?php _e('View Page', 'apollo') ?>
                        )</a>
                <?php endif; ?>
            </h1>
        </div>
        <?php
        /** @Ticket #13482 */
        if (isset($_SESSION['save_'. Apollo_DB_Schema::_EDUCATOR_PT])) {
            if ($_SESSION['save_'. Apollo_DB_Schema::_EDUCATOR_PT] == 'add-new') {
                $this->formSubmitSuccessRedirect(__( Apollo_Form_Static::_SUCCESS_ADD_NEW_MESSAGE, 'apollo' ));
            } else {
                $this->formSubmitSuccessRedirect(__( Apollo_Form_Static::_SUCCESS_MESSAGE, 'apollo'));
            }
            unset($_SESSION['save_'. Apollo_DB_Schema::_EDUCATOR_PT]);
        }
        ?>
        <?php
        if ( isset($_GET['warning']) ):
            ?>
            <div><span class="error"><?php _e('Please enter your Educator Profile first !', 'apollo') ?></span></div>
        <?php endif; ?>

        <?php

        parent::formHeader();
    }

    /*Process form submit*/
    public function postMethod()
    {
        // validate nonce
        if ( $this->validateNonce() ) {
            return $this->validateClassObject->save($this->formData);
        }
    }

    /*process form get method*/
    public function getMethod()
    {
        $this->successMessage = __(Apollo_Form_Static::_SUCCESS_MESSAGE, 'apollo');
        $this->failMessage = __(Apollo_Form_Static::_FAIL_MESSAGE, 'apollo');

        if($this->formRequestMethod == 'get') {

            if (isset($_SESSION['apollo'][Apollo_DB_Schema::_EDUCATOR_PT . Apollo_SESSION::SUFFIX_ADD_PIMAGE])) {
                unset($_SESSION['apollo'][Apollo_DB_Schema::_EDUCATOR_PT . Apollo_SESSION::SUFFIX_ADD_PIMAGE]);
            }
        }
        $id = Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_EDUCATOR_PT);

        if ($id) {
            if(get_post_status($id) == 'publish' ||  get_post_status($id) == 'pending'){
                $postInfo = get_post($id, ARRAY_A);
                $status = $postInfo['post_status'];
                $id = $postInfo['ID'];
                //get all meta;
                /** @Ticket #13482 */
                $metaCFData = get_apollo_meta($id, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA , true);
                $metaData = get_apollo_meta($id, Apollo_DB_Schema::_APL_EDUCATOR_DATA, true);
                if (!is_array($metaCFData) && !empty($metaCFData))
                    $metaCFData = unserialize($metaCFData);

                if (!is_array($metaData) && !empty($metaData))
                    $metaData = unserialize($metaData);

                if (is_array($metaCFData)) {
                    foreach ($metaCFData as $key => $value) {
                        $postInfo[$key] = $value;
                    }

                }
                if (is_array($metaData)) {
                    foreach ($metaData as $key => $value) {
                        $postInfo[$key] = $value;
                    }
                }

                /*@ticket #18400: 0002504: Arts Education Customizations - Add educator category type field to front end Educator profile form - Item 1*/
                $postInfo[Apollo_DB_Schema::_APL_EDU_TYPE] = wp_get_post_terms($id, Apollo_DB_Schema::_APL_EDU_TYPE, array('fields' => 'ids'));
                $postInfo['ID'] = $id;
                $this->formData = $postInfo;
                $this->successMessage = Apollo_Form_Static::_SUCCESS_MESSAGE;
            }
        }

}
    //Show message Fail Submit

//override parent for additional field
    public function formSubmitAction()
    {
        $addFieldsValidateClass = new Apollo_Submit_Form(Apollo_DB_Schema::_EDUCATOR_PT, array());
        $this->isSaveSuccess = false;
        $this->isSubmitFail = false;
        //post method

        if ($this->formRequestMethod == 'post') {
            $this->cleanPost();
            $this->isSubmitFail = true;
            if ($this->validateClassObject->isValidAll($this->mergerPostRule())) {
                $this->isSaveSuccess = true;
                $this->isSubmitFail = false;
            }
            if (!$addFieldsValidateClass->isValidAll($this->mergePostAFields())) {
                $this->isSaveSuccess = false;
                $this->isSubmitFail = true;
            }
            //save data
            if ($this->isSaveSuccess)
                $this->id = $this->postMethod();
            //end save date
        }
    }

    public function mergePostAFields()
    {
        $data = array();
        $group_fields = Apollo_Custom_Field::get_group_fields(Apollo_DB_Schema::_EDUCATOR_PT);
        foreach ($group_fields as $group) {
            $fields = isset($group['fields']) ? $group['fields'] : array();
            if (count($fields) > 0) {
                foreach ($fields as $field) {
                    $data[$field->name] = isset($_POST[$field->name]) ? $_POST[$field->name] : '';
                }
            }
        }
        return $data;
    }

    /**
     * Set nonce info
     *
     * @return void
     */
    public function setNonceInfo()
    {
        $this->nonceName   = Apollo_Const::_APL_NONCE_NAME;
        $this->nonceAction = Apollo_Const::_APL_NONCE_ACTION_EDUCATION_PROFILE_PAGE;
    }
}