<?php

class Apollo_Submit_Educator extends Apollo_Submit_Form {
    
    public function __construct() {
        $post_type = Apollo_DB_Schema::_EDUCATOR_PT;
        
         $rules =  array(
            'id'                => array(),
            'post_title'        => array('required'),
            'post_content'      => array('required'),
            'post_status'       => array('required'),
             
            Apollo_DB_Schema::_APL_EDUCATOR_ADD1   => array(),
            Apollo_DB_Schema::_APL_EDUCATOR_ADD2   => array(),
            Apollo_DB_Schema::_APL_EDUCATOR_REGION   => array(),
            Apollo_DB_Schema::_APL_EDUCATOR_CITY   => array(),
            Apollo_DB_Schema::_APL_EDUCATOR_STATE  => array(),
            Apollo_DB_Schema::_APL_EDUCATOR_ZIP    => array(),
            Apollo_DB_Schema::_APL_EDUCATOR_COUNTY => array(),
            Apollo_DB_Schema::_APL_EDUCATOR_PHONE1 => array(),
            Apollo_DB_Schema::_APL_EDUCATOR_FAX => array(),
            Apollo_DB_Schema::_APL_EDUCATOR_EMAIL  => array('email'),
            Apollo_DB_Schema::_APL_EDUCATOR_URL    => array('url'),
        );
         
        parent::__construct($post_type, $rules);
    }

    public function save($arrData, $arrCustomField = array(), $arrCFSearch = array() ) {

        $id = isset($arrData['ID']) && !empty($arrData['ID']) ? $arrData['ID'] : 0;
        // Invalid data
        $educator_id = Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_EDUCATOR_PT);

        if ($educator_id != $id) {
            wp_safe_redirect(home_url());
            exit;
        }


        $post = array(
            'ID'            => isset($arrData['ID'])?$arrData['ID']:'',
            'post_title'    => Apollo_App::clean_data_request(Apollo_App::clean_data_request($arrData['post_title'])),
            'post_content'  => Apollo_App::convertCkEditorToTinyCME($arrData['post_content']),
            'post_author'   => get_current_user_id(),
            'post_type'     => Apollo_DB_Schema::_EDUCATOR_PT,
        );

        // UPDATE POST
        /** @Ticket #13482 */
        $is_new = true;
        if($id){

            $is_new = false;
            unset($post['post_author']);
            wp_update_post($post);

            // Keep icons due to it not exist in the front end form
            $arrData[Apollo_DB_Schema::_APL_EDUCATOR_POST_ICONS] = Apollo_App::apollo_get_meta_data($id, Apollo_DB_Schema::_APL_EDUCATOR_POST_ICONS, Apollo_DB_Schema::_APL_EDUCATOR_DATA );

        }else{
            $post['post_status'] = 'pending';
            $id = wp_insert_post($post);
        }

        /*@ticket #18400: 0002504: Arts Education Customizations - Add educator category type field to front end Educator profile form - Item 1*/
        $educatorType = isset($arrData[Apollo_DB_Schema::_APL_EDU_TYPE]) ? $arrData[Apollo_DB_Schema::_APL_EDU_TYPE] : '';
        wp_set_post_terms( $id, $educatorType , Apollo_DB_Schema::_APL_EDU_TYPE );
        unset($arrData[Apollo_DB_Schema::_APL_EDU_TYPE]);

        $group_fields = Apollo_Custom_Field::get_group_fields( Apollo_DB_Schema::_EDUCATOR_PT);
        $customFieldValue = array();
        $customSearchFieldValue = array();

        if(is_array($group_fields) && !empty($group_fields)){
            foreach($group_fields as $group){
                if(isset($group['fields']) && !empty($group['fields'])){
                    foreach($group['fields'] as $field){
                        if(isset($arrData[$field->name])){
                            if(is_array( $arrData[$field->name])){
                                $customFieldValue[$field->name] = $arrData[$field->name];
                            }
                            elseif($field->cf_type == 'wysiwyg'){
                                $customFieldValue[$field->name] = Apollo_App::convertCkEditorToTinyCME($arrData[$field->name]);
                            }
                            else{
                                $customFieldValue[$field->name] = Apollo_App::clean_data_request($arrData[$field->name]);
                            }
                            if(Apollo_Custom_Field::has_explain_field($field)){
                                $exFieldName = $field->name.'_explain';
                                $customFieldValue[$exFieldName] = Apollo_App::clean_data_request($arrData[$exFieldName]);
                            }

                            /** @Ticket #13482 */
                            $customSearchFieldValue[$field->name] = $arrData[$field->name];

                        } else if ($id) {
                            /** @Ticket #13482 */
                            if ($field->cf_type != 'checkbox') {
                                $customFieldValue[$field->name] = Apollo_App::apollo_get_meta_data($id, $field->name, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA);
                                $location = maybe_unserialize( $field->location );
                                if (!in_array( 'ff', $location)) {
                                    $customSearchFieldValue[$field->name] = Apollo_App::apollo_get_meta_data($id, $field->name, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA);
                                }
                            }
                        }

                    }
                }
            }
        }
        update_apollo_meta($id, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA, maybe_serialize($customFieldValue));

        // UPDATE META DATA
        unset($arrData['id'], $arrData['post_title'], $arrData['post_content']);

        /** @Ticket #19640 */
        if (!isset($arrData[Apollo_DB_Schema::_APL_EDUCATOR_TMP_STATE])) {
            $arrData[Apollo_DB_Schema::_APL_EDUCATOR_TMP_STATE] = Apollo_App::apollo_get_meta_data($id, Apollo_DB_Schema::_APL_EDUCATOR_TMP_STATE, Apollo_DB_Schema::_APL_EDUCATOR_DATA);
        }

        if (!isset($arrData[Apollo_DB_Schema::_APL_EDUCATOR_TMP_CITY])) {
            $arrData[Apollo_DB_Schema::_APL_EDUCATOR_TMP_CITY] = Apollo_App::apollo_get_meta_data($id, Apollo_DB_Schema::_APL_EDUCATOR_TMP_CITY, Apollo_DB_Schema::_APL_EDUCATOR_DATA);
        }

        if (!isset($arrData[Apollo_DB_Schema::_APL_EDUCATOR_TMP_ZIP])) {
            $arrData[Apollo_DB_Schema::_APL_EDUCATOR_TMP_ZIP] = Apollo_App::apollo_get_meta_data($id, Apollo_DB_Schema::_APL_EDUCATOR_TMP_ZIP, Apollo_DB_Schema::_APL_EDUCATOR_DATA);
        }

        update_apollo_meta($id, Apollo_DB_Schema::_APL_EDUCATOR_DATA, maybe_serialize(Apollo_App::clean_array_data($arrData)));
        /** @Ticket #13482 */
        update_apollo_meta($id, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA, maybe_serialize(Apollo_App::clean_array_data($customFieldValue)));
        update_apollo_meta($id, Apollo_DB_Schema::_APL_POST_TYPE_CF_SEARCH, maybe_serialize(Apollo_App::clean_array_data($customSearchFieldValue)));

        /** @Ticket #13028 */
        update_apollo_meta($id, Apollo_DB_Schema::_APL_EDUCATOR_CITY, !empty($arrData[Apollo_DB_Schema::_APL_EDUCATOR_CITY]) ? $arrData[Apollo_DB_Schema::_APL_EDUCATOR_CITY] : '');

        update_apollo_meta($id, Apollo_DB_Schema::_APL_EDUCATOR_STATE, !empty($arrData[Apollo_DB_Schema::_APL_EDUCATOR_STATE]) ? $arrData[Apollo_DB_Schema::_APL_EDUCATOR_STATE] : '');

        update_apollo_meta($id, Apollo_DB_Schema::_APL_EDUCATOR_ZIP, !empty($arrData[Apollo_DB_Schema::_APL_EDUCATOR_ZIP]) ? $arrData[Apollo_DB_Schema::_APL_EDUCATOR_ZIP] : '');

        $_GET['synchronous_save_data'] = 'true';
        $_GET['target'] = $arrData['target'];


        // SAVE FEATURE IMAGE
        /** @Ticket #12729 - Remove session */
        self::saveFeaturedImageHandler($id, Apollo_DB_Schema::_EDUCATOR_PT);

        global $wp;
        $current_url = home_url(). '/'. add_query_arg(array(),$wp->request);
        wp_redirect(home_url() . '/clean-form?redirect=' . urlencode($current_url). '&post_type='. Apollo_DB_Schema::_EDUCATOR_PT .'&action=' . ($is_new ? 'add-new' : 'update'));

        return $id;
    }
    
    public function getDataFromRequest() {
        $data = array(
            'id'        => isset($_REQUEST['id']) ? sanitize_text_field($_REQUEST['id']) : '',
            'post_title' => isset($_REQUEST['post_title']) ? sanitize_text_field($_REQUEST['post_title']) : '',
            'post_content' => isset($_REQUEST['post_content']) ? stripslashes($_REQUEST['post_content']) : '',
            'post_status' => isset($_REQUEST['post_status']) ? sanitize_text_field($_REQUEST['post_status']) : 'publish',
            Apollo_DB_Schema::_APL_EDUCATOR_ADD1   => isset($_REQUEST[Apollo_DB_Schema::_APL_EDUCATOR_ADD1]) ? sanitize_text_field($_REQUEST[Apollo_DB_Schema::_APL_EDUCATOR_ADD1]) : '',
            Apollo_DB_Schema::_APL_EDUCATOR_ADD2   => isset($_REQUEST[Apollo_DB_Schema::_APL_EDUCATOR_ADD2]) ? sanitize_text_field($_REQUEST[Apollo_DB_Schema::_APL_EDUCATOR_ADD2]) : '',
            Apollo_DB_Schema::_APL_EDUCATOR_CITY   => isset($_REQUEST[Apollo_DB_Schema::_APL_EDUCATOR_CITY]) ? sanitize_text_field($_REQUEST[Apollo_DB_Schema::_APL_EDUCATOR_CITY]) : '',
            Apollo_DB_Schema::_APL_EDUCATOR_STATE  => isset($_REQUEST[Apollo_DB_Schema::_APL_EDUCATOR_STATE]) ? sanitize_text_field($_REQUEST[Apollo_DB_Schema::_APL_EDUCATOR_STATE]) : '',
            Apollo_DB_Schema::_APL_EDUCATOR_ZIP    => isset($_REQUEST[Apollo_DB_Schema::_APL_EDUCATOR_ZIP]) ? sanitize_text_field($_REQUEST[Apollo_DB_Schema::_APL_EDUCATOR_ZIP]) : '',
            Apollo_DB_Schema::_APL_EDUCATOR_COUNTY => isset($_REQUEST[Apollo_DB_Schema::_APL_EDUCATOR_COUNTY]) ? sanitize_text_field($_REQUEST[Apollo_DB_Schema::_APL_EDUCATOR_COUNTY]) : '',
            Apollo_DB_Schema::_APL_EDUCATOR_PHONE1 => isset($_REQUEST[Apollo_DB_Schema::_APL_EDUCATOR_PHONE1]) ? sanitize_text_field($_REQUEST[Apollo_DB_Schema::_APL_EDUCATOR_PHONE1]) : '',
            Apollo_DB_Schema::_APL_EDUCATOR_FAX => isset($_REQUEST[Apollo_DB_Schema::_APL_EDUCATOR_FAX]) ? sanitize_text_field($_REQUEST[Apollo_DB_Schema::_APL_EDUCATOR_FAX]) : '',
            Apollo_DB_Schema::_APL_EDUCATOR_EMAIL  => isset($_REQUEST[Apollo_DB_Schema::_APL_EDUCATOR_EMAIL]) ? sanitize_text_field($_REQUEST[Apollo_DB_Schema::_APL_EDUCATOR_EMAIL]) : '',
            Apollo_DB_Schema::_APL_EDUCATOR_URL    => isset($_REQUEST[Apollo_DB_Schema::_APL_EDUCATOR_URL]) ? sanitize_text_field($_REQUEST[Apollo_DB_Schema::_APL_EDUCATOR_URL]) : '',
            Apollo_DB_Schema::_APL_EDUCATOR_REGION   => isset($_REQUEST[Apollo_DB_Schema::_APL_EDUCATOR_REGION]) ? sanitize_text_field($_REQUEST[Apollo_DB_Schema::_APL_EDUCATOR_REGION]) : Apollo_App::apollo_get_meta_data($_REQUEST['id'], Apollo_DB_Schema::_APL_EDUCATOR_REGION, Apollo_DB_Schema::_APL_EDUCATOR_DATA ),
        );

        
        return Apollo_App::clean_array_data($data);
    }
    
}