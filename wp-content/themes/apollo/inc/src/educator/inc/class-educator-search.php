<?php
require_once APOLLO_TEMPLATES_DIR. '/pages/lib/class-apollo-page-module.php';

class Apollo_Educator_Page extends Apollo_Page_Module {

    public function __construct($isCounting = false) {

        /* @ticket #15646 */
        self::setIsCounting($isCounting);

        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EDUCATION ) ) {
            wp_safe_redirect( '/' );
        }
        
        parent::__construct();
    }

    public function search() {
     
        $arr_params = array(
            'post_type'         => Apollo_DB_Schema::_EDUCATOR_PT,
            'posts_per_page'    => self::getIsCounting() ? 1 : Apollo_App::aplGetPageSize($this->pagesize , Apollo_DB_Schema::_EDUCATOR_NUM_ITEMS_LISTING_PAGE),
            'paged'             => $this->page,
            'post_status'       => array('publish'),
            'order'             => 'ASC',
            'orderby'           => 'post_title',
            'sentence'  => 1, // Exactly search
        );

        if($keyword = self::getKeyword()) {
            $arr_params['s'] =  $keyword;
        }

        $arr_tax_query = array();

        if(isset($_GET['term']) && !empty($_GET['term'])) {
            global $taxonomy;
            $arr_tax_query[] = array(
                'taxonomy'=> $taxonomy,
                'terms' => array($_GET['term']),
            );

        }

        $arr_params['tax_query'] = $arr_tax_query;

        // Set new offset if this is ajax action
        $this->addOffsetToParams($arr_params);

        $this->result = query_posts($arr_params);

        Apollo_Next_Prev::updateSearchResult($GLOBALS['wp_query']->request,Apollo_DB_Schema::_EDUCATOR_PT);
        $this->total = $GLOBALS['wp_query']->found_posts;
        $this->total_pages = ceil($this->getTotal() / $this->pagesize);
        $this->template = APOLLO_TEMPLATES_DIR. '/educator/listing-page/'. $this->_get_template_name(of_get_option(Apollo_DB_Schema::_EDUCATOR_DEFAULT_VIEW_TYPE));

        $this->resetPostData();
    }

}
