<?php

require_once APOLLO_SRC_DIR. '/module.php';

class APL_Event  extends  APL_Module {

    /**
     * get required dependencies
     *
     * @return array dependencies
     */
    public function getDependencies()
    {
        return array(
            'admin',
            'network',
        );
    }

    /**
     * get module name
     */
    public function getModuleName()
    {
        return Apollo_DB_Schema::_EVENT_PT;
    }
}

new APL_Event();