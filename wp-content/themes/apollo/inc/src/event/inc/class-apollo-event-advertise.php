<?php

class Apollo_Event_Advertise
{
    protected $totalEvent,
        $adIndex,
        $advertise = array(),
        $numberEvent = 5;

    public function __construct($totalEvent, $eventOffset)
    {
        $this->totalEvent = $totalEvent;
        $this->adIndex = (int)($eventOffset/$this->numberEvent);
        $this->setAdvertise();
    }



    /**
     * @param $advertiseTemplate: doesn't remove variable, becaue it uses to render template
     * @return string
     */
    private function getTemplate($adData)
    {
        ob_start();
        include APOLLO_TEMPLATES_DIR . '/events/event-advertise/advertise_template.php';
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }

    private function setAdvertise()
    {
        global $sidebars_widgets;
        $sidebarPrimary = isset($sidebars_widgets['apl-sidebar-primary']) ? $sidebars_widgets['apl-sidebar-primary'] : array();
        $widgetIDs = array();
        $customWidgetData = get_option('widget_apollo_custom_widget');

        if (!empty($sidebarPrimary)) {
            foreach ($sidebarPrimary as $sidebar) {
                if (preg_match('/\bapollo_custom_widget\b/', $sidebar)) {
                    $widgetIDs[] = explode("-", $sidebar)[1];
                }
            }
        }

        if (!empty($customWidgetData)) {
            foreach ($widgetIDs as $id) {
                if (isset($customWidgetData[$id]) && isset($customWidgetData[$id]['advertise'])) {
                    $this->advertise[] = $customWidgetData[$id];
                }
            }
        };
    }


    public function renderAdvertise($eventIndex)
    {
        if (!(($eventIndex + 1) %  $this->numberEvent) && isset($this->advertise[$this->adIndex])) {
            $advertiseTemplate = $this->advertise[$this->adIndex];
            $this->adIndex++;
            echo $this->getTemplate($advertiseTemplate);
        }
    }

    public function renderRemainAdvertise()
    {
        if( $this->adIndex <= count( $this->advertise)){
            for($i = $this->adIndex ; $i < count($this->advertise); $i++){
                $advertiseTemplate = $this->advertise[$i];
                echo $this->getTemplate($advertiseTemplate);
            }
        }
    }
}