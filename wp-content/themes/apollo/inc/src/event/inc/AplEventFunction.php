<?php

class AplEventFunction
{


    /**
     * @Ticket #15217
     * Update list review edit
     * @param $eventID
     * @param bool $remove
     */
    public static function eventUpdateListReviewEdit( $eventID, $remove = false ) {
        $currentListID = get_option(Apollo_DB_Schema::_APL_EVENT_REVIEW_EDIT, '');
        $listID = array();
        if (empty($currentListID)) {
            if (!$remove) {
                $listID[] = $eventID;
            }
        } else {
            $listID = explode(",", $currentListID);

            if ($remove) {
                $listID = array_diff($listID, array($eventID));
            } else {
                if (in_array($eventID, $listID)) {
                    return;
                }
                $listID[] = $eventID;
            }
        }
        update_option( Apollo_DB_Schema::_APL_EVENT_REVIEW_EDIT, !empty($listID) ? implode(",", $listID) : '', 'no' );
    }

    /**
     * @Ticket #15217 - Get event count by status
     * @param $status
     * @return int
     */
    public static function getEventCountByStatus( $status ) {
        global $wpdb;
        $where = "";
        switch ( $status ) {
            case 'review-edit' :
                $listReview = get_option(Apollo_DB_Schema::_APL_EVENT_REVIEW_EDIT, '');
                if (empty($listReview)) {
                    return 0;
                }
                $where = " AND p.ID IN ( ". $listReview ." ) AND p.post_status NOT IN ('draft', 'trash')";
                break;
            case 'all' :
                $where = " AND p.post_status NOT IN ('draft', 'trash') ";
                break;
            case 'draft' :
                $where = " AND p.post_status = '{$status}' ";
                break;
            case 'private' :
                /**
                 * @ticket #18894: Display private event in admin list page.
                 */
                $where = " AND (" . self::getQueryEventPrivate('p') . ")";
                break;
            case "mine" :
                /**
                 * @ticket #19052: Count event on tab "mine"
                 */
                global $current_user;
                $userId = !empty($current_user->ID) ? $current_user->ID : '';
                if(!$userId){
                    return 0;
                }
                $where = " AND p.post_author IN ($userId ) AND p.post_status NOT IN ('draft', 'trash')";
                break;
            default :
                $where = " AND p.post_status = '{$status}' ";
                break;
        }
        $postTable = $wpdb->prefix . "posts";
        $postMetaTable = $wpdb->prefix . Apollo_Tables::_APOLLO_EVENT_META;
        $_current_date = current_time( 'Y-m-d' );
        if (in_array($status, array('trash', 'draft'))) {
            $sql = " SELECT COUNT(p.ID) FROM {$postTable} p 
                    WHERE p.post_type = 'event'   AND p.post_status = '{$status}'
                    ";
        } else {
            $sql = "SELECT COUNT(p.ID) FROM {$postTable} p 
                    INNER JOIN {$postMetaTable} mt_start_d ON mt_start_d.apollo_event_id = p.ID
                    AND mt_start_d.meta_key = '_apollo_event_start_date'

                    INNER JOIN {$postMetaTable} mt_end_d   ON mt_end_d.apollo_event_id   = p.ID
                    AND mt_end_d.meta_key = '_apollo_event_end_date'
                    AND ( CAST( mt_end_d.meta_value AS DATE )  >= '{$_current_date}'  OR mt_end_d.meta_value  = ''  )
                    WHERE 1=1  AND p.post_type = 'event' {$where}
         ";
        }
        $postCount = (int) $wpdb->get_var($sql);
        return $postCount;
    }

    public static function getDefaultRejectReasons(){
        return array(
            'grammar' => __('Grammar: We found grammatical errors in your post.', 'apollo'),
            'description' => __('Description: Your description does not have enough information.', 'apollo'),
            'photo' => __('Photo: The photo you provided does not meet our style guidelines.', 'apollo'),
            'Appropriateness' => __('Appropriateness: Your event is not appropriate for our users.', 'apollo'),
            'location' => __('Location: Your event does not take place within our service area.', 'apollo'),
        );
    }

    /**
     * @param $searchData
     * @return string
     */
    public static function renderHtmlAttribute($searchData) {
        if (empty($searchData)){
            return '';
        }
        $attrHtml = '';
        foreach ($searchData as $key => $value) {
            if (!empty($value)) {
                $attrHtml .= ' ' . 'data-' . $key . '="'.$value.'"';
            }
        }
        return $attrHtml;
    }

    /**
     * @param string $startDateAlias
     * @param string $endDateAlias
     * @return string
     */
    public static function buildSqlDateRange($startDateAlias = 'start_date', $endDateAlias = 'mt_end_d') {
        return " (case when (DATEDIFF( {$endDateAlias}.meta_value, {$startDateAlias}.meta_value ) >= ".Apollo_App::orderDateRange().") THEN 1000 ELSE 0 END) AS date_range, DATEDIFF( {$endDateAlias}.meta_value, {$startDateAlias}.meta_value ) AS date_diff ";
    }

    /**
     * @return array
     */
    public static function getConfigDiscountIcon(){
        /**
         * @ticket #18340: 0002410: wpdev55 - Add the discount text and icon is the category page featured and and lower listing areas
         */
        $discountConfig = array(
            "enable-discount-description" => false,
            "include_discount_url" => false,
            "display_all_icon" => false,
        );

        return apply_filters('oc_get_discount_config', $discountConfig);
    }

    /**
     * @ticket #18994: Optimize event query
     * Related task #18894, #18932
     * @param $aliasName
     * @return string
     */
    public static function getQueryEventPrivate($aliasName){
        $currentPrivate = get_option( 'apollo_private_events', '');
        $currentPrivate = Apollo_App::unserialize($currentPrivate);
        $sqlPrivate = '';
        if(!empty($currentPrivate)){
            $currentPrivate = implode(',', $currentPrivate);
            $sqlPrivate = " OR ({$aliasName}.ID IN ($currentPrivate) AND {$aliasName}.post_status <> 'trash')";
        }
        $wherePrivate = " {$aliasName}.post_status = 'private' $sqlPrivate ";

        return $wherePrivate;
    }
}