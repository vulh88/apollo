<?php

/**
 * Created by PhpStorm.
 * User: pc92-vu
 * Date: 02/03/2016
 * Time: 15:12
 */
class AplGenerateSearchDate
{

    protected $isFilteringByDiscount = false;
    protected $startDate = false;
    protected $endDate = false;

    public function __construct($startDate, $endDate, $isFilteringByDiscount)
    {
        $this->isFilteringByDiscount = $isFilteringByDiscount;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }


    /**
     * Generate for searching with Table view
     * @Author vulh
     */
    function generateForView() {

        $isSearchStartDate = $this->startDate && !$this->isFilteringByDiscount;
        $isSearchEndDate = $this->endDate  && !$this->isFilteringByDiscount;
        $sqlString = '';


        if($isSearchStartDate && ! $isSearchEndDate){
            $sqlString = " p.start >= '$this->startDate' AND p.end >= '$this->startDate' ";
        }

        if(!$isSearchStartDate && $isSearchEndDate) {
            $sqlString = " p.end <= '$this->endDate' ";
        }

        if ($isSearchStartDate && $isSearchEndDate) {
            $sqlString = "
                ( p.start <= '$this->startDate' AND p.end <= '$this->endDate' )
                OR
                ( p.start >= '$this->startDate' AND p.end >= '$this->endDate' )
                OR
                ( p.start <= '$this->startDate' AND p.end >= '$this->endDate' )
                OR
                ( p.start >= '$this->startDate' AND p.end <= '$this->endDate' )
            ";
        }

        return $sqlString;
    }

    /**
     * @author: Trilm
     * @return string
     * generate search date by old rule
     */
    public function generate()
    {
        global $wpdb;

        //query by start_date
        //case1: search_start <= event_start <= search_end
        //case2: search_start <= event_end <= search_end
        //case3: even_start<= [search_start,search_end] <= even_end
        //all case: (case1) OR (case2) OR (case3) (two search date have value)
        //create query case1,2,3
        $sqlStringCase1 = '';
        $sqlStringCase2 = '';
        $sqlStringCase3 = '';
        $isSearchStartDate = $this->startDate && !$this->isFilteringByDiscount;
        $isSearchEndDate = $this->endDate  && !$this->isFilteringByDiscount;
        $sqlString = '';

        if($isSearchStartDate){
            $sqlStringCase1 .= ' '.$wpdb->posts.'.ID IN (
                SELECT em.apollo_event_id
                FROM '.$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}.' em
                WHERE em.apollo_event_id = '.$wpdb->posts.'.ID
                    AND em.meta_value >= "'.$this->startDate.'"
                    AND em.meta_key = "'.Apollo_DB_Schema::_APOLLO_EVENT_START_DATE.'"
            )';

            $sqlStringCase2 .= ' '.$wpdb->posts.'.ID IN (
                SELECT em.apollo_event_id
                FROM '.$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}.' em
                WHERE em.apollo_event_id = '.$wpdb->posts.'.ID
                    AND em.meta_value >= "'.$this->startDate.'"
                    AND em.meta_key = "'.Apollo_DB_Schema::_APOLLO_EVENT_END_DATE.'"
            )';
            $sqlStringCase3 .= '  '.$wpdb->posts.'.ID IN (
                SELECT em.apollo_event_id
                FROM '.$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}.' em
                WHERE em.apollo_event_id = '.$wpdb->posts.'.ID
                    AND em.meta_value <= "'.$this->startDate.'"
                    AND em.meta_key = "'.Apollo_DB_Schema::_APOLLO_EVENT_START_DATE.'"
            )';
        }
        if($isSearchEndDate){
            $andOr = 'AND';

            $sqlStringCase2 .= ' '.$andOr.'  '.$wpdb->posts.'.ID IN (
                SELECT em.apollo_event_id
                FROM '.$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}.' em
                WHERE em.apollo_event_id = '.$wpdb->posts.'.ID
                    AND em.meta_value <= "'.$this->endDate.'"
                    AND em.meta_key = "'.Apollo_DB_Schema::_APOLLO_EVENT_END_DATE.'"
            )';
            $andOrCase3 = '';
            if($sqlStringCase3 != '')
                $andOrCase3 = 'AND';
            $sqlStringCase3 .= $andOrCase3.' '.$wpdb->posts.'.ID IN (
                SELECT em.apollo_event_id
                FROM '.$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}.' em
                WHERE em.apollo_event_id = '.$wpdb->posts.'.ID
                    AND em.meta_value >= "'.$this->endDate.'"
                    AND em.meta_key = "'.Apollo_DB_Schema::_APOLLO_EVENT_END_DATE.'"
            )';
        }

        /**
         * Optimize dumplicate query
         *
         */
        if ($isSearchStartDate && $isSearchEndDate) {
            $sqlStringCase1 = ' '.$wpdb->posts.'.ID IN (
                SELECT em.apollo_event_id
                FROM '.$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}.' em
                WHERE em.apollo_event_id = '.$wpdb->posts.'.ID
                    AND em.meta_value >= "'.$this->startDate.'" AND em.meta_value <= "'.$this->endDate.'"
                    AND em.meta_key = "'.Apollo_DB_Schema::_APOLLO_EVENT_START_DATE.'"
            )';

            $sqlStringCase2 = ' '.$wpdb->posts.'.ID IN (
                SELECT em.apollo_event_id
                FROM '.$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}.' em
                WHERE em.apollo_event_id = '.$wpdb->posts.'.ID
                    AND em.meta_value >= "'.$this->startDate.'" AND em.meta_value <= "'.$this->endDate.'"
                    AND em.meta_key = "'.Apollo_DB_Schema::_APOLLO_EVENT_END_DATE.'"
            )';
        }


        //search by all case
        if( $isSearchStartDate == true && $isSearchEndDate == true){
            $sqlStringAllCase = '';
            if($sqlStringCase2 != '')
                $sqlStringAllCase = '('.$sqlStringCase1.') OR ('.$sqlStringCase2.')';
            if($sqlStringCase3 != '')
                $sqlStringAllCase .= 'OR ('.$sqlStringCase3.')';
            if($sqlStringAllCase != '')
                $sqlString .= ' AND ( '.$sqlStringAllCase.' ) ';
        }
        //search all event have start_date >= search_start_date, search_end_date empty
        if($isSearchStartDate == true && $isSearchEndDate == false){
            $sqlString .= ' AND '.$sqlStringCase1;
        }

        //search all event have end_date <= search_end_date, search_end_date empty
        if($isSearchStartDate == false && $isSearchEndDate == true){
            $sqlString .= ' '.$sqlStringCase2;
        }

        return $sqlString;
    }
}