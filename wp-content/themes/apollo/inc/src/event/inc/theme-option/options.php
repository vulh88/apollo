<?php

class ThemeOptionEventDetailPage {

    private static $instance;

    private $iconPosition;
    private $eventIcons;
    private $positionSocial;
    private $dateDisplay;
    private $thumbsUpEnable;
    private $thumbsUpPosition;

    private $enableIndividualDateTime = 1;
    private $characterDescription = 500;
    private $displayWithoutViewMore = 0;

    private function __construct()
    {
        $this->setData();
    }

    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = new ThemeOptionEventDetailPage();
        }

        return self::$instance;
    }

    private function setData() {
        $this->setIconPosition(of_get_option(Apollo_DB_Schema::_EVENT_SHOW_ICON_POSITION, ''));
        $this->setEventIcons(Apollo_App::unserialize( get_option( Apollo_DB_Schema::_APL_EVENT_ICONS ) ));
        $this->setPositionSocial(of_get_option(Apollo_DB_Schema::_EVENT_SOCIAL_MEDIA_LOCATION_DETAIL, 1));
        $this->setThumbsUpEnable(of_get_option(Apollo_DB_Schema::_ENABLE_THUMBS_UP, 1));
        $this->setThumbsUpPosition(of_get_option(Apollo_DB_Schema::_EVENT_SHOW_THUMBS_UP_POSITION, 'default'));

        if(is_single()) {
            //For only single event
            $this->setEnableIndividualDateTime(of_get_option(Apollo_DB_Schema::_ENABLE_INDIVIDUAL_DATE_TIME, 1));
            $this->setCharacterDescription(of_get_option(Apollo_DB_Schema::_APL_EVENT_CHARACTERS_DESCRIPTION, 500));
            $this->setDisplayWithoutViewMore(of_get_option(Apollo_DB_Schema::_ENABLE_DISPLAY_WITHOUT_VIEW_MORE, 0));
            $this->setDateDisplay(of_get_option(Apollo_DB_Schema::_EVENT_DETAIL_PAGE_DATE_DISPLAY_OPTIONS, 'default'));
        }

        /**
         * @ticket #19566: TF - Apollo - Octave - Optimize event category
         */
        if(is_tax()){
            $this->setDateDisplay(of_get_option(Apollo_DB_Schema::_CATEGORY_PAGE_DATE_DISPLAY_OPTIONS,'default'));
        }
    }

    /**
     * @return mixed
     */
    public function getIconPosition()
    {
        return $this->iconPosition;
    }

    /**
     * @param mixed $iconPosition
     */
    public function setIconPosition($iconPosition)
    {
        $this->iconPosition = $iconPosition;
    }

    /**
     * @return mixed
     */
    public function getEventIcons()
    {
        return $this->eventIcons;
    }

    /**
     * @param mixed $eventIcons
     */
    public function setEventIcons($eventIcons)
    {
        $this->eventIcons = $eventIcons;
    }

    /**
     * @return mixed
     */
    public function getPositionSocial()
    {
        return $this->positionSocial;
    }

    /**
     * @param mixed $positionSocial
     */
    public function setPositionSocial($positionSocial)
    {
        $this->positionSocial = $positionSocial;
    }

    /**
     * @return mixed
     */
    public function getThumbsUpEnable()
    {
        return $this->thumbsUpEnable;
    }

    /**
     * @param mixed $thumbsUpEnable
     */
    public function setThumbsUpEnable($thumbsUpEnable)
    {
        $this->thumbsUpEnable = $thumbsUpEnable;
    }

    /**
     * @return mixed
     */
    public function getThumbsUpPosition()
    {
        return $this->thumbsUpPosition;
    }

    /**
     * @param mixed $thumbsUpPosition
     */
    public function setThumbsUpPosition($thumbsUpPosition)
    {
        $this->thumbsUpPosition = $thumbsUpPosition;
    }


    /**
     * @return mixed
     */
    public function getDateDisplay()
    {
        return $this->dateDisplay;
    }

    /**
     * @param mixed $dateDisplay
     */
    public function setDateDisplay($dateDisplay)
    {
        $this->dateDisplay = $dateDisplay;
    }

    /**
     * @return mixed
     */
    public function getEnableIndividualDateTime()
    {
        return $this->enableIndividualDateTime;
    }

    /**
     * @param mixed $enableIndividualDateTime
     */
    public function setEnableIndividualDateTime($enableIndividualDateTime)
    {
        $this->enableIndividualDateTime = $enableIndividualDateTime;
    }

    /**
     * @return int
     */
    public function getCharacterDescription()
    {
        return $this->characterDescription;
    }

    /**
     * @param int $characterDescription
     */
    public function setCharacterDescription($characterDescription)
    {
        $this->characterDescription = $characterDescription;
    }

    /**
     * @return int
     */
    public function getDisplayWithoutViewMore()
    {
        return $this->displayWithoutViewMore;
    }

    /**
     * @param int $displayWithoutViewMore
     */
    public function setDisplayWithoutViewMore($displayWithoutViewMore)
    {
        $this->displayWithoutViewMore = $displayWithoutViewMore;
    }

}