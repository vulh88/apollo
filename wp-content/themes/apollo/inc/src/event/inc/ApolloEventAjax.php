<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class ApolloEventAjax {
    public function __construct()
    {

        $ajax_events = array(
            'show_more_artists_associated' => true,
            'get_more_artists_frontend' => true,
            'check_user_bookmark_object'  => true,
        );


        foreach ($ajax_events as $ajax_event => $nopriv) {
            add_action('wp_ajax_apollo_' . $ajax_event, array($this, $ajax_event));

            if ($nopriv)
                add_action('wp_ajax_nopriv_apollo_' . $ajax_event, array($this, $ajax_event));
        }

        if (Apollo_App::hasWPLM()) {
            global $sitepress;
            if (method_exists($sitepress, 'switch_lang') && isset($_GET['lang']) && $_GET['lang'] !== $sitepress->get_default_language()) {
                $sitepress->switch_lang($_GET['lang'], true);
            }
        }

    }

    /**
     * Get more artists
     *
     * @return object
     */
    public function show_more_artists_associated(){
        $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $event_id = isset($_REQUEST['event_id']) ? $_REQUEST['event_id'] : '';
        $event = get_event($event_id);
        $artists = $event->getAssociatedArtist(true, $page);
        $page++;
        wp_send_json(array(
            'url' => admin_url('admin-ajax.php?action=apollo_show_more_artists_associated&page=' . $page . '&event_id=' . $event_id . '&module=event'),
            'html' =>Apollo_App::getTemplatePartCustom(APOLLO_TEMPLATES_DIR. '/events/html/patials/associated-artist.php',array(
                'artists' => $artists['data'],
            ),true),
            'have_more' => $artists['have_more']
        ));
    }

    /**
     * Get more artists for frontend
     *
     * @return object
     */
    public function get_more_artists_frontend(){
        if(isset($_GET['post_id'])){
            $post_id = $_GET['post_id'];
            $textSearch = isset($_GET['text_search']) ? $_GET['text_search'] : '';
            $offset = isset($_REQUEST['offset']) ?intval($_REQUEST['offset']) : 0;

            $artists = APL_Artist_Function::getListArtistForEvent($offset, $textSearch, $post_id);

            $offset += count($artists['data']);

            wp_send_json(array(
                'html' => APL_Artist_Function::renderArtistsForEvent($artists['data'], true),
                'have_more' => $artists['have_more'],
                'offset' => $offset
            ));

        }else{
            wp_send_json(array(
                'html' => '',
                'have_more' => false,
            ));
        }
    }

    /**
     * Check user click ADDED button or not
     * @author vulh
     * @return mixed
     */
    public function check_user_bookmark_object() {
        $ids = $_POST['ids'];

        if (empty($ids)) {
            wp_send_json(array(
                'status' => false
            ));
            return false;
        }

        $aplQuery = new Apl_Query(Apollo_Tables::_BOOKMARK);
        $postType = $_POST['post_type'];
        $userID = get_current_user_id();

        $items = $aplQuery->get_where("user_id = $userID AND post_type = '$postType' AND post_id IN ($ids)");

        $result = array();
        if ($items) {
            foreach($items as $item) {
                $result[] = $item->post_id;
            }
        }

        wp_send_json(array(
            'status' => true,
            'result'  => $result
        ));
    }
}

new ApolloEventAjax();
