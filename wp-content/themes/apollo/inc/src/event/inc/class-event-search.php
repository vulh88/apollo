<?php
require_once APOLLO_TEMPLATES_DIR. '/pages/lib/class-apollo-page-module.php';

class Apollo_Event_Page extends Apollo_Page_Module {

    // These properties are used for event print feature
    private $_isPrintingSearchEventResult = false;
    private $_printEventIDs = array();
    private $_printType = '';
    static $_isSeparatedPrimaryEventCategory = false;
    static $_forSolr = false;
    static $_incNonePrimaryCategory = false;

    // Avoid include post content in the select if not needed
    static $exceptPostContentInSelect = false;

    static $officialTags = false;

    /**
     * @ticket #19409: Auto-fill the events to the home spotlight - Item 2
     */
    static private $spotlightToday = false;

    /**
     * @param mixed $spotlightToday
     */
    public static function setSpotlightToday($spotlightToday)
    {
        self::$spotlightToday = $spotlightToday;
    }

    public function __construct($exceptPostContentInSelect = false, $isCounting = false, $officialTags = false) {

        /* @ticket #15646 */
        self::setIsCounting($isCounting);

        self::$exceptPostContentInSelect = $exceptPostContentInSelect;

        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EVENT_PT ) ) {
            wp_safe_redirect( '/' );
        }

        self::$officialTags = $officialTags;
        parent::__construct();
    }

    public static function getThemeToolData() {
        global $wpdb;
        $keyword = self::getKeyword();

        if (! $keyword) return '';

        $themeToolTbl = $wpdb->{Apollo_Tables::_APL_THEME_TOOL};
        $sql = "
            SELECT * FROM $wpdb->terms term
            INNER JOIN $themeToolTbl AS tt ON tt.cat_id = term.term_id
            WHERE term.name LIKE '%$keyword%' AND (tt.leave_as_normal = 0 OR tt.leave_as_normal IS NULL)
        ";

        $themeTool = new Apollo_Theme_Tool();
        $data = $themeTool->getStorageData($wpdb->get_results($sql));
        $eventMetaTbl = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};
        $venueMetaTbl = $wpdb->{Apollo_Tables::_APL_VENUE_META};

        $venueWhereArr = array();

        if ( $data['cities'] ) {
            $venueWhereArr[] = "vm.meta_key = '".Apollo_DB_Schema::_VENUE_CITY."'
                            AND vm.meta_value IN ('".implode("','", $data['cities']). "')";
        }

        if ( $data['zips'] ) {
            $venueWhereArr[] = "vm.meta_key = '".Apollo_DB_Schema::_VENUE_ZIP."'
                        AND vm.meta_value IN ('".implode("','", $data['zips']). "')";
        }

        $venueWhere = !empty($venueWhereArr) ? implode(' OR ', $venueWhereArr) : '';

        $locationSql = $venueWhere ? "
            SELECT mt.apollo_event_id as event_id
            FROM $eventMetaTbl mt
            WHERE mt.meta_key = '".Apollo_DB_Schema::_APOLLO_EVENT_VENUE."'
            AND
            mt.meta_value IN (
                SELECT vm.apollo_venue_id as venue_id
                FROM $venueMetaTbl vm
                WHERE $venueWhere
            )

        " : '';
        $eventIdLocationResult = $wpdb->get_col($locationSql);

        return array(
            'event_ids_location' => $eventIdLocationResult,
            'event_ids'          => $data['events']
        );
    }

    /*
     * @Ticket #14423
     * author: hieulc
     */
    public static function eventsPrintSeparateByPC($eventData)
    {
        $result = array();
        $eventNonePrimaryKey = array();
        foreach ($eventData as $pcEvent) {
            $pcEvent = is_object($pcEvent) ? get_object_vars($pcEvent) : $pcEvent;
            $eventTypeID = $pcEvent['primary_cate_id'];
            $eventID = $pcEvent['event_id'];

            if(empty($eventTypeID)) {
                $eventNonePrimaryKey[] = $eventID;
            } else {
                $result[$eventTypeID][] = $eventID;
            }
        }

        if(count($eventNonePrimaryKey)) {
            $result[0] = $eventNonePrimaryKey;
        }
        return $result;
    }

    // Implement print search results events
    public function  printSearchEventResult(){
        $this->_isPrintingSearchEventResult = true;
        if(isset($_POST['prt-event-cb']) && !empty($_POST['prt-event-cb'])){
            $this->_printEventIDs = $_POST['prt-event-cb'];
        }
        if(isset($_POST['save_to_pdf_btn']) && !empty($_POST['save_to_pdf_btn'])){
            $this->_printType = $_POST['save_to_pdf_btn'];
        }
        else if (isset($_POST['print_btn']) && !empty($_POST['print_btn'])){
            $this->_printType = $_POST['print_btn'];
        }

        self::$_isSeparatedPrimaryEventCategory = true;
        self::$_incNonePrimaryCategory = true;
        if(isset($_GET['total_result'])) {
            $this->search(intval($_GET['total_result']));
        } else {
            $this->search();
        }
        $printEvents = $this->get_results();
        $printEvents = self::eventsPrintSeparateByPC($printEvents);

        $searchTerms = $this->render_result_title(Apollo_DB_Schema::_EVENT_PT, 'M d, Y', false);

        if($this->_printType === 'saveToPDF'){
            $pdfContent = Apollo_App::getTemplatePartCustom(APOLLO_TEMPLATES_DIR. '/events/html/event-search-save-to-pdf.php',array(
                'searchResults' => $printEvents,
                'printType' => $this->_printType,
                'searchTerms' => $searchTerms
            ),true);
            try
            {
                require_once(APOLLO_INCLUDES_DIR . "/tools/pdflib/mpdf60/mpdf.php");

                $mpdf = new mPDF('c','A4','','' , 0 , 0 , 0 , 0 , 0 , 0);

                $mpdf->SetDisplayMode('fullpage');

                $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list

                $mpdf->WriteHTML($pdfContent);
                header("Content-type:application/pdf");
                $curDate = date('mdY',time());
                $siteName = preg_replace('/www\./i', '', $_SERVER['SERVER_NAME']);
                $pdfFileName = $siteName . "-event-listing-" . $curDate . ".pdf";
                $mpdf->Output($pdfFileName,"D");
            }
            catch(Exception $e) {
                echo $e;
                exit;
            }
        } else {
            if(isset($_GET['debugging'])){
                Apollo_App::getTemplatePartCustom(APOLLO_TEMPLATES_DIR. '/events/html/event-search-save-to-pdf.php',array(
                    'searchResults' => $printEvents,
                    'printType' => $this->_printType
                ),false);
                exit;
            }
            Apollo_App::getTemplatePartCustom(APOLLO_TEMPLATES_DIR. '/events/html/event-search-print.php',array(
                'searchResults' => $printEvents,
                'printType' => $this->_printType,
                'searchTerms' => $searchTerms
            ),false);
        }
        exit;
    }

    public function getPrintEventUrl(){
        $curFullUrl = self::getCurFullUrl();
        $curFullUrlSplit = explode("?",$curFullUrl);
        if(count($curFullUrlSplit) > 1 && !empty($curFullUrlSplit[1])){
            return $curFullUrl . '&print_event_search_result=1' . ($this->total > 0 ? '&total_result=' . $this->total : '');
        } else {
            return $curFullUrl . '?print_event_search_result=1'. ($this->total > 0 ? '&total_result=' . $this->total : '');
        }
    }

    public function getOffset() {
        return intval(($this->page - 1) * $this->pagesize);
    }


    /**
     * Get term taxes by term
     * @author vulh
    */
    function getTermTaxesByTerms($terms) {

        if (!is_array($terms)) $terms = array($terms);

        global $wpdb;

        $termArr = $terms;
        foreach($terms as $term) {
            $childTerms = get_term_children( $term, 'event-type' );
            if ($childTerms) {
                $termArr = array_merge($termArr, $childTerms);
            }
        }

        $result = $wpdb->get_col("
					SELECT term_taxonomy_id
					FROM $wpdb->term_taxonomy
					WHERE term_id IN (".implode(',', $termArr).")
				" );
        return empty($result) ? array(-1): $result;
    }

    /**
     * Get events by term name
     * @author vulh
     */
    function getObjectByTermName($keyword) {
        global $wpdb;

        $result = $wpdb->get_col("
            select object_id from $wpdb->term_relationships tr
            join $wpdb->term_taxonomy tt ON tt.term_taxonomy_id = tr.term_taxonomy_id
            join $wpdb->terms t ON t.term_id = tt.term_id
            where t.name LIKE '%$keyword%'
        ");

        return empty($result) ? array(-1) : $result;
    }

    public static function getVenuesByAccessibility($access = array()) {
        global $wpdb;
        $result = array();
        if (empty($access)) {
            return $result;
        } else {
            //query Venue by custom-accessbility

            $sqlString = "SELECT apollo_venue_id FROM ".$wpdb->{Apollo_Tables::_APL_VENUE_META} ." WHERE meta_key = '".Apollo_DB_Schema::_VENUE_ACCESSIBILITY."' AND (";
            for($i = 0; $i < sizeof($access); $i++) {
                $sqlString .= $wpdb->prepare("meta_value LIKE '%s'", "%$access[$i]%");
                if ($i < sizeof($access) -1) {
                   $sqlString .= " OR ";
                }
            }
            $sqlString .= " )";

            $venues = $wpdb->get_col($sqlString);
            return empty($venues) ? array(-1) : $venues;

        }
    }


    public function search($perPage = false, $forSolr = false, $eventIds = array()) {

        self::$_forSolr = $forSolr;

        if (!$perPage) {
            $perPage = Apollo_App::aplGetPageSize($this->pagesize , Apollo_DB_Schema::_EVENT_NUM_ITEMS_LISTING_PAGE);
        }


        $arr_params = array(
            'post_type'         => Apollo_DB_Schema::_EVENT_PT,
            'posts_per_page'    =>  self::getIsCounting() ? 1 : $perPage,
            'paged'             => $this->page,
            'post_status'       => array('publish'),
        );

        /**
         * @ticket 19409: Auto-fill the events to the home spotlight - Item 2
         */
        if(!empty($eventIds)){
            $arr_params['post__not_in'] = $eventIds;
        }

        $printEventMaxPostPerPage = intval(of_get_option(Apollo_DB_Schema::_MAXIMUM_EVENT_DISPLAY_IN_EVENT_PRINT,''));

        if($this->_isPrintingSearchEventResult){
            $limit = $perPage > $printEventMaxPostPerPage ? $printEventMaxPostPerPage : $perPage;

            if (!$limit) {
                $limit = $printEventMaxPostPerPage > 0 ? $printEventMaxPostPerPage : -1;
            }

            $arr_params = array(
                'post_type'         => Apollo_DB_Schema::_EVENT_PT,
                'posts_per_page'    => $limit,
                'post_status'       => array('publish')
            );
        }

        if(!empty($this->_printEventIDs) && is_array($this->_printEventIDs)){
            $arr_params = array(
                'post_type'         => Apollo_DB_Schema::_EVENT_PT,
                'posts_per_page'    => !empty($printEventMaxPostPerPage) ? intval($printEventMaxPostPerPage) : -1,
                'post_status'       => array('publish'),
                'post__in'          => $this->_printEventIDs
            );
        }

        if($keyword = self::getKeyword()) {
            $arr_params['s'] =  $keyword;
        }

        if(isset($_GET['term']) && !empty($_GET['term'])) {

            $arr_tax_query[] = array(
                'taxonomy'=>'event-type',
                'terms' => array($_GET['term']),
            );
            $arr_params['tax_query'] = $arr_tax_query;
        }

        // Set new offset if this is ajax action
        $this->addOffsetToParams($arr_params);

        add_filter('posts_fields', array(__CLASS__, 'filter_select_event_tbl'), 10, 1);
        add_filter('posts_where', array(__CLASS__, 'filter_where_event_tbl'), 10, 1);
        add_filter('posts_join', array(__CLASS__, 'filter_join_event_tbl'), 10, 1);
        add_filter('posts_orderby', array(__CLASS__, 'filter_order_event_tbl'), 10, 1);
        add_filter('posts_groupby', array($this, 'filter_groupby'), 10, 1);
        add_filter( 'posts_search', array(__CLASS__, 'posts_search'), 10, 1);
        add_filter( 'posts_request', array(__CLASS__,'dump_request'));
        $this->result = query_posts($arr_params);

        // Do not need to run this function when sync data for solr
        if (! self::$_forSolr) {
            Apollo_Next_Prev::updateSearchResult($GLOBALS['wp_query']->request,Apollo_DB_Schema::_EVENT_PT);
        }

        remove_filter('posts_request', array(__CLASS__, 'dump_request'), 10);
        remove_filter('posts_fields', array(__CLASS__, 'filter_select_event_tbl'), 10);
        remove_filter('posts_orderby', array(__CLASS__, 'filter_order_event_tbl'), 10);
        remove_filter('posts_join', array(__CLASS__, 'filter_join_event_tbl'), 10);
        remove_filter('posts_where', array(__CLASS__, 'filter_where_event_tbl'), 10);
        remove_filter('posts_groupby', array($this, 'filter_groupby'), 10);
        remove_filter( 'posts_search', array(__CLASS__, 'posts_search'), 10);

        if (isset($_GET['testing'])) {
            echo $GLOBALS['wp_query']->request;
        }

        $this->total = $GLOBALS['wp_query']->found_posts;
        $this->total_pages = ceil($this->getTotal() / $this->pagesize);
        $this->template = APOLLO_TEMPLATES_DIR. '/events/listing-page/'. $this->_get_template_name(of_get_option(Apollo_DB_Schema::_EVENT_DEFAULT_VIEW_TYPE));
        $this->resetPostData();
    }

    public static function dump_request( $input ) {
        if(isset($_GET['is_debugging'])){
            aplDebug($input,1);
        }
        return $input;
    }

    public static function posts_search($p) {
        $keyword = self::getKeyword();

        if ( ! $keyword ) return $p;

        global $wpdb;

        // Search follow theme tool
        $themeToolSearch = self::getThemeToolData();

        $themeToolWhere = array();
        if ($themeToolSearch['event_ids']) {
            $themeToolWhere[] = "$wpdb->posts.ID IN (".  implode(',', $themeToolSearch['event_ids']).") ";
        }

        if ($themeToolSearch['event_ids_location']) {
            $themeToolWhere[] = "  $wpdb->posts.ID IN ( ".implode(',', $themeToolSearch['event_ids_location'])." ) ";
        }

        $themeToolWhere = $themeToolWhere ? implode(' OR ', $themeToolWhere). ' OR ' : '';

        $termSearch = self::getWhereTaxByKeyword($p);

        $orgEventIdsByKeyword = self::_getEventIDsOnSearchORGNameByKeyword($keyword);

        $orgSearch = " $termSearch OR $wpdb->posts.ID IN (".implode(',', $orgEventIdsByKeyword).")";

        // apply for search keyword with Venue name => load all events have registered with Venue name like '%keyword%'
        $eventIDWithKeywordSearchOnVenueName = self::_getEventIDsOnSearchVenueNameByKeyword($keyword);
        $venueSearch = ' OR  '.$wpdb->posts.'.ID IN (
                '.implode(',',$eventIDWithKeywordSearchOnVenueName).'
            )';
        /** @Ticket #18851 */
        /** check keyword has apostrophes */
        $eventTitle = self::replaceSpecialCharacter("{$wpdb->posts}.post_title", self::$specialString, $keyword);
        return str_replace('AND (((', " AND ( $themeToolWhere $orgSearch $venueSearch $eventTitle OR ((( ", $p) . ')';
    }

    public static function filter_select_event_tbl($select) {
        global $wpdb;

        $postTbl = $wpdb->posts;

        // Do not need to select any column when counting
        if (self::getIsCounting()) {
            return '1';
        }

        if (self::$_forSolr) {
            $select = $postTbl. '.post_title, ' .$postTbl. '.ID';
        }
        else {
            $select = $postTbl. '.post_name, '. $postTbl. '.post_type, ' .$postTbl. '.post_excerpt, ' . $postTbl. '.post_title, ' .$postTbl. '.ID, '.AplEventFunction::buildSqlDateRange('start_date', 'mt_end_d').', (case when (preferred_search.meta_value = 1) THEN 1 ELSE 0 END) preferred_search';
        }

        if (!self::$exceptPostContentInSelect) {
            $select .= ", $postTbl .post_content ";
        }


        // Thienld: join tmp table for separating list events by their primary categories
        if (self::$_isSeparatedPrimaryEventCategory) {
            $select .= ', ' . $wpdb->posts . '.ID AS event_id, primary_category_tbl.pc_primary_category_id AS primary_cate_id, primary_category_tbl.pc_primary_category_name AS primary_cate_name ';
        }

        /*@ticket: #17283 */
        if(!empty($_GET['apl-lat'])  && !empty($_GET['apl-lng'])
            && (!empty($_GET['by_my_location']) && !empty($_GET['min_lat'])
            && !empty($_GET['max_lat']) && !empty($_GET['min_lng']) && !empty($_GET['max_lng']))) {

            $select .=  ", IF(venueLat.meta_value IS NOT NULL AND venueLng.meta_value IS NOT NULL,
                            (6371 *
                                acos(
                                    cos(RADIANS({$_GET['apl-lat']}))
                                    * COS(RADIANS(venueLat.meta_value))
                                    * COS(RADIANS(venueLng.meta_value) - RADIANS({$_GET['apl-lng']}))
                                    +SIN(RADIANS({$_GET['apl-lat']}))
                                    * SIN(RADIANS(venueLat.meta_value))
                                )
                            ),
                            NULL) AS distance";
        }

        return $select;
    }

    public function filter_groupby($groupby){
        global $wpdb;
        if(self::$_isSeparatedPrimaryEventCategory){
            return $wpdb->posts . ".ID, primary_category_tbl.pc_primary_category_id, primary_category_tbl.pc_primary_category_name ";
        }
        return "$wpdb->posts.ID";

    }

    //hook where query
    /***
     *
     * we have 2 search date elements in search form (2 params: search_start_date, search_end_date), we have 3 case for 2 params
     * Case1: search_start_date NOT EMPTY && search_end_date NOT EMPTY
     * ---we have 3 case for this:
     *-----     Case-1-1: search_start_date <= event_start <= search_end_date
     *-----     Case-1-2: search_start_date <= event_end <= search_end_date
     *-----     Case-1-3: event_start <= [search_start,search_end] <= event_end
     *-----     All cases (1-1,1-2,1-3) of case 1: we have a query string (Case-1-1) OR (Case-1-2) OR (Case-1-3)
     *
     * Case2: search_start_date NOT EMPTY && search_end_date EMPTY
     * -----    Get all events have start_date >= search_start_date
     *
     * Case3: search_start_date EMPTY && search_end_date NOT EMPTY
     * -----    Get all events have end_date <= search_end_date
     *
     * After query by date, name, location, category we NOT join it to ACTIVE EVENT QUERY.
     * Only join by search title, search, location
     */
    public static function filter_where_event_tbl ($where) {
        global $wpdb;

        $sqlString = '';
        $isFilteringByDiscount = (isset($_GET['is_discount']) && !empty($_GET['is_discount']));

        $currentDate = current_time('Y-m-d');

        // START ticket #11487 : re-format date from m-d-Y to Y-m-d (standard format)
        $startDate = !empty($_GET['start_date']) ? $_GET['start_date'] : false;
        $endDate   = !empty($_GET['end_date']) ? $_GET['end_date']   : false;

        /**
         * @ticket #19409:  Auto-fill the events to the home spotlight - Item 2
         */
        if(self::$spotlightToday){
            $startDate = $currentDate;
            $endDate = $currentDate;
        }

        $dateFormat = (!empty($_GET['date_format']) && $_GET['date_format'] == 'm-d-Y') ? 'm-d-Y' : 'Y-m-d';
        if (!empty($startDate)) {
            $startDate = Apollo_App::checkIsDateTime($startDate, $dateFormat) ? $startDate : '';
        }
        if (!empty($endDate)) {
            $endDate = Apollo_App::checkIsDateTime($endDate, $dateFormat) ? $endDate : '';
        }

        if ( isset($_GET['date_format']) && $_GET['date_format'] == 'm-d-Y' ) {
            if ( !empty($startDate) ) {
                $startDate = Apollo_App::formatDateFromMDYToYMD($startDate);
            }

            if ( !empty($endDate) ) {
                $endDate = Apollo_App::formatDateFromMDYToYMD($endDate);
            }
        }
        // END ticket #11487

        $argsDateFiltering = array(
            'start_date_filter' => $startDate,
            'end_date_filter' => $endDate,
            'current_date' => $currentDate,
            'event_alias' => 'ec'
        );

        /** @Ticket #13544 */
        if ( $isFilteringByDiscount ) {
            $discount_type = of_get_option(Apollo_DB_Schema::_EVENT_DISCOUNT_CHECKBOX_FILTER_TYPE, 'only_discount_url');
            if ( $discount_type != 'only_discount_url' ) {
                $eventCalendarQuery = '('. self::buildSQLEventsOfEventCalendar($argsDateFiltering). ') AND ';
            } else {
                $eventCalendarQuery = '';
            }
            $sqlString .= '
            AND (
                ' . $eventCalendarQuery . '
                (mt_discount.meta_key = "' . Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL . '" AND mt_discount.meta_value <> "")
            )
            ';
        }

        $eventCalendarQuery = self::getSQLAvailableEventsByDateFilter($argsDateFiltering);
        $sqlString .= ' AND '.$wpdb->posts.'.ID IN (
            '.implode(",", $eventCalendarQuery).'
        )';

        /** @Ticket #13396 */
        $access_option = of_get_option(Apollo_DB_Schema::_ENABLE_VENUE_ACCESSIBILITY_MODE, true);
        if ($access_option) {
            /** @Ticket #12940 - query by accessibility icons */
            if (isset($_GET['accessibility']) && !empty($_GET['accessibility'])) {
                $venue_ids = self::getVenuesByAccessibility($_GET['accessibility']);
            }
        }

        //query by venue
        if(!empty($_GET['event_location'])) {

            /** @Ticket - #12940 */
            if ( !isset($venue_ids) || in_array($_GET['event_location'], $venue_ids)) {
                $location = $_GET['event_location'];
                $eventIDByFilterVenueIDs = self::_getEventIDsByFilterVenue($location);
                $sqlString .= 'AND  '.$wpdb->posts.'.ID IN (
            '.implode(',',$eventIDByFilterVenueIDs).'
            )';
            } else {
                $sqlString .= "AND $wpdb->posts.ID IN (-1)";
            }

        } else if (isset($venue_ids)) {
            $eventIDByFilterVenueIDs = array();
            if (!empty($venue_ids)) {
                $eventIDByFilterVenueIDs = self::_getEventIDsByVenueFilterAccessibility($venue_ids);
            }
            $eventIDByCustomAccess = self::_getEventIDsByEventFilterAccessibility($_GET['accessibility']);
            $eventIDByFilterVenueIDs = array_merge($eventIDByFilterVenueIDs, $eventIDByCustomAccess);
            $sqlString .= 'AND  '.$wpdb->posts.'.ID IN (
            '.implode(',',$eventIDByFilterVenueIDs).'
            )';
        }

        /**
         * Search by ORG
         */
        //query by ORG
        if(!empty($_GET['event_org'])) {
            $orgId = $_GET['event_org'];
            $eventIdsOrg = self::_getEventIDsByFilterOrg($orgId);
            $sqlString .= 'AND  '.$wpdb->posts.'.ID IN (
                '.implode(',',$eventIdsOrg).'
            )';
        }
        // ==========================================


        //query by city
        if(isset($_GET['event_city']) && !empty($_GET['event_city'])) {
            $city = $_GET['event_city'];
            /** @Ticket #16482 - Filter with neighborhood */
            if (!empty($_GET['event_neighborhood'])) {
                $eventIDByFilterCity = self::_getEventIDsByFilterNeighborhood($_GET['event_neighborhood']);
                $eventIDByTmpVenue = array();
            } else {
                $eventIDByFilterCity = self::_getEventIDsByFilterCity($city);
                $eventIDByTmpVenue = self::_getEventIDsByTmpVenue();
            }
            $_eventIds = array_unique(array_merge($eventIDByFilterCity, $eventIDByTmpVenue));

            $sqlString .= ' AND '.$wpdb->posts.'.ID IN (
                '.implode(',',$_eventIds).' )';
        }

        //query by region
        if(isset($_GET['event_region']) && !empty($_GET['event_region'])) {
            $region = $_GET['event_region'];
            $wpdb->escape_by_ref($region);

            if (Apollo_App::enableMappingRegionZipSelection()) {
                $eventIDByFilterRegion = self::_getEventIDsByFilterZipcodeInRegion($region);
            }
            else {
                $eventIDByFilterRegion = self::_getEventIDsByFilterRegion($region);
            }

            if ($eventIDByFilterRegion) {
                $sqlString .= ' AND  '.$wpdb->posts.'.ID IN (
                    '.implode(',',$eventIDByFilterRegion).'
                )';
            }
        }

        $isSearchToday = isset($_GET['today']);

        if ( $isSearchToday ) {
            if ( isset($_GET['start_date']) ) {
                $_GET['start_date'] = current_time('Y-m-d');
            }

            if ( isset($_GET['end_date']) ) {
                $_GET['end_date'] = current_time('Y-m-d');
            }
        }

        $where .= $sqlString;
        $where = Apollo_App::hidePrivateEvent($where);
        return $where;
    }

    private static function _getEventIDsByFilterVenue($location){
        global $wpdb;
        $sql = 'SELECT em.apollo_event_id as event_id
                FROM '.$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}.' em
                WHERE
                    em.meta_value = '.$location.'
                    AND em.meta_key = "'.Apollo_DB_Schema::_APOLLO_EVENT_VENUE.'" GROUP BY event_id ';

        $result = $wpdb->get_col($sql);

        return empty($result) ? array(-1) : $result;

    }

    /**
     * @param $access_ids
     * @return array
     */
    private static function _getEventIDsByVenueFilterAccessibility($access_ids) {
        global $wpdb;
        $sql = "SELECT em.apollo_event_id as event_id
                FROM ".$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}." em 
                WHERE em.meta_value IN( ".implode(',', $access_ids)." ) 
                AND em.meta_key ='".Apollo_DB_Schema::_APOLLO_EVENT_VENUE."' GROUP BY event_id";
        $result = $wpdb->get_col($sql);

        return empty($result) ? array(-1) : $result;
    }

    /**
     * get event ids by accessibility
     * @param $access
     * @return array
     */
    private static function _getEventIDsByEventFilterAccessibility($access) {
        global $wpdb;
        $result = array();
        if (!empty($access)) {
            $sql_access = array();
            foreach ($access as $item) {
                array_push($sql_access, $wpdb->prepare(" em.meta_value LIKE '%s' ", "%$item%"));
            }
            $sql = "SELECT em.apollo_event_id as event_id
                FROM {$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}} em 
                WHERE em.meta_key='". Apollo_DB_Schema::_APOLLO_EVENT_CUSTOM_ACCESSIBILITY."' AND ( " . implode(' OR ', $sql_access) . " )";
            $result = $wpdb->get_col($sql);
        }
        return $result;
    }

    private static function _getEventIDsByFilterOrg($org){
        global $wpdb;
        $eventOrgTbl = $wpdb->{Apollo_Tables::_APL_EVENT_ORG};
        $sql = "SELECT post_id FROM $eventOrgTbl WHERE org_id = $org  GROUP BY post_id ";

        $result = $wpdb->get_col($sql);

        return empty($result) ? array(-1) : $result;

    }

    private static function _getEventIDsByFilterCity($city){
        global $wpdb;
        $sql = ' SELECT em.apollo_event_id as event_id
                FROM '.$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}.' em
                INNER JOIN '.$wpdb->{Apollo_Tables::_APL_VENUE_META}.' mt_venue ON em.meta_value = mt_venue.apollo_venue_id
                WHERE em.meta_key = "'.Apollo_DB_Schema::_APOLLO_EVENT_VENUE.'"
                    AND mt_venue.meta_key = "'.Apollo_DB_Schema::_VENUE_CITY.'"
                    AND mt_venue.meta_value = "'.$city.'" GROUP BY event_id ';

        $result = $wpdb->get_col($sql);
        return empty($result) ? array(-1) : $result;
    }

    /**
     * @Ticket #16482 - filter by neighborhood
     * @param $neighborhood
     * @return array
     */
    private static function _getEventIDsByFilterNeighborhood($neighborhood) {
        global $wpdb;
        $sql = ' SELECT em.apollo_event_id as event_id
                FROM '.$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}.' em
                INNER JOIN '.$wpdb->{Apollo_Tables::_APL_VENUE_META}.' mt_venue ON em.meta_value = mt_venue.apollo_venue_id
                WHERE em.meta_key = "'.Apollo_DB_Schema::_APOLLO_EVENT_VENUE.'"
                    AND mt_venue.meta_key = "'.Apollo_DB_Schema::_VENUE_NEIGHBORHOOD.'"
                    AND mt_venue.meta_value = "'.$neighborhood.'" GROUP BY event_id ';

        $result = $wpdb->get_col($sql);
        return empty($result) ? array(-1) : $result;
    }

    private static function _getEventIDsByFilterRegion($region){
        global $wpdb;
        $sql = 'SELECT em.apollo_event_id as event_id
                FROM '.$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}.' em
                INNER JOIN '.$wpdb->{Apollo_Tables::_APL_VENUE_META}.' mt_venue ON em.meta_value = mt_venue.apollo_venue_id
                WHERE em.meta_key = "'.Apollo_DB_Schema::_APOLLO_EVENT_VENUE.'"
                    AND mt_venue.meta_key = "'.Apollo_DB_Schema::_VENUE_REGION.'"
                    AND mt_venue.meta_value = "'.$region.'" GROUP BY event_id ';
        $result = $wpdb->get_col($sql);
        return empty($result) ? array(-1) : $result;
    }

    private static function _getEventIDsByTmpVenue()
    {
        global $wpdb;
        $tableEventMeta = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};
        $templeMetaKey = Apollo_DB_Schema::_APL_EVENT_TMP_VENUE;

        $queryInTempVenue = "
            SELECT mt.apollo_event_id
            FROM $tableEventMeta mt
            WHERE   mt.meta_key = '$templeMetaKey' AND
                    mt.meta_value LIKE '%{$_GET['event_city']}%'
            GROUP BY mt.apollo_event_id
        ";
        $result = $wpdb->get_col($queryInTempVenue);
        return empty($result) ? array(-1) : $result;
    }

    /**
     * Get all events have venue that contain zipcode in region selection
     *
     * @param $region
     * @return array $results
     */
    private static function _getEventIDsByFilterZipcodeInRegion($region) {

        $zipcodes = Apollo_Seach_Form_Data::getRegionZipcodes($region);

        if (! $zipcodes) {
            $zipcodes = array(false);
        };

        global $wpdb;
        $zipQuery = '"'. implode('","', $zipcodes). '"';
        $sql = 'SELECT em.apollo_event_id as event_id
                FROM '.$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}.' em
                INNER JOIN '.$wpdb->{Apollo_Tables::_APL_VENUE_META}.' mt_venue ON em.meta_value = mt_venue.apollo_venue_id
                WHERE em.meta_key = "'.Apollo_DB_Schema::_APOLLO_EVENT_VENUE.'"
                    AND mt_venue.meta_key = "'.Apollo_DB_Schema::_VENUE_ZIP.'"
                    AND mt_venue.meta_value IN('.$zipQuery.') GROUP BY event_id ';

        $result = $wpdb->get_col($sql);
        return empty($result) ? array(-1) : $result;
    }

    private static function _getEventIDsOnSearchVenueNameByKeyword($keyword) {
        global $wpdb;
        $sql = 'SELECT em.apollo_event_id as event_id
                FROM ' . $wpdb->{Apollo_Tables::_APOLLO_EVENT_META} . ' em
                INNER JOIN ' . $wpdb->posts . ' p_venue ON em.meta_value = p_venue.ID
                WHERE em.meta_key = "' . Apollo_DB_Schema::_APOLLO_EVENT_VENUE . '"
                    AND p_venue.post_type = "' . Apollo_DB_Schema::_VENUE_PT . '"
                    AND p_venue.post_title LIKE "%' . $keyword . '%" GROUP BY event_id';

        $result = $wpdb->get_col($sql);
        return empty($result) ? array(-1) : $result;
    }

    private static function _getEventIDsOnSearchORGNameByKeyword($keyword) {
        global $wpdb;
        $eventOrgTbl = $wpdb->{Apollo_Tables::_APL_EVENT_ORG};
        $sql = "SELECT orgEvent.post_id AS event_id
                FROM $eventOrgTbl as orgEvent
                INNER JOIN $wpdb->posts pOrg ON orgEvent.org_id = pOrg.ID AND pOrg.post_title LIKE '%$keyword%'
                GROUP BY event_id";

        $result = $wpdb->get_col($sql);
        return empty($result) ? array(-1) : $result;
    }

    //hook join query
    public static function filter_join_event_tbl ($join) {
        global $wpdb;

        /*@ticket #17236*/
        if(self::$officialTags){
            global $post;
            $join .= " JOIN {$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}} official_tags ON $wpdb->posts.ID   = official_tags.apollo_event_id 
            AND official_tags.meta_key = '". Apollo_DB_Schema::_APL_EVENT_OFFICIAL_TAGS."' 
            AND (" . self::$officialTags . ")   
            AND $wpdb->posts.ID != $post->ID";
        }

        $join .= " LEFT JOIN {$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}} preferred_search ON $wpdb->posts.ID   = preferred_search.apollo_event_id AND preferred_search.meta_key = '". Apollo_DB_Schema::_E_PREFERRED_SEARCH ."' ";

        $join .= " LEFT JOIN {$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}} mt_end_d ON $wpdb->posts.ID   = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."' ";

        $join .= self::getJoinTaxByKeyword();

        $join .= ' INNER JOIN '.$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}.' start_date ON start_date.meta_key = "'.Apollo_DB_Schema::_APOLLO_EVENT_START_DATE.'" AND start_date.apollo_event_id = '.$wpdb->posts.'.ID ';

        /** @Ticket #13396 */
        if (!empty($_GET['accessibility']) && !of_get_option(Apollo_DB_Schema::_ENABLE_VENUE_ACCESSIBILITY_MODE, true)) {
            $access_search = array();
            foreach ($_GET['accessibility'] as $access) {
                array_push($access_search, $wpdb->prepare(" mt_access.meta_value LIKE '%s' ", "%$access%"));
            }
            $join .= " INNER JOIN {$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}} mt_access ON $wpdb->posts.ID = mt_access.apollo_event_id AND mt_access.meta_key='". Apollo_DB_Schema::_APOLLO_EVENT_CUSTOM_ACCESSIBILITY ."' AND ( " . implode( ' OR ', $access_search) . " )";
        }


        if ( isset($_GET['is_discount']) && !empty($_GET['is_discount']) ) {
            $join .= ' INNER JOIN '.$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}.' mt_discount ON mt_discount.apollo_event_id = '.$wpdb->posts.'.ID ';
            $join .= ' INNER JOIN '.$wpdb->{Apollo_Tables::_APL_EVENT_CALENDAR}.' ec ON ec.event_id = '.$wpdb->posts.'.ID ';
        }

        /**
         * Thienld: join tmp table for separating list events by their primary categories
         * print page
        */

        /*
         * @Ticket #14423
         * author: hieulc
         */
        if (self::$_isSeparatedPrimaryEventCategory) {
            $join .= (self::$_incNonePrimaryCategory ? " LEFT " : " INNER ") . ' JOIN (
                SELECT empc.apollo_event_id AS pc_event_id, empc.meta_value AS pc_primary_category_id, cq_evt.name AS pc_primary_category_name
                FROM ' . $wpdb->{Apollo_Tables::_APOLLO_EVENT_META} . ' empc
                INNER JOIN ' . $wpdb->terms . ' cq_evt ON empc.`meta_value` = cq_evt.`term_id`
                WHERE empc.meta_key = "' . Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID . '"
                GROUP BY empc.apollo_event_id, empc.meta_value, cq_evt.name
            ) AS primary_category_tbl
            ON ' . $wpdb->posts . '.ID = primary_category_tbl.pc_event_id
        ';
        }

        if (isset($_GET['free_event'])) {
            $join .= " INNER JOIN " . $wpdb->{Apollo_Tables::_APOLLO_EVENT_META} . " mt_free ON mt_free.apollo_event_id = " . $wpdb->posts.".ID AND mt_free.meta_key='". Apollo_DB_Schema::_APOLLO_EVENT_FREE_ADMISSION ."' AND mt_free.meta_value='".$_GET['free_event']."'";
        }

        /*@ticket: #17283 */
        if(!empty($_GET['apl-lat'])  && !empty($_GET['apl-lng'])
        && (!empty($_GET['by_my_location']) && !empty($_GET['min_lat'])
        && !empty($_GET['max_lat']) && !empty($_GET['min_lng']) && !empty($_GET['max_lng']))){
            $minLat = $_GET['min_lat'];
            $maxLat = $_GET['max_lat'];
            $minLng = $_GET['min_lng'];
            $maxLng = $_GET['max_lng'];

            $join .= "
                    INNER JOIN {$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}} metaEvent ON (ID = metaEvent.apollo_event_id AND metaEvent.meta_key = '".Apollo_DB_Schema::_APOLLO_EVENT_VENUE."')
                    INNER JOIN {$wpdb->{Apollo_Tables::_APL_VENUE_META}} venueLat ON metaEvent.meta_value = venueLat.apollo_venue_id 
                        AND venueLat.meta_key = '".Apollo_DB_Schema::_VENUE_LATITUDE."' AND venueLat.meta_value >= $minLat AND venueLat.meta_value <= $maxLat
                    INNER JOIN {$wpdb->{Apollo_Tables::_APL_VENUE_META}} venueLng ON metaEvent.meta_value = venueLng.apollo_venue_id 
                        AND venueLng.meta_key = '".Apollo_DB_Schema::_VENUE_LONGITUDE."' AND venueLng.meta_value >= $minLng AND venueLng.meta_value <= $maxLng
                    ";
        }
        return $join;
    }

    public static function getSQLAvailableEventsByDateFilter($args){
        global $wpdb;
        $startDateFilter = $args['start_date_filter'];
        $endDateFilter = $args['end_date_filter'];
        $currentDate = $args['current_date'];
        $resultSql = " SELECT evc.event_id
                       FROM ".$wpdb->{Apollo_Tables::_APL_EVENT_CALENDAR}." evc
                       WHERE 1=1
                     ";

        if(!empty($startDateFilter) && !empty($endDateFilter)){
            $resultSql .= " AND (evc.date_event BETWEEN \"".$startDateFilter."\" AND \"".$endDateFilter."\") ";
        } elseif (!empty($startDateFilter)){
            $resultSql .= " AND evc.date_event >= \"".$startDateFilter."\" ";
        } elseif (!empty($endDateFilter)){
            $resultSql .= " AND (evc.date_event BETWEEN \"".$currentDate."\" AND \"".$endDateFilter."\") ";
        } else {
            $resultSql .= " AND evc.date_event >= \"".$currentDate."\" ";
        }

        $resultSql .= " GROUP BY evc.event_id ";

        $result = $wpdb->get_col($resultSql);

        return empty($result) ? array(-1) : $result;
    }

    public static function buildSQLEventsOfEventCalendar($args){
        $startDateFilter = $args['start_date_filter'];
        $endDateFilter = $args['end_date_filter'];
        $eventAlias = $args['event_alias'];
        $currentDate = $args['current_date'];
        $resultSql = " ".$eventAlias.".ticket_url IS NOT NULL AND ".$eventAlias.".ticket_url <> '' ";

        if(!empty($startDateFilter) && !empty($endDateFilter)){
            $resultSql .= " AND (".$eventAlias.".date_event BETWEEN \"".$startDateFilter."\" AND \"".$endDateFilter."\") ";
        } elseif (!empty($startDateFilter)){
            $resultSql .= " AND ".$eventAlias.".date_event >= \"".$startDateFilter."\" ";
        } elseif (!empty($endDateFilter)){
            $resultSql .= " AND (".$eventAlias.".date_event BETWEEN \"".$currentDate."\" AND \"".$endDateFilter."\") ";
        } else {
            $resultSql .= " AND ".$eventAlias.".date_event >= \"".$currentDate."\" ";
        }
        return $resultSql;
    }

    /* last name is not query with city in same query: Good to know that. because we don't need join an extra query to sort by last_name  */
    public static function filter_order_event_tbl($order) {

        // Get result only for solr, we do not need to sorl result
        if (self::$_forSolr || self::getIsCounting()) {
            return '';
        }

        $arr_order = Apollo_Sort_Manager::getSortOneFieldForPostType('event');
        $orderArr = array();

        //preferred_search rule only for search event not for listing
        $orderArr[] = "preferred_search DESC";

        // Thienld: join tmp table for separating list events by their primary categories
        if (self::$_isSeparatedPrimaryEventCategory) {
            $orderArr[] = " primary_category_tbl.pc_primary_category_name ";
        }

        if($arr_order['type_sort'] === 'apollo_meta') {
            $orderArr[] = "date_range ASC";
            if(
                $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_START_DATE
                && $arr_order["metakey_name"] !== Apollo_DB_Schema::_APOLLO_EVENT_END_DATE
            ) {
                $orderArr[] = "tblSortMeta.meta_value {$arr_order['order']}";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_START_DATE) {
                $orderArr[] = "start_date.meta_value {$arr_order['order']}";
            }
            else if($arr_order["metakey_name"] === Apollo_DB_Schema::_APOLLO_EVENT_END_DATE) {
                $orderArr[] = "mt_end_d.meta_value {$arr_order['order']}";
            }
            $orderArr[] = "date_diff ASC";
        }
        else if($arr_order['type_sort'] === 'post') {
            global $wpdb;
            $orderArr[] = " {$wpdb->posts}.{$arr_order['order_by']} {$arr_order['order']} ";
        }

        $order = "";
        /*@ticket: #17283 */
        if(!empty($_GET['apl-lat'])  && !empty($_GET['apl-lng'])
        && (!empty($_GET['by_my_location']) && !empty($_GET['min_lat'])
        && !empty($_GET['max_lat']) && !empty($_GET['min_lng']) && !empty($_GET['max_lng']))) {

            $order = " ISNULL(distance), distance ASC, ";
        }

        if( $orderArr){
            $order .= implode(',',$orderArr);
        }

        return $order;
    }

    private static function queryActiveEventSQL($sqlString){
        global $wpdb;
        $currentDate = current_time('Y-m-d');
        $sqlString .= 'AND (  '.$wpdb->posts.'.ID IN (
                SELECT em.apollo_event_id
                FROM '.$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}.' em
                WHERE em.apollo_event_id = '.$wpdb->posts.'.ID
                    AND em.meta_value >= "'.$currentDate.'"
                    AND em.meta_key = "'.Apollo_DB_Schema::_APOLLO_EVENT_START_DATE.'"
            )';
        $sqlString .= 'OR (  '.$wpdb->posts.'.ID IN (
                SELECT em.apollo_event_id
                FROM '.$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}.' em
                WHERE em.apollo_event_id = '.$wpdb->posts.'.ID
                    AND em.meta_value >= "'.$currentDate.'"
                    AND em.meta_key = "'.Apollo_DB_Schema::_APOLLO_EVENT_END_DATE.'"
            ) ) )';
        return $sqlString;
    }

    /**
     * @return string
     */
    public function getCurrentTemplateName(){
        if (isset( $_REQUEST['view'])  && $_REQUEST['view'])
            return $_REQUEST['view'];

        $template = of_get_option(Apollo_DB_Schema::_EVENT_DEFAULT_VIEW_TYPE, 1);

        switch ( $template ):
            case 1:
                return 'thumb';
            case 2:
                return 'list';
        endswitch;

        return '';
    }

}
