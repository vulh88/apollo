<?php
require_once APOLLO_TEMPLATES_DIR. '/pages/lib/class-apollo-page-module.php';

class Apollo_Public_Art_Page extends Apollo_Page_Module {
    
    public function __construct($isCounting = false) {

        /* @ticket #15646 */
        self::$isCounting = $isCounting;

        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_PUBLIC_ART_PT ) ) {
            wp_safe_redirect( '/' );
        }
        
        parent::__construct();
    }

    public function search() {
        $arr_params = array(
            'post_type'         => Apollo_DB_Schema::_PUBLIC_ART_PT,
            'posts_per_page'    => self::getIsCounting() ? 1 : Apollo_App::aplGetPageSize($this->pagesize , Apollo_DB_Schema::_PUBLIC_ART_NUM_ITEMS_LISTING_PAGE),
            'paged'             => $this->page,
            'post_status'       => array('publish'),

        );

        if($keyword = self::getKeyword()) {
            $arr_params['s'] =  $keyword;
        }

        if(isset($_GET['term']) && !empty($_GET['term'])) {
            $arr_tax_query[] = array(
                'taxonomy'=> Apollo_DB_Schema::_PUBLIC_ART_PT.'-type',
                'terms' => array($_GET['term']),
            );
            $arr_params['tax_query'] = $arr_tax_query;
        }
        
        if(isset($_GET['collection']) && !empty($_GET['collection'])) {
            $arr_tax_query[] = array(
                'taxonomy'=> Apollo_DB_Schema::_PUBLIC_ART_COL,
                'terms' => array($_GET['collection']),
            );
            $arr_params['tax_query'] = $arr_tax_query;
        }
        
        if(isset($_GET['location']) && !empty($_GET['location'])) {
            $arr_tax_query[] = array(
                'taxonomy'=> Apollo_DB_Schema::_PUBLIC_ART_LOC,
                'terms' => array($_GET['location']),
            );
            $arr_params['tax_query'] = $arr_tax_query;
        }
        
        if(isset($_GET['medium']) && !empty($_GET['medium'])) {
            $arr_tax_query[] = array(
                'taxonomy'=> Apollo_DB_Schema::_PUBLIC_ART_MED,
                'terms' => array($_GET['medium']),
            );
            $arr_params['tax_query'] = $arr_tax_query;
        }

        // Set new offset if this is ajax action
        $this->addOffsetToParams($arr_params);

        //No need search in this time
        add_filter('posts_where', array(__CLASS__, 'filter_where_tbl'), 10, 1);
        add_filter('posts_join', array(__CLASS__, 'filter_join_tbl'), 10, 1);
        add_filter('posts_orderby', array(__CLASS__, 'filter_order_tbl'), 10, 1);
        add_filter('posts_search', array(__CLASS__, 'posts_search'), 10, 1);
        add_filter('posts_groupby', array($this, 'filter_groupby'), 10, 1);

        $this->result = query_posts($arr_params);

        Apollo_Next_Prev::updateSearchResult($GLOBALS['wp_query']->request,Apollo_DB_Schema::_PUBLIC_ART_PT);
        remove_filter('posts_orderby', array(__CLASS__, 'filter_order_tbl'), 10);
        remove_filter('posts_join', array(__CLASS__, 'filter_join_tbl'), 10);
        remove_filter('posts_where', array(__CLASS__, 'filter_where_tbl'), 10);
        remove_filter('posts_search', array(__CLASS__, 'posts_search'), 10, 1);
        remove_filter('posts_groupby', array($this, 'filter_groupby'), 10, 1);

        $this->setTotal($GLOBALS['wp_query']->found_posts);
        $this->total_pages = ceil($this->getTotal() / $this->pagesize);

        $this->template = APOLLO_TEMPLATES_DIR. '/public-art/listing-page/'. $this->_get_template_name(of_get_option(Apollo_DB_Schema::_PUBLIC_ART_DEFAULT_VIEW_TYPE));
        $this->resetPostData();
    }

    public static function filter_where_tbl ($where) {
        global $wpdb;
        
        // JUST ALPHA
        if(isset($_GET['_alpha']) && !empty($_GET['_alpha'])) {

            $_alpha = $_GET['_alpha'];
            $wpdb->escape_by_ref($_alpha);
            return $where ." AND CAST({$wpdb->posts}.post_title AS CHAR) LIKE '". $_alpha ."%'";
        }
        
        // NAME
        if(isset($_GET['_name']) && !empty($_GET['_name'])) {

            $_name = $_GET['_name'];
            $wpdb->escape_by_ref($_name);
            return $where ." AND CAST({$wpdb->posts}.post_title AS CHAR) LIKE '". $_name ."%'";
        }

        // OTHER CASE
        if(isset($_GET['city']) && !empty($_GET['city'])) {
            $city = $_GET['city'];

            $wpdb->escape_by_ref($city);
            $where .= " AND CAST(mt_city.meta_value AS CHAR) LIKE '%". $city ."%' ";
        }
        
        if(isset($_GET['zip']) && !empty($_GET['zip'])) {
            $zip = $_GET['zip'];

            $wpdb->escape_by_ref($zip);
            $where .= " AND CAST(mt_zip.meta_value AS CHAR) LIKE '". $zip ."%' ";
        }
        
        /*Artist*/
        if(isset($_GET['_artist']) && !empty($_GET['_artist'])) {
            $_artist = sprintf('%s%s%s', 's', $_GET['_artist'], 'e' );
            $wpdb->escape_by_ref($_artist);
            $where .= " AND CAST(mt_artist.meta_value AS CHAR) LIKE '%". $_artist ."%' ";
        }
        
        $where .= " AND {$wpdb->posts}.post_type = '".Apollo_DB_Schema::_PUBLIC_ART_PT."' ";
       
        return $where;
    }

    public static function filter_join_tbl ($join) {
        global $wpdb;
       
        $mtTbl = $wpdb->{Apollo_Tables::_APL_PUBLIC_ART_META};
        
        if(isset($_GET['city']) && !empty($_GET['city'])) {
            $join .=
                    " INNER JOIN {$mtTbl} mt_city ON "
                    . "(mt_city.meta_key = '". Apollo_DB_Schema::_PUBLIC_ART_CITY ."' AND {$wpdb->posts}.ID = mt_city.apollo_public_art_id)";
        }
        
        if(isset($_GET['zip']) && !empty($_GET['zip'])) {
            $join .=
                    " INNER JOIN {$mtTbl} mt_zip ON "
                    . "(mt_zip.meta_key = '". Apollo_DB_Schema::_PUBLIC_ART_ZIP ."' AND {$wpdb->posts}.ID = mt_zip.apollo_public_art_id)";
        }
        
        if(isset($_GET['_artist']) && !empty($_GET['_artist'])) {
            $join .=
                    " INNER JOIN {$mtTbl} mt_artist ON "
                    . "(mt_artist.meta_key = '". Apollo_DB_Schema::_APL_PUBLIC_ART_ARTIST_SEARCHING ."' AND {$wpdb->posts}.ID = mt_artist.apollo_public_art_id)";
        }
        
        $join .= self::getJoinTaxByKeyword();
        
        return $join;
    }

    /* last name is not query with city in same query: Good to know that. because we don't need join an extra query to sort by last_name  */
    public static function filter_order_tbl ($order) {
        // Ignore sort the result when counting
        if (self::getIsCounting()) {
            return false;
        }
        global $wpdb;
        $order= $wpdb->posts.'.post_title ASC';
        return $order;
    }

}
