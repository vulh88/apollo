<div class="apl-pb apl-pb-tags">
    <dl class="dropdown">
        <dt>
            <a href="javascript:void(0)">
                <span class="hida">Select tags</span>
                <p class="multiSel"></p>
            </a>
        </dt>

        <dd>
            <div class="mutliSelect">
                <ul>
                    <?php foreach ($data['tags'] as $tag): ?>
                    <li>
                        <label>
                            <input name="<?php echo $data['tags_name'] . '[]'; ?>"
                                   type="checkbox"

                                    <?php echo in_array($tag->id, $data['tags_associated']) ? 'checked' : '' ?>
                                    value="<?php echo $tag->id ?>"
                                    data-title="<?php echo $tag->value ?>"
                            />
                            <?php echo $tag->value ?>
                        </label>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </dd>
    </dl>
</div>