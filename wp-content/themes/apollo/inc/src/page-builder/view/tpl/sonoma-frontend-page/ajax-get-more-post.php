<?php

$post_type = $template_args['post_type'];

switch ($post_type) {
    case 'post':
        foreach ($template_args['posts']['data'] as $item) : ?>
            <div class="uk-width-large-1-4 uk-width-medium-1-3 uk-width-small-1-2 uk-width-1-1 article-item item-orange">
                <a href="<?php echo get_permalink($item['ID']) ?>" target="_blank" class="event-org-lk"></a>
                <div class="article-title">
                    <img src="<?php echo isset($item['circle_image_url']) ? $item['circle_image_url'] : $item['feature_image_url'] ?>"
                         alt="<?php echo $item['post_title'] ?>"
                         width=300
                         height=300
                         class="teaser-bg wp-post-image" />
                    <div class="content-teaser">
                        <p>
                            <a href="<?php echo get_permalink($item['ID']) ?>">
                                <span class="ev-tt" data-ride="" data-n="2"><?php echo $item['post_title'] ?></span>
                            </a>
                            <br>
                        </p>

                    </div>
                </div>
            </div>
        <?php endforeach;
        break;

    case Apollo_DB_Schema::_ARTIST_PT:
        foreach ($template_args['posts']['data'] as $item) : ?>
            <div class="uk-width-large-1-4 uk-width-medium-1-3 uk-width-small-1-2 uk-width-1-1 article-item item-orange">
                <a href="<?php echo get_permalink($item['ID']) ?>" target="_blank" class="event-org-lk"></a>
                <div class="article-title">
                    <img src="<?php echo $item['feature_image_url'] ?>"
                         alt="<?php echo $item['post_title'] ?>"
                         width=300
                         height=300
                         class="teaser-bg wp-post-image" />
                    <div class="content-teaser">
                        <p>
                            <a href="<?php echo get_permalink($item['ID']) ?>">
                                <span class="ev-tt" data-ride="" data-n="2"><?php echo $item['post_title'] ?></span>
                            </a>
                            <br>
                        </p>

                    </div>
                </div>
            </div>
        <?php endforeach;
        break;

    case Apollo_DB_Schema::_CLASSIFIED:
        foreach ($template_args['posts']['data'] as $item) : ?>
            <a class="item-events" target="_blank" href="<?php echo get_permalink($item['ID']) ?>">
                <div class="thumb" style="background-image: url(<?php echo $item['feature_image_url'] ?>)">
                    <i class="fa fa-clock-o"></i>
                </div>
                <div class="content">
                    <h3><?php echo $item['post_title'] ?></h3>
                    <time>
                        <?php echo $item['exp_date'] ; ?>
                        <?php echo !empty($item['deadline_date']) ? ' - ' . $item['deadline_date'] : '' ; ?>
                    </time>
                </div>

            </a>
        <?php endforeach;
        break;

    case Apollo_DB_Schema::_ORGANIZATION_PT:
        foreach ($template_args['posts']['data'] as $item) : ?>
            <div class="uk-width-large-1-4 uk-width-medium-1-3 uk-width-small-1-2 uk-width-1-1 article-item item-orange">
                <a href="<?php echo get_permalink($item['ID']) ?>" target="_blank" class="event-org-lk"></a>
                <div class="article-title">
                    <img src="<?php echo $item['feature_image_url'] ?>"
                         alt="<?php echo $item['post_title'] ?>"
                         width=300
                         height=300
                         class="teaser-bg wp-post-image" />
                    <div class="content-teaser">
                        <p>
                            <a href="<?php echo get_permalink($item['ID']) ?>">
                                <span class="ev-tt" data-ride="" data-n="2"><?php echo $item['post_title'] ?></span>
                            </a>
                            <br>
                        </p>

                    </div>
                </div>
            </div>
        <?php endforeach;
        break;
}
?>