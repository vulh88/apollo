jQuery(document).ready(function($){

    /* REMOVE  RECORD */
    $('body').on('click', '.apollo-tag-form-field .del', function () {
        var $this = $(this), desc = $this.data('confirm'), $parent = $this.parent();
        if (confirm(desc)) {
            $parent.remove();
        }
    });

    /* active menu page builder */
    activeMenuAddNewPageBuilder();

    function activeMenuAddNewPageBuilder() {
        var apl_searchParams = new URLSearchParams(window.location.search);
        if(apl_searchParams.get('module') == 'page-builder' && (apl_searchParams.get('post_type') == 'page' || apl_searchParams.get('action') == 'edit')) {
            $("#adminmenu .wp-menu-open").removeClass('wp-menu-open wp-has-current-submenu')
                .addClass(' wp-not-current-submenu');
            $("#adminmenu li.current").removeClass('current');
            $('#toplevel_page_apollo-page-builder-module')
                .addClass('current wp-has-current-submenu wp-menu-open').find('li:last-child').addClass('current');

            if($("#pageparentdiv").length > 0) {
                $("#pageparentdiv .post-attributes-label-wrapper label[for='parent_id']").remove();
                $("#pageparentdiv .post-attributes-label-wrapper label[for='menu_order']").remove();
                $("#pageparentdiv #parent_id, #pageparentdiv #menu_order").remove();
                $("#pageparentdiv .inside p:last-child").remove();
                $('#pageparentdiv #page_template option[value!="page_sonoma_fire_2017.php"]').remove();
            }

            //change href action new
            $(".wrap .wp-heading-inline ~ a.page-title-action").attr('href', 'post-new.php?post_type=page&module=page-builder');
        }
    }

    $('body').on('click', '.delete-tags', function (e) {
        e.preventDefault();
        var tag_id = $(this).data('tag-id');
        var msg_confirm = $(this).data('title-confirm');
        var _this = $(this);
        if(!tag_id) {
            confirmDeletetag($(this), msg_confirm);
            return;
        }

        $.ajax({
            url: APL.ajax_url + '?action=apl_pbm_admin_check_tagged',
            type: 'post',
            data: {tag_id: tag_id},
            beforeSend: function() {
                $(window).block($.apl.blockUI);
            },
            success: function(res) {
                $(window).unblock();
                if (res.status == true) {
                    msg_confirm = _this.data('title-confirm-tagged');
                }
                confirmDeletetag(_this, msg_confirm);
            },
            complete: function() {
                $(window).unblock();
            },
        });
    });

    function confirmDeletetag(ele, $msg_confirm) {
        var _confirm = confirm($msg_confirm);
        if (_confirm) {
            ele.closest('.apollo-tag-form-field').remove();
        } else {
            return false;
        }
    }

    //check all
    $( '.pbm-apl-select-all' ).click(function() {
        if ( $( this ).attr('checked') == 'checked' ) {
            $( '.pbm-apl-select-all' ).attr( 'checked', '' );
            $('.pbm-table-post input[type="checkbox"]').attr('checked', '');
        } else {
            $('.pbm-table-post  input[type="checkbox"]').removeAttr('checked');
            $( '.pbm-apl-select-all' ).removeAttr( 'checked' );
        }
    });

    //@Ticket 15249
    $("#page-builder-manage-display-sort").sortable({
        items: '.item-block-content-sort',
        cursor: 'move',
        axis: 'y',
        forcePlaceholderSize: true,
        helper: 'clone',
        opacity: 0.65,
        scrollSensitivity: 40,
        start: function(event, ui) {
        },
        stop: function(event, ui) {
        },
        update: function(event, ui) {
        }
    });
});