jQuery(document).ready(function($){
    /*
	Dropdown with Multiple checkbox select with jQuery - May 27, 2013
	(c) 2018 @hieuluong
    */

    $("body").on('click', ".apl-pb-tags .dropdown dt a", function(e) {
        e.preventDefault();
        var ul = $(this).closest('.apl-pb-tags').find(".dropdown dd ul");
        var currentVisible = ul.is(":visible");
        $(".apl-pb-tags .dropdown dd ul").hide();

        if(currentVisible) {
            ul.hide();
        } else ul.show();

    });

    $(".apl-pb-tags .dropdown dd ul li a").on('click', function(e) {
        e.preventDefault();
        $(this).closest('.apl-pb-tags').find(".dropdown dd ul").hide();
    });

    function getSelectedValue(id) {
        return $("#" + id).find("dt a span.value").html();
    }

    $(document).bind('click', function(e) {
        var $clicked = $(e.target);
        if (!$clicked.parents().hasClass("dropdown"))
            $(".apl-pb-tags .dropdown dd ul").hide();
    });

    $('body').on('click', '.apl-pb-tags .mutliSelect input[type="checkbox"]', function() {
        var $this = $(this);
        var apl_pb_tags = $(this).closest('.apl-pb-tags');
        var title = $this.closest('.mutliSelect').find('input[type="checkbox"]').data('title'),
            title = $this.data('title');

        if ($this.is(':checked')) {
            showItemChecked(apl_pb_tags, title);
        } else {
            apl_pb_tags.find('span[title="' + title + '"]').remove();
            if(apl_pb_tags.find('.tag-select-title').length <= 1) {
                apl_pb_tags.find('.flag-more').remove();
            }
            var ret = apl_pb_tags.find(".hida");
            apl_pb_tags.find('.dropdown dt a').append(ret);
            if(apl_pb_tags.find('.multiSel').html() == '')
                apl_pb_tags.find(".hida").show();
        }
    });

    $('.apl-pb-tags .mutliSelect input[type="checkbox"]:checked').each(function() {
        var title = $(this).data('title');
        if($(this).val()) {
            var apl_pb_tags = $(this).closest('.apl-pb-tags');
            showItemChecked(apl_pb_tags, title);
        }
    });

    function showItemChecked(apl_pb_tags, title) {
        var html = '<span class="tag-select-title" title="' + title + '">' + title + '</span><span title="' + title + '"class="comma">,</span>';
        if($("body.user-dashboard-body").length > 0) {
            if (apl_pb_tags.find('.flag-more').length > 0) {
                apl_pb_tags.find('.flag-more').remove();
            }
        }

        apl_pb_tags.find('.multiSel').append(html);
        apl_pb_tags.find(".hida").hide();

        if(apl_pb_tags.find('.tag-select-title').length <= 1) {
            apl_pb_tags.find('.flag-more').remove();
        }
    }
});