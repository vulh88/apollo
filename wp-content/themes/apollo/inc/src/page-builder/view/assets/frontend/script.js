jQuery(document).ready(function($) {
    $('#apl-pbm-popup-open-video').on('click', 'a.close', function(e) {
        e.preventDefault();
        $('#apl-pbm-popup-open-video').addClass('hidden');
        $('#apl-pbm-popup-open-video .video-wrapper').html('');
    });

    /**
     * Colorbox
     */
    $('.colorbox.youtube').colorbox({
        iframe: true,
        innerWidth: 640,
        innerHeight: 390,
        maxWidth: '95%',
        maxHeight: '95%',
    });
    $('.colorbox.vimeo').colorbox({
        iframe: true,
        innerWidth: 500,
        innerHeight: 409,
        maxWidth: '95%',
        maxHeight: '95%',
    });

    var pbm_width_window = $(window).width();
    var pbm_width_popup_audio = 100;
    if(pbm_width_window > 960) {
        pbm_width_popup_audio = 50;
    }

    $("#apl-pbm-block-audio .audio a").colorbox({inline:true, width: pbm_width_popup_audio + "%"});

    /**
     * Load view more
     *
     **/
    $('body').on('click', '.pbm-load-more', function(e) {
        e.preventDefault();
        var _this = $(this);
        if(_this.is(':disabled')) {
            return;
        }

        _this.prop('disabled', true);

        var dataOption = _this.data();

        var _pbm_blockUI = {
            message: dataOption.blockuihtml,
            css: dataOption.blockuicss,
        };

        var _pbm_container = window;
        if (dataOption.container !== undefined) {
            _pbm_container = dataOption.container;
        }
        $(_pbm_container).block(_pbm_blockUI);

        $(document).ajaxStop(function() {
            $(_pbm_container).unblock(_pbm_blockUI);
        });

        $.post(dataOption.sourcedata, function(result) {
            if (result.url === undefined || result.url === '') {
                console.log('To Using this plugin you must return next page url');
                return;
            }
            $(dataOption.holder).after(result.html);
            $(_this).data('sourcedata', result.url);

            if (result.have_more !== undefined && result.have_more === false) {
                _this.hide();
            }

            _this.prop('disabled', false);
        });
    });
    /**
     * End load view more
     *
     **/
});