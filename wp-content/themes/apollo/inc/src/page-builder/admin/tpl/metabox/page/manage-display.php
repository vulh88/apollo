<div id="page-builder-manage-display">
    <table class="wp-list-table widefat">
        <thead>
        <tr>
            <th align='left'>Entire Sections</th>
            <th class='align-center'>Show</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($template_args['display_section'] as $k => $item): ?>
            <tr class="item-block-content-sort">
                <td align='left'>
                    <label><?php echo $template_args['display_section_title'][$k] ?></label>
                </td>
                <td>
                    <input name="<?php echo $template_args['meta_key'] . "[{$k}][show]" ?>" <?php echo $item['show'] ? 'checked' : '' ?> type="checkbox" value=1 >
                    <input type='hidden' value=1 name='<?php echo $template_args['meta_key'] . "[{$k}][active]" ?>'>
                </td></tr >
        <?php endforeach; ?>
        </tbody>
    </table>
</div>