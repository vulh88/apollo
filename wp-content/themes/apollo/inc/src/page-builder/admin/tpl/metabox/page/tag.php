<?php
$tags = $template_args['tags'];
?>

<div id="page-builder-tags-data" >
    <div class="page-builder-tags-wrapper">
        <script type="text/plain" id="manage-tag-section">
                    <div class="apollo-tag-form-field">
                    <div class="options_group event-box">
                        <p class="form-field timeline_desc_field ">
                            <input type="text" name="tags[]">
                        </p>
                    </div>
                    <div
                                        data-title-confirm="<?php _e('Are you sure to remove this record ?') ?>"
                                        data-title-confirm-tagged="<?php _e('This Tag has been tagged into some instances. Are you sure to remove this record ?') ?>"
                                        class="delete-tags"><i class="fa fa-times"></i></div>
                </script>
        <div class="page-builder-tags-list">
            <?php

            if(!empty($tags)) {
                foreach ($tags as $tag):
                    ?>
                    <div class="apollo-tag-form-field">
                        <div class="options_group event-box">
                            <p class="form-field">
                                <input type="text" name="tags_old[<?php echo $tag->id ?>]" value="<?php echo $tag->value ?>" >
                            </p>
                        </div>
                        <div
                                data-tag-id="<?php echo $tag->id ?>"
                                data-title-confirm="<?php _e('Are you sure to remove this record ?') ?>"
                                data-title-confirm-tagged="<?php _e('This Tag has been tagged into some instances. Are you sure to remove this record ?') ?>"
                                class="delete-tags"><i class="fa fa-times"></i></div>
                    </div>
                <?php
                endforeach;
            }
            ?>
        </div>
    </div>

    <input type="button" class="button button-primary button-large apollo-btn-right"
           data-ride="shadow-man"
           data-template="#manage-tag-section"
           data-append-to = ".page-builder-tags-list:append"
           value="<?php _e( 'Add', 'apollo' ) ?>" />
    <br/><br/>
</div>