<div id="page-builder-manage-display-sort">
    <?php foreach ($template_args['sort_section'] as $item): ?>
        <div class="item-block-content-sort">
            <label><?php echo $template_args['sort_section_title'][$item] ?></label >
            <input name="<?php echo $template_args['meta_key'] . "[]" ?>" type = "hidden" value = "<?php echo $item ?>" >
        </div>
    <?php endforeach; ?>
</div>