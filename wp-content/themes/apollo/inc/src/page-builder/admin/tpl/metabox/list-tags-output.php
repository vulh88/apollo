<div class="options_group event-box">
    <div class="form-field apl_pbm_selected_tags_field">
        <?php echo $template_args['select_tags'] ?>
        <div class="description">
            <?php __('The instances with these selected tags will be auto-pulled into the corresponding pages within Page Builder module', 'apollo' ) ?>
        </div>
    </div>
</div>