<?php
$post_type = $template_args['post_type'];
?>
<table class="wp-list-table widefat fixed posts apl-list-custom-table">
    <thead>
    <tr>
        <th>Title</th>
        <th>Show</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($template_args['data'] as $row): ?>
        <tr>
            <td><a href="<?php echo get_edit_post_link($row['ID'], true) ?>" target="_blank"><?php echo $row['post_title'] ?></a></td>
            <td>
                <div class="options_group">
                    <?php
                    apollo_wp_checkbox(
                        array(
                            'id'            => 'post_assign'. $row['ID'],
                            'name'          => "apollo_page_builder_post_type[{$post_type}][{$row['ID']}][show]",
                            'label'         => '',
                            'desc_tip'      => 'true',
                            'description'   => "",
                            'value'         => $row['show'],
                            'cbvalue'       => 1,
                            'type'          => 'checkbox',
                        )
                    );
                    ?>
                    <input type="hidden" name="apollo_page_builder_post_type[<?php echo $post_type ?>][<?php echo $row['ID'] ?>][post_id]" value="<?php echo $row['ID'] ?>">
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>