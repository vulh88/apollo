<div class="panel apollo_options_panel">
    <div class="options_group">
        <?php wp_editor(
                $template_args['meta_value'],
                $template_args['meta_key'],
                array(  'textarea_name' => $template_args['meta_key'],
                        'editor_height' => 200)
              );
        ?>
    </div>
</div>
<div class="clear"></div>