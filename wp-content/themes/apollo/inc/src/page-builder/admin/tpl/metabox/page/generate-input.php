<div class="options_group pbm-option-group-title">
    <p class="form-field">
        <label><?php echo $template_args['field_title'] ?></label>
        <input type="text" class="short" name="<?php echo $template_args['meta_key'] ?>" value="<?php echo $template_args['meta_value'] ?>" placeholder="">
    </p>
    <div class="clearfix"></div>
</div>
<p class='apollo_options_panel'><?php echo __('Description', 'apollo') ?></p>