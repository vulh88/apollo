<?php
$wp_list_table = _get_list_table( 'WP_Posts_List_Table' );
$results = $template_args['results'];
$post_status = $template_args['post_status'];
$action_delete = $template_args['action_delete'];
$total_post = $template_args['total_post'];
$pagesize = $template_args['pagesize'];
$fields = array(
    'title'  => __( 'Title', 'apollo' ),
    'author'  => __( 'Author', 'apollo' ),
    'date'  => __( 'Date', 'apollo' ),
);
?>

<div class="wrap clear">
    <fieldset>
        <legend>
            <h2><?php _e( 'Page builder', 'apollo' ) ?><a href="post-new.php?post_type=page&module=page-builder" class="add-new-h2">Add New</a></h2>
        </legend>

        <ul class="subsubsub">
            <li class="all"><a class="<?php echo $post_status == 'all' ? 'current' : '' ?>" href="admin.php?page=apollo-page-builder-module&post_status=all">All </a> |</li>
            <li class="publish"><a class="<?php echo $post_status == 'publish' ? 'current' : '' ?>" href="admin.php?page=apollo-page-builder-module&post_status=publish">Published </a> |</li>
            <li class="trash"><a class="<?php echo $post_status == 'trash' ? 'current' : '' ?>" href="admin.php?page=apollo-page-builder-module&post_status=trash">Trash </a></li>
        </ul>

        <div class="tablenav top">
            <form method="post" action="<?php echo admin_url() ?>admin.php?page=apollo-page-builder-module&paged=<?php echo isset( $_GET['paged'] ) ? $_GET['paged'] : 1 ?>">
                <div class="alignleft actions bulkactions">
                    <label for="bulk-action-selector-top" class="screen-reader-text"><?php _e( 'Select bulk action', 'apollo' ) ?></label>
                    <select name="action" id="bulk-action-selector-top">
                        <option value="-1" selected="selected"><?php _e( 'Bulk Actions', 'apollo' ) ?></option>
                        <?php if($post_status == 'publish') : ?>
                            <option value="trash"><?php _e( 'Move to Trash', 'apollo' ) ?></option>
                        <?php endif ?>

                        <?php if($post_status == 'trash') : ?>
                            <option value="restore"><?php _e( 'Restore', 'apollo' ) ?></option>
                        <?php endif ?>
                    </select>
                    <input type="submit" class="button action" value="<?php _e( 'Apply', 'apollo' ) ?>">
                </div>

                <div class="tablenav-pages one-page">
                    <span class="displaying-num"><?php echo sprintf( __( '%d items', 'apollo' ), count($results) ) ?></span>
                </div>
                <br class="clear">

                <table class="wp-list-table widefat fixed posts apl-list-custom-table pbm-table-post">
                    <thead>
                    <tr>
                        <th style="width: 20px">
                            <input class="pbm-apl-select-all" type="checkbox">
                        </th>
                        <?php
                        $title = '';
                        foreach( $fields as $v ):
                            $title .= '<th>'.$v.'</th>';
                        endforeach;
                        echo $title;
                        ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ( $results ):
                        foreach ( $results as $k => $r ): ?>
                            <tr class="status-publish hentry <?php echo $k % 2 ? 'alternate' : '' ?> iedit author-self level-0">
                                <th>
                                    <input type="checkbox" name="post_ids[]" id="user_<?php echo $r->ID ?>" class="subscriber" value="<?php echo $r->ID ?>">
                                </th>
                                <td>
                                    <a href="<?php echo admin_url() ?>post.php?&action=edit&module=page-builder&post=<?php echo $r->ID ?>"><?php echo $r->post_title ?></a>
                                    <div class="row-actions">
                                        <span class="id"><?php _e( 'ID', 'apollo' ) ?>: <?php echo $r->post_title ?> | </span>

                                        <?php if($action_delete == 'remove'): ?>
                                            <span class="edit">
                                            <a href="<?php echo admin_url() ?>post.php?&action=edit&module=page-builder&post=<?php echo $r->ID ?>" title="<?php _e( 'Edit this item' ) ?>"><?php _e( 'Edit', 'apollo' ) ?></a>
                                        | </span>

                                            <span data-confirm="<?php _e( 'Are you sure to remove this page ?', 'apollo' ) ?>" class="trash">
                                            <a href="<?php echo admin_url() ?>admin.php?page=apollo-page-builder-module&action=trash&id=<?php echo $r->ID ?>&paged=<?php echo isset( $_GET['paged'] ) && $_GET['paged'] ? intval( $_GET['paged'] ) : 1 ?>" title="<?php echo $r->name ?>" rel="permalink"><?php _e( 'Trash', 'apollo' ) ?></a>
                                            </span>

                                        <?php else: ?>
                                            <span class="restore">
                                            <a href="<?php echo admin_url() ?>admin.php?page=apollo-page-builder-module&action=restore&post_status=trash&id=<?php echo $r->ID ?>&paged=<?php echo isset( $_GET['paged'] ) && $_GET['paged'] ? intval( $_GET['paged'] ) : 1 ?>"  title="<?php _e( 'Restore' ) ?>"><?php _e( 'Restore', 'apollo' ) ?></a>
                                        | </span>
                                            <span class="delete">
                                            <a href="<?php echo admin_url() ?>admin.php?page=apollo-page-builder-module&action=delete&id=<?php echo $r->ID ?>&paged=<?php echo isset( $_GET['paged'] ) && $_GET['paged'] ? intval( $_GET['paged'] ) : 1 ?>" title="<?php echo $r->name ?>" rel="permalink"><?php _e( 'Delete this item permanently', 'apollo' ) ?></a>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </td>

                                <td><?php echo $wp_list_table->column_author($r) ?></td>
                                <td><?php echo $r->email ?></td>
                            </tr>
                        <?php endforeach;
                    else:
                        echo '<tr class="no-items"><td class="colspanchange" colspan="7">'.__('No page found', 'apollo').'</td></tr>';
                    endif;
                    ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th style="width: 20px">
                            <input class="apl-cb-select-all" type="checkbox">
                        </th>
                        <?php echo $title; ?>
                    </tr>
                    </tfoot>
                </table>

                <div class="tablenav bottom">
                    <div class="tablenav-pages">
                        <?php
                        $pagenum = isset( $_GET['paged'] ) ? absint( $_GET['paged'] ) : 1;
                        $num_of_pages = ceil( $total_post / $pagesize );
                        $page_links = paginate_links( array(
                            'base' => add_query_arg( 'paged', '%#%' ),
                            'format' => '',
                            'prev_text' => __( '&laquo;', 'apollo' ),
                            'next_text' => __( '&raquo;', 'apollo' ),
                            'total' => $num_of_pages,
                            'current' => $pagenum
                        ) );
                        echo $page_links;
                        ?>
                    </div>
                    <br class="clear">
                </div>
            </form>
        </div>
    </fieldset>
</div>