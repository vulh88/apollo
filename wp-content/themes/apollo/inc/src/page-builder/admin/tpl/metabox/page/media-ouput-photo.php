<table class="wp-list-table widefat fixed posts apl-list-custom-table">
    <thead>
    <tr>
        <th>Thumbnail</th>
        <th>Instance Title</th>
        <th>Pulled From</th>
        <th>Show</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($template_args['data'] as $row): ?>
        <tr>
            <td>
                <?php echo wp_get_attachment_image( $row['media_id'], 'thumbnail', null, array('class' => 'pbm-metabox-photo-img-thumbnail')) ?>
            </td>
            <td>
                <a href="<?php echo get_edit_post_link($row['ID'], true) ?>" target="_blank"><?php echo $row['post_title'] ?></a>
            </td>
            <td><?php echo $row['post_type'] ?></td>
            <td>
                <div class="options_group">
                    <?php
                    apollo_wp_checkbox(
                        array(
                            'id'            => 'photo_assign_'. $row['ID'],
                            'name'          => APL_Page_Builder_Const::APL_PBM_PAGE_PHOTO_SHOW_KEY ."[{$row['media_id']}][show]",
                            'label'         => '',
                            'desc_tip'      => 'true',
                            'description'   => "",
                            'value'         => $row['show'],
                            'cbvalue'       => 1,
                            'type'          => 'checkbox',
                        )
                    );
                    ?>
                    <input type="hidden" name="<?php echo APL_Page_Builder_Const::APL_PBM_PAGE_PHOTO_SHOW_KEY ."[" . $row['media_id']. "][media_id]" ?> value="<?php echo $row['ID'] ?>">

                </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>