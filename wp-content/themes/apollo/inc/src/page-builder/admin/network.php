<?php
/***
 * A main class to handle all the things via the network admin section
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class APL_Page_Builder_Network_Admin {

    /**
     * APL_Page_Builder_Network_Admin constructor.
     */
    public function __construct()
    {
        add_filter('apl_network_adding_configs',[ $this, 'addPageBuilderModuleConfig'],10,1);
        add_filter('apl_network_after_save',[ $this, 'saveNetworkEnablePageBuilderModule'],10,1);
    }

    /**
     * Add Page Builder config ON/OFF within Network Admin
     * @param array $params
     * @return array|mixed
     */
    public function addPageBuilderModuleConfig($params = []){
        if(empty($params)){
            return [];
        }
        $siteConfigs = $params['site_configs'];
        $pageNow = $params['page_now'];
        $currentBlogID = $params['current_blog_id'];
        $siteConfigs[APL_Page_Builder::PAGE_BUILDER_ENABLED_STATUS_KEY] = [
            'label' => __('Enable Page Builder', 'apollo'),
            'value' => $pageNow == 'site-new.php' ? 0 : APL_Page_Builder::isActivatedModule($currentBlogID),
            'type'  => 'radio'
        ];
        return $siteConfigs;
    }

    public function saveNetworkEnablePageBuilderModule($blog_id) {
        $enable = 0;
        if (isset( $_POST['blog'][APL_Page_Builder::PAGE_BUILDER_ENABLED_STATUS_KEY])) {
            $enable = $_POST['blog'][APL_Page_Builder::PAGE_BUILDER_ENABLED_STATUS_KEY];
        }
        update_blog_option($blog_id, APL_Page_Builder::PAGE_BUILDER_ENABLED_STATUS_KEY, $enable);
    }
}

if ( is_network_admin() ) {
    new APL_Page_Builder_Network_Admin();
}
