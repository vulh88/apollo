<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Apollo_Page_Builder_Module_Meta_Box_Closing {

    /**
     * Output the metabox
     */
    public static function output( $post) {
        $post_id = $post->ID;
        $closing_model = new APL_Page_Builder_Closing_Model(['post_id' => $post_id]);
        APL_PBM_MetaBox_Abstract::generateMetaBoxEditor($closing_model->getAttribute('meta_key_description'), $closing_model->getDescription());
        echo '<input type="hidden" value="page-builder" name="module">';
    }

    public static function save($post_id) {
        if($_POST) {
            update_post_meta($post_id, 'page_builder_module', 1);

            $closing_model = new APL_Page_Builder_Closing_Model(['post_id' => $post_id]);
            $closing_model->saveMetaKeys($_POST);
        }
    }
}