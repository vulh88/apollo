<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Apollo_Page_Builder_Module_Meta_Box_Artist {

    public static function output( $post) {
        $post_type = Apollo_DB_Schema::_ARTIST_PT;
        $post_id = $post->ID;
        $data = APL_Page_Builder_Model::getAssociatedPagePost($post_id, $post_type, true);
        $data = $data['data'];
        $model = new APL_Page_Builder_Artist_Model(['post_id' => $post_id]);

        APL_PBM_MetaBox_Abstract::generateInput($model->getAttribute('meta_key_title'), $model->getTitle());
        APL_PBM_MetaBox_Abstract::generateMetaBoxEditor($model->getAttribute('meta_key_description'), $model->getDescription());

        $file = APL_PAGE_BUILDER_ADMIN_PATH . "/tpl/metabox/page/metabox-table-post.php";
        Apollo_App::getTemplatePartCustom($file, compact('data', 'post_type'), false);
    }

    public static function save($post_id) {
        if($_POST) {
            //save title, description
            $artist_model = new APL_Page_Builder_Artist_Model(['post_id' => $post_id]);
            $artist_model->saveMetaKeys($_POST);

            //save is_show associated post
            $data = isset($_POST['apollo_page_builder_post_type'][Apollo_DB_Schema::_ARTIST_PT])
                ? $_POST['apollo_page_builder_post_type'][Apollo_DB_Schema::_ARTIST_PT] : '';
            if (!empty($data) && $post_id) {
                $tags_page = APL_Page_Builder_Model::getTags($post_id);
                $ids_tag = array();
                foreach ($tags_page as $tag) {
                    $ids_tag[] = $tag->id;
                }
                APL_Page_Builder_Model::updateIsShowAssociatedPostsPage($ids_tag, $data);
            }
        }
    }
}