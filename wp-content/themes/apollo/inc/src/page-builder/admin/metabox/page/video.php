<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Apollo_Page_Builder_Module_Meta_Box_Video {

    public static function output($post) {
        $post_id = $post->ID;
        $data = APL_Page_Builder_Model::getAssociatedVideoAudioPost($post_id, 'video', true);
        APL_Page_Builder_Model::formatVideoPost($data);
        $video_model = new APL_Page_Builder_Video_Model(['post_id' => $post_id]);

        APL_PBM_MetaBox_Abstract::generateInput($video_model->getAttribute('meta_key_title'), $video_model->getTitle());
        APL_PBM_MetaBox_Abstract::generateMetaBoxEditor($video_model->getAttribute('meta_key_description'), $video_model->getDescription());

        $file = APL_PAGE_BUILDER_ADMIN_PATH . "/tpl/metabox/page/media-ouput-video.php";
        Apollo_App::getTemplatePartCustom($file, compact('data'), false);
    }

    public static function save( $post_id) {
        if($_POST) {
            //save title, description
            $video_model = new APL_Page_Builder_Video_Model(['post_id' => $post_id]);
            $video_model->saveMetaKeys($_POST);

            //save is_show associated post
            $input_video = isset($_POST[APL_Page_Builder_Const::APL_PBM_PAGE_VIDEO_SHOW_KEY])
                ? $_POST[APL_Page_Builder_Const::APL_PBM_PAGE_VIDEO_SHOW_KEY] : array();
            if ($post_id) {
                $tags_page = APL_Page_Builder_Model::getTags($post_id);
                $ids_tag = array();
                foreach ($tags_page as $tag) {
                    $ids_tag[] = $tag->id;
                }

                APL_Page_Builder_Model::updateIsShowAssociatedVideoAudioPage($post_id, $ids_tag, $input_video, 'video');
            }
        }
    }

}