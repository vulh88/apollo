<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * APL_PBM_MetaBox_Classified_List_Tags
 */

class APL_PBM_MetaBox_Classified_List_Tags extends APL_PBM_MetaBox_Abstract {

    /**
     * Output the metabox
     */
    public static function output( $post ) {
        wp_nonce_field( 'apollo_classified_meta_nonce', 'apollo_classified_meta_nonce' );
        $file = APL_PAGE_BUILDER_ADMIN_PATH . "/tpl/metabox/list-tags-output.php";
        Apollo_App::getTemplatePartCustom($file, array(
            'select_tags' => APL_Page_Builder_Select_Tags::renderSelectTags($post->ID, APL_Page_Builder_Const::APL_PBM_SELECTED_TAG_FIELD_KEY, 'post'),
        ), false);
    }

    /**
     * Save meta box data
     * @param $post_id
     * @param $post
     */
    public static function save( $post_id, $post ) {
        $tag_ids = isset($_POST[APL_Page_Builder_Const::APL_PBM_SELECTED_TAG_FIELD_KEY]) ? $_POST[APL_Page_Builder_Const::APL_PBM_SELECTED_TAG_FIELD_KEY] : array();
        if(is_array($tag_ids)) {
            self::saveListTagsHandler([
                'post_id'   => $post_id,
                'post'      => $post,
                'tag_ids'   => $tag_ids,
                'type'      => 'post'
            ]);
        }
    }
}