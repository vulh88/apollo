<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * APL_PBM_MetaBox_Abstract
 */

abstract class APL_PBM_MetaBox_Abstract {

    public static function generateInput($meta_key, $meta_value, $field_title = '') {
        if(empty($field_title)) {
            $field_title = __('Title', 'apollo');
        }

        $file = APL_PAGE_BUILDER_ADMIN_PATH . "/tpl/metabox/page/generate-input.php";
        Apollo_App::getTemplatePartCustom($file, compact('field_title', 'meta_key', 'meta_value'), false);
    }

    public static function generateMetaBoxEditor($meta_key, $meta_value) {
        $file = APL_PAGE_BUILDER_ADMIN_PATH . "/tpl/metabox/page/generate-editor.php";
        Apollo_App::getTemplatePartCustom($file, compact('meta_key', 'meta_value'), false);
    }

    /**
     * Save meta box data
     * @param array $data_save
     */
    public static function saveListTagsHandler( $data = array() ) {
        $post_id = $data['post_id'];
        $type = $data['type'];
        $tag_ids = $data['tag_ids'];
        switch ($type) {
            case 'post':
                APL_Page_Builder_Model::savePostTags($post_id, $tag_ids);
                break;

            case 'photo':
                APL_Page_Builder_Model::savePhotoTags($post_id, $tag_ids, $type, isset($data['ids_gallery']) ? $data['ids_gallery'] : null);
                break;

            case 'video':
                APL_Page_Builder_Model::saveVideoAudioTags($post_id, 'video', $tag_ids);
                break;

            case 'audio':
                APL_Page_Builder_Model::saveVideoAudioTags($post_id, 'audio', $tag_ids);
                break;
        }
    }
}