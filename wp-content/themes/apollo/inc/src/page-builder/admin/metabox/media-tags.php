<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Org_Address
 */

class APL_PBM_MetaBox_Media_Tags extends APL_PBM_MetaBox_Abstract {

    /**
     * Output the metabox for feature image
     */
    public static function output_feature_image($content, $post_id ) {
        $media_id = get_post_thumbnail_id($post_id);
        $tag_render = '';
        if(in_array(get_post_type($post_id), APL_Page_Builder::getPostTypeAllow())) {
            $tag_render = APL_Page_Builder_Select_Tags::renderSelectTags($post_id, APL_Page_Builder_Const::APL_PBM_SELECTED_TAGS_FEATURE_IMAGE_KEY, 'photo', $media_id);
        }
        return $content .= $tag_render;
    }

    /**
     * Save meta box for gallery photo
     * @param $post_id
     * @param $post
     */
    public static function save_gallery_tags( $post_id, $post ) {
        if(isset($post['post_ID']) && $post_id != $post['post_ID'])
            return;

        $tag_ids = isset($_POST[APL_Page_Builder_Const::APL_PBM_SELECTED_TAGS_GALLERY_KEY]) ? $_POST[APL_Page_Builder_Const::APL_PBM_SELECTED_TAGS_GALLERY_KEY] : array();
        $post_type = get_post_type($post_id);
        $ids_gallery = array();
        $attachment_ids = '';

        switch ($post_type) {
            case Apollo_DB_Schema::_ORGANIZATION_PT:
                $attachment_ids = isset($_POST['org_image_gallery']) ? $_POST['org_image_gallery'] : array();
                break;

            case Apollo_DB_Schema::_ARTIST_PT:
                $attachment_ids = isset($_POST['image_gallery']) ? $_POST['image_gallery'] : array();
                break;

            case Apollo_DB_Schema::_CLASSIFIED:
                $attachment_ids = isset($_POST['classified_image_gallery']) ? $_POST['classified_image_gallery'] : array();
                break;

        }

        if(!empty($attachment_ids)) {
            $ids_gallery = array_filter( explode( ',', $attachment_ids) );
        }

        if(isset($_POST['_thumbnail_id']) && $_POST['_thumbnail_id'] > 0) {
            $tag_ids_of_feature_image = isset($_POST[APL_Page_Builder_Const::APL_PBM_SELECTED_TAGS_FEATURE_IMAGE_KEY]) ? $_POST[APL_Page_Builder_Const::APL_PBM_SELECTED_TAGS_FEATURE_IMAGE_KEY] : null;
            if ($tag_ids_of_feature_image) {
                $thumbnail_id = $_POST['_thumbnail_id'];
                $tag_ids[$thumbnail_id] = $tag_ids_of_feature_image;
                $ids_gallery[] = $thumbnail_id;
            }
        }

        if(is_array($tag_ids)) {
            self::saveListTagsHandler([
                'post_id'       => $post_id,
                'post'          => $post,
                'tag_ids'       => $tag_ids,
                'ids_gallery'   => $ids_gallery,
                'type'          => 'photo'
            ]);
        }
    }
    /**
     * Save meta box for video
     * @param $post_id
     * @param $post
     */
    public static function save_videos_tags( $post_id, $post ) {
        if(isset($post['post_ID']) && $post_id != $post['post_ID'])
            return;
        $tag_ids = isset($_POST[APL_Page_Builder_Const::APL_PBM_SELECTED_TAGS_VIDEO_KEY]) ? $_POST[APL_Page_Builder_Const::APL_PBM_SELECTED_TAGS_VIDEO_KEY] : array();

        $video_embed = isset($_POST['video_embed']) ? $_POST['video_embed'] : array();
        foreach ($tag_ids as $k=> $video) {
            if((!isset($video_embed[$k]) || empty($video_embed[$k])) && isset($tag_ids[$k])) {
                unset($tag_ids[$k]);
            }
        }

        if(is_array($tag_ids)) {
            self::saveListTagsHandler([
                'post_id'       => $post_id,
                'post'          => $post,
                'tag_ids'       => $tag_ids,
                'type'          => 'video'
            ]);
        }
    }

    /**
     * Save meta box for audio
     * @param $post_id
     * @param $post
     */
    public static function save_audio_tags( $post_id, $post ) {
        if(isset($post['post_ID']) && $post_id != $post['post_ID'])
            return;
        $tag_ids = isset($_POST['apl_pbm_selected_tags_audio']) ? $_POST['apl_pbm_selected_tags_audio'] : array();

        $audio_embed = isset($_POST['audio_embed']) ? $_POST['audio_embed'] : array();
        foreach ($tag_ids as $k=> $audio) {
            if((!isset($audio_embed[$k]) || empty($audio_embed[$k])) && isset($tag_ids[$k])) {
                unset($tag_ids[$k]);
            }
        }
        if(is_array($tag_ids)) {
            self::saveListTagsHandler([
                'post_id'       => $post_id,
                'post'          => $post,
                'tag_ids'       => $tag_ids,
                'type'          => 'audio'
            ]);
        }
    }
}