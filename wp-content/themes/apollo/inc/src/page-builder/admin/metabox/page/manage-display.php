<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Apollo_Page_Builder_Module_Meta_Box_Manage_Display {

    /**
     * Output the metabox
     */
    public static function output( $post ) {
        $display_section = APL_Page_Builder_Page_Model::getSectionDisplay($post->ID);
        $meta_key = APL_Page_Builder_Page_Model::getMetaKeySectionDisplay();
        $display_section_title = APL_Page_Builder_Const::APL_PBM_SECTION_DISPLAY_TTTLE_DEFAULT;

        $file = APL_PAGE_BUILDER_ADMIN_PATH . "/tpl/metabox/page/manage-display.php";
        Apollo_App::getTemplatePartCustom($file, compact('display_section', 'meta_key', 'display_section_title'), false);
    }

    public static function save($post_id) {
        if($_POST) {
            APL_Page_Builder_Page_Model::saveSectionDisplay($post_id, $_POST);
        }
    }
}