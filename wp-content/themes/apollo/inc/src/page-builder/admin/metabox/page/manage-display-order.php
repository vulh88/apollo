<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Apollo_Page_Builder_Module_Meta_Box_Manage_Display_Order {

    /**
     * Output the metabox
     */
    public static function output( $post ) {
        $sort_section = APL_Page_Builder_Page_Model::getSectionOrder($post->ID);
        $meta_key = APL_Page_Builder_Page_Model::getMetaKeySectionOrder();
        $sort_section_title = APL_Page_Builder_Const::APL_PBM_SECTION_SORT_TITLE_DEFAULT;

        $file = APL_PAGE_BUILDER_ADMIN_PATH . "/tpl/metabox/page/manage-display-order.php";
        Apollo_App::getTemplatePartCustom($file, compact('sort_section', 'meta_key', 'sort_section_title'), false);
    }

    public static function save($post_id) {
        if($_POST) {
            APL_Page_Builder_Page_Model::saveSectionOrder($post_id, $_POST);
        }
    }
}