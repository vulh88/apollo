<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Apollo_Page_Builder_Module_Meta_Box_Audio {

    public static function output($post) {
        $post_id = $post->ID;
        $data = APL_Page_Builder_Model::getAssociatedVideoAudioPost($post_id, 'audio', true);
        $audio_model = new APL_Page_Builder_Audio_Model(['post_id' => $post_id]);

        APL_PBM_MetaBox_Abstract::generateInput($audio_model->getAttribute('meta_key_title'), $audio_model->getTitle());
        APL_PBM_MetaBox_Abstract::generateMetaBoxEditor($audio_model->getAttribute('meta_key_description'), $audio_model->getDescription());

        $file = APL_PAGE_BUILDER_ADMIN_PATH . "/tpl/metabox/page/media-ouput-audio.php";
        Apollo_App::getTemplatePartCustom($file, compact('data'), false);
    }

    public static function save( $post_id) {
        if($_POST) {
            //save title, description
            $audio_model = new APL_Page_Builder_Audio_Model(['post_id' => $post_id]);
            $audio_model->saveMetaKeys($_POST);

            //save is_show associated post
            $input_audio = isset($_POST[APL_Page_Builder_Const::APL_PBM_PAGE_AUDIO_SHOW_KEY])
                ? $_POST[APL_Page_Builder_Const::APL_PBM_PAGE_AUDIO_SHOW_KEY] : array();
            if ($post_id) {
                $tags_page = APL_Page_Builder_Model::getTags($post_id);
                $ids_tag = array();
                foreach ($tags_page as $tag) {
                    $ids_tag[] = $tag->id;
                }

                APL_Page_Builder_Model::updateIsShowAssociatedVideoAudioPage($post_id, $ids_tag, $input_audio, 'audio');
            }
        }
    }

}