<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Apollo_Page_Builder_Module_Meta_Box_Manage_Tags {

    public static function output( $post ) {
        wp_nonce_field( 'apollo_page_builder_meta_nonce', 'apollo_page_builder_meta_nonce');
        $tags = APL_Page_Builder_Model::getTags($post->ID);

        $file = APL_PAGE_BUILDER_ADMIN_PATH . "/tpl/metabox/page/tag.php";
        Apollo_App::getTemplatePartCustom($file, compact('tags'), false);
    }

    public static function save($post_id) {
        global $wpdb;

        if(get_post_type($post_id) != 'page') {
            return;
        }
        $tags = isset($_POST['tags']) ? $_POST['tags'] : array();
        $tags_old = isset($_POST['tags_old']) ? $_POST['tags_old'] : array();
        $tag_old_ids = array();

        $tags_table_name = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_TAGS;

        //get list tag
        $tags_page = APL_Page_Builder_Model::getTags($post_id);

        //update old tags
        foreach ($tags_old as $k => $tag) {
            $wpdb->update($tags_table_name,
                array(
                    'value' => $tag
                ),
                array(
                    'id' => $k,
                )
            );
            $tag_old_ids[] = $k;
        }

        //delete tags removed
        if (!empty($tags_page)) {
            foreach ($tags_page as $tag) {
                if (!in_array($tag->id, $tag_old_ids)) {
                    $tags_delete[] = $tag->id;
                    APL_Page_Builder_Model::deleteTag($tag->id);

                }
            }
        }

        //insert new tags
        foreach ($tags as $tag) {
            if ($tag) {
                $wpdb->insert($tags_table_name, array(
                    'post_id' => $post_id,
                    'value' => $tag
                ));
            }
        }
    }
}