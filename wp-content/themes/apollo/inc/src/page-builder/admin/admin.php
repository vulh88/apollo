<?php
/***
 * A main class to handle all the things via the admin section
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class APL_Page_Builder_Admin {

    private static $currentPostType = '';

    /**
     * APL_Page_Builder_Admin constructor.
     */
    public function __construct()
    {
        self::$currentPostType = $this->getCurrentPostType();
        require_once APL_PAGE_BUILDER_METABOX_PATH . '/abstract.php';

        add_action( 'admin_menu', array( $this, 'addMenu' ) );
        add_action( 'admin_enqueue_scripts', [ $this, 'loadAdminAssets'] );

        if( ( (isset($_GET['post_type']) && $_GET['post_type'] == 'page') || (self::$currentPostType == 'page') )
            && isset($_REQUEST['module']) && $_REQUEST['module'] == 'page-builder') {

            $this->requireMetaBoxesPage();

            add_action( 'add_meta_boxes', array( $this, 'addMetaBoxesPage' ), 30 );

            // Save Meta Boxes
            add_action( 'apollo_process_pbm_page_meta', 'Apollo_Page_Builder_Module_Meta_Box_Manage_Tags::save', 10, 1 );
            add_action( 'apollo_process_pbm_page_meta', 'Apollo_Page_Builder_Module_Meta_Box_Closing::save', 10, 2 );
            add_action( 'apollo_process_pbm_page_meta', 'Apollo_Page_Builder_Module_Meta_Box_Post::save', 10, 2 );
            add_action( 'apollo_process_pbm_page_meta', 'Apollo_Page_Builder_Module_Meta_Box_Classified::save', 10, 2 );
            add_action( 'apollo_process_pbm_page_meta', 'Apollo_Page_Builder_Module_Meta_Box_Artist::save', 10, 2 );
            add_action( 'apollo_process_pbm_page_meta', 'Apollo_Page_Builder_Module_Meta_Box_Org::save', 10, 2 );
            add_action( 'apollo_process_pbm_page_meta', 'Apollo_Page_Builder_Module_Meta_Box_Photo::save', 10, 2 );
            add_action( 'apollo_process_pbm_page_meta', 'Apollo_Page_Builder_Module_Meta_Box_Video::save', 10, 2 );
            add_action( 'apollo_process_pbm_page_meta', 'Apollo_Page_Builder_Module_Meta_Box_Audio::save', 10, 2 );
            add_action( 'apollo_process_pbm_page_meta', 'Apollo_Page_Builder_Module_Meta_Box_Manage_Display::save', 10, 2 );
            add_action( 'apollo_process_pbm_page_meta', 'Apollo_Page_Builder_Module_Meta_Box_Manage_Display_Order::save', 10, 2 );

            add_action( 'save_post', array( $this, 'saveMetaBoxesPage' ), 1, 2 );

            add_filter('redirect_post_location', function($location) {
                $location .= "&module=page-builder";
                return $location;
            });
        }
        else {
            //add metabox list tags for feature image
            require_once APL_PAGE_BUILDER_METABOX_PATH . '/media-tags.php';
            add_filter('admin_post_thumbnail_html', 'APL_PBM_MetaBox_Media_Tags::output_feature_image', 10, 2);

            add_action('add_meta_boxes', [$this, 'addMetaBoxListTagsToModules'], 30);
            add_action('save_post', [$this, 'saveMetaBoxInPost']);

            //circle image metabox
            if(wp_get_theme() == "Sonoma Theme" || defined( 'DOING_AJAX' )) {
                require_once APL_PAGE_BUILDER_INC_PATH . '/pbm-multi-post-thumbnails.php';
                $circleImageThumbnail = new pbmMultiPostThumbnails(array('label' => __('Circle Image', 'apollo'), 'id' => 'circle_image', 'post_type' => 'post'));
                add_action('add_meta_boxes', array($circleImageThumbnail, 'add_metabox'));
            }

        }

        add_action("wp_ajax_apl_pbm_admin_check_tagged", [ $this, 'checkTagged']);
        add_action("wp_ajax_nopriv_apl_pbm_admin_check_tagged", [ $this, "checkTagged"]);

        add_action('load-edit.php', array( $this, 'redirect_to_page_bulder_after_move_to_trash' ), 10, 2 );
    }

    function redirect_to_page_bulder_after_move_to_trash() {
        $screen = get_current_screen();
        if('edit-page' == $screen->id) {
            if (isset($_GET['trashed']) && intval($_GET['trashed']) >0 && isset($_GET['post_type']) && isset($_GET['post_type']) == 'page') {
                print_r($screen);
                if(get_post_meta($_GET['ids'], 'page_builder_module', true)) {
                    wp_safe_redirect('admin.php?page=apollo-page-builder-module');
                    exit;
                }
            }
        }
    }

    public function addMenu(){
        //Add a menu of admin menu
        add_menu_page( __('Page builder', 'apollo'), __('Page builder', 'apollo'), 'administrator', 'apollo-page-builder-module', array( $this, 'columns' ), ''. home_url().'/wp-content/themes/apollo/inc/admin/assets/images/aflag-icon.png', 37);

        // Add a submenu of Page builder
        add_submenu_page('apollo-page-builder-module', __( 'Pages', 'apollo' ), __( 'Pages', 'apollo' ), 'administrator', 'apollo-page-builder-module', null);
        add_submenu_page('apollo-page-builder-module', __('Add New','apollo'), __('Add New','apollo'), 'administrator', 'post-new.php?post_type=page&module=page-builder', null);
    }

    public function loadAdminAssets()
    {
        wp_enqueue_style('page_builder_module_admin_css');
        wp_enqueue_script('page_builder_module_admin_js');
        wp_enqueue_style('page_builder_module_select_tags_css');
        wp_enqueue_script('page_builder_module_select_tags_js');

        if (wp_get_theme() == "Sonoma Theme" && self::$currentPostType == 'post') {
            wp_enqueue_script('apl_pbm_admin_circle_image_js', get_template_directory_uri() . '/inc/src/page-builder/view/assets/admin/pbm-multi-post-thumbnails-admin.js', false);
        }
    }

    //generate table list page
    public function columns() {
        //delete post
        $this->deleteRestorePost();

        $pagesize = get_option( 'posts_per_page' );
        $pagenum = isset( $_GET['paged'] ) ? absint( $_GET['paged'] ) : 1;
        $offset =  ( $pagenum - 1 ) * $pagesize;
        $post_status = 'publish';
        $action_delete = 'remove';

        if(isset($_GET['post_status'])) {
            $post_status = $_GET['post_status'];
            if($post_status == 'trash') {
                $action_delete = 'delete';
            }
        }
        $args = array(
            'posts_per_page'   => get_option( 'posts_per_page' ),
            'post_type' => 'page',
            'meta_key' => 'page_builder_module',
            'offset' => $offset,
            'post_status' => $post_status
        );

        if($post_status == 'all') {
            unset($args['post_status']);
        }

        $wp_query = new WP_Query( $args );
        $results = $wp_query->posts;
        $total_post = $wp_query->found_posts;

        $file = APL_PAGE_BUILDER_ADMIN_PATH . "/tpl/admin-table-manage.php";
        Apollo_App::getTemplatePartCustom($file, compact('results', 'action_delete', 'post_status', 'total_post', 'pagesize'), false);
    }

    public function requireMetaBoxesPage() {
        require_once APL_PAGE_BUILDER_METABOX_PATH . '/page/closing.php';
        require_once APL_PAGE_BUILDER_METABOX_PATH . '/page/post.php';
        require_once APL_PAGE_BUILDER_METABOX_PATH . '/page/classified.php';
        require_once APL_PAGE_BUILDER_METABOX_PATH . '/page/artist.php';
        require_once APL_PAGE_BUILDER_METABOX_PATH . '/page/org.php';
        require_once APL_PAGE_BUILDER_METABOX_PATH . '/page/manage-display-order.php';
        require_once APL_PAGE_BUILDER_METABOX_PATH . '/page/manage-display.php';
        require_once APL_PAGE_BUILDER_METABOX_PATH . '/page/photo.php';
        require_once APL_PAGE_BUILDER_METABOX_PATH . '/page/video.php';
        require_once APL_PAGE_BUILDER_METABOX_PATH . '/page/audio.php';
        require_once APL_PAGE_BUILDER_METABOX_PATH . '/page/tags.php';
    }

    public function addMetaBoxesPage() {

        add_meta_box( 'apollo-page-builder-data', __( 'Closing', 'apollo' ),
            'Apollo_Page_Builder_Module_Meta_Box_Closing::output', 'page', 'normal');

        add_meta_box( 'apollo-page-builder-metabox-posts', __( 'What’s happening', 'apollo' ),
            'Apollo_Page_Builder_Module_Meta_Box_Post::output', 'page', 'normal', 'default');

        add_meta_box( 'apollo-page-builder-metabox-events', __( 'Classifieds', 'apollo' ),
            'Apollo_Page_Builder_Module_Meta_Box_Classified::output', 'page', 'normal', 'default');

        add_meta_box( 'apollo-page-builder-metabox-artists', __( 'Artists', 'apollo' ),
            'Apollo_Page_Builder_Module_Meta_Box_Artist::output', 'page', 'normal', 'default');

        add_meta_box( 'apollo-page-builder-metabox-organizations', __( 'Organizations', 'apollo' ),
            'Apollo_Page_Builder_Module_Meta_Box_Org::output', 'page', 'normal', 'default');


        add_meta_box( 'apollo-page-builder-manage-tags', __( 'Manage Tags', 'apollo' ),
            'Apollo_Page_Builder_Module_Meta_Box_Manage_Tags::output', 'page', 'side');

        add_meta_box( 'apollo-page-builder-manage-display', __( 'Manage the display', 'apollo' ),
            'Apollo_Page_Builder_Module_Meta_Box_Manage_Display::output', 'page', 'side');

        add_meta_box( 'apollo-page-builder-manage-display-order', __( 'Manage the display order', 'apollo' ),
            'Apollo_Page_Builder_Module_Meta_Box_Manage_Display_Order::output', 'page', 'side');

        add_meta_box( 'apollo-page-builder-metabox-photos', __( 'Photos', 'apollo' ),
            'Apollo_Page_Builder_Module_Meta_Box_Photo::output', 'page', 'normal');

        add_meta_box( 'apollo-page-builder-metabox-videos', __( 'Videos', 'apollo' ),
            'Apollo_Page_Builder_Module_Meta_Box_Video::output', 'page', 'normal');

        add_meta_box( 'apollo-page-builder-metabox-audios', __( 'Audios', 'apollo' ),
            'Apollo_Page_Builder_Module_Meta_Box_Audio::output', 'page', 'normal');

        remove_meta_box( 'slugdiv', 'page', 'normal' ); // Slug Metabox
        remove_meta_box( 'trackbacksdiv', 'page', 'normal' ); // Trackback Metabox
        remove_meta_box( 'postimagediv', 'page', 'side' ); // Featured image Metabox
        remove_meta_box( 'page-background-image', 'page', 'side' ); // Featured image Metabox
        remove_meta_box( 'commentsdiv', 'page', 'normal' ); // Comments Metabox
        remove_meta_box( 'Sonoma_page_summary', 'page', 'normal' ); // Comments Metabox
        remove_meta_box( 'commentstatusdiv', 'page', 'normal' ); // Comments Status Metabox
        remove_meta_box( 'authordiv', 'page', 'normal' ); // Author Metabox
        remove_meta_box( 'postcustom', 'page', 'normal' ); // Custom Fields Metabox
        remove_meta_box( 'postexcerpt', 'page', 'normal' ); // Excerpt Metabox
        remove_meta_box( 'revisionsdiv', 'page', 'normal' ); // Revisions Metabox
        remove_meta_box( 'trackbacksdiv', 'page', 'normal' ); // Trackback Metabox
    }

    public function saveMetaBoxesPage($post_id, $post) {
        do_action( 'apollo_process_pbm_page_meta', $post_id, $post );
    }

    public function checkTagged() {
        $tag_id = isset($_REQUEST['tag_id']) ? $_REQUEST['tag_id'] : '';
        $have = false;
        if($tag_id) {
            global $wpdb;
            $table_tag_post_name = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_POSTS_TAGS;

            $result_post_tag_data = $wpdb->get_row( " SELECT count(*) as total FROM $table_tag_post_name WHERE tag_id = $tag_id" );
            if(intval($result_post_tag_data->total) > 0) {
                $have = true;
            } else {
                $table_media_name = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_MEDIA_TAGS;
                $result_media_tag_data = $wpdb->get_row( " SELECT count(*) as total FROM $table_media_name WHERE tag_id = $tag_id" );
                if(intval($result_media_tag_data->total) > 0) {
                    $have = true;
                }
            }
        }

        wp_send_json(array(
            'status' => $have,
        ));
    }

    public function addMetaBoxListTagsToModules(){
        //add metabox list tags for post
        switch (self::$currentPostType) {
            case Apollo_DB_Schema::_ORGANIZATION_PT:
                require_once APL_PAGE_BUILDER_METABOX_PATH . '/org-list-tags.php';
                add_meta_box( 'apollo-org-list-tags', __( 'Select Tags for this organization', 'apollo' ),
                    'APL_PBM_MetaBox_Organization_List_Tags::output', self::$currentPostType, 'normal' );
                break;
            case Apollo_DB_Schema::_CLASSIFIED:
                require_once APL_PAGE_BUILDER_METABOX_PATH . '/classified-list-tags.php';
                add_meta_box( 'apollo-classified-list-tags', __( 'Select Tags for this classified', 'apollo' ),
                    'APL_PBM_MetaBox_Classified_List_Tags::output', self::$currentPostType, 'normal' );
                break;
            case Apollo_DB_Schema::_ARTIST_PT:
                require_once APL_PAGE_BUILDER_METABOX_PATH . '/artist-list-tags.php';
                add_meta_box( 'apollo-classified-list-tags', __( 'Select Tags for this Artist', 'apollo' ),
                    'APL_PBM_MetaBox_Artist_List_Tags::output', self::$currentPostType, 'normal' );
                break;
            case 'post':
                require_once APL_PAGE_BUILDER_METABOX_PATH . '/blog-list-tags.php';
                add_meta_box( 'apollo-classified-list-tags', __( 'Select Tags for this blog post', 'apollo' ),
                    'APL_PBM_MetaBox_Blog_List_Tags::output', self::$currentPostType, 'normal' );
                break;
            default:
                break;
        }
    }

    /**
     * @param $post_id
     * @return void
     */
    public function saveMetaBoxInPost($post_id){
        //save metabox list tags for photo, video, feature image
        require_once APL_PAGE_BUILDER_METABOX_PATH . '/media-tags.php';
        APL_PBM_MetaBox_Media_Tags::save_gallery_tags($post_id, $_POST);
        APL_PBM_MetaBox_Media_Tags::save_videos_tags($post_id, $_POST);
        APL_PBM_MetaBox_Media_Tags::save_audio_tags($post_id, $_POST);

        //save metabox list tags for post
        self::$currentPostType = $this->getCurrentPostType($post_id);
        switch (self::$currentPostType) {
            case Apollo_DB_Schema::_ORGANIZATION_PT:
                require_once APL_PAGE_BUILDER_METABOX_PATH . '/org-list-tags.php';
                APL_PBM_MetaBox_Organization_List_Tags::save($post_id, $_POST);
                break;
            case Apollo_DB_Schema::_CLASSIFIED:
                require_once APL_PAGE_BUILDER_METABOX_PATH . '/classified-list-tags.php';
                APL_PBM_MetaBox_Classified_List_Tags::save($post_id, $_POST);
                break;
            case Apollo_DB_Schema::_ARTIST_PT:
                require_once APL_PAGE_BUILDER_METABOX_PATH . '/artist-list-tags.php';
                APL_PBM_MetaBox_Artist_List_Tags::save($post_id, $_POST);
                break;
            case 'post':
                require_once APL_PAGE_BUILDER_METABOX_PATH . '/blog-list-tags.php';
                APL_PBM_MetaBox_Blog_List_Tags::save($post_id, $_POST);
                break;
            default:
                break;
        }
    }

    /**
     * @param string $post_id
     * @return false|null|string
     */
    public function getCurrentPostType($post_id = '') {
        if(!empty($post_id)){
            return get_post_type($post_id);
        }
        $postID = isset( $_GET['post'] ) ? $_GET['post'] : ( isset( $_POST['post_ID'] ) ? $_POST['post_ID'] : '' );
        return !empty($postID) ? get_post_type($postID) : null;
    }

    private function deleteRestorePost() {
        global $wpdb;
        $apl_query = new Apl_Query( $wpdb->prefix . 'posts' );

        if(isset($_POST['action']) && isset($_POST['post_ids'])) {
            $ids = $_POST['post_ids'];
            switch ($_POST['action']) {
                case 'trash':
                    if(is_array($ids) && count($ids) > 0) {
                        $sql = "UPDATE " . $wpdb->prefix . "posts SET post_status = 'trash' where ID in (" . implode(',', array_map('absint', $ids)) . ")";
                        $wpdb->query($sql);
                    }
                    break;
                case 'restore':
                    if(is_array($ids) && count($ids) > 0) {
                        $sql = "UPDATE " . $wpdb->prefix . "posts SET post_status = 'publish' where ID in (" . implode(',', array_map('absint', $ids)) . ")";
                        $wpdb->query($sql);
                    }
            }
        } else if ( isset($_GET['action']) && isset($_GET['id']) && $_GET['id']) {
            //Delete one item
            $id = $_GET['id'];
            switch ($_GET['action']) {
                case 'trash':
                    $apl_query->update(array('post_status' => 'trash'), array("id" => $id));
                    break;
                case 'delete':
                    APL_Page_Builder_Model::deletePost($id);
                    break;
                case 'restore':
                    $apl_query->update(array('post_status' => 'publish'), array("id" => $id));
                    break;
            }
        }
    }
}

if (is_admin()) {
    new APL_Page_Builder_Admin();
}
