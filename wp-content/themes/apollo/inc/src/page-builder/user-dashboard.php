<?php
/***
 * A main class to handle all the things (hooks + any customizations) via the frontend section
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class APL_Page_Builder_UserDashBoard {


    /**
     * APL_Page_Builder_FrontEnd constructor.
     */
    public function __construct()
    {
        require_once APL_PAGE_BUILDER_METABOX_PATH . '/abstract.php';

        add_action('init', [$this, 'frontendPageEnqueuer']);

        add_action( 'wp_enqueue_scripts', function() {
            wp_enqueue_style('page_builder_module_select_tags_css');
            wp_enqueue_script('page_builder_module_select_tags_js');
        });

        add_action('pbm_fe_save_post_tags', [$this, 'savePostTags'],  10, 2 );
        add_action('pbm_fe_save_media_tags', [$this, 'savePhotoTags'],  10, 2 );
        add_action('pbm_fe_save_video_tags', [$this, 'saveVideoTags'],  10, 2 );
        add_action('pbm_fe_save_audio_tags', [$this, 'saveAudioTags'],  10, 2 );

        add_filter('apl_pbm_fe_render_select_tags_section',[ $this, 'renderSelectTagsSection'], 10, 1);
        add_filter('apl_pbm_fe_render_video_select_tags_section',[ $this, 'renderVideoSelectTagsSection'], 10, 1);
        add_filter('apl_pbm_fe_render_audio_select_tags_section',[ $this, 'renderAudioSelectTagsSection'], 10, 1);

    }

    public function frontendPageEnqueuer() {
        wp_register_script('apl_pbm_frontend_page_script', get_template_directory_uri() . '/inc/src/page-builder/view/assets/frontend/script.js', false, '1.0.0' );
        wp_register_style('apl_pbm_frontend_page_css', get_template_directory_uri() . '/inc/src/page-builder/view/assets/frontend/style.css', false, '1.0.0' );
        //wp_localize_script( 'apl_pbm_frontend_page_script', 'apl_pbm', array( 'urlLoadMorePost' => admin_url( 'admin-ajax.php' )));

        wp_enqueue_script('jquery');
        wp_enqueue_style('apl_pbm_frontend_page_css');
        wp_enqueue_script('apl_pbm_frontend_page_script');
    }

    /**
     * Save associated post - tags
     * @param $post_id
     */
    public function savePostTags($post_id, $params = []) {
        if(!in_array(get_post_type($post_id), APL_Page_Builder::getPostTypeAllow())) {
            return;
        }

        $tag_ids = isset($params['data']['apl_pbm_fe_post_selected_tags'])
            ? $params['data']['apl_pbm_fe_post_selected_tags'] : array();

        APL_PBM_MetaBox_Abstract::saveListTagsHandler([
            'post_id'   => $post_id,
            'tag_ids'   => $tag_ids,
            'type'      => 'post'
        ]);
    }

    /**
     * Save associated video - tags
     * @param $post_id
     */
    public function saveVideoTags($post_id, $params = []) {
        if(!in_array(get_post_type($post_id), APL_Page_Builder::getPostTypeAllow())) {
            return;
        }

        $tag_ids = isset($params['data']['apl_pbm_fe_selected_video_tags'])
            ? $params['data']['apl_pbm_fe_selected_video_tags'] : array();

        if(is_array($tag_ids)) {
            APL_PBM_MetaBox_Abstract::saveListTagsHandler([
                'post_id'   => $post_id,
                'tag_ids'   => $tag_ids,
                'type'      => 'video'
            ]);
        }
    }

    /**
     * @Ticket #15263
     * Save associated video - tags
     * @param $post_id
     */
    public function saveAudioTags($post_id, $params = []) {
        if(!in_array(get_post_type($post_id), APL_Page_Builder::getPostTypeAllow())) {
            return;
        }

        $tag_ids = isset($params['data']['apl_pbm_fe_selected_audio_tags'])
            ? $params['data']['apl_pbm_fe_selected_audio_tags'] : array();

        if(is_array($tag_ids)) {
            APL_PBM_MetaBox_Abstract::saveListTagsHandler([
                'post_id'   => $post_id,
                'tag_ids'   => $tag_ids,
                'type'      => 'audio'
            ]);
        }
    }


    /**
     * Save associated photo - tags
     */
    public function savePhotoTags($post_id, $params = []) {
        if(!in_array(get_post_type($post_id), APL_Page_Builder::getPostTypeAllow())) {
            return;
        }

        $ajax_request = isset($params['ajax_request']) ? $params['ajax_request'] : false;
        $ids_gallery = isset($params['ids_gallery']) ? $params['ids_gallery'] : array();
        $arrIdAttachment = isset($params['arrIdAttachment']) ? $params['arrIdAttachment'] : array();
        $tag_ids = array();

        if (!$ajax_request) {
            $tag_ids = isset($_POST['apl_pbm_fe_selected_gallery_tags']) ? $_POST['apl_pbm_fe_selected_gallery_tags'] : array();
            $tag_ids_of_feature_image= isset($_POST['apl_pbm_fe_selected_feature_image_tags']) ? $_POST['apl_pbm_fe_selected_feature_image_tags'] : null;

        } else {
            $dataGalleryTags = isset($_GET['dataGalleryTags']) ? $_GET['dataGalleryTags'] : array();
            foreach ($dataGalleryTags as $item) {
                $tag_id = str_replace('apl_pbm_fe_selected_gallery_tags[', '', $item['name']);
                $tag_id = str_replace('][]', '', $tag_id);
                $tag_ids[$tag_id][] = $item['value'];
            }
            $tag_ids_of_feature_image= isset($_GET['dataFeatureTags']) ? $_GET['dataFeatureTags'] : null;
        }

        foreach ($arrIdAttachment as $k => $v) {
            if ($k != $v) {
                $tag_ids[$v] = $tag_ids[$k];
                unset($tag_ids[$k]);
            }
        }

        if($tag_ids_of_feature_image) {
            $thumbnail_id = get_post_thumbnail_id($post_id);
            if($thumbnail_id) {
                $tag_ids[$thumbnail_id] = $tag_ids_of_feature_image;
                $ids_gallery[] = $thumbnail_id;
            }
        }

        APL_PBM_MetaBox_Abstract::saveListTagsHandler([
            'post_id'   => $post_id,
            'tag_ids'   => $tag_ids,
            'ids_gallery'   => $ids_gallery,
            'type'      => 'photo'
        ]);
    }

    public function renderSelectTagsSection($params) {
        require_once APL_PAGE_BUILDER_INC_PATH . '/form-elements/select-tags.php';

        $formFields = array(
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk ' . (isset($params['hidden_field']) ? $params['hidden_field'] : ''),
                'container' => '',
            ), array(
                'type' => 'Title',
                'name' => 'title',
                'value' => __('Select tags', 'apollo'),
                'class' => '',

            ),
            'apollo_pbm_select_tags_group_field' => array(
                'type' => 'SelectTags',
                'name' => 'pbm_select_tags',
                'value' => $params['post_id'],
            ),
            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',

            )
        );

        return array_merge($params['form'], $formFields);
    }

    public function renderVideoSelectTagsSection($params) {
        require_once APL_PAGE_BUILDER_INC_PATH . '/form-elements/video-select-tags.php';

        $formFields = array(
            'type' => 'VideoSelectTags',
            'name' => 'video_embed[]',
            'place_holder' => __('YouTube or Vimeo URL','apollo'),
            'title' => __('Video embed','apollo'),
            'class' => 'inp-txt-event',
            'validate' => array(
                'rule' => array(
                    Apollo_Form::_FORM_REQUIRED,
                    Apollo_Form::_FORM_YOUTUBE_LINK
                )
            ),
            'data' => $params['formData']
        );
        $params['form']['video_Loop']['children'][0]['video_embed'] = $formFields;

        return $params['form'];
    }

    public function renderAudioSelectTagsSection($params) {
        require_once APL_PAGE_BUILDER_INC_PATH . '/form-elements/audio-select-tags.php';

        $formFields = array(
            'type' => 'AudioSelectTags',
            'name' => 'embed[]',
            'place_holder' => __('Audio link, iframe embed or javascript embed ','apollo'),
            'title' => __('Audio link, iframe embed or javascript embed ','apollo'),
            'class' => 'desc-video',
            'validate' => array(
                'rule' => array(
                    Apollo_Form::_FORM_EMBED,
                )
            ),
            'data' => $params['formData']
        );
        $params['form']['audio_Loop']['children'][0]['embed'] = $formFields;

        return $params['form'];
    }
}

if ( ! is_admin() || defined( 'DOING_AJAX' )) {
    new APL_Page_Builder_UserDashBoard();
}
