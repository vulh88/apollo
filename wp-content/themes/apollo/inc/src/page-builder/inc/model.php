<?php
/***
 * A main class to handle all the things via querying within the database
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class APL_Page_Builder_Model {

    /**
     * APL_Page_Builder_Model constructor.
     */
    public function __construct()
    {

    }

    public static function getTags($page_id = null) {
        global $wpdb;
        $table_name = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_TAGS;
        $post_table_name = $wpdb->prefix . 'posts';
        $sql = "select $table_name.* from $table_name join $post_table_name on $table_name.post_id = $post_table_name.id and $post_table_name.post_type = 'page' ";
        if($page_id) {
            $sql .= " where $table_name.post_id = $page_id";
        }

        $tags = $wpdb->get_results($sql);
        return $tags;
    }

    public static function getAssociatedTags($post_id = null, $type, $media_id = null) {
        global $wpdb;
        $tags = array();
        $result = array();

        switch ($type) {
            case 'post':
                $table_name = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_POSTS_TAGS;
                $tags = $wpdb->get_results("select tag_id from $table_name where post_id = $post_id");
                break;

            case 'photo':
                $table_name = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_MEDIA_TAGS;
                if($media_id) {
                    $tags = $wpdb->get_results("select tag_id from $table_name where post_id = $post_id and media_type = 'photo' and media_id = $media_id");
                }
                break;

            case 'video':
            case 'audio':
                $table_name = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_MEDIA_TAGS;
                if($media_id > -1) {
                    $tags = $wpdb->get_results("select tag_id from $table_name where post_id = $post_id and media_type = '$type' and media_id = $media_id");
                }
                break;
        }

        if(is_array($tags)) {
            foreach ($tags as $tag) {
                $result[] = $tag->tag_id;
            }
        }
        return $result;
    }

    public static function getAssociatedPagePost($post_id, $type, $is_admin = false, $limit = 0, $offset = 0) {
        global $wpdb;

        $result = array();
        $tag_table_name = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_TAGS;
        $post_tags_table_name = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_POSTS_TAGS;

        $sql = "SELECT SQL_CALC_FOUND_ROWS DISTINCT p.ID, p.post_title, post_tags.show FROM {$wpdb->posts} p ";
        $sql .= " JOIN $tag_table_name tags ON tags.post_id = $post_id";
        $sql .= " JOIN $post_tags_table_name post_tags ON post_tags.post_id = p.ID AND post_tags.tag_id = tags.id";
        $sql .= " WHERE p.post_status = 'publish' ";
        $sql .= " AND p.post_type = '" . $type . "'";

        if(!$is_admin) {
            $sql .= " AND post_tags.show = 1";
        }

        if($limit) {
            $sql .= " LIMIT $limit OFFSET " . intval($offset);
        }

        $result = array('data' => $wpdb->get_results($sql, ARRAY_A));

        if($limit) {
            $total = $wpdb->get_var("SELECT FOUND_ROWS()");
            $result['total'] = $total;
            $result['offset'] = intval($offset) + count($result['data']);
            $result['have_more'] = count($result['data']) + $offset < $total;
        }
        return $result;
    }

    public static function getAssociatedPagePostClassified($post_id, $type, $is_admin = false, $limit = 0, $offset = 0) {
        global $wpdb;

        $tag_table_name = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_TAGS;
        $post_tags_table_name = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_POSTS_TAGS;
        $classified_table_name = $wpdb->prefix . Apollo_Tables::_APL_CLASSIFIED_META;
        $classified_exp_date_key = Apollo_DB_Schema::_APL_CLASSIFIED_EXP_DATE;

        $sql = "SELECT SQL_CALC_FOUND_ROWS DISTINCT p.ID, p.post_title, post_tags.show, classified_meta.meta_value FROM {$wpdb->posts} p ";
        $sql .= " JOIN $tag_table_name tags ON tags.post_id = $post_id";
        $sql .= " JOIN $post_tags_table_name post_tags ON post_tags.post_id = p.ID AND post_tags.tag_id = tags.id";
        $sql .= " LEFT JOIN $classified_table_name classified_meta ON classified_meta.apollo_classified_id = p.ID and classified_meta.meta_key = '$classified_exp_date_key'";
        $sql .= " WHERE p.post_status = 'publish' ";
        $sql .= " AND p.post_type = '" . $type . "'";

        if(!$is_admin) {
            $sql .= " AND post_tags.show = 1";
        }

        $sql .= " ORDER BY p.post_title DESC ";

        if($limit) {
            $sql .= " LIMIT $limit OFFSET " . intval($offset);
        }

        $result = array('data' => $wpdb->get_results($sql, ARRAY_A));

        if($limit) {
            $total = $wpdb->get_var("SELECT FOUND_ROWS()");
            $result['total'] = $total;
            $result['offset'] = intval($offset) + count($result['data']);
            $result['have_more'] = count($result['data']) + $offset < $total;
        }
        return $result;
    }

    public static function getAssociatedMediaPost($page_id, $is_admin = false) {
        global $wpdb;
        $tag_table_name = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_TAGS;
        $media_tags_table_name = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_MEDIA_TAGS;

        $sql = "SELECT DISTINCT p.ID, p.post_title, p.post_type, media_tags.show, media_tags.media_id FROM {$wpdb->posts} p ";
        $sql .= " JOIN $tag_table_name tags ON tags.post_id = $page_id";
        $sql .= " JOIN $media_tags_table_name media_tags ON media_tags.post_id = p.ID AND media_tags.tag_id = tags.id AND media_tags.media_type = 'photo'";
        $sql .= " WHERE p.post_status = 'publish'";
        if(!$is_admin) {
            $sql .= " AND media_tags.show = 1 ";
        }
        $result = $wpdb->get_results($sql, ARRAY_A);

        return $result;
    }

    public static function getAssociatedVideoAudioPost($page_id, $type, $is_admin = false) {
        global $wpdb;
        $tag_table_name = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_TAGS;
        $media_tags_table_name = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_MEDIA_TAGS;

        $sql = "SELECT DISTINCT p.ID, p.post_title, p.post_type, media_tags.show, media_tags.media_id FROM {$wpdb->posts} p ";
        $sql .= " JOIN $tag_table_name tags ON tags.post_id = $page_id";
        $sql .= " JOIN $media_tags_table_name media_tags ON media_tags.post_id = p.ID AND media_tags.tag_id = tags.id AND media_tags.media_type = '$type'";
        $sql .= " WHERE p.post_status = 'publish'";
        if(!$is_admin) {
            $sql .= " AND media_tags.show = 1 ";
        }
        $result = $wpdb->get_results($sql, ARRAY_A);

        return $result;
    }

    public static function savePostTags($post_id, $tag_ids = array()) {
        global $wpdb;
        $table_name = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_POSTS_TAGS;
        $tags_associated = self::getAssociatedTags($post_id, 'post');
        foreach ($tag_ids as $tag_id) {
            if(!in_array($tag_id, $tags_associated)) {
                $wpdb->insert($table_name, array(
                    'post_id' => $post_id,
                    'tag_id' => $tag_id,
                ));
            }
        }

        $tags_delete = array_diff($tags_associated, $tag_ids);
        if(count($tags_delete) > 0) {
            $wpdb->query("delete from $table_name where post_id = $post_id and tag_id in (" . implode(',', array_map('absint', $tags_delete)) . ")");
        }
    }

    public static function savePhotoTags($post_id, $ids_media = array(), $media_type, $ids_gallery) {
        global $wpdb;
        $table_name = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_MEDIA_TAGS;

        $ids_media_insert = array();

        foreach ($ids_media as $id_media => $tag_ids) {
            $ids_media_insert[] = $id_media;
            foreach ($tag_ids as $tag_id) {
                $tags_associated = self::getAssociatedTags($post_id, 'photo', $id_media);

                if(!in_array($tag_id, $tags_associated)) {
                    $wpdb->insert($table_name, array(
                        'post_id' => $post_id,
                        'tag_id' => $tag_id,
                        'media_id' => $id_media,
                        'media_type' => 'photo',
                    ));
                }
            }
            $tags_delete = array_diff($tags_associated, $tag_ids);

            if(count($tags_delete) > 0) {
                $wpdb->query("delete from $table_name where post_id = $post_id and media_type = 'photo' and media_id = $id_media and tag_id in (" . implode(',', array_map('absint', $tags_delete)) . ")");
            }
        }

        $sql = "delete from $table_name where post_id = $post_id and media_type = 'photo'";
        if(!empty($ids_media_insert))
            $sql .= " and media_id not in (" . implode(',', array_map('absint', $ids_media_insert)) . ")";
        $wpdb->query($sql);
    }

    public static function saveVideoAudioTags($post_id, $type, $media_tag_ids = array()) {
        global $wpdb;
        $table_name = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_MEDIA_TAGS;
        $ids_media_insert = array();

        if(count($media_tag_ids) > 0) {
            foreach ($media_tag_ids as $id_media => $tag_ids) {
                $ids_media_insert[] = $id_media;
                foreach ($tag_ids as $tag_id) {
                    $tags_associated = self::getAssociatedTags($post_id, $type, $id_media);

                    if (!in_array($tag_id, $tags_associated)) {
                        $wpdb->insert($table_name, array(
                            'post_id' => $post_id,
                            'tag_id' => $tag_id,
                            'media_id' => $id_media,
                            'media_type' => $type,
                        ));
                    }
                }
                $tags_delete = array_diff($tags_associated, $tag_ids);

                if (count($tags_delete) > 0) {
                    $wpdb->query("delete from $table_name where post_id = $post_id and media_type = '$type' and media_id = $id_media and tag_id in (" . implode(',', array_map('absint', $tags_delete)) . ")");
                }
            }
        }

        $sql = "delete from $table_name where post_id = $post_id and media_type = '$type'";
        if(!empty($ids_media_insert))
            $sql .= " and media_id not in (" . implode(',', array_map('absint', $ids_media_insert)) . ")";

        $wpdb->query($sql);
    }

    public static function updateIsShowAssociatedPostsPage($ids_tag, $data) {
        global $wpdb;
        $posts_tag_table_name = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_POSTS_TAGS;

        foreach ($data as $key => $item) {
            $sql = "UPDATE $posts_tag_table_name SET `show` = " . (isset($item['show']) ? $item['show'] : 0) . " 
            WHERE post_id = $key AND tag_id in (" . implode(',', array_map('absint', $ids_tag)) . ")";
            $wpdb->query($sql);
        }
    }

    public static function updateIsShowAssociatedPhotosPage($ids_tag, $data) {
        global $wpdb;
        $media_tag_table_name = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_MEDIA_TAGS;

        foreach ($data as $key => $item) {
            $sql = "UPDATE $media_tag_table_name SET `show` = " . (isset($item['show']) ? $item['show'] : 0) . " 
            WHERE media_id = $key  AND media_type = 'photo' AND tag_id in (" . implode(',', array_map('absint', $ids_tag)) . ")";

            $wpdb->query($sql);
        }
    }

    public static function updateIsShowAssociatedVideoAudioPage($post_id, $ids_tag, $data, $type) {
        global $wpdb;
        $media_tag_table_name = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_MEDIA_TAGS;

        foreach ($data as $key => $medias) {
            foreach ($medias as $media_id => $media) {
                $sql = "UPDATE $media_tag_table_name SET `show` = " . (isset($media['show']) ? $media['show'] : 0) . " 
            WHERE post_id=$key AND media_id = $media_id AND media_type = '$type'";
                $wpdb->query($sql);
            }
        }
    }

    public static function formatVideoPost(&$data, $get_thumnail = false) {
        $thumbnail_video_default = APL_Page_Builder_Const::APL_PBM_VIDEO_DEFAULT;
        $meta_video = array();

        foreach ($data as &$item) {
            switch ($item['post_type']) {
                case Apollo_DB_Schema::_ARTIST_PT:
                    if(!isset($meta_video[$item['ID']])) {
                        $flag_meta = Apollo_App::unserialize(get_apollo_meta($item['ID'], Apollo_DB_Schema::_APL_ARTIST_DATA, TRUE));
                        $meta_video[$item['ID']] = isset( $flag_meta[Apollo_DB_Schema::_APL_ARTIST_VIDEO] ) ? $flag_meta[Apollo_DB_Schema::_APL_ARTIST_VIDEO] : '';
                    }
                    $videos = $meta_video[$item['ID']];
                    if(isset($videos[$item['media_id']])) {
                        $item['video_info'] = $videos[$item['media_id']];
                    }

                    break;
                case Apollo_DB_Schema::_ORGANIZATION_PT:
                    if(!isset($meta_video[$item['ID']])) {
                        $meta_video[$item['ID']] = Apollo_App::unserialize( get_apollo_meta( $item['ID'], Apollo_DB_Schema::_APL_ORG_VIDEO, TRUE ) );
                    }
                    $videos = $meta_video[$item['ID']];

                    if(isset($videos[$item['media_id']]['video_embed'])) {
                        $item['video_info']['embed'] = $videos[$item['media_id']]['video_embed'];
                    }

                    if(isset($videos[$item['media_id']]['video_desc'])) {
                        $item['video_info']['desc'] = esc_attr(base64_decode($videos[$item['media_id']]['video_desc']));
                    }
                    break;

            }

            if($get_thumnail) {
                try {
                    if (isset($item['video_info']['embed'])) {
                        if (Apollo_App::is_vimeo($item['video_info']['embed'])) {
                            $matches = array();
                            if (!strpos($item['video_info']['embed'], "player")) {
                                preg_match('/\/\/(www\.)?vimeo.com\/(\d+)($|\/)/', $item['video_info']['embed'], $matches);
                                if (!empty($matches))
                                    $item['video_info']['embed'] = "https://player.vimeo.com/video/" . $matches[2];
                            }
                            $item['video_info']['video_type'] = 'vimeo';
                        } else if (Apollo_App::is_youtube_url($item['video_info']['embed'])) {
                            $item['video_info']['video_type'] = 'youtube';
                        }

                        $thumb = new Apollo_Video($item['video_info']['embed']);
                        $item['video_info']['thumb'] = $thumb->getThumb();
                        $item['video_info']['code'] = $thumb->getCode();
                        if (empty($item['video_info']['thumb'])) {
                            $item['video_info']['thumb'] = $thumbnail_video_default;
                        }
                    }
                }catch(Throwable $e) {

                }
            }
        }
    }

    public static function formatAudioPost(&$data) {
        $thumbnail_video_default = APL_Page_Builder_Const::APL_PBM_AUDIO_DEFAULT;
        $meta_video = array();

        foreach ($data as &$item) {
            switch ($item['post_type']) {
                case Apollo_DB_Schema::_ARTIST_PT:
                    if(!isset($meta_video[$item['ID']])) {
                        $meta_video[$item['ID']]  = @unserialize( get_apollo_meta($item['ID'], Apollo_DB_Schema::_APL_ARTIST_AUDIO, TRUE ) );
                    }
                    $videos = $meta_video[$item['ID']];
                    if(isset($videos[$item['media_id']]['embed'])) {
                        $item['audio_info']['embed'] = str_replace('\\','',base64_decode($videos[$item['media_id']]['embed']));
                    }

                    if(isset($videos[$item['media_id']]['desc'])) {
                        $item['audio_info']['desc'] = esc_attr(base64_decode($videos[$item['media_id']]['desc']));
                    }

                    break;
                case Apollo_DB_Schema::_ORGANIZATION_PT:
                    if(!isset($meta_video[$item['ID']])) {
                        $meta_video[$item['ID']] = Apollo_App::unserialize( get_apollo_meta( $item['ID'], Apollo_DB_Schema::_APL_ORG_AUDIO, TRUE ) );
                    }
                    $videos = $meta_video[$item['ID']];

                    if(isset($videos[$item['media_id']]['embed'])) {
                        $item['audio_info']['embed'] = str_replace('\\','',base64_decode($videos[$item['media_id']]['embed']));
                    }

                    if(isset($videos[$item['media_id']]['desc'])) {
                        $item['audio_info']['desc'] = esc_attr(base64_decode($videos[$item['media_id']]['desc']));
                    }
                    break;
            }

            $item['audio_info']['type'] = self::checkAudioLinkType($item['audio_info']['embed']);
            $item['audio_info']['thumb'] = $thumbnail_video_default;
        }
    }

    public static function checkAudioLinkType($str){
        $embebCheck = new Embed();
        $isFileLink = $embebCheck->checkMimeType($str);

        if($isFileLink) {
            $ext = strtolower(pathinfo($str, PATHINFO_EXTENSION));
            if($ext == 'mp4')
                return 'embed';
            return 'file';
        }
        $isIframe = $embebCheck->checkIframeObject($str);
        if($isIframe)
            return 'iframe';
        $isEmbed = $embebCheck->checkEmbed($str);
        if($isEmbed)
            return 'embed';
        $isAudio = $embebCheck->checkAudio($str);
        if($isAudio)
            return 'audio';
        $isObject = $embebCheck->checkObject($str);
        if($isObject)
            return 'object';
    }

    public static function deletePost($post_id) {
        global $wpdb;
        $tags = self::getTags($post_id);
        foreach ($tags as $tag) {
            self::deleteTag($tag->id);
        }
        wp_delete_post($post_id);
    }

    public static function deleteTag($tag_id) {
        global $wpdb;
        $table_tag_name = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_TAGS;
        $table_tag_post_name = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_POSTS_TAGS;
        $table_media_name = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_MEDIA_TAGS;

        $wpdb->query("delete from $table_tag_post_name where tag_id = $tag_id");
        $wpdb->query("delete from $table_media_name where tag_id = $tag_id");
        $wpdb->query("delete from $table_tag_name where id = $tag_id");
    }
}

new APL_Page_Builder_Model();