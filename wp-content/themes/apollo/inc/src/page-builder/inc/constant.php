<?php
/***
 * A class to define list of constants and defined values uses inside Page Builder Module
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

define( 'APL_PAGE_BUILDER_BASE_PATH', dirname(__DIR__));
define( 'APL_PAGE_BUILDER_INC_PATH', __DIR__);
define( 'APL_PAGE_BUILDER_ADMIN_PATH', APL_PAGE_BUILDER_BASE_PATH. '/admin');
define( 'APL_PAGE_BUILDER_METABOX_PATH', APL_PAGE_BUILDER_ADMIN_PATH. '/metabox');
define( 'APL_PAGE_BUILDER_URL', get_template_directory_uri() . '/inc/src/page-builder');

class APL_Page_Builder_Const {
    const APL_PBM_SELECTED_TAG_FIELD_KEY = 'apl_pbm_selected_tags';
    const APL_PBM_SELECTED_TAGS_GALLERY_KEY = 'apl_pbm_selected_tags_gallery';
    const APL_PBM_SELECTED_TAGS_VIDEO_KEY = 'apl_pbm_selected_tags_video';
    const APL_PBM_SELECTED_TAGS_FEATURE_IMAGE_KEY = 'apl_pbm_selected_tags_feature_image';
    const APL_PBM_PAGE_PHOTO_SHOW_KEY = 'apl_pbm_page_photo_show';
    const APL_PBM_PAGE_VIDEO_SHOW_KEY = 'apl_pbm_page_video_show';
    const APL_PBM_PAGE_AUDIO_SHOW_KEY = 'apl_pbm_page_audio_show';

    const APL_PBM_FE_POST_SELECTED_TAG_FIELD_KEY = 'apl_pbm_fe_post_selected_tags';
    const APL_PBM_FE_SELECTED_TAGS_FEATURE_IMAGE_KEY = 'apl_pbm_fe_selected_feature_image_tags';
    const APL_PBM_FE_SELECTED_TAGS_GALLERY_KEY = 'apl_pbm_fe_selected_gallery_tags';
    const APL_PBM_FE_SELECTED_TAGS_VIDEO_KEY = 'apl_pbm_fe_selected_video_tags';

    const APL_PBM_NUMBER_POST_BLOG = 3;
    const APL_PBM_NUMBER_POST_ARTIST = 15;
    const APL_PBM_NUMBER_POST_ORG = 10;
    const APL_PBM_NUMBER_POST_CLASSIFIED = 21;

    const APL_PBM_IMAGE_DEFAULT = APL_PAGE_BUILDER_URL . "/view/assets/images/somona-missing-image-placeholder.jpg";
    const APL_PBM_VIDEO_DEFAULT = APL_PAGE_BUILDER_URL . "/view/assets/images/video.png";
    const APL_PBM_AUDIO_DEFAULT = APL_PAGE_BUILDER_URL . "/view/assets/images/audio.png";
    const APL_PBM_META_KEY_SECTION_DISPLAY = 'apl_pbm_section_display';
    const APL_PBM_META_KEY_SECTION_SORT = 'apl_pbm_section_sort';

    const APL_PBM_SECTION_SORT_DEFAULT = array('post', 'photo', 'video', 'audio', 'classified', 'artist', 'organization');
    const APL_PBM_SECTION_SORT_TITLE_DEFAULT = array(
        'post' => "What's Happening",
        'photo' => "Photos",
        'video' => "Videos",
        'audio' => "Audios",
        'classified' => "Events",
        'artist' => "Creatives",
        'organization' => "Organizations",
    );

    const APL_PBM_SECTION_DISPLAY_DEFAULT = array(
        'introduction' => array('show' => 1, 'active' => 1),
        'post' => array('show' => 1, 'active' => 1),
        'classified' => array('show' => 1, 'active' => 1),
        'artist' => array('show' => 1, 'active' => 1),
        'organization' => array('show' => 1, 'active' => 1),
        'photo' => array('show' => 1, 'active' => 1),
        'video' => array('show' => 1, 'active' => 1),
        'audio' => array('show' => 1, 'active' => 1),
        'closing' => array('show' => 1, 'active' => 1),
    );

    const APL_PBM_SECTION_DISPLAY_TTTLE_DEFAULT = array(
        'introduction' => "Introduction",
        'post' => "What's Happening",
        'classified' => "Classifieds",
        'artist' => "Creatives",
        'organization' => "Organizations",
        'photo' => "Photos",
        'video' => "Videos",
        'audio' => "Audios",
        'closing' => "Closing"
    );
}

class Page_Builder_DB_Schema {
    const _APOLLO_PAGE_BUILDER_DATA = '_apollo_page_builder_data';
    const _APOLLO_PAGE_BUILDER_POSTMETA = '_apollo_page_builder_postmeta';
}