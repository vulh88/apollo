<?php
/**
 * Created by PhpStorm.
 * User: HieuLuong
 * Date: 2018/03/02
 */

class SelectTags extends Apollo_Form_Element{

    public function render(){
        parent::render();
        $id = esc_attr($this->eValue);

        $tags_html = '';
        if(has_filter('apl_pbm_admin_render_select_tags')) {
            $tags_html = apply_filters('apl_pbm_admin_render_select_tags', [
                'post_id' => $id,
                'tags_name' => 'apl_pbm_fe_post_selected_tags',
                'type' => 'post',
                'media_id' => '',
            ]);
        }
        
        echo $tags_html;
    }
}