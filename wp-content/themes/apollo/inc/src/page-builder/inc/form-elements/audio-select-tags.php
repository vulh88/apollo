<?php

class AudioSelectTags extends TextAreaAudio{

    public function render(){
        parent::render();
        $value = esc_attr($this->eValue);

        $arrData = $this->arrData;
        $media_id = '';
        if(!empty($arrData['embed']) && count($arrData['embed']) > 0) {
            foreach ($arrData['embed'] as $k => $embed) {
                if (htmlentities($embed) == $value) {
                    $media_id = $k;
                    break;
                }
            }
        }

        $tags_html = '';
        if(has_filter('apl_pbm_admin_render_select_tags')) {
            $tags_html = apply_filters('apl_pbm_admin_render_select_tags', [
                'post_id' => $arrData['ID'],
                'tags_name' => "apl_pbm_fe_selected_audio_tags[$media_id]",
                'type' => 'audio',
                'media_id' => $media_id,
                'wrap_html' => array(
                    'first' => "<div class='pbm-video-select-tags'>",
                    'last' => '</div>'
                )
            ]);
        }
        echo $tags_html;
    }
}