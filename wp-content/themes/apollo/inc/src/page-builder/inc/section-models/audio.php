<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class APL_Page_Builder_Audio_Model extends APL_Page_Builder_Section_Abstract {

    protected $post_type = 'audio';

    public function __construct($args = array())
    {
        parent::__construct($args);
        $this->title_default = __('Audios');
    }

    public function getFullData() {
        $data = $this->getData();

        if($data['display']) {
            $data['list'] = APL_Page_Builder_Model::getAssociatedVideoAudioPost($this->post_id, 'audio', false);
            APL_Page_Builder_Model::formatAudioPost($data['list']);
        }

        return $data;
    }
}