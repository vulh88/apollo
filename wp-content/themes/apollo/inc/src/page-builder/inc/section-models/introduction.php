<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class APL_Page_Builder_Introduction_Model extends APL_Page_Builder_Section_Abstract {

    protected $post_type = 'introduction';

    public function __construct($args = array())
    {
        parent::__construct($args);
    }

    public function getFullData() {
        $data = array(
            'order' => 0,
            'display' => $this->getDisplay(),
            'block_name' => $this->post_type,
            'title' => '',
            'description' => '',
            'list' => array()
        );

        if($data['display']) {
            $data['order'] = -1;
            $data['title'] = get_the_title($this->post_id);
            $data['description'] = get_post_field('post_content', $this->post_id);
        }

        return $data;
    }

}