<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class APL_Page_Builder_Post_Model extends APL_Page_Builder_Section_Abstract {

    protected $post_type = 'post';

    public function __construct($args = array())
    {
        parent::__construct($args);
        $this->title_default = __('What’s Happening');
    }


    public function getFullData($offset = 0) {
        $data = $this->getData();

        if($data['display']) {
            $data['list'] = APL_Page_Builder_Model::getAssociatedPagePost(
                $this->post_id,
                $this->post_type,
                false,
                APL_Page_Builder_Const::APL_PBM_NUMBER_POST_BLOG,
                $offset
            );

            $this->formatDataPosts($data['list']['data']);

            if(wp_get_theme() == "Sonoma Theme") {
                $this->formatDataCircleImagePostBlog($data['list']['data']);
            }

        }

        return $data;
    }

    private function formatDataCircleImagePostBlog(&$posts) {
        $feature_image_url_default = APL_Page_Builder_Const::APL_PBM_IMAGE_DEFAULT;
        foreach ($posts as &$item) {
            $circle_image_id = get_post_meta($item['ID'], 'pbm_post_circle_image_thumbnail_id', true);
            if($circle_image_id) {
                $item['circle_image_url'] = wp_get_attachment_image_src($circle_image_id, 'thumbnail')[0];
            } else {
                $item['circle_image_url'] = $feature_image_url_default;
            }
        }
    }
}