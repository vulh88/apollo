<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class APL_Page_Builder_Video_Model extends APL_Page_Builder_Section_Abstract {

    protected $post_type = 'video';

    public function __construct($args = array())
    {
        parent::__construct($args);
        $this->title_default = __('Videos');
    }

    public function getFullData() {
        $data = $this->getData();

        if($data['display']) {
            $data['list'] = APL_Page_Builder_Model::getAssociatedVideoAudioPost($this->post_id, 'video', false);
            require_once APOLLO_INCLUDES_DIR . '/class-apollo-video.php';
            APL_Page_Builder_Model::formatVideoPost($data['list'], true);
        }

        return $data;
    }
}