<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class APL_Page_Builder_Closing_Model extends APL_Page_Builder_Section_Abstract {

    protected $post_type = 'closing';

    public function __construct($args = array())
    {
        parent::__construct($args);
    }

    public function getFullData() {
        $data = $this->getData();
        $data['order'] = 1000;
        return $data;
    }

}