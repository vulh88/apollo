<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

interface APL_Page_Builder_Section_Interface {
    public function getTitle();
    public function getDescription();
    public function saveMetaKeys($post);
}