<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class APL_Page_Builder_Photo_Model extends APL_Page_Builder_Section_Abstract {

    protected $post_type = 'photo';

    public function __construct($args = array())
    {
        parent::__construct($args);
        $this->title_default = __('Photos');
    }

    public function getFullData() {
        $data = $this->getData();

        if($data['display']) {
            $data['list'] = APL_Page_Builder_Model::getAssociatedMediaPost($this->post_id);
        }

        return $data;
    }
}