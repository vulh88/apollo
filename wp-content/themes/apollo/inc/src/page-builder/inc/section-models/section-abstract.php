<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

abstract class APL_Page_Builder_Section_Abstract implements APL_Page_Builder_Section_Interface {

    protected $post_type;
    protected $post_id;
    protected $title;
    protected $description;
    protected $meta_key_title;
    protected $meta_key_description;
    protected $title_default;

    public function __construct($args)
    {
        if(isset($args['post_id'])) {
            $this->post_id = $args['post_id'];
        }

        $this->meta_key_title = 'apl_' . $this->post_type . '_meta_key_title';
        $this->meta_key_description = 'apl_' . $this->post_type . '_meta_key_description';
    }

    public function saveMetaKeys($post) {
        if(isset($post[$this->meta_key_title])) {
            update_post_meta($this->post_id, $this->meta_key_title, $post[$this->meta_key_title]);
        }

        if(isset($post[$this->meta_key_description])) {
            update_post_meta($this->post_id, $this->meta_key_description, $post[$this->meta_key_description]);
        }
    }

    public function getAttribute($attribute) {
        return $this->{$attribute};
    }

    public function getTitle() {
        $this->title = get_post_meta($this->post_id, $this->meta_key_title, true);
        return $this->title;
    }

    public function getDescription() {
        $this->description = get_post_meta($this->post_id, $this->meta_key_description, true);
        return $this->description;
    }

    public function getDisplay() {
        $section_display = APL_Page_Builder_Page_Model::getSectionDisplay($this->post_id);
        return $section_display[$this->post_type]['show'];
    }

    public function getOrder() {
        $section_order = APL_Page_Builder_Page_Model::getSectionOrder($this->post_id);
        foreach ($section_order as $k => $order) {
            if($order == $this->post_type) {
                return $k;
            }
        }

        return -1;
    }

    public function getData() {
        $data = array(
            'order' => 0,
            'display' => false,
            'block_name' => $this->post_type,
            'title' => '',
            'description' => '',
            'list' => array()
        );

        $data['display'] = $this->getDisplay();
        if($data['display']) {
            $data['order'] = $this->getOrder();
            $data['title'] = $this->getTitle();
            $data['description'] = $this->getDescription();

            if(empty($data['title'])) {
                $data['title'] = $this->title_default;
            }
        }

        return $data;
    }

    protected function formatDataPosts(&$posts) {
        $feature_image_url_default = APL_Page_Builder_Const::APL_PBM_IMAGE_DEFAULT;

        foreach ($posts as &$post) {
            if(has_post_thumbnail($post['ID'])){
                $image_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post['ID'] ), 'single-post-thumbnail' );
                $post['feature_image_url'] = $image_src[0];
            } else {
                $post['feature_image_url'] = $feature_image_url_default;
            }
        }
    }
}