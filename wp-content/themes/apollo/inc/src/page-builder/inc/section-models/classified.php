<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class APL_Page_Builder_Classified_Model extends APL_Page_Builder_Section_Abstract {

    protected $post_type = Apollo_DB_Schema::_CLASSIFIED;

    public function __construct($args = array())
    {
        parent::__construct($args);
        $this->title_default = __('Classifieds');
    }

    public function getFullData($offset = 0) {
        $data = $this->getData();

        if($data['display']) {
            $data['list'] = APL_Page_Builder_Model::getAssociatedPagePostClassified(
                $this->post_id,
                $this->post_type,
                false,
                APL_Page_Builder_Const::APL_PBM_NUMBER_POST_CLASSIFIED,
                $offset
            );
            $this->formatDataPosts($data['list']['data']);
            $this->formatDataPostClassified($data['list']['data']);
        }

        return $data;
    }

    private function formatDataPostClassified(&$posts) {
        $metaClassifiedsPosts = array();
        foreach ($posts as &$item) {
            if(empty($metaClassifiedsPosts[$item['ID']])) {
                $meta = Apollo_App::unserialize(get_apollo_meta($item['ID'], Apollo_DB_Schema::_APL_CLASSIFIED_DATA, TRUE));
                $metaClassifiedsPosts[$item['ID']] = $meta;
            } else {
                $meta = $metaClassifiedsPosts[$item['ID']];
            }
            $item['exp_date'] = isset($meta[Apollo_DB_Schema::_CLASSIFIED_EXP_DATE]) ? $meta[Apollo_DB_Schema::_CLASSIFIED_EXP_DATE] : '';
            $item['exp_date_origin'] = $item['exp_date'];

            if($item['exp_date']) {
                $item['exp_date'] = Apollo_App::apl_date('M j, Y', strtotime($item['exp_date']));
            }
        }
    }


}