<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class APL_Page_Builder_Organization_Model extends APL_Page_Builder_Section_Abstract {

    protected $post_type = Apollo_DB_Schema::_ORGANIZATION_PT;

    public function __construct($args = array())
    {
        parent::__construct($args);
        $this->title_default = __('Organizations & Enterprises');
    }

    public function getFullData($offset = 0) {
        $data = $this->getData();

        if($data['display']) {
            $data['list'] = APL_Page_Builder_Model::getAssociatedPagePost(
                $this->post_id,
                $this->post_type,
                false,
                APL_Page_Builder_Const::APL_PBM_NUMBER_POST_ORG,
                $offset
            );
            $this->formatDataPosts($data['list']['data']);
        }

        return $data;
    }

}