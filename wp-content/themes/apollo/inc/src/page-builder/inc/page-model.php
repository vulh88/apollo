<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class APL_Page_Builder_Page_Model {

    public static function getMetaKeySectionDisplay() {
        return APL_Page_Builder_Const::APL_PBM_META_KEY_SECTION_DISPLAY;
    }

    public static function getSectionDisplay($post_id) {
        $meta_key = self::getMetaKeySectionDisplay();
        $value = Apollo_App::unserialize(get_post_meta($post_id, $meta_key, true));
        if(empty($value)) {
            $value = APL_Page_Builder_Const::APL_PBM_SECTION_DISPLAY_DEFAULT;
        }
        if(!isset($value['audio'])) {
            $value['audio'] = array('show' => 1, 'active' => 1);
        }

        return $value;
    }

    public static function saveSectionDisplay($post_id, $post) {
        $meta_key = self::getMetaKeySectionDisplay();
        $values = isset($post[$meta_key]) ? $_POST[$meta_key] : '';
        $display_section_default = APL_Page_Builder_Const::APL_PBM_SECTION_DISPLAY_DEFAULT;

        foreach ($display_section_default as $k => $section) {
            if(!isset($values[$k]['show'])) {
                $values[$k] = array('show' => 0);
            }
        }

        update_post_meta( $post_id, $meta_key, serialize(Apollo_App::clean_array_data($values)));
    }


    public static function getMetaKeySectionOrder() {
        return APL_Page_Builder_Const::APL_PBM_META_KEY_SECTION_SORT;
    }

    public static function getSectionOrder($post_id) {
        $meta_key = self::getMetaKeySectionOrder();
        $value = Apollo_App::unserialize(get_post_meta($post_id, $meta_key, true));
        if(empty($value)) {
            $value = APL_Page_Builder_Const::APL_PBM_SECTION_SORT_DEFAULT;
        }

        $check_exist_video = false;
        foreach ($value as $item) {
            if($item == 'audio') {
                $check_exist_video = true;
                break;
            }
        }
        if(!$check_exist_video) {
            $value[] = 'audio';
        }

        return $value;
    }

    public static function saveSectionOrder($post_id, $post) {
        $meta_key = self::getMetaKeySectionOrder();
        $values = isset($post[$meta_key]) ? $_POST[$meta_key] : '';
        update_post_meta( $post_id, $meta_key, serialize(Apollo_App::clean_array_data($values)));
    }
}