<?php
/***
 * A main class to handle all the things via querying within the database
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class APL_Page_Builder_Select_Tags {

    /**
     * APL_Page_Builder_Model constructor.
     */
    public function __construct()
    {

    }

    public static function renderSelectTags($post_id, $tags_name, $type, $media_id = null) {
        $tags = APL_Page_Builder_Model::getTags();
        $tags_associated = array();

        if($post_id) {
            switch ($type) {
                case 'post':
                    $tags_associated = APL_Page_Builder_Model::getAssociatedTags($post_id, $type);
                    break;

                case 'photo':
                case 'video':
                case 'audio':
                    $tags_associated = APL_Page_Builder_Model::getAssociatedTags($post_id, $type, $media_id);
                    break;
            }
        }

        return Apollo_App::renderHTML(
            APL_PAGE_BUILDER_BASE_PATH . '/view/tpl/tags-select.php',
            array(
                'tags' => $tags,
                'tags_associated' => $tags_associated,
                'tags_name'    => $tags_name
            )
        );
    }
}

new APL_Page_Builder_Select_Tags();