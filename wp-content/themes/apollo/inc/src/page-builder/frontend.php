<?php
/***
 * A main class to handle all the things (hooks + any customizations) via the frontend section
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class APL_Page_Builder_FrontEnd {


    /**
     * APL_Page_Builder_FrontEnd constructor.
     */
    public function __construct()
    {
        add_filter('apl_pbm_get_data_posts_associated',[ $this, 'getPostsAssociatedPage'],10,1);
        add_action( 'init', [ $this, 'frontendPageEnqueuer'] );
        add_action("wp_ajax_apl_pbm_load_more_post", [ $this, 'loadMorePost']);
        add_action("wp_ajax_nopriv_apl_pbm_load_more_post", [ $this, "loadMorePost"]);
    }

    public function getPostsAssociatedPage($params = []){
        if(empty($params)){
            return [];
        }

        $post_id = $params['common']['post_id'];

        $data = array();

        $introduction_model = new APL_Page_Builder_Introduction_Model(['post_id' => $post_id]);
        $data['introduction'] = $introduction_model->getFullData();

        $post_model = new APL_Page_Builder_Post_Model(['post_id' => $post_id]);
        $data['post'] = $post_model->getFullData();

        $artist_model = new APL_Page_Builder_Artist_Model(['post_id' => $post_id]);
        $data['artists'] = $artist_model->getFullData();

        $classified_model = new APL_Page_Builder_Classified_Model(['post_id' => $post_id]);
        $data['classifieds'] = $classified_model->getFullData();

        $org_model = new APL_Page_Builder_Organization_Model(['post_id' => $post_id]);
        $data['orgs'] = $org_model->getFullData();

        $closing_model = new APL_Page_Builder_Closing_Model(['post_id' => $post_id]);
        $data['closing'] = $closing_model->getFullData();

        $video_model = new APL_Page_Builder_Video_Model(['post_id' => $post_id]);
        $data['videos'] = $video_model->getFullData();

        $photo_model = new APL_Page_Builder_Photo_Model(['post_id' => $post_id]);
        $data['photos'] = $photo_model->getFullData();

        $audio_model = new APL_Page_Builder_Audio_Model(['post_id' => $post_id]);
        $data['audios'] = $audio_model->getFullData();

        foreach ($data as $k => $item) {
            if(!$item['display'])
                unset($data[$k]);
        }

        usort($data, function($a, $b) {
            return $a['order'] > $b['order'];
        });

        $params['posts'] = $data;

        return $params;
    }


    public function loadMorePost() {

        $offset = isset($_REQUEST['offset']) ?intval($_REQUEST['offset']) : 0;
        $post_id = isset($_REQUEST['post_id']) ? intval($_REQUEST['post_id']) : null;
        $post_type = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';
        if ($post_id && $post_type) {
            $model = null;
            switch ($post_type) {
                case 'post':
                    $model = new APL_Page_Builder_Post_Model(['post_id' => $post_id]);
                    break;
                case 'organization':
                    $model = new APL_Page_Builder_Organization_Model(['post_id' => $post_id]);
                    break;
                case 'artist':
                    $model = new APL_Page_Builder_Artist_Model(['post_id' => $post_id]);
                    break;
                case 'classified':
                    $model = new APL_Page_Builder_Classified_Model(['post_id' => $post_id]);
                    break;
            }

            if($model) {
                $posts = $model->getFullData($offset);
                $file = APL_PAGE_BUILDER_BASE_PATH . '/view/tpl/sonoma-frontend-page/ajax-get-more-post.php';
                $html = Apollo_App::getTemplatePartCustom($file, array(
                    'posts' => $posts['list'],
                    'post_type' => $post_type
                ), true);

                wp_send_json(array(
                    'url' => admin_url("admin-ajax.php?action=apl_pbm_load_more_post&type=$post_type&post_id=$post_id&offset=" . $posts['list']['offset']),
                    'html' => $html,
                    'total' => $posts['list']['total'],
                    'have_more' => $posts['list']['have_more'],
                ));
            }
            wp_send_json(array(
                'url' => '',
                'html' => '',
                'total' => 0,
                'have_more' => false,
            ));
        }

        wp_send_json(array(
            'url' => 'error',
            'html' => "",
            'total' => 0,
            'have_more' => false,
        ));
    }

    public function frontendPageEnqueuer() {
        $src = get_template_directory_uri() . '/inc/src/page-builder/view/assets/frontend/';

        wp_register_script('apl_pbm_frontend_page_script', $src . 'script.js', false, '1.0.0' );
        wp_register_style('apl_pbm_frontend_page_css', $src . 'style.css', false, '1.0.0' );
        wp_localize_script( 'apl_pbm_frontend_page_script', 'apl_pbm', array( 'urlLoadMorePost' => admin_url( 'admin-ajax.php' )));

        wp_enqueue_script('jquery');
        wp_enqueue_style('apl_pbm_frontend_page_css');
        wp_enqueue_script('apl_pbm_frontend_page_script');

        wp_enqueue_script('apl_pbm_frontend_page_colorbox_js', $src . 'colorbox/jquery.colorbox-min.js', false);
        wp_enqueue_style('apl_pbm_frontend_page_colorbox_css', $src . 'colorbox/colorbox.css', false);

    }
}

if ( ! is_admin() || defined( 'DOING_AJAX' )) {
    new APL_Page_Builder_FrontEnd();
}
