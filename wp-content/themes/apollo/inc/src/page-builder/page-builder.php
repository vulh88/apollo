<?php
/***
 * A main class to initialize everything via the Page Builder module
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



class APL_Page_Builder {

    const PAGE_BUILDER_ENABLED_STATUS_KEY = '_apollo_enable_page_builder';
    const _APL_PAGE_BUILDER_TABLE_TAGS = 'apollo_page_builder_tags';
    const _APL_PAGE_BUILDER_TABLE_POSTS_TAGS = 'apollo_page_builder_posts_tags';
    const _APL_PAGE_BUILDER_TABLE_MEDIA_TAGS = 'apollo_page_builder_media_tags';

    /***
     * @var array - path to the dependency classes
     */
    private static $dependencies = [
        'const' => __DIR__ . '/inc/constant.php',
        'model' => __DIR__ . '/inc/model.php',
        'select-tags' => __DIR__ . '/inc/select-tags.php',
        'admin' => __DIR__ . '/admin/admin.php',
        'network' => __DIR__ . '/admin/network.php',
        'frontend' =>__DIR__ . '/frontend.php',
        'user-dashboard' =>__DIR__ . '/user-dashboard.php',
        'section-interface' =>__DIR__ . '/inc/section-models/section-interface.php',
        'section-abstract' =>__DIR__ . '/inc/section-models/section-abstract.php',
        'page-model' =>__DIR__ . '/inc/page-model.php',
        'closing-model' =>__DIR__ . '/inc/section-models/closing.php',
        'post-model' =>__DIR__ . '/inc/section-models/post.php',
        'artist-model' =>__DIR__ . '/inc/section-models/artist.php',
        'classified-model' =>__DIR__ . '/inc/section-models/classified.php',
        'organization-model' =>__DIR__ . '/inc/section-models/organization.php',
        'photo-model' =>__DIR__ . '/inc/section-models/photo.php',
        'video-model' =>__DIR__ . '/inc/section-models/video.php',
        'audio-model' =>__DIR__ . '/inc/section-models/audio.php',
        'introduction-model' =>__DIR__ . '/inc/section-models/introduction.php',
    ];

    private static $moduleActivated = false;

    /**
     * APL_Page_Builder constructor.
     */
    public function __construct()
    {
        self::$moduleActivated = self::isActivatedModule();

        foreach (self::$dependencies as $key => $dcp){
            if ( $key == 'network' ){ // always load network to be able to turn ON/OFF the module
                require_once "$dcp";
            } elseif ( self::$moduleActivated ) { // the remaining dependencies ones only are loaded once module is activated.
                require_once "$dcp";
            }
        }

        if(self::$moduleActivated) {
            $this->checkTables();

            // Add any hooks in here on initializing of WP
            add_action( 'init', [$this, 'initializeGeneral'] );
        }

        //add filter
        add_filter('apl_pbm_module_active',[ $this, 'getActiveModule'], 10, 1);
        add_filter('apl_pbm_admin_render_select_tags',[ $this, 'renderSelectTags'], 10, 1);

    }

    public function initializeGeneral() {
        wp_register_style('page_builder_module_admin_css', get_template_directory_uri() . '/inc/src/page-builder/view/assets/admin/style.css', false, '1.0.0' );
        wp_register_script('page_builder_module_admin_js', get_template_directory_uri() . '/inc/src/page-builder/view/assets/admin/script.js', false, '1.0.0' );
        wp_register_style('page_builder_module_select_tags_css', get_template_directory_uri() . '/inc/src/page-builder/view/assets/select-tags/style.css', false, '1.0.0' );
        wp_register_script('page_builder_module_select_tags_js', get_template_directory_uri() . '/inc/src/page-builder/view/assets/select-tags/script.js', false, '1.0.0' );
    }

    /**
     * Check if Enable/Disable Page Builder Module
     * @param string $_blog_id
     * @return bool|mixed
     */
    public static function isActivatedModule( $_blog_id = '') {
        global $blog_id;

        if ( ! $_blog_id ) {
            $blogID = $blog_id;
        } else {
            $blogID = $_blog_id;
        }

        if ( !function_exists( 'get_blog_option' ) ) return false;

        return get_blog_option( $blogID,  self::PAGE_BUILDER_ENABLED_STATUS_KEY );
    }

    public static function getActiveModule($params = []) {
        return self::$moduleActivated;
    }

    public function renderSelectTags($params = []){
        if(empty($params) || !self::$moduleActivated){
            return null;
        }

        $post_id = isset($params['post_id']) ? $params['post_id'] : '';
        $tags_name = isset($params['tags_name']) ? $params['tags_name'] : '';
        $type = isset($params['type']) ? $params['type'] : '';
        $media_id = isset($params['media_id']) ? $params['media_id'] : '';

        $html = isset($params['wrap_html']['first']) ? $params['wrap_html']['first'] : '';
        $html .= APL_Page_Builder_Select_Tags::renderSelectTags($post_id, $tags_name, $type, $media_id);
        $html .= isset($params['wrap_html']['last']) ? $params['wrap_html']['last'] : '';
        return $html;
    }

    public static function getPostTypeAllow() {
        return array(Apollo_DB_Schema::_ORGANIZATION_PT, Apollo_DB_Schema::_CLASSIFIED, Apollo_DB_Schema::_ARTIST_PT, 'post');
    }

    private function checkTables() {
        global $wpdb;
        $collate = '';

        $wpdb->hide_errors();

        if ( $wpdb->has_cap( 'collation' ) ) {
            if ( ! empty($wpdb->charset ) ) {
                $collate .= "DEFAULT CHARACTER SET $wpdb->charset";
            }
            if ( ! empty($wpdb->collate ) ) {
                $collate .= " COLLATE $wpdb->collate";
            }
        }
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        $apollo_pbm_tables = "";

        //tags table
        $tags_tbn = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_TAGS;
        if($wpdb->get_var("SHOW TABLES LIKE '$tags_tbn'") != $tags_tbn) {
            $apollo_pbm_tables = "
                  CREATE TABLE $tags_tbn (
                  id bigint (11) NOT NULL auto_increment,
                  post_id bigint(11) NOT NULL,
                  `value` varchar(255) not null default '',
                  PRIMARY KEY (id)
                ) $collate; ";
        }

        //posts_tags table
        $posts_tags_tbn = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_POSTS_TAGS;
        if($wpdb->get_var("SHOW TABLES LIKE '$posts_tags_tbn'") != $posts_tags_tbn) {
            $apollo_pbm_tables .= "
                  CREATE TABLE $posts_tags_tbn (
                  id bigint (11) NOT NULL auto_increment,
                  post_id bigint(11) NOT NULL,
                  tag_id bigint(11) NOT NULL,
                  `show` boolean default 1,
                  PRIMARY KEY (id)
                ) $collate; ";
        }

        //media_tags table
        $media_tags_tbn = $wpdb->prefix . APL_Page_Builder::_APL_PAGE_BUILDER_TABLE_MEDIA_TAGS;
        if($wpdb->get_var("SHOW TABLES LIKE '$media_tags_tbn'") != $media_tags_tbn) {
            $apollo_pbm_tables .= "
                  CREATE TABLE $media_tags_tbn (
                  id bigint (11) NOT NULL auto_increment,
                  post_id bigint(11) NOT NULL,
                  tag_id bigint(11) NOT NULL,
                  media_id bigint(11) NOT NULL,
                  media_type varchar(50) not null default '',
                  `show` boolean default 1,
                  PRIMARY KEY (id)
                ) $collate; ";
        }

        if(!empty($apollo_pbm_tables)) {
            dbDelta( $apollo_pbm_tables );
        }
    }
}

new APL_Page_Builder();