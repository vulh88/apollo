<?php

abstract class  APL_Module {

    /**
     * @var $dependencies
     */
    protected $dependencies;

    /**
     * @var $moduleName
     */
    protected $moduleName;

    /**
     * get required dependencies
     *
     * @return array dependencies
     */
    abstract public function getDependencies();

    /**
     * get module name
     */
    abstract public function getModuleName();


    public function __construct()
    {
        $this->dependencies = $this->getDependencies();
        $this->_requireDependencies();
    }

    /**
     * require dependencies
     * @return mixed
     */
    private function _requireDependencies() {
        $base = APOLLO_SRC_DIR. '/'. $this->getModuleName();

        foreach ($this->dependencies as $dp) {
            switch ($dp) {
                case 'admin':
                    $file = $base. "/admin/admin.php";
                    if (is_admin() && file_exists($file)) {
                        require_once $file;
                    }
                    break;
                case 'network':
                    $file = $base. "/admin/network.php";
                    if (is_network_admin() && file_exists($file)) {
                        require_once $file;
                    }
                    break;
                case 'front-end':
                    $file = $base. "/front-end.php";
                    if (is_front_page() && file_exists($file)) {
                        require_once $file;
                    }
                    break;
                default:
                    $file = $base. "/$dp";
                    if (file_exists($file)) {
                        require_once $file;
                    }
                    break;
            }
        }
    }

}