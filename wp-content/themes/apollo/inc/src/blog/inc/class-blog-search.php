<?php
/** @Ticket #13364 */
require_once APOLLO_TEMPLATES_DIR. '/pages/lib/class-apollo-page-module.php';

class Apollo_Blog_Page extends Apollo_Page_Module
{
    public static $_order_by;
    public static $_order = 'ASC';
    public static $post_type = 'post';

    public function __construct($isCounting = false, $postType = 'post')
    {
        /* @ticket #15646 */
        self::setIsCounting($isCounting);

        if($postType != 'page'){
            if ( ! Apollo_App::is_avaiable_module( $postType ) ) {
                wp_safe_redirect( '/' );
            }
        }

        self::$post_type = $postType;
        parent::__construct();
    }

    public function search()
    {
        /*@ticket #17885: 0002466: Promo video homepage - Modify the "Content" field of the widget to include the editing tool bar and access to Media Library*/
        if (Apollo_App::is_ajax()) {
            $offset = $this->getAjaxOnePageOffset();
        } else {
            $offset   = $this->page == 1 ? 0 : ($this->page - 1) * $this->pagesize;
        }

        global $wpdb;
        $keyword = self::getKeyword();
        $postType = self::$post_type;

        if (self::getIsCounting()) {
            $searchText = " SQL_CALC_FOUND_ROWS p.ID";
        } else {
            $searchText = " SQL_CALC_FOUND_ROWS p.ID, p.post_name, p.post_type, p.post_title, p.post_excerpt, post_author, p.post_content ";
        }

        /*@ticket update #17885 0002477: Global Search modifiction - Add the results tab to the end of the other tabs with default label "Pages"*/
        $joinTable = "";
        if($postType === 'page'){
            $joinTable = " join {$wpdb->postmeta} meta on (p.ID = meta.post_id and meta.meta_key = 'is_global_page' and meta.meta_value = 'no') ";
        }

        $sql = "SELECT {$searchText} FROM {$wpdb->posts} AS p $joinTable WHERE
            (
                p.post_title LIKE '%".$keyword."%'
                OR p.post_content LIKE '%".$keyword."%'
                OR p.post_excerpt LIKE '%".$keyword."%'
            )
            AND p.post_title <> ''
            AND p.post_type IN ('$postType')
            AND ( p.post_status = 'publish' )
              
";
        if (!self::getIsCounting()) {
            $arr_order = Apollo_Sort_Manager::getSortOneFieldForPostType('post');
            $sql .= " ORDER BY p.{$arr_order['order_by']} {$arr_order['order']} ";
            $sql .= " LIMIT {$offset}, {$this->pagesize} ";
        }

        $this->result = $wpdb->get_results($sql, OBJECT_K);
        $this->total = $total = $wpdb->get_var("SELECT FOUND_ROWS()");;
        $this->total_pages = ceil($this->total / $this->pagesize);
        $this->resetPostData();

        return $this->result;
    }
}
