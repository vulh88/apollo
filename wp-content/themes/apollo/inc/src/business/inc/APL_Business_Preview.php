<?php
/*@ticket #18087: [CF] 20181030 - [ORG][Admin form] Preview Changes button for the business - item 4, 5*/
class APL_Business_Preview extends Apollo_Business
{
    private $previewData;

    public function __construct($business)
    {
        parent::__construct($business, Apollo_DB_Schema::_BUSINESS_PT);

        $this->previewData = isset($_SESSION['business_data']) ? $_SESSION['business_data'] : array();

        $featured = isset($this->previewData['featured']) ? $this->previewData['featured'] : '';
        $this->setFeaturesMetaKey($featured);
    }

    /**
     * @param bool $replace
     * @return bool|mixed|string
     */
    public function get_title($replace = false)
    {
        $this->post->post_title =  isset($this->previewData['post_title']) ? $this->previewData['post_title'] : '';
        return parent::get_title($replace);
    }

    /**
     * @param null $max_length
     * @param bool $is_filter
     * @return array
     */
    public function get_content($max_length = null, $is_filter = false)
    {
        $this->post->post_content = isset($this->previewData['post_content']) ? $this->previewData['post_content'] : '';
        return parent::get_content($max_length, $is_filter);
    }

    /**
     * @return string
     */
    public function getHours()
    {
        return isset($this->previewData['hours']) ? $this->previewData['hours'] : '';
    }

    /**
     * @return string
     */
    public function getReservations()
    {
        return isset($this->previewData['reservations']) ? $this->previewData['reservations'] : '';
    }

    /**
     * @return string
     */
    public function getOtherInfo()
    {
        return isset($this->previewData['other_info']) ? $this->previewData['other_info'] : '';
    }

    /**
     * @return array|int|WP_Error
     */
    public function get_categories()
    {
        if(isset($this->previewData['categories'])){

            if(empty($this->previewData['categories']))
                return false;

            $termId = $this->previewData['categories'];
            $termId = explode("," ,$termId);
            $terms = get_terms( array(
                'taxonomy' => 'business-type',
                'include' => $termId,
                'hide_empty'  => false,
            ) );

            return $terms;
        }
        else{
            return parent::get_categories();
        }

    }
}