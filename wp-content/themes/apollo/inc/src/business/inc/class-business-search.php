<?php
require_once APOLLO_TEMPLATES_DIR. '/pages/lib/class-apollo-page-module.php';

class Apollo_Business_Page extends Apollo_Page_Module {

    public function __construct($isCounting = false) {

        /* @ticket #15646 */
        self::setIsCounting($isCounting);

        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_BUSINESS_PT ) ) {
            wp_safe_redirect( '/' );
        }

        $this->initTemplateData();

        parent::__construct();

        // Override set per page
        if (!Apollo_App::is_ajax()) {
            $this->pagesize = Apollo_App::aplGetPageSize($this->pagesize , APL_Business_Module_Theme_Option::_BUSINESS_NUM_ITEMS_LISTING_PAGE);
        }
    }

    /**
     * Init template data
     *
     * @author vulh
     * @return mixed
     */
    protected function initTemplateData()
    {
        // Set base path template
        // Ticket #11686
        $this->basePathTemplate = APOLLO_TEMPLATES_DIR. '/business/listing-page/';
        $this->viewType = of_get_option(APL_Business_Module_Theme_Option::_BUSINESS_VIEW_TYPE, 'default');

        if ($this->viewType == 'list2') {
            $this->template = $this->basePathTemplate. 'list2.php';
        }
        else {
            $this->template = $this->basePathTemplate. $this->_get_template_name(of_get_option(APL_Business_Module_Theme_Option::_BUSINESS_DEFAULT_VIEW_TYPE));
        }
    }

    public function search() {
        // Inside search-area of Business but search posts with post_type = ORGANIZATION because the relationship between Business and Organization
        $arr_params = array(
            'post_type'         => Apollo_DB_Schema::_BUSINESS_PT,
            'posts_per_page'    => self::getIscounting() ? 1 : $this->pagesize,
            'paged'             => $this->page,
            'post_status'       => array('publish'),

        );

        if(isset($_GET['term']) && !empty($_GET['term'])) {
            $arr_tax_query[] = array(
                'taxonomy'=> Apollo_DB_Schema::_BUSINESS_PT.'-type',
                'terms' => array($_GET['term']),
            );
            $arr_params['tax_query'] = $arr_tax_query;
        }

        /** @Ticket #17838 */
        if(isset($_GET['business_service']) && !empty($_GET['business_service'])) {
            $arr_params['post__in'] = self::getBusinessIDsByFilteredServices($_GET['business_service']);
        }

        // Set new offset if this is ajax action
        $this->addOffsetToParams($arr_params);

        //No need search in this time
        add_filter('posts_where', array(__CLASS__, 'filter_where_business_tbl'), 10, 1);
        add_filter('posts_join', array(__CLASS__, 'filter_join_business_tbl'), 10, 1);
        add_filter('posts_orderby', array(__CLASS__, 'filter_order_business_tbl'), 10, 1);
        add_filter('posts_search', array(__CLASS__, 'posts_search'), 10, 1);
        add_filter('posts_groupby', array($this, 'filter_groupby'), 10, 1);

        $this->result = query_posts($arr_params);

        Apollo_Next_Prev::updateSearchResult($GLOBALS['wp_query']->request,Apollo_DB_Schema::_BUSINESS_PT);
        remove_filter('posts_orderby', array(__CLASS__, 'filter_order_business_tbl'), 10);
        remove_filter('posts_join', array(__CLASS__, 'filter_join_business_tbl'), 10);
        remove_filter('posts_where', array(__CLASS__, 'filter_where_business_tbl'), 10);
        remove_filter('posts_search', array(__CLASS__, 'posts_search'), 10);
        remove_filter('posts_groupby', array($this, 'filter_groupby'), 10);

        $this->total = $GLOBALS['wp_query']->found_posts;
        $this->total_pages = ceil($this->getTotal() / $this->pagesize);

        $this->resetPostData();
    }

    public static function posts_search($p) {
       return $p;
    }

    public static function filter_where_business_tbl ($where) {
        global $wpdb;

        /*Hotfix #17886 0002477: Global Search modifiction - [Business] When an apostrophe is included in the listing's name it does not show any results in global results or right column widget results*/
        if(self::getKeyword()){

            $rootKeyword = array(self::getKeyword());
            $rootKeyword[] = str_replace("'", "‘", self::getKeyword());
            $rootKeyword[] = str_replace("'", "’", self::getKeyword());
            $rootKeyword = array_unique($rootKeyword);

            foreach ($rootKeyword as $character){

                $searchMultiWords[] = $wpdb->prepare(" $wpdb->posts.post_excerpt like '%s' OR $wpdb->posts.post_content like'%s' ", "%$character%", "%$character%");
            }

            $searchMultiWords = implode(" OR ",$searchMultiWords );

            $searchTitle = self::replaceSpecialCharacter("{$wpdb->posts}.post_title", self::$specialString, self::getKeyword());
            $where .= " AND ($searchMultiWords $searchTitle) ";
        }

        $endableBusiness = Apollo_App::checkEnableBusiness();
        // only get businesses have association with org
        $where .= " AND bsMeta.meta_key = '".Apollo_DB_Schema::_APL_BUSINESS_ORG."' AND bsMeta.meta_value <> '' ";
        $where .= " 
                        AND  {$wpdb->posts}.ID IN (
                           $endableBusiness
                    )
        ";

        // OTHER CASE
        if(isset($_GET['city']) && !empty($_GET['city'])) {
            $city = $_GET['city'];

            $wpdb->escape_by_ref($city);
            $where .= " AND CAST(mt_city.meta_value AS CHAR) LIKE '". $city ."%' ";
        }

        // OTHER CASE
        if(isset($_GET['region']) && !empty($_GET['region'])) {
            $region = $_GET['region'];

            $wpdb->escape_by_ref($region);

            /** @Ticket #18016 */
            $idsNotHaveAddress = self::_getIdsNotHaveAddress();
            $tbl = $wpdb->posts;
            $sqlNotHaveAddress = "";
            if (!empty($idsNotHaveAddress)) {
                $sqlNotHaveAddress = " AND $tbl.ID NOT IN ( ". implode(',', $idsNotHaveAddress) ." ) ";
            }
            if ($enableRegionSelection = Apollo_App::enableMappingRegionZipSelection()) {
                $ids = self::_getIDsByFilterSelectedRegion($region);
                $where .= " AND $tbl.ID IN ( ".implode(',', $ids)." ) " . $sqlNotHaveAddress;
            }
            else {
                $where .= " AND CAST(mt_region.meta_value AS CHAR) LIKE '". $region ."%' " . $sqlNotHaveAddress;
            }
        }

        return $where;
    }

    /**
     * @Ticket #18016
     * get all business does not have address1
     * @return array
     */
    private static function _getIdsNotHaveAddress() {
        global $wpdb;
        /** @Ticket #18016 */
        $businessOrgField = Apollo_DB_Schema::_APL_ORG_BUSINESS;
        $addressField = Apollo_DB_Schema::_APL_ORG_ADDRESS;
        $address1TextQuery = '_org_address1";s:0';
        $sql = "    SELECT om.meta_value
                    FROM {$wpdb->{Apollo_Tables::_APL_ORG_META}} om
                    INNER JOIN {$wpdb->{Apollo_Tables::_APL_ORG_META}} address ON om.apollo_organization_id = address.apollo_organization_id 
                    WHERE om.meta_key = '{$businessOrgField}' AND address.meta_key = '{$addressField}'  
                        AND address.meta_value LIKE '%{$address1TextQuery}%' ";

        $result = $wpdb->get_col($sql);

        return empty($result) ? array(-1) : $result;

    }

    /**
     * Get IDs by selected regions
     *
     * @param $region
     * @return array
     */
    private static function _getIDsByFilterSelectedRegion($region){
        global $wpdb;
        if (!$zipcodes = Apollo_Seach_Form_Data::getRegionZipcodes($region)) {
            $zipcodes = array(false);
        }
        $zipQuery = '"'. implode('","', $zipcodes). '"';
        $sql = '    SELECT em.apollo_business_id
                    FROM '.$wpdb->{Apollo_Tables::_APOLLO_BUSINESS_META}.' em
                    WHERE em.meta_value  IN('.$zipQuery.')
                        AND em.meta_key = "'.Apollo_DB_Schema::_BUSINESS_ZIP.'"';

        $result = $wpdb->get_col($sql);

        return empty($result) ? array(-1) : $result;

    }

    public static function arrayObjBSToArrayIDs($bsObjects = array()){
        if(empty($bsObjects)) return array(-1);
        $resultIDs = array();
        foreach($bsObjects as $item){
            $resultIDs[] = $item->ID;
        }
        return $resultIDs;
    }

    public static function getBusinessIDsByFilteredServices($arrServices = array()){
        $result = array(-1);
        $aplQuery = new Apl_Query(Apollo_Tables::_APOLLO_BUSINESS_META);
        $metaBSIDKey = "apollo_" . Apollo_DB_Schema::_BUSINESS_PT . "_id";
        $queryData = $aplQuery->get_where(" meta_key LIKE '".Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY."_%' AND meta_value IN ('".implode("','",$arrServices)."') "," DISTINCT(".$metaBSIDKey.") ");
        if($queryData){
            foreach($queryData as $bsMetaObj){
                $result[] = $bsMetaObj->$metaBSIDKey;
            }
        }
        return $result;
    }

    public static function filter_join_business_tbl ($join) {
        global $wpdb;
        $join .= " LEFT JOIN {$wpdb->{Apollo_Tables::_APOLLO_BUSINESS_META}} as bsMeta ON ({$wpdb->posts}.ID = bsMeta.apollo_business_id)";

        if(isset($_GET['city']) && !empty($_GET['city'])) {
            $join .=
                " INNER JOIN {$wpdb->{Apollo_Tables::_APOLLO_BUSINESS_META}} mt_city ON "
                . "(mt_city.meta_key = '". Apollo_DB_Schema::_BUSINESS_CITY ."' AND {$wpdb->posts}.ID = mt_city.apollo_business_id)";
        }

        if(isset($_GET['region']) && !empty($_GET['region'])) {
            $join .=
                " INNER JOIN {$wpdb->{Apollo_Tables::_APOLLO_BUSINESS_META}} mt_region ON "
                . "(mt_region.meta_key = '". Apollo_DB_Schema::_BUSINESS_REGION ."' AND {$wpdb->posts}.ID = mt_region.apollo_business_id)";
        }

        $join .= self::renderJoinCustomFieldSearch(Apollo_Tables::_APOLLO_BUSINESS_META, 'apollo_business_id');

        return $join;
    }

    /* last name is not query with city in same query: Good to know that. because we don't need join an extra query to sort by last_name  */
    public static function filter_order_business_tbl ($order) {

        if (self::getIsCounting()) {
            return false;
        }

        global $wpdb;
        $order= $wpdb->posts.'.post_title ASC';
        return $order;
    }

    /**
     * Override get offset for one page
     *
     * @author vulh
     * @return integer
    */
    public function getAjaxOnePageOffset() {
        // get a number of view more items in Theme Options
        $offset = of_get_option(APL_Business_Module_Theme_Option::_BUSINESS_NUM_ITEMS_LISTING_PAGE);
        if(empty($offset))
            $offset = of_get_option(Apollo_DB_Schema::_APL_NUMBER_ITEMS_FIRST_PAGE, Apollo_Display_Config::APL_DEFAULT_NUMBER_ITEMS_FIRST_PAGE);
        if ($this->page > 2) {
            $offset += $this->pagesize * ($this->page - 2);
        }
        return intval($offset);
    }

    /**
     * Override get have view more
     *
     * @author vulh
     * @return integer
     */
    public function have_more_on_viewmore() {
        $numItemFirstPage = of_get_option(APL_Business_Module_Theme_Option::_BUSINESS_NUM_ITEMS_LISTING_PAGE);
        if(empty($numItemFirstPage))
            $numItemFirstPage = of_get_option(Apollo_DB_Schema::_APL_NUMBER_ITEMS_FIRST_PAGE, Apollo_Display_Config::APL_DEFAULT_NUMBER_ITEMS_FIRST_PAGE);
        $numViewMore = of_get_option(Apollo_DB_Schema::_APL_NUMBER_ITEMS_VIEW_MORE, Apollo_Display_Config::APL_DEFAULT_NUMBER_ITEMS_VIEW_MORE);

        return ($numItemFirstPage + ($numViewMore*($this->page - 1))) < intval( $this->total );
    }
}
