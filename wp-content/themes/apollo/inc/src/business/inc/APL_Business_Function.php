<?php

class APL_Business_Function
{

    /**
     * @param $trees
     * @param $default_value
     * @param int $max_level
     * @param array $hiddenCatIDs
     * @param int $parentIDFeature - a category is dining (selected from theme option) - applying to the business module
     * @param bool $isChild - mark all child of a dining as a dining category - applying to the business module
     * @param int $firstParentID
     */
    public static function build_option_tree($trees,
                                             $default_value,
                                             $max_level = Apollo_Display_Config::TREE_MAX_LEVEL,
                                             $hiddenCatIDs = array(),
                                             $parentIDFeature = 0,
                                             $isChild = false,
                                             $firstParentID = 0) {

        if($max_level <= 0) return;

        foreach($trees as $tree ):


            if ($tree->parent == 0) {
                $firstParentID = $tree->term_id;
            }

            // Parent is a feature (dining) or child is a feature
            $isDining = $isChild || $parentIDFeature == $tree->term_id;
            ?>
            <option data-first-parent="<?php echo $firstParentID ?>" data-dining="<?php echo $isDining ? 1 : 0; ?>" value="<?php echo $tree->term_id ?>" <?php selected($tree->term_id, $default_value) ?>><?php echo str_repeat('--', (Apollo_Display_Config::TREE_MAX_LEVEL - $max_level)).' ' . $tree->name ?></option>
            <?php

            if(isset($tree->childs) && !empty($tree->childs)):

                self::build_option_tree($tree->childs, $default_value, $max_level-1, $hiddenCatIDs, $parentIDFeature, $isDining, $firstParentID);
            endif;
        endforeach;
    }

    /**
     * @param array $arg
     * @return array|mixed
     */
    public static function getTreeBusinessType($arg = array('hide_empty' => true, 'orderby' => 'name', 'order' => 'ASC')) {

        /*@ticket #17933: [CF] 20181012 - [Business][Category] Display the associated parent and sub-category type filters in the search widget*/
        if (get_query_var( 'taxonomy' )) {
            global $apl_current_term_object;
            $currentTerm = $apl_current_term_object ? $apl_current_term_object : Apollo_App::getCurrentTerm();
            $parentTerm = self::_getParentTerm($currentTerm);
            $terms = array($parentTerm);
            $childTermIDs = get_term_children($parentTerm->term_id, 'business-type');

            if(!empty($childTermIDs)){
                $arg['include'] = $childTermIDs;
                $childTerms = get_terms('business-type', $arg);
                $terms = array_merge($terms, $childTerms);
            }
        }
        else {
            $terms = get_terms(Apollo_DB_Schema::_BUSINESS_TYPE, $arg);
        }

        return Apollo_App::build_tree_category($terms);
    }

    /**
     * @param $currentTerm
     * @return mixed
     */
    private static function _getParentTerm($currentTerm) {
        if (!$currentTerm->parent) {
            return $currentTerm;
        }

        $parent = get_term($currentTerm->parent, 'business-type');
        return self::_getParentTerm($parent);
    }

}