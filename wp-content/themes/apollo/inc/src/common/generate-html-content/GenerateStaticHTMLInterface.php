<?php

interface GenerateStaticHTMLInterface
{
    /**
     * Generate static HTML
     * @return string
     */
    public function generateHtml();
}