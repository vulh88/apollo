<?php

include_once APOLLO_SRC_DIR . '/common/generate-html-content/GenerateStaticHTMLInterface.php';
include_once APOLLO_SRC_DIR . '/common/generate-html-content/GenerateStaticHTMLAbstract.php';


class EducationStaticHTMLFactory extends GenerateStaticHTMLAbstract implements GenerateStaticHTMLInterface
{
    public function __construct($locationKey)
    {
        $this->locationKey = $locationKey;
        parent::__construct();
    }

    /**
     * Return suffix for the static HTML file
     * @return string
     */
    public function setModuleOptName()
    {
        return '_education';
    }

    /**
     * Generate static HTML
     * @return string
     */
    public function generateHtml()
    {
        return $this->generate();
    }
}