<?php

abstract class GenerateStaticHTMLAbstract {

    /**
     * 3 display types from the Theme options
    */
    const CUSTOM_CONTENT_TYPE = 0;
    const GLOBAL_CONTENT_TYPE = 1;
    const REFERENCE_CONTENT_TYPE = 2;

    /**
     * 3 prefixes of Theme option configurations
    */
    const CONTENT_TYPE_OPT_PREFIX = '_content_type';
    const DISPLAY_TYPE_OPT_PREFIX = '_display_type';
    const REF_MODULE_OPT_PREFIX = '_reference_module';

    /**
     * @var $moduleOptName
     * The suffix of the HTML file is normally module name for instance "_education, _event, _post, _artist"
     */
    protected $moduleOptName;

    /**
     * @var $locationKey
     * The key defines a location of HTML such as HEADER, FOOTER, PARTNER LOGO
     */
    protected $locationKey;

    /**
     * @var $moduleLocationKey
     * The meaning is the same as $locationKey, but it contains $suffix
     */
    protected $moduleLocationKey;

    /**
     * Return suffix for the static HTML file
     * @return string
     */
    abstract public function setModuleOptName();

    public function __construct()
    {
        $this->moduleOptName = $this->setModuleOptName();
        $this->moduleLocationKey = $this->locationKey. $this->moduleOptName;
    }

    /**
     * @param $fileName
     * @return bool|string
     * @throws Exception
     */
    private function getContent($fileName){
        $fileName = Apollo_App::getUploadBaseInfo('html_dir') . '/' . $fileName . '.html';
        if (file_exists($fileName)) {
            $content = file_get_contents($fileName);
        }

        return !empty($content) ? $content : '';
    }

    /**
     * @return bool|string
     * @throws Exception
     */
    public function generate()
    {

        // Check if the current module is enabling override/extend static HTML feature
        $contentType = of_get_option($this->getContentTypeOptKey(), 0);

        switch ($contentType) {
            case self::CUSTOM_CONTENT_TYPE: // Custom its private template

                $displayOption = of_get_option($this->getDisplayTypeTypeOptKey(), 1); //default is override the global template
                $content = $this->getContent($this->moduleLocationKey); // Get static HTML of the module

                // Extend global template
                if (!$displayOption) {
                    $content = $this->getContent($this->locationKey) . $content;
                }
                break;


            case self::GLOBAL_CONTENT_TYPE: // Use the global template
            /**
             * This is a reference content type. However, we do not accept reference option in another reference option to avoid the circle loop code. For instance:
             * We want to display the footer of education on the blog module. In this case, the header blog option type is "reference" to education module. If education header has
             * the "display type" is reference to another one as well, we stop rendering
            */
            default:
                $content = $this->getContent($this->locationKey);
                break;
        }

        return $content;
    }

    /**
     * Get reference module option name (for instance education, event, etc)
     * @param $moduleOptName
     */
    public function getReferenceModuleSuffix(&$moduleOptName) {
        $keyModule = $this->locationKey. $moduleOptName; // For instance: _header_blog, _header_education, etc

        // Check if this module is enabled to get content from another module
        if (of_get_option(self::CONTENT_TYPE_OPT_PREFIX . $keyModule, 0) == self::REFERENCE_CONTENT_TYPE) {

            // Get reference module value
            $key = self::REF_MODULE_OPT_PREFIX. $keyModule;
            $moduleOptName = of_get_option($key, $moduleOptName);
        }
    }

    /**
     * Get content type option key
     * @return string
     */
    public function getContentTypeOptKey() {
        return self::CONTENT_TYPE_OPT_PREFIX . $this->moduleLocationKey;
    }

    /**
     * Get display type option key
     * @return string
     */
    public function getDisplayTypeTypeOptKey() {
        return self::DISPLAY_TYPE_OPT_PREFIX . $this->moduleLocationKey;
    }

    /**
     * Get ref module option key
     * @return string
     */
    public function getRefModuleOptKey() {
        return self::REF_MODULE_OPT_PREFIX . $this->moduleLocationKey;
    }
}