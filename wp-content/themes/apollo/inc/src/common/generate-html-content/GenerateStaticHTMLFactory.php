<?php

class GenerateStaticHTMLFactory
{
    /**
     * @param $locationKey
     * @return string
     */
    public function generateHtml($locationKey) {

        global $current_module;
        $module = !empty($current_module) ? $current_module : $this->getCurrentModule();
        $GLOBALS['current_module'] = $module;

        switch ($module){
            case 'education':
                include_once APOLLO_SRC_DIR. '/common/generate-html-content/EducationStaticHTMLFactory.php';
                $education = new EducationStaticHTMLFactory($locationKey);
                $content = $education->generateHtml();
                break;
            case 'blog':
                include_once APOLLO_SRC_DIR. '/common/generate-html-content/BlogStaticHTMLFactory.php';
                $blog = new BlogStaticHTMLFactory($locationKey);
                $content = $blog->generateHtml();
                break;
            default:
                $content = Apollo_App::get_static_html_content($locationKey);
                break;
        }

        return do_shortcode(Apollo_App::replaceTextHeader($content));
    }

    /**
     * @return string
     */
    public function getCurrentModule(){

        $postType = get_post_type();
        $module = '';
        $pageTemplate = get_page_template();
        $pageTemplate = substr($pageTemplate, strrpos($pageTemplate, '/') + 1);
        $currentTaxType = $currentTaxType = get_query_var( 'taxonomy' );

        if(get_query_var( '_apollo_program_search' ) === 'true'
            || get_query_var( '_apollo_educator_search' )  === 'true'
            || $postType == 'educator'
            || $postType == 'program'
            || ($postType == 'page' && $pageTemplate == "educator-landing-template.php"
            || in_array($currentTaxType, array('artistic-discipline', 'program-type', 'educator-type')))
        )
        {
            $module = 'education';
        }
        else if(get_query_var( '_apollo_page_blog' ) === 'true'
            || (is_single() && $postType == Apollo_DB_Schema::_BLOG_POST_PT)){
            $module = 'blog';
        }

        return $module;
    }
}