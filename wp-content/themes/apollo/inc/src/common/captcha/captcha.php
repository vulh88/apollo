<?php
/**
 * @ticket #18933: Apply reCaptcha to the FE registration form
 */
?>
    <input type="hidden" id="cptchar_enable" value="<?php echo get_current_user_id() ? 0 : 1; ?>"/>
<?php
if (!get_current_user_id()):
    $captcha = new Apollo_Captcha();
    $req = true;
    ?>

    <div class="capcha-frm">
        <div class="apl_capchar_err_container">
                <span class="error" id="apl_capchar_err">
                    <span id="required_err" class="_required hidden">* <?php echo __('Captcha is required!', 'apollo') ?></span>
                    <span id="captcha_err"
                          class="_captcha hidden">* <?php echo __('Captcha not matched, Please try again', 'apollo') ?></span>
                </span>
        </div>
        <div class="capcha-form">
            <div>
                <div class="inp-half ">
                    <label class="apl-capcha-label" for="_captcha"><?php echo __('CAPTCHA Code') ?>
                        <?php echo $req ? '<span class="required">(*)</span>' : '' ?>
                    </label>
                    <input
                            data-error_holder="#apl_capchar_err"
                            data-captcha_image="#respond #_captcha_image"
                            data-error_hide="hidden"
                            data-check="required,captcha"
                            data-owner_form="#capcha-form"
                            data-url_check_captcha="<?php echo $captcha->getUrlCheckCaptcha() ?>"
                            id="_captcha" name="captcha_code" class="inp inp-txt" type="text" value=""
                            size="30"/></div>
                <div class="inp-half custom-cpt-image">
                    <img id="_captcha_image" src="<?php echo $captcha->getImagePath() ?>"/>
                </div>
            </div>
        </div>
    </div>
<?php endif;