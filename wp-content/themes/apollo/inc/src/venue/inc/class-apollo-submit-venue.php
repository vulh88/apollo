<?php

class Apollo_Submit_Venue extends Apollo_Submit_Form {
    public $errors = array();
    public function __construct($rule) {
        $this->post_type = Apollo_DB_Schema::_VENUE_PT;
        $this->rules = $rule;
    }
    public function save($arrData, $arrCustomField = array(), $arrCFSearch = array()) {
        $postStatus = Apollo_App::insertStatusRule(Apollo_DB_Schema::_VENUE_PT);
        if(isset($arrData['ID']) && $arrData['ID'] != ''){
            $postStatus = get_post_status($arrData['ID']);
        }

        $post = array(
            'ID'            => isset($arrData['ID'])?$arrData['ID']:'',
            'post_title'    => Apollo_App::clean_data_request( $arrData['post_title'] ),
            'post_content'  => Apollo_App::convertCkEditorToTinyCME($arrData['post_content']),
            'post_author'   => get_current_user_id(),
            'post_type'     => Apollo_DB_Schema::_VENUE_PT,
            'post_status'   =>$postStatus,
        );
        $postId = $post['ID'];
        if(isset($post['ID']) && !empty($post['ID'])){
            unset($post['post_author']);
            wp_update_post($post);
            
            // Keep icons due to it not exist in the front end form
            $arrData[Apollo_DB_Schema::_APL_VENUE_POST_ICONS] = Apollo_App::apollo_get_meta_data($postId, Apollo_DB_Schema::_APL_VENUE_POST_ICONS, Apollo_DB_Schema::_APL_VENUE_DATA );
            $isAddingNewItem = false;
        }else{
            $postId = wp_insert_post($post);
            $isAddingNewItem = true;
        }
//        $icons = Apollo_App::apollo_get_meta_data($postId, Apollo_DB_Schema::_APL_ORG_POST_ICONS, Apollo_DB_Schema::_APL_ORG_DATA );

        if(isset($arrData[Apollo_DB_Schema::_VENUE_TYPES])){
            wp_set_post_terms( $postId, $arrData[Apollo_DB_Schema::_VENUE_TYPES],Apollo_DB_Schema::_VENUE_PT.'-type' );
        }

        unset($arrData[Apollo_DB_Schema::_VENUE_TYPES]);
        $arrayMetaGroup = $this->getMetaGroup($arrData,$postId);
        //$arrayMetaGroup[Apollo_DB_Schema::_APL_ORG_DATA][ Apollo_DB_Schema::_APL_ORG_POST_ICONS] = $icons;
        foreach($arrayMetaGroup as $group => $value){
            if($group != Apollo_DB_Schema::_VENUE_TYPES){
                //meta
                update_apollo_meta($postId, $group, is_array($value) ? Apollo_App::clean_array_data($value) : $value);
            }
        }

        //update custom field
        $group_fields = Apollo_Custom_Field::get_group_fields( Apollo_DB_Schema::_VENUE_PT );
        $customFieldValue = array();



        if(is_array($group_fields) && !empty($group_fields)){
            foreach($group_fields as $group){
                if(isset($group['fields']) && !empty($group['fields'])){
                    foreach($group['fields'] as $field){
                        if(isset($arrData[$field->name])){
                            if(is_array( $arrData[$field->name])){
                                $customFieldValue[$field->name] = $arrData[$field->name];
                            }
                            elseif($field->cf_type == 'wysiwyg'){
                                $customFieldValue[$field->name] = Apollo_App::convertCkEditorToTinyCME($arrData[$field->name]);
                            }
                            else{
                                $customFieldValue[$field->name] = Apollo_App::clean_data_request($arrData[$field->name]);
                            }
                            if(Apollo_Custom_Field::has_explain_field($field)){
                                $exFieldName = $field->name.'_explain';
                                $customFieldValue[$exFieldName] = Apollo_App::clean_data_request($arrData[$exFieldName]);
                            }
                        } else if ($postId) {
                            $customFieldValue[$field->name] = Apollo_App::apollo_get_meta_data($postId, $field->name, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA);
                        }
                    }
                }
            }
        }
        update_apollo_meta($postId, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA, serialize($customFieldValue));

        /**
         * Thienld : send email to confirm admin the new post is created
         **/
        if(has_action('apollo_email_confirm_new_item_created') && $isAddingNewItem){
            do_action('apollo_email_confirm_new_item_created',$postId,Apollo_DB_Schema::_VENUE_PT,false);
        }

        //Truonghn :Send mail to admin when the post is edited
        if(has_action('apollo_email_confirm_item_edited') && !$isAddingNewItem && !current_user_can('manage_options') && $postStatus == 'publish' ){
            do_action('apollo_email_confirm_item_edited',$postId,Apollo_DB_Schema::_VENUE_PT, false);
        }

        return $postId;
    }

    private function getMetaGroup($arrData,$postId){

        
        $arrayMetaGroup = array(
            Apollo_DB_Schema::_APL_VENUE_ADDRESS => array(
                Apollo_DB_Schema::_VENUE_ADDRESS1 => $arrData[ Apollo_DB_Schema::_VENUE_ADDRESS1 ],
                Apollo_DB_Schema::_VENUE_ADDRESS2 => $arrData[Apollo_DB_Schema::_VENUE_ADDRESS2],
                Apollo_DB_Schema::_VENUE_CITY => $arrData[Apollo_DB_Schema::_VENUE_CITY],
                Apollo_DB_Schema::_VENUE_STATE => $arrData[Apollo_DB_Schema::_VENUE_STATE],
                Apollo_DB_Schema::_VENUE_ZIP => $arrData[Apollo_DB_Schema::_VENUE_ZIP],
                Apollo_DB_Schema::_VENUE_REGION => isset($arrData[Apollo_DB_Schema::_VENUE_REGION])? $arrData[Apollo_DB_Schema::_VENUE_REGION] : Apollo_App::apollo_get_meta_data($postId, Apollo_DB_Schema::_VENUE_REGION, Apollo_DB_Schema::_APL_VENUE_ADDRESS ),
                Apollo_DB_Schema::_VENUE_NEIGHBORHOOD => isset($arrData[Apollo_DB_Schema::_VENUE_NEIGHBORHOOD]) ? $arrData[Apollo_DB_Schema::_VENUE_NEIGHBORHOOD] : '',
            ),
            Apollo_DB_Schema::_VENUE_ZIP => $arrData[Apollo_DB_Schema::_VENUE_ZIP],

        );
        unset(
            $arrData['id'],
            $arrData['post_title'],
            $arrData['post_content'],
            $arrData['post_author'],
            $arrData['post_type'],
            $arrData['post_status'],
            $arrData[Apollo_DB_Schema::_VENUE_ADDRESS1 ],
            $arrData[Apollo_DB_Schema::_VENUE_ADDRESS2 ],
            $arrData[Apollo_DB_Schema::_VENUE_STATE],
            $arrData[Apollo_DB_Schema::_VENUE_ZIP],
            $arrData[Apollo_DB_Schema::_VENUE_CITY],
            $arrData[Apollo_DB_Schema::_VENUE_REGION]
        );
        $arrayMetaGroup[Apollo_DB_Schema::_APL_VENUE_DATA] = $arrData;
        $arrayMetaGroup[Apollo_DB_Schema::_VENUE_LATITUDE] = Apollo_App::apollo_get_meta_data($postId, Apollo_DB_Schema::_VENUE_LATITUDE );
        $arrayMetaGroup[Apollo_DB_Schema::_VENUE_LONGITUDE] = Apollo_App::apollo_get_meta_data($postId, Apollo_DB_Schema::_VENUE_LONGITUDE );
        /** @Ticket #13028 */
        $arrayMetaGroup[Apollo_DB_Schema::_VENUE_PHONE] = $arrData[Apollo_DB_Schema::_VENUE_PHONE];
        /** @Ticket #13874 */
        $arrayMetaGroup[Apollo_DB_Schema::_VENUE_NEIGHBORHOOD] = isset($arrData[Apollo_DB_Schema::_VENUE_NEIGHBORHOOD]) ? $arrData[Apollo_DB_Schema::_VENUE_NEIGHBORHOOD] : '';
        return $arrayMetaGroup;

    }
    public function saveVideo($arrData, $arrCustomField = array(), $arrCFSearch = array()) {
        $data = array();
        if(isset($arrData['video_embed']) && is_array($arrData['video_desc']) ){
            foreach($arrData['video_embed'] as $k => $val){
                $arrDataItem = array(
                    'video_embed' => $val,
                    'video_desc' => base64_encode(Apollo_App::clean_data_request($arrData['video_desc'][$k]))
                );
                $data[] = $arrDataItem;
            }
        }
        update_apollo_meta( $arrData['ID'], Apollo_DB_Schema::_APL_VENUE_VIDEOS, serialize( $data ) );

    }
    public function saveAudio($arrData, $arrCustomField = array(), $arrCFSearch = array()) {
        $data = array();
        if(isset($arrData['embed']) && isset($arrData['desc']) && is_array($arrData['desc']) ){
            foreach($arrData['embed'] as $k => $val){
                $val = str_replace('\\','',$val);
                $arrDataItem = array(
                    'embed' => base64_encode($val),
                    'desc' => base64_encode(Apollo_App::clean_data_request($arrData['desc'][$k]))
                );
                $data[] = $arrDataItem;
            }
        }
        $dataSerialize = serialize($data);
        update_apollo_meta( $arrData['ID'], Apollo_DB_Schema::_APL_ORG_AUDIO, $dataSerialize );

    }

    public function isValidAll($arrData){

        return parent::isValidAll($arrData);
    }

    public function the_errors() {
        if ( ! $this->errors ) return false;

        return '<span class="error wrap"> * '.__( 'There are some errors in your submitted data. Please check them again !', 'apollo' ).'</span>';
    }

    public function the_error_class( $field ) {
        if ( isset( $this->errors[$field] ) ) return 'inp-error';
    }

    public function the_field_error( $field ) {

        if ( $this->errors && isset( $this->errors[$field] ) && $this->errors[$field] ) {
            return '<span class="error"> '.$this->errors[$field].'</span>';
        }
    }

    public function get_value( $field, $parent = '' ) {

        if ( $parent == Apollo_DB_Schema::_APOLLO_EVENT_DATA ) {
            $parent = 'event_data';
        }

        if ( $parent == Apollo_DB_Schema::_APL_EVENT_TMP_VENUE ) {
            $parent = 'tmp_venue';
        }

        switch ( $field ) {
            case Apollo_DB_Schema::_APOLLO_EVENT_START_DATE:
                $field = 'start_date';
                break;

            case Apollo_DB_Schema::_APOLLO_EVENT_END_DATE:
                $field = 'end_date';
                break;

            case Apollo_DB_Schema::_APOLLO_EVENT_FREE_ADMISSION:
                $field = 'free_event';
                break;

            case Apollo_DB_Schema::_APL_EVENT_TMP_ORG:
                $field = 'tmp_org';
                break;

            case Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID:
                $field = 'pri_cat';
                break;

            case Apollo_DB_Schema::_APOLLO_EVENT_ORGANIZATION:
                $field = 'r_org';
                break;

            case Apollo_DB_Schema::_APOLLO_EVENT_VENUE:
                $field = 'venue';
                break;

            case Apollo_DB_Schema::_APOLLO_EVENT_DAYS_OF_WEEK:
                $field = 'days_of_week';
                break;
        }

        if ( ! $parent ) return isset( $this->{$field} ) ? $this->{$field} : '';
        $parent = $this->{$parent};
        return ( isset( $parent[$field] ) ) ? $parent[$field] : '';
    }
}