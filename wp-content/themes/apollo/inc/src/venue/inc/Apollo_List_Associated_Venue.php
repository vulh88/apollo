<?php

/**
 * Class Apollo_List_Associated_Venue
 *
 * @ticket #11493
 */
class Apollo_List_Associated_Venue
{
    /**
     * @var integer
     */
    protected $page;

    /**
     * @var integer
     */
    protected $perPage;

    /**
     * @var array
     */
    protected $orgs;

    /**
     * @var bool
     */
    protected $hasMore;

    /**
     * @var integer
     */
    protected $totalVenues;

    /**
     * Apollo_List_Associated_Org constructor.
     *
     * @param array $themeToolAssociatedVenues
     */
    public function __construct($themeToolAssociatedVenues = array())
    {
        $this->page     = 1;
        $this->perPage = -1;
        $this->hasMore  = false;

        $this->venues      = $themeToolAssociatedVenues;
        $this->totalVenues = $this->venues ? count($this->venues) : 0;
    }

    /**
     * Set current page
     *
     * @param integer $page
     *
     * @return object
     */
    public function currentPage($page)
    {
        if ( !is_integer($page) ) {
            return $this;
        }

        $this->page = $page;

        return $this;
    }

    /**
     * Change current page size
     *
     * @param integer $size
     *
     * @return $this
     */
    public function changePerPage($size)
    {
        if ( !is_integer($size) ) {
            return $this;
        }

        $this->perPage = $size;

        return $this;
    }

    /**
     * Get associated venues from Event Theme tool
     *
     * @param integer $termId
     *
     * @return array
     */
    public function getThemeToolAssociatedVenues($termId = 0)
    {
        if ( $termId == 0 ) {
            return array();
        }

        // get term based on ID
        require APOLLO_TEMPLATES_DIR . '/taxonomy/inc/single.php';
        $term     = get_term($termId);
        $category = new Apollo_Single_Category($term);
        $data     = $category->themeToolClass->getThemeToolData();

        $this->venues      = !is_array($data->associated_venues)
                             ? maybe_unserialize($data->associated_venues)
                             : $data->associated_venues;
        $this->totalVenues = count($this->venues);

        return $this->venues;
    }

    /**
     * Check is empty associated venues of Event Theme Tool
     *
     * @return bool
     */
    public function isEmpty()
    {
        return $this->totalVenues < 0;
    }

    /**
     * Check has more items
     *
     * @return bool
     */
    public function isShowMore()
    {
        if ($this->perPage <= 0) {
            return false;
        }
        $currentPosition = ($this->page * $this->perPage);
        return $currentPosition < $this->totalVenues;
    }

    /**
     * Render associated organizations to HTML
     *
     * @return string
     */
    public function renderVenueHtml()
    {
        if ( count($this->venues) < 0 ) {
            return '';
        }

        // get Venue ID, sort by venue title
        $associatedVenues = get_posts(array(
            'posts_per_page' => $this->perPage,
            'offset'         => ($this->page - 1) * $this->perPage,
            'orderby'        => 'post_title',
            'order'          => 'ASC',
            'post__in'       => $this->venues,
            'post_type'      => Apollo_DB_Schema::_VENUE_PT,
            'post_status'    => 'publish',
            'fields'         => 'ids'
        ));

        ob_start();
        /** @Ticket #13448 */
        include APOLLO_TEMPLATES_DIR . '/venue/partial/_venue-tpl.php';;
        return ob_get_clean();
    }
}
