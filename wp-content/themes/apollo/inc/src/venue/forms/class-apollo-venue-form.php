<?php

/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/7/2015
 * Time: 11:41 AM
 */
class Apollo_Venue_Form extends Apollo_Venue_Base_Form
{

    protected $isAddingNewVenue = false;

    /**
     * @return boolean
     */
    public function isIsAddingNewVenue()
    {
        return $this->isAddingNewVenue;
    }

    /**
     * @param boolean $isAddingNewVenue
     */
    public function setIsAddingNewVenue($isAddingNewVenue)
    {
        $this->isAddingNewVenue = $isAddingNewVenue;
    }

    /* Init form element then set data for them */
    public function formInit()
    {
        global $apollo_event_access;

        $arrData = $this->formData;

        $stateVal = '';
        $cityVal = '';
        if( $arrData ){
            $stateVal = isset($arrData[Apollo_DB_Schema::_VENUE_STATE]) ? $arrData[Apollo_DB_Schema::_VENUE_STATE] : '';
            $cityVal = isset($arrData[Apollo_DB_Schema::_VENUE_CITY]) ? $arrData[Apollo_DB_Schema::_VENUE_CITY]: '';
        } else {
            $list_states = Apollo_App::getListState();
            $default_states =  of_get_option(Apollo_DB_Schema::_APL_DEFAULT_STATE);
            $cityVal = of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY);
            if( $default_states && in_array($default_states,$list_states) ) {
                $stateVal = $default_states;
            } else if(count($list_states) == 1) {
                $stateVal = key($list_states);
            }
        }

        /** @Ticket #13643 */
        $requiredEmail = of_get_option(Apollo_DB_Schema::_VENUE_ENABLE_EMAIL_REQUIREMENT, 0);
        $requiredPhone = of_get_option(Apollo_DB_Schema::_VENUE_ENABLE_PHONE_REQUIREMENT, 0);
        $requiredAddress = of_get_option(Apollo_DB_Schema::_VENUE_ENABLE_ADDRESS_REQUIREMENT, 0);
        $requiredState = of_get_option(Apollo_DB_Schema::_VENUE_ENABLE_STATE_REQUIREMENT, 0);
        $requiredCity = of_get_option(Apollo_DB_Schema::_VENUE_ENABLE_CITY_REQUIREMENT, 0);
        $requiredNeighborhood = of_get_option(APL_Theme_Option_Site_Config_SubTab::_VENUE_ENABLE_NEIGHBORHOOD_REQUIREMENT, 0);
        $requiredZip = of_get_option(Apollo_DB_Schema::_VENUE_ENABLE_ZIP_REQUIREMENT, 0);
        $requiredClass = 'validate[required]';

        /** @Ticket #13856 */
        $neighborhoodOptions = APL_Lib_Territory_Neighborhood::getNeighborhoodByCity($cityVal, $stateVal);

        /**
         * @ticket #19642: [CF] 20190402 - Auto fill the 'contact information' name and email - Item 6
         */
        $contactName = isset($arrData[Apollo_DB_Schema::_VENUE_CONTACT_NAME]) ? $arrData[Apollo_DB_Schema::_VENUE_CONTACT_NAME] : '';
        $current_user = wp_get_current_user();
        if (!$contactName) {
            $contactName = !empty($current_user->display_name) ? $current_user->display_name : '';
        }

        $contactEmail = isset($arrData[Apollo_DB_Schema::_VENUE_CONTACT_EMAIL]) ? $arrData[Apollo_DB_Schema::_VENUE_CONTACT_EMAIL] : '';
        if (!$contactEmail) {
            $contactEmail = !empty($current_user->user_email) ? $current_user->user_email : '';
        }
        $formElementItems = array(
            //group1: information
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

                'ID' => array(
                    'type' => 'Hidden',
                    'name' => 'ID',
                    'place_holder' => __('', 'apollo'),
                    'value' => isset($arrData['ID']) ? $arrData['ID'] : '',
                    'class' => 'inp inp-txt',
                    'title' => __('Name', 'apollo'),
                    'container' => '',
                ),
                'post_status' => array(
                    'type' => 'Hidden',
                    'name' => 'post_status',
                    'place_holder' => __('Post status', 'apollo'),
                    'value' => isset($arrData['post_status']) ? $arrData['post_status'] : 'pending',
                    'class' => '',
                    'title' => __('Post status', 'apollo'),
                    'validate' => true,
                ),
                'post_title' => array(
                    'type' => 'Text',
                    'name' => 'post_title',
                    'place_holder' => __('Venue Name (*)', 'apollo'),
                    'value' => isset($arrData['post_title']) ? $arrData['post_title'] : '',
                    'class' => 'inp inp-txt validate[required]',
                    'data_attributes' => array(
                        'data-errormessage-value-missing="'.__('Venue name is required','apollo').'"'
                    ),
                    'title' => __('Venue Name', 'apollo'),
                    'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_REQUIRED
                        )
                    ),
                ),
                'post_content' => array(
                    'type' => 'Wysiwyg',
                    'name' => 'post_content',
                    'place_holder' => __('Description (*)', 'apollo'),
                    'value' => Apollo_App::convertTinyCMEToCkEditor(isset($arrData['post_content']) ? $arrData['post_content'] : ''),
                    'class' => 'inp-desc-event custom-validate-field',
                    'id' => 'venue-description',
                    'data_attributes' => array(
                        'data-custom-validate="wysiwyg"',
                        'data-error-message="'.__('Description is required','apollo').'"',
                        'data-id="venue-description"'
                    ),
                    'title' => __('Description (*)', 'apollo'),
                    'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_REQUIRED
                        )
                    ),

                ),


                //address
                array(
                    'type' => 'Div',
                    'name' => 'title',
                    'value' => '',
                    'class' => 'el-blk',
                    'container' => '',
                ),
                Apollo_DB_Schema::_VENUE_ADDRESS1 => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_VENUE_ADDRESS1,
                    'place_holder' => $requiredAddress ? __('Address 1 (*)', 'apollo') : __('Address 1', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_VENUE_ADDRESS1]) ? $arrData[Apollo_DB_Schema::_VENUE_ADDRESS1] : '',
                    'class' => sprintf('inp inp-txt %s', $requiredAddress ? $requiredClass : ''),
                    'data_attributes' => array(
                        'data-errormessage-value-missing="'.__('Address 1 is required','apollo').'"'
                    ),
                    'title' => $requiredAddress ? __('Address 1 (*)', 'apollo') : __('Address 1', 'apollo'),
                    'container' => 'hafl fl',
                    'validate' => array(
                        'rule' => array(
                            $requiredAddress ? Apollo_Form::_FORM_REQUIRED : ''
                        )
                    ),
                ),
                Apollo_DB_Schema::_VENUE_ADDRESS2 => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_VENUE_ADDRESS2,
                    'place_holder' => __('Address 2', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_VENUE_ADDRESS2]) ? $arrData[Apollo_DB_Schema::_VENUE_ADDRESS2] : '',
                    'class' => 'inp inp-txt',
                    'title' => __('Address 2', 'apollo'),
                    'container' => 'hafl fr',

                ),
                array(
                    'type' => 'CloseDiv',
                    'name' => 'title',
                    'value' => '',
                    'class' => '',
                    'container' => '',
                ),
                //end address

                Apollo_DB_Schema::_VENUE_STATE => array(
                    'type' => 'Select',
                    'name' => Apollo_DB_Schema::_VENUE_STATE,
                    'class' => sprintf('event apl-territory-state %s', $requiredState ? 'custom-validate-field' : ''),
                    'data_attributes' => array(
                        'data-custom-validate="select2"',
                        'data-error-message="'.__('State is required','apollo').'"'
                    ),
                    'title' => $requiredState ? __('State (*)', 'apollo') : __('State', 'apollo'),
                    'value' => array(
                        'list_item' => Apollo_App::getStateByTerritory(TRUE, false),
                        'selected_item' => $stateVal,
                    ),
                    'id' => 'apl-us-states',
                    'validate' => array(
                        'rule' => array(
                            $requiredState ? Apollo_Form::_FORM_REQUIRED : ''
                        )
                    ),
                ),


                //state - zip
                array(
                    'type' => 'Div',
                    'name' => 'title',
                    'value' => '',
                    'class' => 'el-blk',
                    'container' => '',
                ),
                Apollo_DB_Schema::_VENUE_CITY => array(
                    'type' => 'Select',
                    'name' => Apollo_DB_Schema::_VENUE_CITY,
                    'place_holder' => $requiredCity ? __('City (*)', 'apollo') : __('City', 'apollo'),
                    'id' => 'apl-us-cities',
                    'value' => array(
                        'list_item' => Apollo_App::getCityByTerritory(true, $stateVal, false),
                        'selected_item' => $cityVal
                    ),
                    'container' => 'hafl fl',
                    'class' => sprintf('event apl-territory-city %s', $requiredCity ? 'custom-validate-field' : ''),
                    'data_attributes' => array(
                        'data-custom-validate="select2"',
                        'data-error-message="'.__('City is required','apollo').'"'
                    ),
                    'title' => $requiredCity ? __('City (*)', 'apollo') : __('City', 'apollo'),
                    'validate' => array(
                        'rule' => array(
                            $requiredCity ? Apollo_Form::_FORM_REQUIRED : ''
                        )
                    ),
                ),

                Apollo_DB_Schema::_VENUE_ZIP => array(
                    'type' => 'Select',
                    'name' => Apollo_DB_Schema::_VENUE_ZIP,
                    'place_holder' => $requiredZip ? __('Zip (*)', 'apollo') : __('Zip', 'apollo'),
                    'value' => array(
                        'list_item' => Apollo_App::getZipByTerritory(true, $stateVal, $cityVal, false),
                        'selected_item' => isset($arrData[Apollo_DB_Schema::_VENUE_ZIP]) ? $arrData[Apollo_DB_Schema::_VENUE_ZIP] : ''
                    ),
                    'id' => 'apl-us-zip',
                    'class' => sprintf('event apl-territory-zipcode %s', $requiredZip ? 'custom-validate-field' : ''),
                    'data_attributes' => array(
                        'data-custom-validate="select2"',
                        'data-error-message="'.__('Zip is required','apollo').'"'
                    ),
                    'title' => $requiredZip ? __('Zip (*)', 'apollo') : __('Zip', 'apollo'),
                    'validate' => array(
                        'rule' => array(
                            $requiredZip ? Apollo_Form::_FORM_REQUIRED : ''
                        )
                    ),
                    'container' => 'hafl fr',
                ),
                array(
                    'type' => 'CloseDiv',
                    'name' => 'title',
                    'value' => '',
                    'class' => '',
                    'container' => '',

                ),
            //Neighborhood
            Apollo_DB_Schema::_VENUE_NEIGHBORHOOD => array(
                'type' => 'Select',
                'name' => Apollo_DB_Schema::_VENUE_NEIGHBORHOOD,
                'place_holder' => $requiredNeighborhood ? __('Select Neighborhood (*)', 'apollo') : __('Select Neighborhood', 'apollo'),
                'class' => sprintf('event apl-territory-neighborhood %s', $requiredNeighborhood ? 'custom-validate-field' : ''),
                'title' => $requiredNeighborhood ? __('Select Neighborhood (*)', 'apollo') : __('Select Neighborhood', 'apollo'),
                'value' => array(
                    'list_item' => $neighborhoodOptions,
                    'selected_item' => isset($arrData[Apollo_DB_Schema::_VENUE_NEIGHBORHOOD]) ? $arrData[Apollo_DB_Schema::_VENUE_NEIGHBORHOOD] : ''
                ),
                'id' => 'apl-neighborhood',
                'data_attributes' => array(
                    'data-custom-validate="select2"',
                    'data-error-message="'.__('Neighborhood is required','apollo').'"'
                ),
                'container' => 'el-blk full neighborhood-box ' . (empty($neighborhoodOptions) ? 'hidden' : ''),
            ),

            //end state - zip
            Apollo_DB_Schema::_VENUE_REGION => array(
                'type' => 'Select',
                'name' => Apollo_DB_Schema::_VENUE_REGION,
                'class' => 'event',
                'title' => __('Region ', 'apollo'),
                'value' => array(
                    'list_item' => Apollo_App::get_regions(false, false),
                    'selected_item' => isset($arrData[Apollo_DB_Schema::_VENUE_REGION]) ? $arrData[Apollo_DB_Schema::_VENUE_REGION] : ''
                ),
                'id' => 'apl-regions',
                'display' => Apollo_App::showRegion(),
                'container' => 'el-blk full '. (Apollo_App::enableMappingRegionZipSelection() ? ' hidden' : ''),
            ),

            Apollo_DB_Schema::_VENUE_PHONE => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_VENUE_PHONE,
                    'place_holder' => $requiredPhone ? __('Phone (*)', 'apollo') : __('Phone', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_VENUE_PHONE]) ? $arrData[Apollo_DB_Schema::_VENUE_PHONE] : '',
                    'class' => sprintf('inp inp-txt %s', $requiredPhone ? $requiredClass : ''),
                    'title' => $requiredPhone ? __('Phone (*)', 'apollo') : __('Phone', 'apollo'),

                ),
                Apollo_DB_Schema::_VENUE_FAX => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_VENUE_FAX,
                    'place_holder' => __('Fax', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_VENUE_FAX]) ? $arrData[Apollo_DB_Schema::_VENUE_FAX] : '',
                    'class' => 'inp inp-txt',
                    'title' => __('Fax', 'apollo'),
                ),
                Apollo_DB_Schema::_VENUE_EMAIL => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_VENUE_EMAIL,
                    'place_holder' => $requiredEmail ? __('Email (*)', 'apollo') : __('Email', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_VENUE_EMAIL]) ? $arrData[Apollo_DB_Schema::_VENUE_EMAIL] : '',
                    'class' => sprintf('inp inp-txt %s validate[funcCall[disableSpace], custom[email]]', $requiredEmail ? $requiredClass : ''),
                    'title' => __('Email', 'apollo'),
                    'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_EMAIL,
                            $requiredEmail ? Apollo_Form::_FORM_REQUIRED : ''
                        )
                    ),
                ),

                Apollo_DB_Schema::_VENUE_PARKING_INFO => array(
                    'type' => 'Wysiwyg',
                    'name' =>  Apollo_DB_Schema::_VENUE_PARKING_INFO,
                    'place_holder' => __('Parking Info (*)', 'apollo'),
                    'value' => Apollo_App::convertTinyCMEToCkEditor(isset($arrData[ Apollo_DB_Schema::_VENUE_PARKING_INFO]) ? $arrData[ Apollo_DB_Schema::_VENUE_PARKING_INFO] : ''),
                    'class' => 'inp inp-txt inp-desc custom-validate-field',
                    'data_attributes' => array(
                        'data-custom-validate="wysiwyg"',
                        'data-error-message="'.__('Parking Info is required','apollo').'"',
                        'data-id="'.Apollo_DB_Schema::_VENUE_PARKING_INFO.'"'
                    ),
                    'title' => __('Parking Info (*)', 'apollo'),
                    'id' => 'parking-info',
                    'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_REQUIRED
                        )
                    ),
                ),
                Apollo_DB_Schema::_VENUE_PUBLIC_HOURS => array(
                    'type' => 'Wysiwyg',
                    'name' =>  Apollo_DB_Schema::_VENUE_PUBLIC_HOURS,
                    'place_holder' => __('Public Hours (if applicable)', 'apollo'),
                    'value' => Apollo_App::convertTinyCMEToCkEditor(isset($arrData[ Apollo_DB_Schema::_VENUE_PUBLIC_HOURS]) ? $arrData[ Apollo_DB_Schema::_VENUE_PUBLIC_HOURS] : ''),
                    'class' => 'inp-desc-event inp-desc',
                    'title' => __('Public Hours (if applicable)', 'apollo'),
                    'id' => 'public-hours'
                ),
                Apollo_DB_Schema::_VENUE_PUBLIC_ADMISSION_FEEDS => array(
                    'type' => 'Wysiwyg',
                    'name' =>  Apollo_DB_Schema::_VENUE_PUBLIC_ADMISSION_FEEDS,
                    'place_holder' => __('Public Admission Fee(s) (if applicable)', 'apollo'),
                    'value' => Apollo_App::convertTinyCMEToCkEditor(isset($arrData[ Apollo_DB_Schema::_VENUE_PUBLIC_ADMISSION_FEEDS]) ? $arrData[ Apollo_DB_Schema::_VENUE_PUBLIC_ADMISSION_FEEDS] : ''),
                    'class' => 'inp-desc-event inp-desc',
                    'title' => __('Public Admission Fee(s) (if applicable)', 'apollo'),
                    'id' => 'public-admission-fee'
                ),
            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),


            //Group 2: website
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),
            //onwer website
            //title
            array(
                'type' => 'Title',
                'name' => 'title',
                'value' => __('Website, Blog and Social Media Links', 'apollo'),
                'class' => '',

            ),
            //end title
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'el-blk',
                'container' => '',
            ),
            Apollo_DB_Schema::_VENUE_WEBSITE_URL => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_VENUE_WEBSITE_URL,
                'place_holder' => __('Website URL', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_VENUE_WEBSITE_URL]) ? $arrData[Apollo_DB_Schema::_VENUE_WEBSITE_URL] : '',
                'container' => 'hafl fl',
                'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]]',
                'title' => __('Website URL', 'apollo'),
                'validate' => array(
                    'rule' => array(
                        Apollo_Form::_FORM_URL,

                    )
                )
            ),
            Apollo_DB_Schema::_VENUE_PUBLIC_BLOG_URL => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_VENUE_PUBLIC_BLOG_URL,
                'place_holder' => __('Blog URL', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_VENUE_PUBLIC_BLOG_URL]) ? $arrData[Apollo_DB_Schema::_VENUE_PUBLIC_BLOG_URL] : '',
                'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]]',
                'title' => __('Blog URL', 'apollo'),
                'container' => 'hafl fr',
                'validate' => array(
                    'rule' => array(
                        Apollo_Form::_FORM_URL,

                    )
                )
            ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'el-blk',
                'container' => '',
            ),
            Apollo_DB_Schema::_VENUE_INSTAGRAM_URL => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_VENUE_INSTAGRAM_URL,
                'place_holder' => __('Instagram URL', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_VENUE_INSTAGRAM_URL]) ? $arrData[Apollo_DB_Schema::_VENUE_INSTAGRAM_URL] : '',
                'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]]',
                'title' => __('Instagram URL', 'apollo'),
                'container' => 'hafl fl',
                'validate' => array(
                    'rule' => array(
                        Apollo_Form::_FORM_URL,

                    )
                )
            ),
            Apollo_DB_Schema::_VENUE_PINTEREST_URL => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_VENUE_PINTEREST_URL,
                'place_holder' => __('Pinterest URL', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_VENUE_PINTEREST_URL]) ? $arrData[Apollo_DB_Schema::_VENUE_PINTEREST_URL] : '',
                'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]]',
                'title' => __('Pinterest URL', 'apollo'),
                'container' => 'hafl fr',
                'validate' => array(
                    'rule' => array(
                        Apollo_Form::_FORM_URL,

                    )
                )
            ),
            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'el-blk',
                'container' => '',
            ),
            Apollo_DB_Schema::_VENUE_FACEBOOK_URL => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_VENUE_FACEBOOK_URL,
                'place_holder' => __('Facebook URL', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_VENUE_FACEBOOK_URL]) ? $arrData[Apollo_DB_Schema::_VENUE_FACEBOOK_URL] : '',
                'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]]',
                'title' => __('Facebook URL', 'apollo'),
                'container' => 'hafl fl',
                'validate' => array(
                    'rule' => array(
                        Apollo_Form::_FORM_URL,

                    )
                )
            ),
            Apollo_DB_Schema::_VENUE_TWITTER_URL => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_VENUE_TWITTER_URL,
                'place_holder' => __('Twitter URL', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_VENUE_TWITTER_URL]) ?
                    $arrData[Apollo_DB_Schema::_VENUE_TWITTER_URL] : '',
                'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]]',
                'title' => __('Twitter URL', 'apollo'),
                'container' => 'hafl fr',
                'validate' => array(
                    'rule' => array(
                        Apollo_Form::_FORM_URL,

                    )
                )
            ),
            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'el-blk',
                'container' => '',
            ),
            Apollo_DB_Schema::_VENUE_LINKEDIN_URL => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_VENUE_LINKEDIN_URL,
                'place_holder' => __('LinkedIn URL', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_VENUE_LINKEDIN_URL]) ? $arrData[Apollo_DB_Schema::_VENUE_LINKEDIN_URL] : '',
                'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]]',
                'title' => __('LinkedIn URL', 'apollo'),
                'container' => 'hafl fl',
                'validate' => array(
                    'rule' => array(
                        Apollo_Form::_FORM_URL,

                    )
                )
            ),
            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),
            array(
                    'type' => 'CloseDiv',
                    'name' => 'title',
                    'value' => '',
                    'class' => '',
                    'container' => '',
                ),

            //group3: contact information
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),
                //title
                array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => __('Contact Info', 'apollo'),
                    'class' => ''
                ),
                //end title
                Apollo_DB_Schema::_VENUE_CONTACT_NAME => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_VENUE_CONTACT_NAME,
                    'place_holder' => __('Contact name (*)', 'apollo'),
                    'value' => trim($contactName),
                    'class' => 'inp inp-txt validate[required]',
                    'data_attributes' => array(
                        'data-errormessage-value-missing="'.__('Contact name is required','apollo').'"'
                    ),
                    'title' => __('Contact name', 'apollo'),
                    'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_REQUIRED
                        )
                    ),
                ),
                Apollo_DB_Schema::_VENUE_CONTACT_EMAIL => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_VENUE_CONTACT_EMAIL,
                    'place_holder' => __('Contact email (*)', 'apollo'),
                    'value' => $contactEmail,
                    'class' => 'inp inp-txt validate[required,funcCall[disableSpace],custom[email]]',
                    'data_attributes' => array(
                        'data-errormessage-value-missing="'.__('Contact email is required','apollo').'"'
                    ),
                    'title' => __('Contact email', 'apollo'),
                    'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_REQUIRED,
                            Apollo_Form::_FORM_EMAIL
                        )
                    ),
                ),
                Apollo_DB_Schema::_VENUE_CONTACT_PHONE => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_VENUE_CONTACT_PHONE,
                    'place_holder' => __('Contact phone (*)', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_VENUE_CONTACT_PHONE]) ? $arrData[Apollo_DB_Schema::_VENUE_CONTACT_PHONE] : '',
                    'class' => 'inp inp-txt validate[required]',
                    'data_attributes' => array(
                        'data-errormessage-value-missing="'.__('Contact phone is required','apollo').'"'
                    ),
                    'title' => __('Contact phone', 'apollo'),
                    'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_REQUIRED
                        )
                    ),
                ),
            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            //group4: accessibility
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),
                //title
                array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => __('ACCESSIBILITY INFORMATION', 'apollo'),
                    'class' => ''
                ),
                Apollo_DB_Schema::_VENUE_ACCESSIBILITY => array(
                    'type' => 'Access_Group',
                    'name' => Apollo_DB_Schema::_VENUE_ACCESSIBILITY,
                    'class' => 'event',
                    'value' => array(
                        'list_item' =>   $apollo_event_access,
                        'selected_item' => isset($arrData) ? $arrData : '',
                    ),
                    'post_type' => Apollo_DB_Schema::_VENUE_PT,
                    'group_name' => Apollo_DB_Schema::_APL_VENUE_DATA
                ),
                array(
                    'type' => 'CloseDiv',
                    'name' => 'title',
                    'value' => '',
                    'class' => '',
                    'container' => '',
                ),


                //group5: venue type
                array(
                    'type' => 'Div',
                    'name' => 'title',
                    'value' => '',
                    'class' => 'artist-blk',
                    'container' => '',
                ),
                array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => __('VENUE TYPE', 'apollo'),
                    'class' => 'margin-bottom-10'
                ),
                //end title
                Apollo_DB_Schema::_VENUE_TYPES => array(
                    'title' => __('Venue types', 'apollo'),
                    'type' => 'Category_Group',
                    'name' => Apollo_DB_Schema::_VENUE_TYPES,
                    'class' => 'event',
                    'value' => isset($arrData[Apollo_DB_Schema::_VENUE_TYPES]) ? $arrData[Apollo_DB_Schema::_VENUE_TYPES] : '',
                    'post_type' => Apollo_DB_Schema::_VENUE_PT,
                ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

            'Additional_Form' => array(
                'type' => 'Additional_fields',
                'name' => '',
                'class' => 'full-custom',
                'title' => '',
                'id' => '',
                'container' => 'artist-blk',
                'place_holder' => __('Contact name', 'apollo'),
                'value' => array(
                    'post_type' => Apollo_DB_Schema::_VENUE_PT,
                    'post_data' => $arrData,
                    'post_id' => $arrData['ID']
                )
            ),


            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            //submit button
            'Submit_btn' => array(
                'type' => 'Submit',
                'container' => 'artist-blk',
                'class' => 'submit-btn submit-form-with-validation-engine',
                'title' => __('UPDATE PROFILE', 'apollo')
            ),
        );
        $this->elements = $formElementItems;
    }

    public function formHeader()
    {
        ?>
        <div class="dsb-welc custom">
            <h1 ><?php _e('Venue Information', 'apollo') ?>
                <?php
                $id = $this->id ? $this->id : $this->formData['ID'];
                if ($id && isset($this->formData['post_status']) &&
                    ($this->formData['post_status'] == 'publish' || $this->formData['post_status'] == 'pending') && $this->formData
                ): ?>
                    <a class="view-page-link" target="_blank"
                       href="<?php echo get_the_permalink($id) ?>">(<?php _e('View Page', 'apollo') ?>
                        )</a>
                <?php endif; ?>
            </h1>
            <?php
                $this->successMessage();
            ?>
        </div>

        <?php
            if ( isset($_GET['warning']) ):
        ?>
        <div><span class="error"><?php _e('Please enter your Venue profile first !', 'apollo') ?></span></div>
        <?php endif; ?>

        <?php
        parent::formHeader();
        /* Thienld: handle show sucess message when saved successfully */
        if(Apollo_App::isInputMultiplePostMode()){
            if(isset($_SESSION['saved-post-success'])){
                $this->successMessage = __(Apollo_Form_Static::_SUCCESS_MESSAGE, 'apollo');
                parent::formSubmitSuccess();
                unset($_SESSION['saved-post-success']);
            }
        }
    }

    /*Process form submit*/
    public function postMethod()
    {
        // validate nonce
        if ( $this->validateNonce() ) {
            return $this->validateClassObject->save($this->formData);
        }
    }

    /*process form get method*/
    public function getMethod()
    {
        $this->successMessage = __(Apollo_Form_Static::_SUCCESS_MESSAGE, 'apollo');

        $id = Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_VENUE_PT);

        if(!empty($id)) {
            if(get_post_status($id) == 'publish' ||  get_post_status($id) == 'pending'){
                $postInfo = get_post($id, ARRAY_A);
                $status = $postInfo['post_status'];
                $id = $postInfo['ID'];
                //get all meta;
                $metaAddr = get_apollo_meta($id, Apollo_DB_Schema::_APL_VENUE_ADDRESS, true);
                $metaData = get_apollo_meta($id, Apollo_DB_Schema::_APL_VENUE_DATA, true);

                if (!is_array($metaAddr) && !empty($metaAddr))
                    $metaAddr = unserialize($metaAddr);
                if (!is_array($metaData) && !empty($metaData))
                    $metaData = unserialize($metaData);

                if (is_array($metaAddr)) {
                    foreach ($metaAddr as $key => $value) {
                        $postInfo[$key] = $value;
                    }
                }
                if (is_array($metaData)) {
                    foreach ($metaData as $key => $value) {
                        $postInfo[$key] = $value;
                    }
                }

                $postInfo[Apollo_DB_Schema::_VENUE_TYPES] = wp_get_post_terms($id, Apollo_DB_Schema::_VENUE_PT . '-type', array(
                    'fields' => 'ids'
                ));
                $postInfo['ID'] = $id;
                $this->formData = $postInfo;
                $this->successMessage = Apollo_Form_Static::_SUCCESS_MESSAGE;
            }
        }
    }
    //Show message Fail Submit

//override parent for additional field
    public function formSubmitAction()
    {
        // Handle submit form
        do_action('apollo_submit_form');

        $addFieldsValidateClass = new Apollo_Submit_Form(Apollo_DB_Schema::_VENUE_PT, array());
        $this->isSaveSuccess = false;
        $this->isSubmitFail = false;
        //post method

        if ($this->formRequestMethod == 'post') {
            $this->cleanPost();

            $this->isSubmitFail = true;
            if ($this->validateClassObject->isValidAll($this->mergerPostRule())) {
                $this->isSaveSuccess = true;
                $this->isSubmitFail = false;
            }
            if (!$addFieldsValidateClass->isValidAll($this->mergePostAFields())) {
                $this->isSaveSuccess = false;
                $this->isSubmitFail = true;
            }
            //save data
            if ($this->isSaveSuccess){
                $this->id = $this->postMethod();
            }

            if(Apollo_App::isInputMultiplePostMode()){
                // Thienld : handle for case submit post of inputting multiple post
                // Take user to the edit post page on dashboard when saved data success (add new / edit action)
                if($this->isSaveSuccess){
                    $_SESSION['saved-post-success'] = 1;
                    $redirectUrl = APL_Dashboard_Hor_Tab_Options::VENUE_PROFILE_URL;
                    $isAgencyArea = intval(get_query_var('_is_agency_educator_page',0)) === 1;
                    if($isAgencyArea){
                        $redirectUrl = APL_Dashboard_Hor_Tab_Options::AGENCY_VENUE_PROFILE_URL;
                    }
                    wp_redirect(site_url($redirectUrl) . '/' .$this->id);
                    exit();
                }
            }
            //end save date
        }
    }

    public function mergePostAFields()
    {
        $data = array();
        $group_fields = Apollo_Custom_Field::get_group_fields(Apollo_DB_Schema::_VENUE_PT);
        foreach ($group_fields as $group) {
            $fields = isset($group['fields']) ? $group['fields'] : array();
            if (count($fields) > 0) {
                foreach ($fields as $field) {
                    $data[$field->name] = isset($_POST[$field->name]) ? $_POST[$field->name] : '';
                }
            }
        }
        return $data;
    }

    public function formSubmitProcess(){
        $this->formSubmitAction();
        $this->formHeader();
    }

    // Thienld : These functions apply for mode input multiple post
    public function setCurDataHorizontalTab(){
        $this->activatedTab = APL_Dashboard_Hor_Tab_Options::VENUE_PROFILE_URL;
        $isAgencyArea = intval(get_query_var('_is_agency_educator_page',0)) === 1;
        if($isAgencyArea){
            $this->activatedTab = APL_Dashboard_Hor_Tab_Options::AGENCY_VENUE_PROFILE_URL;
        }
    }

    public function getTabData(){
        $tabData = array(
            'profile' => APL_Dashboard_Hor_Tab_Options::VENUE_PROFILE_URL,
            'photo' => APL_Dashboard_Hor_Tab_Options::VENUE_PHOTOS_URL,
            'video' => APL_Dashboard_Hor_Tab_Options::VENUE_VIDEO_URL
        );
        $isAgencyArea = intval(get_query_var('_is_agency_educator_page',0)) === 1;
        if($isAgencyArea){
            $tabData = array(
                'profile' => APL_Dashboard_Hor_Tab_Options::AGENCY_VENUE_PROFILE_URL,
                'photo' => APL_Dashboard_Hor_Tab_Options::AGENCY_VENUE_PHOTOS_URL,
                'video' => APL_Dashboard_Hor_Tab_Options::AGENCY_VENUE_VIDEO_URL
            );
        }
        return $tabData;
    }

    public function getTabLinks(){
        $tabData = $this->getTabData();
        $tabLinks = array(
            $tabData['profile'] => __("PROFILE","apollo"),
            $tabData['photo'] => __("PHOTO","apollo"),
            $tabData['video'] => __("VIDEO","apollo")
        );
        return $tabLinks;
    }

    /**
     * Set nonce info
     *
     * @return void
     */
    public function setNonceInfo()
    {
        $this->nonceName   = Apollo_Const::_APL_NONCE_NAME;
        $this->nonceAction = Apollo_Const::_APL_NONCE_ACTION_VENUE_PROFILE_PAGE;
    }
}