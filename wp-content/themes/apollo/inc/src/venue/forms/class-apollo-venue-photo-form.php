<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/25/2015
 * Time: 4:47 PM
 */

class Apollo_Venue_Photo_Form extends Apollo_Venue_Base_Form{
    public function formInit(){

        $arrData = $this->formData;
        $formElementItems = array(
            'ID' =>  array(
                'type' => 'Hidden',
                'name' => 'ID',
                'place_holder' =>__('Id','apollo'),
                'value' =>  isset($arrData['ID'])?$arrData['ID']:'',
                'class' => 'inp inp-txt',
                'title' =>__('Name','apollo'),
                'validate' => true,
                'id' => 'venue_id',
                'container' => 'ebl'
            ),
            'target' =>  array(
                'type' => 'Hidden',
                'name' => 'ID',
                'place_holder' =>__('target','apollo'),
                'value' =>  Apollo_DB_Schema::_VENUE_PT,
                'class' => 'inp inp-txt',
                'title' =>__('Name','apollo'),
                'validate' => true,
                'id' => 'venue_target',
                'container' => 'ebl'
            ),
            'photo_tab' => array(
                'type' => 'Tab',
                'place_holder' =>__('Name','apollo'),
                'value' =>  $arrData['ID'],
                'container' => 'el-blk',
                'children' => array(
                    'primary_photo' => array(
                        'type' => 'ShortCode',
                        'name' => 'primary_photo',
                        'title' => __('PRIMARY IMAGE','apollo'),
                        'target' => Apollo_DB_Schema::_VENUE_PT,
                        'value' =>  'apollo_upload_and_drop ',
                    ),
                    'gallery_photo' => array(
                        'type' => 'ShortCode',
                        'name' => 'gallery_photo',
                        'title' => __('GALLERY','apollo'),
                        'target' => Apollo_DB_Schema::_VENUE_PT,
                        'value' =>  'apollo_upload_gallery ',
                    ),
                )
            ),
            'Submit_btn' => array(
                'type' => 'Button',
                'class' => 'submit-btn',
                'id' => 'apollo_submit_venue_photo',
                'title' => __('SUBMIT PHOTO','apollo')
            )
        );
        $this->elements = $formElementItems;
    }
    public function getMethod(){
        $id = Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_VENUE_PT);
        if(!empty($id)) {
            $this->formData['ID'] = $id;
        }
    }

    public function postMethod(){
    }

    //Show message Success Submit


    // Thienld : These functions apply for mode input multiple post
    public function setCurDataHorizontalTab(){
        $this->activatedTab = APL_Dashboard_Hor_Tab_Options::VENUE_PHOTOS_URL;
        $isAgencyArea = intval(get_query_var('_is_agency_educator_page',0)) === 1;
        if($isAgencyArea){
            $this->activatedTab = APL_Dashboard_Hor_Tab_Options::AGENCY_VENUE_PHOTOS_URL;
        }
    }

    public function getTabData(){
        $tabData = array(
            'profile' => APL_Dashboard_Hor_Tab_Options::VENUE_PROFILE_URL,
            'photo' => APL_Dashboard_Hor_Tab_Options::VENUE_PHOTOS_URL,
            'video' => APL_Dashboard_Hor_Tab_Options::VENUE_VIDEO_URL
        );
        $isAgencyArea = intval(get_query_var('_is_agency_educator_page',0)) === 1;
        if($isAgencyArea){
            $tabData = array(
                'profile' => APL_Dashboard_Hor_Tab_Options::AGENCY_VENUE_PROFILE_URL,
                'photo' => APL_Dashboard_Hor_Tab_Options::AGENCY_VENUE_PHOTOS_URL,
                'video' => APL_Dashboard_Hor_Tab_Options::AGENCY_VENUE_VIDEO_URL
            );
        }
        return $tabData;
    }

    public function getTabLinks(){
        $tabData = $this->getTabData();
        $tabLinks = array(
            $tabData['profile'] => __("PROFILE","apollo"),
            $tabData['photo'] => __("PHOTO","apollo"),
            $tabData['video'] => __("VIDEO","apollo")
        );
        return $tabLinks;
    }

    /**
     * Set nonce info
     *
     * @return void
     */
    public function setNonceInfo()
    {
        $this->nonceName   = Apollo_Const::_APL_NONCE_NAME;
        $this->nonceAction = Apollo_Const::_APL_NONCE_ACTION_VENUE_PHOTO_PAGE;
    }
}