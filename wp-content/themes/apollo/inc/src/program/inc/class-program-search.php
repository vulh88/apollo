<?php
require_once APOLLO_TEMPLATES_DIR. '/pages/lib/class-apollo-page-module.php';

class Apollo_Program_Page extends Apollo_Page_Module {

    public function __construct($isCounting = false) {

        /* @ticket #15646 */
        self::setIsCounting($isCounting);

        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EDUCATION ) ) {
            wp_safe_redirect( '/' );
        }
        
        parent::__construct();
    }

    public function search() {

        $arr_params = array(
            'post_type'         => Apollo_DB_Schema::_PROGRAM_PT,
            'posts_per_page'    => self::getIsCounting() ? 1 : Apollo_App::aplGetPageSize($this->pagesize , Apollo_DB_Schema::_PROGRAM_NUM_ITEMS_LISTING_PAGE),
            'paged'             => $this->page,
            'post_status'       => array('publish'),
        );

        if($keyword = self::getKeyword()) {
            $arr_params['s'] =  $keyword;
        }
        
        $arr_tax_query = array();

        if(isset($_GET['term']) && !empty($_GET['term'])) {
            global $taxonomy;
            $arr_tax_query[] = array(
                'taxonomy'=> $taxonomy,
                'terms' => array($_GET['term']),
            );

        }

        if ( isset($_GET['program_type']) && !empty($_GET['program_type']) ) {
            $arr_tax_query[] = array(
                'taxonomy'  => 'program-type',
                'terms'     => array(Apollo_App::clean_data_request($_GET['program_type'])),
            );
        }
       
        if ( isset($_GET['artistic_discipline']) && !empty($_GET['artistic_discipline']) ) {
            $arr_tax_query[] = array(
                'taxonomy'  => Apollo_DB_Schema::_APL_PROG_TAX_ART_DESC,
                'terms'     => array(Apollo_App::clean_data_request($_GET['artistic_discipline'])),
            );
        }
        
        if ( isset($_GET['cultural_origin']) && !empty($_GET['cultural_origin']) ) {
            $arr_tax_query[] = array(
                'taxonomy'  => Apollo_DB_Schema::_APL_PROG_TAX_CUL_ORIGIN,
                'terms'     => array(Apollo_App::clean_data_request($_GET['cultural_origin'])),
            );
        }
        
        if ( isset($_GET['_subject']) && !empty($_GET['_subject']) ) {
            $arr_tax_query[] = array(
                'taxonomy'  => Apollo_DB_Schema::_APL_PROG_TAX_SUBJECT,
                'terms'     => array(Apollo_App::clean_data_request($_GET['_subject'])),
            );
        }
        
        if ( isset($_GET['population_served']) && !empty($_GET['population_served']) ) {
            $arr_tax_query[] = array(
                'taxonomy'  => Apollo_DB_Schema::_APL_PROG_TAX_POP_SER,
                'terms'     => array(Apollo_App::clean_data_request($_GET['population_served'])),
            );
        }
        
        $arr_params['tax_query'] = $arr_tax_query;

        // Set new offset if this is ajax action
        $this->addOffsetToParams($arr_params);

        add_filter( 'posts_where', array($this, 'filter_where'), 10, 1 );
        add_filter( 'posts_join', array($this, 'filter_join'), 10, 1 );
        add_filter( 'posts_orderby', array($this, 'filter_orderby'), 10, 1 );
        add_filter( 'posts_groupby', array($this, 'filter_groupby'), 10, 1 );
        add_filter( 'posts_search', array(__CLASS__, 'posts_search'), 10, 1 );
        add_filter( 'posts_groupby', array($this, 'filter_groupby'), 10, 1 );
        
        $this->result = query_posts($arr_params);

        Apollo_Next_Prev::updateSearchResult($GLOBALS['wp_query']->request,Apollo_DB_Schema::_PROGRAM_PT);
        remove_filter( 'posts_where', array($this, 'filter_where'), 10, 1 );
        remove_filter( 'posts_join', array($this, 'filter_join'), 10, 1 );
        remove_filter( 'posts_orderby', array($this, 'filter_orderby'), 10, 1 );
        remove_filter( 'posts_groupby', array($this, 'filter_groupby'), 10, 1 );
        remove_filter( 'posts_search', array(__CLASS__, 'posts_search'), 10, 1);
        remove_filter( 'posts_groupby', array($this, 'filter_groupby'), 10, 1);

        $this->setTotal($GLOBALS['wp_query']->found_posts);
        $this->total_pages = ceil($this->getTotal() / $this->pagesize);

        $this->template = APOLLO_TEMPLATES_DIR. '/program/listing-page/'. $this->_get_template_name(of_get_option(Apollo_DB_Schema::_PROGRAMS_DEFAULT_VIEW_TYPE));

        $this->resetPostData();
    }
    
    function filter_groupby($groupby) {
        global $wpdb;
        return "$wpdb->posts.ID";
    }
    
    function filter_where( $where ) {
        global $wpdb;

        /*quocpt - additional condition to get program is not expired*/
        $currentDay = current_time('Y-m-d');
        $where .= " AND ( (CAST('$currentDay' AS DATE) <= CAST(mt_exd.meta_value AS DATE) OR mt_exd.meta_value IS NULL OR mt_exd.meta_value = ''  )) ";

        if ( isset($_GET['sd']) && !empty( $_GET['sd'] )) {
            $startd = Apollo_App::clean_data_request(  $_GET['sd'] );
            $where .= " AND CAST(mt_sd.meta_value AS DATE) >= '$startd' ";
        }
        if ( isset($_GET['ed']) && !empty( $_GET['ed'] )) {
            $endd = Apollo_App::clean_data_request(  $_GET['ed'] );
            $where .= " AND CAST(mt_sd.meta_value AS DATE) <= '$endd'";
        }
        if ( isset($_GET['bil']) && !empty($_GET['bil']) ) {
            $where .= " AND mt_bil.meta_value = '".Apollo_App::clean_data_request( $_GET['bil'] )."'";
        }
        if ( isset($_GET['educator_type']) && !empty( $_GET['educator_type'] )) {
            $where .= " AND " . $wpdb->posts . ".ID  IN ( SELECT prog_id from ".$wpdb->{Apollo_Tables::_APL_PROGRAM_EDUCATOR}." WHERE edu_id
            IN ( SELECT object_id from ".$wpdb->{Apollo_Tables::_APOLLO_TERM_RELATIONSHIPS}." WHERE term_taxonomy_id
             IN (SELECT term_taxonomy_id FROM ".$wpdb->{Apollo_Tables::_APOLLO_TERM_TAXONOMY}." WHERE term_id = '".Apollo_App::clean_data_request($_GET['educator_type']) ."'))) ";
        }
        
        $_arr_cf_search = $this->get_custom_search_fields();
        $_cf_query = array();
        if ( $_arr_cf_search ) {
            foreach ( $_arr_cf_search as $key ) {
                
                if ( ! isset( $_GET[$key] ) || ! $_GET[$key] ) continue;
                $_cf_query[] = "mt_edu_cf.meta_value LIKE '%".$key."%'";
                
            }
            if ( $_cf_query ) {
                $where .= " AND ( ".  implode(' AND ', $_cf_query)." )";
            }
        }
        return $where;
    }
    
    function get_custom_search_fields() {
        /** @Ticket #13482 */
        $custom_search_fields = Apollo_Custom_Field::get_field_by_location( Apollo_DB_Schema::_EDUCATOR_PT );
        $_arr_key_cf_query = array();
        if ( !empty($custom_search_fields) ) {
            foreach ( $custom_search_fields as $fields ) {
                $location = maybe_unserialize($fields->location);
                if ( in_array( 'rsb', $location) || in_array( 'af_ff_dp_rsb', $location)) {
                    $_arr_key_cf_query[] = $fields->name;
                }
            }
        }
        return $_arr_key_cf_query;
    }
    
    function filter_join( $join ) {
        
        global $wpdb;
        $meta_tbl = $wpdb->{Apollo_Tables::_APL_PROGRAM_META};
        $meta_edu_tbl = $wpdb->{Apollo_Tables::_APL_EDUCATOR_META};
        $post_tbl = $wpdb->posts;
        $prog_edu_tbl = $wpdb->{Apollo_Tables::_APL_PROGRAM_EDUCATOR};

        /*quocpt - additional condition to get program is not expired*/
        $join .= " LEFT JOIN $meta_tbl AS mt_exd ON mt_exd.apollo_program_id = $post_tbl.ID AND mt_exd.meta_key = '".Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND."'";

        if ( isset($_GET['sd']) && !empty( $_GET['sd'] )) {
            $join .= " INNER JOIN $meta_tbl AS mt_sd ON mt_sd.apollo_program_id = $post_tbl.ID AND mt_sd.meta_key = '".Apollo_DB_Schema::_APL_PROGRAM_STARTD."'";
        }
        if ( isset($_GET['bil']) && !empty($_GET['bil']) ) {
            $join .= " INNER JOIN $meta_tbl AS mt_bil ON mt_bil.apollo_program_id = $post_tbl.ID AND mt_bil.meta_key = '".Apollo_DB_Schema::_APL_PROGRAM_IS_BILINGUAL."'";
        }

        /** @Ticket #12788 */
        if ( isset($_GET['free']) && !empty($_GET['free']) ) {
            $join .= " INNER JOIN $meta_tbl AS mt_free ON mt_free.apollo_program_id = $post_tbl.ID AND mt_free.meta_key = '".Apollo_DB_Schema::_APL_PROGRAM_IS_FREE."' AND mt_free.meta_value = 'yes'";
        }

        if ( isset( $_GET['edu'] ) && $_GET['edu'] ) {
            $join .= " INNER JOIN $prog_edu_tbl AS prog_edu ON prog_edu.prog_id = $post_tbl.ID AND edu_id = ".Apollo_App::clean_data_request( $_GET['edu'] )." ";
        } else {
            $join .= " LEFT JOIN $prog_edu_tbl AS prog_edu ON prog_edu.prog_id = $post_tbl.ID";
        }

        /** @Ticket #13482 */
        $join .= " LEFT JOIN $meta_edu_tbl AS mt_edu_cf ON prog_edu.edu_id = mt_edu_cf.apollo_educator_id AND mt_edu_cf.meta_key = '".Apollo_DB_Schema::_APL_POST_TYPE_CF_SEARCH."'";
        
        $join .= self::getJoinTaxByKeyword();
        
        return $join;
    }
    
    function filter_orderby( $orderby ) {
        // Ignore sort the result when counting
        if (self::getIsCounting()) {
            return false;
        }
        return $orderby;
    }

    public static function posts_search($p) {
        $keyword = self::getKeyword();

        if ( ! $keyword ) return $p;

        $termSearch = self::getWhereTaxByKeyword($p);

        /** @Ticket #12796 */
        global $wpdb;
        $pro_ids = self::_getIDsByEductionMeta($keyword);

        if($pro_ids){

            $educatorQuery = " OR $wpdb->posts.ID IN (" . implode(",", $pro_ids) . ") ";
        } else {
            $educatorQuery = '';
        }
        $searchTitle = self::replaceSpecialCharacter("{$wpdb->posts}.post_title", self::$specialString, $keyword);
        return str_replace('AND (((', " AND ( $termSearch $searchTitle OR ((( ", $p) . $educatorQuery . ') ';
    }

    /**
     * @Ticket #12796
     * Get ids with Education Title, Description, Excerpt
     * @param $keyword
     * @return array
     */
    private static function _getIDsByEductionMeta($keyword){
        global $wpdb;
        $keyword = '%'. $keyword . '%';
        $meta_tbl = $wpdb->{Apollo_Tables::_APL_PROGRAM_META};
        $sql = $wpdb->prepare("SELECT pm.apollo_program_id
                                FROM $wpdb->posts AS p 
                                INNER JOIN $meta_tbl AS pm ON pm.meta_key = '%s' AND pm.meta_value = p.ID
                                WHERE (post_title LIKE '%s' OR post_excerpt LIKE '%s' OR post_content LIKE '%s') AND post_type = %s AND post_status = 'publish'", Apollo_DB_Schema::_PROGRAM_EDU_ID, $keyword, $keyword, $keyword, Apollo_DB_Schema::_EDUCATOR_PT);

        $result = $wpdb->get_col($sql);

        return empty($result) ? array() : $result;

    }

}
