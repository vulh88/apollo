<?php

class Apollo_Submit_Program extends Apollo_Submit_Form {
    private $id = '';
    public $nonceName   = '';
    public $nonceAction = '';
    
    public function __construct() {
        
        $post_type         = Apollo_DB_Schema::_PROGRAM_PT;
        $this->nonceName   = Apollo_Const::_APL_EDUCATION_NONCE_NAME;
        $this->nonceAction = Apollo_Const::_APL_NONCE_ACTION_EDUCATION_ADD_PROGRAM_PAGE;
        
        $rules = array(
            'id'        => array(),
            'post_title' => array('required'),
            'post_content' => array('required'),
            'post_status' => array('required'),
            Apollo_DB_Schema::_APL_PROGRAM_CNAME   => array('required'),
            Apollo_DB_Schema::_APL_PROGRAM_PHONE => array(),
            Apollo_DB_Schema::_APL_PROGRAM_EMAIL  => array('email'),
            Apollo_DB_Schema::_APL_PROGRAM_URL    => array('url'),

            Apollo_DB_Schema::_APL_PROGRAM_STARTD    => array(),
            Apollo_DB_Schema::_APL_PROGRAM_ENDD    => array( 'endateLargerThanStartdate:' . Apollo_DB_Schema::_APL_PROGRAM_STARTD),
            Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND    => array(),
            Apollo_DB_Schema::_APL_PROGRAM_CANCEL_POLICY => array(),
            Apollo_DB_Schema::_APL_PROGRAM_REFERENCES => array(),

            Apollo_DB_Schema::_APL_PROGRAM_IS_BILINGUAL => array(),
            /** @Ticket #12788 */
            Apollo_DB_Schema::_APL_PROGRAM_IS_FREE => array(),
            Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_DATE => array(),
            Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_TIME => array(),
            Apollo_DB_Schema::_APL_PROGRAM_SPACE_TECHNICAL => array(),
            Apollo_DB_Schema::_APL_PROGRAM_LOCATION => array(),
            Apollo_DB_Schema::_APL_PROGRAM_FEE => array(),
            Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_CORE => array(),
            Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_ESSENTIAL => array(),
            Apollo_DB_Schema::_APL_PROGRAM_TEKS_GUIDELINES => array(),
            Apollo_DB_Schema::_APL_PROGRAM_LENGTH_PROGRAM => array(),
            Apollo_DB_Schema::_APL_PROGRAM_MAX_STUDENTS => array('largerThan:0'),
            'program_type' => array(''),
            'population_served' => array(''),
            'subject' => array(''),
            'cultural_origin' => array(''),
            'artistic_discipline' => array(''),
            'video_embed' => array('youtubeUrl'),
            'video_desc' => array(),
        );

        parent::__construct($post_type, $rules);
    }
    
    public function getDataFromRequest() {
        $data = array(
            'id'        => isset($_REQUEST['id']) ? sanitize_text_field($_REQUEST['id']) : '',
            'post_title' => isset($_REQUEST['post_title']) ? sanitize_text_field($_REQUEST['post_title']) : '',
            'post_content' => isset($_REQUEST['post_content']) ? stripslashes($_REQUEST['post_content']) : '',
            'post_status' => isset($_REQUEST['post_status']) ? sanitize_text_field($_REQUEST['post_status']) : 'publish',

            Apollo_DB_Schema::_APL_PROGRAM_CNAME   => isset($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_CNAME]) ? sanitize_text_field($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_CNAME]) : '',
            Apollo_DB_Schema::_APL_PROGRAM_PHONE   => isset($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_PHONE]) ? sanitize_text_field($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_PHONE]) : '',
            Apollo_DB_Schema::_APL_PROGRAM_EMAIL   => isset($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_EMAIL]) ? sanitize_text_field($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_EMAIL]) : '',
            Apollo_DB_Schema::_APL_PROGRAM_URL  => isset($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_URL]) ? sanitize_text_field($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_URL]) : '',
            Apollo_DB_Schema::_APL_PROGRAM_STARTD    => isset($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_STARTD]) ? sanitize_text_field($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_STARTD]) : '',
            Apollo_DB_Schema::_APL_PROGRAM_ENDD => isset($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_ENDD]) ? sanitize_text_field($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_ENDD]) : '',
            Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND    => isset($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND]) ? sanitize_text_field($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND]) : '',
            Apollo_DB_Schema::_APL_PROGRAM_CANCEL_POLICY => isset($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_CANCEL_POLICY]) ? ($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_CANCEL_POLICY]) : '',
            Apollo_DB_Schema::_APL_PROGRAM_REFERENCES => isset($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_REFERENCES]) ? ($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_REFERENCES]) : '',
            Apollo_DB_Schema::_APL_PROGRAM_IS_BILINGUAL  => isset($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_IS_BILINGUAL]) ? sanitize_text_field($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_IS_BILINGUAL]) : 'no',
            /** @Ticket #12788*/
            Apollo_DB_Schema::_APL_PROGRAM_IS_FREE  => isset($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_IS_FREE]) ? sanitize_text_field($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_IS_FREE]) : 'no',
            Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_DATE    => isset($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_DATE]) ? ($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_DATE]) : '',
            Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_TIME    => isset($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_TIME]) ? ($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_TIME]) : '',
            Apollo_DB_Schema::_APL_PROGRAM_SPACE_TECHNICAL    => isset($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_SPACE_TECHNICAL]) ? ($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_SPACE_TECHNICAL]) : '',
            Apollo_DB_Schema::_APL_PROGRAM_LOCATION    => isset($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_LOCATION]) ? ($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_LOCATION]) : '',
            Apollo_DB_Schema::_APL_PROGRAM_FEE    => isset($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_FEE]) ? ($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_FEE]) : '',
            Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_CORE    => isset($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_CORE]) ? ($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_CORE]) : '',
            Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_ESSENTIAL    => isset($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_ESSENTIAL]) ? ($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_ESSENTIAL]) : '',
            Apollo_DB_Schema::_APL_PROGRAM_TEKS_GUIDELINES    => isset($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_TEKS_GUIDELINES]) ? sanitize_text_field($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_TEKS_GUIDELINES]) : 'no',
            Apollo_DB_Schema::_APL_PROGRAM_LENGTH_PROGRAM    => isset($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_LENGTH_PROGRAM]) ? sanitize_text_field($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_LENGTH_PROGRAM]) : '',
            Apollo_DB_Schema::_APL_PROGRAM_MAX_STUDENTS    => isset($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_MAX_STUDENTS]) ? sanitize_text_field($_REQUEST[Apollo_DB_Schema::_APL_PROGRAM_MAX_STUDENTS]) : '',
            'program_type'    => isset($_REQUEST['program_type']) ? array_map('intval', $_REQUEST['program_type']) : array(),
            'population_served'    => isset($_REQUEST['population_served']) ? array_map('intval',$_REQUEST['population_served']) : array(),
            'subject'    => isset($_REQUEST['subject']) ? array_map('intval', $_REQUEST['subject']) : array(),
            'cultural_origin'    => isset($_REQUEST['cultural_origin']) ? array_map('intval', $_REQUEST['cultural_origin']) : array(),
            'artistic_discipline'    => isset($_REQUEST['artistic_discipline']) ? array_map('intval',$_REQUEST['artistic_discipline']) : array(),

            Apollo_DB_Schema::_APL_PROGRAM_VIDEO_EMBED => isset($_REQUEST['video_embed']) ? $_REQUEST['video_embed'] : array(),
            Apollo_DB_Schema::_APL_PROGRAM_VIDEO_DESC => isset($_REQUEST['video_desc']) ? $_REQUEST['video_desc'] : array(),

        );
        return Apollo_App::clean_array_data($data);
    }
    
    public function save($arrData) {

        // validate nonce field
        if ( !isset($_POST[$this->nonceName]) || !wp_verify_nonce($_POST[$this->nonceName], $this->nonceAction) ) {
            return false;
        }

        $arrTextAreaField = array(
            'post_content',
            Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_DATE,
            Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_TIME,
            Apollo_DB_Schema::_APL_PROGRAM_SPACE_TECHNICAL,
            Apollo_DB_Schema::_APL_PROGRAM_LOCATION,
            Apollo_DB_Schema::_APL_PROGRAM_FEE,
            Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_CORE,
            Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_ESSENTIAL,
            Apollo_DB_Schema::_APL_PROGRAM_CANCEL_POLICY,
            Apollo_DB_Schema::_APL_PROGRAM_REFERENCES,
        );
        foreach($arrTextAreaField as $field) {
            $arrData[$field] = Apollo_App::convertCkEditorToTinyCME($arrData[$field]);
        }

        $post = array(
            'ID'            => '',
            'post_title'    => $arrData['post_title'],
            'post_content'  => $arrData['post_content'],
            'post_author'   => get_current_user_id(),
            'post_type'     => $this->post_type,
            'post_status'   => $arrData['post_status'],
        );
        $isNew = true;
        if (isset($arrData['ID'])) {
            $arrData['id'] = $arrData['ID'];
        }
        if($arrData['id'] !== '') {
            $post['ID'] = $arrData['id'];
            unset($post['post_author']);
            $isNew = false;
        }

        /**
         * @ticket #19655: [CF] 20190403 - FE Program dashboard - Add Copy/Delete Buttons
         */
        if(get_query_var('_apollo_copy_program')){
            $isNew = true;
            unset($post['ID']);
        }

        /**
         * @ticket #18432: Add two steps: Preview and 'Success' page when submitted program form - item 3
         */
        if($isNew){
            $post['post_status'] = 'unconfirmed';
        }

        // UPDATE POST
        $id = wp_insert_post( $post );
        $this->id = $id;
        /* UPDATE CATEGORY REFERENCES */
        $post_title = $arrData['post_title'];
        unset($arrData['post_title'], $arrData['id'], $arrData['post_content'], $arrData['post_status']);

        /// UPDATE PROGRAM TYPE
        wp_set_post_terms( $id, $arrData['program-type'] , 'program-type' );
        unset($arrData['program_type']);

        /// UPDATE Population Served
        wp_set_post_terms( $id, $arrData[Apollo_DB_Schema::_APL_PROG_TAX_POP_SER] , Apollo_DB_Schema::_APL_PROG_TAX_POP_SER );
        unset($arrData[Apollo_DB_Schema::_APL_PROG_TAX_POP_SER]);

        /// UPDATE Subject
        wp_set_post_terms( $id, $arrData[Apollo_DB_Schema::_APL_PROG_TAX_SUBJECT] , Apollo_DB_Schema::_APL_PROG_TAX_SUBJECT );
        unset($arrData[Apollo_DB_Schema::_APL_PROG_TAX_SUBJECT]);

        /// UPDATE Artistic Discipline
        wp_set_post_terms( $id, $arrData[Apollo_DB_Schema::_APL_PROG_TAX_ART_DESC] , Apollo_DB_Schema::_APL_PROG_TAX_ART_DESC );
        unset($arrData[Apollo_DB_Schema::_APL_PROG_TAX_ART_DESC]);

        /// UPDATE Cultural Origin
        wp_set_post_terms( $id, $arrData[Apollo_DB_Schema::_APL_PROG_TAX_CUL_ORIGIN] , Apollo_DB_Schema::_APL_PROG_TAX_CUL_ORIGIN );
        unset($arrData[Apollo_DB_Schema::_APL_PROG_TAX_CUL_ORIGIN]);
        /* END - UPDATE CATEGORY */

        // UPDATE PDF DOCUMENTS
        $this->_process_upload_pdf($id, 'upload_pdf', '_references');
        unset($arrData['upload_pdf_references']);

        $this->_process_upload_pdf($id, 'upload_pdf', '_related_materials');
        unset($arrData['upload_pdf_related_materials']);

        // SAVE EMBED VIDEO
        update_apollo_meta($id, Apollo_DB_Schema::_APL_PROGRAM_VIDEO_EMBED, $arrData['video_embed']);
        unset($arrData['video_embed']);

        update_apollo_meta($id, Apollo_DB_Schema::_APL_PROGRAM_VIDEO_DESC, $arrData['video_desc']);
        unset($arrData['video_desc']);

        update_apollo_meta( $id, Apollo_DB_Schema::_APL_PROGRAM_IS_BILINGUAL, $arrData[Apollo_DB_Schema::_APL_PROGRAM_IS_BILINGUAL] );
        unset($arrData[$arrData[Apollo_DB_Schema::_APL_PROGRAM_IS_BILINGUAL]]);

        /** @Ticket #12788 */
        update_apollo_meta( $id, Apollo_DB_Schema::_APL_PROGRAM_IS_FREE, $arrData[Apollo_DB_Schema::_APL_PROGRAM_IS_FREE] );
        unset($arrData[$arrData[Apollo_DB_Schema::_APL_PROGRAM_IS_FREE]]);

        // UPDATE START DATE
        update_apollo_meta($id, Apollo_DB_Schema::_APL_PROGRAM_STARTD, $arrData[Apollo_DB_Schema::_APL_PROGRAM_STARTD]);
        unset($arrData[Apollo_DB_Schema::_APL_PROGRAM_STARTD]);

        // UPDATE END DATE
        update_apollo_meta($id, Apollo_DB_Schema::_APL_PROGRAM_ENDD, $arrData[Apollo_DB_Schema::_APL_PROGRAM_ENDD]);
        unset($arrData[Apollo_DB_Schema::_APL_PROGRAM_ENDD]);

        // UPDATE EXPIRATION DATE
        update_apollo_meta($id, Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND, $arrData[Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND]);
        unset($arrData[Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND]);

        // UPDATE META DATA - LEFT DATA
        update_apollo_meta($id, Apollo_DB_Schema::_APL_PROGRAM_DATA, $arrData);

        $group_fields = Apollo_Custom_Field::get_group_fields( Apollo_DB_Schema::_PROGRAM_PT );
        $customFieldValue = array();
        if(is_array($group_fields) && !empty($group_fields)){
            foreach($group_fields as $group){
                if(isset($group['fields']) && !empty($group['fields'])){
                    foreach($group['fields'] as $field){
                        if(isset($arrData[$field->name])){
                            if(is_array( $arrData[$field->name])){
                                $customFieldValue[$field->name] = $arrData[$field->name];
                            }
                            elseif($field->cf_type == 'wysiwyg'){
                                $customFieldValue[$field->name] = Apollo_App::convertCkEditorToTinyCME($arrData[$field->name]);
                            }
                            else{
                                $customFieldValue[$field->name] = Apollo_App::clean_data_request($arrData[$field->name]);
                            }

                            if(Apollo_Custom_Field::has_explain_field($field)){
                                $exFieldName = $field->name.'_explain';
                                $customFieldValue[$exFieldName] = Apollo_App::clean_data_request($arrData[$exFieldName]);
                            }

                            if(Apollo_Custom_Field::has_other_choice($field)){
                                $exFieldName = $field->name.'_other_choice';
                                $customFieldValue[$exFieldName] = Apollo_App::clean_data_request($arrData[$exFieldName]);
                            }

                        } else if ($id) {
                            /**
                             * This field can display on the FE however its value is striped and then need to update the value to empty
                            */
                            if (Apollo_Custom_Field::can_display( $field )) {
                                $customFieldValue[$field->name] = '';
                            }
                            else { // Keep data for some fields are not authorized to display in the FE form
                                $customFieldValue[$field->name] = Apollo_App::apollo_get_meta_data($id, $field->name, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA);
                            }
                        }
                    }
                }
            }
        }
        update_apollo_meta($id, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA, $customFieldValue);

        // UPDATE IMAGES
        $pid = $id;

        /// SAVE GALLERY
        self::saveGalleryHandler($pid, $post['post_type']);

        
        // Update Educator
        $educatorID = Apollo_User::getCurrentAssociatedID(Apollo_DB_Schema::_EDUCATOR_PT);
        $this->_update_educator( $educatorID, $pid );

       /* global $wp;
        $current_url = home_url(). '/'. add_query_arg(array(),$wp->request);
        wp_redirect(home_url() . '/clean-form?redirect=' . urlencode($current_url) . '&post_type='. Apollo_DB_Schema::_PROGRAM_PT .'&action=' . ($isNew ? 'add-new' : 'update'));*/

        /**
         * @ticket #18432: Add two steps: Preview and 'Success' page when submitted program form - item 3
         */
        wp_redirect( home_url("user/education/program/preview/$pid"));
        return $id;
    }

    /**
     * Update program educator
     * @param $edu_id
     * @param $program_id
     */
    private function _update_educator($edu_id, $program_id) {
        if ( $edu_id && $program_id ) {
            $apl_query = new Apl_Query( Apollo_Tables::_APL_PROGRAM_EDUCATOR );
           
            $edu_id  = intval( $edu_id );
            
            $apl_query->delete( "prog_id = $program_id" );
            
            $apl_query->insert( array(
                'prog_id'   => $program_id,
                'edu_id'    => $edu_id,
            ) );
        }
    }

    private function _process_upload_pdf($id, $name_field = 'upload_pdf', $suffix = '') {
        $arr_attachment_ids = array();
        $post_id = $id;

        $_POST['upload_pdf_label'.$suffix] = isset($_POST['upload_pdf_label'.$suffix]) ? Apollo_App::clean_array_data($_POST['upload_pdf_label'.$suffix]) : array();
        $arr_description = array_map('sanitize_text_field', $_POST['upload_pdf_label'.$suffix]);
        $old_attachment_ids = maybe_unserialize(get_apollo_meta($post_id, $name_field.$suffix, true));
        if(empty($old_attachment_ids)) $old_attachment_ids = array();
        $arrError = array();
        /*@ticket #17020: [CF] 20180803 - [FE Forms] - Add the option to increase the file size upload - Item 1*/
        $maxUploadSize = Apollo_App::maxUploadFileSize('b');

        if(!empty($_FILES[$name_field.$suffix]['name'])) {
            foreach($_FILES[$name_field.$suffix]['name'] as $i => $name) {

                if(empty($name)) continue; /* Silent and continue */

                if ($_FILES[$name_field.$suffix]['error'][$i] !== UPLOAD_ERR_OK) {
                    $arrError[] = array(
                        'message' => 'Error upload file: ' . $name,
                        'error' => true
                    );
                    continue;

                }

                // check filesize
                if($_FILES[$name_field.$suffix]['size'][$i] > $maxUploadSize) {
                    $arrError[] = array(
                        'message' => 'File too large. Max upload size allowed is ' . number_format($maxUploadSize/ (1024 * 1024), 2) . ' MB',
                        'error' => true
                    );
                    continue;
                }

                // copy to upload directory
                if ( ! ( ( $uploads = wp_upload_dir(  ) ) && false === $uploads['error'] ) ) {

                    $arrError[] = array(
                        'message' => 'Missing upload dir for file ' . $name,
                        'error' => true
                    );
                    continue;
                }
                $wp_filetype = wp_check_filetype_and_ext($_FILES[$name_field.$suffix]['tmp_name'][$i], $name );
                $type = empty( $wp_filetype['type'] ) ? '' : $wp_filetype['type'];

                if($type !== 'application/pdf') {
                    $arrError[] = array(
                        'message' => 'File  '. $name .' is invalid. Please upload pdf file only!',
                        'error' => true
                    );
                    continue;
                }

                $unique_name = wp_unique_filename( $uploads['path'], $name );
                $filename = $uploads['path']. '/' . $unique_name;

                $move_new_file = @ move_uploaded_file( $_FILES[$name_field.$suffix]['tmp_name'][$i], $filename );

                if ( false === $move_new_file ) {
                    $arrError[] = array(
                        'message' => 'Cannot move to directory: ' . $uploads['path'],
                        'error' => true
                    );
                    continue;
                }

                $url = $uploads['url'] .'/'. $unique_name;

                /* Save into post attachment */
                $attachment = array(
                    'post_mime_type' => $type,
                    'guid' => $url,
                    'post_title' => $unique_name,
                    'post_content' => '',
                    'post_excerpt' => isset($arr_description[$i]) ?  $arr_description[$i] : '',
                );

                $attachment_id = wp_insert_attachment($attachment, $filename, $post_id);
                if ( !is_wp_error($attachment_id) ) {
                    array_push($arr_attachment_ids, $attachment_id);
                }
            }
        }

        /* Error mechasin */
        $_SESSION['messages']['program-upload_pdf'] = $arrError;

        /* Update old part */
        $_POST['upload_pdf_label_old'.$suffix] = isset($_POST['upload_pdf_label_old'.$suffix]) ? Apollo_App::clean_array_data($_POST['upload_pdf_label_old'.$suffix]) : array();
        $arr_description_old = array_map('sanitize_text_field', $_POST['upload_pdf_label_old'.$suffix]);
        $actual_old_attachment_ids = array();
        foreach($old_attachment_ids as $i => $id) {
            $pdf_post = array(
                'ID'           => $id,
                'post_excerpt' => isset($arr_description_old[$i]) ?  $arr_description_old[$i] : '',
            );
            if (isset($arr_description_old[$i])) {
                array_push($actual_old_attachment_ids, $id);
            }
            wp_update_post( $pdf_post );
        }

        // Merge upload result
        $arr_attachment_ids = array_merge($actual_old_attachment_ids, $arr_attachment_ids);
        update_apollo_meta($post_id, $name_field.$suffix, $arr_attachment_ids);
    }

    /**
     * @return bool
     */
    public function submit_program()
    {
        /**
         * @ticket #18432: Arts Education Customizations - Add two steps: Preview and 'Success' page when submitted program form - item 3
         */
        if ( !isset($_POST[$this->nonceName]) || !wp_verify_nonce($_POST[$this->nonceName], $this->nonceAction) ) {
            return false;
        }

        $programId = intval(get_query_var('_apollo_program_id'));

        if (empty($programId)) {
            wp_redirect(home_url('user/education/add-program'));
        }

        $postStatus = get_post_status($programId);

        if ($postStatus == 'unconfirmed') {
            wp_update_post(array(
                    "ID" => $programId,
                    "post_status" => "pending"
                )
            );
        }

        wp_redirect(home_url('user/education/program/submit-success'));
    }

    /**
     * @ticket #19655: [CF] 20190403 - FE Program dashboard - Add Copy/Delete Buttons
     * @param $postId
     * @return bool
     */
    public function deleteProgram($postId){
        if (
            ! isset( $_POST['_aplNonceField'] )
            || ! wp_verify_nonce( $_POST['_aplNonceField'], 'deleteProgramAction' )
        ) {
            return false;
        }

        $id = !empty($postId) ? $postId : '';
        $program = get_program($id);

        if (!$program->post || $program->post->post_author != get_current_user_id()) return false;

        wp_trash_post($id);

        return true;
    }
}