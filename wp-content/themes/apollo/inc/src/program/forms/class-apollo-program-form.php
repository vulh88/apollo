<?php

class Apollo_Program_Form extends Apollo_Form
{
    /* Init form element then set data for them */

    public function formInit()
    {

        $arrData = $this->formData;
        $standardLabel = of_get_option(Apollo_DB_Schema::_EDUCATOR_PROGRAM_STANDARD_LABEL, __('Standards are customized for every classroom visit', 'apollo'));
        $max_upload_pdf_program = of_get_option(Apollo_DB_Schema::_MAX_UPLOAD_PDF_PROGRAM, Apollo_Display_Config::_MAX_UPLOAD_PDF_DEFAULT);
        /*@ticket #17020*/
        $max_upload_size_in_b = Apollo_App::maxUploadFileSize('b');
        $max_upload_size_in_mb = number_format($max_upload_size_in_b / (1024 * 1024), 2);
        $max_upload_gallery_program = of_get_option(Apollo_DB_Schema::_MAX_UPLOAD_GALLERY_IMG,5);
        $artistic_discipline_label = of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_ARTISTIC_DISCIPLINE_LABEL, __( 'Artistic Discipline', 'apollo' ));
        $programEssentialsLabel = of_get_option(Apollo_DB_Schema::_EDUCATOR_PROGRAM_ESSENTIALS_LABEL, __( 'Program Essentials', 'apollo'));
        $programCoreLabel = of_get_option(Apollo_DB_Schema::_EDUCATOR_PROGRAM_CORE_LABEL, 'Program Core');
        $video_data = array(
            'video_embed' => isset($this->formData['video_embed']) ? $this->formData['video_embed'] : '',
            'video_desc' => isset($this->formData['video_desc']) ? $this->formData['video_desc'] : ''
        );

        /*@ticket #18321 0002504: Arts Education Customizations - Suppress the "Education Standards" section of the form*/
        $enableEducationStandards = of_get_option(APL_Theme_Option_Site_Config_SubTab::_EDUCATOR_PROGRAM_ENABLE_STANDARDS, 1) ? '' : 'hidden';

        /**
         * @ticket #18431 0002504: Arts Education Customizations - Remove a restriction that requires the end date to be longer than the start date and modify toolbar editor on FE Program form - item 1,2
         */
        $editorSetting =   array(
            'editor_height' => 130,
            'editor_class'  => 'inp-desc-event apl-program-custom-editor',
            'tinymce'       => array(
                'toolbar1'      => 'bold,italic,link',
                'toolbar2'      => ''
            ),
            'quicktags'  => array("buttons" => "strong,em,link"),
            'textarea_rows' => 3
        );


        /**
         * @ticket #18582:  Arts Education Customizations - Remove the requirement from the 'Max Number of Students' field.
         */
        $requireMaxStudents = of_get_option(APL_Theme_Option_Site_Config_SubTab::_PROGRAM_ENABLE_REQUIREMENT_MAX_STUDENTS, 0);
        $classRequireMaxStudents = $requireMaxStudents ? 'required, ' : '';
        $placeholderMaxStudents = 'Max. number of students (must be greater than "0")' . ($requireMaxStudents ? ' (*)' : '');

        /**
         * @ticket #19668:  [FE Program Form] Auto-fill the name and email
         */
        $current_user = wp_get_current_user();
        $contactName = isset($arrData[Apollo_DB_Schema::_APL_PROGRAM_CNAME]) ? $arrData[Apollo_DB_Schema::_APL_PROGRAM_CNAME] : '';

        if (!$contactName) {
            $contactName = !empty($current_user->display_name) ? $current_user->display_name : '';
        }

        $contactEmail = isset($arrData[Apollo_DB_Schema::_APL_PROGRAM_EMAIL]) ? $arrData[Apollo_DB_Schema::_APL_PROGRAM_EMAIL] : '';
        if (!$contactEmail) {
            $contactEmail = !empty($current_user->user_email) ? $current_user->user_email : '';
        }

        $formElementItems = array(
            // PROGRAM DETAIL
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

                array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => __('Program detail', 'apollo'),
                    'class' => 'title-bar-blk',
                ),

                'ID' => array(
                    'type' => 'Hidden',
                    'name' => 'ID',
                    'place_holder' => __('Name', 'apollo'),
                    'value' => isset($arrData['ID']) ? $arrData['ID'] : '',
                    'class' => 'inp inp-txt',
                    'title' => __('Name', 'apollo'),
                    'validate' => true,
                    'container' => '',
                ),

                'post_status' => array(
                    'type' => 'Hidden',
                    'name' => 'post_status',
                    'place_holder' => __('Post status', 'apollo'),
                    'value' => isset($arrData['post_status']) ? $arrData['post_status'] : 'pending',
                    'class' => '',
                    'title' => __('Post status', 'apollo'),
                    'validate' => true,
                ),

                'post_title' => array(
                    'type' => 'Text',
                    'name' => 'post_title',
                    'place_holder' => __('Program Name (*)', 'apollo'),
                    'value' => isset($arrData['post_title']) ? $arrData['post_title'] : '',
                    'class' => 'inp inp-txt validate[required]',
                    'data_attributes' => array(
                        'data-errormessage-value-missing="'.__('Program name is required','apollo').'"'
                    ),
                    'title' => __('Program Name (*)', 'apollo'),
                    'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_REQUIRED
                        )
                    ),
                    'container' =>'',
                ),

                'post_content' => array(
                    'type' => 'Wysiwyg',
                    'name' => 'post_content',
                    'value' => Apollo_App::convertTinyCMEToCkEditor(isset($arrData['post_content']) ? $arrData['post_content'] : ''),
                    'class' => 'inp-desc-event custom-validate-field validate[required]',
                    'id' => 'program-description',
                    'title' => __( 'Description (*)', 'apollo' ),
                    'data_attributes' => array(
                        'data-custom-validate="wysiwyg"',
                        'data-error-message="'.__('Description is required','apollo').'"',
                        'data-id="program-description"'
                    ),
                    'validate' => array(
                        'rule' => array(
                                Apollo_Form::_FORM_REQUIRED
                        )
                    )
                ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

                array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => $artistic_discipline_label,
                    'class' => '',
                    'container' => 'title-bar-blk',
                ),

                Apollo_DB_Schema::_APL_PROG_TAX_ART_DESC => array(
                    'title' => $artistic_discipline_label,
                    'type' => 'Category_Group',
                    'name' => Apollo_DB_Schema::_APL_PROG_TAX_ART_DESC,
                    'class' => 'access-listing custom-validate-field',
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_PROG_TAX_ART_DESC]) ? $arrData[Apollo_DB_Schema::_APL_PROG_TAX_ART_DESC] : '',
                    'post_type' => Apollo_DB_Schema::_PROGRAM_PT,
                    'tax' => Apollo_DB_Schema::_APL_PROG_TAX_ART_DESC,
                    'container' => 'access-list artist',
                ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            Apollo_DB_Schema::_APL_PROG_TAX_CUL_ORIGIN . '-div' => array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

            Apollo_DB_Schema::_APL_PROG_TAX_CUL_ORIGIN . '-title' => array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => __( 'Cultural Origin', 'apollo' ),
                    'class' => '',
                    'container' => 'title-bar-blk',
                ),

                Apollo_DB_Schema::_APL_PROG_TAX_CUL_ORIGIN => array(
                    'title' => __('Cultural Origin', 'apollo'),
                    'type' => 'Category_Group',
                    'name' => Apollo_DB_Schema::_APL_PROG_TAX_CUL_ORIGIN,
                    'class' => 'access-listing custom-validate-field',
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_PROG_TAX_CUL_ORIGIN]) ? $arrData[Apollo_DB_Schema::_APL_PROG_TAX_CUL_ORIGIN] : '',
                    'post_type' => Apollo_DB_Schema::_PROGRAM_PT,
                    'tax' => Apollo_DB_Schema::_APL_PROG_TAX_CUL_ORIGIN,
                    'container' => 'access-list artist',
                ),

            Apollo_DB_Schema::_APL_PROG_TAX_CUL_ORIGIN . '-close' => array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

                array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => __( 'Population Served', 'apollo' ),
                    'class' => '',
                    'container' => 'title-bar-blk',
                ),

                Apollo_DB_Schema::_APL_PROG_TAX_POP_SER => array(
                    'title' => __('Population Served', 'apollo'),
                    'type' => 'Category_Group',
                    'name' => Apollo_DB_Schema::_APL_PROG_TAX_POP_SER,
                    'class' => 'access-listing custom-validate-field',
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_PROG_TAX_POP_SER]) ? $arrData[Apollo_DB_Schema::_APL_PROG_TAX_POP_SER] : '',
                    'post_type' => Apollo_DB_Schema::_PROGRAM_PT,
                    'tax' => Apollo_DB_Schema::_APL_PROG_TAX_POP_SER,
                    'container' => 'access-list artist',
                ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

                array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => __( 'Program Type', 'apollo' ),
                    'class' => '',
                    'container' => 'title-bar-blk',
                ),

                'program-type' => array(
                    'title' => __('Program Type', 'apollo'),
                    'type' => 'Category_Group',
                    'name' => 'program-type',
                    'class' => 'access-listing custom-validate-field',
                    'value' => isset($arrData['program-type']) ? $arrData['program-type'] : '',
                    'post_type' => Apollo_DB_Schema::_PROGRAM_PT,
                    'container' => 'access-list artist',
                ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

                array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => __( 'Subject', 'apollo' ),
                    'class' => '',
                    'container' => 'title-bar-blk',
                ),

                Apollo_DB_Schema::_APL_PROG_TAX_SUBJECT => array(
                    'title' => __('Subject', 'apollo'),
                    'type' => 'Category_Group',
                    'name' => Apollo_DB_Schema::_APL_PROG_TAX_SUBJECT,
                    'class' => 'access-listing custom-validate-field',
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_PROG_TAX_SUBJECT]) ? $arrData[Apollo_DB_Schema::_APL_PROG_TAX_SUBJECT] : '',
                    'post_type' => Apollo_DB_Schema::_PROGRAM_PT,
                    'tax' => Apollo_DB_Schema::_APL_PROG_TAX_SUBJECT,
                    'container' => 'access-list artist',
                ),

                Apollo_DB_Schema::_APL_PROGRAM_MAX_STUDENTS => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_PROGRAM_MAX_STUDENTS,
                    'place_holder' => __($placeholderMaxStudents, 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_PROGRAM_MAX_STUDENTS]) ? $arrData[Apollo_DB_Schema::_APL_PROGRAM_MAX_STUDENTS] : '',
                    'class' => "inp inp-txt validate[$classRequireMaxStudents funcCall[disableSpace], custom[integer]",
                    'id' =>Apollo_DB_Schema::_APL_PROGRAM_MAX_STUDENTS,
                    'title' => __('Max. number of students (must be greater than "0")', 'apollo'),
                    'data_attributes' => array(
                        'data-id="'.Apollo_DB_Schema::_APL_PROGRAM_MAX_STUDENTS.'"'
                    ),
                    'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_NUMBER,
                            'largerThan:0'
                        )
                    )
                ),

                Apollo_DB_Schema::_APL_PROGRAM_LENGTH_PROGRAM => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_PROGRAM_LENGTH_PROGRAM,
                    'place_holder' => __('Length of program (min / hours per session)', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_PROGRAM_LENGTH_PROGRAM]) ? $arrData[Apollo_DB_Schema::_APL_PROGRAM_LENGTH_PROGRAM] : '',
                    'class' => 'inp inp-txt caldr',
                    'id' =>Apollo_DB_Schema::_APL_PROGRAM_LENGTH_PROGRAM,
                    'title' => __('Length of program (min / hours per session)', 'apollo'),
                ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

                Apollo_DB_Schema::_APL_PROGRAM_STARTD => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_PROGRAM_STARTD,
                    'place_holder' => __('Start day', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_PROGRAM_STARTD]) ? $arrData[Apollo_DB_Schema::_APL_PROGRAM_STARTD] : '',
                    'class' => 'inp inp-txt apl-validation-datepicker',
                    'id' =>Apollo_DB_Schema::_APL_PROGRAM_STARTD,
                    'container' => 'date-picker',
                    'title' => __('Start day', 'apollo'),
                ),

                Apollo_DB_Schema::_APL_PROGRAM_ENDD => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_PROGRAM_ENDD,
                    'place_holder' => __('End day', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_PROGRAM_ENDD]) ? $arrData[Apollo_DB_Schema::_APL_PROGRAM_ENDD] : '',
                    'class' => 'inp inp-txt  apl-validation-datepicker ',
                    'id' =>Apollo_DB_Schema::_APL_PROGRAM_ENDD,
                    'container' => 'date-picker',
                    'title' => __('End day', 'apollo'),
                    'data_attributes' => array(
                        'data-id="'.Apollo_DB_Schema::_APL_PROGRAM_ENDD.'"'
                    ),
                    'validate' => array(
                        'rule' => array(
                            'endateLargerThanStartdate:'.Apollo_DB_Schema::_APL_PROGRAM_STARTD
                        )
                    )
                ),

                Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND,
                    'place_holder' => __('Expiration day', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND]) ? $arrData[Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND] : '',
                    'class' => 'inp inp-txt  apl-validation-datepicker',
                    'id' =>Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND,
                    'container' => 'date-picker',
                    'title' => __('End day', 'apollo'),
                ),

                Apollo_DB_Schema::_APL_PROGRAM_IS_BILINGUAL => array(
                    'type' => 'OnOffSwitchCheckbox',
                    'name' => Apollo_DB_Schema::_APL_PROGRAM_IS_BILINGUAL,
                    'title' => __( 'Bilingual', 'apollo' ),
                    'id' => 'myonoffswitch',
                    'container' => 'el-blk',
                    'value' => array(
                        'list_item' => 'yes',
                        'selected_item' => isset($arrData[Apollo_DB_Schema::_APL_PROGRAM_IS_BILINGUAL]) ? $arrData[Apollo_DB_Schema::_APL_PROGRAM_IS_BILINGUAL] : ''
                    ),
                ),

                Apollo_DB_Schema::_APL_PROGRAM_IS_FREE => array(
                    'type' => 'OnOffSwitchCheckbox',
                    'name' => Apollo_DB_Schema::_APL_PROGRAM_IS_FREE,
                    'title' => __( 'Free', 'apollo' ),
                    'id' => 'freeonoffswitch',
                    'container' => 'el-blk',
                    'value' => array(
                        'list_item' => 'yes',
                        'selected_item' => isset($arrData[Apollo_DB_Schema::_APL_PROGRAM_IS_FREE]) ? $arrData[Apollo_DB_Schema::_APL_PROGRAM_IS_FREE] : ''
                    ),
                ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

                Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_DATE => array(
                    'type' => 'Wysiwyg',
                    'name' => Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_DATE,
                    'value' => Apollo_App::convertTinyCMEToCkEditor(isset($arrData[Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_DATE]) ? $arrData[Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_DATE] : ''),
                    'class' => 'inp-desc-event',
                    'id' => Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_DATE,
                    'title' => __( 'Available dates', 'apollo' ),
                    'editor_settings' => $editorSetting
                ),

                Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_TIME => array(
                    'type' => 'Wysiwyg',
                    'name' => Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_TIME,
                    'value' => Apollo_App::convertTinyCMEToCkEditor(isset($arrData[Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_TIME]) ? $arrData[Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_TIME] : ''),
                    'class' => 'inp-desc-event',
                    'id' => Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_TIME,
                    'title' => __( 'Time(s)', 'apollo' ),
                    'editor_settings' => $editorSetting
                ),

                Apollo_DB_Schema::_APL_PROGRAM_SPACE_TECHNICAL => array(
                    'type' => 'Wysiwyg',
                    'name' => Apollo_DB_Schema::_APL_PROGRAM_SPACE_TECHNICAL,
                    'value' => Apollo_App::convertTinyCMEToCkEditor(isset($arrData[Apollo_DB_Schema::_APL_PROGRAM_SPACE_TECHNICAL]) ? $arrData[Apollo_DB_Schema::_APL_PROGRAM_SPACE_TECHNICAL] : ''),
                    'class' => 'inp-desc-event',
                    'id' => Apollo_DB_Schema::_APL_PROGRAM_SPACE_TECHNICAL,
                    'title' => __( 'Space / Technical Requirements', 'apollo' ),
                    'editor_settings' => $editorSetting
                ),

                Apollo_DB_Schema::_APL_PROGRAM_LOCATION => array(
                    'type' => 'Wysiwyg',
                    'name' => Apollo_DB_Schema::_APL_PROGRAM_LOCATION,
                    'value' => Apollo_App::convertTinyCMEToCkEditor(isset($arrData[Apollo_DB_Schema::_APL_PROGRAM_LOCATION]) ? $arrData[Apollo_DB_Schema::_APL_PROGRAM_LOCATION] : ''),
                    'class' => 'inp-desc-event',
                    'id' => Apollo_DB_Schema::_APL_PROGRAM_LOCATION,
                    'title' => __( 'Location(s)', 'apollo' ),
                    'editor_settings' => $editorSetting
                ),

                Apollo_DB_Schema::_APL_PROGRAM_FEE => array(
                    'type' => 'Wysiwyg',
                    'name' => Apollo_DB_Schema::_APL_PROGRAM_FEE,
                    'value' => Apollo_App::convertTinyCMEToCkEditor(isset($arrData[Apollo_DB_Schema::_APL_PROGRAM_FEE]) ? $arrData[Apollo_DB_Schema::_APL_PROGRAM_FEE] : ''),
                    'class' => 'inp-desc-event',
                    'id' => Apollo_DB_Schema::_APL_PROGRAM_FEE,
                    'title' => __( 'Fee / ticketing', 'apollo' ),
                    'editor_settings' => $editorSetting
                ),

                Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_CORE => array(
                    'type' => 'Wysiwyg',
                    'name' => Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_CORE,
                    'value' => Apollo_App::convertTinyCMEToCkEditor(isset($arrData[Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_CORE]) ? $arrData[Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_CORE] : ''),
                    'class' => 'inp-desc-event',
                    'id' => Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_CORE,
                    'title' => $programCoreLabel
                ),

                Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_ESSENTIAL => array(
                    'type' => 'Wysiwyg',
                    'name' => Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_ESSENTIAL,
                    'value' => Apollo_App::convertTinyCMEToCkEditor(isset($arrData[Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_ESSENTIAL]) ? $arrData[Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_ESSENTIAL] : ''),
                    'class' => 'inp-desc-event',
                    'id' => Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_ESSENTIAL,
                    'title' => $programEssentialsLabel
                ),

                //_EDUCATOR_ENABLE_PROGRAM_ESSENTIALS_FIELD

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            //PROGRAM / BOOKING CONTACT
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

                array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => __('Program/Booking contact', 'apollo'),
                    'class' => 'title-bar-blk',
                ),

                Apollo_DB_Schema::_APL_PROGRAM_CNAME => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_PROGRAM_CNAME,
                    'place_holder' => __('Contact name (*)', 'apollo'),
                    'value' => $contactName,
                    'class' => 'inp inp-txt validate[required]',
                    'data_attributes' => array(
                        'data-errormessage-value-missing="'.__('Contact Name is required','apollo').'"'
                    ),
                    'title' => __('Contact name (*)', 'apollo'),
                    'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_REQUIRED
                        )
                    ),
                ),

                Apollo_DB_Schema::_APL_PROGRAM_PHONE => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_PROGRAM_PHONE,
                    'place_holder' => __('Contact Phone', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_PROGRAM_PHONE]) ? $arrData[Apollo_DB_Schema::_APL_PROGRAM_PHONE] : '',
                    'class' => 'inp inp-txt',
                    'title' => __('Contact Phone', 'apollo'),
                ),

                Apollo_DB_Schema::_APL_PROGRAM_EMAIL => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_PROGRAM_EMAIL,
                    'place_holder' => __('Contact Email', 'apollo'),
                    'value' => $contactEmail,
                    'class' => 'inp inp-txt validate[funcCall[disableSpace],custom[email]]',
                    'title' => __('Contact Email', 'apollo'),
                    'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_EMAIL
                        )
                    ),
                ),

                Apollo_DB_Schema::_APL_PROGRAM_URL => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_APL_PROGRAM_URL,
                    'place_holder' => __('URL', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_PROGRAM_URL]) ? $arrData[Apollo_DB_Schema::_APL_PROGRAM_URL] : '',
                    'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]]',
                    'container' => '',
                    'title' => __('URL', 'apollo'),
                    'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_URL
                        )
                    ),
                ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            //addition cf
            'Additional_Form' => array(
                'type' => 'Additional_fields',
                'name' => Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA,
                'class' => 'full-custom',
                'id' => '',
                'container' => 'artist-blk',
                'value' => array(
                    'post_type' => Apollo_DB_Schema::_PROGRAM_PT,
                    'post_data' => $arrData,
                    'post_id' => isset($arrData['ID']) ? $arrData['ID'] : '',
                )
            ),

            //CANCELLATION POLICY
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

                array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => __('Cancellation policy', 'apollo'),
                    'class' => 'title-bar-blk',
                ),

                Apollo_DB_Schema::_APL_PROGRAM_CANCEL_POLICY => array(
                    'type' => 'Wysiwyg',
                    'title' => __( 'Cancellation policy', 'apollo' ),
                    'name' => Apollo_DB_Schema::_APL_PROGRAM_CANCEL_POLICY,
                    'value' => Apollo_App::convertTinyCMEToCkEditor(isset($arrData[Apollo_DB_Schema::_APL_PROGRAM_CANCEL_POLICY]) ? $arrData[Apollo_DB_Schema::_APL_PROGRAM_CANCEL_POLICY] : ''),
                    'class' => 'inp-desc-event',
                    'id' => Apollo_DB_Schema::_APL_PROGRAM_CANCEL_POLICY,
                ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            //REFERENCES
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

                array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => __('References', 'apollo'),
                    'class' => 'title-bar-blk',
                ),

                Apollo_DB_Schema::_APL_PROGRAM_REFERENCES => array(
                    'type' => 'Wysiwyg',
                    'title' => __( 'References', 'apollo' ),
                    'name' => Apollo_DB_Schema::_APL_PROGRAM_REFERENCES,
                    'value' => Apollo_App::convertTinyCMEToCkEditor(isset($arrData[Apollo_DB_Schema::_APL_PROGRAM_REFERENCES]) ? $arrData[Apollo_DB_Schema::_APL_PROGRAM_REFERENCES] : ''),
                    'class' => 'inp-desc-event',
                    'id' => Apollo_DB_Schema::_APL_PROGRAM_REFERENCES,
                ),

                'references_document' => array(
                    'type' => 'Document_Group',
                    'value' => array(
                        'post_id' => $arrData['ID'],
                        'hide_header' => true,
                        'document_id' => 'upload_pdf-references',
                        'metadata_key' => 'upload_pdf_references',
                        'suffix' => '_references',
                        'max_upload_pdf' => $max_upload_pdf_program
                    )
                ),

                //Render references upload

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            //RELATED MATERIALS DOCUMENTS
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

                array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => __('Related materials documents', 'apollo'),
                    'class' => 'title-bar-blk',
                ),

                //Render content
                'related_desc' => array(
                    'type' => 'Subtitle',
                    'class' => 'program-label',
                    'value' => sprintf(__("You can upload up to %d documents that will be featured in the Related Materials section of the program's detail page. All documents must be %s format only and must be smaller than %s megabyte each in size", "apollo"), $max_upload_pdf_program, '.PDF', $max_upload_size_in_mb)
                ),

                'related_document' => array(
                    'type' => 'Document_Group',
                    'value' => array(
                        'post_id' => $arrData['ID'],
                        'hide_header' => true,
                        'document_id' => 'upload_pdf-related-materials',
                        'metadata_key' => 'upload_pdf_related_materials',
                        'suffix' => '_related_materials',
                        'max_upload_pdf' => $max_upload_pdf_program
                    )
                ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            // IMAGES
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

                array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => __('Image', 'apollo'),
                    'class' => 'title-bar-blk',
                ),

                'image_desc' => array(
                    'type' => 'Subtitle',
                    'class' => 'program-label',
                    'value' => sprintf(__('You can upload up to %d images that will be featured in your image gallery. All images must be %s format only and must smaller than %s megabyte each in size.', "apollo"), $max_upload_gallery_program, '.jpg, .png, .jpeg', $max_upload_size_in_mb)
                ),

                'gallery_photo' => array(
                    'type' => 'ShortCode',
                    'name' => 'gallery_photo',
                    'title' => __('GALLERY','apollo'),
                    'target' => Apollo_DB_Schema::_PROGRAM_PT,
                    'value' =>  'apollo_upload_gallery',
                    'data_attributes' => array(
                        'translate' => true
                    ),
                ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            // VIDEOS
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

                array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => __('Videos', 'apollo'),
                    'class' => 'title-bar-blk',
                ),

                'video_desc' => array(
                    'type' => 'Subtitle',
                    'class' => 'program-label',
                    'value' => __( 'This form accepts URL links provided by YouTube.com and Vimeo “Share” options.', 'apollo' )
                ),

                'video_data' =>  array(
                    'type' => 'Loop',
                    'place_holder' =>__('Name','apollo'),
                    'class' => 'inp inp-txt',
                    'value' => $video_data,
                    'container' => 'el-blk',
                    'children' => array(
                        //group 1
                        array(
                            'video_embed' => array(
                                'type' => 'YoutubeLink',
                                'name' => 'video_embed[]',
                                'place_holder' => __('YouTube or Vimeo URL"','apollo'),
                                'title' => __('Video embed','apollo'),
                                'class' => 'inp-txt-event',
                                'validate' => array(
                                    'rule' => array(
                                        Apollo_Form::_FORM_YOUTUBE_LINK
                                    )
                                )
                            ),
                            'video_desc' => array(
                                'type' => 'TextArea',
                                'name' => 'video_desc[]',
                                'place_holder' => __('Video Description','apollo'),
                                'title' => __('Video Description','apollo'),
                                'class' => 'desc-video'
                            ),
                        ),
                    ),
                    'control_button' => array(
                        //button 1
                        array(
                            'control_1' => array(
                                'type' =>   'Button',
                                'class'=>   'btn-noW add-new-group',
                                'title' => __(Apollo_Form_Static::AddMoreButtonLabel($video_data),'apollo'),
                                'no_container' =>true,
                                'data'=> array(
                                    'data-add' => __('ADD VIDEO LINK','apollo'),
                                    'data-addmore' => __('ADD MORE LINKS','apollo'),
                                )
                            )
                        ),
                        //button 2
                        array(
                            'control_2' => array(
                                'id' => 'video-preview',
                                'type' =>   'Button',
                                'class'=>   'btn-noW btn-preview',
                                'title' => __('PREVIEW','apollo'),
                                'no_container' =>true,
                                'data' => array(
                                    'data-alert' => __('* Please input valid youtube or vimeo link.','apollo')
                                )
                            )
                        ),
                    )
                ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            // EDUCATION STANDARDS
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk program-blk ' .  $enableEducationStandards,
                'container' => '',
            ),

                array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => __('Education standards', 'apollo'),
                    'class' => 'title-bar-blk',
                ),

                Apollo_DB_Schema::_APL_PROGRAM_TEKS_GUIDELINES => array(
                    'type' => 'Checkbox',
                    'name' => Apollo_DB_Schema::_APL_PROGRAM_TEKS_GUIDELINES,
                    'title' => $standardLabel,
                    'value' => array(
                        'list_item' => 'yes',
                        'selected_item' => isset($arrData[Apollo_DB_Schema::_APL_PROGRAM_TEKS_GUIDELINES]) ? $arrData[Apollo_DB_Schema::_APL_PROGRAM_TEKS_GUIDELINES] : ''
                    ),
                ),

            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            //submit button
            'Submit_btn' => array(
                'type' => 'Submit',
                'class' => 'submit-btn submit-form-with-validation-engine',
                'title' => __('PREVIEW RECORD', 'apollo')
            ),

        );

        //Theme option
        if(!of_get_option( Apollo_DB_Schema::_EDUCATOR_ENABLE_CULTURAL_ORIGIN_DROP, true)){
            unset($formElementItems[Apollo_DB_Schema::_APL_PROG_TAX_CUL_ORIGIN . '-div']);
            unset($formElementItems[Apollo_DB_Schema::_APL_PROG_TAX_CUL_ORIGIN . '-title']);
            unset($formElementItems[Apollo_DB_Schema::_APL_PROG_TAX_CUL_ORIGIN]);
            unset($formElementItems[Apollo_DB_Schema::_APL_PROG_TAX_CUL_ORIGIN . '-close']);
        }

        if(!of_get_option( Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_BILINGUAL, true)){
            unset($formElementItems[Apollo_DB_Schema::_APL_PROGRAM_IS_BILINGUAL]);
        }

        if(!of_get_option( Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_FREE, true)){
            unset($formElementItems[Apollo_DB_Schema::_APL_PROGRAM_IS_FREE]);
        }

        if( !of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_CORE_FIELD, false)) {
            unset($formElementItems[Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_CORE]);
        }
        if( !of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_ESSENTIALS_FIELD, false)) {
            unset($formElementItems[Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_ESSENTIAL]);
        }


        $this->elements = $formElementItems;

    }

    public function formHeader()
    {
        $id = $this->id ? $this->id : $this->formData['ID'];
        ?>
        <div class="dsb-welc custom">
            <h1 ><?php
                if ($id == null) {
                    _e('Add Program', 'apollo');
                } else {
                    _e('Update Program', 'apollo');
                }
                if ( $id && isset($this->formData['post_status']) &&
                    ($this->formData['post_status'] == 'publish' || $this->formData['post_status'] == 'pending') && $this->formData
                ): ?>
                    <a class="view-page-link" target="_blank"
                       href="<?php echo get_the_permalink($id) ?>">(<?php _e('View Page', 'apollo') ?>
                        )</a>
                <?php endif; ?>
            </h1>
        </div>
        <?php
        if (isset($_SESSION['save_'. Apollo_DB_Schema::_PROGRAM_PT])) :
            if ($_SESSION['save_'. Apollo_DB_Schema::_PROGRAM_PT] == 'add-new') :
                $this->formSubmitSuccessRedirect(__( 'Added program successfully, please wait for approving from administrator', 'apollo' ));
            else :
                $this->formSubmitSuccessRedirect(__( Apollo_Form_Static::_SUCCESS_MESSAGE, 'apollo'));
            endif;
            unset($_SESSION['save_'. Apollo_DB_Schema::_PROGRAM_PT]);
        endif; ?>
        <?php
        if ( isset($_GET['warning']) ):
            ?>
            <div><span class="error"><?php _e('Please enter your Educator Profile first !', 'apollo') ?></span></div>
        <?php endif; ?>

        <?php

        parent::formHeader();
    }

    /*Process form submit*/
    public function postMethod()
    {
        // validate nonce
        if ( $this->validateNonce() ) {
            $this->validateClassObject->save($this->formData);
        }
    }

    /*process form get method*/
    public function getMethod()
    {
        $this->successMessage = __(Apollo_Form_Static::_SUCCESS_MESSAGE, 'apollo');
        $this->failMessage = __(Apollo_Form_Static::_FAIL_MESSAGE, 'apollo');


        if ($id = intval( get_query_var( '_apollo_program_id' ) )) {
            $postStatus = get_post_status($id);
            if($postStatus == 'publish' ||  $postStatus == 'pending' ||  $postStatus == 'inherit' || $postStatus == 'unconfirmed'){
                $postInfo = get_post($id, ARRAY_A);
                $status = $postInfo['post_status'];
                $id = $postInfo['ID'];
                //get all meta;
                $metaCFData = get_apollo_meta($id, Apollo_DB_Schema::_APL_PROGRAM_CUSTOM_FIELDS , true);
                $metaData = get_apollo_meta($id, Apollo_DB_Schema::_APL_PROGRAM_DATA, true);
                if (!is_array($metaCFData) && !empty($metaCFData))
                    $metaCFData = unserialize($metaCFData);

                if (!is_array($metaData) && !empty($metaData))
                    $metaData = unserialize($metaData);

                if (is_array($metaCFData)) {
                    foreach ($metaCFData as $key => $value) {
                        $postInfo[$key] = $value;
                    }

                }
                if (is_array($metaData)) {
                    foreach ($metaData as $key => $value) {
                        $postInfo[$key] = $value;
                    }
                }

                $postInfo['upload_pdf_references'] = get_apollo_meta($id, 'upload_pdf_references', true);
                $postInfo['upload_pdf_related_materials'] = get_apollo_meta($id, 'upload_pdf_related_materials', true);

                $postInfo['video_embed'] = get_apollo_meta($id, Apollo_DB_Schema::_APL_PROGRAM_VIDEO_EMBED, true);
                $postInfo['video_desc'] = get_apollo_meta($id, Apollo_DB_Schema::_APL_PROGRAM_VIDEO_DESC, true);

                $postInfo[Apollo_DB_Schema::_APL_PROGRAM_IS_BILINGUAL] = get_apollo_meta($id, Apollo_DB_Schema::_APL_PROGRAM_IS_BILINGUAL, true);
                $postInfo[Apollo_DB_Schema::_APL_PROGRAM_IS_FREE] = get_apollo_meta($id, Apollo_DB_Schema::_APL_PROGRAM_IS_FREE, true);
                $postInfo[Apollo_DB_Schema::_APL_PROGRAM_STARTD] = get_apollo_meta($id, Apollo_DB_Schema::_APL_PROGRAM_STARTD, true);
                $postInfo[Apollo_DB_Schema::_APL_PROGRAM_ENDD] = get_apollo_meta($id, Apollo_DB_Schema::_APL_PROGRAM_ENDD, true);
                $postInfo[Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND] = get_apollo_meta($id, Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND, true);

                $postInfo['program-type'] = wp_get_post_terms($id, Apollo_DB_Schema::_PROGRAM_PT . '-type', array('fields' => 'ids'));
                $postInfo[Apollo_DB_Schema::_APL_PROG_TAX_ART_DESC] = wp_get_post_terms($id, Apollo_DB_Schema::_APL_PROG_TAX_ART_DESC, array('fields' => 'ids'));
                $postInfo[Apollo_DB_Schema::_APL_PROG_TAX_CUL_ORIGIN] = wp_get_post_terms($id, Apollo_DB_Schema::_APL_PROG_TAX_CUL_ORIGIN, array('fields' => 'ids'));
                $postInfo[Apollo_DB_Schema::_APL_PROG_TAX_POP_SER] = wp_get_post_terms($id, Apollo_DB_Schema::_APL_PROG_TAX_POP_SER, array('fields' => 'ids'));
                $postInfo[Apollo_DB_Schema::_APL_PROG_TAX_SUBJECT] = wp_get_post_terms($id, Apollo_DB_Schema::_APL_PROG_TAX_SUBJECT, array('fields' => 'ids'));
                $postInfo['ID'] = $id;
                $this->formData = $postInfo;
                $this->successMessage = Apollo_Form_Static::_SUCCESS_MESSAGE;
            }
        }

}
    //Show message Fail Submit

//override parent for additional field
    public function formSubmitAction()
    {
        $addFieldsValidateClass = new Apollo_Submit_Form(Apollo_DB_Schema::_PROGRAM_PT, array());
        $this->isSaveSuccess = false;
        $this->isSubmitFail = false;
        //post method

        if ($this->formRequestMethod == 'post') {
            $this->cleanPost();
            $this->isSubmitFail = true;
            if ($this->validateClassObject->isValidAll($this->mergerPostRule())) {
                $this->isSaveSuccess = true;
                $this->isSubmitFail = false;
            }
            if (!$addFieldsValidateClass->isValidAll($this->mergePostAFields())) {
                $this->isSaveSuccess = false;
                $this->isSubmitFail = true;
            }
            //save data
            if ($this->isSaveSuccess)
                $this->id = $this->postMethod();
            //end save date
        }
    }

    public function mergePostAFields()
    {
        $data = array();
        $group_fields = Apollo_Custom_Field::get_group_fields(Apollo_DB_Schema::_PROGRAM_PT);
        foreach ($group_fields as $group) {
            $fields = isset($group['fields']) ? $group['fields'] : array();
            if (count($fields) > 0) {
                foreach ($fields as $field) {
                    $data[$field->name] = isset($_POST[$field->name]) ? $_POST[$field->name] : '';
                }
            }
        }
        return $data;
    }

    /**
     * Set nonce info
     *
     * @return void
     */
    public function setNonceInfo()
    {
        $this->nonceName   = Apollo_Const::_APL_EDUCATION_NONCE_NAME;
        $this->nonceAction = Apollo_Const::_APL_NONCE_ACTION_EDUCATION_ADD_PROGRAM_PAGE;
    }
}