<?php
require_once APOLLO_TEMPLATES_DIR. '/pages/lib/class-apollo-page-module.php';

class Apollo_Classified_Page extends Apollo_Page_Module {
    
    public function __construct($isCounting = false) {

        /* @ticket #15646 */
        self::setIsCounting($isCounting);

        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_CLASSIFIED ) ) {
            wp_safe_redirect( '/' );
        }
        
        parent::__construct();
    }

    public function search() {
        $arr_params = array(
            'post_type'         => Apollo_DB_Schema::_CLASSIFIED,
            'posts_per_page'    => self::getIsCounting() ? 1 : Apollo_App::aplGetPageSize($this->pagesize , Apollo_DB_Schema::_CLASSIFIED_NUM_ITEMS_LISTING_PAGE),
            'paged'             => $this->page,
            'post_status'       => array('publish'),

        );

        if($keyword = self::getKeyword()) {
            $arr_params['s'] =  $keyword;
        }

        if(isset($_GET['term']) && !empty($_GET['term'])) {
            $arr_tax_query[] = array(
                'taxonomy'=> Apollo_DB_Schema::_CLASSIFIED.'-type',
                'terms' => array($_GET['term']),
            );
            $arr_params['tax_query'] = $arr_tax_query;
        }


        // Set new offset if this is ajax action
        $this->addOffsetToParams($arr_params);

        //No need search in this time
        add_filter('posts_where', array(__CLASS__, 'filter_where_tbl'), 10, 1);
        add_filter('posts_join', array(__CLASS__, 'filter_join_tbl'), 10, 1);
        add_filter('posts_orderby', array(__CLASS__, 'filter_order_tbl'), 10, 1);
        add_filter( 'posts_search', array(__CLASS__, 'posts_search'), 10, 1);
        add_filter( 'posts_groupby', array($this, 'filter_groupby'), 10, 1 );
        
        $this->result = query_posts($arr_params);

        remove_filter('posts_orderby', array(__CLASS__, 'filter_order_tbl'), 10);
        remove_filter('posts_join', array(__CLASS__, 'filter_join_tbl'), 10);
        remove_filter('posts_where', array(__CLASS__, 'filter_where_tbl'), 10);
        remove_filter( 'posts_search', array(__CLASS__, 'posts_search'), 10, 1);
        remove_filter( 'posts_groupby', array($this, 'filter_groupby'), 10, 1 );

        $this->total = $GLOBALS['wp_query']->found_posts;
        $this->total_pages = ceil($this->getTotal() / $this->pagesize);


        $this->template = APOLLO_TEMPLATES_DIR. '/classified/listing-page/'. $this->_get_template_name(of_get_option(Apollo_DB_Schema::_CLASSIFIED_DEFAULT_VIEW_TYPE));
        $this->resetPostData();
    }

    public static function filter_where_tbl ($where) {
        global $wpdb;
        $_current_date      = current_time( 'Y-m-d' );
        $sqlString = "AND  ".$wpdb->posts.".ID IN (
                    SELECT em.apollo_classified_id
                    FROM ".$wpdb->{Apollo_Tables::_APL_CLASSIFIED_META}." em
                    WHERE em.apollo_classified_id  = ".$wpdb->posts.".ID
                        AND em.meta_key = '".Apollo_DB_Schema::_APL_CLASSIFIED_EXP_DATE."'
                        AND em.meta_value >= '{$_current_date}'
                )";

        //query by city
        /**
         *  Because city input by user, they can input special character.
         *  We use id for param to search correct
         */
        if(isset($_GET['city']) && !empty($_GET['city'])) {
            $city = urldecode($_GET['city']);
                $sqlString .= 'AND  '.$wpdb->posts.'.ID IN (
                    SELECT em.apollo_classified_id
                    FROM '.$wpdb->{Apollo_Tables::_APL_CLASSIFIED_META}.' em
                    WHERE em.apollo_classified_id  = '.$wpdb->posts.'.ID
                        AND em.meta_value  REGEXP BINARY  \'.*"_classified_city";s:[0-9]+:"'.$city.'".*\'
                        AND em.meta_key = "'.Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS.'"
                )';

        }
        //query by State
        if(isset($_GET['state']) && !empty($_GET['state'])) {
            $state = $_GET['state'];
            $sqlString .= 'AND  '.$wpdb->posts.'.ID IN (
                SELECT em.apollo_classified_id
                FROM '.$wpdb->{Apollo_Tables::_APL_CLASSIFIED_META}.' em
                WHERE em.apollo_classified_id  = '.$wpdb->posts.'.ID
                    AND em.meta_value LIKE \'%'.$state.'%\'
                    AND em.meta_key = "'.Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS.'"
            )';
        }

        //query by zip
        if(isset($_GET['zip']) && !empty($_GET['zip'])) {
            $zip =  urldecode($_GET['zip']);
                $sqlString .= 'AND  '.$wpdb->posts.'.ID IN (
                    SELECT em.apollo_classified_id
                    FROM '.$wpdb->{Apollo_Tables::_APL_CLASSIFIED_META}.' em
                    WHERE em.apollo_classified_id  = '.$wpdb->posts.'.ID
                        AND em.meta_value  REGEXP BINARY  \'.*"_venue_zip";s:[0-9]+:"'.$zip.'".*\'
                        AND em.meta_key = "'.Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS.'"
                )';

        }

        if(isset($_GET['region']) && !empty($_GET['region'])) {
            $region = urldecode($_GET['region']);
            $wpdb->escape_by_ref($region);

            $sqlString .= 'AND  ' . $wpdb->posts . '.ID IN (
                    SELECT em.apollo_classified_id
                    FROM ' . $wpdb->{Apollo_Tables::_APL_CLASSIFIED_META} . ' em
                    WHERE em.apollo_classified_id  = ' . $wpdb->posts . '.ID
                        AND em.meta_value  REGEXP BINARY  \'.*"_classified_region";s:[0-9]+:"' . $region . '".*\'
                        AND em.meta_key = "' . Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS . '"
                )';
        }

        //query by custom-accessbility
        if(isset($_GET['custom']) && !empty($_GET['custom'])) {
            $customs = $_GET['custom'];

            if(is_array($customs) && count($customs)>0){
                $and = 'AND';
                foreach($customs as $custom){
                    $sqlString .= $and.'  '.$wpdb->posts.'.ID IN (
                        SELECT em.apollo_classified_id
                        FROM '.$wpdb->{Apollo_Tables::_APL_CLASSIFIED_META}.' em
                        WHERE em.apollo_classified_id  = '.$wpdb->posts.'.ID
                            AND em.meta_value  LIKE "%'.$custom.'%"
                            AND em.meta_key = "'.Apollo_DB_Schema::_VENUE_ACCESSIBILITY.'"
                    )';
                    //$and = 'OR';
                }
            }
        }


        return $where .= $sqlString;
    }

    public function get_display_type_url( $onepage = '' ) {
        global $wp_rewrite;

        $template = isset( $_REQUEST['view'] ) ? $_REQUEST['view'] : 'table';

        $old_request = add_query_arg(array(
            'view' => $template,
            'onepage' => $onepage,
            'type' => $this->type,
        ));


        $old_request = remove_query_arg('paged', $old_request);

        $qs_regex = '|\?.*?$|';
        preg_match( $qs_regex, $old_request, $qs_match );

        if ( !empty( $qs_match[0] ) ) {
            $query_string = $qs_match[0];
            $old_request = preg_replace( $qs_regex, '', $old_request );
        } else {
            $query_string = '';
        }

        $old_request = preg_replace( "|$wp_rewrite->pagination_base/\d+/?$|", '', $old_request);
        $old_request = preg_replace( '|^' . preg_quote( $wp_rewrite->index, '|' ) . '|i', '', $old_request);

        return untrailingslashit(get_bloginfo('url')) . $old_request. $query_string;
    }

    public static function filter_join_tbl ($join) {
        global $wpdb;
        $postTable = $wpdb->posts;
        $join .= self::getJoinTaxByKeyword();
        if(isset($_GET['orderby']) && !empty($_GET['orderby']))  {
            $orderby = urldecode($_GET['orderby']);
            switch ($orderby){

                case 'organization':
                    $join .= "LEFT JOIN ({$wpdb->{Apollo_Tables::_APL_CLASSIFIED_META}} mt_classified
                                            INNER JOIN {$wpdb->posts} p_org ON  p_org.post_type = '".Apollo_DB_Schema::_ORGANIZATION_PT."'
                                             AND mt_classified.meta_key ='". Apollo_DB_Schema::_APOLLO_CLASSIFIED_ORGANIZATION ."'
                                            AND mt_classified.meta_value = p_org.ID )
                                              ON mt_classified.apollo_classified_id = $postTable.ID" ;
                    break;
                case 'city':
                    $join .= "LEFT JOIN {$wpdb->{Apollo_Tables::_APL_CLASSIFIED_META}} mt_classified ON mt_classified.meta_key ='". Apollo_DB_Schema::_APL_CLASSIFIED_CITY ."'
                    AND mt_classified.apollo_classified_id = $postTable.ID   ";
                    break;
                case 'deadline':
                    $join .= "LEFT JOIN {$wpdb->{Apollo_Tables::_APL_CLASSIFIED_META}} mt_classified ON mt_classified.meta_key ='". Apollo_DB_Schema::_APL_CLASSIFIED_DEADLINE_DATE ."'
                    AND mt_classified.apollo_classified_id = $postTable.ID ";
                    break;
                default :
                    $join .= ''  ;

            }

        }

        
        return $join;
    }

    /* last name is not query with city in same query: Good to know that. because we don't need join an extra query to sort by last_name  */
    public static function filter_order_tbl ($order) {

        if (self::getIsCounting()) {
            return false;
        }

        if(isset($_GET['orderby']) && !empty($_GET['orderby']))  {

            $orderby = urldecode($_GET['orderby']);
            $sort = urldecode($_GET['order']);
            switch ($orderby){
                case 'post_date':
                    $order = $orderby.' '.$sort;
                    break;
                case 'post_title':
                    $order = $orderby.' '.$sort;
                    break;
                case 'organization':
                    $order = 'p_org.post_title '.$sort;
                    break;
                case 'city':
                    $order = 'mt_classified.meta_value '.$sort;
                    break;
                case 'deadline':
                    $order = 'mt_classified.meta_value '.$sort;
                    break;
                default :
                    $order = 'post_title ASC' ;

            }

        }
        return $order;
    }

}
