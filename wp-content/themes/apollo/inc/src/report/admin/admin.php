<?php

/**
 * Class APL_Admin_Report
 *
 * @ticket #11252
 */
class APL_Admin_Report {

    /**
     * Page hook for the options screen
     *
     * @type string
     */
    protected $options_screen = null;

    /**
     * Save notice info
     * @type string
     */
    protected $notice = null;

    protected $should_update_newoptions = false;
    protected $newoptions = array();


    public function __construct() {
        // Modify this setting here: http://apollo-theme.elidev.info/wp-admin/network/site-settings.php?id={$SITE_ID}
        $enableReport = Apollo_App::get_network_enable_reports( get_current_blog_id() );
        if ( intval($enableReport) === 1 ) {
            // Add the options page and menu item.
            add_action( 'admin_menu', array( $this, 'add_menu' ) );
        }
    }

    function add_menu() {
        /**
         * Menu position: https://developer.wordpress.org/reference/functions/add_menu_page/#menu-structure
         */
        add_menu_page(
            __('Reports', 'apollo'),
            __('Reports', 'apollo'),
            'administrator',
            'apollo-report',
            array($this, 'table'),
            home_url() . '/wp-content/themes/apollo/inc/admin/assets/images/aflag-icon.png',
            41 // add this menu before Appearance menu
        );
    }

    function table()
    {

        require_once( APOLLO_SRC_DIR . '/report/admin/inc/report-table.php' );
        $tableView = new APL_Admin_Report_Table();
        $tableView->prepare_items();
        ?>
        <div class="wrap">
            <div id="icon-users" class="icon32"></div>
            <h2>
                <?php _e('Event Report Data', 'apollo') ?>
            </h2>

            <div id="group-buttons-theme-tool" class="postbox" style="padding: 10px;">
                <p>
                    <?php _e('This report provides information about the event "click" activity on the following buttons', 'apollo') ?>:

                <p class="group-buttons">
                    - <?php _e('Official Website:', 'apollo');?><a href="/wp-admin/admin.php?page=apollo-report&only_official_web_clicks=1"> <?php echo sprintf(__('%s clicks', 'apollo'), $tableView->grandTotalOfficialWebsiteClicks) ?><a href="/wp-admin/admin.php?page=apollo-report&only_official_web_clicks=1"></a><br/>
                        - <?php _e('Buy Tickets:', 'apollo') ?><a href="/wp-admin/admin.php?page=apollo-report&only_buy_ticket_clicks=1"> <?php echo sprintf(__('%s clicks', 'apollo'), $tableView->grandTotalBuyTicketClicks);?></a><br/>
                        - <?php _e('Discounts:', 'apollo') ?><a href="/wp-admin/admin.php?page=apollo-report&only_discount_ticket_clicks=1"> <?php echo sprintf(__('%s clicks', 'apollo'), $tableView->grandTotalDiscountTicketClicks);?></a><br/>
                </p>

                <?php _e('This includes clicks on those buttons on the home, event category, event detail, organization detail, and venue detail pages.', 'apollo') ?>
                </p>
            </div>

            <?php
            if ( empty($tableView->errorMessage) ) {
                // create drop down filter by org
                if ( Apollo_App::is_avaiable_module(Apollo_DB_Schema::_ORGANIZATION_PT) ) {
                    $tableView->search_box(__('Filter', 'apollo'), 'admin-evt-org-drop');
                }
                ?>
                <form id="apollo-event-report-filter" method="get" action="/wp-admin/admin.php">
                    <input type="hidden" name="page" value="apollo-report" />
                    <?php
                    wp_nonce_field();
                    $tableView->display();
                    ?>
                </form>

                <?php
            } else {
                echo sprintf('<div class="error notice"><p>%s: %s</p></div>', __('Error', 'apollo'), $tableView->errorMessage);
            }
            ?>
        </div>
        <?php
    }
}

new APL_Admin_Report;