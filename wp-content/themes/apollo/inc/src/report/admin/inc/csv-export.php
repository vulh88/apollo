<?php

class APL_Admin_Report_CSV_Export {

    private $columnHeader = array();
    /**
     * @return array
     */
    public function getColumnHeader()
    {
        return $this->columnHeader;
    }

    /**
     * @param array $columnHeader
     */
    public function setColumnHeader($columnHeader)
    {
        $this->columnHeader = $columnHeader;
    }

    /**
     * @param array $data
     * @param $fileName
     * @return null
     */
    public function exportToCsv($data = array(), $fileName) {
        if (sizeof($data) == 0 ) {
            return null;
        }
        ob_clean();
        ob_start();
        header("Cache-Control: private");
        header("Content-Type: application/stream");
        header("Content-Disposition: attachment; filename={$fileName}");
        $df = fopen('php://output', 'w');

        fputcsv($df, $this->getColumnHeader());
        foreach ($data as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        ob_end_flush();
    }
}