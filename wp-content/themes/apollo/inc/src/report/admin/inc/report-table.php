<?php

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

/**
 * Class APL_Report_Table
 *
 * @ticket #11252
 */
class APL_Admin_Report_Table extends WP_List_Table
{
    private $perPage = 20;
    private $totalItems;
    private $offset = 0;

    public $grandTotalOfficialWebsiteClicks = 0;
    public $grandTotalBuyTicketClicks       = 0;
    public $grandTotalDiscountTicketClicks  = 0;

    private $totalOfficialWebsiteClicks = 0;
    private $totalBuyTicketClicks       = 0;
    private $totalDiscountTicketClicks  = 0;

    public $errorMessage = '';

    /**
     * MongoDB: https://docs.mongodb.com/manual/reference/operator/aggregation/sort/
     * sort = 1 to specify ascending order.   (ASC)
     * sort = -1 to specify descending order. (DESC)
     */
    const MONGODB_SORT_ASC  = 1;
    const MONGODB_SORT_DESC = -1;

    /**
     * Prepare the items for the table to process
     *
     * @return Void
     */
    public function prepare_items()
    {
        $hidden       = $this->get_hidden_columns();
        $sortable     = $this->get_sortable_columns();
        $currentPage  = $this->get_pagenum();
        $perPage      = $this->perPage;
        $this->offset = ($currentPage - 1) * $this->perPage;

        $data         = $this->table_data();
        $columns      = $this->get_columns();
        $this->process_bulk_action();

        $this->set_pagination_args( array(
            'total_items' => $this->totalItems,
            'per_page'    => $perPage
        ) );
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = $data;
    }

    /**
     * Override search_box function of WP_List_Table class to search by Organization drop down list
     *
     * @param string $text
     * @param string $input_id
     */
    public function search_box( $text, $input_id )
    {
        // get current selected organization
        if ( $selected = isset($_GET['org-id']) ? $_GET['org-id'] : '' ) {
            $org = get_post($selected);
        }
        ?>

        <form method="get">
            <input type="hidden" name="page" value="apollo-report" />
            <p class="search-box">
                <label class="screen-reader-text" for="<?php echo $input_id ?>"><?php echo $text; ?>:</label>
                <select name='org-id'
                        id="<?php echo $input_id; ?>"
                        data-value="<?php echo $selected ?>"
                        data-enable-remote="1"
                        data-post-type="<?php echo Apollo_DB_Schema::_ORGANIZATION_PT ?>"
                        data-source-url="apollo_get_remote_associate_data_to_select2_box"
                        class="select apl_select2">
                    <option value='' ><?php _e('Select Organization', 'apollo') ?></option >
                    <?php
                    if ( !empty($org) ) {
                        echo sprintf('<option selected="selected" value="%s">%s</option>', $org->ID, $org->post_title);
                    }
                    ?>
                </select>
                <?php submit_button( $text, 'button', false, false, array('id' => 'search-submit', 'style' => 'margin-top: 10px;') ); ?>
            </p>
        </form>
        <?php
    }

    /**
     * Override the parent columns method. Defines the columns to use in your listing table
     *
     * @return Array
     */
    public function get_columns()
    {
        $columns = array(
            'cb'            => '<input type="checkbox">',
            'item_id'                => __('Event ID', 'apollo'),
            'title'                  => __('Event Name', 'apollo'),
            'org_title'                  => __('Organization', 'apollo'),
            'official_web_clicks'    => sprintf(__('Official Website (%d)', 'apollo'), $this->totalOfficialWebsiteClicks),
            'buy_ticket_clicks'      => sprintf(__('Buy Tickets (%d)', 'apollo'), $this->totalBuyTicketClicks),
            'discount_ticket_clicks' => sprintf(__('Discounts (%d)', 'apollo'), $this->totalDiscountTicketClicks),
            'start_date'             => __('Start Date', 'apollo'),
            'end_date'               => __('End Date', 'apollo'),
            'updated_date'               => __('Last Updated', 'apollo'),
        );

        return $columns;
    }

    /**
     * Define which columns are hidden
     *
     * @return Array
     */
    public function get_hidden_columns()
    {
        return array();
    }

    /**
     * Define the sortable columns
     *
     * @return Array
     */
    public function get_sortable_columns()
    {
        return array(
            'title'      => array( 'title'     , false ),
            'org_title'      => array( 'org_title'     , false ),
            'start_date' => array( 'start_date', false ),
            'end_date'   => array( 'end_date'  , false ),
            'updated_date'   => array( 'updated_date'  , false ),
            'official_web_clicks'   => array( 'official_web_clicks'  , false ),
            'buy_ticket_clicks'   => array( 'buy_ticket_clicks'  , false ),
            'discount_ticket_clicks'   => array( 'discount_ticket_clicks'  , false ),
            'item_id'   => array( 'item_id'  , false ),
        );
    }

    /**
     * Get the table data
     *
     * @return Array
     */
    private function table_data()
    {
        $ins = apl_instance('APL_Lib_Report_Event');
        if ( is_null($ins->collection) ) {
            // can not connect to MongoDB
            $this->errorMessage = $ins->getConnectErrorMessage();
            return array();
        }

        // get data
        $orgID = empty($_GET['org-id']) ? '' : intval($_GET['org-id']);

        // get order info
        $orderBy = empty($_GET['orderby']) ? 'updated_date' : $_GET['orderby'];
        $order   = self::MONGODB_SORT_DESC;
        if ( !empty($_GET['order']) && $_GET['order'] == 'asc') {
            $order = self::MONGODB_SORT_ASC;
        }

        // set filter condition
        $filter = array(); // find all events
        if ( !empty($orgID) ) {
            $filter = array( 'org_id' => $orgID ); // find events based on org ID
        }

        // Set filter by only grand total
        if (!empty($_REQUEST['only_buy_ticket_clicks']) && $_REQUEST['only_buy_ticket_clicks']) {
            $filter['buy_ticket_clicks'] = array('$ne'  => 0);
        }
        if (!empty($_REQUEST['only_official_web_clicks']) && $_REQUEST['only_official_web_clicks']) {
            $filter['official_web_clicks'] = array('$ne'  => 0);
        }
        if (!empty($_REQUEST['only_discount_ticket_clicks']) && $_REQUEST['only_discount_ticket_clicks']) {
            $filter['discount_ticket_clicks'] = array('$ne'  => 0);
        }

        // find events based on filter condition
        $this->totalItems = $ins->collection->count($filter);

        $reports = $ins->collection->find(
            $filter,
            array(
                'limit' => $this->perPage,
                'sort'  => [$orderBy => $order],
                'skip'  => $this->offset,
            )
        );

        // Grant total
        $total = $ins->collection->aggregate(
            [
                ['$group' => [
                    '_id' => null,
                    'buy_ticket_clicks' => ['$sum' => '$buy_ticket_clicks'],
                    'official_web_clicks' => ['$sum' => '$official_web_clicks'],
                    'discount_ticket_clicks' => ['$sum' => '$discount_ticket_clicks'],
                ]
                ]
            ]
        );

        foreach($total as $t) {
            $this->grandTotalBuyTicketClicks = $t['buy_ticket_clicks'];
            $this->grandTotalDiscountTicketClicks = $t['discount_ticket_clicks'];
            $this->grandTotalOfficialWebsiteClicks = $t['official_web_clicks'];
            break;
        }


        $data = array();
        foreach ($reports as $report) {
            $this->totalOfficialWebsiteClicks += $report['official_web_clicks'];
            $this->totalBuyTicketClicks       += $report['buy_ticket_clicks'];
            $this->totalDiscountTicketClicks  += $report['discount_ticket_clicks'];

            $data[] = array(
                'item_id'                => $report['item_id'],
                'title'                  => $report['title'],
                'org_id'                  => $report['org_id'],
                'org_title'                  => $report['org_title'],
                'official_web_clicks'    => $report['official_web_clicks'],
                'buy_ticket_clicks'      => $report['buy_ticket_clicks'],
                'discount_ticket_clicks' => $report['discount_ticket_clicks'],
                'start_date'             => $report['start_date'],
                'end_date'               => $report['end_date'],
                'updated_date'               => $report['updated_date'],
            );
        }

        return $data;
    }

    /**
     * Define what data to show on each column of the table
     *
     * @param  Array $item        Data
     * @param  String $column_name - Current column name
     *
     * @return Mixed
     */
    public function column_default( $item, $column_name )
    {
        switch( $column_name ) {
            case 'item_id':
            case 'updated_date':
            case 'title':
            case 'org_title':
            case 'start_date':
            case 'end_date':
            case 'official_web_clicks':
            case 'buy_ticket_clicks':
            case 'discount_ticket_clicks':
                return $item[ $column_name ];
            default:
                return print_r( $item, true ) ;
        }
    }

    /**
     * Display item ID as an anchor tag
     *
     * @param array $item Item was stored in MongoDB
     *
     * @return string
     */
    public function column_item_id($item)
    {
        $editLink = get_edit_post_link($item['item_id']);
        $title    = $item['item_id'];

        return sprintf('<a target="_blank" href="%s">%s</a>', esc_url($editLink), $title);
    }

    /**
     * Display title as an anchor tag
     *
     * @param array $item Item was stored in MongoDB
     *
     * @return string
     */
    public function column_title($item)
    {
        $editLink = get_edit_post_link($item['item_id']);
        $title    = $item['title'];

        return sprintf('<a target="_blank" href="%s">%s</a>', esc_url($editLink), $title);
    }

    /**
     * Display ORG as an anchor tag
     *
     * @param array $item Item was stored in MongoDB
     *
     * @return string
     */
    public function column_org_title($item)
    {
        $editLink = get_edit_post_link($item['org_id']);
        $title    = $item['org_title'];

        return sprintf('<a target="_blank" href="%s">%s</a>', esc_url($editLink), $title);
    }

    function column_cb($item)
    {
        return sprintf(
            '<input type="checkbox" id="apl-report-event-%2$s" class="apl-event-id" name="%1$s[]" value="%2$s" />',
            'apl-export-ids',
            $item['item_id']
        );
    }

    function get_bulk_actions()
    {
        $actions = array(
            'apl_export_all'    => 'Export all data',
            'apl_export_selected'    => 'Export selected data',
        );

        return $actions;
    }

    function process_bulk_action()
    {

        if ($this->current_action() == 'apl_export_selected') {
            $this->downLoadFile();
        } else if ($this->current_action() == 'apl_export_all') {
            $this->downLoadFile(true);
        }

    }

    /**
     * Export events
     * @param bool $all
     */
    public function downLoadFile($all = false) {

        $ins = apl_instance('APL_Lib_Report_Event');
        if ($all) {
            $reports = $ins->collection->find(
                array(),
                array(
                    'sort'  => ['title' => 1]
                )
            );
        } else {
            $listIds = isset($_GET['apl-selected-event-all-page']) ? $_GET['apl-selected-event-all-page'] : '';
            if (empty($listIds)){
                return;
            }
            $listIds = array_map('intval', explode(',',$listIds));
            $reports = $ins->collection->find(
                array(
                    'item_id' => array( '$in' => $listIds)
                ),
                array(
                    'sort'  => ['title' => 1]
                )
            );
        }

        $data = $this->formatData($reports);

        require_once APOLLO_SRC_DIR . '/report/admin/inc/csv-export.php';
        $export = new APL_Admin_Report_CSV_Export();
        $export->setColumnHeader(
            array(
                'Event ID',
                'Name',
                'URL',
                'Buy Ticket Clicks',
                'Discount Ticket Clicks',
                'Official Website Clicks',
                'ORG ID',
                'ORG Name',
                'Start Date',
                'End Date'
            )
        );
        $export->exportToCsv($data, 'export-event.csv');
        die();
    }

    /**
     * Format to simple data
     * @param $reports
     * @return array
     */
    public function formatData($reports) {
        $data = array();
        foreach ($reports as $report) {

            $data[] = array(
                'item_id'                => $report['item_id'],
                'title'                  => $report['title'],
                'url'                    => $report['url'],
                'buy_ticket_clicks'      => $report['buy_ticket_clicks'],
                'discount_ticket_clicks' => $report['discount_ticket_clicks'],
                'official_web_clicks'    => $report['official_web_clicks'],
                'org_id'                 => $report['org_id'],
                'org_name'               => $report['org_title'],
                'start_date'             => $report['start_date'],
                'end_date'               => $report['end_date'],
            );
        }
        return $data;
    }

    protected function extra_tablenav($which ) {
        echo '<div class="apl-wrap-selected-event alignleft actions"><input type="button" class="button action apl-reset-selected-event" value="Reset selected"><span class="apl-count-event" data-text="'. __("Selected events", "apollo") .'"></span></div>';
    }
}
