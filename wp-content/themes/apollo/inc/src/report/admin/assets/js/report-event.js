(function($) {

    $(document).ready(function () {
        if (sessionStorage.getItem('apl-selected-event-ids') == null || sessionStorage.getItem('apl-selected-event-ids') == 'undefined') {
            sessionStorage.setItem('apl-selected-event-ids', '');
        }
        activeCheckbox();
        updateSeletedCount();
        $('#apollo-event-report-filter .apl-event-id').off('click').on('click', function (e) {
            var current = $(e.currentTarget);
            var currentEventIds = sessionStorage.getItem('apl-selected-event-ids');
            if (typeof current.attr('checked') !== 'undefined') {
                currentEventIds = handleUpdateEventIds(current.val(), currentEventIds, false);
            } else {
                currentEventIds = handleUpdateEventIds(current.val(), currentEventIds, true);
            }
            sessionStorage.setItem('apl-selected-event-ids', currentEventIds);
            updateSeletedCount();
        });

        $('#apollo-event-report-filter #cb-select-all-1, #apollo-event-report-filter #cb-select-all-2').off('click').on('click', function (e) {
            var current = $(e.currentTarget);
            var elements = $('#apollo-event-report-filter').find('.apl-event-id');
            var checked = true;
            if (typeof current.attr('checked') !== 'undefined') {
                checked = false;
            }
            var currentEventIds = sessionStorage.getItem('apl-selected-event-ids');
            if (elements.length > 0) {
                $.each(elements, function (index, item) {
                    currentEventIds = handleUpdateEventIds($(item).val(), currentEventIds, checked);
                });
            }
            sessionStorage.setItem('apl-selected-event-ids', currentEventIds);
            updateSeletedCount();
        });

        $('#apollo-event-report-filter #doaction, #apollo-event-report-filter #doaction2').off('click').on('click', function (e) {
            var current = $(e.currentTarget);
            var currentAction = current.closest('.bulkactions').find('#bulk-action-selector-top');
            var parent = current.closest('#apollo-event-report-filter');
            if (currentAction.length <= 0) {
                currentAction = current.closest('.bulkactions').find('#bulk-action-selector-bottom');
            }
            if (currentAction.length > 0) {
                if (currentAction.val() == 'apl_export_selected'){
                    parent.append('<textarea name="apl-selected-event-all-page" style="display: none;">'+sessionStorage.getItem('apl-selected-event-ids')+'</textarea>');

                }
            }

            parent.submit();
        });

        /** Clear session */
        $('#apollo-event-report-filter .apl-reset-selected-event').off('click').on('click', function (e) {
            sessionStorage.setItem('apl-selected-event-ids', '');
            location.reload();
        });

    });
    var handleUpdateEventIds = function (eventId, listIds, remove) {
        if (listIds != '') {
            listIds = listIds.split(',');
            var index = listIds.indexOf(eventId);
            if (remove && index >= 0) {
                listIds.splice(index, 1);
            }
            if (!remove && index < 0) {
                listIds.push(eventId);
            }
        } else {
            listIds = [eventId];
        }
        return listIds.length > 0 ? listIds.join(',') : '';
    };

    var activeCheckbox = function () {
        var listChecked = sessionStorage.getItem('apl-selected-event-ids');
        if (listChecked != 'undefined' && listChecked != '') {
            listChecked = listChecked.split(',');
            $.each(listChecked, function (index, item) {
                var currentItem = $('#apl-report-event-' + item);
                currentItem.attr('checked', 'checked');
            });
        }
    };

    var updateSeletedCount = function () {
        var ele = $('#apollo-event-report-filter .apl-wrap-selected-event .apl-count-event');
        if (ele.length > 0) {
            var eventCount = sessionStorage.getItem('apl-selected-event-ids');
            if (eventCount == '') {
                ele.html('');
            } else {
                eventCount = eventCount.split(',');
                ele.html('Selected ' + eventCount.length + ' events');
            }
        }
    }

}) (jQuery);