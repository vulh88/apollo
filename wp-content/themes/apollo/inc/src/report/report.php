<?php
require_once APOLLO_SRC_DIR. '/module.php';

class APL_Report  extends APL_Module {



    public function __construct()
    {
        parent::__construct();
    }

    /**
     * get dependencies function
     * Set required dependencies
     *
     * @return array dependencies
     */
    public function getDependencies()
    {
        return array(
            'admin',
            'admin/inc/csv-export.php'
        );
    }

    /**
     * get module name
     */
    public function getModuleName()
    {
        return 'report';
    }
}

new APL_Report;