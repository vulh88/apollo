<?php

class Apollo_Submit_Organization extends Apollo_Submit_Form {

    public function __construct($rule) {
        $this->post_type = Apollo_DB_Schema::_ORGANIZATION_PT;
        $this->rules = $rule;
    }



    public function save($arrData, $arrCustomField = array(), $arrCFSearch = array()) {

        $postStatus = Apollo_App::insertStatusRule(Apollo_DB_Schema::_ORGANIZATION_PT);
        if(isset($arrData['ID']) && $arrData['ID'] != ''){
            $postStatus = get_post_status($arrData['ID']);
        }

        $post = array(
            'ID'            => isset($arrData['ID'])?$arrData['ID']:'',
            'post_title'    => Apollo_App::clean_data_request( $arrData['post_title'] ),
            'post_content'  => Apollo_App::convertCkEditorToTinyCME($arrData['post_content']),
            'post_author'   => get_current_user_id(),
            'post_type'     => Apollo_DB_Schema::_ORGANIZATION_PT,
            'post_status'   =>$postStatus,
        );

        $postId = $post['ID'];
        if(isset($post['ID']) && !empty($post['ID'])){
            unset($post['post_author']);
            wp_update_post($post);
            
            // Keep icons due to it not exist in the front end form
            $arrData[Apollo_DB_Schema::_APL_ORG_POST_ICONS] = Apollo_App::apollo_get_meta_data($postId, Apollo_DB_Schema::_APL_ORG_POST_ICONS, Apollo_DB_Schema::_APL_ORG_DATA );
            // variable for checking new org is saved
            $isAddingNewItem = false;
            $org_venue = Apollo_App::apollo_get_meta_data($arrData['ID'],Apollo_DB_Schema::_APL_ORG_VENUE, Apollo_DB_Schema::_APL_ORG_DATA);
            if( !empty($org_venue )) {
                $arrData[Apollo_DB_Schema::_APL_ORG_VENUE] = $org_venue;
            }
        }else {
            $postId = wp_insert_post($post);
            // variable for checking new org is saved
            $isAddingNewItem = true;
        }
        $icons = Apollo_App::apollo_get_meta_data($postId, Apollo_DB_Schema::_APL_ORG_POST_ICONS, Apollo_DB_Schema::_APL_ORG_DATA );

        if(isset($arrData[Apollo_DB_Schema::_APL_ORG_TERM])){
            wp_set_post_terms( $postId, $arrData[Apollo_DB_Schema::_APL_ORG_TERM],Apollo_DB_Schema::_ORGANIZATION_PT.'-type' );
        }
        else{
            wp_set_post_terms( $postId, '' ,Apollo_DB_Schema::_ORGANIZATION_PT.'-type' );
        }

        unset($arrData[Apollo_DB_Schema::_APL_ORG_TERM]);
        $arrayMetaGroup = $this->getMetaGroup($arrData,$postId);
        $arrayMetaGroup[Apollo_DB_Schema::_APL_ORG_DATA][ Apollo_DB_Schema::_APL_ORG_POST_ICONS] = $icons;

        foreach($arrayMetaGroup as $group => $value){
            if($group != Apollo_DB_Schema::_APL_ORG_TERM){
                //meta
                update_apollo_meta($postId, $group, Apollo_App::clean_array_data($value));
            }
        }

        /** @Ticket #13028 */
        update_apollo_meta($postId, Apollo_DB_Schema::_ORG_PHONE, !empty($arrData[Apollo_DB_Schema::_ORG_PHONE]) ? $arrData[Apollo_DB_Schema::_ORG_PHONE] : '');

        //update custom field
        $group_fields = Apollo_Custom_Field::get_group_fields( Apollo_DB_Schema::_ORGANIZATION_PT );
        $customFieldValue = array();
        if(is_array($group_fields) && !empty($group_fields)){
            foreach($group_fields as $group){
                if(isset($group['fields']) && !empty($group['fields'])){
                    foreach($group['fields'] as $field){
                        if(isset($arrData[$field->name])){
                            if(is_array( $arrData[$field->name])){
                                $customFieldValue[$field->name] = $arrData[$field->name];
                            }
                            elseif($field->cf_type == 'wysiwyg'){
                                $customFieldValue[$field->name] = Apollo_App::convertCkEditorToTinyCME($arrData[$field->name]);
                            }
                            else{
                                $customFieldValue[$field->name] = Apollo_App::clean_data_request($arrData[$field->name]);
                            }

                            if(Apollo_Custom_Field::has_explain_field($field)){
                                $exFieldName = $field->name.'_explain';
                                $customFieldValue[$exFieldName] = Apollo_App::clean_data_request($arrData[$exFieldName]);
                            }

                        } else if ($postId) {
                            $customFieldValue[$field->name] = Apollo_App::apollo_get_meta_data($postId, $field->name, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA);
                        }
                    }
                }
            }
        }
        update_apollo_meta($postId, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA, serialize($customFieldValue));

        // [START] - add or update organization photo or caption galleries
        $_GET['pid'] = $postId;
        $_GET['synchronous_save_data'] = 'true';
        $hasSubmittedFeaturedImageData = false;
        $hasSubmittedGalleryData = false;
        $photo_option = of_get_option(Apollo_DB_Schema::_ORGANIZATION_PHOTO_FORM_LOCATION, 1);
        if($photo_option == 2){
            $_GET['target'] = $arrData['target'];
            $hasSubmittedFeaturedImageData = true;
            $hasSubmittedGalleryData = true;
        }
        if((isset($arrData['target']) && !empty($arrData['target']))){
            $target = $arrData['target'];
        }
//        if(isset($_GET['caption_gallerys']) && !empty($_GET['caption_gallerys'])){
//            $_REQUEST['caption_gallerys'] = $arrData['caption_gallerys'];
//            $hasSubmittedGalleryData = true;
//        }
        $pid = intval($postId);
        // Thienld : handling for add new featured image and galleries from storage data on session.
        // $pidOnSessionStorage : check new or existing org instance in order to get correct featured image or gallery data on session storage.

        if( $hasSubmittedFeaturedImageData ) {

            /** @Ticket #12729 - Remove session */
            self::saveFeaturedImageHandler($pid, Apollo_DB_Schema::_ORGANIZATION_PT);
        }

        if( $hasSubmittedGalleryData ) {

            self::saveGalleryHandler($pid, $post['post_type']);

        }
        // [END] - add or update organization photo or caption galleries


        $this->savePrivateStateCity($postId);

        /**
         * Thienld : send email to confirm admin the new post is created
        **/


        if(has_action('apollo_email_confirm_new_item_created') && $isAddingNewItem){
            do_action('apollo_email_confirm_new_item_created',$postId,Apollo_DB_Schema::_ORGANIZATION_PT,false);
        }

        //Truonghn :Send mail to admin when the post is edited
        if(has_action('apollo_email_confirm_item_edited') && !$isAddingNewItem && !current_user_can('manage_options') && $postStatus == 'publish'){
            do_action('apollo_email_confirm_item_edited',$postId,Apollo_DB_Schema::_ORGANIZATION_PT,false);
        }

        /**
        * Thienld : handle store meta data for business additional fields
        */
        self::saveOrgBusinessFields($arrData, $pid);

        // Check if the check box is cloned data to venue is CHECKED
        if(isset($arrData[Apollo_DB_Schema::_APL_ORG_VENUE]) && $isAddingNewItem){
            // clone org data to Venue
            self::cloneOrgDataToVenue($arrData);
        }

        global $wp;
        $current_url = home_url(). '/'. add_query_arg(array(),$wp->request);
        wp_redirect(home_url() . '/clean-form?redirect=' . urlencode($current_url) . '&post_type='. Apollo_DB_Schema::_ORGANIZATION_PT .'&action=' . ($isAddingNewItem ? 'add-new' : 'update'));

        return $postId;
    }

    /***
     * @param array $args
     * Clone org data to venue when user check in 'My organization is also a venue.' checkbox field.
     * Only clone data only when this is new org data instance
     */
    public function cloneOrgDataToVenue($args = array()){
        $postVenue = array(
            'ID'            => '',
            'post_title'    => $args['post_title'],
            'post_content'  => Apollo_App::convertCkEditorToTinyCME($args['post_content']),
            'post_author'   => get_current_user_id(),
            'post_type'     => Apollo_DB_Schema::_VENUE_PT,
            'post_status'   =>'pending',
        );
        $postVenueId = wp_insert_post($postVenue);

        if(intval($postVenueId) > 0){
            $arrayMetaGroup = $this->getMetaGroup($args,$args['ID']);
            $arrayVenueMetaGroup = array();
            if (is_array($arrayMetaGroup) && !empty($arrayMetaGroup)) {
                foreach ($arrayMetaGroup as $group => $value) {
                    $arrayVenueMetaGroup[str_replace('org', 'venue', $group)] = $arrayMetaGroup[$group];
                    $arrayVenueMetaGroup[str_replace('org', 'venue', $group)] = array();
                    foreach ($arrayMetaGroup[$group] as $key => $v) {
                        if ($key == '_org_linked_in_url') {
                            $arrayVenueMetaGroup[str_replace('org', 'venue', $group)]['_venue_linkedin_url'] = $arrayMetaGroup[$group][$key];
                        } else {
                            $arrayVenueMetaGroup[str_replace('org', 'venue', $group)][str_replace('org', 'venue', $key)] = $arrayMetaGroup[$group][$key];
                        }
                    }
                }
            }
            //clone data to venue
            if( is_array($arrayVenueMetaGroup) && !empty($arrayVenueMetaGroup) )  {
                foreach($arrayVenueMetaGroup as $group => $value){
                    if($args[Apollo_DB_Schema::_APL_ORG_VENUE]){
                        update_metadata('apollo_'.Apollo_DB_Schema::_VENUE_PT,$postVenueId, $group, Apollo_App::clean_array_data($value));
                    }
                }
            }

        }
    }


    public static function saveOrgBusinessFields($arrData = array(), $postID){
        try{
            if( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_BUSINESS_PT)
               && (isset($arrData[Apollo_DB_Schema::_ORG_BUSINESS]) && $arrData[Apollo_DB_Schema::_ORG_BUSINESS] == 'yes')
            ){

                /* Add meta data org_restaurant to org-meta-data */
                if(isset($arrData[Apollo_DB_Schema::_ORG_RESTAURANT])){
                    update_apollo_meta($postID,Apollo_DB_Schema::_ORG_RESTAURANT,$arrData[Apollo_DB_Schema::_ORG_RESTAURANT]);
                } else {
                    update_apollo_meta($postID,Apollo_DB_Schema::_ORG_RESTAURANT,'');
                }

                //update custom field for business
                $bsPostID = Apollo_App::apollo_get_meta_data($postID,Apollo_DB_Schema::_APL_ORG_BUSINESS);

                /*@ticket #17907 [CF] 20181009 - Display business form fields on the FE ORG form*/
                update_apollo_meta($postID, Apollo_DB_Schema::_ORG_BUSINESS, $arrData[Apollo_DB_Schema::_ORG_BUSINESS]);
                if(!$bsPostID){
                    //add new business
                    $bsPostID = self::createBusinessPost($postID);
                    if(intval($bsPostID) > 0){
                        update_apollo_meta($postID, Apollo_DB_Schema::_APL_ORG_BUSINESS, $bsPostID);
                        update_apollo_meta($bsPostID, Apollo_DB_Schema::_APL_BUSINESS_ORG, $postID);
                    }
                }

                $locationInfo = array (
                    Apollo_DB_Schema::_APL_BUSINESS_HOURS => isset($arrData[Apollo_DB_Schema::_APL_BUSINESS_HOURS]) ?  $arrData[Apollo_DB_Schema::_APL_BUSINESS_HOURS] : '',
                    Apollo_DB_Schema::_APL_BUSINESS_RESERVATION => isset($arrData[Apollo_DB_Schema::_APL_BUSINESS_RESERVATION]) ?  $arrData[Apollo_DB_Schema::_APL_BUSINESS_RESERVATION] : '',
                    Apollo_DB_Schema::_APL_BUSINESS_OTHER_INFO => isset($arrData[Apollo_DB_Schema::_APL_BUSINESS_OTHER_INFO]) ?  $arrData[Apollo_DB_Schema::_APL_BUSINESS_OTHER_INFO] : '');

                update_apollo_meta($bsPostID,Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO, serialize( Apollo_App::clean_array_data( $locationInfo ) ) );

                // Business Type
                if(isset($arrData[Apollo_DB_Schema::_APL_BUSINESS_TYPE_DDL]) && !empty($arrData[Apollo_DB_Schema::_APL_BUSINESS_TYPE_DDL])){
                    wp_set_post_terms( $bsPostID, $arrData[Apollo_DB_Schema::_APL_BUSINESS_TYPE_DDL],Apollo_DB_Schema::_BUSINESS_PT.'-type' );
                }
                else{
                    wp_set_post_terms( $bsPostID, '' ,Apollo_DB_Schema::_BUSINESS_PT.'-type' );
                }

                // Business Custom Fields
                $group_fields = Apollo_Custom_Field::get_group_fields( Apollo_DB_Schema::_BUSINESS_PT );
                $customFieldValue = array();
                if(is_array($group_fields) && !empty($group_fields)){
                    foreach($group_fields as $group){
                        if(isset($group['fields']) && !empty($group['fields'])){
                            foreach($group['fields'] as $field){
                                if(isset($arrData[$field->name])){
                                    if(is_array( $arrData[$field->name])){
                                        $customFieldValue[$field->name] = $arrData[$field->name];
                                    }
                                    elseif($field->cf_type == 'wysiwyg'){
                                        $customFieldValue[$field->name] = Apollo_App::convertCkEditorToTinyCME($arrData[$field->name]);
                                    }
                                    else{
                                        $customFieldValue[$field->name] = Apollo_App::clean_data_request($arrData[$field->name]);
                                    }

                                    if(Apollo_Custom_Field::has_explain_field($field)){
                                        $exFieldName = $field->name.'_explain';
                                        $customFieldValue[$exFieldName] = Apollo_App::clean_data_request($arrData[$exFieldName]);
                                    }

                                }
                            }
                        }
                    }
                }
                update_apollo_meta($bsPostID, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA, serialize($customFieldValue));
                // Thienld : save business features meta data to individual meta-key in order to serve for searching
                $aplQuery = new Apl_Query(Apollo_Tables::_APOLLO_BUSINESS_META);
                $metaBSIDKey = "apollo_" . strtolower(Apollo_DB_Schema::_BUSINESS_PT) . "_id";
                $aplQuery->delete(" ".$metaBSIDKey. " = '" .$bsPostID . "' AND meta_key LIKE '" . Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY . '_%\'');
                if( isset($customFieldValue[Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY])
                    && !empty($customFieldValue[Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY])
                ) {
                    $selectedBSFeatures = $customFieldValue[Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY];
                    foreach($selectedBSFeatures as $f){
                        update_apollo_meta($bsPostID, Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY . '_' . $f, $f);
                    }
                }
            }
        } catch(Exception $ex){
            aplDebugFile($ex->getMessage(),"Function: saveOrgBusinessFields");
        }
    }

    public static function save_org()
    {
        // check pid belong to this user first
        $pid = isset($_GET['pid']) ? $_GET['pid'] : '';

        if ($pid === '') {
            wp_die();
        }
        $pid = intval($pid);

        $currentAction = Apollo_Const::_APL_NONCE_ACTION_ORGANIZATION_PHOTO_PAGE;
        if ( isset($_REQUEST['current_page']) && $_REQUEST['current_page'] == 'apollo_artist_gallery_page' ) {
            $currentAction = Apollo_Const::_APL_NONCE_ACTION_ARTIST_GALLERY_PAGE;
        }

        // ticket #10927: validate nonce from AJAX submit
//        if ( !isset($_REQUEST['_aplNonceField']) || !wp_verify_nonce($_REQUEST['_aplNonceField'], $currentAction) ) {
//            wp_send_json(array(
//                'status' => 'failed',
//            ));
//        }

        // Thienld : handling for add new featured image and galleries from storage data on session.
        // $pidOnSessionStorage : in this case is only for existing organization profile.
        // Because this case is called when org profile is created completely.

        /** @Ticket: #12729 */
        $p = get_post($pid);

        self::saveFeaturedImageHandler($pid, $p->post_type);

        /** get gallery info */

        self::saveGalleryHandler($pid, $p->post_type);

        if(!isset($_GET['synchronous_save_data'])){
            wp_send_json(array(
                'status' => 'ok',
            ));
        }
    }

    private function getMetaGroup($arrData,$postId){
        $isAddNewStateZip = isset($_POST['add_new_state']) && $_POST['add_new_state'] == 1;
        $cityVal = $isAddNewStateZip ? $arrData[Apollo_DB_Schema::_APL_PRI_CITY] : $arrData[Apollo_DB_Schema::_ORG_CITY];
        $stateVal = $isAddNewStateZip ? $arrData[Apollo_DB_Schema::_APL_PRI_STATE] : $arrData[Apollo_DB_Schema::_ORG_STATE];
        $zipVal = $isAddNewStateZip ? $arrData[Apollo_DB_Schema::_APL_PRI_ZIP] : $arrData[Apollo_DB_Schema::_ORG_ZIP];

        /** @Ticket #19640 */
        if (isset($arrData[Apollo_DB_Schema::_APL_ORG_TMP_STATE])) {
            $otherState = sanitize_text_field($arrData[Apollo_DB_Schema::_APL_ORG_TMP_STATE]);
        } else {
            $otherState = Apollo_App::apollo_get_meta_data($postId, Apollo_DB_Schema::_APL_ORG_TMP_STATE, Apollo_DB_Schema::_APL_ORG_ADDRESS);
        }

        if (isset($arrData[Apollo_DB_Schema::_APL_ORG_TMP_CITY])) {
            $otherCity = sanitize_text_field($arrData[Apollo_DB_Schema::_APL_ORG_TMP_CITY]);
        } else {
            $otherCity = Apollo_App::apollo_get_meta_data($postId, Apollo_DB_Schema::_APL_ORG_TMP_CITY, Apollo_DB_Schema::_APL_ORG_ADDRESS);
        }

        if (isset($arrData[Apollo_DB_Schema::_APL_ORG_TMP_ZIP])) {
            $otherZip = sanitize_text_field($arrData[Apollo_DB_Schema::_APL_ORG_TMP_ZIP]);
        } else {
            $otherZip = Apollo_App::apollo_get_meta_data($postId, Apollo_DB_Schema::_APL_ORG_TMP_ZIP, Apollo_DB_Schema::_APL_ORG_ADDRESS);
        }
        
        $arrayMetaGroup[Apollo_DB_Schema::_APL_ORG_ADDRESS] = array(
            Apollo_DB_Schema::_ORG_ADDRESS1 => $arrData[ Apollo_DB_Schema::_ORG_ADDRESS1 ],
            Apollo_DB_Schema::_ORG_ADDRESS2 => $arrData[Apollo_DB_Schema::_ORG_ADDRESS2],
            Apollo_DB_Schema::_ORG_CITY => $cityVal,
            Apollo_DB_Schema::_ORG_STATE => $stateVal,
            Apollo_DB_Schema::_ORG_ZIP => $zipVal,
            Apollo_DB_Schema::_APL_ORG_TMP_STATE => $otherState,
            Apollo_DB_Schema::_APL_ORG_TMP_CITY => $otherCity,
            Apollo_DB_Schema::_APL_ORG_TMP_ZIP => $otherZip,
            Apollo_DB_Schema::_ORG_REGION => isset($arrData[Apollo_DB_Schema::_ORG_REGION])? $arrData[Apollo_DB_Schema::_ORG_REGION] : Apollo_App::apollo_get_meta_data($postId, Apollo_DB_Schema::_ORG_REGION, Apollo_DB_Schema::_APL_ORG_ADDRESS ),

        );

        $_POST[Apollo_DB_Schema::_ORG_STATE] = $stateVal;
        $_POST[Apollo_DB_Schema::_ORG_CITY] = $cityVal;
        $_POST[Apollo_DB_Schema::_ORG_ZIP] = $zipVal;
        
        unset(
            $arrData['id'],
            $arrData['post_title'],
            $arrData['post_content'],
            $arrData['post_author'],
            $arrData['post_type'],
            $arrData['post_status'],
            $arrData[Apollo_DB_Schema::_ORG_ADDRESS1 ],
            $arrData[Apollo_DB_Schema::_ORG_ADDRESS2 ],
            $arrData[Apollo_DB_Schema::_ORG_CITY],
            $arrData[Apollo_DB_Schema::_ORG_STATE],
            $arrData[Apollo_DB_Schema::_ORG_ZIP],
            $arrData[Apollo_DB_Schema::_APL_ORG_TMP_STATE],
            $arrData[Apollo_DB_Schema::_APL_ORG_TMP_CITY],
            $arrData[Apollo_DB_Schema::_APL_ORG_TMP_ZIP],
            $arrData[Apollo_DB_Schema::_ORG_REGION],
            $arrData[Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY],
            $arrData[Apollo_DB_Schema::_APL_BUSINESS_HOURS],
            $arrData[Apollo_DB_Schema::_APL_BUSINESS_RESERVATION],
            $arrData[Apollo_DB_Schema::_APL_BUSINESS_OTHER_INFO],
            $arrData[Apollo_Const::_APL_ORGANIZATION_NONCE_NAME],
            $arrData[Apollo_DB_Schema::_APL_BUSINESS_TYPE_DDL]

        );
        $arrayMetaGroup[Apollo_DB_Schema::_APL_ORG_DATA] = $arrData;
        return $arrayMetaGroup;

    }
    public function saveVideo($arrData, $arrCustomField = array(), $arrCFSearch = array()) {
        $data = array();
        /* Thienld : handle for case of allowing to add multiple org */
        if(Apollo_App::isInputMultiplePostMode()){
            if( intval(get_query_var( '_edit_post_id', 0)) === 0 ) {
                wp_redirect(site_url('/404'));
            }
        }
        if(isset($arrData['video_embed']) && is_array($arrData['video_desc']) ){
            foreach($arrData['video_embed'] as $k => $val){
                $arrDataItem = array(
                    'video_embed' => $val,
                    'video_desc' => base64_encode(Apollo_App::clean_data_request($arrData['video_desc'][$k]))
                );
                $data[] = $arrDataItem;
            }
        }
        update_apollo_meta( $arrData['ID'], Apollo_DB_Schema::_APL_ORG_VIDEO, serialize( $data ) );

    }
    public function saveAudio($arrData, $arrCustomField = array(), $arrCFSearch = array()) {
        $data = array();
        /* Thienld : handle for case of allowing to add multiple org */
        if(Apollo_App::isInputMultiplePostMode()){
            if( intval(get_query_var( '_edit_post_id', 0)) === 0 ) {
                wp_redirect(site_url('/404'));
            }
        }
        if(isset($arrData['embed']) && isset($arrData['desc']) && is_array($arrData['desc']) ){
            foreach($arrData['embed'] as $k => $val){
                $val = str_replace('\\','',$val);
                $arrDataItem = array(
                    'embed' => base64_encode($val),
                    'desc' => base64_encode(Apollo_App::clean_data_request($arrData['desc'][$k]))
                );
                $data[] = $arrDataItem;
            }
        }
        $dataSerialize = serialize($data);
        update_apollo_meta( $arrData['ID'], Apollo_DB_Schema::_APL_ORG_AUDIO, $dataSerialize );

    }

    public function isValidAll($arrData){
           return parent::isValidAll($arrData);
 	}

    public static function getFeaturedImageFromSession($pid = 0, $deleteSession = true){
        // $pid = 0 : new featured image for new org or target which is creating.
        // $pid > 0 for existing target
        $result = array();
        if(isset($_GET['target']) && !empty($_GET['target'])) {
            $targetFromRequest = $_GET['target'];
        } else if(isset($_POST['target']) && !empty($_POST['target'])) {
            $targetFromRequest = $_POST['target'];
        } else {
            $targetFromRequest = '';
        }
        $target = sanitize_text_field($targetFromRequest);
        $userDisplayName = $GLOBALS['current_user']->display_name;
        if(empty($target)) return $result;
        $sKey = $target . Apollo_SESSION::SUFFIX_ADD_PIMAGE;
        if(!isset($_SESSION['apollo'][$sKey])) return $result;
        if(empty($_SESSION['apollo'][$sKey])) return $result;
        if(!isset($_SESSION['apollo'][$sKey][$pid]) || empty($_SESSION['apollo'][$sKey][$pid])) return $result;
        $result = $_SESSION['apollo'][$sKey][$pid];
        $result['current_user_display_name'] = $userDisplayName;
        $result['target'] = $target;
        if($deleteSession){
            unset($_SESSION['apollo'][$sKey]);
        }
        return $result;
    }

    public static function getGalleryDataFromSession($pid, $deleteSession = true){
        // $pid = 0 : new galleries for new org or target which is creating.
        // $pid > 0 for existing target
        $result = array();
        if(isset($_GET['target']) && !empty($_GET['target'])) {
            $targetFromRequest = $_GET['target'];
        } else if(isset($_POST['target']) && !empty($_POST['target'])) {
            $targetFromRequest = $_POST['target'];
        } else {
            $targetFromRequest = '';
        }
        $target = sanitize_text_field($targetFromRequest);
        $userDisplayName = $GLOBALS['current_user']->display_name;
        if(empty($target)) return $result;
        $sKey = $target . Apollo_SESSION::SUFFIX_ADD_GALLERY;
        if(!isset($_SESSION['apollo'][$sKey])) return $result;
        if(empty($_SESSION['apollo'][$sKey])) return $result;
        if(!isset($_SESSION['apollo'][$sKey][$pid]) || empty($_SESSION['apollo'][$sKey][$pid])) return $result;
        if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'apollo_save_org' && isset($_SESSION['apollo'][$sKey][$pid]) && !isset($_REQUEST['caption_gallerys'])){
            unset($_SESSION['apollo'][$sKey][$pid]);
            return $result;
        }
        $result['galleriesStorageOnSession'] = $_SESSION['apollo'][$sKey][$pid];
        $result['current_user_display_name'] = $userDisplayName;
        $result['target'] = $target;
        $result['caption_galleries'] = isset($_REQUEST['caption_gallerys']) ? $_REQUEST['caption_gallerys'] : array();
        if($deleteSession){
            unset($_SESSION['apollo'][$sKey]);
        }
        return $result;
    }

    /**
     * @ticket #17907: [CF] 20181009 - Display business form fields on the FE ORG form
     * @param $orgID
     * @return int|WP_Error
     */
    public static function createBusinessPost($orgID){
        $orgObj = get_org($orgID);
        $post_arr = array(
            'post_title' => $orgObj->get_title(),
            'post_content' => $orgObj->get_full_content(),
            'post_status' => get_post_status($orgID),
            'post_type' => Apollo_DB_Schema::_BUSINESS_PT,
        );
        $bsID = wp_insert_post($post_arr);
        return $bsID;
    }

}