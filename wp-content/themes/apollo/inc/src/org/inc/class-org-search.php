<?php
require_once APOLLO_TEMPLATES_DIR. '/pages/lib/class-apollo-page-module.php';

class Apollo_Org_Page extends Apollo_Page_Module {
    
    public function __construct($isCounting = false) {

        /* @ticket #15646 */
        self::setIsCounting($isCounting);


        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ORGANIZATION_PT ) ) {
            wp_safe_redirect( '/' );
        }
        
        parent::__construct();
    }

    public function search() {
        $arr_params = array(
            'post_type'         => Apollo_DB_Schema::_ORGANIZATION_PT,
            'posts_per_page'    => self::getIsCounting() ? 1 : Apollo_App::aplGetPageSize($this->pagesize , Apollo_DB_Schema::_ORGANIZATION_NUM_ITEMS_LISTING_PAGE),
            'paged'             => $this->page,
            'post_status'       => array('publish'),

        );

        if($keyword = self::getKeyword()) {
            $arr_params['s'] =  $keyword;
        }

        if(isset($_GET['term']) && !empty($_GET['term'])) {
            $arr_tax_query[] = array(
                'taxonomy'=> Apollo_DB_Schema::_ORGANIZATION_PT.'-type',
                'terms' => array($_GET['term']),
            );
            $arr_params['tax_query'] = $arr_tax_query;
        }

        // Set new offset if this is ajax action
        $this->addOffsetToParams($arr_params);

         //No need search in this time
        add_filter('posts_where', array(__CLASS__, 'filter_where_org_tbl'), 10, 1);
        add_filter('posts_join', array(__CLASS__, 'filter_join_org_tbl'), 10, 1);
        add_filter('posts_orderby', array(__CLASS__, 'filter_order_org_tbl'), 10, 1);
        add_filter('posts_search', array(__CLASS__, 'posts_search'), 10, 1);
        add_filter('posts_groupby', array($this, 'filter_groupby'), 10, 1);
        
        $this->result = query_posts($arr_params);

        Apollo_Next_Prev::updateSearchResult($GLOBALS['wp_query']->request,Apollo_DB_Schema::_ORGANIZATION_PT);
        remove_filter('posts_orderby', array(__CLASS__, 'filter_order_org_tbl'), 10);
        remove_filter('posts_join', array(__CLASS__, 'filter_join_org_tbl'), 10);
        remove_filter('posts_where', array(__CLASS__, 'filter_where_org_tbl'), 10);
        remove_filter('posts_search', array(__CLASS__, 'posts_search'), 10, 1);
        remove_filter('posts_groupby', array($this, 'filter_groupby'), 10, 1);

        $this->setTotal($GLOBALS['wp_query']->found_posts);
        $this->total_pages = ceil($this->getTotal() / $this->pagesize);


        $this->template = APOLLO_TEMPLATES_DIR. '/org/listing-page/'. $this->_get_template_name(of_get_option(Apollo_DB_Schema::_ORGANIZATION_DEFAULT_VIEW_TYPE));
        $this->resetPostData();
    }

    public static function filter_where_org_tbl ($where) {
        global $wpdb;
        $sqlString = '';
        $enableRegionSelection = Apollo_App::enableMappingRegionZipSelection();

        //query by city
        /**
         *  Because city input by user, they can input special character.
         *  We use id for param to search correct
         */
        if (isset($_GET['city']) && !empty($_GET['city'])) {
            $city = $_GET['city'];
            $sqlString .= 'AND  ' . $wpdb->posts . '.ID IN (
                    SELECT em.apollo_organization_id
                    FROM ' . $wpdb->{Apollo_Tables::_APL_ORG_META} . ' em
                    WHERE em.apollo_organization_id  = ' . $wpdb->posts . '.ID
                        AND em.meta_value  REGEXP BINARY  \'.*"_org_city";s:[0-9]+:"' . $city . '".*\'
                        AND em.meta_key = "' . Apollo_DB_Schema::_APL_ORG_ADDRESS . '"
                )';

        }

        if(isset($_GET['region']) && !empty($_GET['region'])) {
            $region = urldecode($_GET['region']);
            $wpdb->escape_by_ref($region);

            /**
             * Get event ids in zipcodes region selection
             */
            if ($enableRegionSelection) {
                $ids = self::_getIDsByFilterSelectedRegion($region);
                $sqlString .= 'AND  '.$wpdb->posts.'.ID IN ('.implode(',', $ids).')';
            }
            else {
                $sqlString .= 'AND  ' . $wpdb->posts . '.ID IN (
                    SELECT em.apollo_organization_id
                    FROM ' . $wpdb->{Apollo_Tables::_APL_ORG_META} . ' em
                    WHERE em.apollo_organization_id  = ' . $wpdb->posts . '.ID
                        AND em.meta_value  REGEXP BINARY  \'.*"_org_region";s:[0-9]+:"' . $region . '".*\'
                        AND em.meta_key = "' . Apollo_DB_Schema::_APL_ORG_ADDRESS . '"
                )';
            }
        }


        //query by State
        if(isset($_GET['state']) && !empty($_GET['state'])) {
            $state = $_GET['state'];
            $sqlString .= 'AND  '.$wpdb->posts.'.ID IN (
                SELECT em.apollo_organization_id
                FROM '.$wpdb->{Apollo_Tables::_APL_ORG_META}.' em
                WHERE em.apollo_organization_id  = '.$wpdb->posts.'.ID
                    AND em.meta_value LIKE \'%'.$state.'%\'
                    AND em.meta_key = "'.Apollo_DB_Schema::_APL_ORG_ADDRESS.'"
            )';
        }

        //query by zip
        if (!$enableRegionSelection && isset($_GET['zip']) && !empty($_GET['zip'])) {
            $zip = $_GET['zip'];
            $sqlString .= 'AND  ' . $wpdb->posts . '.ID IN (
                    SELECT em.apollo_organization_id
                    FROM ' . $wpdb->{Apollo_Tables::_APL_ORG_META} . ' em
                    WHERE em.apollo_organization_id  = ' . $wpdb->posts . '.ID
                        AND em.meta_value  REGEXP BINARY  \'.*"_org_zip";s:[0-9]+:"' . $zip . '".*\'
                        AND em.meta_key = "' . Apollo_DB_Schema::_APL_ORG_ADDRESS . '"
                )';

        }

        /*Handle logic here */
        /**
         * TruongHN : remove  the organization, which has checked 'do not display'
         * checkbox from Organization Directory
         */
        $isTermPage = isset($_GET['term']);
        $isSearchPage = isset($_GET['s']);
        $isSearchListingPage = isset($_GET['keyword']);
        $isListPage = ( ((!$isTermPage && !$isSearchPage ) || $isSearchListingPage) );
        if($isListPage){
            $sqlString .= 'AND  ' . $wpdb->posts . '.ID NOT IN (
                    SELECT em.apollo_organization_id
                    FROM ' . $wpdb->{Apollo_Tables::_APL_ORG_META} . ' em
                    WHERE em.apollo_organization_id  = ' . $wpdb->posts . '.ID
                       AND em.meta_key = "' . Apollo_DB_Schema::_ORG_DO_NOT_DISPLAY . '"
                       AND em.meta_value = "yes"
                )';

        }

        return $where .= $sqlString;
    }

    public static function filter_join_org_tbl ($join) {
        $join .= self::getJoinTaxByKeyword();
        return $join;
    }

    /* last name is not query with city in same query: Good to know that. because we don't need join an extra query to sort by last_name  */
    public static function filter_order_org_tbl ($order) {

        // Ignore sort when counting result
        if (self::getIsCounting()) {
            return false;
        }

        global $wpdb;
        $order= $wpdb->posts.'.post_title ASC';
        return $order;
    }

    /**
     * Get IDs by selected regions
     *
     * @param $region
     * @return array
     */
    private static function _getIDsByFilterSelectedRegion($region){
        global $wpdb;
        if (!$zipcodes = Apollo_Seach_Form_Data::getRegionZipcodes($region)) {
            $zipcodes = array(false);
        }
        $zipQuery = '"'. implode('","', $zipcodes). '"';
        $sql = '    SELECT em.apollo_organization_id
                    FROM '.$wpdb->{Apollo_Tables::_APL_ORG_META}.' em
                    WHERE em.meta_value  IN('.$zipQuery.')
                        AND em.meta_key = "'.Apollo_DB_Schema::_ORG_ZIP.'"';

        $result = $wpdb->get_col($sql);

        return empty($result) ? array(-1) : $result;

    }
}
