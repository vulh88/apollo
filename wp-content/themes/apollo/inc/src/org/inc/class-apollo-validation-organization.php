<?php

class Apollo_Validation_Organization {
    public function __construct() {

    }

    public function isValid($value, $param, $dataHolder){
        $isNotExistingFImagePidInSession = (isset($_POST['_file']) && !empty($_POST['_file'])) ;
        $primary_required = Apollo_App::isEnableRequiredPrimaryImageByModuleName(Apollo_DB_Schema::_ORGANIZATION_PT);
        if(!$isNotExistingFImagePidInSession && $primary_required){
            return false;
        }
        return true;
    }
}