<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class ApolloOrganizationAjax {

    public function __construct()
    {
        $ajax_events = array(
            'preview_change_business' => true
        );

        foreach ($ajax_events as $ajax_event => $nopriv) {
            add_action('wp_ajax_apollo_' . $ajax_event, array($this, $ajax_event));

            if ($nopriv)
                add_action('wp_ajax_nopriv_apollo_' . $ajax_event, array($this, $ajax_event));
        }
    }

    /**
     * @ticket #18087: [CF] 20181030 - [ORG][Admin form] Preview Changes button for the business - item 4, 5
     */
    public function preview_change_business(){

        if (!empty($_REQUEST['business_data'])){
            session_start();
            $businessData = $_REQUEST['business_data'];

            $_SESSION['business_data'] = $businessData;

            wp_send_json(array(
                'status' => "success",
                "preview_link" => get_permalink($businessData["post_id"])
            ));

            return;
        }

        wp_send_json(array(
            'status' => "failed",
        ));
    }
}

new ApolloOrganizationAjax();
