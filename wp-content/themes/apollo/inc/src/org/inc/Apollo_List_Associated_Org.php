<?php

/**
 * Class Apollo_List_Associated_Org
 *
 * @ticket #11493
 */
class Apollo_List_Associated_Org
{
    /**
     * @var integer
     */
    protected $page;

    /**
     * @var integer
     */
    protected $perPage;

    /**
     * @var array
     */
    protected $orgs;

    /**
     * @var bool
     */
    protected $hasMore;

    /**
     * @var integer
     */
    protected $totalOrgs;

    /**
     * Apollo_List_Associated_Org constructor.
     *
     * @param array $themeToolAssociatedOrgs
     */
    public function __construct($themeToolAssociatedOrgs = array())
    {
        $this->page     = 1;
        $this->perPage  = -1;
        $this->hasMore  = false;

        $this->orgs      = $themeToolAssociatedOrgs;
        $this->totalOrgs = $this->orgs ? count($this->orgs) : 0;
    }

    /**
     * Set current page
     *
     * @param integer $page
     *
     * @return object
     */
    public function currentPage($page)
    {
        if ( !is_integer($page) ) {
            return $this;
        }

        $this->page = $page;

        return $this;
    }

    /**
     * Change current page size
     *
     * @param integer $size
     *
     * @return $this
     */
    public function changePerPage($size)
    {
        if ( !is_integer($size) ) {
            return $this;
        }

        $this->perPage = $size;

        return $this;
    }

    /**
     * Get associated organizations from Event Theme tool
     *
     * @param integer $termId
     *
     * @return array
     */
    public function getThemeToolAssociatedOrgs($termId = 0)
    {
        if ( $termId == 0 ) {
            return array();
        }

        // get term based on ID
        require APOLLO_TEMPLATES_DIR . '/taxonomy/inc/single.php';
        $term     = get_term($termId);
        $category = new Apollo_Single_Category($term);
        $data     = $category->themeToolClass->getThemeToolData();

        $this->orgs      = !is_array($data->associated_orgs)
                           ? maybe_unserialize($data->associated_orgs)
                           : $data->associated_orgs;
        $this->totalOrgs = count($this->orgs);

        return $this->orgs;
    }

    /**
     * Check is empty associated orgs of Event Theme Tool
     *
     * @return bool
     */
    public function isEmpty()
    {
        return $this->totalOrgs < 0;
    }

    /**
     * Check has more items
     *
     * @return bool
     */
    public function isShowMore()
    {
        if ($this->perPage <= 0) {
            return false;
        }
        $currentPosition = ($this->page * $this->perPage);
        return $currentPosition < $this->totalOrgs;
    }

    /**
     * Render associated organizations to HTML
     *
     * @return string
     */
    public function renderOrgHtml()
    {
        if ( count($this->orgs) < 0 ) {
            return '';
        }

        // get Org ID, sort by org title
        $associatedOrgs = get_posts(array(
            'posts_per_page' => $this->perPage,
            'offset'         => ($this->page - 1) * $this->perPage,
            'orderby'        => 'post_title',
            'order'          => 'ASC',
            'post__in'       => $this->orgs,
            'post_type'      => Apollo_DB_Schema::_ORGANIZATION_PT,
            'post_status'    => 'publish',
            'fields'         => 'ids'
        ));

        ob_start();
        /** @Ticket #13470 */
        include APOLLO_TEMPLATES_DIR . '/org/partial/_org-tpl.php';;
        return ob_get_clean();
    }
}
