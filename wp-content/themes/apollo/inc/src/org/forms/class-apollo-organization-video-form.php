<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/25/2015
 * Time: 4:47 PM
 */

class Apollo_Organization_Video_Form extends Apollo_Organization_Base_Form{
    public function formInit(){
        $arrData = $this->formData;
        $formElementItems = array(

            //organization
            //title
            //end title
            'ID' =>  array(
                'type' => 'Hidden',
                'name' => 'ID',
                'place_holder' =>__('ID','apollo'),
                'value' =>  $arrData['ID'],
                'class' => 'inp inp-txt',
                'title' =>__('Name','apollo'),
                'validate' => true,
                'container' => 'none'
            ),
            'video_Loop' =>  array(
                'type' => 'Loop',
                'place_holder' =>__('Name','apollo'),
                'value' =>  $arrData['ID'],
                'class' => 'inp inp-txt',
                'value' => $this->formData,
                'container' => 'el-blk',
                'children' => array(
                    //group 1
                    array(
                        'video_embed' => array(
                            'type' => 'YoutubeLink',
                            'name' => 'video_embed[]',
                            'place_holder' => __('YouTube or Vimeo URL"','apollo'),
                            'title' => __('Video embed','apollo'),
                            'class' => 'inp-txt-event',
                            'validate' => array(
                                'rule' => array(
                                    Apollo_Form::_FORM_REQUIRED,
                                    Apollo_Form::_FORM_YOUTUBE_LINK
                                )
                            )
                        ),
                        'video_desc' => array(
                            'type' => 'TextArea',
                            'name' => 'video_desc[]',
                            'place_holder' => __('Video Description','apollo'),
                            'title' => __('Video Description','apollo'),
                            'class' => 'desc-video'
                        ),
                    ),
                ),
                'control_button' => array(
                        //button 1
                        array(
                            'control_1' => array(
                                'type' =>   'Button',
                                'class'=>   'btn-noW add-new-group',
                                'title' => __(Apollo_Form_Static::AddMoreButtonLabel($this->formData),'apollo'),
                                'no_container' =>true,
                                'data'=> array(
                                    'data-add' => __('ADD VIDEO LINK','apollo'),
                                    'data-addmore' => __('ADD MORE LINKS','apollo'),
                                )
                            )
                        ),
                        //button 2
                        array(
                            'control_2' => array(
                                'id' => 'video-preview',
                                'type' =>   'Button',
                                'class'=>   'btn-noW btn-preview',
                                'title' => __('PREVIEW','apollo'),
                                'no_container' =>true,
                                'data' => array(
                                    'data-alert' => __('* Please input valid youtube or vimeo link.','apollo')
                                )
                            )
                        ),
                )
            ),
            //submit button
            'Submit_btn' => array(
                'type' => 'Submit',
                'class' => 'submit-btn',
                'title' => __('SUBMIT VIDEO','apollo')
            )
        );

        /*
         * page builder module
         * @Ticket 15203
         */
        if(has_filter("apl_pbm_fe_render_video_select_tags_section")) {
            $formElementItems = apply_filters('apl_pbm_fe_render_video_select_tags_section', [
                'formData' => $this->formData,
                'form'    => $formElementItems
            ]);
        }

        $this->elements = $formElementItems;
    }

    public function getMethod(){
        $id = Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_ORGANIZATION_PT);

        if(!empty($id)) {
            $orgVideoData =  get_apollo_meta($id,Apollo_DB_Schema::_APL_ORG_VIDEO,true);
            $videoData = @unserialize($orgVideoData);
            //format saved data same as post data
            $this->formatDataLoaded($videoData);
            $this->formData['video_Loop'] = $videoData;
            $this->formData['ID'] = $id;
        }
    }
    public function postMethod()
    {
        // validate nonce
        if ( $this->validateNonce() ) {
            $this->validateClassObject->saveVideo($this->formData);
            $this->isSaveSuccess = true;
            $this->isSubmitFail  = false;
        }
    }
    //override parent
    //Show message Fail Submit
    //Show message Success Submit
    public function formSubmitAction()
    {
        //@ticket #15971: Handle submit form
        do_action('apollo_submit_form');

        $this->isSaveSuccess = false;
        $this->isSubmitFail = false;
        //post method
        if($this->formRequestMethod == 'post'){
            $this->isSubmitFail = true;
            if( $this->validateClassObject->isValidAll($this->mergerPostRule())){
                $this->process();
            }
            if(!isset($_POST['video_embed'])||!$_POST['video_embed'] ){
                $this->process();
            }

            /*
             * @Ticket #15203
             * page builder module
             * save video tags
             */

            if(has_action('pbm_fe_save_video_tags')) {
                do_action('pbm_fe_save_video_tags', $this->formData['ID'], array('data' => $_POST));
            }
        }
    }
    protected function  process(){
        $this->formData = $_POST;
        //save data
        $this->postMethod();
        //end save date
    }
    protected function formatDataLoaded($videoData){
        $arrayData = array();
        if(is_array($videoData)){
            foreach($videoData as $k => $value){
                $desc = '';
                if(isset($value['video_desc'])){
                    $desc = base64_decode($value['video_desc']);
                }
                    $arrayData['video_embed'][] = isset($value['video_embed'])?$value['video_embed']:'';
                    $arrayData['video_desc'][] = $desc;
                }

        }
        $this->formData = $arrayData;

    }

    public function setCurDataHorizontalTab(){
        $this->activatedTab = APL_Dashboard_Hor_Tab_Options::ORG_VIDEO_URL;
        $isAgencyArea = intval(get_query_var('_is_agency_educator_page',0)) === 1;
        if($isAgencyArea){
            $this->activatedTab = APL_Dashboard_Hor_Tab_Options::AGENCY_ORG_VIDEO_URL;
        }
    }

    public function getTabData(){
        $tabData = array(
            'profile' => APL_Dashboard_Hor_Tab_Options::ORG_PROFILE_URL,
            'photo' => APL_Dashboard_Hor_Tab_Options::ORG_PHOTOS_URL,
            'audio' => APL_Dashboard_Hor_Tab_Options::ORG_AUDIO_URL,
            'video' => APL_Dashboard_Hor_Tab_Options::ORG_VIDEO_URL
        );
        $isAgencyArea = intval(get_query_var('_is_agency_educator_page',0)) === 1;
        if($isAgencyArea){
            $tabData = array(
                'profile' => APL_Dashboard_Hor_Tab_Options::AGENCY_ORG_PROFILE_URL,
                'photo' => APL_Dashboard_Hor_Tab_Options::AGENCY_ORG_PHOTOS_URL,
                'audio' => APL_Dashboard_Hor_Tab_Options::AGENCY_ORG_AUDIO_URL,
                'video' => APL_Dashboard_Hor_Tab_Options::AGENCY_ORG_VIDEO_URL
            );
        }
        return $tabData;
    }

    public function getTabLinks(){
        $tabData = $this->getTabData();
        $tabLinks = array(
            $tabData['profile'] => __("PROFILE","apollo"),
            $tabData['photo'] => __("PHOTO","apollo"),
            $tabData['audio'] => __("AUDIO","apollo"),
            $tabData['video'] => __("VIDEO","apollo")
        );
        $locationOrgPhotoForm = of_get_option(Apollo_DB_Schema::_ORGANIZATION_PHOTO_FORM_LOCATION, 1);
        if($locationOrgPhotoForm != 1){
            unset($tabLinks[$tabData['photo']]);
        }
        return $tabLinks;
    }

    /**
     * Set nonce info
     *
     * @return void
     */
    public function setNonceInfo()
    {
        $this->nonceName   = Apollo_Const::_APL_NONCE_NAME;
        $this->nonceAction = Apollo_Const::_APL_NONCE_ACTION_ORGANIZATION_VIDEO_PAGE;
    }
}