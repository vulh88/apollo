<?php

/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/7/2015
 * Time: 11:41 AM
 */
class Apollo_Organization_Form extends Apollo_Organization_Base_Form
{
    protected $isAddingNewOrg = false;

    /**
     * @return boolean
     */
    public function isIsAddingNewOrg()
    {
        return $this->isAddingNewOrg;
    }

    /**
     * @param boolean $isAddingNewOrg
     */
    public function setIsAddingNewOrg($isAddingNewOrg)
    {
        $this->isAddingNewOrg = $isAddingNewOrg;
    }

    /* Init form element then set data for them */
    public function formInit()
    {
        $validPhotoClass = Apollo_App::isEnableRequiredPrimaryImageByModuleName(Apollo_DB_Schema::_ORGANIZATION_PT) ? 'apollo_Validation_Organization' : '';
        $validPhotoClass = $validPhotoClass ? array($validPhotoClass) : '';
        $primary_title = $validPhotoClass ? 'PRIMARY IMAGE(*)' : 'PRIMARY IMAGE';

        global $wpdb;
        $stateTbl = $wpdb->{Apollo_Tables::_APL_STATE_ZIP};
        $arrData = $this->formData;
        $stateVal = '';
        $cityVal = '';
        if( $arrData){
            $stateVal = isset($arrData[Apollo_DB_Schema::_ORG_STATE]) ? $arrData[Apollo_DB_Schema::_ORG_STATE] : '';
            $cityVal = isset($arrData[Apollo_DB_Schema::_ORG_CITY]) ? $arrData[Apollo_DB_Schema::_ORG_CITY]: '';
        } else {
            $list_states = Apollo_App::getListState();
            $default_states =  of_get_option(Apollo_DB_Schema::_APL_DEFAULT_STATE);
            $cityVal = of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY);
            if( $default_states && in_array($default_states,$list_states) ) {
                $stateVal = $default_states;
            } else if(count($list_states) == 1) {
                $stateVal = key($list_states);
            }
        }

        $addNewStateType = isset($_POST['add_new_state']) ? $_POST['add_new_state'] : 0;
        if ($this->isSaveSuccess) {
            $addNewStateType = 0;
        } 
        
        $defaultSZRequired = $addNewStateType == 0;
        $defaultHidden = $addNewStateType == 1 ? 'hidden' : '';
        
        $defaultBtnText = $addNewStateType == 1 ? __('BACK TO DEFAULT STATE/CITY/ZIP DATA', 'apollo') : __('ADD NEW STATE/CITY/ZIP DATA', 'apollo');
    
        $addNewSZRequired = $addNewStateType == 1;
        $addNewSZStart = $addNewSZRequired == 1 ? '(*)' : '';
        $addNewHidden = $addNewStateType == 0 ? 'hidden' : '';

        /** @Ticket #13643 */
        $requiredEmail = of_get_option(Apollo_DB_Schema::_ORGANIZATION_ENABLE_EMAIL_REQUIREMENT, 0);
        $requiredPhone = of_get_option(Apollo_DB_Schema::_ORGANIZATION_ENABLE_PHONE_REQUIREMENT, 0);
        $requiredAddress = of_get_option(Apollo_DB_Schema::_ORGANIZATION_ENABLE_ADDRESS_REQUIREMENT, 0);
        $requiredState = $addNewStateType ? 0 : of_get_option(Apollo_DB_Schema::_ORGANIZATION_ENABLE_STATE_REQUIREMENT, 0);
        $requiredCity = $addNewStateType ? 0 : of_get_option(Apollo_DB_Schema::_ORGANIZATION_ENABLE_CITY_REQUIREMENT, 0);
        $requiredZip = $addNewStateType ? 0 : of_get_option(Apollo_DB_Schema::_ORGANIZATION_ENABLE_ZIP_REQUIREMENT, 0);
        $requiredClass = 'validate[required]';
         /*@ticket #18015: [CF] 20181022 - [ORG] - Add 'on/off' switch for FE Org types on org/business form - item 3*/
        $hideOrgTypes = !of_get_option(Apollo_DB_Schema::_ORGANIZATION_ENABLE_TYPES, 1) ? 'hidden' : '';

        /*@ticket #18084: [CF] 20181030 - [Business] Collapse/expand the business sub-category types by adding an 'arrow' next to each parent type - item 1*/
        add_filter('apl_add_collapse_expand_icon', function(){
            return '<span class="expander" data-toggle-class="show"></span>';
        });

        /**
         * @ticket #19642: [CF] 20190402 - Auto fill the 'contact information' name and email - Item 6
         */
        $contactName = isset($arrData[Apollo_DB_Schema::_ORG_CONTACT_NAME]) ? $arrData[Apollo_DB_Schema::_ORG_CONTACT_NAME] : '';
        $current_user = wp_get_current_user();
        if (!$contactName) {
            $contactName = !empty($current_user->display_name) ? $current_user->display_name : '';
        }

        $contactEmail = isset($arrData[Apollo_DB_Schema::_ORG_CONTACT_EMAIL]) ? $arrData[Apollo_DB_Schema::_ORG_CONTACT_EMAIL] : '';
        if (!$contactEmail) {
            $contactEmail = !empty($current_user->user_email) ? $current_user->user_email : '';
        }

        $enableOtherState = of_get_option(Apollo_DB_Schema::_ORGANIZATION_FE_ENABLE_OTHER_STATE, 0);
        $enableOtherCity = of_get_option(Apollo_DB_Schema::_ORGANIZATION_FE_ENABLE_OTHER_CITY, 0);
        $enableOtherZip = of_get_option(Apollo_DB_Schema::_ORGANIZATION_FE_ENABLE_OTHER_ZIP, 0);

        $formElementItems = array(
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),
                'ID' => array(
                    'type' => 'Hidden',
                    'name' => 'ID',
                    'place_holder' => __('Name', 'apollo'),
                    'value' => isset($arrData['ID']) ? $arrData['ID'] : '',
                    'class' => 'inp inp-txt',
                    'title' => __('Name', 'apollo'),
                    'validate' => true,
                    'container' => '',
                ),
                'post_status' => array(
                    'type' => 'Hidden',
                    'name' => 'post_status',
                    'place_holder' => __('Post status', 'apollo'),
                    'value' => isset($arrData['post_status']) ? $arrData['post_status'] : 'pending',
                    'class' => '',
                    'title' => __('Post status', 'apollo'),
                    'validate' => true,
                ),
                'post_title' => array(
                    'type' => 'Text',
                    'name' => 'post_title',
                    'place_holder' => __('Organization/Business Name (*) ', 'apollo'),
                    'value' => isset($arrData['post_title']) ? $arrData['post_title'] : '',
                    'class' => 'inp inp-txt validate[required]',
                    'data_attributes' => array(
                        'data-errormessage-value-missing="'.__('Organization/Business name is required','apollo').'"'
                    ),
                    'title' => __('Organization/Business Name', 'apollo'),
                    'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_REQUIRED
                        )
                    ),
                ),
                'post_content' => array(
                    'type' => 'Wysiwyg',
                    'name' => 'post_content',
                    'place_holder' => __('Description/Bio (*)', 'apollo'),
                    'value' => Apollo_App::convertTinyCMEToCkEditor(isset($arrData['post_content']) ? $arrData['post_content'] : ''),
                    'class' => 'inp-desc-event custom-validate-field',
                    'id' => 'org-biography-description',
                    'data_attributes' => array(
                        'data-custom-validate="wysiwyg"',
                        'data-error-message="'.__('Description/Bio is required','apollo').'"',
                        'data-id="org-biography-description"'
                    ),
                    'title' => __('Description/Bio (*)', 'apollo'),
                    'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_REQUIRED
                        )
                    ),
                ),

                Apollo_DB_Schema::_ORG_PHONE => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_ORG_PHONE,
                    'place_holder' => $requiredPhone ? __('Phone (*) ', 'apollo') : __('Phone', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_ORG_PHONE]) ? $arrData[Apollo_DB_Schema::_ORG_PHONE] : '',
                    'class' => sprintf('inp inp-txt %s', $requiredPhone ? $requiredClass : ''),
                    'data_attributes' => array(
                        'data-errormessage-value-missing="'.__('Phone is required','apollo').'"'
                    ),
                    'title' => $requiredPhone ? __('Phone (*)', 'apollo') : __('Phone', 'apollo'),
                    'validate' => array(
                        'rule' => array(
                            $requiredPhone ? Apollo_Form::_FORM_REQUIRED : ''
                        )
                    ),
                ),
                Apollo_DB_Schema::_ORG_FAX => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_ORG_FAX,
                    'place_holder' => __('Fax', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_ORG_FAX]) ? $arrData[Apollo_DB_Schema::_ORG_FAX] : '',
                    'class' => 'inp inp-txt',
                    'title' => __('Fax', 'apollo'),
                ),
                Apollo_DB_Schema::_ORG_EMAIL => array(
                    'type' => 'Text',
                    'name' => Apollo_DB_Schema::_ORG_EMAIL,
                    'place_holder' => $requiredEmail ? __('Email (*)', 'apollo') : __('Email', 'apollo'),
                    'value' => isset($arrData[Apollo_DB_Schema::_ORG_EMAIL]) ? $arrData[Apollo_DB_Schema::_ORG_EMAIL] : '',
                    'class' => sprintf('inp inp-txt %s validate[funcCall[disableSpace],custom[email]]', $requiredEmail ? $requiredClass : ''),
                    'data_attributes' => array(
                        'data-errormessage-value-missing="'.__('Email is required','apollo').'"'
                    ),
                    'title' => $requiredEmail ? __('Email (*)', 'apollo') : __('Email', 'apollo'),
                    'validate' => array(
                        'rule' => array(
                            Apollo_Form::_FORM_EMAIL,
                            $requiredEmail ? Apollo_Form::_FORM_REQUIRED : '',
                        )
                    )
                ),
            
            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),
            
            
            
            /** 
             * onwer website 
             * 
             **/
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),
                //title
                array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => __('Location Info', 'apollo'),
                    'class' => '',

                ),
                    
                    Apollo_DB_Schema::_ORG_ADDRESS1 => array(
                        'type' => 'Text',
                        'name' => Apollo_DB_Schema::_ORG_ADDRESS1,
                        'place_holder' => $requiredAddress ? __('Address 1 (*)', 'apollo') : __('Address 1', 'apollo'),
                        'value' => isset($arrData[Apollo_DB_Schema::_ORG_ADDRESS1]) ? $arrData[Apollo_DB_Schema::_ORG_ADDRESS1] : '',
                        'class' => sprintf('inp inp-txt %s', $requiredAddress ? $requiredClass : ''),
                        'data_attributes' => array(
                            'data-errormessage-value-missing="'.__('Address 1 is required','apollo').'"'
                        ),
                        'container' => 'hafl fl',
                        'title' => $requiredAddress ? __('Address 1 (*)', 'apollo') : __('Address 1', 'apollo'),
                        'validate' => array(
                            'rule' => array(
                                $requiredAddress ? Apollo_Form::_FORM_REQUIRED : ''
                            )
                        ),
                    ),
                    Apollo_DB_Schema::_ORG_ADDRESS2 => array(
                        'type' => 'Text',
                        'name' => Apollo_DB_Schema::_ORG_ADDRESS2,
                        'place_holder' => __('Address 2', 'apollo'),
                        'value' => isset($arrData[Apollo_DB_Schema::_ORG_ADDRESS2]) ? $arrData[Apollo_DB_Schema::_ORG_ADDRESS2] : '',
                        'class' => 'inp inp-txt',
                        'container' => 'hafl fr',
                        'title' => __('Address 2', 'apollo'),
                    ),
                    
                    Apollo_DB_Schema::_ORG_REGION => array(
                        'type' => 'Select',
                        'name' => Apollo_DB_Schema::_ORG_REGION,
                        'class' => 'event',
                        'title' => __('Region ', 'apollo'),
                        'value' => array(
                            'list_item' => Apollo_App::get_regions(false, false),
                            'selected_item' => isset($arrData[Apollo_DB_Schema::_ORG_REGION]) ? $arrData[Apollo_DB_Schema::_ORG_REGION] : ''
                        ),
                        'id' => 'apl-regions',
                        'display' => Apollo_App::showRegion(),
                        'container' => 'el-blk full '. (Apollo_App::enableMappingRegionZipSelection() ? ' hidden' : ''),
                    ), 
                    
                    'add_new_state' => array(
                        'type' => 'hidden',
                        'name' => 'add_new_state',
                        'value' => $addNewStateType
                    ),  
            
                    'add_new_state_btn' => array(
                        'type' => 'Button',
                        'class' => 'add-new-state-btn btn-noW',
                        'title' => $defaultBtnText,
                        'no_container' => true,
                        'data' => array(
                            'data-prev' => __('ADD NEW STATE/CITY/ZIP DATA', 'apollo'),
                            'data-next' => __('BACK TO DEFAULT STATE/CITY/ZIP DATA', 'apollo'),
                        ),
                    ),
                    array(
                        'type' => 'Div',
                        'name' => 'title',
                        'value' => '',
                        'class' => 'el-blk state-block-1 '. $defaultHidden,
                        'container' => '',
                    ),
                        Apollo_DB_Schema::_ORG_STATE => array(
                            'type' => 'Select',
                            'name' => Apollo_DB_Schema::_ORG_STATE,
                            'class' => sprintf('event apl-territory-state %s', $requiredState ? 'custom-validate-field' : ''),
                            'data_attributes' => array(
                                'data-custom-validate="select2"',
                                'data-error-message="'.__('State is required','apollo').'"'
                            ),
                            'title' => $requiredState ? __('State (*)', 'apollo') : __('State', 'apollo'),
                            'value' => array(
                                'list_item' => Apollo_App::getStateByTerritory(true, $stateVal),
                                'selected_item' => $stateVal
                            ),
                            'id' => 'apl-us-states',
                            'validate' => array(
                                'rule' => array(
                                    $requiredState ? Apollo_Form::_FORM_REQUIRED : ''
                                )
                            ),
                        ),

                       
                        Apollo_DB_Schema::_ORG_CITY => array(
                            'type' => 'Select',
                            'name' => Apollo_DB_Schema::_ORG_CITY,
                            'place_holder' => __('City (*)', 'apollo'),
                            'id' => 'apl-us-cities',
                            'container' => 'hafl fl',
                            'value' => array(
                                'list_item' => Apollo_App::getCityByTerritory(true, $stateVal, false),
                                'selected_item' => $cityVal
                            ),
                            'class' => sprintf('event apl-territory-city %s', $requiredCity ? 'custom-validate-field' : ''),
                            'data_attributes' => array(
                                'data-custom-validate="select2"',
                                'data-error-message="'.__('City is required','apollo').'"'
                            ),
                            'title' => $requiredCity ? __('City (*)', 'apollo') : __('City', 'apollo'),
                            'validate' => array(
                                'rule' => array(
                                    $requiredCity ? Apollo_Form::_FORM_REQUIRED : ''
                                )
                            ),
                        ),

                        Apollo_DB_Schema::_ORG_ZIP => array(
                            'type' => 'Select',
                            'name' => Apollo_DB_Schema::_ORG_ZIP,
                            'place_holder' => __('Zip (*) ', 'apollo'),
                            'container' => 'hafl fr',
                            'value' => array(
                                'list_item' => Apollo_App::getZipByTerritory(true, $stateVal, $cityVal, false),
                                'selected_item' => isset($arrData[Apollo_DB_Schema::_ORG_ZIP]) ? $arrData[Apollo_DB_Schema::_ORG_ZIP] : ''
                            ),
                            'id' => 'apl-us-zip',
                            'class' => sprintf('event apl-territory-zipcode %s', $requiredZip ? 'custom-validate-field' : ''),
                            'data_attributes' => array(
                                'data-custom-validate="select2"',
                                'data-error-message="'.__('ZipCode is required','apollo').'"'
                            ),
                            'title' => $requiredZip ? __('Zip (*)', 'apollo') : __('Zip', 'apollo'),
                            'validate' => array(
                                'rule' => array(
                                    $requiredZip ? Apollo_Form::_FORM_REQUIRED : ''
                                )
                            ),
                        ),

                );

                /** @ticket #19687
                 * [CF] 20190408 - Add text to create more separation between the address drop menus and the other address fields
                 */
                if ($enableOtherState || $enableOtherCity || $enableOtherZip) {
                    $formElementItems = array_merge($formElementItems, array(
                        array(
                            'type' => 'Subtitle',
                            'value' => __("If your city, state and/or zip are not listed above, please fill in the fields below.", 'apollo'),
                            'class' => 'apl-subtitle-bold',
                        )
                    ));
                }
                /** @Ticket #19640 */
                if ($enableOtherState) {
                    $formElementItems = array_merge($formElementItems, array(
                        Apollo_DB_Schema::_APL_ORG_TMP_STATE => array(
                            'type' => 'Text',
                            'name' => Apollo_DB_Schema::_APL_ORG_TMP_STATE,
                            'place_holder' => __('Other State', 'apollo'),
                            'value' => isset($arrData[Apollo_DB_Schema::_APL_ORG_TMP_STATE]) ? $arrData[Apollo_DB_Schema::_APL_ORG_TMP_STATE] : '',
                            'class' => 'inp inp-txt',
                            'title' => __('Other State', 'apollo'),
                        )
                    ));
                }

                if ($enableOtherCity) {
                    $formElementItems = array_merge($formElementItems, array(
                        Apollo_DB_Schema::_APL_ORG_TMP_CITY => array(
                            'type' => 'Text',
                            'name' => Apollo_DB_Schema::_APL_ORG_TMP_CITY,
                            'place_holder' => __('Other City', 'apollo'),
                            'value' => isset($arrData[Apollo_DB_Schema::_APL_ORG_TMP_CITY]) ? $arrData[Apollo_DB_Schema::_APL_ORG_TMP_CITY] : '',
                            'class' => 'inp inp-txt',
                            'title' => __('Other City', 'apollo'),
                        )
                    ));
                }

                if ($enableOtherZip) {
                    $formElementItems = array_merge($formElementItems, array(
                        Apollo_DB_Schema::_APL_ORG_TMP_ZIP => array(
                            'type' => 'Text',
                            'name' => Apollo_DB_Schema::_APL_ORG_TMP_ZIP,
                            'place_holder' => __('Other Zip Code', 'apollo'),
                            'value' => isset($arrData[Apollo_DB_Schema::_APL_ORG_TMP_ZIP]) ? $arrData[Apollo_DB_Schema::_APL_ORG_TMP_ZIP] : '',
                            'class' => 'inp inp-txt',
                            'title' => __('Other Zip Code', 'apollo'),
                        )
                    ));
                }

                $moreElementItems = array(
                    array(
                        'type' => 'CloseDiv',
                        'name' => 'title',
                        'value' => '',
                        'class' => '',
                        'container' => '',
                    ),    

            
                    array(
                        'type' => 'Div',
                        'name' => 'title',
                        'value' => '',
                        'class' => 'el-blk state-block-2 '. $addNewHidden,
                        'container' => '',
                    ),
                        Apollo_DB_Schema::_APL_PRI_STATE => array(
                            'type' => 'Text',
                            'name' => Apollo_DB_Schema::_APL_PRI_STATE,
                            'place_holder' => __('State (*)', 'apollo'),
                            'value' => isset($arrData[Apollo_DB_Schema::_APL_PRI_STATE]) ? $arrData[Apollo_DB_Schema::_APL_PRI_STATE] : '',
                            'class' => 'inp inp-txt validate[required]',
                            'data_attributes' => array(
                                'data-errormessage-value-missing="'.__('State is required','apollo').'"'
                            ),
                            'title' => sprintf(__('State %s', 'apollo'), $addNewSZStart),
                            'validate' => array(
                                'rule' => array(
                                    $addNewSZRequired ? Apollo_Form::_FORM_REQUIRED : '',
                                    //$addNewStateType == 1 ? Apollo_Form::_FORM_EXISTS. ':'. $stateTbl. ',code, type="state"' : '',
                                )
                            ),
                        ),

                        Apollo_DB_Schema::_APL_PRI_CITY => array(
                            'type' => 'Text',
                            'name' => Apollo_DB_Schema::_APL_PRI_CITY,
                            'place_holder' => __('City (*)', 'apollo'),
                            'value' => isset($arrData[Apollo_DB_Schema::_APL_PRI_CITY]) ? $arrData[Apollo_DB_Schema::_APL_PRI_CITY] : '',
                            'container' => 'hafl fl',
                            'class' => 'inp inp-txt validate[required]',
                            'data_attributes' => array(
                                'data-errormessage-value-missing="'.__('City is required','apollo').'"'
                            ),
                            'title' => sprintf(__('City %s', 'apollo'), $addNewSZStart),
                            'validate' => array(
                                'rule' => array(
                                    $addNewSZRequired ? Apollo_Form::_FORM_REQUIRED : ''
                                )
                            ),
                        ),

                        Apollo_DB_Schema::_APL_PRI_ZIP => array(
                            'type' => 'Text',
                            'name' => Apollo_DB_Schema::_APL_PRI_ZIP,
                            'place_holder' => __('Zip (*)', 'apollo'),
                            'value' => isset($arrData[Apollo_DB_Schema::_APL_PRI_ZIP]) ? $arrData[Apollo_DB_Schema::_APL_PRI_ZIP] : '',
                            'container' => 'hafl fr',
                            'class' => 'inp inp-txt validate[required]',
                            'data_attributes' => array(
                                'data-errormessage-value-missing="'.__('ZipCode is required','apollo').'"'
                            ),
                            'title' => sprintf(__('Zip %s', 'apollo'), $addNewSZStart),
                            'validate' => array(
                                'rule' => array(
                                    $addNewSZRequired ? Apollo_Form::_FORM_REQUIRED : ''
                                )
                            ),
                        ),

                    array(
                        'type' => 'CloseDiv',
                        'name' => 'title',
                        'value' => '',
                        'class' => '',
                        'container' => '',
                    ),
            
                array(
                    'type' => 'CloseDiv',
                    'name' => 'title',
                    'value' => '',
                    'class' => '',
                    'container' => '',
                ),
            

            /** 
             * onwer website 
             * 
             **/
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),
                //title
                array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => __('Website, Blog and Social Media Links', 'apollo'),
                    'class' => '',

                ),


                array(
                    'type' => 'Div',
                    'name' => 'title',
                    'value' => '',
                    'class' => 'el-blk',
                    'container' => '',
                ),
                    Apollo_DB_Schema::_ORG_WEBSITE_URL => array(
                        'type' => 'Text',
                        'name' => Apollo_DB_Schema::_ORG_WEBSITE_URL,
                        'place_holder' => __('Website URL', 'apollo'),
                        'value' => isset($arrData[Apollo_DB_Schema::_ORG_WEBSITE_URL]) ? $arrData[Apollo_DB_Schema::_ORG_WEBSITE_URL] : '',
                        'container' => 'hafl fl',
                        'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]]',
                        'title' => __('Website URL', 'apollo'),
                        'validate' => array(
                            'rule' => array(
                                Apollo_Form::_FORM_URL,

                            )
                        )
                    ),
                    Apollo_DB_Schema::_ORG_BLOG_URL => array(
                        'type' => 'Text',
                        'name' => Apollo_DB_Schema::_ORG_BLOG_URL,
                        'place_holder' => __('Blog URL', 'apollo'),
                        'value' => isset($arrData[Apollo_DB_Schema::_ORG_BLOG_URL]) ? $arrData[Apollo_DB_Schema::_ORG_BLOG_URL] : '',
                        'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]]',
                        'title' => __('Blog URL', 'apollo'),
                        'container' => 'hafl fr',
                        'validate' => array(
                            'rule' => array(
                                Apollo_Form::_FORM_URL,

                    )
                )
            ),
            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'el-blk',
                'container' => '',
            ),
                    Apollo_DB_Schema::_ORG_INSTAGRAM_URL => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_ORG_INSTAGRAM_URL,
                'place_holder' => __('Instagram URL', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_ORG_INSTAGRAM_URL]) ? $arrData[Apollo_DB_Schema::_ORG_INSTAGRAM_URL] : '',
                'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]]',
                'title' => __('Instagram URL', 'apollo'),
                        'container' => 'hafl fl',
                        'validate' => array(
                            'rule' => array(
                                Apollo_Form::_FORM_URL,

                            )
                        )
                    ),
                    Apollo_DB_Schema::_ORG_TWITTER_URL => array(
                        'type' => 'Text',
                        'name' => Apollo_DB_Schema::_ORG_TWITTER_URL,
                        'place_holder' => __('Twitter URL', 'apollo'),
                        'value' => isset($arrData[Apollo_DB_Schema::_ORG_TWITTER_URL]) ?
                            $arrData[Apollo_DB_Schema::_ORG_TWITTER_URL] : '',
                        'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]]',
                        'title' => __('Twitter URL', 'apollo'),
                        'container' => 'hafl fr',
                        'validate' => array(
                            'rule' => array(
                                Apollo_Form::_FORM_URL,

                            )
                        )
                    ),
                array(
                    'type' => 'CloseDiv',
                    'name' => 'title',
                    'value' => '',
                    'class' => '',
                    'container' => '',
                ),

                array(
                    'type' => 'Div',
                    'name' => 'title',
                    'value' => '',
                    'class' => 'el-blk',
                    'container' => '',
                ),
                    Apollo_DB_Schema::_ORG_PINTEREST_URL => array(
                        'type' => 'Text',
                        'name' => Apollo_DB_Schema::_ORG_PINTEREST_URL,
                        'place_holder' => __('Pinterest URL', 'apollo'),
                        'value' => isset($arrData[Apollo_DB_Schema::_ORG_PINTEREST_URL]) ? $arrData[Apollo_DB_Schema::_ORG_PINTEREST_URL] : '',
                        'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]]',
                        'title' => __('Pinterest URL', 'apollo'),
                        'container' => 'hafl fl',
                        'validate' => array(
                            'rule' => array(
                                Apollo_Form::_FORM_URL,

                            )
                        )
                    ),
                    Apollo_DB_Schema::_ORG_FACEBOOK_URL => array(
                        'type' => 'Text',
                        'name' => Apollo_DB_Schema::_ORG_FACEBOOK_URL,
                        'place_holder' => __('Facebook URL', 'apollo'),
                        'value' => isset($arrData[Apollo_DB_Schema::_ORG_FACEBOOK_URL]) ? $arrData[Apollo_DB_Schema::_ORG_FACEBOOK_URL] : '',
                        'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]]',
                        'title' => __('Facebook URL', 'apollo'),
                        'container' => 'hafl fr',
                        'validate' => array(
                            'rule' => array(
                                Apollo_Form::_FORM_URL,

                            )
                        )
                    ),
                array(
                    'type' => 'CloseDiv',
                    'name' => 'title',
                    'value' => '',
                    'class' => '',
                    'container' => '',
                ),

                array(
                    'type' => 'Div',
                    'name' => 'title',
                    'value' => '',
                    'class' => 'el-blk',
                    'container' => '',
                ),
                    Apollo_DB_Schema::_ORG_LINKED_IN_URL => array(
                        'type' => 'Text',
                        'name' => Apollo_DB_Schema::_ORG_LINKED_IN_URL,
                        'place_holder' => __('LinkedIn URL', 'apollo'),
                        'value' => isset($arrData[Apollo_DB_Schema::_ORG_LINKED_IN_URL]) ? $arrData[Apollo_DB_Schema::_ORG_LINKED_IN_URL] : '',
                        'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]]',
                        'title' => __('LinkedIn URL', 'apollo'),
                        'container' => 'hafl fl',
                        'validate' => array(
                            'rule' => array(
                                Apollo_Form::_FORM_URL,

                            )
                        )
                    ),
                    Apollo_DB_Schema::_ORG_DONATE_URL => array(
                        'type' => 'Text',
                        'name' => Apollo_DB_Schema::_ORG_DONATE_URL,
                        'place_holder' => __('Donate', 'apollo'),
                        'value' => isset($arrData[Apollo_DB_Schema::_ORG_DONATE_URL]) ? $arrData[Apollo_DB_Schema::_ORG_DONATE_URL] : '',
                        'class' => 'inp inp-txt apl_url validate[funcCall[disableSpace], custom[url]]',
                        'title' => __('Donate', 'apollo'),
                        'container' => 'hafl fr',
                        'validate' => array(
                            'rule' => array(
                                Apollo_Form::_FORM_URL,

                            )
                        )
                    ),
                array(
                    'type' => 'CloseDiv',
                    'name' => 'title',
                    'value' => '',
                    'class' => '',
                    'container' => '',
                ),
            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),

            //category
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk ' . $hideOrgTypes,
                'container' => '',
            ),    
                //title
                array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => of_get_option(Apollo_DB_Schema::_ORGANIZATION_TYPES_LABEL,__('Organization/Business Types', 'apollo')),
                    'class' => ''
                ),
                //end title
                Apollo_DB_Schema::_APL_ORG_TERM => array(
                    'title' => __('Organization/Business types', 'apollo'),
                    'type' => 'Category_Group',
                    'name' => Apollo_DB_Schema::_APL_ORG_TERM,
                    'class' => 'event',
                    'value' => isset($arrData[Apollo_DB_Schema::_APL_ORG_TERM]) ? $arrData[Apollo_DB_Schema::_APL_ORG_TERM] : '',
                    'post_type' => Apollo_DB_Schema::_ORGANIZATION_PT,
                ),
            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),
            // organization photo
            'org_image_gallery_section' => array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),
            'org_image_gallery_title' => array(
                'type' => 'Title',
                'name' => 'title',
                'value' => __('Organization/Business Images', 'apollo'),
                'class' => ''
            ),
            'org_photo_id' =>  array(
                'type' => 'Hidden',
                'name' => 'image_id',
                'place_holder' =>__('Id','apollo'),
                'value' =>  isset($arrData['ID'])?$arrData['ID']:'',
                'class' => 'inp inp-txt',
                'title' =>__('Name','apollo'),
                'validate' => true,
                'id' => 'org_id',
                'container' => 'ebl'
            ),
            
            'org_photo_target' =>  array(
                'type' => 'Hidden',
                'name' => 'image_target',
                'place_holder' =>__('target','apollo'),
                'value' =>  Apollo_DB_Schema::_ORGANIZATION_PT,
                'class' => 'inp inp-txt',
                'title' =>__('Name','apollo'),
                'validate' => true,
                'id' => 'org_target',
                'container' => 'ebl'
            ),
            Apollo_DB_Schema::_APL_ORG_IMAGE_GALLERY => array(
                'type' => 'Tab',
                'place_holder' =>__('Name','apollo'),
                'value' =>  $arrData['ID'],
                'container' => 'el-blk',
                'children' => array(
                    'primary_photo' => array(
                        'type' => 'ShortCode',
                        'name' => 'primary_photo',
                        'title' => __($primary_title,'apollo'),
                        'target' => Apollo_DB_Schema::_ORGANIZATION_PT,
                        'value' =>  'apollo_upload_and_drop ',
                    ),
                    'gallery_photo' => array(
                        'type' => 'ShortCode',
                        'name' => 'gallery_photo',
                        'title' => __('GALLERY','apollo'),
                        'target' => Apollo_DB_Schema::_ORGANIZATION_PT,
                        'value' =>  'apollo_upload_gallery ',
                    ),
                ),
                'validate' => array(
                    'rule' => $validPhotoClass
                ),
            ),

            'Additional_Form' => array(
                'type' => 'Additional_fields',
                'name' => '',
                'class' => 'full-custom',
                'title' => '',
                'id' => '',
                'container' => 'artist-blk',
                'place_holder' => __('Contact name', 'apollo'),
                'value' => array(
                    'post_type' => Apollo_DB_Schema::_ORGANIZATION_PT,
                    'post_data' => $arrData,
                    'post_id' => $arrData['ID']
                )
            ),

        );

        $formElementItems = array_merge($formElementItems, $moreElementItems);

        $org_venue = Apollo_App::apollo_get_meta_data($arrData['ID'],Apollo_DB_Schema::_APL_ORG_VENUE, Apollo_DB_Schema::_APL_ORG_DATA);
        if($arrData['ID'] && !empty($org_venue) ){
            $valueSelect = $org_venue;
        } else {
            $valueSelect = isset($arrData[Apollo_DB_Schema::_APL_ORG_VENUE]) ? $arrData[Apollo_DB_Schema::_APL_ORG_VENUE] : '';
        }

        $arrayClone = array(
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

            //title
            array(
                'type' => 'Title',
                'name' => 'title',
                'value' => __('Clone data to Venue', 'apollo'),
                'class' => ''
            ),
            Apollo_DB_Schema::_APL_ORG_VENUE => array(
                'type' => 'Checkbox',
                'name' => Apollo_DB_Schema::_APL_ORG_VENUE,
                'id' => Apollo_DB_Schema::_APL_ORG_VENUE,
                'title' => __('My organization is also a venue.', 'apollo'),
                'value' => array(
                    'list_item' => 1,
                    'selected_item' => $valueSelect,
                    'disable' => $arrData['ID'] ? 'disabled' : ''
                ),
            ),
            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),
        );

        /*
       * @Ticket 15203
       * generate select tags
       */
        if(has_filter("apl_pbm_fe_render_select_tags_section")) {
            $formElementItems = apply_filters('apl_pbm_fe_render_select_tags_section', [
                'post_id' => $arrData['ID'],
                'form'    => $formElementItems
            ]);
        }

        $bottomFields = array(
            //submit button
            'Submit_btn' => array(
                'type' => 'Submit',
                'class' => 'submit-btn submit-form-with-validation-engine',
                'title' => __('SUBMIT RECORD', 'apollo')
            ),
            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),
        );

        $contactFields = array(
            //contact info
            array(
                'type' => 'Div',
                'name' => 'title',
                'value' => '',
                'class' => 'artist-blk',
                'container' => '',
            ),

            //title
            array(
                'type' => 'Title',
                'name' => 'title',
                'value' => __('Contact Info', 'apollo'),
                'class' => ''
            ),
            //end title
            Apollo_DB_Schema::_ORG_CONTACT_NAME => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_ORG_CONTACT_NAME,
                'place_holder' => __('Contact name  (*) ', 'apollo'),
                'value' => trim($contactName),
                'class' => 'inp inp-txt validate[required]',
                'data_attributes' => array(
                    'data-errormessage-value-missing="'.__('Contact name is required','apollo').'"'
                ),
                'title' => __('Contact name', 'apollo'),
                'validate' => array(
                    'rule' => array(
                        Apollo_Form::_FORM_REQUIRED
                    )
                ),
            ),
            Apollo_DB_Schema::_ORG_CONTACT_EMAIL => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_ORG_CONTACT_EMAIL,
                'place_holder' => __('Contact email  (*) ', 'apollo'),
                'value' => $contactEmail,
                'class' => 'inp inp-txt validate[required,funcCall[disableSpace],custom[email]]',
                'data_attributes' => array(
                    'data-errormessage-value-missing="'.__('Contact email is required','apollo').'"'
                ),
                'title' => __('Contact email', 'apollo'),
                'validate' => array(
                    'rule' => array(
                        Apollo_Form::_FORM_REQUIRED,
                        Apollo_Form::_FORM_EMAIL
                    )
                ),
            ),
            Apollo_DB_Schema::_ORG_CONTACT_PHONE => array(
                'type' => 'Text',
                'name' => Apollo_DB_Schema::_ORG_CONTACT_PHONE,
                'place_holder' => __('Contact phone  (*) ', 'apollo'),
                'value' => isset($arrData[Apollo_DB_Schema::_ORG_CONTACT_PHONE]) ? $arrData[Apollo_DB_Schema::_ORG_CONTACT_PHONE] : '',
                'class' => 'inp inp-txt validate[required]',
                'data_attributes' => array(
                    'data-errormessage-value-missing="'.__('Contact phone is required','apollo').'"'
                ),
                'title' => __('Contact phone', 'apollo'),
                'validate' => array(
                    'rule' => array(
                        Apollo_Form::_FORM_REQUIRED
                    )
                ),
            ),
            array(
                'type' => 'CloseDiv',
                'name' => 'title',
                'value' => '',
                'class' => '',
                'container' => '',
            ),
        );

        if(of_get_option(Apollo_DB_Schema::_ORGANIZATION_CLONE_TO_VENUE)) {
            $formElementItems = array_merge($formElementItems, $arrayClone);
         }
        /**
         * Thienld : render additional fields of Business Post Type.
         * Because Org and Business have relationship together.
         * **/
        $bsFields = self::getBusinessFields($arrData);
        $formElementItems = array_merge($formElementItems, $bsFields, $contactFields, $bottomFields);

        $locationOrgPhotoForm = of_get_option(Apollo_DB_Schema::_ORGANIZATION_PHOTO_FORM_LOCATION, 1);
        if($locationOrgPhotoForm == 1){
            unset($formElementItems['org_image_gallery_section']);
            unset($formElementItems['org_image_gallery_title']);
            unset($formElementItems['org_photo_id']);
            unset($formElementItems['org_photo_target']);
            unset($formElementItems[Apollo_DB_Schema::_APL_ORG_IMAGE_GALLERY]);
        }
        
        if (Apollo_App::get_network_manage_states_cities() == 0 && isset($formElementItems['add_new_state_btn'])) {
            unset($formElementItems['add_new_state_btn']);
            unset($formElementItems['add_new_state']);
        }
        
        
        $this->elements = $formElementItems;
    }

    public static function getBusinessFields($postedData = array()){

        if( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_BUSINESS_PT))
        {
            /*@ticket #17907: [CF] 20181009 - Display business form fields on the FE ORG form*/
            $postedData = !$postedData ? array('ID' => '') : $postedData;
            $displayAllBusinessField = of_get_option(APL_Business_Module_Theme_Option::_BUSINESS_FE_DISPLAY_ALL_FIELDS, 0);
            $isBusiness = isset($postedData[Apollo_DB_Schema::_ORG_BUSINESS]) ? $postedData[Apollo_DB_Schema::_ORG_BUSINESS] : '';

            if (!$displayAllBusinessField && !$isBusiness){
                return array();
            }

            $showBusinessField = $isBusiness ? '' : 'hidden';

            $bsPostID = Apollo_App::apollo_get_meta_data($postedData['ID'],Apollo_DB_Schema::_APL_ORG_BUSINESS);
            $bsAdditionalFields = get_apollo_meta($bsPostID, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA,true);
            $bsAdditionalFields = !empty($bsAdditionalFields) ? maybe_unserialize($bsAdditionalFields) : array();
            $postedData = array_merge($postedData,$bsAdditionalFields);

            $bsTypes = get_terms('business-type', array('orderby' => 'name', 'order' => 'ASC', 'hide_empty' => 0));
            $selectedBusinessType = wp_get_post_terms($bsPostID, Apollo_DB_Schema::_BUSINESS_PT . '-type', array('fields' => 'ids'));

            // This is checked for case : business types is empty OR the checkbox of "if you are RESTAURANT ..." is unchecked. If they are in two case, we will skip validation it.
            $enabledValidation = ( ! isset($postedData['_org_restaurant']) || empty($bsTypes)) ? false : true;
            $hideBSTClass = empty($bsTypes) ? 'hidden' : '';

            /*@ticket #17907 [CF] 20181009 - Display business form fields on the FE ORG form*/
            $businessLocationInfo = maybe_unserialize(get_apollo_meta($bsPostID, Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO, true));
            $hours = '';
            $reservations = '';
            $other_info = '';
            if(!empty($businessLocationInfo)){
                $hours = isset($businessLocationInfo[Apollo_DB_Schema::_APL_BUSINESS_HOURS]) ? $businessLocationInfo[Apollo_DB_Schema::_APL_BUSINESS_HOURS] : "";
                $reservations = isset($businessLocationInfo[Apollo_DB_Schema::_APL_BUSINESS_RESERVATION]) ? $businessLocationInfo[Apollo_DB_Schema::_APL_BUSINESS_RESERVATION] : "";
                $other_info = isset($businessLocationInfo[Apollo_DB_Schema::_APL_BUSINESS_OTHER_INFO]) ? $businessLocationInfo[Apollo_DB_Schema::_APL_BUSINESS_OTHER_INFO] : "";
            }

            $bsFields = array(
                array(
                    'type' => 'Div',
                    'name' => 'title',
                    'value' => '',
                    'class' => 'artist-blk bs-area',
                    'container' => '',
                ),

                //title
                array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => of_get_option(APL_Business_Module_Theme_Option::_BUSINESS_SERVICES_LABEL, __('Business Services', 'apollo')),
                    'class' => ''
                ),
                array(
                    'type' => 'Checkbox',
                    'name' => Apollo_DB_Schema::_ORG_BUSINESS,
                    'id' => Apollo_DB_Schema::_ORG_BUSINESS,
                    'title' => __('If you are a COMMERCIAL BUSINESS please check this box', 'apollo'),
                    'value' => array(
                        'list_item' => 'yes',
                        'selected_item' => isset($postedData[Apollo_DB_Schema::_ORG_BUSINESS]) ? $postedData[Apollo_DB_Schema::_ORG_BUSINESS] : ''
                    )
                ),
                array(
                    'type' => 'Div',
                    'name' => 'title',
                    'value' => '',
                    'class' => 'artist-blk sub-section-blk org-business-fields '. $hideBSTClass . ' ' .$showBusinessField,
                    'container' => '',
                ),

                //title
                array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => __('Business Type', 'apollo'),
                    'class' => $hideBSTClass
                ),

                array(
                    'type' => 'Div',
                    'name' => 'title',
                    'value' => '',
                    'class' => 'evt-blk ' . $hideBSTClass,
                ),

                array(
                    'type' => 'Div',
                    'name' => 'title',
                    'value' => '',
                    'class' => 'apl-business-category',
                ),

                array(
                    'type' => 'Title',
                    'name' => 'title',
                    'value' => 'Select Business Type',
                    'class' => 'cat-txt',
                ),
                array(
                    'type' => 'Div',
                    'class' => 'expend add-cat-expand',
                    'data_attributes' => array(
                        'data-mod="cat" data-action="hide"',
                    ),
                ),
                array(
                    'type' => 'APL_Form_Icon',
                    'class' => 'fa fa-3x fa-chevron-circle-down',
                    'container' => '',
                ),
                array(
                    'type' => 'CloseDiv',
                ),

                Apollo_DB_Schema::_APL_BUSINESS_TYPE_DDL => array(
                    'type' => 'Category_Group',
                    'post_type' => Apollo_DB_Schema::_BUSINESS_PT,
                    'name' => Apollo_DB_Schema::_APL_BUSINESS_TYPE_DDL,
                    'class' => 'event custom-validate-field ',
                    'title' => __('Business Type ', 'apollo'),
                    'value' => $selectedBusinessType,
                    'display' => !empty($bsTypes),
                    'container' => 'cat-list'
                ),

                array(
                    'type' => 'CloseDiv',
                ),
                array(
                    'type' => 'CloseDiv',
                ),
                array(
                    'type' => 'CloseDiv',
                    'name' => 'title',
                    'value' => '',
                    'class' => 'org-bs-area ' . $hideBSTClass,
                    'container' => '',
                ),

                array(
                    'type' => 'Div',
                    'name' => 'title',
                    'value' => '',
                    'class' => 'artist-blk org-business-fields ' . $showBusinessField,
                    'container' => '',
                ),

                array(
                    'type' => 'Wysiwyg',
                    'id' => Apollo_DB_Schema::_APL_BUSINESS_HOURS,
                    'name' => Apollo_DB_Schema::_APL_BUSINESS_HOURS,
                    'value' => Apollo_App::convertTinyCMEToCkEditor($hours),
                    'class' => 'inp-desc-event',
                    'title' => __('Hours', 'apollo'),
                ),

                array(
                    'type' => 'CloseDiv',
                    'name' => 'title',
                    'value' => '',
                    'class' => '',
                    'container' => '',
                ),

                array(
                    'type' => 'Div',
                    'name' => 'title',
                    'value' => '',
                    'class' => 'artist-blk org-business-fields ' .$showBusinessField,
                    'container' => '',
                ),

                Apollo_DB_Schema::_ORG_RESTAURANT => array(
                    'type' => 'Checkbox',
                    'name' => Apollo_DB_Schema::_ORG_RESTAURANT,
                    'title' => __('If you are a RESTAURANT please check this box.', 'apollo'),
                    'id' => 'org-is-restaurant',
                    'value' => array(
                        'list_item' => 'yes',
                        'selected_item' => isset($postedData[Apollo_DB_Schema::_ORG_RESTAURANT]) ? $postedData[Apollo_DB_Schema::_ORG_RESTAURANT] : ''
                    ),
                    'class' => 'org-business-fields ' . $showBusinessField
                ),
                array(
                    'type' => 'CloseDiv',
                    'name' => 'title',
                    'value' => '',
                    'class' => '' ,
                    'container' => '',
                ),
                array(
                    'type' => 'Additional_fields',
                    'name' => '',
                    'class' => 'full-custom',
                    'title' => '',
                    'id' => '',
                    'container' => 'business-blk org-bs-area sub-section-blk artist-blk org-business-fields ' . $showBusinessField,
                    'value' => array(
                        'post_type' => Apollo_DB_Schema::_BUSINESS_PT,
                        'post_data' => $postedData,
                        'post_id' => $bsPostID,
                        'enabled_validation' => $enabledValidation
                    ),

                ),

                array(
                    'type' => 'Div',
                    'name' => 'title',
                    'value' => '',
                    'class' => 'artist-blk org-bs-area org-business-fields ' .$showBusinessField,
                    'container' => '',
                ),

                array(
                    'type' => 'Wysiwyg',
                    'name' => Apollo_DB_Schema::_APL_BUSINESS_RESERVATION ,
                    'id' => Apollo_DB_Schema::_APL_BUSINESS_RESERVATION,
                    'value' => Apollo_App::convertTinyCMEToCkEditor($reservations),
                    'class' => 'inp-desc-event',
                    'title' => __('Reservation Info', 'apollo'),
                ),

                array(
                    'type' => 'CloseDiv',
                    'name' => 'title',
                    'value' => '',
                    'class' => '' ,
                    'container' => '',
                ),

                array(
                    'type' => 'Div',
                    'name' => 'title',
                    'value' => '',
                    'class' => 'artist-blk org-business-fields ' .$showBusinessField,
                    'container' => '',
                ),

                array(
                    'type' => 'Wysiwyg',
                    'name' => Apollo_DB_Schema::_APL_BUSINESS_OTHER_INFO,
                    'id' => Apollo_DB_Schema::_APL_BUSINESS_OTHER_INFO,
                    'value' => Apollo_App::convertTinyCMEToCkEditor($other_info),
                    'class' => 'inp-desc-event',
                    'title' => __('Other Business Info', 'apollo'),
                ),

                array(
                    'type' => 'CloseDiv',
                    'name' => 'title',
                    'value' => '',
                    'class' => '',
                    'container' => '',
                ),

                array(
                    'type' => 'CloseDiv',
                    'name' => 'title',
                    'value' => '',
                    'class' => '',
                    'container' => '',
                ),
            );
        } else {
            $bsFields = array();
        }
        return $bsFields;
    }

    public function formHeader()
    {
        ?>
        <div class="dsb-welc custom">
            <h1 ><?php _e('Organization/Business Info', 'apollo') ?>
                <?php 
                $id = $this->id ? $this->id : $this->formData['ID'];
                if ( $id && isset($this->formData['post_status']) &&
                    ($this->formData['post_status'] == 'publish' || $this->formData['post_status'] == 'pending') && $this->formData
                ): ?>
                    <a class="view-page-link" target="_blank"
                       href="<?php echo get_the_permalink($id) ?>">(<?php _e('View Page', 'apollo') ?>
                        )</a>
                <?php endif; ?>
            </h1>
        </div>
        <?php
        /** @Ticket #13470 */
        if (isset($_SESSION['save_'. Apollo_DB_Schema::_ORGANIZATION_PT])) {
            if ($_SESSION['save_'. Apollo_DB_Schema::_ORGANIZATION_PT] == 'add-new') {
                $this->formSubmitSuccessRedirect(__( Apollo_Form_Static::_SUCCESS_ADD_NEW_MESSAGE, 'apollo' ));
            } else {
                $this->formSubmitSuccessRedirect(__( Apollo_Form_Static::_SUCCESS_MESSAGE, 'apollo'));
            }
            unset($_SESSION['save_'. Apollo_DB_Schema::_ORGANIZATION_PT]);
        }

        ?>
        <?php
            if ( isset($_GET['warning']) ):
        ?>
        <div><span class="error"><?php _e('Please enter your Organization/Business profile first !', 'apollo') ?></span></div>
        <?php endif; ?>

        <?php
        if ( isset($_GET['required_org']) ):
            ?>
            <div><span class="error"><?php _e('The system is required at least a publish organization to create an event', 'apollo') ?></span></div>
        <?php endif; ?>

        <?php
        parent::formHeader();
        /* Thienld: handle show sucess message when saved successfully */
        if(Apollo_App::isInputMultiplePostMode()){
            if(isset($_SESSION['saved-post-success'])){
                if (isset($_SESSION['is_adding_new_org']) && $_SESSION['is_adding_new_org']) {
                    $this->successMessage = __(Apollo_Form_Static::_SUCCESS_ADD_NEW_MESSAGE, 'apollo');
                } else {
                    $this->successMessage = __(Apollo_Form_Static::_SUCCESS_MESSAGE, 'apollo');
                }
                parent::formSubmitSuccess();
                unset($_SESSION['saved-post-success']);
                unset($_SESSION['is_adding_new_org']);
            }
        }
    }

    /*Process form submit*/
    public function postMethod()
    {
        // validate nonce
        if ( $this->validateNonce() ) {
            return $this->validateClassObject->save($this->formData);
        }
    }

    /*process form get method*/
    public function getMethod()
    {
        $this->successMessage = __(Apollo_Form_Static::_SUCCESS_MESSAGE, 'apollo');
        $id = Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_ORGANIZATION_PT);

        if(!empty($id)) {
            if(get_post_status($id) == 'publish' ||  get_post_status($id) == 'pending'){
                $postInfo = get_post($id, ARRAY_A);
                $status = $postInfo['post_status'];
                $id = $postInfo['ID'];
                //get all meta;
                $metaAddr = get_apollo_meta($id, Apollo_DB_Schema::_APL_ORG_ADDRESS, true);
                $metaData = get_apollo_meta($id, Apollo_DB_Schema::_APL_ORG_DATA, true);
                if (!is_array($metaAddr) && !empty($metaAddr))
                    $metaAddr = unserialize($metaAddr);
                if (!is_array($metaData) && !empty($metaData))
                    $metaData = unserialize($metaData);

                if (is_array($metaAddr)) {
                    foreach ($metaAddr as $key => $value) {
                        $postInfo[$key] = $value;
                    }
                }
                if (is_array($metaAddr)) {
                    foreach ($metaData as $key => $value) {
                        $postInfo[$key] = $value;
                    }
                }

                $postInfo[Apollo_DB_Schema::_APL_ORG_TERM] = wp_get_post_terms($id, Apollo_DB_Schema::_ORGANIZATION_PT . '-type', array(
                    'fields' => 'ids'
                ));
                $postInfo['ID'] = $id;
                $this->formData = $postInfo;
                $this->successMessage = Apollo_Form_Static::_SUCCESS_MESSAGE;
            }
        }
    }
    //Show message Fail Submit

    //override parent for additional field
    public function formSubmitAction()
    {
        // Handle submit form
        do_action('apollo_submit_form');

        $addFieldsValidateClass = new Apollo_Submit_Form(Apollo_DB_Schema::_ORGANIZATION_PT, array());
        $this->isSaveSuccess = false;
        $this->isSubmitFail = false;
        //post method


        if ($this->formRequestMethod == 'post') {
            $this->cleanPost();
            $this->isSubmitFail = true;
            if ($this->validateClassObject->isValidAll($this->mergerPostRule())) {
                $this->isSaveSuccess = true;
                $this->isSubmitFail = false;
            }
            if (!$addFieldsValidateClass->isValidAll($this->mergePostAFields())) {
                $this->isSaveSuccess = false;
                $this->isSubmitFail = true;
            }
            /**
            * Thienld : handle validation for business additional field inside org form profile when form submission
            **/
            $bsAdditionalFieldsValidateClass = new Apollo_Submit_Form(Apollo_DB_Schema::_BUSINESS_PT, array());
            if (Apollo_App::is_avaiable_module( Apollo_DB_Schema::_BUSINESS_PT)
                && (isset($_POST['ID']) && !empty($_POST['ID']))
                && (isset($_POST[Apollo_DB_Schema::_ORG_BUSINESS]) && $_POST[Apollo_DB_Schema::_ORG_BUSINESS] == 'yes')
                && (isset($_POST[Apollo_DB_Schema::_ORG_RESTAURANT]) && $_POST[Apollo_DB_Schema::_ORG_RESTAURANT] == 'yes')
                && !$bsAdditionalFieldsValidateClass->isValidAll($this->mergePostBSFields())
            ){
                $this->isSaveSuccess = false;
                $this->isSubmitFail = true;
            }
            //save data
            if ($this->isSaveSuccess){
                $this->id = $this->postMethod();

                /*
                 * @Ticket 15203
                 * page builder module
                 * save tags
                 */
                if(has_action('pbm_fe_save_post_tags')) {
                    do_action('pbm_fe_save_post_tags', $this->id, array('data' => $_POST));
                }

            }
            if(Apollo_App::isInputMultiplePostMode()){
                // Thienld : handle for case submit post of inputting multiple post
                // Take user to the edit post page on dashboard when saved data success (add new / edit action)
                if($this->isSaveSuccess){
                    $_SESSION['saved-post-success'] = 1;
                    /** @Ticket #13470 */
                    $_SESSION['is_adding_new_org'] = $this->isAddingNewOrg;
                    $redirectUrl = APL_Dashboard_Hor_Tab_Options::ORG_PROFILE_URL;
                    $isAgencyArea = intval(get_query_var('_is_agency_educator_page',0)) === 1;
                    if($isAgencyArea){
                        $redirectUrl = APL_Dashboard_Hor_Tab_Options::AGENCY_ORG_PROFILE_URL;
                    }
                    wp_redirect(site_url($redirectUrl) . '/' .$this->id);
                    exit();
                }
            }
            //end save date
        }
    }

    public function mergePostAFields()
    {
        $data = array();
        $group_fields = Apollo_Custom_Field::get_group_fields(Apollo_DB_Schema::_ORGANIZATION_PT);
        foreach ($group_fields as $group) {
            $fields = isset($group['fields']) ? $group['fields'] : array();
            if (count($fields) > 0) {
                foreach ($fields as $field) {
                    $data[$field->name] = isset($_POST[$field->name]) ? $_POST[$field->name] : '';
                }
            }
        }
        return $data;
    }

    public function mergePostBSFields()
    {
        $data = array();
        $group_fields = Apollo_Custom_Field::get_group_fields(Apollo_DB_Schema::_BUSINESS_PT);
        foreach ($group_fields as $group) {
            $fields = isset($group['fields']) ? $group['fields'] : array();
            if (count($fields) > 0) {
                foreach ($fields as $field) {
                    $data[$field->name] = isset($_POST[$field->name]) ? $_POST[$field->name] : '';
                }
            }
        }
        return $data;
    }

    // Thienld : These functions apply for mode input multiple post
    public function setCurDataHorizontalTab(){
        $this->activatedTab = APL_Dashboard_Hor_Tab_Options::ORG_PROFILE_URL;
        $isAgencyArea = intval(get_query_var('_is_agency_educator_page',0)) === 1;
        if($isAgencyArea){
            $this->activatedTab = APL_Dashboard_Hor_Tab_Options::AGENCY_ORG_PROFILE_URL;
        }
    }

    public function getTabData(){
        $tabData = array(
            'profile' => APL_Dashboard_Hor_Tab_Options::ORG_PROFILE_URL,
            'photo' => APL_Dashboard_Hor_Tab_Options::ORG_PHOTOS_URL,
            'audio' => APL_Dashboard_Hor_Tab_Options::ORG_AUDIO_URL,
            'video' => APL_Dashboard_Hor_Tab_Options::ORG_VIDEO_URL
        );
        $isAgencyArea = intval(get_query_var('_is_agency_educator_page',0)) === 1;
        if($isAgencyArea){
            $tabData = array(
                'profile' => APL_Dashboard_Hor_Tab_Options::AGENCY_ORG_PROFILE_URL,
                'photo' => APL_Dashboard_Hor_Tab_Options::AGENCY_ORG_PHOTOS_URL,
                'audio' => APL_Dashboard_Hor_Tab_Options::AGENCY_ORG_AUDIO_URL,
                'video' => APL_Dashboard_Hor_Tab_Options::AGENCY_ORG_VIDEO_URL
            );
        }
        return $tabData;
    }

    public function getTabLinks(){
        $tabData = $this->getTabData();
        $tabLinks = array(
            $tabData['profile'] => __("PROFILE","apollo"),
            $tabData['photo'] => __("PHOTO","apollo"),
            $tabData['audio'] => __("AUDIO","apollo"),
            $tabData['video'] => __("VIDEO","apollo")
        );
        $locationOrgPhotoForm = of_get_option(Apollo_DB_Schema::_ORGANIZATION_PHOTO_FORM_LOCATION, 1);
        if($locationOrgPhotoForm != 1){
            unset($tabLinks[$tabData['photo']]);
        }
        return $tabLinks;
    }

    /**
     * Set nonce info
     *
     * @return void
     */
    public function setNonceInfo()
    {
        $this->nonceName   = Apollo_Const::_APL_ORGANIZATION_NONCE_NAME;
        $this->nonceAction = Apollo_Const::_APL_NONCE_ACTION_ORGANIZATION_PROFILE_PAGE;
    }
}