<?php
/**
 * Class Apollo_Organization_Base_Form
 *
 * @ticket #15971
 */

class Apollo_Organization_Base_Form extends Apollo_Form
{
    public function formInit()
    {
    }

    public function getMethod()
    {
    }

    public function postMethod()
    {
    }

    /**
     * Set nonce info
     *
     * @return void
     */
    public function setNonceInfo()
    {
    }

    public function authorize()
    {
        $postId = Apollo_User::getSubmittingAssociatedID(Apollo_DB_Schema::_ORGANIZATION_PT);

        // Prevent security error by updating the value of ID hidden input
        if (!empty($_POST['ID']) && $_POST['ID'] != $postId) {
            return false;
        }

        if (empty($postId)){
            return true;
        }

        return Apollo_User::isAssociatedItems($postId, Apollo_DB_Schema::_ORGANIZATION_PT);
    }
}