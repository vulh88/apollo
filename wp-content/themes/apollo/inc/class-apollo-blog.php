<?php

if (!defined('ABSPATH')) {
    exit; // Silent is golden
}

/*
 * Apollo user for wordpress
 *
 * Funciton about new blog created
 */

class Apollo_Blog {

    private $blog_id;

    public function __construct($blog_id) {
        $this->blog_id = $blog_id;
    }
    public function createData() {
        self::cloneSuperAdminUserForNewSite($this->blog_id);
    }

    /** Author: Thienld
     * Auto filling up data for column lang of table POST TERM
     * it's only run with multilingual sites
     */
    public function autoFillLangCodePostTerm(){
        try{
            if(Apollo_App::hasWPLM() && Apollo_App::checkExistedTermLangCol()){
                $aplQuery = new Apl_Query(Apollo_Tables::_POST_TERM);
                $postsTerm = $aplQuery->get_where(' 1=1 ', ' DISTINCT(post_id) ');
                if(!empty($postsTerm)){
                    foreach($postsTerm as $p){
                        Apollo_App::updatePostTermLang($p->post_id);
                    }
                }
            }
        }catch (Exception $ex){
            aplDebugFile("re-active theme : autoFillLangCodePostTerm : " . $ex->getMessage());
        }
    }

    /**
     * @return bool false mean superadmins have success insert into table
     */
    public static function cloneSuperAdminUserForNewSite($blog_id) {
        global $wpdb;

        $asuperadmin = get_super_admins();
        $ssuperadmin = "('".implode("','", $asuperadmin)."')";
        $tbl = Apollo_App::getBlogUserTable();
        $sql = "INSERT INTO $tbl (blog_id, user_id)
                SELECT {$blog_id}, ID
                FROM {$wpdb->users} WHERE user_login IN $ssuperadmin
                ON DUPLICATE KEY UPDATE user_id = user_id
                ";

        $result = $wpdb->query($sql);

        if(!$result) {
            Apollo_Event_System::trigger('clone-super-admin-new-site', array('blog_id' => $blog_id, 'event_name' => 'clone-super-admin-new-site', 'msg' => 'Fail to clone super admin for new site'));
        }
    }
}