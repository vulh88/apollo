<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of class-apollo-logs
 *
 * @author vulh
 */
class Apollo_Logs {
    protected $content;
    protected $fn;
    protected $isFront;
    
    function __construct($content, $fn, $isFront = false) {
        $this->content  = $content;
        $this->fn       = $fn;
        $this->isFront  = $isFront;
    }
    
    function logs() {
        $uploadDir = wp_upload_dir();
        $logDir = $uploadDir['logs_dir'];
        $date = date('Y/m/d H:i:s'). ': ';
        $user = wp_get_current_user();
        $content = "$date - ".(!$this->isFront ? 'Admin FORM - ' : 'GUI - ')." - User: $user->user_login - Action: $this->fn - $this->content\n";
        try {
            @error_log( $content, 3, $logDir."/event-calendar.log");
        } catch (Exception $ex) {

        }
        
    }
}
