<?php

if ( ! defined('ABSPATH') ) exit;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (! class_exists('Apollo_Video') ) :
 
class Apollo_Video {
    
    var $embed;
    var $code;
    var $src;
    
    public function __construct($embed) {
        $this->embed = $embed;
        $this->initCode();
    }
    
    function initCode() {
        if ( Apollo_App::is_youtube_url( $this->embed ) ) {
            $this->setYoutubeCode();
            $this->src = 'https://www.youtube.com/embed/';
        } else if (Apollo_App::is_vimeo($this->embed) ) {
            $this->setVimeoId();
            $this->src = 'https://player.vimeo.com/video/';
        }
    }
    
    function setYoutubeCode() {
        
        if ( $this->code ) return $this->code;
        
        $embed = explode( 'watch?v=' , $this->embed );
                
        if ( isset( $embed[1] ) ) {
            $code = $embed[1];
        } else {
            $embed = explode( 'youtu.be/' , $this->embed );
            if ( ! isset( $embed[1] ) ) {
                $embed = explode( 'embed/' , $this->embed );
            }
            $code = isset( $embed[1] ) ? $embed[1] : '';
        }
        
        return $this->code = $code;
    }
    
    function getThumb() {
        
        $thumb = '';
            
        if ( Apollo_App::is_youtube_url( $this->embed ) ) {
            
            $thumb = 'https://img.youtube.com/vi/'. $this->code .'/0.jpg';
            
        } else if (Apollo_App::is_vimeo( $this->embed ) ) {
            
            $vimeo = unserialize(file_get_contents("https://vimeo.com/api/v2/video/$this->code.php"));
         
            $thumb = $vimeo[0]['thumbnail_medium'];
        }
        return $thumb;
    }
    
    function setVimeoId() {
        $regex = '~
            # Match Vimeo link and embed code
            (?:<iframe [^>]*src=")?         # If iframe match up to first quote of src
            (?:                             # Group vimeo url
                    https?:\/\/             # Either http or https
                    (?:[\w]+\.)*            # Optional subdomains
                    vimeo\.com              # Match vimeo.com
                    (?:[\/\w]*\/videos?)?   # Optional video sub directory this handles groups links also
                    \/                      # Slash before Id
                    ([0-9]+)                # $1: VIDEO_ID is numeric
                    [^\s]*                  # Not a space
            )                               # End group
            "?                              # Match end quote if part of src
            (?:[^>]*></iframe>)?            # Match the end of the iframe
            (?:<p>.*</p>)?                  # Match any title information stuff
            ~ix';

        preg_match( $regex, $this->embed, $matches );

        return  $this->code = $matches[1];
    }
    
    function getCode() {
        return $this->code;
    }
    
    function getSrc() {
        return $this->src;
    }
}

endif;

