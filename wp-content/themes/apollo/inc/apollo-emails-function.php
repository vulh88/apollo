<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

require_once APOLLO_EMAILS_DIR. '/class-abstract-apollo-email.php';

global $apl_email_templates;
if ( $apl_email_templates ) {
    foreach( $apl_email_templates as $k => $v ) {

        if (isset($v['load']) && !$v['load']) {
            continue;
        }

        $file = APOLLO_EMAILS_DIR. sprintf( '/class-apollo-email-%s.php', strtolower(str_replace('_', '-', $k)));
        
        if ( file_exists( $file ) ) {
            require_once $file;
        }
    }
}
