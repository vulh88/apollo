<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/12/2015
 * Time: 11:44 PM
 */
class Apollo_Login_Hook {
    function __construct()
    {
        //init action
        if(count($this->initLoginAction())>0){
            foreach($this->initLoginAction() as $k => $function){
                add_action($k,array($this, $function));
            }
        }
        //init filer
        if(count($this->initLoginFilter())>0){
            foreach($this->initLoginFilter() as $k => $val){
                add_action($k,array($this, $val['function']),$val['priority'],$val['accepted_args']);
            }
        }

    }
    function initLoginAction(){
        return  $arrayAction = array(
            'login_form' => 'username_or_email_login',
            'wp_authenticate' => 'loginProcess',//login success
        );
    }
    function initLoginFilter(){
        return  $arrayFilter = array(
            'authenticate' =>
                array(
                    'function' => 'dr_email_login_authenticate',
                    'priority' => 10,
                    'accepted_args' => 3
                ) //process login

        );
    }
    function loginProcess( &$username){}

    //text + error message
    function username_or_email_login() {
        if ( 'wp-login.php' != basename( $_SERVER['SCRIPT_NAME'] ) )
            return;

        ?><script type="text/javascript">
        // Form Label
        if ( document.getElementById('loginform') )
            document.getElementById('loginform').childNodes[1].childNodes[1].childNodes[0].nodeValue = '<?php echo esc_js( __( 'Username or Email', 'email-login' ) ); ?>';

        // Error Messages
        if ( document.getElementById('login_error') )
            document.getElementById('login_error').innerHTML = document.getElementById('login_error').innerHTML.replace( '<?php echo esc_js( __( 'username' ) ); ?>', '<?php echo esc_js( __( 'Username or Email' , 'email-login' ) ); ?>' );
	    </script>
    <?php
    }

    //login process
    function dr_email_login_authenticate( $user, $username, $password ) {

        if ( is_a( $user, 'WP_User' ) )
            return $user;

        if ( !empty( $username ) ) {
            $username = str_replace( '&', '&amp;', stripslashes( $username ) );
            $user = get_user_by( 'email', $username );
            if ( isset( $user, $user->user_login, $user->user_status ) && 0 == (int) $user->user_status )
                $username = $user->user_login;
        }

        return wp_authenticate_username_password( null, $username, $password );
    }
}
new Apollo_Login_Hook();