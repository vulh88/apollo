<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Apollo Venue Functions
 *
 * Functions for venue specific things.
 *
 * @author 		vulh
 * @category 	Core
 * @package 	inc/Functions
 */

/**
 * Main function for returning venue
 *
 * @param mixed $the_venue Post object or post ID of the venue.
 * @param array $args (default: array()) Contains all arguments to be used to get this venue.
 * @return Apollo_ABS_Module
 */
function get_venue( $the_venue = false ) {
	return new Apollo_Venue( $the_venue, Apollo_DB_Schema::_VENUE_PT );
}

function get_org( $the_org = false ) {
	return new Apollo_Org( $the_org, Apollo_DB_Schema::_ORGANIZATION_PT );
}

function get_spot( $the_spot = false ) {
	return new Apollo_ABS_Module( $the_spot, Apollo_DB_Schema::_SPOT_PT );
}

function get_artist( $the_artist = false ) {
	return new Apollo_Artist( $the_artist, Apollo_DB_Schema::_ARTIST_PT );
}

function get_grant_education( $obj = false ) {
	return new Apollo_Grant_Education( $obj, Apollo_DB_Schema::_GRANT_EDUCATION );
}

function get_educator( $the_educator = false ) {
	return new Apollo_Educator( $the_educator, Apollo_DB_Schema::_EDUCATOR_PT );
}

function get_program( $the_educator = false ) {
	return new Apollo_Program( $the_educator, Apollo_DB_Schema::_PROGRAM_PT );
}
function get_business($the_business = false ) {
	return new Apollo_Business( $the_business, Apollo_DB_Schema::_BUSINESS_PT );
}

function get_apl_blog($post) {
    return new Apollo_ABS_Module( $post, 'post' );
}

function get_edu_evaluation( $the_edu_eval = false ) {
	return new Apollo_Edu_Evaluation( $the_edu_eval, Apollo_DB_Schema::_EDU_EVALUATION );
}

function get_classified( $the_classified = false ) {
	return new Apollo_Classified( $the_classified, Apollo_DB_Schema::_CLASSIFIED );
}

function get_public_art( $the_data = false ) {
	return new Apollo_Public_Art( $the_data, Apollo_DB_Schema::_PUBLIC_ART_PT );
}

function get_iframe_search_widget( $iframe_search_widget_post = false ) {
	return new Apollo_IFrame_Search_Widget( $iframe_search_widget_post, Apollo_DB_Schema::_IFRAME_SEARCH_WIDGET_PT );
}
