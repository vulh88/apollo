<?php

/**
 * Apollo Post type
 *
 * Functions for handling posttype meta.
 *
 * @author 		vulh
 * @category 	inc
 * @package 	Apollo/Functions
 */

if ( !defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Apollo Post type item Meta API - set table name
 *
 * @return void
 */
function apollo_post_type_metadata_wpdb() {

    global $wpdb, $apollo_modules;
    $avaiable_modules = Apollo_App::get_avaiable_modules();
    $avaiable_modules[] = Apollo_DB_Schema::_EVENT_PT;
   
    foreach ( $avaiable_modules as $am ) {
        
        $_ac_mods = array();
        
        if ( isset ( $apollo_modules[$am]['childs'] ) ) $_ac_mods = array_keys( $apollo_modules[$am]['childs'] );
        else $_ac_mods[] = $am;
        
        if ( $_ac_mods ):
            foreach ( $_ac_mods as $am ):
                $_am = str_replace('-','_',$am);
                $table = 'apollo_'. $_am. 'meta';
                $wpdb->$table = $wpdb->prefix . 'apollo_'. $_am. 'meta';

                if ( $wpdb->tables && ! in_array( $table , $wpdb->tables ) ) {
                    $wpdb->tables[] = $table;
                }
            endforeach;
        endif;
    }
}
add_action( 'init', 'apollo_post_type_metadata_wpdb', 0 );
add_action( 'switch_blog', 'apollo_post_type_metadata_wpdb', 0 );

/**
 * Apollo update Event meta data
 * @return void
 */
function update_apollo_meta( $post_id, $meta_key, $meta_value ) {
    $type = get_post_type( $post_id );
    if ( ! $type ) return false;
    $_type = str_replace('-','_',$type);
    update_metadata( 'apollo_'. $_type, $post_id, $meta_key, $meta_value );
}

/**
 * Apollo get Event meta data
 * @param $post_id
 * @param $key
 * @param bool $single
 * @return string
 */
function get_apollo_meta( $post_id, $key, $single = false ) {
    
    $type = get_post_type( $post_id );

    if ( ! $type ) return false;
    $_type = str_replace('-','_',$type);
    return get_metadata( 'apollo_'. $_type, $post_id, $key, $single );
}

/**
 * @Ticket #15941 - Delete apollo meta key
 * @param $postId
 * @param $metaKey
 * @param $metaValue
 * @return bool
 */
function delete_apollo_meta ($postId, $metaKey, $metaValue) {
    $type = get_post_type($postId);
    if ( ! $type) {
        return false;
    }
    $type = str_replace('-','_',$type);
    return delete_metadata('apollo_'. $type, $postId, $metaKey, $metaValue);
}