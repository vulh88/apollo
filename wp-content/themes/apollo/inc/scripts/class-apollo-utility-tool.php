<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if( ! class_exists('Apollo_Utility_Tool ')){

    class Apollo_Utility_Tool {

        // Number of the Separate sites
        protected $offset = 0;
        protected $segment = 3;

        protected $page = 1;

        /** pattern url to run utility tool
        {domain-name}/script/?action={action-name}&site_ids={ALL/single/1,2,3,4/1}
         * */

        const _EVENT_CLONE_TO_VIEW_FOR_EXPORTING = 'event_clone_to_view_for_exporting';

        const _EVENT_CLONE_CUSTOM_ACCESSIBILITY = 'event_clone_custom_accessibility';
        const _VENUE_CLONE_PHONE = 'venue_clone_phone';
        const _VENUE_CLONE_CITY = 'venue_clone_city';
        const _VENUE_CLONE_STATE = 'venue_clone_state';
        const _VENUE_CLONE_ZIP = 'venue_clone_zip';
        const _ORG_CLONE_PHONE = 'org_clone_phone';
        const _ORG_CLONE_CITY = 'org_clone_city';
        const _ORG_CLONE_STATE = 'org_clone_state';
        const _ORG_CLONE_ZIP = 'org_clone_zip';
        const _ARTIST_CLONE_ZIP = 'artist_clone_zip';
        const _CLASSIFIED_CLONE_NAME = 'classified_clone_name';
        const _CLASSIFIED_CLONE_EMAIL = 'classified_clone_email';
        const _CLASSIFIED_CLONE_PHONE = 'classified_clone_phone';
        const _CLASSIFIED_CLONE_STATE = 'classified_clone_state';
        const _CLASSIFIED_CLONE_CITY = 'classified_clone_city';
        const _PUBLIC_ART_CLONE_STATE = 'public_art_clone_state';
        const _ARTIST_CLONE_STATE = 'artist_clone_state';
        const _EDUCATOR_CLONE_STATE = 'educator_clone_state';
        const _EDUCATOR_CLONE_CITY = 'educator_clone_city';
        const _EDUCATOR_CLONE_ZIP = 'educator_clone_zip';
        const _ARTIST_CLONE_LNAME = 'artist_clone_lname';
        const _PAGE_UPDATE_DEFAULT_CONTENT = 'page_update_default_content';

        const _APL_TOOL_SYNC_BUSINESS_ZIP = 'sync_business_zip';


        const _EVENT_ARTIST_META_TO_NEW_TALBE = 'event_artist_meta_to_new_table';
        const _CREATE_TABLE_ON_MULTIPLE_SITES = 'create_tables';

        // Apollo Tool Names
        const _APL_TOOL_SYNC_ARTIST_DATA = 'order_artist_listing';
        const _APL_TOOL_SYNC_DISCOUNT_URL = 'clone_discount_url';
        const _APL_TOOL_SYNC_PROG_EDU_ID = 'clone_prog_edu_id';
        const c = 'flush_rewrite_rules_all_sites';

        // Log constants
        const _APL_LOG_FILE_NAME = 'apollo_utility_tool_log.txt';

        const _APL_TOOL_ALL_SITE_IDS = 'ALL';
        const _APL_TOOL_SINGLE_SITE_ID = 'single';

        const _APL_TOOL_NONCE_FIELD = 'apl_utility_tool_nonce_field';

        const _APL_TOOL_CLONE_CLASSIFIED_CITY = 'clone_classified_city';
        const _APL_TOOL_CLONE_CLASSIFIED_CITY_URL = 'clone_classified_city';

        /**
         * @author: Trilm
         * sync rezion-zip code
         */
        const _APL_TOOL_SYNC_REGION_ZIP_CODE = 'sync_venue_zip_code';
        const _APL_TOOL_SYNC_REGION_ZIP_CODE_URL = 'sync_venue_zip_code';
        const _APL_TOOL_SYNC_BUSINESS_REGION_CITY_URL = 'sync_business_region_city';

        const _APL_TOOL_SYNC_EVENT_PRIVATE_DATA = 'sync_event_private';
        const _APL_CLEAN_CONTENT_EVENT_THEME_TOOL_DATA = 'clean_content_event_theme_tool';

	    const _APL_TOOL_CLEAN_WASTE_DATA_EVENT_CALENDAR = 'clean_waste_data_event_calendar';

        /* @ticket #15312 */
        const _APL_TOOL_MIGRATE_BLOG_USER_TABLE_DATA_TO_USER_MODULES_TABLE = 'migrate_blog_user_table_data_to_user_modules_table';
        const _APL_CHECKING_TOOL_MIGRATE_BLOG_USER_TABLE_DATA_TO_USER_MODULES_TABLE = 'checking_migrate_blog_user_table_data_to_user_modules_table';
        const _APL_TOOL_FLUSH_REWRITE_RULES_ALL_SITES = 'flush_all_rules';

        const _APL_TOOL_CLONE_SYNDICATION_ORG_ARTIST = 'clone_syndication_org_artist';

        static $availableToolNames = array(
            self::_APL_TOOL_SYNC_ARTIST_DATA,
            self::_APL_TOOL_FLUSH_REWRITE_RULES_ALL_SITES
        );

        protected $siteIDs = null;
        static $qsSiteIDs = null; // site_ids with no extracting and formatting
        protected $action = null;
        protected $isApplyAllSite = false;
        protected $isLoggedIn = false;

        /**
         * Manually set site ids
         *
         * @param array $ids
         */
        public function setSiteIds($ids = array())
        {
            $this->siteIDs = $ids;
        }

        /**
         * @return null
         */
        public function getErrorMessage()
        {
            $rm = isset($_SESSION['errorMessage']) ? $_SESSION['errorMessage'] : '';
            unset($_SESSION['errorMessage']);
            return $rm;
        }

        /**
         * @param null $errorMessage
         */
        public function setErrorMessage($errorMessage)
        {
            if(is_array($errorMessage)){
                $errorMessage = implode('<br/>',$errorMessage);
            }
            $_SESSION['errorMessage'] = $errorMessage;
        }

        /**
         * @return null
         */
        public function getSuccessMessage()
        {
            $rm = isset($_SESSION['successMessage']) ? $_SESSION['successMessage'] : '';
            unset($_SESSION['successMessage']);
            return $rm;
        }

        /**
         * @param null $successMessage
         */
        public function setSuccessMessage($successMessage)
        {
            if(is_array($successMessage)){
                $successMessage = implode('<br/>',$successMessage);
            }
            $_SESSION['successMessage'] = $successMessage;
        }

        public function __construct() {
            $this->isLoggedIn = false;
            $this->process();
        }

        protected function process(){
            $this->action = self::getAction();
            $this->siteIDs = self::getSiteIDs();

            if (is_user_logged_in() && is_super_admin() && (isset($_GET['force']) || isset($_GET['private_site']))) {
                $this->actuallyRunCurrentTool();
            }
            elseif($this->canUseUtilityTools() && $this->canRunUtilityTools()) {
                // run
                $this->actuallyRunCurrentTool();
                exit;
            } elseif($this->canUseUtilityTools()){
                // display
                $this->displayCurrentTool();
                exit;
            } else {
                return;
            }
        }

        protected function showErrorMessage($message){
            $this->setErrorMessage(empty($message) ? __("The process has some errors. Please try again !","apollo") : $message);
            wp_redirect($this->buildUrlForProcessingUtilityTool());
        }

        protected function showSuccessMessage($message){
            $this->setSuccessMessage(empty($message) ? __("The process is completed !","apollo") : $message);
            wp_redirect($this->buildUrlForProcessingUtilityTool());
        }

        protected function displayCurrentTool(){
            switch($this->action){

                case self::_EVENT_ARTIST_META_TO_NEW_TALBE:
                    $this->renderConfirmationForm('Event - Migrate event artist meta to new table');
                    break;

                case self::_CREATE_TABLE_ON_MULTIPLE_SITES:
                    $this->renderConfirmationForm('Install - create tables');
                    break;

                case self::_EVENT_CLONE_CUSTOM_ACCESSIBILITY:
                    $this->renderConfirmationForm('Event - Clone custom accessibility');
                    break;
                case self::_EVENT_CLONE_TO_VIEW_FOR_EXPORTING:
                    $this->renderConfirmationForm('Event - Clone events to a view for exporting');
                    break;
                case self::_VENUE_CLONE_PHONE:
                    $this->renderConfirmationForm('Venue - Clone phone as single field from address');
                    break;
                case self::_VENUE_CLONE_CITY:
                    $this->renderConfirmationForm('Venue - Clone city as single field from address');
                    break;
                case self::_VENUE_CLONE_STATE:
                    $this->renderConfirmationForm('Venue - Clone state as single field from address');
                    break;
                case self::_VENUE_CLONE_ZIP:
                    $this->renderConfirmationForm('Venue - Clone zip as single field from address');
                    break;
                case self::_ORG_CLONE_PHONE:
                    $this->renderConfirmationForm('Organization - Clone phone as single field from address');
                    break;
                case self::_ORG_CLONE_CITY:
                    $this->renderConfirmationForm('Organization - Clone city as single field from address');
                    break;
                case self::_ORG_CLONE_STATE:
                    $this->renderConfirmationForm('Organization - Clone state as single field from address');
                    break;
                case self::_ORG_CLONE_ZIP:
                    $this->renderConfirmationForm('Organization - Clone zip as single field from address');
                    break;
                case self::_ARTIST_CLONE_ZIP:
                    $this->renderConfirmationForm('Artist - Clone zip as single field from address');
                    break;
                case self::_CLASSIFIED_CLONE_NAME:
                    $this->renderConfirmationForm('Classified - Clone name as single field from address');
                    break;
                case self::_CLASSIFIED_CLONE_EMAIL:
                    $this->renderConfirmationForm('Classified - Clone email as single field from address');
                    break;
                case self::_CLASSIFIED_CLONE_PHONE:
                    $this->renderConfirmationForm('Classified - Clone phone as single field from address');
                    break;
                case self::_CLASSIFIED_CLONE_STATE:
                    $this->renderConfirmationForm('Classified - Clone state as single field from address');
                    break;
                case self::_CLASSIFIED_CLONE_CITY:
                    $this->renderConfirmationForm('Classified - Clone city as single field from address');
                    break;
                case self::_PUBLIC_ART_CLONE_STATE:
                    $this->renderConfirmationForm('Public art - Clone state as single field from address');
                    break;

                case self::_ARTIST_CLONE_STATE:
                    $this->renderConfirmationForm('Artist - Clone state as single field from address');
                    break;

                case self::_EDUCATOR_CLONE_STATE:
                    $this->renderConfirmationForm('Educator - Clone state as single field from address');
                    break;

                case self::_EDUCATOR_CLONE_CITY:
                    $this->renderConfirmationForm('Educator - Clone city as single field from address');
                    break;

                case self::_EDUCATOR_CLONE_ZIP:
                    $this->renderConfirmationForm('Educator - Clone city as single field from address');
                    break;

                case self::_ARTIST_CLONE_LNAME:
                    $this->renderConfirmationForm('Artist - Clone Last name as single field from artist meta data');
                    break;

                case self::_PAGE_UPDATE_DEFAULT_CONTENT:
                    $this->renderConfirmationForm('Page - Update default content');
                    break;

                case self::_APL_TOOL_SYNC_ARTIST_DATA:
                    $this->renderConfirmationForm();
                    break;
                case self::_APL_TOOL_FLUSH_REWRITE_RULES_ALL_SITES:
                    $this->renderConfirmationFormFlushRewriteRules();
                    break;
                case self::_APL_TOOL_SYNC_DISCOUNT_URL:
                    $this->renderConfirmationFormDiscountUrl();
                    break;

                case self::_APL_TOOL_SYNC_BUSINESS_ZIP:
                    $this->renderConfirmationFormBusinessZip();
                    break;

                /**
                 * @author Trilm
                 */
                case self::_APL_TOOL_SYNC_REGION_ZIP_CODE_URL:
                    $this->renderConfirmationSyncRegionZipUrl();
                    break;

                case self::_APL_CLEAN_CONTENT_EVENT_THEME_TOOL_DATA:
                    $this->renderCleanContentThemeTool();
                    break;

                case self::_APL_TOOL_SYNC_BUSINESS_REGION_CITY_URL:
                    $this->renderConfirmationSyncBusinessRegionCityUrl();
                    break;

                case self::_APL_TOOL_SYNC_EVENT_PRIVATE_DATA:
                    $this->renderEventPrivateUrl();
                    break;

                /**
                 * @author vulh
                 */
                case self::_APL_TOOL_SYNC_PROG_EDU_ID:
                    $this->renderConfirmationSyncEduId();
                    break;

                case self::_APL_TOOL_CLONE_CLASSIFIED_CITY_URL:
                    $this->renderConfirmationCloneClassifiedCity();
                    break;

	            case self::_APL_TOOL_CLEAN_WASTE_DATA_EVENT_CALENDAR:
                    $this->renderConfirmationcleanWasteDataEventCalendar();
                    break;

                case self::_APL_TOOL_CLONE_SYNDICATION_ORG_ARTIST:
                    $this->renderConfirmationCloneSyndicationOrgArtist();
                    break;

                /* @ticket #15312 */
                case self::_APL_TOOL_MIGRATE_BLOG_USER_TABLE_DATA_TO_USER_MODULES_TABLE:
                    $this->renderConfirmationFormMigrateBlogUserTableDataToUserModulesTable();
                    break;
                case self::_APL_CHECKING_TOOL_MIGRATE_BLOG_USER_TABLE_DATA_TO_USER_MODULES_TABLE:
                    $this->renderConfirmationFormMigrateBlogUserTableDataToUserModulesTable();
                    break;
                default:
                    $this->showErrorMessage(__("Action name is not available!","apollo"));
                    break;
            }
        }

        protected function actuallyRunCurrentTool(){

            switch($this->action){

                case self::_EVENT_ARTIST_META_TO_NEW_TALBE:
                    self::cloneEventArtistMetaToNewTable();
                    break;

                case self::_CREATE_TABLE_ON_MULTIPLE_SITES:
                    self::createTables();
                    break;

                case self::_EVENT_CLONE_CUSTOM_ACCESSIBILITY:
                    self::cloneMetaDataToSingleField(array(
                        'meta_table'  => Apollo_Tables::_APOLLO_EVENT_META,
                        'child_meta_key'  => Apollo_DB_Schema::_APOLLO_EVENT_CUSTOM_ACCESSIBILITY,
                        'parent_meta_key'  => Apollo_DB_Schema::_APOLLO_EVENT_DATA,
                        'post_id_key'  => 'apollo_event_id',
                    ));
                    break;

                case self::_EVENT_CLONE_TO_VIEW_FOR_EXPORTING:
                    self::eventCloneToViewForExporting();
                    break;
                case self::_VENUE_CLONE_PHONE:
                    self::cloneMetaDataToSingleField(array(
                        'meta_table'  => Apollo_Tables::_APL_VENUE_META,
                        'child_meta_key'  => Apollo_DB_Schema::_VENUE_PHONE,
                        'parent_meta_key'  => Apollo_DB_Schema::_APL_VENUE_DATA,
                        'post_id_key'  => 'apollo_venue_id',
                    ));
                    break;
                case self::_VENUE_CLONE_CITY:
                    self::cloneMetaDataToSingleField(array(
                        'meta_table'  => Apollo_Tables::_APL_VENUE_META,
                        'child_meta_key'  => Apollo_DB_Schema::_VENUE_CITY,
                        'parent_meta_key'  => Apollo_DB_Schema::_APL_VENUE_ADDRESS,
                        'post_id_key'  => 'apollo_venue_id',
                    ));
                    break;
                case self::_VENUE_CLONE_STATE:
                    self::cloneMetaDataToSingleField(array(
                        'meta_table'  => Apollo_Tables::_APL_VENUE_META,
                        'child_meta_key'  => Apollo_DB_Schema::_VENUE_STATE,
                        'parent_meta_key'  => Apollo_DB_Schema::_APL_VENUE_ADDRESS,
                        'post_id_key'  => 'apollo_venue_id',
                    ));
                    break;
                case self::_VENUE_CLONE_ZIP:
                    self::cloneMetaDataToSingleField(array(
                        'meta_table'  => Apollo_Tables::_APL_VENUE_META,
                        'child_meta_key'  => Apollo_DB_Schema::_VENUE_ZIP,
                        'parent_meta_key'  => Apollo_DB_Schema::_APL_VENUE_ADDRESS,
                        'post_id_key'  => 'apollo_venue_id',
                    ));
                    break;
                case self::_ORG_CLONE_PHONE:
                    self::cloneMetaDataToSingleField(array(
                        'meta_table'  => Apollo_Tables::_APL_ORG_META,
                        'child_meta_key'  => Apollo_DB_Schema::_ORG_PHONE,
                        'parent_meta_key'  => Apollo_DB_Schema::_APL_ORG_DATA,
                        'post_id_key'  => 'apollo_organization_id',
                    ));
                    break;
                case self::_ORG_CLONE_CITY:
                    self::cloneMetaDataToSingleField(array(
                        'meta_table'  => Apollo_Tables::_APL_ORG_META,
                        'child_meta_key'  => Apollo_DB_Schema::_ORG_CITY,
                        'parent_meta_key'  => Apollo_DB_Schema::_APL_ORG_ADDRESS,
                        'post_id_key'  => 'apollo_organization_id',
                    ));
                    break;
                case self::_ORG_CLONE_STATE:
                    self::cloneMetaDataToSingleField(array(
                        'meta_table'  => Apollo_Tables::_APL_ORG_META,
                        'child_meta_key'  => Apollo_DB_Schema::_ORG_STATE,
                        'parent_meta_key'  => Apollo_DB_Schema::_APL_ORG_ADDRESS,
                        'post_id_key'  => 'apollo_organization_id',
                    ));
                    break;
                case self::_ORG_CLONE_ZIP:
                    self::cloneMetaDataToSingleField(array(
                        'meta_table'  => Apollo_Tables::_APL_ORG_META,
                        'child_meta_key'  => Apollo_DB_Schema::_ORG_ZIP,
                        'parent_meta_key'  => Apollo_DB_Schema::_APL_ORG_ADDRESS,
                        'post_id_key'  => 'apollo_organization_id',
                    ));
                    break;

                case self::_ARTIST_CLONE_ZIP:
                    self::cloneMetaDataToSingleField(array(
                        'meta_table'  => Apollo_Tables::_APOLLO_ARTIST_META,
                        'child_meta_key'  => Apollo_DB_Schema::_APL_ARTIST_ZIP,
                        'parent_meta_key'  => Apollo_DB_Schema::_APL_ARTIST_ADDRESS,
                        'post_id_key'  => 'apollo_artist_id',
                    ));
                    break;

                case self::_CLASSIFIED_CLONE_NAME:
                    self::cloneMetaDataToSingleField(array(
                        'meta_table'  => Apollo_Tables::_APL_CLASSIFIED_META,
                        'child_meta_key'  => Apollo_DB_Schema::_CLASSIFIED_NAME,
                        'parent_meta_key'  => Apollo_DB_Schema::_APL_CLASSIFIED_DATA,
                        'post_id_key'  => 'apollo_classified_id',
                    ));
                    break;
                case self::_CLASSIFIED_CLONE_EMAIL:
                    self::cloneMetaDataToSingleField(array(
                        'meta_table'  => Apollo_Tables::_APL_CLASSIFIED_META,
                        'child_meta_key'  => Apollo_DB_Schema::_CLASSIFIED_EMAIL,
                        'parent_meta_key'  => Apollo_DB_Schema::_APL_CLASSIFIED_DATA,
                        'post_id_key'  => 'apollo_classified_id',
                    ));
                    break;
                case self::_CLASSIFIED_CLONE_PHONE:
                    self::cloneMetaDataToSingleField(array(
                        'meta_table'  => Apollo_Tables::_APL_CLASSIFIED_META,
                        'child_meta_key'  => Apollo_DB_Schema::_CLASSIFIED_PHONE,
                        'parent_meta_key'  => Apollo_DB_Schema::_APL_CLASSIFIED_DATA,
                        'post_id_key'  => 'apollo_classified_id',
                    ));
                    break;

                case self::_CLASSIFIED_CLONE_STATE:
                    self::cloneMetaDataToSingleField(array(
                        'meta_table'  => Apollo_Tables::_APL_CLASSIFIED_META,
                        'child_meta_key'  => Apollo_DB_Schema::_CLASSIFIED_STATE,
                        'other_child_meta_key'  => Apollo_DB_Schema::_CLASSIFIED_TMP_STATE,
                        'parent_meta_key'  => Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS,
                        'post_id_key'  => 'apollo_classified_id',
                        'store_to_field'  =>  Apollo_DB_Schema::_APL_CLASSIFIED_STATE,
                    ));
                    break;
                case self::_CLASSIFIED_CLONE_CITY:
                    self::cloneMetaDataToSingleField(array(
                        'meta_table'  => Apollo_Tables::_APL_CLASSIFIED_META,
                        'child_meta_key'  => Apollo_DB_Schema::_CLASSIFIED_CITY,
                        'parent_meta_key'  => Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS,
                        'post_id_key'  => 'apollo_classified_id',
                        'store_to_field'  =>  Apollo_DB_Schema::_APL_CLASSIFIED_CITY,
                    ));
                    break;
                case self::_PUBLIC_ART_CLONE_STATE:

                    self::cloneMetaDataToSingleField(array(
                        'meta_table'  => Apollo_Tables::_APL_PUBLIC_ART_META,
                        'child_meta_key'  => Apollo_DB_Schema::_PUBLIC_ART_STATE,
                        'parent_meta_key'  => Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS,
                        'post_id_key'  => 'apollo_public_art_id',
                    ));
                    break;

                case self::_ARTIST_CLONE_STATE:

                    self::cloneMetaDataToSingleField(array(
                        'meta_table'  => Apollo_Tables::_APOLLO_ARTIST_META,
                        'child_meta_key'  => Apollo_DB_Schema::_APL_ARTIST_STATE,
                        'parent_meta_key'  => Apollo_DB_Schema::_APL_ARTIST_ADDRESS,
                        'post_id_key'  => 'apollo_artist_id',
                    ));
                    break;

                case self::_EDUCATOR_CLONE_STATE:

                    self::cloneMetaDataToSingleField(array(
                        'meta_table'  => Apollo_Tables::_APL_EDUCATOR_META,
                        'child_meta_key'  => Apollo_DB_Schema::_APL_EDUCATOR_STATE,
                        'parent_meta_key'  => Apollo_DB_Schema::_APL_EDUCATOR_DATA,
                        'post_id_key'  => 'apollo_educator_id',
                    ));
                    break;

                case self::_EDUCATOR_CLONE_CITY:

                    self::cloneMetaDataToSingleField(array(
                        'meta_table'  => Apollo_Tables::_APL_EDUCATOR_META,
                        'child_meta_key'  => Apollo_DB_Schema::_APL_EDUCATOR_CITY,
                        'parent_meta_key'  => Apollo_DB_Schema::_APL_EDUCATOR_DATA,
                        'post_id_key'  => 'apollo_educator_id',
                    ));
                    break;

                case self::_EDUCATOR_CLONE_ZIP:

                    self::cloneMetaDataToSingleField(array(
                        'meta_table'  => Apollo_Tables::_APL_EDUCATOR_META,
                        'child_meta_key'  => Apollo_DB_Schema::_APL_EDUCATOR_ZIP,
                        'parent_meta_key'  => Apollo_DB_Schema::_APL_EDUCATOR_DATA,
                        'post_id_key'  => 'apollo_educator_id',
                    ));
                    break;

                case self::_ARTIST_CLONE_LNAME:
                    self::cloneMetaDataToSingleField(array(
                        'meta_table'  => Apollo_Tables::_APOLLO_ARTIST_META,
                        'child_meta_key'  => Apollo_DB_Schema::_APL_ARTIST_LNAME,
                        'parent_meta_key'  => Apollo_DB_Schema::_APL_ARTIST_DATA,
                        'post_id_key'  => 'apollo_artist_id',
                    ));
                    break;

                case self::_PAGE_UPDATE_DEFAULT_CONTENT:
                    self::pageUpdateDefaultContent();
                    break;

                case self::_APL_TOOL_SYNC_ARTIST_DATA:
                    $this->syncArtistData();
                    break;
                case self::_APL_TOOL_FLUSH_REWRITE_RULES_ALL_SITES:
                    $this->flushRewriteRulesForAllSites();
                    break;
                case self::_APL_TOOL_SYNC_DISCOUNT_URL:
                    $this->cloneDiscountURLFromArrayToSingle();
                    break;


                case self::_APL_TOOL_SYNC_BUSINESS_ZIP:
                    $this->cloneOrgZipToBusinessZip();
                    break;

                /**
                 * @author: Trilm
                 * sync region zip code
                 *
                 */
                case self::_APL_TOOL_SYNC_REGION_ZIP_CODE_URL:
                    $this->syncRegionZipcode();
                    break;

                case self::_APL_CLEAN_CONTENT_EVENT_THEME_TOOL_DATA:
                    $this->cleanContentEventThemeTool();
                    break;

                case self::_APL_TOOL_SYNC_BUSINESS_REGION_CITY_URL:
                    $this->syncCityRegionBusiness();
                    break;

                case self::_APL_TOOL_SYNC_EVENT_PRIVATE_DATA:
                    $this->syncPrivateEvents();
                    break;

                /**
                 * @author: vulh
                 * sync educator_id in apollo_program_data meta
                 *
                 */
                case self::_APL_TOOL_SYNC_PROG_EDU_ID:
                    $this->syncProgEduId();
                    break;

                case self::_APL_TOOL_CLONE_CLASSIFIED_CITY:
                    $this->cloneCityFormClassifiedAddress();
                    break;

                case self::_APL_TOOL_CLEAN_WASTE_DATA_EVENT_CALENDAR:
                    $this->cleanWasteDataEventCalendar();
                    break;

                case self::_APL_TOOL_CLONE_SYNDICATION_ORG_ARTIST:
                    $this->cloneSyndicationArtOrg();
                    break;

                /* @ticket #15312 */
                case self::_APL_TOOL_MIGRATE_BLOG_USER_TABLE_DATA_TO_USER_MODULES_TABLE:
                    $this->migrateBlogUserTableDataToUserModulesTable();
                    break;
                    
                case self::_APL_CHECKING_TOOL_MIGRATE_BLOG_USER_TABLE_DATA_TO_USER_MODULES_TABLE:
                    $this->checkingMigrateBlogUserTableDataToUserModulesTable();

                default:
                    $this->showErrorMessage(__("Action name is not available!","apollo"));
                    break;
            }
        }

        protected static function checkApplyingAllSite($siteIDs = ''){
            return !empty($siteIDs) && !is_array($siteIDs) && $siteIDs === self::_APL_TOOL_ALL_SITE_IDS;
        }

        protected static function getAction(){
            return isset($_GET['action']) && !empty($_GET['action']) ? $_GET['action'] : '';
        }

        protected static function getAllSiteIDs(){
            try{
                $aqlQuery = new Apl_Query(Apollo_Tables::_APL_SITES);
                $resultQuery = $aqlQuery->get_where('1=1',Apollo_DB_Schema::_APL_NETWORK_FIELD_BLOG_ID);
                $siteIDs = array();
                if(!empty($resultQuery)){
                    foreach($resultQuery as $item){
                        $item = is_object($item) ? get_object_vars($item) : $item;
                        $siteIDs[] = $item[Apollo_DB_Schema::_APL_NETWORK_FIELD_BLOG_ID];
                    }
                }
                return $siteIDs;
            }catch (Exception $ex){
                self::log($ex->getMessage(),__CLASS__.":getAllSiteIDs");
            }
        }

        protected static function getSiteIDs(){
            $siteIDs = isset($_GET['site_ids']) && !empty($_GET['site_ids']) ? $_GET['site_ids'] : '';
            if(empty($siteIDs)) return '';
            self::$qsSiteIDs = $siteIDs;
            // Case : ALL - update for all sites in network. If specific site isn't needed to update so the script simply does nothing
            if($siteIDs === self::_APL_TOOL_ALL_SITE_IDS) return self::getAllSiteIDs();
            if($siteIDs === self::_APL_TOOL_SINGLE_SITE_ID) return array(get_current_blog_id());
            $siteIDs = trim($siteIDs,',');
            // Case : {list of site_ids} etc: 1,2,3 - update for a bulk of sites. If specific site isn't needed to update so the script simply does nothing
            $arrSiteIDs = explode(',',$siteIDs);
            if(!empty($arrSiteIDs) && is_array($arrSiteIDs)){
                $resultSiteIDs = array();
                foreach($arrSiteIDs as $sid){
                    $sid = preg_replace("/[^0-9]/", "",$sid);
                    if(!empty($sid)){
                        $resultSiteIDs[] = $sid;
                    }
                }
                return $resultSiteIDs;
            }

            // Case : {site_id} of specific site - update for specific site only. If this site isn't needed to update so the script simply does nothing
            $siteIDs = preg_replace("/[^0-9]/", "",$siteIDs);
            return array($siteIDs);
        }

        protected function canUseUtilityTools(){
            if(empty($this->action)) return false;
            if(empty($this->siteIDs)) return false;
            if(is_super_admin()) {
                $this->isLoggedIn = true;
            }
            return true;
        }

        protected function canRunUtilityTools(){
            if(    ! isset($_POST[self::_APL_TOOL_NONCE_FIELD])
                || empty($_POST[self::_APL_TOOL_NONCE_FIELD])
                || ! wp_verify_nonce($_POST[self::_APL_TOOL_NONCE_FIELD],$this->action)){
                return false;
            }
            return true;
        }

        protected function renderConfirmationForm($msg = ''){

            if ($msg == '') {
                $msg = __("This script is used for synchronizing artist profile data.","apollo");
            }

            echo Apollo_App::getTemplatePartCustom(APOLLO_INCLUDES_DIR . '/scripts/html/utility-tool-confirmation.php',array(
                'confirmationMessage' => $msg,
                'proceedUrl' => $this->buildUrlForProcessingUtilityTool(),
                'aplNonceField' => wp_nonce_field($this->action,self::_APL_TOOL_NONCE_FIELD,false,false),
                'errorMessage' => $this->getErrorMessage(),
                'successMessage' => $this->getSuccessMessage(),
                'isLoggedIn' => $this->isLoggedIn
            ));
        }

        /**
         * Display confirmation form for flush rewrite rules all sites feature
         *
         * @ticket #11129
         */
        protected function renderConfirmationFormFlushRewriteRules()
        {
            echo Apollo_App::getTemplatePartCustom(APOLLO_INCLUDES_DIR . '/scripts/html/utility-tool-confirmation.php',array(
                'confirmationMessage' => __("This script is used to flush rewrite rules for all sites.", "apollo"),
                'proceedUrl'          => $this->buildUrlForProcessingUtilityTool(),
                'aplNonceField'       => wp_nonce_field($this->action, self::_APL_TOOL_NONCE_FIELD, false, false),
                'errorMessage'        => $this->getErrorMessage(),
                'successMessage'      => $this->getSuccessMessage(),
                'isLoggedIn'          => $this->isLoggedIn,
            ));
        }

        protected function renderConfirmationFormDiscountUrl(){
            echo Apollo_App::getTemplatePartCustom(APOLLO_INCLUDES_DIR . '/scripts/html/utility-tool-confirmation.php',array(
                'confirmationMessage' => __("This script is used for synchronizing discount url data.","apollo"),
                'proceedUrl' => $this->buildUrlForProcessingUtilityTool(),
                'aplNonceField' => wp_nonce_field($this->action,self::_APL_TOOL_NONCE_FIELD,false,false),
                'errorMessage' => $this->getErrorMessage(),
                'successMessage' => $this->getSuccessMessage(),
                'isLoggedIn' => $this->isLoggedIn
            ));
        }


        protected function renderConfirmationFormBusinessZip() {
            echo Apollo_App::getTemplatePartCustom(APOLLO_INCLUDES_DIR . '/scripts/html/utility-tool-confirmation.php',array(
                'confirmationMessage' => __("This script is used for synchronizing ORG ZIP to a business.","apollo"),
                'proceedUrl' => $this->buildUrlForProcessingUtilityTool(),
                'aplNonceField' => wp_nonce_field($this->action,self::_APL_TOOL_NONCE_FIELD,false,false),
                'errorMessage' => $this->getErrorMessage(),
                'successMessage' => $this->getSuccessMessage(),
                'isLoggedIn' => $this->isLoggedIn
            ));
        }

        /**
         *
         * @author TriLM
         * @param array $queryArgs
         * @return string
         *
         */
        protected function renderConfirmationSyncRegionZipUrl(){
            echo Apollo_App::getTemplatePartCustom(APOLLO_INCLUDES_DIR . '/scripts/html/utility-tool-confirmation.php',array(
                'confirmationMessage' => __("This script is used for synchronizing ZIP data.","apollo"),
                'proceedUrl' => $this->buildUrlForProcessingUtilityTool(),
                'aplNonceField' => wp_nonce_field($this->action,self::_APL_TOOL_NONCE_FIELD,false,false),
                'errorMessage' => $this->getErrorMessage(),
                'successMessage' => $this->getSuccessMessage(),
                'isLoggedIn' => $this->isLoggedIn
            ));
        }

        protected function renderCleanContentThemeTool(){
            echo Apollo_App::getTemplatePartCustom(APOLLO_INCLUDES_DIR . '/scripts/html/utility-tool-confirmation.php',array(
                'confirmationMessage' => __("This script is used for clean event theme tool data.","apollo"),
                'proceedUrl' => $this->buildUrlForProcessingUtilityTool(),
                'aplNonceField' => wp_nonce_field($this->action,self::_APL_TOOL_NONCE_FIELD,false,false),
                'errorMessage' => $this->getErrorMessage(),
                'successMessage' => $this->getSuccessMessage(),
                'isLoggedIn' => $this->isLoggedIn
            ));
        }

        protected function renderConfirmationSyncBusinessRegionCityUrl() {
            echo Apollo_App::getTemplatePartCustom(APOLLO_INCLUDES_DIR . '/scripts/html/utility-tool-confirmation.php',array(
                'confirmationMessage' => __("This script is used for synchronizing Business Region/City data.","apollo"),
                'proceedUrl' => $this->buildUrlForProcessingUtilityTool(),
                'aplNonceField' => wp_nonce_field($this->action,self::_APL_TOOL_NONCE_FIELD,false,false),
                'errorMessage' => $this->getErrorMessage(),
                'successMessage' => $this->getSuccessMessage(),
                'isLoggedIn' => $this->isLoggedIn
            ));
        }

        /**
         *
         * @author vulh
         * @param array $queryArgs
         * @return string
         *
         */
        protected function renderEventPrivateUrl(){
            echo Apollo_App::getTemplatePartCustom(APOLLO_INCLUDES_DIR . '/scripts/html/utility-tool-confirmation.php',array(
                'confirmationMessage' => __("This script is used for synchronizing event private data.","apollo"),
                'proceedUrl' => $this->buildUrlForProcessingUtilityTool(),
                'aplNonceField' => wp_nonce_field($this->action,self::_APL_TOOL_NONCE_FIELD,false,false),
                'errorMessage' => $this->getErrorMessage(),
                'successMessage' => $this->getSuccessMessage(),
                'isLoggedIn' => $this->isLoggedIn
            ));
        }

        /**
         *
         * @author vulh
         * @param array $queryArgs
         * @return string
         *
         */
        protected function renderConfirmationSyncEduId(){
            echo Apollo_App::getTemplatePartCustom(APOLLO_INCLUDES_DIR . '/scripts/html/utility-tool-confirmation.php',array(
                'confirmationMessage' => __("This script is used for synchronizing Educator ID data.","apollo"),
                'proceedUrl' => $this->buildUrlForProcessingUtilityTool(),
                'aplNonceField' => wp_nonce_field($this->action,self::_APL_TOOL_NONCE_FIELD,false,false),
                'errorMessage' => $this->getErrorMessage(),
                'successMessage' => $this->getSuccessMessage(),
                'isLoggedIn' => $this->isLoggedIn
            ));
        }

        protected function renderConfirmationCloneClassifiedCity(){
            echo Apollo_App::getTemplatePartCustom(APOLLO_INCLUDES_DIR . '/scripts/html/utility-tool-confirmation.php',array(
                'confirmationMessage' => __("This script is used for Classified city data.","apollo"),
                'proceedUrl' => $this->buildUrlForProcessingUtilityTool(),
                'aplNonceField' => wp_nonce_field($this->action,self::_APL_TOOL_NONCE_FIELD,false,false),
                'errorMessage' => $this->getErrorMessage(),
                'successMessage' => $this->getSuccessMessage(),
                'isLoggedIn' => $this->isLoggedIn
            ));
        }

        protected function renderConfirmationCloneSyndicationOrgArtist(){
            echo Apollo_App::getTemplatePartCustom(APOLLO_INCLUDES_DIR . '/scripts/html/utility-tool-confirmation.php',array(
                'confirmationMessage' => __("This script is used for syndication ORG and ARTIST.","apollo"),
                'proceedUrl' => $this->buildUrlForProcessingUtilityTool(),
                'aplNonceField' => wp_nonce_field($this->action,self::_APL_TOOL_NONCE_FIELD,false,false),
                'errorMessage' => $this->getErrorMessage(),
                'successMessage' => $this->getSuccessMessage(),
                'isLoggedIn' => $this->isLoggedIn
            ));
        }


        protected function buildUrlForProcessingUtilityTool($queryArgs = array()){
            $mainQueryArgs = array(
                'action' => $this->action,
                'site_ids' => self::$qsSiteIDs,
            );
            $mainQueryArgs = array_merge($mainQueryArgs,$queryArgs);
            $queryString = Apollo_App::apolloBuildHttpQuery($mainQueryArgs,false);
            return network_site_url('/script/?' . $queryString);
        }


        /**
            Sync artist data section
         */
        protected function syncArtistData() {
            try{
                if(!empty($this->siteIDs)){
                    $errorAll = array();
                    $successAll = array();
                    foreach($this->siteIDs as $siteID){
                        $site = get_blog_details($siteID);
                        if(empty($site)) {
                            $errorAll[] = sprintf("SiteID %s is not existed!",$siteID);
                            continue;
                        }
                        $counterSyncEachSite = 0;
                        $errors = array();
                        $this->syncArtistDataBySiteId($siteID,$errors,$counterSyncEachSite);
                        $site = get_object_vars($site);
                        $blogName = !empty($site['blogname']) ? $site['blogname'] : $site['domain'];
                        if(!empty($errors)){
                            $errors = implode('<br/>',$errors);
                            $errorAll[] = $site['blog_id'] . '-' . $blogName . ': ' . $errors;
                        } else {
                            $successAll[] = $site['blog_id'] . '-' . $blogName . ': ' . $counterSyncEachSite . ' instance(s) are synchronized!';
                        }
                    }
                    $this->setErrorMessage($errorAll);
                    $this->setSuccessMessage($successAll);
                    wp_redirect($this->buildUrlForProcessingUtilityTool());
                }
            }catch (Exception $ex){
                self::log($ex->getMessage(),__CLASS__.':syncArtistData');
                $this->showErrorMessage('');
            }
        }

        protected function getMetaDataMultiSite($queryArgs = array()){
            try{
                $table = isset($queryArgs['table']) && !empty($queryArgs['table']) ? $queryArgs['table'] : '';
                $key = isset($queryArgs['key']) && !empty($queryArgs['key']) ? $queryArgs['key'] : '';
                $postID = isset($queryArgs['post-id']) && !empty($queryArgs['post-id']) ? $queryArgs['post-id'] : 0;
                $postIDCol = isset($queryArgs['post-id-col']) && !empty($queryArgs['post-id-col']) ? $queryArgs['post-id-col'] : '';
                $select = isset($queryArgs['select']) && !empty($queryArgs['select']) ? $queryArgs['select'] : '*';
                $siteID = isset($queryArgs['site-id']) && !empty($queryArgs['site-id']) ? $queryArgs['site-id'] : '';
                if(!empty($postID) && !empty($postIDCol)){
                    $postIDQS = " AND ".$postIDCol."='".$postID."'";
                } else {
                    $postIDQS = "";
                }
                if(empty($table) || empty($key) || empty($siteID)) return null;
                $aqlQuery = new Apl_Query(self::getTableNameBySiteID($table,$siteID));
                $metaData = $aqlQuery->get_where("meta_key = '".$key."' ".$postIDQS." ",$select);
                return $metaData;
            }catch (Exception $ex){
                self::log($ex->getMessage(),__CLASS__.':getMetaDataMultiSite');
            }
        }

        protected function addMetaDataMultiSite($queryArgs = array()){
            try{
                $table = isset($queryArgs['table']) && !empty($queryArgs['table']) ? $queryArgs['table'] : '';
                $siteID = isset($queryArgs['site-id']) && !empty($queryArgs['site-id']) ? $queryArgs['site-id'] : '';
                $addedValue = isset($queryArgs['added-value']) && !empty($queryArgs['added-value']) ? $queryArgs['added-value'] : '';
                if(empty($table) || empty($addedValue) || empty($siteID)) return false;
                $aqlQuery = new Apl_Query(self::getTableNameBySiteID($table,$siteID));
                $aqlQuery->insert($addedValue);
                return true;
            }catch (Exception $ex){
                self::log($ex->getMessage(),__CLASS__.':addMetaDataMultiSite');
                return false;
            }
        }

        protected function getPostDataByPostIDMultiSite($queryArgs = array()){
            try{
                $table = isset($queryArgs['table']) && !empty($queryArgs['table']) ? $queryArgs['table'] : '';
                $postID = isset($queryArgs['post-id']) && !empty($queryArgs['post-id']) ? $queryArgs['post-id'] : 0;
                $select = isset($queryArgs['select']) && !empty($queryArgs['select']) ? $queryArgs['select'] : '*';
                $siteID = isset($queryArgs['site-id']) && !empty($queryArgs['site-id']) ? $queryArgs['site-id'] : '';
                if(empty($table) || empty($postID) || empty($siteID)) return null;
                $aqlQuery = new Apl_Query(self::getTableNameBySiteID($table,$siteID));
                $post = $aqlQuery->get_where("ID = '".$postID."'",$select);
                return $post;
            }catch (Exception $ex){
                self::log($ex->getMessage(),__CLASS__.':getPostDataByPostIDMultiSite');
            }
        }

        protected function syncArtistDataBySiteId($siteID = 0, &$errors, &$counterSync){
            try{
                if(!empty($siteID) && $siteID !== 0){
                    $qArgsArtistMeta = array(
                        'table' => Apollo_Tables::_APOLLO_ARTIST_META,
                        'key' => Apollo_DB_Schema::_APL_ARTIST_DATA,
                        'site-id' => $siteID,
                    );
                    $metaData = $this->getMetaDataMultiSite($qArgsArtistMeta);
                    if ($metaData) {
                        foreach( $metaData as $mt ) {
                            $qArgsArtist = array(
                                'table' => 'posts',
                                'post-id' => $mt->apollo_artist_id,
                                'site-id' => $siteID,
                            );
                            $artistPost = $this->getPostDataByPostIDMultiSite($qArgsArtist);
                            if(empty($artistPost)) {
                                // these are artist post which is existing in artist_meta_data but not existing in artist_post
                                // TODO: need implement a new tool to clean waste post meta data of gross system modules.
                                // At present, They are skipped. We only sync all artist lastname for available artist post
                                continue;
                            }
                            $qsArtistLName = array(
                                'table' => Apollo_Tables::_APOLLO_ARTIST_META,
                                'key' => Apollo_DB_Schema::_APL_ARTIST_LNAME,
                                'site-id' => $siteID,
                                'post-id' => $mt->apollo_artist_id,
                                'post-id-col' => 'apollo_artist_id'
                            );
                            $atLastName = $this->getMetaDataMultiSite($qsArtistLName);
                            if(!empty($atLastName)){
                                continue;
                            }
                            // maybe_unserialize twice due to mechanism serialize of wordpress core when save_post.
                            // TODO: find the better way for this.
                            $artistMetaData = maybe_unserialize($mt->meta_value);
                            $artistMetaData = maybe_unserialize($artistMetaData);
                            $atLNameInArray = isset($artistMetaData[Apollo_DB_Schema::_APL_ARTIST_LNAME]) ? $artistMetaData[Apollo_DB_Schema::_APL_ARTIST_LNAME] : '';
                            $this->addMetaDataMultiSite(
                                array(
                                    'table' => Apollo_Tables::_APOLLO_ARTIST_META,
                                    'site-id' => $siteID,
                                    'added-value' => array(
                                        'apollo_artist_id' => $mt->apollo_artist_id,
                                        'meta_key' => Apollo_DB_Schema::_APL_ARTIST_LNAME,
                                        'meta_value' => $atLNameInArray
                                    )
                                )
                            );
                            $counterSync++;
                        }
                    }
                }
            } catch (Exception $ex){
                self::log($ex->getMessage(),__CLASS__.':syncArtistDataBySiteId');
            }
        }

        protected static function getTableNameBySiteID($table,$siteID) {
            global $wpdb;
            $siteId = is_main_site($siteID) ? '' : $siteID . '_';
            return $wpdb->prefix.''.$siteId. $table;
        }


        protected function cloneDiscountURLFromArrayToSingle() {
            global $wpdb;
            $metaTbl = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};
            $current = current_time('Y-m-d');

            $sql = "
            SELECT * FROM $metaTbl mta WHERE mta.meta_key = '".Apollo_DB_Schema::_APOLLO_EVENT_DATA."' AND mta.apollo_event_id IN
            ( SELECT p.ID from $wpdb->posts p
            INNER JOIN $metaTbl mt ON mt.apollo_event_id = p.ID
            WHERE p.post_type = 'event' AND p.post_status = 'publish'
            AND mt.meta_key = '".Apollo_DB_Schema::_APOLLO_EVENT_END_DATE."'
            AND CAST( mt.meta_value AS DATE ) >= '{$current}'
            GROUP BY p.ID )
        ";
            $metaData = $wpdb->get_results($sql);
            $updateEvents = array();
            if ($metaData) {
                $count = 0;
                foreach ($metaData as $mt) {
                    $event = get_event($mt->apollo_event_id);
                    $discountUrl = $event->get_meta_data(Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL);
                    $discountUrlInArr = $event->get_meta_data(Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL, Apollo_DB_Schema::_APOLLO_EVENT_DATA);

                    if ($discountUrl) {
                        continue;
                    }

                    if (isset($_GET['confirm']) && $_GET['confirm'] == 'yes') {
                        update_apollo_meta($mt->apollo_event_id, Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL, $discountUrlInArr);
                    } else {
                        $updateEvents[] = $mt->apollo_event_id;
                        $count++;
                    }
                }

                if (isset($_GET['confirm']) || $_GET['confirm'] != 'yes') {
                    if ($count) {
                        $this->setSuccessMessage(sprintf("<p>There are %s events need to be update the last name. Do you want to continue for upgrading? Please type confirm = yes</p>", $count));
                        //aplDebug($updateEvents);
                    } else {
                        $this->setSuccessMessage('Done');
                    }
                } else {
                    $this->setSuccessMessage('Done');
                }
            }
            wp_redirect($this->buildUrlForProcessingUtilityTool());
        }

        protected function cloneOrgZipToBusinessZip() {


            global $wpdb;

            if (isset($_SESSION['successMessage'])) {
                unset($_SESSION['successMessage']);
            }

            unset($_SESSION['successMessage']);
            if(!empty($this->siteIDs)) {
                $errorAll = array();
                $successAll = array();

                foreach ($this->siteIDs as $siteID) {
                    if (switch_to_blog($siteID)) {
                        $site = get_blog_details($siteID);
                        $blogName = $site->domain;
                        if (empty($site)) {
                            $errorAll[] = sprintf("SiteID %s is not existed!", $siteID);
                            restore_current_blog();
                            continue;
                        }

                        global $wpdb;
                        $metaTbl = $wpdb->{Apollo_Tables::_APL_ORG_META};
                        $sql = "
                            SELECT * FROM $metaTbl mta WHERE mta.meta_key = '".Apollo_DB_Schema::_APL_ORG_BUSINESS."'
                        ";
                        $metaData = $wpdb->get_results($sql);

                        if ($metaData) {
                            $count = 0;
                            foreach ($metaData as $mt) {
                                $org = get_org($mt->apollo_organization_id);
                                $zip = $org->get_meta_data(Apollo_DB_Schema::_ORG_ZIP);

                                if (!$zip) {
                                    continue;
                                }
                                $count++;
                                update_apollo_meta($mt->meta_value, Apollo_DB_Schema::_BUSINESS_ZIP, $zip);
                            }
                        }

                        if($count){
                            $successAll[] = "There are $count instance(s) are clone on the blog $blogName";
                        }
                        else {
                            $successAll[] = "<p>Nothing in: ". $blogName. '</p>';
                        }
                        restore_current_blog();
                    }
                }
            }

            $this->setSuccessMessage(implode( ' <br/>', $successAll));

            wp_redirect($this->buildUrlForProcessingUtilityTool());
        }


        protected function cloneCityFormClassifiedAddress() {

            global $wpdb;

            if (isset($_SESSION['successMessage'])) {
                unset($_SESSION['successMessage']);
            }

            unset($_SESSION['successMessage']);
            if(!empty($this->siteIDs)) {
                $errorAll = array();
                $successAll = array();

                foreach ($this->siteIDs as $siteID) {
                    $site = get_blog_details($siteID);

                    if (empty($site)) {
                        $errorAll[] = sprintf("SiteID %s is not existed!", $siteID);
                        continue;
                    }

                    $prefix = is_main_site($siteID) ?  $wpdb->base_prefix : $wpdb->base_prefix. $siteID. '_';

                    $aqlQuery = new Apl_Query($prefix. Apollo_Tables::_APL_CLASSIFIED_META, true);
                    $metaData = $aqlQuery->get_where("meta_key = '".Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS."'");
                    $count = 0;
                    if ($metaData) {

                        foreach( $metaData as $mt ) {
                            $classified = get_classified($mt->apollo_classified_id);

                            $classifiedCity = $classified->get_meta_data(Apollo_DB_Schema::_APL_CLASSIFIED_CITY);
                            $classifiedCityInAddress = $classified->get_meta_data(Apollo_DB_Schema::_CLASSIFIED_CITY, Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS);
                            $classifiedCityTmpInAddress = $classified->get_meta_data(Apollo_DB_Schema::_CLASSIFIED_TMP_CITY, Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS);
                            $cityVal = $classifiedCityInAddress ? $classifiedCityInAddress : $classifiedCityTmpInAddress;
                            if($classifiedCity || !$classifiedCityInAddress) {
                                continue;
                            }

                            $count++;

                            update_apollo_meta($mt->apollo_classified_id,Apollo_DB_Schema::_APL_CLASSIFIED_CITY,$cityVal);
                        }

                        $blogName = $site->domain;
                    }

                    if($count){
                        $successAll[] = "There are $count instance(s) are clone on the blog $blogName";
                    }
                    else {
                        $successAll[] = "<p>Nothing in: ". $blogName. '</p>';
                    }
                }
            }

            $this->setSuccessMessage(implode( ' <br/>', $successAll));

            wp_redirect($this->buildUrlForProcessingUtilityTool());

        }

        protected static function log($message_log, $header_log){
            $date = date('Y/m/d H:i:s'). ': ';
            $location = ($header_log != '' ? $header_log . ': ' : '');
            $valueToFile = json_encode($message_log);
            $upload_dir = wp_upload_dir();
            $fp = fopen($upload_dir['basedir'] . '/' . self::_APL_LOG_FILE_NAME,'a');
            fwrite($fp, $date. $location. $valueToFile."\r\n");
            fclose($fp);
        }

        /**
         * @author: Trilm
         * Update zip_code of venue
         *
         */
        public function syncRegionZipcode(){
            global $wpdb;

            if (isset($_SESSION['successMessage'])) {
                unset($_SESSION['successMessage']);
            }

            unset($_SESSION['successMessage']);
            if(!empty($this->siteIDs)) {
                $errorAll = array();
                $successAll = array();

                foreach ($this->siteIDs as $siteID) {
                    $site = get_blog_details($siteID);

                    if (empty($site)) {
                        $errorAll[] = sprintf("SiteID %s is not existed!", $siteID);
                        continue;
                    }

                    $prefix = is_main_site($siteID) ?  $wpdb->base_prefix : $wpdb->base_prefix. $siteID. '_';

                    $aqlQuery = new Apl_Query($prefix. Apollo_Tables::_APL_VENUE_META, true);
                    $metaData = $aqlQuery->get_where("meta_key = '".Apollo_DB_Schema::_APL_VENUE_ADDRESS."'");

                    if ($metaData) {
                        $count = 0;
                        foreach( $metaData as $mt ) {
                            $venue = get_venue($mt->apollo_venue_id);

                            $venueZip = $venue->get_meta_data(Apollo_DB_Schema::_VENUE_ZIP);
                            $venueZipInAddress = $venue->get_meta_data(Apollo_DB_Schema::_VENUE_ZIP, Apollo_DB_Schema::_APL_VENUE_ADDRESS);

                            if($venueZip || !$venueZipInAddress) {
                                continue;
                            }

                            $count++;

                            update_apollo_meta($mt->apollo_venue_id,Apollo_DB_Schema::_VENUE_ZIP,$venueZipInAddress);
                        }

                        $blogName = $site->domain;
                    }

                    if($count){
                        $successAll[] = "There are $count instance(s) are synchronized on the blog $blogName";
                    }
                    else {
                        $successAll[] = "<p>Nothing in: ". $blogName. '</p>';
                    }
                }
            }

            $this->setSuccessMessage(implode( ' <br/>', $successAll));


            wp_redirect($this->buildUrlForProcessingUtilityTool());
        }


        /**
         * Clean \r\n of the Theme Tool content
         * @author: vulh
         * @return mixed
         */
        public function cleanContentEventThemeTool(){
            global $wpdb;

            if (isset($_SESSION['successMessage'])) {
                unset($_SESSION['successMessage']);
            }

            unset($_SESSION['successMessage']);
            if(!empty($this->siteIDs)) {
                $errorAll = array();
                $successAll = array();

                foreach ($this->siteIDs as $siteID) {
                    $site = get_blog_details($siteID);

                    if (empty($site)) {
                        $errorAll[] = sprintf("SiteID %s is not existed!", $siteID);
                        continue;
                    }

                    $prefix = is_main_site($siteID) ?  $wpdb->base_prefix : $wpdb->base_prefix. $siteID. '_';

                    $aqlQuery = new Apl_Query($prefix. Apollo_Tables::_APL_THEME_TOOL, true);
                    $items = $aqlQuery->get_where();
                    $executed = 0;
                    if ($items) {
                        $searchArr = array(
                            '\r\n', '\&quot;'
                        );
                        $replaceArr = ['', ''];
                        foreach($items as $item) {
                            $aqlQuery->update([
                                'top_desc' => str_replace($searchArr, $replaceArr, $item->top_desc),
                                'bottom_desc' => str_replace($searchArr, $replaceArr, $item->bottom_desc),
                                'associated_orgs_desc' => str_replace($searchArr, $replaceArr, $item->associated_orgs_desc),
                                'associated_venues_desc' => str_replace($searchArr, $replaceArr, $item->associated_venues_desc),
                            ], [
                                'id' => $item->id
                            ]);
                            $executed++;
                        }
                    }

                    $blogName = $site->domain;
                    if($executed){
                        $successAll[] = "There are $executed instance(s) are cleaned on the blog $blogName";
                    }
                    else {
                        $successAll[] = "<p>Nothing in: ". $blogName. '</p>';
                    }
                }
            }

            $this->setSuccessMessage(implode( ' <br/>', $successAll));


            wp_redirect($this->buildUrlForProcessingUtilityTool());
        }

        /**
         * @author: vulh
         * Sync region city to a dependence field
         *
         */
        public function syncCityRegionBusiness(){
            global $wpdb;

            if (isset($_SESSION['successMessage'])) {
                unset($_SESSION['successMessage']);
            }

            unset($_SESSION['successMessage']);
            if(!empty($this->siteIDs)) {

                $errorAll = array();
                $successAll = array();

                foreach ($this->siteIDs as $siteID) {
                    switch_to_blog($siteID);

                    $site = get_blog_details($siteID);

                    if (empty($site)) {
                        $errorAll[] = sprintf("SiteID %s is not existed!", $siteID);
                        continue;
                    }

                    $prefix = is_main_site($siteID) ?  $wpdb->base_prefix : $wpdb->base_prefix. $siteID. '_';

                    $aqlQuery = new Apl_Query($prefix. Apollo_Tables::_APL_ORG_META, true);
                    $metaData = $aqlQuery->get_where("meta_key = '".Apollo_DB_Schema::_APL_ORG_BUSINESS."'");

                    if ($metaData) {
                        $count = 0;
                        foreach( $metaData as $mt ) {
                            $org = get_org($mt->apollo_organization_id);

                            $businessId = $org->get_meta_data(Apollo_DB_Schema::_APL_ORG_BUSINESS);

                            if (!$businessId) continue;

                            if($city = $org->get_meta_data(Apollo_DB_Schema::_ORG_CITY, Apollo_DB_Schema::_APL_ORG_ADDRESS)) {
                                update_apollo_meta($businessId, Apollo_DB_Schema::_BUSINESS_CITY, $city);
                            }

                            if($region = $org->get_meta_data(Apollo_DB_Schema::_ORG_REGION, Apollo_DB_Schema::_APL_ORG_ADDRESS)) {
                                update_apollo_meta($businessId, Apollo_DB_Schema::_BUSINESS_REGION, $region);
                            }

                            $count++;
                        }

                        $blogName = $site->domain;
                    }

                    if($count){
                        $successAll[] = "There are $count instance(s) are synchronized on the blog $blogName";
                    }
                    else {
                        $successAll[] = "<p>Nothing in: ". $blogName. '</p>';
                    }
                }
            }

            $this->setSuccessMessage(implode( ' <br/>', $successAll));

            switch_to_blog( 1 );
            wp_redirect($this->buildUrlForProcessingUtilityTool());
        }

        /**
         * @author: Vulh
         * Update zip_code of venue
         *
         */
        public function syncPrivateEvents(){

            global $wpdb;

            if (isset($_SESSION['successMessage'])) {
                unset($_SESSION['successMessage']);
            }

            unset($_SESSION['successMessage']);
            if(!empty($this->siteIDs)) {
                $errorAll = array();
                $successAll = array();

                foreach ($this->siteIDs as $siteID) {
                    $site = get_blog_details($siteID);

                    if (empty($site)) {
                        $errorAll[] = sprintf("SiteID %s is not existed!", $siteID);
                        continue;
                    }

                    $prefix = is_main_site($siteID) ?  $wpdb->base_prefix : $wpdb->base_prefix. $siteID. '_';

                    $aqlQuery = new Apl_Query($prefix. Apollo_Tables::_APOLLO_EVENT_META, true);
                    $metaData = $aqlQuery->get_where("meta_key = '".Apollo_DB_Schema::_APOLLO_EVENT_DATA."'");

                    if ($metaData) {
                        $count = 0;
                        foreach( $metaData as $mt ) {
                            $venue = get_venue($mt->apollo_event_id);

                            $isPrivate = $venue->get_meta_data(Apollo_DB_Schema::_E_PRIVATE);

                            if(!$isPrivate) {
                                continue;
                            }

                            $count++;

                            update_apollo_meta($mt->apollo_event_id,Apollo_DB_Schema::_E_PRIVATE,$isPrivate);
                        }

                        $blogName = $site->domain;
                    }

                    if($count){
                        $successAll[] = "There are $count instance(s) are synchronized on the blog $blogName";
                    }
                    else {
                        $successAll[] = "<p>Nothing in: ". $blogName. '</p>';
                    }
                }
            }

            $this->setSuccessMessage(implode( ' <br/>', $successAll));


            wp_redirect($this->buildUrlForProcessingUtilityTool());
        }

        /**
         * @author: vulh
         * Update program_edu_id of program
         *
         */
        public function syncProgEduId(){
            global $wpdb;

            if (isset($_SESSION['successMessage'])) {
                unset($_SESSION['successMessage']);
            }

            unset($_SESSION['successMessage']);
            if(!empty($this->siteIDs)) {
                $errorAll = array();
                $successAll = array();

                foreach ($this->siteIDs as $siteID) {
                    $site = get_blog_details($siteID);

                    if (empty($site)) {
                        $errorAll[] = sprintf("SiteID %s is not existed!", $siteID);
                        continue;
                    }

                    $prefix = is_main_site($siteID) ?  $wpdb->base_prefix : $wpdb->base_prefix. $siteID. '_';

                    $aqlQuery = new Apl_Query($prefix. Apollo_Tables::_APL_PROGRAM_META, true);
                    $metaData = $aqlQuery->get_where("meta_key = '".Apollo_DB_Schema::_APL_PROGRAM_DATA."'");

                    if ($metaData) {
                        $count = 0;
                        foreach( $metaData as $mt ) {
                            $prog = get_program($mt->apollo_program_id);

                            $eduId = $prog->get_meta_data(Apollo_DB_Schema::_PROGRAM_EDU_ID);
                            $eduIdInMtData = $prog->get_meta_data(Apollo_DB_Schema::_PROGRAM_EDU_ID, Apollo_DB_Schema::_APL_PROGRAM_DATA);

                            if($eduId || !$eduIdInMtData) {
                                continue;
                            }

                            $count++;

                            update_apollo_meta($mt->apollo_program_id, Apollo_DB_Schema::_PROGRAM_EDU_ID, $eduIdInMtData);
                        }

                        $blogName = $site->domain;

                        if($count){
                            $successAll[] = "There are $count instance(s) are synchronized on the blog $blogName";
                        }
                        else {
                            $successAll[] = "<p>Nothing in: ". $blogName. '</p>';
                        }
                    }
                }
            }

            $this->setSuccessMessage(implode( ' <br/>', $successAll));


            wp_redirect($this->buildUrlForProcessingUtilityTool());
        }

	    /**
	     * @author: Truonghn
	     * clean waste data form event calendar table
	     */
	    public function cleanWasteDataEventCalendar(){
		    global $wpdb;

		    if (isset($_SESSION['successMessage'])) {
			    unset($_SESSION['successMessage']);
		    }

		    unset($_SESSION['successMessage']);
		    if(!empty($this->siteIDs)) {
			    $errorAll = array();
			    $successAll = array();

			    foreach ($this->siteIDs as $siteID) {
				    $site = get_blog_details($siteID);

				    if (empty($site)) {
					    $errorAll[] = sprintf("SiteID %s is not existed!", $siteID);
					    continue;
				    }

				    $prefix = is_main_site($siteID) ?  $wpdb->base_prefix : $wpdb->base_prefix. $siteID. '_';
				    $aqlQuery = new Apl_Query($prefix. Apollo_Tables::_APL_EVENT_CALENDAR, true);
				    $join = " INNER JOIN ".$prefix. Apollo_Tables::_APOLLO_EVENT_META." est ON t.event_id = est.apollo_event_id AND est.meta_key = '" . Apollo_DB_Schema::_APOLLO_EVENT_START_DATE . "'
                              INNER JOIN ".$prefix. Apollo_Tables::_APOLLO_EVENT_META." eed ON t.event_id = eed.apollo_event_id AND eed.meta_key =  '" . Apollo_DB_Schema::_APOLLO_EVENT_END_DATE . "' ";
				    $where = " t.date_event NOT BETWEEN est.meta_value AND eed.meta_value ";

					$row = $aqlQuery->deleteJoinedTable( $join, $where, true);

				    $blogName = $site->domain;

				    if($row){
					    $successAll[] = "There are $row instance(s) are deleted on the blog $blogName";
				    }
				    else {
					    $successAll[] = "<p>Nothing in: ". $blogName. '</p>';
				    }
			    }
		    }

		    $this->setSuccessMessage(implode( ' <br/>', $successAll));

		    wp_redirect($this->buildUrlForProcessingUtilityTool());
	    }

	    protected function renderConfirmationcleanWasteDataEventCalendar(){
		    echo Apollo_App::getTemplatePartCustom(APOLLO_INCLUDES_DIR . '/scripts/html/utility-tool-confirmation.php',array(
			    'confirmationMessage' => __("This script is used for cleaning waste  data in table event calendar.","apollo"),
			    'proceedUrl' => $this->buildUrlForProcessingUtilityTool(),
			    'aplNonceField' => wp_nonce_field($this->action,self::_APL_TOOL_NONCE_FIELD,false,false),
			    'errorMessage' => $this->getErrorMessage(),
			    'successMessage' => $this->getSuccessMessage(),
			    'isLoggedIn' => $this->isLoggedIn
		    ));
	    }

        /**
         * Flush rewrite rules for all sites
         *
         * @ticket #11129
         * @return void
         */
        protected function flushRewriteRulesForAllSites()
        {
            try {
                if ( !empty($this->siteIDs) ) {
                    $errorAll   = array();
                    $successAll = array();

                    // loop each site
                    foreach ($this->siteIDs as $siteID) {
                        $site = get_blog_details($siteID);
                        if ( empty($site) ) {
                            $errorAll[] = sprintf('SiteID %s is not existed!', $siteID);
                            continue;
                        }

                        // init variables
                        $errors   = array();
                        $site     = get_object_vars($site);
                        $blogName = empty($site['blogname']) ? $site['domain'] : $site['blogname'];

                        // flush rewrite rules for current blog
                        switch_to_blog( $siteID );
                        flush_rewrite_rules();

                        // format error/success information
                        if ( !empty($errors) ) {
                            $errorAll[] = sprintf('%d - %s : %s', $site['blog_id'], $blogName, implode('<br/>', $errors));
                        } else {
                            $successAll[] = sprintf('Site ID: %d - %s was flushed rewrite rules!', $site['blog_id'], $blogName);
                        }
                    }

                    // display error/success information after flush rewrite rules
                    $this->setErrorMessage($errorAll);
                    $this->setSuccessMessage($successAll);
                    switch_to_blog( 1 );
                    wp_redirect($this->buildUrlForProcessingUtilityTool());
                }
            } catch (Exception $ex) {
                self::log($ex->getMessage(), __CLASS__ . ':flushRewriteRulesForAllSites');
                $this->showErrorMessage('');
            }
        }

        /**
         * Set offset to get segment sites
         * @author vulh
         * @param bool $segment
         * @return mixed
         */
        private function setOffsetSiteIDs($segment = false)
        {
            if ($this->isRunSegmentSiteIDs()) {

                $this->offset = empty($_GET['offset']) ? 0 : $_GET['offset'];

                if ($_GET['site_ids'] && $_GET['site_ids'] != 'ALL') {
                    $site_ids = explode(',', $_GET['site_ids']);
                }
                else {

                    if ($segment == -1) {
                        $this->segment = 100000;
                    }

                    $site_ids = get_sites( array(
                        'spam'       => 0,
                        'deleted'    => 0,
                        'number'     => $this->segment,
                        'offset'     => $this->offset,
                        'fields'     => 'ids',
                        'order'      => 'DESC',
                        'orderby'    => 'id',
                    ) );
                }

                $this->siteIDs = $site_ids;
                if (empty($site_ids)) {
                    echo '<p>Done</p>';exit;
                }
            }
        }

        /**
         * Auto call to the next segments site
         * @author @vulh
         * @param $successAll
         * @return mixed
         */
        private function callToSegmentSiteIDs($successAll = false)
        {
            if ($successAll) {
                foreach($successAll as $s) {
                    echo '<p>' . $s . '</p>';
                }
            }

            $this->setSuccessMessage(implode( ' <br/>', $successAll));

            switch_to_blog(1);
            $this->offset = $this->offset + $this->segment;
            ?>
            <script type="text/javascript">
                function nextpage() {
                    location.href = "<?php echo network_site_url(). '?action='. $_GET['action']. '&site_ids=ALL&force&offset='.$this->offset.'' ?>";
                }
                setTimeout( "nextpage()", 1000 );
            </script>
            <?php
            exit;
        }

        /**
         * Check is segment site IDs
         * @author vulh
         * @return boolean
         */
        private function isRunSegmentSiteIDs()
        {
            return isset($_GET['site_ids']) && $_GET['site_ids'] ==  'ALL';
        }

        /**
         * Clone state to single field
         *
         * @author: vulh
         * @param $data
         * @return mixed
         */
        public function cloneMetaDataToSingleField($data){
            global $wpdb;

            $metaTbl = $data['meta_table'];
            $childMetaKey = $data['child_meta_key'];
            $parentMetaKey = $data['parent_meta_key'];

            // Set specific store field to
            $storeToField = !empty($data['store_to_field']) ? $data['store_to_field'] : '';
            if (!$storeToField) {
                $storeToField = $childMetaKey;
            }

            // Find in another child field e.g. classified has other city
            $otherChildMetaKey = !empty($data['other_child_meta_key']) ? $data['other_child_meta_key'] : '';

            $metaPostIDName = $data['post_id_key'];

            if (isset($_SESSION['successMessage'])) {
                unset($_SESSION['successMessage']);
            }

            unset($_SESSION['successMessage']);

            $this->setOffsetSiteIDs();

            if(!empty($this->siteIDs)) {

                $errorAll = array();
                $successAll = array();

                foreach ($this->siteIDs as $siteID) {
                    switch_to_blog($siteID);

                    $site = get_blog_details($siteID);

                    if (empty($site)) {
                        $errorAll[] = sprintf("SiteID %s is not existed!", $siteID);
                        continue;
                    }

                    $prefix = is_main_site($siteID) ?  $wpdb->base_prefix : $wpdb->base_prefix. $siteID. '_';

                    $aqlQuery = new Apl_Query($prefix. $metaTbl, true);
                    $metaData = $aqlQuery->get_where("meta_key = '".$parentMetaKey."'");

                    if ($metaData) {
                        $count = 0;
                        foreach( $metaData as $mt ) {

                            $parentMetaValue = Apollo_App::unserialize($mt->meta_value);

                            $postID = $mt->{$metaPostIDName};

                            $val = !empty($parentMetaValue[$childMetaKey]) ? $parentMetaValue[$childMetaKey] : '';
                            if (!$val && $otherChildMetaKey) {
                                $val = !empty($parentMetaValue[$otherChildMetaKey]) ? $parentMetaValue[$otherChildMetaKey] : '';
                            }

                            if($val) {
                                update_apollo_meta($postID, $storeToField, $val);
                            } else {
                                continue;
                            }

                            $count++;
                        }

                        $blogName = $site->domain;
                    }

                    if($count){
                        $successAll[] = "There are $count instance(s) are synchronized on the blog $blogName";
                    }
                    else {
                        $successAll[] = "<p>Nothing in: ". $blogName. '</p>';
                    }
                }
            }

            $this->setSuccessMessage(implode( ' <br/>', $successAll));

            switch_to_blog( 1 );

            if ($this->isRunSegmentSiteIDs()) {
                $this->callToSegmentSiteIDs($successAll);
            }
            else {
                wp_redirect($this->buildUrlForProcessingUtilityTool());
            }
        }

        /**
         * Update default content
         *
         * @return mixed
         */
        public function pageUpdateDefaultContent(){
            global $wpdb;

            if (isset($_SESSION['successMessage'])) {
                unset($_SESSION['successMessage']);
            }

            unset($_SESSION['successMessage']);

            $this->setOffsetSiteIDs();

            if(!empty($this->siteIDs)) {

                $errorAll = array();
                $successAll = array();

                foreach ($this->siteIDs as $siteID) {
                    switch_to_blog($siteID);

                    $site = get_blog_details($siteID);

                    if (empty($site)) {
                        $errorAll[] = sprintf("SiteID %s is not existed!", $siteID);
                        continue;
                    }
                    $tableName = $wpdb->prefix. 'posts';
                    $sqlUpdate = "UPDATE {$tableName} p 
                                INNER JOIN (
                                    SELECT ID FROM 
                                    (
                                        SELECT p1.ID FROM {$tableName} p1 
                                        WHERE p1.post_content LIKE '%comming soon%' or p1.post_content LIKE '%Comming soon%'
                                    ) tbl
                                ) t ON p.ID = t.ID 
                                SET p.post_content = 'Page not available.'";

                    $wpdb->query($sqlUpdate);
                    $count = $wpdb->rows_affected;
                    $blogName = $site->domain;
                    if($count){
                        $successAll[] = "There are $count instance(s) are synchronized on the blog $blogName";
                    }
                    else {
                        $successAll[] = "<p>Nothing in: ". $blogName. '</p>';
                    }
                }
            }

            $this->setSuccessMessage(implode( ' <br/>', $successAll));

            switch_to_blog( 1 );

            if ($this->isRunSegmentSiteIDs()) {
                $this->callToSegmentSiteIDs($successAll);
            }
            else {
                wp_redirect($this->buildUrlForProcessingUtilityTool());
            }
        }


        /**
         * Clone event artist meta to new table
         *
         * @author: vulh
         * @return mixed
         */
        public function cloneEventArtistMetaToNewTable(){
            global $wpdb;


            if (isset($_SESSION['successMessage'])) {
                unset($_SESSION['successMessage']);
            }

            unset($_SESSION['successMessage']);

            $this->setOffsetSiteIDs();

            if(!empty($this->siteIDs)) {

                $errorAll = array();
                $successAll = array();

                foreach ($this->siteIDs as $siteID) {

                    $site = get_blog_details($siteID);

                    if (empty($site)) {
                        $errorAll[] = sprintf("SiteID %s is not existed!", $siteID);
                        continue;
                    }

                    $prefix = is_main_site($siteID) ?  $wpdb->base_prefix : $wpdb->base_prefix. $siteID. '_';

                    $aqlQuery = new Apl_Query($prefix. Apollo_Tables::_APOLLO_EVENT_META, true);
                    $metaData = $aqlQuery->get_where("meta_key = '_apollo_event_artist'");

                    $targetQuery = new Apl_Query($prefix. Apollo_Tables::_APL_ARTIST_EVENT, true);

                    if ($metaData) {
                        $count = 0;
                        foreach( $metaData as $mt ) {

                            $artistID = intval($mt->meta_value);
                            $eventID = $mt->apollo_event_id;

                            if (!$artistID || !$eventID) {
                                continue;
                            }

                            $total = $targetQuery->get_total("artist_id = $artistID AND event_id = $eventID");

                            if (!$total) {
                                $targetQuery->insert(array(
                                    'artist_id' => $artistID,
                                    'event_id' => $eventID,
                                ));
                                $count++;
                            }
                        }

                        $blogName = $site->domain;
                    }

                    if($count){
                        $successAll[] = "There are $count instance(s) are synchronized on the blog $blogName";
                    }
                    else {
                        $successAll[] = "<p>Nothing in: ". $blogName. '</p>';
                    }
                }
            }

            $this->setSuccessMessage(implode( ' <br/>', $successAll));

            if ($this->isRunSegmentSiteIDs()) {
                $this->callToSegmentSiteIDs($successAll);
            }
            else {
                wp_redirect($this->buildUrlForProcessingUtilityTool());
            }
        }

        /**
         * Clone user event artist to new table
         *
         * @author: vulh
         * @return mixed
         */
        public function createTables(){
            global $wpdb;


            if (isset($_SESSION['successMessage'])) {
                unset($_SESSION['successMessage']);
            }

            unset($_SESSION['successMessage']);

            $this->setOffsetSiteIDs();

            if(!empty($this->siteIDs)) {

                $errorAll = array();
                $successAll = array();
                require_once APOLLO_ADMIN_DIR. '/class-apollo-install-theme.php';
                $installIns = new Apollo_Install_Theme();
                foreach ($this->siteIDs as $siteID) {
                    switch_to_blog($siteID);
                    $site = get_blog_details($siteID);

                    if (empty($site)) {
                        $errorAll[] = sprintf("SiteID %s is not existed!", $siteID);
                        continue;
                    }

                    $installIns->create_tables();
                }
            }

            switch_to_blog(1);

            if ($this->isRunSegmentSiteIDs()) {
                $this->callToSegmentSiteIDs($successAll);
            }
            else {
                wp_redirect($this->buildUrlForProcessingUtilityTool());
            }
        }

        public function cloneSyndicationArtOrg(){

            if (isset($_SESSION['successMessage'])) {
                unset($_SESSION['successMessage']);
            }

            unset($_SESSION['successMessage']);

            $this->setOffsetSiteIDs();
            $successAll = array();
            if(!empty($this->siteIDs)) {
                $errorAll = array();
                foreach ($this->siteIDs as $siteID) {
                    switch_to_blog($siteID);
                    $site = get_blog_details($siteID);

                    if (empty($site)) {
                        $errorAll[] = sprintf("SiteID %s is not existed!", $siteID);
                        continue;
                    }

                    /** Update syndication org*/
                    $orgOrderBy = get_option(Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_ORDERBY, '');
                    if (!empty($orgOrderBy)) {
                        /** Get info from Options table */
                        $orgAll = get_option(Apollo_DB_Schema::_SYNDICATION_ALL_ORGANIZATION, 0);
                        $orgSelectedItems = get_option(Apollo_DB_Schema::_SYNDICATION_SELECTED_ORGANIZATION, '');
                        $orgLimit = get_option(Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_LIMIT, 1000);
                        $orgOrder = get_option(Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_ORDER, 'asc');
                        /** Insert to posts table */
                        wp_insert_post(array(
                            'post_title' => 'Organization',
                            'post_status' => 'publish',
                            'post_type' => Apollo_DB_Schema::_SYNDICATION_ORG_PT,
                            'meta_input' => array(
                                APL_Syndication_Const::_SO_META_ORG_ALL => $orgAll ? '0' : '1',
                                APL_Syndication_Const::_SO_META_ORG_SELECTED => $orgSelectedItems,
                                APL_Syndication_Const::_SO_META_ORG_LIMIT => $orgLimit,
                                APL_Syndication_Const::_SO_META_ORG_ORDER_BY => $orgOrderBy,
                                APL_Syndication_Const::_SO_META_ORG_ORDER => $orgOrder
                            )
                        ));
                    }

                    /** Update syndication artist*/
                    $artOrderBy = get_option(APL_Syndication_Const::_SYNDICATION_ARTIST_ORDERBY, '');
                    if (!empty($artOrderBy)) {
                        /** Get info from Options table */
                        $artAll = get_option(APL_Syndication_Const::_SYNDICATION_ALL_ARTIST, 0);
                        $artSelectedItems = get_option(APL_Syndication_Const::_SYNDICATION_SELECTED_ARTIST, '');
                        $artLimit = get_option(APL_Syndication_Const::_SYNDICATION_ARTIST_LIMIT, 1000);
                        $artOrder = get_option(APL_Syndication_Const::_SYNDICATION_ARTIST_ORDER, 'asc');
                        $artCache = get_option(APL_Syndication_Const::_SYNDICATION_ARTIST_ENABLE_CACHE, 0);
                        /** Insert to posts table */
                        wp_insert_post(array(
                            'post_title' => 'Artist',
                            'post_status' => 'publish',
                            'post_type' => Apollo_DB_Schema::_SYNDICATION_ARTIST_PT,
                            'meta_input' => array(
                                APL_Syndication_Const::_SA_META_ARTIST_ALL => $artAll ? '0' : '1',
                                APL_Syndication_Const::_SA_META_ARTIST_SELECTED => $artSelectedItems,
                                APL_Syndication_Const::_SA_META_ARTIST_LIMIT => $artLimit,
                                APL_Syndication_Const::_SA_META_ARTIST_ORDER_BY => $artOrderBy,
                                APL_Syndication_Const::_SA_META_ARTIST_ORDER => $artOrder,
                                APL_Syndication_Const::_SA_META_ARTIST_ENABLE_CACHE => $artCache
                            )
                        ));
                    }
                }
            }

            switch_to_blog(1);

            if ($this->isRunSegmentSiteIDs()) {
                $this->callToSegmentSiteIDs($successAll);
            }
            else {
                wp_redirect($this->buildUrlForProcessingUtilityTool());
            }
        }

        /**
         * Clone events to a view for exporting
        */
        public function eventCloneToViewForExporting()
        {
            global $wpdb;

            if (isset($_SESSION['successMessage'])) {
                unset($_SESSION['successMessage']);
            }

            unset($_SESSION['successMessage']);

            if (isset($_GET['has_more']) && $_GET['has_more'] == 0) {
                exit("Done");
            }

            if (!isset($_GET['site_id']) || !$siteID = $_GET['site_id']) {
                exit("Missing site ID");
            }

            if (!isset($_GET['start_date']) || !$startDate = $_GET['start_date']) {
                exit("Required start date (2017-10-25)");
            }

            if (!isset($_GET['end_date']) || !$endDate = $_GET['end_date']) {
                exit("Required end date (2017-10-30)");
            }


            $prefix = $siteID == 1 ? $wpdb->prefix : $wpdb->prefix.$siteID.'_';
            $eventMetaTbl = $prefix. Apollo_Tables::_APOLLO_EVENT_META;
            $eventMetaKey = Apollo_DB_Schema::_APOLLO_EVENT_DATA;
            $eventOrgTbl = $prefix. Apollo_Tables::_APL_EVENT_ORG;
            $viewTbl = $prefix. Apollo_Tables::_APL_EVENT_VIEW_FOR_EXPORTING;
            $postTbl = $prefix. 'posts';

            $limit = 50;
            $this->page = isset($_GET['cp']) ? $_GET['cp'] : 1;
            $offset = ($this->page - 1) * $limit;

            if ($this->page == 1) {
                $wpdb->get_results("SET SQL_SAFE_UPDATES = 0");
                $wpdb->get_results("DELETE FROM $viewTbl WHERE 1=1");
            }

            $sql = "
                SELECT SQL_CALC_FOUND_ROWS p.ID as event_id, p.post_title as event_title,
                 p_org.post_title AS org_name, 
                 emt_start.meta_value as start_date, 
                 emt_end.meta_value as end_date, 
                 p_venue.post_title as venue_name,
                 p.post_author,
                 emt_data.meta_value as meta_data,
                 p.post_date as post_date
                
                FROM $postTbl p
                
                INNER JOIN $eventMetaTbl emt_start ON emt_start.apollo_event_id = p.ID AND emt_start.meta_key = '_apollo_event_start_date'
                INNER JOIN $eventMetaTbl emt_end ON emt_end.apollo_event_id = p.ID AND emt_end.meta_key = '_apollo_event_end_date'
                INNER JOIN $eventMetaTbl emt_data ON emt_data.apollo_event_id = p.ID AND emt_data.meta_key = '$eventMetaKey'
                
                LEFT JOIN $eventOrgTbl e_org ON e_org.post_id = p.ID AND e_org.is_main = 'yes'
                LEFT JOIN $postTbl p_org ON p_org.ID = e_org.org_id AND p_org.post_type = 'organization'
                
                LEFT JOIN $eventMetaTbl emt_venue ON emt_venue.apollo_event_id = p.ID AND emt_venue.meta_key = '_apollo_event_venue'
                LEFT JOIN $postTbl p_venue ON p_venue.ID = emt_venue.meta_value AND p_venue.post_type = 'venue'
                
                WHERE p.post_type = 'event' 
                      AND p.post_status = 'publish'
                      AND p.post_date >= '$startDate' AND p.post_date <= '$endDate'
                      
                ORDER BY p.post_date ASC
               
                LIMIT $limit
                OFFSET $offset
            ";

            $events = $wpdb->get_results($sql);

            $total = $wpdb->get_var("SELECT FOUND_ROWS()");
            $hasMore = ($this->page * $limit) < $total;

            if ($events) {
                $aplQuery = new Apl_Query($viewTbl, true);
                foreach($events as $event) {

                    $metaData = Apollo_App::unserialize($event->meta_data);
                    $insertData = array(
                        'event_id'  => intval($event->event_id),
                        'event_title'    => $event->event_title,
                        'org_name'    => $event->org_name,
                        'start_date'    => $event->start_date,
                        'end_date'    => $event->end_date,
                        'venue_name'    => $event->venue_name,
                        'post_author'    => $event->post_author,
                        'contact_name'  => isset($metaData['_contact_name']) ? $metaData['_contact_name'] : '',
                        'contact_email'  => isset($metaData['_contact_email']) ? $metaData['_contact_email'] : '',
                        'date_posted'   => $event->post_date,
                    );



                    $aplQuery->insert($insertData);
                }
            }

            if ($hasMore) {
                $insertedEvents = $this->page * $limit;
                $successAll = array(
                    sprintf("Processing page %s. <b>Exported %s of %s events</b>", $this->page, $insertedEvents, $total)
                );
            }
            else {
                exit(sprintf("<p style='text-align: center; margin: 10px'>%s event(s) are found. Please use the bellow query:<br/> '<b>SELECT * FROM %s</b>' <br/>to execute and export all the data</p>", $total, $viewTbl));
            }

            if ($successAll) {
                foreach($successAll as $s) {
                    echo '<p style="text-align: center; margin: auto">' . $s . '</p>';
                }
            }

            $this->setSuccessMessage(implode( ' <br/>', $successAll));
            $this->page += 1;
            $url = network_site_url(). '/script/?action='.$_GET['action'].'&cp='.$this->page.'&private_site=1&has_more='.$hasMore.'&start_date='.$startDate.'&end_date='.$endDate.'&site_id='.$siteID;

            ?>
            <script type="text/javascript">
                function nextpage() {
                    location.href = "<?php echo $url ?>";
                }
                setTimeout( "nextpage()", 1000 );
            </script>
            <?php
            exit;


        }



        /**
         * Display confirmation form for migrate 'apollo_blog_user' table data to the 'apollo_user_modules' table
         *
         * @ticket #15312 - [CF] 20180314 - Optimize code
         */
        protected function renderConfirmationFormMigrateBlogUserTableDataToUserModulesTable()
        {
            echo Apollo_App::getTemplatePartCustom(APOLLO_INCLUDES_DIR . '/scripts/html/utility-tool-confirmation.php',array(
                'confirmationMessage' => __("This script is used to migrate data from the blog user table to the user modules table for all sites.", "apollo"),
                'proceedUrl'          => $this->buildUrlForProcessingUtilityTool(),
                'aplNonceField'       => wp_nonce_field($this->action, self::_APL_TOOL_NONCE_FIELD, false, false),
                'errorMessage'        => $this->getErrorMessage(),
                'successMessage'      => $this->getSuccessMessage(),
                'isLoggedIn'          => $this->isLoggedIn,
            ));
        }

        /**
         * Migrate old data from the 'apollo_blog_user' table to the new 'apollo_user_modules' table for all sites
         *
         * @ticket #15312 - [CF] 20180314 - Optimize code
         */
        public function migrateBlogUserTableDataToUserModulesTable()
        {
            global $wpdb;

            unset($_SESSION['successMessage']);
            $this->setOffsetSiteIDs();
            $errorAll   = array();
            $successAll = array();
            $aplQuery   = new Apl_Query(Apollo_App::getBlogUserTable());

            if (!empty($this->siteIDs)) {
                foreach ($this->siteIDs as $siteID) {
                    if (intval($siteID) <= 0) {
                        $errorAll[] = sprintf('Could not migrate data for site (ID = %d). Invalid site Id.', $siteID);
                        continue;
                    }

                    $site = get_blog_details($siteID);
                    if (empty($site)) {
                        $errorAll[] = sprintf("Site (ID = %d) is not existed!", $siteID);
                        continue;
                    }

                    $name = empty($site->blogname) ? $site->domain : $site->blogname;
                    $rows = $aplQuery->get_where("blog_id = $siteID");
                    if (!$rows) {
                        $errorAll[] = sprintf('Could not migrate data for site %s (ID = %d). Current site does not have any blog user data.', $name, $siteID);
                        continue;
                    }

                    switch_to_blog($siteID);
                    $aplUserModulesQuery = new Apl_Query($wpdb->prefix . Apollo_Tables::_APL_USER_MODULES);
                    try {
                        foreach ($rows as $row) {
                            if (!empty($row->artist_id)) {
                                $artistExisted = $aplUserModulesQuery->get_total("user_id = $row->user_id AND post_id = $row->artist_id AND post_type = '" . Apollo_DB_Schema::_ARTIST_PT . "'");
                                if (!$artistExisted) {
                                    $aplUserModulesQuery->insert(array(
                                        'user_id'   => $row->user_id,
                                        'post_id'   => $row->artist_id,
                                        'post_type' => Apollo_DB_Schema::_ARTIST_PT,
                                    ));
                                }
                            }

                            if (!empty($row->org_id)) {
                                $orgExisted = $aplUserModulesQuery->get_total("user_id = $row->user_id AND post_id = $row->org_id    AND post_type = '" . Apollo_DB_Schema::_ORGANIZATION_PT . "'");
                                if (!$orgExisted) {
                                    $aplUserModulesQuery->insert(array(
                                        'user_id'   => $row->user_id,
                                        'post_id'   => $row->org_id,
                                        'post_type' => Apollo_DB_Schema::_ORGANIZATION_PT,
                                    ));
                                }
                            }

                            if (!empty($row->venue_id)) {
                                $venueExisted  = $aplUserModulesQuery->get_total("user_id = $row->user_id AND post_id = $row->venue_id  AND post_type = '" . Apollo_DB_Schema::_VENUE_PT . "'");
                                if (!$venueExisted) {
                                    $aplUserModulesQuery->insert(array(
                                        'user_id'   => $row->user_id,
                                        'post_id'   => $row->venue_id,
                                        'post_type' => Apollo_DB_Schema::_VENUE_PT,
                                    ));
                                }
                            }

                            if (!empty($row->agency_id)) {
                                $agencyExisted = $aplUserModulesQuery->get_total("user_id = $row->user_id AND post_id = $row->agency_id AND post_type = '" . Apollo_DB_Schema::_AGENCY_PT . "'");
                                if (!$agencyExisted) {
                                    $aplUserModulesQuery->insert(array(
                                        'user_id'   => $row->user_id,
                                        'post_id'   => $row->agency_id,
                                        'post_type' => Apollo_DB_Schema::_AGENCY_PT,
                                    ));
                                }
                            }

                            if (!empty($row->educator_id)) {
                                $educatorExisted = $aplUserModulesQuery->get_total("user_id = $row->user_id AND post_id = $row->educator_id AND post_type = '" . Apollo_DB_Schema::_EDUCATOR_PT . "'");
                                if (!$educatorExisted) {
                                    $aplUserModulesQuery->insert(array(
                                        'user_id'   => $row->user_id,
                                        'post_id'   => $row->educator_id,
                                        'post_type' => Apollo_DB_Schema::_EDUCATOR_PT,
                                    ));
                                }
                            }
                        }

                        $successAll[] = sprintf('%s (Id = %d) was migrated data.', $name, $siteID);
                    } catch (Exception $ex) {
                        $errorAll[] = sprintf('Could not migrate data for site %s (ID = %d). Exception: ' . $ex->getMessage(), $name, $siteID);
                        continue;
                    }

                    $count = 0;
                    $blogName = $site->domain;
                    if ($count) {
                        $successAll[] = "There are $count instance(s) are synchronized on the blog $blogName";
                    } else {
                        $successAll[] = "<p>Nothing in: ". $blogName. '</p>';
                    }
                }
            }

            $this->setSuccessMessage(implode( ' <br/>', $successAll));

            switch_to_blog( 1 );

            if ($this->isRunSegmentSiteIDs()) {
                $this->callToSegmentSiteIDs($successAll);
            } else {
                wp_redirect($this->buildUrlForProcessingUtilityTool());
            }
        }


        /**
         * Migrate old data from the 'apollo_blog_user' table to the new 'apollo_user_modules' table for all sites
         *
         * @ticket #15312 - [CF] 20180314 - Optimize code
         */
        public function checkingMigrateBlogUserTableDataToUserModulesTable()
        {
            global $wpdb;

            unset($_SESSION['successMessage']);
            $this->setOffsetSiteIDs(-1);
            $errorAll   = array();
            $successAll = array();

            if (!empty($this->siteIDs)) {
                foreach ($this->siteIDs as $siteID) {

                    if (intval($siteID) <= 0) {
                        $errorAll[] = sprintf('Could not migrate data for site (ID = %d). Invalid site Id.', $siteID);
                        continue;
                    }

                    $site = get_blog_details($siteID);
                    if (empty($site)) {
                        $errorAll[] = sprintf("Site (ID = %d) is not existed!", $siteID);
                        continue;
                    }

                    $sql = "
                        select count(*) as total from wp_apollo_blog_user where blog_id = $siteID
                        AND artist_id <> 0 and artist_id is not null
                    ";
                    $n1 = $wpdb->get_results($sql);
                    $n1 = intval($n1[0]->total);

                    $sql = "
                        select count(*) as total from wp_apollo_blog_user where blog_id = $siteID
                        AND educator_id <> 0 and educator_id is not null
                    ";
                    $n2 = $wpdb->get_results($sql);
                    $n2 = intval($n2[0]->total);

                    $sql = "
                        select count(*) as total from wp_apollo_blog_user where blog_id = $siteID
                    AND venue_id <> 0 and venue_id is not null
                    ";
                    $n3 = $wpdb->get_results($sql);
                    $n3 = intval($n3[0]->total);

                    $sql = "
                        select count(*) as total from wp_apollo_blog_user where blog_id = $siteID
                    AND org_id <> 0 and org_id is not null
                    ";
                    $n4 = $wpdb->get_results($sql);
                    $n4 = intval($n4[0]->total);


                    $sql = "
                        select count(*) as total from wp_apollo_blog_user where blog_id = $siteID
                    AND agency_id <> 0 and agency_id is not null
                    ";
                    $n5 = $wpdb->get_results($sql);
                    $n5 = intval($n5[0]->total);

                    $prefix = $siteID == 1 ? "wp_":"wp_". $siteID."_";
                    $MMTbl = $prefix. Apollo_Tables::_APL_USER_MODULES;
                    $sql = "
                    select count(*) as total from $MMTbl
                    ";
                    $n = $wpdb->get_results($sql);
                    $n = intval($n[0]->total);

                    $blogName = $site->domain;
                    $total = $n1+$n2+$n3+$n4+$n5;

                    if ($n == $total) {
                        $successAll[] = "OK! $n rows ON $blogName";
                    } else {
                        $successAll[] = "<p>Different: $n, $total ";
                    }
                }
            }

            $this->setSuccessMessage(implode( ' <br/>', $successAll));

        }

    }

    new Apollo_Utility_Tool();
}
