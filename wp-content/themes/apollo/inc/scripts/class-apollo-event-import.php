<?php

if ( !defined('ABSPATH') ) {exit;}

apl_require_once(APOLLO_INCLUDES_DIR. '/Lib/Helpers/XML.php');

class Apollo_Event_Import {
    
    protected $event_id;
    protected $events = array();
    protected $xmlFilename = '';
    protected $isEdit = false;


    public function __construct($events = array(), $filename = false) {
        $this->events = $events;
        if ($filename) {
            $this->xmlFilename = $filename;
            $this->setIsEdit();
        }
    }
    
    public function saveImport() {

        if ( ! $this->events && $this->events != 'false' ) return false;
        $status = Apollo_App::bypassEventPendingApproveLoggedinUser() ? 'publish' : 'pending';

        foreach( $this->events as $event ) {
            $event = Apollo_App::clean_array_data($event);
            if (!$this->validate($event)) continue;

            // Insert post
            $post = array(
                'ID'            => '',
                'post_title'    => Apollo_App::clean_data_request($event['eventname']),
                'post_status'   => $status,
                'post_content'  => Apollo_App::clean_data_request($event['description']),
                'post_excerpt'  => '',
                'post_author'   => get_current_user_id(),
                'post_type'     => Apollo_DB_Schema::_EVENT_PT,
            );
            $this->event_id = wp_insert_post($post);
            
            if ( !$this->event_id ) continue;
            
            // Insert term
            $this->_addTerms(!empty($event['event-category']) ? $event['event-category'] : '', 
                !empty($event['event-sub-category']) ? $event['event-sub-category'] : '');
            
            // Insert Org
            $this->_addOrg(!empty($event['orgname']) ? $event['orgname'] : '');
            
            // Insert venue
            $this->_addVenue(!empty($event['venuename']) ? $event['venuename'] : '');
            
            // Insert meta data
            $this->_updateMetaData($event);

            $this->_uploadImage(!empty($event['image']) ? $event['image'] : '');
            
            // Create event time
            $this->_createEventTimes($event);
            
        }
        
        if ($this->getIsEdit()) {
            $this->_updateImported();
        }
        
        return TRUE;
    }
    
    public function validate($event) {
        
        if ( empty($event['eventname'])
            || empty($event['orgname'])
            || empty($event['description'])
            || empty($event['event-category'])
            || empty($event['start-date'])
            || empty($event['end-date'])
            || (!empty($event['start-date']) && !empty($event['end-date']) && strtotime($event['end-date']) < strtotime($event['start-date']))
            || empty($event['contactname'])
            || (!empty($event['event-url']) && !filter_var( $event['event-url'], FILTER_VALIDATE_URL )) 
            || (!empty($event['event-email']) && !is_email($event['event-email']) )
            || (!empty($event['contactemail']) && !is_email($event['contactemail']) )
            || (!empty($event['ticket-url']) && !filter_var( $event['ticket-url'], FILTER_VALIDATE_URL )) 
        ) {
            return false;
        }
        return true;
    }
    
    private function _updateImported() {
        $aplQuery = new Apl_Query(Apollo_Tables::_APL_EVENT_IMPORT);
        $aplQuery->update(array(
            'imported'  => 1
        ), array(
            'file_name' => $this->xmlFilename,
        ));
    }
    
    private function _addTerms($primary, $subs) {
        $primary = trim($primary);
        $subs = trim($subs);

        $primaryId = $this->getTermId($primary);

        $terms = array($primaryId);
        
        // Update primary event
        update_apollo_meta( $this->event_id, Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID, $primaryId);
        
        // Update subs
        if ($subs) {
            $subCategories = explode(',', $subs);

            foreach($subCategories as $sub){
                $subId = $this->getTermId($sub);
                $subCategoryIds[] = $subId;
            }

            if ( !empty($subCategoryIds) ) {
                $terms = array_merge($terms, $subCategoryIds);
            }
        }
        
        // Update taxonomies for event
        wp_set_post_terms( $this->event_id, $terms , 'event-type' );
    }

    /*@ticket: #16675:  [Event import tool] Import events by CSV file using 'IDs' or 'Title' for org,
     venue, primary and sub-category types.*/
    public function getTermId($keyWord){

        if ($term = get_term_by('id', $keyWord, 'event-type')) {
            return $term->term_id;
        }

        //get term by name
        if ($term = term_exists($keyWord, 'event-type')) {
            return $term["term_id"];
        }

        $term = wp_insert_term(
            $keyWord,   // the term
            'event-type', // the taxonomy
            array(
                'description' => '',
                'slug' => strtolower(str_replace(' ', '-', $keyWord)),
                'parent' => '',
            )
        );
        return ($term && isset($term["term_id"])) ? $term["term_id"] : '';

    }
    
    private function _addOrg($keyWord)
    {
        global $wpdb;

        /*@ticket: #16675:  [Event import tool] Import events by CSV file using 'IDs' or 'Title' for org,
        venue, primary and sub-category types.*/
        $keyWord = trim($keyWord);

        $prepareValues = array(
            $keyWord,
            $keyWord,
            Apollo_DB_Schema::_ORGANIZATION_PT
        );

        $org = $wpdb->get_results(
            $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE (ID = '%s' or post_title = '%s') 
                              and post_type = '%s' and post_status in ('pending', 'publish')",
                $prepareValues));

        if (empty($org)) {
            $orgId = wp_insert_post(array(
                'ID' => '',
                'post_title' => $keyWord,
                'post_status' => 'publish',
                'post_content' => '',
                'post_excerpt' => '',
                'post_author' => get_current_user_id(),
                'post_type' => Apollo_DB_Schema::_ORGANIZATION_PT,
            ));
        } else {
            $orgId = ($org && isset($org[0])) ? $org[0]->ID : '';
        }

        // Save org in event org table
        $apl_query = new Apl_Query(Apollo_Tables::_APL_EVENT_ORG);
        $org_field = Apollo_DB_Schema::_APL_E_ORG_ID;

        // Delete exist
        $apl_query->delete(" post_id = {$this->event_id}");

        // Client enter the tmp org so we don't need to save primary from the submitted data
        $apl_query->insert(array(
            'post_id' => $this->event_id,
            $org_field => $orgId,
            Apollo_DB_Schema::_APL_E_ORG_IS_MAIN => 'yes',
            Apollo_DB_Schema::_APL_E_ORG_ORDERING => 1,
        ));
    }
    
    private function _addVenue($keyWord) {

        global $wpdb;
        $keyWord = trim($keyWord);

        /*@ticket: #16675:  [Event import tool] Import events by CSV file using 'IDs' or 'Title' for org,
        venue, primary and sub-category types.*/
        $prepareValues = array(
            $keyWord,
            $keyWord,
            Apollo_DB_Schema::_VENUE_PT
        );

        $venue = $wpdb->get_results(
            $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE (ID = '%s' or post_title = '%s') 
                              and post_type = '%s' and post_status in ('pending', 'publish')",
                            $prepareValues));

        if (empty($venue)) {
            $venueId = wp_insert_post(array(
                'ID' => '',
                'post_title' => $keyWord,
                'post_status' => 'pending',
                'post_content' => '',
                'post_excerpt' => '',
                'post_author' => get_current_user_id(),
                'post_type' => Apollo_DB_Schema::_VENUE_PT,
            ));
        } else {
            $venueId = ($venue && isset($venue[0])) ? $venue[0]->ID : '';
        }

        update_apollo_meta( $this->event_id, Apollo_DB_Schema::_APOLLO_EVENT_VENUE, $venueId );
    }
    
    private function _updateMetaData($event) {

        $eventData = array(
            Apollo_DB_Schema::_WEBSITE_URL => !empty($event['event-url']) ? $event['event-url'] : '',
            Apollo_DB_Schema::_ADMISSION_DETAIL => !empty($event['ticket-info']) ? $event['ticket-info'] : '',
            Apollo_DB_Schema::_ADMISSION_PHONE => !empty($event['event-phone']) ? $event['event-phone'] : '',
            Apollo_DB_Schema::_ADMISSION_TICKET_URL => !empty($event['ticket-url']) ? $event['ticket-url'] : '',
            Apollo_DB_Schema::_ADMISSION_TICKET_EMAIL => !empty($event['event-email']) ? $event['event-email'] : '',
            Apollo_DB_Schema::_E_ACB_INFO => '',
            
            Apollo_DB_Schema::_E_CONTACT_NAME => !empty($event['contactname']) ? $event['contactname'] : '',
            Apollo_DB_Schema::_E_CONTACT_EMAIL => !empty($event['contactemail']) ? $event['contactemail'] : '',
            Apollo_DB_Schema::_E_CONTACT_PHONE => !empty($event['contactphone']) ? $event['contactphone'] : '',
        );

        if (!empty($event['start-date'])) {
            $startArr = explode('/', $event['start-date']);
            $newStart = count($startArr) == 3 ? date('Y-m-d', mktime(0,0,0, $startArr[0], $startArr[1], $startArr[2])) : '';
        }

        if (!empty($event['end-date'])) {
            $endArr = explode('/', $event['end-date']);
            $newEnd = count($endArr) == 3 ? date('Y-m-d', mktime(0,0,0, $endArr[0], $endArr[1], $endArr[2])) : '';
        }

        $_arr_meta = array(
            Apollo_DB_Schema::_APOLLO_EVENT_DATA    => maybe_serialize(Apollo_App::clean_array_data($eventData)),
            Apollo_DB_Schema::_APOLLO_EVENT_START_DATE => isset($newStart) ? $newStart : '',
            Apollo_DB_Schema::_APOLLO_EVENT_END_DATE => isset($newEnd) ? $newEnd : '',
            Apollo_DB_Schema::_APOLLO_EVENT_SOURCE => get_current_user_id(),
        );
        
        // Update meta data
        foreach( $_arr_meta as $key => $val ):
            update_apollo_meta( $this->event_id, $key, $val );
        endforeach;
        
    }

    private function _uploadImage($url) {

        require_once(ABSPATH . 'wp-admin/includes/media.php');
        require_once(ABSPATH . 'wp-admin/includes/file.php');
        require_once(ABSPATH . 'wp-admin/includes/image.php');
        $desc = "";

        // add the function above to catch the attachments creation
        add_action('add_attachment', array($this, 'new_attachment'));

        media_sideload_image($url, $this->event_id, $desc);

        // we have the Image now, and the function above will have fired too setting the thumbnail ID in the process, so lets remove the hook so we don't cause any more trouble
        remove_action('add_attachment', array($this, 'new_attachment'));

    }

    function new_attachment($att_id){
        // the post this was sideloaded into is the attachments parent!
        $p = get_post($att_id);
        update_post_meta($p->post_parent,'_thumbnail_id',$att_id);
    }
    
    function _createEventTimes($event) {
        
        $date1 = $event['start-date'];
        $date2 = $event['end-date'];
        
        if ( !$date1
            || !$date2
            || !Apollo_App::is_valid_date($date1, 'm/d/Y')
            || !Apollo_App::is_valid_date($date2, 'm/d/Y')
        ) {
            return;
        }
        
        $_apollo_day_of_week = array();
        for($i = 0; $i < 7; $i++) {
            if (isset($event['day'. $i]) && $event['day'. $i]) {
                $_apollo_day_of_week[] = $i;
            }
        }
        update_apollo_meta($this->event_id, Apollo_DB_Schema::_APOLLO_EVENT_DAYS_OF_WEEK, maybe_serialize($_apollo_day_of_week));
        
        
        if (in_array($date1, Apollo_Calendar::$arrWeeks)) {
            $dates = Apollo_Calendar::getStepsDateByDay($this->event_id, $date1, $date2);
        } else {
            $dates = Apollo_Calendar::getStepsDateByDate($this->event_id, $date1, $date2);
        }
        
        if ($dates) {
            foreach( $dates as $date ) {
                $day = date( 'w', strtotime($date));
                if (isset($event['day'. $day]) && $event['day'. $day]) {
                    $arrDays    = str_split($event['day'. $day]);
                    $dayValue   = $arrDays && count($arrDays) == 3
                        ? '0'.$arrDays[0].':'.$arrDays[1].$arrDays[2]
                        : $arrDays[0].$arrDays[1]. ':'. $arrDays[2]. $arrDays[3]; 
                    
                    Apollo_Calendar::createEvent($this->event_id, array($date), $dayValue, '');
                }
            }
        }
    }
    
    //function defination to convert array to xml
    function arrayToXml($data, &$xmlData = false, $rootNode = 'events') {
        
        if ( $xmlData === false ) {
            //creating object of SimpleXMLElement
            $xmlData = new ApolloSimpleXMLExtended("<?xml version=\"1.0\"?><$rootNode></$rootNode>");
        }
        
        foreach( $data as $key => $value) {
            if ( is_array($value) ) {
                if ( !is_numeric($key) ) {
                    $subnode = $xmlData->addChild("$key");
                    self::arrayToXml($value, $subnode);
                } else {
                    $subnode = $xmlData->addChild("event");
                    self::arrayToXml($value, $subnode);
                }
            } else {
                $xmlData->addChildWithCDATA( "$key", "$value" );
            }
        }
    }

    function getXmlFullPath() {
        
        if ( !$this->xmlFilename ) {
            $this->xmlFilename = time().'-spreadsheet-'. get_current_user_id(). '.xml';
        }

        if (file_exists( $this->xmlFilename ) && !is_writable( $this->xmlFilename )) {
            return false;
        }

        return  Apollo_App::getUploadBaseInfo( 'event_import_dir' ). '/'. $this->xmlFilename;
    }

    static function getTmpFullPath() {

        if(!session_id()) {
            session_start();
        }

        $fullTmpFile = sprintf('tmp-spreadsheet-%s-%s.json', get_current_user_id(), session_id());

        if (file_exists( $fullTmpFile ) && !is_writable( $fullTmpFile )) {
            return false;
        }

        return  Apollo_App::getUploadBaseInfo( 'event_import_dir' ). '/'. $fullTmpFile;
    }
    
    function getXmlUrl($filename = false) {
        if ($filename) {
            $this->xmlFilename = $filename;
        }
        return Apollo_App::getUploadBaseInfo( 'event_import_url' ). '/'. $this->xmlFilename;
    }

    function xmlToFile($xml) {
        if (!$file = $this->getXmlFullPath()) {
            return false;
        }
        //saving generated xml file
        return $xml->asXML($file);
    }

    function insertImportForLater() {
        $aplQuery = new Apl_Query(Apollo_Tables::_APL_EVENT_IMPORT);
        $aplQuery->insert(array(
            'file_name'   => $this->xmlFilename,
            'user_id'     => get_current_user_id(),
            'import_date' => date('Y-m-d'),
        ));
    }
    
    function updateImportForLater() {
        $aplQuery = new Apl_Query(Apollo_Tables::_APL_EVENT_IMPORT);
        $aplQuery->update(array(
            'file_name'   => $this->xmlFilename,
            'user_id'     => get_current_user_id(),
            'import_date' => date('Y-m-d'),
        ), array('file_name' => $this->xmlFilename));
    }


    private static function cleanEmptyObjectForTableCell($eventRow = array()){
        if(empty($eventRow)) return $eventRow;
        foreach($eventRow as $cellKey => $cellVal){
            $cellValObjectVar = is_object($cellVal) ? get_object_vars($cellVal) : $cellVal;
            if(empty($cellValObjectVar)){
                $eventRow->$cellKey = "";
            }
        }
        return $eventRow;
    }

    function xmlToArray() {
        try {
            $xml = (array) simplexml_load_file($this->getXmlUrl($this->xmlFilename), 'SimpleXMLElement', LIBXML_NOCDATA);
            $data = json_decode(json_encode($xml));
            
            if ($data && isset($data->event) && $data->event) {
                
                if ( count($data->event) == 1 ) {
                    return array((array) self::cleanEmptyObjectForTableCell($data->event));
                }
                
                foreach ($data->event as $i => $d) {
                    $data->event[$i] = (array) self::cleanEmptyObjectForTableCell($d);
                }
            }
            return isset($data->event) ? $data->event : array();
        } catch (Exception $ex) {
            return array();
        }
    }
    
    function getAllImports() {
        $aplQuery = new Apl_Query(Apollo_Tables::_APL_EVENT_IMPORT);
        return $aplQuery->get_where("user_id=".  get_current_user_id()."", '*', '', ' ORDER BY imported ASC ');
    }
    
    function setIsEdit() {
        $aplQuery = new Apl_Query(Apollo_Tables::_APL_EVENT_IMPORT);
        $this->isEdit = $aplQuery->get_total("file_name = '$this->xmlFilename'");
    }

    function setEvents($events) {
        $this->events = $events;
    }

    function getIsEdit() {
        return $this->isEdit;
    }

    public static function replaceSpecialCharToDoubleQuote(&$array_data = array()){
        foreach($array_data as &$row){
            foreach($row as &$item){
                $item = str_replace('@@@','"',$item);
            }
        }
    }

    public static function extractPassImportEventData($encodeEventData = ""){
        if(empty($encodeEventData)) return array();
        $extractFormatEvents = Apollo_App::clean_data_request(urldecode($encodeEventData));
        $result = json_decode($extractFormatEvents,true);
        //self::replaceSpecialCharToDoubleQuote($result);
        return $result;
    }

    private function _formatReadCSVDataToEventItem($readDataEachLine = array()){
        $eventItemArr = array();
        foreach(APL_Event_CSV_Import::$arrayEventKeyToColIndex as $key => $indexCSVCol){
            $cellVal = $readDataEachLine[$indexCSVCol];
            $cellVal = str_replace("\\","",$cellVal);
            $cellVal = trim($cellVal);
            if(in_array($key,array('day0','day1','day2','day3','day4','day5','day6',)) && !empty($cellVal)){
                $timeInDay = strtotime($cellVal);
                $time24HrsFormatted = date("Hi",$timeInDay);
                $cellVal = $time24HrsFormatted;
            }
            $eventItemArr[$key] = $cellVal;
        }
        return $eventItemArr;
    }

    public function importEventByCSVFileHandler($file = array()){
        // move uploaded file to uploads/sites/.../event-import folder
        $uploadDir = wp_upload_dir();
        $eventImportDir = $uploadDir['basedir'] . '/event-import';
        if(!file_exists($eventImportDir) || !is_dir($eventImportDir)){
            mkdir($eventImportDir,0777);
        }

        $filename = $eventImportDir . '/' . current_time('timestamp') . '.csv';

        if(isset($file['tmp_name'])) {
            @move_uploaded_file($file['tmp_name'],$filename);
        }
        // do import
        if(!file_exists($filename)){
            return array(
                'msg'   => __('Import process failed. please try again !', 'apollo'),
                'success'   =>  false
            );
        }
        $importedData = array();
        $importedFile = fopen($filename,"r");

        try{
            $rowIndex = 0;
            while(! feof($importedFile))
            {
                $dataByEachLine = fgetcsv($importedFile);
                if($rowIndex === 0) {
                    $rowIndex += 1;
                    continue;
                }
                if(!empty($dataByEachLine)){
                    $importedData[] = $this->_formatReadCSVDataToEventItem($dataByEachLine);
                }
                $rowIndex += 1;
            }
            fclose($importedFile);
        }catch (Exception $ex){
            return array(
                'msg'   => __('Import process failed. please try again !', 'apollo'),
                'success'   =>  false
            );
        }
        // remove uploaded file
        @unlink($filename);
        // return array with provided format to interact with handsontable
        if(empty($importedData)){
            return array(
                'msg'   => __('Imported file is blank. Nothing to import.', 'apollo'),
                'success'   =>  false
            );
        }
        return array(
            'success' => true,
            'data' => $importedData
        );
    }

}
