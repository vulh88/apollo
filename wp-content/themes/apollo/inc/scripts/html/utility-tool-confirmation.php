<?php
    $confirmationMessage = isset($template_args['confirmationMessage']) && !empty($template_args['confirmationMessage']) ? $template_args['confirmationMessage'] : __("This script is used for undefined purpose","apollo");
    $proceedUrl = isset($template_args['proceedUrl']) && !empty($template_args['proceedUrl']) ? $template_args['proceedUrl'] : network_site_url('/404');
    $successMessage = isset($template_args['successMessage']) && !empty($template_args['successMessage']) ? $template_args['successMessage'] : '';
    $errorMessage = isset($template_args['errorMessage']) && !empty($template_args['errorMessage']) ? $template_args['errorMessage'] : '';
    $nonceField = isset($template_args['aplNonceField']) && !empty($template_args['aplNonceField']) ? $template_args['aplNonceField'] : '';
    $isLoggedIn = isset($template_args['isLoggedIn']) ? $template_args['isLoggedIn'] : false;
?>
<section class="main">
    <div class="inner apl-tool">
        <h1><?php _e("Apollo Utility Tools","apollo"); ?></h1>
        <?php if(!$isLoggedIn) : ?>
            <div class="message info"><?php echo sprintf(__("You have no permission to run this action. Please <a href=\"%s\">LOGIN</a> as role of super admin to proceed.","apollo"),site_url('login')); ?></div>
        <?php else : ?>
            <?php if(!empty($successMessage)) : ?>
                <h2><?php _e("Completed!","apollo"); ?></h2>
                <div class="message success"><?php echo $successMessage; ?></div>
            <?php endif; ?>
            <?php if(!empty($errorMessage)) : ?>
                <h2><?php _e("Error!","apollo"); ?></h2>
                <div class="message error"><?php echo $errorMessage; ?></div>
            <?php endif; ?>
            <h2><?php _e("Information","apollo"); ?></h2>
            <form method="post" action="<?php echo $proceedUrl; ?>" id="form-tool-utility">
                <?php echo $nonceField; ?>
                <div class="message info"><?php echo $confirmationMessage; ?></div>
                <input type="submit" class="btn-run-tool" value="<?php _e("Run","apollo"); ?>" />
            </form>
        <?php endif; ?>
    </div>
</section>

<style>

    .apl-tool h1{
        font-size: 25pt;
        border-bottom: 1px solid #5A5A5A;
        margin-bottom: 30px;
    }

    .apl-tool .btn-run-tool{
        display: inline-block;
        text-decoration: none;
        font-size: 14px;
        margin: 10px 0;
        padding: 10px 40px;
        cursor: pointer;
        border: 1px solid #0073aa;
        -webkit-appearance: none;
        white-space: nowrap;
        -moz-box-sizing: border-box;
        background: #00a0d2;
        color: #fff;
        font-weight: bold;
    }

    .apl-tool a.btn-run-tool {
        text-decoration: none;
    }

    .apl-tool h2{
        margin-bottom: 5px;
    }

    .apl-tool .message{
        font-family: "Open sans", Arial, Helvetica, sans-serif;
        font-size: 14px;
        line-height: 21px;
        font-weight: 600;
    }

    .apl-tool .error{
        color: #fa2805;
        margin-bottom: 5px;
    }

    .apl-tool .success{
        color: green;
        padding: 10px 0;
        display: block;
        margin-bottom: 5px;
    }

    .apl-tool .info{
        color: #555;
        padding: 10px 0;
    }

</style>

<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.btn-run-tool').off('click').on('click',function(e){
            var rc = confirm("<?php _e("Would you like to proceed this action?"); ?>");
            return rc;
        });
    });
</script>