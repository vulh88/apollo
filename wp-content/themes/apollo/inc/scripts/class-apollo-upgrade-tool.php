<?php

class Apollo_Upgrade_Tool {

    protected $siteId = '';


    public function __construct() {
        echo '<h1>Apollo Upgrade</h1><br/>';
        
        echo "<ul>"; ?>
        <li>Use case=1 parameter to update the venue zip and venue city</li>
        <li>Use case=2 parameter to update the discount url</li>
        <li>Use case=3 Not availble</li>
        <li>Use case=4 Transfer territory data from plugin to theme options</li>
        <li>Use case=5 Transfer state/city/zip (s) from wp_apollo_state_zip to wp_apollo_state_zip_v1<br/>
            .We change logic to save data
        </li>
        <li>Use case=6 Migrate discount url from event data</li>

        <li>Use case=8 Clone last name of artist from array to single field</li>
        <?php
        echo "</ul>";

        $case = isset($_GET['case']) ? $_GET['case'] : '';

        if ( $case ) {
            switch($case):
                case 1:
                    $this->updateVenueZipCity();
                    break;
                case 2:
                    $this->updateEventDiscountUrl();
                    break;
                case 3:
                    $this->updateTerritoryOpt();
                    break;

                case 4:
                    $this->updateTerrPlugin();
                    break;

                case 5:
                    $this->upgradeStateZipV1();
                    break;

                case 6:
                    $this->migrateDiscountUrlFromEventMeta();
                    break;
//                case 7:
//                    $this->cloneStateZipV1();
//                    break;
                
                case 8:
                    $this->cloneFirstNameFromArrayToSingle();
                    break;
                
                case 9:
                    $this->cloneDiscountURLFromArrayToSingle();
                    break;

                case 10:
                    $this->createTableView();
                    break;

                default:
                    echo 'No action';
            endswitch;
        }
        exit;
    }

    protected function createTableView() {

        global $wpdb;
        // Create view
        $eventMtTbl = $wpdb->prefix. Apollo_Tables::_APOLLO_EVENT_META;
        $venueMtTbl = $wpdb->prefix. Apollo_Tables::_APL_VENUE_META;
        $prgMtTbl = $wpdb->prefix. Apollo_Tables::_APL_ORG_META;
        $eventViewName = $wpdb->prefix. Apollo_Tables::_APOLLO_EVENT_VIEW;
        $postTbl = $wpdb->posts;

        $startViewTbl = $wpdb->prefix. 'apollo_view_event_start';

        $startDateView = sprintf(" CREATE VIEW %s AS SELECT meta_value start, apollo_event_id AS event_id from %s WHERE meta_key = '%s' ",
            $startViewTbl, $eventMtTbl, Apollo_DB_Schema::_APOLLO_EVENT_START_DATE);

        $endDateViewTbl = $wpdb->prefix. 'apollo_view_event_end';
        $endDateView = sprintf(" CREATE VIEW %s AS SELECT meta_value end, apollo_event_id AS event_id from %s WHERE meta_key = '%s' ",
            $endDateViewTbl, $eventMtTbl, Apollo_DB_Schema::_APOLLO_EVENT_END_DATE);

        $preferedViewTbl = $wpdb->prefix. 'apollo_view_event_prefered';
        $preferedView = sprintf(" CREATE VIEW %s AS SELECT meta_value is_prefered, apollo_event_id AS event_id from %s WHERE meta_key = '%s' ",
            $preferedViewTbl, $eventMtTbl, Apollo_DB_Schema::_E_PREFERRED_SEARCH);

        $privateViewTbl = $wpdb->prefix. 'apollo_view_event_private';
        $privateView = sprintf(" CREATE VIEW %s AS SELECT meta_value is_private, apollo_event_id AS event_id from %s WHERE meta_key = '%s' ",
            $privateViewTbl, $eventMtTbl, Apollo_DB_Schema::_E_PRIVATE);



        $eventView = sprintf(" CREATE VIEW %s AS
        SELECT p.ID, p.post_title, p.post_content, p.post_excerpt, p.post_type, p.post_status, e.end, s.start,
        prefered.is_prefered as is_prefered,
        private.is_private as is_private


        FROM %s p
        LEFT JOIN %s AS s ON s.event_id = p.ID
        LEFT JOIN %s AS e ON e.event_id = p.ID
        LEFT JOIN %s AS prefered ON prefered.event_id = p.ID
        LEFT JOIN %s AS private ON private.event_id = p.ID

        WHERE post_type = 'event'
        ",
            $eventViewName, $wpdb->posts, $startViewTbl, $endDateViewTbl, $preferedViewTbl, $privateViewTbl);


        $wpdb->query($startDateView);
        $wpdb->query($endDateView);
        $wpdb->query($preferedView);
        $wpdb->query($privateView);

        $wpdb->query($eventView);
    }

    private function getTableName($tbl) {
        global $wpdb;
        $siteId = is_main_site($this->siteId) ? '' : $this->siteId. '_';
        return $wpdb->prefix.''.$siteId. $tbl;
    }

    function updateVenueZipCity() {

        $siteId = isset($_GET['site_id']) ? $_GET['site_id'] : '';

        if ( !$siteId ) {
            echo 'Site ID is empty';
            exit;
        }

        $this->siteId = $siteId;

        $aplQuery = new Apl_Query($this->getTableName(Apollo_Tables::_APL_VENUE_META), true);
        $results = $aplQuery->get_where("meta_key = '".Apollo_DB_Schema::_APL_VENUE_ADDRESS."' ");


        if ( ! $results ) {
            echo 'No data';
            exit;
        }
        $countZip = $countCity = 0;
        foreach ( $results as $result ) {
            $isExistVenueZip = $aplQuery->get_row('meta_key = "'.Apollo_DB_Schema::_VENUE_ZIP.'" AND apollo_venue_id = '.$result->apollo_venue_id.' ');

            $addressData = maybe_unserialize($result->meta_value);
            if (!is_array($addressData)) {
                $addressData = maybe_unserialize($addressData);
            }

            if ( !$isExistVenueZip ) {

                if ($addressData && isset($addressData[Apollo_DB_Schema::_VENUE_ZIP])) {
                    $countZip++;
                    $aplQuery->insert(array(
                        'apollo_venue_id' => $result->apollo_venue_id,
                        'meta_key'        => Apollo_DB_Schema::_VENUE_ZIP,
                        'meta_value'      =>   $addressData[Apollo_DB_Schema::_VENUE_ZIP]
                    ));

                    echo '<p>Insert zip of: venue_id = '.$result->apollo_venue_id.' - Zip = '.$addressData[Apollo_DB_Schema::_VENUE_ZIP].'</p>';
                }
            } else if (!$isExistVenueZip->meta_value) {
                if ($addressData && isset($addressData[Apollo_DB_Schema::_VENUE_ZIP])) {
                    $countZip++;
                    $aplQuery->update(array(
                        'meta_key'        => Apollo_DB_Schema::_VENUE_ZIP,
                        'meta_value'      =>   $addressData[Apollo_DB_Schema::_VENUE_ZIP]
                    ), array(
                        'meta_id'   => $isExistVenueZip->meta_id
                    ));

                    echo '<p>Update zip of: venue_id = '.$result->apollo_venue_id.' - Zip = '.$addressData[Apollo_DB_Schema::_VENUE_ZIP].'</p>';
                }
            }

            $isExistVenueCity = $aplQuery->get_row('meta_key = "'.Apollo_DB_Schema::_VENUE_CITY.'" AND apollo_venue_id = '.$result->apollo_venue_id.' ');

            if ( !$isExistVenueCity ) {

                if ($addressData && isset($addressData[Apollo_DB_Schema::_VENUE_CITY])) {
                    $countCity++;
                    $aplQuery->insert(array(
                        'apollo_venue_id' => $result->apollo_venue_id,
                        'meta_key'        => Apollo_DB_Schema::_VENUE_CITY,
                        'meta_value'      =>   $addressData[Apollo_DB_Schema::_VENUE_CITY]
                    ));
                    echo '<p>Insert City of: venue_id = '.$result->apollo_venue_id.' - City = '.$addressData[Apollo_DB_Schema::_VENUE_CITY].'</p>';
                }
            } else if (!$isExistVenueCity->meta_value) {
                if ($addressData && isset($addressData[Apollo_DB_Schema::_VENUE_CITY])) {
                    $countCity++;
                    $aplQuery->update(array(
                        'meta_key'        => Apollo_DB_Schema::_VENUE_CITY,
                        'meta_value'      =>   $addressData[Apollo_DB_Schema::_VENUE_CITY]
                    ), array(
                        'meta_id'   => $isExistVenueCity->meta_id
                    ));
                    echo '<p>Update City of: venue_id = '.$result->apollo_venue_id.' - City = '.$addressData[Apollo_DB_Schema::_VENUE_CITY].'</p>';
                }
            }

        }

        echo sprintf('<p>There are %s venues</p>', $results ? count($results) : 0);
        echo sprintf('<p>There are %s updated zip</p>', $results ? $countZip : 0);
        echo sprintf('<p>There are %s updated City</p>', $results ? $countCity : 0);

        exit;
    }

    function updateEventDiscountUrl() {

        $siteId = isset($_GET['site_id']) ? $_GET['site_id'] : '';

        if ( !$siteId ) {
            echo 'Site ID is empty';
            exit;
        }

        $this->siteId = $siteId;

        $aplQuery = new Apl_Query($this->getTableName(Apollo_Tables::_APOLLO_EVENT_META), true);
        $results = $aplQuery->get_where("meta_key = '".Apollo_DB_Schema::_APOLLO_EVENT_DATA."' ");

        if ( ! $results ) {
            echo 'No data';
            exit;
        }
        $count = 0;
        foreach ( $results as $result ) {
            $isExist = $aplQuery->get_row('meta_key = "'.Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL.'" AND apollo_event_id = '.$result->apollo_event_id.' ');
            if ( !$isExist ) {
                $metaData = maybe_unserialize($result->meta_value);
                if ($metaData && isset($metaData[Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL])) {
                    $count++;
                    $aplQuery->insert(array(
                        'apollo_event_id' => $result->apollo_event_id,
                        'meta_key'        => Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL,
                        'meta_value'      =>   $metaData[Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL]
                    ));
                    echo '<p>Insert discount url of: event_id = '.$result->apollo_event_id.' - Discount URL = '.$metaData[Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL].'</p>';
                }
            }

        }

        echo sprintf('<p>There are %s events</p>', $results ? count($results) : 0);
        echo sprintf('<p>There are %s updated Discount URL</p>', $results ? count($count) : 0);

        exit;
    }

    function updateTerritoryOpt() {

        $states = of_get_option(Apollo_DB_Schema::_TERR_STATES);
        $cities = of_get_option(Apollo_DB_Schema::_TERR_CITIES);
        $zips = of_get_option(Apollo_DB_Schema::_TERR_ZIPCODE);

        aplDebug($states);
        aplDebug($cities);
        aplDebug($zips);

        $terrData = array();
        if ($states) {

            $newStates = array();
            foreach($states as $state => $v) {
                $terrData[$state] = array();
                $newStates[$state] = 1;
            }

            $cities = explode("<>", $cities);

            if ($cities) {
                $cities = array_unique($cities);

                foreach($cities as $city) {
                    if (!$city) continue;
                    $cityData = explode(" (", $city);
                    $_state = trim(str_replace(")", "", $cityData[1]));

                    if (isset($terrData[$_state])) {
                        $terrData[$_state][$cityData[0]] = array();
                    }
                }
            }

            if ($zips) {
                $zips = explode("<>", $zips);

                global $wpdb;
                $aplQuery = new Apl_Query('wp_apollo_state_zip');
                foreach($zips as $zip) {

                    $zipData = $aplQuery->get_row("zipcode='$zip'");
                    if ($zipData && isset($terrData[$zipData->state_prefix][$zipData->city])) {
                        $terrData[$zipData->state_prefix][$zipData->city][$zip]='on';
                    }
                }
            }

        }
        echo '<p>New Territory data</p>';
        aplDebug($terrData);
        echo "<p>---------------------------------------------------------------</p>";

        $oldThemeOpt = get_option('_apollo_theme_options');
        echo "<p>Old theme option</p>";
        aplDebug($oldThemeOpt);

        echo "<p>---------------------------------------------------------------</p>";
        echo '<p>set update=1 to update new data</p>';
        if (isset($_GET['update']) && $_GET['update'] == 1) {
            $oldThemeOpt[Apollo_DB_Schema::_TERR_DATA] = $terrData;
            $oldThemeOpt[Apollo_DB_Schema::_TERR_STATES] = $newStates;
            aplDebug($oldThemeOpt);
            update_option('_apollo_theme_options', $oldThemeOpt);
        }

    }

    function updateTerrPlugin() {

        $terrPost = get_posts(array(
            'post_type' => 'territory',
            'posts_per_page' => 1,
            'post_status'   => 'publish',
            'orderby' => 'post_date',
            'order' => 'desc'
        ));

        if ($terrPost) {
            $post = $terrPost[0];
            $states = get_post_meta($post->ID, 'meta-states', TRUE);
            $cities = get_post_meta($post->ID, 'meta-terrcity', TRUE);
            $zips = get_post_meta($post->ID, 'meta-terrzip', TRUE);


            $terrData = array();
            if ($states) {
                $newStates = array();
                foreach($states as $state) {
                    $terrData[$state] = array();
                    $newStates[$state] = 1;
                }

                if ($cities) {
                    $cities = array_unique($cities);

                    foreach($cities as $city) {
                        if (!$city) continue;
                        $cityData = explode(" (", $city);
                        $_state = trim(str_replace(")", "", $cityData[1]));

                        if (isset($terrData[$_state])) {
                            $terrData[$_state][$cityData[0]] = array();
                        }
                    }
                }

                if ($zips) {

                    global $wpdb;
                    $aplQuery = new Apl_Query('wp_apollo_state_zip');
                    foreach($zips as $zip) {

                        $zipData = $aplQuery->get_row("zipcode='$zip'");
                        if ($zipData && isset($terrData[$zipData->state_prefix][$zipData->city])) {
                            $terrData[$zipData->state_prefix][$zipData->city][$zip]='on';
                        }
                    }
                }

            }

            echo '<p>New Territory data</p>';
            aplDebug($terrData);
            echo "<p>---------------------------------------------------------------</p>";

            $oldThemeOpt = get_option('_apollo_theme_options');
            echo "<p>Old theme option</p>";
            aplDebug($oldThemeOpt);

            echo "<p>---------------------------------------------------------------</p>";
            echo '<p>set update=1 to update new data</p>';
            if (isset($_GET['update']) && $_GET['update'] == 1) {
                $oldThemeOpt[Apollo_DB_Schema::_TERR_DATA] = $terrData;
                $oldThemeOpt[Apollo_DB_Schema::_TERR_STATES] = $newStates;
                aplDebug($oldThemeOpt);
                update_option('_apollo_theme_options', $oldThemeOpt);
            }

        }

    }


    function upgradeStateZipV1() {
       
        global $wpdb;
        $oldTbl = $wpdb->prefix. 'apollo_state_zip';
        $newTbl = $wpdb->prefix. 'apollo_state_zip_v1';
        $oldQuery = new Apl_Query($oldTbl, true);
        $newQuery = new Apl_Query($newTbl, true);

        $states = $oldQuery->get_where();
       
        if ($states) {
            foreach( $states as $st ) {

                $sCheck = $newQuery->get_row("code = '$st->state_prefix' and type = 'state'");
                if (! $sCheck) {
                    $newQuery->insert(array(
                        'code'  => $st->state_prefix,
                        'name'  => $st->state_name,
                        'type'  => 'state'
                    ));
                    $newResult = $newQuery->get_row("code='$st->state_prefix' AND type='state'");
                    $stateId = $newResult->id;
                } else {
                    $stateId = $sCheck->id;
                }
                echo "state = ". $stateId. '<br/>';
                $cCheck = $newQuery->get_row("parent = $stateId AND type = 'city' AND code = '$st->city'");

                if ($cCheck == null || !$cCheck) {

                    $newQuery->insert(array(
                        'code'  => $st->city,
                        'name'  => $st->city,
                        'type'  => 'city',
                        'parent'    => $stateId
                    ) );

                    $newResult = $newQuery->get_row("code='$st->city' AND type='city' AND parent = $stateId");
                    $cityId = $newResult->id;
                } else {
                    $cityId = $cCheck->id;
                }

                $zCheck = $newQuery->get_row("parent = $cityId AND type = 'zip' AND code= '$st->zipcode'");
                if (! $zCheck) {
                    $newQuery->insert(array(
                        'code'  => $st->zipcode,
                        'name'  => $st->zipcode,
                        'type'  => 'zip',
                        'parent'    => $cityId,
                    ));
                }

            }
        }
    }
    
    function migrateDiscountUrlFromEventMeta() {
        ini_set('max_execution_time', 300);
        global $wpdb;
      
        $metaTbl = Apollo_Tables::_APOLLO_EVENT_META;
        $current = current_time('Y-m-d');
      
        $sql = "
            SELECT p.ID from $wpdb->posts p 
            INNER JOIN $metaTbl mt ON mt.apollo_event_id = p.ID     
            WHERE p.post_type = 'event' AND p.post_status = 'publish' 
            AND mt.meta_key = '".Apollo_DB_Schema::_APOLLO_EVENT_END_DATE."'
            AND CAST( mt.meta_value AS DATE ) >= '{$current}'   
            GROUP BY p.ID
        ";
        $posts = $wpdb->get_results($sql);
      
        $numEvent = 0;
        aplDebug($posts);
        if ( $posts ) {
            foreach( $posts as $i => $p ) {
               
                if ( ! Apollo_App::apollo_get_meta_data($p->ID, Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL) ) {
                    $metaData = maybe_unserialize(Apollo_App::apollo_get_meta_data( $p->ID , Apollo_DB_Schema::_APOLLO_EVENT_DATA));
                    
                    if ( isset($metaData[Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL]) ) {
                        $numEvent++;
                        
                        if ( isset($_GET['update']) && $_GET['update'] == 1 ) {
                            update_apollo_meta( $p->ID, Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL, $metaData[Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL] );
                        }
                    }
                }
            }
        } 
        echo 'There are '. $numEvent. ' events. Set update param is equal to 1 to update';
        
    }
    
    function cloneFirstNameFromArrayToSingle() {
        $aqlQuery = new Apl_Query(Apollo_Tables::_APOLLO_ARTIST_META);
        $metaData = $aqlQuery->get_where("meta_key = '".Apollo_DB_Schema::_APL_ARTIST_DATA."'");
     
        if ($metaData) {
            $count = 0;
            foreach( $metaData as $mt ) {
                $artist = get_artist($mt->apollo_artist_id);
                $lname = $artist->get_meta_data(Apollo_DB_Schema::_APL_ARTIST_LNAME);
                $lnameInArr = $artist->get_meta_data(Apollo_DB_Schema::_APL_ARTIST_LNAME, Apollo_DB_Schema::_APL_ARTIST_DATA);
               
                if ( $lname ) {
                    continue;
                }
                
                if ( isset($_GET['confirm']) && $_GET['confirm'] == 'yes' ) {
                    update_apollo_meta( $mt->apollo_artist_id, Apollo_DB_Schema::_APL_ARTIST_LNAME, $lnameInArr );
                } else {
                    $count++;
                }
            }
            
            if ( isset($_GET['confirm']) || $_GET['confirm'] != 'yes' ) {
                if ( $count ) {
                    echo sprintf("<p>There are %s artists need to be update the last name. Do you want to continue for upgrading? Please type confirm = yes</p>", $count);
                } else {
                    echo 'Done';
                }
            } else {
                echo 'Done';
            }
            
            
            
        }
    }
    
    function cloneDiscountURLFromArrayToSingle() {
        global $wpdb;

        $metaTbl = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};
        $current = current_time('Y-m-d');

        $sql = "
            SELECT * FROM $metaTbl mta WHERE mta.meta_key = '".Apollo_DB_Schema::_APOLLO_EVENT_DATA."' AND mta.apollo_event_id IN
            ( SELECT p.ID from $wpdb->posts p 
            INNER JOIN $metaTbl mt ON mt.apollo_event_id = p.ID     
            WHERE p.post_type = 'event' AND p.post_status = 'publish' 
            AND mt.meta_key = '".Apollo_DB_Schema::_APOLLO_EVENT_END_DATE."'
            AND CAST( mt.meta_value AS DATE ) >= '{$current}'   
            GROUP BY p.ID )
        ";    
        $metaData = $wpdb->get_results($sql);
        $updateEvents = array();
        if ($metaData) {
            $count = 0;
            foreach ($metaData as $mt) {
                $event = get_event($mt->apollo_event_id);
                $discountUrl = $event->get_meta_data(Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL);
                $discountUrlInArr = $event->get_meta_data(Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL, Apollo_DB_Schema::_APOLLO_EVENT_DATA);

                if ($discountUrl) {
                    continue;
                }
                
                if (isset($_GET['confirm']) && $_GET['confirm'] == 'yes') {
                    update_apollo_meta($mt->apollo_event_id, Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL, $discountUrlInArr);
                } else {
                    $updateEvents[] = $mt->apollo_event_id; 
                    $count++;
                }
            }

            if (isset($_GET['confirm']) || $_GET['confirm'] != 'yes') {
                if ($count) {
                    echo sprintf("<p>There are %s events need to be update the last name. Do you want to continue for upgrading? Please type confirm = yes</p>", $count);
                    aplDebug($updateEvents);
                } else {
                    echo 'Done';
                }
            } else {
                echo 'Done';
            }
        }
    }
    
}

$ob = new Apollo_Upgrade_Tool();