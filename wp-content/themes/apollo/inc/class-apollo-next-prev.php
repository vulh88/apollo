<?php
//Trilm apollo_next_prev
class Apollo_Next_Prev{

    /**
     * @author Trilm
     */
    public static function initSearchResult($module_type){
        $listing = self::getSearchResult($module_type);
        if(!$listing){
            $name = ucfirst($module_type);
            if($module_type == Apollo_DB_Schema::_ORGANIZATION_PT){
                $module_type = 'org';
                $name = ucfirst($module_type);
            }elseif ($module_type == Apollo_DB_Schema::_PUBLIC_ART_PT){
                $name = 'Public_Art';
            }
            $path =  APOLLO_TEMPLATES_DIR. '/pages/'.$module_type.'/lib/class-'.$module_type.'-search.php';
            if (file_exists($path)){
                include($path);
                $className = "Apollo_{$name}_Page";
                if(class_exists($className)){
                    $moduleSearch = new $className();
                    $moduleSearch->search();
                }
            }
        }
    }

    /**
     * @author: Trilm
     * @param $data, $module_type
     */
    public static function updateSearchResult($strSql,$module_type = ''){
        //only default search must remove limit
        list($strSql) = explode('LIMIT',$strSql);
        if($strSql!= ''){
            if(isset($_SESSION[Apollo_DB_Schema::_SEARCH_SESSION_KEY]) && is_array( $_SESSION[Apollo_DB_Schema::_SEARCH_SESSION_KEY])){
                $_SESSION[Apollo_DB_Schema::_SEARCH_SESSION_KEY][$module_type] = $strSql;
            }
            else{
                $_SESSION[Apollo_DB_Schema::_SEARCH_SESSION_KEY] = array( $module_type => $strSql);
            }

        }
        if ( isset($_GET['is_debug']) && $_GET['is_debug']) {
            aplDebug($_SESSION);
        }

    }

    /**
     * @author Trilm
     * @return mixed
     */
    public static function getSearchResult($module_type){
        global $wpdb;
        $preferredData = array();
        $sqlStr = isset($_SESSION[Apollo_DB_Schema::_SEARCH_SESSION_KEY][$module_type])?$_SESSION[Apollo_DB_Schema::_SEARCH_SESSION_KEY][$module_type]:null;
        $data =  $wpdb->get_results($sqlStr);
        if(isset($_GET['is_debugging']) && $_GET['is_debugging']){
            aplDebug(
                array(
                    '$_SESSION' => $_SESSION,
                    '$_SESSION_$module_type' => $_SESSION[Apollo_DB_Schema::_SEARCH_SESSION_KEY][$module_type],
                    'data' => $data,
                    '$module_type' => $module_type,
                ),1
            );
        }
        return self::convertData($data);
    }

    public static function convertData($data){
        $return = array();
        if(is_array($data) && count($data) ){
            foreach($data as $k => $value){
                $return[] = $value->ID;
            }
        }
        return $return;
    }

    /**
     * @auhthor Trilm
     * @param $postID
     */
    public static function getPostPosition($postID,$module_type){
        $searchListing = self::getSearchResult($module_type);

        if(is_array($searchListing) && count($searchListing) > 0){
            foreach ($searchListing as $k => $item){
                if($postID == $item){
                    return   array('pos'=> $k, 'listing' => $searchListing );
                }
            }
        }


        return array('pos'=> -1, 'listing' => $searchListing );
    }

    /**
     * @author Trilm
     * @param $postID
     * @param string $case
     * @return string|void
     */
    public static function getPostLink($posInfo ,$case = 'next'){

        $pos = $posInfo['pos'];
        $listing = $posInfo['listing'];
        $nextTitle =  __('Next Listing','apollo');
        $prevTitle =   __('Previous Listing','apollo');
        $nextIcon  = ' <i class="fa fa-chevron-right" aria-hidden="true"></i>
  ';
        $prevIcon  = '<i class="fa fa-chevron-left" aria-hidden="true"></i>
  ';
        $format = '';
        //search result has items.
        if(is_array($listing) && count($listing) > 0){
            $check = false;
            $next = '';
            //next link
            if($case == 'next'){
                $title = $nextTitle;
                $icon = $nextIcon;
                $format = '<a href="$link" >$title</a> $icon ';
                if($pos  <= count($listing) - 2){
                    $check = true;
                    $next = $pos + 1;
                }
            }
            //previous link
            else{
                $title = $prevTitle;
                $icon = $prevIcon;
                if($pos >= 1){
                    $check = true;
                    $next = $pos - 1;
                }
                $format = '$icon <a href="$link" >$title</a>  ';
            }
            if($check && $check != ''){
                $item = isset($listing[$next])?$listing[$next]:null;
                if($item){
                    $link = get_the_permalink($item);
                    $format = str_replace('$link',$link,$format);
                    $format = str_replace('$icon',$icon,$format);
                    $format = str_replace('$title',$title,$format);
                    return  $format;
                }
            }
        }
        return  '';
    }

    /**
     * @author Trilm
     * @param $postId
     */
    public static function getNavLink($postId,$module_type){
        $posInfo = self::getPostPosition($postId,$module_type);
        $nextLink = self::getPostLink($posInfo,'next');
        $previousLink = self::getPostLink($posInfo,'prev');

        if((!$nextLink || trim($nextLink) == '') &&  (!$previousLink || $previousLink == '')){
            return;
        }

        $html = "
            <div class=\"nav-link\">
                <ul>
                    <li class=\"pre\"> $previousLink</li>
                    <li class=\"next\">$nextLink  </li>
                </ul>
            </div>
        ";
        echo $html;
    }


}