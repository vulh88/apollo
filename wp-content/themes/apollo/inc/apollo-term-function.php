<?php

/**
 * Apollo Terms
 *
 * Functions for handling terms/term meta.
 *
 * @author 		vulh
 * @category 	inc
 * @package 	Apollo/Functions
 */

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo Term Meta API - Update term meta
 *
 * @param mixed $term_id
 * @param mixed $meta_key
 * @param mixed $meta_value
 * @param string $prev_value (default: '')
 * @return bool
 */
function update_apollo_term_meta( $term_id, $meta_key, $meta_value, $prev_value = '' ) {
    return update_metadata( 'apollo_term', $term_id, $meta_key, $meta_value, $prev_value );
}

/**
 * Apollo Term Meta API - Get term meta
 *
 * @param mixed $term_id
 * @param mixed $key
 * @param bool $single (default: true)
 * @return mixed
 */
function get_apollo_term_meta( $term_id, $key, $single = true ) {
	return get_metadata( 'apollo_term', $term_id, $key, $single );
}

/**
 * Apollo Term item Meta API - set table name
 *
 * @return void
 */
function apollo_taxonomy_metadata_wpdbfix() {
	global $wpdb;
	$termmeta_name = Apollo_Tables::_APOLLO_TERM_META;

    $wpdb->{$termmeta_name} = $wpdb->prefix . $termmeta_name;

	$wpdb->tables[] = $termmeta_name;
}
add_action( 'init', 'apollo_taxonomy_metadata_wpdbfix', 0 );
add_action( 'switch_blog', 'apollo_taxonomy_metadata_wpdbfix', 0 );


/**
 * Apollo Dropdown categories
 *
 * @param string $type $taxonomy ( default: event-type )
 * @param int $show_counts (default: 1)
 * @param int $hierarchical (default: 1)
 * @param int $show_uncategorized (default: 1)
 * @return string
 */
function apollo_dropdown_categories( $taxonomy = 'event-type', $args = array(), $select_text = '', $deprecated_hierarchical = 1, $deprecated_show_uncategorized = 1, $deprecated_orderby = '', $forceSelected = false ) {
	global $wp_query;
    
    if( ! $select_text ) $select_text = __( 'Select Category', 'apollo' );
	if($taxonomy == Apollo_DB_Schema::_APL_EDU_TYPE)  $select_text = __( 'Select Educator Type', 'apollo' );
    
	if ( ! is_array( $args ) ) {

		$args['show_counts']        = $args;
		$args['hierarchical']       = $deprecated_hierarchical;
		$args['show_uncategorized'] = $deprecated_show_uncategorized;
		$args['orderby']            = $deprecated_orderby;
	}
   
	$defaults = array(
		// 'pad_counts'         => 0, // ThienLD : If turn this argument ON => error http://redmine.elidev.info/issues/12303 will be appeared. Reason : in some case 'parent' term = null|empty then it causes the error. Ref error link : http://lists.automattic.com/pipermail/wp-trac/2010-December/098898.html
		'show_counts'        => 1,
		'hierarchical'       => 1,
		'hide_empty'         => 1,
		'show_uncategorized' => 1,
		'orderby'            => 'name',
		'selected'           => $forceSelected ? $forceSelected : (isset( $wp_query->query[$taxonomy] ) ? $wp_query->query[$taxonomy] : ''),
		'menu_order'         => false
	);

	$args = wp_parse_args( $args, $defaults );

	if ( $args['orderby'] == 'order' ) {
		$args['menu_order'] = 'asc';
		$args['orderby']    = 'name';
	}

	$terms = get_terms( $taxonomy, $args );
    
    /**
     * Change text of first option in event type 
     * combo box due to we need one opt is home 
     * page in display management event
     * 
     */ 
    $first_select_text = isset( $_REQUEST['dm'] ) && $_REQUEST['dm'] ? __( 'Home page', 'apollo' ) : $select_text; 

	$enableAutoComplete = isset($args['auto_complete']) && $args['auto_complete'] ? 'apl_select2' : '';

    $output  = "<select class='".$enableAutoComplete."' name='{$taxonomy}' id='dropdown-{$taxonomy}'>";
	$output .= '<option value="" ' .  selected( isset( $_GET[$taxonomy] ) ? $_GET[$taxonomy] : '', '', false ) . '>' . $first_select_text . '</option>';
	
    if ( ! $terms ) {
        echo $output;
        return;
    }
    $output .= apollo_walk_category_dropdown_tree( $terms, 0, $args );
	$output .="</select>";

    echo $output;
}

/**
 * Walk the Event Categories.
 *
 * @return mixed
 */
function apollo_walk_category_dropdown_tree() {
    
	if ( ! class_exists( 'Apollo_Post_Type_Dropdown_Walker' ) ) {
        include_once( APOLLO_INCLUDES_DIR . '/walkers/class-post-type-dropdown-walker.php' );
    }
		
	$args = func_get_args();

	// the user's options are the third parameter
	if ( empty( $args[2]['walker']) || !is_a($args[2]['walker'], 'Walker' ) ) {
        $walker = new Apollo_Post_Type_Dropdown_Walker();
    } else {
        $walker = $args[2]['walker'];
    }
    
	return call_user_func_array(array( &$walker, 'walk' ), $args );
}

/**
 * Move a term before the a	given element of its hierarchy level
 *
 * @param int $the_term
 * @param int $next_id the id of the next sibling element in save hierarchy level
 * @param string $taxonomy
 * @param int $index (default: 0)
 * @param mixed $terms (default: null)
 * @return int
 */
function apollo_reorder_terms( $the_term, $next_id, $taxonomy, $index = 0, $terms = null ) {

	if( ! $terms ) $terms = get_terms($taxonomy, 'menu_order=ASC&hide_empty=0&parent=0' );
	if( empty( $terms ) ) return $index;

	$id	= $the_term->term_id;

	$term_in_level = false; // flag: is our term to order in this level of terms

	foreach ($terms as $term) {

		if( $term->term_id == $id ) { // our term to order, we skip
			$term_in_level = true;
			continue; // our term to order, we skip
		}
		// the nextid of our term to order, lets move our term here
		if(null !== $next_id && $term->term_id == $next_id) {
			$index++;
			$index = apollo_set_term_order($id, $index, $taxonomy, true);
		}

		// set order
		$index++;
		$index = apollo_set_term_order($term->term_id, $index, $taxonomy);

		// if that term has children we walk through them
		$children = get_terms($taxonomy, "parent={$term->term_id}&menu_order=ASC&hide_empty=0");
		if( !empty($children) ) {
			$index = apollo_reorder_terms( $the_term, $next_id, $taxonomy, $index, $children );
		}
	}

	// no nextid meaning our term is in last position
	if( $term_in_level && null === $next_id )
		$index = apollo_set_term_order($id, $index+1, $taxonomy, true);

	return $index;
}

/**
 * Set the sort order of a term
 *
 * @param int $term_id
 * @param int $index
 * @param string $taxonomy
 * @param bool $recursive (default: false)
 * @return int
 */
function apollo_set_term_order( $term_id, $index, $taxonomy, $recursive = false ) {

	$term_id 	= (int) $term_id;
	$index 		= (int) $index;


    $meta_name = str_replace( '-' , '_', $taxonomy). '_order';

	update_apollo_term_meta( $term_id, $meta_name, $index );

	if( ! $recursive ) return $index;

	$children = get_terms($taxonomy, "parent=$term_id&menu_order=ASC&hide_empty=0");

	foreach ( $children as $term ) {
		$index ++;
		$index = apollo_set_term_order($term->term_id, $index, $taxonomy, true);
	}

	clean_term_cache( $term_id, $taxonomy );

	return $index;
}

// filter categories
add_filter( 'terms_clauses', 'apollo_terms_clauses', 10, 3 );
function apollo_terms_clauses($args) {

	if (get_query_var('_apollo_api')) return $args;

    global $wpdb;
    $apl_termmeta_tbl = $wpdb->{Apollo_Tables::_APOLLO_TERM_META};
    $args['join'] .= " LEFT JOIN $apl_termmeta_tbl AS apl_tm ON t.term_id = apl_tm.apollo_term_id AND apl_tm.meta_key = 'population_served_order'";
    $args['orderby'] = "ORDER BY apl_tm.meta_value+0, t.name";
    $args['order'] = ' ASC';
    $args['fields'] .= ',apl_tm.*';

    return $args;
}
