<?php

if(!defined('ABSPATH')) {
    die; // Silent is golem
}
/**
 * User: tuanphp
 * Date: 22/10/2014
 * Time: 17:07
 */

class Apollo_Single_Post_Hook {
    public function __construct() {
        add_action('template_redirect', array(__CLASS__, 'afterGetPostMod'));

        /* From another file but effected to single events */
        add_action('init', array(__CLASS__, 'checkCaptcha'), 11, 2);
    }

    public static  function afterGetPostMod() {
        
        if ( ! is_single() ) return FALSE;
        self::__actionFunction();
        self::__filterFunction();
        
        
        add_action('comment_form_after', function() {
            Apollo_App::add_ads_block(FALSE);
        });

        if ( ! is_single('post') && ! is_single('page') ) {
            add_action('apollo_social_factory', array( __CLASS__, 'hook_ads_top' ));
            add_action('comment_form_after', array( __CLASS__, 'hook_ads_bottom' ));
        }
    }

    private static function __actionFunction() {
        add_action('wp_head', array(__CLASS__, 'addOpenGraphFaceBook'), 10, 2);

    }

    public static function checkCaptcha() {
        if(is_user_logged_in() )
            return;

        if(isset($_POST['_apollo_submit_comment_event'])) {
            // Check captcha and show error;
            if(!isset($_POST['captcha_code']) || (isset($_POST['captcha_code']) && $_POST['captcha_code'] !== Apollo_Captcha::getCurrentCode())) {
                Apollo_Captcha::_destroySession();
                wp_die( __( "<strong>ERROR</strong>: Captcha code is not match!" ) );
            }
        }
    }

    private static function __filterFunction() {
        add_filter('post_class', array(__CLASS__, 'removeHentryClass'), 10, 3);
    }

    public static function addOpenGraphFaceBook() {
        global $post;
        
        // Get post module data
        $the_post_mod = Apollo_ABS_Module::get_post_mod( $post );
        
        if ( ! method_exists( $the_post_mod , 'get_post_mod' ) ) return false;
        
        // Get summary
        $a_summary = $the_post_mod->get_summary(100);
        $t_summary = $a_summary['text'];
        if($a_summary['have_more'] === true) {
            $t_summary .= '...';
        }

        echo '<meta property="og:title" content="' . $the_post_mod->get_title() . '"/>';
        echo '<meta property="og:type" content="article"/>';
        echo '<meta property="og:url" content="' . $the_post_mod->get_permalink() . '"/>';
        echo '<meta property="og:image" content="' . $the_post_mod->get_article_image_url() . '"/>';
        echo '<meta property="og:description" content="' . esc_attr($t_summary) . '"/>';

    }

    public static function removeHentryClass($classes, $class, $postId) {

        $key = array_search("hentry", $classes, false);
        if($key !== false) {
            unset($classes[$key]);
        }

        return $classes;
    }
    
    public static function hook_ads_bottom() {
        Apollo_App::add_ads_block(FALSE);
    }
    
    public static function hook_ads_top() {
        Apollo_App::add_ads_block();
    }
    
}

new Apollo_Single_Post_Hook();