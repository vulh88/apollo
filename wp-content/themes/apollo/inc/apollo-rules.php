<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class Exists {

    public function isValid($value, $param) {
        list($table, $column, $where) = explode(',', $param);

        $aplQuery = new Apl_Query($table, true);
        return $aplQuery->get_total(" $column = '$value' AND $where ") == 0;
    }

}

class Required {
    public function isValid($value) {
        if(is_array($value)) {
            $value = array_filter($value);
        }
        else if(is_string($value)) {
            $value = trim($value);
        }

        return !empty($value);
    }
}

class Email {
    public function isValid($value) {
        if ( ! $value ) return true;
        return is_email($value);
    }
}
class Number {
    public function isValid($value) {
        if ( ! $value ) return true;
        return is_numeric($value);
    }
}
class Url {
    function isValid($url){
        // first do some quick sanity checks:
        $url = trim($url);
        if( !empty($url) ) {
            if (!$url || !is_string($url)) {
                return false;
            }
           return preg_match( '/^(http|https):\\/\\/[a-z0-9_]+([\\-\\.]{1}[a-z_0-9]+)*\\.[_a-z]{2,5}'.'((:[0-9]{1,5})?\\/.*)?$/i' ,$url) == 1;
        }
        return true;
    }
}

class StartDateLargerNow {
    public function isValid($value, $param, $arrDataHolder) {
        return $value > date('Y-m-d');
    }
}

class EndateLargerThanStartdate {
    public function isValid($value, $startDateKey, $arrDataHolder) {
        if ( ! $value || (! $arrDataHolder[$startDateKey] && empty($_POST[$startDateKey])) ) {
            return true;
        } else if (! $arrDataHolder[$startDateKey] && !empty($_POST[$startDateKey])) {
            return $value >= $_POST[$startDateKey];
        }
        return $value >= $arrDataHolder[$startDateKey];
    }
}

class largerThan{
    public function isValid($value, $minvalue) {
        if($value == ''){
            return true;
        }

        $value = intval($value);

        return $value > $minvalue;
    }
}

class YoutubeUrl{
    public function isValid($value, $param, $arrDataHolder = array()) {

        if(empty($value)) return true; /* alllow empty url */

        // check youtube
        if(is_string($value)) {
            return filter_var( $value, FILTER_VALIDATE_URL ) && ( Apollo_App::is_youtube_url($value) || Apollo_App::is_vimeo($value) );
        }

        if(is_array($value)) {
            foreach($value as $v) {
                if(empty($v)) return false;
                if(!empty($v) && filter_var( $v, FILTER_VALIDATE_URL ) === false) {
                    return false;
                }
            }
        }

        if(is_array($value)) {
            foreach($value as $v) {
                if( !empty($v) && !Apollo_App::is_youtube_url($v) && !Apollo_App::is_vimeo($v) ) {
                    return false;
                }
            }
        }

        return true;
    }
}
//Tri start check embed
class Embed{
    public function isValid($value, $param, $arrDataHolder = array()) {
        if(is_string($value)){
            if(empty($value)){
                return false;
            }
            return $this->checkStringUrl($value);
        }
        if(is_array($value)){
            foreach($value as $v){
                if(empty($v))
                    return false;
                if(!empty($v) && !$this->checkStringUrl($v)){
                    return false;
                }
            }
        }
        return true;
    }

    public function getAudioType(){
        return  array(
            'mp3',
            'mp4',
            'wav',
            'wav',
            'aif',
            'flac'
        );
    }

    public function checkStringUrl($url){
        if(empty($url))
            return false;
        $urlClass = new Url();
        //check url
        if($urlClass->isValid($url) == true){
            if (strpos($url,"embed") != false){
                return true;
            }
            if (strpos($url,"/m/") != false){
                return true;
            }
            /** @Ticket #19321 - Sound cloud link */
            if (strpos($url, 'https://soundcloud.com') >= 0) {
                return true;
            }
            //foreach($audioTypes as $type){
            if($this->checkMimeType($url)){
                return true;
            }
        }
        //check iframe
        if($this->checkIframeObject($url))
            return true;
        //check embed
        if($this->checkEmbedObject($url))
            return true;
        return false;
    }
    public function checkEmbedObject($url){

        if($this->checkObject($url))
            return true;
        if($this->checkAudio($url))
            return true;
        if($this->checkEmbed($url))
            return true;
        return false;
    }
    public function checkObject($url){
        if(strpos($url,"object") != false && strpos($url,"</object>") != false)
            return true;
        return false;
    }
    public function checkAudio($url){
        if(strpos($url,"audio") != false && strpos($url,"</audio>") != false)
            return true;
    }
    public function checkEmbed($url){
        if(strpos($url,"embed") != false && strpos($url,"</embed>") != false)
            return true;
    }
    public function checkIframeObject($url){
        if(strpos($url,"iframe") != false && strpos($url,"</iframe>"))
            return true;
        return false;
    }
    public function checkMimeType($url){
        $audioTypes = $this->getAudioType();
        $ext = strtolower(pathinfo($url, PATHINFO_EXTENSION));
        if(in_array($ext, $audioTypes)){
            return true;
        }
    }
}
class dateLargerNow {
    public function isValid($value, $param, $arrDataHolder) {
    	if ( ! $value ) return true;
    	return $value > date('Y-m-d');
    }
}

/**
 * @ticket #18695: add validate primary photo
 */
class Apollo_Validation_Primary_Photo{
    public function isValid($value, $param, $arrDataHolder) {

        if(empty($_POST['_file'])){
            return false;
        }
        return true;
    }
}