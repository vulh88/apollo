<?php

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

class Apollo_Calendar {

    private static $space = ' - ';

    public static $arrWeeks = array(
        0 => 'SUN',
        1 => 'MON',
        2 => 'TUE',
        3 => 'WED',
        4 => 'THU',
        5 => 'FRI',
        6 => 'SAT',
    );

    public  static function getAllEventTime($event_id)
    {

        global $wpdb;
        $tbl_ec = $wpdb->{Apollo_Tables::_APL_EVENT_CALENDAR};

        $sql = "select event_id, date_event, time_from, time_to, ticket_url, booth_override from $tbl_ec where event_id = '%s'";

        $test = $wpdb->get_results($wpdb->prepare($sql, array(
                $event_id
            )));
        return $test;
    }

    public static function parseEventFrame($adata)
    {
        $arr = array();

        if(empty($adata)) {
            return $arr;
        }

        foreach($adata as $_ => $obj) {
            $d = $obj->date_event;
            $tf = $obj->time_from;
            $tt = $obj->time_to;
            $eid = $obj->event_id;
            $arr[$d][] = array(
                'event_id' => $eid,
                'time_from' => $tf,
                'time_to' => $tt,
            );
        }

        return $arr;
    }

    public static function editEvent($date_event, $event_id, $otime_from, $otime_to,  $time_from, $time_to){
        global $wpdb;
        $tbl_ec = $wpdb->{Apollo_Tables::_APL_EVENT_CALENDAR};


        $date_event = (array)$date_event;

        $sp = '(' . implode(", ",array_fill(0, count($date_event), '%s')) .')';

        $sql = "update $tbl_ec set time_from = '%s', time_to = '%s' where date_event in $sp and event_id = '%s' and time_from = '%s' and time_to = '%s'";

        $arrP = array(
            $time_from, $time_to,
        );

        $arrP = array_merge($arrP, $date_event);
        $arrP = array_merge($arrP, array(
                $event_id, $otime_from, $otime_to
            ));

        return $wpdb->query($wpdb->prepare($sql, $arrP));
    }

    public static function createEvent($event_id, $date_event, $time_from, $time_to){
        global $wpdb;
        $tbl_ec = $wpdb->{Apollo_Tables::_APL_EVENT_CALENDAR};

        if(!is_array($date_event)) {
            $r = $wpdb->insert($tbl_ec, array(
                    'time_from' => $time_from,
                    'time_to' => $time_to,
                    'event_id' => $event_id,
                    'date_event' => $date_event,
                ));

            if($r) {
                $a = new stdClass();
                $a->event_id = $event_id;
                $a->date_event = $date_event;
                $a->time_from = $time_from;
                $a->time_to = $time_to;

                return $a;
            }
            else {
                return false;
            }
        }

        $arrdata = array();
        $sql = "insert into {$tbl_ec} (event_id, date_event, time_from, time_to) values ";
        $value = array();


        foreach($date_event as $_ => $v) {
            $value[] = "('%s', '%s', '%s', '%s')";
            $arrdata = array_merge($arrdata, array(
                    $event_id, $v, $time_from, $time_to
                ));
        }

        if ( ! $value ) return false;

        $sv = implode(", ", $value);

        $fsql = $sql.$sv .' on duplicate key update event_id = event_id, date_event = date_event, time_from = time_from, time_to = time_to ;';

        $r = $wpdb->query($wpdb->prepare($fsql, $arrdata));

        return $r;

    }

    public static function deleteEventByListDate($eid, $date_event)
    {
        global $wpdb;
        $tbl_ec = $wpdb->{Apollo_Tables::_APL_EVENT_CALENDAR};

        if($date_event === 'all')
        //if(1)
        {
            $sql = "delete from $tbl_ec where event_id = '%s'";
            $arrP = array(
                $eid
            );
        }
        //we no need it that time
        else {
            $adate = explode(",", $date_event);

            $sp = '';
            for($i =0; $i <= count($adate) - 2 ; $i++){
                $date = $adate[$i];
                $sp .= 'date_event = \''.$date.'\' OR ';

            }
            $date = $adate[count($adate)-1];
            $sp .= 'date_event = \''.$date.'\'';
            $sql = "delete from $tbl_ec where ($sp) and event_id = '%s'";
            $arrP[] = $eid;
        }
        return !!$wpdb->query($wpdb->prepare($sql, $arrP));
    }

    public static function deleteEventByListDateAndTime($eid, $date_event, $time_from, $time_to)
    {
        global $wpdb;
        $tbl_ec = $wpdb->{Apollo_Tables::_APL_EVENT_CALENDAR};

        $adate = $date_event;

        $sp = '(' . implode(", ",array_fill(0, count($adate), '%s')) .')';

        $sql = "delete from $tbl_ec where date_event in $sp and event_id = '%s' and time_from = %s and time_to = %s";

        $arrP = $adate;
        $arrP[] = $eid;
        $arrP[] = $time_from;
        $arrP[] = $time_to;


        return !!$wpdb->query($wpdb->prepare($sql, $arrP));
    }

    public static function deleteEventByTimeFrame($eid, $date_from, $date_to)
    {
        global $wpdb;
        $tbl_ec = $wpdb->{Apollo_Tables::_APL_EVENT_CALENDAR};

        if($date_from === 'all' && $date_to === 'all') {
            $sql = "delete from $tbl_ec where event_id = '%s'";
            $arrP = array(
                $eid
            );
        }
        else {
            $sql = "delete from $tbl_ec where date_event >= '%s' and date_event <= '%s' and event_id = '%s'";
            $arrP = array(
                $date_from,
                $date_to,
                $eid
            );
        }

        return $wpdb->get_var($wpdb->prepare($sql, $arrP));
    }

    public static function deleteEventById($id){
        global $wpdb;
        $tbl_ec = $wpdb->{Apollo_Tables::_APL_EVENT_CALENDAR};

        $aid = (array)$id;

        $aid = array_map('intval', $aid);

        $sid = "(".implode(", ", $aid) . ")";

        global $wpdb;
        $tbl_ec = $wpdb->{Apollo_Tables::_APL_EVENT_CALENDAR};

        $sql = "delete from {$tbl_ec} where id in $sid";

        return $wpdb->query($wpdb->prepare($sql));

    }

    public static function createEvents($event_id, $day, $timefrom, $timeend)
    {
        //get
        $start_date = get_apollo_meta($event_id, Apollo_DB_Schema::_APOLLO_EVENT_START_DATE, true);
        $end_date = get_apollo_meta($event_id, Apollo_DB_Schema::_APOLLO_EVENT_END_DATE, true);

        if(empty($start_date) || empty($end_date)) {
            wp_die(__('Something is missing', 'apollo'));
        }

        $arrdate = self::getArrDateByCondition(array(
                'start_date' => $start_date,
                'end_date' => $end_date,
                'day' => $day
            ));

        return self::createEvent($event_id, $arrdate, $timefrom, $timeend);

    }

    public static function getStepsDateByDate($event_id, $d1, $d2) {
        $arrS = array();

        $days_of_week = maybe_unserialize( get_apollo_meta($event_id, Apollo_DB_Schema::_APOLLO_EVENT_DAYS_OF_WEEK, true) );
        if (empty($days_of_week)) {
            /** not exists days of week */
            return $arrS;
        }
        $t1 = strtotime($d1);
        $t2 = strtotime($d2);

        for($i = $t1; $i <= $t2; $i+= 24*3600) {

            $day = date('w', $i);

            if ( $days_of_week && ! in_array( $day , $days_of_week) ) continue;

            $arrS[] = date('Y-m-d', $i);
        }

        return $arrS;
    }

    /*MON, SUN*/
    public static function getStepsDateByDay($eid, $d1, $d2, $format = 'Y-m-d') {

        //get
        $start_date = get_apollo_meta($eid, Apollo_DB_Schema::_APOLLO_EVENT_START_DATE, true);
        $end_date = get_apollo_meta($eid, Apollo_DB_Schema::_APOLLO_EVENT_END_DATE, true);
        $days_of_week = maybe_unserialize( get_apollo_meta($eid, Apollo_DB_Schema::_APOLLO_EVENT_DAYS_OF_WEEK, true) );

        $arrdayAllow = self::getDaysBetweenDay($d1, $d2);

        $arrS = array();

        $t1 = strtotime($start_date);
        $t2 = strtotime($end_date);

        for($i = $t1; $i <= $t2; $i+= 24*3600) {

            $day = date('w', $i);
            if ( $days_of_week && ! in_array( $day , $days_of_week) ) continue;

            $_d = strtoupper(date('D', $i));
            if(in_array($_d, $arrdayAllow)) {
                $arrS[] = date($format, $i);
            }

        }

        return $arrS;
    }

    private static function getDaysBetweenDay($fd, $ed) {

        $arr = array();

        $arrW = Apollo_Calendar::$arrWeeks;

        $key1 = array_search($fd, $arrW);
        $key2 = array_search($ed, $arrW);

        for($i = $key1; $i <= $key2; $i++) {
            $arr[] = $arrW[$i];
        }

        return $arr;
    }

    private static function getArrDateByCondition($arrcondition){

        $arr = array();

        $sd = $arrcondition['start_date'];
        $ed = $arrcondition['end_date'];
        $day = $arrcondition['day'];

        $sdt = strtotime($sd);
        $edt = strtotime($ed);

        if(date('w', $sdt) === $day) {
            $s = $sdt;
        }
        else {
            //  'sun' | 'mon' | 'tue' | 'wed' | 'thu' | 'fri' | 'sat' | 'sun'
            $arrw = self::$arrWeeks;
            $s = strtotime('next '.$arrw[$day], $sdt);
        }

        for($i = $s ; $i <= $edt; $i += 7*24*3600) {
            $arr[] = date('Y-m-d', $i);
        }

        return $arr;
    }

    public static function specialEdit($ids, $time_from, $time_to){

    }

    public static function specialDelete($eid, $date_event){

        //get
        $start_date = get_apollo_meta($eid, Apollo_DB_Schema::_APOLLO_EVENT_START_DATE, true);
        $end_date = get_apollo_meta($eid, Apollo_DB_Schema::_APOLLO_EVENT_END_DATE, true);

        if(empty($start_date) || empty($end_date)) {
            return array(
                'error' => true,
                'msg' => 'Start date endate is required!',
            );
        }

        $aday = explode(",", $date_event);

        $aday = self::getDaysBetweenDay($aday[0], $aday[1]);

        $arr = array_flip(self::$arrWeeks);

        $result = true;

        $arrall_dates = array();

        foreach($aday as $_ => $d) {
            $arrdate = self::getArrDateByCondition(array(
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'day' => $arr[$d]
                ));

            $arrall_dates = array_merge($arrall_dates, $arrdate);

            $arrdate = implode(",", $arrdate);

            self::deleteEventByListDate($eid, $arrdate);

        }

        return array(
            'error' => false,
            'data' => $arrall_dates,
        );
    }

    public static function delete($eid, $startDate = "", $endDate = "") {
        global $wpdb;

        $tbl_ec = $wpdb->{Apollo_Tables::_APL_EVENT_CALENDAR};

        $whereDate = $startDate && $endDate ? " AND (date_event < '$startDate' OR date_event > '$endDate')" : "";

        $sql = "delete from {$tbl_ec} where event_id = '%s' $whereDate";

        return $wpdb->query($wpdb->prepare($sql, array(
                    $eid
                )));

    }

    /*TriLM delete all event in cell*/
    public  static function deleteAllEventInCell($event_id,$date_event)
    {
        global $wpdb;
        $tbl_ec = $wpdb->{Apollo_Tables::_APL_EVENT_CALENDAR};
        $sql = "delete  from $tbl_ec where event_id = '%s' and date_event = \"'%s'\"";
        return $wpdb->get_results($wpdb->prepare($sql, array(
            $event_id,
            $date_event
        )));
    }

    /*TriLM delete event in date range*/
    public static function deleteInRange($eid, $startDateNew = "", $endDateNew = "", $startDateOld = "", $endDateOld= "" ) {
        global $wpdb;
        $caseSQL = self::dateInRangeCase($startDateNew, $endDateNew , $startDateOld , $endDateOld,$eid);
        if($caseSQL == false)
            return false;
        $tbl_ec = $wpdb->{Apollo_Tables::_APL_EVENT_CALENDAR};
        $whereDate = " AND ($caseSQL)";
        $sql = "delete from {$tbl_ec} where event_id = '%s' $whereDate";

        return $wpdb->query($wpdb->prepare($sql, array(
            $eid
        )));
    }
    public static function dateInRangeCase( $startDateNew, $endDateNew , $startDateOld , $endDateOld,$eventID){
        if( empty($startDateNew) || empty($endDateNew) || empty($startDateOld) || empty($endDateOld) )
            return false;
        $newStart = strtotime($startDateNew);
        $newEnd = strtotime($endDateNew);
        $oldStart = strtotime($startDateOld);
        $oldEnd = strtotime($endDateOld);

        $rangeStart = '';
        $rangeEnd = '';


        //case 1: echo 'case 1';
        if($oldEnd >= $newEnd && $oldStart >= $newStart ){
            $rangeStart =  $startDateOld;
            $rangeEnd =  $endDateNew;

        }
        //case 2:
        if($oldEnd <= $newEnd && $oldStart <= $newStart ){

            $rangeStart =  $startDateNew;
            $rangeEnd =  $endDateOld;


        }
        //case 3:
        if($oldEnd >= $newEnd && $oldStart <= $newStart ){
            $rangeStart =  $startDateNew;
            $rangeEnd =  $endDateNew;


        }
        //case 4:
        if($oldEnd <= $newEnd && $oldStart >= $newStart){
            $rangeStart =  $startDateOld;
            $rangeEnd =  $endDateOld;


        }
        $sql = 'date_event < \''.$rangeStart.'\' OR date_event > \''.$rangeEnd.'\'';

        //delete disable cell
        //get disable cell

        $getDisableEventCell =  Apollo_App::apollo_get_meta_data($eventID,Apollo_DB_Schema::_APL_EVENT_DISABLE_CALENDAR_CELL);
        if(is_array($getDisableEventCell) && count($getDisableEventCell) > 0){
            foreach($getDisableEventCell as $k => $disableCell){
                $timeRangeStart = strtotime($rangeStart);
                $timeRangeEnd = strtotime($rangeEnd);
                $time = strtotime($disableCell);
                if(!($timeRangeStart<=$time && $time<=$timeRangeEnd)){
                    unset($getDisableEventCell[$k]);
                }
            }
            update_apollo_meta($eventID,Apollo_DB_Schema::_APL_EVENT_DISABLE_CALENDAR_CELL,$getDisableEventCell);
        }
        return $sql;
    }

    /**
     *
     * @author: Trilm
     * @return array
     *
     */
    public static function static_get_disable_cell($event_id){
        $meta = Apollo_App::apollo_get_meta_data($event_id, Apollo_DB_Schema::_APL_EVENT_DISABLE_CALENDAR_CELL);
        return array(
            'result' => $meta
        );
    }

    /**
     * @author: Trilm
     *
     */
    public static function static_get_all_event($event_id){
        $adata = self::getAllEventTime($event_id);
        $pdata = self::parseEventFrame($adata);
        return array(
            'result' => $pdata
        );
    }
}
