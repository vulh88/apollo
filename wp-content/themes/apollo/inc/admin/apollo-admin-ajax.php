<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
/**
 * Apollo Apollo_AJAX
 *
 * AJAX Event Handler
 *
 * @class 		Apollo_AJAX
 * @package		inc/Classes
 * @category	Class
 * @author 		vulh
 */
class Apollo_Admin_Ajax {

    /**
     * Hook into ajax events
     */
    public function __construct() {

        // apollo => nopriv
        $ajax_events = array(
            // save coordinate by user session
            'spotlight_ordering'    => FALSE,
            'add_to_laureate_award' => FALSE,
            'remove_upload_pdf'     => FALSE,
            'term_ordering'         => FALSE,
            're_order_custom_fields'=> FALSE,
            'send_test_email'       => FALSE,
            'preview_template'      => FALSE,
            'keep_add_new_user_password'    => FALSE,
            'filter_city_zip_by_states' => FALSE,
            'get_states'                => FALSE,
            'add_state_city_zip'        => false,
            'delete_state_city_zip'    => FALSE,
            'save_custom_event_options' => FALSE,
            'clone_state_zip_v1' => FALSE,
            'is_main_site' => FALSE,
            'get_trans_term_name'   => FALSE,
            'show_more_syndication_individual_events' => FALSE,
            'show_more_event_theme_individual_events' => FALSE,
            'show_more_event_theme_individual_orgs' => FALSE,
            'show_more_event_theme_individual_venues' => FALSE,
            'show_more_venue_iframe_search_widget' => FALSE,
            'cache_network_site' => FALSE,
            'sync_solr_data' => FALSE,
            'delete_sync_solr_data' => FALSE,
            'compare_solr_old_new_config' => FALSE,
            'empty_cache_search_widget' => FALSE,
            'clear_top_ten' => false,
            'show_more_posts_type'  => false,
            'create_wp_editor'  => false,
            'get_more_spot_feat_event'  => false,
            'get_token'  => false,
            'refresh_public_art_map'  => false,
            'get_users' => false,
            'empty_spotlight_cache' => false,
            'get_more_artists' => false,
            'syndication_clear_cache'   => false,
            'add_neighborhood'   => false,
            'remove_neighborhood'   => false,
            'neighborhood_get_data'   => false,
            'admin_get_neighborhood_by_city'   => false,
            'user_get_more_association_options'   => false,
            'admin_clear_cache'   => false,
            'get_more_associated_users' => false,
            'get_more_associated_agencies' => false,
            'navigation_pending_counter'    => false,
            'event_update_review_edit'    => false,
            'reject_event'    => false,
            'get_event_count'    => false,
            'agency_get_more_association_options'   => false,
            'get_remote_user_data_to_select2_box'   => false,
            'ajax_display_list_table' => false,
        );

        if(empty($ajax_events)) return;

        foreach ( $ajax_events as $ajax_event => $nopriv ) {
            add_action( 'wp_ajax_apollo_' . $ajax_event, array( $this, $ajax_event ) );

            if ( $nopriv )
                add_action( 'wp_ajax_nopriv_apollo_' . $ajax_event, array( $this, $ajax_event ) );
        }
    }

    public function ajax_display_list_table() {

        $postType = isset($_REQUEST['post_type']) ? $_REQUEST['post_type'] : '';
        switch ($postType) {
            case 'syndication-org':
                require_once APOLLO_ADMIN_DIR . '/post-types/meta-boxes/syndication/org/APL_Org_Syndication_Table.php';
                $listSelectedItem = '';
                if (isset($_GET['post_id'])) {
                    $listSelectedItem = explode(',', get_post_meta($_GET['post_id'], APL_Syndication_Const::_SO_META_ORG_SELECTED, true));
                }
                $tableView = new APL_Org_Syndication_Table();
                $tableView->setListOrgSelected($listSelectedItem);
                $textSearch = __('Search org', 'apollo');
                break;
            default:
                require_once APOLLO_ADMIN_DIR . '/post-types/meta-boxes/syndication/artist/APL_Artist_Syndication_Table.php';
                $listSelectedItem = '';
                if (isset($_GET['post_id'])) {
                    $listSelectedItem = explode(',', get_post_meta($_GET['post_id'], APL_Syndication_Const::_SA_META_ARTIST_SELECTED, true));
                }
                $tableView = new APL_Artist_Syndication_Table();
                $tableView->setListArtistSelected($listSelectedItem);
                $textSearch = __('Search artist', 'apollo');
                break;
        }

        $tableView->prepare_items();

        ob_start();
        $tableView->search_box($textSearch, 'syndication-module-search-box');
        $tableView->display();
        $display = ob_get_clean();
        die( wp_json_encode( $display ) );
    }

    /**
     * Enable display the pending count number on menu for modules (Events, Orgs, Venues, Artists, Classifieds, Educators, Programs)
     *
     * @ticket #15216 - [CF] 20180305 - [Admin] Adding pending counter items into the left menu like Comments - item 4
     */
    public function navigation_pending_counter()
    {
        $availableModules = Apollo_App::get_avaiable_modules(get_current_blog_id());

        // check for modules: Event, Org, Venue, Artist, Classified
        $counters = array();
        $modules = array(
            Apollo_DB_Schema::_ORGANIZATION_PT,
            Apollo_DB_Schema::_VENUE_PT,
            Apollo_DB_Schema::_ARTIST_PT,
            Apollo_DB_Schema::_CLASSIFIED,
        );
        foreach ($modules as $module) {
            if (in_array($module, $availableModules)) {
                $counters[$module] = $this->_showPendingCount($module);
            }
        }

        // perform different query for Event module
        if (in_array(Apollo_DB_Schema::_EVENT_PT, $availableModules)) {
            $counters[Apollo_DB_Schema::_EVENT_PT] = $this->_showPendingCount(Apollo_DB_Schema::_EVENT_PT);
        }

        // check for 2 modules (Educator & Program)
        if (in_array(Apollo_DB_Schema::_EDUCATION, $availableModules)) {
            $counters[Apollo_DB_Schema::_EDUCATOR_PT] = $this->_showPendingCount(Apollo_DB_Schema::_EDUCATOR_PT);
            $counters[Apollo_DB_Schema::_PROGRAM_PT]  = $this->_showPendingCount(Apollo_DB_Schema::_PROGRAM_PT);
        }

        wp_send_json(array('data' => $counters));
    }

    /**
     * Show the pending count number for modules
     *
     * @param string $module
     *
     * @return string
     * @ticket #15216 - [CF] 20180305 - [Admin] Adding pending counter items into the left menu like Comments - item 4
     */
    private function _showPendingCount($module = '')
    {
        global $wpdb;

        if ($module == Apollo_DB_Schema::_EVENT_PT) {
            $pendingCount = AplEventFunction::getEventCountByStatus('pending');
        } elseif($module == Apollo_DB_Schema::_CLASSIFIED) {
            $current = current_time('Y-m-d');
            $pendingCount = (int) $wpdb->get_var("
                                SELECT COUNT(*) from $wpdb->posts p 
                                JOIN {$wpdb->{Apollo_Tables::_APL_CLASSIFIED_META}} m ON m.apollo_classified_id = p.ID AND m.meta_key = '_apl_classified_exp_date'  
                                WHERE p.post_type = 'classified' AND p.post_status = 'pending'  AND m.meta_value >= '{$current}'
                            ");
        } else {
            $pendingCount = (int) $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM $wpdb->posts WHERE post_type = %s AND post_status = 'pending'", $module));
        }

        return $pendingCount ? " <span class='awaiting-mod count-1' title='title'><span class='pending-count'>$pendingCount</span></span>" : '';
    }

    /**
     * @Ticket #16209 - get user data
     */
    public function get_remote_user_data_to_select2_box() {
        global $wpdb;
        $total = 0;
        $incompleteResults = true;
        $results = array();
        $blogUserTable = Apollo_App::getBlogUserTable();
        $currentBlog = get_current_blog_id();
        $offset = 0;
        if (isset($_GET['page'])) {
            $offset = intval($_GET['page'] - 1) * Apollo_Display_Config::APL_USER_MANAGEMENT_LIMIT;
        } else {
            $results[] = array(
                'id' => '',
                'text' => __('Select Author', 'apollo')
            );
        }
        $keyword = isset($_GET['q']) ? '%'.$_GET['q'].'%' : '';
        $queryUserId = "SELECT user_id FROM {$blogUserTable} WHERE blog_id = {$currentBlog} ";
        $userInBlog = $wpdb->get_col($queryUserId);
        if ($userInBlog) {
            $users = new WP_User_Query( array(
                'include'     => $userInBlog,
                'search'      => $keyword,
                'orderby'     => 'login',
                'order'       => 'ASC',
                'offset'      => $offset,
                'number'      => Apollo_Display_Config::APL_USER_MANAGEMENT_LIMIT
            ) );
            if ($users->get_results()) {
                foreach ($users->get_results() as $user) {
                    $results[] = array(
                        'id' => $user->ID,
                        'text' => $user->data->display_name . '('. $user->data->user_email . ')'
                    );
                }
                $total = $users->get_total();
                $incompleteResults = false;
            }
        }
        wp_send_json(array(
            'items'     => $results,
            'total_count'   => $total,
            'incomplete_results' => $incompleteResults
        ));
    }

    /**
     * @Ticket #15217 - Get count event by event status.
     */
    public function get_event_count( ) {
        $listStatus = $_POST['list_status'];
        $result = array();
        if (!empty($listStatus)) {
            foreach ($listStatus as $status) {
                $result[$status] = AplEventFunction::getEventCountByStatus($status);
            }
        }
        wp_send_json(array(
            'event_count' => $result
        ));
    }

    /**
     * Get more artists
     *
     * @return object
     */
    public function get_more_artists(){
        if(isset($_GET['post_id'])){
            $post_id = $_GET['post_id'];
            $textSearch = isset($_GET['text_search']) ? $_GET['text_search'] : '';
            $offset = isset($_REQUEST['offset']) ?intval($_REQUEST['offset']) : 0;
            $module = isset($_GET['module']) ? $_GET['module'] : '';

            if($module == 'event') {
                $artists = APL_Artist_Function::getListArtistForEvent($offset, $textSearch, $post_id);
                // Get list selected artists
                $html = APL_Artist_Function::renderArtistsForEvent($artists['data']);
            } else {
                $artists = APL_Artist_Function::getListArtists($offset, $textSearch);
                // Get list selected artists
                $event = get_public_art($post_id);
                $selectedArtistIDs = $event->getAssociatedArtistIDs();
                $html = Apollo_App::renderArtists($artists['data'], $selectedArtistIDs);
            }

            $offset += count($artists['data']);

            wp_send_json(array(
                'url' => admin_url('admin-ajax.php?action=apollo_get_more_artists&module=' . $module. '&post_id=' . $post_id . '&offset=' . $offset . '&text_search=' . $textSearch),
                'html' => $html,
                'have_more' => $artists['have_more'],
            ));

        }else{
            wp_send_json(array(
                'url' => admin_url('admin-ajax.php?action=apollo_get_more_artists&offset='),
                'html' => '',
                'have_more' => false,
            ));
        }
    }


    /**
     * User Association get more items by post_type.
     */
    public function user_get_more_association_options(){
        if(isset($_GET['post_type']) && isset($_GET['user_id'])){
            $offset = isset($_GET['offset']) ? $_GET['offset'] : 0;
            $textSearch = isset($_GET['text_search']) ? $_GET['text_search'] : '';
            $listOption = ApolloAssociationFunction::getAssociationModuleForUser($_GET['user_id'], $_GET['post_type'], $offset, $textSearch);


            wp_send_json(array(
                'url'           => admin_url('admin-ajax.php?action=apollo_user_get_more_association_options&post_type='. $_GET['post_type'] .'&offset='.$listOption['offset'].'&user_id='.$_GET['user_id'].'&text_search='.$textSearch),
                'html' => Apollo_App::userRenderAssociationItems($listOption['data']),
                'have_more'    => $listOption['have_more']
            ));
        }else{
            wp_send_json(array(
                'url'           => admin_url('admin-ajax.php?action=apollo_user_get_more_association_options'),
                'html' => '',
                'have_more'    => false
            ));
        }
    }

    /**
     * Agency Association get more items by post_type.
     */
    public function agency_get_more_association_options(){
        if(isset($_GET['post_type']) && isset($_GET['agency_id'])){
            $offset = isset($_GET['offset']) ? $_GET['offset'] : 0;
            $textSearch = isset($_GET['text_search']) ? $_GET['text_search'] : '';
            if ($_GET['post_type'] == 'user') {
                $listOption = ApolloAssociationFunction::getUsersForAgency($_GET['agency_id'], $offset, $textSearch);
            } else {
                $listOption = ApolloAssociationFunction::getAssociationModuleForAgency($_GET['agency_id'], $_GET['post_type'], $offset, $textSearch);
            }

            wp_send_json(array(
                'url'           => admin_url('admin-ajax.php?action=apollo_agency_get_more_association_options&post_type='. $_GET['post_type'] .'&offset='.$listOption['offset'].'&agency_id='.$_GET['agency_id'].'&text_search='.$textSearch),
                'html' => Apollo_App::userRenderAssociationItems($listOption['data']),
                'have_more'    => $listOption['have_more']
            ));
        }else{
            wp_send_json(array(
                'url'           => admin_url('admin-ajax.php?action=apollo_agency_get_more_association_options'),
                'html' => '',
                'have_more'    => false
            ));
        }
    }

    /**
     * Get more associated users by module
     *
     * @ticket #15226 - [CF] 20180126 - [2049#c12311][Admin] Selected association users on the organization, venue, educator, artist detail
     */
    public function get_more_associated_users()
    {
        $postId   = isset($_GET['post_id'])   ? $_GET['post_id']   : '';
        $postType = isset($_GET['post_type']) ? $_GET['post_type'] : 'artist';
        $offset   = isset($_GET['offset'])    ? $_GET['offset']    : 0;

        if (empty($postId) || $postId <= 0) {
            wp_send_json(array(
                'url'       => admin_url('admin-ajax.php?action=apollo_get_more_associated_users'),
                'html'      => '',
                'have_more' => false,
            ));
        }

        $result = ApolloAssociationFunction::getSelectedAssociationUsers(
            $postId,
            $postType,
            Apollo_Display_Config::APL_ASSOCIATED_USERS_DETAIL_PAGE_LIMIT,
            $offset
        );
        $url = sprintf(
            'admin-ajax.php?action=apollo_get_more_associated_users&post_id=%d&post_type=%s&offset=%d',
            $postId,
            $postType,
            $result['offset']
        );

        wp_send_json(array(
            'url'       => admin_url($url),
            'html'      => ApolloAssociationFunction::renderAssociatedUsers($result['users'], true),
            'have_more' => $result['hasMore'],
        ));
    }

    /**
     * Get more associated agencies by module
     *
     * @ticket #15226 - [CF] 20180126 - [2049#c12311][Admin] Selected association users on the organization, venue, educator, artist detail
     */
    public function get_more_associated_agencies()
    {
        $postId   = isset($_GET['post_id'])   ? $_GET['post_id']   : '';
        $postType = isset($_GET['post_type']) ? $_GET['post_type'] : 'artist';
        $offset   = isset($_GET['offset'])    ? $_GET['offset']    : 0;

        if (empty($postId) || $postId <= 0) {
            wp_send_json(array(
                'url'       => admin_url('admin-ajax.php?action=apollo_get_more_associated_agencies'),
                'html'      => '',
                'have_more' => false,
            ));
        }

        $result = ApolloAssociationFunction::getSelectedAssociationAgencies(
            $postId,
            $postType,
            Apollo_Display_Config::APL_ASSOCIATED_USERS_DETAIL_PAGE_LIMIT,
            $offset
        );
        $url = sprintf(
            'admin-ajax.php?action=apollo_get_more_associated_agencies&post_id=%d&post_type=%s&offset=%d',
            $postId,
            $postType,
            $result['offset']
        );

        wp_send_json(array(
            'url'       => admin_url($url),
            'html'      => ApolloAssociationFunction::renderAssociatedAgencies($result['agencies'], true),
            'have_more' => $result['hasMore'],
        ));
    }

    /**
     * @Ticket #15217 - Update list review event
     * @return bool
     */
    public function event_update_review_edit() {
        $id = $_POST['post_id'];
        if (empty($id)) {
            wp_send_json(array(
                'status' => false
            ));
            return false;
        }
        AplEventFunction::eventUpdateListReviewEdit($id, true);
        wp_send_json(array(
            'status' => true
        ));
    }

    /**
     * @Ticket #15850 - Reject event
     * @return bool
     */
    public function reject_event() {
        $id = $_POST['post_id'];
        wp_trash_post($id);
        $reasons = isset($_POST['reasons']) ? $_POST['reasons'] : array();
        $reasonDetail = isset($_POST['reasonDetail']) ? $_POST['reasonDetail'] : '';
        if (!empty($reasonDetail)) {
            $reasonDetail = str_replace(array("\r\n", "\n", "\r"), "<br />", $reasonDetail);
        }
        do_action( 'apollo_email_confirm_rejected_item_to_owner', $id, Apollo_DB_Schema::_EVENT_PT, $reasons, $reasonDetail, false );
        if (empty($id)) {
            wp_send_json(array(
                'status' => false
            ));
            return false;
        }
        AplEventFunction::eventUpdateListReviewEdit($id, true);
        wp_send_json(array(
            'status' => true,
            'redirectLink' => admin_url( 'edit.php?post_type='. Apollo_DB_Schema::_EVENT_PT . '&post_status=pending')
        ));
        exit;
    }

    /**
     * Syndication clear cache
     */
    public function syndication_clear_cache()
    {
        $type = isset($_GET['type']) ? $_GET['type'] : '';

        if ($type == 'artist') {
            $cacheObj = aplc_instance('APLC_Inc_Files_Syndication_Artist_CacheFactory');
            if (!empty($_GET['post_id'])){
                $cacheObj->clear(APLC_Inc_Files_Syndication_Artist_CacheFactory::ARTIST, $_GET['post_id']);
            }
            $cacheObj->clear(APLC_Inc_Files_Syndication_Artist_CacheFactory::CATEGORY);
            $cacheObj->clear(APLC_Inc_Files_Syndication_Artist_CacheFactory::STYLE);
            $cacheObj->clear(APLC_Inc_Files_Syndication_Artist_CacheFactory::MEDIUM);
            $cacheObj->clear(APLC_Inc_Files_Syndication_Artist_CacheFactory::CITY);
        }
        else {
            require_once APOLLO_ADMIN_SYNDICATE_DIR. '/class-apollo-syndication-caching-handler.php';
            Apollo_Syndicate_Caching_Handler::clearCache();
        }
        wp_send_json(array(
            'status' => 'TRUE',
            'hidden' => 1,
            'msg' => __("Clear cache successfully", 'apollo')
        ));
    }

    /**
     * @Ticket #15013 - Clear cache by class name.
     */
    public function admin_clear_cache() {
        if (!empty($_POST['class_name'])) {
            if (!empty($_POST['params']) && !empty($_POST['file_type'])) {
                $fileCache = aplc_instance($_POST['class_name'], $_POST['params']);
                $eventCaching = new APLC_Inc_Event_Category_Cache();
                $eventCaching->setCurrentEventCategoryID($_POST['params']);
                if ($_POST['event_transient'] == 'EventCategoryFeaturedBlock') {
                    $eventCaching->setEventFeaturedCache('');
                }
                if ($_POST['event_transient'] == 'EventCategorySpotlightBlock') {
                    $eventCaching->setEventSpotLightCache('');
                }
            }
            else if (!empty($_POST['params']) && !empty($_POST['extension'])) {
                $fileCache = aplc_instance($_POST['class_name'], $_POST['params']);
                $fileCache->setExtension($_POST['extension']);
            }
            else if (strpos($_POST['class_name'], 'APLC_Inc_Files_EventIFrameSearchWidget') !== false) {

                $data = explode(',', $_POST['class_name']);
                list($id, $swCacheClass) = $data;
                $fileCache = aplc_instance($swCacheClass, array('ifsw_id' => $id));
            }
            else {
                $fileCache = aplc_instance($_POST['class_name']);
            }
            $fileCache->remove();
            if (!empty($_POST['params']) && !empty($_POST['syn_transient'])){
                Apollo_Admin_Cache::admin_clear_syndication_transient_cache($_POST['params']);
            }

            wp_send_json(array(
                'status' => 1
            ));
        }
        wp_send_json(array(
            'status' => 0
        ));
    }

    /**
     * Get limitation users
     * @return object
     */
    public function get_users() {

        global $wpdb;

        $page = !empty($_GET['page']) ? $_GET['page'] : 1;
        $perPage = 5;

        $args = wp_parse_args( array(
            'number' => $perPage,
            'paged' => $page,
            'fields'    => array('ID', 'user_login'),
            'search'    => isset($_GET['q']) ? '%'. $_GET['q']. '%' : '',
            'count_total'   => true,
            'search_columns' => array('user_login', 'user_email', 'user_nicename'),
            'blog_id'   => get_current_blog_id()
        ) );

        $user_search = new WP_User_Query($args);

        $users = (array) $user_search->get_results();

        if ($page == 1) {
            $results[] = array(
                'id'    => '',
                'text'  => __('Select a user', 'apollo')
            );
        }
        else {
            $results = [];
        }


        if( !empty($users) )  {

            foreach( $users as $user ) {

                $results[] = array(
                    'id' => $user->ID,
                    'text'  => $user->user_login
                );

            }
        }

        $total = $user_search->get_total();
        $incompleteResults = Apollo_App::hasMore($page, $perPage, $total);

        wp_send_json(array(
            'items'     => $results,
            'total_count'   => $total,
            'incomplete_results' => $incompleteResults
        ));
    }

    public function get_more_spot_feat_event(){
        $syndFilterClassFilePath = APOLLO_ADMIN_DIR .'/post-types/meta-boxes/syndication/class-apollo-meta-box-syndication-filter.php';
        if(file_exists($syndFilterClassFilePath)){
            require_once($syndFilterClassFilePath);
        }
        if(class_exists('Apollo_Meta_Box_Syndication_Filter')){
            $syndicationID = isset($_POST['syndication_id']) ? $_POST['syndication_id'] : -1 ;
            $curPage = isset($_POST['cur_page']) ? $_POST['cur_page'] : 1 ;
            $result =  Apollo_Meta_Box_Syndication_Filter::getHTMLSpotFeatEventsWithPagination(array(
                'syndication_id' => $syndicationID,
                'cur-page' => $curPage
            ));
            wp_send_json(array(
                'msg' => __('SUCCESS', 'apollo'),
                'status'    => 'TRUE',
                'resultHTML'    => $result
            ));
        }
        wp_send_json(array(
            'msg' => __('FAILED, syndication filter file or class does not exist.','apollo'),
            'status'    => 'FALSE'
        ));
    }

    public function clear_top_ten() {
        require_once APOLLO_INCLUDES_DIR. '/apollo-statistic-system.php';
        $result = Apollo_Statistic_System::exportTopVisitEventForDomain(get_current_blog_id(), 'json', false, true);
        wp_send_json($result);
    }

    public function empty_cache_search_widget(){
        try{
            if (!empty($_POST['ifswids'])){
                $pathToHandlingCachesClass = APOLLO_ADMIN_DIR. '/tools/cache/Inc/search-widget-cache-class.php';
                if(file_exists($pathToHandlingCachesClass) && is_file($pathToHandlingCachesClass)){
                    require_once($pathToHandlingCachesClass);
                }
                if(class_exists('APLC_Search_Widget_Cache_Class')){
                    $ifswids =  $_POST['ifswids'];
                    foreach( $ifswids as $id){
                        APLC_Search_Widget_Cache_Class::deleteCache(array('ifswid' => $id));
                    }
                }
            }
            wp_send_json(array(
                'msg'   => __('All IFrame with selected items were emptied completely !', 'apollo'),
                'success'   =>  'TRUE'
            ));
        }catch (Exception $ex){
            wp_send_json(array(
                'msg'   => $ex->getMessage(),
                'success'   =>  'FALSE'
            ));
        }
    }

    public function cache_network_site(){
        if (!empty($_POST['blog_ids'])){
            $blog_ids =  $_POST['blog_ids'];
            foreach( $blog_ids as $id){
                update_blog_option( $id, $_POST['key'], $_POST['value'] );
            }
        }
        exit;
    }
    public function spotlight_ordering() {
        $ids = isset( $_POST['ids'] ) ? $_POST['ids'] : '';

        if ( ! $ids || ! $ids_arr = explode( ',' , $ids ) ) exit;

        foreach ( $ids_arr as $k => $id ) {

            if ( ! $id ) continue;

            wp_update_post( array(
                'menu_order' => $k,
                'ID'    => $id,
            ) );
        }
        exit;
    }

    public function empty_spotlight_cache() {
        $cacheFileClass = aplc_instance('APLC_Inc_Files_HomeSpotlight');
        $filename = $cacheFileClass->filename;
        $cacheManagementFilePath = APOLLO_INCLUDES_DIR. '/admin/tools/cache/Inc/apollo-sites-caching-management.php';
        if(file_exists($cacheManagementFilePath)){
            require_once $cacheManagementFilePath;
            if(class_exists('APLC_Site_Caching_Management')){
                $currentSiteID = get_current_blog_id();
                $cleanCachedSite = new APLC_Site_Caching_Management($currentSiteID);
                $cleanCachedSite->emptyCachedFile($filename);
            }
        }
    }

    public function save_custom_event_options(){

        $eventOptsData  = isset($_POST[Apollo_SESSION::SAVE_EVENT_OPTIONS]) && !empty($_POST[Apollo_SESSION::SAVE_EVENT_OPTIONS]) ? $_POST[Apollo_SESSION::SAVE_EVENT_OPTIONS] : array();
        if( empty($eventOptsData) ) {
            echo Apollo_App::responseHandler(false, array(
                'message' => __("Have some errors, please try again","apollo")
            ));
        } else {
            require_once APOLLO_TEMPLATES_DIR. '/pages/lib/class-apollo-page-module.php';
            $eventOptsData = urldecode($eventOptsData);
            $eventOptsData = Apollo_Page_Module::apolloGetAllQueryStringToArray($eventOptsData);
            $_SESSION[Apollo_SESSION::SAVE_EVENT_OPTIONS] = $eventOptsData;
            $_SESSION[Apollo_SESSION::FLAG_FOR_SAVING_EVENT_OPTIONS] = 1;
            echo Apollo_App::responseHandler(true);
        }
        exit;
    }

    public function add_to_laureate_award() {
        $ids = isset( $_GET['ids'] ) ? $_GET['ids'] : '';
        $year = isset($_GET['year']) ? $_GET['year'] : '';
        $type = isset($_GET['type']) ? $_GET['type'] : 'artist';

        $arr_id = explode( ',' , $ids );

        if ( ! $ids || ! $arr_id  || !$year) {
            wp_send_json(array(
                    'code' => 0,
                    'msg' => __('Some condition is missing!')
                ));
        }

        global $wpdb;
        $wpdb->escape_by_ref($type);
        $sql = "delete from {$wpdb->{Apollo_Tables::_APL_TIMELINE}} where year_award = '%s' and type = '%s'";
        $wpdb->query($wpdb->prepare($sql, array(
                    (int)$year,
                    $type,
                )));

        // build data for insert
        $arr_param_for_query = array();
        $value_for_query = array();
        foreach($arr_id as $index => $id ) {
            $arr_param_for_query[] = $id;
            $arr_param_for_query[] = $year;
            $arr_param_for_query[] = $type;

            $value_for_query[]= '(%s, %s, %s)';
        }

        $sql = "insert into {$wpdb->{Apollo_Tables::_APL_TIMELINE}} (p_id, year_award, type )" . ' VALUES ' . implode(", ", $value_for_query);

        $result = $wpdb->query($wpdb->prepare($sql, $arr_param_for_query));

        if($result !== false && $result !== null) {
            wp_send_json(array(
                    'code' => 1,
                    'msg' => __('Save info successfully'),
                ));
        }
        else {
            $msg = 'Something wrong happen! Please check your database!';

            if(isset($wpdb->last_error)) {
                $msg = $wpdb->last_error;
            }
            wp_send_json(array(
                    'code' => 0,
                    'msg' => $msg,
                ));
        }
    }

    public function remove_upload_pdf() {
        $postid = isset( $_GET['postid'] ) ? $_GET['postid'] : '';
        $attid = isset($_GET['attid']) ? (int)$_GET['attid'] : '';

        if(empty($postid) || empty($attid)) {
            wp_send_json(array(
                'error'=> true,
                'data' => array(
                    'msg' => 'Not information for delete mission.'
                )
            ));
        }

        $field = isset($_GET['field']) && !empty($_GET['field']) ? sanitize_text_field($_GET['field']) : 'upload_pdf';

        $post = get_post($postid);
        $post_type = $post->post_type;
        if ( $post_type == Apollo_DB_Schema::_PROGRAM_PT ) {
            $field .= '_related_materials';
        }

        $old_attachment_ids = maybe_unserialize(get_apollo_meta($postid, $field, true));
        $old_attachment_ids = !empty($old_attachment_ids) ? $old_attachment_ids : array();
        $old_attachment_ids = array_map('intval', $old_attachment_ids);

        if(array_search($attid, $old_attachment_ids, true) === false ) {
            wp_send_json(array(
                'error'=> true,
                'data' => array(
                    'msg' => 'Something wrong data not found!'
                )
            ));

        }

        unset($old_attachment_ids[array_search($attid, $old_attachment_ids, true)]);
        update_apollo_meta($postid, $field, array_values($old_attachment_ids));

        wp_send_json(array(
            'error'=> false,
            'data' => array(
                'msg' => 'Delete successfully!',
            )
        ));

    }

    /**
	 * Ajax request handling for categories ordering
	 */
	public function term_ordering() {
		global $wpdb;

		$id = (int) $_POST['id'];
		$next_id  = isset($_POST['nextid']) && (int) $_POST['nextid'] ? (int) $_POST['nextid'] : null;
		$taxonomy = isset($_POST['thetaxonomy']) ? esc_attr( $_POST['thetaxonomy'] ) : null;
		$term = get_term_by('id', $id, $taxonomy);

		if ( !$id || !$term || !$taxonomy ) die(0);

		apollo_reorder_terms( $term, $next_id, $taxonomy );

		$children = get_terms($taxonomy, "child_of=$id&menu_order=ASC&hide_empty=0");

		if ( $term && sizeof($children) ) {
			echo 'children';
			die;
		}
	}

    public function re_order_custom_fields() {

        if ( ! isset( $_REQUEST['ids'] )) return FALSE;
        $ids = explode( ',', $_REQUEST['ids']);
        if ($ids) {
            $apl_query = new Apl_Query( Apollo_Tables::_APL_CUSTOM_FIELD );
            foreach ( $ids as $k => $id ) {
                $apl_query->update( array('cf_order' => $k + 1 ), array( 'id' => $id ));
            }
        }
    }

    public function send_test_email() {
        $action = Apollo_App::clean_data_request($_GET['id']);
        $email  = Apollo_App::clean_data_request($_GET['email']);
        $class = 'Apollo_Email_'. Apollo_App::processClassAction($action);
        $instance = new $class();
        $result = $instance->sendTest($email);
        wp_send_json(array(
            'success'=> $result,
        ));
    }

    public function preview_template() {
        $_SESSION['apl_preview_email_content'] = 'tmp_email_template'.session_id(). '.html';
        file_put_contents(Apollo_App::getUploadBaseInfo( 'html_dir' ). '/'. $_SESSION['apl_preview_email_content'], stripslashes($_POST['content']));
        exit;
    }

    /**
     * Keep the real password when add new user
     * so in the filter send notification email we will use it
     */
    public function keep_add_new_user_password() {
        $_SESSION['apl_new_user_pass'] = $_POST['password'];
        exit;
    }

    public function filter_city_zip_by_states() {
        global $wpdb;
        $_terrData = of_get_option(Apollo_DB_Schema::_TERR_DATA);
        $arr_states = json_decode(Apollo_App::clean_data_request($_GET['_data']));
        $result='';
        if($arr_states){
            $inlist = "'" .implode("','", $arr_states). "'";

            $privateStates = Apollo_App::get_network_manage_states_cities();
            $tbl = ($privateStates ? $wpdb->prefix : $wpdb->base_prefix). Apollo_Tables::_APL_STATE_ZIP;
            $querystr = "
                SELECT cs.code as zipcode, cityTbl.name as city, cs.id as zipId, stateTbl.code as state_prefix, stateTbl.name as state_name
                FROM $tbl cs
                INNER JOIN $tbl cityTbl ON cityTbl.id = cs.parent AND cityTbl.type = 'city'
                INNER JOIN $tbl stateTbl ON stateTbl.id = cityTbl.parent AND stateTbl.type = 'state' AND stateTbl.code IN ($inlist)
                WHERE cs.type = 'zip'
                ORDER BY stateTbl.name ASC, cityTbl.name ASC
             ";

            $scz = $wpdb->get_results($querystr, OBJECT);

            if($scz){
                $seenCity = array();
                ob_start();
                include APOLLO_ADMIN_DIR.'/theme-options/default-template/territory/list-cities-zips.php';
                $result = ob_get_contents();
                ob_get_clean();
            }
        }
        wp_send_json(array(
            'success'=> $result,
        ));
    }

    public function get_states() {

        global $wpdb;
        $table = $wpdb->{Apollo_Tables::_APL_STATE_ZIP};

        $mod = isset($_GET['mod']) ? $_GET['mod']: '';
        $relativeId = isset($_GET['relativeId']) ? $_GET['relativeId'] : '';

        if ( $mod == 'city' && $relativeId ) {
            $where = "type='city' AND parent = $relativeId";
        } else if ($mod == 'zip' && $relativeId) {
            $where = "type='zip' AND parent = $relativeId";
        } else {
            $where = "type='state'";
        }

        $sql = "
            SELECT z.*, p.post_type, p.ID as pid, p.post_title, p.guid FROM $table z
            LEFT JOIN $wpdb->posts as p ON p.ID = z.source
            WHERE $where ORDER BY name asc
        ";

//        $aplQuery = new Apl_Query(Apollo_Tables::_APL_STATE_ZIP);
//        $result = $aplQuery->get_where($where, '*', '', ' ORDER BY name asc ');
        wp_send_json(array(
            'result'=> $wpdb->get_results($sql),
        ));
    }

    /**
     * Delete state, city or zip. Do not delete if:
     * - State has any city
     * - City has any zip code
     *
     * @ticket #11322
     * @return json
     */
    public function delete_state_city_zip()
    {
        $tbl  = Apollo_Tables::_APL_STATE_ZIP;
        $data = json_decode(file_get_contents("php://input"));
        if ( !isset($data->id) || ! $data->id ) {
            wp_send_json(array('msg' => __('Missing id', 'apollo'), 'status' => false));
        }

        // find current item by ID
        $aplQuery = new Apl_Query($tbl);
        $result   = $aplQuery->get_row("id = $data->id");
        if ( is_null($result) ) {
            wp_send_json(array('msg' => __('Item not found', 'apollo'), 'status' => false));
            exit;
        }

        // find item's child elements
        $childWhere = '';
        if ($result->type == 'state') {
            $childWhere = "parent = $data->id AND type = 'city'";
        } else if ($result->type == 'city') {
            $childWhere = "parent = $data->id AND type = 'zip'";
        }

        if ( !empty($childWhere) && $aplQuery->get_total($childWhere) > 0 ) {
            wp_send_json(array(
                'msg' => __('Could not delete because this item has child elements', 'apollo'),
                'status' => false
            ));
            exit;
        }

        $aplQuery->delete("id=$data->id");
        wp_send_json(array(
            'msg'    => __('Deleted successfully', 'apollo'),
            'result' => '',
            'status' => 1,
        ));
    }

    public function add_state_city_zip() {

        $tbl = Apollo_Tables::_APL_STATE_ZIP;

        $data = json_decode(file_get_contents("php://input"));

        $code = isset($data->code) ? $data->code : '';
        $name = isset($data->name) ? $data->name : '';
        $type = isset($data->type) ? $data->type : '';
        $parent = isset($data->parent) ? $data->parent : '';

        $id = isset($data->id) ? $data->id : '';

        if ( $type == 'zip' || $type == 'city' ) {
            $code = $name;
        }

        if ( $type == 'state' ) {
            if ( ! $code || ! $type ) {
                wp_send_json(array(
                    'msg' => "Missing code",
                    'status'    => false
                ));
                exit;
            }
        } else {
            if ( ! $name ) {
                wp_send_json(array(
                    'msg' => "Missing name",
                    'status'    => false
                ));
                exit;
            }
        }

        $aplQuery = new Apl_Query($tbl);

        $idWhere = $id ? " id != $id AND " : ' ';

        if ($type != 'state') {
            $idWhere .= $parent ? " parent = $parent AND " : '';
        }

        if ( $aplQuery->get_total("$idWhere code='$code' AND type='$type'") ) {
            wp_send_json(array(
                'msg' => "This item has already existed",
                'status'    => true,
                'result'    => array(),
                'status'    => 0
            ));
            exit;
        } else if ( $id ) {
            $aplQuery->update(array(
                'code'  => $code,
                'name'  => $name,
                'type'  => $type
            ), array('id' => $id));

            wp_send_json(array(
                'msg' => __('Updated successfully','apollo'),
                'status'    => true,
                'result'    => array(),
                'status'    => 1
            ));
        } else {
            $aplQuery->insert(array(
                'code'  => $code,
                'name'  => $name,
                'type'  => $type,
                'parent' => $parent
            ));

            $parentWhere = $type != 'state' ? " AND parent = $parent" :'';
            $where = "code='$code' AND type = '$type' $parentWhere";

            $getNew = $aplQuery->get_row($where);

            wp_send_json(array(
                'msg' => __('Inserted successfully','apollo'),
                'status'    => true,
                'result'    => array('id' => $getNew->id),
                'status'    => 1
            ));
        }


        wp_send_json(array(
            'msg' => "Success",
            'status'    => true,
            'result'    => array(),
            'status'    => 1
        ));

        exit;
    }

    public function clone_state_zip_v1(){

        global $wpdb;
        $oldTbl = $wpdb->base_prefix. Apollo_Tables::_APL_STATE_ZIP;
        $newTbl = $wpdb->prefix. Apollo_Tables::_APL_STATE_ZIP;

        $sql ="SELECT * FROM $newTbl ";
        $rows = $wpdb->get_results($sql);
        if(!$rows && $newTbl != $oldTbl) {
            $sql3 = "DROP TABLE IF EXISTS  $newTbl ";
            $wpdb->get_results($sql3);

            $sql2 = " CREATE TABLE $newTbl  AS
              SELECT id, code, name, type, parent
                FROM $oldTbl";
            $wpdb->get_results($sql2);

        }

    }
    public  function is_main_site(){
        global $blog_id;
        if ($blog_id == 1) {
            wp_send_json(array(
                'msg' => "Success",
                'status'    => true,
                'result'    => array()
            ));
        } else {
            wp_send_json(array(
                'msg' => "Fail",
                'status'    => false,
                'result'    => array()

            ));
        }
    }

    function get_trans_term_name() {
        $slug = $_REQUEST['slug'];

        global $sitepress;
        $languages = $sitepress ? $sitepress->get_active_languages() : false;
        $term = get_term_by('slug', $slug, 'event-type');
        if (! $term || !$languages) {
            wp_send_json(array(
                'msg' => "Fail",
                'status'    => false,
                'result'    => array()

            ));
        }

        $output = array();
        foreach ($languages as $lang => $v):
            $translated_term_id = wpml_object_id_filter($term->term_id, 'event-type', true, $lang);
            $translated_term_object = get_term_by('id', $translated_term_id, 'event-type');
            $output[$lang] = $translated_term_object ? $translated_term_object->name : $term->name;
        endforeach;

        wp_send_json(array(
            'msg' => __('OK', 'apollo'),
            'status'    => true,
            'result'    => $output

        ));
    }

    public function show_more_syndication_individual_events(){

        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/syndication/class-apollo-meta-box-syndication-filter.php';
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/class-apollo-admin-syndication-meta-boxes.php';

        if( ! class_exists('Apollo_Meta_Box_Syndication_Filter')
            || ! class_exists('Apollo_App')
            || ! class_exists('Apollo_Admin_Syndication_Meta_Boxes')
        ){
            wp_send_json(array(
                'msg' => __('FAILED', 'apollo'),
                'status'    => 'FALSE'
            ));
        }

        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $syndicationAccID = isset($_POST['account_id']) ? intval($_POST['account_id']) : 0;

        $getMoreEvents = Apollo_Meta_Box_Syndication_Filter::getIndividualEvents($page);
        $indEventsHTML = Apollo_App::getTemplatePartCustom(APOLLO_ADMIN_DIR .'/post-types/meta-boxes/syndication/templates/individual-event-part.php',array(
            'individualEvents' => $getMoreEvents,
            'accountID' => $syndicationAccID

        ));

        wp_send_json(array(
            'msg' => __('SUCCESS', 'apollo'),
            'status'    => 'TRUE',
            'resultHTML'    => $indEventsHTML
        ));
    }

    public function show_more_event_theme_individual_events(){
        global $wpdb;
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $selectedEvents = isset($_POST['selectedEvent']) ? $_POST['selectedEvent'] : '';
        $selectedEventsArr = !empty($selectedEvents) ? explode(',',$selectedEvents) : array();
        $orderBy = isset($_POST['orderBy']) ? $_POST['orderBy'] : 'date';
        $sortDefault = isset($_POST['sortDefault']) ? $_POST['sortDefault'] : 'ASC';

        $sqlEvents = Apollo_App::getSQLIndividualEvents(false,$page,Apollo_DB_Schema::_APL_MAX_EVENT_THEMES_PER_PAGE, $orderBy, $sortDefault);
        $getMoreEvents = $wpdb->get_results($sqlEvents);
        $indEventsHTML = Apollo_App::getTemplatePartCustom(APOLLO_ADMIN_DIR.'/_templates/et-individual-event-tpl.php',array(
            'individualEvents' => $getMoreEvents,
            'selectedEvents' => $selectedEventsArr
        ));

        wp_send_json(array(
            'msg' => __('SUCCESS', 'apollo'),
            'status'    => 'TRUE',
            'resultHTML'    => $indEventsHTML
        ));
    }

    /**
     * Get more individual organization via AJAX in Admin Event tool
     *
     * @ticket #11493
     * @return json
     */
    public function show_more_event_theme_individual_orgs()
    {
        global $wpdb;
        $page            = isset($_POST['page'])         ? intval($_POST['page'])      : 1;
        $selectedOrgs    = isset($_POST['selectedItem']) ? $_POST['selectedItem']      : '';
        $selectedOrgsArr = !empty($selectedOrgs)         ? explode(',', $selectedOrgs) : array();

        $getMoreOrgs = $wpdb->get_results(Apollo_App::getSQLIndividualOrgs($page, Apollo_DB_Schema::_APL_MAX_ORG_PER_PAGE));
        $indOrgsHTML = Apollo_App::getTemplatePartCustom(APOLLO_ADMIN_DIR . '/_templates/et-individual-org-tpl.php', array(
            'individualOrgs' => $getMoreOrgs,
            'selectedOrgs'   => $selectedOrgsArr
        ));

        wp_send_json(array(
            'msg'        => __('SUCCESS', 'apollo'),
            'status'     => 'TRUE',
            'resultHTML' => $indOrgsHTML
        ));
    }

    /**
     * Get more individual venue via AJAX in Admin Event tool
     *
     * @ticket #11493
     * @return json
     */
    public function show_more_event_theme_individual_venues()
    {
        global $wpdb;
        $page              = isset($_POST['page'])         ? intval($_POST['page'])       : 1;
        $selectedVenues    = isset($_POST['selectedItem']) ? $_POST['selectedItem']       : '';
        $selectedVenuesArr = !empty($selectedVenues)       ? explode(',',$selectedVenues) : array();

        $getMoreVenues = $wpdb->get_results(Apollo_App::getSQLIndividualVenues($page, Apollo_DB_Schema::_APL_MAX_VENUE_PER_PAGE));
        $indVenuesHTML = Apollo_App::getTemplatePartCustom(APOLLO_ADMIN_DIR.'/_templates/et-individual-venue-tpl.php',array(
            'individualVenues' => $getMoreVenues,
            'selectedVenues'   => $selectedVenuesArr
        ));

        wp_send_json(array(
            'msg'        => __('SUCCESS', 'apollo'),
            'status'     => 'TRUE',
            'resultHTML' => $indVenuesHTML
        ));
    }

    public function show_more_venue_iframe_search_widget(){
        global $wpdb;
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $selectedVenues = isset($_POST['selectedVenues']) ? $_POST['selectedVenues'] : '';
        $selectedVenuesArr = !empty($selectedVenues) ? explode(',',$selectedVenues) : array();
        $venues = Apollo_App::getListPostItemWithPagination( array(
            'post_type' => Apollo_DB_Schema::_VENUE_PT,
            'posts_per_page' => Apollo_DB_Schema::_APL_MAX_VENUE_PER_PAGE,
            'page' => $page
        ) );
        $resultsData = array();
        if ($venues) {
            foreach ($venues as $v ){
                $resultsData[$v->ID] = $v->post_title;
            }
        }
        $indVenuesHTML ='';
        $index = ($page -1 ) * Apollo_DB_Schema::_APL_MAX_VENUE_PER_PAGE ;
        $metaVemue = APL_Iframe_Search_Widget_Const::META_VENUE ;

        foreach ( $resultsData as $k_choice => $choice ){
            $checked =  $selectedVenuesArr && in_array($k_choice, $selectedVenuesArr) ? 'checked' : '' ;
            $indVenuesHTML .= '<p class="form-field '.$metaVemue.'-'.$index.'_field ">
                                <input type="checkbox" '.$checked.'   class="checkbox " name="meta_venues[]" id="'.$metaVemue.'-'.$index.'" value="'.$k_choice.'">
                                    <label for="'.$metaVemue.'-'.$index.'">'.$choice.'</label>
                                 </p>';
            $index += 1;
        }
        wp_send_json(array(
            'msg' => __('SUCCESS', 'apollo'),
            'status'    => 'TRUE',
            'resultHTML'    => $indVenuesHTML
        ));
    }

    /** @Ticket - #13856 */
    public function neighborhood_get_data() {
        if (isset($_GET['iDisplayStart'])) {
            if (isset($_GET['iSortCol_0']) && isset($_GET['sSortDir_0'])) {
                if ($_GET['iSortCol_0'] == 0) {
                    $order_column = 'n.name';
                } else {
                    $order_column = 'c.code';
                }
                $sort_order = $_GET['sSortDir_0'];
            } else {
                $order_column = 'n.id';
                $sort_order = 'DESC';
            }
            $result = APL_Lib_Territory_Neighborhood::getDataToDatatables(array(
                'search' => $_GET['sSearch'],
                'order_column' => $order_column,
                'sort_order' => $sort_order,
                'limit' => $_GET['iDisplayStart'],
                'offset' => $_GET['iDisplayLength'],
                'parent_id' => isset($_GET['parent_id'])? $_GET['parent_id'] : '0'
            ));
            wp_send_json(array(
                'recordsTotal' => $result['total'],
                'recordsFiltered' => $result['total'],
                'data' => $result['data']
            ));
            exit;
        } else {
            wp_send_json(array(
                'recordsTotal' => 0,
                'recordsFiltered' => 0,
                'data' => array()
            ));
            exit;
        }
    }

    /**
     * @Ticket #13856
     * Add or Update Neighborhood
     */
    public function add_neighborhood() {
        if (!empty($_POST['neighborhood_data']['name']) && !empty($_POST['neighborhood_data']['city'])) {
            $result = APL_Lib_Territory_Neighborhood::save(array(
                'id' => $_POST['neighborhood_data']['id'],
                'name' => $_POST['neighborhood_data']['name'],
                'parent_id' => $_POST['neighborhood_data']['city']
            ));
            wp_send_json($result);
            exit;
        } else {
            wp_send_json(array(
                'status' => false,
                'message' => __( 'Data not empty!', 'apollo')
            ));
            exit;
        }
    }

    /**
     * @Ticket #13856
     * Delete Neighborhood
     */
    public function remove_neighborhood() {
        if (!empty($_POST['neighborhood_id'])) {
            $result = APL_Lib_Territory_Neighborhood::delete((intval($_POST['neighborhood_id'])));
            wp_send_json(array(
                'status' => $result,
                'message' => $result ? __( 'Success!', 'apollo' ) : __( 'Failed', 'apollo' )
            ));
            exit;
        } else {
            wp_send_json(array(
                'status' => false,
                'message' => __( 'ID not found!', 'apollo')
            ));
            exit;
        }
    }

    public function admin_get_neighborhood_by_city() {
        $optionRender = APL_Lib_Territory_Neighborhood::renderNeighborhoodOptions($_POST['parent_code'], $_POST['state'], false);
        wp_send_json(array(
            'status' => true,
            'data' => $optionRender
        ));
        exit;
    }

    /**
     * @author TriLM
     * Solr sync data
     */
    public function sync_solr_data(){
        if($_POST){

            try {
                $solrSearchModule = new Apollo_Solr_Search();
                $value = $solrSearchModule->insertPost();
            }
            catch(Exception $e) {
                echo $e;
            }

            if($value){
                wp_send_json(array(
                    'status' => '1',
                    'message' => __('Sync data success','apollo')
                ));
                exit;
            }
            wp_send_json(array(
                'status' => '0',
                'message' => __('Sync data fail','apollo')
            ));
            exit;
        }
    }

    /**
     * @author TriLM
     * Solr delete data
     */
    public function delete_sync_solr_data(){
        if($_POST){
            $solrSearchModule = new Apollo_Solr_Search();
            $value = $solrSearchModule->delete();
            if($value){
                wp_send_json(array(
                    'status' => '1',
                    'message' => __('Delete data success','apollo')
                ));
                exit;
            }
            wp_send_json(array(
                'status' => '0',
                'message' => __('Delete data fail','apollo')
            ));
            exit;
        }
    }

    /**
     * @author TriLM
     * check old config wit
     */
    public function compare_solr_old_new_config(){
        $solrConfigVal = isset($_POST['val'])?$_POST['val']:'';
        if($solrConfigVal != ''){
            $solrSearch = new Apollo_Solr_Search();
            $check = $solrSearch->compareConfig($solrConfigVal);
            if($check == true ){
                wp_send_json(array(
                    'status' => 1,
                ));
                exit;
            }
            wp_send_json(array(
                'status' => 0,
            ));
            exit;
        }
        wp_send_json(array(
            'status' => 0,
        ));
        exit;

    }
    function create_wp_editor(){
        $quicktags_settings = array( 'buttons' => 'strong,em,link,block,del,code,close,more' );
        $settings = array('wpautop' => true,'tinymce'       => array(
            'toolbar1'      => 'bold,italic,underline,separator,alignleft,aligncenter,alignright,separator,link,unlink,undo,redo',
            'toolbar2'      => 'strikethrough,hr, forecolor, pastetext, removeformat, charmap, outdent,indent',
        ), 'media_buttons' => false, 'quicktags' => $quicktags_settings, 'textarea_rows' => '5', 'textarea_name' => 'icon_desc[]');
        echo '<label for="icon_desc">'._e( 'Description', 'apollo' ) . '</label>';
        wp_editor('', $_GET['id'], $settings);
        exit;
    }
    function show_more_posts_type() {

        $page = isset($_GET['page']) ?$_GET['page']  : 1;
        $postType = isset($_GET['post_type']) ?$_GET['post_type']  : '';
        $selected = isset($_GET['selected']) ?$_GET['selected']  : '';
        $metaName = isset($_GET['meta_name']) ?$_GET['meta_name']  : '';

        if (!$postType) {
            wp_send_json(array(
                'data'  => false,
                'hasMore'   => false,
            ));
            exit;
        }

        $orgListing = apl_instance('APL_Lib_Helpers_PostTypeListing', array(
            'postType' => $postType,
            'page'  => $page,
        ));
        $page++;

        $orgListing->setLimit(100);

        wp_send_json(array(
            'html'  => $orgListing->renderMultipleCheckbox(explode(',', $selected), $metaName),
            'action'    => admin_url(sprintf('admin-ajax.php?action=apollo_show_more_posts_type&page=%s&selected=%s&post_type=%s&meta_name=%s', $page, $selected, $postType, $metaName)),
            'hasMore'   => $orgListing->hasMore(),
        ));
        exit;
    }
    public  static  function get_token()
    {
        $length = isset($_POST['key_length']) ?$_POST['key_length']  : 40;
        $appId = isset($_POST['appId']) ?$_POST['appId'] : '';
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet) - 1;
        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[Apollo_App::apl_crypto_rand_secure(0, $max)];
        }

        if(!$token){
            wp_send_json(array(
                'status' => '0',
                'message' => __('Get API TOken key fail','apollo'),
                'data' => $token,
            ));
            exit;
        } else {
            wp_send_json(array(
                'status' => '1',
                'message' => __('Get API Token key success','apollo'),
                'data'   => $token.'_'. $appId
            ));
            exit;
        }

    }

    /**
     * @author Trilm
     * refresh_public_art_map cache
     */
    public static function  refresh_public_art_map(){
        $time = date('Y-m-d h:i:s', time() );
        update_option (Apollo_DB_Schema::_PUBLIC_ART_MAP_REFRESH_TIME,$time);
        wp_send_json(array(
            'refreshed_time' => $time
        ));
    }
}

new Apollo_Admin_Ajax();
