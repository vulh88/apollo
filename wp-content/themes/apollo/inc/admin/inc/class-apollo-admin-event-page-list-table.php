<?php
/**
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'WP_List_Table' ) ) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

if (!class_exists('Apollo_Admin_Event_Page_List_Table')) {
    class Apollo_Admin_Event_Page_List_Table extends WP_List_Table{

        static $hookSubMenuPage = '';
        static $page = '';
        static $postType = '';
        static $taxonomySlug = '';

        /** Class constructor */
        public function __construct($args = array()) {
            self::$hookSubMenuPage = isset($args['hookSubMenuPage']) ? $args['hookSubMenuPage'] : '';
            self::$page = isset($args['page']) ? $args['page'] : '';
            self::$postType = isset($args['postType']) ? $args['postType'] : '';
            global $apollo_modules;
            self::$taxonomySlug = $apollo_modules[self::$postType]['taxonomy_slug'];
            parent::__construct( array(
                'singular' => __( 'Test', 'apollo' ),
                'plural'   => __( 'Test', 'apollo' ),
                'ajax'     => false,
                'screen'   => self::$hookSubMenuPage
            ) );
            add_action( 'admin_head', array( &$this, 'admin_header' ) );
        }

        /**
         * Associative array of columns
         * @return array
         */
        function get_columns(){
            $columns = array(
                'name'    => __( 'Name' , 'apollo'),
                'leave_as_normal' => 'Leave as normal',
            );
            return $columns;
        }

        function prepare_items() {
            $per_page = $this->get_items_per_page('event_per_page',Apollo_Event_Import_File::_APOLLO_IEF_NUM_NUM_PER_PAGE);
            $current_page = $this->get_pagenum();
            $total_items  = $this->record_count();

            $this->set_pagination_args(array(
                'total_items' => $total_items, //WE have to calculate the total number of items
                'per_page' => $per_page //WE have to determine how many items to show on a page
            ));

            $this->items = $this->getEventTheme($per_page, $current_page);
        }

        /**
         * Returns the count of records in the database.
         * @return null|string
         */
        private function record_count() {
            global $wpdb;
            $sql = self::prepareSQL(true);
            $result = $wpdb->get_results($sql,ARRAY_A);
            return intval($result[0]['total']);
        }

        public function prepareSQL($countTotal = false){
            $this->process_bulk_action();
            global $wpdb;
            $apolloThemeTbl = $wpdb->{Apollo_Tables::_APL_THEME_TOOL};
            $termTbl = $wpdb->terms;
            $sql = "SELECT * ";
            if($countTotal){
                $sql = "SELECT COUNT(*) as total ";
            }
            $sql .= "
                FROM {$termTbl} t INNER JOIN {$apolloThemeTbl} th ON t.term_id = th.cat_id
            ";

            // filter query event import file if have

            $sql .= self::generateSearchSQL();
            $sql .= self::generateOrderBySQL();
            return $sql;
        }

        public static function generateSearchSQL(){
            global $wpdb;
            $sql = " WHERE 1=1 ";
            if(isset($_GET['s']) && !empty($_GET['s'])){
                $sql .= " AND t.name LIKE '%" . ($_GET['s']) . "%'";
            }

            return $sql;

        }

        public static function generateOrderBySQL(){
            $sql = " ORDER BY t.name";

            if(isset($_GET['orderby']) && !empty($_GET['orderby'])){
                $orderBy = $_GET['orderby'];
            }

            if(isset($_GET['order']) && !empty($_GET['order'])){
                $order = $_GET['order'];
            } else {
                $order = 'ASC';
            }

            if(!empty($orderBy)){
                if($_GET['orderby'] == 'leave_as_normal'){
                    $sql = " ORDER BY th." . $orderBy . " " . $order;
                }else{
                    $sql = " ORDER BY t." . $orderBy . " " . $order;
                }
            }
            return $sql;
        }

        public function getEventTheme($per_page = 10, $page_number = 1){
            global $wpdb;
            $sql = self::prepareSQL();
            $sql .= " LIMIT " . $per_page;
            $sql .= ' OFFSET ' . ($page_number - 1) * $per_page;
            $result = $wpdb->get_results($sql,ARRAY_A);
            return $this->formatEventTheme($result);
        }

        public function formatEventTheme($eventTheme = array()){
            if(empty($eventTheme)){
                return array();
            }
            $result = array();
            foreach ($eventTheme as $t){
                $result[] = array(
                    'name'    => $t['name'],
                    'leave_as_normal' => $t['leave_as_normal'],
                    'view_action' => $t['slug'],
                    'remove_action' => $t['id']
                );
            }
            return $result;
        }

        public function getCategoriesSelected(){
            global $wpdb;
            $apolloThemeTbl = $wpdb->{Apollo_Tables::_APL_THEME_TOOL};
            $termTbl = $wpdb->terms;
            $sql = " SELECT * FROM $termTbl t INNER JOIN $apolloThemeTbl th ON t.term_id = th.cat_id ";
            $terms = $wpdb->get_results($sql);
            // get list terms
            $ts = array();
            foreach( $terms as $t ) {
                $_ts = get_term($t->cat_id, 'event-type');
                $ts[] = $_ts->slug;
            }
            if(!empty($ts)){
                return implode(',', $ts);
            }else{
                return '';
            }
        }

        /**
         * Render a column when no column specific method exist.
         * @param array $item
         * @param string $column_name
         * @return mixed
         * $value_hidden: this value content in hidden field, use it for edit field
         * $column_value: this value is display in list product
         */
        public function column_default($item, $column_name) {
            return isset($item[$column_name]) ? $item[$column_name] : '';
        }

        public function column_name($item){
            $linkEdit = $this->getPermalink() . '&add_new=1&id='. $item['remove_action'];
            $linkRemove = $this->getPermalink($item['remove_action'], true, $_GET);
            $linkView = home_url(). '/' . self::$taxonomySlug . '/' . $item['view_action'];
            $linkViewTheme = sprintf('<a href="%s" target="_blank">'.$item['name'].'</a>', $linkEdit);
            $actions = array(
                'id' => 'ID: ' . $item['remove_action'],
                'edit' => sprintf('<a href="%s" target="_blank">'.__('Edit','apollo').'</a>', $linkEdit),
                'view' => sprintf('<a href="%s" target="_blank">'.__('View','apollo').'</a>', $linkView),
                'remove' => sprintf('<a href="%s">'.__('Remove','apollo').'</a>', $linkRemove),
            );
            return $linkViewTheme . $this->row_actions($actions);
        }

        public function column_leave_as_normal($item){
            return $item['leave_as_normal'] == 0 ? __('No', 'apollo') : __('Yes', 'apollo');
        }

        function admin_header() {
            $page = ( isset($_GET['page'] ) ) ? esc_attr( $_GET['page'] ) : false;
            if( self::$page != $page )
                return;
            echo '<style type="text/css">';
            echo '.wp-list-table .column-leave_as_normal { width: 150px;}';
            echo '</style>';
        }

        /**
         * Columns to make sortable.
         * @return array
         */
        public function get_sortable_columns() {
            $sortable_columns = array(
                'name' => array('name', false),
                'leave_as_normal' => array('leave_as_normal', false)
            );
            return $sortable_columns;
        }

        public function process_bulk_action() {
            // security check!
            if ( isset( $_POST['_wpnonce'] ) && ! empty( $_POST['_wpnonce'] ) ) {

                $nonce  = filter_input( INPUT_POST, '_wpnonce', FILTER_SANITIZE_STRING );
                $action = 'bulk-' . $this->_args['plural'];

                if ( ! wp_verify_nonce( $nonce, $action ) )
                    wp_die( 'Nope! Security check failed!' );

            }

            $action = $this->current_action();

            switch ( $action ) {
                case 'delete':
                    $bulkDeleteIds = isset($_POST['bulk-delete']) && !empty($_POST['bulk-delete']) ? $_POST['bulk-delete'] : array();
                    if(!empty($bulkDeleteIds)){
                        $this->removeTheme($bulkDeleteIds);
                        wp_redirect(admin_url(self::getRedirectCurrentPageUrl()));
                    }
                    break;
                default:
                    break;
            }
            return;
        }

        public function no_items() {
            _e('No Theme available.', 'apollo');
        }

        public static function getRedirectCurrentPageUrl($query_string_array = array()){
            $base_url = sprintf( "edit.php?post_type=%s&page=%s",
                esc_attr( self::$postType),
                esc_attr( self::$page)
            );
            if(!empty($query_string_array)){
                $array_item_query_string = array();
                foreach($query_string_array as $key => $value){
                    if($key != 'post_type' && $key != 'page' && $key !='id' && $key != 'isDel'){
                        $array_item_query_string[] = strtolower(str_replace(' ','_',$key)) . '=' . $value;
                    }
                }
                $base_url .= '&' . implode('&', $array_item_query_string);
            }
            return $base_url;
        }

        public function getPermalink($id="", $isDel = false, $query_string_array = array()) {
            $link = admin_url().'edit.php?post_type='.self::$postType.'&page=event_themes';
            if(!empty($query_string_array)){
                $array_item_query_string = array();
                foreach($query_string_array as $key => $value){
                    if($key != 'post_type' && $key != 'page' && $key !='id' && $key != 'isDel'){
                        $array_item_query_string[] = strtolower(str_replace(' ','_',$key)) . '=' . $value;
                    }
                }
                if(!empty($array_item_query_string)){
                    $link .= '&' . implode('&', $array_item_query_string);
                }
            }
            if ($isDel && $id) return $link .= '&id='. $id.'&isDel=1';

            if ($id) return $link .= '&add_new=1&id='. $id;

            return $link;
        }

        public function removeTheme($ids = array()){
            if(!empty($ids)){
                $aplQuery = new Apl_Query(Apollo_Tables::_APL_THEME_TOOL);
                $aplQuery->delete('id in('.join(",", $ids).')');
            }
        }

        public function addTheme() {
            global $wpdb;
            $id = $term = false;

            if ( isset($_POST['submit']) ) {
                $data = (object) $_POST;

                $ttOrder = isset($_POST['tt_order']) ? $_POST['tt_order'] : '';
                $topDesc = isset($_POST['top_desc']) ? Apollo_App::removeCDATATag(stripslashes($_POST['top_desc'])) : '';
                $botDesc = isset($_POST['bottom_desc']) ? Apollo_App::removeCDATATag(stripslashes($_POST['bottom_desc'])) : '';
                $catSlug = isset($_POST['event-type']) ? $_POST['event-type'] : '';
                $leaveAsNormal = isset($_POST['leave_as_normal']) ? $_POST['leave_as_normal'] : '';
                $entityType = isset($_POST['entity_type']) ? $_POST['entity_type'] : '';
                $locationType = isset($_POST['location_type']) ? $_POST['location_type'] : '';
                $locationData = isset($_POST['location_data']) ? $_POST['location_data'] : '';

                // START @ticket ##11493: store associated orgs & venues
                // get selected orgs
                $associatedOrgs  = isset($_POST['final_selected_orgs']) ? $_POST['final_selected_orgs'] : '';
                $associatedOrgs  = !empty($associatedOrgs) ? explode(',', $associatedOrgs) : '';

                // get selected venues
                $associatedVenues = isset($_POST['final_selected_venues']) ? $_POST['final_selected_venues'] : '';
                $associatedVenues = !empty($associatedVenues) ? explode(',', $associatedVenues) : '';

                // get org & venue description
                $associatedOrgDesciption   = isset($_POST['associated_orgs_desc'])   ? Apollo_App::removeCDATATag(stripslashes($_POST['associated_orgs_desc']))   : '';
                $associatedVenueDesciption = isset($_POST['associated_venues_desc']) ? Apollo_App::removeCDATATag(stripslashes($_POST['associated_venues_desc'])) : '';
                // END @ticket ##11493

                $events = isset($_POST['final_selected_events']) ? $_POST['final_selected_events'] : '';
                $events = !empty($events) ? explode(',',$events) : '';

                $wpdb->escape_by_ref($ttOrder);
                $wpdb->escape_by_ref($catSlug);
                // Vandd @ticket #12036
//                $wpdb->escape_by_ref($topDesc);
//                $wpdb->escape_by_ref($botDesc);
//                $wpdb->escape_by_ref($associatedOrgDesciption);
//                $wpdb->escape_by_ref($associatedVenueDesciption);

                $wpdb->escape_by_ref($entityType);

                $catData = get_term_by( 'slug', $catSlug, 'event-type');

                $dataPost = array(
                    'tt_order'      => $ttOrder,
                    'post_type'     => Apollo_DB_Schema::_EVENT_PT,
                    'top_desc'      => $topDesc,
                    'bottom_desc'   => $botDesc,
                    'cat_id'        => $catData ? $catData->term_id : 0,
                    'entity_type'   => $entityType,
                    'location_type' => $locationType,
                    'location_data' => maybe_serialize($locationData),
                    'events'        => maybe_serialize($events),
                    'leave_as_normal'   => $leaveAsNormal,
                );

                // @ticket #11493: store Associated Orgs & Associated Venues
                if ( !Apollo_App::checkReactivation() ) {
                    $dataPost = array_merge($dataPost, array(
                        'associated_orgs'        => maybe_serialize($associatedOrgs),
                        'associated_venues'      => maybe_serialize($associatedVenues),
                        'associated_orgs_desc'   => $associatedOrgDesciption,
                        'associated_venues_desc' => $associatedVenueDesciption
                    ));
                }

                $id = isset($_POST['id']) ? $_POST['id'] : '';
                $aplQuery = new Apl_Query( Apollo_Tables::_APL_THEME_TOOL );
                if ($id) {
                    $aplQuery->update($dataPost, array('id' => $id));
                } else {
                    $aplQuery->insert($dataPost);
                    $id = $aplQuery->get_bottom('id');
                }
                /* Thienld: new instance to handle caching logic for event category page */
                do_action('apl_event_cache_empty_all_cache');

                wp_safe_redirect( $this->getPermalink($id) );
            } else {
                if (isset($_GET['id']) && $_GET['id']) {
                    $id = $_GET['id'];
                }

                if( $id ) {
                    $aplQuery = new Apl_Query(Apollo_Tables::_APL_THEME_TOOL);
                    $data = $aplQuery->get_row("id=$id");
                    $term = get_term($data->cat_id, 'event-type');
                    $catSlug =  $term->slug;
                } else {
                    $catSlug = isset($_GET['slug']) ? $_GET['slug'] : '';
                }
            }

            // Get events
            $sqlEvents = Apollo_App::getSQLIndividualEvents(false,1,Apollo_DB_Schema::_APL_MAX_EVENT_THEMES_PER_PAGE);
            $sqlEventsTotal = Apollo_App::getSQLIndividualEvents(true);
            $eventPosts = $wpdb->get_results($sqlEvents);
            $eventTotal = $wpdb->get_results($sqlEventsTotal);
            $eventTotalValue = !empty($eventTotal) ? intval($eventTotal[0]->total) : 0;
            $terrData = of_get_option(Apollo_DB_Schema::_TERR_DATA);

            global $apollo_modules;

            $leaveAsNormal = isset($data->leave_as_normal) ? $data->leave_as_normal : 0;

            $viewLink = '';
            if ($catSlug) {
                $event_m = $apollo_modules[self::$postType];
                $viewLink = '<a target="_blank" href="'.home_url().'/'.$event_m['taxonomy_slug'].'/'.$catSlug.'">('. __('View', 'apollo').')</a>';
            }

            ?>
            <div class="wrap">
                <h2><?php echo isset($_GET['id']) && $_GET['id'] ? _e('Edit Theme', 'apollo') : _e('Add New Theme', 'apollo'); ?> <?php echo $viewLink ?></h2>
                <form method="post" action="" id="event-theme-tool-frm">
                    <p class="submit">
                        <input type="submit" name="submit" class="button button-primary" value="<?php _e('Save Theme', 'apollo') ?>">
                    </p>
                    <table class="form-table">
                        <tbody>
                        <input type="hidden" name="id" value="<?php echo $id; ?>" />
                        <tr class="form-field form-required">
                            <th scope="row"><label for="name"><?php _e('Category', 'apollo') ?></label></th>
                            <td>
                                <?php apollo_dropdown_categories('event-type', array('hide_empty' => 0), '', 1,1,'', $catSlug); ?>
                            </td>
                        </tr>


                        <tr class="form-field form-required">
                            <th scope="row"><label for="name"><?php _e('Leave as normal', 'apollo') ?></label></th>
                            <td>
                                <input type="checkbox" name="leave_as_normal" value="1" <?php echo $leaveAsNormal == 1 ? 'checked' : '' ?> />
                            </td>
                        </tr>

                        <tr class="form-field <?php echo $leaveAsNormal ? 'hidden' : '' ?>">
                            <th scope="row"><label for="slug"><?php _e('Display Order', 'apollo') ?></label></th>
                            <td>
                                <?php $ttOrder = isset($data->tt_order) ? $data->tt_order : '' ?>
                                <select name="tt_order">
                                    <option <?php echo $ttOrder == 'END_DATE_ASC' ? 'selected' : '' ?> value="END_DATE_ASC"><?php _e('End Date Ascending', 'apollo') ?></option>
                                    <option <?php echo $ttOrder == 'ALPHABETICAL_ASC' ? 'selected' : '' ?> value="ALPHABETICAL_ASC"><?php _e('Alphabetic Ascending', 'apollo') ?></option>
                                    <option <?php echo $ttOrder == 'START_DATE_ASC' ? 'selected' : '' ?> value="START_DATE_ASC"><?php _e('Start Date Ascending', 'apollo') ?></option>
                                </select>
                            </td>
                        </tr>

                        <tr class="form-field">
                            <th scope="row"><label for="top_desc"><?php _e('Top Description', 'apollo') ?></label></th>
                            <td>
                                <?php wp_editor( isset($data->top_desc) ? Apollo_App::convertContentEditorToHtml($data->top_desc, TRUE) : '', 'top_desc', array(
                                    'textarea_name' => 'top_desc',
                                    'media_buttons' => true,
                                    'wpautop'       => true,
                                    'editor_height' => 200,
                                    'teeny' => true,
                                ) ); ?>
                            </td>
                        </tr>

                        <tr class="form-field">
                            <th scope="row"><label for="bottom_desc"><?php _e('Bottom Description', 'apollo') ?></label></th>
                            <td>
                                <?php wp_editor( isset($data->bottom_desc) ? Apollo_App::convertContentEditorToHtml($data->bottom_desc, TRUE) : '', 'bottom_desc', array(
                                    'textarea_name' => 'bottom_desc',
                                    'media_buttons' => true,
                                    'wpautop'       => true,
                                    'editor_height' => 200,
                                    'teeny' => true,
                                ) ); ?>
                            </td>
                        </tr>

                        <tr class="form-field <?php echo $leaveAsNormal ? 'hidden' : '' ?>">
                            <th scope="row"><label for=""><?php _e('Entity Type', 'apollo') ?></label></th>
                            <td>
                                <?php $_selectedEntityType = isset($data->entity_type) ? $data->entity_type : '' ?>
                                <select name="entity_type">
                                    <option <?php echo $_selectedEntityType == 'ALL' ? 'selected' : '' ?> value="ALL"><?php _e('All Events', 'apollo') ?></option>
                                    <option <?php echo $_selectedEntityType == 'IND' ? 'selected' : '' ?> value="IND"><?php _e('Individual', 'apollo') ?></option>
                                </select>
                            </td>
                        </tr>

                        <tr class="form-field <?php echo $leaveAsNormal ? 'hidden' : '' ?>">
                            <th scope="row"><label for=""><?php _e('Events', 'apollo') ?></label></th>
                            <td>
                                <?php
                                $_selectedEvents = false;
                                if ( isset($data->events) ) {
                                    $_selectedEvents = !is_array($data->events) ? maybe_unserialize($data->events) : $data->events;
                                }
                                ?>
                                <input type="button" name="btn-select-all" data-refer='input[name="events[]"]' value="<?php _e('Select All', 'apollo') ?>" class="button-secondary left" style="margin-right: 10px;" />
                                <input type="button" name="btn-un-select-all" data-refer='input[name="events[]"]' value="<?php _e('Un-select All', 'apollo') ?>" class="button-secondary left" style="margin-right: 10px;" />
                                <input type="button" name="btn-sort-event-by-title" data-sort-type='title' value="<?php _e('Filter by Title', 'apollo') ?>" class="button-secondary left" class="button-secondary left" style="margin-right: 10px;" />
                                <input type="button" name="btn-sort-event-by-date" data-sort-type='date' value="<?php _e('Filter by End Date', 'apollo') ?>" class="button-secondary left" />
                                <br/><br/>
                                <div id="individual-events" class="apl-list-panel c1">
                                    <div>
                                        <input type="hidden" name="final_selected_events" id="final_selected_events" value="<?php echo !empty($_selectedEvents) ? implode(',',$_selectedEvents) : ''; ?>" />
                                        <div class="remove-items">
                                            <?php
                                            if ( $eventPosts ):
                                                foreach( $eventPosts as $event ):
                                                    $event = get_event($event);
                                                    $org = $event->event_org_links();
                                                    ?>
                                                    <label class="event-item">
                                                        <input <?php echo $_selectedEvents && in_array($event->post->ID, $_selectedEvents) ? 'checked="checked"' : '' ?> value="<?php echo $event->post->ID ?>" type="checkbox" name="events[]" class="ind-event-selection" />
                                                        <?php echo $event->get_title(true) ?> <i style="font-size: 12px">( <?php _e('Presented By: ') ?><?php echo $org ?> - <?php echo $event->render_sch_date() ?> )</i>
                                                        <a style="float: right" href="<?php echo get_edit_post_link($event->post->ID) ?>"><?php _e('Edit', 'apollo') ?></a>
                                                    </label><br>
                                                <?php endforeach; endif; ?>
                                            <div class="wrap-lm-et options_group">
                                                <?php if($eventTotalValue > count($eventPosts)) : ?>
                                                    <a style="float:left;margin-top: 10px; margin-left: 5px;" data-total-events="<?php echo $eventTotalValue; ?>" data-selected-events="<?php echo !empty($_selectedEvents) ? implode(',',$_selectedEvents) : ''; ?>" class="et-sm-individual-events button-primary" href="javascript:void(0);" ><?php _e("Show More Events","apollo"); ?></a>
                                                    <span class="spinner" style="float: left; margin-top: 10px; visibility: visible;"></span>
                                                <?php else : ?>
                                                    <input type="hidden" data-total-events="<?php echo $eventTotalValue; ?>" data-selected-events="<?php echo !empty($_selectedEvents) ? implode(',',$_selectedEvents) : ''; ?>" class="et-sm-individual-events" />
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </td>
                        </tr>

                        <?php
                        $locationType = isset($data->location_type) ? $data->location_type : '';
                        $locationData = isset($data->location_data) ? maybe_unserialize($data->location_data) : '';

                        ?>
                        <tr class="form-field <?php echo $leaveAsNormal ? 'force-hidden' : '' ?>">
                            <th scope="row"><label for="bottom-description"><?php _e('City or Zip', 'apollo') ?></label></th>
                            <td>

                                <fieldset class="apl-list-panel c1">
                                    <legend><input <?php echo $locationType == 1 ? 'checked' : '' ?> type="radio" name="location_type" value="1" /><?php _e('City', 'apollo') ?></legend>
                                    <div>
                                        <?php
                                        $zips = array();
                                        $allCities = array();
                                        if ( $terrData ):
                                            ksort($terrData);
                                            foreach( $terrData as $state => $cities ):
                                                if ( ! $cities ) continue;
                                                foreach($cities as $city => $_zips):
                                                    if (! $_zips) continue;
                                                    $zips = array_merge($zips, array_keys($_zips));
                                                    $cities[$city] = $state;
                                                endforeach;
                                                ksort($cities);
                                                $allCities = array_merge($allCities, $cities);
                                            endforeach;
                                        endif;

                                        if(!empty($allCities)):
                                            foreach($allCities as $city => $state):
                                                ?>
                                                <label>
                                                    <input
                                                        <?php echo !$locationType ? 'disabled' : '';
                                                        echo $locationType == 1 && $locationData && in_array($city, $locationData) ? 'checked' : '' ?>
                                                        value="<?php echo $city ?>" type="checkbox" class="city" name="location_data[]">
                                                    <?php echo $city ?> (<?php echo $state ?>)
                                                </label><br>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                </fieldset>

                                <fieldset class="apl-list-panel c2">
                                    <legend><input <?php echo $locationType == 0 ? 'checked' : '' ?> type="radio" name="location_type" value="0" /><?php _e('Zip', 'apollo') ?></legend>
                                    <div>
                                        <?php

                                        if ( $zips ):
                                            $zips = Apollo_App::sortOrderForZipCodes($zips);
                                            foreach( $zips as $zip ):
                                                ?>
                                                <label>
                                                    <input <?php
                                                    echo $locationType ? 'disabled' : '';
                                                    echo ! $locationType && $locationData && in_array($zip, $locationData) ? 'checked' : '' ?> value="<?php echo $zip ?>" class="zip" type="checkbox" name="location_data[]">
                                                    <?php echo $zip ?>
                                                </label><br>
                                            <?php endforeach; endif; ?>
                                    </div>
                                </fieldset>
                            </td>
                        </tr>

                        <!-- @ticket #11493: include associated orgs -->
                        <?php require_once APOLLO_ADMIN_DIR . '/inc/partial/theme-tool/_associated-orgs.php'; ?>

                        <!-- @ticket #11493: include associated venues -->
                        <?php require_once APOLLO_ADMIN_DIR . '/inc/partial/theme-tool/_associated-venues.php'; ?>

                        </tbody>
                    </table>
                    <p class="submit">
                        <input type="submit" name="submit" class="button button-primary" value="<?php _e('Save Theme', 'apollo') ?>">
                    </p>
                </form>
            </div>

            <?php


        }

    }
}