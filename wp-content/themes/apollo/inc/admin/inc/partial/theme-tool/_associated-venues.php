<?php

/**
 * @ticket #11493: [CF] 20170310 - [Event Theme page] Add a new Venues and ORGs listing
 */

// Get venues
$venues     = $wpdb->get_results(Apollo_App::getSQLIndividualVenues(1, Apollo_DB_Schema::_APL_MAX_VENUE_PER_PAGE));
$venueTotal = $wpdb->get_var("SELECT FOUND_ROWS()");

// Get editor data
$editorData = isset($data->associated_venues_desc) ? Apollo_App::convertContentEditorToHtml($data->associated_venues_desc, true) : '';

// check leave as normal
$leaveAsNormal = isset($data->leave_as_normal) ? $data->leave_as_normal : 0;

// Get selected venues
$_selectedVenues = false;
if ( isset($data->associated_venues) ) {
    $_selectedVenues = !is_array($data->associated_venues) ? maybe_unserialize($data->associated_venues) : $data->associated_venues;
}

?>

<tr class="form-field <?php echo $leaveAsNormal ? 'hidden' : '' ?>">
    <th scope="row">
        <label for=""><?php _e('Associated Venues', 'apollo') ?></label>
    </th>

    <td>
        <div>
            <!-- Load WYSIWYG editor -->
            <?php
            wp_editor($editorData, 'associated_venues_desc', array(
                'textarea_name' => 'associated_venues_desc',
                'media_buttons' => true,
                'wpautop'       => true,
                'editor_height' => 200,
                'teeny'         => true,
            ) ); ?>
        </div>
        <span class="clearfix"></span>
        <br/>

        <input type="button" name="btn-select-all" data-refer='input[name="associated_venues[]"]' value="<?php _e('Select All', 'apollo') ?>" class="button-secondary left" style="margin-right: 10px;" />
        <input type="button" name="btn-un-select-all" data-refer='input[name="associated_venues[]"]' value="<?php _e('Un-select All', 'apollo') ?>" class="button-secondary left" style="margin-right: 10px;" />
        <br/><br/>
        <div id="individual-venues" class="apl-list-panel c1">
            <div>
                <input type="hidden" name="final_selected_venues" id="final_selected_venues"
                       value="<?php echo !empty($_selectedVenues) ? implode(',',$_selectedVenues) : ''; ?>" />
                <div class="remove-items">
                    <?php
                    if ( $venues ):
                        foreach( $venues as $venue ):
                            $venue = get_venue($venue);
                            ?>
                            <label>
                                <input type="checkbox" name="associated_venues[]" class="ind-event-selection"
                                       value="<?php echo $venue->post->ID ?>"
                                       <?php echo $_selectedVenues && in_array($venue->post->ID, $_selectedVenues) ? 'checked="checked"' : '' ?>  />
                                <?php
                                    $title = $venue->get_title(true);
                                    echo ( $title == false || empty($title) ) ? __('No title', 'apollo') : $title;
                                ?>
                                <a style="float: right" href="<?php echo get_edit_post_link($venue->post->ID) ?>"><?php _e('Edit', 'apollo') ?></a>
                            </label>
                            <br>
                        <?php endforeach; endif; ?>
                    <div class="wrap-lm-et options_group">
                        <?php if($venueTotal > count($venues)) : ?>
                            <a style="float:left;margin-top: 10px; margin-left: 5px;"
                               data-total-venues="<?php echo $venueTotal; ?>"
                               data-selected-venues="<?php echo !empty($_selectedVenues) ? implode(',', $_selectedVenues) : ''; ?>"
                               class="et-sm-individual-venues button-primary"
                               href="javascript:void(0);" >
                                <?php _e("Show More Venues","apollo"); ?>
                            </a>
                            <span class="spinner" style="float: left; margin-top: 10px; visibility: visible;"></span>
                        <?php else : ?>
                            <input type="hidden"
                                   data-total-venues="<?php echo $venueTotal; ?>"
                                   data-selected-venues="<?php echo !empty($_selectedVenues) ? implode(',', $_selectedVenues) : ''; ?>"
                                   class="et-sm-individual-venues" />
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </td>
</tr>
