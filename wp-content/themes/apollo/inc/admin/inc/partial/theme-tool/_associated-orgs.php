<?php
/**
 * @ticket #11493: [CF] 20170310 - [Event Theme page] Add a new Venues and ORGs listing
 */

// Get orgs
$orgs     = $wpdb->get_results(Apollo_App::getSQLIndividualOrgs(1, Apollo_DB_Schema::_APL_MAX_ORG_PER_PAGE));
$orgTotal = $wpdb->get_var("SELECT FOUND_ROWS()");

// Get editor data
$editorData = isset($data->associated_orgs_desc) ? Apollo_App::convertContentEditorToHtml($data->associated_orgs_desc, true) : '';

// check leave as normal
$leaveAsNormal = isset($data->leave_as_normal) ? $data->leave_as_normal : 0;

// Get selected orgs
$_selectedOrgs = false;
if ( isset($data->associated_orgs) ) {
    $_selectedOrgs = !is_array($data->associated_orgs) ? maybe_unserialize($data->associated_orgs) : $data->associated_orgs;
}

?>

<tr class="form-field <?php echo $leaveAsNormal ? 'hidden' : '' ?>">
    <th scope="row">
        <label for=""><?php _e('Associated Organizations', 'apollo') ?></label>
    </th>

    <td>
        <!-- Load WYSIWYG editor -->
        <div>
            <?php
            wp_editor($editorData, 'associated_orgs_desc', array(
                'textarea_name' => 'associated_orgs_desc',
                'media_buttons' => true,
                'wpautop'       => true,
                'editor_height' => 200,
                'teeny'         => true,
            ) ); ?>
        </div>
        <span class="clearfix"></span>
        <br/>

        <input type="button" name="btn-select-all" data-refer='input[name="associated_orgs[]"]' value="<?php _e('Select All', 'apollo') ?>" class="button-secondary left" style="margin-right: 10px;" />
        <input type="button" name="btn-un-select-all" data-refer='input[name="associated_orgs[]"]' value="<?php _e('Un-select All', 'apollo') ?>" class="button-secondary left" style="margin-right: 10px;" />
        <br/><br/>
        <div id="individual-orgs" class="apl-list-panel c1">
            <div>
                <input type="hidden" name="final_selected_orgs" id="final_selected_orgs"
                    value="<?php echo !empty($_selectedOrgs) ? implode(',', $_selectedOrgs) : ''; ?>" />
                <div class="remove-items">
                    <?php
                    if ( $orgs ):
                        foreach( $orgs as $org ):
                            $org = get_org($org);
                            ?>
                            <label>
                                <input type="checkbox" name="associated_orgs[]" class="ind-event-selection"
                                    value="<?php echo $org->post->ID ?>"
                                    <?php echo $_selectedOrgs && in_array($org->post->ID, $_selectedOrgs) ? 'checked="checked"' : '' ?> />
                                    <?php
                                        $title = $org->get_title(true);
                                        echo ( $title == false || empty($title) ) ? __('No title', 'apollo') : $title;
                                    ?>
                                <a style="float: right" href="<?php echo get_edit_post_link($org->post->ID) ?>"><?php _e('Edit', 'apollo') ?></a>
                            </label>
                            <br>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <div class="wrap-lm-et options_group">
                        <?php if($orgTotal > count($orgs)) : ?>
                            <a style="float:left;margin-top: 10px; margin-left: 5px;"
                               data-total-orgs="<?php echo $orgTotal; ?>"
                               data-selected-orgs="<?php echo !empty($_selectedOrgs) ? implode(',', $_selectedOrgs) : ''; ?>"
                               class="et-sm-individual-orgs button-primary"
                               href="javascript:void(0);" >
                                <?php _e("Show More Organizations","apollo"); ?>
                            </a>
                            <span class="spinner" style="float: left; margin-top: 10px; visibility: visible;"></span>
                        <?php else : ?>
                            <input type="hidden"
                                   data-total-orgs="<?php echo $orgTotal; ?>"
                                   data-selected-orgs="<?php echo !empty($_selectedOrgs) ? implode(',', $_selectedOrgs) : ''; ?>"
                                   class="et-sm-individual-orgs" />
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </td>
</tr>
