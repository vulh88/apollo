<?php
/**
 * Admin functions for the events post type
 *
 * @author 		vulh
 * @category 	admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'WP_List_Table' ) ) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

if ( ! class_exists( 'Apollo_Admin_Import_Event_File' ) ) :

    class Apollo_Admin_Import_Event_File extends WP_List_Table {

        static $hookSubMenuPage = '';
        static $page = '';
        static $postType = '';

        /** Class constructor */
        public function __construct($args = array()) {
            self::$hookSubMenuPage = isset($args['hookSubMenuPage']) ? $args['hookSubMenuPage'] : '';
            self::$page = isset($args['page']) ? $args['page'] : '';
            self::$postType = isset($args['postType']) ? $args['postType'] : '';
            parent::__construct( array(
                'singular' => __( 'Event Import File', 'apollo' ),
                'plural'   => __( 'Event Import Files', 'apollo' ),
                'ajax'     => false,
                'screen'   => self::$hookSubMenuPage
            ) );
        }

        public function no_items() {
            _e('No Event Import Files available.', 'apollo');
        }
        /**
         * Retrieve customers data from the database
         * @param int $per_page
         * @param int $page_number
         * @return mixed
         */
        public function getEventImportFile( $per_page = 10, $page_number = 1) {
            global $wpdb;
            $sql = self::prepareSQL();
            $sql .= " LIMIT " . $per_page;
            $sql .= ' OFFSET ' . ($page_number - 1) * $per_page;
            $result = $wpdb->get_results($sql,ARRAY_A);
            return $this->formatEventImportFile($result);
        }

        public function formatEventImportFile($eventImportedFiles = array()){
            if(empty($eventImportedFiles)) return array();
            $result = array();
            foreach($eventImportedFiles as $eif){
                $result[] = array(
                    Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_ID => $eif[Apollo_Event_Import_File::_APOLLO_IEF_DB_COLUMN_ID],
                    Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_FILENAME => $eif[Apollo_Event_Import_File::_APOLLO_IEF_DB_COLUMN_FILENAME],
                    Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_USERNAME => self::getDisplayUserName($eif[Apollo_Event_Import_File::_APOLLO_IEF_DB_COLUMN_USER_ID]),
                    Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_IMPORTED_DATE => self::getDisplayImportDate($eif[Apollo_Event_Import_File::_APOLLO_IEF_DB_COLUMN_IMPORTED_DATE]),
                    Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_IS_IMPORTED => self::getDisplayImportedStatus($eif[Apollo_Event_Import_File::_APOLLO_IEF_DB_COLUMN_IS_IMPORTED]),
                    Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_USER_ID => $eif[Apollo_Event_Import_File::_APOLLO_IEF_DB_COLUMN_USER_ID]
                );
            }
            return $result;
        }

        static function getDisplayFileName($filename){
            $baseFileName = basename($filename);
            $eplFileName = explode('-',$baseFileName);
            $rsFileName = $baseFileName;
            if(!empty($eplFileName) && is_array($eplFileName)){
                $timestamp = trim($eplFileName[0]);
                $rsFileName = date(Apollo_Display_Config::_APL_DATETIME_FORMAT_PATTERN_FOR_FILENAME,$timestamp);
                $eplFileName[0] = $rsFileName;
                $rsFileName = implode(' - ',$eplFileName);
            }
            return $rsFileName;
        }

        static function getDisplayUserName($user_id){
            $userData = get_userdata($user_id);
            return !empty($userData) && !empty($userData->data) ? $userData->data->user_login : '';
        }

        static function getDisplayImportDate($import_date){
            return $import_date;
        }

        static function getDisplayImportedStatus($import_status){
            return !$import_status ? "No" : "Yes";
        }

        public static function prepareSQL($countTotal = false){
            global $wpdb;
            $sql = "SELECT eif.* ";
            if($countTotal){
                $sql = "SELECT COUNT(*) as total ";
            }
            $sql .= "
                FROM {$wpdb->{Apollo_Tables::_APL_EVENT_IMPORT}} eif
            ";
            // filter query event import file if have
            $sql .= self::generateFilterSQL();
            $sql .= self::generateOrderBySQL();
            return $sql;
        }

        public static function generateFilterSQL(){
            global $wpdb;
            $sql = " WHERE 1=1 ";

            if(isset($_GET['user']) && !empty($_GET['user'])){
                $sql .= " AND eif.user_id = '" . intval($_GET['user']) . "'";
            }

            return $sql;

        }

        public static function generateOrderBySQL(){
            $sql = " ORDER BY eif.id";

            if(isset($_GET['orderby']) && !empty($_GET['orderby'])){
                $orderBy = $_GET['orderby'];
                switch ($orderBy) {
                    case Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_FILENAME:
                        $orderBy = Apollo_Event_Import_File::_APOLLO_IEF_DB_COLUMN_FILENAME;
                        break;
                    case Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_USERNAME:
                        $orderBy = Apollo_Event_Import_File::_APOLLO_IEF_DB_COLUMN_USER_ID;
                        break;
                    case Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_IMPORTED_DATE:
                        $orderBy = Apollo_Event_Import_File::_APOLLO_IEF_DB_COLUMN_IMPORTED_DATE;
                        break;
                    case Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_IS_IMPORTED:
                        $orderBy = Apollo_Event_Import_File::_APOLLO_IEF_DB_COLUMN_IS_IMPORTED;
                        break;
                    default:
                        $orderBy = '';
                        break;
                }
            }

            if(isset($_GET['order']) && !empty($_GET['order'])){
                $order = $_GET['order'];
            } else {
                $order = 'ASC';
            }

            if(!empty($orderBy)){
                $sql = " ORDER BY eif." . $orderBy . " " . $order;
            }
            return $sql;
        }

        /**
         * Handles data query and filter, sorting, and pagination.
         */
        public function prepare_items() {
            $this->process_bulk_action();
            $this->handleExtraActions();
            $per_page = $this->get_items_per_page('event_per_page',Apollo_Event_Import_File::_APOLLO_IEF_NUM_NUM_PER_PAGE);
            $current_page = $this->get_pagenum();
            $total_items  = $this->record_count();

            $this->set_pagination_args(array(
                'total_items' => $total_items, //WE have to calculate the total number of items
                'per_page' => $per_page //WE have to determine how many items to show on a page
            ));

            $this->items = $this->getEventImportFile($per_page, $current_page);
        }

        public function handleExtraActions(){
            if(isset($_GET['extra_action']) && !empty($_GET['extra_action'])){
                switch($_GET['extra_action']){
                    case 'delete':
                        if(isset($_GET['file_id']) && !empty($_GET['file_id'])){
                            $resultDel = $this->_deleteEventImportFileById(array($_GET['file_id']));
                            if($resultDel){
                                $_SESSION['action_completed'] = 'delete';
                                wp_redirect(admin_url(self::getRedirectCurrentPageUrl()));
                                exit();
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        /**
         * Returns the count of records in the database.
         * @return null|string
         */
        private function record_count() {
            global $wpdb;
            $sql = self::prepareSQL(true);
            $result = $wpdb->get_results($sql,ARRAY_A);
            return intval($result[0]['total']);
        }

        /**
         * Associative array of columns
         * @return array
         */
        public function get_columns($default_columns = array()) {
            $columns = array(
                'cb' => 'ID',
                Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_FILENAME => __('Filename','apollo'),
                Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_USERNAME => __('Username','apollo'),
                Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_IMPORTED_DATE => __('Created Date','apollo'),
                Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_IS_IMPORTED => __('Import Status','apollo'),
                Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_REMOVE_ACTION => __(' ','apollo'),
            );
            return $columns;
        }

        /**
         * Render a column when specific method exist.
         * @param array $item
         * @return mixed
         * $value_hidden: this value content in hidden field, use it for edit field
         * $column_value: this value is display in list product
         */
        public function column_cb($item) {
            $iefID = isset($item[Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_ID]) ? $item[Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_ID] : "";
            $cb = '<input type="checkbox" class="" name="bulk-delete[]" value="' . $iefID . '" />';
            return $cb;
        }

        public function column_filename($item){
            $upload_dir = wp_upload_dir();
            $rawFileName = $item[Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_FILENAME];
            $baseFileName = self::getDisplayFileName($rawFileName);
            $eventImportFilePath = $upload_dir['event_import_dir'] . '/' . $rawFileName;
            if(file_exists($eventImportFilePath) && is_file($eventImportFilePath)){
                $eventImportFileUrl = $upload_dir['event_import_url'] . '/' . $rawFileName;
            } else {
                $eventImportFileUrl = network_site_url('/404');
            }
            $actionDownloadFile = $eventImportFileUrl;
            $actions = array(
                'download'    => sprintf('<a href="%s" target="_blank">'.__('View File','apollo').'</a>',$actionDownloadFile),
            );
            $linkViewFile = sprintf('<a href="%s" target="_blank">'.$baseFileName.'</a>',$actionDownloadFile);
            $filename = $linkViewFile  . $this->row_actions($actions);
            return $filename;
        }

        public function column_username($item){
            $linkToUserProfile = get_edit_user_link($item[Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_USER_ID]);
            return sprintf('<a href="%s" target="_blank">'.$item[Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_USERNAME].'</a>',$linkToUserProfile);
        }

        public function column_remove_action($item){
            $removeLink = self::getRedirectCurrentPageUrl(array(
                'extra_action' => 'delete',
                'file_id' => $item[Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_ID]
            ));
            return sprintf('<a href="%s" class="remove-event-import-file">'.__('Remove','apollo').'</a>',$removeLink);
        }

        /**
         * Columns to make sortable.
         * @return array
         */
        public function get_sortable_columns() {
            $sortable_columns = array(
                Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_FILENAME => array(Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_FILENAME, false),
                Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_USERNAME => array(Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_USERNAME, false),
                Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_IMPORTED_DATE => array(Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_IMPORTED_DATE, false),
                Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_IS_IMPORTED => array(Apollo_Event_Import_File::_APOLLO_IEF_COLUMN_IS_IMPORTED, false),
            );
            return $sortable_columns;
        }

        /**
         * Render a column when no column specific method exist.
         * @param array $item
         * @param string $column_name
         * @return mixed
         * $value_hidden: this value content in hidden field, use it for edit field
         * $column_value: this value is display in list product
         */
        public function column_default($item, $column_name) {
            return isset($item[$column_name]) ? $item[$column_name] : '';
        }

        /**
         * Add extra markup in the toolbars before or after the list
         * @param string $which, helps you decide if you add the markup after (bottom) or before (top) the list
         */
        public function extra_tablenav( $which ) {
            if(!$this->has_items()) {
                $button_disabled = "disabled";
            } else {
                $button_disabled = "";
            }
            if ( $which == "top" ){
                ?>
                <div class="alignleft actions" id="alignleft-actions" style="">
                    <?php
                        wp_dropdown_users(array(
                                'show_option_all' => __('Filter by all users','apollo'),
                                'selected' => isset($_GET['user']) && !empty($_GET['user']) ? $_GET['user'] : ''
                            )
                        );
                    ?>
                    <input type="submit" name="filter-action" id="post-query-submit" class="button" value="Filter" />
                    <a href="<?php echo self::getRedirectCurrentPageUrl(); ?>" id="reset-event-import-file-list" class="button">Reset</a>
                </div>
            <?php
            }
            if ( $which == "bottom" ){
                echo "";
            }
        }

        public function get_bulk_actions() {

            return array(
                'delete' => __( 'Delete', 'apollo' )
            );

        }

        public function process_bulk_action() {
            // security check!
            if ( isset( $_POST['_wpnonce'] ) && ! empty( $_POST['_wpnonce'] ) ) {

                $nonce  = filter_input( INPUT_POST, '_wpnonce', FILTER_SANITIZE_STRING );
                $action = 'bulk-' . $this->_args['plural'];

                if ( ! wp_verify_nonce( $nonce, $action ) )
                    wp_die( 'Nope! Security check failed!' );

            }

            $action = $this->current_action();

            switch ( $action ) {

                case 'delete':
                    $bulkDeleteIds = isset($_GET['bulk-delete']) && !empty($_GET['bulk-delete']) ? $_GET['bulk-delete'] : array();
                    $resultDel = $this->_deleteEventImportFileById($bulkDeleteIds);
                    if($resultDel){
                        $_SESSION['action_completed'] = 'delete';
                        wp_redirect(admin_url(self::getRedirectCurrentPageUrl()));
                        exit();
                    }
                    break;
                default:
                    break;
            }
            return;
        }

        private function _deleteEventImportFileById($arrayEIFIDs = array()){
           try{
               global $wpdb;
               if(empty($arrayEIFIDs)) return false;
               $filenames = self::_getFilenameByFileIDs($arrayEIFIDs);
               if(!empty($filenames)){
                   foreach($filenames as $file){
                       self::deletePhysicalEventImportFile($file[Apollo_Event_Import_File::_APOLLO_IEF_DB_COLUMN_FILENAME]);
                   }
                   $wpdb->query( " DELETE FROM {$wpdb->{Apollo_Tables::_APL_EVENT_IMPORT}} WHERE id IN (".implode(',',$arrayEIFIDs).")" );
               }
           } catch (Exception $ex){
               return false;
           }
           return true;
        }

        private static function _getFilenameByFileIDs($fileIDs = array()){
            global $wpdb;
            if(empty($fileIDs)) return null;
            $sql = " SELECT eif.".Apollo_Event_Import_File::_APOLLO_IEF_DB_COLUMN_FILENAME."
                FROM {$wpdb->{Apollo_Tables::_APL_EVENT_IMPORT}} eif
                WHERE eif.id IN (".implode(',',$fileIDs).")
            ";
            $result = $wpdb->get_results($sql,ARRAY_A);
            return $result;
        }

        public static function getRedirectCurrentPageUrl($query_string_array = array()){
            $base_url = sprintf( "edit.php?post_type=%s&page=%s",
                esc_attr( self::$postType),
                esc_attr( self::$page)
            );
            if(!empty($query_string_array)){
                $array_item_query_string = array();
                foreach($query_string_array as $key => $value){
                    $array_item_query_string[] = strtolower(str_replace(' ','_',$key)) . '=' . $value;
                }
                $base_url .= '&' . implode('&', $array_item_query_string);
            }
            return $base_url;
        }

        public static function deletePhysicalEventImportFile($filename){
            try{
                $upload_dir = wp_upload_dir();
                $deleteFilePath = $upload_dir['event_import_dir'] . '/' . basename($filename);
                if(file_exists($deleteFilePath) && is_file($deleteFilePath)) {
                    @unlink($deleteFilePath);
                    return true;
                }
            }catch (Exception $ex){
                return false;
            }
        }

    }

endif;