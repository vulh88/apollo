<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Handles taxonomies in admin
 *
 * @class 		Apollo_Admin_Taxonomies
 * @package		inc/Admin
 * @category	Class
 * @author 		vulh
 */
class Apollo_Admin_Taxonomies {


    public function __construct() {

        // Process  term
        add_action( 'create_term', array ( $this, 'save_dis_sch_fields' ), 10, 3 );
        add_action( 'delete_term', array ( $this, 'delete_term' ), 5 );

        // Event term
        add_action( "create_term", array( $this, 'save_event_hidden' ), 5, 3 );
        add_action( 'created_term', array( $this, 'save_event_hidden' ), 10, 3 );
		add_action( 'edit_term', array( $this, 'save_event_hidden' ), 10, 3 );

        // form
        add_action( 'district-school_add_form_fields', array( $this, 'add_dis_sch_fields' ) );
        add_action( 'district-school_edit_form_fields', array( $this, 'edit_dis_sch_fields' ), 10, 2 );

        // Add bulk action to event tax
        add_action('admin_footer', array( $this, 'custom_bulk_admin_footer' ));

        // form
        add_action( 'event-type_add_form_fields', array( $this, 'add_event_fields' ) );
        add_action( 'event-type_edit_form_fields', array( $this, 'edit_event_fields' ), 10, 2 );

        // Add columns
		add_filter( 'manage_edit-event-type_columns', array( $this, 'event_type_columns' ) );
		add_filter( 'manage_event-type_custom_column', array( $this, 'event_type_column' ), 10, 3 );
        add_filter( 'manage_edit-event-type_sortable_columns', array( $this, 'custom_columns_sort' ) );

        // remove the html filtering for wp editor
        remove_filter( 'pre_term_description', 'wp_filter_kses' );
        remove_filter( 'term_description', 'wp_kses_data' );

        // Add display public checkbox field and description field to edit taxonomy form
        $arrTax = array('classified-type', 'artist-type', 'educator-type' , 'organization-type' ,
                            'program-type' , 'public-art-type' , 'venue-type', 'business-type');
        foreach( $arrTax as $taxonomyName){
            add_action( $taxonomyName . '_edit_form_fields',   array( $this,'edit_description_field' ));
            add_action( $taxonomyName . '_add_form_fields',   array( $this,'add_description_field' ));
            //change columns
            add_filter( 'manage_edit-'.$taxonomyName.'_columns', array( $this, 'taxonomy_columns' ) );
            //add custom columns
            add_filter( 'manage_'.$taxonomyName.'_custom_column', array( $this, 'taxonomy_column' ), 10, 3 );

            if ( $taxonomyName == 'artist-type'){
                add_filter( 'manage_edit-'.$taxonomyName.'_columns', array( $this, 'artist_type_columns' ) );
                //add custom columns
                add_filter( 'manage_'.$taxonomyName.'_custom_column', array( $this, 'artist_type_column' ), 10, 3 );
            }
        }

        /*@ticket #17234 0002398 - Recommendation Widgets - [Admin] Official tag for event and post*/
        $currentTheme = function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '';
        if ( $currentTheme == 'octave-child') {
            require_once APOLLO_ADMIN_DIR . '/taxonomy/event/class-apollo-admin-event-taxonomies.php';
            require_once APOLLO_ADMIN_DIR . '/taxonomy/post/class-apollo-admin-post-taxonomies.php';
            add_filter( 'bulk_actions-edit-event-tag', array($this,'apl_add_set_official_bulk_actions'), 10, 1 );
            add_filter( 'bulk_actions-edit-post_tag', array($this,'apl_add_set_official_bulk_actions'), 10, 1 );
        }


    }

    /**
     * @ticket #17234 0002398 - Recommendation Widgets - [Admin] Official tag for event and post
     * @param $bulk_actions
     * @return mixed
     */
    public function apl_add_set_official_bulk_actions($bulk_actions) {
        $bulk_actions['apl_set_official'] = __( 'Set official', 'apollo');
        return $bulk_actions;
    }

    /**
     * add WP editor description field  to  taxonomy form  instead of default text area field
     * @param $tag
     */
    public function edit_description_field($tag)
    {
        $displayPublic = get_apollo_term_meta( $tag->term_id, Apollo_DB_Schema::_APL_TAXONOMY_DISPLAY_PUBLIC_FIELD, true );?>
        <style type="text/css">
             .term-description-wrap {
                display: none !important;
            }
        </style>
        <script type="text/javascript">
            jQuery(function($) {
                $('WWtextarea#description').closest('tr.form-field').remove();
            });
        </script>
        <table class="form-table">
            <tr class="form-field">
                <th scope="row" valign="top">
                    <label for="description"><?php _ex('Description', 'apollo'); ?></label>
                </th>
                <td>
                    <?php
                    $settings = array('wpautop' => true, 'media_buttons' => true, 'quicktags' => true, 'textarea_rows' => '15', 'textarea_name' => 'description', "drag_drop_upload" => true);
                    wp_editor(html_entity_decode($tag->description), 'cat_description', $settings); ?>
                    <br/>
                    <span class="description"><?php _e('The description is not prominent by default; however, some themes may show it.','apollo'); ?></span>
                </td>
            </tr>
        </table>
        <table class="form-table">
            <tr class='form-field form-required term-name-wrap'>
                <th scope="row" valign="top"><label for='display-public'><?php echo __('Display to public', 'apollo') ?></label></th>
                <td>
                    <input id='display-public' <?php echo $displayPublic == 1 ? 'checked="true"' : ''; ?>  size='40' type='checkbox' name='display-public'/>
                    <br/>
                    <br/>
                    <span class="description"> <?php echo __("If it's checked the text in the description field will display to public", "apollo") ?></span>
                </td>
            </tr>
        </table>
        <?php if($tag->taxonomy == 'artist-type') {
            $displayFEFrom = get_apollo_term_meta( $tag->term_id, Apollo_DB_Schema::_APL_TAXONOMY_DO_NOT_DISPLAY_ON_FE, true );?>
        <table class="form-table">
            <tr class='form-field form-required term-name-wrap'>
                <th scope="row" valign="top"><label for='display-public'><?php echo __('Do not display on FE form', 'apollo') ?></label></th>
                <td>
                    <input id='display-public' <?php echo $displayFEFrom == 1 ? 'checked="true"' : ''; ?>  size='40' type='checkbox' name='display-FE'/>
                    <br/>
                    <br/>
                    <span class="description"> <?php echo __("If it is checked, this category will not display as an option in the FE artist profile form", "apollo") ?></span>
                </td>
            </tr>
        </table>
        <?php }
    }

    public function add_description_field($tag)
    { ?>
        <style type="text/css">
            .edit-tags-php form#addtag .term-description-wrap {
                display: none !important;
            }
        </style>
        <script type="text/javascript">
            jQuery(function($) {
                $('.edit-tags-php form#addtag textarea#tag-description').closest('div.form-field').remove();
            });
        </script>
        <div class="form-field">
            <label for="description"><?php _ex('Description', 'Apollo'); ?></label>
            <?php $settings = array('wpautop' => true, 'media_buttons' => true, 'quicktags' => true, 'textarea_rows' => '15', 'textarea_name' => 'description');
            wp_editor('', 'taxonomy_description', $settings); ?>
            <br/>
            <p class="description"><?php _e('The description is not prominent by default; however, some themes may show it.', 'apollo'); ?></p>
        </div>
        <div class="form-field">
            <span for='display-public'><?php echo __('Display to public', 'apollo') ?>&nbsp;&nbsp;</span>
            <input id='display-public' size='40' type='checkbox' name='display-public'/>
        </div>
        <?php if( $tag == 'artist-type') { ?>
            <div class="form-field">
                <span for='display-FE'><?php echo __('Do not display on FE form', 'apollo') ?>&nbsp;&nbsp;</span>
                <input id='display-FE' size='40' type='checkbox' name='display-FE'/>
            </div>
        <?php
        }
    }

    public function taxonomy_columns($existing_columns){
        if ( empty( $existing_columns ) && ! is_array( $existing_columns ) ) {
            $existing_columns = array();
        }

        unset($existing_columns['description']);
        $new_columns          = array();
        $new_columns['display_public'] = __( 'Display Public', 'apollo' );
        return array_merge( $existing_columns, $new_columns );
    }

    public function taxonomy_column( $columns, $column, $id ) {
        if ( $column === 'description' ) {
            return '';
        }

        if ( $column == 'display_public' ) {
            $displayPublic = get_apollo_term_meta( $id, Apollo_DB_Schema::_APL_TAXONOMY_DISPLAY_PUBLIC_FIELD, true );
            $columns .= $displayPublic ? __('Yes', 'apollo') : __('No', 'apollo');
        }

        return $columns;
    }

    public function artist_type_columns($existing_columns){
        if ( empty( $existing_columns ) && ! is_array( $existing_columns ) ) {
            $existing_columns = array();
        }
        $new_columns['display_FE'] = __( 'Do not display on FE from', 'apollo' );
        return array_merge( $existing_columns, $new_columns );
    }

    public function artist_type_column( $columns, $column, $id ) {

        if ( $column == 'display_FE' ) {
            $displayFE = get_apollo_term_meta( $id, Apollo_DB_Schema::_APL_TAXONOMY_DO_NOT_DISPLAY_ON_FE, true );
            $columns .= $displayFE ? __('Yes', 'apollo') : __('No', 'apollo');
        }

        return $columns;
    }
    /**
	 * Hidden column added to category admin.
	 *
	 * @access public
	 * @param mixed $columns
	 * @return array
	 */
	public function event_type_columns( $columns ) {
		$new_columns          = array();
		$new_columns['hidden_field'] = __( 'Hidden', 'apollo' );
        $new_columns['theme_tool'] = __( 'Theme Tool', 'apollo' );
        unset($columns['posts']);
        $new_columns['event_count'] = __('Count','apollo');


		return array_merge( $columns, $new_columns );
	}

	/**
	 * Hidden column value added to category admin.
	 *
	 * @access public
	 * @param mixed $columns
	 * @param mixed $column
	 * @param mixed $id
	 * @return array
	 */
	public function event_type_column( $columns, $column, $id ) {

        global $wpdb;
        $currentDate = current_time('Y-m-d');
        $meta_tbl  = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};


		if ( $column == 'hidden_field' ) {
			$hidden 	= get_apollo_term_meta( $id, Apollo_DB_Schema::_APL_EVENT_TAX_HIDE, true );

			$columns .= $hidden ? __('Yes', 'apollo') : __('No', 'apollo');
		}

        if ( $column == 'theme_tool' ) {
            $themeToolClass = new Apollo_Theme_Tool($id);
            $columns .= $themeToolClass->isThemeTool() ? __('Yes', 'apollo') : __('No', 'apollo');
        }

        //  filter event not expired
        if ( $column == 'event_count') {

            $termIds = array_merge( array($id), get_term_children( $id, 'event-type' ) );
            $termTaxes = $wpdb->get_col( "
					SELECT term_taxonomy_id
					FROM $wpdb->term_taxonomy
					WHERE taxonomy = 'event-type'
					AND term_id IN (".implode(',', $termIds).")
				" );

            $sqlString = " SELECT p.ID FROM $wpdb->posts p

            LEFT JOIN $wpdb->term_relationships tr ON (p.ID = tr.object_id)

            WHERE 1=1 AND ( tr.term_taxonomy_id IN (".implode(',', $termTaxes).") ) AND post_type = 'event'

            AND (
                p.post_status IN ('publish', 'future', 'pending', 'private') AND p.post_status <> 'draft'
            )

            GROUP BY ID";

            if (! $data = $wpdb->get_results($sqlString)) {
                $data = array();
            }

            $term = get_term( $id, 'event-type');
            $link = !empty($term)  ? admin_url('edit.php?event-type='.$term->slug.'&post_type=event&post_status=all&advanced=true&past_event=1') : '';
            $columns .= '<a href='.$link.'>'.count($data).'</a>';

        }

		return $columns;
	}

    public function custom_columns_sort( $columns ) {
        $custom = array(
            'event_count'     => 'posts',

        );
        return wp_parse_args( $custom, $columns );
    }

    function save_event_hidden($term_id, $tt_id, $taxonomy) {
        $a = $_POST;
        if ( $taxonomy == 'event-type' && !isset($_POST['_inline_edit']) ) { // In edit page
            $hidden = isset($_POST['hidden']) ? 1 : 0;
            update_apollo_term_meta($term_id, Apollo_DB_Schema::_APL_EVENT_TAX_HIDE, $hidden);

            $hidden_spotlight = isset($_POST['hidden_spotlight']) ? 1 : 0;
            update_apollo_term_meta($term_id, Apollo_DB_Schema::_APL_EVENT_TAX_HIDE_SPOT, $hidden_spotlight);

            // Only set data once the hidden checkbox is checked
            $exceptUserIds = $hidden && isset($_POST['hidden_except_user_ids']) ? $_POST['hidden_except_user_ids'] : '';
            update_apollo_term_meta($term_id, Apollo_DB_Schema::_APL_EVENT_TAX_HIDE_EXCEPT_USER_IDS, $exceptUserIds);
        }
        if ( $taxonomy != 'event-type' && $taxonomy != 'category' ) {
            $value = isset($_POST['display-public']) ? 1 : 0;
            update_apollo_term_meta($term_id, Apollo_DB_Schema::_APL_TAXONOMY_DISPLAY_PUBLIC_FIELD, $value);
        }
        if ( $taxonomy == 'artist-type' ) {
            $value = isset($_POST['display-FE']) ? 1 : 0;
            update_apollo_term_meta($term_id, Apollo_DB_Schema::_APL_TAXONOMY_DO_NOT_DISPLAY_ON_FE, $value);
        }


        /**
         * #14856 [CF] 20180122 - Performance issue - Caching category drop-down list of event search widget
         * @author vulh
        */
        $cacheFileClass = aplc_instance('APLC_Inc_Files_EventWidgetSearchCategory');
        $cacheFileClass->remove();
    }

    function delete_event_hidden($term_id, $tt_id, $taxonomy) {
        if ( $taxonomy == 'event-type' ) {
            global $wpdb;
            $apl_query = new Apl_Query($wpdb->prefix. Apollo_DB_Schema::_APOLLO_TERM_META);
            $apl_query->delete("apollo_term_id = $term_id");
            }
        }

    /**
     * Save event type field
     * @access public
     * @param mixed $term_id
     * @param mixed $tt_id
     * @param mixed $taxonomy
     */
    public function save_dis_sch_fields( $term_id, $tt_id, $taxonomy ) {

    }

    private function district_school_form_field( $is_school = false ) {
        $is_edit = isset( $_REQUEST['action'] ) && $_REQUEST['action'] == 'edit';
        $terms = get_terms( 'district-school', array( 'parent' => 0, 'hide_empty' => false ) );

        ?>
        <script>
            jQuery(function() {
                jQuery( '.taxonomy-district-school #col-left .col-wrap .form-wrap' ).prepend("\
                    <ul>\n\
                        <li <?php echo ! $is_school ? 'class=\"active\"' : '' ?> ><a href='<?php echo admin_url() ?>edit-tags.php?taxonomy=district-school&post_type=<?php echo Apollo_DB_Schema::_EDU_EVALUATION ?>'><?php _e( 'Districts', 'apollo' ) ?></a></li>\n\
                        <li <?php echo $is_school ? 'class=\"active\"' : '' ?>><a href='<?php echo admin_url() ?>edit-tags.php?school-page=true&taxonomy=district-school&post_type=<?php echo Apollo_DB_Schema::_EDU_EVALUATION ?>'><?php _e( 'Schools', 'apollo' ) ?></a></li>\n\
                    </ul>");

                <?php

                    if ( ! $is_school ) {
                        if ( $is_edit ) {
                        ?>
                            jQuery( 'tr.form-field #parent' ).parent().parent().hide();
                            jQuery( '.taxonomy-district-school .wrap h2' ).text( '<?php _e( 'Edit District', 'apollo' ) ?>' );
                        <?php } else { ?>
                            jQuery( '.form-wrap .form-field #parent' ).parent().hide();
                        <?php } ?>
                <?php
                    } else {

                ?>
                    <?php if ( $is_edit ) { ?>
                        jQuery( 'td #parent' ).parent().prev().children( 'label' ).text('<?php _e( 'District', 'apollo' ) ?>');
                        jQuery( '.taxonomy-district-school .wrap h2' ).text( '<?php _e( 'Edit School', 'apollo' ) ?>' );
                    <?php } else { ?>
                        jQuery( '.form-wrap .form-field #parent' ).prev().text('<?php _e( 'District', 'apollo' ) ?>');
                    <?php } ?>

                    jQuery( '#parent .level-1' ).hide();

                    <?php if ( $terms ): ?>
                    jQuery( '#parent option[value="-1"]' ).remove();
                    <?php endif; ?>
                <?php } ?>

            });
        </script>
        <?php
    }

    /**
     * Custom Event Org field
     * @access public
     * @return void
     */
    public function add_dis_sch_fields() {
        $this->district_school_form_field( isset( $_REQUEST['school-page'] ) );
    }

    /**
     * Edit event type fields
     * @access public
     * @param mixed $term
     * @param mixed $taxonomy
     *
     */
    public function edit_dis_sch_fields( $term, $taxonomy ) {
        $this->district_school_form_field( $term->parent );
    }

    /**
     * When a term is deleted, delete its meta
     *
     * @access public
     * @param mixed $term_id
     * @return vold
     */
    public function delete_term( $term_id ) {

        if ( ! ( int ) $term_id ) {
            return false;
        }

        global $wpdb;

        $wpdb->query( " DELETE FROM {$wpdb->apollo_termmeta} WHERE apollo_term_id = $term_id" );

    }

    function custom_bulk_admin_footer() {
        global $post_type;

        if ( $post_type != Apollo_DB_Schema::_EVENT_PT ) return;

        ?>
            <script>
                (function($) {
                    $(function() {
                        $('.taxonomy-event-type select[name="action"]').append('<option value="hide"><?php _e('Hidden', 'apollo') ?></option>');
                    });

                }) (jQuery);
            </script>
        <?php
    }

    /**
     * Custom Event Org field
     * @access public
     * @return void
     */
    public function add_event_fields() {
        ?>
		<div class="form-field">
            <span for="tag-slug"><?php _e( 'Hidden', 'apollo' ) ?>&nbsp;&nbsp;</span>
            <input name="hidden" id="hidden" type="checkbox" value="" />
        </div>
		<?php
    }

    /**
     * Edit event type fields
     * @access public
     * @param mixed $term
     * @param mixed $taxonomy
     *
     */
    public function edit_event_fields( $term, $taxonomy ) {
        $hidden = get_apollo_term_meta( $term->term_id, Apollo_DB_Schema::_APL_EVENT_TAX_HIDE, true );
        $hidden_spotlight = get_apollo_term_meta( $term->term_id, Apollo_DB_Schema::_APL_EVENT_TAX_HIDE_SPOT, true );
        $hiddenExceptUserIDs = get_apollo_term_meta( $term->term_id, Apollo_DB_Schema::_APL_EVENT_TAX_HIDE_EXCEPT_USER_IDS, true );

        ?>
        <tr class="form-field form-required">
			<th scope="row"><label for="name"><?php _e('Hidden', 'apollo') ?></label></th>
			<td><input name="hidden" id="apl-event-cat-hidden" type="checkbox" value="1" size="40" <?php echo $hidden == 1 ? 'checked' : '' ?> ></td>
		</tr>

        <tr class="form-field">
            <th scope="row"><label for="name"><?php _e('Hidden except for User ID', 'apollo') ?></label></th>
            <td>
                <input <?php echo !$hidden ? 'disabled' : '' ?> name="hidden_except_user_ids" id="hidden_except_user_ids" type="text" value="<?php echo $hiddenExceptUserIDs ?>" size="40"  >
                <i class="apl-full"><?php _e('Each user ID should be separated by a comma (UserID 1, UserID 2)', 'apollo') ?></i>
            </td>
        </tr>

        <tr class="form-field form-required">
            <th scope="row"><label for="name"><?php _e('Hide Category Spotlight', 'apollo') ?></label></th>
            <td><input name="hidden_spotlight" type="checkbox" value="1" size="40" <?php echo $hidden_spotlight == 1 ? 'checked' : '' ?> ></td>
        </tr>

		<?php
    }

}

new Apollo_Admin_Taxonomies();

// Update hidden status event tax
if ( isset( $_POST['action'] ) && $_POST['action'] == 'hide' ) {
   $ids = $_POST['delete_tags'];
   if ($ids) {
       foreach($ids as $id) {
           update_apollo_term_meta($id, Apollo_DB_Schema::_APL_EVENT_TAX_HIDE, 1);
       }
   }
}

/*@ticket #17234 0002398 - Recommendation Widgets - [Admin] Official tag for event and post*/
/*update offical for event tag and post tag */
if ( isset( $_POST['action'] ) && $_POST['action'] == 'apl_set_official' ) {
    $ids = isset($_POST['delete_tags']) ? $_POST['delete_tags'] :'';
    if ($ids) {
        foreach($ids as $id) {
            if(isset($_POST['taxonomy']) && $_POST['taxonomy'] === 'event-tag'){
                update_apollo_term_meta($id, Apollo_DB_Schema::_APL_EVENT_TAG_OFFICIAL, 1);
            }

            if(isset($_POST['taxonomy']) && $_POST['taxonomy'] === 'post_tag'){
                update_apollo_term_meta($id, Apollo_DB_Schema::_APL_POST_TAG_OFFICIAL, 1);
            }
        }
    }
}