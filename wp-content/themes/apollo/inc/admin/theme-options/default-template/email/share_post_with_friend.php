<tr>
    <td>
        <p>Dear {email}, </p>
        <p>I found this on {site_url} and thought you might be interested.</p>
        <p>Click on the link to check it out: {link_share} </p>
    </td>
</tr>