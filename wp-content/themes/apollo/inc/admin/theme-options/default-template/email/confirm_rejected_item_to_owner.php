<tr>
    <td>
        <p>Dear {username},</p>
        <p>We're sorry to inform you that your event titled {event_title} has not been approved for the following reasons:</p>
        <p>{reasons}</p>
        <p>{reason_detail}</p>
        <p>Please note our guidelines for submissions and re-submit! We look forward to sharing your upcoming events with our arts and cultural community.</p>
        <p>If you have any questions, please contact: <a href="mailto:{support_email}">{support_email}</a></p>
        <p>Thank you, </p>
        <p>The {site_name} Team</p>
    </td>
</tr>