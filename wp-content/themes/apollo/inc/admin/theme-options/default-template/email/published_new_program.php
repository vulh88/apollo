<tr>
    <td>
        <p>Dear {username},</p>
        <p>You recieved this email because your program is published on the <a href="{site_url}">{site_name}</a> site</p>
        <p>Please click here to see your program: <a href="{program_url}">{program_title}</a></p>
        <p>Best Regards, </p>
        <p>The Team @ <a href="{site_url}">{site_name}<a></p>
    </td>
</tr>