<tr>
    <td>
        <p>Dear administrator,</p>
        <p>The {new_post_title} listing edited: <a href="{edit_post_link}" >{post_name}</a> </p>
        <p>Best Regards, </p>
        <p>The Team @ {site_name}</p>
    </td>
</tr>