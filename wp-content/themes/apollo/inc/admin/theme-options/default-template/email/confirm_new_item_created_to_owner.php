<tr>
    <td>
        <p>Dear {username},</p>
        <p>New {new_post_title} listing posted: {post_name}</p>
        <p>Best Regards, </p>
        <p>The Team @ {site_name}</p>
    </td>
</tr>