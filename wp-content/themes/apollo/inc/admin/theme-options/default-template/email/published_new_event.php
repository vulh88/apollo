<tr>
    <td>
        <p>Dear {username},</p>
        <p>You recieved this email because your event is published on the <a href="{site_url}">{site_name}</a> site</p>
        <p>Please click here to see your event: <a href="{event_url}">{event_title}</a></p>
        <p>Best Regards, </p>
        <p>The Team @ <a href="{site_url}">{site_name}<a></p>
    </td>
</tr>