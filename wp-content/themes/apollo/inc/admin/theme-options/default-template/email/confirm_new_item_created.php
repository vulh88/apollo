<tr>
    <td>
        <p>Dear administrator,</p>
        <p>New {new_post_title} listing posted: {post_name}</p> created by: {posted_user}
        <p>Best Regards, </p>
        <p>The Team @ {site_name}</p>
    </td>
</tr>