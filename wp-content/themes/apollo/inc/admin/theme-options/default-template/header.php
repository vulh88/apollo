<div class="logo"><a href="<?php echo esc_url(home_url('/')); ?>" > <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/uploads/logo.png" alt="LOGO"></a></div>

<div class="top-blk login-top">
    <a style="display: none" href="#" class="_login_url" data-oct-title="<?php _e('Login','apollo') ?>"><?php _e('LOGIN', 'apollo') ?></a>
    <a style="display: none"  href="#" class="_register_url" data-oct-title="<?php _e('Register','apollo') ?>"><?php _e('REGISTER', 'apollo') ?></a>

    <a style="display: none"  href="#" class="_logout_url" data-oct-title="<?php _e('Logout','apollo') ?>"><?php _e('LOGOUT', 'apollo') ?></a>
    <a style="display: none"  href="#" class="_my_account_url" data-oct-title="<?php _e('Account','apollo') ?>"><?php _e('MY ACCOUNT', 'apollo') ?></a>
</div>

<div class="top-blk social-top">
    <a href="#" target="_blank" class="lk" style="display: none;"><i class="fa fa-linkedin fa-lg">&nbsp;</i></a>
    <a href="#" target="_blank" class="fb" style="display: none;"><i class="fa fa-facebook fa-lg">&nbsp;</i></a>
    <a href="#" target="_blank" class="tu" style="display: none;"><i class="fa fa-twitter fa-lg">&nbsp;</i></a>
    <a href="#" target="_blank" class="yt" style="display: none;"><i class="fa fa-youtube fa-lg">&nbsp;</i></a>
    <a href="#" target="_blank" class="vm" style="display: none;"><i class="fa fa-vimeo-square">&nbsp;</i></a>
    <a href="#" target="_blank" class="insta" style="display: none;"><i class="fa fa-instagram fa-lg"></i></a>
</div>

<div class="top-blk search-box" data-oct-title="<?php _e('Search','apollo') ?>">
    <span class="search-lbl"><?php _e('SEARCH', 'apollo') ?></span>
    <form method="get" id="searchform" action="<?php echo home_url(); ?>" class="form-search">
        <input name="s" type="text" placeholder="<?php _e( 'Quick Search ...', 'apollo' ) ?>" class="inp inp-txt solr-search">
        <button type="submit" class="btn btn-link"> <i class="fa fa-search fa-flip-horizontal fa-lg"></i></button>
    </form>

</div>