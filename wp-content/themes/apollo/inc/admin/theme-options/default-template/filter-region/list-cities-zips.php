<?php

    $contentClass = '';
    if($filterBy == Apollo_DB_Schema::_APL_FILTERING_BY_REGION_DEFAULT){
        $contentClass = 'hidden';
    }

?>
<div id="region-content" class="region-content <?php echo $contentClass; ?>">

    <div class="heading region">
        <label><h4>Select zip code for region</h4></label>
        <select class="of-input " name="_apollo_theme_options[<?php Apollo_DB_Schema::_APL_SELECT_REGION ?>]" id="<?php echo Apollo_DB_Schema::_APL_SELECT_REGION ?>">
            <?php
            foreach($region as $key => $item){
                ?>
                <option value="<?php echo ltrim(rtrim($item)); ?>"><?php echo $item; ?></option>
                <?php
            }
            ?>
        </select>
    </div>

    <ul class="region-zip-list ">
        <li>
            <label>
                <input value="" id="check_all_region_zip" type="checkbox">
                <strong><?php _e('Select / UnSelect  All', 'apollo') ?> </strong>&nbsp;
            </label>
        </li>
        <?php
        $i =1 ;
        $class = '';
        foreach ($region as $regionKey => $regionTitle) {
            foreach ($zipList as $zipKey => $zipTitle) {
                $checked = '';
                $storeRegion = isset($dataRegion[$regionKey]) ? $dataRegion[$regionKey] : null;
                if ($storeRegion != null) {
                    if (in_array($zipKey, $storeRegion)) {
                        $checked = 'checked';
                    }
                }

                if($i == 1){
                    $class = 'active';
                }
                $regionKey = ltrim(rtrim($regionKey));
                ?>
                <li class=" select-zip <?php echo $class; ?>" data-parent="<?php echo ltrim(rtrim($regionKey)); ?>">
                <span>
                     <input
                         class = "zip-item"
                         <?php echo $checked; ?>
                         data-parent="<?php echo ltrim(rtrim($regionKey)); ?>"
                         type="checkbox"
                         name="_apollo_theme_options[<?php echo Apollo_DB_Schema::_APL_FILTERING_BY_REGION_ZIP_LIST ?>][<?php echo $regionKey; ?>][<?php echo $zipTitle ?>]"
                         value="<?php echo $zipTitle; ?>"/><?php echo $zipTitle ?>
                </span>
                </li>
                <?php
            };
            $class = 'hidden';
            $i ++;
        }
        ?>
    </ul>
    <!---end select zip --->

</div>

