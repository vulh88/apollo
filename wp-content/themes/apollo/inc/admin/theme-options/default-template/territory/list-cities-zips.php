<br>
<label>
    <strong><?php _e( 'Select All', 'apollo' ) ?> </strong>&nbsp;<input value="" id="check_all_zip_city" type="checkbox" >
</label>
<br>
<br>
<?php foreach( $scz as $st ):

        $_cities = isset($_terrData[$st->state_prefix]) ? array_keys($_terrData[$st->state_prefix]) : '';

        $cityState = $st->city.'<>'.$st->state_prefix;
        $disabled = $seenCity && in_array($cityState, $seenCity) ? "disabled" : '';

        $_zips = isset($_terrData[$st->state_prefix][$st->city]) && isset($_terrData[$st->state_prefix][$st->city])
        && $_terrData[$st->state_prefix][$st->city]
            ? array_keys($_terrData[$st->state_prefix][$st->city]) : '';
        ?>
        <li><span><?php echo $st->state_name ?></span>

                            <span><input data-value="<?php echo $cityState ?>" <?php echo $disabled ?> <?php echo $_cities && in_array($st->city, $_cities) && !$disabled ? 'checked' : '' ?> class="city"
                                         type="checkbox" name="_apollo_theme_options[<?php echo Apollo_DB_Schema::_TERR_DATA ?>][<?php echo $st->state_prefix ?>][<?php echo $st->city ?>]"
                                         value="" /> <?php echo $st->city ?></span>


                            <span><input <?php echo $_zips && in_array($st->zipcode, $_zips) ? 'checked' : '' ?> class="zip" type="checkbox"
                                                                                                                 name="_apollo_theme_options[<?php echo Apollo_DB_Schema::_TERR_DATA ?>][<?php echo $st->state_prefix ?>][<?php echo $st->city ?>][<?php echo $st->zipcode ?>]"
                                    /><?php echo $st->zipcode ?></span>
        </li>
        <?php
        if ( !in_array($cityState, $seenCity) ) $seenCity[] = $cityState;
endforeach; ?>
