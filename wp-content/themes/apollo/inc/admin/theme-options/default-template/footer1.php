<section class="fullw clients">
    <div class="inner t-c">
        <h3 class="f-ttl">OUR PARTNERS &amp; SPONSORS</h3>
        <div class="list-logo">
            <a href="#"> <img src="/wp-content/themes/apollo/assets/uploads/l1_1.jpg" alt="logo 1"></a>
            <a href="#"> <img src="/wp-content/themes/apollo/assets/uploads/l2_1.jpg" alt="logo 2"></a>
            <a href="#"> <img src="/wp-content/themes/apollo/assets/uploads/l3_1.jpg" alt="logo 3"></a>
            <a href="#"> <img src="/wp-content/themes/apollo/assets/uploads/l4_1.jpg" alt="logo 4"></a>
            <a href="#"> <img src="/wp-content/themes/apollo/assets/uploads/l5_1.jpg" alt="logo 5"></a>
        </div>
    </div>
</section>