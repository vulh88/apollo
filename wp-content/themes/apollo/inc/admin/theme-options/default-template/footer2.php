<section class="fullw bg-gray">
    <div class="inner">
        <ul class="menu-footer">
            <li>
                <h3 class="f-ttl">DIRECTORIES</h3>
                <ul>
                    <li><a href="/org/listing/">Organizations</a></li>
                    <li><a href="/venue/listing/">Venues</a></li>
                    <li><a href="/profile/listing/">Artist Profiles</a></li>
                    <li><a href="/org/detail/220132753/Utah_Division_of_Arts_and_Museums">Local Arts Councils</a></li>
                </ul>
            </li><li>
                <h3 class="f-ttl">SHOW BY AREA</h3>
                <ul>
                    <li><a href="/categories/index/14/365/">Central Utah</a></li>
                    <li><a href="/categories/index/14/367/">Eastern Utah</a></li>
                    <li><a href="/categories/index/14/368/">Greater Salt Lake</a></li>
                    <li><a href="/categories/index/14/371/">Northern Utah</a></li>
                    <li><a href="/categories/index/14/373/">Southeastern Utah</a></li>
                    <li><a href="/categories/index/14/375/">Southern Utah</a></li>
                </ul>
            </li><li>
                <h3 class="f-ttl">RESOURCES</h3>
                <ul>
                    <li><a href="/video/listing/">Video Central</a></li>
                    <li><a href="/categories/index/11/0">Classes &amp; Workshops</a></li>
                </ul>
            </li><li>
                <h3 class="f-ttl">ARTIST OPPORTUNITIES</h3>
                <ul>
                    <li><a href="/classifieds/listing/4/1">Auditions</a></li>
                    <li><a href="/classifieds/listing/4/12">Classes for Adults</a></li>
                    <li><a href="/classifieds/listing/4/7">Miscellaneous</a></li>
                    <li><a href="/classifieds/listing/4/6">Volunteers</a></li>
                    <li><a href="/classifieds/listing/4/5">Jobs</a></li>
                    <li><a href="/classifieds/listing/4/4">For Sale/Free</a></li>
                    <li><a href="/classifieds/listing/4/3">For Rent</a></li>
                    <li><a href="/classifieds/listing/4/2">Call For Artists</a></li>
                    <li><a href="/classifieds/listing/4/13">Classes for Kids</a></li>
                    <li><a href="/classifieds/listing/4/11">Workshops</a></li>
                </ul>
             </li><li>
                <h3 class="f-ttl">SUBMIT LISTINGS</h3>
                <ul>
                    <li><a href="/page/submit_event">Submit Events</a></li>
                    <li><a href="/submit/org">Organization</a></li>
                    <li><a href="/user/login">Artist Profile</a></li>
                    <li><a href="/submit/classified">Art Opportunities</a></li>
                    <li><a onclick="openReportWindow()" href="#">Submit Listing Change</a></li>
                    <li><a href="/page/how_to_submit">How To Submit</a></li>
                </ul>
             </li><li>
                <h3 class="f-ttl">ABOUT US</h3>
                <ul>
                    <li><a href="/page/contact_us">Contact Us</a></li>
                    <li><a href="/page/overview">Overview</a></li>
                    <li><a href="/page/partners_page">Partners</a></li>
                    <li><a href="/page/press">Press Lounge</a></li>
                </ul>
            </li>
        </ul>
    </div>
</section>