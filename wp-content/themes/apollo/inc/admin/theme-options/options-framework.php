<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Don't load if optionsframework_init is already defined
if (is_admin() && ! function_exists( 'optionsframework_init' ) ) :

function optionsframework_init() {

	//  If user can't edit theme options, exit
	if ( ! current_user_can( 'edit_theme_options' ) ) return false;

    $pluginDirPath = plugin_dir_path( __FILE__ );
    
    require $pluginDirPath . 'includes/class-options-media-uploader.php';
    require $pluginDirPath . 'includes/class-options-framework.php';
    require_once $pluginDirPath . 'includes/class-options-framework-admin.php';


    if (Apollo_App::isThemeOption()) {
        // Fields
        require $pluginDirPath . 'includes/fields/field.php';
        require $pluginDirPath . 'includes/fields/button.php';
        require $pluginDirPath . 'includes/fields/text.php';
        require $pluginDirPath . 'includes/fields/textarea.php';
        require $pluginDirPath . 'includes/fields/multiselect-sort.php';
        /*@ticket #18343: 0002504: Arts Education Customizations - Add Arts Ed masthead logo and footers to Blog listing and detail pages.*/
        require $pluginDirPath . 'includes/fields/override-html-static-content.php';

        // Loads the required Options Framework classes.
        require $pluginDirPath . 'includes/class-options-framwork-color.php';


        require $pluginDirPath . 'includes/class-options-interface.php';
        require $pluginDirPath . 'includes/class-options-sanitization.php';
        require $pluginDirPath . 'includes/class-apollo-preserved-markup-editor.php';

        require $pluginDirPath. 'includes/email/inc/class-apollo-option-default-opts-template.php';
        require $pluginDirPath. 'includes/email/class-apollo-abstract-email-management.php';
        require $pluginDirPath. 'includes/email/class-apollo-options-email-option.php';
        require $pluginDirPath. 'includes/email/class-apollo-options-email-templates.php';
        require $pluginDirPath. 'includes/territory/class-apollo-options-territory-option.php';
        require $pluginDirPath. 'includes/filter-region/class-apollo-filter-region-option.php';

        // all settings files for site config tabs and its sub-tabs
        require $pluginDirPath. 'includes/modules/class-apollo-options-general.php';
        require $pluginDirPath. 'includes/modules/class-apollo-options-event.php';
        require $pluginDirPath. 'includes/modules/class-apollo-options-blog.php';
        require $pluginDirPath. 'includes/modules/class-apollo-options-organization.php';
        require $pluginDirPath. 'includes/modules/class-apollo-options-public-art.php';
        require $pluginDirPath. 'includes/modules/class-apollo-options-artist.php';
        require $pluginDirPath. 'includes/modules/class-apollo-options-education.php';
        require $pluginDirPath. 'includes/modules/class-apollo-options-search.php';
        require $pluginDirPath. 'includes/modules/class-apollo-options-business.php';
        require $pluginDirPath. 'includes/modules/class-apollo-options-classified.php';
        require $pluginDirPath. 'includes/modules/class-apollo-options-venue.php';
        require $pluginDirPath. 'includes/modules/class-apollo-options-programs.php';
        require $pluginDirPath. 'includes/modules/class-apollo-options-syndication.php';
        require $pluginDirPath. 'includes/modules/class-apollo-options-news.php';


        // implementing for Apollo Option Backup Tool
        require $pluginDirPath. 'includes/backup/class-apollo-options-backup-tool.php';
        require $pluginDirPath. 'includes/backup/class-apollo-options-import.php';
        require $pluginDirPath. 'includes/backup/class-apollo-options-export.php';

        // Instantiate the main plugin class.
        $options_framework = new Options_Framework;
        $options_framework->init();
    }


	// Instantiate the options page.
	$GLOBALS['apollo_options_framework_admin'] = $options_framework_admin = new Options_Framework_Admin;
	$options_framework_admin->init();

    if (Apollo_App::isThemeOption()) {
        // Instantiate the media uploader class
        $options_framework_media_uploader = new Options_Framework_Media_Uploader;
        $options_framework_media_uploader->init();

        // Instantiate the preserved markup editor class
        $preserved_markup_editor = new Apollo_Preserved_Markup_Editor();
        $preserved_markup_editor->init();
    }
}

add_action( 'init', 'optionsframework_init', 20 );

endif;


if ( !function_exists('of_get_option_id') ) {
    function of_get_option_id() {
        return '_apollo_theme_options';
    }
}

/**
 * Helper function to return the theme option value.
 * If no value has been saved, it returns $default.
 * Needed because options are saved as serialized strings.
 *
 * Not in a class to support backwards compatibility in themes.
 */

if ( ! function_exists( 'of_get_option' ) ) :

    function of_get_option( $name, $default = false, $forceEnglish = false ) {
        $config = of_get_option_id();

        if ( ! $config ) {
            return $default;
        }

        // Thienld : custom logic get option cache.
        // if of_get_option_cache() !== false -> return options cached for current request
        // elseif of_get_option_cache() === false -> no options was cached on cache folder -> auto-regenerating cache for theme option after get from DB
        $options = get_option( $config );
        //$options = of_get_option_cache();
//        if ($options === false) {
//            $options = get_option( $config );
//            // regenerating cache to local cached folder
//            $cacheManagementFilePath = APOLLO_INCLUDES_DIR. '/admin/tools/cache/Inc/apollo-sites-caching-management.php';
//            if(file_exists($cacheManagementFilePath)){
//                require_once $cacheManagementFilePath;
//                if(class_exists('APLC_Site_Caching_Management')){
//                    $currentSiteID = get_current_blog_id();
//                    $cleanCachedSite = new APLC_Site_Caching_Management($currentSiteID);
//                    $cleanCachedSite->showAdminMessage = false;
//                    // Keep all option in file for next query theme options from frontend area.
//                    $cleanCachedSite->cacheThemeOptions($options);
//                }
//            }
//        }

        global $apollo_theme_wplm;
        if (!$forceEnglish && $apollo_theme_wplm && in_array($name, $apollo_theme_wplm) && Apollo_App::hasWPLM()) {
            global $sitepress;
            $curLang = $sitepress->get_current_language();
            $name .= $curLang != 'en' ? '_'. $curLang : '';
        }


        if ( isset( $options[$name] ) && $options[$name] != NULL ) {
            return $options[$name];
        }

        return $default;
    }

endif;


if ( !function_exists('of_get_option_cache') ) {
    function of_get_option_cache() {
        $uploadDir = wp_upload_dir();
        $dir = $uploadDir['cache'];
        $jsonOptionFilePath = $dir. '/'. of_get_option_id(). '.json';
        if(!is_file($jsonOptionFilePath) || !file_exists($jsonOptionFilePath)){
            return false;
        }
        $content = @file_get_contents($jsonOptionFilePath);
        return !empty($content) ? json_decode($content,true) : false;
    }
}

if ( ! function_exists( 'render_php_to_string' ) ) :
    
    function render_php_to_string($file, $vars=null) {
     
        if (is_array($vars) && !empty($vars)) {
            extract($vars);
        }
        ob_start();
        if (file_exists($file)) {
            include $file;
        }

        return ob_get_clean();
    }

endif;

if ( ! function_exists( 'get_override_css_abs_path' ) ) :
    function get_override_css_abs_path() {

        return Apollo_App::getUploadBaseInfo('css_dir'). '/override.css';
    }
endif;

if ( !function_exists( 'get_primary_color_css_abs_path' ) ) :
    
    function get_primary_color_css_abs_path() {

        return Apollo_App::getUploadBaseInfo('css_dir').  '/primary_color.css';
    }

endif;

if ( !function_exists( 'get_override_css_url' ) ) :

    function get_override_css_url() {
        return Apollo_App::getUploadBaseInfo('css_url').  '/override.css';
    }

endif;

if ( !function_exists( 'get_primary_color_css_url' ) ) :

    function get_primary_color_css_url() {
    
        return Apollo_App::getUploadBaseInfo('css_url'). '/primary_color.css';

    }
endif;

if ( !function_exists('get_current_domain') ):
function get_current_domain() {
    $sites = get_blog_details(get_current_blog_id());
    return $sites->domain;
}
endif;

// Setup the WordPress core custom background feature.
add_theme_support( 'custom-background', apply_filters( 'apollo_custom_background_args', array(
    'default-color' => APOLLO_DEFAULT_BACKGROUND_COLOR
) ) );

if ( !function_exists( 'adjust_brightness' ) ) {
    
    function adjust_brightness( $hex, $steps = 20 ) {
        
        // Steps should be between -255 and 255. Negative = darker, positive = lighter
        $steps = max(-255, min(255, $steps));

        // Format the hex color string
        $hex = str_replace('#', '', $hex);
        
        if (strlen($hex) == 3) {
            $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
        }

        // Get decimal values
        $r = hexdec(substr($hex,0,2));
        $g = hexdec(substr($hex,2,2));
        $b = hexdec(substr($hex,4,2));

        // Adjust number of steps and keep it inside 0 to 255
        $r = max(0,min(255,$r + $steps));
        $g = max(0,min(255,$g + $steps));  
        $b = max(0,min(255,$b + $steps));

        $r_hex = str_pad(dechex($r), 2, '0', STR_PAD_LEFT);
        $g_hex = str_pad(dechex($g), 2, '0', STR_PAD_LEFT);
        $b_hex = str_pad(dechex($b), 2, '0', STR_PAD_LEFT);

        return '#'. $r_hex.$g_hex.$b_hex;
    }

}

function get_new_color_from_pattern($source, $dest, $dest_relation) {
    return Apollo_App::getNewColorFromDefault($source, $dest, $dest_relation);
}

if ( !function_exists( 'check_is_bright_color' ) ) {
    
    function check_is_bright_color( $hex )  {

        $hex = str_replace( '#' , '', $hex );

        if (strlen($hex) == 3) {
            $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
        }

        $r = hexdec(substr($hex,0,2));
        $g = hexdec(substr($hex,2,2));
        $b = hexdec(substr($hex,4,2));
        $a = $r + $g + $b;
        return $r + $g + $b > 382;
    }
}

if ( ! function_exists( 'get_html_file_abs_path' ) ) :
    
    function get_html_file_abs_path( $filename, $ext = 'html' ) {
        return Apollo_App::getUploadBaseInfo( 'html_dir' ). '/'. $filename. '.'. $ext;
    }
endif;

if ( ! function_exists( 'get_html_file_url' ) ) :
    
    function get_html_file_url( $filename, $ext = 'html' ) {
        return Apollo_App::getUploadBaseInfo('html_url'). '/'. $filename. '.'. $ext;
    }
endif;
