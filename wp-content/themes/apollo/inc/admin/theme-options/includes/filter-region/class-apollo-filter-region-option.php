<?php
/**
 * @author Trilm
 * Apollo_Options_Filter_Region
 *
 */
class Apollo_Options_Filter_Region
{

    protected $filterOptions;
    protected $options = array();
    protected $isDefaultSearch;
    protected $table;

    /**
     * @return array
     */
    public function getFilterOptions()
    {
        return $this->filterOptions;
    }

    /**
     * @param array $filterOptions
     */
    public function setFilterOptions($filterOptions)
    {
        $this->filterOptions = $filterOptions;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param array $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }


    public function __construct()
    {
        global $wpdb;
        $privateStates = Apollo_App::get_network_manage_states_cities();
        $this->table = ($privateStates ? $wpdb->prefix : $wpdb->base_prefix). Apollo_Tables::_APL_STATE_ZIP;



        //filter options
        $this->filterOptions = array(
            Apollo_DB_Schema::_APL_FILTERING_BY_REGION_DEFAULT => __('Default', 'apollo'),
            Apollo_DB_Schema::_APL_FILTERING_BY_REGION_LISTING => __('Region Selection', 'apollo'),
        );

        //options
        $this->options[] = array(
            'name' => __('Filter by region ', 'apollo'),
            'desc' => '',
            'id' => Apollo_DB_Schema::_APL_FILTERING_BY_REGION,
            'std' =>  Apollo_DB_Schema::_APL_FILTERING_BY_REGION_DEFAULT,
            'type' => 'select',
            'options' => $this->filterOptions,
            'class' => 'apollo_input_number no-border inline'
        );


            //ziplist
            $this->options[] = array(
                'id'    => '',
                'type' 		=> 'custom_layout',
                'name'  => __('Select zipcodes'),
                'layout'      => $this->renderZipList(),
                'wrapper_class' => 'full',
                'class' => 'contain-city-zip select-zip-code',
            );


    }

    /**
     * @author: Trilm
     * get all region in theme_option -> Territory -> regions
     */

    public function getRegions()
    {
        $regions = of_get_option(Apollo_DB_Schema::_REGIONS  );
        $regionsArray =explode(',',$regions);
        $regionsArrayReturn = array();

        //we use name same as key
        foreach($regionsArray as $val){
            $regionsArrayReturn[$val] = $val;
        }

        return $regionsArrayReturn;
    }

    /**
     *
     * @author: Trilm
     * get all zip
     *
     */
    public function getAllZips(){
        global $wpdb;
        $_states = of_get_option(Apollo_DB_Schema::_TERR_STATES, false);
        $_terrData = of_get_option(Apollo_DB_Schema::_TERR_DATA);
        $states = $_states ? array_keys($_states) : array();
        $cities = array();
        $zips = array();
        //if ( ! $states ) return false;
        $inlist_states = "'" .implode("','", $states). "'";
        if($_terrData){
            foreach ($_terrData as $arr_cit_sta){
                if($arr_cit_sta){
                    foreach ($arr_cit_sta as $_cit => $arr_zip ){
                        $cities[] = $_cit;
                        if($arr_zip){
                            foreach ( $arr_zip as $_zip => $val ) {
                                $zips[] = $_zip;
                            }
                        }
                    }
                }
            }
        }
        $inlist_cities = "'" .implode("','", $cities). "'";
        $inlist_zips = "'" .implode("','", $zips). "'";

        //$querystr = "SELECT * from wp_apollo_state_zip where state_prefix IN ($inlist_states) AND city IN ($inlist_cities) AND zipcode IN ($inlist_zips) order by state_prefix, city";

        $querystr = "
                SELECT cs.code as zipcode, cityTbl.name as city, cs.id as zipId, stateTbl.code as state_prefix, stateTbl.name as state_name
                FROM $this->table cs
                INNER JOIN $this->table cityTbl ON cityTbl.id = cs.parent AND cityTbl.type = 'city' AND cityTbl.name IN ($inlist_cities)
                INNER JOIN $this->table stateTbl ON stateTbl.id = cityTbl.parent AND stateTbl.type = 'state' AND stateTbl.code IN ($inlist_states)
                WHERE cs.code IN ($inlist_zips) AND cs.type = 'zip'
                ORDER BY cs.code ASC
             ";


        $scz = $wpdb->get_results($querystr, OBJECT);
        $zipList = array();
        foreach($scz as $val){
            $zipList[$val->zipcode] = $val->zipcode;
        }
        return $zipList;

    }


    /**
     *
     * @author: Trilm
     * list all zip to checkbox
     */
    public function renderZipList(){
        $zipList = $this->getAllZips();
        $region = $this->getRegions();
        $dataRegion =  Apollo_Options_Filter_Region::getRegionOptions();

        $filterBy = of_get_option(Apollo_DB_Schema::_APL_FILTERING_BY_REGION);


        ob_start();
        include APOLLO_ADMIN_DIR . '/theme-options/default-template/filter-region/list-cities-zips.php';
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }

    public static function getRegionOptions($region = '', $zip = ''){
        $data  = of_get_option(Apollo_DB_Schema::_APL_FILTERING_BY_REGION_LISTING);
        $data = json_decode($data,true);
        if(isset($data[$region][$zip]) && !empty($data[$region][$zip])){
            return $data[$region][$zip];
        }
        return $data;
    }
}


/**
 *
 * @author: Trilm
 * Class Apollo_Options_Filter_Region_Factory
 *
 */
class Apollo_Options_Filter_Region_Factory{
    public static function init($options){
        $regionFilter = new Apollo_Options_Filter_Region();
        $filterOption =  $regionFilter->getOptions();
        return array_merge($options,$filterOption);
    }
}