<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Apollo_Options_Business' ) ) :

/**
 * Description of Apollo_Options_Business
 *
 * @author thienld
 */
class Apollo_Options_Business {

    public function __construct() {
        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_BUSINESS_PT ) ) {
            return;
        }
        add_filter('of_options_first', array( $this, 'getOptions' ), 4);
    }

    public function getOptions($opts) {

        $options = empty($opts) ? array() : $opts;

        // open the tab content
        $options[] = array(
            'tab-id'        => APL_Theme_Option_Site_Config_SubTab::BUSINESS_SETTING,
            'type'        => 'sub_tab_content_opened',
        );

        $options[] = array(
            'name'     => __( 'Tab Name', 'apollo' ),
            'desc'     => '',
            'id'   => APL_Business_Module_Theme_Option::TAB_NAME,
            'std'  => APL_Business_Module_Theme_Option::TAB_NAME_DEFAULT_VALUE,
            'type'     => 'text',
            'class' => 'no-border inline'
        );

        $options[] = array(
            'name'     => __( 'Introduction Text', 'apollo' ),
            'desc'     => __( 'Max 255 Characters', 'apollo' ),
            'id'   => APL_Business_Module_Theme_Option::INTRODUCTION,
            'std'  => '',
            'type'     => 'editor',
            'class'     => 'no-border inline full_width max-w-100-per',
            'settings' => array('rows' => 4)
        );

        $options[] = array(
            'name'      => __( 'Select 4 business type that represent Dining, Accommodation, Bars/Clubs, Local business', 'apollo' ),
            'type'      => 'header_block'
        );


        $types = APL_Business_Function::getTreeBusinessType(array('hide_empty' => false, 'orderby' => 'name', 'order' => 'ASC'));

        $iconDefault = Apollo_App::getDefaultMarkersBusIcons();


        // Business type : Dining

        $selectedVal = of_get_option(APL_Business_Module_Theme_Option::DINING_SELECT);
        ob_start();
        Apollo_Event::build_option_tree($types, $selectedVal, Apollo_Display_Config::TREE_MAX_LEVEL, 'business-type');
        $htmlOptions = ob_get_clean();

        $options[] = array(
            'name'       => __( 'Dining', 'apollo' ),
            'desc'       => '',
            'id'         => APL_Business_Module_Theme_Option::DINING_SELECT,
            'std'        => '0',
            'type'       => 'select',
            'class'      => 'apollo_input_number no-border inline',
            'options_html' => $htmlOptions,
            'null_option'  => true,
        );

        $options[] = array(
            'name'  => __( ' ', 'apollo' ),
            'desc'  => __("Display name","apollo"),
            'id'    => APL_Business_Module_Theme_Option::DINING_SELECT . '_text',
            'std'   => '',
            'type'  => 'text',
            'class' => 'no-border inline'
        );

        $options[] = array(
            'name'  => __( ' ', 'apollo' ),
            'desc'  => __("Google Map Marker Custom (Enter marker url)","apollo"),
            'id'    => APL_Business_Module_Theme_Option::DINING_SELECT . '_marker',
            'std'   => '',
            'type'  => 'text',
            'class' => 'no-border inline',
            'icon-src' => $iconDefault[APL_Business_Module_Theme_Option::DINING_SELECT]['marker'],
            'icon-title' => __("Icon default","apollo")
        );

        // Business type : Accommodation

        $selectedVal = of_get_option(APL_Business_Module_Theme_Option::ACCOMMODATION_SELECT);
        ob_start();
        Apollo_Event::build_option_tree($types, $selectedVal, Apollo_Display_Config::TREE_MAX_LEVEL, 'business-type');
        $htmlOptions = ob_get_clean();

        $options[] = array(
            'name'       => __( 'Accommodation', 'apollo' ),
            'desc'       => '',
            'id'         => APL_Business_Module_Theme_Option::ACCOMMODATION_SELECT,
            'std'        => '0',
            'type'       => 'select',
            'class'      => 'apollo_input_number no-border inline',
            'options_html' => $htmlOptions,
            'null_option'  => true,
        );

        $options[] = array(
            'name'  => __( ' ', 'apollo' ),
            'desc'  => __("Display name","apollo"),
            'id'    => APL_Business_Module_Theme_Option::ACCOMMODATION_SELECT . '_text',
            'std'   => '',
            'type'  => 'text',
            'class' => 'no-border inline'
        );

        $options[] = array(
            'name'  => __( ' ', 'apollo' ),
            'desc'  => __("Google Map Marker Custom (Enter marker url)","apollo"),
            'id'    => APL_Business_Module_Theme_Option::ACCOMMODATION_SELECT . '_marker',
            'std'   => '',
            'type'  => 'text',
            'class' => 'no-border inline',
            'icon-src' => $iconDefault[APL_Business_Module_Theme_Option::ACCOMMODATION_SELECT]['marker'],
            'icon-title' => __("Icon default","apollo")
        );


        // Business type : Bars/Clubs

        $selectedVal = of_get_option(APL_Business_Module_Theme_Option::BARS_CLUBS_SELECT);
        ob_start();
        Apollo_Event::build_option_tree($types, $selectedVal, Apollo_Display_Config::TREE_MAX_LEVEL, 'business-type');
        $htmlOptions = ob_get_clean();

        $options[] = array(
            'name'       => __( 'Bars/Clubs', 'apollo' ),
            'desc'       => '',
            'id'         => APL_Business_Module_Theme_Option::BARS_CLUBS_SELECT,
            'std'        => '0',
            'type'       => 'select',
            'class'      => 'apollo_input_number no-border inline',
            'options_html' => $htmlOptions,
            'null_option'  => true,
        );

        $options[] = array(
            'name'  => __( ' ', 'apollo' ),
            'desc'  => __("Display name","apollo"),
            'id'    => APL_Business_Module_Theme_Option::BARS_CLUBS_SELECT . '_text',
            'std'   => '',
            'type'  => 'text',
            'class' => 'no-border inline'
        );

        $options[] = array(
            'name'  => __( ' ', 'apollo' ),
            'desc'  => __("Google Map Marker Custom (Enter marker url)","apollo"),
            'id'    => APL_Business_Module_Theme_Option::BARS_CLUBS_SELECT . '_marker',
            'std'   => '',
            'type'  => 'text',
            'class' => 'no-border inline',
            'icon-src' => $iconDefault[APL_Business_Module_Theme_Option::BARS_CLUBS_SELECT]['marker'],
            'icon-title' => __("Icon default","apollo")
        );

        // Business type : Local Business
        $selectedVal = of_get_option(APL_Business_Module_Theme_Option::LOCAL_BUSINESS_SELECT);
        ob_start();
        Apollo_Event::build_option_tree($types, $selectedVal, Apollo_Display_Config::TREE_MAX_LEVEL, 'business-type');
        $htmlOptions = ob_get_clean();


        $options[] = array(
            'name'       => __( 'Local Business', 'apollo' ),
            'desc'       => '',
            'id'         => APL_Business_Module_Theme_Option::LOCAL_BUSINESS_SELECT,
            'std'        => '0',
            'type'       => 'select',
            'class'      => 'apollo_input_number no-border inline',
           'options_html' => $htmlOptions,
            'null_option'  => true,
        );

        $options[] = array(
            'name'  => __( ' ', 'apollo' ),
            'desc'  => __("Display name","apollo"),
            'id'    => APL_Business_Module_Theme_Option::LOCAL_BUSINESS_SELECT . '_text',
            'std'   => '',
            'type'  => 'text',
            'class' => 'no-border inline'
        );

        $options[] = array(
            'name'  => __( ' ', 'apollo' ),
            'desc'  => __("Google Map Marker Custom (Enter marker url)","apollo"),
            'id'    => APL_Business_Module_Theme_Option::LOCAL_BUSINESS_SELECT . '_marker',
            'std'   => '',
            'type'  => 'text',
            'class' => 'no-border inline',
            'icon-src' => $iconDefault[APL_Business_Module_Theme_Option::LOCAL_BUSINESS_SELECT]['marker'],
            'icon-title' => __("Icon default","apollo")
        );

        // select business type which will be displayed on the map
        $displayedBSTypes = array(
            APL_Business_Module_Theme_Option::DINING_SELECT => __('Dining','apollo'),
            APL_Business_Module_Theme_Option::ACCOMMODATION_SELECT => __('Accommodation','apollo'),
            APL_Business_Module_Theme_Option::BARS_CLUBS_SELECT => __('Bars/Clubs','apollo'),
            APL_Business_Module_Theme_Option::LOCAL_BUSINESS_SELECT => __('Local Business','apollo'),
        );
        $options[] = array(
            'id'    => APL_Business_Module_Theme_Option::DISPLAY_BUSINESS_TYPES,
            'type' 		=> 'multicheck',
            'name'  => __('Select the business types you want to display on the map','apollo'),
            'wrapper_class' => 'bs-type-multi-cb',
            'class' => 'no-border inline',
            'options'      => $displayedBSTypes,
        );

        // Display business types within of the venue
        $displayBSTWithin = array(
            1609    => __( 'One mile radius', 'apollo' ), // To km
            804    => __( 'Half mile radius', 'apollo' ),
            80.45    => __( 'One Block', 'apollo' ),
            241.35    => __( 'Three Blocks', 'apollo' ),
        );

        $options[] = array(
            'name'       => __( 'Select a distance value to display businesses within of the venue', 'apollo' ),
            'desc'       => '',
            'id'      => APL_Business_Module_Theme_Option::DISPLAY_BS_TYPE_WITHIN,
            'std'    => '0',
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline bs-dt-to-display',
            'options' => $displayBSTWithin
        );

        $options[] = array(
            'name'      => __( 'What\'s Nearby Map Active ', 'apollo' ),
            'id'        => APL_Business_Module_Theme_Option::WHATS_NEARBY_MAP_ACTIVE,
            'std'       => 1,
            'desc'      => '',
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        /**
         * @ticket #18849: [Business] Modify the URL path - Item 1
         */
        $options[] = array(
            'name'     => __( 'Business category custom slug', 'apollo' ),
            'desc'     =>  __( "The default value is 'business-type'. The custom slug will be replaced to the default value if the slug already exists.", 'apollo' ),
            'id'   => APL_Theme_Option_Site_Config_SubTab::_BUSINESS_TYPE_CUSTOM_SLUG,
            'std'  => Apollo_DB_Schema::_BUSINESS_TYPE,
            'type'     => 'slug',
            'class' => ' no-border inline apl-check-exited-slug',
            'attr' => array(
                'textBanned' => Options_Framework::SYSTEM_SLUGS,
                'textDefault'   => 'business-type'
            ),
        );

        $options[] = array(
            'name'      => __( 'Search', 'apollo' ),
            'type'      => 'header_block'
        );

        /** @Ticket #17979 */
        $options[] = array(
            'name'     => __( 'Keyword placeholder', 'apollo' ),
            'id'   => Apollo_DB_Schema::_APL_BUSINESS_SEARCH_KEYWORD_PLACEHOLDER,
            'std'  => __( 'Search by Keyword', 'apollo' ),
            'type'     => 'text',
            'class' => 'no-border inline'
        );

        $options[] = array(
            'name'      => __( 'Enable Search City', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_BUSINESS_SEARCH_CITY,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1 => __( 'Yes', 'apollo' ),
                0 => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'      => __( 'Enable Search Region', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_BUSINESS_SEARCH_REGION,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1 => __( 'Yes', 'apollo' ),
                0 => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        /** @Ticket #13228 */
        $options[] = array(
            'name'     => __( 'Text Features label', 'apollo' ),
            'id'   => Apollo_DB_Schema::_APL_BUSINESS_SEARCH_WIDGET_LABEL_FEATURES,
            'std'  => __( 'Features', 'apollo' ),
            'type'     => 'text',
            'class' => 'no-border inline'
        );

        $options[] = array(
            'name'      => __( 'Open the Features list', 'apollo' ),
            'id'        => Apollo_DB_Schema::_APL_BUSINESS_SEARCH_WIDGET_OPEN_LIST_FEATURES,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1 => __( 'Yes', 'apollo' ),
                0 => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'      => __( 'Listing Page', 'apollo' ),
            'type'      => 'header_block'
        );

        $options[] = array(
            'name'     => __( 'Number of items per page', 'apollo' ),
            'id'       => APL_Business_Module_Theme_Option::_BUSINESS_NUM_ITEMS_LISTING_PAGE,
            'type'     => 'text',
            'class'    => 'apollo_input_number no-border inline'
        );

        $options[] = array(
            'name'      => __( 'Enable category title', 'apollo' ),
            'id'        => APL_Business_Module_Theme_Option::_ENABLE_BUSINESS_TAXONOMY_TITLE,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1 => __( 'Yes', 'apollo' ),
                0 => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        $business_view_type = array(
            'default'    => __( 'Default', 'apollo' ),
            'list2'      => __( 'List 2', 'apollo' ),
        );
        $options[] = array(
            'name'       => __( 'View Type', 'apollo' ),
            'desc'       => '',
            'id'      => APL_Business_Module_Theme_Option::_BUSINESS_VIEW_TYPE,
            'std'    => 'default',
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => $business_view_type
        );

        $business_default_view_type = array(
            '1'    => __( 'Tile (default)', 'apollo' ),
            '2'      => __( 'List', 'apollo' ),
        );
        $options[] = array(
            'name'       => __( 'Default View Type', 'apollo' ),
            'desc'       => '',
            'id'      => APL_Business_Module_Theme_Option::_BUSINESS_DEFAULT_VIEW_TYPE,
            'std'    => '1',
            'preview'   => true,
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => $business_default_view_type
        );


        $options[] = array(
            'name'      => __( 'Enable Spotlight', 'apollo' ),
            'id'        => APL_Business_Module_Theme_Option::_BUSINESS_ENABLE_SPOT,
            'std'       => 1,
            'desc'      => '',
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        /**
         * @ticket #18515: Add the options allow control the transition speed of the slides.
         */
        $options[] = array(
            'name'     => __( 'SlideShow Speed', 'apollo' ),
            'id'   => Apollo_DB_Schema::_APL_BUSINESS_SLIDESHOW_SPEED,
            'std'  =>  Apollo_DB_Schema::_APL_SLIDESHOW_SPEED_DEFAULT,
            'desc' => __('Set the speed of the slideshow cycling, in milliseconds, minimum = 2000', 'apollo'),
            'type'     => 'number',
            'class' => 'no-border inline apl-slideshow-speed',
            'attr'    => "min = 2000",
            'wplm'  => true,
        );

        // Spotlight type
        $spot_type = array(
            'small'    => __( 'Small', 'apollo' ),
            'large'     => __( 'Full screen', 'apollo' ),
        );

        $options[] = array(
            'name'       => __( 'Spotlight Type', 'apollo' ),
            'id'        => Apollo_DB_Schema::_SPOT_TYPE_BUSINESS,
            'std'       => 'small',
            'type'       => 'select',
            'desc'  => __('This spotlight will be displayed in the business category page (directory page)', 'apollo'),
            'class'   => 'apollo_input_number no-border inline bs-dt-to-display',
            'options' => $spot_type
        );

        $options[] = array(
            'name'     => __( 'Total spotlight items', 'apollo' ),
            'desc'     => sprintf( __( 'Default %s spotlight items', 'apollo' ), 10 ),
            'id'   => Apollo_DB_Schema::_NUM_BUSINESS_SPOTLIGHT,
            'std'  => Apollo_Display_Config::NUM_BUSINESS_SPOTLIGHT,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );

        /** @Ticket #13130 */

        $semiTransparentTypes = array(
            'below'  => __( 'Below the Spotlight graphic', 'apollo' ),
            'def'     => __( 'Default', 'apollo' ),
        );
        // Semi-transparent Type
        $options[] = array(
            'name'       => __( 'Semi-transparent Type', 'apollo' ),
            'id'        => Apollo_DB_Schema::_APL_BUSINESS_SEMI_TRANSPARENT_TYPE,
            'std'       => 'below',
            'type'      => 'select',
            'options'   => $semiTransparentTypes,
            'class'     => 'no-border inline ignore-preview-event',
        );

        // Semi-transparent Background color
        $options[] = array(
            'name'       => __( 'Semi-transparent background color', 'apollo' ),
            'id'        => Apollo_DB_Schema::_APL_BUSINESS_SEMI_TRANSPARENT_TYPE_BACKGROUND_COLOR,
            'type'      => 'color',
            'std'       => '#000000',
            'class'     => 'no-border apollo_input_url apollo_input_url_newline',
        );

        /** @Ticket #13627 */
        $options[] = array(
            'name'      => __( 'Enable Discount Button', 'apollo' ),
            'id'        => Apollo_DB_Schema::_APL_BUSINESS_ENABLE_DISCOUNT_BUTTON,
            'std'       => 0,
            'desc'      => '',
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'      => __( 'Detail Page & Search Widget', 'apollo' ),
            'type'      => 'header_block'
        );


        $options[] = array(
            'name'      => __( 'Enable the Feature Section', 'apollo' ),
            'id'        => APL_Business_Module_Theme_Option::_BUSINESS_ENABLE_FEATURE_SEC,
            'std'       => 1,
            'desc'      => '',
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        /** @ticket #12061*/
        // Primary image type
        $primary_img_type = array(
            'default'    => __( 'Default', 'apollo' ),
            'full'     => __( 'Full width of body area', 'apollo' ),
        );

        $options[] = array(
            'name'       => __( 'Primary image type, include slider', 'apollo' ),
            'id'        => Apollo_DB_Schema::_PRIMARY_IMAGE_TYPE_BUSINESS,
            'std'       => 'default',
            'type'       => 'select',
            'class'   => 'no-border inline bs-dt-to-display',
            'options' => $primary_img_type
        );

        // Feature
        $feature = array(
            'default'    => __( 'Default', 'apollo' ),
            'simple'     => __( 'Simple', 'apollo' ),
        );

        $options[] = array(
            'name'       => __( 'Feature content style', 'apollo' ),
            'id'        => Apollo_DB_Schema::_FEATURE_BUSINESS,
            'std'       => 'default',
            'type'       => 'select',
            'class'   => 'no-border inline bs-dt-to-display',
            'options' => $feature
        );

        $options[] = array(
            'name'       => __( 'Block label style', 'apollo' ),
            'id'        => Apollo_DB_Schema::_BLOCK_LABEL_STYLE_BUSINESS,
            'std'       => 'default',
            'type'       => 'select',
            'class'   => 'no-border inline bs-dt-to-display',
            'options' =>  array(
                'default'    => __( 'Default', 'apollo' ),
                'simple'     => __( 'Simple', 'apollo' ),
            )
        );

        /** @Ticket #13627 */
        $options[] = array(
            'name'       => __( 'Icon position', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_BUSINESS_SHOW_ICON_POSITION,
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => array(
                'after_thumbs_up'   => __( 'After the thumbs up', 'apollo' ),
                'after_title'      => __( 'After the title', 'apollo' ),
            )
        );


        /*@ticket #17624 [CF] 20180920 - [Business Detail] Add options to Turn 'On/Off' both block 'Upcoming Events' and 'Nearby Events'*/
        $options[] = array(
            'name'      => __( 'Enable Upcoming Events ', 'apollo' ),
            'id'        => APL_Business_Module_Theme_Option::_BUSINESS_ENABLE_UPCOMING_EVENTS,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        /*@ticket #17624 [CF] 20180920 - [Business Detail] Add options to Turn 'On/Off' both block 'Upcoming Events' and 'Nearby Events'*/
        $options[] = array(
            'name'      => __( 'Enable Nearby Events', 'apollo' ),
            'id'        => APL_Business_Module_Theme_Option::_BUSINESS_ENABLE_NEARBY_EVENTS,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        /*@ticket #17907: [CF] 20181009 - Display business form fields on the FE ORG form*/
        $options[] = array(
            'name'      => __( 'FE Form', 'apollo' ),
            'type'      => 'header_block'
        );

        $options[] = array(
            'name'      => __( 'Display all business form fields', 'apollo' ),
            'id'        => APL_Business_Module_Theme_Option::_BUSINESS_FE_DISPLAY_ALL_FIELDS,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        /*@ticket #18085 [CF] 20181030 - [Business] Label options - item 2, 3*/
        $options[] = array(
            'name'  =>  __( 'Business Services Label ', 'apollo' ),
            'id'    => APL_Business_Module_Theme_Option::_BUSINESS_SERVICES_LABEL,
            'std'   => __( 'Business Services', 'apollo' ),
            'type'  => 'text',
            'class' => 'no-border inline'
        );

        // close the tab content
        $options[] = array(
            'type'        => 'sub_tab_content_closed'
        );

        return $options;
    }

}

new Apollo_Options_Business();

endif;