<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Apollo_Options_Venue' ) ) :

    /**
     * Description of Apollo_Options_Venue
     *
     * @author truonghn
     */
    class Apollo_Options_Venue {

        public function __construct() {
            if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_VENUE_PT ) ) {
                return;
            }
            add_filter('of_options_first', array( $this, 'getOptions' ), 5);
        }

        public function getOptions($opts) {

            $options = empty($opts) ? array() : $opts;

            // open the tab content
            $options[] = array(
                'tab-id'        => APL_Theme_Option_Site_Config_SubTab::VENUE_SETTING,
                'type'        => 'sub_tab_content_opened',
            );

            $options[] = array(
                'name'      => __( 'General', 'apollo' ),
                'type'      => 'header_block'
            );

            /**
             * @author vulh
             * @ticket #19027 - 0002522: wpdev54 Customization - Custom venue slug
             */
            $options[] = array(
                'name'     => __( 'Custom single & landing page slug', 'apollo' ),
                'desc'     =>  __( "The default value is 'venue'", 'apollo' ),
                'id'   => APL_Theme_Option_Site_Config_SubTab::VENUE_CUSTOM_SLUG,
                'std'  => 'venue',
                'type'     => 'slug',
                'class' => ' no-border inline'
            );

            $options[] = array(
                'name'     => __( 'Custom venue type slug', 'apollo' ),
                'desc'     =>  __( "The default value is 'venue-type'", 'apollo' ),
                'id'   => APL_Theme_Option_Site_Config_SubTab::VENUE_TYPE_CUSTOM_SLUG,
                'std'  => 'venue-type',
                'type'     => 'slug',
                'class' => ' no-border inline apl-check-exited-slug',
                'attr' => array(
                    'textBanned' => Options_Framework::SYSTEM_SLUGS,
                    'textDefault'   => 'venue-type'
                ),
            );

            /**
             * @author vulh
             * @ticket #19027 - 0002522: wpdev54 Customization - Custom venue slug
             */
            $options[] = array(
                'name'     => __( 'Custom label', 'apollo' ),
                'id'       => APL_Theme_Option_Site_Config_SubTab::VENUE_CUSTOM_LABEL,
                'type'     => 'text',
                'desc'     =>  __( "The default value is 'Venue'", 'apollo' ),
                'std'   => __("Venue", "apollo"),
                'class'    => 'no-border inline'
            );

            // Bypass Approval Process
            $options[] = array(
                'name'      => __( 'Bypass Approval', 'apollo' ),
                'id'        => Apollo_DB_Schema::_ENABLE_VENUE_BYPASS_APPROVAL_PROCESS,
                'std'       => 0,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border',
            );

            /**
             * @ticket #18657: Change the layout structure for Artist, Organization, Venue
             */
            $ocNumberDescription = apply_filters('apl_add_number_of_character_description', array(), Apollo_DB_Schema::_VENUE_PT);
            if (!empty($ocNumberDescription)) {
                $options = array_merge($options, $ocNumberDescription);
            }

            /**
             * ThienLD : add more settings to serve ticket #12725
            */
            $options[] = array(
                'name'      => __( 'FE Venue Form', 'apollo' ),
                'type'      => 'header_block'
            );

            // Enable required for FE venue primary image
            $options[] = array(
                'name'      => __(  "Enable requirement for Venue's primary image", "apollo" ),
                'id'        => Apollo_DB_Schema::_VENUE_ENABLE_REQUIRED_PRIMARY_IMAGE,
                'std'       => 0,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border',
            );

            /** @Ticket #13643 */
            $options[] = array(
                'name'      => __( 'Enable Requirement fields', 'apollo' ),
                'type'      => 'header_block'
            );

            $venueReqFields = array(
                Apollo_DB_Schema::_VENUE_ENABLE_ADDRESS_REQUIREMENT => __( 'Address', 'apollo' ),
                Apollo_DB_Schema::_VENUE_ENABLE_CITY_REQUIREMENT => __( 'City', 'apollo' ),
                APL_Theme_Option_Site_Config_SubTab::_VENUE_ENABLE_NEIGHBORHOOD_REQUIREMENT => __( 'Neighborhood', 'apollo' ),
                Apollo_DB_Schema::_VENUE_ENABLE_EMAIL_REQUIREMENT => __( 'Email', 'apollo' ),
                Apollo_DB_Schema::_VENUE_ENABLE_STATE_REQUIREMENT => __( 'State', 'apollo' ),
                Apollo_DB_Schema::_VENUE_ENABLE_PHONE_REQUIREMENT => __( 'Phone', 'apollo' ),
                Apollo_DB_Schema::_VENUE_ENABLE_ZIP_REQUIREMENT => __( 'Zip', 'apollo' ),
            );

            foreach($venueReqFields as $key => $label) {
                $options[] = array(
                    'name'      => $label,
                    'id'        => $key,
                    'std'       => 0,
                    'type'      => 'radio',
                    'options'   => array(
                        1           => __( 'Yes', 'apollo' ),
                        0           => __( 'No', 'apollo' ),
                    ),
                    'class'     => 'no-border',
                );
            }

            $options[] = array(
                'name'      => __( 'Search widget', 'apollo' ),
                'type'      => 'header_block'
            );

            /**
             * @author vulh
             * @ticket #19027 - 0002522: wpdev54 Customization - Custom venue slug
             */
            $options[] = array(
                'name'     => __( 'Venue type label', 'apollo' ),
                'id'       => APL_Theme_Option_Site_Config_SubTab::VENUE_WIDGET_SEARCH_VENUE_TYPE_LABEL,
                'type'     => 'text',
                'std'   => __("Venue Type", "apollo"),
                'class'    => 'no-border inline'
            );

            //Search region
            $options[] = array(
                'name'      => __( 'Enable region', 'apollo' ),
                'id'        => Apollo_DB_Schema::_APL_VENUE_WIDGET_SEARCH_ENABLE_REGION,
                'std'       => 1,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border',
            );

            //Search state
            $options[] = array(
                'name'      => __( 'Enable state', 'apollo' ),
                'id'        => Apollo_DB_Schema::_APL_VENUE_WIDGET_SEARCH_ENABLE_STATE,
                'std'       => 1,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border',
            );

            //Search zip
            $options[] = array(
                'name'      => __( 'Enable zip code', 'apollo' ),
                'id'        => Apollo_DB_Schema::_APL_VENUE_WIDGET_SEARCH_ENABLE_ZIP,
                'std'       => 1,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border',
            );

            $options[] = array(
                'name'      => __( 'Listing Page', 'apollo' ),
                'type'      => 'header_block'
            );


            $options[] = array(
                'name'     => __( 'Number of items per page', 'apollo' ),
                'id'       => Apollo_DB_Schema::_VENUE_NUM_ITEMS_LISTING_PAGE,
                'type'     => 'text',
                'class'    => 'apollo_input_number no-border inline'
            );

            $venue_default_view_type = array(
                '1'    => __( 'Tile (default)', 'apollo' ),
                '2'      => __( 'List', 'apollo' ),
            );
            $options[] = array(
                'name'       => __( 'Default View Type', 'apollo' ),
                'desc'       => '',
                'id'      => Apollo_DB_Schema::_VENUE_DEFAULT_VIEW_TYPE,
                'std'    => '1',
                'preview'   => true,
                'type'       => 'select',
                'class'   => 'apollo_input_number no-border inline',
                'options' => $venue_default_view_type
            );


            $currentTheme = function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '';

            if ( $currentTheme == APL_SM_DIR_THEME_NAME) {

                $options[] = array(
                    'name'      => __( 'Add bookmark Title', 'apollo' ),
                    'type'      => 'header_block'
                );

                // Enable Facebook Enable
                $options[] = array(
                    'name'      => __( 'Enable ADD IT button', 'apollo' ),
                    'id'        => Apollo_DB_Schema::_VENUE_ENABLE_ADD_IT_BTN,
                    'std'       => 1,
                    'type'      => 'radio',
                    'options'   => array(
                        1           => __( 'Yes', 'apollo' ),
                        0           => __( 'No', 'apollo' ),
                    ),
                    'class'     => 'no-border apollo_input_url',
                );

                $options[] = array(
                    'name'     => __( 'Text of ADD IT Button', 'apollo' ),
                    'id'   => Apollo_DB_Schema::_VENUE_TEXT_OF_ADD_IT,
                    'std'  => __('ADD IT', 'apollo'),
                    'type'     => 'text',
                    'class' => 'no-border inline',
                    'wplm'  => true
                );

                $options[] = array(
                    'name'     => __( 'Text of ADDED Button', 'apollo' ),
                    'id'   => Apollo_DB_Schema::_VENUE_TEXT_OF_ADDED_IT,
                    'std'  => __('ADDED', 'apollo'),
                    'type'     => 'text',
                    'class' => 'no-border inline',
                    'wplm'  => true
                );

            }

            // close the tab content
            $options[] = array(
                'type'        => 'sub_tab_content_closed'
            );

            return $options;
        }
    }

    new Apollo_Options_Venue();

endif;