<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Apollo_Options_Syndication' ) ) :

    /**
     * Description of Apollo_Options_Syndication
     *
     * @author truonghn
     */
    class Apollo_Options_Syndication {

        public function __construct() {

            if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_SYNDICATION_PT ) ) {
                return;
            }
            add_filter('of_options_first', array( $this, 'getOptions' ), 5);
        }

        public function getOptions($opts) {

            $options = empty($opts) ? array() : $opts;

            // open the tab content
            $options[] = array(
                'tab-id'        => APL_Theme_Option_Site_Config_SubTab::SYNDICATION_SETTING,
                'type'        => 'sub_tab_content_opened',
            );


            $options[] = array(
                'name'     => __( 'Expiry time (hours)', 'apollo' ),
                'desc'     => sprintf(__('The default value is %s hours','apollo'), Apollo_Display_Config::APL_SYNDICATION_EXPIRED_TIME),
                'id'   => Apollo_DB_Schema::_SYNDICATION_EXPIRED_TIME,
                'std'  => Apollo_Display_Config::APL_SYNDICATION_EXPIRED_TIME,
                'type'     => 'text',
                'class' => 'apollo_input_number no-border inline'
            );

            // close the tab content
            $options[] = array(
                'type'        => 'sub_tab_content_closed'
            );

            return $options;
        }
    }

    new Apollo_Options_Syndication();

endif;