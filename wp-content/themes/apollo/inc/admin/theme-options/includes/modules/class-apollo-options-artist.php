<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Apollo_Options_Artist' ) ) :

/**
 * Description of Apollo_Options_Artist
 *
 * @author thienld
 */
class Apollo_Options_Artist {

    public function __construct() {
        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ARTIST_PT ) ) {
            return;
        }
        add_filter('of_options_first', array( $this, 'getOptions' ), 5);
    }

    public function getOptions($opts) {

        $options = empty($opts) ? array() : $opts;

        // open the tab content
        $options[] = array(
            'tab-id'        => APL_Theme_Option_Site_Config_SubTab::ARTIST_SETTING,
            'type'        => 'sub_tab_content_opened',
        );

        $options[] = array(
            'name'      => __( 'General', 'apollo' ),
            'type'      => 'header_block'
        );

        /*@ticket #17123 */
        $options[] = array(
            'name'       => __( 'Slug' ),
            'id'        => Apollo_DB_Schema::_ARTIST_CUSTOM_SLUG,
            'std'       => __( 'artist', 'apollo' ),
            'type'      => 'slug',
            'class' => 'no-border inline',
        );

        /*@ticket #17123 */
        $options[] = array(
            'name'       => __( 'Single Label' ),
            'id'        => Apollo_DB_Schema::_ARTIST_CUSTOM_SINGLE_LABEL,
            'std'       => __( 'Artist', 'apollo' ),
            'type'      => 'text',
            'class' => 'no-border inline',
        );

        /*@ticket #17123 */
        $options[] = array(
            'name'       => __( ' Plural Label', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ARTIST_CUSTOM_LABEL,
            'std'       => __( 'Artists', 'apollo' ),
            'type'      => 'text',
            'class' => 'no-border inline',
        );

        // Enable Facebook Enable
        $options[] = array(
            'name'       => __( 'More about artist title', 'apollo' ),
            'id'        => Apollo_DB_Schema::_TEMP_ARTIST_MORE_ABOUT,
            'std'       => __( 'MORE ABOUT THIS ARTIST', 'apollo' ),
            'type'      => 'text',
            'class' => 'no-border inline',
        );

        // Bypass Approval Process
        $options[] = array(
            'name'      => __( 'Bypass Approval', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_ARTIST_BYPASS_APPROVAL_PROCESS,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'       => __( 'Activate Member Field', 'apollo' ),
            'id'      => Apollo_DB_Schema::_ARTIST_ACTIVE_MEMBER_FIELD,
            'std'    => 0,
            'type'       => 'radio',
            'class'     => 'no-border',
            'desc'  => __('If the "YES" value of this field is selected then ONLY artists that are checked "Member" on the admin detail forms, all fields are available from the FE form. In contrast, only first name, last name, email, phone, city, artist category types, and primary image are available.', 'apollo'),
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
        );
        
        // Enable Individual Dates and Times
        $options[] = array(
            'name'      => __( 'Enable County in the FE Form', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ARTIST_ENABLE_COUNTY,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        /**
         * @ticket #18657: Change the layout structure for Artist, Organization, Venue
        */
        $ocNumberDescription = apply_filters('apl_add_number_of_character_description', array(), Apollo_DB_Schema::_ARTIST_PT);
        if (!empty($ocNumberDescription)) {
            $options = array_merge($options, $ocNumberDescription);
        }

        $options[] = array(
            'name'      => __( 'Artist type label', 'apollo' ),
            'id'        => APL_Theme_Option_Site_Config_SubTab::ARTIST_TYPE_SEARCH_WIDGET_LABEL,
            'std'       => __("Artist Type", 'apollo'),
            'type'      => 'text',
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'      => __( 'Enable this field', 'apollo' ),
            'id'        => APL_Theme_Option_Site_Config_SubTab::ARTIST_ENABLE_TYPE_SEARCH_WIDGET,
            'std'       => 1,
            'type'      => 'radio',
            'desc'      => __('Apply to the widget search', 'apollo'),
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border font-w-normal-heading',
        );

        $options[] = array(
            'name'      => __( 'Artist style label', 'apollo' ),
            'id'        => APL_Theme_Option_Site_Config_SubTab::ARTIST_STYLE_SEARCH_WIDGET_LABEL,
            'std'       => __("Artist Style", 'apollo'),
            'type'      => 'text',
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'      => __( 'Enable this field', 'apollo' ),
            'id'        => APL_Theme_Option_Site_Config_SubTab::ARTIST_ENABLE_STYLE_SEARCH_WIDGET,
            'std'       => 1,
            'type'      => 'radio',
            'desc'      => __('Apply to the FE form, FE detail, Widget search', 'apollo'),
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border font-w-normal-heading',
        );

        $options[] = array(
            'name'      => __( 'Artist medium label', 'apollo' ),
            'id'        => APL_Theme_Option_Site_Config_SubTab::ARTIST_MEDIUM_SEARCH_WIDGET_LABEL,
            'std'       => __("Artist Medium", 'apollo'),
            'type'      => 'text',
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'      => __( 'Enable this field', 'apollo' ),
            'id'        => APL_Theme_Option_Site_Config_SubTab::ARTIST_ENABLE_MEDIUM_SEARCH_WIDGET,
            'std'       => 1,
            'type'      => 'radio',
            'desc'      => __('Apply to the FE form, FE detail, Widget search', 'apollo'),
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border font-w-normal-heading',
        );

        /*@ticket #18310: 0002510: Artist Directory Plugin > New Artist option*/
        $options[] = array(
            'name'      => __( 'Admin Artist Form', 'apollo' ),
            'type'      => 'header_block'
        );

        $options[] = array(
            'name'      => __(  "Enable New Artist option", "apollo" ),
            'id'        => APL_Theme_Option_Site_Config_SubTab::_ARTIST_ENABLE_OPTION_NEW_ARTIST,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        /**
         * ThienLD : add more settings to serve ticket #12725
         */
        $options[] = array(
            'name'      => __( 'FE Artist Form', 'apollo' ),
            'type'      => 'header_block'
        );


        /**
         * @ticket #18695: [CF] 20181221 - Add the option for image upload to the main artist profile form.
         */
        $artistPhotoLocation = array(
            1    => __( 'Photo Sub menu (Default)', 'apollo' ),
            2      => __( 'Inner Artist Profile Form', 'apollo' ),
        );

        $options[] = array(
            'name'       => __( 'Location of section Artist Photo', 'apollo' ),
            'desc'       => '',
            'id'      => APL_Theme_Option_Site_Config_SubTab::_ARTIST_PHOTO_FORM_LOCATION,
            'std'    => 1,
            'type'       => 'select',
            'class'   => 'no-border inline',
            'options' => $artistPhotoLocation
        );

        // Enable required for FE artist primary image
        $options[] = array(
            'name'      => __(  "Enable requirement for Artist's primary image", "apollo" ),
            'id'        => Apollo_DB_Schema::_ARTIST_ENABLE_REQUIRED_PRIMARY_IMAGE,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        /** @Ticket #18747 */
        $ratioOptions = array(
            1           => __( 'Yes', 'apollo' ),
            0           => __( 'No', 'apollo' ),
        );

        $options[] = array(
            'name'      => __(  "Enable requirement Artist description", "apollo" ),
            'id'        => Apollo_DB_Schema::_ARTIST_FE_ENABLE_REQUIRED_DESCRIPTION,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => $ratioOptions,
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'      => __(  "Enable requirement Artist category type", "apollo" ),
            'id'        => Apollo_DB_Schema::_ARTIST_FE_ENABLE_REQUIRED_TYPE,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => $ratioOptions,
            'class'     => 'no-border',
        );

        /** @Ticket #19640 */
        $options[] = array(
            'name'      => __( 'Enable other State field', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ARTIST_FE_ENABLE_OTHER_STATE,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border apollo_input_url',
        );
        $options[] = array(
            'name'      => __( 'Enable other City field', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ARTIST_FE_ENABLE_OTHER_CITY,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border apollo_input_url',
        );

        $options[] = array(
            'name'      => __( 'Enable other Zip Code field', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ARTIST_FE_ENABLE_OTHER_ZIP,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border apollo_input_url',
        );


        /** @Ticket #16287 */
        $options[] = array(
            'name'     => __( 'Max upload PDFs', 'apollo' ),
            'desc'     => sprintf(__( 'default %s PDFs', 'apollo' ), Apollo_Display_Config::_MAX_UPLOAD_PDF_DEFAULT),
            'id'   => Apollo_DB_Schema::_MAX_UPLOAD_PDF_ARTIST,
            'std'  => Apollo_Display_Config::_MAX_UPLOAD_PDF_DEFAULT,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );

        /**
         * Max character
         * @ticket #17181
         * @author vulh
         */
        $options[] = array(
            'name'      => __( '-- Max characters', 'apollo' ),
            'type'      => 'header_block'
        );
        $options[] = array(
            'name'     => __( "Other artist types", 'apollo' ),
            'std'   => APL_Theme_Option_Site_Config_SubTab::ARTIST_FORM_MAX_OTHER_DEFAULT,
            'desc'     => sprintf(__( 'default %s characters', 'apollo' ), APL_Theme_Option_Site_Config_SubTab::ARTIST_FORM_MAX_OTHER_DEFAULT),
            'id'   => APL_Theme_Option_Site_Config_SubTab::ARTIST_FORM_MAX_OTHER_TYPE,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );
        $options[] = array(
            'name'     => __( "Other artist style", 'apollo' ),
            'std'   => APL_Theme_Option_Site_Config_SubTab::ARTIST_FORM_MAX_OTHER_DEFAULT,
            'desc'     => sprintf(__( 'default %s characters', 'apollo' ), APL_Theme_Option_Site_Config_SubTab::ARTIST_FORM_MAX_OTHER_DEFAULT),
            'id'   => APL_Theme_Option_Site_Config_SubTab::ARTIST_FORM_MAX_OTHER_STYLE,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );
        $options[] = array(
            'name'     => __( "Other artist medium", 'apollo' ),
            'std'   => APL_Theme_Option_Site_Config_SubTab::ARTIST_FORM_MAX_OTHER_DEFAULT,
            'desc'     => sprintf(__( 'default %s characters', 'apollo' ), APL_Theme_Option_Site_Config_SubTab::ARTIST_FORM_MAX_OTHER_DEFAULT),
            'id'   => APL_Theme_Option_Site_Config_SubTab::ARTIST_FORM_MAX_OTHER_MEDIUM,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );

        /** @Ticket #17721 0002366: wpdev19 - [Dashboard] Adjust the artist form - item 7, 8, 9 */
        $options[] = array(
            'name'     => __( 'Description field', 'apollo' ),
            'desc'     => sprintf(__( 'default %s characters', 'apollo' ), APL_Theme_Option_Site_Config_SubTab::ARTIST_FORM_MAX_DESCRIPTION_DEFAULT),
            'id'   => APL_Theme_Option_Site_Config_SubTab::ARTIST_FORM_MAX_DESCRIPTION_FIELD,
            'std'  => APL_Theme_Option_Site_Config_SubTab::ARTIST_FORM_MAX_DESCRIPTION_DEFAULT,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );


        // Enable requirement  for the "County" field
        $options[] = array(
            'name'      => __( '-- Enable Requirement fields', 'apollo' ),
            'type'      => 'header_block'
        );

        $artistReqFields = array(
            Apollo_DB_Schema::_ARTIST_ENABLE_ADDRESS_REQUIREMENT => __( 'Address', 'apollo' ),
            Apollo_DB_Schema::_ARTIST_ENABLE_COUNTY_REQUIREMENT => __( 'County', 'apollo' ),
            Apollo_DB_Schema::_ARTIST_ENABLE_CITY_REQUIREMENT => __( 'City', 'apollo' ),
            Apollo_DB_Schema::_ARTIST_ENABLE_EMAIL_REQUIREMENT => __( 'Email', 'apollo' ),
            Apollo_DB_Schema::_ARTIST_ENABLE_STATE_REQUIREMENT => __( 'State', 'apollo' ),
            Apollo_DB_Schema::_ARTIST_ENABLE_PHONE_REQUIREMENT => __( 'Phone', 'apollo' ),
            Apollo_DB_Schema::_ARTIST_ENABLE_ZIP_REQUIREMENT => __( 'Zip', 'apollo' ),
        );

        foreach($artistReqFields as $key => $label) {
            $options[] = array(
                'name'      => $label,
                'id'        => $key,
                'std'       => 0,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border',
            );
        }

        /*@ticket #17168 */
        $options[] = array(
            'name'      => __( '-- Artist Label', 'apollo' ),
            'type'      => 'header_block'
        );

        /*@ticket #17719 */
        $options[] = array(
            'name'       => __( 'Artist menu label', 'apollo' ),
            'id'        =>  Apollo_DB_Schema::_ARTIST_MENU_LABEL,
            'std'       => __( 'ARTIST', 'apollo' ),
            'type'      => 'text',
            'class' => 'no-border inline',
        );

        /*@ticket #17719 */
        $options[] = array(
            'name'       => __( 'Artist profile title', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ARTIST_PROFILE_TITLE,
            'std'       => __( 'Artist Information', 'apollo' ),
            'type'      => 'text',
            'class' => 'no-border inline',
        );

        $options[] = array(
            'name'       => __( 'Artist email label', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ARTIST_EMAIL_LABEL,
            'std'       => __( 'Check here if you DO NOT want your Email to appear on your Artist Profile page.', 'apollo' ),
            'type'      => 'text',
            'class' => 'no-border inline',
        );

        $options[] = array(
            'name'       => __( 'Artist phone label', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ARTIST_PHONE_LABEL,
            'std'       => __( 'Check here if you DO NOT want your Phone Number to appear on your Artist Profile page.', 'apollo' ),
            'type'      => 'text',
            'class' => 'no-border inline',
        );

        $options[] = array(
            'name'       => __( 'Artist address label', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ARTIST_ADDRESS_LABEL,
            'std'       => __( 'Check here if you DO NOT want your Address to appear on your Artist Profile page.', 'apollo' ),
            'type'      => 'text',
            'class' => 'no-border inline',
        );

        $options[] = array(
            'name'       => __( 'Artist PDF label', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ARTIST_FORM_PDF_LABEL,
            'std'       => __( 'Resume and/or Work Samples (PDF)', 'apollo' ),
            'type'      => 'text',
            'class' => 'no-border inline',
        );

        $options[] = array(
            'name'       => __( 'Bio statement type', 'apollo' ),
            'desc'       => '',
            'id'      =>  Apollo_DB_Schema::_ARTIST_BIO_STATEMENT_TYPE,
            'std'    => 0,
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => array(
                0     => __( 'WYSIWYG editor', 'apollo' ),
                1     => __( 'Plain text', 'apollo' ),
            )
        );
        /*end @ticket 17168 */

        //Search widget
        $options[] = array(
            'name'      => __( 'Search widget', 'apollo' ),
            'type'      => 'header_block'
        );
        //zip
        $options[] = array(
            'name'      => __( 'Enable zip', 'apollo' ),
            'id'        => Apollo_DB_Schema::_APL_ENABLE_ARTIST_WIDGET_SEARCH_ZIP,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        //city
        $options[] = array(
            'name'      => __( 'Enable city', 'apollo' ),
            'id'        => Apollo_DB_Schema::_APL_ENABLE_ARTIST_WIDGET_SEARCH_CITY,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'      => __( 'Listing Page', 'apollo' ),
            'type'      => 'header_block'
        );

        //@ticket #17123
        $options[] = array(
            'name'       => __( 'Featured label' ),
            'id'        => Apollo_DB_Schema::_ARTIST_FEATURED_LABEL,
            'std'       => __( 'Featured Creatives', 'apollo' ),
            'type'      => 'text',
            'class' => 'no-border inline',
        );

        //@ticket #17123
        $options[] = array(
            'name'       => __( 'Normal label' ),
            'id'        => Apollo_DB_Schema::_ARTIST_NORMAL_LABEL,
            'std'       => __( 'More Creatives', 'apollo' ),
            'type'      => 'text',
            'class' => 'no-border inline',
        );

        //@ticket #17717: 0002366: wpdev19 - [Artist] modify the "More Creatives" tab label to "All Creatives", all artist profiles (member or not member) will appear in this list - item 2
        $options[] = array(
            'name'       => __( 'Filter more artists member tab' ),
            'id'        => Apollo_DB_Schema::_FILTER_MORE_ARTIST_MEMBER_LABEL,
            'std'       => __( 'normal', 'apollo' ),
            'type'      => 'radio',
            'options'   => array(
                'normal'           => __( 'Only artists are not member', 'apollo' ),
                'all'           => __( 'All artists', 'apollo' ),
            ),
            'class' => 'no-border inline',
        );

        $options[] = array(
            'name'     => __( 'Number of items per page', 'apollo' ),
            'id'       => Apollo_DB_Schema::_ARTIST_NUM_ITEMS_LISTING_PAGE,
            'type'     => 'text',
            'class'    => 'apollo_input_number no-border inline'
        );


        $artist_default_view_type = array(
            '1'    => __( 'Tile (default)', 'apollo' ),
            '2'      => __( 'List', 'apollo' ),
        );
        $options[] = array(
            'name'       => __( 'Default View Type', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_ARTIST_DEFAULT_VIEW_TYPE,
            'std'    => '1',
            'preview'   => true,
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => $artist_default_view_type
        );

        $order_options = array(
            'ALPHABETICAL_ASC'      => __( 'Last Name Asc', 'apollo' ),
            'ALPHABETICAL_DESC'      => __( 'Last Name Desc', 'apollo' ),
        );

        $options[] = array(
            'name'       => __( 'Artist Listing Order', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_ARTIST_LISTING_ORDER,
            'std'    => 'ALPHABETICAL_ASC',
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => $order_options
        );

        $currentTheme = function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '';

        if ( $currentTheme == APL_SM_DIR_THEME_NAME) {

            $options[] = array(
                'name'      => __( 'Add Bookmark Title', 'apollo' ),
                'type'      => 'header_block'
            );

            // Enable Facebook Enable
            $options[] = array(
                'name'      => __( 'Enable ADD IT button', 'apollo' ),
                'id'        => Apollo_DB_Schema::_ARTIST_ENABLE_ADD_IT_BTN,
                'std'       => 1,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border apollo_input_url',
            );

            $options[] = array(
                'name'     => __( 'Text of ADD IT Button', 'apollo' ),
                'id'   => Apollo_DB_Schema::_ARTIST_TEXT_OF_ADD_IT,
                'std'  => __('ADD IT', 'apollo'),
                'type'     => 'text',
                'class' => 'no-border inline',
                'wplm'  => true
            );

            $options[] = array(
                'name'     => __( 'Text of ADDED Button', 'apollo' ),
                'id'   => Apollo_DB_Schema::_ARTIST_TEXT_OF_ADDED_IT,
                'std'  => __('ADDED', 'apollo'),
                'type'     => 'text',
                'class' => 'no-border inline',
                'wplm'  => true
            );

        }

        // close the tab content
        $options[] = array(
            'type'        => 'sub_tab_content_closed'
        );

        return $options;
    }
}

new Apollo_Options_Artist();

endif;