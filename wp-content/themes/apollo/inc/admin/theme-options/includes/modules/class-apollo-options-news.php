<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

if (!class_exists('Apollo_Options_News')) :

    /**
     * @ticket #18127: News Module
     * Description of Apollo_Options_News
     */
    class Apollo_Options_News
    {

        public function __construct()
        {
            if (!Apollo_App::is_avaiable_module(Apollo_DB_Schema::_NEWS_PT)) {
                return;
            }
            add_filter('of_options_first', array($this, 'getOptions'), 4);
        }

        public function getOptions($opts)
        {

            $options = empty($opts) ? array() : $opts;

            // open the tab content
            $options[] = array(
                'tab-id' => APL_Theme_Option_Site_Config_SubTab::NEWS_SETTING,
                'type' => 'sub_tab_content_opened',
            );

            $options[] = array(
                'name' => __('General', 'apollo'),
                'type' => 'header_block'
            );

            $options[] = array(
                'name' => __('Number of characters description truncation', 'apollo'),
                'desc' => sprintf(__('%s characters by default', 'apollo'), APL_News_Config::_NEWS_NUM_OF_CHAR_DEFAULT),
                'id' => APL_News_Config::_NEWS_NUM_OF_CHAR,
                'std' => APL_News_Config::_NEWS_NUM_OF_CHAR_DEFAULT,
                'type' => 'text',
                'class' => 'apollo_input_number no-border inline'
            );

            $options[] = array(
                'name' => __('News custom label', 'apollo'),
                'desc' => __("The default value is 'News'", 'apollo'),
                'id' => APL_News_Config::_NEWS_CUSTOM_LABEL,
                'std' => 'News',
                'type' => 'text',
                'class' => ' no-border inline'
            );

            $options[] = array(
                'name' => __('News slug', 'apollo'),
                'desc' => __("The default value is 'news'", 'apollo'),
                'id' => APL_News_Config::_NEWS_CUSTOM_SLUG,
                'std' => 'news',
                'type' => 'slug',
                'class' => ' no-border inline'
            );

            $options[] = array(
                'name' => __('Display', 'apollo'),
                'type' => 'header_block'
            );

            $ListingDisplayType = array(
                'default' => __('Default', 'apollo'),
                'simple' => __('Simple ', 'apollo'),
            );
            $options[] = array(
                'name' => __('News Listing Type', 'apollo'),
                'desc' => '',
                'id' => APL_News_Config::_NEWS_LISTING_TYPE,
                'std' => 'default',
                'type' => 'select',
                'preview' => true,
                'class' => 'apollo_input_number no-border inline',
                'options' => $ListingDisplayType
            );

            $options[] = array(
                'name' => __('News Detail Type', 'apollo'),
                'desc' => '',
                'id' => APL_News_Config::_NEWS_DISPLAY_STYLE,
                'std' => 'default',
                'type' => 'select',
                'preview' => true,
                'class' => 'apollo_input_number no-border inline',
                'options' => array(
                    'default' => __('Default', 'apollo'),
                )
            );

            $newsTypes = array(
                'parent-only' => __('Display parent type only', 'apollo'),
                'child-only' => __('Display child type only', 'apollo'),
                'parent-child' => __('Display both parent and child types', 'apollo'),

            );
            $options[] = array(
                'name' => __('News Types', 'apollo'),
                'desc' => '',
                'id' => APL_News_Config::_NEWS_CATEGORY_TYPES,
                'std' => 'parent-only',
                'type' => 'select',
                'class' => 'apollo_input_number no-border inline',
                'options' => $newsTypes
            );

            $newsOrder = array(
                'title-asc' => __('Title Asc', 'apollo'),
                'title-desc' => __('Title Desc', 'apollo'),
                'date-asc' => __('Publish Date Ascending', 'apollo'),
                'date-desc' => __('Publish Date Descending', 'apollo'),
                'order-asc' => __('Special Order Asc', 'apollo'),
                'order-desc' => __('Special Order Desc', 'apollo'),
            );
            $options[] = array(
                'name' => __('The order of news', 'apollo'),
                'desc' => '',
                'id' => APL_News_Config::_NEWS_CATEGORY_PAGE_ORDER,
                'std' => 'title-asc',
                'type' => 'select',
                'class' => 'apollo_input_number no-border inline',
                'options' => $newsOrder
            );

            // close the tab content
            $options[] = array(
                'type' => 'sub_tab_content_closed'
            );

            return $options;
        }
    }

    new Apollo_Options_News();

endif;