<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Apollo_Options_Classified' ) ) :

    /**
     * Description of Apollo_Options_Public_Art
     *
     * @author truonghn
     */
    class Apollo_Options_Classified {

        public function __construct() {
            if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_CLASSIFIED ) ) {
                return;
            }
            add_filter('of_options_first', array( $this, 'getOptions' ), 5);
        }

        public function getOptions($opts) {

            $options = empty($opts) ? array() : $opts;


            // open the tab content
            $options[] = array(
                'tab-id'        => APL_Theme_Option_Site_Config_SubTab::CLASSIFIED_SETTING,
                'type'        => 'sub_tab_content_opened',
            );


            $options[] = array(
                'name'      => __( 'General', 'apollo' ),
                'type'      => 'header_block'
            );

            // Bypass Approval Process
            $options[] = array(
                'name'      => __( 'Bypass Approval', 'apollo' ),
                'id'        => Apollo_DB_Schema::_ENABLE_CLASSIFIED_BYPASS_APPROVAL_PROCESS,
                'std'       => 0,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border',
            );

            $options[] = array(
                'name'     => __( 'Classified custom label', 'apollo' ),
                'desc'     =>  __( "The default value is 'Classified'", 'apollo' ),
                'id'   => Apollo_DB_Schema::_CLASSIFIED_CUSTOM_LABEL,
                'std'  => 'Classified',
                'type'     => 'text',
                'class' => ' no-border inline'
            );

            $options[] = array(
                'name'     => __( 'Classified custom slug', 'apollo' ),
                'desc'     =>  __( "The default value is 'classified'", 'apollo' ),
                'id'   => Apollo_DB_Schema::_CLASSIFIED_CUSTOM_SLUG,
                'std'  => 'classified',
                'type'     => 'slug',
                'class' => ' no-border inline'
            );

            /** @Ticket #16639 */
            $options[] = array(
                'name'     => __( 'Max upload PDFs', 'apollo' ),
                'desc'     => sprintf(__( 'default %s PDFs', 'apollo' ), Apollo_Display_Config::_MAX_UPLOAD_PDF_DEFAULT),
                'id'   => Apollo_DB_Schema::_MAX_UPLOAD_PDF_CLASSIFIED,
                'std'  => Apollo_Display_Config::_MAX_UPLOAD_PDF_DEFAULT,
                'type'     => 'text',
                'class' => 'apollo_input_number no-border inline'
            );

            $options[] = array(
                'name'      => __( 'Enable requirement for State field', 'apollo' ),
                'id'        => Apollo_DB_Schema::_CLASSIFIED_ENABLE_REQUIREMENTS_STATE,
                'std'       => 1,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border apollo_input_url',
            );

            $options[] = array(
                'name'      => __( 'Enable requirement for City field', 'apollo' ),
                'id'        => Apollo_DB_Schema::_CLASSIFIED_ENABLE_REQUIREMENTS_CITY,
                'std'       => 1,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border apollo_input_url',
            );

            $options[] = array(
                'name'      => __( 'Enable requirement for Zip field', 'apollo' ),
                'id'        => Apollo_DB_Schema::_CLASSIFIED_ENABLE_REQUIREMENTS_ZIP,
                'std'       => 0,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border apollo_input_url',
            );

            $options[] = array(
                'name'      => __( 'Enable requirement for Email field', 'apollo' ),
                'id'        => Apollo_DB_Schema::_CLASSIFIED_ENABLE_REQUIREMENTS_EMAIL,
                'std'       => 0,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border apollo_input_url',
            );

            $options[] = array(
                'name'      => __( 'Listing Page', 'apollo' ),
                'type'      => 'header_block'
            );

            $options[] = array(
                'name'     => __( 'Number of items per page', 'apollo' ),
                'id'       => Apollo_DB_Schema::_CLASSIFIED_NUM_ITEMS_LISTING_PAGE,
                'type'     => 'text',
                'class'    => 'apollo_input_number no-border inline'
            );

            $classified_default_view_type = array(
                '1'    => __( 'Tile ', 'apollo' ),
                '2'      => __( 'List', 'apollo' ),
                '3'      => __( 'Table (default)', 'apollo' ),
            );
            $options[] = array(
                'name'       => __( 'Default View Type', 'apollo' ),
                'desc'       => '',
                'id'      => Apollo_DB_Schema::_CLASSIFIED_DEFAULT_VIEW_TYPE,
                'std'    => '3',
                'preview'   => true,
                'type'       => 'select',
                'class'   => 'apollo_input_number no-border inline',
                'options' => $classified_default_view_type
            );

            $options[] = array(
                'name'      => __( 'Listing Page And Detail page', 'apollo' ),
                'type'      => 'header_block'
            );


            $options[] = array(
                'name'      => __( 'Enable Primary Image', 'apollo' ),
                'id'        => Apollo_DB_Schema::_CLASSIFIED_ENABLE_PRIMARY_IMAGE,
                'std'       => 1,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border apollo_input_url',
            );

            $options[] = array(
                'name'      => __( 'Front-end Form ', 'apollo' ),
                'type'      => 'header_block'
            );

            $options[] = array(
                'name'      => __( 'Enable other State field', 'apollo' ),
                'id'        => Apollo_DB_Schema::_CLASSIFIED_FE_ENABLE_OTHER_STATE,
                'std'       => 0,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border apollo_input_url',
            );
            $options[] = array(
                'name'      => __( 'Enable other City field', 'apollo' ),
                'id'        => Apollo_DB_Schema::_CLASSIFIED_FE_ENABLE_OTHER_CITY,
                'std'       => 0,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border apollo_input_url',
            );

            $options[] = array(
                'name'      => __( 'Enable other Zip Code field', 'apollo' ),
                'id'        => Apollo_DB_Schema::_CLASSIFIED_FE_ENABLE_OTHER_ZIP,
                'std'       => 0,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border apollo_input_url',
            );

            $currentTheme = function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '';

            if ( $currentTheme == APL_SM_DIR_THEME_NAME) {

                $options[] = array(
                    'name'      => __( 'Add Bookmark  Title', 'apollo' ),
                    'type'      => 'header_block'
                );

                // Enable Facebook Enable
                $options[] = array(
                    'name'      => __( 'Enable ADD IT button', 'apollo' ),
                    'id'        => Apollo_DB_Schema::_CLASSIFIED_ENABLE_ADD_IT_BTN,
                    'std'       => 1,
                    'type'      => 'radio',
                    'options'   => array(
                        1           => __( 'Yes', 'apollo' ),
                        0           => __( 'No', 'apollo' ),
                    ),
                    'class'     => 'no-border apollo_input_url',
                );

                $options[] = array(
                    'name'     => __( 'Text of ADD IT Button', 'apollo' ),
                    'id'   => Apollo_DB_Schema::_CLASSIFIED_TEXT_OF_ADD_IT,
                    'std'  => __('ADD IT', 'apollo'),
                    'type'     => 'text',
                    'class' => 'no-border inline',
                    'wplm'  => true
                );

                $options[] = array(
                    'name'     => __( 'Text of ADDED Button', 'apollo' ),
                    'id'   => Apollo_DB_Schema::_CLASSIFIED_TEXT_OF_ADDED_IT,
                    'std'  => __('ADDED', 'apollo'),
                    'type'     => 'text',
                    'class' => 'no-border inline',
                    'wplm'  => true
                );

            }

            // close the tab content
            $options[] = array(
                'type'        => 'sub_tab_content_closed'
            );

            return $options;
        }
    }

    new Apollo_Options_Classified();

endif;