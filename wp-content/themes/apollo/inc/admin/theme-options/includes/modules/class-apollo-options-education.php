<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Apollo_Options_Education' ) ) :

/**
 * Description of Apollo_Options_Public_Art
 *
 * @author thienld
 */
class Apollo_Options_Education {

    public function __construct() {
        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EDUCATION ) ) {
            return;
        }
        add_filter('of_options_first', array( $this, 'getOptions' ), 5);
    }

    public function getOptions($opts) {

        $options = empty($opts) ? array() : $opts;

        // open the tab content
        $options[] = array(
            'tab-id'        => APL_Theme_Option_Site_Config_SubTab::EDUCATION_SETTING,
            'type'        => 'sub_tab_content_opened',
        );

        /*@ticket #16294: Education (educator) > Add a custom text field for the text label of this link*/
        $options[] = array(
            'name'  =>  __( 'Custom text of the Website label', 'apollo' ),
            'id'    => 'custom_text_of_website_label',
            'std'   => __( 'Website', 'apollo' ),
            'type'  => 'text',
            'class' => 'no-border inline'
        );

        $currentTheme =  !empty(wp_get_theme()) ? wp_get_theme()->stylesheet : '';
        if ( $currentTheme == 'apollo') {
            /*@ticket #18190: 0002504: Arts Education Customizations - Customize the search box below the nav menu - item 3*/
            $options[] = array(
                'name' => __('Enable override the global search below navigation', 'apollo'),
                'desc' => __('', 'apollo'),
                'id' => Apollo_DB_Schema::_EDUCATOR_ENABLE_SEARCH_GLOBAL_CUSTOM,
                'std' => 0,
                'type' => 'radio',
                'options' => array(
                    1 => __('Yes', 'apollo'),
                    0 => __('No', 'apollo'),
                ),
                'class' => 'no-border inline'
            );
        }

        /**
         * @ticket #18497: 0002504: Arts Education Customizations - Change home link on all page custom arts ed masthead
         */
        $options[] = array(
            'name' => __("Override the Home URL on the breadcrumb", 'apollo'),
            'desc' => __('', 'apollo'),
            'id' => APL_Theme_Option_Site_Config_SubTab::_EDUCATOR_ENABLE_OVERRIDE_BREADCRUMB_LINK,
            'std' => 0,
            'type' => 'radio',
            'options' => array(
                1 => __('Yes', 'apollo'),
                0 => __('No', 'apollo'),
            ),
            'class' => 'no-border inline'
        );

        $options[] = array(
            'name'     => __( "-- Home URL on breadcrumb", 'apollo' ),
            'desc'     => '',
            'id'   => APL_Theme_Option_Site_Config_SubTab::_EDUCATOR_BREADCRUMB_LINK,
            'std'  => '',
            'type'     => 'text',
            'class' => 'apollo_input_url no-border inline'
        );

        /* End */

        $options[] = array(
            'name'      => __( 'Block Title', 'apollo' ),
            'type'      => 'header_block'
        );

        $options[] = array(
            'name'  =>  __( 'Teachers! Apply For A Grant For This Program', 'apollo' ),
            'id'    => Apollo_DB_Schema::_EDUCATOR_TEACHERS_TITLE,
            'std'   => __( 'Teachers! Apply for a Grant for this Program', 'apollo' ),
            'type'  => 'text',
            'class' => 'no-border inline'

        );

        // Header logo and text display type option
        $activate_educator_apply_prog_arr = array(
            1  => __( 'Yes', 'apollo' ),
            0  => __( 'No', 'apollo' ),
        );

        // Spotlight activate option
        $options[] = array(
            'name'        => __( 'Enable this button', 'apollo' ),
            'desc'        => __( '', 'apollo' ),
            'id'      => Apollo_DB_Schema::_EDUCATOR_ENABLE_TEACHERS_BTN,
            'std'     => 0,
            'type'        => 'radio',
            'options'   => $activate_educator_apply_prog_arr,
            'class' => 'no-border inline'
        );

        /** @Ticket #19640 */

        $options[] = array(
            'name'      => __( 'FE Educator Form', 'apollo' ),
            'type'      => 'header_block'
        );

        $options[] = array(
            'name'      => __( 'Enable other State field', 'apollo' ),
            'id'        => Apollo_DB_Schema::_EDUCATOR_FE_ENABLE_OTHER_STATE,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border apollo_input_url',
        );
        $options[] = array(
            'name'      => __( 'Enable other City field', 'apollo' ),
            'id'        => Apollo_DB_Schema::_EDUCATOR_FE_ENABLE_OTHER_CITY,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border apollo_input_url',
        );

        $options[] = array(
            'name'      => __( 'Enable other Zip Code field', 'apollo' ),
            'id'        => Apollo_DB_Schema::_EDUCATOR_FE_ENABLE_OTHER_ZIP,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border apollo_input_url',
        );

        $options[] = array(
            'name'      => __( 'Listing Page', 'apollo' ),
            'type'      => 'header_block'
        );

        $options[] = array(
            'name'     => __( 'Number of items per page', 'apollo' ),
            'id'       => Apollo_DB_Schema::_EDUCATOR_NUM_ITEMS_LISTING_PAGE,
            'type'     => 'text',
            'class'    => 'apollo_input_number no-border inline'
        );

        $education_default_view_type = array(
            '1'    => __( 'Tile (default)', 'apollo' ),
            '2'      => __( 'List', 'apollo' ),
        );
        $options[] = array(
            'name'       => __( 'Default View Type', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_EDUCATOR_DEFAULT_VIEW_TYPE  ,
            'std'    => '1',
            'preview'   => true,
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => $education_default_view_type
        );

        $options[] = array(
            'name'      => __( 'Detail Page', 'apollo' ),
            'type'      => 'header_block'
        );

        /*@ticket #18397: 0002504: Arts Education Customizations - Change logic the comment field of educator - item 5*/
        $options[] = array(
            'name' => __('Comment field', 'apollo'),
            'desc' => __('', 'apollo'),
            'id' => APL_Theme_Option_Site_Config_SubTab::_EDUCATOR_COMMENT_FIELD,
            'std' => 1,
            'type' => 'radio',
            'options' => array(
                1 => __('Educator', 'apollo'),
                0 => __('Program', 'apollo'),
            ),
            'class' => 'no-border inline',
            'desc'  => __('Select the educator option if you want users to comment on an educator object. In contrast, if the "program" option is selected the comment feature will map with the selected program', 'apollo')
        );

        $options[] = array(
            'name'      => __( 'Spotlight', 'apollo' ),
            'type'      => 'header_block'
        );

        /**
         * @ticket #18515: Add the options allow control the transition speed of the slides.
         */
        $options[] = array(
            'name'     => __( 'SlideShow Speed', 'apollo' ),
            'id'   => Apollo_DB_Schema::_APL_EDUCATION_SLIDESHOW_SPEED,
            'std'  =>  Apollo_DB_Schema::_APL_SLIDESHOW_SPEED_DEFAULT,
            'desc' => __('Set the speed of the slideshow cycling, in milliseconds, minimum = 2000', 'apollo'),
            'type'     => 'number',
            'class' => 'apl-slideshow-speed no-border inline',
            'attr'    => "min = 2000",
            'wplm'  => true,
        );

        /**Vandd @ticket #12148 */
        $options[] = array(
            'name'      => __( 'Menu Swapper', 'apollo' ),
            'type'      => 'header_block'
        );

        $activate_swapper_menu = array(
            1  => __( 'Yes', 'apollo' ),
            0  => __( 'No', 'apollo' ),
        );

        $options[] = array(
            'name'        => __( 'Enable Menu Swapper', 'apollo' ),
            'desc'        => __( '', 'apollo' ),
            'id'      => Apollo_DB_Schema::_EDUCATOR_ENABLE_MSWP,
            'std'     => 0,
            'type'        => 'radio',
            'options'   => $activate_swapper_menu,
            'class' => 'no-border inline'
        );

        $options[] = array(
            'name'       => __( 'Replace this Theme Location', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_EDUCATOR_MSWP_TARGET_POST_META,
            'std'    => 'none',
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => Apollo_App::renderOptionsSwapperMenu('target')
        );

        $options[] = array(
            'name'       => __( 'With this Theme Location', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_EDUCATOR_MSWP_LOC_POST_META,
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => Apollo_App::renderOptionsSwapperMenu('current')
        );


        /*@ticket #18331: 0002504: Arts Education Customizations - Displaying alternate masthead, nav menu, right column and footer for a list of defined pages/modules*/
        $overrideHtmlStaticContent = new Apollo_Override_Html_Static_Content('education');
        $overrideHtmlOptions = $overrideHtmlStaticContent->renderOptions();
        if(!empty($overrideHtmlOptions)){
            $options = array_merge($options, $overrideHtmlOptions);
        }

        // close the tab content
        $options[] = array(
            'type'        => 'sub_tab_content_closed'
        );

        return $options;
    }


}

new Apollo_Options_Education();

endif;