<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Apollo_Options_Programs' ) ) :

    /**
     * Description of Apollo_Options_Programs
     *
     * @author truonghn
     */
    class Apollo_Options_Programs {

        public function __construct() {
            if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EDUCATION ) ) {
                return;
            }
            add_filter('of_options_first', array( $this, 'getOptions' ), 7);
        }

        public function getOptions($opts) {

            $options = empty($opts) ? array() : $opts;

            // open the tab content
            $options[] = array(
                'tab-id'        => APL_Theme_Option_Site_Config_SubTab::PROGRAMS_SETTING,
                'type'        => 'sub_tab_content_opened',
            );

            /** Vandd - @Task #12782 */
            $options[] = array(
                'name'      => __( 'General', 'apollo' ),
                'type'      => 'header_block'
            );

            $options[] = array(
                'name'      => __( 'Enable Cultural Origin', 'apollo' ),
                'id'        => Apollo_DB_Schema::_EDUCATOR_ENABLE_CULTURAL_ORIGIN_DROP,
                'desc'      => __( 'FE & Admin form, Search widget.' , 'apollo' ),
                'std'       => 1,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border',
            );

            $options[] = array(
                'name'      => __( 'Enable Bilingual Checkbox', 'apollo' ),
                'id'        => Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_BILINGUAL,
                'desc'      => __( 'FE & Admin form, Search widget, Detail page.' , 'apollo' ),
                'std'       => 1,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border',
            );

            $options[] = array(
                'name'      => __( 'Enable Free Checkbox', 'apollo' ),
                'id'        => Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_FREE,
                'desc'      => __( 'FE & Admin form, Search widget.' , 'apollo' ),
                'std'       => 1,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border',
            );

            /** @Ticket #16287 */
            $options[] = array(
                'name'     => __( 'Max upload PDFs', 'apollo' ),
                'desc'     => sprintf(__( 'default %s PDFs', 'apollo' ), Apollo_Display_Config::_MAX_UPLOAD_PDF_DEFAULT),
                'id'   => Apollo_DB_Schema::_MAX_UPLOAD_PDF_PROGRAM,
                'std'  => Apollo_Display_Config::_MAX_UPLOAD_PDF_DEFAULT,
                'type'     => 'text',
                'class' => 'apollo_input_number no-border inline'
            );

            /**
             * @ticket #18582:  Arts Education Customizations - Remove the requirement from the 'Max Number of Students' field.
             */
            $options[] = array(
                'name'      => __(  "Enable requirement for Max Number of Students field", "apollo" ),
                'id'        => APL_Theme_Option_Site_Config_SubTab::_PROGRAM_ENABLE_REQUIREMENT_MAX_STUDENTS,
                'std'       => 0,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border',
            );

            $options[] = array(
                'name'      => __( 'Enable Dropdown Search Widget', 'apollo' ),
                'type'      => 'header_block'
            );

            $options[] = array(
                'name'      => __( 'Enable Population Served ', 'apollo' ),
                'id'        => Apollo_DB_Schema::_EDUCATOR_ENABLE_POPULATION_SERVED_DROP,
                'std'       => 1,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border',
            );
            $options[] = array(
                'name'      => __( 'Enable Subject ', 'apollo' ),
                'id'        => Apollo_DB_Schema::_EDUCATOR_ENABLE_SUBJECT_DROP,
                'std'       => 1,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border',
            );
            $options[] = array(
                'name'      => __( 'Enable Artistic Discipline ', 'apollo' ),
                'id'        => Apollo_DB_Schema::_EDUCATOR_ENABLE_ARTISTIC_DISCIPLINE_DROP,
                'std'       => 1,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border',
            );


            $options[] = array(
                'name'      => __( 'Custom Dropdown menu name', 'apollo' ),
                'type'      => 'header_block'
            );

            $options[] = array(
                'name'  =>  __( 'Artistic Discipline  Label', 'apollo' ),
                'id'    => Apollo_DB_Schema::_EDUCATOR_ENABLE_ARTISTIC_DISCIPLINE_LABEL,
                'std'   => __( 'Artistic Discipline', 'apollo' ),
                'type'  => 'text',
                'class' => 'no-border inline'

            );

            $options[] = array(
                'name'      => __( 'Enable fields in the Front-end/ Back-end form', 'apollo' ),
                'type'      => 'header_block'
            );

            $options[] = array(
                'name'      => __( 'Enable Program Core field ', 'apollo' ),
                'id'        => Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_CORE_FIELD,
                'std'       => 0,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border',
            );

            $options[] = array(
                'name'      => __( 'Enable Program Essentials field ', 'apollo' ),
                'id'        => Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_ESSENTIALS_FIELD,
                'std'       => 0,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border',
            );

            $options[] = array(
                'name'      => __( 'Custom name of label fields', 'apollo' ),
                'type'      => 'header_block'
            );

            $options[] = array(
                'name'  =>  __( 'Program Core Label', 'apollo' ),
                'id'    => Apollo_DB_Schema::_EDUCATOR_PROGRAM_CORE_LABEL,
                'std'   => __( 'Program Core', 'apollo' ),
                'type'  => 'text',
                'class' => 'no-border inline'

            );
            $options[] = array(
                'name'  =>  __( 'Program Essentials Label', 'apollo' ),
                'id'    => Apollo_DB_Schema::_EDUCATOR_PROGRAM_ESSENTIALS_LABEL,
                'std'   => __( 'Program Essentials', 'apollo' ),
                'type'  => 'text',
                'class' => 'no-border inline'
            );

            $options[] = array(
                'name'  =>  __( 'Standards are customized for every classroom visit label', 'apollo' ),
                'id'    => Apollo_DB_Schema::_EDUCATOR_PROGRAM_STANDARD_LABEL,
                'std'   => __( 'Standards are customized for every classroom visit', 'apollo' ),
                'type'  => 'text',
                'class' => 'no-border inline'
            );

            /*@ticket #18321 0002504: Arts Education Customizations - Suppress the "Education Standards" section of the form*/
            $options[] = array(
                'name'      => __( 'FE Program Form', 'apollo' ),
                'type'      => 'header_block'
            );

            // Enable required for FE artist primary image
            $options[] = array(
                'name'      => __(  "Enable Education Standards", "apollo" ),
                'id'        => APL_Theme_Option_Site_Config_SubTab::_EDUCATOR_PROGRAM_ENABLE_STANDARDS,
                'std'       => 1,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border',
            );

            /**
             * @ticket #19655: [CF] 20190403 - FE Program dashboard - Add Copy/Delete Buttons
             */
            $options[] = array(
                'name'      => __(  "Enable Copy button", "apollo" ),
                'id'        => APL_Theme_Option_Site_Config_SubTab::_PROGRAM_ENABLE_DISPLAY_COPY_BUTTON,
                'std'       => 0,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border',
            );

            $options[] = array(
                'name'      => __(  "Enable  Delete button", "apollo" ),
                'id'        => APL_Theme_Option_Site_Config_SubTab::_PROGRAM_ENABLE_DISPLAY_DELETE_BUTTON,
                'std'       => 0,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border',
            );

            $options[] = array(
                'name'      => __( 'Listing Page', 'apollo' ),
                'type'      => 'header_block'
            );

            $options[] = array(
                'name'     => __( 'Number of items per page', 'apollo' ),
                'id'       => Apollo_DB_Schema::_PROGRAM_NUM_ITEMS_LISTING_PAGE,
                'type'     => 'text',
                'class'    => 'apollo_input_number no-border inline'
            );

            $programs_default_view_type = array(
                '1'    => __( 'Tile (default)', 'apollo' ),
                '2'      => __( 'List', 'apollo' ),
            );
            $options[] = array(
                'name'       => __( 'Default View Type', 'apollo' ),
                'desc'       => '',
                'id'      => Apollo_DB_Schema::_PROGRAMS_DEFAULT_VIEW_TYPE,
                'std'    => '1',
                'preview'   => true,
                'type'       => 'select',
                'class'   => 'apollo_input_number no-border inline',
                'options' => $programs_default_view_type
            );

            /**Vandd @ticket #12148 */
            $options[] = array(
                'name'      => __( 'Menu Swapper', 'apollo' ),
                'type'      => 'header_block'
            );

            $activate_swapper_menu = array(
                1  => __( 'Yes', 'apollo' ),
                0  => __( 'No', 'apollo' ),
            );

            $options[] = array(
                'name'        => __( 'Enable Menu Swapper', 'apollo' ),
                'desc'        => __( '', 'apollo' ),
                'id'      => Apollo_DB_Schema::_PROGRAM_ENABLE_MSWP,
                'std'     => 0,
                'type'        => 'radio',
                'options'   => $activate_swapper_menu,
                'class' => 'no-border inline'
            );

            $options[] = array(
                'name'       => __( 'Replace this Theme Location', 'apollo' ),
                'desc'       => '',
                'id'      => Apollo_DB_Schema::_PROGRAM_MSWP_TARGET_POST_META,
                'std'    => 'none',
                'type'       => 'select',
                'class'   => 'apollo_input_number no-border inline',
                'options' => Apollo_App::renderOptionsSwapperMenu('target')
            );

            $options[] = array(
                'name'       => __( 'With this Theme Location', 'apollo' ),
                'desc'       => '',
                'id'      => Apollo_DB_Schema::_PROGRAM_MSWP_LOC_POST_META,
                'type'       => 'select',
                'class'   => 'apollo_input_number no-border inline',
                'options' => Apollo_App::renderOptionsSwapperMenu('current')
            );


            // close the tab content
            $options[] = array(
                'type'        => 'sub_tab_content_closed'
            );

            return $options;
        }
    }

    new Apollo_Options_Programs();

endif;