<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Apollo_Options_Organization' ) ) :

/**
 * Description of Apollo_Options_Organization
 *
 * @author thienld
 */
class Apollo_Options_Organization {

    public function __construct() {
        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ORGANIZATION_PT ) ) {
            return;
        }
        add_filter('of_options_first', array( $this, 'getOptions' ),3);
    }

    public function getOptions($opts) {

        $options = empty($opts) ? array() : $opts;

        // open the tab content
        $options[] = array(
            'tab-id'        => APL_Theme_Option_Site_Config_SubTab::ORGANIZATION_SETTING,
            'type'        => 'sub_tab_content_opened',
            'containing-tab'  => 'site-config-sub-tab',
        );

        $options[] = array(
            'name'      => __( 'General', 'apollo' ),
            'type'      => 'header_block'
        );

        $orgPhotoLocation = array(
            '1'    => __( 'Photo Sub menu (Default)', 'apollo' ),
            '2'      => __( 'Inner Org Profile Form', 'apollo' ),
        );
        $options[] = array(
            'name'       => __( 'Location of section Organization Photo', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_ORGANIZATION_PHOTO_FORM_LOCATION,
            'std'    => '1',
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => $orgPhotoLocation
        );

        // Bypass Approval Process
        $options[] = array(
            'name'      => __( 'Bypass Approval', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_ORG_BYPASS_APPROVAL_PROCESS,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );


        $options[] = array(
            'name'       => __( 'Activate Member Field', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_ORGANIZATION_ACTIVE_MEMBER_FIELD,
            'std'    => 0,
            'type'       => 'radio',
            'class'     => 'no-border',
            'desc'  => __('If the "YES" value of this field is selected then ONLY organizations that are checked "Member" on the admin detail forms should appear in the FE event submission form\'s Organization drop menu. AND the fields to add a new organization on that form should be suppressed.', 'apollo'),
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
        );

        /** @Ticket #13597 */
        $options[] = array(
            'name'       => __( 'Icon position', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_ORGANIZATION_SHOW_ICON_POSITION,
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => array(
                'after_thumbs_up'   => __( 'After the thumbs up', 'apollo' ),
                'after_title'      => __( 'After the title', 'apollo' ),
            )
        );

        /**
         * @ticket #18657: Change the layout structure for Artist, Organization, Venue
         */
        $ocNumberDescription = apply_filters('apl_add_number_of_character_description', array(), Apollo_DB_Schema::_ORGANIZATION_PT);
        if (!empty($ocNumberDescription)) {
            $options = array_merge($options, $ocNumberDescription);
        }

        /** @Ticket #13517 */
        $options[] = array(
            'name'      => __( 'Block title', 'apollo' ),
            'type'      => 'header_block'
        );

        $options[] = array(
            'name'  =>  __( 'Check Discounts Button Text', 'apollo' ),
            'id'    => Apollo_DB_Schema::_APL_ORGANIZATION_CHECK_DISCOUNTS_TEXT,
            'std'   => __( 'Check Discounts', 'apollo' ),
            'desc'   => __( 'Check Discounts by default', 'apollo' ),
            'type'  => 'text',
            'class' => 'no-border inline'

        );

        /*@ticket #18015: [CF] 20181022 - [ORG] - Add 'on/off' switch for FE Org types on org/business form - item 3*/
        $options[] = array(
            'name'      => __( 'Form', 'apollo' ),
            'type'      => 'header_block'
        );

        $options[] = array(
            'name'      => __(  "Enable ORG Types", "apollo" ),
            'id'        => Apollo_DB_Schema::_ORGANIZATION_ENABLE_TYPES,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        /*@ticket #18223: Turn on the secondary org address*/
        $options[] = array(
            'name'      => __( '-- Admin Form', 'apollo' ),
            'type'      => 'header_block'
        );
        $options[] = array(
            'name'      => __(  "Enable Secondary Address", "apollo" ),
            'id'        => Apollo_DB_Schema::_APL_ENABLE_SECONDARY_ORG_ADDRESS,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        /**
         * ThienLD : add more settings to serve ticket #12725
         */
        $options[] = array(
            'name'      => __( '-- FE Organization Form', 'apollo' ),
            'type'      => 'header_block'
        );

        // Enable required for FE org primary image
        $options[] = array(
            'name'      => __(  "Enable requirement for Organization's primary image", "apollo" ),
            'id'        => Apollo_DB_Schema::_ORGANIZATION_ENABLE_REQUIRED_PRIMARY_IMAGE,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        /** @Ticket #19640 */
        $options[] = array(
            'name'      => __( 'Enable other State field', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ORGANIZATION_FE_ENABLE_OTHER_STATE,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border apollo_input_url',
        );
        $options[] = array(
            'name'      => __( 'Enable other City field', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ORGANIZATION_FE_ENABLE_OTHER_CITY,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border apollo_input_url',
        );

        $options[] = array(
            'name'      => __( 'Enable other Zip Code field', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ORGANIZATION_FE_ENABLE_OTHER_ZIP,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border apollo_input_url',
        );

        /*@ticket #18085 [CF] 20181030 - [Business] Label options - item 2, 3*/
        $options[] = array(
            'name'  =>  __( 'ORG Types Label ', 'apollo' ),
            'id'    => Apollo_DB_Schema::_ORGANIZATION_TYPES_LABEL,
            'std'   => __( 'Organization/Business Types', 'apollo' ),
            'type'  => 'text',
            'class' => 'no-border inline'
        );

        $options[] = array(
            'name'      => __( 'Clone data to venue', 'apollo' ),
            'type'      => 'header_block'
        );

        $options[] = array(
            'name'      => __( 'Enable clone data to the venue profile form when add new organization ', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ORGANIZATION_CLONE_TO_VENUE,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        /** @Ticket #13643 */
        $options[] = array(
            'name'      => __( 'Enable Requirement fields', 'apollo' ),
            'type'      => 'header_block'
        );

        $orgReqFields = array(
            Apollo_DB_Schema::_ORGANIZATION_ENABLE_ADDRESS_REQUIREMENT => __( 'Address', 'apollo' ),
            Apollo_DB_Schema::_ORGANIZATION_ENABLE_CITY_REQUIREMENT => __( 'City', 'apollo' ),
            Apollo_DB_Schema::_ORGANIZATION_ENABLE_EMAIL_REQUIREMENT => __( 'Email', 'apollo' ),
            Apollo_DB_Schema::_ORGANIZATION_ENABLE_STATE_REQUIREMENT => __( 'State', 'apollo' ),
            Apollo_DB_Schema::_ORGANIZATION_ENABLE_PHONE_REQUIREMENT => __( 'Phone', 'apollo' ),
            Apollo_DB_Schema::_ORGANIZATION_ENABLE_ZIP_REQUIREMENT => __( 'Zip', 'apollo' ),
        );

        foreach($orgReqFields as $key => $label) {
            $options[] = array(
                'name'      => $label,
                'id'        => $key,
                'std'       => 0,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border',
            );
        }

        //Search widget
        $options[] = array(
            'name'      => __( 'Search widget', 'apollo' ),
            'type'      => 'header_block'
        );

        //region
        $options[] = array(
            'name'      => __( 'Enable region', 'apollo' ),
            'id'        => Apollo_DB_Schema::_APL_ENABLE_ORGANIZATION_WIDGET_SEARCH_REGION,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        //state
        $options[] = array(
            'name'      => __( 'Enable state', 'apollo' ),
            'id'        => Apollo_DB_Schema::_APL_ENABLE_ORGANIZATION_WIDGET_SEARCH_STATE,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        //zip
        $options[] = array(
            'name'      => __( 'Enable zip code', 'apollo' ),
            'id'        => Apollo_DB_Schema::_APL_ENABLE_ORGANIZATION_WIDGET_SEARCH_ZIP,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'      => __( 'Listing Page', 'apollo' ),
            'type'      => 'header_block'
        );


        $options[] = array(
            'name'     => __( 'Number of items per page', 'apollo' ),
            'id'       => Apollo_DB_Schema::_ORGANIZATION_NUM_ITEMS_LISTING_PAGE,
            'type'     => 'text',
            'class'    => 'apollo_input_number no-border inline'
        );

        $org_default_view_type = array(
            '1'    => __( 'Tile (default)', 'apollo' ),
            '2'      => __( 'List', 'apollo' ),
        );
        $options[] = array(
            'name'       => __( 'Default View Type', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_ORGANIZATION_DEFAULT_VIEW_TYPE,
            'std'    => '1',
            'preview'   => true,
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => $org_default_view_type
        );

        $currentTheme = function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '';

        if ( $currentTheme == APL_SM_DIR_THEME_NAME) {

            $options[] = array(
                'name'      => __( 'Add Bookmark Title', 'apollo' ),
                'type'      => 'header_block'
            );

            // Enable Facebook Enable
            $options[] = array(
                'name'      => __( 'Enable ADD IT button', 'apollo' ),
                'id'        => Apollo_DB_Schema::_ORG_ENABLE_ADD_IT_BTN,
                'std'       => 1,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border apollo_input_url',
            );

            $options[] = array(
                'name'     => __( 'Text of ADD IT Button', 'apollo' ),
                'id'   => Apollo_DB_Schema::_ORG_TEXT_OF_ADD_IT,
                'std'  => __('ADD IT', 'apollo'),
                'type'     => 'text',
                'class' => 'no-border inline',
                'wplm'  => true
            );

            $options[] = array(
                'name'     => __( 'Text of ADDED Button', 'apollo' ),
                'id'   => Apollo_DB_Schema::_ORG_TEXT_OF_ADDED_IT,
                'std'  => __('ADDED', 'apollo'),
                'type'     => 'text',
                'class' => 'no-border inline',
                'wplm'  => true
            );

        }

        /*@ticket #18010: [CF] 20181022 - [Business] Add a new Theme Option for redirecting Org detail URL to Business detail URL - item 2*/
        $options[] = array(
            'name'      => __( 'Detail Page', 'apollo' ),
            'type'      => 'header_block'
        );

        $options[] = array(
            'name'      => __( 'Automatically redirecting to Business detail URL', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ORG_AUTO_REDIRECT_TO_BUSINESS_DETAIL_URL,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        // close the tab content
        $options[] = array(
            'type'        => 'sub_tab_content_closed'
        );

        return $options;
    }
}

new Apollo_Options_Organization();

endif;