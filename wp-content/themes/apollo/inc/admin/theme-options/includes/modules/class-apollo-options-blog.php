<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Apollo_Options_Blog' ) ) :

/**
 * Description of Apollo_Options_Blog
 *
 * @author thienld
 */
class Apollo_Options_Blog {

    public function __construct() {
        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_BLOG_POST_PT ) ) {
            return;
        }
        add_filter('of_options_first', array( $this, 'getOptions' ), 4);
    }

    public function getOptions($opts) {

        $options = empty($opts) ? array() : $opts;

        // open the tab content
        $options[] = array(
            'tab-id'        => APL_Theme_Option_Site_Config_SubTab::BLOG_SETTING,
            'type'        => 'sub_tab_content_opened',
        );

        $options[] = array(
            'name'      => __( 'General', 'apollo' ),
            'type'      => 'header_block'
        );

        $options[] = array(
            'name'     => __( 'Number of characters description truncation', 'apollo' ),
            'desc'     => sprintf( __( '%s characters by default', 'apollo' ), Apollo_Display_Config::_BLOG_NUM_OF_CHAR ),
            'id'   => Apollo_DB_Schema::_BLOG_NUM_OF_CHAR,
            'std'  => Apollo_Display_Config::_BLOG_NUM_OF_CHAR,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );

        $options[] = array(
            'name'     => __( 'Blog custom label', 'apollo' ),
            'desc'     =>  __( "The default value is 'Blog'", 'apollo' ),
            'id'   => Apollo_DB_Schema::_BLOG_CUSTOM_LABEL,
            'std'  => 'Blog',
            'type'     => 'text',
            'class' => ' no-border inline'
        );

        $options[] = array(
            'name'     => __( 'Blog slug', 'apollo' ),
            'desc'     =>  __( "The default value is 'blog'", 'apollo' ),
            'id'   => Apollo_DB_Schema::_BLOG_CUSTOM_SLUG,
            'std'  => 'blog',
            'type'     => 'slug',
            'class' => ' no-border inline'
        );

        $options[] = array(
            'name'     => __( 'Number of characters headline', 'apollo' ),
            'desc'     =>  __( '75 characters by default', 'apollo' ),
            'id'   => Apollo_DB_Schema::_BLOG_NUM_OF_HEADLINE,
            'std'  => 75,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );

        /**
         * @ticket #18497: 0002504: Arts Education Customizations - Change home link on all page custom arts ed masthead
         */
        $options[] = array(
            'name' => __("Override the Home URL on the breadcrumb", 'apollo'),
            'desc' => __('', 'apollo'),
            'id' => APL_Theme_Option_Site_Config_SubTab::_BLOG_ENABLE_OVERRIDE_BREADCRUMB_LINK,
            'std' => 0,
            'type' => 'radio',
            'options' => array(
                1 => __('Yes', 'apollo'),
                0 => __('No', 'apollo'),
            ),
            'class' => 'no-border inline'
        );

        $options[] = array(
            'name'     => __( "-- Home URL on breadcrumb", 'apollo' ),
            'desc'     => '',
            'id'   => APL_Theme_Option_Site_Config_SubTab::_BLOG_BREADCRUMB_LINK,
            'std'  => '',
            'type'     => 'text',
            'class' => 'apollo_input_url no-border inline'
        );

        /** @Ticket #19723 */
        $featuredImageMaxHeight = apply_filters('apl_blog_featured_image_max_height', array());
        if( !empty($featuredImageMaxHeight)){
            $options = array_merge($options, $featuredImageMaxHeight);
        }

        /* End @ticket #18497 */
        $currentTheme =  !empty(wp_get_theme()) ? wp_get_theme()->stylesheet : '';
        if ( $currentTheme == 'apollo') {
            /*@ticket #18489: 0002504: Arts Education Customizations - http://212.98.167.242/mantisbt/view.php?id=2504#c16336*/
            $options[] = array(
                'name' => __('Enable override the global search below navigation', 'apollo'),
                'id' => Apollo_DB_Schema::_BLOG_ENABLE_OVERRIDE_SEARCH_GLOBAL_CUSTOM,
                'std' => 0,
                'type' => 'radio',
                'options' => array(
                    1 => __('Yes', 'apollo'),
                    0 => __('No', 'apollo'),
                ),
                'class' => 'no-border inline'
            );
            $availableModules = Apollo_App::get_avaiable_modules();
            $listModules = array('' => __('Select module', 'apollo'));

            foreach($availableModules as $module){
                if(in_array($module, array('agency', 'syndication', 'photo-slider', 'iframesw', 'post'))){
                    continue;
                }
                $listModules[$module] = ucfirst($module);
            }

            $options[] = array(
                'name'       => __( '-- Direct to module', 'apollo' ),
                'desc'       => '',
                'id'      => Apollo_DB_Schema::_BLOG_OVERRIDE_SEARCH_GLOBAL_CUSTOM_DIRECT_MODULE,
                'std'    => '',
                'type'       => 'select',
                'class'   => 'no-border inline',
                'options' => $listModules
            );
        }


        $options[] = array(
            'name'      => __( 'Block Title', 'apollo' ),
            'type'      => 'header_block'
        );


        $options[] = array(
            'name'     => __( 'Related Articles Label', 'apollo' ),
            'desc'     =>  __( "The default value is 'Related Stories'", 'apollo' ),
            'id'   => Apollo_DB_Schema::_BLOG_LABEL_RELATED_ARTICLES,
            'std'  => 'Related Stories',
            'type'     => 'text',
            'class' => ' no-border inline'
        );

        $options[] = array(
            'name'      => __( 'Display', 'apollo' ),
            'type'      => 'header_block'
        );

        /*@ticket #18343 0002504: Arts Education Customizations - Add Arts Ed masthead logo and footers to Blog listing and detail pages.*/
        $options[] = array(
            'name'        => __( 'Enable Default Image', 'apollo' ),
            'id'      => APL_Theme_Option_Site_Config_SubTab::_BLOG_ENABLE_DEFAULT_IMAGE,
            'std'     => 1,
            'type'        => 'radio',
            'options'   => array(
                1  => __( 'Yes', 'apollo' ),
                0  => __( 'No', 'apollo' ),
            ),
            'class' => 'no-border inline'
        );

        $ListingDisplayType = array(
            'default'    => __( 'Default', 'apollo' ),
            'simple'      => __( 'Simple ', 'apollo' ),
        );
        $options[] = array(
            'name'       => __( 'Blog Listing Type', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_BLOG_LISTING_TYPE,
            'std'    => 'default',
            'type'       => 'select',
            'preview'   => true,
            'class'   => 'apollo_input_number no-border inline',
            'options' => $ListingDisplayType
        );


        $blogDisplayStyle = apply_filters('_add_blog_display_style', array(
            'default'    => __( 'Default', 'apollo' ),
            'header-new'      => __( 'Date on the top', 'apollo' ),
        ));

        $blogTypes = array(
            'parent-only'    => __( 'Display parent type only', 'apollo' ),
            'child-only'   => __('Display child type only', 'apollo' ),
            'parent-child'      => __( 'Display both parent and child types', 'apollo' ),

        );
        $options[] = array(
            'name'       => __( 'Blog Types', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_BLOG_CATEGORY_TYPES,
            'std'    => 'parent-only',
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => $blogTypes
        );

        $blogOrder = array(
            'title-asc'    => __( 'Title Asc', 'apollo' ),
            'title-desc'   => __('Title Desc', 'apollo' ),
            'date-asc'      => __( 'Publish Date Ascending', 'apollo' ),
            'date-desc'      => __( 'Publish Date Descending', 'apollo' ),
            'order-asc'      => __( 'Special Order Asc', 'apollo' ),
            'order-desc'      => __( 'Special Order Desc', 'apollo' ),
        );
        $options[] = array(
            'name'       => __( 'The order of posts', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_BLOG_CATEGORY_PAGE_ORDER,
            'std'    => 'title-asc',
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => $blogOrder
        );

        /**
         * @ticket #18402 0002504: Arts Education Customizations - [Blog listing page] - Suppress 'Author' and 'Cateogry' - Item 3,4
         */
        $options[] = array(
            'name'      => __( 'Listing page', 'apollo' ),
            'type'      => 'header_block'
        );

        $options[] = array(
            'name'        => __( 'Enable Author', 'apollo' ),
            'id'      => Apollo_DB_Schema::_BLOG_ENABLE_AUTHOR,
            'std'     => 1,
            'type'        => 'radio',
            'options'   => array(
                1  => __( 'Yes', 'apollo' ),
                0  => __( 'No', 'apollo' ),
            ),
            'class' => 'no-border inline'
        );

        $options[] = array(
            'name'        => __( 'Enable Category', 'apollo' ),
            'id'      => Apollo_DB_Schema::_BLOG_ENABLE_CATEGORY,
            'std'     => 1,
            'type'        => 'radio',
            'options'   => array(
                1  => __( 'Yes', 'apollo' ),
                0  => __( 'No', 'apollo' ),
            ),
            'class' => 'no-border inline'
        );

        /**
         * @ticket #18403: Arts Education Customizations - [Blog detail page] - Add the option suppress the 'Excerpt' field - Item 5
         */
        $options[] = array(
            'name'      => __( 'Detail page', 'apollo' ),
            'type'      => 'header_block'
        );

        $options[] = array(
            'name'       => __( 'Blog Detail Type', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_BLOG_DISPLAY_STYLE,
            'std'    => 'default',
            'type'       => 'select',
            'preview'   => true,
            'class'   => 'apollo_input_number no-border inline',
            'options' => $blogDisplayStyle
        );

        $options[] = array(
            'name'        => __( 'Enable Excerpt Field', 'apollo' ),
            'id'      => APL_Theme_Option_Site_Config_SubTab::_BLOG_ENABLE_EXCERPT,
            'std'     => 1,
            'type'        => 'radio',
            'options'   => array(
                1  => __( 'Yes', 'apollo' ),
                0  => __( 'No', 'apollo' ),
            ),
            'class' => 'no-border inline'
        );

        /**
         * @ticket #18768
         */
        $options[] = array(
            'name'        => __( 'Enable Thumbs Up', 'apollo' ),
            'id'      => APL_Theme_Option_Site_Config_SubTab::_BLOG_ENABLE_THUMBS_UP,
            'std'     => 1,
            'type'        => 'radio',
            'options'   => array(
                1  => __( 'Yes', 'apollo' ),
                0  => __( 'No', 'apollo' ),
            ),
            'class' => 'no-border inline'
        );

        /*@ticket #18220 Arts Education Customizations - handle the blog main and detail pages to use the arts education nav menu*/
        $options[] = array(
            'name'      => __( 'Menu Swapper', 'apollo' ),
            'type'      => 'header_block'
        );

        $activate_swapper_menu = array(
            1  => __( 'Yes', 'apollo' ),
            0  => __( 'No', 'apollo' ),
        );

        $options[] = array(
            'name'        => __( 'Enable Menu Swapper', 'apollo' ),
            'desc'        => __( '', 'apollo' ),
            'id'      => Apollo_DB_Schema::_BLOG_ENABLE_MSWP,
            'std'     => 0,
            'type'        => 'radio',
            'options'   => $activate_swapper_menu,
            'class' => 'no-border inline'
        );

        $options[] = array(
            'name'       => __( 'Replace this Theme Location', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_BLOG_MSWP_TARGET_POST_META,
            'std'    => 'none',
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => Apollo_App::renderOptionsSwapperMenu('target')
        );

        $options[] = array(
            'name'       => __( 'With this Theme Location', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_BLOG_MSWP_LOC_POST_META,
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => Apollo_App::renderOptionsSwapperMenu('current')
        );
        //end

        /*@ticket #18343: 0002504: Arts Education Customizations - Add Arts Ed masthead logo and footers to Blog listing and detail pages*/
        $overrideHtmlStaticContent = new Apollo_Override_Html_Static_Content('blog');
        $overrideHtmlOptions = $overrideHtmlStaticContent->renderOptions();
        if(!empty($overrideHtmlOptions)){
            $options = array_merge($options, $overrideHtmlOptions);
        }
        // close the tab content
        $options[] = array(
            'type'        => 'sub_tab_content_closed'
        );

        return $options;
    }
}

new Apollo_Options_Blog();
endif;