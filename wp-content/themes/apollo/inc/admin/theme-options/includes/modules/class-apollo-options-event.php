<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Apollo_Options_Event' ) ) :

/**
 * Description of Apollo_Options_Event
 *
 * @author thienld
 */
class Apollo_Options_Event {
    
    public function __construct() {
        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EVENT_PT ) ) {
            return;
        }
        add_filter('of_options_first', array( $this, 'getOptions' ),2);
    }

    public function getOptions($opts) {

        $options = empty($opts) ? array() : $opts;

        // open the tab content
        $options[] = array(
            'tab-id'        => APL_Theme_Option_Site_Config_SubTab::EVENT_SETTING,
            'type'        => 'sub_tab_content_opened',
        );

        $options[] = array(
            'name'      => __( 'General', 'apollo' ),
            'type'      => 'header_block'
        );


        $options[] = array(
            'name'     => __( 'On going date range', 'apollo' ),
            'id'   => Apollo_DB_Schema::_APL_EVENT_ONGOING_DATE_RANGE,
            'std'  => Apollo_DB_Schema::_APL_EVENT_NUMBER_MONTH_CHANGE_MODE_CALENDAR,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline',
            'desc'  => sprintf(__("An event is available time in month is equal or greater than this config will become an Ongoing Event (default:%s)", "apollo"), Apollo_DB_Schema::_APL_EVENT_NUMBER_MONTH_CHANGE_MODE_CALENDAR)
        );


        $options[] = array(
            'name'     => __( 'Number of characters description truncation', 'apollo' ),
            'desc'     => __('500  characters by default','apollo'),
            'id'   => Apollo_DB_Schema::_APL_EVENT_CHARACTERS_DESCRIPTION,
            'std'  => 500,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );


        // Header logo and text display type option
        $order_options = array(
            'START_DATE_ASC'   => __( 'Start Date Ascending', 'apollo' ),
            'END_DATE_ASC'      => __( 'End Date Ascending', 'apollo' ),
            'ALPHABETICAL_ASC'      => __( 'Title Asc', 'apollo' ),
            'ALPHABETICAL_DESC'      => __( 'Title Desc', 'apollo' ),
        );

        $options[] = array(
            'name'       => __( 'Event Listing Order', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_EVENT_SETTING_ORDER,
            'std'    => DEFAULT_ORDER_START_DATE,
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => $order_options
        );



        // Header logo and text display type option
        $offer_datestimes_options = array(
            'discount_only'    => __( 'Discount Only', 'apollo' ),
            'all'      => __( 'All Dates & Times', 'apollo' ),
        );

        $options[] = array(
            'name'       => __( 'Filter type of Offer Dates/times widget  ', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_EVENT_OFFER_WIDGET_FILTER_TYPE,
            'std'    => 'discount_only',
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => $offer_datestimes_options
        );

        // Bypass Approval Process
        $options[] = array(
            'name'      => __( 'Enable Bypass Approval', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_EVENT_BYPASS_APPROVAL_PROCESS,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
            'desc'  => __("Event is published immediately, do not need to approve from administrator", "apollo"),
        );

        // Bypass Approval Process
        $options[] = array(
            'name'      => __( 'Bypass Approval Method', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_EVENT_BYPASS_APPROVAL_METHOD,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Apply to all users', 'apollo' ),
                0           => __( 'Individual users only', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        /** @Ticket - #12941 */
        $options[] = array(
            'name'      => __( 'Venue Accessibility Icons mode', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_VENUE_ACCESSIBILITY_MODE,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1 => __( 'Yes', 'apollo' ),
                0 => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );


        /* @Ticket - #13548 */
        $icon_position = array(
            'after_thumbs_up'   => __( 'After the thumbs up', 'apollo' ),
            'after_title'      => __( 'After the title', 'apollo' ),
        );

        /*@ticket #18181: 0002410: wpdev55 - Modify the position of the 'discount' icon*/
        $newPositionIcon = apply_filters('apl_add_event_icons_position', array());
        if( !empty($newPositionIcon)){
            $icon_position = array_merge($icon_position, $newPositionIcon);
        }

        $options[] = array(
            'name'       => __( 'Event icons position', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_EVENT_SHOW_ICON_POSITION,
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => $icon_position
        );
        /* end @Ticket - #13548 */

        /* @Ticket - #14350 */
        $thumbs_up_position = array(
            'default'   => __( 'Default', 'apollo' ),
            'next_title'      => __( 'Next to the title', 'apollo' ),
        );

        $options[] = array(
            'name'       => __( 'Event thumbs up position', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_EVENT_SHOW_THUMBS_UP_POSITION,
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'std'     => 'default',
            'options' => $thumbs_up_position
        );

        /**
         * @ticket #19409: [CF] 20190311 - Auto-fill the events to the home spotlight - Item 2
         */
        $options[] = array(
            'name'       => __( 'Auto filled the home spotlight event', 'apollo' ),
            'desc'       => 'The system will be auto filled the event spotlight from event "today" (or whatever the most current events are)',
            'id'      => APL_Theme_Option_Site_Config_SubTab::_EVENT_HOME_SPOTLIGHT_AUTO_FILL,
            'type'       => 'radio',
            'class'   => 'no-border inline',
            'std'     => 0,
            'options' => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            )
        );

        /* @ticket #16609:  Merge the 'Add to Outlook' and 'Add to Google Calendar' icons into one popup window. */
        if (has_filter('apl_custom_event_options')) {
            $options[] = apply_filters('apl_custom_event_options', []);
        }

        $displayIconWithDiscountUrl = array();
        $displayIconWithDiscountUrl = apply_filters('apl_display_icon_with_discount_url_option', $displayIconWithDiscountUrl);
        if (!empty($displayIconWithDiscountUrl)){
            $options = array_merge($options, $displayIconWithDiscountUrl);
        }

        /*@ticket #17348: 0002410: wpdev55 - Requirements part2 - [Event] Display event discount text*/
        $eventDiscountFE = apply_filters('apl_add_option_each_event_discount_description', array());
        if (!empty($eventDiscountFE)) {
            $options = array_merge($options, $eventDiscountFE);
        }

        $options[] = array(
            'name'      => __( 'Search', 'apollo' ),
            'type'      => 'header_block'
        );

        $options[] = array(
            'name'     => __( 'Text of Include Discount Offers', 'apollo' ),
            'desc'     => sprintf( __( 'default Include Discount Offers', 'apollo' ), Apollo_Display_Config::_TEXT_OF_DISCOUNT_OFFER ),
            'id'   => Apollo_DB_Schema::_TEXT_OF_DISCOUNT_OFFER,
            'std'  => Apollo_Display_Config::_TEXT_OF_DISCOUNT_OFFER,
            'type'     => 'text',
            'class' => 'no-border inline',
            'wplm'  => true
        );

        /** @Ticket #13506 */
        $options[] = array(
            'name'      => __( 'Enable Include Discount Offers checkbox', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_DISCOUNT_OFFER_CHBOX,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        /** @Ticket #13544 */
        $discount_checkbox_options = array(
            'only_discount_url' => __( 'Only Discount URL', 'apollo' ),
            'default'           => __( 'Include activated offers on a specific date', 'apollo' )
        );

        $options[] = array(
            'name'      => __( 'Discount Checkbox Filter Type', 'apollo' ),
            'id'        => Apollo_DB_Schema::_EVENT_DISCOUNT_CHECKBOX_FILTER_TYPE,
            'std'       => 'only_discount_url',
            'type'      => 'select',
            'options'   => $discount_checkbox_options,
            'class'     => 'no-border inline',
        );

        //@Ticket #16603-filter events near by my location
        $myLocation = array();
        $myLocation = apply_filters('apl_filter_by_my_location_option', $myLocation);
        if (!empty($myLocation)) {
            $options = array_merge($options, $myLocation);
        }

        $options[] = array(
            'name'      => __( 'Event Search Type', 'apollo' ),
            'id'        => Apollo_DB_Schema::_EVENT_SEARCH_TYPE,
            'std'       => 'rgt',
            'type'      => 'radio',
            'options'   => array(
                'rgt'           => __( 'Right Column', 'apollo' ),
                'hor'           => __( 'Horizontal', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        /** @Ticket #15733 */
        $options[] = array(
            'name'      => __( 'Enable Reset Button On Horizontal Search Bar', 'apollo' ),
            'id'        => APL_Theme_Option_Site_Config_SubTab::_ENABLE_RESET_BUTTON_ON_HORIZONTAL_TAB,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'      => __( 'Horizontal Event Search Location in the Home page', 'apollo' ),
            'id'        => Apollo_DB_Schema::_HOR_EVENT_SEARCH_LOC,
            'std'       => 'default',
            'type'      => 'radio',
            'options'   => array(
                'default'           => __( 'Default', 'apollo' ),
                'ins_spot'           => __( 'Inside the ALT Spotlight', 'apollo' ),
            ),
            'class'     => 'no-border',
        );


        $options[] = array(
            'name'      => __( 'Enable Event Search Organization', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_EVENT_SEARCH_ORG,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1 => __( 'Yes', 'apollo' ),
                0 => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'      => __( 'Enable Event Search Venue', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_EVENT_SEARCH_VENUE,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1 => __( 'Yes', 'apollo' ),
                0 => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        /** @Ticket - #12940 */
        $options[] = array(
            'name'      => __( 'Enable Event Search Accessibility', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_EVENT_SEARCH_ACCESSIBILITY,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1 => __( 'Yes', 'apollo' ),
                0 => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'      => __( 'Enable Event Search Date', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_EVENT_SEARCH_DATE,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1 => __( 'Yes', 'apollo' ),
                0 => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'      => __( 'Enable Event Search City', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_EVENT_SEARCH_CITY,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1 => __( 'Yes', 'apollo' ),
                0 => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'      => __( 'Enable Event Search Neighborhood', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_EVENT_SEARCH_NEIGHBORHOOD,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1 => __( 'Yes', 'apollo' ),
                0 => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'      => __( 'Enable Event Search Region', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_EVENT_SEARCH_REGION,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1 => __( 'Yes', 'apollo' ),
                0 => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        /** @Ticket #13392 */
        $options[] = array(
            'name'      => __( 'Enable Free Event Checkbox', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_EVENT_SEARCH_FREE_EVENT,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1 => __( 'Yes', 'apollo' ),
                0 => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );


        /** @Ticket #16782: Calendar Month search widget - Month view layout**/
        $optionSearchEvent = array();
        $optionSearchEvent = apply_filters('apl_add_custom_options_for_search_event', $optionSearchEvent);
        if (!empty($optionSearchEvent)) {
            $options = array_merge($options, $optionSearchEvent);
        }

        /**
         * @ticket #19483: 0002522: wpdev54 Customization - Improve the spacing of the social media icons in the masthead - Item 3,6,7
         */
        $options[] = array(
            'name'     => __( 'Text of keyword search field', 'apollo' ),
            'id'   => APL_Theme_Option_Site_Config_SubTab::_APL_EVENT_TEXT_OF_KEYWORD_SEARCH,
            'std'  => __('Search by Keyword', 'apollo'),
            'type'     => 'text',
            'class' => 'no-border inline',
        );

        $options[] = array(
            'name'      => __( 'Block Title', 'apollo' ),
            'type'      => 'header_block'
        );

        $options[] = array(
            'name'  =>  __( 'Buy Tickets Button Text', 'apollo' ),
            'id'    => Apollo_DB_Schema::_APL_EVENT_BUY_TICKET_TEXT,
            'std'   => __( 'Buy Tickets', 'apollo' ),
            'desc'   => __( 'Buy Tickets by default', 'apollo' ),
            'type'  => 'text',
            'class' => 'no-border inline'

        );

        $options[] = array(
            'name'  =>  __( 'Check Discounts Button Text', 'apollo' ),
            'id'    => Apollo_DB_Schema::_APL_EVENT_CHECK_DISCOUNTS_TEXT,
            'std'   => __( 'Check Discounts', 'apollo' ),
            'desc'   => __( 'Check Discounts by default', 'apollo' ),
            'type'  => 'text',
            'class' => 'no-border inline'

        );

        $options[] = array(
            'name'  =>  __( 'Booth Deal Button Text', 'apollo' ),
            'id'    => Apollo_DB_Schema::_APL_EVENT_BOOTH_OVERRIDE,
            'std'   => __( 'Booth Deal', 'apollo' ),
            'desc'   => __( '"Booth Deal" by default', 'apollo' ),
            'type'  => 'text',
            'class' => 'no-border inline'
        );

        $options[] = array(
            'name'  =>  __( 'Booth Deal button URL', 'apollo' ),
            'id'    => Apollo_DB_Schema::_APL_EVENT_BOOTH_OVERRIDE_URL,
            'std'   => '',
            'desc'   => __( 'Link for the Booth Deal Button', 'apollo' ),
            'type'  => 'text',
            'class' => 'no-border inline apollo_input_url'
        );

        $options[] = array(
            'name'  =>  __( 'Load More Text of Offer dates/times Widget', 'apollo' ),
            'id'    => Apollo_DB_Schema::_APL_LOAD_MORE_TXT_OFFER_DATESTIME_WIDGET,
            'std'   => __( 'Load More ...', 'apollo' ),
            'desc'   => __( 'Text of Load More button of Offer dates/times Widget', 'apollo' ),
            'type'  => 'text',
            'class' => 'no-border inline'

        );

        $options[] = array(
            'name'     => __( 'Event Video Text', 'apollo' ),
            'desc'     => sprintf( __( 'default %s', 'apollo' ), __(Apollo_Display_Config::EVENT_VIDEO_TEXT,'apollo') ),
            'id'   => Apollo_DB_Schema::EVENT_VIDEO_TEXT,
            'std'  => Apollo_Display_Config::EVENT_VIDEO_TEXT,
            'type'     => 'text',
            'class' => 'no-border inline'
        );

        $options[] = array(
            'name'     => __( 'Event Gallery Text', 'apollo' ),
            'desc'     => sprintf( __( 'default %s ', 'apollo' ), __(Apollo_Display_Config::EVENT_GALLERY_TEXT,'apollo') ),
            'id'   => Apollo_DB_Schema::EVENT_GALLERY_TEXT,
            'std'  => Apollo_Display_Config::EVENT_GALLERY_TEXT,
            'type'     => 'text',
            'class' => 'no-border inline'
        );

        $options[] = array(
            'name'      => __( 'FE Event form', 'apollo' ),
            'type'      => 'header_block'
        );

        /** @Ticket - #13392 */
        $options[] = array(
            'name'      => __( 'Enable Free Event Checkbox', 'apollo' ),
            'id'        => Apollo_DB_Schema::_EVENT_ENABLE_FREE_EVENT,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1 => __( 'Yes', 'apollo' ),
                0 => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        // Enable Primary image must be perfect square
        $options[] = array(
            'name'      => __( 'Enable SQUARE requirement for FE primary image', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_EVENT_PRIMARY_IMG_SQUARE,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        // Enable required for FE event URL field
        $options[] = array(
            'name'      => __( 'Enable requirement for FE event URL field ', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_REQUIREMENT_EVENT_URL_FIELD,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        // Enable required for FE event primary image
        $options[] = array(
            'name'      => __(  "Enable requirement for Event's primary image", "apollo" ),
            'id'        => Apollo_DB_Schema::_ENABLE_REQUIREMENT_EVENT_PRIMARY_IMAGE,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        // START: @ticket #11430
        $options[] = array(
            'name'    => __("Enable requirement for event's summary", "apollo"),
            'id'      => Apollo_DB_Schema::_ENABLE_REQUIREMENT_EVENT_SUMMARY,
            'std'     => 0,
            'type'    => 'radio',
            'options' => array(
                1 => __('Yes', 'apollo'),
                0 => __('No', 'apollo'),
            ),
            'class'   => 'no-border',
        );
        // END: @ticket #11430

        // event photo activate option
        $enable_photo_video_event_array = array(
            1  => __( 'Yes', 'apollo' ),
            0  => __( 'No', 'apollo' ),
        );

        $options[] = array(
            'name'        => __( 'Enable Gallery Photos of Dashboard Event Submission', 'apollo' ),
            'desc'        => __( '', 'apollo' ),
            'id'      => Apollo_DB_Schema::_ENABLE_PHOTO_EVENT,
            'std'     => 1,
            'type'        => 'radio',
            'options'   => $enable_photo_video_event_array,
            'class'     => 'no-border',
        );

        /*
         * @Ticket #13831
         * @Auth HieuLuong
         */
        $options[] = array(
            'name'        => __( 'Enable summary field', 'apollo' ),
            'desc'        => __( '', 'apollo' ),
            'id'      => APL_Theme_Option_Site_Config_SubTab::_APL_ENABLE_EVENT_SUMMARY_FIELD,
            'std'     => 1,
            'type'        => 'radio',
            'options'   => $enable_photo_video_event_array,
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'        => __( 'Enable Registered Organizations drop menu', 'apollo' ),
            'desc'        => __( '', 'apollo' ),
            'id'      => APL_Theme_Option_Site_Config_SubTab::_APL_ENABLE_EVENT_REGISTERED_ORGANIZATIONS_FIELD,
            'std'     => 1,
            'type'        => 'radio',
            'options'   => $enable_photo_video_event_array,
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'        => __( 'Enable Accessibility Info', 'apollo' ),
            'desc'        => __( '', 'apollo' ),
            'id'      => APL_Theme_Option_Site_Config_SubTab::_APL_ENABLE_EVENT_ACCESSIBILITY_FIELD,
            'std'     => 1,
            'type'        => 'radio',
            'options'   => $enable_photo_video_event_array,
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'        => __( 'Enable Additional Time Information', 'apollo' ),
            'desc'        => __( '', 'apollo' ),
            'id'      => APL_Theme_Option_Site_Config_SubTab::_APL_ENABLE_EVENT_TIMEINFO_FIELD,
            'std'     => 1,
            'type'        => 'radio',
            'options'   => $enable_photo_video_event_array,
            'class'     => 'no-border',
        );


        // event video activate option
        $options[] = array(
            'name'        => __( 'Enable Videos', 'apollo' ),
            'desc'        => __( '', 'apollo' ),
            'id'      => Apollo_DB_Schema::_ENABLE_VIDEO_EVENT,
            'std'     => 1,
            'type'        => 'radio',
            'options'   => $enable_photo_video_event_array,
            'class'     => 'no-border',
        );

        /** @Ticket #17452 */
        $options[] = array(
            'name'        => __( 'Enable Tags section', 'apollo' ),
            'desc'        => __( '', 'apollo' ),
            'id'      => Apollo_DB_Schema::_ENABLE_TAGS_SECTION_FE_FORM,
            'std'     => 1,
            'type'        => 'radio',
            'options'   => $enable_photo_video_event_array,
            'class'     => 'no-border',
        );

        /** @Ticket #18746 */
        $options[] = array(
            'name'        => __( 'Enable Artists section', 'apollo' ),
            'desc'        => __( '', 'apollo' ),
            'id'      => Apollo_DB_Schema::_EVENT_ENABLE_ARTISTS_SECTION_FE_FORM,
            'std'     => 1,
            'type'        => 'radio',
            'options'   => $enable_photo_video_event_array,
            'class'     => 'no-border',
        );

        /**
         * @ticket #19185: [CF] 20190215 - Add a character limit field to "Theme Options > Site Config > Event > FE Event Form" - item 2
         */
        $options[] = array(
            'name'     => __( 'A Number of characters for The Event Title', 'apollo' ),
            'desc'     => sprintf( __( '%s characters by default. Only apply to the Apollo and Octave themes', 'apollo' ), APL_Theme_Option_Site_Config_SubTab::_APL_EVENT_TITLE_CHARACTERS_LIMIT_DEFAULT),
            'id'   => APL_Theme_Option_Site_Config_SubTab::_APL_EVENT_TITLE_CHARACTERS_LIMIT,
            'std'  => APL_Theme_Option_Site_Config_SubTab::_APL_EVENT_TITLE_CHARACTERS_LIMIT_DEFAULT,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );

        $options[] = array(
            'name'     => __( 'A Number of characters for the Summary field', 'apollo' ),
            'desc'     => __('250  characters by default','apollo'),
            'id'   => Apollo_DB_Schema::_APL_EVENT_CHARACTERS_SUM,
            'std'  => 250,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );

        // START: @ticket #11430
        $options[] = array(
            'name'     => __('Description for the Summary field', 'apollo'),
            'id'       => Apollo_DB_Schema::_APL_EVENT_SUMMARY_FIELD,
            'std'      => Apollo_Display_Config::_APL_FE_EVENT_FORM_SUMMARY_FIELD,
            'type'     => 'textarea',
            'class'    => 'no-border inline full_width max-w-100-per',
            'settings' => array('rows' => 2),
        );

        /*
         * @Ticket 13842
         */
        $options[] = array(
            'name'     => __( 'A Number of characters for the Description field', 'apollo' ),
            'desc'     => __('No limit characters by default','apollo'),
            'id'   => APL_Theme_Option_Site_Config_SubTab::_APL_EVENT_DESCRIPTION_CHARACTER_LIMIT,
            'std'  => '',
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );

        $options[] = array(
            'name'     => __('Description for the Description field', 'apollo'),
            'id'       => Apollo_DB_Schema::_APL_EVENT_DESCRIPTION_FIELD,
            'std'      => Apollo_Display_Config::_APL_FE_EVENT_FORM_DESCRIPTION_FIELD,
            'type'     => 'textarea',
            'class'    => 'no-border inline full_width max-w-100-per',
            'settings' => array('rows' => 2),
        );
        // END: @ticket #11430

        /*
         * @Ticket 13842
         */
        $options[] = array(
            'name'     => __( 'A Number of characters for Ticket / Admission / Registration Information field', 'apollo' ),
            'desc'     => __('No limit characters by default','apollo'),
            'id'   => APL_Theme_Option_Site_Config_SubTab::_APL_EVENT_TICKET_REGISTRATION_DESCRIPTION_CHARACTER_LIMIT,
            'std'  => '',
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );


        $options[] = array(
            'name'      => __( 'FE Events User Dashboard', 'apollo' ),
            'type'      => 'header_block'
        );


        $options[] = array(
            'name'      => __(  " Enable  Edit button ", "apollo" ),
            'id'        => Apollo_DB_Schema::_ENABLE_DISPLAY_EDIT_BUTTON,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'      => __(  "Enable  Delete button", "apollo" ),
            'id'        => Apollo_DB_Schema::_ENABLE_DISPLAY_DELETE_BUTTON,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        /** Vandd - @Ticket #12434 */
        $options[] = array(
            'name'      => __(  "Required Organization profile", "apollo" ),
            'id'        => Apollo_DB_Schema::_ENABLE_REQUIRED_ORG_PROFILE,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        /** Vandd - @Ticket #12435 */
        $enable_import_option = Apollo_App::get_network_event_import_tool();
        if($enable_import_option){
            $options[] = array(
                'name'       => __( 'Enable Import tab for all users', 'apollo' ),
                'desc'       => 'Allow ALL registered users to use Import Event Tool',
                'id'      => Apollo_DB_Schema::_EVENT_ENABLE_IMPORT_TAB_FOR_ALL_USERS,
                'std'    =>  0 ,
                'type'       => 'radio',
                'options' => array(
                    1     => __( 'Yes', 'apollo'),
                    0     => __( 'No', 'apollo'),
                ),
                'class'   => 'no-border'
            );
        }

        $options[] = array(
            'name'      => __( 'Admin Display Management ', 'apollo' ),
            'type'      => 'header_block'
        );

        $admin_order_options = array(
            'mt_start_d'  => __( 'Start Date Ascending', 'apollo' ),
            'mt_end_d'      => __( 'End Date Ascending', 'apollo' ),

        );

        $options[] = array(
            'name'       => __( 'Event Listing Order', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_EVENT_ADMIN_DISPLAY_ORDER,
            'std'    =>  'mt_end_d' ,
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => $admin_order_options
        );


        $options[] = array(
            'name'      => __( 'Admin Event form', 'apollo' ),
            'type'      => 'header_block'
        );


        // Disable requirement for Event's  Date time fields
        $options[] = array(
            'name'      => __(  "Disable requirement for Event's  Date time fields", "apollo" ),
            'id'        => Apollo_DB_Schema::DISABLE_REQUIREMENT_EVENT_DATE_TIME,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        /*@ticket 17346 */
        $enableDiscountText = apply_filters('apl_enable_discount_text_for_event_form', array());
        if (!empty($enableDiscountText)) {
            $options = array_merge($options, $enableDiscountText);
        }

        $options[] = array(
            'name'      => __( 'Detail page', 'apollo' ),
            'type'      => 'header_block'
        );

        // Enable Individual Dates and Times
        $options[] = array(
            'name'      => __( 'Enable Individual Dates and Times', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_INDIVIDUAL_DATE_TIME,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border apollo_input_url',
        );

        /*@ticket #17304: 0002410: wpdev55 - Requirements part1 - [Page 5] Change order of buttons under summary text to*/
        $mapButton = apply_filters('apl_add_option_map_it_button', array());
        if (!empty($mapButton)) {
            $options = array_merge($options, $mapButton);
        }

        /*@ticket #17304: 0002410: wpdev55 - Requirements part1 - [Page 5] Change order of buttons under summary text to*/
        $calendarButton = apply_filters('apl_add_option_add_to_calendar_button', array());
        if (!empty($calendarButton)) {
            $options = array_merge($options, $calendarButton);
        }

        // Display all text without "View More"
        $options[] = array(
            'name'      => __( 'Display all text without "View More"', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_DISPLAY_WITHOUT_VIEW_MORE,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        $event_social_media_location = array(
            '1'    => __( 'Above the title (Default)', 'apollo' ),
            '2'      => __( 'Under Presented by ...', 'apollo' ),
        );
        $options[] = array(
            'name'       => __( 'Location of Social Media Buttons on Detail page', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_EVENT_SOCIAL_MEDIA_LOCATION_DETAIL,
            'std'    => '1',
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => $event_social_media_location
        );

        // @Ticket 13913
        $options[] = array(
            'name'       => __( 'Tell A Friend Email method', 'apollo' ),
            'desc'       => '',
            'id'      => APL_Theme_Option_Site_Config_SubTab::_APL_SHARE_EMAIL_WITH_FRIENT_METHOD,
            'std'    => 'apollo_system',
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => array(
                "apollo_system" => "Send message directly from the form",
                "client_email" => "Using your own email client"
            )
        );

        /** @Ticket #17303 0002410: wpdev55 - Requirements part1 - [Page 5] Move Bookmark (save) icon after other share icons*/
        $bookmarkButtonLocation = array();
        $bookmarkButtonLocation = apply_filters('apl_add_option_bookmark_button_location', $bookmarkButtonLocation);
        if (!empty($bookmarkButtonLocation)) {
            $options = array_merge($options, $bookmarkButtonLocation);
        }

        /*@ticket #17730: [CF] 20180928 - Convert "date bubble" to the site's standard "date bar" above event image - Item 9*/
        $date_display_options_type = array(
            'default'    => __( 'Default', 'apollo' ),
            'strip-top'      => __( 'Date strip - top', 'apollo' )
        );

        $date_display_options_type = apply_filters('apl_add_custom_display_options_type', $date_display_options_type);

        $options[] = array(
            'name'       => __( 'Date Display Option', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_EVENT_DETAIL_PAGE_DATE_DISPLAY_OPTIONS,
            'std'    => 'default',
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => $date_display_options_type
        );

        $options[] = array(
            'name'      => __( 'Category Page', 'apollo' ),
            'type'      => 'header_block'
        );

        // Enable category page spotlight
        $options[] = array(
            'name'      => __( 'Enable Spotlight ', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_CATEGORY_PAGE_SPOTLIGHT,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border apollo_input_url',
        );

        $options[] = array(
            'name'       => __( 'Spotlight Style', 'apollo' ),
            'id'        => Apollo_DB_Schema::_EVENT_CATEGORY_SPOTLIGHT_STYLE,
            'std'       => 'default',
            'type'      => 'select',
            'options'   => array(
                'default'   => __('Image on the left (Default)', 'apollo'),
                'full'      => __('Image on the top', 'apollo'),
            ),
            'class'     => 'no-border inline',
        );

        $options[] = array(
            'name'       => __( 'Date Display Option', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_CATEGORY_PAGE_DATE_DISPLAY_OPTIONS,
            'std'    => 'default',
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => $date_display_options_type
        );

        $options[] = array(
            'name'      => __( 'Listing Page', 'apollo' ),
            'type'      => 'header_block'
        );


        $options[] = array(
            'name'     => __( 'Number of items per page', 'apollo' ),
            'id'       => Apollo_DB_Schema::_EVENT_NUM_ITEMS_LISTING_PAGE,
            'type'     => 'text',
            'class'    => 'apollo_input_number no-border inline'
        );

        //@Ticket #16859 - on/off Tile filtering
        $tileOption = array();
        $tileOption = apply_filters('apl_add_event_listing_tile_view_option', $tileOption);
        if (!empty($tileOption)) {
            $options = array_merge($options, $tileOption);
        }

        $event_default_view_type = array(
            '1'    => __( 'Tile (default)', 'apollo' ),
            '2'      => __( 'List', 'apollo' ),
        );

        $event_default_view_type = apply_filters('apl_event_listing_custom_default_view_type', $event_default_view_type);

        $options[] = array(
            'name'       => __( 'Default View Type', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_EVENT_DEFAULT_VIEW_TYPE,
            'std'    => '1',
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => $event_default_view_type
        );

        $event_listing_filter_icon_option = array();
        $event_listing_filter_icon_option = apply_filters('apl_event_listing_filter_icon_display', $event_listing_filter_icon_option);
        if (!empty($event_listing_filter_icon_option)){
            $options = array_merge($options, $event_listing_filter_icon_option);
        }

        /*@ticket #17349 */
        $enableClockIcon = apply_filters('apl_add_option_enable_clock_icon', array());
        if (!empty($enableClockIcon)){
            $options = array_merge($options, $enableClockIcon);
        }

        /*@ticket #17347 */
        $optionDiscount = apply_filters('apl_add_option_event_discount', array());
        if (!empty($optionDiscount)){
            $options = array_merge($options, $optionDiscount);
        }

        /** @Ticket #13565 */
        $options[] = array(
            'name'      => __( 'Top Spotlight', 'apollo' ),
            'type'      => 'header_block'
        );

        $options[] = array(
            'name'      => __( 'Enable On Landing Page', 'apollo' ),
            'id'        => Apollo_DB_Schema::_EVENT_ENABLE_TOP_SPOTLIGHT,
            'std'       => 0,
            'desc'      => '',
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'      => __( 'Enable On Category page', 'apollo' ),
            'id'        => Apollo_DB_Schema::_EVENT_CATE_ENABLE_TOP_SPOTLIGHT,
            'std'       => 0,
            'desc'      => '',
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        /**
         * @ticket #18515: Add the options allow control the transition speed of the slides.
         */
        $options[] = array(
            'name'     => __( 'SlideShow Speed', 'apollo' ),
            'id'   => Apollo_DB_Schema::_APL_EVENT_SLIDESHOW_SPEED,
            'std'  =>  Apollo_DB_Schema::_APL_SLIDESHOW_SPEED_DEFAULT,
            'desc' => __('Set the speed of the slideshow cycling, in milliseconds, minimum = 2000', 'apollo'),
            'type'     => 'number',
            'class' => 'apl-slideshow-speed no-border inline',
            'attr'    => "min = 2000",
            'wplm'  => true,
        );

        $options[] = array(
            'name'     => __( 'Total Items On the Landing Page', 'apollo' ),
            'desc'     => sprintf( __( 'Default %s Spotlight Items', 'apollo' ), 10 ),
            'id'   => Apollo_DB_Schema::_EVENT_TOP_SPOTLIGHT_NUM,
            'std'  => Apollo_Display_Config::_EVENT_TOP_SPOTLIGHT_NUM,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );

        $options[] = array(
            'name'     => __( 'Total Items On the Category Page', 'apollo' ),
            'desc'     => sprintf( __( 'Default %s Spotlight Items', 'apollo' ), 10 ),
            'id'   => Apollo_DB_Schema::_EVENT_CATE_TOP_SPOTLIGHT_NUM,
            'std'  => Apollo_Display_Config::_EVENT_TOP_SPOTLIGHT_NUM,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );

        // Spotlight type
        $spot_type = array(
            'small'    => __( 'Small', 'apollo' ),
            'large'     => __( 'Full screen', 'apollo' ),
        );

        $options[] = array(
            'name'       => __( 'Top Spotlight Type', 'apollo' ),
            'id'        => Apollo_DB_Schema::_EVENT_TOP_SPOTLIGHT_TYPE,
            'std'       => 'small',
            'type'       => 'select',
            'desc'  => __('This spotlight will be displayed in the event category page (directory page)', 'apollo'),
            'class'   => 'apollo_input_number no-border inline bs-dt-to-display',
            'options' => $spot_type
        );

        $semiTransparentTypes = array(
            'below'  => __( 'Below the Spotlight graphic', 'apollo' ),
            'def'     => __( 'Default', 'apollo' ),
        );
        // Semi-transparent Type
        $options[] = array(
            'name'       => __( 'Semi-transparent Type', 'apollo' ),
            'id'        => Apollo_DB_Schema::_EVENT_TOP_SPOTLIGHT_SEMI_TRANSPARENT_TYPE,
            'std'       => 'below',
            'type'      => 'select',
            'options'   => $semiTransparentTypes,
            'class'     => 'no-border inline ignore-preview-event',
        );

        // Semi-transparent Background color
        $options[] = array(
            'name'       => __( 'Semi-transparent background color', 'apollo' ),
            'id'        => Apollo_DB_Schema::_EVENT_TOP_SPOTLIGHT_SEMI_TRANSPARENT_TYPE_BG_COLOR,
            'type'      => 'color',
            'std'       => '#000000',
            'class'     => 'no-border apollo_input_url apollo_input_url_newline',
        );

        $options[] = array(
            'name'        => __( 'Event search to print setting', 'apollo' ),
            'type'        => 'header_block'
        );

        $options[] = array(
            'name'     => __( 'Number of maximum events for displaying in each event primary category', 'apollo' ),
            'desc'     => __('leave blank: no limit listed events within each primary category','apollo'),
            'id'   => Apollo_DB_Schema::_MAXIMUM_EVENT_DISPLAY_IN_PRIMARY_CATEGORY,
            'std'  => '',
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );

        $options[] = array(
            'name'     => __( 'Max events for displaying in event print feature', 'apollo' ),
            'desc'     => __('leave blank: no limit listed events','apollo'),
            'id'   => Apollo_DB_Schema::_MAXIMUM_EVENT_DISPLAY_IN_EVENT_PRINT,
            'std'  => '',
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );

        /* Make-up html for header and footer of printing search result events */
        $options[] = array(
            'name'        => __( 'Header Template Print Search Event', 'apollo' ),
            'type'        => 'header_block'
        );
        $options[] = array(
            'desc'     => __( '', 'apollo' ),
            'id'   => Apollo_DB_Schema::_HEADER_PRINTING_SEARCH_EVENT,
            'std'  => render_php_to_string( APOLLO_THEME_HEADER_PRINT_SEARCH_EVENT ),
            'class' => 'full',
            'type'     => 'editor',
            'is_static_file' => true,
        );

        $options[] = array(
            'name'        => __( 'Footer Template Print Search Event', 'apollo' ),
            'type'        => 'header_block'
        );
        $options[] = array(
            'desc'     => __( '', 'apollo' ),
            'id'   => Apollo_DB_Schema::_FOOTER_PRINTING_SEARCH_EVENT,
            'std'  => render_php_to_string( APOLLO_THEME_FOOTER_PRINT_SEARCH_EVENT ),
            'class' => 'full',
            'type'     => 'editor',
            'is_static_file' => true,
        );

        // close the tab content
        $options[] = array(
            'type'        => 'sub_tab_content_closed'
        );

        return $options;
    }
}

new Apollo_Options_Event();

endif;