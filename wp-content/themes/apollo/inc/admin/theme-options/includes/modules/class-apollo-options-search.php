<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Apollo_Options_Search' ) ) :

/**
 * Description of Apollo_Options_Search
 *
 * @author thienld
 */
class Apollo_Options_Search {

    public function __construct() {

        add_filter('of_options_first', array( $this, 'getOptions' ), 6);
    }

    public function getOptions($opts) {

        $options = empty($opts) ? array() : $opts;

        // open the tab content
        $options[] = array(
            'tab-id'        => APL_Theme_Option_Site_Config_SubTab::SEARCH_SETTING,
            'type'        => 'sub_tab_content_opened',
        );

        // Enable Facebook Enable


        $options[] = array(
            'name'      => __( 'Global search tabs', 'apollo' ),
            'type'      => 'header_block'
        );

        $currentTheme =  !empty(wp_get_theme()) ? wp_get_theme()->stylesheet : '';
        if ( $currentTheme == 'apollo') {

            /*@ticket #18136: 0002477: Global Search modifiction - New global search option*/
            $options[] = array(
                'name' => __('Place global search bar below the navigation ', 'apollo'),
                'id' => APL_Theme_Option_Site_Config_SubTab::PLACE_GLOBAL_SEARCH,
                'std' => 0,
                'desc' => '',
                'type' => 'radio',
                'options' => array(
                    1 => __('Yes', 'apollo'),
                    0 => __('No', 'apollo'),
                ),
                'class' => 'no-border',
            );
        }

        $selectedSearchMods = of_get_option( APL_Theme_Option_Site_Config_SubTab::GLOBAL_SEARCH_TABS, false, true );
        $options[] = array(
            'name'     => '',
            'desc'     => '',
            'id'   => APL_Theme_Option_Site_Config_SubTab::GLOBAL_SEARCH_TABS,
            'std'  => '',
            'type'     => 'multiselect_sort',
            'class' => 'apollo_input_number no-border inline',
            'options' => Apollo_App::render_tabs_for_theme_options($selectedSearchMods),
            'selected'  => $selectedSearchMods,
            'data-type'  => 'normal',
            'data-attr-btn' => '',
        );

        $options[] = array(
            'name'  => '',
            'id'   => APL_Theme_Option_Site_Config_SubTab::GLOBAL_SEARCH_TABS. '_of_asname',
            'multiple'  => true,
            'type'  => 'hidden',
            'class' => 'trans',
            'cloneTranstion'    => true,
        );

        // Clone data for translation
        if ( Apollo_App::hasWPLM() ) {
            global $sitepress;
            $languages = $sitepress ? $sitepress->get_active_languages() : false;
            if ($languages) {
                foreach ($languages as $lang => $v) {
                    if ($lang == 'en') continue;

                    $options[] = array(
                        'name'  => '',
                        'id'   => APL_Theme_Option_Site_Config_SubTab::GLOBAL_SEARCH_TABS. '_of_asname_'. $lang,
                        'multiple'  => true,
                        'type'  => 'hidden',
                        'class' => 'trans',
                        'cloneTranstion'    => true,
                        'apply_filter'    => false,
                    );
                }
            }
        }


        $options[] = array(
            'name'      => __( 'General', 'apollo' ),
            'type'      => 'header_block'
        );


        $options[] = array(
            'name'     => __( 'The "View More" number in search result page ', 'apollo' ),
            'desc'     => sprintf(__('%s items by default','apollo'), Apollo_Display_Config::APL_DEFAULT_NUMBER_ITEMS_VIEW_MORE),
            'id'   => Apollo_DB_Schema::_APL_NUMBER_ITEMS_VIEW_MORE,
            'std'  => Apollo_Display_Config::APL_DEFAULT_NUMBER_ITEMS_VIEW_MORE,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );

        /**
         * @Ticket - #13827
         * @author HieuLuong
         * view result option
         */
        $options[] = array(
            'name'       => __( 'Default view', 'apollo' ),
            'id'         => Apollo_DB_Schema::_APL_SEARCH_TYPE_VIEW,
            'desc'       => '',
            'type'       => 'select',
            'class'   => 'no-border inline',
            'options' => array(
                'thumb' => 'Tile',
                'list' => 'List',
            )
        );

        /**
         * @author Trilm
         * filter region option
         */
        $options =  Apollo_Options_Filter_Region_Factory::init($options);


        /***
         * @author Trilm
         * solr-search setting
         */
        if(!Apollo_Solr_Search::isDisable()){

            $options[] = array(
                'name' => __( 'Solr search engine', 'apollo' ),
                'type' => 'header_block'
            );


//            $options[] = array(
//                'name'     => __( 'Host', 'apollo' ),
//                'id'   => Apollo_DB_Schema::_SOLR_SEARCH_HOST,
//                'std'  => '192.168.1.15',
//                'type'     => 'text',
//                'class' => ' no-border inline solr-engine'
//            );
//
//            $options[] = array(
//                'name'     => __( 'Port', 'apollo' ),
//                'id'   => Apollo_DB_Schema::_SOLR_SEARCH_PORT,
//                'std'  => '8983',
//                'type'     => 'text',
//                'class' => ' no-border inline solr-engine'
//            );

//            $options[] = array(
//                'name'     => __( 'Path', 'apollo' ),
//                'id'   => Apollo_DB_Schema::_SOLR_SEARCH_PATH,
//                'std'  => '/solr/apollo_solr',
//                'type'     => 'text',
//                'class' => ' no-border inline solr-engine'
//            );

//        $options[] = array(
//            'name'     => __( 'User name', 'apollo' ),
//            'id'   => Apollo_DB_Schema::_SOLR_SEARCH_USER_NAME,
//            'std'  => '',
//            'type'     => 'text',
//            'class' => ' no-border inline'
//        );
//
//        $options[] = array(
//            'name'     => __( 'User name', 'apollo' ),
//            'id'   => Apollo_DB_Schema::_SOLR_SEARCH_PASSWORD,
//            'std'  => '',
//            'type'     => 'password',
//            'class' => 'no-border inline'
//        );

            $options[] = array(
                'name'     => __( 'Timeout', 'apollo' ),
                'id'   => Apollo_DB_Schema::_SOLR_SEARCH_TIME_OUT,
                'std'  => '30',
                'type'     => 'text',
                'class' => 'no-border inline solr-engine'
            );

            $options[] = array(
                'name'     => __( 'Max query row per post type', 'apollo' ),
                'id'   => Apollo_DB_Schema::_SOLR_MAX_QUERY_ROW_PER_POST_TYPE,
                'std'  => '5',
                'type'     => 'text',
                'class' => 'no-border inline solr-engine'
            );

            $options[] = array(
                'name'     => __( 'Max query row', 'apollo' ),
                'id'   => Apollo_DB_Schema::_SOLR_MAX_QUERY_ROW,
                'std'  => 50,
                'type'     => 'text',
                'class' => 'no-border inline solr-engine'
            );

            $postTypes =  Apollo_App::get_avaiable_modules();
            $postTypeArray  = array();
            foreach($postTypes as $item){
                $postTypeArray[$item] = $item;
            }
            // Spotlight activate option
            $options[] = array(
                'name'        => __( 'Solr post type', 'apollo' ),
                'desc'        => __( '', 'apollo' ),
                'id'      => Apollo_DB_Schema::_SOLR_POST_TYPE_LIST,
                'std'     => 0,
                'type'        => 'multicheck',
                'options'   => $postTypeArray,
                'wrapper_class' => 'solr-engine',
                'class'     => 'no-border',
            );


            $options[] = array(
                'name' 	=> __( 'Sync solr data', 'apollo' ),
                'desc' 	=> '',
                'id' 	=> 'sync_solr_data',
                'std' 	=> '',
                'type' 	=> 'text',
                'class' => 'no-border inline hide-text',
                'not_save'  => true,
                'before'    => '',
                'not_save'  => true,
                'after'     => '<input type="button" value="'.__('Submit sync data', 'apollo').'" id="apl_sync_solr_data" class="sync_solr_data" ><input type="button" value="'.__('Delete sync data', 'apollo').'" id="apl_delete_sync_solr_data" class="delete_sync_solr_data" >'
            );
        }

        // close the tab content
        $options[] = array(
            'type'        => 'sub_tab_content_closed'
        );

        return $options;
    }
}

new Apollo_Options_Search();

endif;