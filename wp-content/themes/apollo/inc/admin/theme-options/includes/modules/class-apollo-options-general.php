<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Apollo_Options_General' ) ) :

/**
 * Description of Apollo_Options_General
 *
 * @author thienld
 */
class Apollo_Options_General {

    static $availableModSettings = null;

    static $apolloModuleToSetting = array(
        Apollo_DB_Schema::_EVENT_SEARCH => APL_Theme_Option_Site_Config_SubTab::SEARCH_SETTING,
        Apollo_DB_Schema::_BLOG_POST_PT => APL_Theme_Option_Site_Config_SubTab::BLOG_SETTING,
        Apollo_DB_Schema::_EVENT_PT => APL_Theme_Option_Site_Config_SubTab::EVENT_SETTING,
        Apollo_DB_Schema::_ORGANIZATION_PT => APL_Theme_Option_Site_Config_SubTab::ORGANIZATION_SETTING,
        Apollo_DB_Schema::_VENUE_PT => APL_Theme_Option_Site_Config_SubTab::VENUE_SETTING,
        Apollo_DB_Schema::_ARTIST_PT => APL_Theme_Option_Site_Config_SubTab::ARTIST_SETTING,
        Apollo_DB_Schema::_PUBLIC_ART_PT => APL_Theme_Option_Site_Config_SubTab::PUBLIC_ART_SETTING,
        Apollo_DB_Schema::_EDUCATION => APL_Theme_Option_Site_Config_SubTab::EDUCATION_SETTING,
        Apollo_DB_Schema::_PROGRAM_PT => APL_Theme_Option_Site_Config_SubTab::PROGRAMS_SETTING,
        Apollo_DB_Schema::_CLASSIFIED => APL_Theme_Option_Site_Config_SubTab::CLASSIFIED_SETTING,
        Apollo_DB_Schema::_BUSINESS_PT => APL_Theme_Option_Site_Config_SubTab::BUSINESS_SETTING,
        Apollo_DB_Schema::_SYNDICATION_PT => APL_Theme_Option_Site_Config_SubTab::SYNDICATION_SETTING,
        Apollo_DB_Schema::_NEWS_PT => APL_Theme_Option_Site_Config_SubTab::NEWS_SETTING,
    );

    public function __construct() {
        add_filter('of_options_first', array( $this, 'getOptions' ),1);
        self::$availableModSettings = array(
            APL_Theme_Option_Site_Config_SubTab::EVENT_SETTING => __('Event', 'apollo'),
            APL_Theme_Option_Site_Config_SubTab::ORGANIZATION_SETTING => __('Organization', 'apollo'),
            APL_Theme_Option_Site_Config_SubTab::BLOG_SETTING => __('Blog', 'apollo'),
            APL_Theme_Option_Site_Config_SubTab::PUBLIC_ART_SETTING => __('Public Art', 'apollo'),
            APL_Theme_Option_Site_Config_SubTab::ARTIST_SETTING => __('Artist', 'apollo'),
            APL_Theme_Option_Site_Config_SubTab::EDUCATION_SETTING => __('Education', 'apollo'),
            APL_Theme_Option_Site_Config_SubTab::CLASSIFIED_SETTING => __('Classified', 'apollo'),
            APL_Theme_Option_Site_Config_SubTab::VENUE_SETTING => __('Venue', 'apollo'),
            APL_Theme_Option_Site_Config_SubTab::PROGRAMS_SETTING => __('Program', 'apollo'),
            APL_Theme_Option_Site_Config_SubTab::SEARCH_SETTING => __('Search', 'apollo'),
            APL_Theme_Option_Site_Config_SubTab::BUSINESS_SETTING => __('Business', 'apollo'),
            APL_Theme_Option_Site_Config_SubTab::SYNDICATION_SETTING => __('Syndication', 'apollo'),
            APL_Theme_Option_Site_Config_SubTab::NEWS_SETTING => __('News', 'apollo'),
        );
    }


    private static function getAvailableModulesTabSettings(){
        $tabSettings = array(
            APL_Theme_Option_Site_Config_SubTab::GENERAL_SETTING => __('General', 'apollo'),
        );
        foreach(self::$apolloModuleToSetting as $mod => $modSetting ){
            if($mod == Apollo_DB_Schema::_EVENT_SEARCH){
                $tabSettings[$modSetting] = self::$availableModSettings[$modSetting];
                continue;
            }
            if($mod == Apollo_DB_Schema::_PROGRAM_PT && Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EDUCATION )){
                $tabSettings[$modSetting] = self::$availableModSettings[$modSetting];
                continue;
            }
            if ( ! Apollo_App::is_avaiable_module( $mod ) ) {
                continue;
            }
            $tabSettings[$modSetting] = self::$availableModSettings[$modSetting];
        }
        return $tabSettings;
    }

    public function getOptions($opts) {

        $options = empty($opts) ? array() : $opts;

        $options[] = array(
            'name' => __( 'Site Config', 'apollo' ),
            'type' => 'heading',
            'extra-classes' => 'sub-tab-wrapper'
        );


        $iconDefault = Apollo_App::getDefaultMarkersBusIcons();
        $options[] = array(
            'type' 		=> 'header_tab_js',
            'tabs'      => self::getAvailableModulesTabSettings(),
        );

        // open the tab content
        $options[] = array(
            'tab-id'        => APL_Theme_Option_Site_Config_SubTab::GENERAL_SETTING,
            'type'        => 'sub_tab_content_opened',
            'containing-tab'  => 'site-config-sub-tab',
        );

        // Header
        $options[] = array(
            'name'        => __( 'Social Settings', 'apollo' ),
            'type'        => 'header_block'
        );

        $options[] = array(
            'name'     => __( 'Facebook', 'apollo' ),
            'desc'     => '',
            'id'   => Apollo_DB_Schema::_FACEBOOK,
            'std'  => '',
            'type'     => 'text',
            'class' => 'apollo_input_url no-border inline'
        );

        $options[] = array(
            'name'     => __( 'Linkedin', 'apollo' ),
            'desc'     => '',
            'id'   => Apollo_DB_Schema::_LINKEDIN,
            'std'  => '',
            'type'     => 'text',
            'class' => 'apollo_input_url no-border inline'
        );

        $options[] = array(
            'name'     => __( 'Twitter', 'apollo' ),
            'desc'     => '',
            'id'   => Apollo_DB_Schema::_TWITTER,
            'std'  => '',
            'type'     => 'text',
            'class' => 'apollo_input_url no-border inline'
        );

        /**
         * @ticket #19186: Enable instagram for the apollo and octave themes
         */
        $options[] = array(
            'name'     => __( 'Instagram', 'apollo' ),
            'desc'     => '',
            'id'   => Apollo_DB_Schema::_INSTAGRAM,
            'std'  => '',
            'type'     => 'text',
            'class' => 'apollo_input_url no-border inline'
        );

        $options[] = array(
            'name'     => __( 'Youtube', 'apollo' ),
            'desc'     => '',
            'id'   => Apollo_DB_Schema::_YOUTUBE,
            'std'  => '',
            'type'     => 'text',
            'class' => 'apollo_input_url no-border inline'
        );

        $options[] = array(
            'name'     => __( 'Vimeo', 'apollo' ),
            'desc'     => '',
            'id'   => Apollo_DB_Schema::_VIMEO,
            'std'  => '',
            'type'     => 'text',
            'class' => 'apollo_input_url no-border inline'
        );


        /** @Ticket #13525 */
        $options[] = array(
            'name'     => __( 'Email text hover', 'apollo' ),
            'desc'     => '',
            'id'   => Apollo_DB_Schema::_EMAIL_TEXT_HOVER,
            'std'  => 'Email',
            'type'     => 'text',
            'class' => ' no-border inline'
        );

        $iconStyle = array();
        $iconStyle = apply_filters('apl_add_social_icon_style', $iconStyle);
        if (!empty($iconStyle)) {
            $options = array_merge($options, $iconStyle);
        }

        // Header
        $options[] = array(
            'name'      => __( 'Enable Facebook Features', 'apollo' ),
            'type'      => 'header_block'
        );

        // Header Enable Facebook Features
        $activate_enable_facebook_features = array(
            1           => __( 'Yes', 'apollo' ),
            0           => __( 'No', 'apollo' ),
        );
        // Enable Facebook Enable
        $options[] = array(
            'name'      => __( 'Enable Facebook Features', 'apollo' ),
            'id'        => Apollo_DB_Schema::_FB_ENABLE,
            'std'       => APOLLO_THEME_ACTIVATE_OVERRIDE_CSS,
            'type'      => 'radio',
            'options'   => $activate_enable_facebook_features,
            'class'     => 'no-border apollo_input_url',
        );
        $options[] = array(
            'name'      => __( 'Facebook AppID', 'apollo' ),
            'desc'      => '',
            'id'        => Apollo_DB_Schema::_FB_APP_ID,
            'std'       => '',
            'type'      => 'text',
            'class'     => 'no-border inline'
        );

        $options[] = array(
            'name'      => __( 'Facebook API Key', 'apollo' ),
            'desc'      => '',
            'id'        => Apollo_DB_Schema::_FB_API_KEY,
            'std'       => '',
            'type'      => 'text',
            'class'     => 'no-border inline'
        );

        $options[] = array(
            'name'      => __( 'Facebook Secret', 'apollo' ),
            'desc'      => '',
            'id'        => Apollo_DB_Schema::_FB_SECRET_KEY,
            'std'       => '',
            'type'      => 'text',
            'class'     => 'no-border inline'
        );

        // Header
        $options[] = array(
            'name'      => __( 'Google API Key', 'apollo' ),
            'type'      => 'header_block'
        );

        // Enable Facebook Enable
        $options[] = array(
            'name'      => __( 'Google API key server', 'apollo' ),
            'desc'      => '',
            'id'        => Apollo_DB_Schema::_GOOGLE_API_KEY_SERVER,
            'std'       => '',
            'type'      => 'text',
            'class'     => 'no-border inline',
        );

        $options[] = array(
            'name'      => __( 'Google API key browser', 'apollo' ),
            'desc'      => '',
            'id'        => Apollo_DB_Schema::_GOOGLE_API_KEY_BROWSER,
            'std'       => '',
            'type'      => 'text',
            'class'     => 'no-border inline',
        );
        $options[] = array(
            'name'      => __( 'Google Analytics Code', 'apollo' ),
            'desc'      => '',
            'id'        => Apollo_DB_Schema::_GOOGLE_ANALYTICS_KEY,
            'std'       => '',
            'type'      => 'text',
            'class'     => 'no-border inline',
        );

        // Google map Settings
        $options[] = array(
            'name'      => __( 'Google map', 'apollo' ),
            'type'      => 'header_block'
        );

        $options[] = array(
            'name'      => __( 'Default your location Maker icon', 'apollo' ),
            'desc'      => '',
            'id'        => Apollo_DB_Schema::_GOOGLE_MAP_MY_POSITION_MARKER_ICON,
            'std'       => '',
            'type'      => 'text',
            'class'     => 'no-border inline',
            'icon-src' => $iconDefault[APL_Business_Module_Theme_Option::YOUR_LOCATION_ICON]['marker'],
        );


        $options[] = array(
            'name'      => __( 'Default google map maker icon', 'apollo' ),
            'desc'      => '',
            'id'        => Apollo_DB_Schema::_GOOGLE_MAP_MARKER_ICON,
            'std'       => '',
            'type'      => 'text',
            'class'     => 'no-border inline',
            'desc_img'  => '/wp-content/themes/apollo/assets/images/icon-location.png',
            'icon-src' =>  $iconDefault[APL_Business_Module_Theme_Option::GOOGLE_MAP_ICON]['marker'],

        );

        //@Ticket #16850 - The google map 'marker icon' selected
        $selectedMarker = array();
        $selectedMarker = apply_filters('apl_add_google_map_selected_marker_option', $selectedMarker);
        if (!empty($selectedMarker)) {
            $options = array_merge($options, $selectedMarker);
        }

        $options[] = array(
            'name'      => __( 'Maker icon width', 'apollo' ),
            'desc'      => __('18 (px) by default', 'apollo'),
            'id'        => Apollo_DB_Schema::_GOOGLE_MAP_MARKER_ICON_WIDTH,
            'type'      => 'text',
            'class'     => 'no-border inline',
        );

        $options[] = array(
            'name'      => __( 'Maker icon height', 'apollo' ),
            'desc'      => __('18 (px) by default', 'apollo'),
            'id'        => Apollo_DB_Schema::_GOOGLE_MAP_MARKER_ICON_HEIGHT,
            'type'      => 'text',
            'class'     => 'no-border inline',
        );

        //@ticket #16193: [Google map] Add a theme option to turn their map section 'off'
        $options[] = array(
            'name'      => __( 'Disable Google Map', 'apollo' ),
            'desc'      => __('This option will be applied to the venue detail page and the simple venue map on the event detail pages', 'apollo'),
            'id'        => 'turn_off_map_section',
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border inline',
        );


        $options[] = array(
            'name'      => __( 'Show Static Map by default ', 'apollo' ),
            'id'        => APL_Business_Module_Theme_Option::WHATS_NEARBY_STATIC,
            'std'       => 1,
            'desc'      => '',
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'     => __( "Text of What's Nearby Button", 'apollo' ),
            'id'   => Apollo_DB_Schema::_TEXT_OF_NEARBY,
            'std'  => __("What's Nearby", 'apollo'),
            'type'     => 'text',
            'class' => 'no-border inline',
            'wplm'  => true
        );

        $options[] = array(
            'name'     => __( "Text of What's Nearby description", 'apollo' ),
            'id'   => Apollo_DB_Schema::_TEXT_OF_NEARBY_DESCRIPTION,
            'std'  => __("See what's near this venue", 'apollo'),
            'type'     => 'text',
            'class' => 'no-border inline',
            'wplm'  => true
        );

        //@Ticket #16589 - Allow adding options for geo-location
        $geoLocation = array();
        $geoLocation = apply_filters('apl_add_geo_location_option', $geoLocation);
        if (!empty($geoLocation)) {
            $options = array_merge($options, $geoLocation);
        }

        // SEO Settings
        $options[] = array(
            'name'      => __( 'SEO Settings', 'apollo' ),
            'type'      => 'header_block'
        );

        $options[] = array(
            'name'     => __( 'Keywords', 'apollo' ),
            'desc'     => __( '', 'apollo' ),
            'id'   => Apollo_DB_Schema::_SITE_SEO_KEYWORDS,
            'std'  => '',
            'type'     => 'textarea',
            'class'     => 'no-border inline full_width max-w-100-per',
            'settings' => array('rows' => 4)
        );

        $options[] = array(
            'name'     => __( 'Description', 'apollo' ),
            'desc'     => __( '', 'apollo' ),
            'id'   => Apollo_DB_Schema::_SITE_SEO_DESCRIPTION,
            'std'  => '',
            'class'     => 'no-border inline full_width max-w-100-per',
            'type'     => 'textarea',
            'settings' => array('rows' => 6)
        );


        /*************************************************************************/

        $options[] = array(
            'name'      => __( 'Uploads Settings', 'apollo' ),
            'type'      => 'header_block'
        );

        // Enable Retina
        $options[] = array(
            'name'      => __( 'Enable Retina', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_RETINA_DISPLAY,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border apollo_input_url',
        );

        $options[] = array(
            'name'     => __( 'Max upload gallery images', 'apollo' ),
            'desc'     => sprintf( __( 'default %s images', 'apollo' ), Apollo_Display_Config::MAX_UPLOAD_GALLERY_IMG ),
            'id'   => Apollo_DB_Schema::_MAX_UPLOAD_GALLERY_IMG,
            'std'  => Apollo_Display_Config::MAX_UPLOAD_GALLERY_IMG,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );

        $options[] = array(
            'desc'        => __( 'Enter a url of not found image', 'apollo' ),
            'id'      => Apollo_DB_Schema::_DEFAULT_NO_IMAGE,
            'std'     => '',
            'class' => 'apollo_input_url no-border inline',
            'type'        => 'text',
            'name'     => __( 'Not Found Image', 'apollo' ),
        );

        // Header
        $options[] = array(
            'name'      => __( 'Display Settings', 'apollo' ),
            'type'      => 'header_block'
        );


        $options[] = array(
            'name' => __('Number of items per page', 'apollo'),
            'desc' => sprintf(__('%s items by default', 'apollo'), apl_get_page_size_default()),
            'id' => Apollo_DB_Schema::_APL_NUMBER_ITEMS_FIRST_PAGE,
            'std' => apl_get_page_size_default(),
            'type' => 'text',
            'class' => 'apollo_input_number no-border inline'
        );

        $options[] = array(
            'name'     => __( 'View more', 'apollo' ),
            'desc'     => sprintf( __( 'default %s items', 'apollo' ), Apollo_Display_Config::NUM_VIEW_MORE ),
            'id'   => Apollo_DB_Schema::_NUM_VIEW_MORE,
            'std'  => Apollo_Display_Config::NUM_VIEW_MORE,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );

        // Enable Comment
        $options[] = array(
            'name'      => __( 'Enable Comment', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_COMMENT,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        // Enable Thumbs up
        $options[] = array(
            'name'      => __( 'Enable Thumbs Up', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_THUMBS_UP,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'      => __( 'Use placeholder graphic', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_PLACEHOLDER_IMG,
            'std'       => 1,
            'type'      => 'radio',
            'desc'   => __("Applied to Business, ORG, Venue, Artist, Classified list view"),
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        //@ticket 14597
        $options[] = array(
            'name'      => __( 'Icon size', 'apollo' ),
            'id'        => APL_Theme_Option_Site_Config_SubTab::_APL_ICON_SIZE,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Free size', 'apollo' ),
                0           => __( 'Fixed size', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'      => __( 'Enable ADD IT button', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_ADD_IT_BTN,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border apollo_input_url',
        );

        /** @Ticket #17303 0002410: wpdev55 - Requirements part1 - [Page 5] Move Bookmark (save) icon after other share icons*/
        $bookmarkButtonType = array();
        $bookmarkButtonType = apply_filters('apl_add_option_bookmark_button_type', $bookmarkButtonType);
        if (!empty($bookmarkButtonType)) {
            $options = array_merge($options, $bookmarkButtonType);
        }

        $options[] = array(
            'name'     => __( 'Text of ADD IT Button', 'apollo' ),
            'id'   => Apollo_DB_Schema::_TEXT_OF_ADD_IT,
            'std'  => __('ADD IT', 'apollo'),
            'type'     => 'text',
            'class' => 'no-border inline',
            'wplm'  => true
        );

        $options[] = array(
            'name'     => __( 'Text of ADDED Button', 'apollo' ),
            'id'   => Apollo_DB_Schema::_TEXT_OF_ADDED_IT,
            'std'  => __('ADDED', 'apollo'),
            'type'     => 'text',
            'class' => 'no-border inline',
            'wplm'  => true
        );

        /**
         * @ticket #18422: 0002522: wpdev54 Customization - Change listing page graphics 'square'.
         */
        $graphicsType = array(
            'search-thumb-square'  => __( 'Square', 'apollo' ),
            ''     => __( 'Rectangle', 'apollo' )
        );

        // Enable Facebook Enable
        $options[] = array(
            'name'       => __('Graphics type of tile view', 'apollo' ),
            'id'        => Apollo_DB_Schema::_APL_GRAPHIC_TYPE,
            'std'       => '',
            'type'      => 'select',
            'options'   => $graphicsType,
            'class'     => 'no-border inline',
            'preview'   => true
        );

        /**
         * @ticket #19131: Octave Theme - Change all detail page section labels same the section labels on the homepage - item 2
         */
        $sectionLabels = apply_filters('oc_add_options_section_labels', array());
        if(!empty($sectionLabels)){
            $options = array_merge($options, $sectionLabels);
        }

        // Header
        $options[] = array(
            'name'        => __( 'User registration', 'apollo' ),
            'type'        => 'header_block'
        );

        /*
        *@ticket #17262: wpdev19 - Suppress the 'zip code' field in the new account registration form
        *Enable zip code field in registration form
        */
        $options[] = array(
            'name'      => __( 'Enable zip code field ', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_REGISTRATION_ZIP_FIELD,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        // Enable requirement for zip code in registration form
        $options[] = array(
            'name'      => __( 'Enable requirement for zip code field ', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_REQUIREMENT_REGISTRATION_ZIP_FIELD,
            'std'       => 0,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        // Header
        $options[] = array(
            'name'        => __( '404 Page', 'apollo' ),
            'type'        => 'header_block'
        );

        $routes = array(
            '404'     => __( '404 Page (by default)', 'apollo' ),
            'home_page'  => __( 'Go to homepage', 'apollo' ),

        );

        // Enable Facebook Enable
        $options[] = array(
            'name'       => __( 'Routing For 404 Error', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ROUTING_FOR_404_ERROR,
            'std'       => '404',
            'type'      => 'select',
            'options'   => $routes,
            'class'     => 'no-border inline',
        );

        /** support info */
        // Header
        $options[] = array(
            'name'        => __( 'Support Info', 'apollo' ),
            'type'        => 'header_block'
        );

        $options[] = array(
            'name'     => __( 'Email', 'apollo' ),
            'desc'     => __('Tell a Friend'),
            'id'   => Apollo_DB_Schema::_SUPPORT_EMAIL,
            'std'  => '',
            'type'     => 'text',
            'class' => 'apollo_input_email no-border inline'
        );

        // close the tab content
        $options[] = array(
            'type'        => 'sub_tab_content_closed'
        );

        return $options;
    }
}

new Apollo_Options_General();

endif;