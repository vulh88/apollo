<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Apollo_Options_Public_Art' ) ) :

/**
 * Description of Apollo_Options_Public_Art
 *
 * @author thienld
 */
class Apollo_Options_Public_Art {

    public function __construct() {
        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_PUBLIC_ART_PT ) ) {
            return;
        }
        add_filter('of_options_first', array( $this, 'getOptions' ), 5);
    }

    public function getOptions($opts) {

        $options = empty($opts) ? array() : $opts;

        // open the tab content
        $options[] = array(
            'tab-id'        => APL_Theme_Option_Site_Config_SubTab::PUBLIC_ART_SETTING,
            'type'        => 'sub_tab_content_opened',
        );


        $options[] = array(
            'name'     => __( 'Default latitude', 'apollo' ),
            'desc'     => '',
            'id'   => Apollo_DB_Schema::_PUBLIC_ART_SETTING_LATITUDE,
            'std'  => Apollo_Display_Config::_PUBLIC_ART_DF_SETTING_LATITUDE,
            'type'     => 'text',
            'class' => 'no-border inline'
        );

        $options[] = array(
            'name'     => __( 'Default longitude', 'apollo' ),
            'desc'     => '',
            'id'   => Apollo_DB_Schema::_PUBLIC_ART_SETTING_LONGITUDE,
            'std'  => Apollo_Display_Config::_PUBLIC_ART_DF_SETTING_LONGITUDE,
            'type'     => 'text',
            'class' => 'no-border inline'
        );

        $options[] = array(
            'name'     => __( 'Zoom magnification', 'apollo' ),
            'desc'     => '',
            'id'   => Apollo_DB_Schema::_PUBLIC_ART_SETTING_ZOOM_MAGNIFICATION,
            'std'  => Apollo_Display_Config::_PUBLIC_ART_DF_SETTING_ZOOM_MAGNIFICATION,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );

        //public artmap query limit
        $options[] = array(
            'name'     => __( 'Limit public art map', 'apollo' ),
            'desc'     => '',
            'id'   => Apollo_DB_Schema::_PUBLIC_ART_SETTING_LIMIT,
            'std'  => Apollo_Display_Config::_PUBLIC_ART_SETTING_LIMIT_STD,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );

        $options[] = array(
            'name'       => ' ',
            'id'      => Apollo_DB_Schema::_PUBLIC_ART_MAP_REFRESH,
            'title'    => __('Refresh public art map','apollo'),
            'type'       => 'button',
            'attr' => array(
                'data-confirm' => __('Are you sure refresh public art map cache ?','apollo'),
                'class' => ' button-secondary',
            ),
            'class'   => 'apollo_input_number no-border inline',
        );

        /**
         * @ticket #19025: wpdev54 Customization - Change layout the public art on the octave theme
         */
        $ocNumberDescription = apply_filters('apl_add_number_of_character_description', array(), Apollo_DB_Schema::_PUBLIC_ART_PT);
        if (!empty($ocNumberDescription)) {
            $options = array_merge($options, $ocNumberDescription);
        }

        $options[] = array(
            'name'      => __( 'Search widget', 'apollo' ),
            'type'      => 'header_block'
        );

        //Search zip
        $options[] = array(
            'name'      => __( 'Enable zip code', 'apollo' ),
            'id'        => Apollo_DB_Schema::_APL_PUBLIC_ART_WIDGET_SEARCH_ENABLE_ZIP,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border',
        );

        $options[] = array(
            'name'      => __( 'Listing Page', 'apollo' ),
            'type'      => 'header_block'
        );


        $options[] = array(
            'name'     => __( 'Number of items per page', 'apollo' ),
            'id'       => Apollo_DB_Schema::_PUBLIC_ART_NUM_ITEMS_LISTING_PAGE,
            'type'     => 'text',
            'class'    => 'apollo_input_number no-border inline'
        );


        $public_art_default_view_type = array(
            '1'    => __( 'Tile (default)', 'apollo' ),
            '2'      => __( 'List', 'apollo' ),
        );
        $options[] = array(
            'name'       => __( 'Default View Type', 'apollo' ),
            'desc'       => '',
            'id'      => Apollo_DB_Schema::_PUBLIC_ART_DEFAULT_VIEW_TYPE,
            'std'    => '1',
            'preview'   => true,
            'type'       => 'select',
            'class'   => 'apollo_input_number no-border inline',
            'options' => $public_art_default_view_type
        );

        $currentTheme = function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '';

        if ( $currentTheme == APL_SM_DIR_THEME_NAME) {

            $options[] = array(
                'name'      => __( 'Add Bookmark Title', 'apollo' ),
                'type'      => 'header_block'
            );

            // Enable Facebook Enable
            $options[] = array(
                'name'      => __( 'Enable ADD IT button', 'apollo' ),
                'id'        => Apollo_DB_Schema::_PUBLIC_ART_ENABLE_ADD_IT_BTN,
                'std'       => 1,
                'type'      => 'radio',
                'options'   => array(
                    1           => __( 'Yes', 'apollo' ),
                    0           => __( 'No', 'apollo' ),
                ),
                'class'     => 'no-border apollo_input_url',
            );

            $options[] = array(
                'name'     => __( 'Text of ADD IT Button', 'apollo' ),
                'id'   => Apollo_DB_Schema::_PUBLIC_ART_TEXT_OF_ADD_IT,
                'std'  => __('ADD IT', 'apollo'),
                'type'     => 'text',
                'class' => 'no-border inline',
                'wplm'  => true
            );

            $options[] = array(
                'name'     => __( 'Text of ADDED Button', 'apollo' ),
                'id'   => Apollo_DB_Schema::_PUBLIC_ART_TEXT_OF_ADDED_IT,
                'std'  => __('ADDED', 'apollo'),
                'type'     => 'text',
                'class' => 'no-border inline',
                'wplm'  => true
            );

        }

        // close the tab content
        $options[] = array(
            'type'        => 'sub_tab_content_closed'
        );

        return $options;
    }
}

new Apollo_Options_Public_Art();

endif;