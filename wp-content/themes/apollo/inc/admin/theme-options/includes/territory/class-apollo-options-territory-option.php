<?php
/**
 * Created by PhpStorm.
 * User: vulh
 * Date: 9/7/15
 * Time: 2:47 PM
 */


if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Apollo_Options_Territory_Option' ) ) :

    /**
     * Description of Apollo_Options_Territory_Option
     *
     * @author vulh
     */
    class Apollo_Options_Territory_Option extends Apollo_Email_Management  {

        private $states;
        private $table;

        public function __construct() {
            add_filter('of_options', array( $this, 'of_options' ));

            $privateStates = Apollo_App::get_network_manage_states_cities();

            global $wpdb;
            $this->table = ($privateStates ? $wpdb->prefix : $wpdb->base_prefix). Apollo_Tables::_APL_STATE_ZIP;

            $this->setStates();
        }

        public function of_options($options) {

            $options[] = array(
                'name'      => __( 'Territory', 'apollo' ),
                'type'      => 'heading',
                'id'        => 'apollo-territory-manegement'
            );



            $options[] = array(
                'name' 	=> __( 'Regions', 'apollo' ),
                'desc' 	=> __( '', 'apollo' ),
                'id' 	=> Apollo_DB_Schema::_REGIONS,
                'std' 	=> APOLLO_REGIONS,
                'type' 	=> 'textarea',
                'desc'  => __('Each region are separated by commas (Region1, Region2)', 'apollo'),
                'class'     => 'no-border inline full_width full',
                'settings' => array('rows' => 10)
            );



            $options[] = array(
                'id'    => Apollo_DB_Schema::_TERR_STATES,
                'type' 		=> 'multicheck',
                'name'  => __('Select States for your Territory','apollo'),
                'wrapper_class' => 'full',
                'options'      => $this->getStatesOptions(),
                'buttons' => '<input type="button" class="button-secondary '.Apollo_DB_Schema::_TERR_STATES_FILTER_BUTTON.'" value="'.__('View/Edit Cities/Zips','apollo').'" data-alert="'.__('Please select States','apollo').'" />',
            );

            $options[] = array(
                'id'    => '',
                'type' 		=> 'custom_layout',
                'name'  => __('Select relevant cities and/or zipcodes'),
                'layout'      => $this->getCustomLayout(),
                'wrapper_class' => 'full',
                'class' => 'contain-city-zip',
            );

            $options[] = array(
                'id'    => Apollo_DB_Schema::_TERR_DATA_FULL_PASS,
                'type' 		=> 'hidden',
                'class' => 'terr-data-full-pass'
            );

            $options[] = array(
                'id'    => Apollo_DB_Schema::_TERR_DATA,
                'multiple' => true,
                'type' 		=> 'hidden',
            );

            $options[] = array(
                'name' 		=> __( ' Set default State and/ or City', 'apollo' ),
                'type' 		=> 'header_block'
            );

            $options[] = array(
                'name' 	  => __( 'Default State', 'apollo' ),
                'id'        => Apollo_DB_Schema::_APL_DEFAULT_STATE,
                'std'       => 'sta',
                'type'      => 'select',
                'options'   => Apollo_App::getStateByTerritory(),
                'class'     => 'no-border inline',
                'select-class' => 'apl-territory-state',
            );

            $options[] = array(
                'name' 	  => __( 'Default City', 'apollo' ),
                'id'        => Apollo_DB_Schema::_APL_DEFAULT_CITY,
                'std'       => 'sta',
                'type'      => 'select',
                'options'   => Apollo_App::getCityByTerritory(false, of_get_option(Apollo_DB_Schema::_APL_DEFAULT_STATE) ),
                'class'     => 'no-border inline',
                'select-class' => 'apl-territory-city',
            );

            $options[] = array(
                'name' 		=> __( ' Neighborhood', 'apollo' ),
                'type' 		=> 'header_block'
            );

            $options[] = array(
                'id'    => '',
                'type' 		=> 'custom_layout',
                'name'  => __( 'Select City', 'apollo' ),
                'layout'      => $this->getCityNeighborhood(),
                'wrapper_class' => '',
                'class' => 'no-border inline',
            );

            $options[] = array(
                'name'     => __( 'Neighborhood', 'apollo' ),
                'id'   => 'neighborhood-name',
                'std'  => '',
                'type'     => 'text',
                'class' => 'no-border inline',
                'val' => ''
            );

            $options[] = array(
                'name'     => '',
                'id'   => 'neighborhood-id',
                'std'  => '0',
                'type'     => 'hidden',
                'class' => 'no-border inline',
                'val' => ''
            );

            $options[] = array(
                'name' 	  => ' ',
                'id'        => '',
                'std'       => '',
                'type'      => 'multicheck',
                'class'     => 'no-border inline',
                'options' => array(),
                'buttons' => '<input type="button" class="button-secondary add-neighborhood" value="'.__('Add Neighborhood','apollo').'" data-text-add="'.__('Add Neighborhood','apollo').'" data-text-update="'. __('Update Neighborhood') .'" />
                            <input type="button" class="button-secondary reset-neighborhood" value="'.__('Reset', 'apollo').'"/>',
            );

            $options[] = array(
                'id'    => '',
                'type' 		=> 'custom_layout',
                'name'  => ' ',
                'layout'      => $this->getNeighborhoodCustomLayout(),
                'wrapper_class' => '',
                'class' => 'no-border inline',
            );

            return $options;
        }

        protected function getNeighborhoodCustomLayout() {
            add_action('admin_enqueue_scripts', array($this,'datatablesEnqueueScript'));
            $result = Apollo_Territory_Neighborhood::output();
            return $result;

        }
        
        protected function getCityNeighborhood() {
            $result = '<select id="neighborhood-city" name="neighborhood-city">';
            $result .= APL_Lib_Territory_Neighborhood::renderCities(APL_Lib_Territory_Neighborhood::getCityByTerritoryV1());
            $result .= '</select>';
            return $result;
        }

        public function datatablesEnqueueScript() {
            wp_enqueue_style('admin-_datatables_css', APOLLO_ADMIN_CSS_URL. '/datatables/jquery.dataTables.min.css');
            wp_enqueue_script( 'admin_datatables_js' , APOLLO_ADMIN_JS_URL. '/datatables/jquery.dataTables.min.js', array( 'jquery' ));
        }

        protected function getCustomLayout() {

            global $wpdb;
            $_states = of_get_option(Apollo_DB_Schema::_TERR_STATES, false);
            $_terrData = of_get_option(Apollo_DB_Schema::_TERR_DATA);
            $states = $_states ? array_keys($_states) : array();
            $cities = array();
            $zips = array();
            //if ( ! $states ) return false;
            $inlist_states = "'" .implode("','", $states). "'";
            if($_terrData){
                foreach ($_terrData as $arr_cit_sta){
                    if($arr_cit_sta){
                        foreach ($arr_cit_sta as $_cit => $arr_zip ){
                            $cities[] = $_cit;
                            if($arr_zip){
                                foreach ( $arr_zip as $_zip => $val ) {
                                    $zips[] = $_zip;
                                }
                            }
                        }
                    }
                }
            }
            $inlist_cities = "'" .implode("','", $cities). "'";
            $inlist_zips = "'" .implode("','", $zips). "'";

            //$querystr = "SELECT * from wp_apollo_state_zip where state_prefix IN ($inlist_states) AND city IN ($inlist_cities) AND zipcode IN ($inlist_zips) order by state_prefix, city";

            $querystr = "
                SELECT cs.code as zipcode, cityTbl.name as city, cs.id as zipId, stateTbl.code as state_prefix, stateTbl.name as state_name
                FROM $this->table cs
                INNER JOIN $this->table cityTbl ON cityTbl.id = cs.parent AND cityTbl.type = 'city' AND cityTbl.name IN ($inlist_cities)
                INNER JOIN $this->table stateTbl ON stateTbl.id = cityTbl.parent AND stateTbl.type = 'state' AND stateTbl.code IN ($inlist_states)
                WHERE cs.code IN ($inlist_zips) AND cs.type = 'zip'
                ORDER BY cs.code ASC
             ";


            $scz = $wpdb->get_results($querystr, OBJECT);


            $seenCity = array();

            ob_start();
            ?>
            <ul id="apl-terr-city-zip" class="hidden">
                <?php
                if ($scz)
                    include APOLLO_ADMIN_DIR . '/theme-options/default-template/territory/list-cities-zips.php';
                ?>
            </ul>
            <?php
            if ($scz) {
                $_cities_str = array();
                $_zipcodes_str = array();

                if ($_terrData) {
                    foreach($_terrData as $state => $cities) {
                        if (!$state) continue;
                        if ( $cities ) {
                            ksort($cities);
                            foreach ( $cities as $c => $zips ) {
                                $_cities_str[] = $c. '('.$state.')';
                                if ($zips) {
                                    $_zipcodes_str = array_merge( $_zipcodes_str, array_keys($zips) );
                                }
                            }
                        }
                    }
                }

                ?>
                <?php
                if ($_cities_str):
                    ?>
                    <div class="simple-line"><b><?php _e('Currently Selected Cities:', 'apollo') ?></b> <?php echo implode(', ', $_cities_str) ?></div>
                <?php endif; ?>

                <?php
                if ($_zipcodes_str):
                    ?>
                    <div class="simple-line"><b><?php _e('Currently Selected Zipcodes:', 'apollo') ?></b> <?php echo implode(', ', $_zipcodes_str) ?></div>
                <?php endif; ?>

                <?php
            }
            return ob_get_clean();
        }

        private function getStatesOptions() {
            $options = array();

            if ( $this->states ) {
                foreach($this->states as $cz) {
                    $options[$cz->code] = $cz->code. ' - '. $cz->name;
                }
            }
            return $options;
        }

        private function setStates() {

            $options = array();
            $aplQuery = new Apl_Query($this->table, true);
            $this->states = $aplQuery->get_where("type='state' and (parent is NULL OR parent = 0)", '*', '', ' order by name asc ');
            
        }

        private function getStates() {
            return $this->states;
        }
    }

    new Apollo_Options_Territory_Option();

endif;