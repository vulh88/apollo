<?php

if (!defined('ABSPATH')) {
    exit;
}

if ( ! class_exists( 'Apollo_Territory_Neighborhood' ) ) {

    class Apollo_Territory_Neighborhood
    {

        /**
         * @return string
         */
        public static function output() {
            $html = '<div class="wrap-table"><table id="neighborhood-table" class="display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>'.__('Name', 'apollo').'</th>
                        <th>'.__('City', 'apollo').'</th>
                        <th>'.__('Actions', 'apollo').'</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>'.__('Name', 'apollo').'</th>
                        <th>'.__('City', 'apollo').'</th>
                        <th>'.__('Actions', 'apollo').'</th>
                    </tr>
                </tfoot>
              
            </table></div>';
            return $html;
        }

    }
}


