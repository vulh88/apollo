<?php


if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Apollo_Options_Email_Templates' ) ) :
    
class Apollo_Options_Email_Templates extends Apollo_Email_Management {
    
    var $heading, $footer_text;
    
    public function __construct() {
        add_filter('of_options', array($this, 'of_options'));
        
        // Preview email template
        if ( isset($_GET['preview_email_template']) ) {
            add_action( 'admin_init', array( $this, 'previewEmailTemplate' ) );
        }
    }
    
    /**
     * Preview email template
     * 
     * @access public
     * @return string
     * 
     */
    function previewEmailTemplate() {
        $action = isset($_GET['action']) ? $_GET['action'] : '';
        if ( !$action ) wp_safe_redirect ('/wp-admin');
        $emailTemplateClass = 'Apollo_Email_'.Apollo_App::processClassAction($action);
        $emailInstance = new $emailTemplateClass;
        echo $emailInstance->wrap_message($emailInstance->get_content(true));
        exit;
    }
    
    /**
     * Set options
     * 
     * @access public
     * @return object
     */
    public function of_options($options) {
        global $apl_email_templates;

        // open the tab content
        $options[] = array(
            'tab-id'        => APL_Theme_Option_Email_Engine_SubTab::EMAIL_TEMPLATE,
            'type'        => 'sub_tab_content_opened',
        );

        $actions = array();
        if ( $apl_email_templates ) {
            foreach( $apl_email_templates as $key => $data ) {
                $actions[$key] = $data['label'];
            }
        }
      
        $activeAction = 'regis';
        
        $options[] = array(
            'name' 	  => __( 'Select an Action', 'apollo' ),
            'desc' 	  => '',
            'std'               => $activeAction,  
            'priority_value'    => $activeAction,
            'type' 	  => 'select',
            'id'      => 'email-template-action',  
            'class'   => 'apollo_input_number no-border inline ',
            'options' => $actions
        );
        
        foreach( $apl_email_templates as $action => $data ):
            
            if ( ! $action ) continue;

            $classTempName = 'Apollo_Option_'. Apollo_App::processClassAction($action) .'_Options_Template';

            /**
             * If class for each action not exist, get the default configuration template 
             * then get the template of current action.
             * The purpose we can extend the layout template of each action
             * 
             */

            if ( class_exists($classTempName) ) {
                $actionClassTemp = new $classTempName($options, $action, $activeAction);
            } else {
                $actionClassTemp = new Apollo_Option_Default_Options_Template($options, $action, $activeAction);
            }
            $options = $actionClassTemp->getOptions();
            
        endforeach;
        
        //if ( isset( $_GET['header_tab'] ) && $_GET['header_tab'] == 'email_template' ) {

        // Always display 2 fields below because currently logic reload page to active email template was removed.
        // Instead of that is show email template by JS event on change of drop-down-list Actions.

        /** Send test email */
        $options[] = array(
            'name'  => __( 'Send a test email', 'apollo' ),
            'type'  => 'header_block',
        );

        $options[] = array(
            'name' 	=> __( 'Email', 'apollo' ),
            'desc' 	=> '',
            'id' 	=> 'test_email',
            'std' 	=> '',
            'type' 	=> 'text',
            'class' => 'no-border inline ',
            'not_save'  => true,
            'before'    => '',
            'not_save'  => true,
            'after'     => '<input data-default-text="'.__('Send test', 'apollo').'" data-sending="'.__('Sending', 'apollo').'" data-success="'.__('Send email successfully', 'apollo').'" data-error="'.__('Can not send email', 'apollo').'" data-class-action="'.$activeAction.'" data-action="apollo_send_test_email" type="button" name="submit-testing-email" class="btn-lft button-primary btn-send-email-test" value="'.__('Send Test', 'apollo').'">'
        );
        // }

        // close the tab content
        $options[] = array(
            'type'        => 'sub_tab_content_closed'
        );

        return $options;
    }
    
}

new Apollo_Options_Email_Templates();

endif;