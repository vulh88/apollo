<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Apollo_Options_Email_Option' ) ) :

/**
 * Description of Apollo_Option_Email_Options
 *
 * @author vulh
 */
class Apollo_Option_Email_Options extends Apollo_Email_Management  {
    
    public function __construct() {
        add_filter('of_options', array( $this, 'of_options' ));
    }
    
    public function of_options($options) {
        global $apollo_email_option;
        
        $options[] = array(
            'name'      => __( 'Email Engine', 'apollo' ),
            'type'      => 'heading',
            'id'        => 'apollo-email-manegement',
            'extra-classes' => 'sub-tab-wrapper'
        );

        $options[] = array(
            'type' 		=> 'header_tab_js',
            'tabs'      => array(
                APL_Theme_Option_Email_Engine_SubTab::EMAIL_OPTION => __('Email Options', 'apollo'),
                APL_Theme_Option_Email_Engine_SubTab::EMAIL_TEMPLATE => __('Email Templates', 'apollo')
            ),
        );
        
        $hiddenClass = '';

        // open the tab content
        $options[] = array(
            'tab-id'        => APL_Theme_Option_Email_Engine_SubTab::EMAIL_OPTION,
            'type'        => 'sub_tab_content_opened',
        );

        $options[] = array(
            'name' 	=> __( 'From Name', 'apollo' ),
            'desc' 	=> '',
            'id' 	=> Apollo_DB_Schema::_ES_FROM_NAME,
            'std' 	=> $apollo_email_option['from_name'],
            'type' 	=> 'text',
            'class' => 'no-border inline '.$hiddenClass
        );

        $options[] = array(
            'name' 	=> __( 'From Email Address', 'apollo' ),
            'desc' 	=> '',
            'id' 	=> Apollo_DB_Schema::_ES_FROM_EMAIL,
            'std' 	=> '',
            'type' 	=> 'text',
            'class' => 'no-border inline apollo_input_email '. $hiddenClass
        );
        
        $options[] = array(
            'name' 	=> __( 'Logo', 'apollo' ),
            'desc' 	=> '',
            'id' 	=> Apollo_DB_Schema::_ES_FROM_LOGO,
            'std' 	=> '',
            'desc'  => __('Enter a URL of image that you want to show in the email header', 'apollo'),
            'type' 	=> 'text',
            'class' => 'no-border inline apollo_input_url '.$hiddenClass
        );
        
        $options[] = array(
            'name' 	=> __( 'From Footer Text', 'apollo' ),
            'desc' 	=> '',
            'id' 	=> Apollo_DB_Schema::_ES_EMAIL_FOOTER_TEXT,
            'std' 	=> $apollo_email_option['footer_text'],
            'desc'  => __('The text to appear in the footer of email template', 'apollo'),
            'type' 	=> 'textarea',
            'class' => 'no-border inline full '.$hiddenClass
        );

        // Header Enable Facebook Features
        $activate_enable_confirmation_features = array(
            1           => __( 'Yes', 'apollo' ),
            0           => __( 'No', 'apollo' ),
        );

        // Enable Email Confirmation Feature
        $options[] = array(
            'name'      => __( 'Enable Email Confirmation Feature', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ES_EMAIL_CONFIRMATION_FEATURE,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => $activate_enable_confirmation_features,
            'class'     => 'no-border apollo_input_url',
        );

        // close the tab content
        $options[] = array(
            'type'        => 'sub_tab_content_closed'
        );

        return $options;
    }
}

new Apollo_Option_Email_Options();

endif;