<?php


/**
 * Description of Apollo_Option_Default_Options_Template
 *
 * @author vulh
 */
class Apollo_Option_Default_Options_Template {
    
    public $options, $action, $currentAction;
    
    public function __construct($options, $action, $currentAction = '') {
        $this->options          = $options;
        $this->action           = $action;
        $this->currentAction    = $currentAction;
        $this->setOptions();
    }
    
    public function setOptions() {

        $class = $this->action . '-selection' . ' email-template-display';

        if($this->action !== $this->currentAction){
            $class .= ' hidden';
        }
        
        global $apl_email_templates;
        
        $this->options[] = array(
            'name' 	=> __( 'Email Subject', 'apollo' ),
            'desc' 	=> '',
            'id' 	=> $this->action. '_'. Apollo_DB_Schema::_ES_EMAIL_SUBJECT,
            'std' 	=> $apl_email_templates[$this->action]['data']['subject'],
            'type' 	=> 'text',
            'class' => 'no-border inline '. $class,
        );
        
        $this->options[] = array(
            'name' 	=> __( 'Email Heading', 'apollo' ),
            'desc' 	=> '',
            'id' 	=> $this->action. '_'. Apollo_DB_Schema::_ES_EMAIL_HEADING,
            'std' 	=> $apl_email_templates[$this->action]['data']['heading'],
            'type' 	=> 'text',
            'class' => 'no-border inline '. $class,
        );
        
        $this->options[] = array(
            'name'      => __( 'Email Type', 'apollo' ),
            'desc'      => '',
            'id'        => $this->action. '_'. Apollo_DB_Schema::_ES_EMAIL_TYPE,
            'std' 	=> $apl_email_templates[$this->action]['data']['type'],
            'type'      => 'select',
            'class'     => 'hidden no-border inline '. $class,
            'options'   => array(
                'html'  => __('HTML', 'apollo'),
                'plain' => __('Plain', 'apollo'),
            )
        );
        
        $_class =  $this->action == 'grant_education' && $this->currentAction == 'grant_education' ? '' : 'hidden';
        $this->options[] = array(
            'name'      => __( 'Recipient', 'apollo' ),
            'id'        => $this->action. '_'. Apollo_DB_Schema::_ES_EMAIL_RECIPIENT,
            'type'      => 'textarea',
            'class'     => 'of-input no-border inline full '. $_class,
            'desc'      => __('Enter multiple email separated by each line', 'apollo'),
        );
        
        $htmlTemplateLabel = __( 'HTML Template', 'apollo' );
        
        /** Do not need to select params for grant education */
        $this->options[] = array(
            'name'      => $htmlTemplateLabel,
            'id'        => '',
            'desc'      => __('Double click to add a param to email template', 'apollo'),
            'std'       => $apl_email_templates[$this->action]['data']['type'],
            'type'      => 'select',
            'class'     => 'no-border inline '. $class,
            'attr'      => 'multiple data-email-param data-target="#'.$this->action.'"',
            'options'   => $this->_getClassParams()
        );
        
        
        $template_str = apply_filters('apollo_admin_load_email_template', Apollo_App::getEmailTemplateContent($this->action), $this->action);
        $template_std = render_php_to_string( APOLLO_ADMIN_DIR. '/theme-options/default-template/email/'.$this->action. '.php' );
        $this->options[] = array(
            'name'  => $this->action == 'grant_education' ? $htmlTemplateLabel : '',
            'desc' 	=> '',
            'id' 	=> $this->action,
            'std' 	=> $template_std,
            'val'   => $template_str,
            'type' 	=> 'textarea',
            'filetype' => 'html',
            'class' => 'full no-border inline '. $class,
            'wrapper_class' => 'full email-template-control',
            'data_filter'   => 'apollo_load_email_template',
            'is_static_file' => true,
            'buttons'   => array(
                'preview-template' => array( 
                    'label' => __('Preview Template', 'apollo'),
//                    'before'    => sprintf( '<a target="_blank" href="%s?preview_email_template=1&action=%s">' , admin_url(), $this->action),
//                    'after' => '</a>',
                    'attr'  => 'data-template="'.$this->action.'" data-notice="'.__('Please allow popups for this site', 'apollo').'" data-process-text="'.__('Processing', 'apollo').'" data-default-text="'.__('Preview Template', 'apollo').'"'
                ),
                'apl-reset-email-template' => array(
                    'label' => __('Reset Template', 'apollo'),
                    'attr' => 'data-message-confirm="'.__('Do you want reset this template?', 'apollo').'"'
                )
            )
        );
        /** @Ticket #16062 - Manage reason list of the Reject button */
        if (  $this->action == 'confirm_rejected_item_to_owner'){

            $reasonsValue = json_encode(array_values(AplEventFunction::getDefaultRejectReasons()));
            $reasonsValue  = str_replace(",","\n", $reasonsValue);
            $reasonsValue  = str_replace(array('[', ']', '"'),"", $reasonsValue);

            $activate_enable_default_reason = array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            );

            $this->options[] = array(
                'name'      => __( 'Use the default reasons', 'apollo' ),
                'id'        => 'use_default_reason',
                'std'       => 1,
                'type'      => 'radio',
                'options'   => $activate_enable_default_reason,
                'class'     => 'no-border hidden'
            );

            $this->options[] = array(
                'name'      => __( 'Reject Reasons', 'apollo' ),
                'id'        => 'reject_reasons_text',
                'std'       => $reasonsValue,
                'desc'      => 'Separated reasons in each row',
                'type'      => 'textarea',
                'class' => 'full no-border hidden',
                'wrapper_class' => 'full email-template-control',
            );
        }
    }
    
    /**
     * Get list params of class
     * 
     * @access private
     * @return array 
     */
    private function _getClassParams() {
        
        $class = 'Apollo_Email_'. Apollo_App::processClassAction($this->action);
        if ( !class_exists( $class ) )  {
            return false;
        }

        $vars = get_class_vars($class);
        $params = array();
        if ( $vars ) {
            $i = 0;
            foreach( $vars as $var => $l ) {
                $i++;
                $params['{'.$var.'}'] = '{'.$var.'}';
                if ( $i == $class::numberParam ) break;
            }
        }
        $params['{primary_color}'] = '{primary_color}';
        
        return $params;
    }
    
    public function getOptions() {
        return $this->options;
    }
}
