<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Apollo_Email_Management' ) ) :


/**
 * Description of Apollo_Email_Management
 *
 * @author vulh
 */
abstract class Apollo_Email_Management {
    
    abstract public function of_options($options);
    
}

endif;