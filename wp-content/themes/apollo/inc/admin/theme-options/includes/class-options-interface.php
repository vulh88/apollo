<?php

/**
 * @package   Options_Framework
 * 
 */

class Options_Framework_Interface {

	/**
	 * List classes sub-tabs its modules
	*/
	static $configSubTabs = array(
		'Apollo_Options_General',
		'Apollo_Options_Event',
		'Apollo_Options_Blog',
		'Apollo_Options_Organization',
		'Apollo_Options_Public_Art',
		'Apollo_Options_Search',
		'Apollo_Options_Classified',
		'Apollo_Options_Venue',
		'Apollo_Options_Artist',
		'Apollo_Options_Education',
		'Apollo_Options_Programs'
	);

	/**
	 * Generates the tabs that are used in the options menu
	 */
	static function optionsframework_tabs() {
		$counter = 0;
		$options = & Options_Framework::_optionsframework_options();
		$menu = '';

		foreach ( $options as $value ) {
			// Heading for Navigation
			if ( $value['type'] == "heading" ) {
                $hidden = apply_filters('_apl_hide_tabs_unnecessary', $value['name'] );
				$counter++;
				$class = '';
				$class = ! empty( $value['id'] ) ? $value['id'] : $value['name'];
				$class = preg_replace( '/[^a-zA-Z0-9._\-]/', '', strtolower($class) ) . '-tab';
                $class .= ' '.$hidden;
				$menu .= '<a id="options-group-'.  $counter . '-tab" class="nav-tab ' . $class .'" title="' . esc_attr( $value['name'] ) . '" href="' . esc_attr( '#options-group-'.  $counter ) . '">' . esc_html( $value['name'] ) . '</a>';
			}
		}

		return $menu;
	}

	/**
	 * Generates the options fields that are used in the form.
	 */
	static function optionsframework_fields($customOptions = array()) {

		global $allowedtags;
        $option_name = of_get_option_id();
		
        
		$settings = get_option($option_name);
		if (empty($customOptions)) {
            $options = & Options_Framework::_optionsframework_options();
        } else {
		    $options = $customOptions;
        }

		$counter = 0;
		$counterSubHeading = 0;
		$subTabIndex = 0;
		$menu = '';

		if (Apollo_App::hasWPLM()) {
			global $sitepress;
			$defaultLang = $sitepress->get_default_language();
			$languages = $sitepress ? $sitepress->get_active_languages() : false;
		} else {
			$defaultLang = $languages = false;
		}

		foreach ( $options as $value ) {

			$val = '';
			$output = '';
            $fieldName = isset( $value['id'] ) && $value['id'] ? $value['id'] : (isset($value['name']) ? $value['name'] : '') ;
            $hidden = apply_filters('_apl_hide_fields_unnecessary', $fieldName );
			// Wrap all options
			if (     $value['type'] != "multiselect_sort"
				&& ( $value['type'] != "heading" )
				&& ( $value['type'] != "sub_tab_content_opened" )
				&& ( $value['type'] != "sub_tab_content_closed" )
				&& ( $value['type'] != "info" )
                && ( $value['type'] != "header_block" )
				&& ( $value['type'] != "header_tab" )
				&& ( $value['type'] != "header_tab_js" )
			) {
                
				// Keep all ids lowercase with no spaces
				$value['id'] = preg_replace('/[^a-zA-Z0-9._\-]/', '', strtolower($value['id']) );

				$id = 'section-' . $value['id'];

				$class = 'section';
				if ( isset( $value['type'] ) ) {
					$class .= ' section-' . $value['type']. ' '.$hidden;
				}
				if ( isset( $value['class'] ) ) {
					$class .= ' ' . $value['class'];
				}
                
				$output .= '<div id="' . esc_attr( $id ) .'" class="' . esc_attr( $class ) . '">'."\n";
				if(isset($value['before'])) $output .= $value['before'];
				if ( isset( $value['name'] ) && $value['name'] ) {
					$output .= '<h4 class="heading">' . esc_html( $value['name'] ) . '</h4>' . "\n";
				}
				if ( $value['type'] != 'editor' ) {
					$output .= '<div class="option">' . "\n" . '<div class="controls '.(isset($value['wrapper_class']) ? $value['wrapper_class'] : "").'">' . "\n";
				}
				else {
					$output .= '<div class="option">' . "\n" . '<div>' . "\n";
				}
			}

			// Set default value to $val
			if ( isset( $value['std'] ) ) {
				$val = $value['std'];
			}

			// If the option is already saved, override $val
			if ( ( $value['type'] != 'heading' ) && ( $value['type'] != 'info') && ( $value['type'] != 'header_block')
                && ( $value['type'] != 'header_tab') ) {
				if ( !empty($value['id']) && isset( $settings[($value['id'])]) ) {
					$val = $settings[($value['id'])];
					// Striping slashes of non-array options
					if ( !is_array($val) ) {
						$val = stripslashes( $val );
					}
				}
			}

			// If there is a description save it for labels
			$explain_value = '';
			if ( isset( $value['desc'] ) ) {
				$explain_value = $value['desc'];
			}

			if ( has_filter( 'optionsframework_' . $value['type'] ) ) {
				$output .= apply_filters( 'optionsframework_' . $value['type'], $option_name, $value, $val );
			}

            
			switch ( $value['type'] ) {

				// Basic text input
				case 'text':
                case 'number':
					$textField = new Apollo_TO_TextField($settings, $value, $option_name, $defaultLang, $languages);
					$output .= $textField->render();
					break;
				/**
                 * ThienLD: add more field type as "SLUG" to serve for input fields of customizing slug for display in url
                 * This field type also is applied filter to sanitize_title_with_dashes()
                 */
                // Text input for SLUG field
                case 'slug':
                    $slugField = new Apollo_TO_TextField($settings, $value, $option_name, $defaultLang, $languages);
                    $output .= $slugField->render();
                    break;
				// Password input
				case 'password':
					$output .= '<input id="' . esc_attr( $value['id'] ) . '" class="of-input" name="' . esc_attr( $option_name . '[' . $value['id'] . ']' ) . '" type="password" value="' . esc_attr( $val ) . '" />';
					break;

				// Textarea
				case 'textarea':
					$textareaField = new Apollo_TO_TextAreaField($settings, $value, $option_name, $defaultLang, $languages);
					$output .= $textareaField->render();
					break;

				// Select Box
				case 'select':
					$preview = isset($value['preview']) && $value['preview'];
					$imagePath = $preview ? ' data-preview-path="' . APOLLO_ADMIN_IMAGES_URL . '""' : '';
					$output .= '<select '.(isset($value['attr']) ? $value['attr'] : '') . $imagePath . ' class="of-input '.(isset($value['select-class']) ? $value['select-class'] : '') . ($preview ? ' preview-option-button' : '') . '" name="' . esc_attr( $option_name . '[' . $value['id'] . ']' ) . '" id="' . esc_attr( $value['id'] ) . '">';

					if ( isset( $value['priority_value'] ) ) {
						$val = $value['priority_value'];
					}
					if ( isset($value['options']) && $value['options'] ) {
						foreach ($value['options'] as $key => $option ) {
							$output .= '<option'. selected( $val, $key, false ) .' value="' . esc_attr( $key ) . '">' . esc_html( $option ) . '</option>';
						}
					} else if(!empty($value['options_html'])) {
						if ( !empty($value['null_option'] ) ) {
							$output .= '<option>'.__( '-- Select a business type --', 'apollo' ).'</option>';
						}
						$output .= $value['options_html'];
					}

					if (isset($value['selectedItem']) && $value['selectedItem'] && !empty($value['selectedItem']['id'])) {
						$output .= sprintf('<option selected value="%s">%s</option>', $value['selectedItem']['id'], $value['selectedItem']['label']);
					}

					$output .= '</select>';
					break;
				// Radio Box
				case "radio":
					$preview = isset($value['preview']) && $value['preview'];
					$imagePath = $preview ? ' data-preview-path="' . APOLLO_ADMIN_IMAGES_URL . '""' : '';
					$name = $option_name .'['. $value['id'] .']';
					foreach ($value['options'] as $key => $option) {
						$id = $option_name . '-' . $value['id'] .'-'. $key;
						$class = isset( $value['class'] ) ? $value['class'] : '';
						$output .= '<input class="of-input of-radio ' . ($preview ? ' preview-option-button' : '') . '" type="radio" name="' . esc_attr( $name ) . '" ' . $imagePath . ' id="' . esc_attr( $id ) . '" value="'. esc_attr( $key ) . '" '. checked( $val, $key, false) .' /><label for="' . esc_attr( $id ) . '">' . esc_html( $option ) . '</label>';
					}

					if(isset($value['after'])) $output .= $value['after'];

					break;

				// Image Selectors
				case "images":
					$name = $option_name .'['. $value['id'] .']';
					foreach ( $value['options'] as $key => $option ) {
						$selected = '';
						if ( $val != '' && ($val == $key) ) {
							$selected = ' of-radio-img-selected';
						}
						$output .= '<input type="radio" id="' . esc_attr( $value['id'] .'_'. $key) . '" class="of-radio-img-radio" value="' . esc_attr( $key ) . '" name="' . esc_attr( $name ) . '" '. checked( $val, $key, false ) .' />';
						$output .= '<div class="of-radio-img-label">' . esc_html( $key ) . '</div>';
						$output .= '<img src="' . esc_url( $option ) . '" alt="' . $option .'" class="of-radio-img-img' . $selected .'" onclick="document.getElementById(\''. esc_attr($value['id'] .'_'. $key) .'\').checked=true;" />';
					}
					break;

				// Checkbox
				case "checkbox":
					$output .= '<input id="' . esc_attr( $value['id'] ) . '" class="checkbox of-input" type="checkbox" name="' . esc_attr( $option_name . '[' . $value['id'] . ']' ) . '" '. checked( $val, 1, false) .' />';
					$output .= '<label class="explain" for="' . esc_attr( $value['id'] ) . '">' . wp_kses( $explain_value, $allowedtags) . '</label>';
					break;

				// Multicheck
				case "multicheck":
					foreach ($value['options'] as $key => $option) {
						$checked = '';
						$label = $option;
						$option = preg_replace('/[^a-zA-Z0-9._\-]/', '', $key);

						$id = $option_name . '-' . $value['id'] . '-'. $option;
						$name = $option_name . '[' . $value['id'] . '][' . $option .']';

						if ( isset($val[$option]) ) {
							$checked = checked($val[$option], 1, false);
						}

						$output .= '<input id="' . esc_attr( $id ) . '" class="checkbox of-input" type="checkbox" data-value="'.$option.'" name="' . esc_attr( $name ) . '" ' . $checked . ' /><label for="' . esc_attr( $id ) . '">' . esc_html( $label ) . '</label>';
					}
					if(!empty($value['buttons'])){
						$output .= '<div class="clearfix">'.$value['buttons'].'</div>';
					}
					break;


					// Multicheck
					case "custom_layout":
						$output .= $value['layout'];
						break;

				// Color picker
				case "color":
					$default_color = '';
					if ( isset($value['std']) ) {
						if ( $val !=  $value['std'] )
							$default_color = ' data-default-color="' .$value['std'] . '" ';
					}
					$output .= '<input name="' . esc_attr( $option_name . '[' . $value['id'] . ']' ) . '" id="' . esc_attr( $value['id'] ) . '" class="of-color"  type="text" value="' . esc_attr( $val ) . '"' . $default_color .' />';

					break;

				// Uploader
				case "upload":
					$output .= Options_Framework_Media_Uploader::optionsframework_uploader( $value['id'], $val, $explain_value );

					break;

				// Typography
				case 'typography':

					unset( $font_size, $font_style, $font_face, $font_color );

					$typography_defaults = array(
						'size' => '',
						'face' => '',
						'style' => '',
						'color' => ''
					);

					$typography_stored = wp_parse_args( $val, $typography_defaults );

					$typography_options = array(
						'sizes' => of_recognized_font_sizes(),
						'faces' => of_recognized_font_faces(),
						'styles' => of_recognized_font_styles(),
						'color' => true
					);

					if ( isset( $value['options'] ) ) {
						$typography_options = wp_parse_args( $value['options'], $typography_options );
					}

					// Font Size
					if ( $typography_options['sizes'] ) {
						$font_size = '<select class="of-typography of-typography-size" name="' . esc_attr( $option_name . '[' . $value['id'] . '][size]' ) . '" id="' . esc_attr( $value['id'] . '_size' ) . '">';
						$sizes = $typography_options['sizes'];
						foreach ( $sizes as $i ) {
							$size = $i . 'px';
							$font_size .= '<option value="' . esc_attr( $size ) . '" ' . selected( $typography_stored['size'], $size, false ) . '>' . esc_html( $size ) . '</option>';
						}
						$font_size .= '</select>';
					}

					// Font Face
					if ( $typography_options['faces'] ) {
						$font_face = '<select class="of-typography of-typography-face" name="' . esc_attr( $option_name . '[' . $value['id'] . '][face]' ) . '" id="' . esc_attr( $value['id'] . '_face' ) . '">';
						$faces = $typography_options['faces'];
						foreach ( $faces as $key => $face ) {
							$font_face .= '<option value="' . esc_attr( $key ) . '" ' . selected( $typography_stored['face'], $key, false ) . '>' . esc_html( $face ) . '</option>';
						}
						$font_face .= '</select>';
					}

					// Font Styles
					if ( $typography_options['styles'] ) {
						$font_style = '<select class="of-typography of-typography-style" name="'.$option_name.'['.$value['id'].'][style]" id="'. $value['id'].'_style">';
						$styles = $typography_options['styles'];
						foreach ( $styles as $key => $style ) {
							$font_style .= '<option value="' . esc_attr( $key ) . '" ' . selected( $typography_stored['style'], $key, false ) . '>'. $style .'</option>';
						}
						$font_style .= '</select>';
					}

					// Font Color
					if ( $typography_options['color'] ) {
						$default_color = '';
						if ( isset($value['std']['color']) ) {
							if ( $val !=  $value['std']['color'] )
								$default_color = ' data-default-color="' .$value['std']['color'] . '" ';
						}
						$font_color = '<input name="' . esc_attr( $option_name . '[' . $value['id'] . '][color]' ) . '" id="' . esc_attr( $value['id'] . '_color' ) . '" class="of-color of-typography-color  type="text" value="' . esc_attr( $typography_stored['color'] ) . '"' . $default_color .' />';
					}

					// Allow modification/injection of typography fields
					$typography_fields = compact( 'font_size', 'font_face', 'font_style', 'font_color' );
					$typography_fields = apply_filters( 'of_typography_fields', $typography_fields, $typography_stored, $option_name, $value );
					$output .= implode( '', $typography_fields );

					break;

				// Background
				case 'background':

					$background = $val;

					// Background Color
					$default_color = '';
					if ( isset( $value['std']['color'] ) ) {
						if ( $val !=  $value['std']['color'] )
							$default_color = ' data-default-color="' .$value['std']['color'] . '" ';
					}
					$output .= '<input name="' . esc_attr( $option_name . '[' . $value['id'] . '][color]' ) . '" id="' . esc_attr( $value['id'] . '_color' ) . '" class="of-color of-background-color"  type="text" value="' . esc_attr( $background['color'] ) . '"' . $default_color .' />';

					// Background Image
					if ( !isset($background['image']) ) {
						$background['image'] = '';
					}

					$output .= Options_Framework_Media_Uploader::optionsframework_uploader( $value['id'], $background['image'], null, esc_attr( $option_name . '[' . $value['id'] . '][image]' ) );

					$class = 'of-background-properties';
					if ( '' == $background['image'] ) {
						$class .= ' hide';
					}
					$output .= '<div class="' . esc_attr( $class ) . '">';

					// Background Repeat
					$output .= '<select class="of-background of-background-repeat" name="' . esc_attr( $option_name . '[' . $value['id'] . '][repeat]'  ) . '" id="' . esc_attr( $value['id'] . '_repeat' ) . '">';
					$repeats = of_recognized_background_repeat();

					foreach ($repeats as $key => $repeat) {
						$output .= '<option value="' . esc_attr( $key ) . '" ' . selected( $background['repeat'], $key, false ) . '>'. esc_html( $repeat ) . '</option>';
					}
					$output .= '</select>';

					// Background Position
					$output .= '<select class="of-background of-background-position" name="' . esc_attr( $option_name . '[' . $value['id'] . '][position]' ) . '" id="' . esc_attr( $value['id'] . '_position' ) . '">';
					$positions = of_recognized_background_position();

					foreach ($positions as $key=>$position) {
						$output .= '<option value="' . esc_attr( $key ) . '" ' . selected( $background['position'], $key, false ) . '>'. esc_html( $position ) . '</option>';
					}
					$output .= '</select>';

					// Background Attachment
					$output .= '<select class="of-background of-background-attachment" name="' . esc_attr( $option_name . '[' . $value['id'] . '][attachment]' ) . '" id="' . esc_attr( $value['id'] . '_attachment' ) . '">';
					$attachments = of_recognized_background_attachment();

					foreach ($attachments as $key => $attachment) {
						$output .= '<option value="' . esc_attr( $key ) . '" ' . selected( $background['attachment'], $key, false ) . '>' . esc_html( $attachment ) . '</option>';
					}
					$output .= '</select>';
					$output .= '</div>';

					break;

				// Editor
				case 'editor':
					$output .= '<div class="explain">' . wp_kses( $explain_value, $allowedtags ) . '</div>'."\n";
					echo $output;
					$textarea_name = esc_attr( $option_name . '[' . $value['id'] . ']' );
					$default_editor_settings = array(
						'textarea_name' => $textarea_name,
						'media_buttons' => true,
						'wpautop'   => false,
					);
					$editor_settings = array();
					if ( isset( $value['settings'] ) ) {
						$editor_settings = $value['settings'];
					}

					if ( isset( $value['is_static_file'] ) && $value['is_static_file'] ) {
						if ( !isset( $value['filetype'] ) || $value['filetype'] == 'html' ) {
							$val = @file_get_contents( get_html_file_abs_path( $value['id'] ) );
						} else {
							$val = @file_get_contents(get_override_css_abs_path() );
						}
					} else {
						$val = stripslashes( $val );
					}

					$editor_settings = array_merge( $default_editor_settings, $editor_settings );
					wp_editor( $val, $value['id'], $editor_settings );
					$output = '';
					break;

				// Info
				case "info":
					$id = '';
					$class = 'section';
					if ( isset( $value['id'] ) ) {
						$id = 'id="' . esc_attr( $value['id'] ) . '" ';
					}
					if ( isset( $value['type'] ) ) {
						$class .= ' section-' . $value['type'];
					}
					if ( isset( $value['class'] ) ) {
						$class .= ' ' . $value['class'];
					}

					$output .= '<div ' . $id . 'class="' . esc_attr( $class ) . '">' . "\n";
					if ( isset($value['name']) ) {
						$output .= '<h4 class="heading">' . esc_html( $value['name'] ) . '</h4>' . "\n";
					}
					if ( isset( $value['desc'] ) ) {
						$output .= apply_filters('of_sanitize_info', $value['desc'] ) . "\n";
					}
					$output .= '</div>' . "\n";
					break;

				// Heading for Navigation
				case "heading":
					$counter++;
					if ( $counter >= 2 ) {
						$output .= '</div>'."\n";
					}
					$class = '';
					$class = ! empty( $value['id'] ) ? $value['id'] : $value['name'];
					$class = preg_replace('/[^a-zA-Z0-9._\-]/', '', strtolower($class) );
					$extraClasses = ! empty( $value['extra-classes'] ) ? $value['extra-classes'] : '';
					$extraClasses = preg_replace('/[^a-zA-Z0-9._\-]/', '', strtolower($extraClasses) );
					$output .= '<div id="options-group-' . $counter . '" class="group ' . $class . ' ' . $extraClasses . '">';
					break;
				case "sub_tab_content_closed":
					$output .= '</div>';
					break;
				case "sub_tab_content_opened":
					$counterSubHeading ++;
					$class = ! empty( $value['class'] ) ? $value['class'] : '';
					$class = preg_replace('/[^a-zA-Z0-9._\-]/', '', strtolower($class) );
					$tabID = ! empty( $value['tab-id'] ) ? $value['tab-id'] : '';
					$tabID = preg_replace('/[^a-zA-Z0-9._\-]/', '', strtolower($tabID) );
					$output .= '<div id="'.$tabID.'" class="sub-group ' . $class . '">';
					break;
				case 'header_block':
					$output .= '<label class="heading '.$hidden.'">'.$value['name'].'</label>';
					break;

				case 'header_tab_js':
					if ( $value['tabs'] ) {
						$liStr = '';
						foreach( $value['tabs'] as $tabKey => $tabLabel ) {
						    $hideSubTabClass = '';
						    if ( has_filter('_apl_hide_fields_unnecessary') ){
						        if (apply_filters('_apl_hide_fields_unnecessary',$tabKey) == 'hidden') {
                                    $hideSubTabClass = 'hidden';
                                }
                            }
                            $liStr .= '<li class="'.$hideSubTabClass.'"><a class="" href="#'.$tabKey.'">'.$tabLabel.'</a></li>';
							$subTabIndex++;
						}
						$output .= '<ul class="sub-tab-wrapper">'.$liStr.'</ul>';
					}
					break;
				case 'header_tab':
					if ( $value['tabs'] ) {
						$liStr = '';
						$tk = 0;
						foreach( $value['tabs'] as $tabKey => $tabLabel ) {
							$tabActive = '';
							if ( ($tk == 0 && ! isset( $_GET['header_tab'] )) ||
								( (isset( $_GET['header_tab'] ) && $_GET['header_tab'] == $tabKey) )) {
								$tabActive = 'active';
							}
							$tk++;
							$liStr .= '<li class="'.$tabActive.'"><a href="'.admin_url().'themes.php?page=options-framework&header_tab='.$tabKey.'">'.$tabLabel.'</a></li>';
						}
						$output .= '<ul class="header-tabs">'.$liStr.'</ul>';
					}
					break;

				case 'hidden':
					$number_class = isset ( $value['class'] ) ? $value['class'] : '';
					$multi = isset($value['multiple']) && $value['multiple'] ? '[]' : '';
					$output .= '<input id="' . esc_attr( $value['id'] ) . '" class="of-input '.$number_class.'" name="' . esc_attr( $option_name . '[' . $value['id'] . ']'.$multi.'' ) . '" type="hidden" value="' . esc_attr(is_string($val) ? $val : '' ) . '" />';
					break;

				case
					'multiselect_sort':
						$mulSortField = new Apollo_TO_MultipleSortField($settings, $value, $option_name, $defaultLang, $languages);
						$output .= $mulSortField->render($value, $val);
					break;
                //Button
                case  "button" :
                    $buttonFieldObj = new Apollo_TO_Button($settings, $value, $option_name, $defaultLang, $languages);
                    $output .= $buttonFieldObj->render();
                    break;
            
			}
            
			if (   ( $value['type'] != "heading" )
				&& ( $value['type'] != "sub_tab_content_opened" )
				&& ( $value['type'] != "sub_tab_content_closed" )
				&& ( $value['type'] != "multiselect_sort" )
				&& ( $value['type'] != "info" )
                && ( $value['type'] != "header_block" )
				&& ( $value['type'] != "header_tab" )
				&& ( $value['type'] != "header_tab_js" )
			) {
				
                if ( $value['type'] != "checkbox" && $value['type'] != "editor" && $value['type'] != "upload" ) {
					$previewImage = isset($value['preview']) && $value['preview'] ? '<a data-lightbox="preview-image-option-' . $value['id'] . '" class="preview-option-image-wrapper"><img class="preview-option-image"></a>' : '';
					$output .= '<div class="explain">' . $previewImage . wp_kses( $explain_value, $allowedtags) . '</div>'."\n";
				}
                
                $output .= '</div>';
				
				$output .= '</div></div>'."\n";
			}
			echo $output;
		}
		// Outputs closing div if there tabs
		if ( Options_Framework_Interface::optionsframework_tabs() != '' ) {
			echo '</div>';
		}

	}

}