<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of class-options-framwork-color
 *
 * @author vulh
 */
class Apollo_OF_Color {

    protected $cssData = array();
    protected $options = false;

    public function __construct($options) {
        $this->options = $options;
    }

    private function getValue($key) {
        return isset($this->options[$key]) ? $this->options[$key] : '';
    }

    public function getContent(){

        $subL1Color = $this->getValue(Apollo_DB_Schema::_NAV_SUB_L1_COLOR);
        $subL1HoverColor = $this->getValue(Apollo_DB_Schema::_NAV_SUB_L1_HOVER_COLOR);
        $tierMainHoverColor = $this->getValue(Apollo_DB_Schema::_NAV_TWO_TIER_MAIN_HOVER_BG_COLOR);

        $arrayCss = array(

            // Slider
            array(
                'class' => '.second-slider .slider-content h2 a,
                            .main-slider .slides a,
                            .second-slider.second-slider-v1 .slider-content h2 a
                            ',
                'value' => $this->getValue(Apollo_DB_Schema::_COLOR_SELECTION_FONT_TITLE),
                'property' => 'color'
            ),
            array(
                'class' => '.second-slider .slider-content .desc,
                            .i-slider .i-caption p,
                            .second-slider .slider-content .meta.auth,
                            .second-slider .meta.auth a,
                            .second-slider.second-slider-v1 .slider-content .desc,
                            .second-slider.second-slider-v1 .slider-content .meta.auth,
                            .second-slider.second-slider-v1 .meta.auth a,
                            .second-slider.second-slider-v1 .slider-content .meta.auth a,
                            .semi-trans-large .i-caption p, .semi-trans-small .i-caption p
                            ',
                'value' => $this->getValue(Apollo_DB_Schema::_COLOR_SELECTION_FONT),
                'property' => 'color'
            ),
            array(
                'class' => '.second-slider li.apl-slider-bg,
                            .second-slider.second-slider-v1 li.apl-slider-bg,
                            .second-slider.second-slider-v1 li.apl-slider-bg .slider-content
                           ',
                'value' => $this->getValue(Apollo_DB_Schema::_SLIDER_BACKGROUND),
                'property' => 'background-color'
            ),
            array(
                'class' => '.second-slider .silder-footer,
                            .second-slider .flex-control-nav,
                            .second-slider.second-slider-v1 .silder-footer,
                            .second-slider.second-slider-v1 .flex-control-nav',
                'value' => $this->getValue(Apollo_DB_Schema::_ROTATION_BAR_BACKGROUND),
                'property' => 'background'
            ),
            array(
                'class' => '.second-slider .slider-content .date,
                            .second-slider.second-slider-v1 .slider-content .date',
                'value' => $this->getValue(Apollo_DB_Schema::_COLOR_SELECTION_FONT_DATE),
                'property' => 'color'
            ),
            array(
                'class' => '.second-slider .slider-content a.vmore,
                            .second-slider.second-slider-v1 .slider-content a.vmore',
                'value' => $this->getValue(Apollo_DB_Schema::_COLOR_SELECTION_VIEW_MORE),
                'property' => 'color'
            ),
            // End slider


            // Menu
            array(
                'class' => 'ul.sub-menu.level-1 > li > a, #main_nav li.selected li a',
                'value' => $this->getValue(Apollo_DB_Schema::_NAV_SUB_L1_COLOR) . '!important',
                'property' => 'color'
            ),
            array(
                'class' => 'ul.sub-menu.level-1 > li > a:hover, #main_nav li.selected ul li:hover > a ',
                'value' => $this->getValue(Apollo_DB_Schema::_NAV_SUB_L1_HOVER_COLOR) . '!important',
                'property' => 'color'
            ),
            array(
                'class' => 'ul.sub-menu.level-1 > li > a:hover, #main_nav li.selected ul li:hover > a ',
                'value' => $this->getValue(Apollo_DB_Schema::_NAV_SUB_L1_HOVER_BG_COLOR) . '!important',
                'property' => 'background'
            ),
            array(
                'class' => '.mn-menu .has-child > ul.sub-menu.level-1 > li.has-child > a:after',
                'value' => $subL1Color ? $subL1Color.' transparent transparent transparent' : '',
                'property' => 'border-color'
            ),
            array(
                'class' => '.mn-menu .has-child > ul.sub-menu.level-1 > li.has-child > a:hover:after',
                'value' => $subL1HoverColor ? 'transparent transparent transparent '. $subL1HoverColor : '',
                'property' => 'border-color'
            ),
            array(
                'class' => 'ul.sub-menu.level-2, #main_nav li.selected ul li .menu-hover li > a',
                'value' => $this->getValue(Apollo_DB_Schema::_NAV_SUB_L2_BG_COLOR) . '!important',
                'property' => 'background-color'
            ),

            array(
                'class' => 'ul.sub-menu.level-2 li a:hover, #main_nav li.selected ul li .menu-hover li:hover > a',
                'value' => $this->getValue(Apollo_DB_Schema::_NAV_SUB_L2_HOVER_COLOR). ' !important',
                'property' => 'color'
            ),

            array(
                'class' => '.main-menu .mn-menu .has-child > ul.sub-menu li.has-child .sub-menu li.dropdown:hover > a:after',
                'value' => 'transparent transparent  transparent ' . $this->getValue(Apollo_DB_Schema::_NAV_SUB_L2_HOVER_COLOR). ' !important',
                'property' => 'border-color'
            ),

            array(
                'class' => 'ul.sub-menu.level-2 li a:hover, #main_nav li.selected ul li:hover .menu-hover a:hover',
                'value' => $this->getValue(Apollo_DB_Schema::_NAV_SUB_L2_HOVER_BG_COLOR). ' !important',
                'property' => 'background-color'
            ),

            array(
                'class' => 'ul.sub-menu.level-2 li a, #main_nav li.selected ul li .menu-hover li a',
                'value' => $this->getValue(Apollo_DB_Schema::_NAV_SUB_L2_COLOR). ' !important',
                'property' => 'color'
            ),
            array(
                'class' => '.two-tiers #main_nav > li > a:hover',
                'value' => ($tierMainHoverColor ? $tierMainHoverColor : 'none'). ' !important',
                'property' => 'background'
            ),

            array(
                'class' => '.two-tiers #main_nav > li.selected > a',
                'value' => ($tierMainHoverColor ? $tierMainHoverColor : ' none'). ' !important',
                'property' => 'background-color'
            ),
            array(
                'class' => 'ul.sub-menu.level-1, #main_nav > li.selected > a , #main_nav li.selected > ul, .menu-background',
                'value' => $this->getValue(Apollo_DB_Schema::_NAV_SUB_L1_BG_COLOR) . '!important',
                'property' => 'background-color'
            ),

        );

        $colorCss = '';
        foreach($arrayCss as $value){
            if($value['value'] != '' && $value['property'] != ''){
                $colorCss .= $value['class'].'{
                    '.$value['property'].' : '.$value['value'].';
                }';
            }
        }

        return $colorCss;
    }

}
