<?php
/**
 * @package   Options_Framework
 * 
 */

class Options_Framework_Admin {

	/**
     * Page hook for the options screen
     *
     * @type string
     */
    protected $options_screen = null;

    /**
     * Save notice info
     * @type string
     */
    protected $notice = null;
    
    protected $should_update_newoptions = false;
    protected $newoptions = array();

    /**
     * Hook in the scripts and styles
     *
     */
    public function init() {

        // Add the options page and menu item.
        add_action( 'admin_menu', array( $this, 'add_custom_options_page' ) );

		// Checks if options are available
    	if ( Apollo_App::isThemeOption() ) {
            // Gets options to load
            if (!$options = & Options_Framework::_optionsframework_options()) return;

            // Add the required scripts and styles
            add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_styles' ) );
            add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_scripts' ) );

            // Settings need to be registered after admin_init
            add_action( 'admin_init', array( $this, 'settings_init' ) );

            // Adds options menu to the admin bar
            add_action( 'wp_before_admin_bar_render', array( $this, 'optionsframework_admin_bar' ) );

            add_action( 'apollo_theme_option_save', array( $this, 'clear_page_cache' ) );
        }
        
    }

    function clear_page_cache() {
        global $wp_rewrite;
        $wp_rewrite->init();
        $wp_rewrite->flush_rules();
    }


    /**
     * Registers the settings
     * 
     */
    function settings_init() {

        register_setting( 'optionsframework', of_get_option_id(),  array ( $this, 'validate_options' ) );

        // Displays notice after options save
        add_action( 'optionsframework_after_validate', array( $this, 'save_options_notice' ) );


    }

    function re_settings() {
		// Registers the settings fields and callback
		register_setting( 'optionsframework', of_get_option_id(),  array ( $this, 're_validate_options' ) );
    }

	/*
	 * Define menu options (still limited to appearance section)
	 *
	 * Examples usage:
	 *
	 * add_filter( 'optionsframework_menu', function( $menu ) {
	 *     $menu['page_title'] = 'The Options';
	 *	   $menu['menu_title'] = 'The Options';
	 *     return $menu;
	 * });
	 *
	 *
	 */
	static function menu_settings() {

		$menu = array(
			'page_title' => __( 'Theme Options', 'apollo' ),
			'menu_title' => __( 'Theme Options', 'apollo' ),
			'capability' => 'edit_theme_options',
			'menu_slug' => 'options-framework'
		);

		return apply_filters( 'optionsframework_menu', $menu );
	}

	/**
     * Add a subpage called "Theme Options" to the appearance menu.
     */
	function add_custom_options_page() {
        
		$menu = $this->menu_settings();
		$this->options_screen = add_theme_page( $menu['page_title'], $menu['menu_title'], $menu['capability'], $menu['menu_slug'], array( $this, 'options_page' ) );
        
        $menu = array(
			'page_title' => __( 'Theme Options', 'apollo' ),
			'menu_title' => __( 'Theme Options', 'apollo' ),
			'capability' => 'edit_theme_options',
			'menu_slug' => 'options-framework'
		);
        
        add_theme_page( __( 'Re-active Theme', 'apollo' ), __( 'Re-active Theme', 'apollo' ), 're_active_theme', 're-active-theme', array( $this, 're_active_theme' ) );

	}

	/**
     * Loads the required stylesheets
     *
     */
	function enqueue_admin_styles( $hook ) {
		if ( $this->options_screen != $hook )
	        return;
		wp_enqueue_style( 'optionsframework', OPTIONS_FRAMEWORK_DIRECTORY . 'css/optionsframework.css', array(), Options_Framework::VERSION );
		wp_enqueue_style( 'wp-color-picker' );
	}

	/**
     * Loads the required javascript
     *
     */
	function enqueue_admin_scripts( $hook ) {

		if ( $this->options_screen != $hook )
	        return;

		// Enqueue custom option panel JS
        wp_register_script('codemirror-main', OPTIONS_FRAMEWORK_DIRECTORY . 'plugin/codemirror/lib/codemirror.js', array('jquery'));
        wp_register_script('codemirror-css', OPTIONS_FRAMEWORK_DIRECTORY . 'plugin/codemirror/lib/css.js', array('jquery'));
        wp_register_script('codemirror-show-hint', OPTIONS_FRAMEWORK_DIRECTORY . 'plugin/codemirror/addon/hint/show-hint.js', array('jquery'));
        wp_register_script('codemirror-css-hint', OPTIONS_FRAMEWORK_DIRECTORY . 'plugin/codemirror/addon/hint/css-hint.js', array('jquery'));
        wp_register_script('codemirror-javascript', OPTIONS_FRAMEWORK_DIRECTORY . 'plugin/codemirror/lib/javascript.js', array('jquery'));
        wp_register_script('codemirror-matchbrackets', OPTIONS_FRAMEWORK_DIRECTORY . 'plugin/codemirror/addon/edit/matchbrackets.js', array('jquery'));
        wp_register_script('codemirror-continuecomment', OPTIONS_FRAMEWORK_DIRECTORY . 'plugin/codemirror/addon/comment/continuecomment.js"', array('jquery'));
        wp_register_script('codemirror-comment', OPTIONS_FRAMEWORK_DIRECTORY . 'plugin/codemirror/addon/comment/comment.js"', array('jquery'));
        wp_register_script('codemirror-autorefresh', OPTIONS_FRAMEWORK_DIRECTORY . 'plugin/codemirror/addon/display/autorefresh.js"', array('jquery'));
        wp_enqueue_script( 'theme-options-codemirror', OPTIONS_FRAMEWORK_DIRECTORY . 'plugin/codemirror/addon/hint/css-hint.js',
            array('codemirror-main', 'codemirror-matchbrackets', 'codemirror-continuecomment', 'codemirror-comment', 'codemirror-javascript','codemirror-autorefresh', 'codemirror-css', 'codemirror-show-hint'), Options_Framework::VERSION );

        wp_enqueue_style( 'codemirror-main', OPTIONS_FRAMEWORK_DIRECTORY . 'plugin/codemirror/lib/codemirror.css', array(), Options_Framework::VERSION );
        wp_enqueue_style( 'codemirror-show-hint', OPTIONS_FRAMEWORK_DIRECTORY . 'plugin/codemirror/addon/hint/show-hint.css', array(), Options_Framework::VERSION );
        wp_enqueue_script( 'theme-options-function', OPTIONS_FRAMEWORK_DIRECTORY . 'js/theme-options/theme-options-function.js', array( 'jquery','wp-color-picker','jquery-ui-tabs' ), Options_Framework::VERSION );
        wp_enqueue_script( 'override-html-static-content', OPTIONS_FRAMEWORK_DIRECTORY . 'js/theme-options/override-html-static-content.js', array( 'jquery','wp-color-picker','jquery-ui-tabs' ), Options_Framework::VERSION );

        // Inline scripts from options-interface.php
		add_action( 'admin_head', array( $this, 'of_admin_head' ) );
	}

	function of_admin_head() {
		// Hook to add custom scripts
		do_action( 'optionsframework_custom_scripts' );
	}

	/**
     * Builds out the options panel.
     *
	 * If we were using the Settings API as it was intended we would use
	 * do_settings_sections here.  But as we don't want the settings wrapped in a table,
	 * we'll call our own custom optionsframework_fields.  See options-interface.php
	 * for specifics on how each individual field is generated.
	 *
	 * Nonces are provided using the settings_fields()
	 *
     */
	 function options_page() {

	     /* Ticket 13608
         * Handle backup before restore theme options data
	     */
         if ( isset($_SESSION['export-before-restore'])) {
             echo "<script> window.open('{$_SESSION['export-before-restore']}', '_blank');</script>";
             unset($_SESSION['export-before-restore']);
         }
         /* Handle import data to restore theme options data */
         if(isset($_FILES['restore-theme-options']) && !empty($_FILES['restore-theme-options'])
            && isset($_POST['import-theme-option-processing']) && !empty($_POST['import-theme-option-processing']))
         {
             // Instantiate the apollo backup/import settings data.
             $apolloBackupTool = new Apollo_Options_Import();
             $optionKeys = array(); // & Options_Framework::_optionsframework_options();
             $resultImport = $apolloBackupTool->processImportingOptionData($_FILES['restore-theme-options'], $optionKeys);
             if($resultImport){
                 $_SESSION['restore-process-success'] = 1;
                 wp_redirect(admin_url('/themes.php?page=options-framework'));
                 exit();
             } else {
                 $errorMessages = $apolloBackupTool->getErrorMessage(true);
             }
         }

         if ( isset( $_SESSION['is-request-backup-theme'] ) ) {
             // This will be move to location of handling request Import/Export.
             // Instantiate the apollo backup/import settings data.
             unset($_SESSION['is-request-backup-theme']);
             $url = admin_url( 'themes.php?page=options-framework&process_backup_theme_options=yes' );
             echo "<script> window.open('{$url}', '_blank');</script>";
         }

         if ( isset( $_GET['process_backup_theme_options'] ) ) {
             // Instantiate the apollo backup/import settings data.
             $apolloBackupTool = new Apollo_Options_Export();
             $optionKeys = & Options_Framework::_optionsframework_options();
             $resBackUpThemeOpt = $apolloBackupTool->processBackupOptionData($_POST, $optionKeys);
             if(!$resBackUpThemeOpt){
                 $errorMessages = $apolloBackupTool->getErrorMessage(true);
             } else {
                 exit();
             }
         }

         $warningMsg = __("WARNING: The Active member (Site config > Organization) field config is enabled. If the 'Enable Registered Organizations drop menu' (Site config > Event) config is disabled, you can not submit an event from FE event form. Do you want continue?", 'apollo');

         ?>

		<div id="optionsframework-wrap" class="wrap">

		<?php $menu = $this->menu_settings(); ?>
		<h2><?php echo esc_html( $menu['page_title'] ); ?></h2>
        <!--backup-theme-options-->
        <div class="wrap-custom">
            <span class="error-message"><?php echo !empty($errorMessages) ? $errorMessages : ""; ?></span>
            <?php if(isset($_SESSION['restore-process-success'])) :
                        unset($_SESSION['restore-process-success']);
//                        unset($_SESSION['ignore-validate-option']);
                ?>
                <span class="custom-text-success"><?php echo __("Theme option is restored successfully!","apollo"); ?></span>
            <?php endif; ?>
        </div>
        <div class="wrap-custom">
            <a href="#" class="backup-settings button-primary left"><?php esc_attr_e( 'Export', 'apollo' ); ?></a>
            <label for="restore-theme-options">
                <a href="#" class="restore-theme-options-button button-primary left"><?php esc_attr_e( 'Import', 'apollo' ); ?></a>
            </label>
            <form id="restore-theme-option-data" method="post" action="<?php echo admin_url('/themes.php?page=options-framework'); ?>" name="restore-theme-option-data" enctype="multipart/form-data">
                <input type="file" name="restore-theme-options" id="restore-theme-options" class="restore-theme-options hidden" />
            </form>
        </div>

	    <h2 class="nav-tab-wrapper">
	        <?php echo Options_Framework_Interface::optionsframework_tabs(); ?>
	    </h2>

	    <?php settings_errors( 'options-framework' ); ?>

	    <div id="optionsframework-metabox" class="metabox-holder">
		    <div id="optionsframework" class="postbox">
				<form action="options.php" method="post" id="form-theme-options">
                <div id="optionsframework-submit">
					<input data-warning-msg="<?php echo $warningMsg ?>" type="submit" class="button-primary apl-submit-option" name="update" value="<?php esc_attr_e( 'Save Options', 'apollo' ); ?>" />
					<input type="submit" class="reset-button button-secondary" name="reset" value="<?php esc_attr_e( 'Restore Defaults', 'apollo' ); ?>" onclick="return confirm( '<?php print esc_js( __( 'Click OK to reset. Any theme settings will be lost!', 'apollo' ) ); ?>' );" />
					<div class="clear"></div>
				</div>    
				<?php settings_fields( 'optionsframework' ); ?>
				<?php Options_Framework_Interface::optionsframework_fields(); /* Settings */ ?>
				<div id="optionsframework-submit">
					<input type="submit" data-warning-msg="<?php echo $warningMsg ?>" class="button-primary apl-submit-option" name="update" value="<?php esc_attr_e( 'Save Options', 'apollo' ); ?>" />
					<input type="submit" class="reset-button button-secondary" name="reset" value="<?php esc_attr_e( 'Restore Defaults', 'apollo' ); ?>" onclick="return confirm( '<?php print esc_js( __( 'Click OK to reset. Any theme settings will be lost!', 'apollo' ) ); ?>' );" />
					<div class="clear"></div>
				</div>
				</form>
			</div> <!-- / #container -->
		</div>
		
		</div> <!-- / .wrap -->

	<?php
	}

    function re_validate_options() {
         
        return $this->newoptions;
    }
    
    /**
     * Validate Options.
     *
     * This runs after the submit/reset button has been clicked and
     * validates the inputs.
     *
     * @uses $_POST['reset'] to restore default options
     */
    function validate_options( $input ) {

        if ( get_option( 'apollo_re_active_theme' ) ) {
            return;
        }

        do_action('apollo_theme_option_save');



        /*
         * Restore Defaults.
         *
         * In the event that the user clicked the "Restore Defaults"
         * button, the options defined in the theme's options.php
         * file will be added to the option for the active theme.
         */

        if ( isset( $_POST['reset'] ) ) {

            add_settings_error( 'options-framework', 'restore_defaults', __( 'Default options restored.', 'apollo' ), 'updated fade' );

            /* Ticket 13608
            * Handle backup before restore theme options data
	        */
            $apolloBackupTool = new Apollo_Options_Export();
            $optionKeys = &Options_Framework::_optionsframework_options();
            $resBackUpThemeOpt = $apolloBackupTool->processBackupOptionData($_POST, $optionKeys, true);
            if($resBackUpThemeOpt) {
                $_SESSION['export-before-restore'] = $resBackUpThemeOpt;
            }
            return $this->get_default_values();
        }


        /*
         * Update Settings
         *
         * This used to check for $_POST['update'], but has been updated
         * to be compatible with the theme customizer introduced in WordPress 3.4
         */


        /* Handle for territory data */
        if(isset($input[Apollo_DB_Schema::_TERR_DATA_FULL_PASS]) && !empty($input[Apollo_DB_Schema::_TERR_DATA_FULL_PASS])){
            $decodeJsonOutputComponent = urldecode($input[Apollo_DB_Schema::_TERR_DATA_FULL_PASS]);
            $jsonDecode = json_decode($decodeJsonOutputComponent,true);
            $input[Apollo_DB_Schema::_TERR_DATA] = $jsonDecode['customTerrData'];
            unset($input[Apollo_DB_Schema::_TERR_DATA_FULL_PASS]);
        }
        /* End of handle for territory data */



		$clean = array();
		$options = & Options_Framework::_optionsframework_options();
        
        global $sitepress, $apollo_theme_wplm;
        $languages = $sitepress ? $sitepress->get_active_languages() : false;

        foreach ($options as $option) {

            if (!isset($option['id'])) {
                continue;
            }

            if (!isset($option['type'])) {
                continue;
            }

            $id = preg_replace('/[^a-zA-Z0-9._\-]/', '', strtolower($option['id']));

            // Set checkbox to false if it wasn't sent in the $_POST
            if ('checkbox' == $option['type'] && !isset($input[$id])) {
                $input[$id] = false;
            }

            // Set each item in the multicheck to false if it wasn't sent in the $_POST
            if ('multicheck' == $option['type'] && !isset($input[$id])) {
                foreach ($option['options'] as $key => $value) {
                    $input[$id][$key] = false;
                }
            }

            // Not apply filter
            if (isset($option['apply_filter']) && !$option['apply_filter']) {
                $clean[$id] = $input[$id];
                continue;
            }

            // For a value to be submitted to database it must pass through a sanitization filter
            if (has_filter('of_sanitize_' . $option['type'])) {
                if (isset($option['is_static_file']) && $option['is_static_file']) {
                    if (!isset($option['filetype']) || $option['filetype'] == 'html' || $option['filetype'] == 'php' || $option['filetype'] == 'js') {
                        $fileType = isset($option['filetype']) ? $option['filetype'] : 'html';
                        self::update_html_file($input[$id], $id, false, $fileType);
                    } else {
                        // Update override css
                        $this->update_override_css($input);
                    }

                    $clean[$id] = '';
                } else {
                    $clean[$id] = apply_filters('of_sanitize_' . $option['type'], isset($input[$id]) ? $input[$id] : '', $option);

                    // Translation value
                    if (Apollo_App::hasWPLM() && in_array($id, $apollo_theme_wplm) && $languages) {
                        $langKeys = array_keys($languages);

                        foreach ($langKeys as $lkey):
                            $_id = $id . '_' . $lkey;
                            $clean[$_id] = apply_filters('of_sanitize_' . $option['type'], isset($input[$_id]) ? $input[$_id] : '', $option);
                        endforeach;
                    }

                }

            }
        }



        // Update primary color css
        $this->update_primary_color_css( $input ); 

        // Hook to run after validation
        do_action( 'optionsframework_after_validate', $clean );
        
        
        // Check input data here
        if ( $clean[Apollo_DB_Schema::_FACEBOOK] && ! filter_var( $clean[Apollo_DB_Schema::_FACEBOOK], FILTER_VALIDATE_URL ) ) {
            $clean[Apollo_DB_Schema::_FACEBOOK] = of_get_option( Apollo_DB_Schema::_FACEBOOK );
        }
        
        if ( $clean[Apollo_DB_Schema::_LINKEDIN] && ! filter_var( $clean[Apollo_DB_Schema::_LINKEDIN], FILTER_VALIDATE_URL ) ) {
            $clean[Apollo_DB_Schema::_LINKEDIN] = of_get_option( Apollo_DB_Schema::_LINKEDIN );
        }
        
        if ( $clean[Apollo_DB_Schema::_TWITTER] && ! filter_var( $clean[Apollo_DB_Schema::_TWITTER], FILTER_VALIDATE_URL ) ) {
            $clean[Apollo_DB_Schema::_TWITTER] = of_get_option( Apollo_DB_Schema::_TWITTER );
        }   
        
        if ( $clean[Apollo_DB_Schema::_YOUTUBE] && ! filter_var( $clean[Apollo_DB_Schema::_YOUTUBE], FILTER_VALIDATE_URL ) ) {
            $clean[Apollo_DB_Schema::_YOUTUBE] = of_get_option( Apollo_DB_Schema::_YOUTUBE );
        }

        if ( $clean[Apollo_DB_Schema::_NUMBER_FEATURED_EVENTS] && ! filter_var( $clean[Apollo_DB_Schema::_NUMBER_FEATURED_EVENTS], FILTER_VALIDATE_INT ) ) {
            $clean[Apollo_DB_Schema::_NUMBER_FEATURED_EVENTS] = of_get_option( Apollo_DB_Schema::_NUMBER_FEATURED_EVENTS );
        }
        
        if ( $clean[Apollo_DB_Schema::_NUM_HOME_SPOTLIGHT_EVENT] && ! filter_var( $clean[Apollo_DB_Schema::_NUM_HOME_SPOTLIGHT_EVENT], FILTER_VALIDATE_INT ) ) {
            $clean[Apollo_DB_Schema::_NUM_HOME_SPOTLIGHT_EVENT] = of_get_option( Apollo_DB_Schema::_NUM_HOME_SPOTLIGHT_EVENT );
        }

        // Backup Theme Options
        if ( isset( $_POST['backup-theme-options'] ) ) {
            $_SESSION['is-request-backup-theme'] = 1;
        }

        /* Thienld : trigger empty all local caches for renewing cache after saving theme-option on current site */
        $cacheManagementFilePath = APOLLO_INCLUDES_DIR. '/admin/tools/cache/Inc/apollo-sites-caching-management.php';
        if(file_exists($cacheManagementFilePath)){
            require_once $cacheManagementFilePath;
            if(class_exists('APLC_Site_Caching_Management')){
                $currentSiteID = get_current_blog_id();
                $cleanCachedSite = new APLC_Site_Caching_Management($currentSiteID);
                $cleanCachedSite->showAdminMessage = false;
                $cleanCachedSite->emptyAllLocalCaches();
                // Keep all option in file for next query theme options from frontend area.
                $cleanCachedSite->cacheThemeOptions($clean);
            }
        }


        /**
        * Trilm: handle filtering_by_region_zip_list
        *
         */
        if(isset($input[Apollo_DB_Schema::_APL_FILTERING_BY_REGION_ZIP_LIST]) && !empty($input[Apollo_DB_Schema::_APL_FILTERING_BY_REGION_ZIP_LIST])){
            $temp = json_encode($input[Apollo_DB_Schema::_APL_FILTERING_BY_REGION_ZIP_LIST]);
            unset( $input[Apollo_DB_Schema::_APL_FILTERING_BY_REGION_ZIP_LIST]);
            $input[Apollo_DB_Schema::_APL_FILTERING_BY_REGION_ZIP_LIST] = $temp;
        }
        $clean[Apollo_DB_Schema::_APL_FILTERING_BY_REGION_LISTING] =  $input[Apollo_DB_Schema::_APL_FILTERING_BY_REGION_ZIP_LIST];


        /**
         * @Ticket #19528 - Home Featured Articles
         */
        /** Get data from input with the import action */
        $homeFeaturedPostOptions = array(
            Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_TYPE,
            Apollo_DB_Schema::_HOME_FEATURED_ARTICLES_TITLE,
            Apollo_DB_Schema::_ACTIVE_HOME_FEATURED_ARTICLES,
            Apollo_DB_Schema::_HOME_CMS_PAGE_BLOG_FEATURED,
            Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_LARGE_LEFT,
            Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_LARGE_RIGHT,
            Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_TOP,
            Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_MIDDLE,
            Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_BOTTOM,
            Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_POSTION
        );
        if (!isset($_POST['import-theme-option-processing'])) {
            foreach ($homeFeaturedPostOptions as $key) {
                $clean[$key] = of_get_option($key);
            }
        } else {
            foreach ($homeFeaturedPostOptions as $key) {
                if (isset($input[$key])) {
                    $clean[$key] = $input[$key];
                }
            }
        }

        return $clean;
    }


    /**
     * Update override css
     * @param string $css CSS content
     * @return boolean | void
     */ 
    public function update_override_css( $input ) {

        $file = get_override_css_abs_path();

        if(file_exists($file) && !is_writable($file)) {
            $this->notice = __('Can not write override css file. Please set write permission for ', 'apollo'). $file;
            return false;
        }
        $content = $input[Apollo_DB_Schema::_OVERRIDE_CSS] ? $input[Apollo_DB_Schema::_OVERRIDE_CSS] : render_php_to_string( APOLLO_THEME_OVERRIDE_CSS );
        file_put_contents( $file, $content );
    }
    
    public function update_primary_color_css( $input ) {
        
        // Update primary color 
        $primary_color = $input[Apollo_DB_Schema::_PRIMARY_COLOR];
        
        $hover_active_step = check_is_bright_color( $primary_color ) ? -15 : 50 ;
        $brightestStep = check_is_bright_color( $primary_color ) ? -15 : 180 ;
        $menu_border_step = 30;
        $currentTheme = function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '';
        $file = apply_filters('_load_primary_color', APOLLO_ADMIN_DIR. '/theme-options/default-template/primary-color.less');
        if ( $currentTheme == APL_SM_DIR_THEME_NAME) {
            $file = SONOMA_ADMIN_DIR. '/theme-options/default-template/primary-color.less';
        }
        $css_content = @file_get_contents( $file );
        if ( ! $css_content ) {
            echo sprintf( __( '%s not exists ', 'vpps' ), $file );
            exit;
        }
        /* user dashboard css */
		$css_content = str_replace( '$user_dashboard_calendar_', get_new_color_from_pattern('#7a6617', '#e8b96a', $primary_color), $css_content);
        $css_content = str_replace( '$user_dashboard_navbar_background_color', get_new_color_from_pattern('#fcb234', '#e8b96a', $primary_color), $css_content);
        $css_content = str_replace( '$user_dashboard_li_border_bottom_color', get_new_color_from_pattern('#fcb234', '#f1c170', $primary_color), $css_content);
        $css_content = str_replace( '$user_dashboard_sub_li_border_left_color', get_new_color_from_pattern('#fcb234', '#e0b162', $primary_color), $css_content);
        $css_content = str_replace( '$user_dashboard_sub_li_border_bottom_color', get_new_color_from_pattern('#fcb234', '#D29E48', $primary_color), $css_content);
        $css_content = str_replace( '$user_dashboard_sub_ul_background_color', get_new_color_from_pattern('#fcb234', '#ca9843', $primary_color), $css_content);
        $css_content = str_replace( '$user_dashboard_sub_a_background_color', get_new_color_from_pattern('#fcb234', '#bd8d3c', $primary_color), $css_content);
        $css_content = str_replace( '$user_dashboard_a_border_left_color', get_new_color_from_pattern('#fcb234', '#a77e3a', $primary_color), $css_content);
        $css_content = str_replace( '$user_dashboard_sub_a_active_bg_color', get_new_color_from_pattern('#fcb234', '#bd8d3c', $primary_color), $css_content);
        //$user_dashboard_sub_a_active_bg_color

        //$selected_tab_color =  check_is_bright_color( $color ) ? '#6a6363' : '#ffffff';
        //$css_content = str_replace( '$selected_tab_color', $selected_tab_color, $css_content );
        $css_content = str_replace( '$main_color_event_block_icon', adjust_brightness( $primary_color, $brightestStep ), $css_content );
        $css_content = str_replace( '$main_color_brightness', adjust_brightness( $primary_color, $hover_active_step ), $css_content );
        $css_content = str_replace( '$main_color_menu_border', adjust_brightness( $primary_color, $menu_border_step ), $css_content );
        $css_content = str_replace( '$main_color', $primary_color, $css_content );
        
        /* hovering search color */
        $hovering_search_content_color = $input[Apollo_DB_Schema::_HOVERING_SEARCH_CONTENT_COLOR];
        $css_content = str_replace( '[hover_search_content_color]', $hovering_search_content_color, $css_content );
        
        $css_content = str_replace( '$eventWidgetHover', $hovering_search_content_color, $css_content );

        /** @Ticket #16467 - Child theme - Top header height option*/
        $css_content = apply_filters('_apl_custom_primary_css', $css_content, $input);
        
        $file = get_primary_color_css_abs_path();
        $sites = get_current_site();

        if(file_exists($file) && !is_writable($file)) {
            $this->notice = __('Can not write Primary color css file. Please set write permission for ', 'apollo').$file;
            return false;
        }

        // Update color
        $colorClass = new Apollo_OF_Color($input);
        $colorContent = $colorClass->getContent();

        file_put_contents( $file, $css_content. $colorContent);
    }
    
    /**
	 * Display message when options have been saved
	 */

	function save_options_notice() {
      
		add_settings_error( 'options-framework', 'save_options', 
            $this->notice ? $this->notice : __('Options saved', 'apollo') , 'updated fade' );
	}

	/**
	 * Get the default values for all the theme options
	 *
	 * Get an array of all default values as set in
	 * options.php. The 'id','std' and 'type' keys need
	 * to be defined in the configuration array. In the
	 * event that these keys are not present the option
	 * will not be included in this function's output.
	 *
	 * @return array Re-keyed options configuration array.
	 *
	 */ 

	function get_default_values() {
        $output = array();
        $config = & Options_Framework::_optionsframework_options();
        foreach ( (array) $config as $option ) {
                if ( ! isset( $option['id'] ) ) {
                        continue;
                }
                if ( ! isset( $option['std'] ) ) {
                        continue;
                }
                if ( ! isset( $option['type'] ) ) {
                        continue;
                }

                if ( has_filter( 'of_sanitize_' . $option['type'] ) ) {
        if ( isset( $option['is_static_file'] ) && $option['is_static_file'] ) {
            if ( !isset( $option['filetype'] ) || $option['filetype'] == 'html' || $option['filetype'] == 'php' ) {
                // Truonghn - updates #9711 - Custom default template when restore default in theme options
                $defaultTemplateContent = '';
                if( has_filter('_apl_custom_theme_options_file_path_handler')  ) {
                    $defaultTemplateContent = apply_filters('_apl_custom_theme_options_file_path_handler', $option['id']);
                }
                $content = !empty($defaultTemplateContent) ?  render_php_to_string($defaultTemplateContent) : $option['std'];
                self::update_html_file( $content , $option['id'], FALSE, $option['filetype'] );
            }
            $output[$option['id']] = '';

        } else {
            $output[$option['id']] = apply_filters( 'of_sanitize_' . $option['type'], $option['std'], $option );
        }
                }
        }
        $featured_tab = of_get_option( Apollo_DB_Schema::_HOME_CAT_FEATURES );
        $output[Apollo_DB_Schema::_HOME_CAT_FEATURES] = $featured_tab;
        $this->update_primary_color_css( $output );
        return $output;
	}

	/**
	 * Add options menu item to admin bar
	 */

	function optionsframework_admin_bar() {

		$menu = $this->menu_settings();
		global $wp_admin_bar;

		$wp_admin_bar->add_menu( array(
			'parent' => 'appearance',
			'id' => 'of_theme_options',
			'title' => __( 'Theme Options', 'apollo' ),
			'href' => admin_url( 'themes.php?page=' . $menu['menu_slug'] )
		) );
	}
    
    /**
     * Update html file
     * @param string $content HTML content
     * @return boolean | void
     */ 
    public static function update_html_file( $content, $filename, $check_exists = FALSE, $ext = 'html' ) {
       
        if ( ! $ext ) $ext = 'html';
        
        $file = get_html_file_abs_path( $filename, $ext );
        
        if( file_exists( $file ) && !is_writable( $file ) ) {
            //$this->notice = __('Can not write file. Please set write permission for ', 'apollo'). $file;
            return false;
        }
        
        /**
         * If file exists when re-activate theme should not override the file
         */
        if ( $check_exists && file_exists($file) ) {
            return true;
        }
        
        file_put_contents( $file, $content );
    }
    
   /**
     * Re active theme
     */
    public function re_active_theme() {
        // Keep processing re active theme
        register_setting( 'apollo_active_theme', 'apollo_re_active_theme');
        update_option( 'apollo_re_active_theme' , 1, 'no' );
        $install = new Apollo_Install_Theme();
        //$install->cleanup();

        //$install->create_roles();

        $install->create_account_manager_role();
        
        // Create default page register - Register page,
        $install->create_default_pages();

        /** Ignore remove widget when reactive a theme */
//        $install->backward_widget_sidebar_name();
        
        // Insert default data
        $install->insert_default_data();
        
        // Update table
        $install->create_tables();
        
        $this->add_new_options();
        if ( $this->should_update_newoptions ) {
            // Load Options Framework Settings
            update_option( of_get_option_id() , $this->newoptions );
            
        }

        /**
         * When re-active theme, we will update activation version option
         *
         * @ticket #11493
         */
        $install->updateThemeVersion();

        // Update email templates
        $install->create_default_email_templates();
        
        $url = admin_url( 'themes.php?page=theme_activation_options' );

        echo "<script>window.location.href = '{$url}'</script>";
    }
    
    function add_new_options() {
        // Default and db options, update new opts
        $options = optionsframework_options();
        
        // Old options
        $old_opts = get_option( of_get_option_id() );
        
        $db_opts = array_keys( $old_opts );
        foreach( $options as $o ) {

            if ( isset( $o['is_static_file'] ) && $o['is_static_file'] ) {

                $file = get_html_file_abs_path( $o['id'] );
                if ( ! file_exists( $file ) ) {
                    // Truonghn - updates #9711 - Custom default template when restore default in theme options [sonoma site]
                    $defaultTemplateContent = '';
                    if( has_filter('_apl_custom_theme_options_file_path_handler')  ) {
                        $defaultTemplateContent = apply_filters('_apl_custom_theme_options_file_path_handler', $o['id']);
                    }
                    $content = !empty($defaultTemplateContent) ?  render_php_to_string($defaultTemplateContent) : $o['std'];
                    $this->update_html_file( $content, $o['id'] );
                }

                continue;
            }

            // Not has new options
            if ( ! isset( $o['id'] ) || ($db_opts && in_array( $o['id'] , $db_opts)) ) continue;
             
            $this->should_update_newoptions = true;
            $old_opts[$o['id']] = $o['std']; // Get default options
        }
        $this->newoptions = $old_opts;
        
        // Re setting options
        $this->re_settings();
    }

    public function getOptionsScreen(){
        return $this->options_screen;
    }
    
}
