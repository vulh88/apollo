<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Apollo_Options_Import' ) ) :

    /**
     * Helping import options data from backed-up file
     *
     * @author thienld
     */
    class Apollo_Options_Import extends Apollo_Options_Backup_Tool {


        protected $importedFile = null;

        static $zipContainerFolder = "";

        protected $themeOptionData = null;

        public function __construct() {

        }



        public function processImportingOptionData($importedFile = null, $themeOptionKeys = array()){
//            update_site_option(Apollo_DB_Schema::_APOLLO_PROCESSING_IMPORT_SETTINGS,'1');
            $resCreatedDir = $this->createZipExtractFolder();
            if(!$resCreatedDir){
                $this->setErrorMessage(__("To use this function, please make sure to have folder \"" . self::_APOLLO_BACKUP_THEME_OPTIONS_FOLDER_NAME . "\" with writable permission (this folder is placed in the same level with wp-content folder)", "apollo"));
                return false;
            }
            $this->themeOptionKeys = $themeOptionKeys;
            $this->importedFile = $importedFile;
            if(empty($this->importedFile) || $this->importedFile['error'] != 0 || $this->importedFile['size'] <= 0) {
                $this->setErrorMessage(__("Import file is empty!",'apollo'));
                return false;
            }
            // validation available file for importing
            $resultValidation = $this->validateImportedFile();
            if(!$resultValidation){
                return false;
            }
            // move zip file to folder extract and extract them inner their container
            $resultMovedZipFile = $this->moveAndExtractZipFile();
            if(!$resultMovedZipFile){
                return false;
            }
            // handling implement logic of importing in order to restore theme options
            $resultProcessedRestoring = $this->loadDataXMLFileAndRestoring();
//            update_site_option(Apollo_DB_Schema::_APOLLO_PROCESSING_IMPORT_SETTINGS,'0');
            if(!$resultProcessedRestoring){
                return false;
            }
            return true;
        }

        protected function validateImportedFile(){
            if(empty($this->importedFile['name'])){
                $this->setErrorMessage(__("Filename is invalid!",'apollo'));
                return false;
            }
            $extractFilename = $tmpExtractFileName = explode('.',$this->importedFile['name']);
            $ext = array_pop($extractFilename);
            if(count($tmpExtractFileName) < 2 || $ext !== 'zip'){
                $this->setErrorMessage(__("File Type is not allowed!",'apollo'));
                return false;
            }
            return true;
        }

        protected function moveAndExtractZipFile(){
            try{
                // empty Directory contain extracting content of zip file
                self::removeDirectory(self::$pathToExtractZipFolder,false,true);
                // move file to extract folder which has been created
                $zipFile = self::$pathToExtractZipFolder . '/' . $this->importedFile['name'] ;
                @move_uploaded_file($this->importedFile['tmp_name'],$zipFile);
                if(!file_exists($zipFile) || !is_file($zipFile)){
                    $this->setErrorMessage(__("Importing file is invalid!",'apollo'));
                    return false;
                }
                require_once(ABSPATH .'/wp-admin/includes/class-pclzip.php');
                $archive = new PclZip($zipFile);
                // create directory to contain whole content of zip file
                self::$zipContainerFolder = self::$pathToExtractZipFolder . '/' . str_replace('.','_',$this->importedFile['name']);
                if (!@file_exists(self::$zipContainerFolder)) {
                    @mkdir(self::$zipContainerFolder, 0777);
                }
                $list = $archive->extract(PCLZIP_OPT_PATH, self::$zipContainerFolder,
                                          PCLZIP_OPT_REMOVE_ALL_PATH);
                if($list <= 0){
                    $this->setErrorMessage(__("Importing file is not a zip archive!"),"apollo");
                    $this->setErrorMessage($archive->errorInfo(true),"apollo");
                    return false;
                } else {
                    @unlink($zipFile);
                }
            }catch (Exception $ex){
                $this->setErrorMessage($ex->getMessage());
                return false;
            }
            return true;
        }

        protected function loadDataXMLFileAndRestoring(){
            try{
                if(!file_exists(self::$zipContainerFolder)){
                    $this->setErrorMessage(__("Process of restore has some error!",'apollo'));
                    return false;
                }
                // do anything to restore backup theme options data.
                libxml_use_internal_errors(true);
                $apolloThemeOptionsFile = self::$zipContainerFolder . '/' . self::_APOLLO_THEME_OPTIONS_BACKUP_FILENAME . '.xml';
                if(!file_exists($apolloThemeOptionsFile)){
                    $this->setErrorMessage(__("Theme config file is not existing!",'apollo'));
                    return false;
                }
                $xmlContent = file_get_contents($apolloThemeOptionsFile);
                $themeOptRestoredData = simplexml_load_string($xmlContent);
                if ($themeOptRestoredData === false) {
                    $this->setErrorMessage(__("Failed loading XML: ",'apollo'));
                    foreach(libxml_get_errors() as $error) {
                        $this->setErrorMessage($error->message);
                    }
                } else {
                    $this->themeOptionData = self::objectToArray($themeOptRestoredData);
                    return $this->handleRestoreThemeOptionData();
                }
            }catch (Exception $ex){
                $this->setErrorMessage($ex->getMessage());
                return false;
            }
        }

        protected function handleRestoreThemeOptionData(){
            if(empty($this->themeOptionData)){
                $this->setErrorMessage(__("XML file is empty!",'apollo'));
                return false;
            }
            // do something to restore theme options data
            $this->_restoreThemeOptionData();
            $this->_moveAndOverwriteStaticFiles();
            $this->_moveAndOverwriteOverrideCssFile();
            $this->_moveAndOverwriteUpdatePrimaryColorFile();
            $errorMessages = $this->getErrorMessage();
            return empty($errorMessages);
        }

        private function _restoreThemeOptionData(){
            try{
                $formattedThemeOptData = $this->themeOptionData;
                unset($formattedThemeOptData[self::_APOLLO_KEY_STATIC_HTML_FILE]);
                unset($formattedThemeOptData[self::_APOLLO_KEY_OVERRIDE_CSS_FILE]);
                unset($formattedThemeOptData[self::_APOLLO_KEY_UPDATE_PRIMARY_COLOR]);
                unset($formattedThemeOptData[self::_APOLLO_KEY_CURRENT_DOMAIN_NAME]);
                self::formatTerritoryCitiesAndZipsKeysToTruthKey($formattedThemeOptData);
                update_option( of_get_option_id() , $formattedThemeOptData);
            } catch (Exception $ex){
                $this->setErrorMessage($ex->getMessage());
                return false;
            }
        }

        private function _moveAndOverwriteStaticFiles(){
            try{
                $arrayStaticFiles = $this->themeOptionData[self::_APOLLO_KEY_STATIC_HTML_FILE];
                if(empty($arrayStaticFiles)) {
                    $this->setErrorMessage(__("Warning: No static file is restored!","apollo"));
                    return false;
                }
                $apolloUploadStaticDir = Apollo_App::getUploadBaseInfo( 'html_dir' );


                foreach($arrayStaticFiles as $sFile){
                    if(!file_exists(self::$zipContainerFolder . '/' . $sFile)){
                        continue;
                    }
                    $sourceFile = self::$zipContainerFolder . '/' . $sFile;
                    $destinationFile = $apolloUploadStaticDir . '/' . $sFile;
                    $resultReplacedDomainName = $this->_replaceAllDomainNameInContentFile($sourceFile);
                    if($resultReplacedDomainName){
                        copy($sourceFile, $destinationFile);
                    }
                }
            } catch (Exception $ex){
                $this->setErrorMessage($ex->getMessage());
                return false;
            }
            return true;
        }

        private function _moveAndOverwriteOverrideCssFile(){
            try{
                $overrideCssFile = $this->themeOptionData[self::_APOLLO_KEY_OVERRIDE_CSS_FILE];
                if(empty($overrideCssFile)) {
                    $this->setErrorMessage(__("Warning: No override css file is restored!","apollo"));
                    return false;
                }
                if(!file_exists(self::$zipContainerFolder . '/' . $overrideCssFile)){
                    $this->setErrorMessage(__("Warning: Override CSS file is not existing in zip archive!","apollo"));
                    return false;
                }
                $apolloOverrideCssDir = Apollo_App::getUploadBaseInfo('css_dir');
                $sourceFile = self::$zipContainerFolder . '/' . $overrideCssFile;
                $destinationFile = $apolloOverrideCssDir . '/' . $overrideCssFile;
                $resultReplacedDomainName = $this->_replaceAllDomainNameInContentFile($sourceFile);
                if($resultReplacedDomainName){
                    @copy($sourceFile, $destinationFile);
                }
                unset($this->themeOptionData[self::_APOLLO_KEY_OVERRIDE_CSS_FILE]);
            } catch (Exception $ex){
                $this->setErrorMessage($ex->getMessage());
                return false;
            }
            return true;
        }

        private function _moveAndOverwriteUpdatePrimaryColorFile(){
            try{
                $updatePrimaryColorFile = $this->themeOptionData[self::_APOLLO_KEY_UPDATE_PRIMARY_COLOR];
                if(empty($updatePrimaryColorFile)) {
                    $this->setErrorMessage(__("Warning: No updating primary color file is restored!","apollo"));
                    return false;
                }
                if(!file_exists(self::$zipContainerFolder . '/' . $updatePrimaryColorFile)){
                    $this->setErrorMessage(__("Warning: Updating primary color file is not existing in zip archive!","apollo"));
                    return false;
                }
                $apolloOverrideCssDir = Apollo_App::getUploadBaseInfo('css_dir');
                $sourceFile = self::$zipContainerFolder . '/' . $updatePrimaryColorFile;
                $destinationFile = $apolloOverrideCssDir . '/' . $updatePrimaryColorFile;
                $resultReplacedDomainName = $this->_replaceAllDomainNameInContentFile($sourceFile);
                if($resultReplacedDomainName){
                    @copy($sourceFile, $destinationFile);
                }
                unset($this->themeOptionData[self::_APOLLO_KEY_UPDATE_PRIMARY_COLOR]);
            } catch (Exception $ex){
                $this->setErrorMessage($ex->getMessage());
                return false;
            }
            return true;
        }

        private function _replaceAllDomainNameInContentFile($file){
            try{
                if(!self::_APOLLO_ALLOW_TO_REPLACE_ALL_DOMAIN_NAME){
                    return true;
                }
                if(!file_exists($file)){
                    $this->setErrorMessage(__("Source file for copying is not existing!","apollo"));
                    return false;
                }
                $contentFile = @file_get_contents($file);
                $restoredDomainName = $this->themeOptionData[self::_APOLLO_KEY_CURRENT_DOMAIN_NAME];
                $contentFile = str_replace($restoredDomainName,Apollo_App::apollo_get_current_domain(),$contentFile);
                @file_put_contents($file,$contentFile);
            } catch (Exception $ex){
                $this->setErrorMessage($ex->getMessage());
                return false;
            }
            return true;
        }

        public static function objectToArray($obj)
        {
            $arrObj = is_object($obj) ? get_object_vars($obj) : $obj;
            foreach ($arrObj as $key => $val) {
                $val = (is_array($val) || is_object($val)) ? self::objectToArray($val) : $val;
                $arr[$key] = (is_array($val) && empty($val)) ? "" : $val;
            }
            return isset($arr) ? $arr : array();
        }

    }

endif;