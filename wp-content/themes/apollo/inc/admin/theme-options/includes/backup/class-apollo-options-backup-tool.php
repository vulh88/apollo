<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

if (!class_exists('Apollo_Options_Backup_Tool')) :

    /**
     * Class name : Apollo_Options_Backup_Tool
     * Helping backup and import options data
     * @author thienld
     */
    abstract class Apollo_Options_Backup_Tool
    {

        private $errorMessage = array();
        protected $themeOptionPostedData;
        protected $themeOptionKeys;
        const _APOLLO_ZIP_FOLDER_NAME = 'theme-opt';
        const _APOLLO_BACKUP_THEME_OPTIONS_FOLDER_NAME = 'apollo-backup-theme-options';
        const _APOLLO_THEME_OPTIONS_BACKUP_FILENAME = 'apollo-theme-option';
        const _APOLLO_THEME_OPTIONS_COMPRESS_FOLDER = 'apollo-zip-compress';
        const _APOLLO_THEME_OPTIONS_EXTRACT_FOLDER = 'apollo-zip-extract';

        // apollo array key of xml file
        const _APOLLO_KEY_STATIC_HTML_FILE = 'static_files';
        const _APOLLO_KEY_OVERRIDE_CSS_FILE = 'override_css_file';
        const _APOLLO_KEY_UPDATE_PRIMARY_COLOR = 'updated_primary_color_css';
        const _APOLLO_KEY_CURRENT_DOMAIN_NAME = 'current_size_url';

        // settings for import/export tool
        const _APOLLO_ALLOW_TO_REPLACE_ALL_DOMAIN_NAME = false;

        static $pathToZipFolder = '';
        static $pathToCompressZipFolder = '';
        static $urlToCompressZipFolder = '';
        static $pathToExtractZipFolder = '';

        public function __construct()
        {
            // do something
        }

        /**
         * @param string $errorMessage
         */
        public function setErrorMessage($errorMessage)
        {
            $this->errorMessage[] = $errorMessage;
        }

        /**
         * @return string
         */
        public function getErrorMessage($outputString = false)
        {
            return $outputString ? implode('<br/>',$this->errorMessage) : $this->errorMessage;
        }


        protected function createZipFolder()
        {
            try {
                $upload_dir = wp_upload_dir();
                $date = date('Y-m-d-m-s');
                $backupThemeOptionsFolder = $upload_dir['basedir'] . '/' . Apollo_Options_Backup_Tool::_APOLLO_BACKUP_THEME_OPTIONS_FOLDER_NAME;
                if (!@file_exists($backupThemeOptionsFolder)) {
                    @mkdir($backupThemeOptionsFolder, 0777);
                }
                // theme-opt-domain-yyyy-mm-dd m:s
                $domainName = Apollo_App::apollo_get_current_domain();
                self::$pathToZipFolder = $backupThemeOptionsFolder . '/' . self::_APOLLO_ZIP_FOLDER_NAME . '-' . strtolower($domainName) . '-'. $date ;
                if (!@file_exists(self::$pathToZipFolder)) {
                    @mkdir(self::$pathToZipFolder, 0777);
                }

                self::$pathToCompressZipFolder = $backupThemeOptionsFolder . '/' . self::_APOLLO_THEME_OPTIONS_COMPRESS_FOLDER;
                self::$urlToCompressZipFolder = $upload_dir['baseurl'] . '/' . Apollo_Options_Backup_Tool::_APOLLO_BACKUP_THEME_OPTIONS_FOLDER_NAME . '/' . self::_APOLLO_THEME_OPTIONS_COMPRESS_FOLDER;
                if (!@file_exists(self::$pathToCompressZipFolder)) {
                    @mkdir(self::$pathToCompressZipFolder, 0777);
                }
                return true;
            } catch (Exception $ex) {
                return false;
            }
        }

        protected function createZipExtractFolder(){
            try {
                $upload_dir = wp_upload_dir();
                $backupThemeOptionsFolder = $upload_dir['basedir'] . '/' . Apollo_Options_Backup_Tool::_APOLLO_BACKUP_THEME_OPTIONS_FOLDER_NAME;
                if (!@file_exists($backupThemeOptionsFolder)) {
                    @mkdir($backupThemeOptionsFolder, 0777);
                }

                self::$pathToExtractZipFolder = $backupThemeOptionsFolder . '/' . self::_APOLLO_THEME_OPTIONS_EXTRACT_FOLDER;
                if (!@file_exists(self::$pathToExtractZipFolder)) {
                    @mkdir(self::$pathToExtractZipFolder, 0777);
                }
                return true;
            } catch (Exception $ex) {
                return false;
            }
        }

        protected function generateZipFile($getLinkFileExport = false)
        {
            // do something for downloading zip file
            $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(self::$pathToZipFolder));
            foreach ($iterator as $item) {
                @chmod($item, 0777);
            }
            $compressFilename = self::$pathToCompressZipFolder . '/' . basename(self::$pathToZipFolder) . '.zip';
            self::removeDirectory(self::$pathToCompressZipFolder,false);
            $resultGenerateZipFile = self::zip(self::$pathToZipFolder,$compressFilename);
            self::removeDirectory(self::$pathToZipFolder,true,true);
            if($resultGenerateZipFile !== true){
                $this->setErrorMessage($resultGenerateZipFile);
                return false;
            }
            if($getLinkFileExport)
                return self::$urlToCompressZipFolder . '/' . basename($compressFilename);
            else wp_safe_redirect(self::$urlToCompressZipFolder . '/' . basename($compressFilename));
            exit();
        }

        public static function zip($source, $destination)
        {
            if (!file_exists($source)) {
                return false;
            }
            require_once(ABSPATH .'/wp-admin/includes/class-pclzip.php');
            $archive = new PclZip($destination);
            $resultList = $archive->add($source, PCLZIP_OPT_REMOVE_PATH, $source);
            return $resultList === 0 ? $archive->errorInfo(true) : true;
        }

        public static function removeDirectory($path, $isRemovedDir = true, $isFormatted = false) {
            try{
                $baseDirName = basename($path);
                $baseDirNameEncode = base64_encode($baseDirName);
                $oldPath = $path;
                $newPath = str_replace($baseDirName,$baseDirNameEncode,$path);
                if($isFormatted){
                    rename($oldPath,$newPath);
                    $path = $newPath;
                }
                $files = glob($path . '/*');
                foreach ($files as $file) {
                    is_dir($file) ? self::removeDirectory($file,true,$isFormatted) : unlink($file);
                }
                if($isRemovedDir){
                    rmdir($path);
                } elseif($isFormatted) {
                    rename($newPath,$oldPath);
                }
                return;
            }catch (Exception $ex){
                aplDebugFile($ex->getMessage(),"Remove Directory");
            }
        }

        protected static function formatTerritoryCitiesAndZipsKeysToString(&$themeOptsData){
            if(empty($themeOptsData)) return array();
            if(!isset($themeOptsData[Apollo_DB_Schema::_TERR_DATA])) return array();
            foreach($themeOptsData[Apollo_DB_Schema::_TERR_DATA] as &$states){
                if(!empty($states)){
                    foreach($states as $key => &$cities){
                        $countSpacing = 0;
                        $newCityKey = str_replace(' ','___spacing___',$key, $countSpacing);
                        if(!empty($cities)){
                            $newZipFormat = array();
                            foreach($cities as $zip => $zipStatus){
                                $newZipFormat['apollo' . $zip . 'apollo'] = $zipStatus ;
                            }
                            $cities = $newZipFormat;
                        }
                        if($countSpacing > 0){
                            $states[$newCityKey] = $cities;
                            unset($states[$key]);
                        }
                    }
                }
            }
        }

        protected static function formatTerritoryCitiesAndZipsKeysToTruthKey(&$themeOptsData){
            if(empty($themeOptsData)) return array();
            if(!isset($themeOptsData[Apollo_DB_Schema::_TERR_DATA])) return array();
            foreach($themeOptsData[Apollo_DB_Schema::_TERR_DATA] as &$states){
                if(!empty($states)){
                    foreach($states as $key => &$cities){
                        $countSpacing = 0;
                        $newCityKey = str_replace('___spacing___',' ',$key, $countSpacing);
                        if(!empty($cities)){
                            $newZipFormat = array();
                            foreach($cities as $zip => $zipStatus){
                                $newZipFormat[intval(str_replace('apollo','',$zip))] = $zipStatus ;
                            }
                            $cities = $newZipFormat;
                        }
                        if($countSpacing > 0){
                            $states[$newCityKey] = $cities;
                            unset($states[$key]);
                        }
                    }
                }
            }
        }

    }

endif;