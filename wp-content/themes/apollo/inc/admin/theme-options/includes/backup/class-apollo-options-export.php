<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Apollo_Options_Export' ) ) :

    /**
     * Helping backup current options data
     *
     * @author thienld
     */
    class Apollo_Options_Export extends Apollo_Options_Backup_Tool {



        public function __construct() {

        }

        private function _formatBackupData($data = array()){
            $formattedData = get_option(of_get_option_id(),array());
            self::formatTerritoryCitiesAndZipsKeysToString($formattedData);
            $formattedData[self::_APOLLO_KEY_STATIC_HTML_FILE] = array();
            foreach ( $this->themeOptionKeys as $option ) {

                if ( ! isset( $option['id'] ) ) {
                    continue;
                }

                if ( ! isset( $option['type'] ) ) {
                    continue;
                }

                $id = preg_replace( '/[^a-zA-Z0-9._\-]/', '', strtolower( $option['id'] ) );

                if ( isset( $option['is_static_file'] ) && $option['is_static_file'] ) {
                    if ( !isset( $option['filetype'] ) || $option['filetype'] == 'html' || $option['filetype'] == 'php' || $option['filetype'] == 'js' ) {
                        $fileType = isset($option['filetype']) ? $option['filetype'] : 'html';
                        $formattedData[self::_APOLLO_KEY_STATIC_HTML_FILE][] = $id . '.' . $fileType;
                        $sourceStaticHTMLFile = get_html_file_abs_path( $id, $fileType );
                        $this->moveStaticHTMLFileToZipFolder($sourceStaticHTMLFile);
                    }
                }
            }
            // export file of override css
            $formattedData[self::_APOLLO_KEY_OVERRIDE_CSS_FILE] = basename(get_override_css_abs_path());
            $this->moveStaticHTMLFileToZipFolder(get_override_css_abs_path());
            // export file of primary color css
            $formattedData[self::_APOLLO_KEY_UPDATE_PRIMARY_COLOR] = basename(get_primary_color_css_abs_path());
            $this->moveStaticHTMLFileToZipFolder(get_primary_color_css_abs_path());
            $formattedData[self::_APOLLO_KEY_CURRENT_DOMAIN_NAME] = Apollo_App::apollo_get_current_domain();
            return $formattedData;
        }

        protected function moveStaticHTMLFileToZipFolder($sourceFile){
            try{
                $file = $sourceFile;
                if(@file_exists($file)){
                    $targetFilename = self::$pathToZipFolder . '/' . basename($file);
                    @copy($file,$targetFilename);
                }
            }catch (Exception $ex){

            }
        }

        public function processBackupOptionData($requestData, $themeOptionKeys, $getLinkFileExport = false){
            $resCreatedZipDir = $this->createZipFolder();
            if(!$resCreatedZipDir){
                $this->setErrorMessage(__("To use this function, please make sure to have folder \"" . self::_APOLLO_BACKUP_THEME_OPTIONS_FOLDER_NAME . "\" with writable permission (this folder is placed in the same level with wp-content folder)", "apollo"));
                return false;
            }
            $this->themeOptionPostedData = $requestData;
            $this->themeOptionKeys = $themeOptionKeys;
            if(!file_exists(self::$pathToZipFolder) || !is_dir(self::$pathToZipFolder)){
                $this->setErrorMessage(__("Have something wrong in process of backup theme options","apollo"));
                return false;
            }
            $backupData = $this->_formatBackupData();
            $backupFilename = self::_createBackupFilename();
            $doc = self::toXML($backupData,'ThemeOptions');
            $doc->save($backupFilename);
            $finalResult = $this->generateZipFile($getLinkFileExport);
            return $finalResult;
        }

        private static function _createBackupFilename(){
            return self::$pathToZipFolder . '/' . self::_APOLLO_THEME_OPTIONS_BACKUP_FILENAME . '.xml';
        }

        /**
         * The main function for converting to an XML document.
         * Pass in a multi dimensional array and this recrusively loops through and builds up an XML document.
         *
         * @param array $data
         * @param string $rootNodeName - what you want the root node to be - defaultsto data.
         * @param SimpleXMLElement $xml - should only be used recursively
         * @return string XML
         */
        public static function toXML( $data, $rootNodeName = 'ResultSet', &$xml=null ) {
            // turn off compatibility mode as simple xml throws a wobbly if you don't.
            if ( ini_get('zend.ze1_compatibility_mode') == 1 ) ini_set ( 'zend.ze1_compatibility_mode', 0 );
            if ( is_null( $xml ) ) //$xml = simplexml_load_string( "" );
                $xml = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><$rootNodeName />");

            // loop through the data passed in.
            foreach( $data as $key => $value ) {

                $numeric = false;

                // no numeric keys in our xml please!
                if ( is_numeric( $key ) ) {
                    $numeric = 1;
                    $key = $rootNodeName;
                }

                // delete any char not allowed in XML element names
                $key = preg_replace('/[^a-z0-9\-\_\.\:]/i', '', $key);

                // if there is another array found recrusively call this function
                if ( is_array( $value ) ) {
                    $node = self::isAssoc( $value ) || $numeric ? $xml->addChild( $key ) : $xml;

                    // recrusive call.
                    if ( $numeric ) $key = 'anon';
                    self::toXml( $value, $key, $node );
                } else {

                    // add single node.
                    $value = htmlspecialchars( $value );
                    $xml->addChild( $key, $value );
                }
            }

            // if you want the XML to be formatted, use the below instead to return the XML
            $doc = new DOMDocument('1.0');
            $doc->preserveWhiteSpace = false;
            $doc->loadXML( $xml->asXML() );
            $doc->formatOutput = true;
            return $doc;
        }


        /**
         * Convert an XML document to a multi dimensional array
         * Pass in an XML document (or SimpleXMLElement object) and this recrusively loops through and builds a representative array
         *
         * @param string $xml - XML document - can optionally be a SimpleXMLElement object
         * @return array ARRAY
         */
        public static function toArray( $xml ) {
            if ( is_string( $xml ) ) $xml = new SimpleXMLElement( $xml );
            $children = $xml->children();
            if ( !$children ) return (string) $xml;
            $arr = array();
            foreach ( $children as $key => $node ) {
                $node = self::toArray( $node );

                // support for 'anon' non-associative arrays
                if ( $key == 'anon' ) $key = count( $arr );

                // if the node is already set, put it into an array
                if ( isset( $arr[$key] ) ) {
                    if ( !is_array( $arr[$key] ) || $arr[$key][0] == null ) $arr[$key] = array( $arr[$key] );
                    $arr[$key][] = $node;
                } else {
                    $arr[$key] = $node;
                }
            }
            return $arr;
        }

        // determine if a variable is an associative array
        public static function isAssoc( $array ) {
            return (is_array($array) && 0 !== count(array_diff_key($array, array_keys(array_keys($array)))));
        }

    }

endif;