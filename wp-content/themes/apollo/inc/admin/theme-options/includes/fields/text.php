<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of text
 *
 * @author vulh
 */
class Apollo_TO_TextField extends Apollo_TO_Field {
        
    function __construct($settings, $field, $option_name, $defaultLang, $languages) {
        parent::__construct($settings, $field, $option_name, $defaultLang, $languages);
    }
    
    function render() {
        $field = $this->field;
        global $apollo_theme_wplm;
        if (!$this->languages || !in_array($field['id'], $apollo_theme_wplm)) $this->languages = array('');
        
        $output = '';
        
        
        if (isset($field['before'])) $output .= $field['before'];
       
        $output .= $this->_renderField();
        $output .= $this->_renderTransGroup();
        if (isset($field['after'])) $output .= $field['after'];
        return $output;
    }
    
    private function _renderField($lang = 'en') {
        
        $field = $this->field;
        $val = $this->getValue($lang);
        $id = $this->getId($lang);

        if(isset($field['icon-src'])){
            $iconSrc = !empty($val) ? $val : $field['icon-src'];
            $hasIcon = true;
        } else {
            $iconSrc = '';
            $hasIcon = false;
        }
        if(isset($field['icon-title'])){
            $iconTitle = $field['icon-title'];
        } else {
            $iconTitle = '';
        }

        $number_class = isset ( $field['class'] ) ? $field['class'] : '';

        $transLangLink = $this->getLangLink($lang);

        $type = !empty($field['type']) ? $field['type'] : 'text';
        $attributes = $this->renderAttr();

        if($hasIcon){
            $html = '<div class="opt-fw-sf-gr">';
                $html .= '<div class="sf-text-icon">';
                    $html .= '<input id="' . $id . '" class="of-input '.$number_class.'" '
                        . 'name="' . $this->getName($lang) . '" '
                        . 'type="'.$type.'" value="' . esc_attr( $val ) . '" '.$attributes.' />';
                    $html .= '<img src="'. $iconSrc .'" class="icon-default" alt="'. $iconTitle .'" />';
                $html .= '</div>';
            $html .= '</div>';
        } else {
            $html = '<input id="' . $id . '" class="of-input '.$number_class.'" '
                . 'name="' . $this->getName($lang) . '" '
                . 'type="'.$type.'" value="' . esc_attr( $val ) . '" '.$attributes.' />';
        }

        return $html . $transLangLink;
    }

    private function _renderTransGroup() {
        if (empty($this->languages) || (isset($this->languages[0]) && !$this->languages[0]) ) {
            return '';
        }
        $output = '';
        foreach( $this->languages as $lang => $langVal ):
            if ($lang == 'en') continue;
            $output .= $this->_renderField($lang);
        endforeach;
        
        return '<fieldset class="apl-lang-tool"><a class="trans">'.__("Translations", "apollo").'</a><div class="container hidden">'.$output.'</div></fieldset>';
    }
}
