<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of text
 *
 * @author vulh
 */
class Apollo_TO_TextAreaField extends Apollo_TO_Field {
        
    function __construct($settings, $field, $option_name, $defaultLang, $languages) {
        parent::__construct($settings, $field, $option_name, $defaultLang, $languages);
    }
    
    function render() {
        $field = $this->field;
        global $apollo_theme_wplm;
        if (!$this->languages || !in_array($field['id'], $apollo_theme_wplm)) $this->languages = array('');
        
        $output = '';
        $rows = '8';

        if ( isset( $field['settings']['rows'] ) ) {
            $custom_rows = $field['settings']['rows'];
            if ( is_numeric( $custom_rows ) ) {
                $rows = $custom_rows;
            }
        }

        if ( isset( $field['buttons'] ) ) {
            $output .= '<ul class="data-button">';
            foreach( $field['buttons'] as $btnID => $btnData ) {
                $output .= sprintf('<li>%s<input '.$btnData["attr"].' type="button" id="'.$btnID.'" value="'.$btnData['label'].'"  />%s</li>', 
                    isset($btnData['before']) ? $btnData['before'] : '', isset($btnData['after']) ? $btnData['after'] : '');
            }
            $output .= '</ul>';
        }

        $output .= $this->_renderField();
        $output .= $this->_renderTransGroup();
        
        return $output;
    }
    
    private function _renderField($lang = '') {

        $field = $this->field;

        $rows = '8';
        $class = isset ( $field['class'] ) ? $field['class'] : '';
        return $this->getLangLink($lang). '<textarea '.(isset($field["data_attr"]) ? $field["data_attr"] : "").' '
            . 'id="' . $this->getId($lang) . '" class="of-input '.$class.'" '
            . 'name="' . $this->getName($lang) . '" rows="' . $rows . '">' . esc_textarea( $this->getValue($lang) ) . '</textarea>';


    }

    private function _renderTransGroup() {
        if (empty($this->languages) || (isset($this->languages[0]) && !$this->languages[0]) ) {
            return '';
        }
        $output = '';
        
        foreach( $this->languages as $lang => $langVal ):
            if ($lang == 'en') continue;
            $output .= $this->_renderField($lang);
        endforeach;
        
        return '<fieldset class="apl-lang-tool"><a class="trans">'.__("Translations", "apollo").'</a><div class="container hidden">'.$output.'</<div></fieldset>';
    }
}
