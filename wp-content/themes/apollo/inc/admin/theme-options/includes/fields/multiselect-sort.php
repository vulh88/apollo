<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of text
 *
 * @author vulh
 */
class Apollo_TO_MultipleSortField extends Apollo_TO_Field {

    function __construct($settings, $field, $option_name, $defaultLang, $languages) {
        parent::__construct($settings, $field, $option_name, $defaultLang, $languages);
    }

    function render() {
        $field = $this->field;
        global $apollo_theme_wplm;
        if (!$this->languages || !in_array($field['id'], $apollo_theme_wplm)) $this->languages = array('');
        
        $settings = $this->settings;
        $option_name = $this->option_name;
        ob_start();
        ?>
        <div class=' section wrap-multiselect'>
            
            <!-- Eg data for adding -->
            <div class="trans-group-eg hidden"><?php echo $this->_renderTransGroup(false, 0); ?></div>
            
            <h4 class="clearfix"><?php echo $field['name'] ?></h4>
            <select class="hidden hidden-multiselect" multiple="multiple" name="<?php echo esc_attr( $option_name. '['. $field['id'] . ']' ); ?>[]">
                <?php echo $field['options'] ?>
            </select>

            <select data-color-name="<?php echo esc_attr( $option_name. '['. $field['id'] . '_of_color]' ); ?>[]"
                    data-color-picker="<?php echo isset($field['hasColorPicker']) && $field['hasColorPicker'] ? 1:0 ?>"
                    class="multiselect" multiple="multiple"
                    data-as-name="<?php echo esc_attr( $option_name. '['. $field['id'] . '_of_asname]' ); ?>[]"
                    data-checkbox="<?php echo isset($field['hasCheckBox']) && $field['hasCheckBox'] ? 1:0 ?>"
                    data-checkbox-name="<?php echo esc_attr( $option_name. '['. $field['id'] . '_use_cutom_link]' ); ?>[]"
                    data-custom-link="<?php echo isset($field['hasCustomLink']) && $field['hasCustomLink'] ? 1:0 ?>"
                    data-custom-link-name="<?php echo esc_attr( $option_name. '['. $field['id'] . '_custom_link]' ); ?>[]"
            >
                <?php echo $field['options'] ?>
            </select>
            <button <?php echo isset($field['data-attr-btn']) ? $field['data-attr-btn'] : '' ?> type="button" class="add-multi-select-btn button"><?php _e( 'Add', 'apollo' ); ?></button>
            <div class="selected-event">
                <ol>
                    <?php

                    $colorKey = $field["id"].'_of_color';
                    $asKey = $field["id"].'_of_asname';
                    /*@ticket #17733: CF] 20180928 - [Homepage][Feature events] Custom link for the 'Spotlight' category page - Item 5*/
                    $customLinkKey = $field["id"].'_custom_link';
                    $checkboxKey = $field["id"].'_use_cutom_link';

                    $colorValArr = isset($settings[$colorKey]) ? $settings[$colorKey] : '';
                    $asValArr = isset($settings[$asKey]) ? $settings[$asKey] : '';
                    $customLinkValArr = isset($settings[$customLinkKey]) ? $settings[$customLinkKey] : '';
                    $checkboxValArr = isset($settings[$checkboxKey]) ? $settings[$checkboxKey] : '';
                    
                    $colorPickerClass = isset($field['hasColorPicker']) && $field['hasColorPicker'] ? 'of-color' : '';
                    $colorName = $option_name.'['.$colorKey.'][]';
                    $asName =$option_name.'['.$asKey.'][]';

                    $customLinkName = $option_name.'['.$customLinkKey.'][]';
                    $checkboxName = $option_name.'['.$checkboxKey.'][]';
                    if ( $selected_events = $field['selected'] ) {
                        foreach ( $selected_events as $i => $sl ) {
                            if( ! is_string( $sl ) || !$sl ) continue;


                            $dataType = !empty($field['data-type']) && $field['data-type'] ? $field['data-type'] : '';

                            switch($dataType) {
                                case 'term':
                                    $term = get_term_by( 'slug' , $sl, 'event-type' );
                                    // Not found term or hidden term
                                    if ( ! $term || (isset($field['exclude']) && $field['exclude'] && in_array($term->term_id, $field['exclude']) ) ) continue;

                                    $asVal = isset($asValArr[$i]) ? $asValArr[$i] : $term->name;
                                    $label = $term->name;
                                    break;
                                default:
                                    $term = false;
                                    $asVal = isset($asValArr[$i]) ? $asValArr[$i] : '';
                                    $label = ucfirst($sl);

                            }

                            $colorVal = isset( $colorValArr[$i] ) ? $colorValArr[$i] : '#ff0000';
                            $customLinkVal = (isset( $customLinkValArr[$i] ) &&   $customLinkValArr[$i]) ? $customLinkValArr[$i] : '';
                            $checkboxVal = (isset( $checkboxValArr[$i] ) &&  $checkboxValArr[$i]) ? $checkboxValArr[$i] : 0;

                            ?>
                            <li data-value="<?php echo $sl ?>"><label><?php echo $label ?></label><i class="fa fa-remove"></i>

                            <div class="as-is">
                                <input value="<?php echo $asVal ?>" name="<?php echo $asName ?>" id="of-asname<?php echo $i ?>" type="text"/>
                                <?php

                                    switch($dataType) {
                                        case 'term':
                                            echo $this->_renderTransGroup($term, $i);
                                            break;
                                        default:
                                            echo $this->_renderNormalTrans($i);

                                    }
                                ?>
                            </div>
                            <?php if ($colorPickerClass): ?>
                            <input value="<?php echo $colorVal ?>" name="<?php echo $colorName ?>" id="of-color<?php echo $i ?>" type="text" class="<?php echo $colorPickerClass ?>" />
                            <?php endif; ?>

                            <?php if (isset($field['hasCheckBox']) && $field['hasCheckBox']): ?>
                                <div class="apl-of-checkbox-contain">
                                    <label> <?php echo __('Use custom link', 'apollo') ?></label>
                                    <input <?php echo $checkboxVal ? 'checked' : '' ?>   type="checkbox" class="apl-use-custom-link" />
                                    <input name="<?php echo $checkboxName ?>"  type="text" value="<?php echo $checkboxVal?>" class="apl-use-custom-link-val hidden"/>
                                </div>
                            <?php endif; ?>
                            <?php if (isset($field['hasCustomLink']) && $field['hasCustomLink']): ?>
                            <div>
                                <input value="<?php echo $customLinkVal?>" name="<?php echo $customLinkName ?>"  type="text" class="apollo_input_url" />
                            </div>
                            <?php endif; ?>

                            <?php echo '</li>' ?>

                    <?php
                        }
                    }
                    ?>
                </ol>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }
    
    private function _renderTransGroup($term, $i) {
        if (empty($this->languages) || (isset($this->languages[0]) && !$this->languages[0]) ) {
            return '';
        }
        $output = '';
        
        foreach( $this->languages as $lang => $langVal ):
            if ($lang == 'en') continue;
            
            $asKey = $this->getId($lang, '_of_asname');
            $asValArr = isset($this->settings[$asKey]) ? $this->settings[$asKey] : '';

            $asName = $this->option_name.'['.$asKey.'][]';
            
            if ($term) {
                $translated_term_id = wpml_object_id_filter($term->term_id, 'event-type', true, $lang);
                $translated_term_object = get_term_by('id', $translated_term_id, 'event-type');
                $asVal = isset($asValArr[$i]) ? $asValArr[$i] : $translated_term_object->name;
            } else {
                $asVal = '';
            }
            
            $langName = $this->getLangLink($lang);
            if ($term) {
                $output .= '<input data-lang="'.$lang.'" value="'.$asVal.'" name="'.$asName.'" type="text"/>'. $langName;
            } else {
                $output .= '<input data-lang="'.$lang.'" value="'.$asVal.'" data-name="'.$asName.'" type="text"/>'. $langName;
            }
            
            
        endforeach;
        
        return '<fieldset class="apl-lang-tool"><a class="trans">'.__("Translations", "apollo").'</a><div class="container hidden">'.$output.'</<div></fieldset>';
    }

    private function _renderNormalTrans($i) {
        if (empty($this->languages) || (isset($this->languages[0]) && !$this->languages[0]) ) {
            return '';
        }
        $output = '';

        foreach( $this->languages as $lang => $langVal ):
            if ($lang == 'en') continue;

            $asKey = $this->getId($lang, '_of_asname');
            $asValArr = isset($this->settings[$asKey]) ? $this->settings[$asKey] : '';

            $asName = $this->option_name.'['.$asKey.'][]';

            $langName = $this->getLangLink($lang);

            $asVal = !empty($asValArr[$i]) ? $asValArr[$i] : '';
            $output .= '<input data-lang="'.$lang.'" value="'.$asVal.'" data-name="'.$asName.'" name="'.$asName.'" type="text"/>'. $langName;



        endforeach;

        return '<fieldset class="apl-lang-tool"><a class="trans">'.__("Translations", "apollo").'</a><div class="container hidden">'.$output.'</<div></fieldset>';
    }
}
