<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Description of text
 *
 * @author vulh
 */
class Apollo_TO_Button extends Apollo_TO_Field {
        
    function __construct($settings, $field, $option_name, $defaultLang, $languages) {
        parent::__construct($settings, $field, $option_name, $defaultLang, $languages);
    }
    
    function render() {
        $output = '';
        $id =  $this->field['id'];
        $title =  $this->field['title'];
        $attrText = $this->renderAttr();
        $output .= "<button $attrText type='button' id='$id' >$title</button>";
        return $output;
    }
}
