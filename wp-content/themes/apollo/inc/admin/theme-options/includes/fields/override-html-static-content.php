<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}


class Apollo_Override_Html_Static_Content
{
    private $module = '';

    const HEADER = '_header_';
    const CONTENT_TYPE_HEADER = '_content_type_header_';
    const DISPLAY_TYPE_HEADER = '_display_type_header_';
    const REFERENCE_MODULE_HEADER = '_reference_module_header_';

    const FOOTER1 = '_footer1_';
    const CONTENT_TYPE_FOOTER1 = '_content_type_footer1_';
    const DISPLAY_TYPE_FOOTER1 = '_display_type_footer1_';
    const REFERENCE_MODULE_FOOTER1 = '_reference_module_footer1_';

    const FOOTER2 = '_footer2_';
    const CONTENT_TYPE_FOOTER2 = '_content_type_footer2_';
    const DISPLAY_TYPE_FOOTER2 = '_display_type_footer2_';
    const REFERENCE_MODULE_FOOTER2 = '_reference_module_footer2_';

    const FOOTER3 = '‌_site_info_';
    const CONTENT_TYPE_FOOTER3 = '_content_type_site_info_';
    const DISPLAY_TYPE_FOOTER3 = '_display_type_site_info_';
    const REFERENCE_MODULE_FOOTER3 = '_reference_module_site_info_';

    /**
     * Apollo_Override_Html_Static_Content constructor.
     * @param $module
     */
    public function __construct($module)
    {
        $this->module = $module;
    }

    /**
     * @return array
     */
    public function renderOptions()
    {

        $referenceModules = array(
            '' => __('Select Module', 'apollo'),
            '_education' => __('Education', 'apollo'),
        );

        if(isset($referenceModules["_" . $this->module])){
            //remove itself in reference list
            unset($referenceModules["_" . $this->module]);
        }

        $dataTypeOptions = array(
            1 => __('Default', 'apollo'),
            0 => __('Custom', 'apollo'),
            2 => __('Reference module', 'apollo')
        );

        $displayOptions = array(
            1 => __('Override the global data', 'apollo'),
            0 => __('Extend the global data', 'apollo')
        );

        $options[] = array(
            'name' => __('Override/Extend masthead, footer content', 'apollo'),
            'type' => 'header_block'
        );

        $options[] = array(
            'name' => __('-- Header', 'apollo'),
            'type' => 'header_block'
        );

        $options[] = array(
            'name' => __('Data type', 'apollo'),
            'id' => $this::CONTENT_TYPE_HEADER . $this->module,
            'std' => 1,
            'type' => 'radio',
            'options' => $dataTypeOptions,
            'class' => 'no-border inline apl-override-html-header-type'
        );

        $options[] = array(
            'name' => __('Display option', 'apollo'),
            'id' => $this::DISPLAY_TYPE_HEADER . $this->module,
            'std' => 1,
            'type' => 'radio',
            'options' => $displayOptions,
            'class' => 'no-border inline apl-override-html-header-custom hidden'
        );

        $options[] = array(
            'id' => $this::HEADER . $this->module,
            'std' => '',
            'class' => 'full apl-override-html-header-custom hidden',
            'type' => 'editor',
            'is_static_file' => true,
        );

        $options[] = array(
            'name' => __('Reference Module', 'apollo'),
            'id' => $this::REFERENCE_MODULE_HEADER . $this->module,
            'std' => '',
            'type' => 'select',
            'class' => 'no-border inline apl-override-html-header-reference hidden',
            'options' => $referenceModules
        );

        $options[] = array(
            'name' => __('-- Footer 1 - Partner logos', 'apollo'),
            'type' => 'header_block'
        );

        $options[] = array(
            'name' => __('Data type', 'apollo'),
            'id' => $this::CONTENT_TYPE_FOOTER1 . $this->module,
            'std' => 1,
            'type' => 'radio',
            'options' => $dataTypeOptions,
            'class' => 'no-border inline apl-override-html-footer1-type'
        );

        $options[] = array(
            'name' => __('Display option', 'apollo'),
            'id' => $this::DISPLAY_TYPE_FOOTER1 . $this->module,
            'std' => 1,
            'type' => 'radio',
            'options' => $displayOptions,
            'class' => 'no-border inline apl-override-html-footer1-custom hidden'
        );

        $options[] = array(
            'id' => $this::FOOTER1 . $this->module,
            'std' => '',
            'class' => 'full apl-override-html-footer1-custom hidden',
            'type' => 'editor',
            'is_static_file' => true,
        );

        $options[] = array(
            'name' => __('Reference Module', 'apollo'),
            'id' => $this::REFERENCE_MODULE_FOOTER1 . $this->module,
            'std' => '',
            'type' => 'select',
            'class' => 'no-border inline apl-override-html-footer1-reference hidden',
            'options' => $referenceModules
        );


        $options[] = array(
            'name' => __('-- Footer 2 - Bottom Navigation (columns links)', 'apollo'),
            'type' => 'header_block'
        );

        $options[] = array(
            'name' => __('Data type', 'apollo'),
            'id' => $this::CONTENT_TYPE_FOOTER2 . $this->module,
            'std' => 1,
            'type' => 'radio',
            'options' => $dataTypeOptions,
            'class' => 'no-border inline apl-override-html-footer2-type'
        );

        $options[] = array(
            'name' => __('Display option', 'apollo'),
            'id' => $this::DISPLAY_TYPE_FOOTER2 . $this->module,
            'std' => 1,
            'type' => 'radio',
            'options' => $displayOptions,
            'class' => 'no-border inline apl-override-html-footer2-custom hidden'
        );

        $options[] = array(
            'id' => $this::FOOTER2 . $this->module,
            'std' => render_php_to_string(APOLLO_THEME_FOOTER2),
            'class' => 'full apl-override-html-footer2-custom hidden',
            'type' => 'editor',
            'is_static_file' => true,
        );

        $options[] = array(
            'name' => __('Reference Module', 'apollo'),
            'id' => $this::REFERENCE_MODULE_FOOTER2 . $this->module,
            'std' => '',
            'type' => 'select',
            'class' => 'no-border inline apl-override-html-footer2-reference hidden',
            'options' => $referenceModules
        );

        $options[] = array(
            'name' => __('-- Footer 3 - Site Info (standard Artsopolis theme default logo, tweet, contact)', 'apollo'),
            'type' => 'header_block'
        );

        $options[] = array(
            'name' => __('Data type', 'apollo'),
            'id' => $this::CONTENT_TYPE_FOOTER3 . $this->module,
            'std' => 1,
            'type' => 'radio',
            'options' => $dataTypeOptions,
            'class' => 'no-border inline apl-override-html-site_info-type'
        );

        $options[] = array(
            'name' => __('Display option', 'apollo'),
            'id' => $this::DISPLAY_TYPE_FOOTER3 . $this->module,
            'std' => 1,
            'type' => 'radio',
            'options' => $displayOptions,
            'class' => 'no-border inline apl-override-html-site_info-custom hidden'
        );

        $options[] = array(
            'desc' => __('', 'apollo'),
            'id' => $this::FOOTER3 . $this->module,
            'std' => '',
            'class' => 'full apl-override-html-site_info-custom hidden',
            'type' => 'editor',
            'is_static_file' => true,
        );

        $options[] = array(
            'name' => __('Reference Module', 'apollo'),
            'id' => $this::REFERENCE_MODULE_FOOTER3 . $this->module,
            'std' => '',
            'type' => 'select',
            'class' => 'no-border inline apl-override-html-site_info-reference hidden',
            'options' => $referenceModules
        );

        return $options;
    }
}
