<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Apollo_TO_Lang {
    protected $lang;
    
    public function __construct($lang = '') {
        $this->lang = $lang;
    }
    
    function getLangCode() {
        return !$this->lang || $this->lang == 'en' ? '' : $this->lang;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of text
 *
 * @author vulh
 */
class Apollo_TO_Field {
    protected $settings = '';
    protected $option_name = '';
    protected $defaultLang = '';
    protected $languages = '';
    protected $field = '';
    
    function __construct($settings, $field, $option_name, $defaultLang, $languages) {
        $this->settings    = $settings;
        $this->field       = $field;
        $this->option_name = $option_name;
        $this->defaultLang = $defaultLang;
        $this->languages   = $languages;
        $this->option_name = of_get_option_id();
    }
    
    function getId($lang = '', $prefix = '') {
        $lang = $this->getLangCode($lang);
        return esc_attr($this->field['id'].$prefix . $lang);
    }
    
    function getLangCode($lang = '') {
        return !$lang || $lang == 'en' ?  '' : '_'. $lang;
    }
    
    function getName($lang = '') {
        if (isset( $this->field['not_save'] ) && $this->field['not_save']) return '';
        return esc_attr( $this->option_name . '[' . $this->getId($lang). ']' );
    }
    
    function getValue($lang = '') {
        
        if ( !empty($this->field['is_static_file']) ) {
            $extArr = array('html', 'php', 'js');
            
            if (isset($this->field['filetype'])) {
                $this->field['filetype'] = $this->field['filetype'] == null ? 'html' : $this->field['filetype'];
            } else {
                $this->field['filetype'] = 'html';
            }

            if(in_array($this->field['filetype'], $extArr)){
                $val = @file_get_contents( get_html_file_abs_path( $this->field['id'], $this->field['filetype'] ) );
            } else {
                $val = @file_get_contents(get_override_css_abs_path() );
            }

            // Set default value to $val
            if ( !$val && isset( $this->field['std'] ) ) return  $this->field['std'];


            return $val;
        }

        if ( isset( $this->field['val'] ) ) {
            return $this->field['val'];
        }
        
        $val = isset($this->settings[$this->getId($lang)]) ? $this->settings[$this->getId($lang)] : '';
        
        // Striping slashes of non-array options
        if ( !is_array($val) ) {
            $val = stripslashes( $val );
        }
        
        // Set default value to $val
        if ( !$val && isset( $this->field['std'] ) ) {
            $val =  $this->field['std'];
        }
        return $val;
    }
    
    function getLangLink($lang = '') {
        return $lang != 'en' && !empty($this->languages[$lang]) ? '<span class="item-lang">'.$this->languages[$lang]['native_name']. '</span>' : '';
    }

    /**
     * render attr
     * @return string
     */
    protected function renderAttr() {
        $value = $this->field;

        $attr = isset($value['attr'])?$value['attr']:null;

        if (is_string($attr)) {
            return $attr;
        }

        $attrText = '';
        if(is_array($attr) && count($attr)){
            foreach ($attr as $key => $val){
                $attrText .= " $key='$val' ";
            }
        }
        return $attrText;
    }
    
}

