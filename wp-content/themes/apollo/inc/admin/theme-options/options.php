<?php

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 */

function optionsframework_options() {

    // fire a hook filter for any adding options at top
    $options = apply_filters('of_options_first', null);

    if(empty($options)){
        $options = array();
    }

    /*************************************************************************/
    $options[] = array(
        'name' => __( 'Design', 'apollo' ),
        'type' => 'heading'
    );

    $isAvailEvent = Apollo_App::is_avaiable_module(Apollo_DB_Schema::_EVENT_PT);

    // Site primary color option
    $options[] = array(
        'name'        => __( 'Primary Color', 'apollo' ),
        'type'        => 'header_block'
    );
    $options[] = array(
        'desc'        => __( '', 'apollo' ),
        'id'         => Apollo_DB_Schema::_PRIMARY_COLOR,
        'std'     => APOLLO_THEME_PRIMARY_COLOR,
        'type'        => 'color'
    );

    // Site primary color option
    $options[] = array(
        'name'        => __( 'Hovering Color on Listing', 'apollo' ),
        'type'        => 'header_block'
    );
    $options[] = array(
        'desc'        => __( '', 'apollo' ),
        'id'      => Apollo_DB_Schema::_HOVERING_SEARCH_CONTENT_COLOR,
        'std'     => APOLLO_THEME_HOVERING_SEARCH_CONTENT_COLOR,
        'type'        => 'color'
    );

    /** @Ticket #18668 - Masthead Setting options */
    $octaveMastheadSettings = array();
    $octaveMastheadSettings = apply_filters('apl_custom_masthead_options', $octaveMastheadSettings);
    if (!empty($octaveMastheadSettings)) {
        $options = array_merge($options, $octaveMastheadSettings);
    }

    // Site primary color option
    $options[] = array(
        'name'        => __( 'Menu Setting', 'apollo' ),
        'type'        => 'header_block'
    );

    if($twoTier = of_get_option(Apollo_DB_Schema::_NAVIGATION_BAR_STYLE) == 'two-tiers'){

        $options[] = array(
            'name'      => 'Menu Tier 1 :',
            'desc'        => __( 'Hover Background Color', 'apollo' ),
            'id'      => Apollo_DB_Schema::_NAV_TWO_TIER_MAIN_HOVER_BG_COLOR,
            'std'     => '',
            'type'        => 'color'
        );

        $textMenuOptions1 = __('Menu Tier 2 :', 'apollo');
        $textMenuOptions2 = __('Sub Menu Level 1:', 'apollo');
    } else {
        $textMenuOptions1 = __('Sub Menu Level 1:', 'apollo');
        $textMenuOptions2 = __('Sub Menu Level 2:', 'apollo');
    }
    $options[] = array(
        'name'      => $textMenuOptions1,
        'desc'        => __( 'Font Color', 'apollo' ),
        'id'      => Apollo_DB_Schema::_NAV_SUB_L1_COLOR,
        'std'     => '#f9f9f9',
        'type'        => 'color'
    );

    $options[] = array(
        'desc'        => __( 'Background Color', 'apollo' ),
        'id'      => Apollo_DB_Schema::_NAV_SUB_L1_BG_COLOR,
        'std'     => '#222222',
        'type'        => 'color'
    );

    $options[] = array(
        'desc'        => __( 'Hover Color', 'apollo' ),
        'id'      => Apollo_DB_Schema::_NAV_SUB_L1_HOVER_COLOR,
        'std'     => '#222222',
        'type'        => 'color'
    );

    $options[] = array(
        'desc'        => $twoTier ? __( 'Hover Background Color', 'apollo' ) : __( 'Hover Background Color', 'apollo' ),
        'id'      => Apollo_DB_Schema::_NAV_SUB_L1_HOVER_BG_COLOR,
        'std'     => '#f9f9f9',
        'type'        => 'color'
    );

    $options[] = array(
        'name'      => $textMenuOptions2,
        'desc'        => __( 'Font Color', 'apollo' ),
        'id'      => Apollo_DB_Schema::_NAV_SUB_L2_COLOR,
        'std'     => '#f9f9f9',
        'type'        => 'color'
    );

    $options[] = array(
        'desc'        => __( 'Background Color', 'apollo' ),
        'id'      => Apollo_DB_Schema::_NAV_SUB_L2_BG_COLOR,
        'std'     => '#222222',
        'type'        => 'color'
    );

    $options[] = array(
        'desc'        => __( 'Hover Color', 'apollo' ),
        'id'      => Apollo_DB_Schema::_NAV_SUB_L2_HOVER_COLOR,
        'std'     => '#222222',
        'type'        => 'color'
    );

    $options[] = array(
        'desc'        => __( 'Hover Background Color', 'apollo' ),
        'id'      => Apollo_DB_Schema::_NAV_SUB_L2_HOVER_BG_COLOR,
        'std'     => '#f9f9f9',
        'type'        => 'color'
    );

    /* Ticket @13588 */
    $options[] = array(
        'name'        => __( 'Collapse all children on mobile view', 'apollo' ),
        'type'        => 'radio',
        'id'          => Apollo_DB_Schema::_COLLAPSEALLCHILDRENTMOBILE,
        'std'         => 0,
        'options'   => array(
            1  => __( 'Yes', 'apollo' ),
            0  => __( 'No', 'apollo' ),
        ),
        'class'     => 'no-border',
    );

    /** @Ticket #18668 - Navigation Setting options */
    $octaveNavigationSettings = array();
    $octaveNavigationSettings = apply_filters('apl_custom_navigation_options', $octaveNavigationSettings);
    if (!empty($octaveMastheadSettings)) {
        $options = array_merge($options, $octaveNavigationSettings);
    }

    // Header
    $options[] = array(
        'name'        => __( 'Style Settings', 'apollo' ),
        'type'        => 'header_block'
    );


    // Header logo and text display type option
    $activate_override_css_array = array(
        1  => __( 'Yes', 'apollo' ),
        0  => __( 'No', 'apollo' ),
    );

    // Override CSS activate option
    $options[] = array(
        'name'        => __( 'Activate Override CSS', 'apollo' ),
        'id'      => Apollo_DB_Schema::_ACTIVATE_OVERRIDE_CSS,
        'std'     => APOLLO_THEME_ACTIVATE_OVERRIDE_CSS,
        'type'        => 'radio',
        'options'   => $activate_override_css_array,
        'class'     => 'no-border',
    );

    $options[] = array(
        'name'        => __( 'Override CSS', 'apollo' ),
        'type'        => 'header_block'
    );
    $options[] = array(
        'desc'     => __( 'Content of css override file here', 'apollo' ),
        'id'   => Apollo_DB_Schema::_OVERRIDE_CSS,
        'std'  => render_php_to_string( APOLLO_THEME_OVERRIDE_CSS ),
        'class' => 'full',
        'type'     => 'textarea',
        'is_static_file' => true,
        'filetype'  => 'css',
    );

    // Override CSS version
    $options[] = array(
        'name'        => __( 'Override CSS version', 'apollo' ),
        'id'      => Apollo_DB_Schema::_OVERRIDE_CSS_VERSION,
        'std'     => '1.0',
        'type'        => 'text',
        'class'     => 'no-border',
    );


    /*************************************************************************/

    // Header Options Area
    $options[] = array(
        'name' => __( 'Global Content', 'apollo' ),
        'type' => 'heading'
    );

    $options[] = array(
        'name'        => __( 'Navigation', 'apollo' ),
        'type'        => 'header_block'
    );

    $navigationTypes = array(
        'def'     => __( ' Default', 'apollo' ),
        'two-rows'  => __( 'Two rows', 'apollo' ),
        'two-tiers'  => __( 'Two tiers', 'apollo' ),
            );

    // Navigation bar style option
    $options[] = array(
        'name'       => __( 'Navigation bar style ', 'apollo' ),
        'id'        => Apollo_DB_Schema::_NAVIGATION_BAR_STYLE,
        'std'       => 'def',
        'type'      => 'select',
        'options'   => $navigationTypes,
        'class'     => 'no-border inline',
    );


    $options[] = array(
        'name'        => __( 'Tracking Script (inside head tag)', 'apollo' ),
        'type'        => 'header_block'
    );
    $options[] = array(
        'id'        => Apollo_DB_Schema::_HEADER_TRACKING_SCRIPT,
        'std'       => render_php_to_string( Apollo_DB_Schema::_HEADER_TRACKING_SCRIPT ),
        'type'      => 'textarea',
        'class'     => 'full',
        'wrapper_class'     => 'full',
        'filetype'          => 'js',
        'is_static_file'    => true,
    );

    /** @Ticket #15370 */
    $options[] = array(
        'name'        => __( 'Script In Body Tag', 'apollo' ),
        'type'        => 'header_block'
    );
    $options[] = array(
        'id'        => Apollo_DB_Schema::_SCRIPT_IN_BODY_TAG,
        'std'       => render_php_to_string( Apollo_DB_Schema::_SCRIPT_IN_BODY_TAG ),
        'type'      => 'textarea',
        'class'     => 'full',
        'wrapper_class'     => 'full',
        'filetype'          => 'js',
        'is_static_file'    => true,
    );

    $options[] = array(
        'name'        => __( 'Top Header', 'apollo' ),
        'type'        => 'header_block'
    );
    $options[] = array(
        'name'      => __( 'Enable top header ', 'apollo' ),
        'id'        => Apollo_DB_Schema::_ENABLE_TOP_HEADER,
        'std'       => 0,
        'type'      => 'radio',
        'options'   => array(
            1           => __( 'Yes', 'apollo' ),
            0           => __( 'No', 'apollo' ),
        ),
        'class'     => 'no-border',
    );

    $options[] = array(
        'name'      => __( 'Scroll with page ', 'apollo' ),
        'id'        => Apollo_DB_Schema::_SCROLL_WITH_PAGE,
        'std'       => 1,
        'desc'  => __("Apply for the top bar, masthead"),
        'type'      => 'radio',
        'options'   => array(
            1           => __( 'Yes', 'apollo' ),
            0           => __( 'No', 'apollo' ),
        ),
        'class'     => 'no-border',
    );

    /** @Ticket #16467 - Allow adding options for top header*/
    $octaveTopHeader = array();
    $octaveTopHeader = apply_filters('apl_custom_top_header_options', $octaveTopHeader);
    if (!empty($octaveTopHeader)) {
        $options = array_merge($options, $octaveTopHeader);
    }

    $options[] = array(
        'desc'     => __( '', 'apollo' ),
        'id'   => Apollo_DB_Schema::_TOP_HEADER,
        'std'  => render_php_to_string( APOLLO_THEME_TOP_HEADER ),
        'class' => 'full',
        'type'     => 'editor',
        'is_static_file' => true,
    );

    $options[] = array(
        'name'        => __( 'Header', 'apollo' ),
        'type'        => 'header_block'
    );

    /** @Ticket #16467 - Allow adding options for header */
    $octaveHeader = array();
    $octaveHeader = apply_filters('apl_custom_header_options', $octaveHeader);
    if (!empty($octaveHeader)) {
        $options = array_merge($options, $octaveHeader);
    }

    $options[] = array(
        'desc'     => __( '', 'apollo' ),
        'id'   => Apollo_DB_Schema::_HEADER,
        'std'  => render_php_to_string( APOLLO_THEME_HEADER ),
        'class' => 'full',
        'type'     => 'editor',
        'is_static_file' => true,
    );

    $options[] = array(
        'name'        => __( 'Footer 1 - Partner logos', 'apollo' ),
        'type'        => 'header_block'
    );
    $options[] = array(
        'desc'     => __( '', 'apollo' ),
        'id'   => Apollo_DB_Schema::_FOOTER1,
        'std'  => render_php_to_string( APOLLO_THEME_FOOTER1 ),
        'class' => 'full',
        'type'     => 'editor',
        'is_static_file' => true,
    );


    $options[] = array(
        'name'        => __( 'Footer 2 - Bottom Navigation (columns links)', 'apollo' ),
        'type'        => 'header_block'
    );
    $options[] = array(
        'desc'     => __( '', 'apollo' ),
        'id'   => Apollo_DB_Schema::_FOOTER2,
        'std'  => render_php_to_string( APOLLO_THEME_FOOTER2 ),
        'class' => 'full',
        'type'     => 'editor',
        'is_static_file' => true,
    );

    $options[] = array(
        'name'        => __( 'Footer 3 - Site Info (standard Artsopolis theme default logo, tweet, contact)', 'apollo' ),
        'type'        => 'header_block'
    );
    $options[] = array(
        'desc'     => __( '', 'apollo' ),
        'id'   => Apollo_DB_Schema::_FOOTER3,
        'std'  => render_php_to_string( APOLLO_THEME_FOOTER3 ),
        'class' => 'full',
        'type'     => 'editor',
        'is_static_file' => true,
    );

    $options[] = array(
        'name'        => __( 'Footer 4 - Site Credits (copyright and privacy policy info)', 'apollo' ),
        'type'        => 'header_block'
    );
    $options[] = array(
        'desc'     => __( '', 'apollo' ),
        'id'   => Apollo_DB_Schema::_FOOTER4,
        'std'  => render_php_to_string( APOLLO_THEME_FOOTER4 ),
        'class' => 'full',
        'type'     => 'editor',
        'is_static_file' => true,
    );


    $options[] = array(
        'name'        => __( 'Footer 5 - Artsopolis network member city list', 'apollo' ),
        'type'        => 'header_block'
    );
    $options[] = array(
        'desc'     => __( '', 'apollo' ),
        'id'   => Apollo_DB_Schema::_FOOTER5,
        'std'  => render_php_to_string( APOLLO_THEME_FOOTER5 ),
        'class' => 'full',
        'type'     => 'editor',
        'is_static_file' => true,
    );

    $options[] = array(
        'name'        => __( 'Content 404 Page', 'apollo' ),
        'type'        => 'header_block'
    );

    $options[] = array(
        'desc'     => __( '', 'apollo' ),
        'id'   => Apollo_DB_Schema::_PAGE_404,
        'std'  => render_php_to_string( APOLLO_THEME_404 ),
        'class' => 'full',
        'type'     => 'editor',
        'is_static_file' => true,
    );


    $options[] = array(
        'name'        => __( 'Additional Javascript', 'apollo' ),
        'type'        => 'header_block'
    );
    $options[] = array(
        'desc'     => __( '', 'apollo' ),
        'id'   => Apollo_DB_Schema::_ADDITIONAL_JS,
        'std'  => render_php_to_string( Apollo_DB_Schema::_ADDITIONAL_JS ),
        'class' => 'full',
        'filetype' => 'js',
        'wrapper_class' => 'full',
        'type'     => 'textarea',
        'is_static_file' => true,
    );

    /*************************************************************************/
    // Header Options Area
    $options[] = array(
        'name' => __( 'Home Content', 'apollo' ),
        'type' => 'heading'
    );

    $options[] = array(
        'name'        => __( 'Default Layout Homepage', 'apollo' ),
        'type'        => 'header_block'
    );
    $options[] = array(
        'desc'        => __( '', 'apollo' ),
        'id'        => Apollo_DB_Schema::_DEFAULT_HOME_LAYOUT,
        'std'     => APOLLO_DEFAULT_LAYOUT,
        'type'        => 'images',
        'options'  => array(
            'right_sidebar_one_column'     => APOLLO_ADMIN_IMAGES_URL . '/right-sidebar-one-column.png',
            'right_sidebar_two_columns'    => APOLLO_ADMIN_IMAGES_URL . '/right-sidebar-two-columns.png',
            'full_960'    => APOLLO_ADMIN_IMAGES_URL . '/home-full-960.jpg',
        )
    );

    // Header
    $options[] = array(
        'name'      => __( 'Homepage Settings', 'apollo' ),
        'type'      => 'header_block'
    );


    if (defined('OC_ADD_DEFAULT_DATA_TYPE') && OC_ADD_DEFAULT_DATA_TYPE){
        $options[] = array(
            'name'      => __( 'Data type', 'apollo' ),
            'id'        => 'is_default_data_type',
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Default', 'apollo' ),
                0           => __( 'Widget area', 'apollo' ),
            ),
            'class'     => 'no-border apollo_input_url',
        );
    }

    //@ticket: #17250: Octave Theme - [Feature block] New feature block title 
    $featureBlockTitle = apply_filters('apl_add_option_feature_block_title_style', array());
    if (!empty($featureBlockTitle)) {
        $options = array_merge($options, $featureBlockTitle);
    }

    if ($isAvailEvent) {

        $options[] = array(
            'name'     => __( 'Facebook Sharing Logo', 'apollo' ),
            'id'   => Apollo_DB_Schema::_FB_SHARING_LOGO,
            'std'  => '',
            'type'     => 'text',
            'class' => 'apollo_input_url no-border inline'
        );

        $options[] = array(
            'name'     => __( 'Num Featured Events', 'apollo' ),
            'desc'     => sprintf( __( 'default %s events', 'apollo' ), Apollo_Display_Config::PAGE_SIZE ),
            'id'   => Apollo_DB_Schema::_NUMBER_FEATURED_EVENTS,
            'std'  => '',
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );

        $options[] = array(
            'name'     => __( 'Num Spotlight Events', 'apollo' ),
            'desc'     => sprintf( __( 'default %s events', 'apollo' ), Apollo_Display_Config::NUM_HOME_SPOTLIGHT ),
            'id'   => Apollo_DB_Schema::_NUM_HOME_SPOTLIGHT_EVENT,
            'std'  => Apollo_Display_Config::NUM_HOME_SPOTLIGHT,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );

        $options[] = array(
            'name'     => __( 'Number of characters description truncation for Event Spotlight', 'apollo' ),
            'desc'     => sprintf( __( '%s characters by default', 'apollo' ), Apollo_Display_Config::HOME_SMALL_SPOTLIGHT_EVENT_OVERVIEW_NUMBER_CHAR ),
            'id'   => Apollo_DB_Schema::_NUM_CHARS_SPOTLIGHT_EVENT_DESC,
            'std'  => Apollo_Display_Config::HOME_SMALL_SPOTLIGHT_EVENT_OVERVIEW_NUMBER_CHAR,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );

    }
    
    $spot_type = array(
        'large'     => __( 'Alternate Spotlight', 'apollo' ),
        'medium'   => __( 'Event spotlight', 'apollo' ),
        'slider'    => __( 'Event Slider in Date Flags', 'apollo' ),
        'small'    => __( 'Small Alternate Spotlight', 'apollo' ),
    );
    
    if (!$isAvailEvent) {
        unset($spot_type['medium']);
    }

    //@ticket #17821 0002466: Promo video homepage - New option for the home spotlight
    $customSpotlightType = apply_filters('apl_add_custom_spotlight_type', array());
    if( !empty($customSpotlightType)){
        $spot_type = array_merge($spot_type, $customSpotlightType);
    }
    
    // Enable Facebook Enable
    $options[] = array(
        'name'       => __( 'Spotlight Type', 'apollo' ),
        'id'        => Apollo_DB_Schema::_SPOT_TYPE,
        'std'       => APOLLO_THEME_ACTIVATE_SPOT_TYPE,
        'type'      => 'radio',
        'preview'   => true,
        'options'   => $spot_type,
        'class'     => 'no-border apollo_input_url apollo_input_url_newline apl-spot-type',
    );

    /**
     * @ticket #18515: Add the options allow control the transition speed of the slides.
     */
    $options[] = array(
        'name'     => __( 'SlideShow Speed', 'apollo' ),
        'id'   => Apollo_DB_Schema::_APL_HOME_SLIDESHOW_SPEED,
        'std'  => Apollo_DB_Schema::_APL_SLIDESHOW_SPEED_DEFAULT,
        'desc' => __('Set the speed of the slideshow cycling, in milliseconds, minimum = 2000', 'apollo'),
        'type'     => 'number',
        'class' => 'no-border inline apl-slideshow-speed',
        'attr'    => "min = 2000",
        'wplm'  => true,
    );

    //@ticket #17821 0002466: Promo video homepage - New option for the home spotlight
    $customSpotlightContain = apply_filters('apl_add_custom_spotlight_contain', array());
    if( !empty($customSpotlightContain)){
        $options = array_merge($options, $customSpotlightContain);
    }

    /**
     * @ticket #18509: wpdev54 Customization - Display full width and height of the images on the slider
     */
    $sliderDisplayFull = apply_filters('apl_home_slider_display_full', array());
    if (!empty($sliderDisplayFull)) {
        $options = array_merge($options, $sliderDisplayFull);
    }

    /** @Ticket #16649 - Allow adding options for top header*/
    $octaveTopHeader = array();
    $octaveTopHeader = apply_filters('apl_custom_home_setting_options', $octaveTopHeader);
    if (!empty($octaveTopHeader)) {
        $options = array_merge($options, $octaveTopHeader);
    }


    $currentTheme = function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '';

    if ( $currentTheme == APL_SM_DIR_THEME_NAME) {

        $options[] = array(
            'name' => __('Num of  Spotlight displays on HomePage - Large Slider', 'apollo'),
            'desc' => sprintf(__('default %s spotlight', 'apollo'), Apollo_Display_Config::NUM_HOME_SPOTLIGHT),
            'id' => Apollo_DB_Schema::_APL_SPOT_LIGHT_NUM_DISPLAY_SLIDER,
            'std' => Apollo_Display_Config::NUM_HOME_SPOTLIGHT,
            'type' => 'text',
            'class' => 'apollo_input_number no-border inline'
        );
    }

    $options[] = array(
        'name' => __('Event Spotlight Background Image','apollo'),
        'id' => Apollo_DB_Schema::_EVENT_SPOTLIGHT_BG_IMG,
        'type' => 'upload',
        'class'     => 'no-border',
        'desc'  => __('This option only apply for Event Spotlight on Home page', 'apollo')
    );

    $semiTransparentTypes = array(
        'def'     => __( 'Default', 'apollo' ),
        'below'  => __( 'Below the Spotlight graphic', 'apollo' ),
    );

    // Semi-transparent Type
    $options[] = array(
        'name'       => __( 'Semi-transparent Type', 'apollo' ),
        'id'        => Apollo_DB_Schema::_HOME_SEMI_TRANSPARENT_TYPE,
        'std'       => 'def',
        'type'      => 'select',
        'options'   => $semiTransparentTypes,
        'class'     => 'no-border inline ignore-preview-event',
    );

    // Semi-transparent Background color
    $options[] = array(
        'name'       => __( 'Semi-transparent background color', 'apollo' ),
        'id'        => Apollo_DB_Schema::_HOME_SEMI_TRANSPARENT_TYPE_BACKGROUND_COLOR,
        'type'      => 'color',
        'std'       => '#000000',
        'class'     => 'no-border apollo_input_url apollo_input_url_newline',
    );
    
    $options[] = array(
        'name' => __('Color for title','apollo'),
        'id' => Apollo_DB_Schema::_COLOR_SELECTION_FONT_TITLE,
        'type' => 'color',
        'std'       => '#ffffff',
        'class'     => 'no-border apollo_input_url apollo_input_url_newline',
    );

    $options[] = array(
        'name' => __('Color for description','apollo'),
        'id' => Apollo_DB_Schema::_COLOR_SELECTION_FONT,
        'type' => 'color',
        'std'       => '#ffffff',
        'class'     => 'no-border apollo_input_url apollo_input_url_newline',
    );
    
    
    /** Home featured events */
    if ($isAvailEvent) {
        
        // Event slider color
        $options[] = array(
            'name' => __('Slider background','apollo'),
            'id' => Apollo_DB_Schema::_SLIDER_BACKGROUND,
            'type' => 'color',
            'std'       => '#ebebeb',
            'class'     => 'no-border apollo_input_url apollo_input_url_newline',
        );

        $options[] = array(
            'name' => __('Slider rotation bar background','apollo'),
            'id' => Apollo_DB_Schema::_ROTATION_BAR_BACKGROUND,
            'type' => 'color',
            'std'       => '#cdcbcc',
            'class'     => 'no-border apollo_input_url apollo_input_url_newline',
        );

        $options[] = array(
            'name' => __('Color for date','apollo'),
            'id' => Apollo_DB_Schema::_COLOR_SELECTION_FONT_DATE,
            'type' => 'color',
            'std'       => '#999999',
            'class'     => 'no-border apollo_input_url apollo_input_url_newline',
        );
        /** End Event slider color */
        
        $options[] = array(
            'name'        => __( 'Home Featured Events', 'apollo' ),
            'type'        => 'header_block'
        );

        // Header logo and text display type option
        $activate_topten_array = array(
            1  => __( 'Yes', 'apollo' ),
            0  => __( 'No', 'apollo' ),
        );

        // Spotlight activate option
        $options[] = array(
            'name'        => __( 'Activate Top Ten Tab', 'apollo' ),
            'desc'        => __( '', 'apollo' ),
            'id'      => Apollo_DB_Schema::_ACTIVATE_TOPTEN_TAB,
            'std'     => 0,
            'type'        => 'radio',
            'options'   => $activate_topten_array,
            'class'     => 'no-border to-topten',
            'before'   => '<div><input data-processing="'.__("Processing", "apollo").'" class="button" name="" id="clear-top-ten" value="'.__('Refresh Top Ten data', 'apollo').'" type="button" ></div>',
        );

        $options[] = array(
            'name'       => __( 'Data type', 'apollo' ),
            'id'        => Apollo_DB_Schema::_HOME_FEATURED_DATA_TYPE,
            'std'       => 'default',
            'type'      => 'select',
            'options'   => array(
                'default'        => __('Default', 'apollo'),
                'only_discount'  => __('Only discount events')
            ),
            'class'     => 'no-border inline',
        );

        //@ticket: #17252: Octave Theme - [Event feature] New title style for the horizontal feature block - item 8, 9, 10
        $rowTitleStyle = apply_filters('apl_add_option_row_title_style', array());
        if (!empty($rowTitleStyle)) {
            $options = array_merge($options, $rowTitleStyle);
        }

        $options[] = array(
            'name'     => __( 'How many hours that events will be expired in the Top ten tab?', 'apollo' ),
            'desc'     => sprintf( __( 'The default value is %s hours', 'apollo' ), Apollo_Display_Config::TOP_TEN_HOURS_AGO ),
            'id'   => Apollo_DB_Schema::_TOP_TEN_HOURS_AGO,
            'std'  => Apollo_Display_Config::TOP_TEN_HOURS_AGO,
            'type'     => 'text',
            'class' => 'no-border inline apollo_input_number'
        );


        // Event home featured CMS page
        $options[] = array(
            'name'        => __( 'Enable Events Featured Content', 'apollo' ),
            'desc'        => __( '', 'apollo' ),
            'id'      => Apollo_DB_Schema::_HOME_CMS_PAGE_EVENT_FEATURED,
            'std'     => 0,
            'type'        => 'radio',
            'options'   => array(
                1  => __( 'Yes', 'apollo' ),
                0  => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border'
        );

        // ticket #11105
        $options[] = array(
            'name'  => __('Label Featured Events', 'apollo'),
            'desc'  => sprintf(__('The default value is %s', 'apollo'), __(Apollo_Display_Config::DEFAULT_FEATURED_EVENTS_LABEL, 'apollo')),
            'id'    => Apollo_DB_Schema::_FEATURED_EVENTS_LABEL,
            'std'   => Apollo_Display_Config::DEFAULT_FEATURED_EVENTS_LABEL,
            'type'  => 'text',
            'class' => 'no-border inline',
        );

        $options[] = array(
            'name'     => __( 'Label Top Ten Tab', 'apollo' ),
            'desc'     => sprintf( __( 'The default value is %s', 'apollo' ), __(Apollo_Display_Config::TOP_TEN_LABEL ,'apollo') ),
            'id'   => Apollo_DB_Schema::_TOPTEN_LABEL_TAB,
            'std'  => '',
            'type'     => 'text',
            'class' => 'no-border inline'
        );

        // On/Off hover effect grid type
        $activate_hover_array = array(
            1  => __( 'Yes', 'apollo' ),
            0  => __( 'No', 'apollo' ),
        );

        // Spotlight activate option
        $options[] = array(
            'name'        => __( 'Enable Hover Style In Two Columns Layout', 'apollo' ),
            'desc'        => __( '', 'apollo' ),
            'id'      => Apollo_DB_Schema::_ACTIVATE_HOVER_EFFECT_IN_GRID,
            'std'     => 1,
            'type'        => 'radio',
            'options'   => $activate_hover_array,
            'class'     => 'no-border',
        );

        // Remove the text description
        $options[] = array(
            'name'      => __( 'Enable the description', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ENABLE_THE_TEXT_DESCRIPTION,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border ',
        );

        // Character limit of text description
        $options[] = array(
            'name'     => __( 'Number of characters description truncation', 'apollo' ),
            'desc'     => __('150  characters by default','apollo'),
            'id'   => Apollo_DB_Schema::_APL_EVENT_HOME_FEATURED_CHARACTERS_DESCRIPTION,
            'std'  => 150,
            'type'     => 'text',
            'class' => 'apollo_input_number no-border inline'
        );

        /*@ticket #16886: Octave Theme - [CF] 20180730 - Three columns options of feature events block*/
        $feaEventTypes = apply_filters('apl_featured_event_display_option', array(
            'sta'     => __( 'Standard (by default)', 'apollo' ),
            'hor'  => __( 'Horizontal', 'apollo' ),
            'ver'  => __( 'Vertical - Default Style', 'apollo' ),
            'ver-no-title'  => __( 'Vertical - No Tab Title', 'apollo' ),
            'ver1'     => __( 'Vertical - Columns are located next to each other', 'apollo' ),
        ));

        /** @Ticket #17850 */
        $itemsPerRow = apply_filters('apl_custom_item_per_row', array());
        if (!empty($itemsPerRow)) {
            $options = array_merge($options, $itemsPerRow);
        }

        // Enable Facebook Enable
        $options[] = array(
            'name'       => __( 'Featured Event Display Option', 'apollo' ),
            'id'        => Apollo_DB_Schema::_HOME_FEATURED_TYPE,
            'std'       => 'sta',
            'type'      => 'select',
            'options'   => $feaEventTypes,
            'preview'   => true,
            'class'     => 'no-border inline ignore-preview-event',
        );

        /** @Ticket #16983 - Octave Theme - [CF] 20180730 - Apply horizontal style for the home feature event block on the home page*/
        $maxTotalEvents = apply_filters('apl_add_max_total_event', array());
        if (!empty($maxTotalEvents)) {
            $options = array_merge($options, $maxTotalEvents);
        }

        $homeFeaDatOpts = array(
            'circle_tl'     => __( 'Date bubble - upper left', 'apollo' ),
            'rec_t'    => __( 'Date strip - top', 'apollo' ),
            'rec_b'    => __( 'Date strip - bottom', 'apollo' ),
            'rec_above_image'    => __( 'Date strip - above the image', 'apollo' ),
            'rec_br'   => __( 'Date box - bottom right', 'apollo' ),
            'flags'   => __( 'Date Flags ', 'apollo' ),
        );


        /*@ticket #17252 Octave Theme - [Event feature] New title style for the horizontal feature block - item 8, 9, 10*/
        $dateBelowTile = apply_filters('apl_add_option_home_featured_date_option', array());
        $homeFeaDatOpts = array_merge($homeFeaDatOpts, $dateBelowTile);

        // Enable Facebook Enable
        $options[] = array(
            'name'       => __( 'Date Display Option', 'apollo' ),
            'id'        => Apollo_DB_Schema::_HOME_FEATURED_DATE_OPTION,
            'std'       => 'sta',
            'type'      => 'select',
            'preview'   => true,
            'options'   => $homeFeaDatOpts,
            'class'     => 'no-border inline',
        );

        $selected_events = of_get_option( Apollo_DB_Schema::_HOME_CAT_FEATURES, false, true );

        // Multiple select
        $args = array(
            'selected'   => $selected_events,
        );

        $terms = get_terms( 'event-type', array( 'menu_order' => 'asc', 'orderby' => 'name', 'hide_empty' => false ) );

        if($terms instanceof WP_Error) {
            $terms = array();
        }

        $options[] = array(
            'name'     => __( 'Select Category Tabs', 'apollo' ),
            'desc'     => '',
            'id'   => Apollo_DB_Schema::_HOME_CAT_FEATURES,
            'std'  => '',
            'type'     => 'multiselect_sort',
            'class' => 'apollo_input_number no-border inline',
            'options' => apollo_walk_category_dropdown_tree( $terms, 0, $args ),
            'hasColorPicker' => true,
            'hasCheckBox' => true,
            'hasCustomLink' => true,
            'selected'   => $selected_events,
            'data-type'  => 'term',
            'data-attr-btn' => 'data-alert="'.__('Vertical view mode only allow 4 primary categories','apollo').'"',
        );
        $options[] = array(
            'name'  => '',
            'id'   => Apollo_DB_Schema::_HOME_CAT_FEATURES. '_of_color',
            'multiple'  => true,
            'type'  => 'hidden',
        );

        $options[] = array(
            'name'  => '',
            'id'   => Apollo_DB_Schema::_HOME_CAT_FEATURES. '_of_asname',
            'multiple'  => true,
            'type'  => 'hidden',
            'class' => 'trans',
            'cloneTranstion'    => true,
        );

        /*@ticket #17733: CF] 20180928 - [Homepage][Feature events] Custom link for the 'Spotlight' category page - Item 5*/
        $options[] = array(
            'name'  => '',
            'id'   => Apollo_DB_Schema::_HOME_CAT_FEATURES. '_use_cutom_link',
            'type'  => 'hidden',
            'multiple'  => true,
        );

        $options[] = array(
            'name'  => '',
            'id'   => Apollo_DB_Schema::_HOME_CAT_FEATURES. '_custom_link',
            'type'  => 'hidden',
            'multiple'  => true,
        );

        // Clone data for translation
        if ( Apollo_App::hasWPLM() ) {
            global $sitepress;
            $languages = $sitepress ? $sitepress->get_active_languages() : false;
            if ($languages) {
                foreach ($languages as $lang => $v) {
                    if ($lang == 'en') continue;
                    $options[] = array(
                        'name'  => '',
                        'id'   => Apollo_DB_Schema::_HOME_CAT_FEATURES. '_of_asname_'. $lang,
                        'multiple'  => true,
                        'type'  => 'hidden',
                        'class' => 'trans',
                        'cloneTranstion'    => true,
                        'apply_filter'    => false,
                    );
                }
            }
        }
    } /** End Home featured events */

    /** Home featured articles */

    /** End Home content*/    
    
    $options[] = array(
        'name'        => __( 'Top Sub Contain', 'apollo' ),
        'type'        => 'header_block'
    );
    $options[] = array(
        'desc'     => '',
        'id'   => Apollo_DB_Schema::_TOP_SUB_CONTAIN,
        'std'  => render_php_to_string( APOLLO_THEME_SUB_CONTAIN_TOP ),
        'class' => 'full',
        'type'     => 'editor',
        'is_static_file' => true,
    );

    $options[] = array(
        'name'        => __( 'Bottom Sub Contain', 'apollo' ),
        'type'        => 'header_block'
    );
    $options[] = array(
        'desc'     => '',
        'id'   => Apollo_DB_Schema::_BOTTOM_SUB_CONTAIN,
        'std'  => render_php_to_string( APOLLO_THEME_SUB_CONTAIN_BOTTOM ),
        'class' => 'full',
        'type'     => 'editor',
        'is_static_file' => true,
    );


    $options[] = array(
        'name'        => __( 'Featured Blog - Top Content', 'apollo' ),
        'type'        => 'header_block'
    );
    $options[] = array(
        'desc'     => '',
        'id'   => Apollo_DB_Schema::_FEATURED_BLOG_TOP_CONTENT,
        'class' => 'full',
        'type'     => 'editor',
        'is_static_file' => true,
    );

    return $options;
}