ApolloORGAdmin = function () {
    function getTinyMCEContent(id){
        var content = '';
        if ( $('#wp-'+id+'-wrap').hasClass("tmce-active") ) {
            // if the WordPress editor is in "Visual" mode
            content =  tinymce.get(id).getContent();
        } else {
            // if the WordPress editor is in "Text" mode
            content = $('#' + id).val();
        }
        return content;
    }
    function previewBusinessChanges() {
        /*@ticket #18087: [CF] 20181030 - [ORG][Admin form] Preview Changes button for the business - item 4, 5*/
        $('.preview-business').off('click').on('click', function (e) {

            var postTitle = $("#apollo-business-information #_apl_business_title").val(),
                postContent = getTinyMCEContent('_apl_business_description'),
                categories = $("input[name='apl_business_admin_form[_apl_business_categories][]']:checked").map(function () {
                    return $(this).val();
                }).get(),
                featured = $("input[name='_apl_post_type_cf_data[_apl_bs_features][]']:checked").map(function () {
                    return $(this).val();
                }).get(),
                hours = getTinyMCEContent('_apl_business_hours'),
                reservations = getTinyMCEContent('_apl_business_reservation'),
                otherInfo = getTinyMCEContent('_apl_business_other_info');

            if (typeof categories === "object") {
                categories = categories.join();
            }
            var businessData = {
                "post_id": $(this).attr("data-post-id"),
                "post_title": postTitle,
                "post_content": postContent,
                "categories": categories,
                "featured": featured,
                "hours": hours,
                "reservations": reservations,
                "other_info": otherInfo
            };

            $.ajax({
                url: APL.ajax_url,
                type: "post",
                data: {
                    action: 'apollo_preview_change_business',
                    business_data: businessData,
                    module: 'organization',
                },
                beforeSend: function () {
                    $(window).block(jQuery.apl.blockUI);
                },
                success: function (res) {
                    if (res && res.preview_link) {
                        var preview_link = res.preview_link + "?preview_business=true";
                        window.open(
                            preview_link,
                            '_blank'
                        );
                    }
                    $(window).unblock();
                },
                error: function (error) {
                    $(window).unblock();
                }
            });
        });
    }

    return {
        init: function () {
            previewBusinessChanges();
        }
    };
}();

jQuery(document).ready(function () {
    ApolloORGAdmin.init();
});