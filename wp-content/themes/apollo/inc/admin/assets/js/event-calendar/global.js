/**
 * Created by tuanphp on 31/10/2014.
 */

(function($) {

    var $body = $('body'), $window = $(window);


    $(function() {    
        
        if ( $.apl.is_mobile() ) {
            $('body').addClass('on-mobile');
            $( '.single .pt.print' ).hide();
            $( '.single .pt.email' ).hide();
        }
        
        /* HANDLE TOP SOCIAL BUTTON FUNCTION  */
        $.apl.handle_social_button();
        
        // Handle see more taxonomies
        $( '.wc-l' ).on( 'click', '#apollo-see-more-taxonomy', $.apl.handle_see_more_taxonomy );
        
        // Handle content of registered page
        $.apl.handle_registered_page_content();
        
        /* RESIZE EVENT DESKTOP OR CHANGE MOBILE DIRECTION*/
        $.apl.handle_screen_size();
        $( window ).resize( $.apl.handle_screen_size );
       
    });
    
    
    
    /***************************************************************************
     *   Register global function for apollo 
     **/
    $.apl = {
        google_map: {
            has_load: false,
            load_me_and_do: function(){ console.log('Please init me!') },
            google_map_event: function() { /* will override later */ },
            set_has_load: function() { console.log('Please init me!') },
            current_callback: '', /* save current callback at the first time! */
            key: APL.google.apikey.browser /* transfer from server ? - @todo remove later */
        },
        handle_social_button: function() {},
        handle_see_more_taxonomy: function() {},
        handle_registered_page_content: function() {},
        handle_screen_size: function() {},
        is_mobile: function() {},
        init_autocomplete: function() {},
        send_id_obj: function() {},
        send_selector_ids: function() {},
        clear_form: function() {},
    };

    $.apl.blockUI = {
        message: APL.resource.loading.message,
        css: $.parseJSON(APL.resource.loading.image_style)
    };

    $.apl.is_mobile = function() {

        if(typeof sessionStorage !== "undefined") {
            if ( sessionStorage.desktop )
                return false;
            else if ( localStorage.mobile ) // mobile storage
                return true;
        }

        // alternative
        var mobile = ['iphone','ipad','android','blackberry','nokia','opera mini','windows mobile','windows phone','iemobile'];
        for ( var i in mobile )
            if (navigator.userAgent.toLowerCase().indexOf(mobile[i]) > 0) return true;

        // nothing found. assume desktop
        return false;
    };
    
    $.apl.google_map.set_has_load = function() {
        $.apl.google_map.has_load = true;
        eval($.apl.google_map.current_callback)();
    };
    /**/
    $.apl.google_map.load_me_and_do = function(callback) {
        $.apl.google_map.current_callback = callback;
        if(typeof callback !== "string") {
            throw "This callback must be a string";
        }

        if($.apl.google_map.has_load === false) {
            $.apl.google_map.has_load = true;
            $.getScript('https://maps.googleapis.com/maps/api/js?key=' + APL.google.apikey.browser + '&callback=jQuery.apl.google_map.set_has_load');
        }
        else {
           eval(callback)();
        }
    };

    $.apl.handle_social_button = function() {
        var social_links = APL.config.social_links,
            fb = $( '.social-top .fb' ),    
            lk = $( '.social-top .lk' ),
            go = $( '.social-top .go' ),
            tu = $( '.social-top .tu' ),
            yt = $( '.social-top .yt' ); 
      
        if ( ! social_links.lk && ! social_links.fb && ! social_links.go && ! social_links.tu && ! social_links.yt ) {
            $( '.top-head .top-blk.social-top' ).hide();
        } else {
            $( '.top-head .top-blk.social-top' ).show();
        }    
    
        if ( social_links.lk ) {
            lk.attr( 'href', social_links.lk );
            lk.show();
        } else {
            lk.hide();
        }

        if ( social_links.fb ) {
            fb.attr( 'href', social_links.fb );
            fb.show();
        } else {
            fb.hide();
        }

        if ( social_links.go ) {
            go.attr( 'href', social_links.go );
            go.show();
        } else {
            go.hide();
        }

        if ( social_links.tu ) {
            tu.attr( 'href', social_links.tu );
            tu.show();
        } else {
            tu.hide();
        }

        if ( social_links.yt ) {
            yt.attr( 'href', social_links.yt );
            yt.show();
        } else {
            yt.hide();
        }
    };

    $.apl.handle_see_more_taxonomy = function() {

        var btn         = $( this ),    
            total       = btn.attr( 'total' ),
            per_page    = btn.attr( 'per-page' ),
            container   = $( '#apollo-taxonomy-container' );

        $.ajax({
            url: APL.ajax_url,
            data: {
                action:         'apollo_taxonomy_get_more',
                total:          total,
                per_page:       per_page,
                num_displayed:  $('.apollo-taxonomy-row').length,
                taxonomy:       btn.attr( 'taxonomy' )
            },
            success: function ( res ) {
                var review_container = 'more-reviews-start-'+ $('.apollo-taxonomy-row').length,
                    result           = eval( "("+ res + ")" );

                container.append( '<div id="' + review_container + '"></div>' );
                $( '#'+ review_container ).hide();
                $( '#'+ review_container ).html( result.html );
                $( '#'+ review_container ).slideDown( 400 );

                if ( ! result.display_btn ) {
                    btn.hide();
                }    
            }
        });
    };
    
    /**
    * Replace register form by registered infomation after finish register
    * */ 
    $.apl.handle_registered_page_content = function() {

        if ( typeof( apollo_registered_page ) != 'undefined' && apollo_registered_page == 'yes' ) {
            $( 'article' ).html( $( '#apollo-registered-page' ).html() );
        }
    };

    $.apl.handle_screen_size = function() {
       
        if(APL.is_front_page === "1") {
            if ( APL.config.layout.front_page == 'right_sidebar_two_columns' ) {
                var device_width = window.innerWidth
                    || document.documentElement.clientWidth
                    || document.body.clientWidth;
                if ( device_width <= 960 ) {
                    $('body').removeClass('grid');
                } else {
                    $('body').addClass('grid');
                }
            }
        }
    };

    $.apl.__ = function(text) {
        return text; /* @todo Don't translate yet. transfer from server */
    };
    
    /**
     * Bookmark/unbookmark event/organization/venue
     * @param ajaxurl
     * @param id
     * @param post_type
     */
    ajaxAddBookmark = function(ajaxurl, id, post_type, btn) {
        var _blockScreen = {
            message: APL.resource.loading.message,
            css: jQuery.parseJSON(APL.resource.loading.image_style)
        },
        _btn = $(btn);

        $.ajax({
            type: 'post',
            url: ajaxurl,
            data: {
                'action': 'add_bookmark',
                'id': id,
                'post_type': post_type
            },
            beforeSend: function() {
                $(window).block(_blockScreen);
            },
            success: function(res) {
                if (res.action == "not_login") {
                    alert(res.message);
                } else {
               
                    if (res.action == "add") {
                        _btn.addClass("bookmark_highlight");
                        _btn.text( _btn.attr('added-text') );
                    } else {
                        _btn.removeClass("bookmark_highlight");
                        _btn.text( _btn.attr('add-text') );
                    }
                }
                $('body').unblock();
            }
        });
    };


    /* LOG FUNC */
    $('body').on('click', '[data-ride="ap-logclick"]', function() {
        var $this = $(this), data= $this.data();

        $.ajax({
            type: 'GET',
            url: APL.ajax_url,
            data: data,
            success: function(response) {
                /* For test only */
                console.log(response);
            }
        });
    });
    /* END - LOG FUNC */

    apollo_submit_form = function( frm, btn ) {
        
        if ( btn && document.forms[frm].case_action ) {
            document.forms[frm].case_action.value = btn;
        }
        document.forms[frm].submit();
        
    };
    
    apollo_is_add_tmp = function ( btn, mod ) {
        
        $.ajax({
            'url': APL.ajax_url,
            'data': {
                action: 'apollo_is_add_tmp',
                btn: btn,
                mod: mod,
            },
            success: function() {
                
            }
        });
    };
    
    checkCBBefore = function(elm) {
        
        if ( $(elm).prev().attr('checked') == 'checked' ) {
            $(elm).prev().removeAttr('checked');
        } else {
            $(elm).prev().attr('checked', true);
        }
    };
    
    /**
     * Call auto complete
     * @param elm
     * @param {string} action Action ajax
     * */
    $.apl.init_autocomplete = function( elm, action ) {
        var obj = $( elm )
        obj.autocomplete({
            source : function (request, response) 
            {     
                var s = obj.val();
                $.ajax({
                    url: APL.ajax_url,
                    data: {
                        action: action,
                        s: s,
                    },
                    success: function (data) { 

                        response($.map(eval(data), function(item) {

                            return {
                                id: item.ID,
                                value: item.post_title,
                            };
                        }));
                    },
                    error : function (a,b,c) {

                    }
                });
            },                
            select: function (event, ui) { 
                console.log(ui.item);
            }               
        });
    };

    var bind_event_raing = function() {
        var $app_event_rating = $('[data-ride="ap-event_rating"]');
        var length = $app_event_rating.length;
        var uids = [];

        $app_event_rating.each(function (i, v) {
            var $this = $(this),
                _data = $this.data();

            if($this.attr('binded') === 'true') return true;

            var data = {
                item_id:  _data.i,
                item_type: _data.t,
                item_msg: _data.msg,
            };

            $this.on('click', function() {

                /* Collect data */
                data['action'] = 'apollo_update_rating';

                $.ajax({
                    url: APL.ajax_url,
                    data: data,

                    beforeSend: function() {
                        $(window).block($.apl.blockUI);
                    },
                    success: function(data) {
                        if(data.error === false) {
                            var $_count = $this.find('._count'),  _cc = parseInt($_count.text(), 10);
                            _cc = isNaN(_cc) ? 0 : _cc;
                            $_count.text(_cc + 1);
                        }
                        else {
                            alert(data.data.msg);
                        }
                    },
                    complete: function() {
                        $(window).unblock();

                    }

                })
            });

            uids.push(_data.i + ':' + _data.t);
            $this.attr('binded', 'true');

            if(i === length-1 ){
                /* lazy load */
                data['action'] = 'apollo_get_ratings';

                /* Action */
                data['uids'] = uids.join(",");
                $.ajax({
                    url: APL.ajax_url,
                    data: data,
                    success: function(data) {
                        if(data.error === false) {
                            $.each(data.data.counts, function(i, v) {
                                $('#_event_rating_' + v.i + '_' + v.t).find('._count').text(v.c);
                            });

                        }
                    }
                });
            }
        });
    };

    bind_event_raing();

    $('body').on('viewmore', function() {
        bind_event_raing();
    });

    (function() {
        var oalert = window.alert;
        $.apl.oalert = function() {
            oalert.apply(window, arguments);
        };
    })();


    $body.on('click', '[data-type="link"]', function(e) {
        var $this = $(this);
        window.location.href= $this.data('url');
    });

    $body.on('viewmore', function() {
        $window.trigger( 'viewmore');
    });

    /* CLAMP */
    $window.on('load resize custom-clamp',function(e) {
        $('[data-ride="ap-clamp"]').each(function() {
            var $this = $(this), n = $this.data('n');

            var $hiddenItem = $this, $oldItem = $this;
            while($hiddenItem[0].clientHeight === 0) {
                $oldItem = $hiddenItem;
                $hiddenItem = $hiddenItem.parent();
            }
            $hiddenItem = $oldItem;

            var previousCss  = $hiddenItem.attr("style");

            $hiddenItem.css({
                visibility: 'hidden',
                display:    'block'
            });

            $clamp($this[0], {clamp: n, useNativeClamp: false});

            $hiddenItem.attr("style", previousCss ? previousCss : "");

            $this.data('hasclamp', "true");
        });
    });
    $window.on('viewmore',function(e) {
        $('[data-ride="ap-clamp"]').each(function() {
            var $this = $(this), n = $this.data('n');

            if($this.data("hasclamp")=== "true") return true;

            var $hiddenItem = $this, $oldItem = $this;
            while($hiddenItem[0].clientHeight === 0) {
                $oldItem = $hiddenItem;
                $hiddenItem = $hiddenItem.parent();
            }
            $hiddenItem = $oldItem;

            var previousCss  = $hiddenItem.attr("style");
            $hiddenItem.css({
                visibility: 'hidden',
                display:    'block'
            });

            $clamp($this[0], {clamp: n, useNativeClamp: false});
            $hiddenItem.attr("style", previousCss ? previousCss : "");

        });
    });
    /* END-CLAMP */
    
    $.apl.send_id_obj = function() {
        var data = $(this).data();
        
        if ( ! $( data.source ).val() ) {
            alert(data.msg);
            return;
        };
        
        $(window).block({
            message: APL.resource.loading.message,
            css: $.parseJSON(APL.resource.loading.image_style)
        });

        var _data_source = $( data.source ).val();
        $.get( 
            APL.ajax_url, 
            { 
                action: data.action, 
                eid: _data_source,
            }, function(response) {
              
                $(window).unblock();

                window.location.reload();
            }
        );
    };
    
    $.apl.send_selector_ids = function() {
        var $this = $(this),
            _data = $(this).data(),
            
            _alist_value = [],
            _key = _data.data.split(':')[0],
            _param = _data.data.split(':')[1];    
            
        $(_data.data_source_selector).each(function(_,elem) {
            _alist_value.push($(elem).attr(_param));
        });
        
        if ( ! _alist_value.length ) {
            alert(_data.msg);
            return;
        };
        
        if ( ! _alist_value.length ) return;
        
        $.ajax({
            url: APL.ajax_url,
            
            data: {
                action: _data.action, 
                eids: _alist_value.join(','),
            },
            beforeSend: function() {
                $(window).block({
                    message: APL.resource.loading.message,
                    css: $.parseJSON(APL.resource.loading.image_style)
                });
            },
            success: function(res) {
                window.location.reload();
            },
            complete: function() {
                $(window).unblock();
            }
        });
    };

    $.apl.clear_form = function () {
        var _data_form = $(this).data();
        
        $( _data_form.form+ ' input:checkbox' ).attr('checked', false);
        $( _data_form.form+ ' input:text' ).val('');
        $( _data_form.form+ ' select option[selected="selected"]' ).removeAttr('selected');
        $( _data_form.form+ ' select option[selected]' ).removeAttr('selected');
        $( _data_form.form+ ' select' ).val('');
        $( _data_form.form+ ' input:text' ).removeAttr('value');
        
        if ( $( '#select2-educator-provider-container' ) ) {
            $( '#select2-educator-provider-container' ).text($('#educator-provider option').first().text());
        }
        
        if ( $('.qualification') ) {
            $('.qualification').removeClass('active');
        }
        
    };

    /* CHANGE EDITOR SIZE */
    $window.on('resize', function() {
        var $tools = $( '#wp-post_content-editor-tools'), contentWrapWidth =  $("#wp-post_content-wrap").width();
        $tools.css( {
            position: 'absolute',
            top: 0,
            width: contentWrapWidth
        } );
    });
    /* END */


    /* CLOSE ME PLUGIN */
    $body.on('click', '[data-ride="remove-relation"]', function() {
        var $this = $(this), mainwrapper = $this.data('mainwrapper'), target = $this.data('target'), desc = $this.data('confirm'), $target = '', action = $this.data('action');
        if(confirm(desc)) {

            if(typeof $this[target] === 'function' ) {
                $target = $this[target]();
            }
            else if(typeof $this.find(target) !== 'undefined' && typeof $this.find(target).length !== 'undefined') {
                $target = $this.closest(target);
            }

            $target.remove();

            if(typeof action !== 'undefined') {

                var _dataaction = $this.data('action_data'), _arrdataaction = _dataaction.split(','), _postid = $.trim(_arrdataaction[0]), _attachment_id = $.trim(_arrdataaction[1]), _field_name = typeof _arrdataaction[2] !=='undefined' ?  $.trim(_arrdataaction[2]) : '' ;
                $.ajax({
                    action: 'GET',
                    url: APL.ajax_url + '?action=' + action + '&postid=' + _postid + '&attid=' + _attachment_id + '&field=' + _field_name,
                    success: function(res) {
                        console.log(res);
                    }

                })
            }

        }

        $body.trigger('remove-relation', {mainwrapper: mainwrapper});

    });
    /* END*/

    /* UPlOAD PLUGIN */
    $body.on('change', '[data-ride="upload_file"]', function() {
        var $this = $(this), maxfilesize = $this.data('maxfilesize');
        if(typeof this.files[0] !== 'undefined' && this.files[0].size > maxfilesize) {
            alert('File too large. Maximum size allow is ' + (maxfilesize / (1024 * 1024)).toFixed(2) + 'MB');
            this.value= '';
        }

    });
    /* END */

    /* SHADOW MAN */
    $body.on('click', '[data-ride="shadow-man"]', function(e) {
        var $this = $(this),
            maxtimes= $this.data('maxtimes'),
            $mainwrapper = $($this.data('mainwrapper')),
            $template = $mainwrapper.find($this.data('template')+':first'),
            bornname = $this.data('bornname');

        if($template.length <= 0 ) {
            return false;
        }

        if(typeof maxtimes !== 'undefined' && typeof bornname !== 'undefined') {
            if($mainwrapper.find(bornname).length >= maxtimes) {
                alert('Max ' + maxtimes + ' item allow!');
                return false;
            }
        }

        var _append_to = $this.data('append-to'), _selector = _append_to, _method = 'append';
        if(_append_to.indexOf(':') !== '-1') {
            var _arr = _append_to.split(':');
            _selector = _arr[0];
            _method = _arr[1];
        }

        $mainwrapper.find(_selector + ':first')[_method]($template.html());

        $body.trigger('shadown-man', {'mainwrapper': $this.data('mainwrapper')});
    });
    /* END */

    /* Change css for divider */
    jQuery('[data-ride="shadow-man"]').length && (function() {
        jQuery('[data-ride="shadow-man"]').each(function() {
            var $this = $(this), mainwrapper = $this.data('mainwrapper');
            $(mainwrapper + ' .divider-shadow').eq(0).css('display', 'none');

        });
    })();
    $body.on('shadown-man remove-relation', function(e, data) {
        var mainwrapper = data['mainwrapper'];
        $(mainwrapper + ' .divider-shadow').eq(0).css('display', 'none');
    });
    /* END */

    var simpleTemplate = function(tpl, data) {
        var re = /<%([^%>]+)?%>/g, match;
        while(match = re.exec(tpl)) {
            tpl = tpl.replace(match[0], data[match[1]])
        }
        return tpl;
    };

    /* Redirect plugin */
    $body.on('click', '[data-ride="ap-redirect"]', function() {
        var $this = $(this), data= $this.data(), url = data['url'], arrParams = data['params'].split(','), mainParam = data['mainparam'];

        /* get arr value */
        var objData = {};
        $(arrParams).each(function(i, v) {
            var _d = data[v];
            if(typeof _d === 'string' && _d.indexOf('exp') !== -1) {
                _d = eval(_d.substring(_d.indexOf('exp') + 4));
            }
            objData[v] = _d;
        });

        /* check for eror */
        if(objData[mainParam] === '') {
            alert(data['emptymsg']);
            return false;
        }

        /* replace template */
        url = simpleTemplate(url, objData);

        window.location.href = url;

    });
    /* End */

    /* Plugin Load Asyn */
    $('[data-ride="ap-load_asyn"]').each(function() {
        var $this = $(this), data= $this.data(), url = data['url'], arrParams = data['params'].split(','), methodInsert = data['methodinsert'];

        /* get arr value */
        var objData = {};
        $(arrParams).each(function(i, v) {
            var _d = data[v];
            if(typeof _d === 'string' && _d.indexOf('exp') !== -1) {
                _d = eval(_d.substring(_d.indexOf('exp') + 4));
            }
            objData[v] = _d;
        });

        /* replace template */
        url = simpleTemplate(url, objData);

        $.ajax({
            url: url,
            method: 'GET',
            success: function(res) {
                if(methodInsert === 'append') {
                    $this.append(res.html);
                    $body.trigger('load_asyn.'+data['type']);
                }
            },
            complete: function(a, b) {

            }
        })
    });
    /* End */

    $body.on('load_asyn.load_edit_program', function() {
        $('.apl_select2').select2();
    });
})(jQuery);

