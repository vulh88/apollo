/**
 * Created by tuanphp on 27/10/2014.
 */

/* ========================================================================
 * PYTHWEB: MORE.js v1.1.0
 * Requires jQuery v1.7 or later
 * Requires jQuery blockUI plugin or Later
 *
 * Examples at: http://pythweb.com
 * Copyright (c) 2007-2013 M. Alsup
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 * ======================================================================== */

+function ($) {
    'use strict';

    // More CLASS DEFINITION
    // =========================

    var More = function (element, options) {
        this.$element    = $(element).on('click', $.proxy(this.more, this));
        this.options     = options;
    };

    More.VERSION  = '1.1.0';

    More.DEFAULTS = {
        "sourcetype": "url",
        "holder": "#moreContainer",
        "methodinsert": "after"
    };

    More.prototype.more = function() {
        var _this = this,
            options = this.options;
        this.$holder     = $(this.options.holder);

        var _blockUI = {
            message: this.options.blockuihtml,
            css: this.options.blockuicss
        };

        var _container = window;
        if(this.options.container !== undefined) {
            _container = this.options.container;
        }
        $(_container).block(_blockUI);
       
        $(document).ajaxStop(function() {
            $(_container).unblock(_blockUI);
        });
        switch(this.options.sourcetype) {
            case 'url':
                this.options.sourcedata = _this.$element.data('sourcedata');
                $.post(this.options.sourcedata, function( data ) {
                    if(data.url === undefined || data.url === "") {
                        console.log('To Using this plugin you must return next page url');
                        return;
                    }
                    _this.$holder[_this.options.methodinsert]( data.html );
                    _this.$element.data('sourcedata', data.url);

                    if(data.have_more !== undefined && data.have_more === false) {
                        _this.$element.hide();
                    }

                    $('body').trigger( 'viewmore', data.have_more );
                }).
                    done(function() {

                    }).
                    fail(function() {

                    }).
                    always(function(){

                    });

                this.$holder[this.options.methodinsert]()
                break;
            default:
                break;
        }
    }

    function Plugin(option) {
        return this.each(function () {
            var $this   = $(this)
            var data    = $this.data('bs.more')
            var options = $.extend({}, More.DEFAULTS, $this.data(), typeof option == 'object' && option)

            if (!data) $this.data('bs.more', (data = new More(this, options)))
        })
    }

    $.fn.more             = Plugin
    $.fn.more.Constructor = More

    $(window).on('load', function () {
        $('[data-ride="ap-more"]').each(function () {
            var $more = $(this)
            Plugin.call($more, $more.data())
        })
    })

}(jQuery);

/*!
 * jQuery blockUI plugin
 * Version 2.66.0-2013.10.09
 * Requires jQuery v1.7 or later
 *
 * Examples at: http://malsup.com/jquery/block/
 * Copyright (c) 2007-2013 M. Alsup
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 * Thanks to Amir-Hossein Sobhi for some excellent contributions!
 */

;(function() {
    /*jshint eqeqeq:false curly:false latedef:false */
    "use strict";

    function setup($) {
        $.fn._fadeIn = $.fn.fadeIn;

        var noOp = $.noop || function() {};

        // this bit is to ensure we don't call setExpression when we shouldn't (with extra muscle to handle
        // confusing userAgent strings on Vista)
        var msie = /MSIE/.test(navigator.userAgent);
        var ie6  = /MSIE 6.0/.test(navigator.userAgent) && ! /MSIE 8.0/.test(navigator.userAgent);
        var mode = document.documentMode || 0;
        var setExpr = $.isFunction( document.createElement('div').style.setExpression );

        // global $ methods for blocking/unblocking the entire page
        $.blockUI   = function(opts) { install(window, opts); };
        $.unblockUI = function(opts) { remove(window, opts); };

        // convenience method for quick growl-like notifications  (http://www.google.com/search?q=growl)
        $.growlUI = function(title, message, timeout, onClose) {
            var $m = $('<div class="growlUI"></div>');
            if (title) $m.append('<h1>'+title+'</h1>');
            if (message) $m.append('<h2>'+message+'</h2>');
            if (timeout === undefined) timeout = 3000;

            // Added by konapun: Set timeout to 30 seconds if this growl is moused over, like normal toast notifications
            var callBlock = function(opts) {
                opts = opts || {};

                $.blockUI({
                    message: $m,
                    fadeIn : typeof opts.fadeIn  !== 'undefined' ? opts.fadeIn  : 700,
                    fadeOut: typeof opts.fadeOut !== 'undefined' ? opts.fadeOut : 1000,
                    timeout: typeof opts.timeout !== 'undefined' ? opts.timeout : timeout,
                    centerY: false,
                    showOverlay: false,
                    onUnblock: onClose,
                    css: $.blockUI.defaults.growlCSS
                });
            };

            callBlock();
            var nonmousedOpacity = $m.css('opacity');
            $m.mouseover(function() {
                callBlock({
                    fadeIn: 0,
                    timeout: 30000
                });

                var displayBlock = $('.blockMsg');
                displayBlock.stop(); // cancel fadeout if it has started
                displayBlock.fadeTo(300, 1); // make it easier to read the message by removing transparency
            }).mouseout(function() {
                $('.blockMsg').fadeOut(1000);
            });
            // End konapun additions
        };

        // plugin method for blocking element content
        $.fn.block = function(opts) {
            if ( this[0] === window ) {
                $.blockUI( opts );
                return this;
            }
            var fullOpts = $.extend({}, $.blockUI.defaults, opts || {});
            this.each(function() {
                var $el = $(this);
                if (fullOpts.ignoreIfBlocked && $el.data('blockUI.isBlocked'))
                    return;
                $el.unblock({ fadeOut: 0 });
            });

            return this.each(function() {
                if ($.css(this,'position') == 'static') {
                    this.style.position = 'relative';
                    $(this).data('blockUI.static', true);
                }
                this.style.zoom = 1; // force 'hasLayout' in ie
                install(this, opts);
            });
        };

        // plugin method for unblocking element content
        $.fn.unblock = function(opts) {
            if ( this[0] === window ) {
                $.unblockUI( opts );
                return this;
            }
            return this.each(function() {
                remove(this, opts);
            });
        };

        $.blockUI.version = 2.66; // 2nd generation blocking at no extra cost!

        // override these in your code to change the default behavior and style
        $.blockUI.defaults = {
            // message displayed when blocking (use null for no message)
            message:  '<h1>Please wait...</h1>',

            title: null,		// title string; only used when theme == true
            draggable: true,	// only used when theme == true (requires jquery-ui.js to be loaded)

            theme: false, // set to true to use with jQuery UI themes

            // styles for the message when blocking; if you wish to disable
            // these and use an external stylesheet then do this in your code:
            // $.blockUI.defaults.css = {};
            css: {
                padding:	0,
                margin:		0,
                width:		'30%',
                top:		'40%',
                left:		'35%',
                textAlign:	'center',
                color:		'#000',
                border:		'3px solid #aaa',
                backgroundColor:'#fff',
                cursor:		'wait'
            },

            // minimal style set used when themes are used
            themedCSS: {
                width:	'30%',
                top:	'40%',
                left:	'35%'
            },

            // styles for the overlay
            overlayCSS:  {
                backgroundColor:	'#000',
                opacity:			0.6,
                cursor:				'wait'
            },

            // style to replace wait cursor before unblocking to correct issue
            // of lingering wait cursor
            cursorReset: 'default',

            // styles applied when using $.growlUI
            growlCSS: {
                width:		'350px',
                top:		'10px',
                left:		'',
                right:		'10px',
                border:		'none',
                padding:	'5px',
                opacity:	0.6,
                cursor:		'default',
                color:		'#fff',
                backgroundColor: '#000',
                '-webkit-border-radius':'10px',
                '-moz-border-radius':	'10px',
                'border-radius':		'10px'
            },

            // IE issues: 'about:blank' fails on HTTPS and javascript:false is s-l-o-w
            // (hat tip to Jorge H. N. de Vasconcelos)
            /*jshint scripturl:true */
            iframeSrc: /^https/i.test(window.location.href || '') ? 'javascript:false' : 'about:blank',

            // force usage of iframe in non-IE browsers (handy for blocking applets)
            forceIframe: false,

            // z-index for the blocking overlay
            baseZ: 1000,

            // set these to true to have the message automatically centered
            centerX: true, // <-- only effects element blocking (page block controlled via css above)
            centerY: true,

            // allow body element to be stetched in ie6; this makes blocking look better
            // on "short" pages.  disable if you wish to prevent changes to the body height
            allowBodyStretch: true,

            // enable if you want key and mouse events to be disabled for content that is blocked
            bindEvents: true,

            // be default blockUI will supress tab navigation from leaving blocking content
            // (if bindEvents is true)
            constrainTabKey: true,

            // fadeIn time in millis; set to 0 to disable fadeIn on block
            fadeIn:  200,

            // fadeOut time in millis; set to 0 to disable fadeOut on unblock
            fadeOut:  400,

            // time in millis to wait before auto-unblocking; set to 0 to disable auto-unblock
            timeout: 0,

            // disable if you don't want to show the overlay
            showOverlay: true,

            // if true, focus will be placed in the first available input field when
            // page blocking
            focusInput: true,

            // elements that can receive focus
            focusableElements: ':input:enabled:visible',

            // suppresses the use of overlay styles on FF/Linux (due to performance issues with opacity)
            // no longer needed in 2012
            // applyPlatformOpacityRules: true,

            // callback method invoked when fadeIn has completed and blocking message is visible
            onBlock: null,

            // callback method invoked when unblocking has completed; the callback is
            // passed the element that has been unblocked (which is the window object for page
            // blocks) and the options that were passed to the unblock call:
            //	onUnblock(element, options)
            onUnblock: null,

            // callback method invoked when the overlay area is clicked.
            // setting this will turn the cursor to a pointer, otherwise cursor defined in overlayCss will be used.
            onOverlayClick: null,

            // don't ask; if you really must know: http://groups.google.com/group/jquery-en/browse_thread/thread/36640a8730503595/2f6a79a77a78e493#2f6a79a77a78e493
            quirksmodeOffsetHack: 4,

            // class name of the message block
            blockMsgClass: 'blockMsg',

            // if it is already blocked, then ignore it (don't unblock and reblock)
            ignoreIfBlocked: false
        };

        // private data and functions follow...

        var pageBlock = null;
        var pageBlockEls = [];

        function install(el, opts) {
            var css, themedCSS;
            var full = (el == window);
            var msg = (opts && opts.message !== undefined ? opts.message : undefined);
            opts = $.extend({}, $.blockUI.defaults, opts || {});

            if (opts.ignoreIfBlocked && $(el).data('blockUI.isBlocked'))
                return;

            opts.overlayCSS = $.extend({}, $.blockUI.defaults.overlayCSS, opts.overlayCSS || {});
            css = $.extend({}, $.blockUI.defaults.css, opts.css || {});
            if (opts.onOverlayClick)
                opts.overlayCSS.cursor = 'pointer';

            themedCSS = $.extend({}, $.blockUI.defaults.themedCSS, opts.themedCSS || {});
            msg = msg === undefined ? opts.message : msg;

            // remove the current block (if there is one)
            if (full && pageBlock)
                remove(window, {fadeOut:0});

            // if an existing element is being used as the blocking content then we capture
            // its current place in the DOM (and current display style) so we can restore
            // it when we unblock
            if (msg && typeof msg != 'string' && (msg.parentNode || msg.jquery)) {
                var node = msg.jquery ? msg[0] : msg;
                var data = {};
                $(el).data('blockUI.history', data);
                data.el = node;
                data.parent = node.parentNode;
                data.display = node.style.display;
                data.position = node.style.position;
                if (data.parent)
                    data.parent.removeChild(node);
            }

            $(el).data('blockUI.onUnblock', opts.onUnblock);
            var z = opts.baseZ;

            // blockUI uses 3 layers for blocking, for simplicity they are all used on every platform;
            // layer1 is the iframe layer which is used to supress bleed through of underlying content
            // layer2 is the overlay layer which has opacity and a wait cursor (by default)
            // layer3 is the message content that is displayed while blocking
            var lyr1, lyr2, lyr3, s;
            if (msie || opts.forceIframe)
                lyr1 = $('<iframe class="blockUI" style="z-index:'+ (z++) +';display:none;border:none;margin:0;padding:0;position:absolute;width:100%;height:100%;top:0;left:0" src="'+opts.iframeSrc+'"></iframe>');
            else
                lyr1 = $('<div class="blockUI" style="display:none"></div>');

            if (opts.theme)
                lyr2 = $('<div class="blockUI blockOverlay ui-widget-overlay" style="z-index:'+ (z++) +';display:none"></div>');
            else
                lyr2 = $('<div class="blockUI blockOverlay" style="z-index:'+ (z++) +';display:none;border:none;margin:0;padding:0;width:100%;height:100%;top:0;left:0"></div>');

            if (opts.theme && full) {
                s = '<div class="blockUI ' + opts.blockMsgClass + ' blockPage ui-dialog ui-widget ui-corner-all" style="z-index:'+(z+10)+';display:none;position:fixed">';
                if ( opts.title ) {
                    s += '<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">'+(opts.title || '&nbsp;')+'</div>';
                }
                s += '<div class="ui-widget-content ui-dialog-content"></div>';
                s += '</div>';
            }
            else if (opts.theme) {
                s = '<div class="blockUI ' + opts.blockMsgClass + ' blockElement ui-dialog ui-widget ui-corner-all" style="z-index:'+(z+10)+';display:none;position:absolute">';
                if ( opts.title ) {
                    s += '<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">'+(opts.title || '&nbsp;')+'</div>';
                }
                s += '<div class="ui-widget-content ui-dialog-content"></div>';
                s += '</div>';
            }
            else if (full) {
                s = '<div class="blockUI ' + opts.blockMsgClass + ' blockPage" style="z-index:'+(z+10)+';display:none;position:fixed"></div>';
            }
            else {
                s = '<div class="blockUI ' + opts.blockMsgClass + ' blockElement" style="z-index:'+(z+10)+';display:none;position:absolute"></div>';
            }
            lyr3 = $(s);

            // if we have a message, style it
            if (msg) {
                if (opts.theme) {
                    lyr3.css(themedCSS);
                    lyr3.addClass('ui-widget-content');
                }
                else
                    lyr3.css(css);
            }

            // style the overlay
            if (!opts.theme /*&& (!opts.applyPlatformOpacityRules)*/)
                lyr2.css(opts.overlayCSS);
            lyr2.css('position', full ? 'fixed' : 'absolute');

            // make iframe layer transparent in IE
            if (msie || opts.forceIframe)
                lyr1.css('opacity',0.0);

            //$([lyr1[0],lyr2[0],lyr3[0]]).appendTo(full ? 'body' : el);
            var layers = [lyr1,lyr2,lyr3], $par = full ? $('body') : $(el);
            $.each(layers, function() {
                this.appendTo($par);
            });

            if (opts.theme && opts.draggable && $.fn.draggable) {
                lyr3.draggable({
                    handle: '.ui-dialog-titlebar',
                    cancel: 'li'
                });
            }

            // ie7 must use absolute positioning in quirks mode and to account for activex issues (when scrolling)
            var expr = setExpr && (!$.support.boxModel || $('object,embed', full ? null : el).length > 0);
            if (ie6 || expr) {
                // give body 100% height
                if (full && opts.allowBodyStretch && $.support.boxModel)
                    $('html,body').css('height','100%');

                // fix ie6 issue when blocked element has a border width
                if ((ie6 || !$.support.boxModel) && !full) {
                    var t = sz(el,'borderTopWidth'), l = sz(el,'borderLeftWidth');
                    var fixT = t ? '(0 - '+t+')' : 0;
                    var fixL = l ? '(0 - '+l+')' : 0;
                }

                // simulate fixed position
                $.each(layers, function(i,o) {
                    var s = o[0].style;
                    s.position = 'absolute';
                    if (i < 2) {
                        if (full)
                            s.setExpression('height','Math.max(document.body.scrollHeight, document.body.offsetHeight) - (jQuery.support.boxModel?0:'+opts.quirksmodeOffsetHack+') + "px"');
                        else
                            s.setExpression('height','this.parentNode.offsetHeight + "px"');
                        if (full)
                            s.setExpression('width','jQuery.support.boxModel && document.documentElement.clientWidth || document.body.clientWidth + "px"');
                        else
                            s.setExpression('width','this.parentNode.offsetWidth + "px"');
                        if (fixL) s.setExpression('left', fixL);
                        if (fixT) s.setExpression('top', fixT);
                    }
                    else if (opts.centerY) {
                        if (full) s.setExpression('top','(document.documentElement.clientHeight || document.body.clientHeight) / 2 - (this.offsetHeight / 2) + (blah = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + "px"');
                        s.marginTop = 0;
                    }
                    else if (!opts.centerY && full) {
                        var top = (opts.css && opts.css.top) ? parseInt(opts.css.top, 10) : 0;
                        var expression = '((document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + '+top+') + "px"';
                        s.setExpression('top',expression);
                    }
                });
            }

            // show the message
            if (msg) {
                if (opts.theme)
                    lyr3.find('.ui-widget-content').append(msg);
                else
                    lyr3.append(msg);
                if (msg.jquery || msg.nodeType)
                    $(msg).show();
            }

            if ((msie || opts.forceIframe) && opts.showOverlay)
                lyr1.show(); // opacity is zero
            if (opts.fadeIn) {
                var cb = opts.onBlock ? opts.onBlock : noOp;
                var cb1 = (opts.showOverlay && !msg) ? cb : noOp;
                var cb2 = msg ? cb : noOp;
                if (opts.showOverlay)
                    lyr2._fadeIn(opts.fadeIn, cb1);
                if (msg)
                    lyr3._fadeIn(opts.fadeIn, cb2);
            }
            else {
                if (opts.showOverlay)
                    lyr2.show();
                if (msg)
                    lyr3.show();
                if (opts.onBlock)
                    opts.onBlock();
            }

            // bind key and mouse events
            bind(1, el, opts);

            if (full) {
                pageBlock = lyr3[0];
                pageBlockEls = $(opts.focusableElements,pageBlock);
                if (opts.focusInput)
                    setTimeout(focus, 20);
            }
            else
                center(lyr3[0], opts.centerX, opts.centerY);

            if (opts.timeout) {
                // auto-unblock
                var to = setTimeout(function() {
                    if (full)
                        $.unblockUI(opts);
                    else
                        $(el).unblock(opts);
                }, opts.timeout);
                $(el).data('blockUI.timeout', to);
            }
        }

        // remove the block
        function remove(el, opts) {
            var count;
            var full = (el == window);
            var $el = $(el);
            var data = $el.data('blockUI.history');
            var to = $el.data('blockUI.timeout');
            if (to) {
                clearTimeout(to);
                $el.removeData('blockUI.timeout');
            }
            opts = $.extend({}, $.blockUI.defaults, opts || {});
            bind(0, el, opts); // unbind events

            if (opts.onUnblock === null) {
                opts.onUnblock = $el.data('blockUI.onUnblock');
                $el.removeData('blockUI.onUnblock');
            }

            var els;
            if (full) // crazy selector to handle odd field errors in ie6/7
                els = $('body').children().filter('.blockUI').add('body > .blockUI');
            else
                els = $el.find('>.blockUI');

            // fix cursor issue
            if ( opts.cursorReset ) {
                if ( els.length > 1 )
                    els[1].style.cursor = opts.cursorReset;
                if ( els.length > 2 )
                    els[2].style.cursor = opts.cursorReset;
            }

            if (full)
                pageBlock = pageBlockEls = null;

            if (opts.fadeOut) {
                count = els.length;
                els.stop().fadeOut(opts.fadeOut, function() {
                    if ( --count === 0)
                        reset(els,data,opts,el);
                });
            }
            else
                reset(els, data, opts, el);
        }

        // move blocking element back into the DOM where it started
        function reset(els,data,opts,el) {
            var $el = $(el);
            if ( $el.data('blockUI.isBlocked') )
                return;

            els.each(function(i,o) {
                // remove via DOM calls so we don't lose event handlers
                if (this.parentNode)
                    this.parentNode.removeChild(this);
            });

            if (data && data.el) {
                data.el.style.display = data.display;
                data.el.style.position = data.position;
                if (data.parent)
                    data.parent.appendChild(data.el);
                $el.removeData('blockUI.history');
            }

            if ($el.data('blockUI.static')) {
                $el.css('position', 'static'); // #22
            }

            if (typeof opts.onUnblock == 'function')
                opts.onUnblock(el,opts);

            // fix issue in Safari 6 where block artifacts remain until reflow
            var body = $(document.body), w = body.width(), cssW = body[0].style.width;
            body.width(w-1).width(w);
            body[0].style.width = cssW;
        }

        // bind/unbind the handler
        function bind(b, el, opts) {
            var full = el == window, $el = $(el);

            // don't bother unbinding if there is nothing to unbind
            if (!b && (full && !pageBlock || !full && !$el.data('blockUI.isBlocked')))
                return;

            $el.data('blockUI.isBlocked', b);

            // don't bind events when overlay is not in use or if bindEvents is false
            if (!full || !opts.bindEvents || (b && !opts.showOverlay))
                return;

            // bind anchors and inputs for mouse and key events
            var events = 'mousedown mouseup keydown keypress keyup touchstart touchend touchmove';
            if (b)
                $(document).bind(events, opts, handler);
            else
                $(document).unbind(events, handler);

            // former impl...
            //		var $e = $('a,:input');
            //		b ? $e.bind(events, opts, handler) : $e.unbind(events, handler);
        }

        // event handler to suppress keyboard/mouse events when blocking
        function handler(e) {
            // allow tab navigation (conditionally)
            if (e.type === 'keydown' && e.keyCode && e.keyCode == 9) {
                if (pageBlock && e.data.constrainTabKey) {
                    var els = pageBlockEls;
                    var fwd = !e.shiftKey && e.target === els[els.length-1];
                    var back = e.shiftKey && e.target === els[0];
                    if (fwd || back) {
                        setTimeout(function(){focus(back);},10);
                        return false;
                    }
                }
            }
            var opts = e.data;
            var target = $(e.target);
            if (target.hasClass('blockOverlay') && opts.onOverlayClick)
                opts.onOverlayClick(e);

            // allow events within the message content
            if (target.parents('div.' + opts.blockMsgClass).length > 0)
                return true;

            // allow events for content that is not being blocked
            return target.parents().children().filter('div.blockUI').length === 0;
        }

        function focus(back) {
            if (!pageBlockEls)
                return;
            var e = pageBlockEls[back===true ? pageBlockEls.length-1 : 0];
            if (e)
                e.focus();
        }

        function center(el, x, y) {
            var p = el.parentNode, s = el.style;
            var l = ((p.offsetWidth - el.offsetWidth)/2) - sz(p,'borderLeftWidth');
            var t = ((p.offsetHeight - el.offsetHeight)/2) - sz(p,'borderTopWidth');
            if (x) s.left = l > 0 ? (l+'px') : '0';
            if (y) s.top  = t > 0 ? (t+'px') : '0';
        }

        function sz(el, p) {
            return parseInt($.css(el,p),10)||0;
        }

    }


    /*global define:true */
    if (typeof define === 'function' && define.amd && define.amd.jQuery) {
        define(['jquery'], setup);
    } else {
        setup(jQuery);
    }

})();

/*!
 * Retina.js v1.3.0
 *
 * Copyright 2014 Imulus, LLC
 * Released under the MIT license
 *
 * Retina.js is an open source script that makes it easy to serve
 * high-resolution images to devices with retina displays.
 */

(function() {
    var root = (typeof exports === 'undefined' ? window : exports);
    var config = {
        // An option to choose a suffix for 2x images
        retinaImageSuffix : '@2x',

        // Ensure Content-Type is an image before trying to load @2x image
        // https://github.com/imulus/retinajs/pull/45)
        check_mime_type: true,

        // Resize high-resolution images to original image's pixel dimensions
        // https://github.com/imulus/retinajs/issues/8
        force_original_dimensions: false,
        isApplyRetina: function(img) {
            return !!(img.src.match(/wp-content\/uploads/i) && img.src.match('//' + document.domain) );
        }
    };

    function Retina() {}

    root.Retina = Retina;

    Retina.configure = function(options) {
        if (options === null) {
            options = {};
        }

        for (var prop in options) {
            if (options.hasOwnProperty(prop)) {
                config[prop] = options[prop];
            }
        }
    };

    Retina.init = function(context) {
        if (context === null) {
            context = root;
        }

        var existing_onload = context.onload || function(){};

        context.onload = function() {
            var images = document.getElementsByTagName('img'), retinaImages = [], i, image;
            for (i = 0; i < images.length; i += 1) {
                image = images[i];
                if (root.Retina.isApplyRetina(image)) {
                    retinaImages.push(new RetinaImage(image));
                }
            }
            existing_onload();
        };
    };

    Retina.isApplyRetina = typeof config['isApplyRetina'] !== 'undefined' ? config['isApplyRetina'] :  function (image) {
        return !!!image.getAttributeNode('data-no-retina');
    };

    Retina.isRetina = typeof config['isRetina'] !== 'undefined' ? config['isRetina'] : function(){
        var mediaQuery = '(-webkit-min-device-pixel-ratio: 1.5), (min--moz-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3/2), (min-resolution: 1.5dppx)';

        if (root.devicePixelRatio > 1) {
            return true;
        }

        if (root.matchMedia && root.matchMedia(mediaQuery).matches) {
            return true;
        }

        return false;
    };


    var regexMatch = /\.\w+$/;
    function suffixReplace (match) {
        return config.retinaImageSuffix + match;
    }

    function RetinaImagePath(path, at_2x_path) {
        this.path = path || '';
        if (typeof at_2x_path !== 'undefined' && at_2x_path !== null) {
            this.at_2x_path = at_2x_path;
            this.perform_check = false;
        } else {
            if (undefined !== document.createElement) {
                var locationObject = document.createElement('a');
                locationObject.href = this.path;
                locationObject.pathname = locationObject.pathname.replace(regexMatch, suffixReplace);
                this.at_2x_path = locationObject.href;
            } else {
                var parts = this.path.split('?');
                parts[0] = parts[0].replace(regexMatch, suffixReplace);
                this.at_2x_path = parts.join('?');
            }
            this.perform_check = true;
        }
    }

    root.RetinaImagePath = RetinaImagePath;

    RetinaImagePath.confirmed_paths = [];

    RetinaImagePath.prototype.is_external = function() {
        return !!(this.path.match(/^https?\:/i) && !this.path.match('//' + document.domain) );
    };

    RetinaImagePath.prototype.check_2x_variant = function(callback) {
        var http, that = this;
        if (this.is_external()) {
            return callback(false);
        } else if (!this.perform_check && typeof this.at_2x_path !== 'undefined' && this.at_2x_path !== null) {
            return callback(true);
        } else if (this.at_2x_path in RetinaImagePath.confirmed_paths) {
            return callback(true);
        } else {
            http = new XMLHttpRequest();
            http.open('HEAD', this.at_2x_path);
            http.onreadystatechange = function() {
                if (http.readyState !== 4) {
                    return callback(false);
                }

                if (http.status >= 200 && http.status <= 399) {
                    if (config.check_mime_type) {
                        var type = http.getResponseHeader('Content-Type');
                        if (type === null || !type.match(/^image/i)) {
                            return callback(false);
                        }
                    }

                    RetinaImagePath.confirmed_paths.push(that.at_2x_path);
                    return callback(true);
                } else {
                    return callback(false);
                }
            };
            http.send();
        }
    };


    function RetinaImage(el) {
        this.el = el;
        this.path = new RetinaImagePath(this.el.getAttribute('src'), this.el.getAttribute('data-at2x'));
        var that = this;
        this.path.check_2x_variant(function(hasVariant) {
            if (hasVariant) {
                that.swap();
            }
        });
    }

    root.RetinaImage = RetinaImage;

    RetinaImage.prototype.swap = function(path) {
        if (typeof path === 'undefined') {
            path = this.path.at_2x_path;
        }

        var that = this;
        function load() {
            if (! that.el.complete) {
                setTimeout(load, 5);
            } else {
                if (config.force_original_dimensions) {
                    that.el.setAttribute('width', that.el.offsetWidth);
                    that.el.setAttribute('height', that.el.offsetHeight);
                }

                that.el.setAttribute('src', path);
            }
        }
        load();
    };

    // ThienLD : auto trigger Retina init
    jQuery(document).ready(function () {
        if (typeof Retina !== 'undefined' && Retina.isRetina()) {
            Retina.init(null);
        }    
    });
})();

/**
 * simplePagination.js v1.6
 * A simple jQuery pagination plugin.
 * http://flaviusmatis.github.com/simplePagination.js/
 *
 * Copyright 2012, Flavius Matis
 * Released under the MIT license.
 * http://flaviusmatis.github.com/license.html
 */

(function($){

    var methods = {
        init: function(options) {
            var o = $.extend({
                items: 1,
                itemsOnPage: 1,
                pages: 0,
                displayedPages: 5,
                edges: 2,
                currentPage: 0,
                hrefTextPrefix: '#page-',
                hrefTextSuffix: '',
                prevText: 'Prev',
                nextText: 'Next',
                ellipseText: '&hellip;',
                cssStyle: 'light-theme',
                labelMap: [],
                selectOnClick: true,
                nextAtFront: false,
                invertPageOrder: false,
                useStartEdge : true,
                useEndEdge : true,
                onPageClick: function(pageNumber, event) {
                    // Callback triggered when a page is clicked
                    // Page number is given as an optional parameter
                },
                onInit: function() {
                    // Callback triggered immediately after initialization
                }
            }, options || {});

            var self = this;

            o.pages = o.pages ? o.pages : Math.ceil(o.items / o.itemsOnPage) ? Math.ceil(o.items / o.itemsOnPage) : 1;
            if (o.currentPage)
                o.currentPage = o.currentPage - 1;
            else
                o.currentPage = !o.invertPageOrder ? 0 : o.pages - 1;
            o.halfDisplayed = o.displayedPages / 2;

            this.each(function() {
                self.addClass(o.cssStyle + ' simple-pagination').data('pagination', o);
                methods._draw.call(self);
            });

            o.onInit();

            return this;
        },

        selectPage: function(page) {
            methods._selectPage.call(this, page - 1);
            return this;
        },

        prevPage: function() {
            var o = this.data('pagination');
            if (!o.invertPageOrder) {
                if (o.currentPage > 0) {
                    methods._selectPage.call(this, o.currentPage - 1);
                }
            } else {
                if (o.currentPage < o.pages - 1) {
                    methods._selectPage.call(this, o.currentPage + 1);
                }
            }
            return this;
        },

        nextPage: function() {
            var o = this.data('pagination');
            if (!o.invertPageOrder) {
                if (o.currentPage < o.pages - 1) {
                    methods._selectPage.call(this, o.currentPage + 1);
                }
            } else {
                if (o.currentPage > 0) {
                    methods._selectPage.call(this, o.currentPage - 1);
                }
            }
            return this;
        },

        getPagesCount: function() {
            return this.data('pagination').pages;
        },

        getCurrentPage: function () {
            return this.data('pagination').currentPage + 1;
        },

        destroy: function(){
            this.empty();
            return this;
        },

        drawPage: function (page) {
            var o = this.data('pagination');
            o.currentPage = page - 1;
            this.data('pagination', o);
            methods._draw.call(this);
            return this;
        },

        redraw: function(){
            methods._draw.call(this);
            return this;
        },

        disable: function(){
            var o = this.data('pagination');
            o.disabled = true;
            this.data('pagination', o);
            methods._draw.call(this);
            return this;
        },

        enable: function(){
            var o = this.data('pagination');
            o.disabled = false;
            this.data('pagination', o);
            methods._draw.call(this);
            return this;
        },

        updateItems: function (newItems) {
            var o = this.data('pagination');
            o.items = newItems;
            o.pages = methods._getPages(o);
            this.data('pagination', o);
            methods._draw.call(this);
        },

        updateItemsOnPage: function (itemsOnPage) {
            var o = this.data('pagination');
            o.itemsOnPage = itemsOnPage;
            o.pages = methods._getPages(o);
            this.data('pagination', o);
            methods._selectPage.call(this, 0);
            return this;
        },

        _draw: function() {
            var	o = this.data('pagination'),
                interval = methods._getInterval(o),
                i,
                tagName;

            methods.destroy.call(this);

            tagName = (typeof this.prop === 'function') ? this.prop('tagName') : this.attr('tagName');

            var $panel = this;
            if(o.heading) {
                $(o.heading).appendTo(this);
            }


            if(o.ul === undefined) {
                o.ul = "ul";
            }

            var _ulclass = o.ulclass !== undefined ? "class='" + o.ulclass + "'" : '';
            $panel = $('<' + o.ul +' '+ _ulclass + ' >' + '</' + o.ul + '>').appendTo(this);

            // Generate Prev link
            if (o.prevText) {
                methods._appendItem.call(this, !o.invertPageOrder ? o.currentPage - 1 : o.currentPage + 1, {text: o.prevText, classes: 'prev'});
            }

            // Generate Next link (if option set for at front)
            if (o.nextText && o.nextAtFront) {
                methods._appendItem.call(this, !o.invertPageOrder ? o.currentPage + 1 : o.currentPage - 1, {text: o.nextText, classes: 'next'});
            }

            // Generate start edges
            if (!o.invertPageOrder) {
                if (interval.start > 0 && o.edges > 0) {
                    if(o.useStartEdge) {
                        var end = Math.min(o.edges, interval.start);
                        for (i = 0; i < end; i++) {
                            methods._appendItem.call(this, i);
                        }
                    }
                    if (o.edges < interval.start && (interval.start - o.edges != 1)) {
                        if(o.ellipse) {
                            $panel.append(o.ellipse);
                        }
                        else {
                            $panel.append('<li class="disabled"><span class="ellipse">' + o.ellipseText + '</span></li>');
                        }

                    } else if (interval.start - o.edges == 1) {
                        methods._appendItem.call(this, o.edges);
                    }
                }
            } else {
                if (interval.end < o.pages && o.edges > 0) {
                    if(o.useStartEdge) {
                        var begin = Math.max(o.pages - o.edges, interval.end);
                        for (i = o.pages - 1; i >= begin; i--) {
                            methods._appendItem.call(this, i);
                        }
                    }

                    if (o.pages - o.edges > interval.end && (o.pages - o.edges - interval.end != 1)) {
                        if(o.ellipse) {
                            $panel.append(o.ellipse);
                        }
                        else {
                            $panel.append('<li class="disabled"><span class="ellipse">' + o.ellipseText + '</span></li>');
                        }

                    } else if (o.pages - o.edges - interval.end == 1) {
                        methods._appendItem.call(this, interval.end);
                    }
                }
            }

            // Generate interval links
            if (!o.invertPageOrder) {
                for (i = interval.start; i < interval.end; i++) {
                    methods._appendItem.call(this, i);
                }
            } else {
                for (i = interval.end - 1; i >= interval.start; i--) {
                    methods._appendItem.call(this, i);
                }
            }

            // Generate end edges
            if (!o.invertPageOrder) {
                if (interval.end < o.pages && o.edges > 0) {
                    if (o.pages - o.edges > interval.end && (o.pages - o.edges - interval.end != 1)) {
                        if(o.ellipse !== undefined) {
                            $panel.append(o.ellipse);
                        }
                        else {
                            $panel.append('<li class="disabled"><span class="ellipse">' + o.ellipseText + '</span></li>');
                        }

                    } else if (o.pages - o.edges - interval.end == 1) {
                        methods._appendItem.call(this, interval.end);
                    }
                    if(o.useEndEdge) {
                        var begin = Math.max(o.pages - o.edges, interval.end);
                        for (i = begin; i < o.pages; i++) {
                            methods._appendItem.call(this, i);
                        }
                    }
                }
            } else {
                if (interval.start > 0 && o.edges > 0) {
                    if (o.edges < interval.start && (interval.start - o.edges != 1)) {
                        if(o.ellipse !== undefined) {
                            $panel.append(o.ellipse);
                        }
                        else {
                            $panel.append('<li class="disabled"><span class="ellipse">' + o.ellipseText + '</span></li>');
                        }

                    } else if (interval.start - o.edges == 1) {
                        methods._appendItem.call(this, o.edges);
                    }

                    if(o.useEndEdge) {
                        var end = Math.min(o.edges, interval.start);
                        for (i = end - 1; i >= 0; i--) {
                            methods._appendItem.call(this, i);
                        }
                    }
                }
            }

            // Generate Next link (unless option is set for at front)
            if (o.nextText && !o.nextAtFront) {
                methods._appendItem.call(this, !o.invertPageOrder ? o.currentPage + 1 : o.currentPage - 1, {text: o.nextText, classes: 'next'});
            }
        },

        _getPages: function(o) {
            var pages = Math.ceil(o.items / o.itemsOnPage);
            return pages || 1;
        },

        _getInterval: function(o) {
            return {
                start: Math.ceil(o.currentPage > o.halfDisplayed ? Math.max(Math.min(o.currentPage - o.halfDisplayed, (o.pages - o.displayedPages)), 0) : 0),
                end: Math.ceil(o.currentPage > o.halfDisplayed ? Math.min(o.currentPage + o.halfDisplayed, o.pages) : Math.min(o.displayedPages, o.pages))
            };
        },

        _appendItem: function(pageIndex, opts) {
            var self = this, options, $link, o = self.data('pagination'), $linkWrapper = $('<li></li>'), $ul = self.find(o.ul);

            pageIndex = pageIndex < 0 ? 0 : (pageIndex < o.pages ? pageIndex : o.pages - 1);

            options = {
                text: pageIndex + 1,
                classes: ''
            };

            if (o.labelMap.length && o.labelMap[pageIndex]) {
                options.text = o.labelMap[pageIndex];
            }

            options = $.extend(options, opts || {});

            if (pageIndex == o.currentPage || o.disabled) {
                if (o.disabled) {
                    $linkWrapper.addClass('disabled');
                } else {
                    $linkWrapper.addClass('active');
                }
                if(opts !== undefined) {
                    if(opts.classes === 'prev') {
                        $link = $('<a class="page-link">' + (options.text) + '</a>');
                    }
                    else if(opts.classes === 'next') {
                        $link = $('<a class="page-link">' + (options.text) + '</a>');
                    }
                }
                else {
                    $link = $('<a class="active">' + (options.text) + '</a>');
                }

            } else {
                $link = $('<a href="' + o.hrefTextPrefix + (pageIndex + 1) + o.hrefTextSuffix + '" class="page-link">' + (options.text) + '</a>');
                $link.click(function(event){
                    return methods._selectPage.call(self, pageIndex, event);
                });
            }

            if (options.classes && opts.classes !== 'prev' && opts.classes !== 'next') {
                $link.addClass(options.classes);
            }

            $linkWrapper.append($link);

            if ($ul.length) {
                $ul.append($linkWrapper);
            } else {
                self.append($linkWrapper);
            }
        },

        _selectPage: function(pageIndex, event) {
            var o = this.data('pagination');
            o.currentPage = pageIndex;
            if (o.selectOnClick) {
                methods._draw.call(this);
            }
            return o.onPageClick(pageIndex + 1, event);
        }

    };

    $.fn.pagination = function(method) {

        console.log('z');
        // Method calling logic
        if (methods[method] && method.charAt(0) != '_') {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' +  method + ' does not exist on jQuery.pagination');
        }

    };

})(jQuery);

/**
 * alertify
 * An unobtrusive customizable JavaScript notification system
 *
 * @author Fabien Doiron <fabien.doiron@gmail.com>
 * @copyright Fabien Doiron 2013
 * @license MIT <http://opensource.org/licenses/mit-license.php>
 * @link http://fabien-d.github.com/alertify.js/
 * @module alertify
 * @version 0.3.11
 */
(function (global, undefined) {
    "use strict";
    var document = global.document,
        Alertify;

    Alertify = function () {

        var _alertify = {},
            dialogs   = {},
            isopen    = false,
            keys      = { ENTER: 13, ESC: 27, SPACE: 32 },
            queue     = [],
            $, btnCancel, btnClose, btnOK, btnReset, btnResetBack, btnFocus, elCallee, elCover, elDialog, elLog, form, input, getTransitionEvent;

        /**
         * Markup pieces
         * @type {Object}
         */
        dialogs = {
            buttons : {
                holder : "<nav class=\"alertify-buttons\">{{buttons}}</nav>",
                submit : "<button type=\"submit\" class=\"alertify-button alertify-button-ok\" id=\"alertify-ok\">{{ok}}</button>",
                ok     : "<button class=\"alertify-button alertify-button-ok\" id=\"alertify-ok\">{{ok}}</button>",
                cancel : "<button class=\"alertify-button alertify-button-cancel\" id=\"alertify-cancel\">{{cancel}}</button>"
            },
            input   : "<div class=\"alertify-text-wrapper\"><input type=\"text\" class=\"alertify-text\" id=\"alertify-text\"></div>",
            message : "<p class=\"alertify-message\">{{message}}</p>",
            log     : "<article class=\"alertify-log{{class}}\">{{message}}</article>"
        };

        /**
         * Return the proper transitionend event
         * @return {String}    Transition type string
         */
        getTransitionEvent = function () {
            var t,
                type,
                supported   = false,
                el          = document.createElement("fakeelement"),
                transitions = {
                    "WebkitTransition" : "webkitTransitionEnd",
                    "MozTransition"    : "transitionend",
                    "OTransition"      : "otransitionend",
                    "transition"       : "transitionend"
                };

            for (t in transitions) {
                if (el.style[t] !== undefined) {
                    type      = transitions[t];
                    supported = true;
                    break;
                }
            }

            return {
                type      : type,
                supported : supported
            };
        };

        /**
         * Shorthand for document.getElementById()
         *
         * @param  {String} id    A specific element ID
         * @return {Object}       HTML element
         */
        $ = function (id) {
            return document.getElementById(id);
        };

        /**
         * Alertify private object
         * @type {Object}
         */
        _alertify = {

            /**
             * Labels object
             * @type {Object}
             */
            labels : {
                ok     : "OK",
                cancel : "Cancel"
            },

            /**
             * Delay number
             * @type {Number}
             */
            delay : 5000,

            /**
             * Whether buttons are reversed (default is secondary/primary)
             * @type {Boolean}
             */
            buttonReverse : false,

            /**
             * Which button should be focused by default
             * @type {String}	"ok" (default), "cancel", or "none"
             */
            buttonFocus : "ok",

            /**
             * Set the transition event on load
             * @type {[type]}
             */
            transition : undefined,

            /**
             * Set the proper button click events
             *
             * @param {Function} fn    [Optional] Callback function
             *
             * @return {undefined}
             */
            addListeners : function (fn) {
                var hasOK     = (typeof btnOK !== "undefined"),
                    hasCancel = (typeof btnCancel !== "undefined"),
                    hasClose = (typeof btnClose !== "undefined"),
                    hasInput  = (typeof input !== "undefined"),
                    val       = "",
                    self      = this,
                    ok, cancel, common, key, reset;

                // ok event handler
                ok = function (event) {
                    if (typeof event.preventDefault !== "undefined") event.preventDefault();
                    common(event);
                    if (typeof input !== "undefined") val = input.value;
                    if (typeof fn === "function") {
                        if (typeof input !== "undefined") {
                            fn(true, val);
                        }
                        else fn(true);
                    }
                    return false;
                };

                // cancel event handler
                cancel = function (event) {
                    if (typeof event.preventDefault !== "undefined") event.preventDefault();
                    common(event);
                    if (typeof fn === "function") fn(false);
                    return false;
                };

                // common event handler (keyup, ok and cancel)
                common = function (event) {
                    self.hide();
                    self.unbind(document.body, "keyup", key);
                    self.unbind(btnReset, "focus", reset);
                    if (hasOK) self.unbind(btnOK, "click", ok);
                    if (hasCancel) self.unbind(btnCancel, "click", cancel);
                };

                // keyup handler
                key = function (event) {
                    var keyCode = event.keyCode;
                    if ((keyCode === keys.SPACE && !hasInput) || (hasInput && keyCode === keys.ENTER)) ok(event);
                    if (keyCode === keys.ESC && hasCancel) cancel(event);
                };

                // reset focus to first item in the dialog
                reset = function (event) {
                    if (hasInput) input.focus();
                    else if (!hasCancel || self.buttonReverse) btnOK.focus();
                    else btnCancel.focus();
                };

                // handle reset focus link
                // this ensures that the keyboard focus does not
                // ever leave the dialog box until an action has
                // been taken
                this.bind(btnReset, "focus", reset);
                this.bind(btnResetBack, "focus", reset);
                // handle OK click
                if (hasOK) this.bind(btnOK, "click", ok);

                // handle Close click
                if (hasClose) this.bind(btnClose, "click", cancel);

                // handle Cancel click
                if (hasCancel) this.bind(btnCancel, "click", cancel);
                // listen for keys, Cancel => ESC
                this.bind(document.body, "keyup", key);
                if (!this.transition.supported) {
                    this.setFocus();
                }
            },

            /**
             * Bind events to elements
             *
             * @param  {Object}   el       HTML Object
             * @param  {Event}    event    Event to attach to element
             * @param  {Function} fn       Callback function
             *
             * @return {undefined}
             */
            bind : function (el, event, fn) {
                if (typeof el.addEventListener === "function") {
                    el.addEventListener(event, fn, false);
                } else if (el.attachEvent) {
                    el.attachEvent("on" + event, fn);
                }
            },

            /**
             * Use alertify as the global error handler (using window.onerror)
             *
             * @return {boolean} success
             */
            handleErrors : function () {
                if (typeof global.onerror !== "undefined") {
                    var self = this;
                    global.onerror = function (msg, url, line) {
                        self.error("[" + msg + " on line " + line + " of " + url + "]", 0);
                    };
                    return true;
                } else {
                    return false;
                }
            },

            /**
             * Append button HTML strings
             *
             * @param {String} secondary    The secondary button HTML string
             * @param {String} primary      The primary button HTML string
             *
             * @return {String}             The appended button HTML strings
             */
            appendButtons : function (secondary, primary) {
                return this.buttonReverse ? primary + secondary : secondary + primary;
            },

            /**
             * Build the proper message box
             *
             * @param  {Object} item    Current object in the queue
             *
             * @return {String}         An HTML string of the message box
             */
            build : function (item) {
                var html    = "",
                    type    = item.type,
                    message = item.message,
                    css     = item.cssClass || "";

                html += "<div class=\"alertify-dialog\">";
                html += "<a id=\"alertify-resetFocusBack\" class=\"alertify-resetFocus\" href=\"#\">Reset Focus</a>";

                if (_alertify.buttonFocus === "none") html += "<a href=\"#\" id=\"alertify-noneFocus\" class=\"alertify-hidden\"></a>";

                // doens't require an actual form
                if (type === "prompt") html += "<div id=\"alertify-form\">";

                html += "<article class=\"alertify-inner\">";
                html += dialogs.message.replace("{{message}}", message);

                if (type === "prompt") html += dialogs.input;

                html += dialogs.buttons.holder;
                html += "</article>";

                if (type === "prompt") html += "</div>";

                html += "<a id=\"alertify-resetFocus\" class=\"alertify-resetFocus\" href=\"#\">Reset Focus</a>";
                html += "<div id='alertify-close' class=\"alertify-close\"><i class=\"fa fa-times fa-lg\"></i></div>";
                html += "</div>";

                switch (type) {
                    case "confirm":
                        html = html.replace("{{buttons}}", this.appendButtons(dialogs.buttons.cancel, dialogs.buttons.ok));
                        html = html.replace("{{ok}}", this.labels.ok).replace("{{cancel}}", this.labels.cancel);
                        break;
                    case "prompt":
                        html = html.replace("{{buttons}}", this.appendButtons(dialogs.buttons.cancel, dialogs.buttons.submit));
                        html = html.replace("{{ok}}", this.labels.ok).replace("{{cancel}}", this.labels.cancel);
                        break;
                    case "alert":
                        html = html.replace("{{buttons}}", dialogs.buttons.ok);
                        html = html.replace("{{ok}}", this.labels.ok);
                        break;
                    default:
                        break;
                }

                elDialog.className = "alertify alertify-" + type + " " + css;
                elCover.className  = "alertify-cover";
                return html;
            },

            /**
             * Close the log messages
             *
             * @param  {Object} elem    HTML Element of log message to close
             * @param  {Number} wait    [optional] Time (in ms) to wait before automatically hiding the message, if 0 never hide
             *
             * @return {undefined}
             */
            close : function (elem, wait) {
                // Unary Plus: +"2" === 2
                var timer = (wait && !isNaN(wait)) ? +wait : this.delay,
                    self  = this,
                    hideElement, transitionDone;

                // set click event on log messages
                this.bind(elem, "click", function () {
                    hideElement(elem);
                });
                // Hide the dialog box after transition
                // This ensure it doens't block any element from being clicked
                transitionDone = function (event) {
                    event.stopPropagation();
                    // unbind event so function only gets called once
                    self.unbind(this, self.transition.type, transitionDone);
                    // remove log message
                    elLog.removeChild(this);
                    if (!elLog.hasChildNodes()) elLog.className += " alertify-logs-hidden";
                };
                // this sets the hide class to transition out
                // or removes the child if css transitions aren't supported
                hideElement = function (el) {
                    // ensure element exists
                    if (typeof el !== "undefined" && el.parentNode === elLog) {
                        // whether CSS transition exists
                        if (self.transition.supported) {
                            self.bind(el, self.transition.type, transitionDone);
                            el.className += " alertify-log-hide";
                        } else {
                            elLog.removeChild(el);
                            if (!elLog.hasChildNodes()) elLog.className += " alertify-logs-hidden";
                        }
                    }
                };
                // never close (until click) if wait is set to 0
                if (wait === 0) return;
                // set timeout to auto close the log message
                setTimeout(function () { hideElement(elem); }, timer);
            },

            /**
             * Create a dialog box
             *
             * @param  {String}   message        The message passed from the callee
             * @param  {String}   type           Type of dialog to create
             * @param  {Function} fn             [Optional] Callback function
             * @param  {String}   placeholder    [Optional] Default value for prompt input field
             * @param  {String}   cssClass       [Optional] Class(es) to append to dialog box
             *
             * @return {Object}
             */
            dialog : function (message, type, fn, placeholder, cssClass) {
                // set the current active element
                // this allows the keyboard focus to be resetted
                // after the dialog box is closed
                elCallee = document.activeElement;
                // check to ensure the alertify dialog element
                // has been successfully created
                var check = function () {
                    if ((elLog && elLog.scrollTop !== null) && (elCover && elCover.scrollTop !== null)) return;
                    else check();
                };
                // error catching
                if (typeof message !== "string") throw new Error("message must be a string");
                if (typeof type !== "string") throw new Error("type must be a string");
                if (typeof fn !== "undefined" && typeof fn !== "function") throw new Error("fn must be a function");
                // initialize alertify if it hasn't already been done
                this.init();
                check();

                queue.push({ type: type, message: message, callback: fn, placeholder: placeholder, cssClass: cssClass });
                if (!isopen) this.setup();

                return this;
            },

            /**
             * Extend the log method to create custom methods
             *
             * @param  {String} type    Custom method name
             *
             * @return {Function}
             */
            extend : function (type) {
                if (typeof type !== "string") throw new Error("extend method must have exactly one paramter");
                return function (message, wait) {
                    this.log(message, type, wait);
                    return this;
                };
            },

            /**
             * Hide the dialog and rest to defaults
             *
             * @return {undefined}
             */
            hide : function () {
                var transitionDone,
                    self = this;
                // remove reference from queue
                queue.splice(0,1);
                // if items remaining in the queue
                if (queue.length > 0) this.setup(true);
                else {
                    isopen = false;
                    // Hide the dialog box after transition
                    // This ensure it doens't block any element from being clicked
                    transitionDone = function (event) {
                        event.stopPropagation();
                        // unbind event so function only gets called once
                        self.unbind(elDialog, self.transition.type, transitionDone);
                    };
                    // whether CSS transition exists
                    if (this.transition.supported) {
                        this.bind(elDialog, this.transition.type, transitionDone);
                        elDialog.className = "alertify alertify-hide alertify-hidden";
                    } else {
                        elDialog.className = "alertify alertify-hide alertify-hidden alertify-isHidden";
                    }
                    elCover.className  = "alertify-cover alertify-cover-hidden";
                    // set focus to the last element or body
                    // after the dialog is closed
                    elCallee.focus();
                }
            },

            /**
             * Initialize Alertify
             * Create the 2 main elements
             *
             * @return {undefined}
             */
            init : function () {
                // ensure legacy browsers support html5 tags
                document.createElement("nav");
                document.createElement("article");
                document.createElement("section");
                // cover
                if ($("alertify-cover") == null) {
                    elCover = document.createElement("div");
                    elCover.setAttribute("id", "alertify-cover");
                    elCover.className = "alertify-cover alertify-cover-hidden";
                    document.body.appendChild(elCover);
                }
                // main element
                if ($("alertify") == null) {
                    isopen = false;
                    queue = [];
                    elDialog = document.createElement("section");
                    elDialog.setAttribute("id", "alertify");
                    elDialog.className = "alertify alertify-hidden";
                    document.body.appendChild(elDialog);
                }
                // log element
                if ($("alertify-logs") == null) {
                    elLog = document.createElement("section");
                    elLog.setAttribute("id", "alertify-logs");
                    elLog.className = "alertify-logs alertify-logs-hidden";
                    document.body.appendChild(elLog);
                }
                // set tabindex attribute on body element
                // this allows script to give it focus
                // after the dialog is closed
                document.body.setAttribute("tabindex", "0");
                // set transition type
                this.transition = getTransitionEvent();
            },

            /**
             * Show a new log message box
             *
             * @param  {String} message    The message passed from the callee
             * @param  {String} type       [Optional] Optional type of log message
             * @param  {Number} wait       [Optional] Time (in ms) to wait before auto-hiding the log
             *
             * @return {Object}
             */
            log : function (message, type, wait) {
                // check to ensure the alertify dialog element
                // has been successfully created
                var check = function () {
                    if (elLog && elLog.scrollTop !== null) return;
                    else check();
                };
                // initialize alertify if it hasn't already been done
                this.init();
                check();

                elLog.className = "alertify-logs";
                this.notify(message, type, wait);
                return this;
            },

            /**
             * Add new log message
             * If a type is passed, a class name "alertify-log-{type}" will get added.
             * This allows for custom look and feel for various types of notifications.
             *
             * @param  {String} message    The message passed from the callee
             * @param  {String} type       [Optional] Type of log message
             * @param  {Number} wait       [Optional] Time (in ms) to wait before auto-hiding
             *
             * @return {undefined}
             */
            notify : function (message, type, wait) {
                var log = document.createElement("article");
                log.className = "alertify-log" + ((typeof type === "string" && type !== "") ? " alertify-log-" + type : "");
                log.innerHTML = message;
                // append child
                elLog.appendChild(log);
                // triggers the CSS animation
                setTimeout(function() { log.className = log.className + " alertify-log-show"; }, 50);
                this.close(log, wait);
            },

            /**
             * Set properties
             *
             * @param {Object} args     Passing parameters
             *
             * @return {undefined}
             */
            set : function (args) {
                var k;
                // error catching
                if (typeof args !== "object" && args instanceof Array) throw new Error("args must be an object");
                // set parameters
                for (k in args) {
                    if (args.hasOwnProperty(k)) {
                        this[k] = args[k];
                    }
                }
            },

            /**
             * Common place to set focus to proper element
             *
             * @return {undefined}
             */
            setFocus : function () {
                if (input) {
                    input.focus();
                    input.select();
                }
                else btnFocus.focus();
            },

            /**
             * Initiate all the required pieces for the dialog box
             *
             * @return {undefined}
             */
            setup : function (fromQueue) {
                var item = queue[0],
                    self = this,
                    transitionDone;

                // dialog is open
                isopen = true;
                // Set button focus after transition
                transitionDone = function (event) {
                    event.stopPropagation();
                    self.setFocus();
                    // unbind event so function only gets called once
                    self.unbind(elDialog, self.transition.type, transitionDone);
                };
                // whether CSS transition exists
                if (this.transition.supported && !fromQueue) {
                    this.bind(elDialog, this.transition.type, transitionDone);
                }
                // build the proper dialog HTML
                elDialog.innerHTML = this.build(item);
                // assign all the common elements
                btnReset  = $("alertify-resetFocus");
                btnResetBack  = $("alertify-resetFocusBack");
                btnOK     = $("alertify-ok")     || undefined;
                btnClose     = $("alertify-close")     || undefined;
                btnCancel = $("alertify-cancel") || undefined;
                btnFocus  = (_alertify.buttonFocus === "cancel") ? btnCancel : ((_alertify.buttonFocus === "none") ? $("alertify-noneFocus") : btnOK),
                    input     = $("alertify-text")   || undefined;
                form      = $("alertify-form")   || undefined;
                // add placeholder value to the input field
                if (typeof item.placeholder === "string" && item.placeholder !== "") input.value = item.placeholder;
                if (fromQueue) this.setFocus();
                this.addListeners(item.callback);
            },

            /**
             * Unbind events to elements
             *
             * @param  {Object}   el       HTML Object
             * @param  {Event}    event    Event to detach to element
             * @param  {Function} fn       Callback function
             *
             * @return {undefined}
             */
            unbind : function (el, event, fn) {
                if (typeof el.removeEventListener === "function") {
                    el.removeEventListener(event, fn, false);
                } else if (el.detachEvent) {
                    el.detachEvent("on" + event, fn);
                }
            }
        };

        return {
            alert   : function (message, fn, cssClass) { _alertify.dialog(message, "alert", fn, "", cssClass); return this; },
            confirm : function (message, fn, cssClass) { _alertify.dialog(message, "confirm", fn, "", cssClass); return this; },
            extend  : _alertify.extend,
            init    : _alertify.init,
            log     : function (message, type, wait) { _alertify.log(message, type, wait); return this; },
            prompt  : function (message, fn, placeholder, cssClass) { _alertify.dialog(message, "prompt", fn, placeholder, cssClass); return this; },
            success : function (message, wait) { _alertify.log(message, "success", wait); return this; },
            error   : function (message, wait) { _alertify.log(message, "error", wait); return this; },
            set     : function (args) { _alertify.set(args); },
            labels  : _alertify.labels,
            debug   : _alertify.handleErrors
        };
    };

    // AMD and window support
    if (typeof define === "function") {
        define([], function () { return new Alertify(); });
    } else if (typeof global.alertify === "undefined") {
        global.alertify = new Alertify();
    }

}(this));


(function() { if (window.define) { var define = window.define; } if (window.require) { var require = window.require; } if (window.jQuery && jQuery.fn && jQuery.fn.select2 && jQuery.fn.select2.amd) { var define = jQuery.fn.select2.amd.define; var require = jQuery.fn.select2.amd.require; }/**
 * @license almond 0.2.9 Copyright (c) 2011-2014, The Dojo Foundation All Rights Reserved.
 * Available via the MIT or new BSD license.
 * see: http://github.com/jrburke/almond for details
 */
//Going sloppy to avoid 'use strict' string cost, but strict practices should
//be followed.
    /*jslint sloppy: true */
    /*global setTimeout: false */

    var requirejs, require, define;
    (function (undef) {
        var main, req, makeMap, handlers,
            defined = {},
            waiting = {},
            config = {},
            defining = {},
            hasOwn = Object.prototype.hasOwnProperty,
            aps = [].slice,
            jsSuffixRegExp = /\.js$/;

        function hasProp(obj, prop) {
            return hasOwn.call(obj, prop);
        }

        /**
         * Given a relative module name, like ./something, normalize it to
         * a real name that can be mapped to a path.
         * @param {String} name the relative name
         * @param {String} baseName a real name that the name arg is relative
         * to.
         * @returns {String} normalized name
         */
        function normalize(name, baseName) {
            var nameParts, nameSegment, mapValue, foundMap, lastIndex,
                foundI, foundStarMap, starI, i, j, part,
                baseParts = baseName && baseName.split("/"),
                map = config.map,
                starMap = (map && map['*']) || {};

            //Adjust any relative paths.
            if (name && name.charAt(0) === ".") {
                //If have a base name, try to normalize against it,
                //otherwise, assume it is a top-level require that will
                //be relative to baseUrl in the end.
                if (baseName) {
                    //Convert baseName to array, and lop off the last part,
                    //so that . matches that "directory" and not name of the baseName's
                    //module. For instance, baseName of "one/two/three", maps to
                    //"one/two/three.js", but we want the directory, "one/two" for
                    //this normalization.
                    baseParts = baseParts.slice(0, baseParts.length - 1);
                    name = name.split('/');
                    lastIndex = name.length - 1;

                    // Node .js allowance:
                    if (config.nodeIdCompat && jsSuffixRegExp.test(name[lastIndex])) {
                        name[lastIndex] = name[lastIndex].replace(jsSuffixRegExp, '');
                    }

                    name = baseParts.concat(name);

                    //start trimDots
                    for (i = 0; i < name.length; i += 1) {
                        part = name[i];
                        if (part === ".") {
                            name.splice(i, 1);
                            i -= 1;
                        } else if (part === "..") {
                            if (i === 1 && (name[2] === '..' || name[0] === '..')) {
                                //End of the line. Keep at least one non-dot
                                //path segment at the front so it can be mapped
                                //correctly to disk. Otherwise, there is likely
                                //no path mapping for a path starting with '..'.
                                //This can still fail, but catches the most reasonable
                                //uses of ..
                                break;
                            } else if (i > 0) {
                                name.splice(i - 1, 2);
                                i -= 2;
                            }
                        }
                    }
                    //end trimDots

                    name = name.join("/");
                } else if (name.indexOf('./') === 0) {
                    // No baseName, so this is ID is resolved relative
                    // to baseUrl, pull off the leading dot.
                    name = name.substring(2);
                }
            }

            //Apply map config if available.
            if ((baseParts || starMap) && map) {
                nameParts = name.split('/');

                for (i = nameParts.length; i > 0; i -= 1) {
                    nameSegment = nameParts.slice(0, i).join("/");

                    if (baseParts) {
                        //Find the longest baseName segment match in the config.
                        //So, do joins on the biggest to smallest lengths of baseParts.
                        for (j = baseParts.length; j > 0; j -= 1) {
                            mapValue = map[baseParts.slice(0, j).join('/')];

                            //baseName segment has  config, find if it has one for
                            //this name.
                            if (mapValue) {
                                mapValue = mapValue[nameSegment];
                                if (mapValue) {
                                    //Match, update name to the new value.
                                    foundMap = mapValue;
                                    foundI = i;
                                    break;
                                }
                            }
                        }
                    }

                    if (foundMap) {
                        break;
                    }

                    //Check for a star map match, but just hold on to it,
                    //if there is a shorter segment match later in a matching
                    //config, then favor over this star map.
                    if (!foundStarMap && starMap && starMap[nameSegment]) {
                        foundStarMap = starMap[nameSegment];
                        starI = i;
                    }
                }

                if (!foundMap && foundStarMap) {
                    foundMap = foundStarMap;
                    foundI = starI;
                }

                if (foundMap) {
                    nameParts.splice(0, foundI, foundMap);
                    name = nameParts.join('/');
                }
            }

            return name;
        }

        function makeRequire(relName, forceSync) {
            return function () {
                //A version of a require function that passes a moduleName
                //value for items that may need to
                //look up paths relative to the moduleName
                return req.apply(undef, aps.call(arguments, 0).concat([relName, forceSync]));
            };
        }

        function makeNormalize(relName) {
            return function (name) {
                return normalize(name, relName);
            };
        }

        function makeLoad(depName) {
            return function (value) {
                defined[depName] = value;
            };
        }

        function callDep(name) {
            if (hasProp(waiting, name)) {
                var args = waiting[name];
                delete waiting[name];
                defining[name] = true;
                main.apply(undef, args);
            }

            if (!hasProp(defined, name) && !hasProp(defining, name)) {
                throw new Error('No ' + name);
            }
            return defined[name];
        }

        //Turns a plugin!resource to [plugin, resource]
        //with the plugin being undefined if the name
        //did not have a plugin prefix.
        function splitPrefix(name) {
            var prefix,
                index = name ? name.indexOf('!') : -1;
            if (index > -1) {
                prefix = name.substring(0, index);
                name = name.substring(index + 1, name.length);
            }
            return [prefix, name];
        }

        /**
         * Makes a name map, normalizing the name, and using a plugin
         * for normalization if necessary. Grabs a ref to plugin
         * too, as an optimization.
         */
        makeMap = function (name, relName) {
            var plugin,
                parts = splitPrefix(name),
                prefix = parts[0];

            name = parts[1];

            if (prefix) {
                prefix = normalize(prefix, relName);
                plugin = callDep(prefix);
            }

            //Normalize according
            if (prefix) {
                if (plugin && plugin.normalize) {
                    name = plugin.normalize(name, makeNormalize(relName));
                } else {
                    name = normalize(name, relName);
                }
            } else {
                name = normalize(name, relName);
                parts = splitPrefix(name);
                prefix = parts[0];
                name = parts[1];
                if (prefix) {
                    plugin = callDep(prefix);
                }
            }

            //Using ridiculous property names for space reasons
            return {
                f: prefix ? prefix + '!' + name : name, //fullName
                n: name,
                pr: prefix,
                p: plugin
            };
        };

        function makeConfig(name) {
            return function () {
                return (config && config.config && config.config[name]) || {};
            };
        }

        handlers = {
            require: function (name) {
                return makeRequire(name);
            },
            exports: function (name) {
                var e = defined[name];
                if (typeof e !== 'undefined') {
                    return e;
                } else {
                    return (defined[name] = {});
                }
            },
            module: function (name) {
                return {
                    id: name,
                    uri: '',
                    exports: defined[name],
                    config: makeConfig(name)
                };
            }
        };

        main = function (name, deps, callback, relName) {
            var cjsModule, depName, ret, map, i,
                args = [],
                callbackType = typeof callback,
                usingExports;

            //Use name if no relName
            relName = relName || name;

            //Call the callback to define the module, if necessary.
            if (callbackType === 'undefined' || callbackType === 'function') {
                //Pull out the defined dependencies and pass the ordered
                //values to the callback.
                //Default to [require, exports, module] if no deps
                deps = !deps.length && callback.length ? ['require', 'exports', 'module'] : deps;
                for (i = 0; i < deps.length; i += 1) {
                    map = makeMap(deps[i], relName);
                    depName = map.f;

                    //Fast path CommonJS standard dependencies.
                    if (depName === "require") {
                        args[i] = handlers.require(name);
                    } else if (depName === "exports") {
                        //CommonJS module spec 1.1
                        args[i] = handlers.exports(name);
                        usingExports = true;
                    } else if (depName === "module") {
                        //CommonJS module spec 1.1
                        cjsModule = args[i] = handlers.module(name);
                    } else if (hasProp(defined, depName) ||
                        hasProp(waiting, depName) ||
                        hasProp(defining, depName)) {
                        args[i] = callDep(depName);
                    } else if (map.p) {
                        map.p.load(map.n, makeRequire(relName, true), makeLoad(depName), {});
                        args[i] = defined[depName];
                    } else {
                        throw new Error(name + ' missing ' + depName);
                    }
                }

                ret = callback ? callback.apply(defined[name], args) : undefined;

                if (name) {
                    //If setting exports via "module" is in play,
                    //favor that over return value and exports. After that,
                    //favor a non-undefined return value over exports use.
                    if (cjsModule && cjsModule.exports !== undef &&
                        cjsModule.exports !== defined[name]) {
                        defined[name] = cjsModule.exports;
                    } else if (ret !== undef || !usingExports) {
                        //Use the return value from the function.
                        defined[name] = ret;
                    }
                }
            } else if (name) {
                //May just be an object definition for the module. Only
                //worry about defining if have a module name.
                defined[name] = callback;
            }
        };

        requirejs = require = req = function (deps, callback, relName, forceSync, alt) {
            if (typeof deps === "string") {
                if (handlers[deps]) {
                    //callback in this case is really relName
                    return handlers[deps](callback);
                }
                //Just return the module wanted. In this scenario, the
                //deps arg is the module name, and second arg (if passed)
                //is just the relName.
                //Normalize module name, if it contains . or ..
                return callDep(makeMap(deps, callback).f);
            } else if (!deps.splice) {
                //deps is a config object, not an array.
                config = deps;
                if (config.deps) {
                    req(config.deps, config.callback);
                }
                if (!callback) {
                    return;
                }

                if (callback.splice) {
                    //callback is an array, which means it is a dependency list.
                    //Adjust args if there are dependencies
                    deps = callback;
                    callback = relName;
                    relName = null;
                } else {
                    deps = undef;
                }
            }

            //Support require(['a'])
            callback = callback || function () {};

            //If relName is a function, it is an errback handler,
            //so remove it.
            if (typeof relName === 'function') {
                relName = forceSync;
                forceSync = alt;
            }

            //Simulate async callback;
            if (forceSync) {
                main(undef, deps, callback, relName);
            } else {
                //Using a non-zero value because of concern for what old browsers
                //do, and latest browsers "upgrade" to 4 if lower value is used:
                //http://www.whatwg.org/specs/web-apps/current-work/multipage/timers.html#dom-windowtimers-settimeout:
                //If want a value immediately, use require('id') instead -- something
                //that works in almond on the global level, but not guaranteed and
                //unlikely to work in other AMD implementations.
                setTimeout(function () {
                    main(undef, deps, callback, relName);
                }, 4);
            }

            return req;
        };

        /**
         * Just drops the config on the floor, but returns req in case
         * the config return value is used.
         */
        req.config = function (cfg) {
            return req(cfg);
        };

        /**
         * Expose module registry for debugging and tooling
         */
        requirejs._defined = defined;

        define = function (name, deps, callback) {

            //This module may not have dependencies
            if (!deps.splice) {
                //deps is not an array, so probably means
                //an object literal or factory function for
                //the value. Adjust args.
                callback = deps;
                deps = [];
            }

            if (!hasProp(defined, name) && !hasProp(waiting, name)) {
                waiting[name] = [name, deps, callback];
            }
        };

        define.amd = {
            jQuery: true
        };
    }());

    define("almond", function(){});

    define('jquery',[],function () {
        var _$ = jQuery || $;

        if (_$ == null && console && console.error) {
            console.error(
                    'Select2: An instance of jQuery or a jQuery-compatible library was not ' +
                    'found. Make sure that you are including jQuery before Select2 on your ' +
                    'web page.'
            );
        }

        return _$;
    });

    define('select2/utils',[], function () {
        var Utils = {};

        Utils.Extend = function (ChildClass, SuperClass) {
            var __hasProp = {}.hasOwnProperty;

            function BaseConstructor () {
                this.constructor = ChildClass;
            }

            for (var key in SuperClass) {
                if (__hasProp.call(SuperClass, key)) {
                    ChildClass[key] = SuperClass[key];
                }
            }

            BaseConstructor.prototype = SuperClass.prototype;
            ChildClass.prototype = new BaseConstructor();
            ChildClass.__super__ = SuperClass.prototype;

            return ChildClass;
        };

        function getMethods (theClass) {
            var proto = theClass.prototype;

            var methods = [];

            for (var methodName in proto) {
                var m = proto[methodName];

                if (typeof m !== 'function') {
                    continue;
                }

                if (methodName === 'constructor') {
                    continue;
                }

                methods.push(methodName);
            }

            return methods;
        }

        Utils.Decorate = function (SuperClass, DecoratorClass) {
            var decoratedMethods = getMethods(DecoratorClass);
            var superMethods = getMethods(SuperClass);

            function DecoratedClass () {
                var unshift = Array.prototype.unshift;

                var argCount = DecoratorClass.prototype.constructor.length;

                var calledConstructor = SuperClass.prototype.constructor;

                if (argCount > 0) {
                    unshift.call(arguments, SuperClass.prototype.constructor);

                    calledConstructor = DecoratorClass.prototype.constructor;
                }

                calledConstructor.apply(this, arguments);
            }

            DecoratorClass.displayName = SuperClass.displayName;

            function ctr () {
                this.constructor = DecoratedClass;
            }

            DecoratedClass.prototype = new ctr();

            for (var m = 0; m < superMethods.length; m++) {
                var superMethod = superMethods[m];

                DecoratedClass.prototype[superMethod] =
                    SuperClass.prototype[superMethod];
            }

            var calledMethod = function (methodName) {
                // Stub out the original method if it's not decorating an actual method
                var originalMethod = function () {};

                if (methodName in DecoratedClass.prototype) {
                    originalMethod = DecoratedClass.prototype[methodName];
                }

                var decoratedMethod = DecoratorClass.prototype[methodName];

                return function () {
                    var unshift = Array.prototype.unshift;

                    unshift.call(arguments, originalMethod);

                    return decoratedMethod.apply(this, arguments);
                };
            };

            for (var d = 0; d < decoratedMethods.length; d++) {
                var decoratedMethod = decoratedMethods[d];

                DecoratedClass.prototype[decoratedMethod] = calledMethod(decoratedMethod);
            }

            return DecoratedClass;
        };

        var Observable = function () {
            this.listeners = {};
        };

        Observable.prototype.on = function (event, callback) {
            this.listeners = this.listeners || {};

            if (event in this.listeners) {
                this.listeners[event].push(callback);
            } else {
                this.listeners[event] = [callback];
            }
        };

        Observable.prototype.trigger = function (event) {
            var slice = Array.prototype.slice;

            this.listeners = this.listeners || {};

            if (event in this.listeners) {
                this.invoke(this.listeners[event], slice.call(arguments, 1));
            }

            if ('*' in this.listeners) {
                this.invoke(this.listeners['*'], arguments);
            }
        };

        Observable.prototype.invoke = function (listeners, params) {
            for (var i = 0, len = listeners.length; i < len; i++) {
                listeners[i].apply(this, params);
            }
        };

        Utils.Observable = Observable;

        Utils.generateChars = function (length) {
            var chars = '';

            for (var i = 0; i < length; i++) {
                var randomChar = Math.floor(Math.random() * 36);
                chars += randomChar.toString(36);
            }

            return chars;
        };

        Utils.bind = function (func, context) {
            return function () {
                func.apply(context, arguments);
            };
        };

        Utils._convertData = function (data) {
            for (var originalKey in data) {
                var keys = originalKey.split('-');

                var dataLevel = data;

                if (keys.length === 1) {
                    continue;
                }

                for (var k = 0; k < keys.length; k++) {
                    var key = keys[k];

                    // Lowercase the first letter
                    // By default, dash-separated becomes camelCase
                    key = key.substring(0, 1).toLowerCase() + key.substring(1);

                    if (!(key in dataLevel)) {
                        dataLevel[key] = {};
                    }

                    if (k == keys.length - 1) {
                        dataLevel[key] = data[originalKey];
                    }

                    dataLevel = dataLevel[key];
                }

                delete data[originalKey];
            }

            return data;
        };

        Utils.hasScroll = function (index, el) {
            // Adapted from the function created by @ShadowScripter
            // and adapted by @BillBarry on the Stack Exchange Code Review website.
            // The original code can be found at
            // http://codereview.stackexchange.com/q/13338
            // and was designed to be used with the Sizzle selector engine.

            var $el = jQuery(el);
            var overflowX = el.style.overflowX;
            var overflowY = el.style.overflowY;

            //Check both x and y declarations
            if (overflowX === overflowY &&
                (overflowY === 'hidden' || overflowY === 'visible')) {
                return false;
            }

            if (overflowX === 'scroll' || overflowY === 'scroll') {
                return true;
            }

            return ($el.innerHeight() < el.scrollHeight ||
                $el.innerWidth() < el.scrollWidth);
        };

        return Utils;
    });

    define('select2/results',[
        'jquery',
        './utils'
    ], function ($, Utils) {
        function Results ($element, options, dataAdapter) {
            this.$element = $element;
            this.data = dataAdapter;
            this.options = options;

            Results.__super__.constructor.call(this);
        }

        Utils.Extend(Results, Utils.Observable);

        Results.prototype.render = function () {
            var $results = $(
                '<ul class="select2-results__options" role="tree"></ul>'
            );

            if (this.options.get('multiple')) {
                $results.attr('aria-multiselectable', 'true');
            }
           
            this.$results = $results;

            return $results;
        };

        Results.prototype.clear = function () {
            this.$results.empty();
        };

        Results.prototype.displayMessage = function (params) {
            this.clear();
            this.hideLoading();

            var $message = $(
                '<li role="treeitem" class="select2-results__option"></li>'
            );

            var message = this.options.get('translations').get(params.message);

            $message.text(message(params.args));

            this.$results.append($message);
        };

        Results.prototype.append = function (data) {
            this.hideLoading();

            var $options = [];

            if (data.results == null || data.results.length === 0) {
                if (this.$results.children().length === 0) {
                    this.trigger('results:message', {
                        message: 'noResults'
                    });
                }

                return;
            }

            data.results = this.sort(data.results);
           
            for (var d = 0; d < data.results.length; d++) {
                var item = data.results[d];

                var $option = this.option(item);
               
                $options.push($option);
            }

            this.$results.append($options);
        };

        Results.prototype.position = function ($results, $dropdown) {
            var $resultsContainer = $dropdown.find('.select2-results');
            $resultsContainer.append($results);
        };

        Results.prototype.sort = function (data) {
            var sorter = this.options.get('sorter');

            return sorter(data);
        };

        Results.prototype.setClasses = function () {
            var self = this;

            this.data.current(function (selected) {
                var selectedIds = $.map(selected, function (s) {
                    return s.id.toString();
                });

                var $options = self.$results
                    .find('.select2-results__option[aria-selected]');

                $options.each(function () {
                    var $option = $(this);

                    var item = $.data(this, 'data');

                    if (item.id != null && selectedIds.indexOf(item.id.toString()) > -1) {
                        $option.attr('aria-selected', 'true');
                    } else {
                        $option.attr('aria-selected', 'false');
                    }
                });

                var $selected = $options.filter('[aria-selected=true]');

                // Check if there are any selected options
                if ($selected.length > 0) {
                    // If there are selected options, highlight the first
                    $selected.first().trigger('mouseenter');
                } else {
                    // If there are no selected options, highlight the first option
                    // in the dropdown
                    $options.first().trigger('mouseenter');
                }
            });
        };

        Results.prototype.showLoading = function (params) {
            this.hideLoading();

            var loadingMore = this.options.get('translations').get('searching');

            var loading = {
                disabled: true,
                loading: true,
                text: loadingMore(params)
            };
            var $loading = this.option(loading);
            $loading.className += ' loading-results';

            this.$results.prepend($loading);
        };

        Results.prototype.hideLoading = function () {
            this.$results.find('.loading-results').remove();
        };

        Results.prototype.option = function (data) {
            var option = document.createElement('li');
            option.className = 'select2-results__option';
        
            var attrs = {
                'role': 'treeitem',
                'aria-selected': 'false',
                'aria-notallow': data.notallow
            };
         
            if (data.disabled) {
                delete attrs['aria-selected'];
                attrs['aria-disabled'] = 'true';
            }

            if (data.id == null) {
                delete attrs['aria-selected'];
            }

            if (data._resultId != null) {
                option.id = data._resultId;
            }

            if (data.children) {
                attrs.role = 'group';
                attrs['aria-label'] = data.text;
                delete attrs['aria-selected'];
            }

            for (var attr in attrs) {
                var val = attrs[attr];

                option.setAttribute(attr, val);
            }

            if (data.children) {
                var $option = $(option);

                var label = document.createElement('strong');
                label.className = 'select2-results__group';

                var $label = $(label);
                this.template(data, label);

                var $children = [];

                for (var c = 0; c < data.children.length; c++) {
                    var child = data.children[c];

                    var $child = this.option(child);

                    $children.push($child);
                }

                var $childrenContainer = $('<ul></ul>', {
                    'class': 'select2-results__options select2-results__options--nested'
                });

                $childrenContainer.append($children);

                $option.append(label);
                $option.append($childrenContainer);
            } else {
                this.template(data, option);
            }

            $.data(option, 'data', data);

            return option;
        };

        Results.prototype.bind = function (container, $container) {
            var self = this;

            var id = container.id + '-results';

            this.$results.attr('id', id);

            container.on('results:all', function (params) {
                self.clear();
                self.append(params.data);

                if (container.isOpen()) {
                    self.setClasses();
                }
            });

            container.on('results:append', function (params) {
                self.append(params.data);

                if (container.isOpen()) {
                    self.setClasses();
                }
            });

            container.on('query', function (params) {
                self.showLoading(params);
            });

            container.on('select', function () {
                if (!container.isOpen()) {
                    return;
                }

                self.setClasses();
            });

            container.on('unselect', function () {
                if (!container.isOpen()) {
                    return;
                }

                self.setClasses();
            });

            container.on('open', function () {
                // When the dropdown is open, aria-expended="true"
                self.$results.attr('aria-expanded', 'true');
                self.$results.attr('aria-hidden', 'false');

                self.setClasses();
                self.ensureHighlightVisible();
            });

            container.on('close', function () {
                // When the dropdown is closed, aria-expended="false"
                self.$results.attr('aria-expanded', 'false');
                self.$results.attr('aria-hidden', 'true');
                self.$results.removeAttr('aria-activedescendant');
            });

            container.on('results:select', function () {
                var $highlighted = self.getHighlightedResults();

                if ($highlighted.length === 0) {
                    return;
                }

                var data = $highlighted.data('data');

                if ($highlighted.attr('aria-selected') == 'true') {
                    if (self.options.get('multiple')) {
                        self.trigger('unselect', {
                            data: data
                        });
                    } else {
                        self.trigger('close');
                    }
                } else {
                    self.trigger('select', {
                        data: data
                    });
                }
            });

            container.on('results:previous', function () {
                var $highlighted = self.getHighlightedResults();

                var $options = self.$results.find('[aria-selected]');

                var currentIndex = $options.index($highlighted);

                // If we are already at te top, don't move further
                if (currentIndex === 0) {
                    return;
                }

                var nextIndex = currentIndex - 1;

                // If none are highlighted, highlight the first
                if ($highlighted.length === 0) {
                    nextIndex = 0;
                }

                var $next = $options.eq(nextIndex);

                $next.trigger('mouseenter');

                var currentOffset = self.$results.offset().top;
                var nextTop = $next.offset().top;
                var nextOffset = self.$results.scrollTop() + (nextTop - currentOffset);

                if (nextIndex === 0) {
                    self.$results.scrollTop(0);
                } else if (nextTop - currentOffset < 0) {
                    self.$results.scrollTop(nextOffset);
                }
            });

            container.on('results:next', function () {
                var $highlighted = self.getHighlightedResults();

                var $options = self.$results.find('[aria-selected]');

                var currentIndex = $options.index($highlighted);

                var nextIndex = currentIndex + 1;

                // If we are at the last option, stay there
                if (nextIndex >= $options.length) {
                    return;
                }

                var $next = $options.eq(nextIndex);

                $next.trigger('mouseenter');

                var currentOffset = self.$results.offset().top +
                    self.$results.outerHeight(false);
                var nextBottom = $next.offset().top + $next.outerHeight(false);
                var nextOffset = self.$results.scrollTop() + nextBottom - currentOffset;

                if (nextIndex === 0) {
                    self.$results.scrollTop(0);
                } else if (nextBottom > currentOffset) {
                    self.$results.scrollTop(nextOffset);
                }
            });

            container.on('results:focus', function (params) {
                params.element.addClass('select2-results__option--highlighted');
            });

            container.on('results:message', function (params) {
                self.displayMessage(params);
            });

            if ($.fn.mousewheel) {
                this.$results.on('mousewheel', function (e) {
                    var top = self.$results.scrollTop();

                    var bottom = (
                        self.$results.get(0).scrollHeight -
                        self.$results.scrollTop() +
                        e.deltaY
                        );

                    var isAtTop = e.deltaY > 0 && top - e.deltaY <= 0;
                    var isAtBottom = e.deltaY < 0 && bottom <= self.$results.height();

                    if (isAtTop) {
                        self.$results.scrollTop(0);

                        e.preventDefault();
                        e.stopPropagation();
                    } else if (isAtBottom) {
                        self.$results.scrollTop(
                                self.$results.get(0).scrollHeight - self.$results.height()
                        );

                        e.preventDefault();
                        e.stopPropagation();
                    }
                });
            }

            this.$results.on('mouseup', '.select2-results__option[aria-selected]',
                function (evt) {
                    var $this = $(this);

                    var data = $this.data('data');

                    if ($this.attr('aria-selected') === 'true') {
                        if (self.options.get('multiple')) {
                            self.trigger('unselect', {
                                originalEvent: evt,
                                data: data
                            });
                        } else {
                            self.trigger('close');
                        }

                        return;
                    }

                    self.trigger('select', {
                        originalEvent: evt,
                        data: data
                    });
                });

            this.$results.on('mouseenter', '.select2-results__option[aria-selected]',
                function (evt) {
                    var data = $(this).data('data');

                    self.getHighlightedResults()
                        .removeClass('select2-results__option--highlighted');

                    self.trigger('results:focus', {
                        data: data,
                        element: $(this)
                    });
                });
        };

        Results.prototype.getHighlightedResults = function () {
            var $highlighted = this.$results
                .find('.select2-results__option--highlighted');

            return $highlighted;
        };

        Results.prototype.destroy = function () {
            this.$results.remove();
        };

        Results.prototype.ensureHighlightVisible = function () {
            var $highlighted = this.getHighlightedResults();

            if ($highlighted.length === 0) {
                return;
            }

            var $options = this.$results.find('[aria-selected]');

            var currentIndex = $options.index($highlighted);

            var currentOffset = this.$results.offset().top;
            var nextTop = $highlighted.offset().top;
            var nextOffset = this.$results.scrollTop() + (nextTop - currentOffset);

            var offsetDelta = nextTop - currentOffset;
            nextOffset -= $highlighted.outerHeight(false) * 2;

            if (currentIndex <= 2) {
                this.$results.scrollTop(0);
            } else if (offsetDelta > this.$results.outerHeight() || offsetDelta < 0) {
                this.$results.scrollTop(nextOffset);
            }
        };

        Results.prototype.template = function (result, container) {
            var template = this.options.get('templateResult');

            var content = template(result);

            if (content == null) {
                container.style.display = 'none';
            } else {
                container.innerHTML = content;
            }
        };

        return Results;
    });

    define('select2/keys',[

    ], function () {
        var KEYS = {
            BACKSPACE: 8,
            TAB: 9,
            ENTER: 13,
            SHIFT: 16,
            CTRL: 17,
            ALT: 18,
            ESC: 27,
            SPACE: 32,
            PAGE_UP: 33,
            PAGE_DOWN: 34,
            END: 35,
            HOME: 36,
            LEFT: 37,
            UP: 38,
            RIGHT: 39,
            DOWN: 40,
            DELETE: 46,

            isArrow: function (k) {
                k = k.which ? k.which : k;

                switch (k) {
                    case KEY.LEFT:
                    case KEY.RIGHT:
                    case KEY.UP:
                    case KEY.DOWN:
                        return true;
                }

                return false;
            }
        };

        return KEYS;
    });

    define('select2/selection/base',[
        'jquery',
        '../utils',
        '../keys'
    ], function ($, Utils, KEYS) {
        function BaseSelection ($element, options) {
            this.$element = $element;
            this.options = options;

            BaseSelection.__super__.constructor.call(this);
        }

        Utils.Extend(BaseSelection, Utils.Observable);

        BaseSelection.prototype.render = function () {
            var $selection = $(
                    '<span class="select2-selection" tabindex="0" role="combobox" ' +
                    'aria-autocomplete="list" aria-haspopup="true" aria-expanded="false">' +
                    '</span>'
            );

            $selection.attr('title', this.$element.attr('title'));

            this.$selection = $selection;

            return $selection;
        };

        BaseSelection.prototype.bind = function (container, $container) {
            var self = this;

            var id = container.id + '-container';
            var resultsId = container.id + '-results';

            this.container = container;

            this.$selection.attr('aria-owns', resultsId);

            this.$selection.on('keydown', function (evt) {
                self.trigger('keypress', evt);

                if (evt.which === KEYS.SPACE) {
                    evt.preventDefault();
                }
            });

            container.on('results:focus', function (params) {
                self.$selection.attr('aria-activedescendant', params.data._resultId);
            });

            container.on('selection:update', function (params) {
                self.update(params.data);
            });

            container.on('open', function () {
                // When the dropdown is open, aria-expanded="true"
                self.$selection.attr('aria-expanded', 'true');

                self._attachCloseHandler(container);
            });

            container.on('close', function () {
                // When the dropdown is closed, aria-expanded="false"
                self.$selection.attr('aria-expanded', 'false');
                self.$selection.removeAttr('aria-activedescendant');

                self.$selection.focus();

                self._detachCloseHandler(container);
            });

            container.on('enable', function () {
                self.$selection.attr('tabindex', '0');
            });

            container.on('disable', function () {
                self.$selection.attr('tabindex', '-1');
            });
        };

        BaseSelection.prototype._attachCloseHandler = function (container) {
            var self = this;

            $(document.body).on('mousedown.select2.' + container.id, function (e) {
                var $target = $(e.target);

                var $select = $target.closest('.select2');

                var $all = $('.select2.select2-container--open');

                $all.each(function () {
                    var $this = $(this);

                    if (this == $select[0]) {
                        return;
                    }

                    var $element = $this.data('element');

                    $element.select2('close');
                });
            });
        };

        BaseSelection.prototype._detachCloseHandler = function (container) {
            $(document.body).off('mousedown.select2.' + container.id);
        };

        BaseSelection.prototype.position = function ($selection, $container) {
            var $selectionContainer = $container.find('.selection');
            $selectionContainer.append($selection);
        };

        BaseSelection.prototype.destroy = function () {
            this._detachCloseHandler(this.container);
        };

        BaseSelection.prototype.update = function (data) {
            throw new Error('The `update` method must be defined in child classes.');
        };

        return BaseSelection;
    });

    define('select2/selection/single',[
        'jquery',
        './base',
        '../utils',
        '../keys'
    ], function ($, BaseSelection, Utils, KEYS) {
        function SingleSelection () {
            SingleSelection.__super__.constructor.apply(this, arguments);
        }

        Utils.Extend(SingleSelection, BaseSelection);

        SingleSelection.prototype.render = function () {
            var $selection = SingleSelection.__super__.render.call(this);

            $selection.addClass('select2-selection--single');

            $selection.html(
                    '<span class="select2-selection__rendered"></span>' +
                    '<span class="select2-selection__arrow" role="presentation">' +
                    '<b role="presentation"></b>' +
                    '</span>'
            );

            return $selection;
        };

        SingleSelection.prototype.bind = function (container, $container) {
            var self = this;

            SingleSelection.__super__.bind.apply(this, arguments);

            var id = container.id + '-container';

            this.$selection.find('.select2-selection__rendered').attr('id', id);
            this.$selection.attr('aria-labelledby', id);

            this.$selection.on('mousedown', function (evt) {
                // Only respond to left clicks
                if (evt.which !== 1) {
                    return;
                }

                self.trigger('toggle', {
                    originalEvent: evt
                });
            });

            this.$selection.on('focus', function (evt) {
                // User focuses on the container
            });

            this.$selection.on('blur', function (evt) {
                // User exits the container
            });

            container.on('selection:update', function (params) {
                self.update(params.data);
            });
        };

        SingleSelection.prototype.clear = function () {
            this.$selection.find('.select2-selection__rendered').empty();
        };

        SingleSelection.prototype.display = function (data) {
            var template = this.options.get('templateSelection');

            return template(data);
        };

        SingleSelection.prototype.selectionContainer = function () {
            return $('<span></span>');
        };

        SingleSelection.prototype.update = function (data) {
            if (data.length === 0) {
                this.clear();
                return;
            }

            var selection = data[0];

            var formatted = this.display(selection);

            this.$selection.find('.select2-selection__rendered').html(formatted);
        };

        return SingleSelection;
    });

    define('select2/selection/multiple',[
        'jquery',
        './base',
        '../utils'
    ], function ($, BaseSelection, Utils) {
        function MultipleSelection ($element, options) {
            MultipleSelection.__super__.constructor.apply(this, arguments);
        }

        Utils.Extend(MultipleSelection, BaseSelection);

        MultipleSelection.prototype.render = function () {
            var $selection = MultipleSelection.__super__.render.call(this);

            $selection.addClass('select2-selection--multiple');

            $selection.html(
                '<ul class="select2-selection__rendered"></ul>'
            );

            return $selection;
        };

        MultipleSelection.prototype.bind = function (container, $container) {
            var self = this;

            MultipleSelection.__super__.bind.apply(this, arguments);

            this.$selection.on('click', function (evt) {
                self.trigger('toggle', {
                    originalEvent: evt
                });
            });

            this.$selection.on('click', '.select2-selection__choice__remove',
                function (evt) {
                    var $remove = $(this);
                    var $selection = $remove.parent();

                    var data = $selection.data('data');

                    self.trigger('unselect', {
                        originalEvent: evt,
                        data: data
                    });
                });
        };

        MultipleSelection.prototype.clear = function () {
            this.$selection.find('.select2-selection__rendered').empty();
        };

        MultipleSelection.prototype.display = function (data) {
            var template = this.options.get('templateSelection');

            return template(data);
        };

        MultipleSelection.prototype.selectionContainer = function () {
            var $container = $(
                    '<li class="select2-selection__choice">' +
                    '<span class="select2-selection__choice__remove" role="presentation">' +
                    '&times;' +
                    '</span>' +
                    '</li>'
            );

            return $container;
        };

        MultipleSelection.prototype.update = function (data) {
            this.clear();

            if (data.length === 0) {
                return;
            }

            var $selections = [];

            for (var d = 0; d < data.length; d++) {
                var selection = data[d];

                var formatted = this.display(selection);
                var $selection = this.selectionContainer();

                $selection.append(formatted);
                $selection.data('data', selection);

                $selections.push($selection);
            }

            this.$selection.find('.select2-selection__rendered').append($selections);
        };

        return MultipleSelection;
    });

    define('select2/selection/placeholder',[
        '../utils'
    ], function (Utils) {
        function Placeholder (decorated, $element, options) {
            this.placeholder = this.normalizePlaceholder(options.get('placeholder'));

            decorated.call(this, $element, options);
        }

        Placeholder.prototype.normalizePlaceholder = function (_, placeholder) {
            if (typeof placeholder === 'string') {
                placeholder = {
                    id: '',
                    text: placeholder
                };
            }

            return placeholder;
        };

        Placeholder.prototype.createPlaceholder = function (decorated, placeholder) {
            var $placeholder = this.selectionContainer();

            $placeholder.html(this.display(placeholder));
            $placeholder.addClass('select2-selection__placeholder')
                .removeClass('select2-selection__choice');

            return $placeholder;
        };

        Placeholder.prototype.update = function (decorated, data) {
            var singlePlaceholder = (
                data.length == 1 && data[0].id != this.placeholder.id
                );
            var multipleSelections = data.length > 1;

            if (multipleSelections || singlePlaceholder) {
                return decorated.call(this, data);
            }

            this.clear();

            var $placeholder = this.createPlaceholder(this.placeholder);

            this.$selection.find('.select2-selection__rendered').append($placeholder);
        };

        return Placeholder;
    });

    define('select2/selection/allowClear',[
        'jquery'
    ], function ($) {
        function AllowClear () { }

        AllowClear.prototype.bind = function (decorated, container, $container) {
            var self = this;

            decorated.call(this, container, $container);

            this.$selection.on('mousedown', '.select2-selection__clear',
                function (evt) {
                    // Ignore the event if it is disabled
                    if (self.options.get('disabled')) {
                        return;
                    }

                    evt.stopPropagation();

                    var data = $(this).data('data');

                    for (var d = 0; d < data.length; d++) {
                        var unselectData = {
                            data: data[d]
                        };

                        // Trigger the `unselect` event, so people can prevent it from being
                        // cleared.
                        self.trigger('unselect', unselectData);

                        // If the event was prevented, don't clear it out.
                        if (unselectData.prevented) {
                            return;
                        }
                    }

                    self.$element.val(self.placeholder.id).trigger('change');

                    self.trigger('toggle');
                });
        };

        AllowClear.prototype.update = function (decorated, data) {
            decorated.call(this, data);

            if (this.$selection.find('.select2-selection__placeholder').length > 0 ||
                data.length === 0) {
                return;
            }

            var $remove = $(
                    '<span class="select2-selection__clear">' +
                    '&times;' +
                    '</span>'
            );
            $remove.data('data', data);

            this.$selection.find('.select2-selection__rendered').append($remove);
        };

        return AllowClear;
    });

    define('select2/selection/search',[
        'jquery',
        '../utils',
        '../keys'
    ], function ($, Utils, KEYS) {
        function Search (decorated, $element, options) {
            decorated.call(this, $element, options);
        }

        Search.prototype.render = function (decorated) {
            var $search = $(
                    '<li class="select2-search select2-search--inline">' +
                    '<input class="select2-search__field" type="search" tabindex="-1"' +
                    ' role="textbox" />' +
                    '</li>'
            );

            this.$searchContainer = $search;
            this.$search = $search.find('input');

            var $rendered = decorated.call(this);

            return $rendered;
        };

        Search.prototype.bind = function (decorated, container, $container) {
            var self = this;

            decorated.call(this, container, $container);

            container.on('open', function () {
                self.$search.attr('tabindex', 0);

                self.$search.focus();
            });

            container.on('close', function () {
                self.$search.attr('tabindex', -1);

                self.$search.val('');
            });

            container.on('enable', function () {
                self.$search.prop('disabled', false);
            });

            container.on('disable', function () {
                self.$search.prop('disabled', true);
            });

            this.$selection.on('keydown', '.select2-search--inline', function (evt) {
                evt.stopPropagation();

                self.trigger('keypress', evt);

                self._keyUpPrevented = evt.isDefaultPrevented();

                var key = evt.which;

                if (key === KEYS.BACKSPACE && self.$search.val() === '') {
                    var $previousChoice = self.$searchContainer
                        .prev('.select2-selection__choice');

                    if ($previousChoice.length > 0) {
                        var item = $previousChoice.data('data');

                        self.searchRemoveChoice(item);
                    }
                }
            });

            this.$selection.on('keyup', '.select2-search--inline', function (evt) {
                self.handleSearch(evt);
            });
        };

        Search.prototype.createPlaceholder = function (decorated, placeholder) {
            this.$search.attr('placeholder', placeholder.text);
        };

        Search.prototype.update = function (decorated, data) {
            this.$search.attr('placeholder', '');

            decorated.call(this, data);

            this.$selection.find('.select2-selection__rendered')
                .append(this.$searchContainer);

            this.resizeSearch();
        };

        Search.prototype.handleSearch = function () {
            this.resizeSearch();

            if (!this._keyUpPrevented) {
                var input = this.$search.val();

                this.trigger('query', {
                    term: input
                });
            }

            this._keyUpPrevented = false;
        };

        Search.prototype.searchRemoveChoice = function (decorated, item) {
            this.trigger('unselect', {
                data: item
            });

            this.trigger('open');

            this.$search.val(item.text + ' ');
        };

        Search.prototype.resizeSearch = function () {
            this.$search.css('width', '25px');

            var width = '';

            if (this.$search.attr('placeholder') !== '') {
                width = this.$selection.find('.select2-selection__rendered').innerWidth();
            } else {
                var minimumWidth = this.$search.val().length + 1;

                width = (minimumWidth * 0.75) + 'em';
            }

            this.$search.css('width', width);
        };

        return Search;
    });

    define('select2/selection/eventRelay',[
        'jquery'
    ], function ($) {
        function EventRelay () { }

        EventRelay.prototype.bind = function (decorated, container, $container) {
            var self = this;
            var relayEvents = [
                'open', 'opening',
                'close', 'closing',
                'select', 'selecting',
                'unselect', 'unselecting'
            ];

            var preventableEvents = ['opening', 'closing', 'selecting', 'unselecting'];

            decorated.call(this, container, $container);

            container.on('*', function (name, params) {
                // Ignore events that should not be relayed
                if (relayEvents.indexOf(name) === -1) {
                    return;
                }

                // The parameters should always be an object
                params = params || {};

                // Generate the jQuery event for the Select2 event
                var evt = $.Event('select2:' + name, {
                    params: params
                });

                self.$element.trigger(evt);

                // Only handle preventable events if it was one
                if (preventableEvents.indexOf(name) === -1) {
                    return;
                }

                params.prevented = evt.isDefaultPrevented();
            });
        };

        return EventRelay;
    });

    define('select2/translation',[
        'jquery'
    ], function ($) {
        function Translation (dict) {
            this.dict = dict || {};
        }

        Translation.prototype.all = function () {
            return this.dict;
        };

        Translation.prototype.get = function (key) {
            return this.dict[key];
        };

        Translation.prototype.extend = function (translation) {
            this.dict = $.extend({}, translation.all(), this.dict);
        };

        // Static functions

        Translation._cache = {};

        Translation.loadPath = function (path) {
            if (!(path in Translation._cache)) {
                var translations = require(path);

                Translation._cache[path] = translations;
            }

            return new Translation(Translation._cache[path]);
        };

        return Translation;
    });

    define('select2/diacritics',[

    ], function () {
        var diacritics = {
            '\u24B6': 'A',
            '\uFF21': 'A',
            '\u00C0': 'A',
            '\u00C1': 'A',
            '\u00C2': 'A',
            '\u1EA6': 'A',
            '\u1EA4': 'A',
            '\u1EAA': 'A',
            '\u1EA8': 'A',
            '\u00C3': 'A',
            '\u0100': 'A',
            '\u0102': 'A',
            '\u1EB0': 'A',
            '\u1EAE': 'A',
            '\u1EB4': 'A',
            '\u1EB2': 'A',
            '\u0226': 'A',
            '\u01E0': 'A',
            '\u00C4': 'A',
            '\u01DE': 'A',
            '\u1EA2': 'A',
            '\u00C5': 'A',
            '\u01FA': 'A',
            '\u01CD': 'A',
            '\u0200': 'A',
            '\u0202': 'A',
            '\u1EA0': 'A',
            '\u1EAC': 'A',
            '\u1EB6': 'A',
            '\u1E00': 'A',
            '\u0104': 'A',
            '\u023A': 'A',
            '\u2C6F': 'A',
            '\uA732': 'AA',
            '\u00C6': 'AE',
            '\u01FC': 'AE',
            '\u01E2': 'AE',
            '\uA734': 'AO',
            '\uA736': 'AU',
            '\uA738': 'AV',
            '\uA73A': 'AV',
            '\uA73C': 'AY',
            '\u24B7': 'B',
            '\uFF22': 'B',
            '\u1E02': 'B',
            '\u1E04': 'B',
            '\u1E06': 'B',
            '\u0243': 'B',
            '\u0182': 'B',
            '\u0181': 'B',
            '\u24B8': 'C',
            '\uFF23': 'C',
            '\u0106': 'C',
            '\u0108': 'C',
            '\u010A': 'C',
            '\u010C': 'C',
            '\u00C7': 'C',
            '\u1E08': 'C',
            '\u0187': 'C',
            '\u023B': 'C',
            '\uA73E': 'C',
            '\u24B9': 'D',
            '\uFF24': 'D',
            '\u1E0A': 'D',
            '\u010E': 'D',
            '\u1E0C': 'D',
            '\u1E10': 'D',
            '\u1E12': 'D',
            '\u1E0E': 'D',
            '\u0110': 'D',
            '\u018B': 'D',
            '\u018A': 'D',
            '\u0189': 'D',
            '\uA779': 'D',
            '\u01F1': 'DZ',
            '\u01C4': 'DZ',
            '\u01F2': 'Dz',
            '\u01C5': 'Dz',
            '\u24BA': 'E',
            '\uFF25': 'E',
            '\u00C8': 'E',
            '\u00C9': 'E',
            '\u00CA': 'E',
            '\u1EC0': 'E',
            '\u1EBE': 'E',
            '\u1EC4': 'E',
            '\u1EC2': 'E',
            '\u1EBC': 'E',
            '\u0112': 'E',
            '\u1E14': 'E',
            '\u1E16': 'E',
            '\u0114': 'E',
            '\u0116': 'E',
            '\u00CB': 'E',
            '\u1EBA': 'E',
            '\u011A': 'E',
            '\u0204': 'E',
            '\u0206': 'E',
            '\u1EB8': 'E',
            '\u1EC6': 'E',
            '\u0228': 'E',
            '\u1E1C': 'E',
            '\u0118': 'E',
            '\u1E18': 'E',
            '\u1E1A': 'E',
            '\u0190': 'E',
            '\u018E': 'E',
            '\u24BB': 'F',
            '\uFF26': 'F',
            '\u1E1E': 'F',
            '\u0191': 'F',
            '\uA77B': 'F',
            '\u24BC': 'G',
            '\uFF27': 'G',
            '\u01F4': 'G',
            '\u011C': 'G',
            '\u1E20': 'G',
            '\u011E': 'G',
            '\u0120': 'G',
            '\u01E6': 'G',
            '\u0122': 'G',
            '\u01E4': 'G',
            '\u0193': 'G',
            '\uA7A0': 'G',
            '\uA77D': 'G',
            '\uA77E': 'G',
            '\u24BD': 'H',
            '\uFF28': 'H',
            '\u0124': 'H',
            '\u1E22': 'H',
            '\u1E26': 'H',
            '\u021E': 'H',
            '\u1E24': 'H',
            '\u1E28': 'H',
            '\u1E2A': 'H',
            '\u0126': 'H',
            '\u2C67': 'H',
            '\u2C75': 'H',
            '\uA78D': 'H',
            '\u24BE': 'I',
            '\uFF29': 'I',
            '\u00CC': 'I',
            '\u00CD': 'I',
            '\u00CE': 'I',
            '\u0128': 'I',
            '\u012A': 'I',
            '\u012C': 'I',
            '\u0130': 'I',
            '\u00CF': 'I',
            '\u1E2E': 'I',
            '\u1EC8': 'I',
            '\u01CF': 'I',
            '\u0208': 'I',
            '\u020A': 'I',
            '\u1ECA': 'I',
            '\u012E': 'I',
            '\u1E2C': 'I',
            '\u0197': 'I',
            '\u24BF': 'J',
            '\uFF2A': 'J',
            '\u0134': 'J',
            '\u0248': 'J',
            '\u24C0': 'K',
            '\uFF2B': 'K',
            '\u1E30': 'K',
            '\u01E8': 'K',
            '\u1E32': 'K',
            '\u0136': 'K',
            '\u1E34': 'K',
            '\u0198': 'K',
            '\u2C69': 'K',
            '\uA740': 'K',
            '\uA742': 'K',
            '\uA744': 'K',
            '\uA7A2': 'K',
            '\u24C1': 'L',
            '\uFF2C': 'L',
            '\u013F': 'L',
            '\u0139': 'L',
            '\u013D': 'L',
            '\u1E36': 'L',
            '\u1E38': 'L',
            '\u013B': 'L',
            '\u1E3C': 'L',
            '\u1E3A': 'L',
            '\u0141': 'L',
            '\u023D': 'L',
            '\u2C62': 'L',
            '\u2C60': 'L',
            '\uA748': 'L',
            '\uA746': 'L',
            '\uA780': 'L',
            '\u01C7': 'LJ',
            '\u01C8': 'Lj',
            '\u24C2': 'M',
            '\uFF2D': 'M',
            '\u1E3E': 'M',
            '\u1E40': 'M',
            '\u1E42': 'M',
            '\u2C6E': 'M',
            '\u019C': 'M',
            '\u24C3': 'N',
            '\uFF2E': 'N',
            '\u01F8': 'N',
            '\u0143': 'N',
            '\u00D1': 'N',
            '\u1E44': 'N',
            '\u0147': 'N',
            '\u1E46': 'N',
            '\u0145': 'N',
            '\u1E4A': 'N',
            '\u1E48': 'N',
            '\u0220': 'N',
            '\u019D': 'N',
            '\uA790': 'N',
            '\uA7A4': 'N',
            '\u01CA': 'NJ',
            '\u01CB': 'Nj',
            '\u24C4': 'O',
            '\uFF2F': 'O',
            '\u00D2': 'O',
            '\u00D3': 'O',
            '\u00D4': 'O',
            '\u1ED2': 'O',
            '\u1ED0': 'O',
            '\u1ED6': 'O',
            '\u1ED4': 'O',
            '\u00D5': 'O',
            '\u1E4C': 'O',
            '\u022C': 'O',
            '\u1E4E': 'O',
            '\u014C': 'O',
            '\u1E50': 'O',
            '\u1E52': 'O',
            '\u014E': 'O',
            '\u022E': 'O',
            '\u0230': 'O',
            '\u00D6': 'O',
            '\u022A': 'O',
            '\u1ECE': 'O',
            '\u0150': 'O',
            '\u01D1': 'O',
            '\u020C': 'O',
            '\u020E': 'O',
            '\u01A0': 'O',
            '\u1EDC': 'O',
            '\u1EDA': 'O',
            '\u1EE0': 'O',
            '\u1EDE': 'O',
            '\u1EE2': 'O',
            '\u1ECC': 'O',
            '\u1ED8': 'O',
            '\u01EA': 'O',
            '\u01EC': 'O',
            '\u00D8': 'O',
            '\u01FE': 'O',
            '\u0186': 'O',
            '\u019F': 'O',
            '\uA74A': 'O',
            '\uA74C': 'O',
            '\u01A2': 'OI',
            '\uA74E': 'OO',
            '\u0222': 'OU',
            '\u24C5': 'P',
            '\uFF30': 'P',
            '\u1E54': 'P',
            '\u1E56': 'P',
            '\u01A4': 'P',
            '\u2C63': 'P',
            '\uA750': 'P',
            '\uA752': 'P',
            '\uA754': 'P',
            '\u24C6': 'Q',
            '\uFF31': 'Q',
            '\uA756': 'Q',
            '\uA758': 'Q',
            '\u024A': 'Q',
            '\u24C7': 'R',
            '\uFF32': 'R',
            '\u0154': 'R',
            '\u1E58': 'R',
            '\u0158': 'R',
            '\u0210': 'R',
            '\u0212': 'R',
            '\u1E5A': 'R',
            '\u1E5C': 'R',
            '\u0156': 'R',
            '\u1E5E': 'R',
            '\u024C': 'R',
            '\u2C64': 'R',
            '\uA75A': 'R',
            '\uA7A6': 'R',
            '\uA782': 'R',
            '\u24C8': 'S',
            '\uFF33': 'S',
            '\u1E9E': 'S',
            '\u015A': 'S',
            '\u1E64': 'S',
            '\u015C': 'S',
            '\u1E60': 'S',
            '\u0160': 'S',
            '\u1E66': 'S',
            '\u1E62': 'S',
            '\u1E68': 'S',
            '\u0218': 'S',
            '\u015E': 'S',
            '\u2C7E': 'S',
            '\uA7A8': 'S',
            '\uA784': 'S',
            '\u24C9': 'T',
            '\uFF34': 'T',
            '\u1E6A': 'T',
            '\u0164': 'T',
            '\u1E6C': 'T',
            '\u021A': 'T',
            '\u0162': 'T',
            '\u1E70': 'T',
            '\u1E6E': 'T',
            '\u0166': 'T',
            '\u01AC': 'T',
            '\u01AE': 'T',
            '\u023E': 'T',
            '\uA786': 'T',
            '\uA728': 'TZ',
            '\u24CA': 'U',
            '\uFF35': 'U',
            '\u00D9': 'U',
            '\u00DA': 'U',
            '\u00DB': 'U',
            '\u0168': 'U',
            '\u1E78': 'U',
            '\u016A': 'U',
            '\u1E7A': 'U',
            '\u016C': 'U',
            '\u00DC': 'U',
            '\u01DB': 'U',
            '\u01D7': 'U',
            '\u01D5': 'U',
            '\u01D9': 'U',
            '\u1EE6': 'U',
            '\u016E': 'U',
            '\u0170': 'U',
            '\u01D3': 'U',
            '\u0214': 'U',
            '\u0216': 'U',
            '\u01AF': 'U',
            '\u1EEA': 'U',
            '\u1EE8': 'U',
            '\u1EEE': 'U',
            '\u1EEC': 'U',
            '\u1EF0': 'U',
            '\u1EE4': 'U',
            '\u1E72': 'U',
            '\u0172': 'U',
            '\u1E76': 'U',
            '\u1E74': 'U',
            '\u0244': 'U',
            '\u24CB': 'V',
            '\uFF36': 'V',
            '\u1E7C': 'V',
            '\u1E7E': 'V',
            '\u01B2': 'V',
            '\uA75E': 'V',
            '\u0245': 'V',
            '\uA760': 'VY',
            '\u24CC': 'W',
            '\uFF37': 'W',
            '\u1E80': 'W',
            '\u1E82': 'W',
            '\u0174': 'W',
            '\u1E86': 'W',
            '\u1E84': 'W',
            '\u1E88': 'W',
            '\u2C72': 'W',
            '\u24CD': 'X',
            '\uFF38': 'X',
            '\u1E8A': 'X',
            '\u1E8C': 'X',
            '\u24CE': 'Y',
            '\uFF39': 'Y',
            '\u1EF2': 'Y',
            '\u00DD': 'Y',
            '\u0176': 'Y',
            '\u1EF8': 'Y',
            '\u0232': 'Y',
            '\u1E8E': 'Y',
            '\u0178': 'Y',
            '\u1EF6': 'Y',
            '\u1EF4': 'Y',
            '\u01B3': 'Y',
            '\u024E': 'Y',
            '\u1EFE': 'Y',
            '\u24CF': 'Z',
            '\uFF3A': 'Z',
            '\u0179': 'Z',
            '\u1E90': 'Z',
            '\u017B': 'Z',
            '\u017D': 'Z',
            '\u1E92': 'Z',
            '\u1E94': 'Z',
            '\u01B5': 'Z',
            '\u0224': 'Z',
            '\u2C7F': 'Z',
            '\u2C6B': 'Z',
            '\uA762': 'Z',
            '\u24D0': 'a',
            '\uFF41': 'a',
            '\u1E9A': 'a',
            '\u00E0': 'a',
            '\u00E1': 'a',
            '\u00E2': 'a',
            '\u1EA7': 'a',
            '\u1EA5': 'a',
            '\u1EAB': 'a',
            '\u1EA9': 'a',
            '\u00E3': 'a',
            '\u0101': 'a',
            '\u0103': 'a',
            '\u1EB1': 'a',
            '\u1EAF': 'a',
            '\u1EB5': 'a',
            '\u1EB3': 'a',
            '\u0227': 'a',
            '\u01E1': 'a',
            '\u00E4': 'a',
            '\u01DF': 'a',
            '\u1EA3': 'a',
            '\u00E5': 'a',
            '\u01FB': 'a',
            '\u01CE': 'a',
            '\u0201': 'a',
            '\u0203': 'a',
            '\u1EA1': 'a',
            '\u1EAD': 'a',
            '\u1EB7': 'a',
            '\u1E01': 'a',
            '\u0105': 'a',
            '\u2C65': 'a',
            '\u0250': 'a',
            '\uA733': 'aa',
            '\u00E6': 'ae',
            '\u01FD': 'ae',
            '\u01E3': 'ae',
            '\uA735': 'ao',
            '\uA737': 'au',
            '\uA739': 'av',
            '\uA73B': 'av',
            '\uA73D': 'ay',
            '\u24D1': 'b',
            '\uFF42': 'b',
            '\u1E03': 'b',
            '\u1E05': 'b',
            '\u1E07': 'b',
            '\u0180': 'b',
            '\u0183': 'b',
            '\u0253': 'b',
            '\u24D2': 'c',
            '\uFF43': 'c',
            '\u0107': 'c',
            '\u0109': 'c',
            '\u010B': 'c',
            '\u010D': 'c',
            '\u00E7': 'c',
            '\u1E09': 'c',
            '\u0188': 'c',
            '\u023C': 'c',
            '\uA73F': 'c',
            '\u2184': 'c',
            '\u24D3': 'd',
            '\uFF44': 'd',
            '\u1E0B': 'd',
            '\u010F': 'd',
            '\u1E0D': 'd',
            '\u1E11': 'd',
            '\u1E13': 'd',
            '\u1E0F': 'd',
            '\u0111': 'd',
            '\u018C': 'd',
            '\u0256': 'd',
            '\u0257': 'd',
            '\uA77A': 'd',
            '\u01F3': 'dz',
            '\u01C6': 'dz',
            '\u24D4': 'e',
            '\uFF45': 'e',
            '\u00E8': 'e',
            '\u00E9': 'e',
            '\u00EA': 'e',
            '\u1EC1': 'e',
            '\u1EBF': 'e',
            '\u1EC5': 'e',
            '\u1EC3': 'e',
            '\u1EBD': 'e',
            '\u0113': 'e',
            '\u1E15': 'e',
            '\u1E17': 'e',
            '\u0115': 'e',
            '\u0117': 'e',
            '\u00EB': 'e',
            '\u1EBB': 'e',
            '\u011B': 'e',
            '\u0205': 'e',
            '\u0207': 'e',
            '\u1EB9': 'e',
            '\u1EC7': 'e',
            '\u0229': 'e',
            '\u1E1D': 'e',
            '\u0119': 'e',
            '\u1E19': 'e',
            '\u1E1B': 'e',
            '\u0247': 'e',
            '\u025B': 'e',
            '\u01DD': 'e',
            '\u24D5': 'f',
            '\uFF46': 'f',
            '\u1E1F': 'f',
            '\u0192': 'f',
            '\uA77C': 'f',
            '\u24D6': 'g',
            '\uFF47': 'g',
            '\u01F5': 'g',
            '\u011D': 'g',
            '\u1E21': 'g',
            '\u011F': 'g',
            '\u0121': 'g',
            '\u01E7': 'g',
            '\u0123': 'g',
            '\u01E5': 'g',
            '\u0260': 'g',
            '\uA7A1': 'g',
            '\u1D79': 'g',
            '\uA77F': 'g',
            '\u24D7': 'h',
            '\uFF48': 'h',
            '\u0125': 'h',
            '\u1E23': 'h',
            '\u1E27': 'h',
            '\u021F': 'h',
            '\u1E25': 'h',
            '\u1E29': 'h',
            '\u1E2B': 'h',
            '\u1E96': 'h',
            '\u0127': 'h',
            '\u2C68': 'h',
            '\u2C76': 'h',
            '\u0265': 'h',
            '\u0195': 'hv',
            '\u24D8': 'i',
            '\uFF49': 'i',
            '\u00EC': 'i',
            '\u00ED': 'i',
            '\u00EE': 'i',
            '\u0129': 'i',
            '\u012B': 'i',
            '\u012D': 'i',
            '\u00EF': 'i',
            '\u1E2F': 'i',
            '\u1EC9': 'i',
            '\u01D0': 'i',
            '\u0209': 'i',
            '\u020B': 'i',
            '\u1ECB': 'i',
            '\u012F': 'i',
            '\u1E2D': 'i',
            '\u0268': 'i',
            '\u0131': 'i',
            '\u24D9': 'j',
            '\uFF4A': 'j',
            '\u0135': 'j',
            '\u01F0': 'j',
            '\u0249': 'j',
            '\u24DA': 'k',
            '\uFF4B': 'k',
            '\u1E31': 'k',
            '\u01E9': 'k',
            '\u1E33': 'k',
            '\u0137': 'k',
            '\u1E35': 'k',
            '\u0199': 'k',
            '\u2C6A': 'k',
            '\uA741': 'k',
            '\uA743': 'k',
            '\uA745': 'k',
            '\uA7A3': 'k',
            '\u24DB': 'l',
            '\uFF4C': 'l',
            '\u0140': 'l',
            '\u013A': 'l',
            '\u013E': 'l',
            '\u1E37': 'l',
            '\u1E39': 'l',
            '\u013C': 'l',
            '\u1E3D': 'l',
            '\u1E3B': 'l',
            '\u017F': 'l',
            '\u0142': 'l',
            '\u019A': 'l',
            '\u026B': 'l',
            '\u2C61': 'l',
            '\uA749': 'l',
            '\uA781': 'l',
            '\uA747': 'l',
            '\u01C9': 'lj',
            '\u24DC': 'm',
            '\uFF4D': 'm',
            '\u1E3F': 'm',
            '\u1E41': 'm',
            '\u1E43': 'm',
            '\u0271': 'm',
            '\u026F': 'm',
            '\u24DD': 'n',
            '\uFF4E': 'n',
            '\u01F9': 'n',
            '\u0144': 'n',
            '\u00F1': 'n',
            '\u1E45': 'n',
            '\u0148': 'n',
            '\u1E47': 'n',
            '\u0146': 'n',
            '\u1E4B': 'n',
            '\u1E49': 'n',
            '\u019E': 'n',
            '\u0272': 'n',
            '\u0149': 'n',
            '\uA791': 'n',
            '\uA7A5': 'n',
            '\u01CC': 'nj',
            '\u24DE': 'o',
            '\uFF4F': 'o',
            '\u00F2': 'o',
            '\u00F3': 'o',
            '\u00F4': 'o',
            '\u1ED3': 'o',
            '\u1ED1': 'o',
            '\u1ED7': 'o',
            '\u1ED5': 'o',
            '\u00F5': 'o',
            '\u1E4D': 'o',
            '\u022D': 'o',
            '\u1E4F': 'o',
            '\u014D': 'o',
            '\u1E51': 'o',
            '\u1E53': 'o',
            '\u014F': 'o',
            '\u022F': 'o',
            '\u0231': 'o',
            '\u00F6': 'o',
            '\u022B': 'o',
            '\u1ECF': 'o',
            '\u0151': 'o',
            '\u01D2': 'o',
            '\u020D': 'o',
            '\u020F': 'o',
            '\u01A1': 'o',
            '\u1EDD': 'o',
            '\u1EDB': 'o',
            '\u1EE1': 'o',
            '\u1EDF': 'o',
            '\u1EE3': 'o',
            '\u1ECD': 'o',
            '\u1ED9': 'o',
            '\u01EB': 'o',
            '\u01ED': 'o',
            '\u00F8': 'o',
            '\u01FF': 'o',
            '\u0254': 'o',
            '\uA74B': 'o',
            '\uA74D': 'o',
            '\u0275': 'o',
            '\u01A3': 'oi',
            '\u0223': 'ou',
            '\uA74F': 'oo',
            '\u24DF': 'p',
            '\uFF50': 'p',
            '\u1E55': 'p',
            '\u1E57': 'p',
            '\u01A5': 'p',
            '\u1D7D': 'p',
            '\uA751': 'p',
            '\uA753': 'p',
            '\uA755': 'p',
            '\u24E0': 'q',
            '\uFF51': 'q',
            '\u024B': 'q',
            '\uA757': 'q',
            '\uA759': 'q',
            '\u24E1': 'r',
            '\uFF52': 'r',
            '\u0155': 'r',
            '\u1E59': 'r',
            '\u0159': 'r',
            '\u0211': 'r',
            '\u0213': 'r',
            '\u1E5B': 'r',
            '\u1E5D': 'r',
            '\u0157': 'r',
            '\u1E5F': 'r',
            '\u024D': 'r',
            '\u027D': 'r',
            '\uA75B': 'r',
            '\uA7A7': 'r',
            '\uA783': 'r',
            '\u24E2': 's',
            '\uFF53': 's',
            '\u00DF': 's',
            '\u015B': 's',
            '\u1E65': 's',
            '\u015D': 's',
            '\u1E61': 's',
            '\u0161': 's',
            '\u1E67': 's',
            '\u1E63': 's',
            '\u1E69': 's',
            '\u0219': 's',
            '\u015F': 's',
            '\u023F': 's',
            '\uA7A9': 's',
            '\uA785': 's',
            '\u1E9B': 's',
            '\u24E3': 't',
            '\uFF54': 't',
            '\u1E6B': 't',
            '\u1E97': 't',
            '\u0165': 't',
            '\u1E6D': 't',
            '\u021B': 't',
            '\u0163': 't',
            '\u1E71': 't',
            '\u1E6F': 't',
            '\u0167': 't',
            '\u01AD': 't',
            '\u0288': 't',
            '\u2C66': 't',
            '\uA787': 't',
            '\uA729': 'tz',
            '\u24E4': 'u',
            '\uFF55': 'u',
            '\u00F9': 'u',
            '\u00FA': 'u',
            '\u00FB': 'u',
            '\u0169': 'u',
            '\u1E79': 'u',
            '\u016B': 'u',
            '\u1E7B': 'u',
            '\u016D': 'u',
            '\u00FC': 'u',
            '\u01DC': 'u',
            '\u01D8': 'u',
            '\u01D6': 'u',
            '\u01DA': 'u',
            '\u1EE7': 'u',
            '\u016F': 'u',
            '\u0171': 'u',
            '\u01D4': 'u',
            '\u0215': 'u',
            '\u0217': 'u',
            '\u01B0': 'u',
            '\u1EEB': 'u',
            '\u1EE9': 'u',
            '\u1EEF': 'u',
            '\u1EED': 'u',
            '\u1EF1': 'u',
            '\u1EE5': 'u',
            '\u1E73': 'u',
            '\u0173': 'u',
            '\u1E77': 'u',
            '\u1E75': 'u',
            '\u0289': 'u',
            '\u24E5': 'v',
            '\uFF56': 'v',
            '\u1E7D': 'v',
            '\u1E7F': 'v',
            '\u028B': 'v',
            '\uA75F': 'v',
            '\u028C': 'v',
            '\uA761': 'vy',
            '\u24E6': 'w',
            '\uFF57': 'w',
            '\u1E81': 'w',
            '\u1E83': 'w',
            '\u0175': 'w',
            '\u1E87': 'w',
            '\u1E85': 'w',
            '\u1E98': 'w',
            '\u1E89': 'w',
            '\u2C73': 'w',
            '\u24E7': 'x',
            '\uFF58': 'x',
            '\u1E8B': 'x',
            '\u1E8D': 'x',
            '\u24E8': 'y',
            '\uFF59': 'y',
            '\u1EF3': 'y',
            '\u00FD': 'y',
            '\u0177': 'y',
            '\u1EF9': 'y',
            '\u0233': 'y',
            '\u1E8F': 'y',
            '\u00FF': 'y',
            '\u1EF7': 'y',
            '\u1E99': 'y',
            '\u1EF5': 'y',
            '\u01B4': 'y',
            '\u024F': 'y',
            '\u1EFF': 'y',
            '\u24E9': 'z',
            '\uFF5A': 'z',
            '\u017A': 'z',
            '\u1E91': 'z',
            '\u017C': 'z',
            '\u017E': 'z',
            '\u1E93': 'z',
            '\u1E95': 'z',
            '\u01B6': 'z',
            '\u0225': 'z',
            '\u0240': 'z',
            '\u2C6C': 'z',
            '\uA763': 'z',
            '\u0386': '\u0391',
            '\u0388': '\u0395',
            '\u0389': '\u0397',
            '\u038A': '\u0399',
            '\u03AA': '\u0399',
            '\u038C': '\u039F',
            '\u038E': '\u03A5',
            '\u03AB': '\u03A5',
            '\u038F': '\u03A9',
            '\u03AC': '\u03B1',
            '\u03AD': '\u03B5',
            '\u03AE': '\u03B7',
            '\u03AF': '\u03B9',
            '\u03CA': '\u03B9',
            '\u0390': '\u03B9',
            '\u03CC': '\u03BF',
            '\u03CD': '\u03C5',
            '\u03CB': '\u03C5',
            '\u03B0': '\u03C5',
            '\u03C9': '\u03C9',
            '\u03C2': '\u03C3'
        };

        return diacritics;
    });

    define('select2/data/base',[
        '../utils'
    ], function (Utils) {
        function BaseAdapter ($element, options) {
            BaseAdapter.__super__.constructor.call(this);
        }

        Utils.Extend(BaseAdapter, Utils.Observable);

        BaseAdapter.prototype.current = function (callback) {
            throw new Error('The `current` method must be defined in child classes.');
        };

        BaseAdapter.prototype.query = function (params, callback) {
            throw new Error('The `query` method must be defined in child classes.');
        };

        BaseAdapter.prototype.bind = function (container, $container) {
            // Can be implemented in subclasses
        };

        BaseAdapter.prototype.destroy = function () {
            // Can be implemented in subclasses
        };

        BaseAdapter.prototype.generateResultId = function (container, data) {
            var id = container.id + '-result-';

            id += Utils.generateChars(4);

            if (data.id != null) {
                id += '-' + data.id.toString();
            } else {
                id += '-' + Utils.generateChars(4);
            }
            return id;
        };

        return BaseAdapter;
    });

    define('select2/data/select',[
        './base',
        '../utils',
        'jquery'
    ], function (BaseAdapter, Utils, $) {
        function SelectAdapter ($element, options) {
            this.$element = $element;
            this.options = options;

            SelectAdapter.__super__.constructor.call(this);
        }

        Utils.Extend(SelectAdapter, BaseAdapter);

        SelectAdapter.prototype.current = function (callback) {
            var data = [];
            var self = this;

            this.$element.find(':selected').each(function () {
                var $option = $(this);

                var option = self.item($option);

                data.push(option);
            });

            callback(data);
        };

        SelectAdapter.prototype.select = function (data) {
            var self = this;

            // If data.element is a DOM nose, use it instead
            if ($(data.element).is('option')) {
                data.element.selected = true;

                this.$element.trigger('change');

                return;
            }

            if (this.$element.prop('multiple')) {
                this.current(function (currentData) {
                    var val = [];

                    data = [data];
                    data.push.apply(data, currentData);

                    for (var d = 0; d < data.length; d++) {
                        id = data[d].id;

                        if (val.indexOf(id) === -1) {
                            val.push(id);
                        }
                    }

                    self.$element.val(val);
                    self.$element.trigger('change');
                });
            } else {
                var val = data.id;

                this.$element.val(val);

                this.$element.trigger('change');
            }
        };

        SelectAdapter.prototype.unselect = function (data) {
            var self = this;

            if (!this.$element.prop('multiple')) {
                return;
            }

            if ($(data.element).is('option')) {
                data.element.selected = false;

                this.$element.trigger('change');

                return;
            }

            this.current(function (currentData) {
                var val = [];

                for (var d = 0; d < currentData.length; d++) {
                    id = currentData[d].id;

                    if (id !== data.id && val.indexOf(id) === -1) {
                        val.push(id);
                    }
                }

                self.$element.val(val);

                self.$element.trigger('change');
            });
        };

        SelectAdapter.prototype.bind = function (container, $container) {
            var self = this;

            this.container = container;

            container.on('select', function (params) {
                self.select(params.data);
            });

            container.on('unselect', function (params) {
                self.unselect(params.data);
            });
        };

        SelectAdapter.prototype.destroy = function () {
            // Remove anything added to child elements
            this.$element.find('*').each(function () {
                // Remove any custom data set by Select2
                $.removeData(this, 'data');
            });
        };

        SelectAdapter.prototype.query = function (params, callback) {
            var data = [];
            var self = this;

            var $options = this.$element.children();

            $options.each(function () {
                var $option = $(this);

                if (!$option.is('option') && !$option.is('optgroup')) {
                    return;
                }

                var option = self.item($option);

                var matches = self.matches(params, option);

                if (matches !== null) {
                    data.push(matches);
                }
            });

            callback({
                results: data
            });
        };

        SelectAdapter.prototype.option = function (data) {
            var option;

            if (data.children) {
                option = document.createElement('optgroup');
                option.label = data.text;
            } else {
                option = document.createElement('option');
                option.innerText = data.text;
            }

            if (data.id) {
                option.value = data.id;
            }

            if (data.disabled) {
                option.disabled = true;
            }

            if (data.selected) {
                option.selected = true;
            }

            var $option = $(option);

            var normalizedData = this._normalizeItem(data);
            normalizedData.element = option;

            // Override the option's data with the combined data
            $.data(option, 'data', normalizedData);

            return $option;
        };

        SelectAdapter.prototype.item = function ($option) {
            var data = {};

            data = $.data($option[0], 'data');

            if (data != null) {
                return data;
            }
            
            if ($option.is('option')) {
                data = {
                    id: $option.val(),
                    text: $option.html(),
                    disabled: $option.prop('disabled'),
                    selected: $option.prop('selected'),
                    notallow: $($option).data().notallow != undefined ? $($option).data().notallow : false,
                };
            } else if ($option.is('optgroup')) {
                data = {
                    text: $option.prop('label'),
                    children: []
                };

                var $children = $option.children('option');
                var children = [];

                for (var c = 0; c < $children.length; c++) {
                    var $child = $($children[c]);

                    var child = this.item($child);

                    children.push(child);
                }

                data.children = children;
            }

            data = this._normalizeItem(data);
            data.element = $option[0];

            $.data($option[0], 'data', data);

            return data;
        };

        SelectAdapter.prototype._normalizeItem = function (item) {
            if (!$.isPlainObject(item)) {
                item = {
                    id: item,
                    text: item
                };
            }

            item = $.extend({}, {
                text: ''
            }, item);

            var defaults = {
                selected: false,
                disabled: false
            };

            if (item.id != null) {
                item.id = item.id.toString();
            }

            if (item.text != null) {
                item.text = item.text.toString();
            }

            if (item._resultId == null && item.id && this.container != null) {
                item._resultId = this.generateResultId(this.container, item);
            }

            return $.extend({}, defaults, item);
        };

        SelectAdapter.prototype.matches = function (params, data) {
            var matcher = this.options.get('matcher');

            return matcher(params, data);
        };

        return SelectAdapter;
    });

    define('select2/data/array',[
        './select',
        '../utils',
        'jquery'
    ], function (SelectAdapter, Utils, $) {
        function ArrayAdapter ($element, options) {
            var data = options.get('data') || [];

            ArrayAdapter.__super__.constructor.call(this, $element, options);

            $element.append(this.convertToOptions(data));
        }

        Utils.Extend(ArrayAdapter, SelectAdapter);

        ArrayAdapter.prototype.select = function (data) {
            var $option = this.$element.find('option[value="' + data.id + '"]');

            if ($option.length === 0) {
                $option = this.option(data);

                this.$element.append($option);
            }

            ArrayAdapter.__super__.select.call(this, data);
        };

        ArrayAdapter.prototype.convertToOptions = function (data) {
            var self = this;

            var $existing = this.$element.find('option');
            var existingIds = $existing.map(function () {
                return self.item($(this)).id;
            }).get();

            var $options = [];

            // Filter out all items except for the one passed in the argument
            function onlyItem (item) {
                return function () {
                    return $(this).val() == item.id;
                };
            }

            for (var d = 0; d < data.length; d++) {
                var item = this._normalizeItem(data[d]);

                // Skip items which were pre-loaded, only merge the data
                if (existingIds.indexOf(item.id) >= 0) {
                    var $existingOption = $existing.filter(onlyItem(item));

                    var existingData = this.item($existingOption);
                    var newData = $.extend(true, {}, existingData, item);

                    var $newOption = this.option(existingData);

                    $existingOption.replaceWith($newOption);

                    continue;
                }

                var $option = this.option(item);

                if (item.children) {
                    var $children = this.convertToOptions(item.children);

                    $option.append($children);
                }
                
                $options.push($option);
            }

            return $options;
        };

        return ArrayAdapter;
    });

    define('select2/data/ajax',[
        './array',
        '../utils',
        'jquery'
    ], function (ArrayAdapter, Utils, $) {
        function AjaxAdapter ($element, options) {
            this.ajaxOptions = options.get('ajax');

            if (this.ajaxOptions.processResults != null) {
                this.processResults = this.ajaxOptions.processResults;
            }

            ArrayAdapter.__super__.constructor.call(this, $element, options);
        }

        Utils.Extend(AjaxAdapter, ArrayAdapter);

        AjaxAdapter.prototype.processResults = function (results) {
            return results;
        };

        AjaxAdapter.prototype.query = function (params, callback) {
            var matches = [];
            var self = this;

            if (this._request) {
                this._request.abort();
                this._request = null;
            }

            var options = $.extend({
                type: 'GET'
            }, this.ajaxOptions);

            if (typeof options.url === 'function') {
                options.url = options.url(params);
            }

            if (typeof options.data === 'function') {
                options.data = options.data(params);
            }

            function request () {
                var $request = $.ajax(options);

                $request.success(function (data) {
                    var results = self.processResults(data, params);

                    if (console && console.error) {
                        // Check to make sure that the response included a `results` key.
                        if (!results || !results.results || !$.isArray(results.results)) {
                            console.error(
                                    'Select2: The AJAX results did not return an array in the ' +
                                    '`results` key of the response.'
                            );
                        }
                    }

                    callback(results);
                });

                self._request = $request;
            }

            if (this.ajaxOptions.delay && params.term !== '') {
                if (this._queryTimeout) {
                    window.clearTimeout(this._queryTimeout);
                }

                this._queryTimeout = window.setTimeout(request, this.ajaxOptions.delay);
            } else {
                request();
            }
        };

        return AjaxAdapter;
    });

    define('select2/data/tags',[
        'jquery'
    ], function ($) {
        function Tags (decorated, $element, options) {
            var tags = options.get('tags');

            var createTag = options.get('createTag');

            if (createTag !== undefined) {
                this.createTag = createTag;
            }

            decorated.call(this, $element, options);

            if ($.isArray(tags)) {
                for (var t = 0; t < tags.length; t++) {
                    var tag = tags[t];
                    var item = this._normalizeItem(tag);

                    var $option = this.option(item);

                    this.$element.append($option);
                }
            }
        }

        Tags.prototype.query = function (decorated, params, callback) {
            var self = this;

            this._removeOldTags();

            if (params.term == null || params.term === '' || params.page != null) {
                decorated.call(this, params, callback);
                return;
            }

            function wrapper (obj, child) {
                var data = obj.results;

                for (var i = 0; i < data.length; i++) {
                    var option = data[i];

                    var checkChildren = (
                        option.children != null &&
                        !wrapper({
                            results: option.children
                        }, true)
                        );

                    var checkText = option.text === params.term;

                    if (checkText || checkChildren) {
                        if (child) {
                            return false;
                        }

                        obj.data = data;
                        callback(obj);

                        return;
                    }
                }

                if (child) {
                    return true;
                }

                var tag = self.createTag(params);

                if (tag != null) {
                    var $option = self.option(tag);
                    $option.attr('data-select2-tag', true);

                    self.$element.append($option);

                    self.insertTag(data, tag);
                }

                obj.results = data;

                callback(obj);
            }

            decorated.call(this, params, wrapper);
        };

        Tags.prototype.createTag = function (decorated, params) {
            return {
                id: params.term,
                text: params.term
            };
        };

        Tags.prototype.insertTag = function (_, data, tag) {
            data.unshift(tag);
        };

        Tags.prototype._removeOldTags = function (_) {
            var tag = this._lastTag;

            var $options = this.$element.find('option[data-select2-tag]');

            $options.each(function () {
                if (this.selected) {
                    return;
                }

                $(this).remove();
            });
        };

        return Tags;
    });

    define('select2/data/tokenizer',[
        'jquery'
    ], function ($) {
        function Tokenizer (decorated, $element, options) {
            var tokenizer = options.get('tokenizer');

            if (tokenizer !== undefined) {
                this.tokenizer = tokenizer;
            }

            decorated.call(this, $element, options);
        }

        Tokenizer.prototype.bind = function (decorated, container, $container) {
            decorated.call(this, container, $container);

            this.$search =  container.dropdown.$search || container.selection.$search ||
                $container.find('.select2-search__field');
        };

        Tokenizer.prototype.query = function (decorated, params, callback) {
            var self = this;

            function select (data) {
                self.select(data);
            }

            params.term = params.term || '';

            var tokenData = this.tokenizer(params, this.options, select);

            if (tokenData.term !== params.term) {
                // Replace the search term if we have the search box
                if (this.$search.length) {
                    this.$search.val(tokenData.term);
                    this.$search.focus();
                }

                params.term = tokenData.term;
            }

            decorated.call(this, params, callback);
        };

        Tokenizer.prototype.tokenizer = function (_, params, options, callback) {
            var separators = options.get('tokenSeparators') || [];
            var term = params.term;
            var i = 0;

            var createTag = this.createTag || function (params) {
                return {
                    id: params.term,
                    text: params.term
                };
            };

            while (i < term.length) {
                var termChar = term[i];

                if (separators.indexOf(termChar) === -1) {
                    i++;

                    continue;
                }

                var part = term.substr(0, i);
                var partParams = $.extend({}, params, {
                    term: part
                });

                var data = createTag(partParams);

                callback(data);

                // Reset the term to not include the tokenized portion
                term = term.substr(i + 1) || '';
                i = 0;
            }

            return {
                term: term
            };
        };

        return Tokenizer;
    });

    define('select2/data/minimumInputLength',[

    ], function () {
        function MinimumInputLength (decorated, $e, options) {
            this.minimumInputLength = options.get('minimumInputLength');

            decorated.call(this, $e, options);
        }

        MinimumInputLength.prototype.query = function (decorated, params, callback) {
            params.term = params.term || '';

            if (params.term.length < this.minimumInputLength) {
                this.trigger('results:message', {
                    message: 'inputTooShort',
                    args: {
                        minimum: this.minimumInputLength,
                        input: params.term,
                        params: params
                    }
                });

                return;
            }

            decorated.call(this, params, callback);
        };

        return MinimumInputLength;
    });

    define('select2/data/maximumInputLength',[

    ], function () {
        function MaximumInputLength (decorated, $e, options) {
            this.maximumInputLength = options.get('maximumInputLength');

            decorated.call(this, $e, options);
        }

        MaximumInputLength.prototype.query = function (decorated, params, callback) {
            params.term = params.term || '';

            if (this.maximumInputLength > 0 &&
                params.term.length > this.maximumInputLength) {
                this.trigger('results:message', {
                    message: 'inputTooLong',
                    args: {
                        minimum: this.maximumInputLength,
                        input: params.term,
                        params: params
                    }
                });

                return;
            }

            decorated.call(this, params, callback);
        };

        return MaximumInputLength;
    });

    define('select2/data/maximumSelectionLength',[

    ], function (){
        function MaximumSelectionLength (decorated, $e, options) {
            this.maximumSelectionLength = options.get('maximumSelectionLength');

            decorated.call(this, $e, options);
        }

        MaximumSelectionLength.prototype.query =
            function (decorated, params, callback) {
                var self = this;

                this.current(function (currentData) {
                    var count = currentData != null ? currentData.length : 0;
                    if (self.maximumSelectionLength > 0 &&
                        count >= self.maximumSelectionLength) {
                        self.trigger('results:message', {
                            message: 'maximumSelected',
                            args: {
                                maximum: self.maximumSelectionLength
                            }
                        });
                        return;
                    }
                    decorated.call(self, params, callback);
                });
            };

        return MaximumSelectionLength;
    });

    define('select2/dropdown',[
        'jquery',
        './utils'
    ], function ($, Utils) {
        function Dropdown ($element, options) {
            this.$element = $element;
            this.options = options;

            Dropdown.__super__.constructor.call(this);
        }

        Utils.Extend(Dropdown, Utils.Observable);

        Dropdown.prototype.render = function () {
            var $dropdown = $(
                    '<span class="select2-dropdown">' +
                    '<span class="select2-results"></span>' +
                    '</span>'
            );

            $dropdown.attr('dir', this.options.get('dir'));

            this.$dropdown = $dropdown;

            return $dropdown;
        };

        Dropdown.prototype.position = function ($dropdown, $container) {
            // Should be implmented in subclasses
        };

        Dropdown.prototype.destroy = function () {
            // Remove the dropdown from the DOM
            this.$dropdown.remove();
        };

        Dropdown.prototype.bind = function (container, $container) {
            var self = this;

            container.on('select', function (params) {
                self._onSelect(params);
            });

            container.on('unselect', function (params) {
                self._onUnSelect(params);
            });
        };

        Dropdown.prototype._onSelect = function () {
            this.trigger('close');
        };

        Dropdown.prototype._onUnSelect = function () {
            this.trigger('close');
        };

        return Dropdown;
    });

    define('select2/dropdown/search',[
        'jquery',
        '../utils'
    ], function ($, Utils) {
        function Search () { }

        Search.prototype.render = function (decorated) {
            var $rendered = decorated.call(this);

            var $search = $(
                    '<span class="select2-search select2-search--dropdown">' +
                    '<input class="select2-search__field" type="search" tabindex="-1"' +
                    ' role="textbox" />' +
                    '</span>'
            );

            this.$searchContainer = $search;
            this.$search = $search.find('input');

            $rendered.prepend($search);

            return $rendered;
        };

        Search.prototype.bind = function (decorated, container, $container) {
            var self = this;

            decorated.call(this, container, $container);

            this.$search.on('keydown', function (evt) {
                self.trigger('keypress', evt);

                self._keyUpPrevented = evt.isDefaultPrevented();
            });

            this.$search.on('keyup', function (evt) {
                self.handleSearch(evt);
            });

            container.on('open', function () {
                self.$search.attr('tabindex', 0);

                self.$search.focus();

                window.setTimeout(function () {
                    self.$search.focus();
                }, 0);
            });

            container.on('close', function () {
                self.$search.attr('tabindex', -1);

                self.$search.val('');
            });

            container.on('results:all', function (params) {
                if (params.query.term == null || params.query.term === '') {
                    var showSearch = self.showSearch(params);

                    if (showSearch) {
                        self.$searchContainer.removeClass('select2-search--hide');
                    } else {
                        self.$searchContainer.addClass('select2-search--hide');
                    }
                }
            });
        };

        Search.prototype.handleSearch = function (evt) {
            if (!this._keyUpPrevented) {
                var input = this.$search.val();

                this.trigger('query', {
                    term: input
                });
            }

            this._keyUpPrevented = false;
        };

        Search.prototype.showSearch = function (_, params) {
            return true;
        };

        return Search;
    });

    define('select2/dropdown/hidePlaceholder',[

    ], function () {
        function HidePlaceholder (decorated, $element, options, dataAdapter) {
            this.placeholder = this.normalizePlaceholder(options.get('placeholder'));

            decorated.call(this, $element, options, dataAdapter);
        }

        HidePlaceholder.prototype.append = function (decorated, data) {
            data.results = this.removePlaceholder(data.results);

            decorated.call(this, data);
        };

        HidePlaceholder.prototype.normalizePlaceholder = function (_, placeholder) {
            if (typeof placeholder === 'string') {
                placeholder = {
                    id: '',
                    text: placeholder
                };
            }

            return placeholder;
        };

        HidePlaceholder.prototype.removePlaceholder = function (_, data) {
            var modifiedData = data.slice(0);

            for (var d = data.length - 1; d >= 0; d--) {
                var item = data[d];

                if (this.placeholder.id === item.id) {
                    modifiedData.splice(d, 1);
                }
            }

            return modifiedData;
        };

        return HidePlaceholder;
    });

    define('select2/dropdown/infiniteScroll',[
        'jquery'
    ], function ($) {
        function InfiniteScroll (decorated, $element, options, dataAdapter) {
            this.lastParams = {};

            decorated.call(this, $element, options, dataAdapter);

            this.$loadingMore = this.createLoadingMore();
            this.loading = false;
        }

        InfiniteScroll.prototype.append = function (decorated, data) {
            this.$loadingMore.remove();
            this.loading = false;

            decorated.call(this, data);

            if (this.showLoadingMore(data)) {
                this.$results.append(this.$loadingMore);
            }
        };

        InfiniteScroll.prototype.bind = function (decorated, container, $container) {
            var self = this;

            decorated.call(this, container, $container);

            container.on('query', function (params) {
                self.lastParams = params;
                self.loading = true;
            });

            container.on('query:append', function (params) {
                self.lastParams = params;
                self.loading = true;
            });

            this.$results.on('scroll', function () {
                var isLoadMoreVisible = $.contains(
                    document.documentElement,
                    self.$loadingMore[0]
                );

                if (self.loading || !isLoadMoreVisible) {
                    return;
                }

                var currentOffset = self.$results.offset().top +
                    self.$results.outerHeight(false);
                var loadingMoreOffset = self.$loadingMore.offset().top +
                    self.$loadingMore.outerHeight(false);

                if (currentOffset + 50 >= loadingMoreOffset) {
                    self.loadMore();
                }
            });
        };

        InfiniteScroll.prototype.loadMore = function () {
            this.loading = true;

            var params = $.extend({}, {page: 1}, this.lastParams);

            params.page++;

            this.trigger('query:append', params);
        };

        InfiniteScroll.prototype.showLoadingMore = function (_, data) {
            return data.pagination && data.pagination.more;
        };

        InfiniteScroll.prototype.createLoadingMore = function () {
            var $option = $(
                '<li class="option load-more" role="treeitem"></li>'
            );

            var message = this.options.get('translations').get('loadingMore');

            $option.html(message(this.lastParams));

            return $option;
        };

        return InfiniteScroll;
    });

    define('select2/dropdown/attachBody',[
        'jquery',
        '../utils'
    ], function ($, Utils) {
        function AttachBody (decorated, $element, options) {
            this.$dropdownParent = options.get('dropdownParent') || document.body;

            decorated.call(this, $element, options);
        }

        AttachBody.prototype.bind = function (decorated, container, $container) {
            var self = this;

            var setupResultsEvents = false;

            decorated.call(this, container, $container);

            container.on('open', function () {
                self._showDropdown();
                self._attachPositioningHandler(container);

                if (!setupResultsEvents) {
                    setupResultsEvents = true;

                    container.on('results:all', function () {
                        self._positionDropdown();
                        self._resizeDropdown();
                    });

                    container.on('results:append', function () {
                        self._positionDropdown();
                        self._resizeDropdown();
                    });
                }
            });

            container.on('close', function () {
                self._hideDropdown();
                self._detachPositioningHandler(container);
            });

            this.$dropdownContainer.on('mousedown', function (evt) {
                evt.stopPropagation();
            });
        };

        AttachBody.prototype.position = function (decorated, $dropdown, $container) {
            // Clone all of the container classes
            $dropdown.attr('class', $container.attr('class'));

            $dropdown.removeClass('select2');
            $dropdown.addClass('select2-container--open');

            $dropdown.css({
                position: 'absolute',
                top: -999999
            });

            this.$container = $container;
        };

        AttachBody.prototype.render = function (decorated) {
            var $container = $('<span></span>');

            var $dropdown = decorated.call(this);
            $container.append($dropdown);

            this.$dropdownContainer = $container;

            return $container;
        };

        AttachBody.prototype._hideDropdown = function (decorated) {
            this.$dropdownContainer.detach();
        };

        AttachBody.prototype._attachPositioningHandler = function (container) {
            var self = this;

            var scrollEvent = 'scroll.select2.' + container.id;
            var resizeEvent = 'resize.select2.' + container.id;
            var orientationEvent = 'orientationchange.select2.' + container.id;

            $watchers = this.$container.parents().filter(Utils.hasScroll);
            $watchers.each(function () {
                $(this).data('select2-scroll-position', {
                    x: $(this).scrollLeft(),
                    y: $(this).scrollTop()
                });
            });

            $watchers.on(scrollEvent, function (ev) {
                var position = $(this).data('select2-scroll-position');
                $(this).scrollTop(position.y);
            });

            $(window).on(scrollEvent + ' ' + resizeEvent + ' ' + orientationEvent,
                function (e) {
                    self._positionDropdown();
                    self._resizeDropdown();
                });
        };

        AttachBody.prototype._detachPositioningHandler = function (container) {
            var scrollEvent = 'scroll.select2.' + container.id;
            var resizeEvent = 'resize.select2.' + container.id;
            var orientationEvent = 'orientationchange.select2.' + container.id;

            $watchers = this.$container.parents().filter(Utils.hasScroll);
            $watchers.off(scrollEvent);

            $(window).off(scrollEvent + ' ' + resizeEvent + ' ' + orientationEvent);
        };

        AttachBody.prototype._positionDropdown = function () {
            var $window = $(window);

            var isCurrentlyAbove = this.$dropdown.hasClass('select2-dropdown--above');
            var isCurrentlyBelow = this.$dropdown.hasClass('select2-dropdown--below');

            var newDirection = null;

            var position = this.$container.position();
            var offset = this.$container.offset();

            offset.bottom = offset.top + this.$container.outerHeight(false);

            var container = {
                height: this.$container.outerHeight(false)
            };

            container.top = offset.top;
            container.bottom = offset.top + container.height;

            var dropdown = {
                height: this.$dropdown.outerHeight(false)
            };

            var viewport = {
                top: $window.scrollTop(),
                bottom: $window.scrollTop() + $window.height()
            };

            var enoughRoomAbove = false; //viewport.top < (offset.top - dropdown.height);
            var enoughRoomBelow = true; //viewport.bottom > (offset.bottom + dropdown.height);

            var css = {
                left: offset.left,
                top: container.bottom
            };

            if (!isCurrentlyAbove && !isCurrentlyBelow) {
                newDirection = 'below';
            }

            if (!enoughRoomBelow && enoughRoomAbove && !isCurrentlyAbove) {
                newDirection = 'above';
            } else if (!enoughRoomAbove && enoughRoomBelow && isCurrentlyAbove) {
                newDirection = 'below';
            }

            if (newDirection == 'above' ||
                (isCurrentlyAbove && newDirection !== 'below')) {
                css.top = container.top - dropdown.height;
            }

            if (newDirection != null) {
                this.$dropdown
                    .removeClass('select2-dropdown--below select2-dropdown--above')
                    .addClass('select2-dropdown--' + newDirection);
                this.$container
                    .removeClass('select2-container--below select2-container--above')
                    .addClass('select2-container--' + newDirection);
            }

            this.$dropdownContainer.css(css);
        };

        AttachBody.prototype._resizeDropdown = function () {
            this.$dropdownContainer.width();

            this.$dropdown.css({
                width: this.$container.outerWidth(false) + 'px'
            });
        };

        AttachBody.prototype._showDropdown = function (decorated) {
            this.$dropdownContainer.appendTo(this.$dropdownParent);

            this._positionDropdown();
            this._resizeDropdown();
        };

        return AttachBody;
    });

    define('select2/dropdown/minimumResultsForSearch',[

    ], function () {
        function countResults (data) {
            count = 0;

            for (var d = 0; d < data.length; d++) {
                var item = data[d];

                if (item.children) {
                    count += countResults(item.children);
                } else {
                    count++;
                }
            }

            return count;
        }

        function MinimumResultsForSearch (decorated, $element, options, dataAdapter) {
            this.minimumResultsForSearch = options.get('minimumResultsForSearch');

            decorated.call(this, $element, options, dataAdapter);
        }

        MinimumResultsForSearch.prototype.showSearch = function (decorated, params) {
            if (countResults(params.data.results) < this.minimumResultsForSearch) {
                return false;
            }

            return decorated.call(this, params);
        };

        return MinimumResultsForSearch;
    });

    define('select2/dropdown/selectOnClose',[

    ], function () {
        function SelectOnClose () { }

        SelectOnClose.prototype.bind = function (decorated, container, $container) {
            var self = this;

            decorated.call(this, container, $container);

            container.on('close', function () {
                self._handleSelectOnClose();
            });
        };

        SelectOnClose.prototype._handleSelectOnClose = function () {
            var $highlightedResults = this.getHighlightedResults();

            if ($highlightedResults.length < 1) {
                return;
            }

            $highlightedResults.trigger('mouseup');
        };

        return SelectOnClose;
    });

    define('select2/i18n/en',[],function () {
        // English
        return {
            errorLoading: function () {
                return 'The results could not be loaded.';
            },
            inputTooLong: function (args) {
                var overChars = args.input.length - args.maximum;

                var message = 'Please delete ' + overChars + ' character';

                if (overChars != 1) {
                    message += 's';
                }

                return message;
            },
            inputTooShort: function (args) {
                var remainingChars = args.minimum - args.input.length;

                var message = 'Please enter ' + remainingChars + ' or more characters';

                return message;
            },
            loadingMore: function () {
                return 'Loading more results…';
            },
            maximumSelected: function (args) {
                var message = 'You can only select ' + args.maximum + ' item';

                if (args.maximum != 1) {
                    message += 's';
                }

                return message;
            },
            noResults: function () {
                return 'No results found';
            },
            searching: function () {
                return 'Searching…';
            }
        };
    });

    define('select2/defaults',[
        'jquery',
        './results',

        './selection/single',
        './selection/multiple',
        './selection/placeholder',
        './selection/allowClear',
        './selection/search',
        './selection/eventRelay',

        './utils',
        './translation',
        './diacritics',

        './data/select',
        './data/array',
        './data/ajax',
        './data/tags',
        './data/tokenizer',
        './data/minimumInputLength',
        './data/maximumInputLength',
        './data/maximumSelectionLength',

        './dropdown',
        './dropdown/search',
        './dropdown/hidePlaceholder',
        './dropdown/infiniteScroll',
        './dropdown/attachBody',
        './dropdown/minimumResultsForSearch',
        './dropdown/selectOnClose',

        './i18n/en'
    ], function ($, ResultsList,

                 SingleSelection, MultipleSelection, Placeholder, AllowClear,
                 SelectionSearch, EventRelay,

                 Utils, Translation, DIACRITICS,

                 SelectData, ArrayData, AjaxData, Tags, Tokenizer,
                 MinimumInputLength, MaximumInputLength, MaximumSelectionLength,

                 Dropdown, DropdownSearch, HidePlaceholder, InfiniteScroll,
                 AttachBody, MinimumResultsForSearch, SelectOnClose,

                 EnglishTranslation) {
        function Defaults () {
            this.reset();
        }

        Defaults.prototype.apply = function (options) {
            options = $.extend({}, this.defaults, options);

            if (options.dataAdapter == null) {
                if (options.ajax != null) {
                    options.dataAdapter = AjaxData;
                } else if (options.data != null) {
                    options.dataAdapter = ArrayData;
                } else {
                    options.dataAdapter = SelectData;
                }

                if (options.minimumInputLength > 0) {
                    options.dataAdapter = Utils.Decorate(
                        options.dataAdapter,
                        MinimumInputLength
                    );
                }

                if (options.maximumInputLength > 0) {
                    options.dataAdapter = Utils.Decorate(
                        options.dataAdapter,
                        MaximumInputLength
                    );
                }

                if (options.maximumSelectionLength > 0) {
                    options.dataAdapter = Utils.Decorate(
                        options.dataAdapter,
                        MaximumSelectionLength
                    );
                }

                if (options.tags != null) {
                    options.dataAdapter = Utils.Decorate(options.dataAdapter, Tags);
                }

                if (options.tokenSeparators != null || options.tokenizer != null) {
                    options.dataAdapter = Utils.Decorate(
                        options.dataAdapter,
                        Tokenizer
                    );
                }

                if (options.query != null) {
                    var Query = require(options.amdBase + 'compat/query');

                    options.dataAdapter = Utils.Decorate(
                        options.dataAdapter,
                        Query
                    );
                }

                if (options.initSelection != null) {
                    var InitSelection = require(options.amdBase + 'compat/initSelection');

                    options.dataAdapter = Utils.Decorate(
                        options.dataAdapter,
                        InitSelection
                    );
                }
            }

            if (options.resultsAdapter == null) {
                options.resultsAdapter = ResultsList;

                if (options.ajax != null) {
                    options.resultsAdapter = Utils.Decorate(
                        options.resultsAdapter,
                        InfiniteScroll
                    );
                }

                if (options.placeholder != null) {
                    options.resultsAdapter = Utils.Decorate(
                        options.resultsAdapter,
                        HidePlaceholder
                    );
                }

                if (options.selectOnClose) {
                    options.resultsAdapter = Utils.Decorate(
                        options.resultsAdapter,
                        SelectOnClose
                    );
                }
            }

            if (options.dropdownAdapter == null) {
                if (options.multiple) {
                    options.dropdownAdapter = Dropdown;
                } else {
                    var SearchableDropdown = Utils.Decorate(Dropdown, DropdownSearch);

                    options.dropdownAdapter = SearchableDropdown;
                }

                if (options.minimumResultsForSearch > 0) {
                    options.dropdownAdapter = Utils.Decorate(
                        options.dropdownAdapter,
                        MinimumResultsForSearch
                    );
                }

                options.dropdownAdapter = Utils.Decorate(
                    options.dropdownAdapter,
                    AttachBody
                );
            }

            if (options.selectionAdapter == null) {
                if (options.multiple) {
                    options.selectionAdapter = MultipleSelection;
                } else {
                    options.selectionAdapter = SingleSelection;
                }

                // Add the placeholder mixin if a placeholder was specified
                if (options.placeholder != null) {
                    options.selectionAdapter = Utils.Decorate(
                        options.selectionAdapter,
                        Placeholder
                    );

                    if (options.allowClear) {
                        options.selectionAdapter = Utils.Decorate(
                            options.selectionAdapter,
                            AllowClear
                        );
                    }
                }

                if (options.multiple) {
                    options.selectionAdapter = Utils.Decorate(
                        options.selectionAdapter,
                        SelectionSearch
                    );
                }

                options.selectionAdapter = Utils.Decorate(
                    options.selectionAdapter,
                    EventRelay
                );
            }

            if (typeof options.language === 'string') {
                // Check if the lanugage is specified with a region
                if (options.language.indexOf('-') > 0) {
                    // Extract the region information if it is included
                    var languageParts = options.language.split('-');
                    var baseLanguage = languageParts[0];

                    options.language = [options.language, baseLanguage];
                } else {
                    options.language = [options.language];
                }
            }

            if ($.isArray(options.language)) {
                var languages = new Translation();
                options.language.push('en');

                var languageNames = options.language;

                for (var l = 0; l < languageNames.length; l++) {
                    var name = languageNames[l];
                    var language = {};

                    try {
                        // Try to load it with the original name
                        language = Translation.loadPath(name);
                    } catch (e) {
                        try {
                            // If we couldn't load it, check if it wasn't the full path
                            name = this.defaults.amdLanguageBase + name;
                            language = Translation.loadPath(name);
                        } catch (ex) {
                            // The translation could not be loaded at all. Sometimes this is
                            // because of a configuration problem, other times this can be
                            // because of how Select2 helps load all possible translation files.
                            if (console && console.warn) {
                                console.warn(
                                        'Select2: The lanugage file for "' + name + '" could not be ' +
                                        'automatically loaded. A fallback will be used instead.'
                                );
                            }

                            continue;
                        }
                    }

                    languages.extend(language);
                }

                options.translations = languages;
            } else {
                options.translations = new Translation(options.language);
            }

            return options;
        };

        Defaults.prototype.reset = function () {
            function stripDiacritics (text) {
                // Used 'uni range + named function' from http://jsperf.com/diacritics/18
                function match(a) {
                    return DIACRITICS[a] || a;
                }

                return text.replace(/[^\u0000-\u007E]/g, match);
            }

            function matcher (params, data) {
                // Always return the object if there is nothing to compare
                if ($.trim(params.term) === '') {
                    return data;
                }

                // Do a recursive check for options with children
                if (data.children && data.children.length > 0) {
                    // Clone the data object if there are children
                    // This is required as we modify the object to remove any non-matches
                    var match = $.extend(true, {}, data);

                    // Check each child of the option
                    for (var c = data.children.length - 1; c >= 0; c--) {
                        var child = data.children[c];

                        var matches = matcher(params, child);

                        // If there wasn't a match, remove the object in the array
                        if (matches == null) {
                            match.children.splice(c, 1);
                        }
                    }

                    // If any children matched, return the new object
                    if (match.children.length > 0) {
                        return match;
                    }

                    // If there were no matching children, check just the plain object
                    return matcher(params, match);
                }

                var original = stripDiacritics(data.text).toUpperCase();
                var term = stripDiacritics(params.term).toUpperCase();

                // Check if the text contains the term
                if (original.indexOf(term) > -1) {
                    return data;
                }

                // If it doesn't contain the term, don't return anything
                return null;
            }

            this.defaults = {
                amdBase: 'select2/',
                amdLanguageBase: 'select2/i18n/',
                language: EnglishTranslation,
                matcher: matcher,
                minimumInputLength: 0,
                maximumInputLength: 0,
                maximumSelectionLength: 0,
                minimumResultsForSearch: 0,
                selectOnClose: false,
                sorter: function (data) {
                    return data;
                },
                templateResult: function (result) {
                    return result.text;
                },
                templateSelection: function (selection) {
                    return selection.text;
                },
                theme: 'default',
                width: 'resolve'
            };
        };

        Defaults.prototype.set = function (key, value) {
            var camelKey = $.camelCase(key);

            var data = {};
            data[camelKey] = value;

            var convertedData = Utils._convertData(data);

            $.extend(this.defaults, convertedData);
        };

        var defaults = new Defaults();

        return defaults;
    });

    define('select2/options',[
        'jquery',
        './defaults',
        './utils'
    ], function ($, Defaults, Utils) {
        function Options (options, $element) {
            this.options = options;

            if ($element != null) {
                this.fromElement($element);
            }

            this.options = Defaults.apply(this.options);
        }

        Options.prototype.fromElement = function ($e) {
            var excludedData = ['select2'];

            if (this.options.multiple == null) {
                this.options.multiple = $e.prop('multiple');
            }

            if (this.options.disabled == null) {
                this.options.disabled = $e.prop('disabled');
            }

            if (this.options.language == null) {
                if ($e.prop('lang')) {
                    this.options.language = $e.prop('lang').toLowerCase();
                } else if ($e.closest('[lang]').prop('lang')) {
                    this.options.language = $e.closest('[lang]').prop('lang');
                }
            }

            if (this.options.dir == null) {
                if ($e.prop('dir')) {
                    this.options.dir = $e.prop('dir');
                } else if ($e.closest('[dir]').prop('dir')) {
                    this.options.dir = $e.closest('[dir]').prop('dir');
                } else {
                    this.options.dir = 'ltr';
                }
            }

            $e.prop('disabled', this.options.disabled);
            $e.prop('multiple', this.options.multiple);

            if ($e.data('select2-tags')) {
                if (console && console.warn) {
                    console.warn(
                            'Select2: The `data-select2-tags` attribute has been changed to ' +
                            'use the `data-data` and `data-tags="true"` attributes and will be ' +
                            'removed in future versions of Select2.'
                    );
                }

                $e.data('data', $e.data('select2-tags'));
                $e.data('tags', true);
            }

            if ($e.data('ajax-url')) {
                if (console && console.warn) {
                    console.warn(
                            'Select2: The `data-ajax-url` attribute has been changed to ' +
                            '`data-ajax--url` and support for the old attribute will be removed' +
                            ' in future versions of Select2.'
                    );
                }

                $e.data('ajax--url', $e.data('ajax-url'));
            }

            var data = $e.data();

            data = Utils._convertData(data);

            for (var key in data) {
                if (excludedData.indexOf(key) > -1) {
                    continue;
                }

                if ($.isPlainObject(this.options[key])) {
                    $.extend(this.options[key], data[key]);
                } else {
                    this.options[key] = data[key];
                }
            }

            return this;
        };

        Options.prototype.get = function (key) {
            return this.options[key];
        };

        Options.prototype.set = function (key, val) {
            this.options[key] = val;
        };

        return Options;
    });

    define('select2/core',[
        'jquery',
        './options',
        './utils',
        './keys'
    ], function ($, Options, Utils, KEYS) {
        var Select2 = function ($element, options) {
            if ($element.data('select2') != null) {
                $element.data('select2').destroy();
            }

            this.$element = $element;

            this.id = this._generateId($element);

            options = options || {};

            this.options = new Options(options, $element);

            Select2.__super__.constructor.call(this);

            // Set up containers and adapters

            var DataAdapter = this.options.get('dataAdapter');
            this.data = new DataAdapter($element, this.options);

            var $container = this.render();

            this._placeContainer($container);

            var SelectionAdapter = this.options.get('selectionAdapter');
            this.selection = new SelectionAdapter($element, this.options);
            this.$selection = this.selection.render();

            this.selection.position(this.$selection, $container);

            var DropdownAdapter = this.options.get('dropdownAdapter');
            this.dropdown = new DropdownAdapter($element, this.options);
            this.$dropdown = this.dropdown.render();

            this.dropdown.position(this.$dropdown, $container);

            var ResultsAdapter = this.options.get('resultsAdapter');
            this.results = new ResultsAdapter($element, this.options, this.data);
            this.$results = this.results.render();

            this.results.position(this.$results, this.$dropdown);

            // Bind events

            var self = this;

            // Bind the container to all of the adapters
            this._bindAdapters();

            // Register any DOM event handlers
            this._registerDomEvents();

            // Register any internal event handlers
            this._registerDataEvents();
            this._registerSelectionEvents();
            this._registerDropdownEvents();
            this._registerResultsEvents();
            this._registerEvents();

            // Set the initial state
            this.data.current(function (initialData) {
                self.trigger('selection:update', {
                    data: initialData
                });
            });

            // Hide the original select
            $element.hide();

            // Synchronize any monitored attributes
            this._syncAttributes();

            this._tabindex = $element.attr('tabindex') || 0;

            $element.attr('tabindex', '-1');

            $element.data('select2', this);
        };

        Utils.Extend(Select2, Utils.Observable);

        Select2.prototype._generateId = function ($element) {
            var id = '';

            if ($element.attr('id') != null) {
                id = $element.attr('id');
            } else if ($element.attr('name') != null) {
                id = $element.attr('name') + '-' + Utils.generateChars(2);
            } else {
                id = Utils.generateChars(4);
            }

            id = 'select2-' + id;

            return id;
        };

        Select2.prototype._placeContainer = function ($container) {
            $container.insertAfter(this.$element);

            var width = this._resolveWidth(this.$element, this.options.get('width'));

            if (width != null) {
                $container.css('width', width);
            }
        };

        Select2.prototype._resolveWidth = function ($element, method) {
            var WIDTH = /^width:(([-+]?([0-9]*\.)?[0-9]+)(px|em|ex|%|in|cm|mm|pt|pc))/i;

            if (method == 'resolve') {
                var styleWidth = this._resolveWidth($element, 'style');

                if (styleWidth != null) {
                    return styleWidth;
                }

                return this._resolveWidth($element, 'element');
            }

            if (method == 'element') {
                var elementWidth = $element.outerWidth(false);

                if (elementWidth <= 0) {
                    return 'auto';
                }

                return elementWidth + 'px';
            }

            if (method == 'style') {
                var style = $element.attr('style');

                if (typeof(style) !== 'string') {
                    return null;
                }

                var attrs = style.split(';');

                for (i = 0, l = attrs.length; i < l; i = i + 1) {
                    attr = attrs[i].replace(/\s/g, '');
                    var matches = attr.match(WIDTH);

                    if (matches !== null && matches.length >= 1) {
                        return matches[1];
                    }
                }

                return null;
            }

            return method;
        };

        Select2.prototype._bindAdapters = function () {
            this.data.bind(this, this.$container);
            this.selection.bind(this, this.$container);

            this.dropdown.bind(this, this.$container);
            this.results.bind(this, this.$container);
        };

        Select2.prototype._registerDomEvents = function () {
            var self = this;

            this.$element.on('change.select2', function () {
                self.data.current(function (data) {
                    self.trigger('selection:update', {
                        data: data
                    });
                });
            });

            this._sync = Utils.bind(this._syncAttributes, this);

            if (this.$element[0].attachEvent) {
                this.$element[0].attachEvent('onpropertychange', this._sync);
            }

            var observer = window.MutationObserver ||
                    window.WebKitMutationObserver ||
                    window.MozMutationObserver
                ;

            if (observer != null) {
                this._observer = new observer(function (mutations) {
                    $.each(mutations, self._sync);
                });
                this._observer.observe(this.$element[0], {
                    attributes: true,
                    subtree: false
                });
            }
        };

        Select2.prototype._registerDataEvents = function () {
            var self = this;

            this.data.on('*', function (name, params) {
                self.trigger(name, params);
            });
        };

        Select2.prototype._registerSelectionEvents = function () {
            var self = this;
            var nonRelayEvents = ['toggle'];

            this.selection.on('toggle', function () {
                self.toggleDropdown();
            });

            this.selection.on('*', function (name, params) {
                if (nonRelayEvents.indexOf(name) !== -1) {
                    return;
                }

                self.trigger(name, params);
            });
        };

        Select2.prototype._registerDropdownEvents = function () {
            var self = this;

            this.dropdown.on('*', function (name, params) {
                self.trigger(name, params);
            });
        };

        Select2.prototype._registerResultsEvents = function () {
            var self = this;

            this.results.on('*', function (name, params) {
                self.trigger(name, params);
            });
        };

        Select2.prototype._registerEvents = function () {
            var self = this;

            this.on('open', function () {
                self.$container.addClass('select2-container--open');
            });

            this.on('close', function () {
                self.$container.removeClass('select2-container--open');
            });

            this.on('enable', function () {
                self.$container.removeClass('select2-container--disabled');
            });

            this.on('disable', function () {
                self.$container.addClass('select2-container--disabled');
            });

            this.on('query', function (params) {
                this.data.query(params, function (data) {
                    self.trigger('results:all', {
                        data: data,
                        query: params
                    });
                });
            });

            this.on('query:append', function (params) {
                this.data.query(params, function (data) {
                    self.trigger('results:append', {
                        data: data,
                        query: params
                    });
                });
            });

            this.on('keypress', function (evt) {
                var key = evt.which;

                if (self.isOpen()) {
                    if (key === KEYS.ENTER) {
                        self.trigger('results:select');

                        evt.preventDefault();
                    } else if (key === KEYS.UP) {
                        self.trigger('results:previous');

                        evt.preventDefault();
                    } else if (key === KEYS.DOWN) {
                        self.trigger('results:next');

                        evt.preventDefault();
                    } else if (key === KEYS.ESC || key === KEYS.TAB) {
                        self.close();

                        evt.preventDefault();
                    }
                } else {
                    if (key === KEYS.ENTER || key === KEYS.SPACE ||
                        ((key === KEYS.DOWN || key === KEYS.UP) && evt.altKey)) {
                        self.open();

                        evt.preventDefault();
                    }
                }
            });
        };

        Select2.prototype._syncAttributes = function () {
            this.options.set('disabled', this.$element.prop('disabled'));

            if (this.options.get('disabled')) {
                if (this.isOpen()) {
                    this.close();
                }

                this.trigger('disable');
            } else {
                this.trigger('enable');
            }
        };

        /**
         * Override the trigger method to automatically trigger pre-events when
         * there are events that can be prevented.
         */
        Select2.prototype.trigger = function (name, args) {
            var actualTrigger = Select2.__super__.trigger;
            var preTriggerMap = {
                'open': 'opening',
                'close': 'closing',
                'select': 'selecting',
                'unselect': 'unselecting'
            };

            if (name in preTriggerMap) {
                var preTriggerName = preTriggerMap[name];
                var preTriggerArgs = {
                    prevented: false,
                    name: name,
                    args: args
                };

                actualTrigger.call(this, preTriggerName, preTriggerArgs);

                if (preTriggerArgs.prevented) {
                    args.prevented = true;

                    return;
                }
            }

            actualTrigger.call(this, name, args);
        };

        Select2.prototype.toggleDropdown = function () {
            if (this.options.get('disabled')) {
                return;
            }

            if (this.isOpen()) {
                this.close();
            } else {
                this.open();
            }
        };

        Select2.prototype.open = function () {
            if (this.isOpen()) {
                return;
            }

            this.trigger('query', {});

            this.trigger('open');
        };

        Select2.prototype.close = function () {
            if (!this.isOpen()) {
                return;
            }

            this.trigger('close');
        };

        Select2.prototype.isOpen = function () {
            return this.$container.hasClass('select2-container--open');
        };

        Select2.prototype.enable = function (args) {
            if (console && console.warn) {
                console.warn(
                        'Select2: The `select2("enable")` method has been deprecated and will' +
                        ' be removed in later Select2 versions. Use $element.prop("disabled")' +
                        ' instead.'
                );
            }

            if (args.length === 0) {
                args = [true];
            }

            var disabled = !args[0];

            this.$element.prop('disabled', disabled);
        };

        Select2.prototype.val = function (args) {
            if (console && console.warn) {
                console.warn(
                        'Select2: The `select2("val")` method has been deprecated and will be' +
                        ' removed in later Select2 versions. Use $element.val() instead.'
                );
            }

            if (args.length === 0) {
                return this.$element.val();
            }

            var newVal = args[0];

            if ($.isArray(newVal)) {
                newVal = $.map(newVal, function (obj) {
                    return obj.toString();
                });
            }

            this.$element.val(newVal).trigger('change');
        };

        Select2.prototype.destroy = function () {
            this.$container.remove();

            if (this.$element[0].detachEvent) {
                this.$element[0].detachEvent('onpropertychange', this._sync);
            }

            if (this._observer != null) {
                this._observer.disconnect();
                this._observer = null;
            }

            this._sync = null;

            this.$element.off('.select2');
            this.$element.attr('tabindex', this._tabindex);

            this.$element.show();
            this.$element.removeData('select2');

            this.data.destroy();
            this.selection.destroy();
            this.dropdown.destroy();
            this.results.destroy();

            this.data = null;
            this.selection = null;
            this.dropdown = null;
            this.results = null;
        };

        Select2.prototype.render = function () {
            var $container = $(
                    '<span class="select2 select2-container">' +
                    '<span class="selection"></span>' +
                    '<span class="dropdown-wrapper" aria-hidden="true"></span>' +
                    '</span>'
            );

            $container.attr('dir', this.options.get('dir'));

            this.$container = $container;

            this.$container.addClass('select2-container--' + this.options.get('theme'));

            $container.data('element', this.$element);

            return $container;
        };

        return Select2;
    });

    define('jquery.select2',[
        'jquery',
        './select2/core',
        './select2/defaults'
    ], function ($, Select2, Defaults) {
        // Force jQuery.mousewheel to be loaded if it hasn't already
        try {
            require('jquery.mousewheel');
        } catch (Exception) { }

        if ($.fn.select2 == null) {
            $.fn.select2 = function (options) {
                options = options || {};

                if (typeof options === 'object') {
                    this.each(function () {
                        var instanceOptions = $.extend({}, options, true);

                        var instance = new Select2($(this), instanceOptions);
                    });

                    return this;
                } else if (typeof options === 'string') {
                    var instance = this.data('select2');
                    var args = Array.prototype.slice.call(arguments, 1);

                    return instance[options](args);
                } else {
                    throw new Error('Invalid arguments for Select2: ' + options);
                }
            };
        }

        if ($.fn.select2.defaults == null) {
            $.fn.select2.defaults = Defaults;
        }

        return Select2;
    });

    require('jquery.select2'); jQuery.fn.select2.amd = { define: define, require: require }; }());

/*!
 * Clamp.js 0.5.1
 *
 * Copyright 2011-2013, Joseph Schmitt http://joe.sh
 * Released under the WTFPL license
 * http://sam.zoy.org/wtfpl/
 */

(function(){
    /**
     * Clamps a text node.
     * @param {HTMLElement} element. Element containing the text node to clamp.
     * @param {Object} options. Options to pass to the clamper.
     */
    function clamp(element, options) {
        options = options || {};

        var self = this,
            win = window,
            opt = {
                clamp:              options.clamp || 2,
                useNativeClamp:     typeof(options.useNativeClamp) != 'undefined' ? options.useNativeClamp : true,
                splitOnChars:       options.splitOnChars || ['.', '-', '–', '—', ' '], //Split on sentences (periods), hypens, en-dashes, em-dashes, and words (spaces).
                animate:            options.animate || false,
                truncationChar:     options.truncationChar || '…',
                truncationHTML:     options.truncationHTML
            },

            sty = element.style,
            originalText = element.innerHTML,

            supportsNativeClamp = typeof(element.style.webkitLineClamp) != 'undefined',
            clampValue = opt.clamp,
            isCSSValue = clampValue.indexOf && (clampValue.indexOf('px') > -1 || clampValue.indexOf('em') > -1),
            truncationHTMLContainer;

        if (opt.truncationHTML) {
            truncationHTMLContainer = document.createElement('span');
            truncationHTMLContainer.innerHTML = opt.truncationHTML;
        }


// UTILITY FUNCTIONS __________________________________________________________

        /**
         * Return the current style for an element.
         * @param {HTMLElement} elem The element to compute.
         * @param {string} prop The style property.
         * @returns {number}
         */
        function computeStyle(elem, prop) {
            if (!win.getComputedStyle) {
                win.getComputedStyle = function(el, pseudo) {
                    this.el = el;
                    this.getPropertyValue = function(prop) {
                        var re = /(\-([a-z]){1})/g;
                        if (prop == 'float') prop = 'styleFloat';
                        if (re.test(prop)) {
                            prop = prop.replace(re, function () {
                                return arguments[2].toUpperCase();
                            });
                        }
                        return el.currentStyle && el.currentStyle[prop] ? el.currentStyle[prop] : null;
                    }
                    return this;
                }
            }

            return win.getComputedStyle(elem, null).getPropertyValue(prop);
        }

        /**
         * Returns the maximum number of lines of text that should be rendered based
         * on the current height of the element and the line-height of the text.
         */
        function getMaxLines(height) {
            var availHeight = height || element.clientHeight,
                lineHeight = getLineHeight(element);

            return Math.max(Math.floor(availHeight/lineHeight), 0);
        }

        /**
         * Returns the maximum height a given element should have based on the line-
         * height of the text and the given clamp value.
         */
        function getMaxHeight(clmp) {
            var lineHeight = getLineHeight(element);
            return lineHeight * clmp;
        }

        /**
         * Returns the line-height of an element as an integer.
         */
        function getLineHeight(elem) {
            var lh = computeStyle(elem, 'line-height');
            if (lh == 'normal') {
                // Normal line heights vary from browser to browser. The spec recommends
                // a value between 1.0 and 1.2 of the font size. Using 1.1 to split the diff.
                lh = parseInt(computeStyle(elem, 'font-size')) * 1.2;
            }
            return parseInt(lh);
        }


// MEAT AND POTATOES (MMMM, POTATOES...) ______________________________________
        var splitOnChars = opt.splitOnChars.slice(0),
            splitChar = splitOnChars[0],
            chunks,
            lastChunk;

        /**
         * Gets an element's last child. That may be another node or a node's contents.
         */
        function getLastChild(elem) {
            //Current element has children, need to go deeper and get last child as a text node
            if (elem.lastChild.children && elem.lastChild.children.length > 0) {
                return getLastChild(Array.prototype.slice.call(elem.children).pop());
            }
            //This is the absolute last child, a text node, but something's wrong with it. Remove it and keep trying
            else if (!elem.lastChild || !elem.lastChild.nodeValue || elem.lastChild.nodeValue == '' || elem.lastChild.nodeValue == opt.truncationChar) {
                elem.lastChild.parentNode.removeChild(elem.lastChild);
                return getLastChild(element);
            }
            //This is the last child we want, return it
            else {
                return elem.lastChild;
            }
        }

        /**
         * Removes one character at a time from the text until its width or
         * height is beneath the passed-in max param.
         */
        function truncate(target, maxHeight) {
            if (!maxHeight) {return;}

            /**
             * Resets global variables.
             */
            function reset() {
                splitOnChars = opt.splitOnChars.slice(0);
                splitChar = splitOnChars[0];
                chunks = null;
                lastChunk = null;
            }

            var nodeValue = target.nodeValue.replace(opt.truncationChar, '');

            //Grab the next chunks
            if (!chunks) {
                //If there are more characters to try, grab the next one
                if (splitOnChars.length > 0) {
                    splitChar = splitOnChars.shift();
                }
                //No characters to chunk by. Go character-by-character
                else {
                    splitChar = '';
                }

                chunks = nodeValue.split(splitChar);
            }

            //If there are chunks left to remove, remove the last one and see if
            // the nodeValue fits.
            if (chunks.length > 1) {
                // console.log('chunks', chunks);
                lastChunk = chunks.pop();
                // console.log('lastChunk', lastChunk);
                applyEllipsis(target, chunks.join(splitChar));
            }
            //No more chunks can be removed using this character
            else {
                chunks = null;
            }

            //Insert the custom HTML before the truncation character
            if (truncationHTMLContainer) {
                target.nodeValue = target.nodeValue.replace(opt.truncationChar, '');
                element.innerHTML = target.nodeValue + ' ' + truncationHTMLContainer.innerHTML + opt.truncationChar;
            }

            //Search produced valid chunks
            if (chunks) {
                /* Check if splitChar is ' ' */
                if (splitOnChars.length >= 0 && splitChar === ' ') {
                    do {
                        applyEllipsis(target, chunks.join(splitChar) + splitChar + lastChunk);
                        lastChunk = chunks.pop();
                    }
                    while(element.clientHeight > maxHeight);

                    return element.innerHTML;
                }

                //It fits
                if (element.clientHeight < maxHeight) {
                    //There's still more characters to try splitting on, not quite done yet
                    if (splitOnChars.length >= 0 && splitChar != '') {
                        applyEllipsis(target, chunks.join(splitChar) + splitChar + lastChunk);
                        chunks = null;
                    }
                    //Finished!
                    else {
                        return element.innerHTML;
                    }
                }
            }
            //No valid chunks produced
            else {
                //No valid chunks even when splitting by letter, time to move
                //on to the next node
                if (splitChar == '') {
                    applyEllipsis(target, '');
                    target = getLastChild(element);

                    reset();
                }
            }

            //If you get here it means still too big, let's keep truncating
            if (opt.animate) {
                setTimeout(function() {
                    truncate(target, maxHeight);
                }, opt.animate === true ? 10 : opt.animate);
            }
            else {
                return truncate(target, maxHeight);
            }
        }

        function applyEllipsis(elem, str) {
            elem.nodeValue = str + opt.truncationChar;
        }


// CONSTRUCTOR ________________________________________________________________

        if (clampValue == 'auto') {
            clampValue = getMaxLines();
        }
        else if (isCSSValue) {
            clampValue = getMaxLines(parseInt(clampValue));
        }

        var clampedText;
        if (supportsNativeClamp && opt.useNativeClamp) {
            sty.overflow = 'hidden';
            sty.textOverflow = 'ellipsis';
            sty.webkitBoxOrient = 'vertical';
            sty.display = '-webkit-box';
            sty.webkitLineClamp = clampValue;

            if (isCSSValue) {
                sty.height = opt.clamp + 'px';
            }
        }
        else {
            var height = getMaxHeight(clampValue);
            if (height < element.clientHeight) {
                clampedText = truncate(getLastChild(element), height);
            }
        }
        

        return {
            'original': originalText,
            'clamped': clampedText
        }
    }

    window.$clamp = clamp;
})();