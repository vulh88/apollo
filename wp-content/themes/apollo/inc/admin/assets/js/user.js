jQuery(function($) {

    // Event gallery file uploads
    var $user_image_id = $('#user_image'),
        $event_images = $('#user_images_container .event_images');
    //

    if ($('#user_images_container .image').length) {
        $('.user_image a').hide();
    }

    $('.user_image').on( 'click', 'a', handle_add_user_image);

    $('#user_images_container').on( 'click', 'a.delete', delete_user_image );

    /**
     * Open specific attachment id
     * */

    $('#user_images_container').on('click', '.image', function() {
        var $this = $(this),
            file_frame = wp.media.frames.file_frame = wp.media({
                id: 'multi_image',
                title: $this.data('title'),
                button: {
                    text: $this.data('text')
                },
                library: {
                    type: 'image',
                    //post__in:ids
                },
                //multiple: true,
            });

        file_frame.on('open',function(){

            var selection = file_frame.state().get('selection');
            var selected = $this.data().attachment_id; // the id of the image
            if (selected) {
                selection.add(wp.media.attachment(selected));
            }
        });

        file_frame.open();

        var $el = $(this)
        var attachment_ids = $user_image_id.val();

        // When an image is selected, run a callback.
        file_frame.on( 'select', function() {
            attachment_ids ='';
            var selection = file_frame.state().get('selection');

            selection.map( function( attachment ) {
                attachment = attachment.toJSON();
                if ( attachment.id ) {
                    attachment_ids = attachment_ids ? attachment_ids : attachment.id;

                    $event_images.html('\
                        <div class="image" data-attachment_id="' + attachment.id + '">\
                                <img src="' + attachment.url + '" />\
                                  <p><a href="#" class="delete" title="Delete">Delete</a></p>\
                        </div>');
                }
            });

            $user_image_id.val( attachment_ids );

        });


    });

    function handle_add_user_image( event ) {

        var $el = $(this),
            attachment_ids = $user_image_id.val();

        event.preventDefault();

        var event_gallery_frame = _apollo_open_media_frame( $el );

        // When an image is selected, run a callback.
        event_gallery_frame.on( 'select', function() {
            var selection = event_gallery_frame.state().get('selection');

            selection.map( function( attachment ) {
                attachment = attachment.toJSON();
                if ( attachment.id ) {
                    attachment_ids = attachment_ids ? attachment_ids + "," + attachment.id : attachment.id;

                    $event_images.html('\
                        <div class="image" data-attachment_id="' + attachment.id + '">\
                                <img src="' + attachment.url + '" />\
                                        <p><a href="#" class="delete" title="' + $el.data('delete') + '">' + $el.data('text') + '</a></p>\
                        </div>');
                }
            });

            if ($('#user_images_container .image').length) {
                $('.user_image a').hide();
            }
            $user_image_id.val( attachment_ids );

        });


    }

    function _apollo_open_media_frame( $el ) {

        // Create the media frame.
        var media_frame = wp.media.frames.event_gallery = wp.media({
            // Set the title of the modal.
            title: $el.data('choose'),
            button: {
                text: $el.data('update')
            },
            states : [
                new wp.media.controller.Library({
                    title: $el.data('choose'),
                    filterable : 'all',
                    multiple: true
                })
            ]
        });

        // Finally, open the modal.
        media_frame.open();

        return media_frame;
    }

    function delete_user_image() {

        $(this).closest('.image').remove();

        var attachment_ids = '';

        $user_image_id.val( attachment_ids );
        $('#event_images_container ul li.image').css('cursor','default').each(function() {
            var attachment_id = jQuery(this).attr( 'data-attachment_id' );
            attachment_ids = attachment_ids + attachment_id + ',';
        });

        $('.user_image a').show();

        return false;


    }

    $('.apl-user-association-list').off('click', '.association-checked').on('click', '.association-checked', function (e) {
        var current = $(e.currentTarget);
        var parent = current.closest('.association-list');
        var checkedEle = parent.find('.apl-association-checked-list');
        var uncheckedEle = parent.find('.apl-association-unchecked-list');
        var listChecked = checkedEle.val();
        var listUnchecked = uncheckedEle.val();
        listChecked = listChecked != '' ? listChecked.split(',') : [];
        listUnchecked = listUnchecked != '' ? listUnchecked.split(',') : [];
        if (current.is(':checked')) {
            listChecked.push(current.val());
            if (listUnchecked.length > 0) {
                var indexUnchecked = listUnchecked.indexOf(current.val());
                if(indexUnchecked > -1){
                    listUnchecked.splice(indexUnchecked, 1);
                }
            }
        } else {
            listUnchecked.push(current.val());
            if (listChecked.length > 0) {
                var indexChecked = listChecked.indexOf(current.val());
                if (indexChecked > -1) {
                    listChecked.splice(indexChecked, 1);
                }
            }
        }
        checkedEle.val(listChecked.length > 0 ? listChecked.join(',') : '');
        uncheckedEle.val(listUnchecked.length > 0 ? listUnchecked.join(',') : '');

    });

    $('.remove-search-text').off('click').on('click', handle_search_association);
    $('.search-association').off('keyup').on('keyup', handle_show_icon_remove_text_search);
    function handle_show_icon_remove_text_search(e){
        e.preventDefault();
        var current = $(e.currentTarget);
        var iconRemove = current.closest('.group-search').find('.remove-search-text');
        if(iconRemove.length > 0){
            if(current.val() != ''){
                if(iconRemove.hasClass('hidden')){
                    iconRemove.removeClass('hidden');
                }
            }else{
                if(!iconRemove.hasClass('hidden')){
                    iconRemove.addClass('hidden');
                }
            }
            delaySearch(function(){
                handle_search_association(e);
            }, 500);
        }
    }

    var delaySearch = (function(){
        var timer = 0;
        return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();

    function handle_search_association(e){
        e.preventDefault();
        var current = $(e.currentTarget);
        var inputSearch = current.closest('.group-search').find('.search-association');
        if(inputSearch.length > 0 ){
            if(current.hasClass('empty-search') && inputSearch.val() != ''){
                inputSearch.val('');
                if(!current.hasClass('hidden')){
                    current.addClass('hidden');
                }
            }

            var associationElement = current.closest('.association-list');
            if(associationElement.length > 0){
                var userID = associationElement.attr('data-user-id');
                var postType =  associationElement.attr('data-post-type');

                var adminType = inputSearch.attr('data-admin-type');
                var data = {
                    text_search: inputSearch.val(),
                    post_type: postType
                };
                if (adminType == 'agency') {
                    /** Data for Agency */
                    data.action = 'apollo_agency_get_more_association_options';
                    data.agency_id = userID;
                } else {
                    /** Data for User */
                    data.action = 'apollo_user_get_more_association_options';
                    data.user_id = userID;
                }

                $.ajax({
                    url: APL.ajax_url,
                    type: 'GET',
                    dataType: 'JSON',
                    data: data,
                    beforeSend: function () {
                        associationElement.block(jQuery.apl.blockUI);
                    },
                    success: function (response) {
                        var listAssociation = associationElement.find('.apl-user-association-list');
                        var loadMore = associationElement.closest('.wrap-multiselect').find('.association-load-more');
                        if(inputSearch) {
                            listAssociation.html(response.html);
                        } else {
                            listAssociation.append(response.html);
                        }
                        if(response.have_more) {
                            loadMore.removeClass('hidden');
                            loadMore.find('.ct-btn-sm-event').css('display', 'block').data('sourcedata', response.url);
                        } else {
                            loadMore.addClass('hidden');
                        }

                        associationElement.unblock();
                    }
                });
            }
        }
    }

    $('.wrap-agency-mode').off('click', '.agency-mode').on('click', '.agency-mode', function (e) {
        var current = $(e.currentTarget);
        var blockModules = $('.user-association');
        var agencyModules = $('.apl-custom-postbox.agency');
        if (current.val() == 1) {
            if (!blockModules.hasClass('hidden')) {
                blockModules.addClass('hidden');
            }
            if (agencyModules.hasClass('hidden')) {
                agencyModules.removeClass('hidden');
            }
        } else {
            if (blockModules.hasClass('hidden')) {
                blockModules.removeClass('hidden');
            }
            if (!agencyModules.hasClass('hidden')){
                agencyModules.addClass('hidden');
            }
        }
    });

});



