ApolloPageAdmin = function () {
    /*@ticket #18192: 0002504: Arts Education Customizations - Adjust the landing page style - item 4*/
    function showConfigForEducationPage(){
        var pageTemplate = $('.post-type-page #page_template option:selected').attr('value');
        var display = pageTemplate === 'educator-landing-template.php' ? 'block' : 'none';

        /*@ticket #18331 show/hide config for education page*/
        if($('#apl_config_for_education_page').length){
            $('#apl_config_for_education_page').css('display', display);
        }

    }

    /*@ticket #18192: 0002504: Arts Education Customizations - Adjust the landing page style - item 4*/
    function changePageTemplate(){

        $('.wp-admin.post-type-page').off('change', '#page_template').on('change', '#page_template', function(){
            showConfigForEducationPage();
        });
    }

    return {
        init: function () {
            showConfigForEducationPage();
            changePageTemplate();
        }
    };
}();

jQuery(document).ready(function () {
    ApolloPageAdmin.init();
});