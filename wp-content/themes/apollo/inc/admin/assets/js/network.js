jQuery(function( $ ){

    $( '#apollo-custom-modules-options' ).on( 'click', '#checkall', function () {
        if ( $( this ).attr('checked') ) {
            $( 'input[name="blog[_apollo_module][]"]' ).attr( 'checked', true );
        } else {
            $( 'input[name="blog[_apollo_module][]"]' ).attr( 'checked', false );
        }

    });

    $( '#apollo-custom-ms-options' ).on( 'click', 'input[name="blog[_apollo_module][]"]', function () {
        if ( ! $( this ).attr( 'checked' ) ) {
            $( '#apollo-custom-ms-options #checkall' ).attr( 'checked', false );
        } else {
            var lists       = $( 'form input[name="blog[_apollo_module][]"]' ),
                check_all   = true;
                
            $.each( lists, function ( i, val ) {
                if ( ! $( val ).attr( 'checked' ) ) {
                    check_all = false;
                    return;
                }
            }); 
            
            $( '#apollo-custom-ms-options #checkall' ).attr( 'checked', check_all );
           
        }
    });

    $(document).ready(function(){
        $('.form-table tbody').prepend( $( '.apollo-custom-ms-options' ) );

        if($('input#all-user-search-input').length > 0){
            var inputSearch = $('#all-user-search-input');
            var curSearchVal = inputSearch.val();
            var replacement = '*';
            curSearchVal = trimSpecifyCharacter(replacement,curSearchVal);
            inputSearch.val(curSearchVal);
        }

        if($('body').hasClass('network-admin')){
            $('#bulk-action-selector-top').append('<option value="enablecache">Enable Cache</option>');
            $('#bulk-action-selector-top').append('<option value="disablecache">Disable Cache</option>');
            $('#bulk-action-selector-top').append('<option value="enablesolr">Enable Solr search engine</option>');
            $('#bulk-action-selector-top').append('<option value="disablesolr">Disable Solr search engine</option>');
        }
        $('.network-admin #doaction').click(function() {
            var ids = [];
            var value = 0;
            var _action = $('#bulk-action-selector-top').val();
            $('input[name="allblogs[]"]').each(function() {
                if ($(this).attr('checked')) {
                    ids.push($(this).val());
                }
            });

            var key = '_apollo_enable_solr_search';
            if (_action == 'enablecache' || _action == 'disablecache') {
                key = '_apollo_enable_local_cache';
            }

            if( _action == 'enablecache' || _action == 'disablecache' || _action == 'enablesolr' || _action == 'disablesolr') {
                if( _action == 'enablecache' || _action == 'enablesolr'){
                    value = 1;
                }
                $.ajax({
                    url: apollo_admin_obj.ajax_url,
                    method: 'post',
                    data: {
                        action: 'apollo_cache_network_site',
                        blog_ids: ids,
                        value: value,
                        key: key,
                    },
                    success: function (res) {
                        window.location.reload();
                    }
                });
            }
        });
    });


    var trimSpecifyCharacter = function(char,string){
        if(string[0] === char){
            string = string.substr(1,string.length);
        }
        if(string[string.length - 1] === char){
            string = string.substr(0,string.length - 1);
        }
        return string;
    };

    $('#search-submit').off("click").on("click",function(e){
        if($(e.currentTarget).closest("form").find("input#all-user-search-input").length > 0){
            var inputSearch = $('#all-user-search-input');
            var searchForm = $(e.currentTarget).closest("form");
            var curSearchVal = inputSearch.val();
            var replacement = '*';
            curSearchVal = trimSpecifyCharacter(replacement,curSearchVal);
            inputSearch.val('*' + curSearchVal + '*');
            searchForm.submit();
        }
        return true;
    });

    $('.users_page_sync_users-network').off('click', '.confirm-transfer-user').on('click', '.confirm-transfer-user', function (e) {
        e.preventDefault();
        var current = $(e.currentTarget);
        var r = confirm(current.attr('data-message'));
        if ( r ) {
             current.closest('#transfer-user-to').submit();
        }
        
    })

    $('.users_page_sync_users').off('click', '#apl-move-multiple-users').on('click', '#apl-move-multiple-users', function (e) {
        e.preventDefault();
        var current = $(e.currentTarget);
        var listIds = $('input[name="user_ids[]"]:checked');
        if (listIds.length <= 0) {
            return;
        }
        var r = confirm(current.attr('data-message'));
        if (r) {
            var userForm = current.closest('#transfer-multiple-user');
            $.each(listIds, function (index, item) {
                $(userForm).append(
                    '<input type="hidden" name="user_ids[]" value="'+($(item).val())+'"/>'
                );
            });
            userForm.submit();
        }

    })
});
