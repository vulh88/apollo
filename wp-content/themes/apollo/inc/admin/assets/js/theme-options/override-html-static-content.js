Apollo_Theme_Option_Override_Html = function () {

    /*@ticket #18343: 0002504: Arts Education Customizations - Add Arts Ed masthead logo and footers to Blog listing and detail pages*/
    //element: header, footer1, footer2, site_info
    var showOverrideStaticHTML = function (element) {
        var currentTab = $(".siteconfig .ui-tabs-active.ui-state-active a").text().toLowerCase();

        if (!currentTab || currentTab == '') {
            return;
        }

        /**
         * Current active tab such as: blog-setting-sub-tab..
         * */
        var settingTab = currentTab + "-settings-sub-tab";
        settingTab = "#" + settingTab;

        /**
         * ex: _apollo_theme_options[_content_type_header_education]
         */
        var contentType = "input[name='_apollo_theme_options[_content_type_" + element + "_" + currentTab + "]']:checked";
        contentType = $(contentType).val();

        /**
         * ex: apl-override-html-header-custom
         */
        var contentTypeCustom = $(settingTab + " .apl-override-html-" + element + "-custom");
        contentTypeCustom.addClass("hidden");

        /**
         * ex: apl-override-html-header-reference
         */
        var contentTypeReference = $(settingTab + " .apl-override-html-" + element + "-reference");
        contentTypeReference.addClass("hidden");

        /**Content type:
         * 0: Custom
         * 1: Global data
         * 2: Reference module
         */
        if (contentType == 0) {
            contentTypeCustom.removeClass("hidden");
        }

        if (contentType == 2) {
            contentTypeReference.removeClass("hidden");
        }

    };

    /*@ticket #18343: */
    var overrideStaticChangeEvent = function(){
        $('.apl-override-html-site_info-type .option').off('click').on('click', function () {
            showOverrideStaticHTML('site_info');
        });

        $('.apl-override-html-header-type .option').off('click').on('click', function () {
            showOverrideStaticHTML('header');
        });

        $('.apl-override-html-footer1-type .option').off('click').on('click', function () {
            showOverrideStaticHTML('footer1');
        });

        $('.apl-override-html-footer2-type .option').off('click').on('click', function () {
            showOverrideStaticHTML('footer2');
        });

        $('#optionsframework .sub-tab-wrapper li.ui-state-default').on('click', function (){
            showOverrideStaticHTML('header');
            showOverrideStaticHTML('footer1');
            showOverrideStaticHTML('footer2');
            showOverrideStaticHTML('site_info');
        });
    };
    /*End 18343*/

    return {
        init: function () {
            showOverrideStaticHTML('header');
            showOverrideStaticHTML('footer1');
            showOverrideStaticHTML('footer2');
            showOverrideStaticHTML('site_info');
            overrideStaticChangeEvent();
        }
    };
}();

jQuery(document).ready(function () {
    Apollo_Theme_Option_Override_Html.init();
});