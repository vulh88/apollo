(function($) {
    $(function() {

        // Handle bypass method
        // Ticket #12700
        var $eventBypassProcess = $('#section-_enable_event_bypass_approval_process'),
            $method = $('#section-_enable_event_bypass_approval_method'),
            activeBypassFn = function(isActive) {
                if (isActive) {
                    $method.find('input').prop('disabled', false);
                }
                else {
                    $method.find('input').prop('disabled', true);
                }
            };

        activeBypassFn($eventBypassProcess.find('input[value=1]').is(':checked'));
        $eventBypassProcess.on('click', 'input', function() {

            var $this = $(this),
                isActive = $this.val() == 1;
            activeBypassFn(isActive);
        });
        // End handle event bypass method



        // Conflict option alert message
        $('#optionsframework-metabox').on('click', '.apl-submit-option', function(e) {
            e.preventDefault();
            var $eventRegisterORGDropdown = $('[name="_apollo_theme_options[_enable_event_registered_organizations_field]"]:checked');
            var $orgActiveMemberField = $('[name="_apollo_theme_options[_organization_active_member_field]"]:checked');
            if ($eventRegisterORGDropdown.val() == 0 && $orgActiveMemberField.val() == 1) {
                var cf = confirm($(this).data().warningMsg);

                if (cf) {
                    $(this).closest('form').submit();
                }
                else {
                    return;
                }
            }
            else {
                $(this).closest('form').submit();
            }
        })
        // End Conflict option alert message

        /** @Ticket #13856 */
        $(document).ready(function() {
            var neighborhoodTable = $('#neighborhood-table');
            neighborhoodTable.DataTable( {
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": apollo_admin_obj.ajax_url + "?action=apollo_neighborhood_get_data&parent_id=0",
                "columns": [
                    { "data": "name", "orderable": true},
                    { "data": "code", "orderable": true},
                    { "data": "actions"}
                ],
                "columnDefs": [
                    {
                        "render": function ( data, type, row) {
                            return '<div class="neighborhood-actions" data-id="'+data.id+'" data-city-id="'+data.city_id+'" data-name="'+data.name+'" >' +
                                '<a class="edit-neighborhood"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>' +
                                '<a class="remove-neighborhood"><i class="fa fa-times" aria-hidden="true"></i></a>' +
                                '</div>';
                        },
                        "targets" : 2,
                        "sortable": false,
                        "ordertable": false
                    }
                ]
            } );

            $('#neighborhood-city').off('change').on('change', function (e) {
                var parentID = $(e.currentTarget);
                neighborhoodTable.DataTable().ajax.url(apollo_admin_obj.ajax_url + "?action=apollo_neighborhood_get_data&parent_id=" + parentID.val()).load();
                var displayMessage = $('#neighborhood-name').closest('.controls').find('.explain');
                displayMessage.html('');
            });

            $('.add-neighborhood').off('click').on('click', function(e) {
                var neighborhoodName = $('#neighborhood-name');
                var neighborhoodCity = $('#neighborhood-city');
                var neighborhoodID = $('#neighborhood-id');
                var displayMessage = neighborhoodName.closest('.controls').find('.explain');
                var save = true;
                if (neighborhoodName.val() == '') {
                    if (!neighborhoodName.hasClass('apollo-input-error')) {
                        neighborhoodName.addClass('apollo-input-error');
                        save = false;
                    }
                } else {
                    if (neighborhoodName.hasClass('apollo-input-error')) {
                        neighborhoodName.removeClass('apollo-input-error');
                    }
                }
                if (neighborhoodCity.val() == 0) {
                    if (!neighborhoodCity.hasClass('apollo-input-error')) {
                        neighborhoodCity.addClass('apollo-input-error');
                        save = false;
                    }
                } else {
                    if (neighborhoodCity.hasClass('apollo-input-error')) {
                        neighborhoodCity.removeClass('apollo-input-error');
                    }
                }

                if (save) {
                    $.ajax({
                        url: apollo_admin_obj.ajax_url,
                        method: 'post',
                        dataType: 'json',
                        data:{
                            action : 'apollo_add_neighborhood',
                            neighborhood_data : {
                                id : parseInt(neighborhoodID.val()),
                                name: neighborhoodName.val(),
                                city: neighborhoodCity.val()
                            }
                        },
                        beforeSend: function () {

                        },
                        success: function (response) {
                            if (response.status) {
                                neighborhoodName.val('');
                                displayMessage.html('<span class="text-success">'+response.message+'</span>');
                                neighborhoodTable.DataTable().ajax.reload();
                            } else {
                                displayMessage.html('<span class="text-danger">'+response.message+'</span>');
                            }

                        }
                    });
                } else {
                    return false;
                }

            });

            $('.reset-neighborhood').off('click').on('click', function(e) {
                var neighborhoodName = $('#neighborhood-name');
                if (neighborhoodName.length > 0) {
                    neighborhoodName.val('');
                    neighborhoodName.focus();
                }
                var displayMessage = neighborhoodName.closest('.controls').find('.explain');
                displayMessage.html('');

                var neighborhoodID = $('#neighborhood-id');
                if (neighborhoodID.length > 0) {
                    neighborhoodID.val('0');
                }
                var buttonAdd = $('.add-neighborhood');
                if (buttonAdd.length > 0) {
                    buttonAdd.val(buttonAdd.attr('data-text-add'));
                }

            });

            $('#neighborhood-table').off('click', '.edit-neighborhood').on('click', '.edit-neighborhood', function(e) {
                var current = $(e.currentTarget);
                var neighborhoodData = current.closest('.neighborhood-actions');

                var neighborhoodName = $('#neighborhood-name');
                if (neighborhoodName.length > 0) {
                    neighborhoodName.val(neighborhoodData.attr('data-name'));
                    neighborhoodName.focus();
                    var displayMessage = neighborhoodName.closest('.controls').find('.explain');
                    displayMessage.html('');
                }
                var neighborhoodCity = $('#neighborhood-city');
                if (neighborhoodCity.length > 0) {
                    neighborhoodCity.val(neighborhoodData.attr('data-city-id'))
                }

                var neighborhoodID = $('#neighborhood-id');
                if (neighborhoodID.length > 0) {
                    neighborhoodID.val(neighborhoodData.attr('data-id'));
                }

                var buttonAdd = $('.add-neighborhood');
                if (buttonAdd.length > 0) {
                    buttonAdd.val(buttonAdd.attr('data-text-update'));
                }
                neighborhoodTable.DataTable().ajax.url(apollo_admin_obj.ajax_url + "?action=apollo_neighborhood_get_data&parent_id=" + neighborhoodData.attr('data-city-id')).load();
            });

            $('#neighborhood-table').off('click', '.remove-neighborhood').on('click', '.remove-neighborhood', function(e) {
                if (confirm("Are you sure?")) {
                    var current = $(e.currentTarget);
                    var neighborhoodData = current.closest('.neighborhood-actions');
                    var id = neighborhoodData.attr('data-id');
                    var neighborhoodName = $('#neighborhood-name');
                    var neighborhoodID = $('#neighborhood-id');
                    var displayMessage = neighborhoodName.closest('.controls').find('.explain');
                    if (id) {
                        $.ajax({
                            url: apollo_admin_obj.ajax_url,
                            method: 'post',
                            dataType: 'json',
                            data:{
                                action : 'apollo_remove_neighborhood',
                                neighborhood_id : id
                            },
                            beforeSend: function () {

                            },
                            success: function (response) {
                                if (response.status) {
                                    neighborhoodTable.DataTable().ajax.reload();
                                    if (neighborhoodName.length > 0) {
                                        neighborhoodName.val('');
                                    }
                                    if (neighborhoodID.length > 0) {
                                        neighborhoodID.val('0');
                                    }
                                    if (displayMessage.length > 0) {
                                        displayMessage.html('');
                                    }
                                }
                            }
                        });
                    }
                } else {
                    return false;
                }
            });

            /** @Ticket #15877 */
            $('#form-theme-options').off('click', '#apl-reset-email-template').on('click', '#apl-reset-email-template', function (e) {
                var current = $(e.currentTarget);
                var message = current.data('message-confirm');
                if (confirm(message)) {
                    var template = current.closest('.email-template-control').find('.email-template-display');
                    if (template.length > 0) {
                        template.val('');
                        $('#optionsframework-submit .apl-submit-option').trigger('click');
                    }
                }
            });

            /**
             * @ticket #18515: [CF] 20181205 - Add the options allow control the transition speed of the slides.
             */
            $('.apl-slideshow-speed input').on('change', function() {

                var value = $(this).val();
                var min = $(this).attr('min');

                if(parseInt(value) < min){
                    $(this).val(min);
                }
            });

            /**
             * @ticket #18849: [Business] Modify the URL path - Item 1
             */
            $('.apl-check-exited-slug input').on('change', function() {
                var value = $(this).val();
                var textBanned = $(this).attr('textBanned');
                textBanned = textBanned.split(',');
                var textDefault = $(this).attr('textDefault');

                if(textBanned.indexOf(value.trim()) !== -1){
                    $(this).val(textDefault);
                }
            });
        });


        $('#clear-top-ten').click(function() {
            var $this = $(this),
                label = $this.val();

            $.ajax({
                url: apollo_admin_obj.ajax_url,
                method: 'post',
                data: {
                    action: 'apollo_clear_top_ten',
                },
                beforeSend: function() {
                    $this.addClass('.avoid-clicks');
                    $this.val($this.data().processing);
                },
                success: function(res) {
                    alert(res.msg)
                    $this.val(label);
                    $this.removeClass('.avoid-clicks')
                }
            });
        });

        $( '#apl-terr-city-zip' ).on( 'click', '#check_all_zip_city', function () {
            if ( $( this ).attr('checked') ) {
                $( '#apl-terr-city-zip input:enabled' ).attr( 'checked', true );
            } else {
                $( '#apl-terr-city-zip input' ).attr( 'checked', false );
            }
        });
        $( '#apl-terr-city-zip' ).on( 'click', ' .zip', function () {
            if ( ! $( this ).attr( 'checked' ) ) {
                $( '#apl-terr-city-zip #check_all_zip_city' ).attr( 'checked', false );
            } else {

                var a = $("#apl-terr-city-zip .zip");
                var b = $("#apl-terr-city-zip  .city:enabled");
                if(a.length == a.filter(":checked").length && b.length == b.filter(":checked").length ){
                    $( '#apl-terr-city-zip #check_all_zip_city' ).attr( 'checked', true );
                }
            }
        });
        $( '#apl-terr-city-zip' ).on( 'click', ' .city', function () {
            if ( ! $('#apl-terr-city-zip .city:enabled').attr( 'checked' ) ) {
                $( '#apl-terr-city-zip #check_all_zip_city' ).attr( 'checked', false );
            } else {
                var a = $("#apl-terr-city-zip .zip");
                var b = $("#apl-terr-city-zip  .city:enabled");
                if(a.length == a.filter(":checked").length && b.length == b.filter(":checked").length ){
                    $( '#apl-terr-city-zip #check_all_zip_city' ).attr( 'checked', true );
                }
            }
        });
        $('.wrap-multiselect').on('click', '.apl-lang-tool .trans', function() {
            $(this).next('.container').slideToggle('hidden');
        });

        $('.section.section-text').on('click', '.apl-lang-tool .trans', function() {
            $(this).next('.container').slideToggle('hidden');
        });

        $('.section.section-textarea').on('click', '.apl-lang-tool .trans', function() {
            $(this).next('.container').slideToggle('hidden');
        });

        /* handle script for generate input data from terr data */
        var parseInputs = function (data) {
            var ret = {};
            retloop:
                for (var input in data) {
                    var val = data[input];

                    var parts = input.split('[');
                    var last = ret;

                    for (var i in parts) {
                        var part = parts[i];
                        if (part.substr(-1) == ']') {
                            part = part.substr(0, part.length - 1);
                        }

                        if (i == parts.length - 1) {
                            last[part] = val;
                            continue retloop;
                        } else if (!last.hasOwnProperty(part)) {
                            last[part] = {};
                        }
                        last = last[part];
                    }
                }
            return ret;
        }

        $('#form-theme-options input[type="submit"][name="update"]').off('click').on('click',function(e){
            var inputFullDataPass = $('#_apl_pass_full_terr_data');
            if(inputFullDataPass.length > 0 && inputFullDataPass.val().length == 0){
                var zipCodeData = $('#apl-terr-city-zip :input[class!="city"]').serializeArray();
                var arrayDataSingleDimension = {},
                    replaceKey = '_apollo_theme_options[_apl_terr_data]',
                    replacementKey = 'customTerrData';
                $(zipCodeData).each(function(index, obj){
                    var newKey = obj.name.replace(replaceKey,replacementKey);
                    arrayDataSingleDimension[newKey] = obj.value;
                });
                var separatedDataFormatting = parseInputs(arrayDataSingleDimension);
                var jsonStringifyDataFormatting = JSON.stringify(separatedDataFormatting);
                var encodeURIComponentData = encodeURIComponent(jsonStringifyDataFormatting);
                inputFullDataPass.val(encodeURIComponentData);
                $('#apl-terr-city-zip :input').attr('disabled',true);
            }
            return true;
        });


        /* end handle script for generate input data from terr data */


        $('#section-_apl_terr_states .controls input').click(function() {

            if ($(this).attr('checked') == undefined && $('#section-_apl_terr_states .controls input:checked').length == 0) {
                $('#section-_apl_terr_cities .controls input').val("");
                $('#section-_apl_terr_zipcode .controls input').val("");
                $('#apl-terr-city-zip .city').removeAttr('checked');
                $('#apl-terr-city-zip .zip').removeAttr('checked');
            }
        });



        $('#apl-terr-city-zip .city').click(function() {
            if($(this).attr('checked') != undefined) {
                //$(this).closest('ul').find('[data-value="'+$(this).data().value+'"]').attr('disabled', 'disabled');
            } else {
                //$(this).closest('ul').find('[data-value="'+$(this).data().value+'"]').removeAttr('disabled');
            }
            $(this).removeAttr('disabled');
        });


        $('#apl-terr-city-zip .zip').click(function() {
            var wrap = $('#section-_apl_terr_data .controls');

            if ($(this).attr('checked') != undefined) {
                a = wrap.find('[data-name="'+$(this).data().cityState+'"]')

            } else {

            }
        });

        // Handle hide or show event spotlight image
        var _handleEventSpotlight = function(elm) {

            if (elm.val() == 'medium') {
                $('#_event_spotlight_bg_img').closest('.section').show();
            } else {
                $('#_event_spotlight_bg_img').closest('.section').hide();
            }
        };
        _handleEventSpotlight(jQuery('.apl-spot-type input[checked]'));
        $('.apl-spot-type input').click(function() {
            _handleEventSpotlight($(this));
        });

        // Re arange the value of hidden select for saving
        var _hiddenSelect = $('.hidden.hidden-multiselect'),
            selectedOption = '';

        $.each(_hiddenSelect, function(i, elm) {
            // debugger
            var _selectedTabs = $(elm).closest('.wrap-multiselect').find('.selected-event ol li')

            $.each(_selectedTabs, function(i, v) {
                var value = $(v).data().value;

                // Keep
                selectedOption = $(elm).find('option[value="'+value+'"]');

                // Remove top
                $(elm).find('option[value="'+value+'"]').remove();

                // Add to bottom
                $(elm).append(selectedOption);
            });
        });

        // End Re arange the value of hidden select for saving



        $('#section-_home_featured_type').change(function() {
            if ($(this).find('[value="ver"]').attr('selected') == 'selected' && $('.selected-event ol li').length > 4) {
                alert('Vertical view mode only allow 4 primary categories');
            }
        });

        /**
         * hanlde multiple select color picker
         *
         * */
        var handleMultiSelectColorPicker = function(elm) {
            color = $('.selected-event .wp-picker-container');
            if (elm.val() == 'sta') {
                color.css('pointer-events', 'none')
                    .css('opacity', '0.5');
            } else {
               color.css('pointer-events', '')
                    .css('opacity', 1);
            }
        };
        $('#_home_featured_type').change(function() {
            handleMultiSelectColorPicker($(this));
        });
        $('body').on('aplMultiSelectSort', function(v, hasColor) {
            handleMultiSelectColorPicker($('#_home_featured_type'));
        });
        /* end hanlde multiple select color picker */

        var emailTemplateSelection = $('#email-template-action');
        emailTemplateSelection.change(function(e) {
            var curVal = $(this).val();

            /** @Ticket #16062 - Manage reason list of the Reject button */
            if (curVal === 'confirm_rejected_item_to_owner'){
                $('.use_default_reason').removeClass('hidden');
                $('#reject_reasons_text').removeClass('hidden');
                $('.reject_reasons_text').removeClass('hidden');
            }
            else{
                $('.use_default_reason').addClass('hidden');
                $('.reject_reasons_text').addClass('hidden');
            }

            $('.email-template-display').addClass('hidden');
            $('.' + curVal + '-selection').removeClass('hidden');
            $('input[name="submit-testing-email"]').attr('data-class-action',curVal);
        });

        // Loads the color pickers
        $('.of-color').wpColorPicker();

        handleMultiSelectColorPicker($('#_home_featured_type'));

        // Image Options
        $('.of-radio-img-img').click(function(){
                $(this).parent().parent().find('.of-radio-img-img').removeClass('of-radio-img-selected');
                $(this).addClass('of-radio-img-selected');
        });

        $('.of-radio-img-label').hide();
        $('.of-radio-img-img').show();
        $('.of-radio-img-radio').hide();

        // Loads tabbed sections if they exist
        if ( $('.nav-tab-wrapper').length > 0 ) {
            $.apl_admin.theme_opt.options_framework_tabs();
        }

        $( '.selected-event ol' ).sortable();

        if($('.sub-tab-wrapper').length > 0){
            $('.sub-tab-wrapper').tabs();
        }

        /* Thienld : storage selected current tab on Business area */
        $('#optionsframework ul.ui-tabs-nav li a').on('click',function(e){
            var curItem = $(e.currentTarget).attr("href");
            localStorage.setItem('bs_tab_cur_activated',curItem);
            console.log(curItem);
        });

        var currentActivatedTab = localStorage.getItem('bs_tab_cur_activated');
        if(currentActivatedTab && currentActivatedTab.length > 0){
            $('a[href="'+currentActivatedTab+'"]').trigger('click');
        }

        //Trilm refresh public art map data
        $('#_public_art_map_refresh').click(function(){
            var text = $(this).attr('data-confirm');
            var check =  confirm(text);
            if(check){
                $.ajax({
                    url: apollo_admin_obj.ajax_url,
                    method : 'get',
                    data : {
                        action : 'apollo_refresh_public_art_map'
                    }
                }).done(function(res){
                    //update last time refresh public artmap.
                    localStorage.setItem('p_art_refreshed_time_server',res.refreshed_time);
                    alert('Refresh public art success!');
                });
            }
        });
    });

    $.apl_admin.theme_opt.options_framework_tabs = function() {

        // Hides all the .group sections to start
        $('.group').hide();

        // Find if a selected tab is saved in localStorage
        var active_tab = '';
        if ( typeof(localStorage) != 'undefined' ) {
            active_tab = localStorage.getItem("active_tab");
        }

        // If active tab is saved and exists, load it's .group
        if (active_tab != '' && $(active_tab).length ) {
            $(active_tab).fadeIn();
            $(active_tab + '-tab').addClass('nav-tab-active');
        } else {
            $('.group:first').fadeIn();
            $('.nav-tab-wrapper a:first').addClass('nav-tab-active');
        }

        $('.group').css( 'visibility', 'inherit' );

        // Bind tabs clicks
        $('.nav-tab-wrapper a').click(function(evt) {

            evt.preventDefault();

            // Remove active class from all tabs
            $('.nav-tab-wrapper a').removeClass('nav-tab-active');

            $(this).addClass('nav-tab-active').blur();

            var group = $(this).attr('href');

            if (typeof(localStorage) != 'undefined' ) {
                    localStorage.setItem("active_tab", $(this).attr('href') );
            }

            $('.group').hide();
            $(group).fadeIn();

            // Editor height sometimes needs adjustment when unhidden
            $('.wp-editor-wrap').each(function() {
                    var editor_iframe = $(this).find('iframe');
                    if ( editor_iframe.height() < 30 ) {
                            editor_iframe.css({'height':'auto'});
                    }
            });

        });
    };


    /**
     * @author Trilm
     * check solr old, new config
     */
    $(document).on('change','.solr-engine input',function(){

        var array = [];
        $('.solr-engine input[type=checkbox]').each(function(){
            var itemKey = $(this).attr('data-value');
            var itemValue = $(this).is(':checked');
            if(itemValue == true){
                itemValue = 1;
            }else{
                itemValue = 0;
            }
            var object = {};
            object['value'] = itemValue;
            object['key'] = itemKey;
            array.push(object);
        });
        $.ajax({
            url: apollo_admin_obj.ajax_url,
            method: 'post',
            data: {
                action: 'apollo_compare_solr_old_new_config',
                val: JSON.stringify(array),
            },
            success: function(res) {
               if(res.status == 0){
                    $('#section-sync_solr_data input').attr('disabled','true');
               }else{
                   $('#section-sync_solr_data input').removeAttr('disabled');
               }
            }
        });

    });

}) (jQuery);



(function ($) {

    $(function() {
        $('[data-email-param]').dblclick( function() {
            var target = $($(this).data().target),
                position = target.getCursorPosition(),
                content = target.val(),
                newContent = content.substr(0, position) + $(this).val() + content.substr(position);
            target.val(newContent);
        });

        $('.apollo-email-manegement input#preview-template').click(function() {
            var $this = $(this),
                _data = $this.data();
            $.ajax({
                url: apollo_admin_obj.ajax_url,
                method: 'post',
                data: {
                    action: 'apollo_preview_template',
                    content: $('textarea[name="_apollo_theme_options['+_data.template+']"]').val(),
                },
                beforeSend: function() {
                    $this.val( _data.processText );
                    $this.css('pointer-events', 'none');
                },
                success: function(res) {
                    $this.val( _data.defaultText );
                    $this.css('pointer-events', '');
                    var win = window.open(apollo_admin_obj.admin_url+ '?preview_email_template=1&action='+ _data.template, '_blank');
                    if( win ){
                        //Browser has allowed it to be opened
                        win.focus();
                    } else{
                        //Broswer has blocked it
                        alert(_data.notice);
                    }
                }
            });
        });

        $('[name="submit-testing-email"]').click(function() {
            var $this = $(this),
                _data = $this.data(),
                recipient = $('#test_email').val();
            if ( !recipient ) return false;

            $this.val(_data.sending);
            $this.attr('disabled', 'disabled');
            $.ajax({
                url: apollo_admin_obj.ajax_url,
                data: {
                    action: _data.action,
                    id: $('#email-template-action').val(),
                    email: recipient,
                },
                success: function(res) {
                    if ( res.success ) {
                        alert(_data.success);
                    } else {
                        alert(_data.error);
                    }
                    $this.val(_data.defaultText);
                    $this.removeAttr('disabled');
                }
            });
        });

        $('._apl_terr_filter_button').click(function() {
            var arr_states = [];
            var _alert = $(this).data().alert;
            t = $('#section-_apl_terr_states input[type=checkbox]:checked');
            if(t.length == 0){
                alert(_alert);
                return false;
            }
            $('.contain-city-zip > h4').hide();
            $('#apl-terr-city-zip').hide();
            $.each(t, function(i,v) {
                arr_states.push($(v).data().value);
            })
            $.ajax({
                url: apollo_admin_obj.ajax_url,
                beforeSend: function() {
                    jQuery(window).block(jQuery.apl.blockUI);
                },
                data: {
                    action: 'apollo_filter_city_zip_by_states',
                    _data : JSON.stringify(arr_states),
                },
                success: function(res) {
                    if ( res.success) {
                        $('#apl-terr-city-zip').html(res.success);
                        $('.contain-city-zip > h4').show();
                        $('#apl-terr-city-zip').show();
                        if ($('#apl-terr-city-zip input:enabled').is(':checked')) {
                            $('#check_all_zip_city').attr('checked',true);
                        }

                    }

                    jQuery(window).unblock();
                }
            });
        });

        // handle for button backup theme options data
        $('.backup-settings').off('click').on('click',function( event ){
            event.preventDefault();
            var formSubmission = $('#form-theme-options');
            formSubmission.prepend('' +
                '<input type="hidden" name="backup-theme-options" value="yes" />' +
                '<input type="hidden" name="export-settings" value="yes" />');
            formSubmission.find('#optionsframework-submit input[name="update"]').trigger('click');
            return false;
        });

        // handle for button backup theme options data
        $('.restore-theme-options-button').off('click').on('click',function( event ){
            event.preventDefault();
            $(event.currentTarget).parent().trigger('click');
            return false;
        });

        // handle auto uploading import file for restore theme options data
        $('#restore-theme-options').off('change').on('change',function(e){
            var r = confirm("This function will overwrite all current theme option settings. Click OK to proceed it.");
            if($(e.currentTarget).val().length > 0 && r == true){
                $(e.currentTarget).closest('form').prepend('<input type="hidden" name="import-theme-option-processing" value="yes" />');
                $(e.currentTarget).closest('form').submit();
            } else {
                $(e.currentTarget).val("");
            }
            return false;
        });


        /* @Ticket 13590 / 13606 */
        function overrideCss(id) {
            $("#" + id).wrap('<div id="article_wrapper_' + id + '"><div class="container_content_codemirror"></div> </div>');
            $('#article_wrapper_' + id).append('<div class="handle-tooldrag" id="tooldrag' + id + '"></div>');
            $('#section-' + id + ' .controls.full').css('width', '100%');
            var editor = CodeMirror.fromTextArea(document.getElementById(id), {
                extraKeys: {"Ctrl-Space": "autocomplete"},
                lineNumbers: true,
                mode: "css",
                autoRefresh:true,
                lineWrapping: true,
            });
            $("#" + id +" ~ .CodeMirror").css('background', '#f8f8f8');
        }
        function overrideJs(id) {
            $("#" + id).wrap('<div id="article_wrapper_' + id + '"><div class="container_content_codemirror"></div> </div>');
            $('#article_wrapper_' + id).append('<div class="handle-tooldrag" id="tooldrag' + id + '"></div>');
            $('#section-' + id + ' .controls.full').css('width', '100%');
            CodeMirror.fromTextArea(document.getElementById(id), {
                lineNumbers: true,
                matchBrackets: true,
                continueComments: "Enter",
                extraKeys: {"Ctrl-Q": "toggleComment"},
                mode: "javascript",
                autoRefresh:true,
                lineWrapping: true
            });
            $("#" + id +" ~ .CodeMirror").css('background', '#f8f8f8');
        }
        overrideCss('_override_css');
        overrideJs('_header_tracking_script');
        overrideJs('_script_in_body_tag');
        overrideJs('_apl_additional');

        $( ".handle-tooldrag" ).mousedown(function( e ) {
            $(this).attr('data-start_h', $(this).parent().find('.CodeMirror').height()).attr('data-start_y', e.pageY);
            $('body').attr('data-dragtool', $(this).attr('id')).on("mousemove", onDragTool);
            $(window).on("mouseup", onReleaseTool);
        });

        function onDragTool(e){
            ele = "#" + $('body').attr('data-dragtool');
            container = $(ele).parent().find('.CodeMirror');
            start_h = parseInt($(ele).attr('data-start_h'));

            $(ele).parent().find('.CodeMirror').css('height', Math.max(200, start_h + e.pageY - $(ele).attr('data-start_y')));
        }
        function onReleaseTool(e){
            $('body').off("mousemove", onDragTool);
            $(window).off("mouseup", onReleaseTool);
        }
    });

    $.fn.getCursorPosition = function () {
        var el = $(this).get(0),
            pos = 0;
        if ('selectionStart' in el) {
            pos = el.selectionStart;
        } else if ('selection' in document) {
            el.focus();
            var Sel = document.selection.createRange();
            var SelLength = document.selection.createRange().text.length;
            Sel.moveStart('character', -el.value.length);
            pos = Sel.text.length - SelLength;
        }
        return pos;
    };
})(jQuery);



