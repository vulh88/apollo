(function($) {

    $(document).ready(function(){
        /** @Ticket #17080 */
        var educatorElement = $('body .nf-sub-custom-fields-table select option[value=ninja-form-admin-custom-educator-element]');
        var programElement = $('body .nf-sub-custom-fields-table select option[value=ninja-form-admin-custom-program-element]');
        if (educatorElement.length > 0) {
            educatorElement.closest('select').addClass('apl-custom-ninja-form-educator-select');
            educatorElement.val('');
        }
        if (programElement.length > 0) {
            programElement.closest('select').addClass('apl-custom-ninja-form-program-select');
            programElement.val('');
        }
        $('body').off('change', '.apl-custom-ninja-form-educator-select').on('change', '.apl-custom-ninja-form-educator-select', function (e) {
            var current = $(e.currentTarget);
            var eduId = current.val();
            var proElement = $('body').find('.apl-custom-ninja-form-program-select');
            if (eduId != 0 && eduId != '' && proElement.length > 0) {
                $.ajax({
                    url: APL.ajax_url,
                    data: {
                        action: 'apollo_get_program_by_educator_id',
                        edu_id: eduId
                    },
                    beforeSend: function() {
                        $(window).block($.apl.blockUI);
                    },

                    success : function (res) {
                        $(window).unblock();
                        if (res.data.length > 0) {
                            proElement.html('');
                            proElement.append(res.data);
                        }
                    }
                });
            } else {
                proElement.html('');
            }
        });

    });

}) (jQuery);