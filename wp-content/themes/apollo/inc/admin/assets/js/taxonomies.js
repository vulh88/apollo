(function($) {
    $(function() {

        $('.edit-tags-php form#addtag #submit').hover(function() {
            $('#taxonomy_description-html').click();
        });

        // Hide remove button image
        if ( ! $( '#org-thumbnail-id' ).val() ) {
            $( '.remove-image-button' ).hide(); 
        }

        $( '#wpbody' ).on( 'click', '.upload-image-button', $.apl_admin.taxonomies.handle_upload_image_org );

    });
    
    var file_frame;
    
    $.apl_admin.taxonomies.handle_upload_image_org = function( event ) {
        event.preventDefault();
        
        if ( file_frame ) {
            file_frame.open();
            return;
        }
        
        // Create the media frame.
        file_frame = wp.media.frames.downloadable_file = wp.media({
                title: 'Choose an image',
                button: {
                    text: 'Use image'
                },  
                multiple: false
        });

        // When an image is selected, run a callback.
        file_frame.on( 'select', function() {
                attachment = file_frame.state().get('selection').first().toJSON();

                $('#org-thumbnail-id').val( attachment.id );
                $('#org-thumbnail img').attr('src', attachment.url );
                $('.remove-image-button').show();
        });

        // Finally, open the modal.
        file_frame.open();
    };
}) (jQuery);