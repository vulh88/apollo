
(function($) {
   
    $(function() {
        $( '#the-list' ).on( 'click', '.editinline', $.apl_admin.edit_inline );
    });
  
    $.apl_admin.edit_inline = function() {
        try {
            inlineEditPost.revert();
            var post_id = $(this).closest('tr').attr('id');

            post_id = post_id.replace("post-", "");

            var _inline_data    = $('#_inline_' + post_id ),
                _data           = _inline_data.children('div');
            $.each( _data, function(i, k) {
                try {
                    $('input[name="'+ $(k).attr('class') +'"]').val($(k).text());

                    $('select[name="'+ $(k).attr('class') +'"] option').removeAttr('selected');
                    $('select[name="'+ $(k).attr('class') +'"] option[value="'+$(k).text()+'"]').attr('selected', 'selected');
                } catch(e) {

                }
            });
        }catch(e) {}
    };
    
})(jQuery);
