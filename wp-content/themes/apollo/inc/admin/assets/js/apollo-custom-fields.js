(function($) {

    $(document).ready(function(){
        $('.fields').off('click', 'input.apl-cf-display-control').on('click', 'input.apl-cf-display-control', function(e){
            var current = $(e.currentTarget);
            if (current.is(':checked')) {
                $('.fields').find('input.apl-cf-display-control').attr('checked', false);
                current.attr('checked', true);
            }
        });
    });

}) (jQuery);