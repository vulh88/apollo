(function ($) {
    window.APOLLO =  window.APOLLO || {};
    window.APOLLO.adminIntergration = (function(){
        var module = {};
        var indiEventCurPage = 1;
        module.init = function(){
            $(document).ready(function(){

                if ($('#dateStart').length) {
                    $('#dateStart').datepicker({
                        dateFormat : 'yy-mm-dd'
                    });
                }

                if ($('#dateEnd').length) {
                    $('#dateEnd').datepicker({
                        dateFormat : 'yy-mm-dd'
                    });
                }

                $('.cz-area-selection').hide();
                $('input[name="meta-cityzip"]').each(function(index,item){
                    if($(item).prop('checked') === true){
                        displayCityOrZipSelection(item);
                    }
                });
                $('input[name="meta-cityzip"]').off('click').on('click',function(e){
                    displayCityOrZipSelection(e.currentTarget);
                });

                $('.display-fields-area-selection').hide();
                $('input[name="meta-allown"]').each(function(index,item){
                    if($(item).prop('checked') === true){
                        displayFieldsSelection(item);
                    }
                });
                $('input[name="meta-allown"]').off('click').on('click',function(e){
                    displayFieldsSelection(e.currentTarget);
                });

                $('.create-iframe-ws').hide();
                $('input[name="meta-iframe"]').each(function(index,item){
                    if($(item).prop('checked') === true){
                        displayCreateIFrameWidget(item);
                    }
                });
                $('input[name="meta-iframe"]').off('click').on('click',function(e){
                    displayCreateIFrameWidget(e.currentTarget);
                });

                indiEventCurPage = 1;
                $('.show-more-individual-events').off('click').on('click',function(e){
                    showMoreIndividualEvents(e);
                });

            });
            return module;
        };

        var showMoreIndividualEvents = function (e) {
            var totalEvents = parseInt($(e.currentTarget).attr("data-total-events"));
            var accountID = $('#post_ID').val();
            indiEventCurPage += 1;
            $.ajax({
                url: APL.ajax_url,
                type: 'POST',
                dataType: 'JSON',
                data:{
                    action : 'apollo_show_more_syndication_individual_events',
                    page : indiEventCurPage,
                    account_id : accountID
                },
                beforeSend: function () {
                    $(e.currentTarget).hide();
                    $(".wrap-sm-indi-events .spinner").show();
                },
                success: function (response) {
                    if(response.status === "TRUE" ) {
                        $(".wrap-sm-indi-events .spinner").hide();
                        $(".wrap-ind-events").append(response.resultHTML);
                        var totalDisplayedEvents = $('.wrap-ind-events input[name="meta-eventID[]"]').length;
                        if(totalEvents > totalDisplayedEvents){
                            $(e.currentTarget).show();

                        }
                    }
                }
            });
        };

        var displayCityOrZipSelection = function (selectionInput) {
            $('.cz-area-selection').hide();
            var curSelectionVal = $(selectionInput).val();
            switch (curSelectionVal){
                case 'CITY':
                    $('#city-area-selection').show();
                    break;
                case 'ZIP':
                    $('#zip-area-selection').show();
                    break;
                default:
                    break;
            }
        };

        var displayFieldsSelection = function (selectionInput) {
            $('.display-fields-area-selection').hide();
            var curSelectionVal = $(selectionInput).val();
            switch (curSelectionVal){
                case 'ALL':
                    $('#all-fields-selection').show();
                    break;
                case 'OWN':
                    $('#own-fields-selection').show();
                    break;
                default:
                    break;
            }
        };

        var displayCreateIFrameWidget = function (selectionInput) {
            $('.create-iframe-ws').hide();
            var curSelectionVal = $(selectionInput).val();
            switch (curSelectionVal){
                case 'YES':
                    $('#create-iframe-ws-blk').show();
                    break;
                default:
                    break;
            }
        };

        return module.init();
    })();

})(jQuery);