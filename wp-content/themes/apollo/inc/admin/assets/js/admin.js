function log(txt) {
    console.log(txt);
}

(function($) {


    $.fn.aplApplyRemoteDataToSelect2Box = function(){
        var data = this.data();
        var status = (typeof this.data().status !== 'undefined') ? this.data().status : 'publish';
        if(typeof data.enableRemote == undefined || typeof data.enableRemote == 0){
            this.select2();
        } else {
            this.select2({
                ajax: {
                    url: APL.ajax_url,
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        var q, page;
                        try {
                            q = params.term;
                            page = params.page;
                        } catch (e) {}
                        return {
                            q: q, // search term
                            page: page,
                            action: this.data().sourceUrl,
                            post_type:this.data().postType,
                            post_status: status
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 15) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 0
            });
        }
    };

    /**
     * Handle action in event page and its relation
     * Add admin mene
     *
     * */
    $(function() {

        // display pending counters (@ticket #15216)
        /** @Ticket #15758 - Prevent the navigation pending counter from the network admin */
        var checkNetworkAdmin = $('body.network-admin');
        if (checkNetworkAdmin.length <= 0 ) {
            $.ajax({
                method : 'get',
                data : {
                    action : 'apollo_navigation_pending_counter'
                },
                url : apollo_admin_obj.ajax_url
            }).done(function(res) {
                if (!res.data) {
                    return;
                }

                $.each(res.data, function(module, value) {
                    var $selector = $('#menu-posts-' + module + ' .wp-menu-name');
                    $selector.html($selector.text() + ' ' + value);
                });
            });
        }

        // hide Business Spotlight
        if ( apollo_admin_obj.isEnableBusinessSpot != 1 ) {
            $('#menu-posts-business_spotlight').hide();
        }

        // Change the name of the dropdown business type to avoid overrides taxonomy type in the query string ticket #11186
        $('.post-type-business_spotlight form#posts-filter #dropdown-business-type').attr('name', 'business_term');
        /** @Ticket #13573 */
        $('.post-type-event_spotlight form#posts-filter #dropdown-event-type').attr('name', 'event_term');

        $(document).on('click', '#apl-event-cat-hidden', function() {
            var $excepUserId = $('#hidden_except_user_ids');
            if ($(this).is(':checked')) {
                $excepUserId.removeAttr('disabled');
            }
            else {
                $excepUserId.attr('disabled', true);
            }
        });

        var _select2Selector = $('.apl_select2');
        if( _select2Selector.length > 0) {
            $.each(_select2Selector, function(i, v) {
                if ($(this).data().enableRemote == 1) {
                    $(this).aplApplyRemoteDataToSelect2Box();
                } else {
                    $(this).select2();
                }
                $(this).on("select2:select", function (e) {
                    var data = e.params.data;
                    /** @Ticket #12931 */

                    var selectVenue = $(e.currentTarget).closest('div').find('#_apollo_event_venue');
                    var accessibility = $(e.currentTarget).closest('#normal-sortables').find('#apollo-event-acb');
                    if (apollo_admin_obj.venue_access_mode != 0) {
                        if(selectVenue.length > 0 && accessibility.length > 0){
                            if(data.id != 0) {
                                if (!accessibility.hasClass('hidden')) {
                                    accessibility.addClass('hidden');
                                }
                            } else {
                                if (accessibility.hasClass('hidden')) {
                                    accessibility.removeClass('hidden');
                                }
                            }
                        }
                    }


                    if(typeof data !== 'undefined'){
                        var editItem = $(this).closest('.edit-item-wrap').find('.edit-item');
                        if(editItem.length > 0){
                            if(data.id == 0){
                                if(!editItem.hasClass('hidden')){
                                    editItem.addClass('hidden');
                                }
                                editItem.attr('href', '#');
                            }else{
                                if(typeof data.edit_link !== 'undefined'){
                                    if(editItem.hasClass('hidden')){
                                        editItem.removeClass('hidden');
                                    }
                                    editItem.attr('href', data.edit_link);
                                }else{
                                    var option = $(e.currentTarget).find('option[value="'+data.id+'"]');
                                    if(option.length > 0 && typeof option.attr('edit_link') !== 'undefined'){
                                        if(editItem.hasClass('hidden')){
                                            editItem.removeClass('hidden');
                                        }
                                        editItem.attr('href', option.attr('edit_link'));
                                    }
                                }
                            }
                        }
                    }
                });
            });
        }
        if($('body').hasClass('post-type-event')) {

            $('#change_user').click(function(){
                var is_append = $('.assoc_user_select').attr('append');
                if(is_append!=1){
                    $('.associte_user_loading').addClass('show');
                    $(this).attr('disabled','true');
                    $.ajax({
                        method : 'get',
                        data : {
                            action : 'apollo_get_user_asscoc_list'
                        },
                        url : apollo_admin_obj.ajax_url
                    }).done(function(res){
                        var html = '',
                        list = res.list;
                        /** @Ticket #14602 */
                        var selected = $('.assoc_user_select').find(':selected').val();
                        $.each(list,function(k,v){
                            html += '<option value="'+v.id+'" '+ (selected == v.id ? 'selected' : '') +'>'+v.user_nicename+'</option>';
                        });
                        if (selected > 0) {
                            $('.assoc_user_select option[value='+selected+']').remove();
                        }
                        $('.assoc_user_select').append(html);
                        $('.assoc_user_select').attr('append',1);
                        $('.current-user').hide();
                        $('.select-user').show();
                        $(this).removeAttr('disabled');
                        $('.associte_user_loading').removeClass('show');
                    });
                }else{
                    $('.current-user').hide();
                    $('.select-user').show();
                }
            });
            $('#cancel_change_user').click(function(){
                $('.select-user').hide();
                $('.current-user').show();
            });
            $('#confirm_change_user').click(function(){
                var currentUser = $('#confirm_change_user').attr('data-user-id');
                var newUser = $('#_apollo_event_associated_user').val();
                var postID = $('#confirm_change_user').attr('data-post-id');
                var messageConfirm = $('#confirm_change_user').attr('data-message-confirm');
                var messageWarning = $('#cancel_change_user').attr('data-message-warning');
                if(currentUser != newUser && newUser != 0){
                    if (confirm(messageConfirm)) {
                        $.ajax({
                            method : 'post',
                            url: apollo_admin_obj.ajax_url,
                            data: {
                                action: 'apollo_update_associated_user_event',
                                user_id: newUser,
                                post_id: postID,
                            },
                            success: function (res) {
                                if( res ) {
                                    location.reload();
                                }
                            }
                        });
                    }
                } else {
                    alert(messageWarning);
                }
            });

        }

        /*@ticket 18228:  Artist Directory Plugin > Admin > Syndication - Artist configuration*/
        var disabledContentSyndication = function (value, wrapSelect, wrapSearch){
            if (value == 0) {
                wrapSelect.addClass('disable-content');
                wrapSearch.addClass('disable-content');
            } else {
                wrapSelect.removeClass('disable-content');
                wrapSearch.removeClass('disable-content');
            }
        };

        /** @Ticket #13583 */
        $('#frm-org-syndication input:radio[name="_syndication_all_organization"]').off('change').on('change', function() {
            disabledContentSyndication($(this).val(), $('.wrap-select-org'), $('#org-syndication-search'));
        });

        /*@ticket 18228:  Artist Directory Plugin > Admin > Syndication - Artist configuration*/
        $('#frm-artist-syndication input:radio[name="_syndication_all_artist"]').off('change').on('change', function() {
            disabledContentSyndication($(this).val(), $('.wrap-select-artist'), $('#artist-syndication-search'));
        });

        /** @Ticket #13583 */
        var selectOrg = $('#frm-org-syndication input:radio[name="_syndication_all_organization"]:checked')
        if (selectOrg.length > 0) {
            disabledContentSyndication(selectOrg.val(), $('.wrap-select-org'), $('#org-syndication-search'));
        }

        /*@ticket 18228:  Artist Directory Plugin > Admin > Syndication - Artist configuration*/
        var selectArtist =  $('#frm-artist-syndication input:radio[name="_syndication_all_artist"]:checked');
        if (selectArtist.length > 0) {
            disabledContentSyndication(selectArtist.val(), $('.wrap-select-artist'), $('#artist-syndication-search'));
        }

        /**
         * Thienld : handle load more event for event theme loading list of individual events */
        var etIndiEventCurPage = 1;
        var isLoadingData = false;
        $("#individual-events .wrap-lm-et .spinner").hide();
        $('.et-sm-individual-events').off('click').on('click',function(e){
            showMoreIndividualEventsTheme(e);
        });

        $('.venue-section-iframesw').on('scroll', function() {
            if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                showMoreVenues("iframesw-show-more-venues");
            }
        });
        /**
         * TruongHn : handle for IFrame search widget module
         */


        if($('body').hasClass('post-type-spotlight')) {

            $('.of-color').wpColorPicker(); // WpColorPicker
        }

        if($('body').hasClass('post-type-iframesw')){

            $('.of-color').wpColorPicker(); // WpColorPicker


            var applyLogicParentChildCheckedHandler = function(e){
                var parentID = parseInt($(e.currentTarget).attr("data-parent-id"));
                if($(e.currentTarget).prop('checked') === true){
                    if(parentID !== 0){
                        // is child category
                        $('.parent_' + parentID).prop('checked',true);
                    }
                } else if(parentID === 0){
                    var categoryID = parseInt($(e.currentTarget).val());
                    $('.is_child_of_' + categoryID).prop('checked',false);
                }
            };


            var applyCheckedUncheckedAllHandler = function(element){
                var childCBName = $(element).attr('name');
                var totalPropChecked = $('input[name="'+childCBName+'[]"]:checked').length;
                var totalCBox = $('input[name="'+childCBName+'[]"]').length;
                if(totalCBox == totalPropChecked){
                    $(element).prop('checked',true);
                } else {
                    $(element).prop('checked',false);
                }
            };

            $('.meta_checkbox_all').each(function(index,item){
                applyCheckedUncheckedAllHandler(item);
            });

            $('#apollo-iframe-search-widget-setting' ).on( 'click', 'input[name="meta_venues[]"]', function (e) {
                applyCheckedUncheckedAllHandler($('input[name="meta_venues"]'));

            });

            $('#apollo-iframe-search-widget-setting' ).on( 'clickselect2:select', 'input[name="meta_cities[]"]', function (e) {
                applyCheckedUncheckedAllHandler($('input[name="meta_cities"]'));
            });

            $('#apollo-iframe-search-widget-setting' ).on( 'click', 'input[name="meta_categories[]"]', function (e) {
                applyLogicParentChildCheckedHandler(e);
                applyCheckedUncheckedAllHandler($('input[name="meta_categories"]'));
            });

            $('#apollo-iframe-search-widget-setting' ).on( 'click', 'input[name="meta_regions[]"]', function (e) {
                applyCheckedUncheckedAllHandler($('input[name="meta_regions"]'));
            });

            /* Handle Select all checkbox in detail page*/
            $('#apollo-iframe-search-widget-setting' ).on( 'click', '.meta_checkbox_all', function () {
                var value = $(this).attr('name');
                var _name = value+'[]';
                if ( $( this ).attr('checked') ) {
                    $( 'input[name="'+_name+'"]' ).attr( 'checked', true );
                } else {
                    $( 'input[name="'+_name+'"]' ).attr( 'checked', false );
                }
                if( value == 'meta_venues'){
                    var selectedVenueString = $("#all_venues_id").val();
                    if ( $( this ).attr('checked') ) {
                        $("#iframesw-show-more-venues").attr("data-selected-venues", selectedVenueString);
                        $("#iframesw-venues").val(selectedVenueString);
                    }else{
                        $("#iframesw-show-more-venues").attr("data-selected-venues", '');
                        $("#iframesw-venues").val('');
                    }
                }
            });

            $('#apollo-iframe-search-widget-setting').on('click', '.iframesw_multicb', function () {
                var _name = $(this).attr('name');
                var _id = _name.replace('[]', '_all');
                var lists = $('input[name="' + _name + '"]'),
                    check_all = true;

                $.each(lists, function (i, val) {
                    if (!$(val).attr('checked')) {
                        check_all = false;

                    }
                });
                $('#' + _id).attr('checked', check_all);
            });

            // Handle ON/OFF checkbox in detail page
            $('#apollo-iframe-search-widget-setting').on('change','.iframe-search-widget-enable',function() {
                var _select = $(this).attr('name');
                if($(this).val() == 'OFF') {
                    $('.'+_select).hide();
                } else {
                    $('.'+_select).show();
                }
            });


            /**
             * Thienld: handle action empty cache on bulk edit Search Widget Custom Post Type Screen */
            $('body.post-type-iframesw #doaction, body.post-type-iframesw #doaction2').on('click',function(e) {
                var ids = [];
                var _action = $('#bulk-action-selector-top').val();
                var _action2 = $('#bulk-action-selector-bottom').val();
                var executedAction = _action != '-1' ? _action : _action2;
                if(executedAction == '-1'){
                    alert("Please choose a bulk action before applying it.");
                    return false;
                }
                $('tr.type-iframesw input[type="checkbox"][name="post[]"]').each(function() {
                    if ($(this).attr('checked')) {
                        ids.push($(this).val());
                    }
                });
                if(ids.length <= 0){
                    alert("Please select search widget items you want to do actions on them.");
                    return false;
                }
                if( executedAction == 'ept-cache' ) {
                    $.ajax({
                        url: apollo_admin_obj.ajax_url,
                        method: 'post',
                        data: {
                            action: 'apollo_empty_cache_search_widget',
                            ifswids: ids
                        },
                        dataType: 'JSON',
                        success: function (res) {
                            if(res.success === "TRUE"){
                                window.location.reload();
                            } else {
                                alert(res.msg);
                                return false;
                            }
                        }
                    });
                }
            });
            /**
             * Thienld: END OF handle action empty cache on bulk edit Search Widget Custom Post Type Screen */
        }
        var showMoreVenues = function (e) {
            var totalVenues = parseInt($("#"+e).attr("data-total-venue"));
            var selectedVenues = $("#"+e).attr("data-selected-venues");
            etIndiEventCurPage += 1;
            var totalDisplayedVenues = $('.venue-section-iframesw input[name="meta_venues[]"]').length;
            if(totalVenues > totalDisplayedVenues){
                $.ajax({
                    url: APL.ajax_url,
                    type: 'POST',
                    dataType: 'JSON',
                    data:{
                        action : 'apollo_show_more_venue_iframe_search_widget',
                        page : etIndiEventCurPage,
                        selectedVenues : selectedVenues
                    },
                    beforeSend: function () {
                        $("#venue-area-selection .spinner").show();
                    },
                    success: function (response) {
                        if(response.status === "TRUE" ) {
                            $("#venue-area-selection .spinner").hide();
                            $(".venue-section-iframesw").append(response.resultHTML);
                        }
                    }
                });
            }
        };
        /**
         *
         * END :  TruongHn : handle for IFrame search widget module
         */

        $('.spotlight-feature').on('scroll', function() {
            if( ! isLoadingData
                && ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight)) {
                showMoreSpotFeature("selected-spotlight-event");
            }
        });

        var showMoreSpotFeature = function (e) {
            var totalEvents = $("#total-spot-feat-event").val();
            var totalDisplayedEvents = $('.spotlight-feature input[name="meta-spot[]"]').length;
            isLoadingData = true;
            etIndiEventCurPage += 1;
            var id = parseInt($('#'+e).attr("data-id"));
            if(totalEvents > totalDisplayedEvents) {
                $.ajax({
                    url: APL.ajax_url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        action: 'apollo_get_more_spot_feat_event',
                        cur_page: etIndiEventCurPage,
                        syndication_id: id
                    },
                    beforeSend: function () {
                        $(".spotlight-feature .spinner").show();
                    },
                    success: function (response) {
                        if (response.status === "TRUE") {
                            $(".spotlight-feature .spinner").hide();
                            $("#SpotFea").append(response.resultHTML);
                            isLoadingData = false;
                        }
                    }
                });
            }

        };


        var getSelectedToArray = function(e){
            var IDString = $("#"+e).val();
            return !_.isEmpty(IDString) ? IDString.split(",") : [];
        };

        var addID = function(event_id,e){
            if(parseInt(event_id) > 0){
                var selectedEvents = getSelectedToArray(e);
                if(selectedEvents.indexOf("" + event_id) >= 0){
                    return;
                }
                selectedEvents.push(event_id);
                updateEventAttribute(selectedEvents,e);
            }
        };

        var removeID = function(event_id,e){
            if(parseInt(event_id) > 0){
                var selectedVenues = getSelectedToArray(e);
                var indexOfUnCheckVenue = selectedVenues.indexOf("" + event_id);
                if(indexOfUnCheckVenue >= 0){
                    selectedVenues.splice(parseInt(indexOfUnCheckVenue), 1);
                    updateEventAttribute(selectedVenues,e);
                }
            }
        };

        var updateEventAttribute = function(selectedtArray,e){
            if(_.isArray(selectedtArray)){
                var selectedSpotString = selectedtArray.join(',');
                $("#"+e).val(selectedSpotString);
            }
        };
        /* Handel check/uncheck spot event Sydication */
        $('.spotlight-feature').off('click','input[name="meta-spot[]"]').on('click','input[name="meta-spot[]"]',function(){
            var eventId = parseInt($(this).val());
            if( $(this).prop('checked') == true){
                addID(eventId,'selected-spotlight-event');
            } else {
                removeID(eventId,'selected-spotlight-event');
            }
        });
        /* Handel check/uncheck feature event Sydication */
        $('.spotlight-feature').off('click','input[name="meta-feat[]"]').on('click','input[name="meta-feat[]"]',function(){
            var eventId = parseInt($(this).val());
            if( $(this).prop('checked') == true){
                addID(eventId,'selected-feature-event');
            } else {
                removeID(eventId,'selected-feature-event');
            }
        });

        /* Handel check/uncheck feature position event Sydication */
        var getSelectedFeaturePosToArray = function(){
            var venueIDString = $("#selected-feature-pos").val();
            return !_.isEmpty(venueIDString) ? venueIDString.split(",") : [];
        };

        var addFeaturePos = function(venue_id){
            if(venue_id){
                var selectedVenues = getSelectedFeaturePosToArray();
                if(selectedVenues.indexOf("" + venue_id) >= 0){
                    return;
                }
                selectedVenues.push(venue_id);
                updateFeaturePosAttribute(selectedVenues);
            }
        };

        var removeFeaturePos = function(venue_id){
            if(venue_id){
                var selectedVenues = getSelectedFeaturePosToArray();
                var indexOfUnCheckVenue = selectedVenues.indexOf("" + venue_id);
                if(indexOfUnCheckVenue >= 0){
                    selectedVenues.splice(parseInt(indexOfUnCheckVenue), 1);
                    updateFeaturePosAttribute(selectedVenues);
                }
            }
        };

        var updateFeaturePosAttribute = function(selectedSpotArray){
            if(_.isArray(selectedSpotArray)){
                var selectedSpotString = selectedSpotArray.join(',');
                $("#selected-feature-pos").val(selectedSpotString);
            }
        };

        $('.spotlight-feature').on('change','select[name="meta-pos[]"]',function(){
            var currentSelect = $(this).attr('data-current-selected');
            var newValue = $(this).val();
            $(this).attr('data-current-selected',newValue);
            removeFeaturePos(currentSelect);
            addFeaturePos(newValue);

        });



        $('.post-type-iframesw').on('click','#doaction',function() {
            var ids = [];
            var _action = $('#bulk-action-selector-top').val();
            $('input[name="post[]"]').each(function() {
                if ($(this).attr('checked')) {
                    ids.push($(this).val());
                }
            });
            if( _action == 'ept-cache') {
                $.ajax({
                    url: apollo_admin_obj.ajax_url,
                    method: 'post',
                    data: {
                        action: '',
                        ids: ids
                    },
                    success: function (res) {
                        window.location.reload();
                    }
                });
            }
        });

        var showMoreIndividualEventsTheme = function (e) {
            var totalEvents = parseInt($(e.currentTarget).attr("data-total-events"));
            var selectedEvent = $(e.currentTarget).attr("data-selected-events");
            etIndiEventCurPage += 1;
            $.ajax({
                url: APL.ajax_url,
                type: 'POST',
                dataType: 'JSON',
                data:{
                    action : 'apollo_show_more_event_theme_individual_events',
                    page : etIndiEventCurPage,
                    selectedEvent : selectedEvent,
                    orderBy: eventSortType,
                    sortDefault: eventSortDefault
                },
                beforeSend: function () {
                    $(e.currentTarget).hide();
                    $("#individual-events .wrap-lm-et .spinner").show();
                },
                success: function (response) {
                    if(response.status === "TRUE" ) {
                        $("#individual-events .wrap-lm-et .spinner").hide();
                        $("#individual-events .wrap-lm-et").before(response.resultHTML);
                        var totalDisplayedEvents = $('#individual-events input[name="events[]"]').length;
                        if(totalEvents > totalDisplayedEvents){
                            $(e.currentTarget).show();
                        }
                    }
                }
            });
        };

        /**
         * Thienld: handling get final selection events on admin event theme tools */
        /* add method remove to underscore.js */

        var getSelectedEventThemeToolToArray = function(){
            var eventIDString = $(".et-sm-individual-events").attr("data-selected-events");
            return !_.isEmpty(eventIDString) ? eventIDString.split(",") : [];
        };

        var addThemeToolsIndividualEvents = function(event_id){
            if(parseInt(event_id) > 0){
                var selectedEvents = getSelectedEventThemeToolToArray();
                if(selectedEvents.indexOf("" + event_id) >= 0){
                    return;
                }
                selectedEvents.push(event_id);
                updateThemeToolsIndividualEventsAttribute(selectedEvents);
            }
        };

        var removeThemeToolsIndividualEvents = function(event_id){
            if(parseInt(event_id) > 0){
                var selectedEvents = getSelectedEventThemeToolToArray();
                var indexOfUnCheckEvent = selectedEvents.indexOf("" + event_id);
                if(indexOfUnCheckEvent >= 0){
                    selectedEvents.splice(parseInt(indexOfUnCheckEvent), 1);
                    updateThemeToolsIndividualEventsAttribute(selectedEvents);
                }
            }
        };

        var updateThemeToolsIndividualEventsAttribute = function(selectedEventArray){
            if(_.isArray(selectedEventArray)){
                var selectedEventString = selectedEventArray.join(',');
                $(".et-sm-individual-events").attr("data-selected-events",selectedEventString);
                $("#final_selected_events").val(selectedEventString);
            }
        };

        $('#individual-events').off('click','input[name="events[]"]').on('click','input[name="events[]"]',function(){
            var eventId = parseInt($(this).val());
            if( $(this).prop('checked') == true){
                addThemeToolsIndividualEvents(eventId);
            } else {
                removeThemeToolsIndividualEvents(eventId);
            }
        });

        // START @ticket #11493

        // hide spinner of Associated Orgs
        var etIndiOrgCurPage = 1;
        $("#individual-orgs .wrap-lm-et .spinner").hide();
        $('.et-sm-individual-orgs').off('click').on('click', function(e) {
            var element = {
                type: 'org',
                spinner: '#individual-orgs .wrap-lm-et .spinner',
                dataTotal: 'data-total-orgs',
                dataSelected: 'data-selected-orgs',
                action: 'apollo_show_more_event_theme_individual_orgs',
                items: '#individual-orgs input[name="associated_orgs[]"]',
                wrapper: '#individual-orgs .wrap-lm-et'
            };
            showMoreIndividualOrgsOrVenuesTheme(e, element);
        });

        // hide spinner of Associated Venues
        var etIndiVenueCurPage = 1;
        $("#individual-venues .wrap-lm-et .spinner").hide();
        $('.et-sm-individual-venues').off('click').on('click', function(e) {
            var element = {
                type: 'venue',
                spinner: '#individual-venues .wrap-lm-et .spinner',
                dataTotal: 'data-total-venues',
                dataSelected: 'data-selected-venues',
                action: 'apollo_show_more_event_theme_individual_venues',
                items: '#individual-venues input[name="associated_venues[]"]',
                wrapper: '#individual-venues .wrap-lm-et'
            };
            showMoreIndividualOrgsOrVenuesTheme(e, element);
        });

        var showMoreIndividualOrgsOrVenuesTheme = function (e, element) {
            var totalItems = parseInt($(e.currentTarget).attr(element.dataTotal));
            var selectedItem = $(e.currentTarget).attr(element.dataSelected);

            // increase paging
            if ( element.type == 'org' ) {
                var page = (etIndiOrgCurPage += 1);
            } else if ( element.type == 'venue' ) {
                var page = (etIndiVenueCurPage += 1);
            }

            $.ajax({
                url: APL.ajax_url,
                type: 'POST',
                dataType: 'JSON',
                data:{
                    action : element.action,
                    page : page,
                    selectedItem : selectedItem
                },
                beforeSend: function () {
                    $(e.currentTarget).hide();
                    $(element.spinner).show();
                },
                success: function (response) {
                    if(response.status === "TRUE" ) {
                        $(element.spinner).hide();
                        $(element.wrapper).before(response.resultHTML);
                        var totalDisplayedEvents = $(element.items).length;
                        if(totalItems > totalDisplayedEvents){
                            $(e.currentTarget).show();
                        }
                    }
                }
            });
        };

        $('#individual-orgs').off('click','input[name="associated_orgs[]"]').on('click','input[name="associated_orgs[]"]', function()
        {
            var orgId = parseInt($(this).val()),
                element = {
                    className: '.et-sm-individual-orgs',
                    dataAttr: 'data-selected-orgs',
                    finalID: '#final_selected_orgs'
                };

            if ( $(this).prop('checked') == true ) {
                addThemeToolsIndividualOrgsOrVenues(element, orgId);
            } else {
                removeThemeToolsIndividualOrgsOrVenues(element, orgId);
            }
        });

        $('#individual-venues').off('click','input[name="associated_venues[]"]').on('click','input[name="associated_venues[]"]', function()
        {
            var venueId = parseInt($(this).val()),
                element = {
                    className: '.et-sm-individual-venues',
                    dataAttr: 'data-selected-venues',
                    finalID: '#final_selected_venues'
                };

            if( $(this).prop('checked') == true ){
                addThemeToolsIndividualOrgsOrVenues(element, venueId);
            } else {
                removeThemeToolsIndividualOrgsOrVenues(element, venueId);
            }
        });

        var getSelectedOrgOrVenueThemeToolToArray = function(element)
        {
            var ids = $(element.className).attr(element.dataAttr);
            return !_.isEmpty(ids) ? ids.split(",") : [];
        };

        var addThemeToolsIndividualOrgsOrVenues = function(element, itemId)
        {
            if( parseInt(itemId) > 0 )
            {
                var selectedItems = getSelectedOrgOrVenueThemeToolToArray(element);
                if( selectedItems.indexOf("" + itemId) >= 0 ) {
                    return;
                }
                selectedItems.push(itemId);
                updateThemeToolsIndividualOrgsOrVenuesAttribute(element, selectedItems);
            }
        };

        var removeThemeToolsIndividualOrgsOrVenues = function(element, itemId)
        {
            if( parseInt(itemId) > 0 )
            {
                var selectedItems = getSelectedOrgOrVenueThemeToolToArray(element);
                var indexOfUnCheckItem = selectedItems.indexOf("" + itemId);
                if( indexOfUnCheckItem >= 0 )
                {
                    selectedItems.splice(parseInt(indexOfUnCheckItem), 1);
                    updateThemeToolsIndividualOrgsOrVenuesAttribute(element, selectedItems);
                }
            }
        };

        var updateThemeToolsIndividualOrgsOrVenuesAttribute = function(element, selectedItemArray)
        {
            if( _.isArray(selectedItemArray) )
            {
                var selectedItemString = selectedItemArray.join(',');
                $(element.className).attr(element.dataAttr, selectedItemString);
                $(element.finalID).val(selectedItemString);
            }
        };
        // END @ticket #11493


        /**
         * TRuongHN: handling get final selection venues on admin IFrame search widget */
        var getSelectedVenueIFrameSWToArray = function(){
            var venueIDString = $("#iframesw-show-more-venues").attr("data-selected-venues");
            return !_.isEmpty(venueIDString) ? venueIDString.split(",") : [];
        };

        var addIFrameSWVenues = function(venue_id){
            if(parseInt(venue_id) > 0){
                var selectedVenues = getSelectedVenueIFrameSWToArray();
                if(selectedVenues.indexOf("" + venue_id) >= 0){
                    return;
                }
                selectedVenues.push(venue_id);
                updateIFrameSWVenuesAttribute(selectedVenues);
            }
        };

        var removeIFrameSWVenues = function(venue_id){
            if(parseInt(venue_id) > 0){
                var selectedVenues = getSelectedVenueIFrameSWToArray();
                var indexOfUnCheckVenue = selectedVenues.indexOf("" + venue_id);
                if(indexOfUnCheckVenue >= 0){
                    selectedVenues.splice(parseInt(indexOfUnCheckVenue), 1);
                    updateIFrameSWVenuesAttribute(selectedVenues);
                }
            }
        };

        var updateIFrameSWVenuesAttribute = function(selectedVenueArray){
            if(_.isArray(selectedVenueArray)){
                var selectedVenueString = selectedVenueArray.join(',');
                $("#iframesw-show-more-venues").attr("data-selected-venues",selectedVenueString);
                $("#iframesw-venues").val(selectedVenueString);
            }
        };

        $('#venue-area-selection').off('click','input[name="meta_venues[]"]').on('click','input[name="meta_venues[]"]',function(){
            var venueId = parseInt($(this).val());
            if( $(this).prop('checked') == true){
                addIFrameSWVenues(venueId);
            } else {
                removeIFrameSWVenues(venueId);
            }
        });

        /**
         * end handling get final selection venues on admin IFrame search widget */

        /**
         * Thienld: apply logic between classified city and tmp city required field */
        if($('body').hasClass('post-type-classified')){
            if ( $(".apl-territory-city-tmp").val() != ''){
                $(".apl-territory-city").removeClass('required');
                $(".apl-territory-city").removeClass('apollo-input-error');
                $("[data-id=\"_apl_classified_address[_classified_city]-required\"]").hide();
            }
            if ( $(".apl-territory-state-tmp").val() != ''){
                $(".apl-territory-state").removeClass('required');
                $(".apl-territory-state").removeClass('apollo-input-error');
                $("[data-id=\"_apl_classified_address[_classified_sate]-required\"]").hide();
            }
            $(".apl-territory-city-tmp").on("blur",function(e){
                var curVal = $(e.currentTarget).val();
                if(curVal != ''){
                    $(".apl-territory-city").removeClass('required');
                    $(".apl-territory-city").removeClass('apollo-input-error');
                    $("[data-id=\"_apl_classified_address[_classified_city]-required\"]").hide();
                } else {
                    $(".apl-territory-city").addClass('required');
                }
            });

            $(".apl-territory-city").on("change",function(e){
                var cityVal = $(e.currentTarget).val();
                if(cityVal === '' || parseInt(cityVal) === 0 ){
                    $(".apl-territory-city-tmp").trigger('blur');
                }
            });

            $(".apl-territory-state-tmp").on("blur",function(e){
                var curVal = $(e.currentTarget).val();
                if(curVal != ''){
                    $(".apl-territory-state").removeClass('required');
                    $(".apl-territory-state").removeClass('apollo-input-error');
                    $("[data-id=\"_apl_classified_address[_classified_state]-required\"]").hide();
                } else {
                    $(".apl-territory-state").addClass('required');
                }
            });

            $(".apl-territory-state").on("change",function(e){
                var stateVal = $(e.currentTarget).val();
                if(stateVal === '' || parseInt(stateVal) === 0 ){
                    $(".apl-territory-state-tmp").trigger('blur');
                }
            });
        }

        /**
         * handle search event in event form
         * */
        $('#apollo-post-search-input').keydown(function(event){
            var keyCode = (event.keyCode ? event.keyCode : event.which);
            if (keyCode == 13) {
                $('#apollo-event-search-submit').trigger('click');
                return false;
            }
        });

        $('#insert-media-button').click(function() {
            $(this).attr('data-click', true);
        });

        $('#postimagediv').on('click', '#set-post-thumbnail', function() {
            $('#insert-media-button').attr('data-click', false);
        });

        $('#apollo-event-search-submit').click(function() {
            window.location.href = $(this).data().url+ '&s='+ $('#apollo-post-search-input').val();

        });

        /** End handle search event in event form */

        $('.remove-event-import-file').off('click').on('click',function(e){
            var result = confirm("Would you like to delete this file?");
            return result == true;
        });

        $('.apollo-import-event-files #doaction, .apollo-import-event-files #doaction2').off('click').on('click',function(e){
            if($(e.currentTarget).parent().find('select').val() != -1){
                if($('#the-list .check-column input[type="checkbox"]:checked').length <= 0){
                    alert("Choose file(s) to apply this action.");
                    return false;
                } else {
                    var result = confirm("Would you like to delete selected file(s)?");
                    return result == true;
                }
            } else {
                alert("Choose a bulk action in order to apply it.");
                return false;
            }
        });


        $('#posts-filter').on('click', 'input[name=save_event_opts]', function(event) {
            event.preventDefault();
            var _form = $(this).closest('form'),
                _homeSpots = $('.chb_home_spot'),
                getIds = function(obj) {
                    var output = [];
                    $.each(obj, function(i, v) {
                        if (($(v).attr('type') == 'checkbox')) {
                            if ($(this).is(':checked')) {
                                output.push($(v).val());
                            } else {
                                output.push(0);
                            }
                        } else {
                            output.push($(v).val());
                        }
                    });

                    return output;
                };

            $('input[name=event_ids]').val(getIds($('.event_ids')).join(','));
            $('input[name=chb_home_spot]').val(getIds($('.chb_home_spot')).join(','));
            $('input[name=tab_location]').val(getIds($('.tab_location')).join(','));
            $('input[name=tab_features]').val(getIds($('.tab_features')).join(','));
            $('input[name=chb__is_category_spotlight]').val(getIds($('.chb__is_category_spotlight')).join(','));
            $('input[name=chb__is_category_feature]').val(getIds($('.chb__is_category_feature')).join(','));
            handleSaveEventOptions(_form);
            return false;
        });

        function handleSaveEventOptions(eventOptsForm){
            $.ajax({
                url: apollo_admin_obj.ajax_url,
                method: 'post',
                dataType: 'JSON',
                data: {
                    action: 'apollo_save_custom_event_options',
                    event_options_data: eventOptsForm.serialize()
                },
                success: function(res) {
                    if(res.result === 'SUCCESS') {
                        window.location.reload();
                    } else {
                        alert(res.result.data.message);
                    }
                }
            });
        }

        // Auto check parent when select the child
        $('ul.categorychecklist li ul li').on('click', 'input[type=checkbox]', function() {

            if ($('#_apl_event_term_primary_id').length && $('#_apl_event_term_primary_id').val() == "0") return;

            var parent = $(this).closest('ul').closest('li').children('label').children('input[type=checkbox]'),
                otherChilds = parent.closest('label').next('ul').find('input:checked');
            if (parent.attr('disabled') != 'disabled' && $(this).attr('checked') != undefined) {
                parent.attr('checked', 'checked');
            } else if ( otherChilds.length == 0 ) {
                parent.removeAttr('checked');
            }
        });

        // Hide event cats are theme tool
        var _eventCatLists = $('#event-typechecklist input');
        var arrThemeTools = apollo_admin_obj.theme_tools;
        // ensure arrThemeTools always is type of array for using indexOf function on javascript.
        if (typeof arrThemeTools == 'object') {
            var _convertToArr = $.map(arrThemeTools, function(value, index) {
                return [value];
            });
            arrThemeTools = _convertToArr;
        }

        $.each(_eventCatLists, function(i, v) {
            if ( arrThemeTools.indexOf($(v).val()) != -1 ) {
                $(v).closest('li').hide();
                var _currentElm = $(v).closest('label');
                _currentElm.html(_currentElm.html()+ ' (Theme Tool)')
            }
        });

        var _dropEventTypes = $('#dropdown-event-type option');

        $.each(_dropEventTypes, function(i, v) {
            var _value = $(v).attr('value');
            if ( apollo_admin_obj.theme_tools_cat_slug.indexOf(_value) != -1 ) {
                $(v).html($(v).html()+ (' (Theme Tool)'))
            }
        });


        $('#event-theme-tool-frm [name=submit]').click(function() {
            if (!$('#dropdown-event-type').val()) {
                $('#dropdown-event-type').addClass('error');
                return false;
            }
        });

        // START @ticket #11557 : select/un-select all items
        $('#event-theme-tool-frm [name="btn-select-all"]').click(function() {
            selectAllCheckBoxes(true, $(this).data().refer);
        });
        $('#event-theme-tool-frm [name="btn-un-select-all"]').click(function() {
            selectAllCheckBoxes(false, $(this).data().refer);
        });
        var selectAllCheckBoxes = function(isCheck, element) {
            this.isCheck = isCheck ? true : false;
            var self     = this;

            $(element).each(function() {
                var isCurrentCheck = $(this).attr('checked');

                // behaviour: select all, current is un-check
                if ( self.isCheck && !isCurrentCheck ) {
                    $(this).trigger('click');
                }

                // behaviour: un-select all, current is check
                if ( !self.isCheck && isCurrentCheck ) {
                    $(this).trigger('click');
                }
            });
        };

        // Hook into form submit to disable all inputs are being posted along with the form.
        // Because PHP will not process if max_input_vars in php.ini > 1000 input items
        $( "#event-theme-tool-frm" ).submit(function() {
            $('.remove-items').remove();
            $('#event-theme-tool-frm').submit();
        });

        // sort events by title
        var eventSortType = 'date',
            eventSortDefault = '';
        $('#event-theme-tool-frm [name="btn-sort-event-by-title"]').click(function() {
            sortEvents($(this).data().sortType);
        });
        // sort events by date
        $('#event-theme-tool-frm [name="btn-sort-event-by-date"]').click(function() {
            sortEvents($(this).data().sortType);
        });
        var sortEvents = function(sortType) {
            eventSortDefault = (eventSortDefault == '' || eventSortDefault == 'DESC') ? 'ASC' : 'DESC';

            // reset current paging
            etIndiEventCurPage = 1;

            // get data
            eventSortType     = sortType;
            var totalEvents   = parseInt($('.et-sm-individual-events').attr('data-total-events')),
                selectedEvent = $('.et-sm-individual-events').attr('data-selected-events');

            // remove current events in the list
            $('#event-theme-tool-frm #individual-events').find('.event-item').remove();
            $('#event-theme-tool-frm #individual-events').find('br').remove();

            var _blockUI = {
                message: APL.resource.loading.message,
                css: $.parseJSON(APL.resource.loading.image_style)
            };

            // load events
            $.ajax({
                url: APL.ajax_url,
                type: 'POST',
                dataType: 'JSON',
                data:{
                    action : 'apollo_show_more_event_theme_individual_events',
                    page : etIndiEventCurPage,
                    selectedEvent : selectedEvent,
                    orderBy: eventSortType,
                    sortDefault: eventSortDefault
                },
                beforeSend: function () {
                    $(window).block(_blockUI);
                },
                success: function (response) {
                    $('body').unblock();
                    if(response.status === "TRUE" ) {
                        $("#individual-events .wrap-lm-et .spinner").hide();
                        $("#individual-events .wrap-lm-et").before(response.resultHTML);
                        var totalDisplayedEvents = $('#individual-events input[name="events[]"]').length;
                        if(totalEvents > totalDisplayedEvents){
                            $('.et-sm-individual-events').show();
                        }
                    }
                }
            });
        };
        // END @ticket #11557

        $('#event-theme-tool-frm [name="leave_as_normal"]').click(function() {
            console.log($(this).attr('checked'));
            if ($(this).attr('checked')) {
                $('select[name="entity_type"]').closest('tr').hide();
                $('input[name="location_type"]').closest('tr').hide();
                $('select[name="tt_order"]').closest('tr').hide();
                $('#individual-events').closest('tr').hide();
                $('#individual-orgs').closest('tr').hide();
                $('#individual-venues').closest('tr').hide();
            } else {
                $('select[name="entity_type"]').closest('tr').show();

                if ($('select[name="entity_type"]').val() == 'ALL') {
                    $('input[name="location_type"]').closest('tr').show();
                    $('input[name="location_type"]').closest('tr').removeClass('force-hidden');
                } else {
                    $('#individual-events').closest('tr').show  ();
                }

                $('#individual-orgs').closest('tr').show();
                $('#individual-venues').closest('tr').show();

                $('select[name="tt_order"]').closest('tr').show();
            }
        });

        if ( $('#them-tool-lists').length ) {
            var themeToolLists = $('#them-tool-lists').val().split(',');
            $.each(themeToolLists, function(i, v) {
                if (v) {
                    if ( !$('#dropdown-event-type option[value='+v+']').attr('selected') ) {
                        $('#dropdown-event-type option[value='+v+']').attr('disabled', 'disabled');
                    }
                }
            });
        }

        var themeToolFn = function(elm) {
            if ( elm.val() == 'ALL' ) {
                $('#event-theme-tool-frm #individual-events').closest('.form-field').hide();
                $('#event-theme-tool-frm .apl-list-panel.c2').closest('.form-field').show();
            } else {
                var isLeaveAsNormalCheck = $('#event-theme-tool-frm [name="leave_as_normal"]').attr('checked');
                if ( !isLeaveAsNormalCheck ) {
                    $('#event-theme-tool-frm #individual-events').closest('.form-field').show();
                }
                $('#event-theme-tool-frm .apl-list-panel.c2').closest('.form-field').hide();
            }
        };
        themeToolFn($('#event-theme-tool-frm [name="entity_type"]'));
        $('#event-theme-tool-frm [name="entity_type"]').change(function() {
            themeToolFn($(this));
        });

        $('#frm-list-theme-tool [name="submitnew"]').click(function(e) {
            if ( !$('#dropdown-event-type').val() ) {
                $('#dropdown-event-type').addClass('error');
                return false;
            }else{
                /** Vandd custom default submit*/
                var current = $(e.currentTarget);
                var addNewLink = current.attr('add_new_link');
                if(addNewLink != ''){
                    addNewLink += '&add_new=1&slug=' + $('#dropdown-event-type').val();
                    window.location.href = addNewLink;
                }
            }
        });
        $('#event-theme-tool-frm [name=location_type]').click(function() {
            if($(this).val() == 1) {
                $('#event-theme-tool-frm .zip').attr('disabled', 'disabled');
                $('#event-theme-tool-frm .zip').attr('checked', false);
                $('#event-theme-tool-frm .city').removeAttr('disabled');
            } else {
                $('#event-theme-tool-frm .zip').removeAttr('disabled');
                $('#event-theme-tool-frm .city').attr('disabled', 'disabled');
                $('#event-theme-tool-frm .city').attr('checked', false);
            }
        });
        /**End theme tool page*/

        $('#createusersub').click(function() {


            if( $('#noconfirmation').is(':checked') ) {
                $.ajax({
                    url: apollo_admin_obj.ajax_url,
                    method: 'post',
                    data: {
                        action: 'apollo_keep_add_new_user_password',
                        password: $('#createuser #pass1-text').val()
                    },
                    success: function(res) {

                    }
                });
            }
        });

        $('.apl-territory-state').on('change', function() {
            var $this = $(this);
            var terrData = APL.terrData;
            zipData = [];

            var wrapperClass = $this.data().wrapper;
            //cities
            try {
                if($this.val() != 0){
                    var cityData = terrData[$this.val()];
                    $.each(cityData, function(i1, v1) {
                        $.each(v1, function(i2, v2) {
                            zipData.push(i2);
                        })
                    })
                } else {
                    $.each(terrData, function(i, v) {
                        if(v && v !== "null" && v!== "undefined"){
                            cityData = $.extend(cityData,v);
                            $.each(v, function(i1, v1) {
                                $.each(v1, function(i2, v2) {
                                    zipData.push(i2);
                                })
                            })
                        }
                    })
                }
            } catch(e) {
                cityData = '';
            }

            var cityWrapper = wrapperClass ? $this.closest(wrapperClass).find('.apl-territory-city') : '.apl-territory-city';
            if($(cityWrapper).length){
                var opts = $('.apl-territory-city option');
                var html = "<option value=0>"+opts.first().html()+"</option>";

                if (cityData) {

                    var cityDataArr = $.map(cityData, function(value, index) {
                        return [index];
                    });

                    cityDataArr.sort(function(a, b) {
                        if(a < b) return -1;
                        if(a > b) return 1;
                        return 0;
                    });

                    $.each(cityDataArr, function(i, v) {
                        var value = typeof i == 'number' ? v : i;
                        html += '<option value="'+value+'">'+value+'</option>';
                    });
                }
                $(cityWrapper).html(html);
            }

            var zipcodeWrapper = wrapperClass ? $this.closest(wrapperClass).find('.apl-territory-zipcode') : '.apl-territory-zipcode';
            if($(zipcodeWrapper).length > 0){
                var opts2 = $('.apl-territory-zipcode option');
                var html2 = "<option value=0>"+opts2.first().html()+"</option>";

                if (zipData) {
                    zipData.sort(function(a, b) {
                        if(a < b) return -1;
                        if(a > b) return 1;
                        return 0;
                    });
                    $.each(zipData, function(i, v) {
                        html2 += '<option value="'+v+'">'+v+'</option>';
                    });
                    $(zipcodeWrapper).html(html2);
                }
            }

            /** @Ticket #13874 */
            var neighborhood = $('.apl-territory-neighborhood');
            var wrapNeighborhood = neighborhood.closest('.neighborhood-box');
            neighborhood.empty();
            if(!wrapNeighborhood.hasClass('hidden')) {
                wrapNeighborhood.addClass('hidden');
            }
        });

        $('.apl-territory-city').on('change', function() {
            var $this = $(this);
            var terrData = APL.terrData;
            var selectedState = 0;
            var selectedCity = $this.val();
            var stateData = [];
            var cityData = '';
            var zipData = '';
            var wrapperClass = $this.data().wrapper;
            var stateWrapper = wrapperClass ? $this.closest(wrapperClass).find('.apl-territory-state') : '.apl-territory-state';
            var state = $(stateWrapper).val();
            var done = false;
            try {

                if(selectedCity != 0){
                    if ( state != 0 && typeof state !== "undefined" ) { //case up to down
                        var zipData = terrData[state][selectedCity] ;
                        showNeighborhood(selectedCity, state);
                    } else {
                        $.each(terrData, function(i, v) {
                            if(v && v !== "null" && v!== "undefined"){
                                stateData.push(i);
                                if(!done) {
                                    $.each(v, function (i1, v1) {
                                        if (i1 === selectedCity) {
                                            selectedState = i;
                                            zipData = v1;
                                            cityData = v;
                                            done = true;
                                            return false;//get first state appropriate and break
                                        }
                                    })
                                }
                            }
                        });
                        showNeighborhood(selectedCity, selectedState);
                    }
                } else {
                    $(stateWrapper).trigger('change');
                    return;
                }

            } catch(e){
                zipData = '';
            }
            if (state == 0 ) {
                if (stateData.length > 0) {
                    stateData.sort = function(a, b) {
                        if(a < b) return -1;
                        if(a > b) return 1;
                        return 0;
                    };
                    var opts = $('.apl-territory-state option');
                    var html = "<option value=0>"+opts.first().html()+"</option>";
                    var SelectedText = opts.first().html();
                    $.each(stateData, function(i, v) {
                        var selected = '';
                        if(v == selectedState){
                            selected = 'selected="selected"';
                            SelectedText = v ;
                        }
                        html += '<option value="'+v+'"'+selected+'>'+v+'</option>';
                    });
                    $(stateWrapper).html(html);
                }
            }

            if (cityData) {
                var cityDataArr = $.map(cityData, function(value, index) {
                    return [index];
                });

                cityDataArr.sort(function(a, b) {
                    if(a < b) return -1;
                    if(a > b) return 1;
                    return 0;
                });

                var opts1 = $('.apl-territory-city option');
                var html1 = "<option value=0>"+opts1.first().html()+"</option>";
                var SelectedText = opts1.first().html();
                $.each(cityDataArr, function(i, v) {
                    var selected = v == selectedCity ? 'selected="selected"' : '';
                    html1 += '<option value="'+v+'"'+selected+'>'+v+'</option>';
                    SelectedText = v ;
                });
                var outputWrapper = wrapperClass ? $this.closest(wrapperClass).find('.apl-territory-city') : '.apl-territory-city';
                $(outputWrapper).html(html1);
            }

            var zipcodeWrapper = wrapperClass ? $this.closest(wrapperClass).find('.apl-territory-zipcode') : '.apl-territory-zipcode';
            if(zipcodeWrapper) {
                var opts2 = $('.apl-territory-zipcode option');
                var html2 = "<option value=0>" + opts2.first().html() + "</option>";

                if (zipData) {
                    var zipDataArr = $.map(zipData, function(value, index) {
                        return [index];
                    });

                    zipDataArr.sort(function(a, b) {
                        if(a < b) return -1;
                        if(a > b) return 1;
                        return 0;
                    });

                    $.each(zipDataArr, function (i, v) {
                        html2 += '<option value="' + v + '">' + v + '</option>';
                    });
                }
                $(zipcodeWrapper).html(html2);
            }

        });

        var showNeighborhood = function (city, state) {
            /** @Ticket #13874 */
            var neighborhood = $('.apl-territory-neighborhood');
            if( neighborhood.length > 0 ) {
                var wrapNeighborhood = neighborhood.closest('.neighborhood-box');
                if(city == 0) {
                    if(!wrapNeighborhood.hasClass('hidden')) {
                        wrapNeighborhood.addClass('hidden');
                    }
                    neighborhood.empty();
                } else {
                    $.ajax({
                        url: apollo_admin_obj.ajax_url,
                        method: 'post',
                        dataType: 'json',
                        data:{
                            action : 'apollo_admin_get_neighborhood_by_city',
                            parent_code: city,
                            state: state
                        },
                        beforeSend: function () {
                            $(window).block(jQuery.apl.blockUI);
                        },
                        success: function (response) {
                            if (response.status) {
                                if(response.data != '') {
                                    if(wrapNeighborhood.hasClass('hidden')) {
                                        wrapNeighborhood.removeClass('hidden');
                                    }
                                } else {
                                    if(!wrapNeighborhood.hasClass('hidden')) {
                                        wrapNeighborhood.addClass('hidden');
                                    }
                                }
                                neighborhood.empty();
                                neighborhood.append(response.data);
                            } else {
                                neighborhood.empty();
                            }
                            $(window).unblock();
                        }
                    });
                }
            }
        };

        $('.apl-territory-zipcode').on('change', function() {
            var wrapperClass = $(this).data().wrapper;
            var cityWrapper = wrapperClass ? $(this).closest(wrapperClass).find('.apl-territory-city') : '.apl-territory-city';

            if ( $(cityWrapper).val() == 0 ){
                var $this = $(this);
                var terrData = APL.terrData;
                var selectedState = 0;
                var selectedCity = 0;
                var selectedZip = $this.val();
                var stateData = [];
                var cityData = '';
                var zipData = '';
                var done = false;
                try {
                    if($this.val() != 0){
                        $.each(terrData, function(i, v) {
                            if(v && v !== "null" && v!== "undefined"){
                                stateData.push(i);
                                $.each(v, function(i1, v1) {
                                    $.each(v1, function(i2, v2) {
                                        if(i2  == selectedZip){
                                            cityData = v;
                                            zipData = v1;
                                            selectedState = i;
                                            selectedCity = i1;
                                            done = true;
                                            return false;
                                        }
                                    });
                                    if(done == true) return false;
                                })
                            }
                        })
                    }
                } catch(e){
                    zipData = '';
                }

                if (stateData.length > 0) {
                    var opts = $('.apl-territory-state option');
                    var html = "<option value=0>"+opts.first().html()+"</option>";
                    var SelectedText = opts.first().html();

                    stateData.sort = function(a, b) {
                        if(a < b) return -1;
                        if(a > b) return 1;
                        return 0;
                    };

                    $.each(stateData, function(i, v) {
                        var selected = '';
                        if(v == selectedState){
                            selected = 'selected="selected"';
                            SelectedText = v ;
                        }
                        html += '<option value="'+v+'"'+selected+'>'+v+'</option>';

                    });
                    var outputWrapper = wrapperClass ? $this.closest(wrapperClass).find('.apl-territory-state') : '.apl-territory-state';
                    $(outputWrapper).html(html);
                }

                if (cityData) {
                    var opts1 = $('.apl-territory-city option');
                    var html1 = "<option value=0>"+opts1.first().html()+"</option>";
                    var SelectedText = opts1.first().html();

                    var cityDataArr = $.map(cityData, function(value, index) {
                        return [index];
                    });

                    cityDataArr.sort(function(a, b) {
                        if(a < b) return -1;
                        if(a > b) return 1;
                        return 0;
                    });

                    $.each(cityDataArr, function(i, v) {
                        var selected = '';
                        if(v == selectedCity){
                            selected = 'selected="selected"';
                            SelectedText = v ;
                        }
                        html1 += '<option value="'+v+'"'+selected+'>'+v+'</option>';
                    });
                    var outputWrapper = wrapperClass ? $this.closest(wrapperClass).find('.apl-territory-city') : '.apl-territory-city';
                    $(outputWrapper).html(html1);

                    /** @Ticket #14181 */
                    showNeighborhood(selectedCity, selectedState);
                }


                if (zipData) {
                    var opts2 = $('.apl-territory-zipcode option');
                    var html2 = "<option value=0>"+opts2.first().html()+"</option>";
                    var SelectedText = opts2.first().html();

                    var zipDataArr = $.map(zipData, function(value, index) {
                        return [index];
                    });

                    zipDataArr.sort(function(a, b) {
                        if(a < b) return -1;
                        if(a > b) return 1;
                        return 0;
                    });

                    $.each(zipDataArr, function(i, v) {
                        var selected = '';
                        if(v == selectedZip){
                            selected = 'selected="selected"';
                            SelectedText = v ;
                        }
                        html2 += '<option value="'+v+'"'+selected+'>'+v+'</option>';
                    });
                }
                var outputWrapper = $this.data().wrapper ? $this.closest($this.data().wrapper).find('.apl-territory-zipcode') : '.apl-territory-zipcode';
                $(outputWrapper).html(html2);
            }
        });

        // Hide the categories menu of grant and edu evaluation
        $('li#menu-posts-grant_education ul').children('li').first().next().next().next().hide();
        $('li#menu-posts-edu_evaluation ul').children('li').first().next().next().next().hide();

        /**
         * Handle click other choice admin
         * */
        $('.radio-other-choice input[type="radio"]').click(function() {
            if ( $(this).val() == 'other_choice' ) {
                $(this).closest('.options_group').find('.other-choice').show();
            } else {
                $(this).closest('.options_group').find('.other-choice').hide();
            }

        });

        $.each($('[data-ride="ap-refer-select"]'), function(i, v) {
            $(v).change(function() {
                var _data = $(this).data(),
                    _target = $(_data.target);
                $.ajax({
                    url: apollo_admin_obj.ajax_url,
                    data: {
                        action: _data.action,
                        id: $(this).val()
                    },
                    success: function(res) {
                        _target.html('<option value="0">'+(_target.children("[value='0']").html())+'</option>'+ res.html);
                        _target.children("option[value='"+$(_target).data().value+"']").attr("selected", 'selected');
                    }
                });
            });
        });

        /** Trigger change event for the refer source data */
        $.each($('[data-ride="ap-refer-select"]'), function(i, v) {

            if ( $(v).val() != 0 ) {
                $(v).trigger('change');
            }
        });


        //$( 'select.postform .level-1' ).attr('disabled', 'disabled');

        $( '.post-type-artist #publish' ).click( $.apl_admin.check_error_form);
        $( '.post-type-event #publish' ).click( $.apl_admin.check_error_form);
        $( '.post-type-venue #publish' ).click( $.apl_admin.check_error_form);
        $( '.post-type-organization #publish' ).click( $.apl_admin.check_error_form);
        $( '.post-type-educator #publish' ).click( $.apl_admin.check_error_form);
        $( '.post-type-program #publish' ).click( $.apl_admin.check_error_form);
        $( '.post-type-edu_evaluation #publish' ).click( $.apl_admin.check_error_form);
        $( '.post-type-grant_education #publish' ).click( $.apl_admin.check_error_form);
        $( '.post-type-classified #publish' ).click( $.apl_admin.check_error_form);
        $( '.post-type-public-art #publish' ).click( $.apl_admin.check_error_form);
        $( '.post-type-photo-slider #publish' ).click( $.apl_admin.check_error_form);

        $("body").on("click", "input.apollo-datepicker", $.apl_admin.event.handle_add_datepicker_quick_edit);

        /**
         * Agencies
         * */
        $( '.apl-cb-select-all' ).click(function() {

            if ( $( this ).attr('checked') == 'checked' ) {
                $( '.apl-cb-select-all' ).attr( 'checked', '' );
                $('input[name="agency[]"]').attr('checked', '');
            } else {
                $('input[name="agency[]"]').removeAttr('checked');
                $( '.apl-cb-select-all' ).removeAttr( 'checked' );
            }
        });


        $( '.apl-list-custom-table .remove' ).click( function(e) {
            e.preventDefault();
            var c = confirm( $(this).data().confirm );
            if ( c ) {
                window.location.href = $(this).children('a').attr('href');
            }
        });

        $( '#apl-doaction' ).click( function() {

            if ( $( 'select[name="action"]' ).val() != 'remove' ) return false;

            var _data = $('table.apl-list-custom-table input[name="agency[]"]'),
                _submit = false;
            $.each( _data, function(i, v) {
                if ( $(v).attr('checked') == 'checked' ) _submit = true;
            });

            if ( _submit ) {
                var c = confirm( $(this).data().confirm );
                if ( c ) $('form').submit();
            }

        });

        $( 'input[name="agency[]"]' ).click(function() {

            var _data = $('table.apl-list-custom-table input[name="agency[]"]'),
                num_checked = 0;
            $.each( _data, function(i, v) {
                if ( $(v).attr('checked') == 'checked' ) num_checked++;
            });

            if ( num_checked == $( 'input[name="agency[]"]' ).length ) $( '.apl-cb-select-all' ).attr( 'checked', 'checked' );
            else $( '.apl-cb-select-all' ).removeAttr( 'checked' );
        });
        /*----------------------------------------------------------------------*/


        /*
         * Add custom fields
         * */
        if ( $("#artist-custom-fields ol").length ) {
            $("#artist-custom-fields ol").sortable();
                }

        $( '#video-event-data' ).on( 'click', '.del', function() {
            if ( $(this).parent().parent().children( '.count' ).length == 1 ) {
                $( $(this).parent().children()[0] ).children().children('input').val('');
                $(this).parent().hide();
            } else {
                $(this).parent().remove();
            }
        });

        //Tri org
        $( '#apollo-event-data' ).on( 'click', '.del', function() {
            if ( $(this).parent().parent().children( '.count' ).length == 1 ) {
                $( $(this).parent().children()[0] ).children().children('input').val('');
                $(this).parent().hide();
            } else {
                $(this).parent().remove();
            }
        });

        $( '#artist-custom-fields .apl-cus-type' ).change(function() {
            var _sel_location = $(this).next().next().next(),
                _last_opt = _sel_location.children().last();

            if ( $(this).val() != 'checkbox' && _last_opt.val() == 'af_ff_dp_rsb' ) {
                _last_opt.attr('disabled', 'disabled')
            } else {
                _last_opt.removeAttr('disabled');
            }

            if ( _sel_location.val() != 'checkbox' ) {
                _sel_location.val(_sel_location.children().first().val());
            }
        });

        $( '#apollo-artist-additional-fields #add-cus-field' ).click(function() {
            var _newobj = $( '#artist-custom-fields ol li' ).first().clone(),
                _sel = $( '#apollo-artist-additional-fields #sel-input-type' ),
                _input_type = _sel.val(),
                _inner_obj = $(_newobj.children().children()[1]);

            if ( $( '#artist-custom-fields ol li' ).length == 1 ) {
                window.location.reload();
            }

            // Get the max index key
            var _old_index = 0,
                _new_index = '';
            $( '#artist-custom-fields ol li input[type="text"]' ).each( function( i, v ) {
                var name = $(v).attr('name');

                _old_index = name.split('_')[1].split('][]')[0]; // Get the index key from field name
                _new_index = Math.max( _old_index, _new_index );

            });
            _new_index++;

            // Change the text label
            _newobj.children().children().first().text( _sel.children('option[value="'+_sel.val()+'"]').text() );
//            _inner_obj.children().first().text( _sel.children('option[value="'+_sel.val()+'"]').text() );

            $( _inner_obj.children('.apl-cus-field') ).val( _sel.val() );
            $( _inner_obj.children('.apl-cus-label') ).val('');

            // Change name of input
            $( _inner_obj.children('.apl-cus-label') ).attr('name', function() {
                return 'labels['+_input_type+'_'+_new_index+'][]';
            });

            // Change the name of input field
            $( _inner_obj.children('.apl-cus-field') ).attr('name', function() {
                return 'fields['+_input_type+'_'+_new_index+'][]';
            });

            // Change the name of location
            $( _inner_obj.children('.apl-cus-location') ).attr('name', function() {
                return 'locations['+_input_type+'_'+_new_index+'][]';
            });

            // Change the name of type
            $( _inner_obj.children('.apl-cus-type') ).attr('name', function() {
                return 'types['+_input_type+'_'+_new_index+'][]';
            });

            $( _inner_obj.children('.apl-cus-type') ).children().removeAttr('selected');
            $( _inner_obj.children('.apl-cus-type') ).val(_input_type);

            // Change name of radio require
            _inner_obj.children('ul').children('li').children('input').attr('name', function() {

                $(this).attr('checked', false);
                if ( $(this).val() == 'no' ) {
                    $(this).attr('checked', true);
                }

                return $(this).attr('name').split('_')[0]+ '_' + _new_index;
            });

            _newobj.children().children().children('.remove').show();

            $( '#artist-custom-fields ol' ).prepend(_newobj);


            // Handle location is sidebar
            var _last_opt = $('select[name^="locations"]').first().children().last();
            _last_opt.removeAttr('disabled');
            if ( _sel.val() !== 'checkbox' ) {
                if ( _last_opt.val() == 'af_ff_dp_rsb' ) {
                    _last_opt.attr("disabled", 'disabled');
                }
            }

            _newobj.show();
        });
        /*-----------------------------------------------------------------------*/

        $.apl_admin.event.add_advanced_menu = function () {
            var clone_li = $('#menu-posts-event ul li').last().clone(),
                a = clone_li.children()[0];


            $(a).attr('href', apollo_admin_obj.advanced_search_url);
            $(a).text(apollo_admin_obj.advanced_search_text);
            clone_li.removeClass('current');
            $('#menu-posts-event ul li').last().before(clone_li);

            // Active this menu
            if ( apollo_admin_obj.is_advanced_search === "1" ) {
                $('.subsubsub').hide();
                $( '#menu-posts-event ul li' ).attr( 'class', '' );
                $( '#menu-posts-event ul li' ).first().addClass('wp-submenu-head');
                $( clone_li ).addClass( 'event-advanced-search' );
                $( '#menu-posts-event ul li.event-advanced-search' ).addClass( 'current' );

                // Remove Bulk action
                //$( '.bulkactions' ).hide();


            }
        };
        $.apl_admin.program.add_advanced_menu = function () {
            var clone_li = $('#menu-posts-program ul li').last().clone(),
                a = clone_li.children()[0];


            $(a).attr('href', apollo_admin_obj.program_advanced_search_url);
            $(a).text(apollo_admin_obj.advanced_search_text);
            clone_li.removeClass('current');
            $('#menu-posts-program ul li').last().before(clone_li);
            clone_li.prev().hide();

            // Active this menu
            if ( apollo_admin_obj.is_advanced_search === "1" ) {
                $('.subsubsub').hide();
                $( '#menu-posts-program ul li' ).attr( 'class', '' );
                $( '#menu-posts-program ul li' ).first().addClass('wp-submenu-head');
                $( clone_li ).addClass( 'program-advanced-search' );
                $( '#menu-posts-program ul li.program-advanced-search' ).addClass( 'current' );

                // Remove Bulk action
                //$( '.bulkactions' ).hide();


            }
        };

        $.apl_admin.classified.add_advanced_menu = function () {
            var clone_li = $('#menu-posts-classified ul li').last().clone(),
                a = clone_li.children()[0];


            $(a).attr('href', apollo_admin_obj.classified_search_url);
            $(a).text(apollo_admin_obj.advanced_search_text);
            clone_li.removeClass('current');
            $('#menu-posts-classified ul li').last().before(clone_li);

            // Active this menu
            if ( apollo_admin_obj.is_advanced_search === "1" ) {
                $('.subsubsub').hide();
                $( '#menu-posts-classified ul li' ).attr( 'class', '' );
                $( '#menu-posts-classified ul li' ).first().addClass('wp-submenu-head');
                $( clone_li ).addClass( 'event-advanced-search' );
                $( '#menu-posts-classified ul li.event-advanced-search' ).addClass( 'current' );

                // Remove Bulk action
                //$( '.bulkactions' ).hide();


            }
        };

        $.apl_admin.event.add_menu = function () {
            var clone_li    = $( '#menu-posts-event ul li' ).last().clone(),
                a           = clone_li.children()[0];


            $( a ).attr( 'href', apollo_admin_obj.event_display_management_url );
            $( a ).text( apollo_admin_obj.event_display_management_text );
            clone_li.removeClass('current');
            $( '#menu-posts-event ul li' ).last().before( clone_li );

            // Active this menu
            if ( apollo_admin_obj.is_event_display_management_page === "1" ) {
                $('.subsubsub').hide();
                $( '#menu-posts-event ul li' ).attr( 'class', '' );
                $( '#menu-posts-event ul li' ).first().addClass('wp-submenu-head');
                $( clone_li ).addClass( 'event-display-management' );
                $( '#menu-posts-event ul li.event-display-management' ).addClass( 'current' );

                // Remove Bulk action
                //$( '.bulkactions' ).hide();


            }

            $( '.inside' ).on( 'click', '#apl-event-new-venue', $.apl_admin.venue.add_new_venue );
            $( '.inside' ).on( 'click', '#apl-event-new-org', $.apl_admin.org.add_new_org );



            /**
             * Handle select primary category
             * */

            var _apl_is_primary_term = '.apl-primary-category',
                primary_select_obj = $( _apl_is_primary_term );

            // Handle click on additional term
            $( '#event-typechecklist' ).on('click', 'input[name="tax_input[event-type][]"]', function () {

                if ( $(this).val() == primary_select_obj.val() ) {
                    return false;
                }

                if ( ! parseInt( primary_select_obj.val() ) ) {
                        $( '#_apl_event_term_primary_id' ).parent().parent().next('.error') .show();
                    return false;
                }

            });

            // Handle click on primary term
            $( '.event-box' ).on( 'change', _apl_is_primary_term, function() {
                $( _apl_is_primary_term ).parent().parent().next('.error') .hide();
                $( '#event-typechecklist li label' ).css( 'opacity', '1' );
                $( '#in-event-type-'+ $( this ).val() ).attr( 'checked', false );
                $( '#event-type-'+ $( this ).val()+ ' label' ).first().css( 'opacity', '0.5' );
            });

            // Uncheck primary term in multiple checkbox terms in event admin panel
//            var primary = $( '.event-box '+_apl_is_primary_term+'' ).val();
//            $( '#event-typechecklist input[value='+ primary +']' ).attr( 'checked', false );

            // Disable selected term in additional category
            if ( parseInt( $(_apl_is_primary_term).val() ) ) {
                $( 'input[name="tax_input[event-type][]"][value="'+$(_apl_is_primary_term).val()+'"]' ).parent().css( 'opacity', 0.5 );
            }


            /*-------------------------------------------------------------------------------*/

            // Add start to event name require field
            if ( $( '#post_type' ) && $( '#post_type' ).val() == 'event' ) {
                var event_title = $( '#title-prompt-text' );
                event_title.html( event_title.text()+ '<span class="re-star">*</span>' );
            }

            // Add start to event name require field
            if ( $( '#post_type' ) && $( '#post_type' ).val() == 'public-art' ) {
                var public_art_title = $( '#title-prompt-text' );
                public_art_title.html( public_art_title.text()+ '<span class="re-star">*</span>' );
            }

            /**
             * Handle click on accessibility
             * */
            $( '#info-access-ul li' ).on( 'click', 'span', function() {
                var input = $(this).prev().prev();
                if ( ! input.attr( 'checked' ) ) {
                     input.attr( 'checked', true );
                } else {
                     input.attr( 'checked', false );
                }
            });
            $( '#info-access-ul li' ).on( 'click', 'img', function() {
                var input = $(this).prev();
                if ( ! input.attr( 'checked' ) ) {
                     input.attr( 'checked', true );
                } else {
                     input.attr( 'checked', false );
                }
            });
            /**-------------------------------------------------------------------------------*/

            // handle add video
            $( '#apl-add-video' ).click( $.apl_admin.event.handle_add_video );
            $( '#apl-add-audio' ).click( $.apl_admin.event.handle_add_audio );


            //Tri LM -- disable
            //$( '.apollo_input_url' ).focus(function() {
            //    if ( ! $(this).val() ) {
            //        $(this).val('http://');
            //    }
            //});

            // Handle add artist question
            $( '#apl-add-artist-question' ).click( $.apl_admin.artist.handle_add_artist_question );
            $( '.apollo-artist-custom-fields' ).on( 'click', '.remove', $.apl_admin.artist.remove_question );
            $( '.artist-question-wrapper' ).on( 'change', 'input[name="_apl_artist_options[]"]', function () {
                $( 'input[name="commit_changed"]' ).val(1);
            });
        };

        //Preview featured type option theme
        var previewFeaturedType = function($this) {
            var path = $this.attr('data-preview-path'),
                layout = $('input[name="_apollo_theme_options[_default_home_layout]"]:checked').val(),
                prefix = layout == 'right_sidebar_two_columns' && $this.val() == 'sta' ? 'right_sidebar_two_columns_' : '',
                url = path + "/preview-" + prefix + $this.val() + '.png';
            $img = $this.closest('.controls ').find('img.preview-option-image');
            $img.attr('src', url);
            $img.closest('a').attr('href', url);
        };
        $(document).on('change', '#_home_featured_type', function() {
            previewFeaturedType($(this));
        });
        $(document).on('click', '#section-_default_home_layout img.of-radio-img-img', function() {
            previewFeaturedType($('#_home_featured_type'));
        });
        previewFeaturedType($('#_home_featured_type'));

        var refreshPreviewOption = function($this) {
            var id = $this.attr('id'),
                defaultTypeList = [
                    '_organization_default_view',
                    '_venue_default_view_type',
                    '_artist_default_view_type',
                    '_public_art_default_view_type',
                    '_educator_default_view_type',
                    '_programs_default_view_type',
                    '_classified_default_view_type',
                    '_business_default_view_type'
                ];
            // Default view type commonly use for all
            if ($.inArray(id, defaultTypeList) !== -1) {
                id = 'default_view';
            }
            var path = $this.attr('data-preview-path'),
                url = path + "/preview-" + id + '-' + $this.val() + '.png';
            $img = $this.closest('.controls ').find('img.preview-option-image');
            $img.attr('src', url);
            $img.closest('a').attr('href', url);
        };
        $(document).on('change', '.section-select:not(.ignore-preview-event) .preview-option-button, .section-radio:not(.ignore-preview-event) .preview-option-button', function() {
            refreshPreviewOption($(this));
        });
        $.each($('.section-select:not(.ignore-preview-event) .preview-option-button'), function() {
            refreshPreviewOption($(this));
        });
        $.each($('.section-radio:not(.ignore-preview-event) .preview-option-button:checked'), function() {
            refreshPreviewOption($(this));
        });


        //Tri LM -- add for create element by script
        $(document).on('focus','.apollo_input_url',function(){
            if($(this).attr('name') == 'video_embed[]')
                return;
            if ( ! $(this).val() ) {
                $(this).val('http://');
            }
        });

        //@ticket #16542: Remove the 'http://' auto fills the URL field when user does not enter anything.
        $(document).on('focusout','.apollo_input_url',function(){
            var value = $(this).val();
            if ( /http:\/{0,2}$/.test(value)) {
                $(this).val('');
            }
        });

        //$(document).on('blur','.apollo_input_url',function(){
        //        var v = $(this).val();
        //        if($(this).attr('name') == 'video_embed[]')
        //           return;
        //        if (!_.isEmpty(v) && v.indexOf("http") < 0){
        //            $(this).val('http://'+v)
        //        }
        //
        //});

        $(".disable-text-fields").prop('disabled', true);


        $.apl_admin.event.add_menu();
        $.apl_admin.event.add_advanced_menu();
        $.apl_admin.classified.add_advanced_menu();
        $.apl_admin.program.add_advanced_menu();

        /**
         * Sort spotlight
         * */
        if ( apollo_admin_obj.typenow == 'spotlight' ) {
            $.apl_admin.apl_admin_sort_tbl({
                request_action: 'apollo_spotlight_ordering'
            });
        }

        if ( apollo_admin_obj.taxnow == 'population-served' ) {
            $.apl_admin.apl_admin_sort_tbl({
                request_action: 'apollo_term_ordering'
            });
        }

        /*@ticket #17733: CF] 20180928 - [Homepage][Feature events] Custom link for the 'Spotlight' category page - Item 5*/
        $('.wp-admin').off('click', '.apl-use-custom-link').on('click', '.apl-use-custom-link', function (e) {
            var value = $(this).is( ':checked' ) ? 1: 0;
            $(this).parent().find('.apl-use-custom-link-val').val(value);
        });
    });


    /**
     * All event and call function built here
     *
     * */
    $(function(){
        $(".apollo_input_price[type=text]").bind( 'keyup change', $.apl_admin.check_number );
        $(".apollo_input_number[type=text]").bind( 'keyup change', $.apl_admin.check_number );
        $(".apollo_input_url[type=text]").bind( 'keyup change', $.apl_admin.check_url );
        $(".apollo-youtube-url[type=text]").bind( 'keyup change', $.apl_admin.check_youtube_url );
        $(".apollo_input_email[type=text]").bind( 'keyup change', $.apl_admin.check_email );

        $('[class^="apollo-number-min"]').bind( 'keyup change', function() {
            var _data   = $(this).data,
                min     = _data.min || 0;

            if ( parseInt( $(this).val() ) < min ) {
                $.apl_admin.display_tip_error( $( this ), '', 'event_number_error' );
            }
            return this;
        });

        $("body").click($.apl_admin.hide_apollo_error_tip);

        // Tooltips
        $.apl_admin.active_tips();
        $('#the-list').on('hover', 'a', function() {
             $.apl_admin.active_tips(); // Active tip after click on quick edit
        });


        //TriLm fix submit button.
        if(!$('body').hasClass('post-new-php')){
            var tempSubmit =  $('body.post-type-event #publish').clone();
            tempSubmit.attr('id','publish_event');
            $('body.post-type-event #publish').after(tempSubmit);

            $('body.post-type-event #publish_event').attr('type','button');
        }

        $.apl_admin.admin_event_link();
    });


    /**
     * Building function here
     *
     * */
    $.apl_admin = {

        // Define for admin.js
        'hide_apollo_error_tip': function() {},
        'check_number': function() {},
        'check_url': function() {},
        'check_email': function() {},
        'is_email': function() {},
        'is_url': function() {},
        'display_tip_error': function() {},
        'active_tips': function () {},
        'apl_admin_sort_tbl': function() {},
        'is_youtube_url': function() {},
        'is_vimeo_url': function() {},
        'check_error_form': function() {},
        'is_audio_embed': function() {},
        // Define for quick event quick edit
        'event': {
            'handle_add_datepicker_quick_edit': function() {},
            'bulk_edit_change_to': function() {},
            'edit_inline': function() {},
            'submit_href': function() {},
            'handle_primary_category': function() {},
            'handle_add_video': function() {},
            'handle_add_audio': function() {},
        },

        // Handle venue function
        'venue': {
            'edit_inline': function () {},
            'add_new_venue': function () {},
        },

        // Handle venue function
        'artist': {
            'edit_inline': function () {},
            'handle_add_artist_question': function() {},
            'remove_question': function() {},
            'remove_field': function() {},

        },

        // Handle educator function
        'educator': {
            'edit_inline': function() {},
        },

        // Handle program function
        'program': {
            'add_advanced_menu': function() {},
            'edit_inline': function() {},
        },

        // Handle org function
        'org': {
            'edit_inline': function () {},
            'add_new_org': function () {},
        },

        // Define function for theme-options-function.js
        'theme_opt': {
            'options_framework_tabs': function() {},
        },

        // Define function for taxonomies.js
        'taxonomies': {
            'handle_upload_image_org': function() {},
        },
        'multi_select': {
            'multiple_remove_selected': function() {},
            'multiple_select_init': function() {},
            'add_multiple_select_value': function() {},
            'get_trans_terms': function() {},
        },
        'event': {
            'add_menu': function() {},
            'add_advanced_menu': function() {}
        },
        'classified': {
            'add_advanced_menu': function() {},
        },
        'admin_event_link' : function(){},
    };

    $.apl_admin.active_tips = function() {
        $(".tips, .help_tip").tipTip({
            'attribute' : 'data-tip',
            'fadeIn' : 50,
            'fadeOut' : 50,
            'delay' : 200
        });
    };

    $.apl_admin.display_tip_error = function( elm, newvalue, error_key_msg ) {

        elm.val( newvalue );
        if ( elm.parent().find('.apollo_error_tip').size() == 0 ) {
                var offset = elm.position();
                elm.after( '<div class="apollo_error_tip">' + eval( "apollo_admin_obj."+ error_key_msg ) + '</div>' );
                $('.apollo_error_tip')
                        .css('left', offset.left + elm.width() - ( elm.width() / 2 ) - ( $('.apollo_error_tip').width() / 2 ) )
                        .css('top', offset.top + elm.height() )
                        .fadeIn('100');
        }
    };
    $.apl_admin.hide_apollo_error_tip =  function() {
        $('.apollo_error_tip').fadeOut('100', function(){ $( this ).remove(); } );
    };

    $.apl_admin.check_number = function() {
        var value    = $( this ).val(),
            regex    = new RegExp( "[^0-9\]+", "gi" ),
            newvalue = value.replace( regex, '' );

        if ( value !== newvalue ) {
            $.apl_admin.display_tip_error( $( this ), newvalue, 'event_number_error' );
        }
        return this;
    };

    $.apl_admin.check_url = function() {

        var value    = $(this).val(),
            expression = "^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?",
            regex    = new RegExp( expression ),
            newvalue = value.replace( regex, '' );


        if ( value.match( regex ) == null ) {
            $.apl_admin.display_tip_error( $( this ), newvalue, 'event_url_error' );
        }
        return this;
    };

    $.apl_admin.check_youtube_url = function() {

        var value    = $(this).val();
        if ( ! $.apl_admin.is_youtube_url(value) && ! $.apl_admin.is_vimeo_url(value) )
            $.apl_admin.display_tip_error( $( this ), value, 'youtube_url_error' );

        return this;
    };

    $.apl_admin.check_email = function() {

        var value    = $(this).val(),
            regex    = new RegExp( '^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$', 'i' ),
            newvalue = value.replace( regex, '' );


        if ( value.match( regex ) == null ) {
            $.apl_admin.display_tip_error( $( this ), newvalue, 'event_email_error' );
        }
        return this;
    };

        $.apl_admin.venue.add_new_venue = function () {
            var name_elm = $( 'input[name="_apl_event_tmp_venue[_venue_name]"]' ),
                name = name_elm.val(),
                btn = $( this );

            if ( ! name ) {
               name_elm.parent().parent().addClass( 'apl-error' );
               return false;
            } else {
                name_elm.parent().parent().removeClass( 'apl-error' );
            }

            $.ajax({
                url: apollo_admin_obj.ajax_url,
                data: {
                    action: 'apollo_add_venue_in_event',
                    name: name,
                    address1: $( 'input[name="_apl_event_tmp_venue[_venue_address1]"]' ).val(),
                    address2: $( 'input[name="_apl_event_tmp_venue[_venue_address2]"]' ).val(),
                    city: $( 'select[name="_apl_event_tmp_venue[_venue_city]"]' ).val(),
                    neighborhood: $( 'select[name="_apl_event_tmp_venue[_venue_neighborhood]"]' ).val(),
                    state: $( 'select[name="_apl_event_tmp_venue[_venue_state]"]' ).val(),
                    zip: $( 'select[name="_apl_event_tmp_venue[_venue_zip]"]' ).val(),
                    region: $( 'select[name="_apl_event_tmp_venue[_venue_region]"]' ).val(),
                    url: $( 'input[name="_apl_event_tmp_venue[_venue_website_url]"]' ).val(),
                    /*@ticket #16898: [CF] 20180730 - [Event admin] Please add latitude/longitude data to the 'temp' venue - Item 4*/
                    lat: $( 'input[name="_apl_event_tmp_venue[_venue_lat]"]' ).val(),
                    long: $( 'input[name="_apl_event_tmp_venue[_venue_lng]"]' ).val(),
                    post: btn.attr( 'data-post' )
                },
                beforeSend: function() {
                    $(window).block(jQuery.apl.blockUI);
                    btn.val( btn.attr( 'data-adding-text' ) );
                    btn.attr( 'disabled', true );
                },
                success: function (res) {

                    var sel_wrap = $( 'select[name="_apollo_event_venue"]' );
                    sel_wrap.prepend( '<option value="'+ res.id +'">'+ name +'</option>' );
                    //$( '.tmp-venue-wrapper' ).fadeOut(300);
                    btn.val( btn.attr( 'data-add-text' ) );
                    $('.tmp-venue-wrapper').find('select').val(0);
                    $('.tmp-venue-wrapper').find('input[type=text]').val('');
                    $('.tmp-venue-wrapper').find('select:last').val(0);
                    btn.attr( 'disabled', false );
                    $( 'select[name="_apollo_event_venue"] option' ).attr( 'selected', false );
                    $( 'select[name="_apollo_event_venue"] option[value="'+res.id+'"]' ).attr( 'selected', 'selected' );

                    if (res) {
                        //add selected option
                        var option = '<option value="'+ res.id +'" selected="selected">'+ name +'</option>';
                        $('#_apollo_event_venue').append(option).trigger('change');
                    }
                    //render edit link.
                    var editItem = btn.closest('.inside').find('.edit-item-wrap .edit-item');
                    if(editItem.length > 0){
                        if(editItem.hasClass('hidden')){
                            editItem.removeClass('hidden');
                        }
                        editItem.attr('href', res.edit_link);
                    }
                    /** @Ticket @14143 - hidden select box */
                    var neighborhood = $('.apl-territory-neighborhood');
                    var wrapNeighborhood = neighborhood.closest('.neighborhood-box');
                    neighborhood.empty();
                    if(!wrapNeighborhood.hasClass('hidden')) {
                        wrapNeighborhood.addClass('hidden');
                    }
                    $(window).unblock();
                }
            });
        };

    $.apl_admin.event.handle_add_video = function () {
        var video_list_obj = $( '.video-list' ).clone(),
            total = $( '.video-wrapper .count' ).length;
        video_list_obj.show();
        video_list_obj.children().children().children('img').remove();
        video_list_obj.children('.del').show();
        video_list_obj.removeClass( 'video-list' );
        video_list_obj.addClass( 'video-list-'+ total );
            $( '.video-wrapper' ).append( video_list_obj );
            $( '.video-list-'+ total+ ' input' ).val('');
            $( '.video-list-'+ total+ ' textarea' ).val('');
        var d = new Date();
        var ed_id = 'icon_desc_' + d.getTime();

        /*
        * @Ticket 15085
        * TO DO: waiting the refactor - page builder
        */
        if($("#container-select-video-tags-field").length > 0) {
            video_list_obj.find('.apollo_input_url').attr('name', 'video_embed[' + total + ']');
            $html_select_video_tags = $("#container-select-video-tags-field").html();
            $html_select_video_tags = $html_select_video_tags.replace(/apl_pbm_selected_tags_video/gi, "apl_pbm_selected_tags_video[" + total + "]");
            video_list_obj.find('.wrap-select-tags').html($html_select_video_tags);
        }



        video_list_obj.find('.apollo_input_open_new_window').prop('checked', false).attr('name', 'open_new_window[' + index_checkbox_open_new_window+ ']').attr('value', 1);
        index_checkbox_open_new_window++;
        $.ajax({
            url: APL.ajax_url,
            data: {action: 'apollo_create_wp_editor', id: ed_id},
            type: 'get',
            success: function( response ){
                video_list_obj.find('.options_group.event-box.artist-video-desc').html('').append(response);
                tinyMCE.execCommand('mceAddEditor', true, ed_id);
                quicktags({id : ed_id});
                new QTags(ed_id);
                QTags._buttonsInit();
                $("textarea#" + ed_id).css('width', '100%');
                $(".apollo_input_url[type=text]").bind( 'keyup change', $.apl_admin.check_url );
                $("#qt_" + ed_id + "_ins").remove();
                $("#qt_" + ed_id + "_img").remove();
                $("#qt_" + ed_id + "_ul").remove();
                $("#qt_" + ed_id + "_ol").remove();
                $("#qt_" + ed_id + "_li").remove();
                $(ed_id + "tmce").trigger('click');
            }
        });
    }

    /*@ticket #18047 add registered org from event temp org*/
    $.apl_admin.org.add_new_org = function () {
        var name_elm = $( 'input[name="_apl_event_tmp_org"]' ),
            name = name_elm.val(),
            btn = $( this );

        if ( ! name ) {
           name_elm.parent().parent().addClass( 'apl-error' );
           return false;
        } else {
            name_elm.parent().parent().removeClass( 'apl-error' );
        }

        $.ajax({
            url: apollo_admin_obj.ajax_url,
            data: {
                action: 'apollo_add_org_in_event',
                name: name,
                post: btn.attr( 'data-post' )
            },
            beforeSend: function() {
                $(window).block(jQuery.apl.blockUI);
                btn.val( btn.attr( 'data-adding-text' ) );
                btn.attr( 'disabled', true );
            },
            success: function (res) {
                var sel_wrap = $( 'select[name="_apollo_event_organization0"]' );
                btn.val( btn.attr( 'data-add-text' ) );
                btn.attr( 'disabled', false );

                if (res) {
                    var option = '<option value="'+ res.id +'" selected="selected">'+ name +'</option>';
                    $('#_apollo_event_organization0').append(option).trigger('change');
                    $('#_apl_event_tmp_org').val('');
                    //render edit link.
                    var editItem = sel_wrap.parent().find('.edit-item-wrap .edit-item');
                    if(editItem.length > 0){
                        if(editItem.hasClass('hidden')){
                            editItem.removeClass('hidden');
                        }
                        editItem.attr('href', res.edit_link);
                    }
                }
            $(window).unblock();
            }
        });
    };

    //Tri add audio admin
    $.apl_admin.event.handle_add_audio = function () {
        var audio_list_obj = $( '.audio-list' ).clone(),
            total = $( '.audio-wrapper .count' ).length;
        audio_list_obj.show();
        audio_list_obj.children().children().children('img').remove();
        audio_list_obj.children('.del').show();
        audio_list_obj.removeClass( 'audio-list' );
        audio_list_obj.addClass( 'audio-list-'+ total );
            $( '.audio-wrapper' ).append( audio_list_obj );
            $( '.audio-list-'+ total+ ' textarea' ).val('');
            $( '.audio-list-'+ total+ ' textarea' ).val('');

        /*
        * @Ticket 15263
        * TO DO: waiting the refactor - page builder
        */
        if($("#container-select-audio-tags-field").length > 0) {
            audio_list_obj.find('.apollo_input_url').attr('name', 'video_embed[' + total + ']');
            $html_select_audio_tags = $("#container-select-audio-tags-field").html();
            $html_select_audio_tags = $html_select_audio_tags.replace(/apl_pbm_selected_tags_audio/gi, "apl_pbm_selected_tags_audio[" + total + "]");
            audio_list_obj.find('.wrap-select-tags').html($html_select_audio_tags);
        }

    };

    $.apl_admin.artist.handle_add_artist_question = function() {
        var list_obj = $( $( '.artist-question-list-0' )[0] ) .clone(),
            total = $( '.artist-question-wrapper .count' ).length;


        list_obj.removeClass( 'artist-question-list-0' );
        list_obj.addClass( 'artist-question-list-'+ total );
        var lbl = list_obj.children().children().children()[0];
        $(lbl).text( $(lbl).data().label+ ' '+ ( total + 1 ) );
        $( '.artist-question-wrapper' ).append( list_obj );
        $( '.artist-question-list-'+ total+ ' input' ).val('');
        $( list_obj.children().children().children()[2] ).show();

        if ( ! list_obj.hasClass( 'count' ) ) list_obj.addClass( 'count' );

        list_obj.show();
    };

    $.apl_admin.artist.remove_question = function ( e ) {
        e.preventDefault();
        var remove_obj = $(this),
            _confirm =  confirm( remove_obj.data().confirm );

        if ( _confirm ) {
            remove_obj.parent().parent().parent().hide( 300, function() {

                if ( remove_obj.length > 1 ) {
                    remove_obj.parent().parent().parent().remove();
                } else {
                    remove_obj.parent().parent().parent().hide();
                }

                // Empty title of question
                remove_obj.parent().children('input').val('');

                // Remove count class to force count question order
                remove_obj.parent().parent().parent().removeClass( 'count' );

                // Empty title of custom field
                remove_obj.parent().children().children('input').val('');
            });
        }
    };


    $.apl_admin.apl_admin_sort_tbl = function(options) {

        var settings = {
            request_action: '',
        };

        if ( options ) $.extend( settings, options );

        $('table.widefat.wp-list-table tbody th, table.widefat tbody td').css('cursor','move');
        $("table.widefat.wp-list-table").sortable({
            items: 'tbody tr:not(.inline-edit-row)',
            cursor: 'move',
            axis: 'y',
            forcePlaceholderSize: true,
            helper: 'clone',
            opacity: 0.65,
            placeholder: 'product-cat-placeholder',
            scrollSensitivity: 40,
            start: function(event, ui) {
                if ( ! ui.item.hasClass('alternate') ) ui.item.css( 'background-color', '#ffffff' );
                ui.item.children('td,th').css('border-bottom-width','0');
                ui.item.css( 'outline', '1px solid #aaa' );
            },
            stop: function(event, ui) {
                ui.item.removeAttr('style');
                ui.item.children('td,th').css('border-bottom-width','1px');
            },
            update: function(event, ui) {
                var termid = ui.item.find('.check-column input').val();	// this post id
                var termparent = ui.item.find('.parent').html(); 	// post parent

                var prevtermid = ui.item.prev().find('.check-column input').val();
                var nexttermid = ui.item.next().find('.check-column input').val();

                // can only sort in same tree
                var prevtermparent = undefined;
                if ( prevtermid != undefined ) {
                        var prevtermparent = ui.item.prev().find('.parent').html();
                        if ( prevtermparent != termparent) prevtermid = undefined;
                }

                var nexttermparent = undefined;
                if ( nexttermid != undefined ) {
                        nexttermparent = ui.item.next().find('.parent').html();
                        if ( nexttermparent != termparent) nexttermid = undefined;
                }

                // Get all ids
                var chboxs = $( 'table tr th.check-column input[type="checkbox"]' );
                var ids = [];
                $.each(chboxs, function (k, v) {
                    var val = $(v).val();
                    if ( val == parseInt( val )  ) ids.push( val );
                });

                // if previous and next not at same tree level, or next not at same tree level and the previous is the parent of the next, or just moved item beneath its own children
                if ( ( prevtermid == undefined && nexttermid == undefined ) || ( nexttermid == undefined && nexttermparent == prevtermid ) || ( nexttermid != undefined && prevtermparent == termid ) ) {
                        $("table.widefat.wp-list-table").sortable('cancel');
                        return;
                }

                // show spinner
                ui.item.find('.check-column input').hide().after('<img alt="processing" src="images/wpspin_light.gif" class="waiting" style="margin-left: 6px;" />');

                // go do the sorting stuff via ajax
                if ( settings['request_action'] ) {
                    $.post( apollo_admin_obj.ajax_url, { id: termid, nextid: nexttermid, thetaxonomy: 'population-served', action: settings['request_action'], ids: ids.join(',') }, function(response){
                            if ( response == 'children' ) window.location.reload();
                            else {
                                ui.item.find('.check-column input').show().siblings('img').remove();
                            }
                    });
                } else {
                    ui.item.find('.check-column input').show().siblings('img').remove();
                }

                // fix cell colors
                $( 'table.widefat tbody tr' ).each(function(){
                        var i = $('table.widefat tbody tr').index(this);
                        if ( i%2 == 0 ) $(this).addClass('alternate');
                        else $(this).removeClass('alternate');
                });
            }
        });
    };

    $.apl_admin.overlay = function(is_remove) {
        var $body = $('body');

        if(is_remove === false) {
            $body.append('<div class="blockUI blockOverlay" id="admin_overlay" style="z-index: 1000; border: none; margin: 0px; padding: 0px; width: 100%; height: 100%; top: 0px; left: 0px; opacity: 0.6; position: fixed; background-color: rgb(0, 0, 0);"></div>');

            $("#admin_overlay").on('click', function() {
                $("#admin_overlay").remove();
                $body.trigger('remove-overlay');
            });
            return;
        }

        $("#admin_overlay").remove();
    };

    $.apl_admin.show_popup_with_content = function(is_remove, _html) {
        var $body = $('body');

        if(is_remove === false) {
            $body.append('<div class="blockUI blockMsg blockPage" id="admin_popup" style="z-index: 1011; position: fixed; padding: 0px; margin: 0px; width: 30%; top: 40%; left: 35%; text-align: center; color: rgb(0, 0, 0); border: 3px solid rgb(170, 170, 170); background-color: rgb(255, 255, 255);">' + _html + '</div>');

            $body.on('remove-overlay', function() {
                $("#admin_popup").remove();
            });
            return;
        }

        $('#admin_popup').remove();
    };

    /**
     * @author: Trilm
     * custom event menu link
     */
    $.apl_admin.admin_event_link = function() {
        var item = $('#adminmenu').find('#menu-posts-event');
        if(item.length == 0)
        {
            item = $('#adminmenu').find('.menu-icon-event');
        }
        var
            aTag = item.children('a'),
            subMenuATag = item.find(' ul.wp-submenu > li.wp-first-item >a'),
            href = aTag.attr('href'),
            newHref = href + '&post_status=pending';

        aTag.attr('href',newHref);
        subMenuATag.attr('href',newHref);


    };


    $(function() {
        var $body = $('body');

        /* artist - choice award  */
        $body.on('click', '#artist_timeline_export', function(e) {
            e.preventDefault();

            /* get data */
            var list_ids = [];
            jQuery('.check-column input:checked[value]').each(function(i, v) {
                list_ids.push($(this).val());
            });

            var year = $('#artist_timeline_year').val();

            $.ajax({
                type: 'get',
                url: apollo_admin_obj.ajax_url,
                data: {
                    action: 'apollo_add_to_laureate_award',
                    ids: list_ids.join(','),
                    year: year
                },
                success: function( res ) {
                    $.apl_admin.overlay(true);
                    $.apl_admin.show_popup_with_content(true);
                    if(res.code === '1') {
                        alert(res.msg);
                    }
                    else {
                        alert(res.msg);
                    }

                }
            });

            return false;
        });

        $body.on('click', '#artist_timeline_popup', function(e) {

            if(!jQuery('.check-column input:checked[value]').length) {
                alert('Please choice Artist');
                return;
            }

            $.apl_admin.overlay(false);

            var html = '<div id="#artist_timeline_popup_html" class="p-15">' +
                '<select id="artist_timeline_year" name="artist_timeline_year">';

            var current_year = (new Date()).getFullYear(), selected = '';
            for( var i = 1980; i <= current_year; i++) {
                selected = '';
                if(current_year === i) {
                    selected = 'selected';
                }
                html += '<option value="' + i + '" ' + selected + ' >' + i + '</option>';
            }

            html  += '</select>';

            html += '<a id="artist_timeline_export" class="button action" > Export </a>';

            html += '</div>';

            $.apl_admin.show_popup_with_content(false, html);

        });

        if ( $("#artist_timeline_popup").length ) {
            /* BAD HACK */
            var html = $("#artist_timeline_popup")[0].outerHTML;
            if(html) {
                $(".tablenav.top #post-query-submit").after(html);
                $("#artist_timeline_popup").remove();
            }
        }

        /* END - BAD HACK */

        /* ARTIST TIMELINE */
        $body.on('click', '[data-ride="shadow-man"]', function(e) {
            var $this = $(this),
                maxtimes= $this.data('maxtimes'),
                $mainwrapper = $($this.data('wrapper')),
                $template = $mainwrapper.find($this.data('template')+':first'),
                bornname = $this.data('bornname');

            if($template.length <= 0 ) {
                return false;
            }

            if(typeof maxtimes !== 'undefined' && typeof bornname !== 'undefined') {
                if($mainwrapper.find(bornname).length >= maxtimes) {
                    var alert_text = $this.data('alert');
                    alert(alert_text.replace('%s',maxtimes));
                    return false;
                }
            }

            var _append_to = $this.data('append-to'), _selector = _append_to, _method = 'append';
            if(_append_to.indexOf(':') !== '-1') {
                var _arr = _append_to.split(':');
                _selector = _arr[0];
                _method = _arr[1];
            }

            $mainwrapper.find(_selector + ':first')[_method]($template.html());
        });

        $body.on('click', '[data-ride="unique-check"]', function() {
            var $this = $(this);
            $this.data('old-value', $this.val());
        });

        $body.on('change', '[data-ride="unique-check"]', function() {
            var $this = $(this), $mygroup = $('[data-mygroup="' + $this.data('mygroup') + '"]'), arrValue = [], currentValue = $this.val();
            $this.addClass('group-active');

            /* Get all value in my group but not me */
            $mygroup = $mygroup.filter(function()  {
                return !$(this).hasClass('group-active');
            });
            $this.removeClass('group-active'); /* fallback */

            $mygroup.each(function(k, v) {
                var _val = $(this).val();
                arrValue.push(_val);
            });

            if(arrValue.indexOf(currentValue) !== -1) {
                alert($this.data('alert'));
                $this.val($this.data('old-value'));
                return false;
            }
        });

        /* REMOVE TIMELINE RECORD */
        $body.on('click', '.apollo-artist-timeline .del', function() {
            var $this = $(this), desc = $this.data('confirm'), $parent = $this.parent();
            if(confirm(desc)) {
                $parent.remove();
            }

        });
        /* END*/

        /* CLOSE ME PLUGIN */
        $body.on('click', '[data-ride="remove-relation"]', function() {
            var $this = $(this), target = $this.data('target'), desc = $this.data('confirm'), $target = '', action = $this.data('action');
            if(confirm(desc)) {

                if(typeof $this[target] === 'function' ) {
                    $target = $this[target]();
                }
                else if(typeof $this.find(target) !== 'undefined' && typeof $this.find(target).length !== 'undefined') {
                    $target = $this.find(target);
                }

                $target.remove();

                if(typeof action !== 'undefined') {

                    var _dataaction = $this.data('action_data'), _arrdataaction = _dataaction.split(','), _postid = $.trim(_arrdataaction[0]), _attachment_id = $.trim(_arrdataaction[1]);
                    $.ajax({
                        action: 'GET',
                        url: ajaxurl + '?action=' + action + '&postid=' + _postid + '&attid=' + _attachment_id,
                        success: function(res) {
                            console.log(res);
                        }

                    })
                }

            }

        });
        /* END - ME PLUGIN */


        /* CLOSE ME PLUGIN */
        $body.on('click', '[data-ride="delete-relation"]', function() {
            var $this = $(this), target = $this.data('target'), desc = $this.data('confirm'), $target = '', action = $this.data('action');
            if(confirm(desc)) {

                if(typeof $this[target] === 'function' ) {
                    $target = $this[target]();
                }
                else if(typeof $this.find(target) !== 'undefined' && typeof $this.find(target).length !== 'undefined') {
                    $target = $this.find(target);
                }

                $target.remove();

                if(typeof action !== 'undefined') {

                    var _dataaction = $this.data('action_data'), _arrdataaction = _dataaction.split(','), _postid = $.trim(_arrdataaction[0]), _attachment_id = $.trim(_arrdataaction[1]);
                    $.ajax({
                        action: 'GET',
                        url: ajaxurl + '?action=' + action + '&postid=' + _postid + '&attid=' + _attachment_id,
                        success: function(res) {
                            console.log(res);
                        }

                    })
                }

            }

        });
        /* END - ME PLUGIN */

        /* UPlOAD PLUGIN */
        $body.on('change', '[data-ride="upload_file"]', function(e) {
            var $this = $(this), maxfilesize = $this.data('maxfilesize');
            if(typeof this.files[0] !== 'undefined' && this.files[0].size > maxfilesize) {
                var alert_text = $this.data('alert');
                alert(alert_text.replace('%s',(maxfilesize / (1024 * 1024)).toFixed(2)));
                this.value= '';
            }

            /** @Ticket #16288 - Remove data upload from media library */
            var current = $(e.currentTarget);
            var parent = current.closest('.upload_pdf_embed_field');
            var choseFromMediaData = parent.find('.apl-choose-pdf-from-media');
            var inputFileName = parent.find('.apl-pdf-file-name');
            if (inputFileName.length > 0) {
                inputFileName.html(this.value);
            }
            if (choseFromMediaData.length > 0) {
                choseFromMediaData.val('');
            }
            var descFromUpload = parent.closest('.apollo-artist-upload_pdf').find('.wpc-75');
            var descFromMedia = parent.closest('.apollo-artist-upload_pdf').find('.apl-desc-pdf-from-media');
            if (descFromUpload.length > 0) {
                descFromUpload.removeClass('hidden');
            }
            if (descFromMedia.length > 0) {
                descFromMedia.val('');
                descFromMedia.addClass('hidden');
            }
        });
        /* END */


    }); // END READY



    /**
     * Multi select and sort able event type
     * */
    $(function() {

        $.apl_admin.multi_select.multiple_select_init();
        var adjustment;

        if ( $("div.selected-event ol").length && $(".selected-event ol").sortable != undefined ) {
            $(".selected-event ol").sortable({
                group: 'simple_with_animation',
                pullPlaceholder: false,

                // animation on drop
                onDrop: function  (item, targetContainer, _super) {
                    var clonedItem = $('<li/>').css({height: 0});
                    item.before(clonedItem);
                    clonedItem.animate({'height': item.height()});

                    item.animate(clonedItem.position(), function  () {
                        clonedItem.detach();
                        _super(item);
                    });
                },

                // set item relative to cursor position
                onDragStart: function ($item, container, _super) {

                    var offset = $item.offset(),
                    pointer = container.rootGroup.pointer;

                    adjustment = {
                        left: pointer.left - offset.left,
                        top: pointer.top - offset.top
                    };

                    _super($item, container);
                },
                onDrag: function ($item, position) {
                    $item.css({
                      left: position.left - adjustment.left,
                      top: position.top - adjustment.top
                    });
                },
                update: function( event, ui ) {
                    $.each( $(this).children(), function( i, val ) {
                        $('.hidden-multiselect option[value="'+ $(val).attr('data-value')).remove();
                        $('.hidden-multiselect' ).append( '<option selected="selected" value="'+ $(val).attr('data-value') +'">'+ $(val).attr('data-value') +'</option' );
                    });
                },
                receive: function(event, ui) {

                }
            });
        }

        $( '.multiselect' ).on( 'dblclick', 'option', function() {
            $.apl_admin.multi_select.add_multiple_select_value( $( this ) );
        });

        // Remove
        $('.selected-event ol').on( 'click', 'i', $.apl_admin.multi_select.multiple_remove_selected );
        $('.selected-event ol').on( 'dblclick', 'li', $.apl_admin.multi_select.multiple_remove_selected );

        $( '.multiselect' ).on( 'click', 'option', function( e ) {

            if ( e.ctrlKey ) {
                $(this).attr( 'selected', 'selected' );
            } else {
                $( '.multiselect option' ).removeAttr( 'selected' );
                $(this).attr( 'selected', 'selected' );
            }
        });

        $( '.wrap-multiselect' ).on( 'click', '.add-multi-select-btn', function() {
            var selected_opts = $(this).prev().find(":selected");
            $.each( selected_opts, function( i, val ) {
                $.apl_admin.multi_select.add_multiple_select_value( $(val) );
            });

        });


    });

    $.apl_admin.multi_select.get_trans_terms = function(slug, callback) {

        if (APL.hasTrans != 1) {
            callback({

            });
            return;
        }


        /**
         * Get translation text for each term
         *
         * */
        $.ajax({
            url: apollo_admin_obj.ajax_url,
            method: 'post',
            dataType: 'JSON',
            beforeSend: function() {
                $('.section.wrap-multiselect').block($.apl.blockUI);
            },
            data: {
                action: 'apollo_get_trans_term_name',
                slug: slug,
            },
            success: function(res) {

                /**
                 * Initialize trans group data for the first time
                 * This is used for add button or double click event
                 *
                 **/
                if (res.status) {
                    var transData = res.result;
                    $.each(transData, function(i, v) {
                        var input = $('.trans-group-eg').find('input[data-lang='+i+']');
                        input.attr('name', input.attr('data-name')).attr('value', v);
                    });
                }

                //---------------------------------------------------

                $('.section.wrap-multiselect').unblock($.apl.blockUI);

                if (typeof callback != undefined) {
                    callback({
                        status: true,
                    });
                }
            }
        }); // End Get translation text for each term
    };

    $.apl_admin.multi_select.add_multiple_select_value = function( elm ) {


        if (elm.parent().next().next().children().children().length > 3 && $('#_home_featured_type').val() == 'ver') {
            alert(elm.closest('.wrap-multiselect').find('.add-multi-select-btn').data('alert'));
            return;
        }

        var text = elm.attr( 'data-text' ),
            value = elm.attr( 'value' );

        // Move current to the bottom
        var currentOpt = $('.hidden-multiselect option[value="'+ value +'"]');
        currentOpt.remove();

        $(elm).closest('select').prev().append(currentOpt.attr('selected', 'selected'));

        $.apl_admin.multi_select.get_trans_terms(value, function() {
            var tranGroupHtml = $('.trans-group-eg').html();

            // Change remove intput name to avoid save wrong data
            var transInput = $('.trans-group-eg').find('input');
            $.each(transInput, function(i, v) {
                $(v).attr('name', '');
            });

            var selected_el = '',
            // Has color picker
            colorElm = elm.parent().data().colorPicker == 1 ? '<input value="#F7F7F7" name="'+elm.parent().data().colorName+'" id="of-color-'+Math.random().toString(20).substring(5)+'" class="of-color" />' : '';

            var asnameElm = typeof tranGroupHtml != 'undefined' ? '<div class="as-is"><input type="text" name="'+elm.parent().attr('data-as-name')+'" value="'+text+'"/>'+tranGroupHtml+'</div>' : '';

            /*@ticket #17733: CF] 20180928 - [Homepage][Feature events] Custom link for the 'Spotlight' category page - Item 5*/
            var customLink = elm.parent().data().customLink == 1 ? '<div><input type="text" name="'+elm.parent().attr('data-custom-link-name')+'" value="" class="apollo_input_url"/></div>' : '';
            var checkbox = elm.parent().data().checkbox == 1 ? '<div class="apl-of-checkbox-contain"><label>Use custom link </label>' +
                '<input type="checkbox" class="apl-use-custom-link"/>' +
                '<input  name="'+elm.parent().attr('data-checkbox-name')+'" type="text" value="0" class="apl-use-custom-link-val hidden"/></div>' : '';

            if ( elm.data().url ) {
                selected_el = '<li data-value="'+ value +'">\n\
                                    <i class="fa fa-remove"></i><a href="'+elm.data().url+'"><label>'+ text +'</label></a>\n\
                                    '+asnameElm+'\n\
                                    '+colorElm+'\n\
                                    '+checkbox+'\n\
                                    '+customLink+'\n\
                                </li>';
            } else {
                selected_el = '<li data-value="'+ value +'">\n\
                                    <i class="fa fa-remove"></i><label>'+ text +'</label>\
                                    '+asnameElm+'\n\
                                    '+colorElm+'\n\
                                    '+checkbox+'\n\
                                    '+customLink+'\n\
                                </li>';
            }

            if ( ! elm.parent().next().next().children().children('[data-value="'+ value +'"]').length && elm.css('opacity') == "1" )
                elm.parent().next().next().children().append(selected_el);

            $(elm).attr( 'disabled', true );

            try {
                $('.of-color').wpColorPicker();
            } catch(e) {}

            $('body').trigger('aplMultiSelectSort', colorElm!= "" );
        });

    };

    $.apl_admin.multi_select.multiple_select_init = function() {
        $('.multiselect').each(function(i, v) {
            var selector_opts = $(v).find('option' ),
                selected_opts = $(v).prev().find('option[selected="selected"]' );

            var selected_arr = [];
            $.each( selected_opts, function ( i, elm ) {
                selected_arr.push( $( elm ).attr( 'value' ) );
            });
            $.each( selector_opts, function ( i, elm ) {

                $( elm ).attr( 'data-text', $( elm ).text() );

                /**
                 * Add rank to text of select opt
                 * */
                var rank = $( elm ).attr( 'class' ).split( '-' )[1],
                    rank_string = '';
                for ( var i = 0; i< rank; i++ ) {
                    rank_string += '-';
                }

                // Disable selected opt
                $( elm ).text( rank_string + ' '+ $( elm ).text() );
                if ( $.inArray( $( elm ).attr( 'value' ), selected_arr ) !== -1 ) {
                    $(elm).attr( 'disabled', true );
                }
            });
        });

    };

    $.apl_admin.multi_select.multiple_remove_selected = function() {
        var elm = $(this).parent();
        if ( $( this ).attr( 'data-value' ) ) {
           elm = $( this );
        }
        var agencyEle = elm.closest('.apl-custom-postbox.agency');
        elm.remove();
        $('.hidden-multiselect option[value="'+ elm.attr('data-value') +'"]').removeAttr( 'selected' );
        $('.multiselect option[value="'+ elm.attr('data-value') +'"]').removeAttr( 'disabled');
        $('.multiselect option[value="'+ elm.attr('data-value') +'"]').removeAttr( 'selected');

    };
    /*--------------------------------------------------------------------------*/

    $.apl_admin.event.handle_add_datepicker_quick_edit =  function() {

        // as an added bonus, if you are afraid of attaching the "datepicker"
        // multiple times, you can check for the "hasDatepicker" class...
        if (!$(this).hasClass("hasDatepicker"))
        {
            $(this).datepicker({
                dateFormat : 'yy-mm-dd'
            });
            $(this).datepicker("show");
        }
    };
    /*
     <iframe src="https://player.vimeo.com/video/123844456" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <p><a href="https://vimeo.com/123844456">Oscar - Daffodil Days</a> from <a href="https://vimeo.com/user1266912">laurie lynch</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
     *
     */
    $.apl_admin.is_youtube_url = function( url ) {
        var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        return (url.match(p)) ? RegExp.$1 : false;
    };

    $.apl_admin.is_vimeo_url = function( url ) {
        return url.match(/^.+vimeo.com\/(.*\/)?([^#\?]*)/);
    };

    $.apl_admin.is_email = function( value ) {

        var regex    = new RegExp( '^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$', 'i' );

        return value.match( regex );

    };

    $.apl_admin.is_url = function( value ) {
       var expression = "^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?",
           regex    = new RegExp( expression );
        return value.match( regex );
    };

    $.apl_admin.check_error_form = function() {
        var error = false,
            add_error_parent = function( _obj ) {
                var _parent = _obj.data().parent;
                if ( _parent ) {
                    $( _parent ).addClass('error');
                }
            },
            remove_error_parent = function( _obj ) {
                var _parent = _obj.data().parent;
                if ( _parent ) {
                    $( _parent ).removeClass('error');
                }
            };

        $( '#apollo-notice' ).hide();

            // Category
        var _cat = $( '.cat_required input' ),
                _cat_val = '';

        if ( _cat.length ) {
            jQuery.each(_cat, function(i,v) {
                if ( jQuery(v).attr('checked') ) {
                    _cat_val = '1';
                }
            });

            if ( ! _cat_val ) {
                $( '#apollo-notice' ).show();
                $( '[data-id="post_cat-required"]' ).show();
                $( '#artist-typediv h3 span' ).addClass( 'apollo-text-error' );
                error = true;
            } else {
                $( '[data-id="post_cat-required"]' ).hide();
                $( '#artist-typediv h3 span' ).removeClass( 'apollo-text-error' );
            }
        }


        var _requires = $( '.required' );

        $.each( _requires, function(i, v) {
                var _require_error = ( ! $( v ).val() || $( v ).val() == '0' );
                // Handle event

                var label_obj = $(v).closest('.options_group').children('label');
                if ( _require_error && $( v ).attr('type') == 'hidden' ) {
                    label_obj.addClass('error_text');
                    error = true;
                }

                if ( $(v).attr('name') == '_apl_event_tmp_org' || $(v).attr('name') == '_apollo_event_organization0' ) {
                    _require_error = ! $('[name="_apl_event_tmp_org"]').val() && $('[name="_apollo_event_organization0"]').val() == '0';
                }

                if ( _require_error ) {
                    $( '#apollo-notice' ).show();

                    if ( $( v ).attr('type') == 'hidden' ) {
                         label_obj.addClass('error_text');
                         $( '[data-id=\"'+$( v ).attr( 'data-name' ) +'-required\"]' ).show();
                    } else {
                        $( '[data-id=\"'+$( v ).attr( 'name' ) +'-required\"]' ).show();
                        $( v ).addClass( 'apollo-input-error' );
                    }
                    error = true;
                    add_error_parent($(v));
                } else {
                    if ( $( v ).attr('type') == 'hidden' ) {
                        $( '[data-id=\"'+$( v ).attr( 'data-name' ) +'-required\"]' ).hide();
                        label_obj.removeClass('error_text');
                    } else {
                        $( '[data-id=\"'+$( v ).attr( 'name' ) +'-required\"]' ).hide();
                        $( v ).removeClass( 'apollo-input-error' );
                    }


                    remove_error_parent($(v));
                }

        });

        var _arr_urls = $('.apollo_input_url');
        $.each( _arr_urls, function(i, v) {

            if ( $( v ).val() ) {
                if ( ! $.apl_admin.is_url($( v ).val()) ) {
                    $( '#apollo-notice' ).show();
                    $( v ).addClass( 'apollo-input-error' );
                    $( '[data-id=\"'+$( v ).attr( 'name' ) +'-url\"]' ).show();
                    error = true;
                    add_error_parent($(v));
                } else {
                    $( '[data-id="'+$( v ).attr( 'name' ) +'-url"]' ).hide();
                    $( v ).removeClass( 'apollo-input-error' );
                    remove_error_parent($(v));
                }

            }

        });

        var _arr_number = $('.apollo_input_number');
        $.each( _arr_number, function(i, v) {

            if ( $( v ).val() ) {
                if ( isNaN($( v ).val()) ) {
                    $( '#apollo-notice' ).show();
                    $( v ).addClass( 'apollo-input-error' );
                    $( '[data-id=\"'+$( v ).attr( 'name' ) +'-url\"]' ).show();
                    error = true;
                    add_error_parent($(v));
                } else {
                    $( '[data-id="'+$( v ).attr( 'name' ) +'-url"]' ).hide();
                    $( v ).removeClass( 'apollo-input-error' );
                    remove_error_parent($(v));
                }

            }

        });

        var _youtube_urls = $( '.apollo-youtube-url' );
        $.each( _youtube_urls, function(i, v) {
            if ( $( v ).val() && ! $.apl_admin.is_youtube_url( $(v).val() ) ) {
                $( '#apollo-notice' ).show();
                $( v ).addClass( 'apollo-input-error' );
                $( '[data-id="'+$( v ).attr( 'name' ) +'-youtube_url"]' ).show();
                error = true;
                add_error_parent($(v));
                return false;
            } else {
                $( '[data-id="'+$( v ).attr( 'name' ) +'-youtube_url"]' ).hide();
                $( v ).removeClass( 'apollo-input-error' );
                remove_error_parent($(v));

            }
        });

        //Tri LM validate video Embed
        var _audio_embeds = $( '.audio_embed' );
        $.each( _audio_embeds, function(i, v) {
            if ( $( v ).val() && ! $.apl_admin.is_audio_embed( $(v).val() ) ) {
                $( '#apollo-notice' ).show();
                $( v ).addClass( 'apollo-input-error' );
                $( '[data-id="'+$( v ).attr( 'name' ) +'-audio_embed"]' ).show();
                error = true;
                console.log($(v));
                add_error_parent($(v));
                return false;
            } else {
                $( '[data-id="'+$( v ).attr( 'name' ) +'-audio_embed"]' ).hide();
                $( v ).removeClass( 'apollo-input-error' );
                remove_error_parent($(v));

            }
        });

        var _requires_editor = $( '.required-editor' );
        $.each( _requires_editor, function(i, v) {

            if ( $(v).find( '.wp-editor-area' ) ) {
                var id = $(v).find( '.wp-editor-area' ).attr('id'),
                    name = $(v).find( '.wp-editor-area' ).attr('name'),
                    editor = tinyMCE.get(id);

                if ( editor && ! editor.getContent() ) {
                    error = true;
                    $( '#apollo-notice' ).show();
                    $(v).find( '.wp-editor-container' ).addClass( 'apollo-input-error' );
                    $( '[data-id=\"'+name +'-required\"]' ).show();
                } else {
                    $( '[data-id=\"'+name +'-required\"]' ).hide();
                    $(v).find( '.wp-editor-container' ).removeClass( 'apollo-input-error' );
                }
            }
        });


        var _arr_emails = $( '.apollo_input_email' );
        $.each( _arr_emails, function(i, v) {

            if ( $( v ).val() ) {

                if ( ! $.apl_admin.is_email($( v ).val()) ) {
                    $( '#apollo-notice' ).show();
                    $( v ).addClass( 'apollo-input-error' );
                    $( '[data-id=\"'+$( v ).attr( 'name' ) +'-email\"]' ).show();
                    error = true;
                    add_error_parent($(v));
                } else {
                    $( '[data-id="'+$( v ).attr( 'name' ) +'-email"]' ).hide();
                    $( v ).removeClass( 'apollo-input-error' );
                    remove_error_parent($(v));
                }
            }

        });

        var _arr_datecompare = $( '.apollo-date-greater' );
        $.each( _arr_datecompare, function(i, v){
            var _end = $(v).val();
            var _today = $(v).attr('data-today');
            if ( _end ) {
                var _compare_to = $(v).data().compare,
                    _start = $( '#'+ _compare_to ).val();
                if ( _start ) {
                    var _endTime = new Date(_end).getTime();
                    var _error_date = _endTime < new Date(_today).getTime() || new Date(_start).getTime() > _endTime;
                    if ( _error_date ) {
                        $( '#apollo-notice' ).show();
                        $(v).addClass('apollo-input-error');
                        $( '#'+ _compare_to ).addClass('apollo-input-error');
                        $( '[data-id=\"'+$( v ).attr( 'name' ) +'-greater\"]' ).show();
                        error = true;
                        add_error_parent($(v));
                    } else {
                        $(v).removeClass('apollo-input-error');
                        $( '#'+ _compare_to ).removeClass('apollo-input-error');
                        remove_error_parent($(v));
                        $( '[data-id=\"'+$( v ).attr( 'name' ) +'-greater\"]' ).hide();
                    }
                }
            }
        });

        var checked = false;
        if( $('body').hasClass('post-type-event')){
            $('.cmb-day-in-week').each(function(){
                if($(this).is(':checked')){
                    checked = true;
                }
            });

            if(checked == false){
                //$('#_apollo_event_data[_apollo_event_days_of_week]-required"').show();
                $('#apollo-notice').show();
                $( '[data-id=\"_apollo_event_data[_apollo_event_days_of_week]-required\"]' ).show();
                $('.datetime_options ').addClass('error');
                $('.cmb-day-in-week').addClass('apollo-input-error');
                error = true;
            }else{
                $( '[data-id=\"_apollo_event_data[_apollo_event_days_of_week]-required\"]' ).hide();
                $('.datetime_options ').removeClass('error');
                $('.cmb-day-in-week').removeClass('apollo-input-error');
            }
        }

        var id = $( '.post-type-venue #content' ).attr('id'),
            name = $( '.post-type-venue #content' ).attr('name'),
            editor = tinyMCE.get(id);
        if ( editor && ! editor.getContent()){
            $('.post-type-venue li[data-id="content-required"]').show();
            $('#apollo-notice').show();
            $('.post-type-venue #content_ifr').addClass("apollo-input-error");
            error =  true;
        }else{
            $('.post-type-venue li[data-id="content-required"]').hide();
            $('.post-type-venue #content_ifr').removeClass("apollo-input-error");
            if(error === true){
                error = true;
            }else{
                error = false;
            }
        }

        //endTriLM

        //hong
        var id = $( '.post-type-classified #content' ).attr('id'),
            name = $( '.post-type-classified #content' ).attr('name'),
            editor = tinyMCE.get(id);
        if ( editor && ! editor.getContent()){
            $('.post-type-classified li[data-id="content-required"]').show();
            $('#apollo-notice').show();
            $('.post-type-classified #content_ifr').addClass("apollo-input-error");
            error =  true;
        }else{
            $('.post-type-classified li[data-id="content-required"]').hide();
            $('.post-type-classified #content_ifr').removeClass("apollo-input-error");
            if(error === true){
                error = true;
            }else{
                error = false;
            }
        }

        var dateLargerThanNow = $( '.apollo-date-greater-than-now' );
        $.each( dateLargerThanNow, function(i, v){
            var _date = $(v).val();
            if ( _date ) {
                var _error_date = new Date().getTime() >= new Date(_date).getTime();
                if ( _error_date ) {
                    $( '#apollo-notice' ).show();
                    $(v).addClass('apollo-input-error');
                    $( '[data-id=\"'+$( v ).attr( 'name' ) +'-larger_than_now\"]' ).show();
                    error = true;
                    add_error_parent($(v));
                } else {
                    $(v).removeClass('apollo-input-error');
                    remove_error_parent($(v));
                    $( '[data-id=\"'+$( v ).attr( 'name' ) +'-larger_than_now\"]' ).hide();
                }

            }
        });

        var id = $( '.post-type-public-art #content' ).attr('id'),
            name = $( '.post-type-public-art #content' ).attr('name'),
            editor = tinyMCE.get(id);

        if ( editor && ! editor.getContent()){
            $('.post-type-public-art li[data-id="content-required"]').show();
            $('#apollo-notice').show();
            $('.post-type-public-art #content_ifr').addClass("apollo-input-error");
            error =  true;
        }else{
            $('.post-type-public-art li[data-id="content-required"]').hide();
            $('.post-type-public-art #content_ifr').removeClass("apollo-input-error");
            if(error === true){
                error = true;
            }else{
                error = false;
            }
        }

        var LatLngRelated = $( '.lat-lng-related' );
        $.each( LatLngRelated, function(i, v){
            var _lat = $(v).val();
            var _compare_to = $(v).data().compare;
            var _lng = $( '#'+ _compare_to ).val();
            var _error_related =false;
            if( ( (!_lat || _lat.length === 0) && (_lng && _lng.length !== 0) ) ||
                ( (_lat && _lat.length !== 0) && (!_lng || _lng.length === 0) )
            ) {
                _error_related = true;
            }
            if ( _error_related ) {
                if($(v).val().length !==0) {
                    $('#apollo-notice').show();
                    $(v).addClass('apollo-input-error');

                    $('#' + _compare_to).addClass('apollo-input-error');
                    $('[data-id=\"' + $('#' + _compare_to).attr('name') + '-lat_lng_related\"]').hide();
                    $('[data-id=\"' + $(v).attr('name') + '-lat_lng_related\"]').show();
                    error = true;
                }
                add_error_parent($(v));
            } else {
                $(v).removeClass('apollo-input-error');
                $( '#'+ _compare_to ).removeClass('apollo-input-error');
                remove_error_parent($(v));
                $( '[data-id=\"'+$( v ).attr( 'name' ) +'-lat_lng_related\"]' ).hide();
            }

        });

        var startDate = new Date($('#_apollo_event_start_date').val()),
            endDate = new Date($('#_apollo_event_end_date').val()),
            startDateYear = startDate.getFullYear(),
            endDateYear = endDate.getFullYear(),
            yearDiff = endDateYear - startDateYear;
        if(yearDiff > apollo_admin_obj.event_maximum_year){
            error =  true;
            $(document).find( '#_apollo_event_start_date' ).addClass( 'apollo-input-error' );
            $(document).find( '#_apollo_event_end_date' ).addClass( 'apollo-input-error' );
            $('#apollo-notice').show();
            $( '[data-id=\"_apollo_event_data[_apollo_event_start_date]-maximum_year\"]' ).show();
        }else{
            $(document).find( '#_apollo_event_start_date' ).removeClass( 'apollo-input-error' );
            $(document).find( '#_apollo_event_end_date' ).removeClass( 'apollo-input-error' );
            $( '[data-id=\"_apollo_event_data[_apollo_event_start_date]-maximum_year\"]' ).hide();
        }


        //end hong
        if ( error ) {
            $('html,body').animate({ scrollTop: $('#wpbody-content').offset().top }, 1000);
            var _blockUI = {
                message: APL.resource.loading.message,
                css: jQuery.parseJSON(APL.resource.loading.image_style)
            };
            $(window).unblock(_blockUI);
            return false;
        }

        if ($('body').hasClass('post-type-event')) {
            var obj = $('#admin_event_save_date_add_new');

            if (obj.length == 0) {
                obj = $('#admin_event_save_date');
            }

            if (!checkHasStartTime(obj)) {
                return false;
            }
        }
    };

    //TriLM is audio embed
    $.apl_admin.is_audio_embed = function($embed){

        //check embed link
        var arrayTypes = [
            'mp3',
            'mp4',
            'wav',
            'aif',
            'flac'
        ];
        var isEmbed = $embed.indexOf('/audio_embed/');
        for(var i=0; i< arrayTypes.length ; i++){
            var pattern = new RegExp('\.'+arrayTypes[i]+"$");
            if (pattern.test($embed) == true) {
                return true;
            }
        }
        var openTagIframe= $embed.indexOf('iframe');
        var closeTagIframe= $embed.indexOf('</iframe>');
        var openTagObject= $embed.indexOf('object');
        var closeTagObject= $embed.indexOf('</object>');

        var openTagEmbed = $embed.indexOf('embed');
        var closeTagEmbed = $embed.indexOf('</embed>');

        /** @Ticket #19321 Sound Cloud link */
        var soundCloudLink = $embed.indexOf('https://soundcloud.com');

        if(isEmbed !=  -1) return true;
        if(openTagIframe != -1 && closeTagIframe != -1) return true;
        if(openTagObject != -1 && closeTagObject != -1) return true;
        if(openTagEmbed != -1 && closeTagEmbed != -1) return true;
        if (soundCloudLink != -1) return true;
        return false;
    };
    checkCBBefore = function(elm) {

        if ( $(elm).prev().attr('checked') == 'checked' ) {
            $(elm).prev().removeAttr('checked');
        } else {
            $(elm).prev().attr('checked', true);
        }
    };

    $(document).on('click','.wrap-ddl-blk input[type="checkbox"]',function(){
        var isChecked = $(this).prop('checked'),
            id = $(this).attr('id') ;
        if($('#hidden'+id).length > 0){
            if(!isChecked){
                $('#hidden'+id).removeAttr('checked');
            }
        }
    });

    /*@ticket #17235 - [Admin] Official tag for event and post */
    $(document).ready(function () {
        $('.wp-admin').off('click', '.apl-official-tag-delete').on('click', '.apl-official-tag-delete', function (e) {
            var current = $(e.currentTarget);
            var currentItem = current.closest('span');
            if (currentItem.length > 0) {
                currentItem.remove();
            }
        });

        $('.wp-admin').off('click', '.add-official-tag').on('click', '.add-official-tag', function (e) {

            var tagValue = $('.apl-select-official-tag').val();
            var postType = $(this).attr("post-type");
            var metaKey = '_apl_' + postType + '_official_tags[]'

            if (!$('#apl-official-tag-' + tagValue).length && parseInt(tagValue)) {
                var tagName = $('.apl-select-official-tag :selected').text();
                var newElement = '<span id="apl-official-tag-' + tagValue + '">' +
                    '<button class="apl-official-tag-delete"><i class="fa fa-times"></i></button>' + tagName +
                    '<input type="hidden" value=' + tagValue + ' name="' + metaKey + '"></span>';

                $('.apl-official-tags-selected').append(newElement);

                $('.apl-select-official-tag').val(0);
            }
        });
    });

})(jQuery);


(function($) {
    $(function() {
        var apl_cf = {
            'helpers': {
                getIndex: function() {},
                getType: function() {},
                getFieldPrefixName: function() {},
            }
        };

        $('.aplcf .fields').on( 'click', '.aplcf_edit_field', function() {
            var form = $(this).closest('.field_meta').next();
            form.css('display', '');
            form.toggleClass('hidden', 'fast');

            var row_active = 'active-row-cf';
            if ( form.hasClass('hidden') ) {
                $(this).closest('table.aplcf').removeClass(row_active);
            } else {
                $(this).closest('table.aplcf').addClass(row_active);
            }

        });

        $('.aplcf .fields').on( 'focus', 'input.name', function() {
            var label = $(this).closest('.aplcf_field_form_table').find('input.label').val();
            if ( $(this).val() ) return false;
            $(this).val( $.trim(label.toLowerCase()).replace(/\s/g,'_') );
        });

        $('.aplcf .fields').on( 'change', '.field_type .type', function() {
            $(this).closest('.aplcf_field_form_table').find('[class^="field_option field_option_"]').hide();
            var _bfLi = $(this).closest('.aplcf_field_form_table').find('.field_location ul li').has("input[value='bf']");
            var _ortherLi = $(this).closest('.aplcf_field_form_table').find('.field_location ul li').has("input[value!='bf']");
            if( $(this).val() == 'gallery') {
                _ortherLi.hide();
                _bfLi.show();
                _bfLi.find('input').prop('checked',true);
                _bfLi.find('input').prop('disabled', true);
            } else {
                _ortherLi.show();
                _bfLi.find('input').prop('checked',false);
                _bfLi.hide()
            }
            $(this).closest('.aplcf_field_form_table').find('.field_option.field_option_'+ $(this).val()).show();

            /*@ticket #18221: 0002504: Arts Education Customizations - Add a checkbox option for "Add Menu Arrow" - item 1*/
            if ($(this).val() === 'multi_checkbox'){
                $(this).closest('.aplcf_field_form_table').find('.apl-custom-field-menu-arrow').show();
            }
            else{
                $(this).closest('.aplcf_field_form_table').find('.apl-custom-field-menu-arrow').hide();
            }
            /** For Display control option */
            var parent = $(this).closest('.field');
            var displayControl = parent.find('.field_display_control input.apl-cf-display-control');
            if (parent.length > 0 && displayControl.length > 0) {
                if ($(this).val() === 'checkbox') {
                    displayControl.removeClass('checkbox-hidden');
                } else {
                    displayControl.addClass('checkbox-hidden');
                    displayControl.attr('checked', false);
                }
            }

        });

        $( '.aplcf .fields' ).on('change', '.field_option-data_manual', function() {
            var fieldWrapper = $(this).closest('.field_form_mask'),
                parentSource    = fieldWrapper.find('.select.field_option-data_source').closest('.field_option'),
                parentReferTo  = fieldWrapper.find('.select.field_option-refer_to').closest('.field_option'),
                parentChoice    = fieldWrapper.find('.select.field_option-choice').closest('.field_option');

            parentSource.hide();
            parentReferTo.hide();
            parentChoice.hide();

            if ( $(this).val() == 'source' ) {
                parentSource.show();
                parentReferTo.show();
            } else {
                parentChoice.show();
            }
        });



        /**
        * @author Trilm
        * filter region theme option
        *
        */

        $( document).on('change', '#select_region', function() {
            $('ul li.select-zip ').removeClass('active');
            var val = $(this).children('option:selected').val();
            $('ul li[data-parent="'+val+'"]').addClass('active');
             checkUncheckAll();
        });


         $( document).on('change', '#filtering_by_region', function() {
                var val = $(this).children('option:selected').val();
               if(val == 1){
                    $('#region-content').removeClass('hidden');
               }else{
                    $('#region-content').addClass('hidden');
               }


        });

        $( document).on('change', '#check_all_region_zip', function() {
            var region = $('#select_region').children('option:selected').val();

            if($( this ).prop( "checked" )){
                $('input[data-parent="'+region+'"]').prop('checked',true);
            }else{
                 $('input[data-parent="'+region+'"]').prop('checked',false);
            }

             checkUncheckAll();

        });

        $('.select-zip .zip-item').click(function(){
            var parent = $(this).attr("data-parent");
            checkUncheckAll();
        });

        function checkUncheckAll(){
            var region = $('#select_region').children('option:selected').val();
            var checkAll = true;
            $('input[data-parent="'+region+'"]').each(function(){
                var check = $( this ).prop( "checked" );
                if(!check){
                    checkAll = false;
                }
            });

            if(checkAll){
                $('#check_all_region_zip').prop('checked',true);
            }else{
                 $('#check_all_region_zip').prop('checked',false);
            }
            return checkAll;
        }

        $(document).ready(function(){
            if($('#select_region').length){
                checkUncheckAll();
            }

            /** @Ticket #12931 */
            if (apollo_admin_obj.venue_access_mode != 0) {
                var eventVenue = parseInt($('#_apollo_event_venue').val(), 0);
                var accessibility = $('#normal-sortables').find('#apollo-event-acb');
                if (eventVenue != 0) {
                    if (accessibility.length > 0 && !accessibility.hasClass('hidden')) {
                        accessibility.addClass('hidden');
                    }
                } else {
                    if (accessibility.length > 0 && accessibility.hasClass('hidden')) {
                        accessibility.removeClass('hidden');
                    }
                }
            }

            /** @Ticket #13424 */
            $('.org-is-business').off('click').on('click', function(e) {
                var isBusiness = $(e.currentTarget);
                var editLink = $('.apl-org-business-edit-link');
                if (editLink.length > 0) {
                    if (isBusiness.prop('checked') == true) {
                        if (editLink.hasClass('hidden')) {
                            editLink.removeClass('hidden');
                        }
                    } else {
                        if (!editLink.hasClass('hidden')) {
                            editLink.addClass('hidden');
                        }
                    }
                }
            });

            /** @Ticket #15013 - Clear cache */
            $('#apl-admin-cache').off('click', '.apl-cache-remove').on('click', '.apl-cache-remove', function (e) {
                if (confirm('Do you want purge cache?')) {
                    e.preventDefault();
                    var current = $(e.currentTarget);
                    var wrapItem = current.closest('li');
                    var className = wrapItem.attr('data-class-name');
                    var params = wrapItem.attr('data-params');
                    if (typeof params === 'undefined') {
                        params = '';
                    }
                    var fileType = wrapItem.attr('data-file-type');
                    if (typeof fileType === 'undefined') {
                        fileType = '';
                    }
                    var extension = wrapItem.attr('data-extension');
                    if (typeof extension === 'undefined') {
                        extension = '';
                    }
                    var expDate = wrapItem.find('.apl-admin-cache-exp');

                    var eventTransient = wrapItem.attr('data-event-transient-name');
                    if (typeof eventTransient === 'undefined') {
                        eventTransient = '';
                    }
                    var syndicationTransient = '';
                    if (current.hasClass('transient-cache')) {
                        syndicationTransient = 1;
                    }
                    var parent = current.closest('.apl-custom-postbox');
                    if (typeof className !== 'undefined') {
                        $.ajax({
                            url: APL.ajax_url,
                            type: 'POST',
                            dataType: 'JSON',
                            data: {
                                action: 'apollo_admin_clear_cache',
                                class_name: className,
                                params: params,
                                file_type: fileType,
                                event_transient: eventTransient,
                                extension: extension,
                                syn_transient: syndicationTransient
                            },
                            beforeSend: function () {
                                parent.block(jQuery.apl.blockUI);
                            },
                            success: function (res) {
                                if (res.status) {
                                    if (!expDate.hasClass('is-empty')) {
                                        expDate.addClass('is-empty');
                                    }
                                    expDate.html('');
                                    if (!wrapItem.hasClass('disabled')) {
                                        wrapItem.addClass('disabled');
                                    }
                                }
                                parent.unblock();
                            }
                        });
                    }
                }
            });

            $('#apl-admin-cache').off('click','.apl-admin-cache-submit').on('click', '.apl-admin-cache-submit', function () {
                var listItem = $('#apl-admin-cache .apl-cache-checkbox:checked');
                if (listItem.length <= 0 ) {
                    alert('Please, select caching items.');
                    return false;
                }
                if (confirm('Do you want purge cache?')) {
                    $('#apl-admin-cache').submit();
                } else {
                    return false;
                }
            });

            $('#apl-admin-cache .nav-tab-wrapper .nav-tab').off('click').on('click', function (e) {
                e.preventDefault();
                var current = $(e.currentTarget);
                 if (current.hasClass('nav-tab-active')){
                     return;
                 }
                 current.closest('#apl-admin-cache').find('.nav-tab-wrapper .nav-tab').removeClass('nav-tab-active');
                 current.addClass('nav-tab-active');
                 var parent = current.closest('#apl-admin-cache').find('.apl-cache-content');
                 if (parent.length > 0) {
                     parent.find('.apl-custom-postbox').addClass('hidden');
                     var currentContent = parent.find( '#' + current.attr('data-tab-id'));
                     if (currentContent.length > 0) {
                         currentContent.removeClass('hidden');
                     }
                 }
            });


            /** @Ticket #14914 */
            $('.wrap-loadmore').off('click', '.load-more-item').on('click', '.load-more-item', function (e) {
                e.preventDefault();
                var current = $(e.currentTarget);
                var postType = current.attr('data-post-type');
                var offset = current.attr('data-offset');
                var arrSelected = current.attr('data-arr-selected');
                var parent = current.closest('.apl-custom-postbox');
                $.ajax({
                    url: APL.ajax_url,
                    type: 'GET',
                    dataType: 'json',
                    data:{
                        action : 'apollo_user_get_more_association_options',
                        post_type: postType,
                        offset: offset,
                        arr_selected: arrSelected
                    },
                    beforeSend: function () {
                        $(parent).block(jQuery.apl.blockUI);
                    },
                    success: function (res) {
                        $(parent).unblock();
                        if (res.opts_str_display) {
                            var associationList = current.closest('.wrap-multiselect').find('.multiselect');
                            if (associationList.length > 0) {
                                associationList.append(res.opts_str_display);
                            }
                        }
                        if (res.have_more) {
                            $(current).attr('data-offset', res.offset);
                        } else {
                            $(current).closest('.wrap-loadmore').addClass('hidden');
                        }
                    }
                });
            });


        });




        $('#datetime-event-data').on('change','#_apollo_event_start_date',function(){
            var value = $(this).val();
            var value_end = $('#_apollo_event_end_date').val();
            var start = new Date(value);
            var end = new Date(value_end);
            var diff = (end - start)/ (1000 * 60 * 60 * 24);
            if(diff < 0 || value_end == '' ){
                $('#_apollo_event_end_date').val(value);
            }
        });

        //$('#apollo-event-data').on('click','#admin_event_save_date_add_new',function(){
        //    $('#publish').trigger('click');
        //});

        $('body.post-type-event').on('click','#publish_event',function(){
            /**@Ticket #13639 */
            var eventSaveDateButton = $('#admin_event_save_date');
            if (eventSaveDateButton.length > 0) {
                eventSaveDateButton.attr('data_event_status', 'publish');
                eventSaveDateButton.trigger('click');
            } else {
                /** @Ticket #15925 - publish the event with original status is draft */
                var originalStatus = $('#original_post_status');
                if  (originalStatus.length > 0 && originalStatus.val() == 'draft') {
                    $('#publish').trigger('click');
                }
            }

        });

        $('.aplcf #add-field').click(function() {

            var index   = apl_cf.helpers.getIndex(),
                field   = $('#aplcf-clone .field').clone();

            // Change field names
            field.change_names();

            // Show form
            field.find('.field_form_mask').show();
            //ield.find('#order-field').attr('');

            // Add and show new field
            $('.fields').append( field );
            field.show();

            // Set new order
            field.find('.field_order span').text(index+1);

            field.find('.field_label .row-title').text($('.aplcf .new-field-label').val());

            field.find('input[name]');
        });

        /**
         * General init data table
         * */
        var _dataTables = $('.apl-admin-datatable');
        $.each(_dataTables, function(i, v) {
            var _id = '#'+ $(v).attr('id');

            $(_id).dataTable({
                "bProcessing": true,

                "fnDrawCallback": function() {
                    // in case your overlay needs to be put away automatically you can put it here
                },
                "fnCreatedRow": function ( row, data, index ) {

                }
            });
        });


        $('#apl-cf-save-btn').click(function() {
            var require_f = $('[data-required]'),
                has_error = false;
            $.each(require_f, function(i, v) {
                if ( ! $(v).val() ) {
                    $(v).addClass('apollo-input-error');
                    has_error = true;
                } else {
                    $(v).removeClass('apollo-input-error');
                }
            });

            if ( has_error ) {
                $( '#apollo-custom-notice' ).show();
                $('html,body').animate({ scrollTop: $( '.apollo-artist-custom-fields' ).offset().top }, 500);
                return false;
            }

            $(this).closest('form').submit();

        });

        $('.aplcf .fields').on('change', 'select.type', function() {
            var index   = apl_cf.helpers.getIndex() - 1;

            var frefix  = 'fields[field_'+(index)+']',
                field   = $(this).closest('.field'),
                type    = $(this).val();

            field.find('.field_option-data_source').closest('.field_option').hide();
            $('select.field_option-data_manual').children( 'option[value="choice"]' ).attr('selected', 'selected');
            field.find('.field_option-desc').attr('name', frefix+ '[meta_'+type+'][desc]');
            field.find('.'+type+'.field_option-choice').attr('name', frefix+ '[meta_'+type+'][choice]');
            field.find('.'+type+'.field_option-data_source').attr('name', frefix+ '[meta_'+type+'][data_source]');
            field.find('.'+type+'.field_option-data_manual').attr('name', frefix+ '[meta_'+type+'][data_manual]');
            field.find('.'+type+'.field_option-refer_to').attr('name', frefix+ '[meta_'+type+'][refer_to]');

            /** @Ticket #14076 */
            field.find('.'+type+'.syn_alternate').attr('name', frefix+ '[meta_'+type+'][alternate_values]');

            if( $('input[type="checkbox"][value="rsb"]') ) {
                var rsb_location = $(this).closest('.field_type').next('.field_location').find('input[value="rsb"]');
                if ( $(this).val() != 'checkbox' ) {
                    rsb_location.parent().hide();
                    rsb_location.attr('checked', false);
                } else {
                    $(this).closest('.field_type').next('.field_location').show();
                    rsb_location.parent().show();
                }
            }

            /** @Ticket #13237 */
            /** @Ticket #13823 */
            var currentField = $(this).closest('.ui-sortable-handle'),
                searchWidget = currentField.find('.field_location input[value=sw]');

            if (searchWidget.length > 0) {
                var arrSeachWdTypes = searchWidget.attr('data-sw').split(',');
                if (arrSeachWdTypes.indexOf($(this).val()) == -1) {
                    searchWidget.closest('li').attr('style', 'display: none;');
                }
            }

        });

        var apl_check_require_multi = function (_obj) {
            var name = _obj.attr('name'),
                groups = $('[name="'+ name +'"]'),
                error = true;

            $.each(groups, function(i, v) {
                if ( $(v).attr('checked') == 'checked' ) error = false;
            });
            var hidden_elm = _obj.closest('.options_group').children('input[type="hidden"]');
            hidden_elm.val( error ? '' : 1 );
        };

        // Add to hidden value of multiple checkbox to check required validate
        $('.options_group .form-field input[type="checkbox"]').click(function() {
            apl_check_require_multi($(this));
        });

        $('.options_group input[type="radio"]').click(function() {
            apl_check_require_multi($(this));
        });

        $('.aplcf').on('click', '.aplcf_delete_field', function() {
            var field = $(this).closest('.field.form_open');
            var removeObj = $('input[name="aplcf_remove_fields"]'),
                newId = field.find('input.id').val();
            removeObj.val(removeObj.val()+ ','+ newId);
            field.remove();
        });

        if ( $(".fields").sortable ) {
            if ( $(".fields") ) {
                $(".fields").sortable({
                    items: '.field',
                    cursor: 'move',
                    axis: 'y',
                    forcePlaceholderSize: true,
                    helper: 'clone',
                    opacity: 0.65,
                    scrollSensitivity: 40,
                    start: function(event, ui) {
                        if ( ! ui.item.hasClass('alternate') ) ui.item.css( 'background-color', '#ffffff' );
                        ui.item.children('td,th').css('border-bottom-width','0');
                        ui.item.css( 'outline', '1px solid #aaa' );
                    },
                    stop: function(event, ui) {
                        ui.item.removeAttr('style');
                        ui.item.children('td,th').css('border-bottom-width','1px');
                    },
                    update: function(event, ui) {

                        // show spinner
                        ui.item.find('.check-column input').hide().after('<img alt="processing" src="images/wpspin_light.gif" class="waiting" style="margin-left: 6px;" />');

                        var chboxs = $( '.aplcf .fields input.id' );
                        var ids = [];
                        $.each(chboxs, function (k, v) {
                            var val = $(v).val();
                            if ( val == parseInt( val )  ) ids.push( val );
                        });

                        // go do the sorting stuff via ajax
                        $.post( apollo_admin_obj.ajax_url, { ids: ids.join(','), action: 'apollo_re_order_custom_fields' }, function(response){

                        });
                    }
                });
            }

            if ( $("table#tbl-apl-custom-field") ) {
                $("table#tbl-apl-custom-field").sortable({
                    items: 'tr.order',
                    cursor: 'move',
                    axis: 'y',
                    forcePlaceholderSize: true,
                    helper: 'clone',
                    opacity: 0.65,
                    scrollSensitivity: 40,
                    start: function(event, ui) {
                        if ( ! ui.item.hasClass('alternate') ) ui.item.css( 'background-color', '#ffffff' );
                        ui.item.children('td,th').css('border-bottom-width','0');
                        ui.item.css( 'outline', '1px solid #aaa' );
                    },
                    stop: function(event, ui) {
                        ui.item.removeAttr('style');
                        ui.item.children('td,th').css('border-bottom-width','1px');
                    },
                    update: function(event, ui) {

                        // show spinner
                        ui.item.find('.check-column input').hide().after('<img alt="processing" src="images/wpspin_light.gif" class="waiting" style="margin-left: 6px;" />');

                        var chboxs = $( 'table#tbl-apl-custom-field input.id' );
                        var ids = [];
                        $.each(chboxs, function (k, v) {
                            var val = $(v).val();
                            if ( val == parseInt( val )  ) ids.push( val );
                        });

                        // go do the sorting stuff via ajax
                        $.post( apollo_admin_obj.ajax_url, { ids: ids.join(','), action: 'apollo_re_order_custom_fields' }, function(response){

                        });

                        // fix cell colors
                        $( 'tr' ).each(function(){
                                var i = $('.aplcf.widefat tr').index(this);
                                if ( i%2 == 0 ) $(this).addClass('alternate');
                                else $(this).removeClass('alternate');
                        });
                    }
                });
            }
        }

        $.fn.change_names = function() {
            var frefix  = 'fields[field_'+apl_cf.helpers.getIndex()+']',
                field   = $(this),
                type    = apl_cf.helpers.getType();

            field.find('[class^="field_option field_option_"]').hide();
            field.find('.field_option_'+type+'').show();

            /*@ticket #18221: 0002504: Arts Education Customizations - Add a checkbox option for "Add Menu Arrow" - item 1*/
            field.find('input.add-menu-arrow').attr('name', frefix+ '[add_menu_arrow]').prop('checked', false);
            field.find('.apl-custom-field-menu-arrow').hide();

            field.find('input.label').attr('name', frefix+ '[label]').val('');
            field.find('input.name').attr('name', frefix+ '[name]').val('');
            field.find('input.id').attr('name', frefix+ '[id]').val('');
            field.find('select.type').attr('name', frefix+ '[type]');
            field.find('input.required').attr('name', frefix+ '[required]');
            field.find('input.location').attr('name', frefix+ '[location][]');
            field.find('input.number').attr('name', frefix+ '[meta_textarea][row]').val('');
            field.find('input.apl-cf-display-control').attr('name', frefix+ '[display_control]').val('yes');

            field.find('input[value="rsb"]').parent().hide();

            var fields = ['select', 'checkbox', 'radio', 'multi_checkbox', 'radio', 'wysiwyg', 'text', 'textarea'],
                fieldOptions = ['choice', 'other_choice', 'explain', 'character_limit', 'validation',
                                'display_style', 'refer_to', 'data_source', 'data_manual'];

            $.each( fields, function (i, v) {
                // Change name of options key for each field
                $.each( fieldOptions, function(i, opt) {
                    var obj = field.find('.'+v+'.field_option-'+opt+'').attr('name', frefix+ '[meta_'+v+']['+opt+']');
                    if ( opt != 'other_choice' && opt != 'explain' ) {
                        obj.val('');
                    }
                });
            });

            // Change the desc name
            field.find('.field_option-desc').attr('name', frefix+ '[meta_'+type+'][desc]').val('');

        };

        apl_cf.helpers.getIndex = function() {
            return $('form .field').length;
        };
        apl_cf.helpers.getType = function() {
            return $('.field').find('select.type').val();
        };
        apl_cf.helpers.getFieldPrefixName = function() {
            return 'fields[field_'+apl_cf.helpers.getIndex()+']';
        };

        $(document).on('click','#apl_sync_solr_data',function(){
            var $this = $(this);
            $this.attr('disabled','true');
             $.ajax({
                url: APL.ajax_url,
                type: 'POST',
                data:{
                    action : 'apollo_sync_solr_data',
                },
                beforeSend: function () {

                },
                success: function (response) {
                    $this.removeAttr('disabled');
                    alert(response.message);
                }
            });
        });

        $(document).on('click','#apl_delete_sync_solr_data',function(){
            var $this = $(this);
            $this.attr('disabled','true');
             $.ajax({
                url: APL.ajax_url,
                type: 'POST',
                data:{
                    action : 'apollo_delete_sync_solr_data',
                },
                beforeSend: function () {

                },
                success: function (response) {
                    $this.removeAttr('disabled');
                    alert(response.message);
                }
            });
        });
        $(document).on('click','#refresh-token-key',function(){
            var $this = $(this);
            var _length = $this.attr('data-token-key-length');

            var _confirm = confirm($this.attr('data-confirm'));

            if (! _confirm) return false;

             $.ajax({
                url: APL.ajax_url,
                type: 'POST',
                 dataType: 'json',
                data:{
                    action : 'apollo_get_token',
                    key_length: _length,
                    appId: $this.attr('data-app-id'),
                },
                success: function (response) {
                    if(response.status == '1'){
                        $('#meta-token-key').val(''+response.data);
                    }
                }
            });
        });
        $(document).on('click', '#empty-spotlight-cache',function(e){
            e.preventDefault();
            $.ajax({
                url: APL.ajax_url,
                type: 'POST',
                dataType: 'json',
                data:{
                    action : 'apollo_empty_spotlight_cache',
                },
                success: function (response) {
                    location.reload();
                }
            });
        });

        /* @Ticker 13700 */
        var viewMoreObj = {showChar: 200, ellipsestext: "...", moretext: "View more", lesstext: "View less" };

        $(".option .controls .explain").each(function(index, ele){
            var content = $(ele).text();
            if(content.length > viewMoreObj.showChar) {
                var c = content.substr(0, viewMoreObj.showChar);
                var h = content.substr(viewMoreObj.showChar, content.length - viewMoreObj.showChar);
                var html = c + '<span class="moreelipses">' + viewMoreObj.ellipsestext + '</span>&nbsp;<span class="morecontent"><span>' + h +
                    '</span>&nbsp;&nbsp;<a href="" class="morelink">' + viewMoreObj.moretext + '</a></span>';
                $(ele).addClass('more').html(html);
            }

        });

        $("body").on('click', '.option .explain .morelink', function(e){
            e.preventDefault();
            if($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(viewMoreObj.moretext);
            } else {
                $(this).addClass("less");
                $(this).html(viewMoreObj.lesstext);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
        });

    });
}) (jQuery);


function checkHasStartTime(obj) {
    if(jQuery('.post-type-event .selected_day').hasClass('disable-requirement')){
        return true;
    } else {
        var dates = jQuery('.date-week .date-cell:not(.disable)'),
            error = false;

        if (dates.length == 0) {
            dates = jQuery('.cl-date .date-cell:not(.disable)');
        }

        jQuery.each(dates, function(i, v) {
            if (!jQuery(v).find('.cl-event-list li').length) {
                jQuery(v).addClass('cell-error');
                error = true;
            }
        });
        if (error) {
            alert(jQuery(obj).data().alert);
            jQuery('html,body').animate({ scrollTop: jQuery('#add-event-calendar').offset().top }, 500);
            return false;
        }
        return true;
    }
}


/**
 * Thienld : the script is used to handle all logic in JS on WP widget section and on Global Widget Area
 * */
//Trilm disable here because we use this function in: /var/www/apollo-theme/wp-content/themes/apollo/inc/widgets/custom/assets/js/admin.js
// (function($){
//     $(function () {
//         $(document).bind('widget-added', function(event, widget){
//             if($(widget).closest('#sidebar-modules-area').length <= 0){
//                 $(widget).find('.apl-mods-sel-blk').remove();
//             }
//         });
//         $('#sidebar-modules-area').on('click','.apl-mod-wg-all',function (e) {
//             var cbAll = $(e.currentTarget);
//             var listContainer = $(e.currentTarget).closest('.apl-wg-modules');
//             if(cbAll.prop('checked') === true){
//                 listContainer.find('.apl-mod-wg-child').prop('checked',true);
//             } else {
//                 listContainer.find('.apl-mod-wg-child').prop('checked',false);
//             }
//         });
//         $('#sidebar-modules-area').on('click','.apl-mod-wg-child',function (e) {
//             var cbItem = $(e.currentTarget);
//             var listContainer = cbItem.closest('.apl-wg-modules');
//             var numOfCBItems = listContainer.find('.apl-mod-wg-child').length;
//             if(listContainer.find('input[type="checkbox"][class="apl-mod-wg-child"]:checked').length == numOfCBItems){
//                 listContainer.find('.apl-mod-wg-all').prop('checked',true);
//             } else {
//                 listContainer.find('.apl-mod-wg-all').prop('checked',false);
//             }
//         });
//         removeModuleSelectionInIncorrectSidebarArea();
//     });
//     var removeModuleSelectionInIncorrectSidebarArea = function () {
//         $('.apl-mods-sel-blk').each(function (index, item) {
//             var sidebarAreaId = $(item).closest('.widgets-sortables').attr('id');
//             var isInWidgetList= $(item).closest('#widget-list').length > 0;
//             if( !isInWidgetList && sidebarAreaId != 'sidebar-modules-area'){
//                 $(item).remove();
//             }else if(!isInWidgetList){
//                 var numOfCBItems = $(item).find('.apl-mod-wg-child').length;
//                 if($(item).find('input[type="checkbox"][class="apl-mod-wg-child"]:checked').length == numOfCBItems){
//                     $(item).find('.apl-mod-wg-all').prop('checked',true);
//                 }
//             }
//         });
//     }
// }) (jQuery);
