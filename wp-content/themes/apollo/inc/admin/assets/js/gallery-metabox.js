jQuery(function($) {

  var file_frame;
  var listIndex = -1;

  $(document).on('click', '.gallery-metabox a.gallery-add', function(e) {

    e.preventDefault();
    var galleryList = $(this).closest(".gallery-metabox").find(".gallery-metabox-list");
    var name = $(this).closest(".gallery-metabox").attr("data-name");
    if (file_frame) file_frame.close();

    file_frame = wp.media.frames.file_frame = wp.media({
      title: $(this).data('uploader-title'),
      button: {
        text: $(this).data('uploader-button-text'),
      },
      multiple: true
    });

    file_frame.on('select', function() {
      var selection = file_frame.state().get('selection');

      selection.map(function (attachment, i) {
        attachment = attachment.toJSON(),
            listIndex -= 1;
        var url = '';
        if (typeof (attachment.sizes.thumbnail) !== 'undefined') {
          url = attachment.sizes.thumbnail.url;
        } else {
          url = attachment.sizes.full.url;
        }
        var contentGallery = $('#apl_af_gallery_item_script_template').html();
        contentGallery = contentGallery.replace(/{{image}}/g, url);
        contentGallery = contentGallery.replace(/{{id}}/g, attachment.id);
        contentGallery = contentGallery.replace(/{{index}}/g, listIndex);
        contentGallery = contentGallery.replace(/{{name}}/g, name);
        galleryList.append(contentGallery);
      });
    });

    makeSortable();
    
    file_frame.open();

  });

  $(document).on('click', '.gallery-metabox a.change-image', function(e) {

    e.preventDefault();

    var that = $(this);

    if (file_frame) file_frame.close();

    file_frame = wp.media.frames.file_frame = wp.media({
      title: $(this).data('uploader-title'),
      button: {
        text: $(this).data('uploader-button-text'),
      },
      multiple: false
    });

    file_frame.on( 'select', function() {
      attachment = file_frame.state().get('selection').first().toJSON();
      var url = '';
      if (typeof (attachment.sizes.thumbnail) !== 'undefined') {
        url = attachment.sizes.thumbnail.url;
      } else {
        url = attachment.sizes.full.url;
      }
      that.parent().find('input:hidden').attr('value', attachment.id);
      that.parent().find('img.image-preview').attr('src', url);
    });

    file_frame.open();

  });



  function makeSortable() {
    $('.gallery-metabox-list').sortable({
      opacity: 0.6,
      stop: function() {
      }
    });
  }

  $(document).on('click', '.gallery-metabox a.remove-image', function(e) {
    e.preventDefault();

    $(this).parents('li').animate({ opacity: 0 }, 200, function() {
      $(this).remove();
    });
  });

  makeSortable();

});