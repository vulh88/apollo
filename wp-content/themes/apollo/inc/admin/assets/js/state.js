'use strict';

/* App Module */

var phonecatApp = angular.module('phonecatApp', [
    'ngRoute',
    'statesControllers',
    'angularUtils.directives.dirPagination',
]);

phonecatApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/states', {
                templateUrl: '/wp-content/themes/apollo/inc/admin/network/templates/list-states.html',
                controller: 'StateListCtrl'
            }).
            when('/clist/:name/:relativeId/:mod', {
                templateUrl: '/wp-content/themes/apollo/inc/admin/network/templates/list-cities.html',
                controller: 'StateListCtrl'
            }).
            when('/zlist/:relativeId/:mod', {
                templateUrl: '/wp-content/themes/apollo/inc/admin/network/templates/list-zips.html',
                controller: 'StateListCtrl'
            }).
            otherwise({
                redirectTo: '/states'
            });
    }]);



'use strict';

/* Controllers */

var statesControllers = angular.module('statesControllers', []);

statesControllers.controller('StateListCtrl', ['$scope', '$http', '$routeParams',
    function($scope, $http, $routeParams) {

        $scope.zipcodes = '';
        $scope.cityId = '';

        $scope.pageStateRoot = apollo_admin_obj.state_root_page;

        var mod = $routeParams.mod,
            relativeId = $routeParams.relativeId;

        $scope.relativeId = relativeId;

        $scope.stateName = $routeParams.name;

        var addParam = mod != undefined ? '&mod='+ mod+ '&relativeId='+ relativeId : '';

        jQuery(window).block(jQuery.apl.blockUI);

        $http.get('/wp-admin/admin-ajax.php?action=apollo_get_states'+ addParam).success(function(data) {
            $scope.states = data.result;
            jQuery(window).unblock();
        });
        $http.get('/wp-admin/admin-ajax.php?action=apollo_is_main_site').success(function(data) {
            $scope.isMainSite = data.status;

        });

        // Zip pagination configuration
        $scope.zipConfig = {
            currentPage: 1,
            pageSize: 10
        };

        $scope.currentPage = 1;
        $scope.pageSize = 10;
        $scope.orderProp = 'name';
        $scope.errors = '';

        $scope.getZips = function() {
            jQuery(window).block(jQuery.apl.blockUI);
            var cityid = $scope.cityId = $scope.zipItem.parent = this.item.id;
            $http.get('/wp-admin/admin-ajax.php?action=apollo_get_states&mod=zip&relativeId='+ cityid ).success(function(data) {
                $scope.zipcodes = data.result;
                jQuery(window).unblock();
            });
        }

        $scope.initParent = function(type) {

            if (type == 'zip') {
                this.zipItem.parent = $scope.cityId;
            } else {
                this.item.parent = $scope.relativeId;
            }

        }

        $scope.clone = function(){
            var _confirm = confirm('Are you sure to clone all common states/cities to this site?');

            if ( !_confirm ) return false;

            jQuery(window).block(jQuery.apl.blockUI);
            $http.get('/wp-admin/admin-ajax.php?action=apollo_clone_state_zip_v1').success(function(data, status, headers, config) {

                    jQuery(window).unblock();
                    location.reload();

                }).error(function(data, status) { // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    $scope.errors.push(status);
                });

        }

        $scope.addItem = function(type) {

            if ( type == 'zip' ) {

                if ( ! $scope.zipItem.name ) {
                    return;
                }

                var dta = {
                    type: $scope.zipItem.type,
                    name: $scope.zipItem.name,
                    code: $scope.zipItem.name,
                    parent: $scope.zipItem.parent,
                };

                var item = $scope.zipItem
            } else {

                if (type == 'city' && ! $scope.item.name ) {
                    return;
                }

                if ( type == 'city' ) {
                    this.item.code = this.item.name;
                }

                var dta = {
                    type: $scope.item.type,
                    name: $scope.item.name,
                    code: $scope.item.code,
                    parent: this.item.parent
                };
                var item = $scope.item
            }

            $http.post('/wp-admin/admin-ajax.php?action=apollo_add_state_city_zip', dta
            ).success(function(data, status, headers, config) {
                $scope.errors = '';
                $scope.success = '';

                if ( data.status ) {
                    item.name = '';
                    item.code = '';
                    $scope.success = data.msg;

                    // Get new id
                    dta.id = data.result.id;
                    if ( item.type == 'zip' ) {
                        $scope.zipcodes.push(dta);
                    } else {
                        $scope.states.push(dta);
                    }

                } else {
                    if ( item.type == 'zip' ) {
                        $scope.zipErrors = data.msg;
                    } else {
                        $scope.errors = data.msg;
                    }

                }

            }).error(function(data, status) { // called asynchronously if an error occurs
                // or server returns response with an error status.
                $scope.errors.push(status);
            });

        }

        $scope.updateItem = function(type) {
            var _this = this;
            if (type != 'zip') {

                if ( !this.item.name ) {
                    return;
                }

                if ( this.item.type == 'city' ) {
                    this.item.code = this.item.name;
                }

                var dta = {
                    type: this.item.type,
                    name: this.item.name,
                    code: this.item.code,
                    id: this.item.id,
                    parent: this.item.parent,
                };
                var type = this.item.type;
            } else {

                if ( !this.zipItem.name ) {
                    return;
                }

                var dta = {
                    type: this.zipItem.type,
                    name: this.zipItem.name,
                    code: this.zipItem.name,
                    id: this.zipItem.id,
                    parent: this.zipItem.parent,
                };
                var type = this.zipItem.type
            }



            $http.post('/wp-admin/admin-ajax.php?action=apollo_add_state_city_zip', dta
            ).success(function(data, status, headers, config) {
                $scope.errors = '';
                $scope.success = '';

                if ( data.status ) {
                    _this.openEditForm = false;
                    $scope.success = data.msg;
                } else {
                    if ( type == 'zip' ) {
                        $scope.zipErrors = data.msg;
                    } else {
                        $scope.errors = data.msg;
                    }

                }

            }).error(function(data, status) { // called asynchronously if an error occurs
                // or server returns response with an error status.
                $scope.errors.push(status);
            });

        }

        $scope.deleteItem = function(type){

            var _confirm = confirm('Are you sure?');

            if ( !_confirm ) return false;

            var id = type != 'zip' ? this.item.id : this.zipItem.id;

            $http.post('/wp-admin/admin-ajax.php?action=apollo_delete_state_city_zip', {'id': id}
            ).success(function(data, status, headers, config) {
                // if delete failed
                if ( data.status != undefined && data.status == false ) {
                    alert(data.msg);
                } else {
                    jQuery("#item_" + id).fadeOut();
                }
            });
        }

        $scope.reset = function() {
            $scope.item.name = '';
            $scope.item.id = '';
            $scope.item.type = '';
        }

        $scope.save = function() {
            console.log(this.state)
        }

        $scope.openForm = function() {
            if (this.openEditForm == undefined) {
                this.openEditForm = true;
            } else if (this.openEditForm) {
                this.openEditForm = false;
            } else {
                this.openEditForm = true;
            }

        }

    }]);


