(function($) {

    $(document).ready(function(){
        /** @Ticket #15217 - Reviewed event */
        $('.apl-reviewed-edit').off('click').on('click', function(e) {
            e.preventDefault();
            var current = $(e.currentTarget);
            var postID = current.attr('data-post-id');
            $.ajax({
                url: APL.ajax_url,
                type: "POST",
                data:{
                    action : 'apollo_event_update_review_edit',
                    post_id: postID
                },
                beforeSend: function () {
                    $(window).block(jQuery.apl.blockUI);
                },
                success : function (res) {
                    if (res.status == true) {
                        var parent = $(current).closest('.reviewed');
                        if (parent.length > 0 && !parent.hasClass('hidden')) {
                            parent.addClass('hidden');

                        }
                    }
                    $(window).unblock();
                }
            });
        });

        $('#apl-accept-reject').off('click').on('click', function(e) {
            e.preventDefault();
            var current = $(e.currentTarget);
            var postID = current.attr('data-post-id');
            var reasons = current.closest('#apl-reject-event-modal').find('.modal-body .apl-wrap-reasons input[name="reject-reasons[]"]:checked');
            var reasonDetail = current.closest('#apl-reject-event-modal').find('.apl-reason-detail');
            var reasonData = [];
            if (reasons.length > 0) {
                $.each(reasons, function (index, item) {
                    reasonData.push($(item).val());
                });
            }
            $('#apl-reject-event-modal').modal('hide');
            $.ajax({
                url: APL.ajax_url,
                type: "POST",
                data:{
                    action : 'apollo_reject_event',
                    post_id: postID,
                    reasons: reasonData,
                    reasonDetail: reasonDetail.val()
                },
                beforeSend: function () {
                    $(window).block(jQuery.apl.blockUI);
                },
                success : function (res) {
                    $(window).unblock();
                    if (res.redirectLink) {
                        window.location.href = res.redirectLink;
                    }
                }
            });
        });

        /** @Ticket #15877 - count characters */
        $('.apl-wrap-reason-detail .apl-reason-detail').off('keyup').on('keyup', function(e){
            var current = $(e.currentTarget);
            var message = current.data('message');
            var limit = parseInt(current.data('limit'));
            var textLength = current.val().length;
            var textRemaining = limit - textLength;
            var count = current.closest('.apl-wrap-reason-detail').find('.apl-reason-detail-count');
            if (count.length > 0) {
                count.html(textRemaining + ' ' + message);
            }
        });

        var wrapEventStatus = $('.post-type-event .subsubsub');
        if (wrapEventStatus.length > 0) {
            var listEventStatus = wrapEventStatus.find("li");
            if (listEventStatus.length > 0) {
                var listStatus = [];
                $.each(listEventStatus, function(index, item){
                    var currentStatus = $(item).find('a').hasClass('current');
                    if (currentStatus) {
                        return true;
                    }
                    listStatus.push($(item).attr('class'));
                });
                if (listStatus.length > 0) {
                    $.ajax({
                        url: APL.ajax_url,
                        type: "POST",
                        data:{
                            action : 'apollo_get_event_count',
                            list_status: listStatus
                        },
                        beforeSend: function () {
                            $(wrapEventStatus).block(jQuery.apl.blockUI);
                        },
                        success : function (res) {
                            if ( res.event_count ) {
                                $.each(listEventStatus, function(index, item) {
                                    var currentStatus = $(item).find('a').hasClass('current');
                                    if (currentStatus) {
                                        var currentCount = $('.post-type-event .displaying-num').html();
                                        if (currentCount.length > 0) {
                                            var countNumber = currentCount.split(" ");
                                            if (countNumber.length > 0) {
                                                $(item).find('a').append('<span class="event-count">('+ countNumber[0] +')</span>');
                                            }
                                        }
                                        return true;
                                    }
                                    var element = $(item).find('a');
                                    var status = $(item).attr('class');
                                    element.append('<span class="event-count">('+ res.event_count[status] +')</span>');
                                });
                            }
                            $(wrapEventStatus).unblock();
                        }
                    });
                }
            }
        }
    });

}) (jQuery);