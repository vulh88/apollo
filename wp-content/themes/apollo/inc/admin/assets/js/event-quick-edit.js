
(function($) {
    $(function() {
        $( '#the-list' ).on( 'click', '.editinline', $.apl_admin.event.edit_inline );
    
        $('#wpbody').on('change', '#apollo-event-fields-bulk .inline-edit-group .change-to', $.apl_admin.event.bulk_edit_change_to );

        $('#the-list').on( 'click', '.submit-href', $.apl_admin.submit_href);

        
        $( '.column-category-spotlight, .column-category-featured' ).on( 'click', '.more-obj', function( e ) {
          
            e.preventDefault();
            
            if ( $(this).prev().hasClass( 'hidden' ) ) {
                $(this).html( $(this).attr( 'data-less' )+ "&#9650;" );
            } else {
                $(this).html( $(this).attr( 'data-more' )+ "&#9660;" );
            }
            
            $(this).prev().toggleClass( 'hidden' );
            
        });
    });
    
    $.apl_admin.submit_href = function() {
        if ( ! $(this).attr('data-href') ) return;
        window.location.href = $(this).attr('data-href');
    };
    
    $.apl_admin.event.bulk_edit_change_to = function() {
        
        if ( $(this).val() > 0 ) {
            $( this ).closest( 'div' ).find( '.alignright' ).show();
    	} else {
            $( this ).closest( 'div' ).find( '.alignright' ).hide();
    	}
    };
    
    $.apl_admin.event.edit_inline = function() {
        try {
            inlineEditPost.revert();
        
            var post_id = $(this).closest('tr').attr('id');

            post_id = post_id.replace("post-", "");

            var apollo_event_inline_data    = $('#apollo_event_inline_' + post_id ),
                _start_date                 = apollo_event_inline_data.find( '._start_date' ).text(),
                _end_date                   = apollo_event_inline_data.find( '._end_date' ).text(),
                _admission_detail           = apollo_event_inline_data.find( '._admission_detail' ).text(),
                _admission_phone            = apollo_event_inline_data.find( '._admission_phone' ).text(),
                _admission_ticket_url       = apollo_event_inline_data.find( '._admission_ticket_url' ).text(),
                _admission_ticket_email     = apollo_event_inline_data.find( '._admission_ticket_email' ).text(),
                _admission_discount_url     = apollo_event_inline_data.find( '._admission_discount_url' ).text(),
                _admission_discount_text     = apollo_event_inline_data.find( '._admission_discount_text' ).text(), /*@ticket #17346 */
                _admission_ticket_info      = apollo_event_inline_data.find( '._admission_ticket_info' ).text(),
                _free_admission             = apollo_event_inline_data.find( '._free_admission' ).text(),
                _event_website_url          = apollo_event_inline_data.find( '._event_website_url' ).text(),
                _contact_email              = apollo_event_inline_data.find( '._contact_email' ).text(),
                _contact_name               = apollo_event_inline_data.find( '._contact_name' ).text(),
                _contact_phone              = apollo_event_inline_data.find( '._contact_phone' ).text();

            $('input[name="_apollo_event_start_date"]', '.inline-edit-row').val( _start_date );
            $('input[name="_apollo_event_end_date"]', '.inline-edit-row').val( _end_date );
            $('textarea[name="_apollo_event_data[_admission_detail]"]', '.inline-edit-row').html( _admission_detail );
            $('input[name="_apollo_event_data[_admission_phone]"]', '.inline-edit-row').val( _admission_phone );
            $('input[name="_apollo_event_data[_admission_ticket_url]"]', '.inline-edit-row').val( _admission_ticket_url );
            $('input[name="_apollo_event_data[_admission_ticket_email]"]', '.inline-edit-row').val( _admission_ticket_email );
            $('input[name="_apollo_event_data[_admission_discount_url]"]', '.inline-edit-row').val( _admission_discount_url );
            $('textarea[name="_apollo_event_data[_admission_ticket_info]"]', '.inline-edit-row').html( _admission_ticket_info );
            /*@ticket #17346 */
            $('input[name="_apollo_event_data[_admission_discount_text]"]', '.inline-edit-row').val( _admission_discount_text );

            $('input[name="_apollo_event_data[_website_url]"]', '.inline-edit-row').val( _event_website_url );

            $('input[name="_apollo_event_data[_contact_email]"]', '.inline-edit-row').val( _contact_email );
            $('input[name="_apollo_event_data[_contact_name]"]', '.inline-edit-row').val( _contact_name );
            $('input[name="_apollo_event_data[_contact_phone]"]', '.inline-edit-row').val( _contact_phone );

            if ( _free_admission == 'yes' ) {
                $( 'input[name="_apollo_event_free_admission"]', '.inline-edit-row' ).attr( 'checked', 'checked' );
            } else {
                $( 'input[name="_apollo_event_free_admission"]', '.inline-edit-row' ).removeAttr( 'checked' );
            }
        } catch(e) {}
    };
    
})(jQuery);
