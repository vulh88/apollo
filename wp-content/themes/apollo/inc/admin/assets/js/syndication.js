(function($) {

    $(function() {

        var $clearCacheBtn = $('#syn-clear-cache');

        $('input[name="enable-file-caching"], input.syndication-enable-caching').on('click', function() {

            var $this = $(this);

            if ($this.val() == 'yes' || $this.val() == '1') {
                $clearCacheBtn.removeAttr('disabled');
            }
            else {
                $clearCacheBtn.attr('disabled', true);
            }
        });

        $clearCacheBtn.on('click', function() {

            var $this = $(this);
            $.ajax({
                method : 'get',
                data : {
                    action : 'apollo_syndication_clear_cache',
                    type: $this.attr('data-type'),
                    post_id: $this.attr('data-post-id')
                },
                url : apollo_admin_obj.ajax_url,
                beforeSend: function() {
                    $this.html($this.data().loading)
                }
            }).done(function(res){
                alert(res.msg)
                $this.html($this.data().text)
            });
        });

    });

    $(document).ready(function () {
        $('#frm-artist-syndication').off('click', '#artist-syn-query-submit').on('click', '#artist-syn-query-submit', function (e) {
            var current = $(e.currentTarget);
            var artistType = current.closest('.wrap-select-artist').find('#dropdown-artist-type');
            var artistMedium = current.closest('.wrap-select-artist').find('#dropdown-artist-medium');
            var url = window.location.search;
            var params = url.split( '&' );
            var redirectUrl = window.location.origin + window.location.pathname;
            if (params.length > 0) {
                var currentParams = [];
                $.each(params, function (index, item) {
                    if (item.indexOf('artist-type') > -1 || item.indexOf('artist-medium') > -1) {
                        return true;
                    }
                    currentParams.push(item);
                });
                currentParams.push('artist-type=' + artistType.val());
                currentParams.push('artist-medium=' + artistMedium.val());
                redirectUrl += currentParams.join('&');

            }
            current.attr('href', redirectUrl);
        });
        $('.wrap-select-artist').off('click', 'input#cb-select-all-1, input#cb-select-all-2').on('click', 'input#cb-select-all-1, input#cb-select-all-2', function(e){
            wpListTableSelectAllItems(e, '#apollo-syndication-artist-info', 'input[name="artist_selected_ids[]"]', '#_meta_artist_selected');
        });

        $('.wrap-select-org').off('click', 'input#cb-select-all-1, input#cb-select-all-2').on('click', 'input#cb-select-all-1, input#cb-select-all-2', function(e){
            wpListTableSelectAllItems(e, '#apollo-syndication-org-info', 'input[name="_org_selected_ids[]"]', '#_meta_org_selected');
        });
    });

    var wpListTableSelectAllItems = function(e, syndicationInfo, selectedIDs, selectedElement) {
        var current = $(e.currentTarget);
        var parent = current.closest(syndicationInfo);
        var listItems = parent.find(selectedIDs);
        var listSelectedEle = parent.find(selectedElement);
        if (listItems.length > 0) {
            var listCurrentItems = [];
            if (listSelectedEle.val() != '') {
                listCurrentItems = listSelectedEle.val().split(',');
            }
            $.each(listItems, function(index, item){
                item = $(item);
                var indexItem = listCurrentItems.indexOf(item.val());
                if ($(current).is(':checked')) {
                    if (indexItem < 0) {
                        listCurrentItems.push(item.val());
                    }
                } else {
                    if (indexItem > -1) {
                        listCurrentItems.splice(indexItem, 1);
                    }
                }
            });
            if (listCurrentItems.length > 0) {
                listSelectedEle.val(listCurrentItems.join(','));
            } else {
                listSelectedEle.val('');
            }
        }
    }


}) (jQuery);