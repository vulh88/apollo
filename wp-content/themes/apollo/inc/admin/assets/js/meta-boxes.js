jQuery(function($) {

    /*@ticket 17249 Octave Theme - [Spotlight] Add new button to the homepage spotlight - item 2*/
    if($('.spotlight-display-setting-background').length){
        $('.spotlight-display-setting-background').wpColorPicker () ;
    }

    var srollMore = $('[data-ride="scroll-more"]');
    var scrollLock = false;
    $.each(srollMore, function(i, v) {
        $(v).scroll(function() {

            if(!scrollLock && $(v).scrollTop() +  $(v).innerHeight()  >  this.scrollHeight - 50) {

                if ($(v).attr('data-has-more') != 'false') {
                    $.ajax({
                        method : 'get',
                        url: $(v).attr('data-action'),
                        beforeSend: function() {
                            scrollLock = true;
                            $(v).append($(v).attr('data-loading'));
                        },
                        success: function (res) {
                            $(v).append(res.html);
                            $(v).attr('data-action', res.action);
                            $(v).attr('data-has-more', res.hasMore);
                            $(v).removeClass('not-allow-click');
                            scrollLock = false;
                            $(v).find('.loading').remove();
                        }
                    });
                }
            }
        });
    });

    $('[data-ride="scroll-more"]').on('click', 'input[type=checkbox]', function() {

            var selected = $(this).closest('[data-ride="scroll-more"]').find('.hidden-meta'),
                selectedArr = selected.val().split(',');
            if (!$(this).is(':checked')) {
                var val = $(this).val();
                var result = selectedArr.filter(function(elem){
                    return elem != val;
                })
                selected.val(result.join(','));
            }
    });



    // TABS
    $('ul.apollo-tabs').show();
    $('div.panel-wrap').each(function(){
            $(this).find('div.panel:not(:first)').hide();
    });
    $('ul.apollo-tabs a').click(function(){
            var panel_wrap =  $(this).closest('div.panel-wrap');
            $('ul.apollo-tabs li', panel_wrap).removeClass('active');
            $(this).parent().addClass('active');
            $('div.panel', panel_wrap).hide();
            $( $(this).attr('href') ).show();
            return false;
    });
    
    /* Initialize date time picker*/
    aplDateTime = {
        dateFormat : 'yy-mm-dd',
        showOn: 'both', 
        buttonImage: apollo_admin_obj.calendar_url, 
        buttonImageOnly: true
    };
    var datetime = aplDateTime;
    
    var datepickerObjs = $('.apollo_input_datepicker');
    $.each(datepickerObjs, function(i, v) {
        $('#'+ $(v).attr('id')).datepicker( datetime );
    });
    
    if ( $('#_apollo_event_start_date').length ) {
        $('#_apollo_event_start_date').datepicker( datetime );
    }
    
    if ( $('#_apollo_event_end_date').length ) {
        $('#_apollo_event_end_date').datepicker( datetime );
    }
    
    if ( $('#_apl_program_start_date').length ) {
        $('#_apl_program_start_date').datepicker( datetime );
    }
    
    if ( $('#_apl_program_end_date').length ) {
        $('#_apl_program_end_date').datepicker( datetime );
    }

    if ( $('#_apl_program_expiration_date').length ) {
        $('#_apl_program_expiration_date').datepicker( datetime );
    }

     // Event gallery file uploads
    var $image_gallery_ids  = $('#event_image_gallery'),
        $event_images       = $('#event_images_container ul.event_images');
    //
     
    $('.add_event_images').on( 'click', 'a', handle_add_event_gallery);
    $('.add_public_art_icon').on( 'click', 'a', handle_add_public_art_map_icon);

    /**
     * Open specific attachment id
     * */

    /*
     * Edit by @Ticket 15149
     * TO DO: waiting the refactor - page builder
     */
    $('#event_images_container').on('click', '.image img', function() {
        var $this = $(this).closest('.image'),
            file_frame = wp.media.frames.file_frame = wp.media({
                id: 'multi_image',
                title: $this.data('title'),
                button: {
                    text: $this.data('text')
                },
                library: {
                    type: 'image', 
                    //post__in:ids
                },
                multiple: true,
            }); 

        file_frame.on('open',function(){

            var selection = file_frame.state().get('selection');
            var selected = $this.data().attachment_id; // the id of the image
            if (selected) {
                selection.add(wp.media.attachment(selected));
            }
        });

        file_frame.open();
    }); // End  * Open specific attachment id
    
    
    // Remove images
    $('#event_images_container').on( 'click', 'a.delete', delete_gallery_image );
    $('#apollo-public-art-gallery-map-icon').on( 'click', '.remove_google_map_icon', function(){
            $('.public_art_image_icon_content').empty();
            $('#public_art_google_icon').val();

            $('.remove_google_map_icon').hide();
            $('.add_public_art_icon').show();
    } );

    $('#apollo-home-spotlight-event-images').on( 'click', 'a', handle_add_home_spotlight_event_image );

    function handle_add_event_gallery( event ) {
        
        var $el = $(this),
            attachment_ids = $image_gallery_ids.val();

        event.preventDefault();

        var event_gallery_frame = apollo_open_media_frame( $el );

        // When an image is selected, run a callback.
        event_gallery_frame.on( 'select', function() {
            var selection = event_gallery_frame.state().get('selection');

            selection.map( function( attachment ) {
                attachment = attachment.toJSON();
                if ( attachment.id ) {
                    attachment_ids = attachment_ids ? attachment_ids + "," + attachment.id : attachment.id;

                    //@Ticket 15085
                    $html_select_tags = '';
                    if($("#container-select-tags-field").length > 0) {
                        $html_select_tags = $("#container-select-tags-field").html();
                        $input_tag_name = $("#container-select-tags-field").data('tag-name');
                        if ($html_select_tags !== 'undefined') {
                            $html_select_tags = $html_select_tags.replace(/input-select-tag-template/gi, $input_tag_name + "[" + attachment.id + "]");
                        }
                    }
                    $event_images.append('\
                        <li class="image" data-attachment_id="' + attachment.id + '">\
                                <img src="' + attachment.url + '" />\
                                <ul class="actions">\
                                        <li><a href="#" class="delete" title="' + $el.data('delete') + '">' + $el.data('text') + '</a></li>\
                                </ul>' + $html_select_tags + '\
                        </li>');
                }
            });
            
             $image_gallery_ids.val( attachment_ids );
            
        });
        
    }
    
    function handle_add_home_spotlight_event_image( event ) {
        var $el = $(this);
        
        event.preventDefault();
        var media_frame = apollo_open_media_frame( $el );
        
        media_frame.on( 'select', function () {
            var selection = media_frame.state().get( 'selection' );
            selection.map(function( attachment ) {
                attachment = attachment.toJSON();
                var image_container = $( '#apollo-home-spotlight-event-images .image-container' );
              
                if ( attachment.id ) {
                    image_container.html('<img src="' + attachment.url + '" />');
                    $( '#_apollo_event_home_spotlight_image' ).val( attachment.id );
                }
            });
        });
    }
    
    function apollo_open_media_frame( $el ) {
        
        // Create the media frame.
        var media_frame = wp.media.frames.event_gallery = wp.media({
            // Set the title of the modal.
            title: $el.data('choose'),
            button: {
                text: $el.data('update')
            },
            states : [
                new wp.media.controller.Library({
                    title: $el.data('choose'),
                    filterable : 'all',
                    multiple: true
                })
            ]
        });
        
        // Finally, open the modal.
        media_frame.open();
        
        return media_frame;
    }
    
    function delete_gallery_image() {
        
        $(this).closest('li.image').remove();

        var attachment_ids = '';

        $('#event_images_container ul li.image').css('cursor','default').each(function() {
                var attachment_id = jQuery(this).attr( 'data-attachment_id' );
                attachment_ids = attachment_ids + attachment_id + ',';
        });

        $image_gallery_ids.val( attachment_ids );

        return false;
        
    }

    function handle_add_public_art_map_icon( event ) {

        var $el = $(this),
            attachment_ids = $image_gallery_ids.val();

        event.preventDefault();

        var event_gallery_frame = apollo_open_media_frame( $el );

        // When an image is selected, run a callback.
        event_gallery_frame.on( 'select', function() {
            var selection = event_gallery_frame.state().get('selection');

            selection.map( function( attachment ) {
                attachment = attachment.toJSON();
                if ( attachment.id ) {
                    $('.public_art_image_icon_content').html('\
                        <a title="Set featured image" href="" class="thickbox">\
                            <img width="266" height="206" src="'+attachment.url+'" class="attachment-266x266" alt="classified-featured-admin-'+attachment.id+'"></a>');
                    $('#public_art_google_icon').val(attachment.id);
                    $('.remove_google_map_icon').show();
                    $('.add_public_art_icon').hide();

                }
            });
        });

    }

    // Ticket 14456 - Public artist
    var $wrapper = $('#apollo-artist-list-in-frm');
    $wrapper.on('click', 'input[type="checkbox"]', function () {
        var $this = $(this);
        var $selectedArtists = $wrapper.closest('div').find('#apl-checked-list-art');
        var selectedArr = $selectedArtists.val().split(',');
        var currentValue = $this.val();
        if ($(this).is(':checked')) {
            selectedArr.push(currentValue);
        }
        else {
            if(selectedArr.length > 0){
                var index = selectedArr.indexOf(currentValue);
                if(index > -1){
                    selectedArr.splice(index, 1);
                }else{
                    selectedArr.push(currentValue);
                }
            }
        }
        $selectedArtists.val(selectedArr.join(','));
    });

    /** @Ticket #14774 - For Artist event */
    var $eventWrapper = $('#apollo-event-artist-list-in-frm');
    $eventWrapper.on('click', 'input[type="checkbox"]', function () {
        var $this = $(this);
        var parent = $eventWrapper.closest('#artist-list');
        var checkedEle = parent.find('#apl-checked-list-art');
        var uncheckedEle = parent.find('#apl-unchecked-list-art');
        var listChecked = checkedEle.val();
        var listUnchecked = uncheckedEle.val();
        listChecked = listChecked != '' ? listChecked.split(',') : [];
        listUnchecked = listUnchecked != '' ? listUnchecked.split(',') : [];
        var currentValue = $this.val();
        if ($(this).is(':checked')) {
            listChecked.push(currentValue);
            if (listUnchecked.length > 0) {
                var indexUnchecked = listUnchecked.indexOf(currentValue);
                if(indexUnchecked > -1){
                    listUnchecked.splice(indexUnchecked, 1);
                }
            }
        } else {
            listUnchecked.push(currentValue);
            if (listChecked.length > 0) {
                var indexChecked = listChecked.indexOf(currentValue);
                if (indexChecked > -1) {
                    listChecked.splice(indexChecked, 1);
                }
            }
        }
        checkedEle.val(listChecked.length > 0 ? listChecked.join(',') : '');
        uncheckedEle.val(listUnchecked.length > 0 ? listUnchecked.join(',') : '');
    });

    /*
    * Vandd
    * Handle search artist
    **/
    $('#remove-search-text').off('click').on('click', handle_search_artist);
    $('#search-text, #search-artists-for-event').off('keyup').on('keyup', handle_show_icon_remove_text_search);
    function handle_show_icon_remove_text_search(e){
        e.preventDefault();
        var current = $(e.currentTarget);
        var iconRemove = current.closest('.group-search').find('#remove-search-text');
        if(iconRemove.length > 0){
            if(current.val() != ''){
                if(iconRemove.hasClass('hidden')){
                    iconRemove.removeClass('hidden');
                }
            }else{
                if(!iconRemove.hasClass('hidden')){
                    iconRemove.addClass('hidden');
                }
            }
            delaySearch(function(){
                handle_search_artist(e);
            }, 500);
        }
    }

    var delaySearch = (function(){
        var timer = 0;
        return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();

    function handle_search_artist(e){
        e.preventDefault();
        var current = $(e.currentTarget);
        if ($('#search-artists-for-event').length > 0) {
            var inputSearch = current.closest('.group-search').find('#search-artists-for-event');
        } else {
            var inputSearch = current.closest('.group-search').find('#search-text');
        }
        if(inputSearch.length > 0 ){
            if(current.hasClass('remove-search') && inputSearch.val() != ''){
                inputSearch.val('');
                if(!current.hasClass('hidden')){
                    current.addClass('hidden');
                }
            }

            var artistElement = current.closest('#artist-list');
            if(artistElement.length > 0){
                var postID = artistElement.attr('post_id');
                var _module =  $('#search-artists-for-event').length > 0 ? 'event' : 'public-art';

                $.ajax({
                    url: APL.ajax_url,
                    type: 'GET',
                    dataType: 'JSON',
                    data:{
                        action : 'apollo_get_more_artists',
                        post_id: postID,
                        text_search: inputSearch.val(),
                        module: _module,
                    },
                    beforeSend: function () {
                        jQuery(window).block(jQuery.apl.blockUI);
                    },
                    success: function (response) {
                        if(inputSearch) {
                            $("#apollo-artist-list-in-frm").html(response.html);
                            /*@ticket: 17450*/
                            $("#apollo-event-artist-list-in-frm").html(response.html);
                        } else {
                            $("#apollo-artist-list-in-frm").append(response.html);
                            /*@ticket: 17450*/
                            $("#apollo-event-artist-list-in-frm").append(response.html);
                        }
                        if(response.have_more) {
                            $(".wrap-box-load-more-artist").removeClass('hidden');
                            $(".wrap-box-load-more-artist a").css('display', 'block').data('sourcedata', response.url);
                        } else {
                            $(".wrap-box-load-more-artist").addClass('hidden');
                        }

                        jQuery(window).unblock();
                    }
                });
            }
        }
    }

    /*
     * Handle artist module 
     **/
    $('#artist-typediv h3 span').html( $( '#artist-typediv h3 span' ).html()+ '<span class="re-star">&nbsp;*</span>' );

    $('#apl-get-lat-long').off('click').on('click', function (e) {
        var current = $(e.currentTarget);
        var address = $('[name="' + current.data('address-id') + '"]').val();
        /** address 2 for venue/org module */
        var address2 = $('[name="' + current.data('address2-id') + '"]').val();
        var state = $('[name="' + current.data('state-id') + '"]').val();
        var city = $('[name="' + current.data('city-id') + '"]').val();
        var zip = $('[name="' + current.data('zip-id') + '"]').val();
        var longitude = $('[name="' + current.data('longitude-id') + '"]').val();
        var latitude = $('[name="' + current.data('latitude-id') + '"]').val();

        address = typeof address != 'undefined' ? address : '';
        address2 = typeof address2 != 'undefined' ? address2 : '';
        address = address == '' ? address2 : address;
        var fullAddress = [];
        if (address != '') {
            fullAddress.push(address);
        }
        if (typeof city != 'undefined' && city != '' && city != '0') {
            fullAddress.push(city);
        }
        if (typeof state != 'undefined' && state != '' && state != '0') {
            if (typeof zip != 'undefined' && zip != '' && zip != '0') {
                state += ' ' + zip
            }
            fullAddress.push(state);
        }
        if (fullAddress.length > 0) {
            address = fullAddress.join(',');
        }
        var browserKey = current.data('google-api-key');
        if (address == '' && (longitude == '' || latitude == '')) {
            return false;
        }
        var wrapMap = current.closest('.options_group').find('#apl-admin-google-map');
        if (wrapMap.length > 0) {
            wrapMap.removeClass('hidden');
        }
        var latElement = current.closest('.inside').find('[name="' + current.data('latitude-id') + '"]');
        var lngElement = current.closest('.inside').find('[name="' + current.data('longitude-id') + '"]');

        $.getScript('https://maps.googleapis.com/maps/api/js?key=' + browserKey, function() {
            geocoder = new google.maps.Geocoder();
            var mapOptions = {
                zoom: 16
            };
            map = new google.maps.Map(document.getElementById('apl-admin-google-map'), mapOptions);
            var latLng = {};
            if (address == '') {
                latLng = {lat:parseFloat(latitude), lng:parseFloat(longitude)};
                map.setCenter(latLng);
                aplAdminSetMarker(map, latLng, latElement, lngElement);
            } else {
                geocoder.geocode( { 'address': address}, function(results, status) {
                    if (status == 'OK') {
                        /** Set data */
                        if (latElement.length > 0) {
                            latElement.val(results[0].geometry.location.lat());
                        }
                        if (lngElement.length > 0) {
                            lngElement.val(results[0].geometry.location.lng());
                        }
                        map.setCenter(results[0].geometry.location);
                        latLng = results[0].geometry.location;
                        aplAdminSetMarker(map, latLng, latElement, lngElement);
                    } else {
                        alert('Geocode was not successful for the following reason: ' + status);
                    }
                });
            }

        });
    });

    var aplAdminSetMarker = function (map, latLng, latElement, lngElement) {
        var marker = new google.maps.Marker({
            map: map,
            position: latLng,
            draggable:true
        });
        marker.addListener('dragend', function (e) {
            if ($(latElement).length > 0) {
                $(latElement).val(e.latLng.lat());
            }
            if ($(lngElement).length > 0) {
                $(lngElement).val(e.latLng.lng());
            }
        });
    }

    var customSyndicationSubmitForm = function (e) {
        var parent = $(e.currentTarget);
        var postType = parent.find('#post_type');
        if (postType.length > 0 && postType.val() == 'syndication') {
            /** Orgs */
            var listOrgCheckbox = parent.find('input[name="meta-orgs[]"]:checked');
            var orgChecked = [];
            if (listOrgCheckbox.length > 0) {
                $.each(listOrgCheckbox, function (index, item) {
                    item = $(item);
                    orgChecked.push(item.val());
                });
            }
            listOrgCheckbox.attr('checked', false);
            var orgInput = '<input name="current-meta-orgs-checked" type="hidden" value="'+ (orgChecked.length > 0 ? orgChecked.join(',') : '') +'" />';
            postType.closest('form').append(orgInput);

            /** Venues */
            var listVenueCheckbox = parent.find('input[name="meta-venues[]"]:checked');
            var venueChecked = [];
            if (listVenueCheckbox.length > 0) {
                $.each(listVenueCheckbox, function (index, item) {
                    item = $(item);
                    venueChecked.push(item.val());
                });
            }
            listVenueCheckbox.attr('checked', false);
            var venueInput = '<input name="current-meta-venues-checked" type="hidden" value="'+ (venueChecked.length > 0 ? venueChecked.join(',') : '') +'" />';
            postType.closest('form').append(venueInput);
        }
    };


    /**
     * @ticket #18621
     * Custom Field is multi checkbox has arrow to collapse expanded.
     * The custom field auto expanded if it have element selected
     */
    function expandCustomField(){
        var toggleClass = $('.wp-admin [data-toggle-class]');
        if(toggleClass.length) {
            toggleClass.each(function () {
                if ($(this).closest('.options_group').find('ul input[type="checkbox"]:checked').length) {
                    $(this).addClass('show');
                }
            });
        }
    }

    function loadWpListTable(wrapper, postType, data) {
        var eleBlockUI = $(wrapper).closest('.postbox');
        var postId = $('input[name="post_ID"]').val();

        $.ajax({
            url: APL.ajax_url,
            type: 'GET',
            dataType: 'JSON',
            data: $.extend(
                {
                    action : "apollo_ajax_display_list_table",
                    post_type : postType,
                    post_id: postId,
                },
                data
            ),
            beforeSend: function () {
                jQuery(eleBlockUI).block(jQuery.apl.blockUI);
            },
            success: function (response) {
                $(wrapper).html(response);
                /** fix duplicate _wpnonce */
                $(wrapper).find('#_wpnonce').remove();
                wpListTableAction(wrapper, postType);
                $(eleBlockUI).unblock();
            }
        });
    }

    function wpListTableAction(wrapper, postType) {
        /** Sort actions */
        $(wrapper).off('click', '.tablenav-pages a').on('click', '.tablenav-pages a', function(e){
            handleWpListTableAction(e, wrapper, postType);
        });
        $(wrapper).off('click', '.manage-column.sorted a').on('click', '.manage-column.sorted a', function(e){
            handleWpListTableAction(e, wrapper, postType);
        });
        $(wrapper).off('click', '.manage-column.sortable a').on('click', '.manage-column.sortable a', function(e){
            handleWpListTableAction(e, wrapper, postType);
        });

        /** Pagination actions */
        $(wrapper).off('keydown', 'input[name=paged]').on('keydown', 'input[name=paged]', function (e) {

            if (13 == e.keyCode) {
                e.preventDefault();
                e.stopPropagation();
                var current = $(e.currentTarget);
                var nextPage =  $(wrapper).find('.pagination-links .next-page');
                var prevPage =  $(wrapper).find('.pagination-links .prev-page');
                var query = '';
                if (nextPage.length > 0) {
                    query = nextPage.attr('href');
                } else {
                    query = prevPage.attr('href');
                }
                var data = formatDataWpListTable(query, postType);
                data.paged = parseInt(current.find('input[name=paged]').val()) || '1';
                loadWpListTable(wrapper, postType, data);
                return false;
            }
        });

        /** Filter actions */
        $(wrapper).off('click', '#apl-ajax-wp-list-table-filter').on('click', '#apl-ajax-wp-list-table-filter', function(e){
            e.preventDefault();
            var artistType = $(wrapper).find('#dropdown-artist-type');
            var artistMedium = $(wrapper).find('#dropdown-artist-medium');
            var data = {
                "artist-type": artistType.val(),
                "artist-medium": artistMedium.val()
            };
            loadWpListTable(wrapper, postType, data);
        });

        $(wrapper).off('click', '#apl-ajax-wp-list-table-reset').on('click', '#apl-ajax-wp-list-table-reset', function(e){
            e.preventDefault();
            loadWpListTable(wrapper, 'syndication-artist', {});
        });

        /** Search actions*/
        $(wrapper).off('click', '#apl-ajax-wp-list-table-search').on('click', '#apl-ajax-wp-list-table-search', function(e){
            e.preventDefault();
            var keyword = $(wrapper).find('#syndication-module-search-box');
            if (keyword.length > 0) {
                var data = {
                    s: keyword.val()
                };
                loadWpListTable(wrapper, postType, data);
            }
        });

        $(wrapper).off('keydown', '#syndication-module-search-box').on('keydown', '#syndication-module-search-box', function(e){
            if (13 == e.keyCode) {
                $(wrapper).find('#apl-ajax-wp-list-table-search').trigger('click');
                return false;
            }
        });

        /** Select all */
        $('#apollo-syndication-artist-info input:radio[name="_meta_artist_all"]').off('change').on('change', function() {
            disableWpListTable($(this).val(), wrapper);
        });

        /** Select all */
        $('#apollo-syndication-org-info input:radio[name="_meta_org_all"]').off('change').on('change', function() {
            disableWpListTable($(this).val(), wrapper);
        });

        /** List table checkbox action */
        $(wrapper).off('click', 'input[name="artist_selected_ids[]"]').on('click', 'input[name="artist_selected_ids[]"]', function(e){
            getSelectedItemWpListTable(e, '#apollo-syndication-artist-info', '#_meta_artist_selected');
        });

        /** List table checkbox action */
        $(wrapper).off('click', 'input[name="_org_selected_ids[]"]').on('click', 'input[name="_org_selected_ids[]"]', function(e) {
            getSelectedItemWpListTable(e, '#apollo-syndication-org-info', '#_meta_org_selected');
        });
    }

    function handleWpListTableAction(e, wrapper, postType) {
        e.preventDefault();
        var current = $(e.currentTarget);
        var query = current.attr('href');
        var data = formatDataWpListTable(query, postType);
        loadWpListTable(wrapper, postType, data);
    }

    function formatDataWpListTable(query, postType) {

        return {
            paged: getParamsByUrl( query, 'paged' ) || '1',
            order: getParamsByUrl( query, 'order' ) || 'asc',
            orderby: getParamsByUrl( query, 'orderby' ) || 'last_name',
            s: getParamsByUrl( query, 's' ) || '',
            post_type: postType,
            "artist-type": getParamsByUrl( query, 'artist-type' ) || '',
            "artist-medium": getParamsByUrl( query, 'artist-type' ) || ''
        };
    }

    function getParamsByUrl (query, variable) {
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable)
                return pair[1];
        }
        return false;
    }

    function expendSyndiactionMenu() {
        var parentMenu = $('#menu-posts-syndication');
        if (parentMenu.length > 0) {
            parentMenu.addClass('wp-menu-open');
            parentMenu.addClass('wp-has-current-submenu');
            parentMenu.removeClass('wp-not-current-submenu');
        }
    }

    function disableWpListTable(value, wrapper) {
        if (value == 1) {
            $(wrapper).addClass('disable-content');
        } else {
            $(wrapper).removeClass('disable-content');
        }
    }

    /** List table checkbox action */
    function getSelectedItemWpListTable(e, syndicationInfo, listSelectedIDs){
        var current = $(e.currentTarget);
        var listSelectedEle = $(syndicationInfo).find(listSelectedIDs);
        if (listSelectedEle.length > 0) {
            var listSelected = listSelectedEle.val();
            if (listSelected == '') {
                if (current.is(':checked')) {
                    listSelectedEle.val(current.val());
                }
            } else {
                listSelected = listSelected.split(',');
                var indexItem = listSelected.indexOf(current.val());
                if (current.is(':checked')) {
                    if (indexItem < 0) {
                        listSelected.push(current.val());
                    }
                } else {
                    if (indexItem > -1) {
                        listSelected.splice(indexItem, 1);
                    }
                }
                listSelectedEle.val(listSelected.join(','));
            }
        }
    }

    $(document).ready(function(){
        var frame;
        var current;
        $('.inside').off('click', '.apl-select-from-library').on('click', '.apl-select-from-library', function (e) {
            e.preventDefault();
            current = $(e.currentTarget);
            if (frame) {
                frame.open();
                return;
            }

            frame = wp.media({
                title: current.data('media-popup-title'),
                button: {
                    text: current.data('media-button-title')
                },
                library: {
                    type: 'application/pdf'
                },
                multiple: false
            });
            frame.on( 'select', function() {
                var attachment = frame.state().get('selection').first().toJSON();



                var parent = current.closest('.upload_pdf_embed_field');
                var choseFromDesktopData = parent.find('input[data-ride="upload_file"]');
                var inputFileName = parent.find('.apl-pdf-file-name');
                var choseFromMediaData = parent.find('.apl-choose-pdf-from-media');
                if (choseFromDesktopData.length > 0) {
                    choseFromDesktopData.val('');
                }
                if (inputFileName.length > 0) {
                    inputFileName.html(attachment.title);
                }
                if (choseFromMediaData.length > 0) {
                    choseFromMediaData.val(attachment.id);
                }
                /** Update description */
                var descFromUpload = parent.closest('.apollo-artist-upload_pdf').find('.wpc-75');
                var descFromMedia = parent.closest('.apollo-artist-upload_pdf').find('.apl-desc-pdf-from-media');
                if (descFromUpload.length > 0) {
                    descFromUpload.val('');
                    descFromUpload.addClass('hidden');
                }
                if (descFromMedia.length > 0) {
                    descFromMedia.val(attachment.description);
                    descFromMedia.removeClass('hidden');
                }



            });
            frame.open();
        });
        /** @Ticket #17193 */
        $('.post-type-syndication').submit(function (e) {
            customSyndicationSubmitForm(e);
        });

        /*@ticket #18221: 0002504: Arts Education Customizations - Add a checkbox option for "Add Menu Arrow" - item 1*/
        $(document).on('click', '[data-toggle-class]', function (params) {
            var className = $(this).data('toggle-class');
            $(this).toggleClass(className);
        });

        expandCustomField();

        var saListTable = $('#apollo-syndication-artist-info .wrap-select-artist');
        if (saListTable.length > 0) {
            var data = formatDataWpListTable('', 'syndication-artist');
            loadWpListTable(saListTable, 'syndication-artist', data);
            expendSyndiactionMenu();
        }

        var synOrgListTable = $('#apollo-syndication-org-info .wrap-select-org');
        if (synOrgListTable.length > 0) {
            var data = formatDataWpListTable("order=asc&orderby=post_title", 'syndication-org');
            loadWpListTable(synOrgListTable, 'syndication-org', data);
            expendSyndiactionMenu();
        }
    });

});



