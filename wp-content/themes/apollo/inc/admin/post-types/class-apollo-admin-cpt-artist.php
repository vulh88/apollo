<?php
/**
 * Admin functions for the Artist type
 *
 * @author 		vulh
 * @category 	admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Apollo_Admin_CPT' ) ) {
    include( 'class-apollo-admin-cpt.php' );
}

if ( ! class_exists( 'Apollo_Admin_Artist' ) ) :
    
class Apollo_Admin_Artist extends Apollo_Admin_CPT {
    
    public function __construct() {
        global $typenow, $pagenow, $wpdb;
      
        $this->type             = Apollo_DB_Schema::_ARTIST_PT;
        $this->meta_tbl  = $wpdb->{Apollo_Tables::_APOLLO_ARTIST_META};
        $this->post_term_tbl   = $wpdb->{Apollo_Tables::_POST_TERM};
        $this->associationUserModuleListComp = apl_instance('APL_Lib_Association_Components_AssociationUsersModuleList');
        $this->associationAgencyModuleComp = apl_instance('APL_Lib_Association_Components_AssociationAgenciesModuleList');

        $this->requireds = array(

            ''.Apollo_DB_Schema::_APL_ARTIST_DATA.'['.Apollo_DB_Schema::_APL_ARTIST_LNAME.']' => array( 
                'label'     => __( 'The last name', 'apollo' ), 
                'validate'  => 'required' 
            ),

            ''.Apollo_DB_Schema::_APL_ARTIST_DATA.'['.Apollo_DB_Schema::_APL_ARTIST_EMAIL.']' => array( 
                'label'     => __( 'The email', 'apollo' ), 
                'validate'  => 'email' 
            ),
            ''.Apollo_DB_Schema::_APL_ARTIST_DATA.'['.Apollo_DB_Schema::_APL_ARTIST_BLOGURL.']' => array( 
                'label'     => __( 'The blog url', 'apollo' ), 
                'validate'  => 'url' 
            ),
            ''.Apollo_DB_Schema::_APL_ARTIST_DATA.'['.Apollo_DB_Schema::_APL_ARTIST_WEBURL.']' => array( 
                'label'     => __( 'The website', 'apollo' ), 
                'validate'  => 'url' 
            ),

            ''.Apollo_DB_Schema::_APL_ARTIST_DATA.'['.Apollo_DB_Schema::_APL_ARTIST_FB.']' => array( 
                'label'     => __( 'The facebook url', 'apollo' ), 
                'validate'  => 'url' 
            ),
            ''.Apollo_DB_Schema::_APL_ARTIST_DATA.'['.Apollo_DB_Schema::_APL_ARTIST_LK.']' => array( 
                'label'     => __( 'The linkedin url', 'apollo' ), 
                'validate'  => 'url' 
            ),
            ''.Apollo_DB_Schema::_APL_ARTIST_DATA.'['.Apollo_DB_Schema::_APL_ARTIST_TW.']' => array( 
                'label'     => __( 'The blog url', 'apollo' ), 
                'validate'  => 'url' 
            ),
            ''.Apollo_DB_Schema::_APL_ARTIST_DATA.'['.Apollo_DB_Schema::_APL_ARTIST_INS.']' => array( 
                'label'     => __( 'The instagram url', 'apollo' ), 
                'validate'  => 'url' 
            ),
            ''.Apollo_DB_Schema::_APL_ARTIST_DATA.'['.Apollo_DB_Schema::_APL_ARTIST_PR.']' => array( 
                'label'     => __( 'The pinterest url', 'apollo' ), 
                'validate'  => 'url' 
            ),
            ''.Apollo_DB_Schema::_APL_ARTIST_ADDRESS.'['.Apollo_DB_Schema::_APL_ARTIST_COUNTRY.']' => array( 
                'label'     => __( 'The county', 'apollo' ), 
                'validate'  => 'required' 
            ),
            'post_cat'  => array(
                'label' => __( 'The category', 'apollo' ),
                'validate'  => 'required',
            ),
            'youtube_link'  => array(
                'label' => __( 'The youtube link', 'apollo' ),
                'validate'  => 'url',
            ),
            'audio_embed[]'  => array(
                'label' => __( 'The Audio Embed', 'apollo' ),
                'validate'  => 'audio_embed',
            ),

        ); 

        // Admin column
        add_filter( 'manage_edit-'.$this->type.'_columns', array( $this, 'edit_columns' ) );
        add_action( 'manage_'.$this->type.'_posts_custom_column', array( $this, 'custom_columns' ), 2 );
        add_filter( 'manage_edit-'.$this->type.'_sortable_columns', array( $this, 'custom_columns_sort' ) );
        
        // Bulk / Quick edit
        /** @Ticket #14398 */
        /** @Ticket #14443 */
        add_action( 'quick_edit_custom_box', array( $this, 'quick_edit' ), 10, 2 );
        add_action( 'save_post', array( $this, 'bulk_and_quick_edit_save_post' ), 10, 2 );
        
        /// Event filtering
        add_action( 'restrict_manage_posts', array( $this, '_filters' ) );
        
        $this->meta_tbl  = $wpdb->{Apollo_Tables::_APOLLO_ARTIST_META};

        // Update query
        if ( $typenow == $this->type && $pagenow == 'edit.php' 
            && ( ( isset( $_GET['post_status'] ) && $_GET['post_status'] == 'publish' )  ) || ! isset( $_GET['post_status'] )
            && $_GET['orderby'] != 'title' && $_GET['orderby'] != 'date') {
            add_filter( 'posts_join' , array( $this, 'filter_meta_join' ) );
            add_filter( 'posts_where', array( $this, 'posts_where' ) );
            add_filter( 'posts_orderby', array( $this, 'change_order_by' ) );  
        }
        
        add_filter( 'wp_insert_post_data', array( $this, 're_update_slug' ), 99, 2 );

        /** @Ticket #13028 */
        add_filter('posts_clauses', array( $this, 'custom_query_posts' ) );
        
        parent::__construct();
    }
    
    public function re_update_slug( $data, $postarr ) {
        
        if ( ! isset( $_POST[Apollo_DB_Schema::_APL_ARTIST_DATA] ) ) return $data;
        
        $artist_data = $_POST[Apollo_DB_Schema::_APL_ARTIST_DATA];
        if ( !in_array( $data['post_status'], array( 'draft', 'pending', 'auto-draft' ) ) ) {
          $data['post_name'] = wp_unique_post_slug( sanitize_title( $artist_data[Apollo_DB_Schema::_APL_ARTIST_FNAME]. ' '. $artist_data[Apollo_DB_Schema::_APL_ARTIST_LNAME] ), $postarr['ID'], $data['post_status'], $data['post_type'], $data['post_parent'] );
        }
        return $data;
    }
    
    /**
     * Custom post join 
     * 
     * @access public
     * @return string
     */
    public function filter_meta_join( $join ) {
        global $wpdb;
        
        $join = str_replace( 'post_id', 'apollo_artist_id', $this->replace_meta_table( $join ) );
        
        // Join to filter event not exprise
        $join .= "
            LEFT JOIN {$this->meta_tbl} mt ON mt.apollo_artist_id = ". $wpdb->posts .".ID AND mt.meta_key = '". Apollo_DB_Schema::_APL_ARTIST_LNAME ."'    
        ";
        
        return $join;
    }
    
    /**
     * Replace meta table 
     * 
     * @access public
     * @return string
     */
    public function replace_meta_table( $str ) {
        global $wpdb;
        return str_replace( $wpdb->postmeta, $wpdb->apollo_artistmeta, $str);
    }
    
    /**
     * Custom post where 
     * 
     * @access public
     * @return string
     */
    public function posts_where( $where ) {
        return $this->replace_meta_table( $where );
    }
    
    /**
     * Change order by
     * 
     * @access public
     * @return string
     */
    public function change_order_by( $str ) {
        
        $str = " mt.meta_value ASC";
        
        return $str;
    }
    
    public function _filters() {
        global $typenow;
        
        if ( $typenow !== $this->type ) {
            return ;
        }
        // show export
        echo '<a id="artist_timeline_popup" href="javascript:void(0);" class="button action">' . __('Export to Artist Timeline') .'</a>';

        // show drop down
        apollo_dropdown_categories( $this->type. '-type' );
    }
    
     /**
     * Custom Quick Edit form
     * 
     * @access public
     * @param mixed $column_type
	 * @param mixed $post_type
    */
    public function quick_edit( $column_type, $post_type ) {
        
        if ( $column_type == 'name' ) {
            require_once APOLLO_ADMIN_DIR. '/views/'.$this->type.'/html-quick-edit.php';
        }
    }

    public function bulk_edit_save( $post_id, $venue ) {
        
    }
    
    /**
     * Quick and Bulk saving
     * 
     * @access public
     * @param mixed $post_id
     * @param mixed $post
     */
    public function bulk_and_quick_edit_save_post( $post_id, $post ) {
       
        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}
        
        // Don't save revisions and autosaves
		if ( wp_is_post_revision( $post_id ) || wp_is_post_autosave( $post_id ) ) {
			return $post_id;
		}
        
        if ( $post->post_type != $this->type ) {
            return $post_id;
        }
        
        // Check user permission
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return $post_id;
		}
        
        $nonce = 'apollo_'.$this->type.'_bulk_edit_nonce';
        if ( isset( $_REQUEST[$nonce] ) && ! wp_verify_nonce( $_REQUEST[$nonce], $nonce ) ) {
			return $post_id;
		}
        
        $post = get_venue( $post );
        
        if ( ! empty( $_REQUEST['apollo_'.$this->type.'_quick_edit'] ) ) {
           
			$this->quick_edit_save( $post_id, $post );
		} else {
			$this->bulk_edit_save( $post_id, $post );
		}

		return $post_id;
    }
    
    /**
	 * Change the columns shown in admin.
	 */
	public function edit_columns( $existing_columns ) {
       
		if ( empty( $existing_columns ) && ! is_array( $existing_columns ) ) {
            $existing_columns = array();
        }

		unset( $existing_columns['title'], $existing_columns['comments'], $existing_columns['date'] );

		$columns = array();
		$columns["cb"] = "<input type=\"checkbox\" />";
		$columns["name"] = __( 'Title', 'apollo' );
        $columns["city"] = __( 'City', 'apollo' );
        $columns["state"] = __( 'State', 'apollo' );
        $columns["zip"] = __( 'Zip', 'apollo' );
        $columns["artist-type"] = __( 'Artist Category(s)', 'apollo' );
        $columns['username'] = __( 'Username', 'apollo' );

        if(Apollo_App::is_avaiable_module(Apollo_DB_Schema::_AGENCY_PT)) {
            $columns['agency'] = __( 'Agency', 'apollo' );
        }

        $columns["date"] = __( 'Posted date', 'apollo' );

        return array_merge( $columns, $existing_columns );
	}
    
    /**
	 * Define our custom columns shown in admin.
	 * @param  string $column
	 */
	public function custom_columns( $column ) {
        global $post, $the_venue;
        
        if ( empty( $the_venue ) || $the_venue->id != $post->ID ) {
            
            $the_venue = get_venue( $post );
        }
       
        
        switch ( $column ) {
            
            case "name" :
				$edit_link = get_edit_post_link( $post->ID );
				
                $fname = Apollo_App::apollo_get_meta_data( $the_venue->id, Apollo_DB_Schema::_APL_ARTIST_FNAME, Apollo_DB_Schema::_APL_ARTIST_DATA );
                $lname = Apollo_App::apollo_get_meta_data( $the_venue->id, Apollo_DB_Schema::_APL_ARTIST_LNAME, Apollo_DB_Schema::_APL_ARTIST_DATA );
                $title = $fname. ' '. $lname;
				$post_type_object = get_post_type_object( $post->post_type );
				$can_edit_post = current_user_can( $post_type_object->cap->edit_post, $post->ID );

				echo '<strong><a class="row-title" href="' . esc_url( $edit_link ) .'">' . $title.'</a>';

				_post_states( $post );

				echo '</strong>';

				if ( $post->post_parent > 0 )
					echo '&nbsp;&nbsp;&larr; <a href="'. get_edit_post_link($post->post_parent) .'">'. get_the_title($post->post_parent) .'</a>';

				// Excerpt view
				if (isset($_GET['mode']) && $_GET['mode']=='excerpt') echo apply_filters('the_excerpt', $post->post_excerpt);

				// Get actions
				$actions = array();

				$actions['id'] = 'ID: ' . $post->ID;

				if ( $can_edit_post && 'trash' != $post->post_status ) {
					$actions['edit'] = '<a href="' . get_edit_post_link( $post->ID, true ) . '" title="' . esc_attr( __( 'Edit this item', 'apollo' ) ) . '">' . __( 'Edit', 'apollo' ) . '</a>';
					$actions['inline hide-if-no-js'] = '<a href="#" class="editinline" title="' . esc_attr( __( 'Edit this item inline', 'apollo' ) ) . '">' . __( 'Quick&nbsp;Edit', 'apollo' ) . '</a>';
				}
				if ( current_user_can( $post_type_object->cap->delete_post, $post->ID ) ) {
					if ( 'trash' == $post->post_status )
						$actions['untrash'] = "<a title='" . esc_attr( __( 'Restore this item from the Trash', 'apollo' ) ) . "' href='" . wp_nonce_url( admin_url( sprintf( $post_type_object->_edit_link . '&amp;action=untrash', $post->ID ) ), 'untrash-post_' . $post->ID ) . "'>" . __( 'Restore', 'apollo' ) . "</a>";
					elseif ( EMPTY_TRASH_DAYS )
						$actions['trash'] = "<a class='submitdelete' title='" . esc_attr( __( 'Move this item to the Trash', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID ) . "'>" . __( 'Trash', 'apollo' ) . "</a>";
					if ( 'trash' == $post->post_status || !EMPTY_TRASH_DAYS )
						$actions['delete'] = "<a class='submitdelete' title='" . esc_attr( __( 'Delete this item permanently', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID, '', true ) . "'>" . __( 'Delete Permanently', 'apollo' ) . "</a>";
				}
				if ( $post_type_object->public ) {
					if ( in_array( $post->post_status, array( 'pending', 'draft', 'future' ) ) ) {
						if ( $can_edit_post )
							$actions['view'] = '<a href="' . esc_url( add_query_arg( 'preview', 'true', get_permalink( $post->ID ) ) ) . '" title="' . esc_attr( sprintf( __( 'Preview &#8220;%s&#8221;', 'apollo' ), $title ) ) . '" rel="permalink">' . __( 'Preview', 'apollo' ) . '</a>';
					} elseif ( 'trash' != $post->post_status ) {
						$actions['view'] = '<a href="' . get_permalink( $post->ID ) . '" title="' . esc_attr( sprintf( __( 'View &#8220;%s&#8221;', 'apollo' ), $title ) ) . '" rel="permalink">' . __( 'View', 'apollo' ) . '</a>';
					}
				}

				$actions = apply_filters( 'post_row_actions', $actions, $post );

				echo '<div class="row-actions">';

				$i = 0;
				$action_count = sizeof($actions);

				foreach ( $actions as $action => $link ) {
					++$i;
					( $i == $action_count ) ? $sep = '' : $sep = ' | ';
					echo "<span class='$action'>$link$sep</span>";
				}
				echo '</div>';
                get_inline_data( $post );
                
                $commuicate_js_str = '';
                $_a_data = array(
                    Apollo_DB_Schema::_APL_ARTIST_FNAME,
                    Apollo_DB_Schema::_APL_ARTIST_LNAME,
                    Apollo_DB_Schema::_APL_ARTIST_PHONE,
                    Apollo_DB_Schema::_APL_ARTIST_EMAIL,
                    Apollo_DB_Schema::_APL_ARTIST_BLOGURL,
                    Apollo_DB_Schema::_APL_ARTIST_WEBURL,
                    Apollo_DB_Schema::_APL_ARTIST_FB,
                    Apollo_DB_Schema::_APL_ARTIST_LK,
                    Apollo_DB_Schema::_APL_ARTIST_TW,
                    Apollo_DB_Schema::_APL_ARTIST_INS,
                    Apollo_DB_Schema::_APL_ARTIST_PR,
                );

                foreach ( $_a_data as $k ) {
                    $commuicate_js_str .= '<div class="'.Apollo_DB_Schema::_APL_ARTIST_DATA.'['.$k.']">' . esc_attr($the_venue->get_meta_data( Apollo_DB_Schema::_APL_ARTIST_DATA. '['. $k .']' )) . '</div>';
                }

				echo '
					<div class="hidden" id="_inline_' . $post->ID . '">
						<div class="menu_order">' . $post->menu_order . '</div>
						'.$commuicate_js_str.'
					</div>
				';
                
			break;
           
        
            case 'city':
                $val = Apollo_App::apollo_get_meta_data( $the_venue->id, Apollo_DB_Schema::_APL_ARTIST_CITY, Apollo_DB_Schema::_APL_ARTIST_ADDRESS );
                echo $val ? $val : '-';
            break;
        
            case 'state':
                $val = Apollo_App::apollo_get_meta_data( $the_venue->id, Apollo_DB_Schema::_APL_ARTIST_STATE, Apollo_DB_Schema::_APL_ARTIST_ADDRESS );
                echo $val ? $val : '-';
            break;
        
            case 'zip':
                $val = Apollo_App::apollo_get_meta_data( $the_venue->id, Apollo_DB_Schema::_APL_ARTIST_ZIP, Apollo_DB_Schema::_APL_ARTIST_ADDRESS );
                echo $val ? $val : '-';
            break;
            case "artist-type" :
                
                $terms = get_the_terms( $post->ID, $column );
                $cat_name = Apollo_App::apollo_get_meta_data( $the_venue->id, Apollo_DB_Schema::_APL_ARTIST_ANOTHER_CAT, Apollo_DB_Schema::_APL_ARTIST_DATA );
         
                $termlist = array();
                
                if ( $terms ):
                    foreach ( $terms as $term ) {
                        if ( $term->parent ) continue;  // Not display sub menu in the list event page

                        $termlist[] = '<a href="' . admin_url( 'edit.php?' . $column . '=' . $term->slug . '&post_type='.$this->type.'' ) . ' ">' . $term->name . '</a>';
                    }
                endif;
                
                if ( $cat_name ) $termlist[] = $cat_name;
                
                echo $termlist ? implode( ', ', $termlist ) : '<span class="na">&ndash;</span>';

			break;
            
            case 'username':
                $this->associationUserModuleListComp->render(array(
                    'id' => $the_venue->id,
                    'post_type' => 'artist',
                ));
            break;

            case 'agency':
                $this->associationAgencyModuleComp->render(array(
                    'id' => $the_venue->id,
                    'post_type' => 'artist',
                ));
                break;
        }
        
    }
    
    /**
	 * Make columns sortable
	 *
	 * @access public
	 * @param mixed $columns
	 * @return array
	 */
	public function custom_columns_sort( $columns ) {
		$custom = array(
		    'name'  => 'title',
            'city'  => Apollo_DB_Schema::_APL_ARTIST_CITY,
            'state' => Apollo_DB_Schema::_APL_ARTIST_STATE,
            'zip'   => Apollo_DB_Schema::_APL_ARTIST_ZIP
		);
        if (!Apollo_App::is_avaiable_module(Apollo_DB_Schema::_AGENCY_PT)) {
            $custom['username'] = 'username';
        }
		return wp_parse_args( $custom, $columns );
	}
    
    private function quick_edit_save( $post_id, $post ) {
        $artist = get_artist( $post );

        $data = Apollo_App::unserialize($artist->get_meta_data(Apollo_DB_Schema::_APL_ARTIST_DATA));
        $input = $_POST[Apollo_DB_Schema::_APL_ARTIST_DATA];

        $data = $this->setQuickEditData($input, $data);

        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_ARTIST_LNAME, $data[Apollo_DB_Schema::_APL_ARTIST_LNAME] );
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_ARTIST_DATA, serialize( Apollo_App::clean_array_data($data) ) );
      
        do_action( 'apollo_'.$this->type.'_quick_edit_save', $post );
    }

    public function custom_query_posts($clauses) {
	    global $wpdb;
        if(!empty($_GET['orderby'])) {
            /** @Ticket #13028*/
            $order = !empty($_GET['order']) ? $_GET['order'] : 'ASC';
            if ($_GET['orderby'] == Apollo_DB_Schema::_APL_ARTIST_CITY
                || $_GET['orderby'] == Apollo_DB_Schema::_APL_ARTIST_STATE
                || $_GET['orderby'] == Apollo_DB_Schema::_APL_ARTIST_ZIP ) {

                $clauses['join'] = " LEFT JOIN " . $this->meta_tbl . " AS mt ON mt.apollo_artist_id=".$wpdb->posts.".ID AND mt.meta_key='" . $_GET['orderby'] . "' ";
                $clauses['orderby'] = " mt.meta_value " . $order;
            }

            /** @Ticket #13029 */
            if($_GET['orderby'] == 'username') {
                $clauses['join'] = "LEFT JOIN " . $this->meta_tbl . " AS mt ON mt.apollo_artist_id=".$wpdb->posts.".ID AND mt.meta_key='" . Apollo_DB_Schema::_APL_ARTIST_ASSOCIATION_USER_NICENAME . "'";
                $clauses['orderby'] = " mt.meta_value " . $order;
            }
        }
        return $clauses;
    }
    
}    
    
endif;    

new Apollo_Admin_Artist();


