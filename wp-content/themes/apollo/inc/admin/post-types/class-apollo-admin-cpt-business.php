<?php
/**
 * Admin functions for the Artist type
 *
 * @author 		vulh
 * @category 	admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Apollo_Admin_CPT' ) ) {
    include( 'class-apollo-admin-cpt.php' );
}

if ( ! class_exists( 'Apollo_Admin_Business' ) ) :

    class Apollo_Admin_Business extends Apollo_Admin_CPT {

        public function __construct() {
            global $typenow, $pagenow, $wpdb;

            $this->type             = Apollo_DB_Schema::_BUSINESS_PT;

            $this->requireds = array(

                'post_title'    => array(
                    'label'     => __( 'The name', 'apollo' ),
                    'validate'  => 'required',
                )
            );

            // Admin column
            add_filter( 'manage_edit-'.$this->type.'_columns', array( $this, 'edit_columns' ) );
            add_action( 'manage_'.$this->type.'_posts_custom_column', array( $this, 'custom_columns' ), 2 );
            add_filter( 'manage_edit-'.$this->type.'_sortable_columns', array( $this, 'custom_columns_sort' ) );


            /// Event filtering
            add_action( 'restrict_manage_posts', array( $this, '_filters' ) );

            // remove move to trash in bulk action
            add_filter( 'bulk_actions-edit-business', array( $this, 'remove_trash_bulk_actions'));

            if ( $typenow == $this->type && $pagenow == 'edit.php' ) {
                add_filter( 'posts_where', array( $this, 'posts_where' ) );
            }

            add_action( 'delete_post', array( $this, 'delete_post' ) );

            parent::__construct();
        }

        public  function remove_trash_bulk_actions( $actions ){
            //unset( $actions[ 'trash' ] );
            return $actions;
        }

        public function delete_post( $post_id ) {
            $orgID = get_apollo_meta($post_id, Apollo_DB_Schema::_APL_BUSINESS_ORG, true);
            if (!empty($orgID)) {
                $meta = maybe_unserialize(Apollo_App::apollo_get_meta_data($orgID, Apollo_DB_Schema::_APL_ORG_DATA));
                if (isset($meta[Apollo_DB_Schema::_APL_ORG_BUSINESS])) {
                    $meta[Apollo_DB_Schema::_APL_ORG_BUSINESS] = '';
                }
                if (isset($meta[Apollo_DB_Schema::_ORG_BUSINESS])) {
                    $meta[Apollo_DB_Schema::_ORG_BUSINESS] = '';
                }
                delete_apollo_meta($orgID, Apollo_DB_Schema::_APL_ORG_BUSINESS, $meta[Apollo_DB_Schema::_APL_ORG_BUSINESS]);
                delete_apollo_meta($orgID, Apollo_DB_Schema::_ORG_BUSINESS, $meta[Apollo_DB_Schema::_ORG_BUSINESS]);
                update_apollo_meta($orgID, Apollo_DB_Schema::_APL_ORG_DATA, serialize($meta));
            }
        }

        public function posts_where( $where ) {

           // global $wpdb;

            
            return $where;
        }

        public function _filters() {
            global $typenow;

            if ( $typenow !== $this->type ) {
                return ;
            }
            // show drop down
            apollo_dropdown_categories( $this->type. '-type' );

        }

        /**
         * Change the columns shown in admin.
         */
        public function edit_columns( $existing_columns ) {

            if ( empty( $existing_columns ) && ! is_array( $existing_columns ) ) {
                $existing_columns = array();
            }

            unset( $existing_columns['title'], $existing_columns['comments'], $existing_columns['date'] );

            $columns = array();
            $columns["cb"] = "<input type=\"checkbox\" />";
            $columns["name"] = __( 'Name', 'apollo' );

            if ( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ORGANIZATION_PT ) ) {
                $columns["org"] = __( 'Associated Organization', 'apollo' );
            } else if( isset( $columns["org"] ) ) {
                unset( $columns["org"] );
            }
            if ( !isset( $_REQUEST['dm'] ) ) {
                $columns["date"] = __( 'Date', 'apollo' );
            }

            return array_merge( $columns, $existing_columns );
        }

        /**
         * Define our custom columns shown in admin.
         * @param  string $column
         */
        public function custom_columns( $column ) {
            global $post, $the_business;

            if ( empty( $the_business ) || $the_business->id != $post->ID ) {

                $the_business = get_business( $post );
            }

            switch ( $column ) {

                case "name" :
                    $edit_link = get_edit_post_link( $post->ID );
                    $title = _draft_or_post_title();
                    $post_type_object = get_post_type_object( $post->post_type );
                    $can_edit_post = current_user_can( $post_type_object->cap->edit_post, $post->ID );

                    echo '<strong><a class="row-title" href="' . esc_url( $edit_link ) .'">' . $title.'</a>';

                    _post_states( $post );

                    echo '</strong>';

                    if ( $post->post_parent > 0 )
                        echo '&nbsp;&nbsp;&larr; <a href="'. get_edit_post_link($post->post_parent) .'">'. get_the_title($post->post_parent) .'</a>';

                    // Excerpt view
                    if (isset($_GET['mode']) && $_GET['mode']=='excerpt') echo apply_filters('the_excerpt', $post->post_excerpt);

                    // Get actions
                    $actions = array();

                    $actions['id'] = 'ID: ' . $post->ID;

                    if ( $can_edit_post && 'trash' != $post->post_status ) {
                        $actions['edit'] = '<a href="' . get_edit_post_link( $post->ID, true ) . '" title="' . esc_attr( __( 'Edit this item', 'apollo' ) ) . '">' . __( 'Edit', 'apollo' ) . '</a>';
                        $actions['inline hide-if-no-js'] = '<a href="#" class="editinline" title="' . esc_attr( __( 'Edit this item inline', 'apollo' ) ) . '">' . __( 'Quick&nbsp;Edit', 'apollo' ) . '</a>';
                    }
                    if ( $post_type_object->public ) {
                        if ( in_array( $post->post_status, array( 'pending', 'draft', 'future' ) ) ) {
                            if ( $can_edit_post )
                                $actions['view'] = '<a href="' . esc_url( add_query_arg( 'preview', 'true', get_permalink( $post->ID ) ) ) . '" title="' . esc_attr( sprintf( __( 'Preview &#8220;%s&#8221;', 'apollo' ), $title ) ) . '" rel="permalink">' . __( 'Preview', 'apollo' ) . '</a>';
                            $actions['trash'] = "<a class='submitdelete' title='" . esc_attr( __( 'Move this item to the Trash', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID ) . "'>" . __( 'Trash', 'apollo' ) . "</a>";

                        }
                        elseif ($post->post_status == 'trash'){
                            $actions['untrash'] = "<a title='" . esc_attr( __( 'Restore this item from the Trash', 'apollo' ) ) . "' href='" . wp_nonce_url( admin_url( sprintf( $post_type_object->_edit_link . '&amp;action=untrash', $post->ID ) ), 'untrash-post_' . $post->ID ) . "'>" . __( 'Restore', 'apollo' ) . "</a>";
                            $actions['delete'] = "<a class='submitdelete' title='" . esc_attr( __( 'Delete this item permanently', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID, '', true ) . "'>" . __( 'Delete Permanently', 'apollo' ) . "</a>";
                        }
                        elseif ( 'trash' != $post->post_status ) {
                            $actions['view'] = '<a href="' . get_permalink( $post->ID ) . '" title="' . esc_attr( sprintf( __( 'View &#8220;%s&#8221;', 'apollo' ), $title ) ) . '" rel="permalink">' . __( 'View', 'apollo' ) . '</a>';
                            $actions['trash'] = "<a class='submitdelete' title='" . esc_attr( __( 'Move this item to the Trash', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID ) . "'>" . __( 'Trash', 'apollo' ) . "</a>";
                        }
                    }

                    $actions = apply_filters( 'post_row_actions', $actions, $post );

                    echo '<div class="row-actions">';

                    $i = 0;
                    $action_count = sizeof($actions);

                    foreach ( $actions as $action => $link ) {
                        ++$i;
                        ( $i == $action_count ) ? $sep = '' : $sep = ' | ';
                        echo "<span class='$action'>$link$sep</span>";
                    }
                    echo '</div>';
                    get_inline_data( $post );
                    $commuicate_js_str = '';

                    echo '
					<div class="hidden" id="_inline_' . $post->ID . '">
						<div class="menu_order">' . $post->menu_order . '</div>
						'.$commuicate_js_str.'
					</div>
				';
                    break;
                case 'org':
                    $org = $the_business->get_org();
                    echo $org ? '<a href="'.get_edit_post_link($org) .'">'.$org->post_title.'</a>' : '--';
                    break;
            }
        }

        /**
         * Make columns sortable
         *
         * @access public
         * @param mixed $columns
         * @return array
         */
        public function custom_columns_sort( $columns ) {
            $custom = array(
                'name'  => 'name',
                'org'   => 'org'
            );
            return wp_parse_args( $custom, $columns );
        }

    }

endif;

new Apollo_Admin_Business();
