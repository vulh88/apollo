<?php
/**
 * Admin functions for the events post type
 *
 * @author 		vulh
 * @category 	admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Apollo_Admin_CPT' ) ) {
    include( 'class-apollo-admin-cpt.php' );
}

if ( ! class_exists( 'Apollo_Admin_Event' ) ) :

/**
 * Apollo_Admin_Event Class
 */
class Apollo_Admin_Event extends Apollo_Admin_CPT {
    private $themeToolClass, $term = false;
    /**
	 * Constructor
	 */
	public function __construct() {

        global $typenow, $wpdb, $pagenow;

        $this->requireds = array(
            'post_title' => array(
                'label'     => __( 'The Event Name', 'apollo' ),
                'validate'  => 'required'
            ),

            ''.Apollo_DB_Schema::_APOLLO_EVENT_DATA.'['.Apollo_DB_Schema::_E_CONTACT_NAME.']' => array(
                'label'     => __( 'The Contact Name', 'apollo' ),
                'validate'  => 'required'
            ),

            ''.Apollo_DB_Schema::_APOLLO_EVENT_DATA.'['.Apollo_DB_Schema::_E_CONTACT_EMAIL.']' => array(
                'label'     => __( 'The Contact Email', 'apollo' ),
                'validate'  => 'required,email'
            ),
            ''.Apollo_DB_Schema::_APOLLO_EVENT_START_DATE.'' => array(
                'label'     => __( 'The Start Date', 'apollo' ),
                'validate'  => 'required'
            ),
            ''.Apollo_DB_Schema::_APOLLO_EVENT_END_DATE.'' => array(
                'label'     => __( 'The End Date', 'apollo' ),
                //'validate'  => 'required,greater'
                'validate'  => 'required'
            ),
            ''.Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID.''  => array(
                'label'     => __( 'The Primary category', 'apollo' ),
                'validate'  => 'required'
            ),

            ''.Apollo_DB_Schema::_APL_EVENT_TMP_VENUE.'['.Apollo_DB_Schema::_VENUE_NAME.']'  => array(
                'label'     => __( 'The Venue Name', 'apollo' ),
                'validate'  => 'required'
            ),
            ''.Apollo_DB_Schema::_APOLLO_EVENT_VENUE.''  => array(
                'label'     => __( 'The Venue', 'apollo' ),
                'validate'  => 'required'
            ),
            ''.Apollo_DB_Schema::_APL_EVENT_TMP_VENUE.'['.Apollo_DB_Schema::_VENUE_WEBSITE_URL.']' => array(
                'label'     => __( 'The Website Venue URL', 'apollo' ),
                'validate'  => 'url'
            ),

            ''.Apollo_DB_Schema::_APOLLO_EVENT_ORGANIZATION.'0' => array(
                'label'     => __( 'The Register Organization', 'apollo' ),
                'validate'  => 'required'
            ),
            ''.Apollo_DB_Schema::_APL_EVENT_TMP_ORG => array(
                'label'     => __( 'The Temp Organization', 'apollo' ),
                'validate'  => 'required'
            ),

            ''.Apollo_DB_Schema::_APOLLO_EVENT_DATA.'['.Apollo_DB_Schema::_WEBSITE_URL.']' => array(
                'label'     => __( 'The Website Url', 'apollo' ),
                'validate'  => 'url'
            ),
            ''.Apollo_DB_Schema::_APOLLO_EVENT_DATA.'['.Apollo_DB_Schema::_ADMISSION_PHONE.']' => array(
                'label'     => __( 'The Admission Phone', 'apollo' ),
                'validate'  => 'required'
            ),

            'video_embed[]'  => array(
                'label' => __( 'The Youtube Link', 'apollo' ),
                'validate'  => 'url',
            ),

                ''.Apollo_DB_Schema::_APOLLO_EVENT_DATA.'['.Apollo_DB_Schema::_ADMISSION_TICKET_URL.']' => array(
                'label'     => __( 'The Ticket Url', 'apollo' ),
                'validate'  => 'url'
            ),
            ''.Apollo_DB_Schema::_APOLLO_EVENT_DATA.'['.Apollo_DB_Schema::_ADMISSION_TICKET_EMAIL.']' => array(
                'label'     => __( 'The Ticket Email', 'apollo' ),
                'validate'  => 'email'
            ),
            ''.Apollo_DB_Schema::_APOLLO_EVENT_DATA.'['.Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL.']' => array(
                'label'     => __( 'The Discount Url', 'apollo' ),
                'validate'  => 'url'
            ),
            ''.Apollo_DB_Schema::_APOLLO_EVENT_DATA.'['.Apollo_DB_Schema::_APOLLO_EVENT_DAYS_OF_WEEK.']' => array(
                'label'     => __( 'Day of Week', 'apollo' ),
                'validate'  => 'required'
            ),
            ''.Apollo_DB_Schema::_APOLLO_EVENT_DATA.'['.Apollo_DB_Schema::_APOLLO_EVENT_START_DATE.']' => array(
                'label'     => __( 'maximun year', 'apollo' ),
                'validate'  => 'maximum_year'
            ),
            ''.Apollo_DB_Schema::_APOLLO_EVENT_DATA.'['.Apollo_DB_Schema::_APOLLO_EVENT_END_DATE.']' => array(
                'label'     => __( 'maximun year', 'apollo' ),
                'validate'  => 'maximum_year'
            ),
        );

        $this->type             = Apollo_DB_Schema::_EVENT_PT;
        $this->meta_tbl  = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};
        $this->post_term_tbl   = $wpdb->{Apollo_Tables::_POST_TERM};

		// Post title fields
		add_filter( 'enter_title_here', array( $this, 'enter_title_here' ), 1, 2 );

        // Featured image text
		add_filter( 'media_view_strings', array( $this, 'media_view_strings' ), 10, 2 );

        // Admin column
        add_filter( 'manage_edit-event_columns', array( $this, 'edit_columns' ) );
        add_action( 'manage_event_posts_custom_column', array( $this, 'custom_columns' ), 2 );
        add_filter( 'manage_edit-event_sortable_columns', array( $this, 'custom_columns_sort' ) );
        add_filter( 'views_edit-event', array( $this, 'custom_views' ), 10, 1 );
        //add_filter( 'request', array( $this, 'custom_columns_orderby' ) );

        // Bulk / Quick edit
        /** @Ticket #14398 */
        /** @Ticket #14443 */
        add_action( 'quick_edit_custom_box', array( $this, 'quick_edit' ), 10, 2 );
        add_action( 'bulk_edit_custom_box', array( $this, 'bulk_edit' ), 10, 2 );
        add_action( 'save_post', array( $this, 'bulk_and_quick_edit_save_post' ), 10, 2 );

        //add_action( 'pre_get_posts', array ( $this, 'pre_get_posts' ) );

        // Event filtering
        add_action( 'restrict_manage_posts', array( $this, 'event_filters' ) );
        add_filter( 'parse_query', array( $this, 'event_filter_query' ) );


        $this->getThemeTool();

        // Change meta table

        $post_status_array = array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'review-edit', 'trash','all')  ;
        $status = ( ( isset( $_GET['post_status'] ) && in_array($_GET['post_status'], $post_status_array)) ) || isset( $_GET['dm'] ) || ! isset( $_GET['post_status'] );
        if ( $typenow == $this->type && $pagenow == 'edit.php' && $status ) {
            add_filter( 'posts_join' , array( $this, 'filter_meta_join' ) );
            add_filter( 'posts_orderby', array( $this, 'replace_meta_table' ) );
            add_filter( 'posts_groupby', array( $this, 'replace_meta_table' ) );
            add_filter( 'posts_where', array( $this, 'posts_where' ) );
            add_filter( 'posts_groupby', array( $this, 'change_group_by' ) );
            add_filter( 'posts_orderby', array( $this, 'change_order_by' ) );
            add_filter('posts_request', array($this,'dump_request'));
        }

        add_action( 'admin_head-edit.php', array( $this, 'move_custom_button' ) );

        add_action( 'admin_footer', array( $this, 'admin_footer' ) );

        add_action( 'post_submitbox_misc_actions', array($this,'copy_event_button') );

        add_action( 'post_submitbox_minor_actions', array($this,'rejectEventButton') );

        add_action( 'post_submitbox_misc_actions', array($this,'review_edit_button') );

        add_action('publish_event', array($this, 'send_email_to_owner'));

        add_action( 'delete_post', array( $this, 'delete_post' ) );

        add_filter( 'bulk_actions-edit-'.$this->type, array($this,'apl_event_register_bulk_actions'), 10, 1 );

        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_bootstrap' ) );

        /**
         * @ticket #19030: Add status "unconfirmed" for event
         */
        add_filter('display_post_states', array($this, 'display_post_states'), 10, 2);

        parent::__construct();
	}

    /**
     * @Ticket #15217 - Add Review list link
     * @param $views
     * @return array
     */
    public function custom_views ( $views ) {
        $result = array();
        if (!empty( $views )) {
            $draftView = isset($views['draft']) ? $views['draft'] : '';
            $unconfirmed = isset($views['unconfirmed']) ? $views['unconfirmed'] : '';
            unset($views['draft']);
            unset($views['private']);
            unset($views['unconfirmed']);
            $checkTrash = false;
            $customViews = $this->getCustomViews($draftView, $unconfirmed);
            foreach ( $views as $class => $view) {
                if ($class == 'trash') {
                    $result = array_merge($result, $customViews);
                    $checkTrash = true;
                }
                $result[$class] = $view;
            }
            if (!$checkTrash) {
                $result = array_merge($result, $customViews);
            }
        }

        return $result;
    }

    /**
     * @param $draftView
     * @return mixed
     */
    private function getCustomViews($draftView, $unconfirmed){
        $classReviewActive = (isset( $_GET['post_status'] ) && $_GET['post_status'] == 'review-edit') ? 'class="current"' : '';
        $classPrivateActive = (isset( $_GET['post_status'] ) && $_GET['post_status'] == 'private') ? 'class="current"' : '';

        /**
         * @ticket #19058:  Reorder display status of event in admin dashboard
         */
        if($unconfirmed){
            $result['unconfirmed'] = $unconfirmed;
        }

        if($draftView){
            $result['draft'] = $draftView;
        }

        $result['private'] = '<a href="edit.php?post_status=private&post_type=event" '.$classPrivateActive.'>'
            . __('Private', 'apollo') .'
                        <span class="count"></span>
                    </a>';
        $result['review-edit'] = '<a href="edit.php?post_status=review-edit&post_type=event" '.$classReviewActive.'>'
            . __('Review Edit', 'apollo') .'
                        <span class="count"></span>
                    </a>';
        return $result;
    }

    /**
     * Add action reviewed to bulk actions.
     * @param $bulk_actions
     * @return mixed
     */
    public function apl_event_register_bulk_actions($bulk_actions) {

        $bulk_actions['apl_reviewed'] = __( 'Reviewed', 'apollo');
        return $bulk_actions;
    }

    public function delete_post( $post_id ) {
        if(empty($post_id)) return;
        global $wpdb;
        $apl_query = new Apl_Query( $wpdb->{Apollo_Tables::_APL_ARTIST_EVENT} , TRUE );
        $apl_query->delete( " event_id = " . $post_id . " " );
    }

    public function dump_request($input)
    {
        $debugContentType = isset($_GET['is_debugging']);
        if($debugContentType){
            aplDebug($input,1);
        }
        return $input;
    }

    /**
     * Send an email to the owner of published event
     */
    public function send_email_to_owner()
    {
        global $post;

        $event = get_event($post);
        if ($event->get_meta_data(Apollo_DB_Schema::_E_CONFIRM)) return false;

        $email = $event->get_meta_data(Apollo_DB_Schema::_E_CONTACT_EMAIL, Apollo_DB_Schema::_APOLLO_EVENT_DATA);
        $contactName = $event->get_meta_data(Apollo_DB_Schema::_E_CONTACT_NAME, Apollo_DB_Schema::_APOLLO_EVENT_DATA);
        if (!$email) return false;

        update_apollo_meta($post->ID, Apollo_DB_Schema::_E_CONFIRM, 1);

        do_action( 'apollo_email_published_new_event', $contactName, $event->get_title(), $event->get_permalink(), $email );
    }

    public function copy_event_button(){
        global $post;
        $html = '';
        if ( isset($_GET['action']) && $_GET['action'] == 'edit' ){
            $copyLink = $this->copy_event_post_link($post->ID,'display',false);
            $html  = '<div id="major-publishing-actions" style="overflow:hidden">';
            $html .= '<div id="publishing-action">';
            $html .= '<a href="'.$copyLink.'" class="button-primary" id="copy_event" target="_blank">' .__("Copy Event","apollo"). '</a>';
            $html .= '</div>';
            $html .= '</div>';
        }
        echo $html;
    }

    public function rejectEventButton(){
        global $post;
        $html = '';
        if ( isset($_GET['action']) && $_GET['action'] == 'edit' && isset($post->post_status) && in_array($post->post_status, array('publish', 'pending'))){
            $html  = '<div id="reject-actions" class=" '. ($post->post_status == 'pending' ? 'event-pending' : 'event-publish') .'" >';
            $html .= '<a  id="apl-reject-event" class="button" data-toggle="modal" data-target="#apl-reject-event-modal" >'.__('Reject','apollo').'</a>';
            $html .= '</div>';
            $html .= $this->renderBootstrapModal($post->ID);

        }
        echo $html;
    }

    /**
     * @Ticket #15863
     * Render bootstrap modal confirm reject event
     * @param $postID
     * @return string
     */
    public function renderBootstrapModal($postID) {
        $confirmMessage = __('This event will be moved to Trash. Are you sure you want to reject it?', 'apollo');

        $defaultReason = of_get_option('use_default_reason', true);
        $reasons = [];

        /** @Ticket #16062 - Manage reason list of the Reject button */
        if (!$defaultReason){

            $reasonsText = of_get_option('reject_reasons_text', false);

            if ($reasonsText){

                $reasonsText = explode("\n", $reasonsText);
                foreach($reasonsText as $reason){
                    $reasons[] = __($reason, 'apollo');
                }
            }
        }

        if(!count($reasons)){
            $reasons = AplEventFunction::getDefaultRejectReasons();
        }

        $html = '<div id="apl-reject-event-modal" class="modal fade" tabindex="-1">';
            $html .= '<div class="modal-dialog">';
                $html .= '<div class="modal-content">';
                    $html .= '<div class="modal-header">';
                        $html .= '<h4 class="modal-title">'.$confirmMessage.'</h4>';
                    $html .= '</div>';
                    $html .= '<div class="modal-body">';
                        $html .= '<div class="apl-wrap-reasons">';
                        foreach ($reasons as $key => $value) {
                            $html .= '<div class="apl-reason-item">';
                            $html .= '<input type="checkbox" name="reject-reasons[]" value="'.$value.'"/>';
                            $html .= '<span>'.$value.'</span>';
                            $html .= '</div>';
                        }
                        $html .= '</div>';
                        $html .= '<div class="apl-wrap-reason-detail">';
                        $html .= '<textarea class="apl-reason-detail" rows="5" data-limit="500" maxlength="500"  
                                data-message="'.__('characters remaining', 'apollo').'" placeholder="'.__('Specific details about the reason', 'apollo').'"></textarea>';
                        $html .= '<span class="apl-reason-detail-count">'.__('500 characters remaining', 'apollo').'</span>';
                        $html .= '</div>';
                    $html .= '</div>';
                    $html .= '<div class="modal-footer">';
                        $html .= '<a id="apl-accept-reject" class="btn button button-primary button-large" data-post-id="'.$postID.'">Ok</a>';
                        $html .= '<a class="btn button btn-secondary button-large" data-dismiss="modal">Cancel</a>';
                    $html .= '</div>';
                $html .= '</div>';
            $html .= '</div>';
        $html .= '</div>';

        return $html;
    }

    /**
     * enqueue bootstrap script
     */
    public function enqueue_bootstrap() {
        wp_register_style( 'apl_admin_bootstrap_modal_css', APOLLO_ADMIN_CSS_URL . '/bootstrap/mini-bootstrap-modal.css', false, APOLLO_ADMIN_CSS_VERSION );
        wp_enqueue_style( 'apl_admin_bootstrap_modal_css' );
        wp_enqueue_script( 'bootstrap_modal_js', APOLLO_ADMIN_JS_URL . '/bootstrap/bootstrap-modal.min.js', array(), APOLLO_ADMIN_JS_VERSION );
    }

    /**
     * @Ticket #15217 - review button for event detail page
     */
    public function review_edit_button(){
        global $post;
        $html = '';
        $listEventReview = get_option(Apollo_DB_Schema::_APL_EVENT_REVIEW_EDIT, '');
        if (!empty($listEventReview)) {
            $listEventReview = explode(",", $listEventReview);
            if (in_array($post->ID, $listEventReview)) {
                if ( isset($_GET['action']) && $_GET['action'] == 'edit' ){
                    $html  = '<div id="reviewed-actions" class="reviewed" style="overflow:hidden">';
                    $html .= '<div id="reviewed-action">';
                    $html .= '<a href="#" 
                    class="button-primary apl-reviewed-edit " 
                    data-post-id="'.$post->ID.'" 
                    data-location="detail-page" 
                    id="review-edit" 
                    target="_blank">' .__("Reviewed","apollo"). '</a>';
                    $html .= '</div>';
                    $html .= '</div>';
                }
            }
        }

        echo $html;
    }


    /**
     * This function calls the creation of a new copy of the selected post (as a draft)
     * then redirects to the edit post screen
     */
    public function copy_event_post_save_as_new_post_draft(){
        $this->copy_event_post_save_as_new_post('draft');
    }

    /**
     * This function calls the creation of a new copy of the selected post (by default preserving the original publish status)
     * then redirects to the post list
     */
    public function copy_event_post_save_as_new_post($status = 'pending'){
        if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'copy_event_post_save_as_new_post' == $_REQUEST['action'] ) ) ) {
            wp_die(__('No post to copy has been supplied!', "apollo"));
        }

        // Get the original post
        $id = (isset($_GET['post']) ? $_GET['post'] : $_POST['post']);
        $post = get_post($id);

        // Copy the post and insert it
        if (isset($post) && $post!=null) {
            $new_id = $this->copy_event_create_copied_post($post, $status);

            if (intval($new_id) === 0 || $new_id === false){
                // Redirect to the post list screen
                wp_redirect( admin_url( 'edit.php?post_type='.$post->post_type) );
            } else {
                // Redirect to the edit screen for the new draft post
                wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_id ) );
            }
            exit;

        } else {
            $post_type_obj = get_post_type_object( $post->post_type );
            wp_die(esc_attr(__('Copy creation failed, could not find original:', "apollo")) . ' ' . htmlspecialchars($id));
        }
    }

    public function copy_event_create_copied_post($post, $status = ''){
        try{
            $newEventID = $this->copy_event_insert_post_data($post, $status);
            if($newEventID !== false && intval($newEventID) > 0){
                // clone all event post meta data
                $this->copy_event_update_event_post_meta_data($post, $newEventID);
                return $newEventID;
            }
        }catch (Exception $ex){
            aplDebugFile($ex->getMessage(), 'Admin Event Copied Feature : copy_event_create_copied_post');
        }
        return 0;
    }

    public function copy_event_insert_post_data($post, $status = 'pending'){
        // We don't want to clone revisions
        if ($post->post_type == 'revision' || $post->post_type != Apollo_DB_Schema::_EVENT_PT) return false;

        $prefix = __("Copy Event");
        $suffix = __("(*)");
        if (!empty($prefix)) $prefix.= " ";
        if (!empty($suffix)) $suffix = " ".$suffix;
        $status = empty($status) ? 'pending' : $status;
        $new_post_author = wp_get_current_user();

        $new_post = array(
            'menu_order' => $post->menu_order,
            'comment_status' => $post->comment_status,
            'ping_status' => $post->ping_status,
            'post_author' => $new_post_author->ID,
            'post_content' => $post->post_content,
            'post_excerpt' => $post->post_excerpt,
            'post_mime_type' => $post->post_mime_type,
            'post_parent' => $new_post_parent = empty($parent_id)? $post->post_parent : $parent_id,
            'post_password' => $post->post_password,
            'post_status' => $new_post_status = (empty($status))? $post->post_status: $status,
            'post_title' => $prefix.$post->post_title.$suffix,
            'post_type' => $post->post_type,
        );

        $new_post_id = wp_insert_post($new_post);

        // If the copy is published or scheduled, we have to set a proper slug.
        if ($new_post_status == 'publish' || $new_post_status == 'future'){
            $post_name = wp_unique_post_slug($post->post_name, $new_post_id, $new_post_status, $post->post_type, $new_post_parent);

            $new_post = array();
            $new_post['ID'] = $new_post_id;
            $new_post['post_name'] = $post_name;

            // Update the post into the database
            wp_update_post( $new_post );
        }

        delete_post_meta($new_post_id, '_dp_original');
        add_post_meta($new_post_id, '_dp_original', $post->ID);

        return $new_post_id;
    }

    public function copy_event_update_event_post_meta_data($post, $copied_post_id){
        if($copied_post_id !== false && intval($copied_post_id) > 0){
            // clone wp_post_meta
            $this->_clone_event_post_meta_data($post, $copied_post_id);
            // clone wp_apollo_event_meta
            $this->_clone_event_meta_data($post, $copied_post_id);
            // clone wp_apollo_event_org
            $this->_clone_event_org_data($post, $copied_post_id);
            // clone wp_apollo_event_calendar
            $this->_clone_event_calendar_data($post, $copied_post_id);
            // clone event category
            $this->_clone_event_categories_data($post, $copied_post_id);
        }
    }

    /**
     * Copy the meta information of a post to another post
     * clone another meta key which is using as wordpress default
     */
    public function _clone_event_post_meta_data($post, $copied_post_id) {
        try{
            $post_meta_keys = get_post_custom_keys($post->ID);
            if (empty($post_meta_keys)) return;
            foreach ($post_meta_keys as $meta_key) {
                $meta_values = get_post_custom_values($meta_key, $post->ID);
                foreach ($meta_values as $meta_value) {
                    $meta_value = maybe_unserialize($meta_value);
                    add_post_meta($copied_post_id, $meta_key, $meta_value);
                }
            }
        }catch (Exception $ex){
            aplDebugFile($ex->getMessage(),"_clone_event_post_meta_data error at event id copied is " . $copied_post_id);
        }
    }

    private function _clone_event_meta_data($post, $copied_post_id){
        try{
            $aqlQuery = new Apl_Query(Apollo_Tables::_APOLLO_EVENT_META);
            $metaData = $aqlQuery->get_where(" apollo_event_id = " . $post->ID);
            if(!empty($metaData)){
                foreach( $metaData as $mt ) {
                    $aqlQuery->insert(array(
                        'apollo_event_id' => $copied_post_id,
                        'meta_key' => $mt->meta_key,
                        'meta_value' => $mt->meta_value
                    ));
                }
            }
        }catch (Exception $ex){
            aplDebugFile($ex->getMessage(),"_clone_event_meta_data error at event id copied is " . $copied_post_id);
        }
    }

    private function _clone_event_org_data($post, $copied_post_id){
        try{
            $aqlQuery = new Apl_Query(Apollo_Tables::_APL_EVENT_ORG);
            $metaData = $aqlQuery->get_where(" post_id = " . $post->ID);
            if(!empty($metaData)){
                foreach( $metaData as $mt ) {
                    $aqlQuery->insert(array(
                        'org_id' => $mt->org_id,
                        'org_ordering' => $mt->org_ordering,
                        'is_main' => $mt->is_main,
                        'post_id' => $copied_post_id
                    ));
                }
            }
        }catch (Exception $ex){
            aplDebugFile($ex->getMessage(),"_clone_event_org_data error at event id copied is " . $copied_post_id);
        }
    }

    private function _clone_event_calendar_data($post, $copied_post_id){
        try{
            $aqlQuery = new Apl_Query(Apollo_Tables::_APL_EVENT_CALENDAR);
            $metaData = $aqlQuery->get_where(" event_id = " . $post->ID);
            if(!empty($metaData)){
                foreach( $metaData as $mt ) {
                    $aqlQuery->insert(array(
                        'event_id' => $copied_post_id,
                        'date_event' => $mt->date_event,
                        'time_from' => $mt->time_from,
                        'time_to' => $mt->time_to,
                        'ticket_url' => $mt->ticket_url,
                        'booth_override' => $mt->booth_override
                    ));
                }
            }
        }catch (Exception $ex){
            aplDebugFile($ex->getMessage(),"_clone_event_calendar_data error at event id copied is " . $copied_post_id);
        }
    }

    private function _clone_event_categories_data($post, $copied_post_id){
        try{
            global $wpdb;
            if (isset($wpdb->terms)) {
                // Clear default category (added by wp_insert_post)
                wp_set_object_terms( $copied_post_id, NULL, 'category' );
                $post_taxonomies = get_object_taxonomies($post->post_type);
                $taxonomies = $post_taxonomies;
                foreach ($taxonomies as $taxonomy) {
                    $post_terms = wp_get_object_terms($post->ID, $taxonomy, array( 'orderby' => 'term_order' ));
                    $terms = array();
                    for ($i=0; $i<count($post_terms); $i++) {
                        $terms[] = $post_terms[$i]->slug;
                    }
                    wp_set_object_terms($copied_post_id, $terms, $taxonomy);
                }
            }
        }catch (Exception $ex){
            aplDebugFile($ex->getMessage(),"_clone_event_categories_data error at event id copied is " . $copied_post_id);
        }
    }

    /**
     * Retrieve duplicate post link for post.
     *
     *
     * @param int $id Optional. Post ID.
     * @param string $context Optional, default to display. How to write the '&', defaults to '&amp;'.
     * @param string $draft Optional, default to true
     * @return string
     */
    private function copy_event_post_link( $id = 0, $context = 'display', $draft = true ) {
        if ( !$post = get_post( $id ) )
            return site_url('/404');

        if ($draft)
            $action_name = "copy_event_post_save_as_new_post_draft";
        else
            $action_name = "copy_event_post_save_as_new_post";

        if ( 'display' == $context )
            $action = '?action='.$action_name.'&amp;post='.$post->ID;
        else
            $action = '?action='.$action_name.'&post='.$post->ID;

        $post_type_object = get_post_type_object( $post->post_type );
        if ( !$post_type_object )
            return site_url('/404');

        return admin_url( "admin.php". $action );
    }

    /**
    * Get theme tool data
    */
    function getThemeTool() {
        if ( $this->_is_cat_display_management_page() ) {
            $this->term = get_term_by('slug', $_REQUEST['event-type'], 'event-type');
            if ( $this->term ) {
                // Initialize the theme tool class
                $this->themeToolClass = new Apollo_Theme_Tool($this->term->term_id);
            }
        }
    }

    public function admin_footer() {
        ?>
        <script type="text/javascript">
            jQuery(document).ready( function($)
            {
                $( '#_apl_event_term_primary_id' ).addClass( 'required' );
            });
        </script>
        <?php
    }

    public function custom_columns_orderby( $vars ) {

        if (isset( $vars['orderby'] )) :

			if ( 'start_date' == $vars['orderby'] ) :
				$vars = array_merge( $vars, array(
					'meta_key' 	=> Apollo_DB_Schema::_APOLLO_EVENT_START_DATE,
					'orderby' 	=> 'meta_value'
				) );
			endif;
			if ( 'end_date' == $vars['orderby'] ) :
				$vars = array_merge( $vars, array(
					'meta_key' 	=> Apollo_DB_Schema::_APOLLO_EVENT_END_DATE,
					'orderby' 	=> 'meta_value'
				) );
			endif;
		endif;

		return $vars;
    }

    /**
     * Order event
     * @param object $query
     */
    public function pre_get_posts( $query ) {

        if ( !is_admin() ) {
            return;
        }

        $post_type = $query->get('post_type');

        if ( $post_type == 'event' )  {

            $query->set( 'orderby', 'meta_value' );
            $query->set( 'meta_key', Apollo_DB_Schema::_APOLLO_EVENT_START_DATE );

            $query->set( 'order', of_get_option( '_admin_event_order' ) ? of_get_option( '_admin_event_order' ) : DEFAULT_ORDER_START_DATE );
        }
    }

	/**
	 * Change title boxes in admin.
	 * @param  string $text
	 * @param  object $post
	 * @return string
	 */
	public function enter_title_here( $text, $post ) {
		if ( $post->post_type == 'event' ) {
            return __( 'Event name', 'apollo' );
        }

		return $text;
	}

    /**
	 * Define our custom columns shown in admin.
	 * @param  string $column
	 */
	public function custom_columns( $column ) {
        global $post, $the_event;

        if ( empty( $the_event ) || $the_event->id != $post->ID ) {

            $the_event = get_event( $post );
        }

        switch ( $column ) {

            case "D" :
                $this->render_duplicate_icon($post, 'evt');
            break;

            case "thumb" :
                echo '<a href="' . get_edit_post_link( $post->ID ) . '">' . $the_event->get_image() . '</a>';
			break;

            case "name" :
				$edit_link = get_edit_post_link( $post->ID );
				$title = _draft_or_post_title();
				$post_type_object = get_post_type_object( $post->post_type );
				$can_edit_post = current_user_can( $post_type_object->cap->edit_post, $post->ID );
                $userDraft = '';
                if ($post->post_status == 'draft') {
                    $userDraft = $the_event->getMetaInMainData(Apollo_DB_Schema::_E_USER_DRAFT);
                    $userDraft = ' - ' . (!empty($userDraft) ?  __('[SUBSCRIBER DRAFT]', 'apollo') : __('[ADMIN DRAFT]', 'apollo'));
                }

				echo '<strong><a class="row-title" href="' . esc_url( $edit_link ) .'">' . $title . $userDraft .'</a>';

				_post_states( $post );

				echo '</strong>';

				if ( $post->post_parent > 0 )
					echo '&nbsp;&nbsp;&larr; <a href="'. get_edit_post_link($post->post_parent) .'">'. get_the_title($post->post_parent) .'</a>';

				// Excerpt view
				if (isset($_GET['mode']) && $_GET['mode']=='excerpt'){
                    echo apply_filters('the_excerpt', $post->post_excerpt);
                }

				// Get actions
				$actions = array();

				$actions['id'] = 'ID: ' . $post->ID;

				if ( $can_edit_post && 'trash' != $post->post_status ) {
					$actions['edit'] = '<a href="' . get_edit_post_link( $post->ID, true ) . '" title="' . esc_attr( __( 'Edit this item', 'apollo' ) ) . '">' . __( 'Edit', 'apollo' ) . '</a>';
					$actions['inline hide-if-no-js'] = '<a href="#" class="editinline" title="' . esc_attr( __( 'Edit this item inline', 'apollo' ) ) . '">' . __( 'Quick&nbsp;Edit', 'apollo' ) . '</a>';
				}
				if ( current_user_can( $post_type_object->cap->delete_post, $post->ID ) ) {
					if ( 'trash' == $post->post_status )
						$actions['untrash'] = "<a title='" . esc_attr( __( 'Restore this item from the Trash', 'apollo' ) ) . "' href='" . wp_nonce_url( admin_url( sprintf( $post_type_object->_edit_link . '&amp;action=untrash', $post->ID ) ), 'untrash-post_' . $post->ID ) . "'>" . __( 'Restore', 'apollo' ) . "</a>";
					elseif ( EMPTY_TRASH_DAYS )
						$actions['trash'] = "<a class='submitdelete' title='" . esc_attr( __( 'Move this item to the Trash', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID ) . "'>" . __( 'Trash', 'apollo' ) . "</a>";
					if ( 'trash' == $post->post_status || !EMPTY_TRASH_DAYS )
						$actions['delete'] = "<a class='submitdelete' title='" . esc_attr( __( 'Delete this item permanently', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID, '', true ) . "'>" . __( 'Delete Permanently', 'apollo' ) . "</a>";
				}

				/** @Ticket 15217 - Reviewed event */
				$listReviewEvent = get_option( Apollo_DB_Schema::_APL_EVENT_REVIEW_EDIT, '');
				if (!empty($listReviewEvent)) {
                    $listReviewEvent = explode(",", $listReviewEvent);
				    if (in_array($post->ID, $listReviewEvent)) {
                        $reviewedLink = '<a class="apl-reviewed-edit" data-post-id="'.$post->ID.'" data-location="list-page" href="#">'. __("Reviewed", "apollo") .'</a>';
                        $actions['reviewed'] = $reviewedLink;
                    }
                }

				if ( $post_type_object->public ) {
					if ( in_array( $post->post_status, array( 'pending', 'draft', 'future' ) ) ) {
						if ( $can_edit_post )
							$actions['view'] = '<a href="' . esc_url( add_query_arg( 'preview', 'true', get_permalink( $post->ID ) ) ) . '" title="' . esc_attr( sprintf( __( 'Preview &#8220;%s&#8221;', 'apollo' ), $title ) ) . '" rel="permalink">' . __( 'Preview', 'apollo' ) . '</a>';
					} elseif ( 'trash' != $post->post_status ) {
						$actions['view'] = '<a href="' . get_permalink( $post->ID ) . '" title="' . esc_attr( sprintf( __( 'View &#8220;%s&#8221;', 'apollo' ), $title ) ) . '" rel="permalink">' . __( 'View', 'apollo' ) . '</a>';
					}
				}

				$actions = apply_filters( 'post_row_actions', $actions, $post );

				echo '<div class="row-actions">';

				$i = 0;
				$action_count = sizeof($actions);

				foreach ( $actions as $action => $link ) {
					++$i;
					( $i == $action_count ) ? $sep = '' : $sep = ' | ';
					echo "<span class='$action'>$link$sep</span>";
				}
				echo '</div>';
                get_inline_data( $post );


                /* Custom inline data for apollo event */
				echo '
					<div class="hidden" id="apollo_event_inline_' . $post->ID . '">
						<div class="menu_order">' . $post->menu_order . '</div>
                        <div class="_start_date">' . $the_event->{Apollo_DB_Schema::_APOLLO_EVENT_START_DATE} . '</div>
                        <div class="_end_date">' . $the_event->{Apollo_DB_Schema::_APOLLO_EVENT_END_DATE} . '</div>
                        <div class="_admission_detail">' . $the_event->get_meta_data( Apollo_DB_Schema::_APOLLO_EVENT_DATA. '['. Apollo_DB_Schema::_ADMISSION_DETAIL .']' ) . '</div>
                        <div class="_admission_phone">' . $the_event->get_meta_data( Apollo_DB_Schema::_APOLLO_EVENT_DATA. '['. Apollo_DB_Schema::_ADMISSION_PHONE .']' ) . '</div>
                        <div class="_admission_ticket_url">' . $the_event->get_meta_data( Apollo_DB_Schema::_APOLLO_EVENT_DATA. '['. Apollo_DB_Schema::_ADMISSION_TICKET_URL .']' ) . '</div>
                        <div class="_admission_ticket_email">' . $the_event->get_meta_data( Apollo_DB_Schema::_APOLLO_EVENT_DATA. '['. Apollo_DB_Schema::_ADMISSION_TICKET_EMAIL .']' ) . '</div>
                        <div class="_admission_discount_url">' . $the_event->get_meta_data( Apollo_DB_Schema::_APOLLO_EVENT_DATA. '['. Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL .']' ) . '</div>
                        <div class="_admission_ticket_info">' . $the_event->get_meta_data( Apollo_DB_Schema::_APOLLO_EVENT_DATA. '['. Apollo_DB_Schema::_ADMISSION_TICKET_INFO .']' ) . '</div>

                        <div class="_contact_email">' . $the_event->get_meta_data( Apollo_DB_Schema::_APOLLO_EVENT_DATA. '['. Apollo_DB_Schema::_E_CONTACT_EMAIL .']' ) . '</div>
                        <div class="_contact_name">' . $the_event->get_meta_data( Apollo_DB_Schema::_APOLLO_EVENT_DATA. '['. Apollo_DB_Schema::_E_CONTACT_NAME .']' ) . '</div>
                        <div class="_contact_phone">' . $the_event->get_meta_data( Apollo_DB_Schema::_APOLLO_EVENT_DATA. '['. Apollo_DB_Schema::_E_CONTACT_PHONE .']' ) . '</div>

                        <div class="_free_admission">' . $the_event->{Apollo_DB_Schema::_APOLLO_EVENT_FREE_ADMISSION} . '</div>
                        <div class="_event_website_url">' . $the_event->get_meta_data( Apollo_DB_Schema::_APOLLO_EVENT_DATA. '['. Apollo_DB_Schema::_WEBSITE_URL .']' ) . '</div>
                        <div class="_admission_discount_text">' . $the_event->get_meta_data( Apollo_DB_Schema::_APOLLO_EVENT_DATA. '['. Apollo_DB_Schema::_ADMISSION_DISCOUNT_TEXT .']' ) . '</div>
					</div>
				';

			break;

             case 'home-spotlight':
                $checked = $the_event->is_home_spotlight() ? 'checked' : '';
				echo '<input '.$checked.' type="checkbox" class="chb_home_spot" value="'. $the_event->id .'" />';
                echo '<input type="hidden" class="event_ids" value="'. $the_event->id .'" />';
			break;

            case 'category-spotlight':
                $checked = $the_event->is_category_spotlight( $_REQUEST['event-type'] ) ? 'checked' : '';
				echo '<input '.$checked.' type="checkbox" class="chb_'.Apollo_DB_Schema::_IS_CATEGORY_SPOTLIGHT.'" value="'. $the_event->id .'" />';
                echo '<input type="hidden" class="event_ids" value="'. $the_event->id .'" />';

			break;

            case 'category-featured':
                $checked = $the_event->is_featured_category( $_REQUEST['event-type'] ) ? 'checked' : '';
				echo '<input '.$checked.' type="checkbox" class="chb_'.Apollo_DB_Schema::_IS_CATEGORY_FEATURE.'" value="'. $the_event->id .'" />';
			break;

            case 'tab-featured':
                $home_tabs = of_get_option( Apollo_DB_Schema::_HOME_CAT_FEATURES, false, true );
                $is_tab_feature = $the_event->is_tab_feature();

                $select_opt = "";

                if ( $home_tabs ) {
                    $select_opt = '<select class="tab_location"><option value="0">-</option>';
                    $i = 0;
                    foreach( $home_tabs as $ht ) {
                        $term = get_term_by( 'slug' , $ht, 'event-type' );
                        $select_opt .= "<option ".( $is_tab_feature && $is_tab_feature->{Apollo_DB_Schema::_TAB_LOCATION} == $ht ? 'selected' : '' )." value='{$ht}'>".$term->name."</option>";
                    }
                    $select_opt .= '</select>';
                }
                $val = $is_tab_feature ? 'checked' : '';
                echo '<input '.$val.' class="tab_features" value="'.$the_event->id.'" type="checkbox" />'. $select_opt;
			break;

            case 'org':
                echo $the_event->event_org_links( ', ' );
            break;

            case 'venue':
                echo $the_event->event_venue_link();
            break;

            case 'start_date':
                if ( $start_date = $the_event->{Apollo_DB_Schema::_APOLLO_EVENT_START_DATE} ) {
                    echo $start_date;
                } else {
                    $this->_the_empty_data();
                }

            break;

            case 'end_date':
                if ( $end_date = $the_event->{Apollo_DB_Schema::_APOLLO_EVENT_END_DATE} ) {
                    echo $end_date;
                } else {
                    $this->_the_empty_data();
                }
            break;

			case "event-type" :
                $curOrder = isset($_GET['order']) && !empty($_GET['order']) ? strtoupper($_GET['order']) : 'ASC';
				if ( ! $terms = wp_get_post_terms($post->ID,$column,array('order' => $curOrder)) ) {
					echo '<span class="na">&ndash;</span>';
				} else {
					$termlist = array();
                    foreach ( $terms as $term ) {
                        if ( $term->parent ) continue;  // Not display sub menu in the list event page

						$termlist[] = '<a href="' . admin_url( 'edit.php?' . $column . '=' . $term->slug . '&post_type=event' ) . ' ">' . $term->name . '</a>';
					}

					echo implode( ', ', $termlist );
				}
			break;
            case 'username':
                $user = get_user_by( 'id', $post->post_author);
                echo '<a target="_BLANK" href="'.  get_edit_user_link($post->post_author).'">'.(isset($user->user_nicename) ? $user->user_nicename : '').'</a>';
                break;

            case 'source':
                // from import or add new form
                $metaSource = $the_event->get_meta_data( Apollo_DB_Schema::_APOLLO_EVENT_SOURCE );
                echo $metaSource ? __('Import Tool', 'apollo') : __('Default', 'apollo');
                break;

            case 'visited_amount':

                if (isset($_GET['visited_amount'])) {
                    $aplQuery = new Apl_Query(Apollo_Tables::_APL_USER_VISIT);
                    $r = $aplQuery->get_row("item_id = $the_event->id AND item_type='event'");
                    echo $r ? $r->count : 0;
                }

                break;
            case 'primary-type':
                $primaryId = $the_event->get_meta_data(Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID);
                $term = !empty($primaryId) ? get_term($primaryId, 'event-type') : '';
                echo $term ? '<a href="'.get_edit_term_link( $term, 'event-type' ).'">'. $term->name. '</a>' : '--';
                break;
        }

    }

    private function _the_empty_data() {
        echo '<span class="na">&ndash;</span>';
    }

	/**
	 * Change the columns shown in admin.
	 */
	public function edit_columns( $existing_columns ) {

		if ( empty( $existing_columns ) && ! is_array( $existing_columns ) ) {
            $existing_columns = array();
        }

		unset( $existing_columns['title'], $existing_columns['comments'], $existing_columns['date'] );

		$columns = array();
		$columns["cb"] = "<input type=\"checkbox\" />";
		$columns['D'] = 'Dup';
		$columns["thumb"] = '<span class="tips" data-tip="' . __( 'Image', 'apollo' ) . '">' . __( 'Image', 'apollo' ) . '</span>';
		$columns["name"] = __( 'Name', 'apollo' );

        if ( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ORGANIZATION_PT ) ) {
            $columns["org"] = __( 'Organization', 'apollo' );
        } else if( isset( $columns["org"] ) ) {
            unset( $columns["org"] );
        }

        // Thienld : add venue column
        if ( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_VENUE_PT ) ) {
            $columns["venue"] = __( 'Venue', 'apollo' );
        } else if( isset( $columns["venue"] ) ) {
            unset( $columns["venue"] );
        }

        /**
         * Only display in Display management event
         *
         */
        if ( isset( $_REQUEST['dm'] ) && $_REQUEST['dm'] ) {

            if ( isset( $_REQUEST['event-type'] ) && $_REQUEST['event-type'] ) {
                // Save in wp_apollo_post_term
                $columns["category-spotlight"] = __( 'Spotlight', 'apollo' );
                $columns["category-featured"] = __( 'Featured', 'apollo' );
            } else {
                // Save in event metadata
                $columns["home-spotlight"] = __( 'Spotlight', 'apollo' );

                // Save in wp_apollo_post_term
                $columns["tab-featured"] = __( 'Featured', 'apollo' );

                // We not use it anymore, but keep it here if after we need
                //$columns["home-featured"] = '<span class="tips" data-tip="' . __( 'Home Featured', 'apollo' ) . '">' . __( 'Home Featured', 'apollo' ) . '</span>';
            }

        }

        $columns["start_date"] = __( 'Start Date', 'apollo' );
        $columns["end_date"] = __( 'End Date', 'apollo' );
        $columns["event-type"] = __( 'Event Category(s)', 'apollo' );
        $columns["primary-type"] = __( 'Primary Category', 'apollo' );

        if ( !isset( $_REQUEST['dm'] ) ) {
            $columns["date"] = __( 'Date', 'apollo' );
        }
        $columns['username'] = __( 'Associated User', 'apollo' );

        $columns["source"] = __( 'Source', 'apollo' );

        if (isset($_GET['visited_amount'])) {
            $columns["visited_amount"] = __( 'Visited Amount', 'apollo' );
        }

		return array_merge( $columns, $existing_columns );
	}

    /**
	 * Change "Featured Image" to "Event Image" throughout media modals.
	 * @param  array  $strings Array of strings to translate.
	 * @param  object $post
	 * @return array
	 */
	public function media_view_strings( $strings = array(), $post = null ) {
		if ( is_object( $post ) ) {
			if ( 'event' == $post->post_type ) {
				$strings['setFeaturedImageTitle'] = __( 'Set event image', 'apollo' );
				$strings['setFeaturedImage']      = __( 'Set event image', 'apollo' );
			}
		}
		return $strings;
	}


	/**
	 * Check if we're editing or adding a event
	 * @return boolean
	 */
	private function is_editing() {
		if ( ! empty( $_GET['post_type'] ) && 'event' == $_GET['post_type'] ) {
			return true;
		}
		if ( ! empty( $_GET['post'] ) && 'event' == get_post_type( $_GET['post'] ) ) {
			return true;
		}
		if ( ! empty( $_REQUEST['post_id'] ) && 'event' == get_post_type( $_REQUEST['post_id'] ) ) {
			return true;
		}
		return false;
	}

    /**
	 * Make event columns sortable
	 *
	 * @access public
	 * @param mixed $columns
	 * @return array
	 */
	public function custom_columns_sort( $columns ) {
		$custom = array(
			'name'               => 'name',
            'category-spotlight' => 'category_spotlight',
            'category-featured'  => 'category_featured',
            'home-spotlight'     => 'home_spotlight',
            'start_date'         => 'start_date',
            'end_date'           => 'end_date',
            'org'                => 'org',
            'venue'              => 'venue',
//            'event-type'         => 'event-type',
            'primary-type'       => 'primary-type',
            'visited_amount'     => 'visited_amount',
            'username'           => 'username'
		);
		return wp_parse_args( $custom, $columns );
	}

    /**
     * Custom Quick Edit form
     *
     * @access public
     * @param mixed $column_name
	 * @param mixed $post_type
    */
    public function quick_edit( $column_type, $post_type ) {

        if ( $column_type != 'name' ) {
            return;
        }

        require_once APOLLO_ADMIN_DIR. '/views/event/html-quick-edit.php';
    }

    /**
     * Custom Bulk Edit
     *
     * @access public
     * @param mixed $column_name
     * @param mixed $post_type
     */
    public function bulk_edit( $column_name, $post_type ) {

        if ( $column_name != 'name' ) {
            return;
        }

        include APOLLO_ADMIN_DIR. '/views/event/html-bulk-edit.php';
    }

    /**
     * Quick and Bulk saving
     *
     * @access public
     * @param mixed $post_id
     * @param mixed $post
     */
    public function bulk_and_quick_edit_save_post( $post_id, $post ) {

        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}

        // Don't save revisions and autosaves
		if ( wp_is_post_revision( $post_id ) || wp_is_post_autosave( $post_id ) ) {
			return $post_id;
		}

        if ( $post->post_type != 'event' ) {
            return $post_id;
        }

        // Check user permission
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return $post_id;
		}

        if ( isset( $_REQUEST['apollo_event_bulk_edit_nonce'] ) && ! wp_verify_nonce( $_REQUEST['apollo_event_bulk_edit_nonce'], 'apollo_event_bulk_edit_nonce' ) ) {
			return $post_id;
		}

        $event = get_event( $post );

        if ( ! empty( $_REQUEST['apollo_event_quick_edit'] ) ) {

			$this->quick_edit_save( $post_id, $event );
		} else {
			$this->bulk_edit_save( $post_id, $event );
		}

        /* Thienld: new instance to handle caching logic for event category page */
        do_action('apl_event_cache_empty_all_cache');

		return $post_id;
    }

    private function quick_edit_save( $post_id, $event ) {

        $this->save_date_range( Apollo_DB_Schema::_APOLLO_EVENT_START_DATE, Apollo_DB_Schema::_APOLLO_EVENT_END_DATE, $post_id );

        $this->save_date_range( Apollo_DB_Schema::_APOLLO_EVENT_START_DATE, Apollo_DB_Schema::_APOLLO_EVENT_END_DATE, $post_id );

        // Check and save event data
        $_apollo_event_data = $_POST[Apollo_DB_Schema::_APOLLO_EVENT_DATA];

        if ( isset( $_apollo_event_data[Apollo_DB_Schema::_ADMISSION_TICKET_URL] )
            && ! filter_var( $_apollo_event_data[Apollo_DB_Schema::_ADMISSION_TICKET_URL], FILTER_VALIDATE_URL ) ) {
            $_apollo_event_data[Apollo_DB_Schema::_ADMISSION_TICKET_URL] = '';
        }

        if ( isset( $_apollo_event_data[Apollo_DB_Schema::_ADMISSION_TICKET_EMAIL] )
            && ! is_email( $_apollo_event_data[Apollo_DB_Schema::_ADMISSION_TICKET_EMAIL] ) ) {
            $_apollo_event_data[Apollo_DB_Schema::_ADMISSION_TICKET_EMAIL] = '';
        }

        if ( isset( $_apollo_event_data[Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL] )
            && ! filter_var( $_apollo_event_data[Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL], FILTER_VALIDATE_URL ) ) {
            $_apollo_event_data[Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL] = '';
        }

        if ( isset( $_apollo_event_data[Apollo_DB_Schema::_WEBSITE_URL] )
            && ! filter_var( $_apollo_event_data[Apollo_DB_Schema::_WEBSITE_URL], FILTER_VALIDATE_URL ) ) {
            $_apollo_event_data[Apollo_DB_Schema::_WEBSITE_URL] = '';
        }

        // Get the root event data
        $event = get_event( get_post( $post_id ) );
        $event_data = @unserialize( $event->get_meta_data( Apollo_DB_Schema::_APOLLO_EVENT_DATA ) );

        $event_data[Apollo_DB_Schema::_ADMISSION_PHONE] = $_apollo_event_data[Apollo_DB_Schema::_ADMISSION_PHONE];
        $event_data[Apollo_DB_Schema::_ADMISSION_TICKET_URL] = $_apollo_event_data[Apollo_DB_Schema::_ADMISSION_TICKET_URL];
        $event_data[Apollo_DB_Schema::_ADMISSION_TICKET_EMAIL] = $_apollo_event_data[Apollo_DB_Schema::_ADMISSION_TICKET_EMAIL];
        $event_data[Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL] = $_apollo_event_data[Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL];
        $event_data[Apollo_DB_Schema::_E_CONTACT_NAME] = $_apollo_event_data[Apollo_DB_Schema::_E_CONTACT_NAME];
        $event_data[Apollo_DB_Schema::_E_CONTACT_EMAIL] = $_apollo_event_data[Apollo_DB_Schema::_E_CONTACT_EMAIL];
        $event_data[Apollo_DB_Schema::_E_CONTACT_PHONE] = $_apollo_event_data[Apollo_DB_Schema::_E_CONTACT_PHONE];
        $event_data[Apollo_DB_Schema::_WEBSITE_URL] = $_apollo_event_data[Apollo_DB_Schema::_WEBSITE_URL];
        $event_data[Apollo_DB_Schema::_ADMISSION_DISCOUNT_TEXT] = $_apollo_event_data[Apollo_DB_Schema::_ADMISSION_DISCOUNT_TEXT];

        /**
         * @ticket #19011: [CF] 20190123 - Add Draft labels to event
         * [ADMIN DRAFT]
         */
        if(isset($_POST['post_status']) && $_POST['post_status'] === 'draft'){
            $event_data[Apollo_DB_Schema::_E_USER_DRAFT] = 0;
        }

        update_apollo_meta( $post_id, Apollo_DB_Schema::_APOLLO_EVENT_DATA, serialize( $event_data ) );

        // Save for searching
        update_apollo_meta( $post_id, Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL, $event_data[Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL] );

        $_apollo_event_free_admission = isset( $_REQUEST[Apollo_DB_Schema::_APOLLO_EVENT_FREE_ADMISSION] ) && $_REQUEST[Apollo_DB_Schema::_APOLLO_EVENT_FREE_ADMISSION] == 'on' ?
            'yes' : 'no';
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APOLLO_EVENT_FREE_ADMISSION, $_apollo_event_free_admission );


        do_action( 'apollo_event_quick_edit_save', $event );
    }

    public function bulk_edit_save( $post_id, $event ) {

        $_apollo_event_data = unserialize( $event->{Apollo_DB_Schema::_APOLLO_EVENT_DATA} );

        if ( !empty( $_REQUEST[ 'change_admission_phone' ] ) && isset( $_REQUEST[ Apollo_DB_Schema::_ADMISSION_PHONE ] )
            && isset( $_apollo_event_data[Apollo_DB_Schema::_ADMISSION_PHONE] ) ) {

            $_apollo_event_data[Apollo_DB_Schema::_ADMISSION_PHONE] = $_REQUEST[ Apollo_DB_Schema::_ADMISSION_PHONE ];
        }

        if ( !empty( $_REQUEST[ 'change_admission_ticket_url' ] ) && isset( $_REQUEST[ Apollo_DB_Schema::_ADMISSION_TICKET_URL ] )
            && isset( $_apollo_event_data[Apollo_DB_Schema::_ADMISSION_TICKET_URL] ) ) {
            $_apollo_event_data[Apollo_DB_Schema::_ADMISSION_TICKET_URL] = $_REQUEST[ Apollo_DB_Schema::_ADMISSION_TICKET_URL ];
        }

        /**
         * @ticket #19011: [CF] 20190123 - Add Draft labels to event
         * [ADMIN DRAFT]
         */
        if(isset($_REQUEST['post_status']) && $_REQUEST['post_status'] === 'draft'){
            $_apollo_event_data = !empty($_apollo_event_data) ? $_apollo_event_data : array();
            $_apollo_event_data[Apollo_DB_Schema::_E_USER_DRAFT] = 0;
        }

        if ( $_apollo_event_data ) {
            update_apollo_meta( $post_id, Apollo_DB_Schema::_APOLLO_EVENT_DATA, serialize( $_apollo_event_data ) );
        }

        do_action( 'apollo_event_bulk_edit_save', $event );
    }

    private function _filterAdvanceSearch()
    {
        $start = isset($_GET['start_date']) ? $_GET['start_date'] : '';
        $end   = isset($_GET['end_date']) ? $_GET['end_date'] : '';
        $includePastEvent = isset($_GET['past_event']) ? 'checked' : '';

        echo '<span class="wrapper-date-range">';
        echo '<input placeholder="'.__("Start", "apollo").'" class="apl-filter" type="text" name="start_date" id="_apollo_event_start_date" value="'.$start.'" />';
        echo '</span>';

        echo '<span class="wrapper-date-range">';
        echo '<input placeholder="'.__("End", "apollo").'" class="apl-filter" type="text" name="end_date" id="_apollo_event_end_date" value="'.$end.'" />';
        echo '</span>';

        echo '<span class="past-event-wrapper">';
        echo '<input '.$includePastEvent.' type="checkbox" value="1" name="past_event" /><label>'.__('Include past events ', 'apollo').'</label>';
        echo '</span>';
    }

    private function _organization_filters() {
        global $typenow;

        if ($typenow == $this->type ) {
            if ($selected = isset($_GET['filter_organization']) ? $_GET['filter_organization'] : '') {
                $org = get_post($selected);
            }
            ?>
            <select name='filter_organization'
                    data-value="<?php echo $selected ?>"
                    data-enable-remote="1"
                    data-post-type="<?php echo Apollo_DB_Schema::_ORGANIZATION_PT ?>"
                    data-source-url="apollo_get_remote_associate_data_to_select2_box"
                    id="admin-evt-venue-drop"
                    class="w-200 select apl_select2"
                >
                <option value=''><?php _e('Select Organization','apollo') ?></option>
                <?php
                    if (!empty($org)) {
                        echo sprintf('<option selected="selected" value="%s">%s</option>', $org->ID, $org->post_title);
                    }
                ?>
            </select>
            <?php
        }


    }
    private function _venue_filters() {
        global $typenow;

        if ($typenow == $this->type ) {

            if ($selected = isset($_GET['filter_venue']) ? $_GET['filter_venue'] : '') {
                $venue = get_post($selected);
            }
            ?>
            <select name='filter_venue'
                    data-value="<?php echo $selected ?>"
                    data-enable-remote="1"
                    data-post-type="<?php echo Apollo_DB_Schema::_VENUE_PT ?>"
                    data-source-url="apollo_get_remote_associate_data_to_select2_box"
                    id="admin-evt-venue-drop"
                    class="w-200 select apl_select2"
            >
                <option value=''><?php _e('Select Venue','apollo') ?></option>
                <?php
                    if (!empty($venue)) {
                        echo sprintf('<option selected="selected" value="%s">%s</option>', $venue->ID, $venue->post_title);
                    }
                ?>
            </select>
            <?php
        }

    }

    private function _users_filter(){
        global $typenow ;

        if ($typenow == $this->type ) {
            $selected = isset($_GET['filter_users']) ? $_GET['filter_users'] : '';
            $user = get_user_by('id', $selected);
            ?>
            <select
                data-value="<?php echo $selected ?>"
                data-enable-remote="1"
                data-source-url="apollo_get_users"
                id=""
                class="w-200 select apl_select2"
                name='filter_users'>"
                <option value=''><?php _e('Select user','apollo') ?></option>

                <?php
                if ($user) {
                    echo sprintf('<option selected="selected" value="%s">%s</option>', $user->ID, $user->user_login);
                }
                ?>

            </select>
        <?php
        }

    }

    //Filter by venue
    private function _modify_filter_venue( $query )
    {
        global $typenow;
        global $pagenow;
        if( $pagenow == 'edit.php' && $typenow == $this->type && isset($_GET['filter_venue']) && $_GET['filter_venue'] )
        {
            $query->query_vars['meta_key'] = '_apollo_event_venue';
            $query->query_vars[ 'meta_value' ] = (int)$_GET['filter_venue'];
        }
    }


    public function event_filters() {
        global $typenow;

        if ( $typenow != $this->type ) {
            return;
        }

        // Add Event type for filtering
        apollo_dropdown_categories('event-type', array('hide_empty' => 0, 'auto_complete' => true));

        // Get all theme tool
        $aplQuery = new Apl_Query(Apollo_Tables::_APL_THEME_TOOL);
        $themeToolData = $aplQuery->get_where();

        $tt = array();
        if ( $themeToolData ) {
            foreach ( $themeToolData as $themeTool ) {
                if (!$themeTool->cat_id) continue;
                $term = get_term($themeTool->cat_id, 'event-type');
                $tt[] = $term->slug;
            }
        }

        echo '<input type="hidden" id="current-theme-tool-onfilter" value="'.  implode(',', $tt).'" />';

        // Add hidden field to give to wordpress know this is Display manage management event page
        if ( isset( $_REQUEST['dm'] ) && $_REQUEST['dm'] ) {
            echo '<input name="dm" type="hidden" value="true" />';
        }

        if ( isset( $_REQUEST['advanced'] ) && $_REQUEST['advanced'] ) {
            echo '<input name="advanced" type="hidden" value="true" />';
        }

        // Create drop down filter by user
        $this->_users_filter();

        if (Apollo_App::is_avaiable_module(Apollo_DB_Schema::_ORGANIZATION_PT)) {
            // Create drop down filter by org
            $this->_organization_filters();
        }

        if (Apollo_App::is_avaiable_module(Apollo_DB_Schema::_VENUE_PT)) {
            // Create drop down filter by venue
            $this->_venue_filters();
        }

        if ($this->isAdvancedSearch()) {
            $this->_filterAdvanceSearch();
        }
    }

    /**
     * Handle event filter query
     */
    public function event_filter_query( $query ) {


        if ( isset($_SESSION[Apollo_SESSION::FLAG_FOR_SAVING_EVENT_OPTIONS])
            && isset( $_SESSION[Apollo_SESSION::SAVE_EVENT_OPTIONS] )
        ){

            $_GET = array_replace($_GET, $_SESSION[Apollo_SESSION::SAVE_EVENT_OPTIONS]);
            unset($_GET['mode']); // Thienld : remove option view excerpt mode when ajax save post term display management
            unset($_SESSION[Apollo_SESSION::FLAG_FOR_SAVING_EVENT_OPTIONS]);
            unset($_SESSION[Apollo_SESSION::SAVE_EVENT_OPTIONS]);

            if ( $this->_is_home_display_management_page() ) {
                $this->_update_home_spotlight();
                $this->_update_tab_feature();
                // Thienld : force clear all caching homepage spotlight and featured events
                $cacheManagementFilePath = APOLLO_INCLUDES_DIR. '/admin/tools/cache/Inc/apollo-sites-caching-management.php';
                if(file_exists($cacheManagementFilePath)){
                    require_once $cacheManagementFilePath;
                    if(class_exists('APLC_Site_Caching_Management')){
                        $currentSiteID = get_current_blog_id();
                        $cleanCachedSite = new APLC_Site_Caching_Management($currentSiteID);
                        $cleanCachedSite->showAdminMessage = false;
                        $cleanCachedSite->emptyAllLocalCaches();
                    }
                }
            } else {
                /**
                 * Delete all the events not in the current list
                 * as clear the lists
                 */

                $eventIds      = isset( $_GET['event_ids'] ) && $_GET['event_ids'] ? $_GET['event_ids'] : array();
                $eventIds      = !empty($eventIds) ? explode(',',$eventIds) : array();
                $catSpotEventIds = isset( $_GET['chb__is_category_spotlight'] ) && $_GET['chb__is_category_spotlight'] ? $_GET['chb__is_category_spotlight'] : array();
                $catSpotEventIds = !empty($catSpotEventIds) ? explode(',',$catSpotEventIds) : array();
                $catFeaEventIds = isset( $_GET['chb__is_category_feature'] ) && $_GET['chb__is_category_feature'] ? $_GET['chb__is_category_feature'] : array();
                $catFeaEventIds = !empty($catFeaEventIds) ? explode(',',$catFeaEventIds) : array();

                try {
                    $removeEventIds = array_diff(array_diff($eventIds, $catSpotEventIds), $catFeaEventIds);
                } catch (Exception $ex) {
                    $removeEventIds = array();
                }

                $this->term = get_term_by('slug', isset($_GET['event-type']) ? $_GET['event-type'] : '', 'event-type');
                $aplQuery = new Apl_Query( Apollo_Tables::_POST_TERM );

                if ($removeEventIds) {
                    $eventStr = "('".implode("','", $removeEventIds)."')";
                    $aplQuery->delete("term_id = ".$this->term->term_id." AND post_id IN $eventStr");
                }

                /*End clear*/

                // Update the list again
                $this->_update_cat_opts( Apollo_DB_Schema::_IS_CATEGORY_SPOTLIGHT );
                $this->_update_cat_opts( Apollo_DB_Schema::_IS_CATEGORY_FEATURE );
            }

            if (function_exists('w3tc_pgcache_flush')) {
                w3tc_pgcache_flush();
            }
            // Thienld : update post term language to solve issue getting spotlight events on homepage
            $this->_update_post_term_by_current_language();

            $this->_remove_post_terms_empty_opts();

            /* Thienld: new instance to handle caching logic for event category page */
            do_action('apl_event_cache_empty_all_cache');
        }
        // Filter by venue
        $this->_modify_filter_venue($query);

    }

    private function _update_post_term_by_current_language(){
        try{
            $event_ids      = isset( $_GET['event_ids'] ) && $_GET['event_ids'] ? $_GET['event_ids'] : array();
            $event_ids = $event_ids ? explode(',', $event_ids) : '';

            if ( ! $event_ids ) return;

            // Thienld : set flag to check new term lang col either existing or not
            if(Apollo_App::checkExistedTermLangCol()){
                $isExistedTermLangCol = true;
            } else {
                $isExistedTermLangCol = false;
            }
            if( Apollo_App::hasWPLM() && $isExistedTermLangCol){
                foreach ( $event_ids as $post_id ):
                    // Thienld : set current lang for term lang col with current lang on saving.
                    Apollo_App::updatePostTermLang($post_id);
                endforeach;
            }
        } catch(Exception $ex){
            aplDebugFile("_update_post_term_by_current_language", $ex->getMessage());
        }
    }

    /**
     * Delete post_term with all selection is empty
     *
     * @access private
     * @return none
     */
    private function _remove_post_terms_empty_opts() {
        global $wpdb;

        $wpdb->query( " DELETE FROM {$wpdb->{Apollo_Tables::_POST_TERM}} WHERE ".Apollo_DB_Schema::_IS_CATEGORY_FEATURE." <> 'yes'
            AND ".Apollo_DB_Schema::_IS_CATEGORY_SPOTLIGHT." <> 'yes'
            AND ( ".Apollo_DB_Schema::_TAB_LOCATION." = '' OR ".Apollo_DB_Schema::_TAB_LOCATION." IS NULL )
        " );

       //clear cached templates
        $fileCache = aplc_instance('APLC_Inc_Files_HomeFeatured');
        $fileCache->remove();
    }


    /**
     * Update tab feature
     *
     * @access private
     * @return none
     *
     */
    private function _update_home_spotlight() {

        $home_spots = isset( $_GET['chb_home_spot'] ) && $_GET['chb_home_spot'] ? $_GET['chb_home_spot'] : array();
        $event_ids  = isset( $_GET['event_ids'] ) && $_GET['event_ids'] ? $_GET['event_ids'] : array();

        $home_spots = $home_spots ? explode(',', $home_spots) : array();
        $event_ids = $event_ids ? explode(',', $event_ids) :  array();

        global $wpdb;

        if ( ! $event_ids ) return false;

        foreach ( $event_ids as $post_id ):
            $val = $home_spots && in_array( $post_id , $home_spots ) ? 'yes' : NULL;

            $updatedVal = array(
                Apollo_DB_Schema::_IS_HOME_SPOTLIGHT => $val,
            );

            $wpdb->update( $wpdb->{Apollo_Tables::_POST_TERM}, $updatedVal , array(
                'post_id'      => $post_id
            ));




        endforeach;
    }

    /**
     * Update tab feature
     *
     * @access private
     * @return none
     *
     */
    private function _update_tab_feature() {

        $tab_features   = isset( $_GET['tab_features'] ) && $_GET['tab_features'] ? $_GET['tab_features'] : array();
        $event_ids      = isset( $_GET['event_ids'] ) && $_GET['event_ids'] ? $_GET['event_ids'] : array();
        $tab_locations  = isset( $_GET['tab_location'] ) && $_GET['tab_location'] ? $_GET['tab_location'] : array();

        $tab_features = $tab_features ? explode(',', $tab_features) : '';
        $event_ids = $event_ids ? explode(',', $event_ids) : '';
        $tab_locations = $tab_locations ? explode(',', $tab_locations) : '';

        global $wpdb;

        if ( ! $tab_locations ) return false;

        $i = 0;
        foreach ( $event_ids as $post_id ):

            if ( in_array( $post_id , $tab_features ) ) {
                if ( isset( $tab_locations[$i] ) && ! $tab_locations[$i] ) {
                    continue;
                }

                $tab_value = $tab_locations[$i];
            } else {
                $tab_value = NULL;
            }

            $wpdb->update( $wpdb->{Apollo_Tables::_POST_TERM}, array(
                Apollo_DB_Schema::_TAB_LOCATION => $tab_value,
            ), array(
                'post_id'      => $post_id
            ));

            $i++;
        endforeach;
    }

    /**
     * Update cat spotlight and feature
     *
     * @access private
     * @return none
     */
    private function _update_cat_opts( $field ) {

        $opt_name = 'chb_'. $field;
        $chb_cat_spots  = isset( $_GET[$opt_name] ) && $_GET[$opt_name] ? $_GET[$opt_name] : array();
        $event_ids      = isset( $_GET['event_ids'] ) && $_GET['event_ids'] ? $_GET['event_ids'] : array();

		//the GET value from the filter dropdown may not match the query string on save
		$url_parse = parse_url($_SERVER['REQUEST_URI']);
		parse_str($url_parse['query'], $querystr);

        $slug           = $querystr['event-type'];

        $chb_cat_spots = $chb_cat_spots ? explode(',', $chb_cat_spots) : [];
        $event_ids = $event_ids ? explode(',', $event_ids) : '';

        if ( ! $slug ) return;

        $term           = get_term_by( 'slug' , $slug, 'event-type' );
        $term_id        = $term->term_id;

        if ( ! $event_ids ) return;

        global $wpdb;

        foreach ( $event_ids as $post_id ):

            $is_checked = in_array( $post_id, $chb_cat_spots );
            if ( ! Apollo_App::has_post_term( $post_id, $term_id ) && $is_checked ) {
                $wpdb->insert( $wpdb->{Apollo_Tables::_POST_TERM}, array(
                    'post_id'   => $post_id,
                    'term_id'   => $term_id,
                    $field => 'yes',
                ) );
                continue;
            }

            $wpdb->update( $wpdb->{Apollo_Tables::_POST_TERM}, array(
                $field => $is_checked ? 'yes' : NULL
            ), array(
                'term_id'   => $term_id,
                'post_id'   => $post_id,
            ));
        endforeach;
    }

    /**
     * Custom post join
     *
     * @access public
     * @return string
     */
    public function filter_meta_join( $join ) {
        global $wpdb, $typenow;

        // Check in case display management page and individual case in theme tools
        if ( $this->themeToolClass && $this->themeToolClass->isThemeTool() && isset( $_GET['dm'] ) ) {
            $join = '';
        }

        $join = str_replace( 'post_id', 'apollo_event_id', $this->replace_meta_table( $join ) );

        // Join to filter event not exprise
        $currentDate = current_time('Y-m-d');

        // If not in display management page display event not have start date and end date
        $emptyEndDate = '';
        if ( ! isset( $_REQUEST['dm'] ) ) {
            $emptyEndDate = " OR mt_end_d.meta_value  = '' ";
        }

        // Dismissing this code if current page is advance searching
        if (
            !isset($_GET['post_status']) || (isset($_GET['post_status']) && !in_array($_GET['post_status'], array('trash', 'draft')) )
        ) {
            if ($this->includePastEvents()) {
                if (isset($_GET['orderby'])) {
                    if ($_GET['orderby'] == 'start_date') {
                        $join .= "
                            INNER JOIN {$this->meta_tbl} mt_start_d ON mt_start_d.apollo_event_id = ". $wpdb->posts .".ID
                            AND mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
                        ";
                    } else if ($_GET['orderby'] == 'end_date') {
                        $join .= "
                            INNER JOIN {$this->meta_tbl} mt_end_d   ON mt_end_d.apollo_event_id   = ". $wpdb->posts .".ID
                            AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
                        ";
                    }

                }

            } else {
                $join .= "
                    INNER JOIN {$this->meta_tbl} mt_start_d ON mt_start_d.apollo_event_id = ". $wpdb->posts .".ID
                    AND mt_start_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'

                    INNER JOIN {$this->meta_tbl} mt_end_d   ON mt_end_d.apollo_event_id   = ". $wpdb->posts .".ID
                    AND mt_end_d.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_END_DATE ."'
                    AND ( CAST( mt_end_d.meta_value AS DATE )  >= '{$currentDate}' $emptyEndDate )
                ";
            }

        }

        // Is home page
        if ( $this->_is_home_display_management_page() ) {

            $join .= " INNER JOIN {$this->post_term_tbl} pt ON pt.post_id = ". $wpdb->posts .".ID
                AND (
                        pt.".Apollo_DB_Schema::_IS_CATEGORY_FEATURE ." = 'yes'

                    OR  pt.". Apollo_DB_Schema::_IS_CATEGORY_SPOTLIGHT ." = 'yes'

                    OR  pt.". Apollo_DB_Schema::_IS_HOME_SPOTLIGHT ." = 'yes'

                    OR  (
                            pt.". Apollo_DB_Schema::_TAB_LOCATION ." IS NOT NULL
                        AND pt.". Apollo_DB_Schema::_TAB_LOCATION ." <> ''
                    )
                )";

            //Filter by zip if has theme tool exists
            if ( $this->themeToolClass && $this->themeToolClass->isThemeTool() ) {
                $join .= $this->themeToolClass->getVenueJoin();
            }

            $join .= "
                INNER JOIN $wpdb->term_relationships tr ON pt.post_id = tr.object_id
                INNER JOIN $wpdb->term_taxonomy tt ON tr.term_taxonomy_id = tt.term_taxonomy_id AND tt.term_id = pt.term_id
            ";

        }

        // Join in category display management
        if ( $this->_is_cat_display_management_page() ) {
            $term = get_term_by('slug', $_GET['event-type'], 'event-type');

            $whereTerm = $term ? " AND term_id = $term->term_id " : '';
            $join .= " LEFT JOIN {$this->post_term_tbl} pt ON pt.post_id = ". $wpdb->posts .".ID $whereTerm";

            //Filter by zip if has theme tool exists
            if ( $this->themeToolClass && $this->themeToolClass->isThemeTool() ) {
                $join .= $this->themeToolClass->getVenueJoin();
            }
        }

        /**
         * Filter by org
         *
         */
        if ( isset( $_REQUEST['orderby'] ) ) {
            $orderby = $_REQUEST['orderby'];
            $event_meta_tbl = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};
            $postTermTbl = $wpdb->{Apollo_Tables::_POST_TERM};
            switch($orderby) {
                case 'home_spotlight':
                case 'category_spotlight':
                case 'category_featured':
                    //$join .= " LEFT JOIN $postTermTbl mt_sort ON mt_sort.post_id = {$wpdb->posts}.ID ";
                    break;
                case 'org':
                    $join .= "
                        LEFT JOIN ".$wpdb->{Apollo_Tables::_APL_EVENT_ORG}." e_org ON ( {$wpdb->posts}.ID = e_org.post_id AND e_org.is_main = 'yes' )
                        LEFT JOIN ".$wpdb->posts." org ON (e_org.org_id = org.ID AND org.post_type = '".Apollo_DB_Schema::_ORGANIZATION_PT."')
                    ";
                    break;
                case 'venue':
                    $join .= "
                        LEFT JOIN ".$event_meta_tbl." em_sortable ON ( {$wpdb->posts}.ID = em_sortable.apollo_event_id AND em_sortable.meta_key = '_apollo_event_venue' )
                        LEFT JOIN ".$wpdb->posts." venue_sort ON (em_sortable.meta_value = venue_sort.ID AND venue_sort.post_type = '".Apollo_DB_Schema::_VENUE_PT."')
                    ";
                    break;
                case 'primary-type':
                    $join .= "
                        LEFT JOIN ".$event_meta_tbl." em_term_primary ON ( {$wpdb->posts}.ID = em_term_primary.apollo_event_id AND em_term_primary.meta_key = '_apl_event_term_primary_id' )
                        LEFT JOIN ".$wpdb->terms." pri_term ON (em_term_primary.meta_value = pri_term.term_id)
                    ";
                    break;
                case 'event-type':
                    $curOrder = isset($_GET['order']) && !empty($_GET['order']) ? strtoupper($_GET['order']) : 'ASC';
                    $join .= "
                        INNER JOIN (
                            SELECT DISTINCT(etr1.`object_id`) AS ev_id, et1.name AS term_name
                            FROM ".$wpdb->term_relationships." etr1
                            INNER JOIN ".$wpdb->term_taxonomy." ett1  ON ( etr1.term_taxonomy_id = ett1.term_taxonomy_id AND ett1.`taxonomy` = 'event-type' AND ett1.parent = 0 )
                            INNER JOIN ".$wpdb->terms." et1 ON (ett1.term_id = et1.term_id)
                            ORDER BY et1.name ".$curOrder."
                          ) AS term_sortable ON term_sortable.ev_id = wp_posts.ID
                    ";
                    break;
                case 'visited_amount':
                    $visitTbl = $wpdb->{Apollo_Tables::_APL_USER_VISIT};
                    $join .= "
                        LEFT JOIN $visitTbl vstTbl ON vstTbl.item_id = {$wpdb->posts}.ID
                            AND vstTbl.item_type = '".Apollo_DB_Schema::_EVENT_PT."'
                        ";
                    break;

            }

        }
        // #10515 - change the default sort order to END DATE ASC and change the default sort order to Primary Event Category Type in alpha order
        if ( isset($_REQUEST['dm']) && $_REQUEST['dm'] && !isset($_REQUEST['orderby'])  && !isset($_REQUEST['order']) ){
            $join .= "
                        LEFT JOIN ".$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}." em_term_primary ON ( {$wpdb->posts}.ID = em_term_primary.apollo_event_id AND em_term_primary.meta_key = '_apl_event_term_primary_id' )
                        LEFT JOIN ".$wpdb->terms." pri_term ON (em_term_primary.meta_value = pri_term.term_id)
                    ";
        }

        return $join;
    }

    private function _is_home_display_management_page() {
        global $typenow;
        return $typenow == Apollo_DB_Schema::_EVENT_PT && isset( $_REQUEST['dm'] ) && $_REQUEST['dm'] == true
            && ( !isset( $_REQUEST['event-type'] ) || ! $_REQUEST['event-type'] );
    }

    private function _is_cat_display_management_page() {
        global $typenow;
        return $typenow == Apollo_DB_Schema::_EVENT_PT && isset( $_REQUEST['dm'] ) && $_REQUEST['dm'] == true
            && isset( $_REQUEST['event-type'] ) && $_REQUEST['event-type'] ;
    }

    /**
     * Custom post where
     *
     * @access public
     * @return string
     */
    public function posts_where( $where ) {

        global $wpdb;

        // Check in case display management page and individual case in theme tools
        if ( $this->themeToolClass && $this->themeToolClass->isThemeTool() && isset( $_GET['dm'] ) ) {
            $where = '';
        }

        if ($this->themeToolClass && $this->themeToolClass->isThemeTool()) {
            $where .= ' AND '. $this->themeToolClass->getWhereInEventIds();
        }

        if ( isset( $_REQUEST['dm'] ) ) {
            $where .= " AND {$wpdb->posts}.post_status <> 'private'
                        AND {$wpdb->posts}.post_status <> 'pending'
                    ";
        }

        // Not get draft in home event
        if ( ( isset( $_REQUEST['post_status'] ) && $_REQUEST['post_status'] != 'draft' ) || ! isset( $_REQUEST['post_status'] ) ) {
            $where .= " AND {$wpdb->posts}.post_status <> 'draft' ";
        }

        if ( isset( $_REQUEST['post_status'] ) && $_REQUEST['post_status'] == 'review-edit' ) {
            $currentListReview = get_option( Apollo_DB_Schema::_APL_EVENT_REVIEW_EDIT, '');
            $where .= " AND {$wpdb->posts}.ID IN ({$currentListReview}) ";
        }

        /**
         * @ticket #18894: Display private event in admin list page.
         */
        if ( isset( $_REQUEST['post_status'] ) && $_REQUEST['post_status'] == 'private' ) {
            $sqlPrivate = AplEventFunction::getQueryEventPrivate($wpdb->posts);
            if(!empty($sqlPrivate)) {
                $where = str_replace("({$wpdb->posts}.post_status = 'private')", $sqlPrivate, $where);
            }
        }

        // Filter by org
        if( is_admin() ) {
            $Tbl = $wpdb->prefix.Apollo_Tables::_APL_EVENT_ORG;
            if ( isset( $_GET['filter_organization'] ) && !empty( $_GET['filter_organization'] ) && intval( $_GET['filter_organization'] ) != 0 ) {
                $org_id = intval( $_GET['filter_organization'] );
                $where .= " AND ID IN (SELECT post_id FROM " . $Tbl ." WHERE org_id = $org_id )";
            }
        }
        // Filter by user
        if( is_admin() ) {
            $Tbl = $wpdb->prefix.'posts';
            if ( isset( $_GET['filter_users'] ) && !empty( $_GET['filter_users'] ) && intval( $_GET['filter_users'] ) != 0 ) {
                $user_id = intval( $_GET['filter_users'] );
                $where .= " AND ID IN (SELECT ID FROM " . $Tbl ." WHERE post_author = $user_id )";
            }
        }

        if (is_admin() && $this->isAdvancedSearch()) {
            /**
             * Search date range generating
             */
            include_once APOLLO_INCLUDES_DIR . '/src/event/inc/AplGenerateSearchDate.php';
            $startDate = isset($_GET['start_date']) && !empty($_GET['start_date']) ? $_GET['start_date'] : false;
            $endDate = isset($_GET['end_date']) && !empty($_GET['end_date']) ? $_GET['end_date'] : false;

            if (! $startDate && ! $endDate
                && empty($_GET['s'])
                && empty($_GET['event-type'])
                && empty($_GET['filter_users'])
                && empty($_GET['filter_organization'])
                && empty($_GET['filter_venue'])
                && empty($_GET['past_event'])
                && empty($_GET['post_status'])
            ) {

                $currentDate = current_time('Y-m-d');
                $limitDate = date('Y-m-d', strtotime("+7 day", strtotime($currentDate)));
            }

            $searchDate = new AplGenerateSearchDate($startDate, $endDate, false);

            $where .= $searchDate->generate();

            // limit the default list of events displayed on the Advanced Search page when
            // the user first goes to that page to only the last 7 days of events
            if (!empty($limitDate)) {
                $where .= " AND mt_end_d.meta_value <= '$limitDate' ";
            }

        }

        return $this->replace_meta_table( $where );
    }

    /**
     * Replace meta table
     *
     * @access public
     * @return string
     */
    public function replace_meta_table( $str ) {
        global $wpdb;
        return str_replace( $wpdb->postmeta, $wpdb->apollo_eventmeta, $str);
    }

    /**
     * Change order by
     *
     * @access public
     * @return string
     */
    public function change_order_by( $str ) {
        global $wpdb;
        $str = $wpdb->posts.".ID DESC";

        // #10515 - In our category Display Management, change the default sort order to END DATE ASC
       if (isset( $_REQUEST['dm'] ) && $_REQUEST['dm'] ){
            $adminOrder = of_get_option(Apollo_DB_Schema::_EVENT_ADMIN_DISPLAY_ORDER, 'mt_end_d' );
             if( isset( $_REQUEST['event-type'] ) && $_REQUEST['event-type'] ){
                 $str = " $adminOrder.meta_value ASC ";
             } else {
                 $str = " pri_term.name IS NULL ASC , pri_term.name ASC, $adminOrder.meta_value ASC ";
             }
        }

        if ( isset( $_REQUEST['order'] ) && $_REQUEST['order'] && isset( $_REQUEST['orderby'] ) ) {
            $order = $_REQUEST['order'];

            switch ( $_REQUEST['orderby'] ) {
                case 'home_spotlight':
                    $str = " pt.".Apollo_DB_Schema::_IS_HOME_SPOTLIGHT." ". $order . ', pt.term_id '. $order;
                break;

                case 'category_spotlight':
                    $str = " pt.".Apollo_DB_Schema::_IS_CATEGORY_SPOTLIGHT." ". $order . ', pt.term_id '. $order;
                break;

                case 'category_featured':
                    $str = " pt.".Apollo_DB_Schema::_IS_CATEGORY_FEATURE." ". $order . ', pt.term_id '. $order;
                break;

                case 'org':
                    $str = " org.post_title IS NULL ". $order .", org.post_title ".$order;
                break;

                case 'venue':
                    $str = " venue_sort.post_title IS NULL ". $order .", venue_sort.post_title ".$order;
                    break;

                case 'primary-type':
                    $str = " pri_term.name IS NULL ". $order .", pri_term.name ".$order;
                    break;

                case 'event-type':
                    $str = " term_sortable.term_name IS NULL ". $order .", term_sortable.term_name ".$order;
                    break;

                case 'visited_amount':
                    $str = " vstTbl.count ". $order;
                break;

                case 'start_date':
                    $str = " mt_start_d.meta_value ". $order;
                    break;
                case 'end_date':
                    $str = " mt_end_d.meta_value ". $order;
                    break;
                case 'name':
                    $str = " ".$wpdb->posts.".post_title ". $order;
                    break;
                case 'username':
                    $str = " ".$wpdb->posts.".post_author IS NULL ". $order .", ".$wpdb->posts.".post_author ".$order;
                    break;
            }

        }

        return $str;
    }

    /**
     * Override Group by
     *
     * @access public
     * @return string
     */
    public function change_group_by( $groupby ) {
        global $wpdb;
        return "$wpdb->posts.ID";
    }

    function move_custom_button(){
        global $current_screen;
        // Not our post type, exit earlier
        if( Apollo_DB_Schema::_EVENT_PT != $current_screen->post_type || ! isset( $_REQUEST['dm'] ) )
            return;

        ?>
        <script type="text/javascript">
            jQuery(document).ready( function($)
            {
                $('.tablenav').append('<input type="hidden" name="event_ids" />\n\
                <input type="hidden" name="chb_home_spot" />\n\
                <input type="hidden" name="tab_location" />\n\
                <input type="hidden" name="tab_features" />\n\
                <input type="hidden" name="chb__is_category_spotlight" />\n\
                <input type="hidden" name="chb__is_category_feature" />');

                $('<div class="alignright save-btn"><input type="submit" name="save_event_opts"  class="button button-primary button-large" value="<?php _e( 'Save', 'apollo' ) ?>" /></div>').insertBefore('.tablenav .tablenav-pages');
                $('<br class="clear">').insertBefore('.tablenav');

            });
        </script>
        <?php
    }

    /**
     * Remove all meta data of this post if it is deleted
     */
    public function remove_meta_data( $post_id ) {
        global $wpdb;
        if(empty($post_id)) return;
        //delete apollo_event_metadata
        $apl_query = new Apl_Query( $this->meta_tbl, TRUE );
        $apl_query->delete( "apollo_".$this->type."_id = ".$post_id."" );
        //delete apollo_event_org
        $apl_query = new Apl_Query( $wpdb->{Apollo_Tables::_APL_EVENT_ORG} , TRUE );
        $apl_query->delete( "post_id = ".$post_id."" );
        //delete apollo_event_calendar
        $apl_query = new Apl_Query( $wpdb->{Apollo_Tables::_APL_EVENT_CALENDAR} , TRUE );
        $apl_query->delete( "event_id = ".$post_id."" );
        //delete apollo_event_rating
        $apl_query = new Apl_Query( $wpdb->{Apollo_Tables::_APL_EVENT_RATING} , TRUE );
        $apl_query->delete( "item_id = ".$post_id." AND item_type = '".$this->type."' " );
    }

    private function includePastEvents()
    {
        return isset($_GET['past_event']);
    }

    /**
     * @ticket #19030: Add status "unconfirmed" for event
     * @param $post_states
     * @param $post
     * @return mixed
     */
    public function display_post_states ($post_states, $post) {
        $post_status = isset( $_REQUEST['post_status'] ) ? $_REQUEST['post_status'] : '';

        /**
         * It show the post status "unconfirmed" on tab "All" and hide on tab "Unconfirmed"
         */
        if ($this->type == Apollo_DB_Schema::_EVENT_PT &&
            'unconfirmed' == $post->post_status && 'unconfirmed' != $post_status )
        {
            $post_states['unconfirmed'] = __('Unconfirmed', 'post status');
        }
        return $post_states;
    }
}

endif;

$aplAdminEvent = new Apollo_Admin_Event();

add_action('admin_action_copy_event_post_save_as_new_post', array( $aplAdminEvent ,'copy_event_post_save_as_new_post') );
add_action('admin_action_copy_event_post_save_as_new_post_draft', array( $aplAdminEvent , 'copy_event_post_save_as_new_post_draft') );

