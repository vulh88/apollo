<?php
/**
 * Admin functions for the public art post type
 *
 * @author 		hong
 * @category 	admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Apollo_Admin_CPT' ) ) {
    include( 'class-apollo-admin-cpt.php' );
}

if ( ! class_exists( 'Apollo_Admin_Public_Art' ) ) :
    
class Apollo_Admin_Public_Art extends Apollo_Admin_CPT {
    
    public function __construct() {
        
        global $wpdb;
        $this->type = Apollo_DB_Schema::_PUBLIC_ART_PT;
        $this->meta_tbl  = $wpdb->{Apollo_Tables::_APL_PUBLIC_ART_META};

        $this->requireds = array(
            'post_title'    => array(
                'label'     => __( 'The Artwork name', 'apollo' ),
                'validate'  => 'required',
            ),
            'content'    => array(
                'label'     => __( 'The content', 'apollo' ),
                'validate'  => 'required',
            ),
            ''.Apollo_DB_Schema::_APL_PUBLIC_ART_DATA.'['.Apollo_DB_Schema::_PUBLIC_ART_LATITUDE.']' => array(
            'label'     => __( 'The Latitude', 'apollo' ),
            'validate'  => 'lat_lng_related',
            ),
            ''.Apollo_DB_Schema::_APL_PUBLIC_ART_DATA.'['.Apollo_DB_Schema::_PUBLIC_ART_LONGITUDE.']' => array(
                'label'     => __( 'The Longitude', 'apollo' ),
                'validate'  => 'lat_lng_related',
            ),

        );

        // Post title fields
        add_filter( 'enter_title_here', array( $this, 'enter_title_here' ), 1, 2 );

        // Admin column
        add_filter( 'manage_edit-'.$this->type.'_columns', array( $this, 'edit_columns' ) );
        add_action( 'manage_'.$this->type.'_posts_custom_column', array( $this, 'custom_columns' ), 2 );
        add_filter( 'manage_edit-'.$this->type.'_sortable_columns', array( $this, 'custom_columns_sort' ) );

        // Bulk / Quick edit
        /** @Ticket #14398 */
        /** @Ticket #14443 */
        add_action( 'quick_edit_custom_box', array( $this, 'quick_edit' ), 10, 2 );
        add_action( 'save_post', array( $this, 'bulk_and_quick_edit_save_post' ), 10, 2 );

        /// Event filtering
        add_action( 'restrict_manage_posts', array( $this, '_filters' ) );

        add_action( 'delete_post', array( $this, 'delete_post' ) );

        /** @Ticket #13028 */
        add_filter('posts_clauses', array( $this, 'custom_query_posts' ) );
        
        parent::__construct();
    }
    
    public function delete_post( $post_id ) {
        if(empty($post_id)) return;
        global $wpdb;
        //delete apollo_artist_public_art
        $apl_query = new Apl_Query( $wpdb->{Apollo_Tables::_APL_ARTIST_PUBLIC_ART} , TRUE );
        $apl_query->delete( " post_id = ".$post_id." " );
    }
    
    public function _filters() {
        global $typenow;
        
        if ( $typenow !== $this->type ) {
            return ;
        }
        
        apollo_dropdown_categories( $this->type. '-type' );
    }
    
     /**
     * Custom Quick Edit form
     * 
     * @access public
     * @param mixed $column_type
	 * @param mixed $post_type
    */
    public function quick_edit( $column_type, $post_type ) {
        
        if ( $column_type != 'name' ) {
            return;
        }     
    
        require_once APOLLO_ADMIN_DIR. '/views/public-art/html-quick-edit.php';
    }

    public function bulk_edit_save( $post_id, $public_art ) {
        
    }
    
    /**
     * Quick and Bulk saving
     * 
     * @access public
     * @param mixed $post_id
     * @param mixed $post
     */
    public function bulk_and_quick_edit_save_post( $post_id, $post ) {
       
        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}
        
        // Don't save revisions and autosaves
		if ( wp_is_post_revision( $post_id ) || wp_is_post_autosave( $post_id ) ) {
			return $post_id;
		}
        
        if ( $post->post_type != $this->type ) {
            return $post_id;
        }
        
        // Check user permission
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return $post_id;
		}
        
        $nonce = 'apollo_'.$this->type.'_bulk_edit_nonce';
        if ( isset( $_REQUEST[$nonce] ) && ! wp_verify_nonce( $_REQUEST[$nonce], $nonce ) ) {
			return $post_id;
		}
        
        $post = get_public_art( $post );
        
        if ( ! empty( $_REQUEST['apollo_'.$this->type.'_quick_edit'] ) ) {
           
			$this->quick_edit_save( $post_id, $post );
		} else {
			$this->bulk_edit_save( $post_id, $post );
		}

		return $post_id;
    }
    
    /**
	 * Change the columns shown in admin.
	 */
	public function edit_columns( $existing_columns ) {
       
		if ( empty( $existing_columns ) && ! is_array( $existing_columns ) ) {
            $existing_columns = array();
        }

		unset( $existing_columns['title'], $existing_columns['comments'], $existing_columns['date'] );

		$columns = array();
		$columns["cb"] = "<input type=\"checkbox\" />";
		$columns['D'] = __( 'Dup', 'apollo' );
		$columns["name"] = __( 'Title', 'apollo' );
        $columns["city"] = __( 'City', 'apollo' );
        $columns["state"] = __( 'State', 'apollo' );
        $columns["zip"] = __( 'Zip', 'apollo' );
        $columns["public-art-type"] = __( 'Public Art Category(s)', 'apollo' );
        $columns["date"] = __( 'Posted date', 'apollo' );

        return array_merge( $columns, $existing_columns );
	}

    public function enter_title_here( $text, $post ) {
        if ( $post->post_type == Apollo_DB_Schema::_PUBLIC_ART_PT ) {
            return __( 'Artwork name', 'apollo' );
        }

        return $text;
    }

    /**
	 * Define our custom columns shown in admin.
	 * @param  string $column
	 */
	public function custom_columns( $column ) {
        global $post, $the_public_art;
        
        if ( empty( $the_public_art ) || $the_public_art->id != $post->ID ) {
            
            $the_public_art = get_public_art( $post );
        }
       
        
        switch ( $column ) {

            case "D" :
                $this->render_duplicate_icon($post);
            break;
            
            case "name" :
				$edit_link = get_edit_post_link( $post->ID );
				$title = _draft_or_post_title();
				$post_type_object = get_post_type_object( $post->post_type );
				$can_edit_post = current_user_can( $post_type_object->cap->edit_post, $post->ID );

				echo '<strong><a class="row-title" href="' . esc_url( $edit_link ) .'">' . $title.'</a>';

				_post_states( $post );

				echo '</strong>';

				if ( $post->post_parent > 0 )
					echo '&nbsp;&nbsp;&larr; <a href="'. get_edit_post_link($post->post_parent) .'">'. get_the_title($post->post_parent) .'</a>';

				// Excerpt view
				if (isset($_GET['mode']) && $_GET['mode']=='excerpt') echo apply_filters('the_excerpt', $post->post_excerpt);

				// Get actions
				$actions = array();

				$actions['id'] = 'ID: ' . $post->ID;

				if ( $can_edit_post && 'trash' != $post->post_status ) {
					$actions['edit'] = '<a href="' . get_edit_post_link( $post->ID, true ) . '" title="' . esc_attr( __( 'Edit this item', 'apollo' ) ) . '">' . __( 'Edit', 'apollo' ) . '</a>';
					$actions['inline hide-if-no-js'] = '<a href="#" class="editinline" title="' . esc_attr( __( 'Edit this item inline', 'apollo' ) ) . '">' . __( 'Quick&nbsp;Edit', 'apollo' ) . '</a>';
				}
				if ( current_user_can( $post_type_object->cap->delete_post, $post->ID ) ) {
					if ( 'trash' == $post->post_status )
						$actions['untrash'] = "<a title='" . esc_attr( __( 'Restore this item from the Trash', 'apollo' ) ) . "' href='" . wp_nonce_url( admin_url( sprintf( $post_type_object->_edit_link . '&amp;action=untrash', $post->ID ) ), 'untrash-post_' . $post->ID ) . "'>" . __( 'Restore', 'apollo' ) . "</a>";
					elseif ( EMPTY_TRASH_DAYS )
						$actions['trash'] = "<a class='submitdelete' title='" . esc_attr( __( 'Move this item to the Trash', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID ) . "'>" . __( 'Trash', 'apollo' ) . "</a>";
					if ( 'trash' == $post->post_status || !EMPTY_TRASH_DAYS )
						$actions['delete'] = "<a class='submitdelete' title='" . esc_attr( __( 'Delete this item permanently', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID, '', true ) . "'>" . __( 'Delete Permanently', 'apollo' ) . "</a>";
				}
				if ( $post_type_object->public ) {
					if ( in_array( $post->post_status, array( 'pending', 'draft', 'future' ) ) ) {
						if ( $can_edit_post )
							$actions['view'] = '<a href="' . esc_url( add_query_arg( 'preview', 'true', get_permalink( $post->ID ) ) ) . '" title="' . esc_attr( sprintf( __( 'Preview &#8220;%s&#8221;', 'apollo' ), $title ) ) . '" rel="permalink">' . __( 'Preview', 'apollo' ) . '</a>';
					} elseif ( 'trash' != $post->post_status ) {
						$actions['view'] = '<a href="' . get_permalink( $post->ID ) . '" title="' . esc_attr( sprintf( __( 'View &#8220;%s&#8221;', 'apollo' ), $title ) ) . '" rel="permalink">' . __( 'View', 'apollo' ) . '</a>';
					}
				}

				$actions = apply_filters( 'post_row_actions', $actions, $post );

				echo '<div class="row-actions">';

				$i = 0;
				$action_count = sizeof($actions);

				foreach ( $actions as $action => $link ) {
					++$i;
					( $i == $action_count ) ? $sep = '' : $sep = ' | ';
					echo "<span class='$action'>$link$sep</span>";
				}
				echo '</div>';
                get_inline_data( $post );
                
                
                $_a_data = array(
                    Apollo_DB_Schema::_PUBLIC_ART_CREATED_DATE,
                    Apollo_DB_Schema::_PUBLIC_ART_CONTACT_NAME,
                    Apollo_DB_Schema::_PUBLIC_ART_CONTACT_PHONE,
                    Apollo_DB_Schema::_PUBLIC_ART_CONTACT_EMAIL,
                    Apollo_DB_Schema::_PUBLIC_ART_WEBSITE_URL,
                    Apollo_DB_Schema::_PUBLIC_ART_DIMENSION,
                    Apollo_DB_Schema::_PUBLIC_ART_LATITUDE,
                    Apollo_DB_Schema::_PUBLIC_ART_LONGITUDE,

                );
                
                $_a_address = array(
                    Apollo_DB_Schema::_PUBLIC_ART_ADDRESS,
                    Apollo_DB_Schema::_PUBLIC_ART_CITY,
                    Apollo_DB_Schema::_PUBLIC_ART_STATE,
                    Apollo_DB_Schema::_PUBLIC_ART_ZIP,
                    Apollo_DB_Schema::_PUBLIC_ART_REGION,
                );
                $commuicate_js_str = '';
                foreach ( $_a_data as $k ) {
                    $commuicate_js_str .= '<div class="'.Apollo_DB_Schema::_APL_PUBLIC_ART_DATA.'['.$k.']">' . $the_public_art->get_meta_data( Apollo_DB_Schema::_APL_PUBLIC_ART_DATA. '['. $k .']' ) . '</div>';
                }
                
                foreach ( $_a_address as $k ) {
                    $commuicate_js_str .= '<div class="'.Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS.'['.$k.']">' . $the_public_art->get_meta_data( Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS. '['. $k .']' ) . '</div>';
                }
                
                echo '
					<div class="hidden" id="_inline_' . $post->ID . '">
						<div class="menu_order">' . $post->menu_order . '</div>
						'.$commuicate_js_str.'
					</div>
				';
                
			break;

            case 'city':
                $val = Apollo_App::apollo_get_meta_data( $the_public_art->id, Apollo_DB_Schema::_PUBLIC_ART_CITY, Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS );
                echo $val ? $val : '-';
            break;
        
            case 'state':
                $val = Apollo_App::apollo_get_meta_data( $the_public_art->id, Apollo_DB_Schema::_PUBLIC_ART_STATE, Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS );
                echo $val ? $val : '-';
            break;

            case 'zip':
                $val = Apollo_App::apollo_get_meta_data( $the_public_art->id, Apollo_DB_Schema::_PUBLIC_ART_ZIP, Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS );
                echo $val ? $val : '-';
                break;

            case "public-art-type" :
				if ( ! $terms = get_the_terms( $post->ID, $column ) ) {
					echo '<span class="na">&ndash;</span>';
				} else {
                    $termlist = array();
					foreach ( $terms as $term ) {
                        if ( $term->parent ) continue;  // Not display sub menu in the list event page
                        
						$termlist[] = '<a href="' . admin_url( 'edit.php?' . $column . '=' . $term->slug . '&post_type='.$this->type.'' ) . ' ">' . $term->name . '</a>';
					}
					echo $termlist ? implode( ', ', $termlist ) : '<span class="na">&ndash;</span>';
				}
			break;

        }
        
    }
    
    /**
	 * Make columns sortable
	 *
	 * @access public
	 * @param mixed $columns
	 * @return array
	 */
	public function custom_columns_sort( $columns ) {
		$custom = array(
            'name'               => 'title',
            'phone'               => 'phone',
            'city'               => 'city',
            'state'               => 'state',
            'zip'               => 'zip',
		);
		return wp_parse_args( $custom, $columns );
	}
    
    private function quick_edit_save( $post_id, $post ) {
        require_once 'meta-boxes/public-art/class-apollo-meta-box-public-art-address.php';
        $public_art = get_public_art( $post );


        // Check and save event data
        $data = Apollo_App::unserialize($public_art->get_meta_data(Apollo_DB_Schema::_APL_PUBLIC_ART_DATA));
        $input = $_POST[Apollo_DB_Schema::_APL_PUBLIC_ART_DATA];
        $data = $this->setQuickEditData($input, $data);
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_PUBLIC_ART_DATA, serialize( $data ) );
        do_action( 'apollo_'.$this->type.'_quick_edit_save', $post );
    }

    public function custom_query_posts($clauses) {
	    global $wpdb;
        if(!empty($_GET['orderby']) ) {
            $order = !empty($_GET['order']) ? $_GET['order'] : 'ASC';
            if ($_GET['orderby'] == 'city' || $_GET['orderby'] == 'state' || $_GET['orderby'] == 'zip' ) {
                switch ($_GET['orderby']) {
                    case 'city' :
                        $sort = Apollo_DB_Schema::_PUBLIC_ART_CITY;
                        break;
                    case 'state' :
                        $sort = Apollo_DB_Schema::_PUBLIC_ART_STATE;
                        break;
                    case 'zip' :
                        $sort = Apollo_DB_Schema::_PUBLIC_ART_ZIP;
                        break;
                    default :
                        $sort = Apollo_DB_Schema::_PUBLIC_ART_CITY;
                        break;
                }
                $clauses['join'] = " LEFT JOIN " . $this->meta_tbl . " AS mt ON mt.apollo_public_art_id=". $wpdb->posts .".ID AND mt.meta_key='" . $sort . "' ";
                $clauses['orderby'] = " mt.meta_value " . $order;
            }
        }
        return $clauses;
    }
    
}    
    
endif;    

return new Apollo_Admin_Public_Art();