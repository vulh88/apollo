<?php
/**
 * Admin functions for the spotlight post type
 *
 * @author 		vulh
 * @category 	admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Apollo_Admin_CPT' ) ) {
    include( 'class-apollo-admin-cpt.php' );
}

if ( ! class_exists( 'Apollo_Admin_Spotlight' ) ) :
    
class Apollo_Admin_Spotlight extends Apollo_Admin_CPT {
    
    public function __construct() {
        
        $this->type = Apollo_DB_Schema::_SPOT_PT;
        
        // Featured image text
		add_filter( 'gettext', array( $this, 'image_gettext' ) );
        
        // Admin column
        add_filter( 'manage_edit-'.$this->type.'_columns', array( $this, 'edit_columns' ) );
        add_action( 'manage_'.$this->type.'_posts_custom_column', array( $this, 'custom_columns' ), 2 );
        add_filter( 'manage_edit-'.$this->type.'_sortable_columns', array( $this, 'custom_columns_sort' ) );

		add_action( 'restrict_manage_posts', array( $this, '_filters' ) );
        
        // Bulk / Quick edit
		/** @Ticket #14398 */
        /** @Ticket #14443 */
        add_action( 'quick_edit_custom_box', array( $this, 'quick_edit' ), 10, 2 );
        add_action( 'save_post', array( $this, 'bulk_and_quick_edit_save_post' ), 10, 2 );

        /** @Ticket #15546 */
        add_action('trash_post', array($this, 'remove_home_spotlight_cache', 10, 2));
        // Change meta table
        global $typenow, $pagenow;
        if ( $typenow == $this->type && $pagenow == 'edit.php' ) {
            add_filter( 'posts_orderby', array( $this, 'change_order_by' ) );  
        }

        // Enqueue custom option panel JS
        wp_enqueue_script( 'wp-color-picker' );
        wp_enqueue_style( 'wp-color-picker' );

        parent::__construct();
    }

    /***
     * @Ticket #15546 - Clear home spotlight cache
     * @param $post_id
     * @param $post
     */
    public function remove_home_spotlight_cache($post_id, $post) {
        if ($post->type == $this->type) {
            $homeSpotlight = aplc_instance('APLC_Inc_Files_HomeSpotlight');
            $homeSpotlight->remove();
        }
    }
     /**
     * Custom Quick Edit form
     * 
     * @access public
     * @param mixed $column_type
	 * @param mixed $post_type
    */
    public function quick_edit( $column_type, $post_type ) {
        
        if ( $column_type != 'name' ) {
            return;
        }     
    
        //require_once APOLLO_ADMIN_DIR. '/views/venue/html-quick-edit.php';
    }

    public function bulk_edit_save( $post_id, $venue ) {
        
    }
    
    /**
     * Quick and Bulk saving
     * 
     * @access public
     * @param mixed $post_id
     * @param mixed $post
     */
    public function bulk_and_quick_edit_save_post( $post_id, $post ) {
       
        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}
        
        // Don't save revisions and autosaves
		if ( wp_is_post_revision( $post_id ) || wp_is_post_autosave( $post_id ) ) {
			return $post_id;
		}
        
        if ( $post->post_type != $this->type ) {
            return $post_id;
        }
        
        // Check user permission
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return $post_id;
		}
        
        $nonce = 'apollo_'.$this->type.'_bulk_edit_nonce';
        if ( isset( $_REQUEST[$nonce] ) && ! wp_verify_nonce( $_REQUEST[$nonce], $nonce ) ) {
			return $post_id;
		}
        
        $post = get_venue( $post );
        
        if ( ! empty( $_REQUEST['apollo_'.$this->type.'_quick_edit'] ) ) {
           
			$this->quick_edit_save( $post_id, $post );
		} else {
			$this->bulk_edit_save( $post_id, $post );
		}

		/** @Ticket #15546 - Clear spotlight cache */
        $homeSpotlight = aplc_instance('APLC_Inc_Files_HomeSpotlight');
        $homeSpotlight->remove();
		return $post_id;
    }
    
    /**
	 * Change the columns shown in admin.
	 */
	public function edit_columns( $existing_columns ) {
       
		if ( empty( $existing_columns ) && ! is_array( $existing_columns ) ) {
            $existing_columns = array();
        }

		unset( $existing_columns['title'], $existing_columns['comments'], $existing_columns['date'] );

		$columns = array();
		$columns["cb"] = "<input type=\"checkbox\" />";
        $columns["thumb"] = __( 'Image', 'apollo' );
		$columns["name"] = __( 'Name', 'apollo' );
        
		return array_merge( $columns, $existing_columns );
	}
    
    /**
	 * Define our custom columns shown in admin.
	 * @param  string $column
	 */
	public function custom_columns( $column ) {
        global $post, $the_spot;
        
        if ( empty( $the_spot ) || $the_spot->id != $post->ID ) {
            
            $the_spot = get_spot( $post );
        }
       
        
        switch ( $column ) {
            
            case "thumb" :
                echo '<a href="' . get_edit_post_link( $post->ID ) . '">' . $the_spot->get_image() . '</a>';
			break;
            
            case "name" :
				$edit_link = get_edit_post_link( $post->ID );
				$title = _draft_or_post_title();
				$post_type_object = get_post_type_object( $post->post_type );
				$can_edit_post = current_user_can( $post_type_object->cap->edit_post, $post->ID );

				echo '<strong><a class="row-title" href="' . esc_url( $edit_link ) .'">' . $title.'</a>';

				_post_states( $post );

				echo '</strong>';

				if ( $post->post_parent > 0 )
					echo '&nbsp;&nbsp;&larr; <a href="'. get_edit_post_link($post->post_parent) .'">'. get_the_title($post->post_parent) .'</a>';

				// Excerpt view
				if (isset($_GET['mode']) && $_GET['mode']=='excerpt') echo apply_filters('the_excerpt', $post->post_excerpt);

				// Get actions
				$actions = array();

				$actions['id'] = 'ID: ' . $post->ID;

				if ( $can_edit_post && 'trash' != $post->post_status ) {
					$actions['edit'] = '<a href="' . get_edit_post_link( $post->ID, true ) . '" title="' . esc_attr( __( 'Edit this item', 'apollo' ) ) . '">' . __( 'Edit', 'apollo' ) . '</a>';
					$actions['inline hide-if-no-js'] = '<a href="#" class="editinline" title="' . esc_attr( __( 'Edit this item inline', 'apollo' ) ) . '">' . __( 'Quick&nbsp;Edit', 'apollo' ) . '</a>';
				}
				if ( current_user_can( $post_type_object->cap->delete_post, $post->ID ) ) {
					if ( 'trash' == $post->post_status )
						$actions['untrash'] = "<a title='" . esc_attr( __( 'Restore this item from the Trash', 'apollo' ) ) . "' href='" . wp_nonce_url( admin_url( sprintf( $post_type_object->_edit_link . '&amp;action=untrash', $post->ID ) ), 'untrash-post_' . $post->ID ) . "'>" . __( 'Restore', 'apollo' ) . "</a>";
					elseif ( EMPTY_TRASH_DAYS )
						$actions['trash'] = "<a class='submitdelete' title='" . esc_attr( __( 'Move this item to the Trash', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID ) . "'>" . __( 'Trash', 'apollo' ) . "</a>";
					if ( 'trash' == $post->post_status || !EMPTY_TRASH_DAYS )
						$actions['delete'] = "<a class='submitdelete' title='" . esc_attr( __( 'Delete this item permanently', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID, '', true ) . "'>" . __( 'Delete Permanently', 'apollo' ) . "</a>";
				}
				if ( $post_type_object->public ) {
					if ( in_array( $post->post_status, array( 'pending', 'draft', 'future' ) ) ) {
						if ( $can_edit_post )
							$actions['view'] = '<a href="' . esc_url( add_query_arg( 'preview', 'true', get_permalink( $post->ID ) ) ) . '" title="' . esc_attr( sprintf( __( 'Preview &#8220;%s&#8221;', 'apollo' ), $title ) ) . '" rel="permalink">' . __( 'Preview', 'apollo' ) . '</a>';
					} elseif ( 'trash' != $post->post_status ) {
						$actions['view'] = '<a href="' . get_permalink( $post->ID ) . '" title="' . esc_attr( sprintf( __( 'View &#8220;%s&#8221;', 'apollo' ), $title ) ) . '" rel="permalink">' . __( 'View', 'apollo' ) . '</a>';
					}
				}

				$actions = apply_filters( 'post_row_actions', $actions, $post );

				echo '<div class="row-actions">';

				$i = 0;
				$action_count = sizeof($actions);

				foreach ( $actions as $action => $link ) {
					++$i;
					( $i == $action_count ) ? $sep = '' : $sep = ' | ';
					echo "<span class='$action'>$link$sep</span>";
				}
				echo '</div>';
                get_inline_data( $post );
                
			break;
           
        }
        
    }
    
    /**
	 * Make columns sortable
	 *
	 * @access public
	 * @param mixed $columns
	 * @return array
	 */
	public function custom_columns_sort( $columns ) {
		$custom = array(
			'name' => 'name'
		);
		return wp_parse_args( $custom, $columns );
	}
    
    private function quick_edit_save( $post_id, $venue ) {
         
    }

	public function _filters() {
		$currentBlogID = get_current_blog_id();
		$optionCachedValue = Apollo_App::get_network_local_cache($currentBlogID);
		if(intval($optionCachedValue) === 1) {
			echo '<button id="empty-spotlight-cache" class="button">'.__('Refresh Home Page Slider','apollo').'</button>';
		}
	}
    
    /**
	 * Check if we're editing or adding
	 * @return boolean
	 */
	private function is_editing() {
        $type = Apollo_DB_Schema::_SPOT_PT;
		if ( ! empty( $_GET['post_type'] ) && $type == $_GET['post_type'] ) {
			return true;
		}
		if ( ! empty( $_GET['post'] ) && $type == get_post_type( $_GET['post'] ) ) {
			return true;
		}
		if ( ! empty( $_REQUEST['post_id'] ) && $type == get_post_type( $_REQUEST['post_id'] ) ) {
			return true;
		}
		return false;
	}
    
	public function image_gettext( $string = '' ) {
		if ( 'Featured Image' == $string && $this->is_editing() ) {
			$string = __( 'Spotlight Image', 'apollo' );
		} elseif ( 'Remove featured image' == $string && $this->is_editing() ) {
			$string = __( 'Remove Spotlight image', 'apollo' );
		} elseif ( 'Set featured image' == $string && $this->is_editing() ) {
			$string = __( 'Set Spotlight image', 'apollo' );
		}
		return $string;
	}
    
    public function change_order_by( $str ) {
        global $wpdb;
        return $wpdb->posts.'.menu_order ASC';
    }
}    
    
endif;    

return new Apollo_Admin_Spotlight();