<?php
/**
 * Admin functions for post types
 *
 * @author 		vulh
 * @category 	Admin
 * @package 	inc/Admin/Post Types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}	

if ( ! class_exists( 'Apollo_Admin_CPT' ) ) :

/**
 * Apollo_Admin_CPT Class
 */
class Apollo_Admin_CPT {

	public $type = '';
    
    public $meta_tbl = '';
    public $post_term_tbl  = '';
    public $requireds = array(); 
    
	/**
	 * Constructor
	 */
	public function __construct( $type = '' ) {
		// Insert into X media browser
		add_filter( 'media_view_strings', array( $this, 'change_insert_into_post' ) );
        
        // Filter count num events
        add_action('admin_footer', array( $this, 'jquery_remove_counts' ));
        
        add_action( 'admin_notices', array( $this, 'post_error_admin_message' ) );
        
        add_action( 'delete_post', array( $this, 'remove_meta_data' ) );
        
        // Add custom require fields
        $custom_fields = Apollo_Custom_Field::get_required_fields_by_type( $this->type );
        
        if ( $custom_fields ) {
            foreach ( $custom_fields as $field ) {
                $validation = Apollo_Custom_Field::get_validation($field);
                
                $validationObj = array();
                
                if ($validation) $validationObj[] = $validation;
                if ( $field->required == 1 ) $validationObj[] = 'required';
                
                $this->requireds[''.Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA.'['.$field->name.']'] =  array(
                    'label' => $field->label,
                    'validate'  => implode(',', $validationObj ),
                );
            }
        }

        /*@ticket #16642 [CF] 20180713 - Remove the associated author when do not select an any author.*/
        add_action( 'post_updated', array($this, 'post_updated'), 10, 3 );
	}

    /**
     * @ticket #16642 [CF] 20180713 - Remove the associated author when do not select an any author.
     * Doesn't update post author for organization, venue, artist
     * @param $post_ID
     * @param $post_after
     * @param $post_before
     */
    public function post_updated($post_ID, $post_after, $post_before) {
        global $wpdb;

        $postTypes = [Apollo_DB_Schema::_VENUE_PT, Apollo_DB_Schema::_ORGANIZATION_PT, Apollo_DB_Schema::_ARTIST_PT];

        if(in_array($post_after->post_type, $postTypes)
            && $post_after->post_author !== $post_before->post_author
            && isset($_POST['action']) && $_POST['action'] == 'inline-save'){

            $dataUpdate = array(
                'post_author' => $post_before->post_author
            );
            $where = array(
                'ID' => $post_ID
            );
            $wpdb->update($wpdb->posts, $dataUpdate, $where);
        }
    }

    public function isAdvancedSearch() {
        return isset($_GET['advanced']);
    }

    /**
     * Remove all meta data of this post if it is deleted
     */
    function remove_meta_data( $post_id ) {
        $apl_query = new Apl_Query( $this->meta_tbl, TRUE );
        $apl_query->delete( "apollo_".$this->type."_id = $post_id" );
    }
    
    function post_error_admin_message() {
        $requires = $this->requireds;
        $required_error = __( '%s is required', 'apollo' );
        $valid_error = __( '%s is invalid', 'apollo' );
        
        echo '<div id="apollo-notice" class="error hidden"><ul>';
            foreach( $requires as $name => $data ):
                
                if ( $name == 'post_cat' ) {
                    ?>
                    <script>
                        jQuery(function() {
                            jQuery( '#<?php echo $this->type ?>-typechecklist' ).addClass( 'cat_required' );
                        });
                    </script>
                    <?php
                }
                
                if ( $name == 'post_title' ) {
                    ?>
                    <script>
                        jQuery(function() {
                            jQuery( '#title' ).addClass( 'required' );
                        });
                    </script>
                    <?php
                }
                
                if ( $name == 'post_excerpt' ) {
                    ?>
                    <script>
                        jQuery(function() {
                            jQuery( '#excerpt' ).addClass( 'required-editor' );
                        });
                    </script>
                    <?php
                }
                
                if ( $name == 'post_content' ) {
                    ?>
                    <script>
                        jQuery(function() {
                            jQuery( '#content' ).addClass( 'required-editor' );
                        });
                    </script>
                    <?php
                }
                
                $validates = explode( ',' , $data['validate'] );
                foreach ( $validates as $v ):
                    echo '<li class="hidden" data-id="'.$name.'-'.str_replace( ' ', '', $v ) .'">';
                        switch( $v ):
                            case 'required':
                                echo sprintf( $required_error, $data['label'] );
                            break;
                            case 'required_editor':
                                echo sprintf( $required_error, $data['label'] );
                            break;
                            case 'url':
                                echo sprintf( $valid_error, $data['label'] );
                            break;
                            case 'youtube_url':
                                echo sprintf( $valid_error, $data['label'] );
                            break;
                            case 'audio_embed':
                                echo sprintf( $valid_error, $data['label'] );
                             break;
                            case 'email':
                                echo sprintf( $valid_error, $data['label'] );
                            break;
                            case 'number':
                                echo sprintf( $valid_error, $data['label'] );
                                break;
                            case 'greater':
                                _e( 'The end date must be greater than the start date and one of them must be greater than or equal today', 'apollo' );
                                break;
                            case 'larger_than_now':
                                _e( $data['label'].' must be greater than today', 'apollo' );
                            break;
                            case 'lat_lng_related':
                                _e( 'latitude and longitude are empty Or entered both', 'apollo' );
                                break;
                            case 'maximum_year':
                                $maximumyear = Apollo_DB_Schema::_APL_EVENT_MAXIMUM_YEAR;
                                echo sprintf(__( 'Events may not last more than %s years. Please revise your end date to less than %s years.', 'apollo' ),$maximumyear,$maximumyear);
                        endswitch;
                    echo '</li>';
                endforeach;
            endforeach;
        echo '</ul></div>';        

    }
    
    public function jquery_remove_counts() {
        
        ?>
        <script type="text/javascript">
        jQuery(function(){
            jQuery("li.all").find("span.count").remove();
            jQuery("li.publish").find("span.count").remove();
            jQuery("li.draft").find("span.count").remove();
            jQuery("li.pending").find("span.count").remove();
            jQuery("li.private").find("span.count").remove();
            jQuery("li.trash").find("span.count").remove();
        });
        </script>
        <?php
    }
    
	/**
	 * Change label for insert buttons.
	 * @access   public
	 * @param array $strings
	 * @return array
	 */
	function change_insert_into_post( $strings ) {
		global $post_type;

		if ( $post_type == $this->type ) {
			$obj = get_post_type_object( $this->type );

			$strings['insertIntoPost']     = sprintf( __( 'Insert into %s', 'apollo' ), $obj->labels->singular_name );
		}
        
		return $strings;
	}

    // Not re sorting categories
    function taxonomy_checklist_checked_ontop_filter ($args) {
        $args['checked_ontop'] = false;
        return $args;
    }
    
    /**
     * Quick and Bulk saving
     * 
     * @access public
     * @param mixed $post_id
     * @param mixed $post
     */
    public function bulk_and_quick_edit_save_post( $post_id, $post ) {
       
        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}
        
        // Don't save revisions and autosaves
		if ( wp_is_post_revision( $post_id ) || wp_is_post_autosave( $post_id ) ) {
			return $post_id;
		}
        
        if ( $post->post_type != $this->type ) {
            return $post_id;
        }
        
        // Check user permission
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return $post_id;
		}
        
        $nonce = 'apollo_'.$this->type.'_bulk_edit_nonce';
        if ( isset( $_REQUEST[$nonce] ) && ! wp_verify_nonce( $_REQUEST[$nonce], $nonce ) ) {
			return $post_id;
		}
        
        $post = get_venue( $post );
        
        if ( ! empty( $_REQUEST['apollo_'.$this->type.'_quick_edit'] ) ) {
           
			$this->quick_edit_save( $post_id, $post );
		} else {
			$this->bulk_edit_save( $post_id, $post );
		}

		return $post_id;
    }
    
     /**
     * Custom Quick Edit form
     * 
     * @access public
     * @param mixed $column_type
	 * @param mixed $post_type
    */
    public function quick_edit( $column_type, $post_type ) {
        
        if ( $column_type != 'name' ) {
            return;
        }     
    
        require_once APOLLO_ADMIN_DIR. '/views/'.$this->type.'/html-quick-edit.php';
    }
    
    public function save_date_range( $start_k, $end_k, $post_id ) {
        $has_error_date = false;
        if ( isset( $_REQUEST[$start_k] ) && $_REQUEST[$start_k] 
            && isset( $_REQUEST[$end_k] ) && $_REQUEST[$end_k] ) {
            
            $start_time = explode('-', $_REQUEST[$start_k]);
            $end_time   = explode('-', $_REQUEST[$end_k]);
            
            $has_error_date = mktime( $start_time[1], $start_time[2], $start_time[0] ) 
                > mktime( $end_time[1], $end_time[2], $end_time[0] ) ;
            
        }
        
        if ( ! $has_error_date ) {
            
            if ( isset( $_REQUEST[$start_k] ) ) {
                update_apollo_meta( $post_id, $start_k, stripslashes( sanitize_text_field( $_REQUEST[$start_k] ) ) );
            }

            if ( isset( $_REQUEST[$end_k] ) ) {
                update_apollo_meta( $post_id, $end_k, stripslashes( sanitize_text_field( $_REQUEST[$end_k] ) ) );
            }
        }
    }
    
    public function render_associated_users($blog_users, $post_id) {
        if ( $blog_users ):
        ?>
        <a href="#TB_inline?height=100&amp;width=400&amp;inlineId=panel-users-<?php echo $post_id ?>" title="<?php echo _draft_or_post_title() ?>" class="thickbox" ><?php _e( 'View Users', 'apollo' ) ?></a>
        <div id="panel-users-<?php echo $post_id ?>" style="display:none">
            <ul>
                <?php foreach ( $blog_users as $u ): 
                    $user = get_user_by( 'id', $u->user_id );
                ?>
                <li><a target="_BLANK" href="<?php echo get_edit_user_link( $u->user_id ) ?>"><?php  echo $user->user_nicename ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <?php
        else:
            echo '-';
        endif;
    }
    
    protected function render_duplicate_icon($post, $dupe_type = 'ven') {
        global $wpdb;
        
        $querystr = "SELECT * from ".$wpdb->prefix."apollo_duplicates where dupe_type = '".$dupe_type."' and post_id_1 = ".$post->ID." limit 1";
        $dupe = $wpdb->get_results($querystr, OBJECT);
        if ( count($dupe) > 0 ){
            $img = '<i title="'.__("Duplicates", "apollo").'" class="has fa fa-files-o"></i>';
            echo "<a href=\"".home_url()."/dupe_window.php?post=".$post->ID."\" onclick=\"javascript:window.open(this.href, 'duplicate','left=220,top=120,width=600,height=350,toolbar=0,resizable=0'); return false;\" >$img</a>";
        } else {  
            echo '<i title="'.__("None Duplicate", "apollo").'" class="none fa fa-files-o"></i>';
        }
    }

    /**
     * Set quick edit data
     * @param $inputData
     * @param $rootData
     * @return array
     */
    public function setQuickEditData($inputData, $rootData)
    {

        if ($inputData) {
            foreach ($inputData as $key => $value) {
                if (isset($rootData[$key])) {
                    $rootData[$key] = $value;
                }
            }
        }
        return $rootData;
    }
}

endif;
