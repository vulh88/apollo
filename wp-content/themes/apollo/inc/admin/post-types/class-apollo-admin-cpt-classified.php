<?php
/**
 * Admin functions for the classified post type
 *
 * @author 		hong
 * @category 	admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Apollo_Admin_CPT' ) ) {
    include( 'class-apollo-admin-cpt.php' );
}

if ( ! class_exists( 'Apollo_Admin_Classified' ) ) :
    
class Apollo_Admin_Classified extends Apollo_Admin_CPT {
    
    public function __construct() {
        
        global $wpdb;
        $this->type = Apollo_DB_Schema::_CLASSIFIED;
        $this->meta_tbl  = $wpdb->{Apollo_Tables::_APL_CLASSIFIED_META};
        /**
         * @ticket #19641: [CF] 20190402 - [Classified] Add the Associated User(s) field to main admin list and detail form - Item 5
         */
        $this->associationUserModuleListComp = apl_instance('APL_Lib_Association_Components_AssociationUsersModuleList');
        
        $this->requireds = array(
            'post_title'    => array(
                'label'     => __( 'The Classified name', 'apollo' ),
                'validate'  => 'required',
            ),

            'content'    => array(
                'label'     => __( 'The content', 'apollo' ),
                'validate'  => 'required',
            ),

            //address

            ''.Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS.'['.Apollo_DB_Schema::_CLASSIFIED_CITY.']' => array(
                'label'     => __( 'The City', 'apollo' ),
                'validate'  => 'required'
            ),
            ''.Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS.'['.Apollo_DB_Schema::_CLASSIFIED_STATE.']' => array(
                'label'     => __( 'The State', 'apollo' ),
                'validate'  => 'required'
            ),

            //data
            ''.Apollo_DB_Schema::_APL_CLASSIFIED_DATA.'['.Apollo_DB_Schema::_CLASSIFIED_EMAIL.']' => array(
                'label'     => __( 'The Email', 'apollo' ), 
                'validate'  => 'email,required'
            ),


            ''.Apollo_DB_Schema::_APL_CLASSIFIED_DATA.'['.Apollo_DB_Schema::_CLASSIFIED_WEBSITE_URL.']' => array(
                'label'     => __( 'The Website', 'apollo' ), 
                'validate'  => 'url' 
            ),

            ''.Apollo_DB_Schema::_APL_CLASSIFIED_DATA.'['.Apollo_DB_Schema::_CLASSIFIED_EXP_DATE.']' => array(
                'label'     => __( 'The Expiry Date', 'apollo' ),
                'validate'  => 'required,larger_than_now'
            ),

            ''.Apollo_DB_Schema::_APL_CLASSIFIED_DATA.'['.Apollo_DB_Schema::_CLASSIFIED_DEADLINE_DATE.']' => array(
                'label'     => __( 'The Deadline Date', 'apollo' ),
                'validate'  => 'larger_than_now'
            ),


            ''.Apollo_DB_Schema::_APL_CLASSIFIED_DATA.'['.Apollo_DB_Schema::_CLASSIFIED_CONTACT_EMAIL.']' => array(
                'label'     => __( 'The Contact Email', 'apollo' ),
                'validate'  => 'email'
            ),

        ); 

        if(isset($_POST[Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS][Apollo_DB_Schema::_CLASSIFIED_TMP_CITY])
            && !empty($_POST[Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS][Apollo_DB_Schema::_CLASSIFIED_TMP_CITY])){
            unset($this->requireds[''.Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS.'['.Apollo_DB_Schema::_CLASSIFIED_CITY.']']);
        }

        // Admin column
        add_filter( 'manage_edit-'.$this->type.'_columns', array( $this, 'edit_columns' ) );
        add_action( 'manage_'.$this->type.'_posts_custom_column', array( $this, 'custom_columns' ), 2 );
        add_filter( 'manage_edit-'.$this->type.'_sortable_columns', array( $this, 'custom_columns_sort' ) );
        
        // Bulk / Quick edit
        /** @Ticket #14398 */
        /** @Ticket #14443 */
        add_action( 'quick_edit_custom_box', array( $this, 'quick_edit' ), 10, 2 );
        add_action( 'save_post', array( $this, 'bulk_and_quick_edit_save_post' ), 10, 2 );
        
        /// Event filtering
        add_action( 'restrict_manage_posts', array( $this, '_filters' ) );
        
        add_action( 'delete_post', array( $this, 'delete_post' ) );

        add_filter( 'posts_where', array( $this, 'posts_where' ) );

        add_filter('posts_request', array($this,'dump_request'));

        /** @Ticket #13028 */

        add_filter('posts_clauses', array( $this, 'custom_query_posts' ) );
        add_filter('posts_fields_request', array($this, 'posts_fields_request'));

        parent::__construct();
    }

    /**
     * Get org name or tmp org
     * @param $input
     * @return string
     */
    public function posts_fields_request ( $input ) {
        if ( isset($_GET['orderby']) && $_GET['orderby'] == Apollo_DB_Schema::_ORGANIZATION_PT ) {
            $input .= ", IF(po.post_title IS NULL, mt_tmp_org.meta_value, po.post_title) AS org_title ";
        }
        return $input;
    }

    public function dump_request($input)
    {
        $debugContentType = isset($_GET['is_debugging']);
        if($debugContentType){
            aplDebug($input,1);
        }
        return $input;
    }

    public function posts_where( $where ) {
        global $wpdb;
        $metaTbl = $wpdb->{Apollo_Tables::_APL_CLASSIFIED_META};
        $postTbl = $wpdb->posts;
        $today = current_time('Y-m-d');
        $filterByExpiredDate = false;
        $filterByStartAndToDate = false;
        if (isset($_GET['expired'])) {
            $filterByExpiredDate = true;
            $where .= " AND $postTbl.ID IN (
                SELECT apollo_classified_id FROM $metaTbl mt WHERE
                (   (mt.meta_key = '".Apollo_DB_Schema::_APL_CLASSIFIED_EXP_DATE."' AND mt.meta_value < '$today')
                    OR (mt.meta_key = '".Apollo_DB_Schema::_APL_CLASSIFIED_DATA."'
		                AND SUBSTRING_INDEX( SUBSTRING_INDEX(mt.meta_value,'_classified_exp_date\";s:10:\"',-1),'\";s:',1) < '$today') )
                ) ";


        }

        if ( (isset($_GET['start_date']) && !empty($_GET['start_date']))
            || (isset($_GET['end_date']) && !empty($_GET['end_date']))
        ) {
            $filterByStartAndToDate = true;
            $from = isset($_GET['start_date']) ? $_GET['start_date'] : '';
            $to = isset($_GET['end_date']) ? $_GET['end_date'] : '';

            $whereDate = " AND $postTbl.ID IN (
                SELECT apollo_classified_id FROM $metaTbl mt WHERE ((mt.meta_key = '%s' AND %s) OR (%s))
            ) ";

            if ($from && $to) {
                $exWhere = " mt.meta_value >= '$from' AND  mt.meta_value <= '$to' ";
                $whereCustomExpiredDate = " mt.meta_key = '".Apollo_DB_Schema::_APL_CLASSIFIED_DATA."'
		                AND SUBSTRING_INDEX( SUBSTRING_INDEX(mt.meta_value,'_classified_exp_date\";s:10:\"',-1),'\";s:',1) between '$from' AND '$to' ";
            } else if($from) {
                $exWhere = "  mt.meta_value >= '$from' ";
                $whereCustomExpiredDate = " mt.meta_key = '".Apollo_DB_Schema::_APL_CLASSIFIED_DATA."'
		                AND SUBSTRING_INDEX( SUBSTRING_INDEX(mt.meta_value,'_classified_exp_date\";s:10:\"',-1),'\";s:',1) >= '$from' ";
            } else if ($to) {
                $exWhere = " mt.meta_value <= '$to' ";
                $whereCustomExpiredDate = " mt.meta_key = '".Apollo_DB_Schema::_APL_CLASSIFIED_DATA."'
		                AND SUBSTRING_INDEX( SUBSTRING_INDEX(mt.meta_value,'_classified_exp_date\";s:10:\"',-1),'\";s:',1) <= '$to' ";
            }


            if(!empty($exWhere) && !empty($whereCustomExpiredDate)) {
                $where .= sprintf($whereDate, Apollo_DB_Schema::_APL_CLASSIFIED_EXP_DATE, $exWhere, $whereCustomExpiredDate);
            }
        }

        if(!$filterByExpiredDate && !$filterByStartAndToDate) {
            $where .= " AND $postTbl.ID IN (
                SELECT apollo_classified_id FROM $metaTbl mt WHERE
                (   (mt.meta_key = '".Apollo_DB_Schema::_APL_CLASSIFIED_EXP_DATE."' AND mt.meta_value >= '$today')
                    OR (mt.meta_key = '".Apollo_DB_Schema::_APL_CLASSIFIED_DATA."'
		                AND SUBSTRING_INDEX( SUBSTRING_INDEX(mt.meta_value,'_classified_exp_date\";s:10:\"',-1),'\";s:',1) >= '$today') )
                ) ";
        }
        return $where;
    }

    public function delete_post( $post_id ) {

    }

    public function _filters() {
        global $typenow;
        
        if ( $typenow !== $this->type ) {
            return ;
        }
        
        apollo_dropdown_categories( $this->type. '-type' );

        $this->_filterAdvanceSearch();
    }

    private function _filterAdvanceSearch()
    {
        if (!$this->isAdvancedSearch()) {
            return '';
        }

        $start = isset($_GET['start_date']) ? $_GET['start_date'] : '';
        $end   = isset($_GET['end_date']) ? $_GET['end_date'] : '';
        $expired = isset($_GET['expired']) ? 'checked' : '';

        echo '<span class="past-event-wrapper">';
        echo '<input '.$expired.' type="checkbox" value="1" name="expired" /><label>'.__('Expired', 'apollo').'&nbsp;&nbsp;</label>';
        echo '</span>';

        echo '<span class="wrapper-date-range">';
        echo '<input placeholder="'.__("From", "apollo").'" class="apl-filter" type="text" name="start_date" id="_apollo_event_start_date" value="'.$start.'" />';
        echo '</span>';

        echo '<span class="wrapper-date-range">';
        echo '<input placeholder="'.__("To", "apollo").'" class="apl-filter" type="text" name="end_date" id="_apollo_event_end_date" value="'.$end.'" />';
        echo '</span>';

        if ( isset( $_REQUEST['advanced'] ) && $_REQUEST['advanced'] ) {
            echo '<input name="advanced" type="hidden" value="true" />';
        }


    }

     /**
     * Custom Quick Edit form
     * 
     * @access public
     * @param mixed $column_type
	 * @param mixed $post_type
    */
    public function quick_edit( $column_type, $post_type ) {
        
        if ( $column_type != 'name' ) {
            return;
        }     
    
        require_once APOLLO_ADMIN_DIR. '/views/classified/html-quick-edit.php';
    }

    public function bulk_edit_save( $post_id, $venue ) {
        
    }
    
    /**
     * Quick and Bulk saving
     * 
     * @access public
     * @param mixed $post_id
     * @param mixed $post
     */
    public function bulk_and_quick_edit_save_post( $post_id, $post ) {
       
        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}
        
        // Don't save revisions and autosaves
		if ( wp_is_post_revision( $post_id ) || wp_is_post_autosave( $post_id ) ) {
			return $post_id;
		}
        
        if ( $post->post_type != $this->type ) {
            return $post_id;
        }
        
        // Check user permission
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return $post_id;
		}
        
        $nonce = 'apollo_'.$this->type.'_bulk_edit_nonce';
        if ( isset( $_REQUEST[$nonce] ) && ! wp_verify_nonce( $_REQUEST[$nonce], $nonce ) ) {
			return $post_id;
		}
        
        $post = get_classified( $post );
        
        if ( ! empty( $_REQUEST['apollo_'.$this->type.'_quick_edit'] ) ) {
           
			$this->quick_edit_save( $post_id, $post );
		} else {
			$this->bulk_edit_save( $post_id, $post );
		}

		return $post_id;
    }
    
    /**
	 * Change the columns shown in admin.
	 */
	public function edit_columns( $existing_columns ) {
       
		if ( empty( $existing_columns ) && ! is_array( $existing_columns ) ) {
            $existing_columns = array();
        }

		unset( $existing_columns['title'], $existing_columns['comments'], $existing_columns['date'] );

		$columns = array();
		$columns["cb"] = "<input type=\"checkbox\" />";
		$columns['D'] = __( 'Dup', 'apollo' );
		$columns["name"] = __( 'Title', 'apollo' );
        $columns["organization"] = __( 'Organization', 'apollo' );
        $columns["city"] = __( 'City', 'apollo' );
        $columns["date"] = __( ' Posted date', 'apollo' );
        $columns["expiration-date"] = __( 'Expiration date', 'apollo' );
        $columns["name"] = __( 'Name', 'apollo' );
        $columns["email"] = __( 'Email', 'apollo' );
        $columns["phone"] = __( 'Phone', 'apollo' );
        $columns["classified-type"] = __( 'Classified Category(s)', 'apollo' );
        $columns['username'] = __( 'Associated User(s)', 'apollo' );

		return array_merge( $columns, $existing_columns );
	}
    
    /**
	 * Define our custom columns shown in admin.
	 * @param  string $column
	 */
	public function custom_columns( $column ) {
        global $post, $the_classified;
        
        if ( empty( $the_classified ) || $the_classified->id != $post->ID ) {
            
            $the_classified = get_classified( $post );
        }
       

        switch ( $column ) {

            case "D" :
                $this->render_duplicate_icon($post);
            break;
            
            case "name" :
				$edit_link = get_edit_post_link( $post->ID );
				$title = _draft_or_post_title();
				$post_type_object = get_post_type_object( $post->post_type );
				$can_edit_post = current_user_can( $post_type_object->cap->edit_post, $post->ID );

				echo '<strong><a class="row-title" href="' . esc_url( $edit_link ) .'">' . $title.'</a>';

				_post_states( $post );

				echo '</strong>';

				if ( $post->post_parent > 0 )
					echo '&nbsp;&nbsp;&larr; <a href="'. get_edit_post_link($post->post_parent) .'">'. get_the_title($post->post_parent) .'</a>';

				// Excerpt view
				if (isset($_GET['mode']) && $_GET['mode']=='excerpt') echo apply_filters('the_excerpt', $post->post_excerpt);

				// Get actions
				$actions = array();

				$actions['id'] = 'ID: ' . $post->ID;

				if ( $can_edit_post && 'trash' != $post->post_status ) {
					$actions['edit'] = '<a href="' . get_edit_post_link( $post->ID, true ) . '" title="' . esc_attr( __( 'Edit this item', 'apollo' ) ) . '">' . __( 'Edit', 'apollo' ) . '</a>';
					$actions['inline hide-if-no-js'] = '<a href="#" class="editinline" title="' . esc_attr( __( 'Edit this item inline', 'apollo' ) ) . '">' . __( 'Quick&nbsp;Edit', 'apollo' ) . '</a>';
				}
				if ( current_user_can( $post_type_object->cap->delete_post, $post->ID ) ) {
					if ( 'trash' == $post->post_status )
						$actions['untrash'] = "<a title='" . esc_attr( __( 'Restore this item from the Trash', 'apollo' ) ) . "' href='" . wp_nonce_url( admin_url( sprintf( $post_type_object->_edit_link . '&amp;action=untrash', $post->ID ) ), 'untrash-post_' . $post->ID ) . "'>" . __( 'Restore', 'apollo' ) . "</a>";
					elseif ( EMPTY_TRASH_DAYS )
						$actions['trash'] = "<a class='submitdelete' title='" . esc_attr( __( 'Move this item to the Trash', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID ) . "'>" . __( 'Trash', 'apollo' ) . "</a>";
					if ( 'trash' == $post->post_status || !EMPTY_TRASH_DAYS )
						$actions['delete'] = "<a class='submitdelete' title='" . esc_attr( __( 'Delete this item permanently', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID, '', true ) . "'>" . __( 'Delete Permanently', 'apollo' ) . "</a>";
				}
				if ( $post_type_object->public ) {
					if ( in_array( $post->post_status, array( 'pending', 'draft', 'future' ) ) ) {
						if ( $can_edit_post )
							$actions['view'] = '<a href="' . esc_url( add_query_arg( 'preview', 'true', get_permalink( $post->ID ) ) ) . '" title="' . esc_attr( sprintf( __( 'Preview &#8220;%s&#8221;', 'apollo' ), $title ) ) . '" rel="permalink">' . __( 'Preview', 'apollo' ) . '</a>';
					} elseif ( 'trash' != $post->post_status ) {
						$actions['view'] = '<a href="' . get_permalink( $post->ID ) . '" title="' . esc_attr( sprintf( __( 'View &#8220;%s&#8221;', 'apollo' ), $title ) ) . '" rel="permalink">' . __( 'View', 'apollo' ) . '</a>';
					}
				}

				$actions = apply_filters( 'post_row_actions', $actions, $post );

				echo '<div class="row-actions">';

				$i = 0;
				$action_count = sizeof($actions);

				foreach ( $actions as $action => $link ) {
					++$i;
					( $i == $action_count ) ? $sep = '' : $sep = ' | ';
					echo "<span class='$action'>$link$sep</span>";
				}
				echo '</div>';
                get_inline_data( $post );


                $_a_data = array(
                    Apollo_DB_Schema::_CLASSIFIED_PHONE,
                    Apollo_DB_Schema::_CLASSIFIED_FAX,
                    Apollo_DB_Schema::_CLASSIFIED_EMAIL,
                    Apollo_DB_Schema::_CLASSIFIED_WEBSITE_URL,
                    Apollo_DB_Schema::_CLASSIFIED_EXP_DATE,
                    Apollo_DB_Schema::_CLASSIFIED_CONTACT_NAME,
                    Apollo_DB_Schema::_CLASSIFIED_CONTACT_PHONE,
                    Apollo_DB_Schema::_CLASSIFIED_CONTACT_EMAIL,
                );

                $_a_address = array(
                    Apollo_DB_Schema::_CLASSIFIED_ADDRESS,
                    Apollo_DB_Schema::_CLASSIFIED_CITY,
                    Apollo_DB_Schema::_CLASSIFIED_STATE,
                    Apollo_DB_Schema::_CLASSIFIED_REGION,
                );
                $commuicate_js_str = '';
                foreach ( $_a_data as $k ) {
                    $commuicate_js_str .= '<div class="'.Apollo_DB_Schema::_APL_CLASSIFIED_DATA.'['.$k.']">' . $the_classified->get_meta_data( Apollo_DB_Schema::_APL_CLASSIFIED_DATA. '['. $k .']' ) . '</div>';
                }

                foreach ( $_a_address as $k ) {
                    $commuicate_js_str .= '<div class="'.Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS.'['.$k.']">' . $the_classified->get_meta_data( Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS. '['. $k .']' ) . '</div>';
                }

                echo '
					<div class="hidden" id="_inline_' . $post->ID . '">
						<div class="menu_order">' . $post->menu_order . '</div>
						'.$commuicate_js_str.'
					</div>
				';
                
			break;

            case 'phone':
                $val = Apollo_App::apollo_get_meta_data( $the_classified->id, Apollo_DB_Schema::_CLASSIFIED_PHONE, Apollo_DB_Schema::_APL_CLASSIFIED_DATA );
                echo $val ? $val : '-';
                break;
            case 'organization':
                $org = $the_classified->classified_org_edit_post_link();
                echo $org;
                break;

            case 'expiration-date':
                $val = Apollo_App::apollo_get_meta_data( $the_classified->id, Apollo_DB_Schema::_CLASSIFIED_EXP_DATE, Apollo_DB_Schema::_APL_CLASSIFIED_DATA );
                echo $val ? $val : '-';
                break;

            case 'city':
                $val = Apollo_App::apollo_get_meta_data( $the_classified->id, Apollo_DB_Schema::_APL_CLASSIFIED_CITY);
                echo $val ? $val : '-';
            break;

            case 'email':
                $val = Apollo_App::apollo_get_meta_data( $the_classified->id, Apollo_DB_Schema::_CLASSIFIED_EMAIL, Apollo_DB_Schema::_APL_CLASSIFIED_DATA );
                echo $val ? $val : '-';
                break;

            case "classified-type" :
				if ( ! $terms = get_the_terms( $post->ID, $column ) ) {
					echo '<span class="na">&ndash;</span>';
				} else {
                    $termlist = array();
					foreach ( $terms as $term ) {
                        if ( $term->parent ) continue;  // Not display sub menu in the list event page
                        
						$termlist[] = '<a href="' . admin_url( 'edit.php?' . $column . '=' . $term->slug . '&post_type='.$this->type.'' ) . ' ">' . $term->name . '</a>';
					}
					echo $termlist ? implode( ', ', $termlist ) : '<span class="na">&ndash;</span>';
				}
			break;

            case "name" :
                $name = Apollo_App::apollo_get_meta_data($post->ID,Apollo_DB_Schema::_CLASSIFIED_NAME,Apollo_DB_Schema::_APL_CLASSIFIED_DATA);
                echo $name;
                break;
            case "phone" :
                $name = Apollo_App::apollo_get_meta_data($post->ID,Apollo_DB_Schema::_CLASSIFIED_PHONE,Apollo_DB_Schema::_APL_CLASSIFIED_DATA);
                echo $name;
                break;
            /**
             * @ticket #19641: [CF] 20190402 - [Classified] Add the Associated User(s) field to main admin list and detail form - Item 5
             */
            case 'username':
                $this->associationUserModuleListComp->render(array(
                    'id' => $post->ID,
                    'post_type' => Apollo_DB_Schema::_CLASSIFIED,
                ));
                break;

        }
        
    }
    
    /**
	 * Make columns sortable
	 *
	 * @access public
	 * @param mixed $columns
	 * @return array
	 */
	public function custom_columns_sort( $columns ) {
		$custom = array(
			'name'               => 'title',
            'phone'               => 'phone',
            'city'               => 'city',
            'organization'       => 'organization',
            'date'               => 'date',
            'expiration-date'    => 'expiration_date',
            'email'              => 'email'
		);
		return wp_parse_args( $custom, $columns );
	}
    
    private function quick_edit_save( $post_id, $post ) {
        
        $classified = get_classified( $post );

        $data = Apollo_App::unserialize($classified->get_meta_data(Apollo_DB_Schema::_APL_CLASSIFIED_DATA));

        // Check and save event data
        $inputData = $_POST[Apollo_DB_Schema::_APL_CLASSIFIED_DATA];

        $data = $this->setQuickEditData($inputData, $data);

        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_CLASSIFIED_DATA, serialize( $data ) );
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_CLASSIFIED_EXP_DATE, $data[Apollo_DB_Schema::_CLASSIFIED_EXP_DATE] );

        do_action( 'apollo_'.$this->type.'_quick_edit_save', $post );
    }

    public function custom_query_posts($clauses) {
	    global $wpdb;
        if( !empty($_GET['orderby'] )) {
            $order = !empty($_GET['order']) ? $_GET['order'] : 'ASC';
            switch ($_GET['orderby']) {
                case 'city' :
                    $clauses['join'] = " LEFT JOIN " . $this->meta_tbl . " AS mt ON mt.apollo_classified_id=".$wpdb->posts.".ID AND mt.meta_key='" . Apollo_DB_Schema::_APL_CLASSIFIED_CITY . "' ";
                    $clauses['orderby'] = " mt.meta_value " . $order;
                    break;
                case 'organization' :
                    $clauses['join'] = " 
                        LEFT JOIN " . $this->meta_tbl . " AS mt ON mt.apollo_classified_id=". $wpdb->posts .".ID AND mt.meta_key='" . Apollo_DB_Schema::_APOLLO_CLASSIFIED_ORGANIZATION . "'   
                        LEFT JOIN " . $wpdb->posts  . " AS po ON po.ID=mt.meta_value AND po.post_type='" . Apollo_DB_Schema::_ORGANIZATION_PT . "' 
                        LEFT JOIN " . $this->meta_tbl . " AS mt_tmp_org ON mt_tmp_org.apollo_classified_id=". $wpdb->posts .".ID AND mt_tmp_org.meta_key='". Apollo_DB_Schema::_APL_CLASSIFIED_TMP_ORG ."'
                    ";
                    $clauses['orderby'] = " org_title " . $order;
                    break;
                case 'expiration_date' :
                    $clauses['join'] = " LEFT JOIN " . $this->meta_tbl . " AS mt ON mt.apollo_classified_id=".$wpdb->posts.".ID AND mt.meta_key='" . Apollo_DB_Schema::_APL_CLASSIFIED_EXP_DATE . "' ";
                    $clauses['orderby'] = " mt.meta_value " . $order;
                    break;
                case 'name' :
                    $clauses['join'] = " LEFT JOIN " . $this->meta_tbl . " AS mt ON mt.apollo_classified_id=".$wpdb->posts.".ID AND mt.meta_key='" . Apollo_DB_Schema::_CLASSIFIED_NAME . "' ";
                    $clauses['orderby'] = " mt.meta_value " . $order;
                    break;
                case 'email' :
                    $clauses['join'] = " LEFT JOIN " . $this->meta_tbl . " AS mt ON mt.apollo_classified_id=".$wpdb->posts.".ID AND mt.meta_key='" . Apollo_DB_Schema::_CLASSIFIED_EMAIL . "' ";
                    $clauses['orderby'] = " mt.meta_value " . $order;
                    break;
                case 'phone' :
                    $clauses['join'] = " LEFT JOIN " . $this->meta_tbl . " AS mt ON mt.apollo_classified_id=".$wpdb->posts.".ID AND mt.meta_key='" . Apollo_DB_Schema::_CLASSIFIED_PHONE . "' ";
                    $clauses['orderby'] = " mt.meta_value " . $order;
                    break;
                default :
                    break;
            }

        }
        return $clauses;
    }
    
}    
    
endif;    

return new Apollo_Admin_Classified();