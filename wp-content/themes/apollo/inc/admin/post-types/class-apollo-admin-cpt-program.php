<?php
/**
 * Admin functions for the Artist type
 *
 * @author 		vulh
 * @category 	admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Apollo_Admin_CPT' ) ) {
    include( 'class-apollo-admin-cpt.php' );
}

if ( ! class_exists( 'Apollo_Admin_Program' ) ) :
    
class Apollo_Admin_Program extends Apollo_Admin_CPT {

    public function __construct() {
        global $typenow, $pagenow, $wpdb;
      
        $this->type             = Apollo_DB_Schema::_PROGRAM_PT;
        $this->meta_tbl  = $wpdb->{Apollo_Tables::_APL_PROGRAM_META};
        $this->post_term_tbl   = $wpdb->{Apollo_Tables::_POST_TERM};
        
        $this->requireds = array(
      
            'post_title' => array( 
                'label'     => __( 'The name', 'apollo' ), 
                'validate'  => 'required' 
            ),
            ''.Apollo_DB_Schema::_APL_PROGRAM_DATA.'['.Apollo_DB_Schema::_APL_PROGRAM_URL.']' => array( 
                'label'     => __( 'The website', 'apollo' ), 
                'validate'  => 'url' 
            ),
            ''.Apollo_DB_Schema::_APL_PROGRAM_DATA.'['.Apollo_DB_Schema::_APL_PROGRAM_EMAIL.']' => array( 
                'label'     => __( 'The email', 'apollo' ), 
                'validate'  => 'email' 
            ),
         
            ''.Apollo_DB_Schema::_APL_PROGRAM_ENDD.'' => array( 
                'label'     => __( 'The End Date', 'apollo' ), 
                'validate'  => 'greater' 
            ),
            'edu_id' => array( 
                'label'     => __( 'The Educator', 'apollo' ), 
                'validate'  => 'required' 
            ),
            'video_embed[]'  => array(
                'label' => __( 'The youtube link', 'apollo' ),
                'validate'  => 'youtube_url',
            ),
            Apollo_DB_Schema::_APL_PROGRAM_DATA .'['. Apollo_DB_Schema::_APL_PROGRAM_MAX_STUDENTS .']' =>array(
                'label'     => __( 'The Max Number of Students', 'apollo' ),
                'validate'  => 'required'
            )
        );
        
        // Admin column
        add_filter( 'manage_edit-'.$this->type.'_columns', array( $this, 'edit_columns' ) );
        add_action( 'manage_'.$this->type.'_posts_custom_column', array( $this, 'custom_columns' ), 2 );
        add_filter( 'manage_edit-'.$this->type.'_sortable_columns', array( $this, 'custom_columns_sort' ) );
        
        // Bulk / Quick edit
        /** @Ticket #14398 */
        /** @Ticket #14443 */
        add_action( 'quick_edit_custom_box', array( $this, 'quick_edit' ), 10, 2 );
        add_action( 'save_post', array( $this, 'bulk_and_quick_edit_save_post' ), 10, 2 );
        
        /// Event filtering
        add_action( 'restrict_manage_posts', array( $this, '_filters' ) );

        add_filter( 'posts_where', array( $this, 'posts_where' ) );

        add_filter('posts_request', array($this,'dump_request'));
        
        $this->meta_tbl  = $wpdb->{Apollo_Tables::_APL_PROGRAM_META};


        add_action('publish_program', array($this, 'send_email_to_owner'));

        add_action( 'delete_post', array( $this, 'delete_post' ) );

        add_filter('display_post_states', array($this, 'display_post_states'), 10, 2);
        parent::__construct();
    }

    public function delete_post( $post_id ) {
        if(empty($post_id)) return;
        // Delete data in program(s) educator
        $apl_query_pe = new Apl_Query( Apollo_Tables::_APL_PROGRAM_EDUCATOR );
        $apl_query_pe->delete( "prog_id = '$post_id'" );
    }


    /**
     * Send an email to the owner of published event
     */
    public function send_email_to_owner()
    {
        global $post;

        /**
         * Send approval email once publishing a program
         */
        if ($post->post_status != 'publish') {
            new Apollo_Email_Published_New_Program();
            $postData = isset($_POST[Apollo_DB_Schema::_APL_PROGRAM_DATA]) ? $_POST[Apollo_DB_Schema::_APL_PROGRAM_DATA] : [];

            if (!isset($postData[Apollo_DB_Schema::_APL_PROGRAM_EMAIL]) || !$postData[Apollo_DB_Schema::_APL_PROGRAM_EMAIL]) {
                return false;
            }
            $email = $postData[Apollo_DB_Schema::_APL_PROGRAM_EMAIL];
            if (!isset($postData[Apollo_DB_Schema::_APL_PROGRAM_CNAME]) || !$postData[Apollo_DB_Schema::_APL_PROGRAM_CNAME]) {
                $contactName = $email;
            }
            $prog = get_program($post);
            do_action( 'apollo_email_published_new_program', $contactName, $prog->get_title(), $prog->get_permalink(), $email );
        }
    }

    public function dump_request($input)
    {
        $debugContentType = isset($_GET['is_debugging']);
        if($debugContentType){
            aplDebug($input,1);
        }
        return $input;
    }

    /**
     * Custom post join 
     * 
     * @access public
     * @return string
     */
    public function filter_meta_join( $join ) {
        global $wpdb;
        
        $join = str_replace( 'post_id', 'apollo_program_id', $this->replace_meta_table( $join ) );
        
        // Join to filter event not exprise
        $join .= "
            LEFT JOIN {$this->meta_tbl} mt ON mt.apollo_program_id = ". $wpdb->posts .".ID
        ";
        
        return $join;
    }
    
    /**
     * Replace meta table 
     * 
     * @access public
     * @return string
     */
    public function replace_meta_table( $str ) {
        global $wpdb;
        return str_replace( $wpdb->postmeta, $wpdb->apollo_programmeta, $str);
    }
    
    /**
     * Custom post where 
     * 
     * @access public
     * @return string
     */
    public function posts_where( $where ) {
        global $wpdb;
        $metaTbl = $wpdb->{Apollo_Tables::_APL_PROGRAM_META};
        $postTbl = $wpdb->posts;
        $today = current_time('Y-m-d');

        if ( ( isset( $_REQUEST['post_status'] ) && $_REQUEST['post_status'] == 'unconfirmed' ) ) {
            $where .= " AND {$wpdb->posts}.post_status = 'unconfirmed' ";
        }

        if (isset($_GET['expired'])) {
            $where .= " AND $postTbl.ID IN (
                SELECT apollo_program_id FROM $metaTbl mt WHERE
              mt.meta_key = '".Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND."' AND mt.meta_value < '$today' ) ";


        }

        return $where;
    }
    
    /**
     * Change order by
     * 
     * @access public
     * @return string
     */
    public function change_order_by( $str ) {
        
        $str = " mt.meta_value ASC ";
        
        return $str;
    }
    
    public function _filters() {
        global $typenow;
        
        if ( $typenow !== $this->type ) {
            return ;
        }

        // show drop down
        apollo_dropdown_categories( $this->type. '-type' );
        //apollo_dropdown_categories( 'population-served', array(), __( 'Select Population Served', 'apollo' ) );
        $this->_filterAdvanceSearch();
    }

    private function _filterAdvanceSearch()
    {
        if (!$this->isAdvancedSearch()) {
            return '';
        }


        $expired = isset($_GET['expired']) ? 'checked' : '';

        echo '<span class="past-event-wrapper">';
        echo '<input '.$expired.' type="checkbox" value="1" name="expired" /><label>'.__('Expired', 'apollo').'&nbsp;&nbsp;</label>';
        echo '</span>';


        if ( isset( $_REQUEST['advanced'] ) && $_REQUEST['advanced'] ) {
            echo '<input name="advanced" type="hidden" value="true" />';
        }


    }

    public function bulk_edit_save( $post_id, $venue ) {
        
    }
    
    /**
	 * Change the columns shown in admin.
	 */
	public function edit_columns( $existing_columns ) {
       
		if ( empty( $existing_columns ) && ! is_array( $existing_columns ) ) {
            $existing_columns = array();
        }

		unset( $existing_columns['title'], $existing_columns['comments'], $existing_columns['date'] );

		$columns = array();
		$columns["cb"] = "<input type=\"checkbox\" />";
		$columns["name"] = __( 'Name', 'apollo' );
      
        $columns["program-type"] = __( 'Program Category(s)', 'apollo' );
        $columns['educator']    = __( 'Educator', 'apollo' );
        
		return array_merge( $columns, $existing_columns );
	}
    
    /**
	 * Define our custom columns shown in admin.
	 * @param  string $column
	 */
	public function custom_columns( $column ) {
        global $post, $the_program;
        
        if ( empty( $the_program ) || $the_program->id != $post->ID ) {
            
            $the_program = get_program( $post );
        }
       
        
        switch ( $column ) {
            
            case "name" :
				$edit_link = get_edit_post_link( $post->ID );
				$title = _draft_or_post_title();
				$post_type_object = get_post_type_object( $post->post_type );
				$can_edit_post = current_user_can( $post_type_object->cap->edit_post, $post->ID );

				echo '<strong><a class="row-title" href="' . esc_url( $edit_link ) .'">' . $title.'</a>';

				_post_states( $post );

				echo '</strong>';

				if ( $post->post_parent > 0 )
					echo '&nbsp;&nbsp;&larr; <a href="'. get_edit_post_link($post->post_parent) .'">'. get_the_title($post->post_parent) .'</a>';

				// Excerpt view
				if (isset($_GET['mode']) && $_GET['mode']=='excerpt') echo apply_filters('the_excerpt', $post->post_excerpt);

				// Get actions
				$actions = array();

				$actions['id'] = 'ID: ' . $post->ID;

				if ( $can_edit_post && 'trash' != $post->post_status ) {
					$actions['edit'] = '<a href="' . get_edit_post_link( $post->ID, true ) . '" title="' . esc_attr( __( 'Edit this item', 'apollo' ) ) . '">' . __( 'Edit', 'apollo' ) . '</a>';
					$actions['inline hide-if-no-js'] = '<a href="#" class="editinline" title="' . esc_attr( __( 'Edit this item inline', 'apollo' ) ) . '">' . __( 'Quick&nbsp;Edit', 'apollo' ) . '</a>';
				}
				if ( current_user_can( $post_type_object->cap->delete_post, $post->ID ) ) {
					if ( 'trash' == $post->post_status )
						$actions['untrash'] = "<a title='" . esc_attr( __( 'Restore this item from the Trash', 'apollo' ) ) . "' href='" . wp_nonce_url( admin_url( sprintf( $post_type_object->_edit_link . '&amp;action=untrash', $post->ID ) ), 'untrash-post_' . $post->ID ) . "'>" . __( 'Restore', 'apollo' ) . "</a>";
					elseif ( EMPTY_TRASH_DAYS )
						$actions['trash'] = "<a class='submitdelete' title='" . esc_attr( __( 'Move this item to the Trash', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID ) . "'>" . __( 'Trash', 'apollo' ) . "</a>";
					if ( 'trash' == $post->post_status || !EMPTY_TRASH_DAYS )
						$actions['delete'] = "<a class='submitdelete' title='" . esc_attr( __( 'Delete this item permanently', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID, '', true ) . "'>" . __( 'Delete Permanently', 'apollo' ) . "</a>";
				}
				if ( $post_type_object->public ) {
					if ( in_array( $post->post_status, array( 'pending', 'draft', 'future' ) ) ) {
						if ( $can_edit_post )
							$actions['view'] = '<a href="' . esc_url( add_query_arg( 'preview', 'true', get_permalink( $post->ID ) ) ) . '" title="' . esc_attr( sprintf( __( 'Preview &#8220;%s&#8221;', 'apollo' ), $title ) ) . '" rel="permalink">' . __( 'Preview', 'apollo' ) . '</a>';
					} elseif ( !in_array($post->post_status, array('trash', 'unconfirmed')) ) {
						$actions['view'] = '<a href="' . get_permalink( $post->ID ) . '" title="' . esc_attr( sprintf( __( 'View &#8220;%s&#8221;', 'apollo' ), $title ) ) . '" rel="permalink">' . __( 'View', 'apollo' ) . '</a>';
					}
				}
                
				$actions = apply_filters( 'post_row_actions', $actions, $post );

				echo '<div class="row-actions">';

				$i = 0;
				$action_count = sizeof($actions);

				foreach ( $actions as $action => $link ) {
					++$i;
					( $i == $action_count ) ? $sep = '' : $sep = ' | ';
					echo "<span class='$action'>$link$sep</span>";
				}
				echo '</div>';
                get_inline_data( $post );
                
                $commuicate_js_str = '';
                $_a_data = array(
                    Apollo_DB_Schema::_APL_PROGRAM_CNAME,
                    Apollo_DB_Schema::_APL_PROGRAM_PHONE,
                    Apollo_DB_Schema::_APL_PROGRAM_EMAIL,
                    Apollo_DB_Schema::_APL_PROGRAM_URL,
                );
                
                foreach ( $_a_data as $k ) {
                    $commuicate_js_str .= '<div class="'.Apollo_DB_Schema::_APL_PROGRAM_DATA.'['.$k.']">' . $the_program->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_DATA. '['. $k .']' ) . '</div>';
                }
                $commuicate_js_str .= '<div class="'.Apollo_DB_Schema::_APL_PROGRAM_STARTD.'">' . $the_program->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_STARTD ). '</div>';
                $commuicate_js_str .= '<div class="'.Apollo_DB_Schema::_APL_PROGRAM_ENDD.'">' . $the_program->get_meta_data( Apollo_DB_Schema::_APL_PROGRAM_ENDD ). '</div>';
                
				echo '
					<div class="hidden" id="_inline_' . $post->ID . '">
						<div class="menu_order">' . $post->menu_order . '</div>
						'.$commuicate_js_str.'
					</div>
				';
                
			break;
           
            case "program-type" :
                
                $terms = get_the_terms( $post->ID, $column );
           
                $termlist = array();
                
                if ( $terms ):
                    foreach ( $terms as $term ) {
                        if ( $term->parent ) continue;  // Not display sub menu in the list event page

                        $termlist[] = '<a href="' . admin_url( 'edit.php?' . $column . '=' . $term->slug . '&post_type='.$this->type.'' ) . ' ">' . $term->name . '</a>';
                    }
                endif;
                
                echo $termlist ? implode( ', ', $termlist ) : '<span class="na">&ndash;</span>';

			break;
        
            case 'educator':
                $apl_query = new Apl_Query( Apollo_Tables::_APL_PROGRAM_EDUCATOR );
                $educator = $apl_query->get_row( " prog_id = $post->ID " );
                if ( $educator ):
                    $educator = get_post($educator->edu_id);
                    echo '<a href="'.  get_edit_post_link( $educator->ID, true ) .'">'.$educator->post_title.'</a>';
                endif;
            break;
        }
        
    }
    
    /**
	 * Make columns sortable
	 *
	 * @access public
	 * @param mixed $columns
	 * @return array
	 */
	public function custom_columns_sort( $columns ) {
		$custom = array(
			'name'  => 'name'
		);
		return wp_parse_args( $custom, $columns );
	}
    
    public function quick_edit_save( $post_id, $post ) {
         
        $program = get_program( $post ); 

        $data = Apollo_App::unserialize($program->get_meta_data(Apollo_DB_Schema::_APL_PROGRAM_DATA));

        $input = $_POST[Apollo_DB_Schema::_APL_PROGRAM_DATA];
        $data = $this->setQuickEditData($input, $data);

        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_PROGRAM_DATA, serialize( $data ) );
      
        // Save date range
        $this->save_date_range( Apollo_DB_Schema::_APL_PROGRAM_STARTD, Apollo_DB_Schema::_APL_PROGRAM_ENDD, $post_id );
        
        do_action( 'apollo_'.$this->type.'_quick_edit_save', $post );
    }

    /**
     * @Ticket #18677
     * @param $post_states
     * @param $post
     */
    public function display_post_states ($post_states, $post) {
        $post_status = isset( $_REQUEST['post_status'] ) ? $_REQUEST['post_status'] : '';
        if ( 'unconfirmed' == $post->post_status && 'unconfirmed' != $post_status ) {
            $post_states['unconfirmed'] = __('Unconfirmed', 'post status');
        }
        return $post_states;
    }
    
}    
    
endif;    

new Apollo_Admin_Program();
