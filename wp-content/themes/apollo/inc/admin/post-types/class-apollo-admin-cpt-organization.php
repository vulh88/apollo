<?php
/**
 * Admin functions for the venue post type
 *
 * @author 		vulh
 * @category 	admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Apollo_Admin_CPT' ) ) {
    include( 'class-apollo-admin-cpt.php' );
}

if ( ! class_exists( 'Apollo_Admin_Org' ) ) :
    
class Apollo_Admin_Org extends Apollo_Admin_CPT {
    
    public function __construct() {
        global $wpdb;
        $this->type = Apollo_DB_Schema::_ORGANIZATION_PT;
        $this->meta_tbl  = $wpdb->{Apollo_Tables::_APL_ORG_META};
        $this->associationUserModuleListComp = apl_instance('APL_Lib_Association_Components_AssociationUsersModuleList');
        $this->associationAgencyModuleComp = apl_instance('APL_Lib_Association_Components_AssociationAgenciesModuleList');

        $this->requireds = array(

            'post_title'    => array(
                'label'     => __( 'The name', 'apollo' ),
                'validate'  => 'required',
            ),
            'post_content'    => array(
            'label'     => __( 'The content', 'apollo' ),
            'validate'  => 'required',
             ),
            ''.Apollo_DB_Schema::_APL_ORG_DATA.'['.Apollo_DB_Schema::_ORG_EMAIL.']' => array(
                'label'     => __( 'The Email', 'apollo' ),
                'validate'  => 'email,required'
            ),
            ''.Apollo_DB_Schema::_APL_ORG_DATA.'['.Apollo_DB_Schema::_ORG_WEBSITE_URL.']' => array(
                'label'     => __( 'The Website', 'apollo' ),
                'validate'  => 'url'
            ),

            /* Social network url error messages */
            ''.Apollo_DB_Schema::_APL_ORG_DATA.'['.Apollo_DB_Schema::_ORG_FACEBOOK_URL.']' => array(
                'label'     => __( 'The Facebook url', 'apollo' ),
                'validate'  => 'url'
            ),
            ''.Apollo_DB_Schema::_APL_ORG_DATA.'['.Apollo_DB_Schema::_ORG_TWITTER_URL.']' => array(
                'label'     => __( 'The Twitter url', 'apollo' ),
                'validate'  => 'url'
            ),
            ''.Apollo_DB_Schema::_APL_ORG_DATA.'['.Apollo_DB_Schema::_ORG_LINKED_IN_URL.']' => array(
                'label'     => __( 'The LinkedIn url', 'apollo' ),
                'validate'  => 'url'
            ),
            ''.Apollo_DB_Schema::_APL_ORG_DATA.'['.Apollo_DB_Schema::_ORG_INSTAGRAM_URL.']' => array(
                'label'     => __( 'The Instagram url', 'apollo' ),
                'validate'  => 'url'
            ),
            ''.Apollo_DB_Schema::_APL_ORG_DATA.'['.Apollo_DB_Schema::_ORG_BLOG_URL.']' => array(
                'label'     => __( 'Blog url', 'apollo' ),
                'validate'  => 'url'
            ),
            ''.Apollo_DB_Schema::_APL_ORG_DATA.'['.Apollo_DB_Schema::_ORG_PINTEREST_URL.']' => array(
                'label'     => __( 'The Pinterest url', 'apollo' ),
                'validate'  => 'url'
            ),
            ''.Apollo_DB_Schema::_APL_ORG_DATA.'['.Apollo_DB_Schema::_ORG_DONATE_URL.']' => array(
                'label'     => __( 'Donate url', 'apollo' ),
                'validate'  => 'url'
            ),
            /* Social network url error messages */


            ''.Apollo_DB_Schema::_APL_ORG_ADDRESS.'['.Apollo_DB_Schema::_ORG_ADDRESS1.']'     => array(
            'label'     => __( 'The Address 1', 'apollo' ),
            'validate'  => 'required',
             ),

            /** @Ticket #13634 */
            ''.Apollo_DB_Schema::_APL_ORG_DATA.'['.Apollo_DB_Schema::_ORG_PHONE.']'     => array(
            'label'     => __( 'The Phone', 'apollo' ),
            'validate'  => 'required',
             ),

            ''.Apollo_DB_Schema::_APL_ORG_ADDRESS.'['.Apollo_DB_Schema::_ORG_STATE.']'   => array(
            'label'     => __( 'The State', 'apollo' ),
            'validate'  => 'required',
             ),

            ''.Apollo_DB_Schema::_APL_ORG_ADDRESS.'['.Apollo_DB_Schema::_ORG_CITY.']'   => array(
            'label'     => __( 'The City', 'apollo' ),
            'validate'  => 'required',
             ),
            ''.Apollo_DB_Schema::_APL_ORG_ADDRESS.'['.Apollo_DB_Schema::_ORG_ZIP.']'   => array(
                'label'     => __( 'The Zip', 'apollo' ),
                'validate'  => 'required',
            ),
           
            ''.Apollo_DB_Schema::_APL_ORG_DATA.'['.Apollo_DB_Schema::_ORG_CONTACT_NAME.']'    => array(
                'label'     => __( 'The Contact Name', 'apollo' ),
                'validate'  => 'required',
            ),
         
            'video_embed[]'  => array(
                'label' => __( 'The Youtube Link', 'apollo' ),
                'validate'  => 'youtube_url',
            ),
            'audio_embed[]'  => array(
                'label' => __( 'The Audio Embed', 'apollo' ),
                'validate'  => 'audio_embed',
            ),
        );

        // Admin column
        add_filter( 'manage_edit-'.$this->type.'_columns', array( $this, 'edit_columns' ) );
        add_action( 'manage_'.$this->type.'_posts_custom_column', array( $this, 'custom_columns' ), 2 );
        add_filter( 'manage_edit-'.$this->type.'_sortable_columns', array( $this, 'custom_columns_sort' ) );

        // Bulk / Quick edit
        /** @Ticket #14398 */
        /** @Ticket #14443 */
        add_action( 'quick_edit_custom_box', array( $this, 'quick_edit' ), 10, 2 );
        add_action( 'save_post', array( $this, 'bulk_and_quick_edit_save_post' ), 10, 2 );

        /// Event filtering
        add_action( 'restrict_manage_posts', array( $this, '_filters' ) );

        add_action( 'delete_post', array( $this, 'delete_post' ) );

        add_action( 'wp_trash_post', array($this,'trash_post') );

        add_action( 'untrashed_post', array($this,'untrashed_post') );

        add_action('load-edit.php',  array($this,'custom_bulk_action'));

        /** @Ticket #13028 */
        add_filter('posts_clauses', array( $this, 'custom_query_posts' ) );

        parent::__construct();
    }
    
    public function delete_post( $post_id ) {
        $apl_query_ae = new Apl_Query( Apollo_Tables::_APL_AGENCY_ORG );
        $apl_query_ae->delete("org_id = $post_id" );
        $bussineed_id = $this->update_business_associated($post_id,false);
        wp_delete_post($bussineed_id);
    }

    /**
     * @author: Trilm
     * hook trash org --> trash business
     */
    public function trash_post($post_id){
        $this->update_business_associated($post_id);
    }

    /**
     * @author: Trilm
     * hook un_trash org --> un_trash business
     */
    public function untrashed_post($post_id){
        $this->update_business_associated($post_id,true,'publish');
    }

    /**
     * @author Trilm
     * @param $post_id
     * @param $need_update
     * @param string $status
     * @return business_id
     */
    public function update_business_associated($post_id, $need_update = true ,$status = 'trash'){
        $bussiness_id = Apollo_App::apollo_get_meta_data($post_id,Apollo_DB_Schema::_APL_ORG_BUSINESS);
        $is_business = Apollo_App::apollo_get_meta_data($post_id,Apollo_DB_Schema::_ORG_BUSINESS,Apollo_DB_Schema::_APL_ORG_DATA);
        $post_type = get_post_type($post_id);

        if($need_update == true){
            if($post_type == Apollo_DB_Schema::_ORGANIZATION_PT){
                if($is_business == 'yes' && $bussiness_id ){
                    $my_post = array(
                        'ID'           => $bussiness_id,
                        'post_status'   => $status,
                    );
                    wp_update_post( $my_post );
                }
            }
        }

        return $bussiness_id;
    }

    /**
     * @author Trilm
     *
     */
    public function custom_bulk_action(){
        $ids = isset($_GET['ids'])?$_GET['ids']:null;
        $actionTrash = isset($_GET['trashed'])?$_GET['trashed']:'';
        $actionUntrashed= isset($_GET['untrashed'])?$_GET['untrashed']:'';
        if($ids){
            $status = '';
            if($actionTrash){
                $status = 'trash';
            }
            elseif ($actionUntrashed) {
                $status = 'publish';
            }

            if($status != ''){

                if (!is_array($ids)) {
                    $ids = array($ids);
                }

                foreach ($ids as $id){
                    $this->update_business_associated($id,true,$status);
                }
            }
        }

    }
    
    public function _filters() {
        global $typenow;
        
        if ( $typenow !== $this->type ) {
            return ;
        }
        
        apollo_dropdown_categories( $this->type. '-type' );
    }
    
     /**
     * Custom Quick Edit form
     * 
     * @access public
     * @param mixed $column_type
	 * @param mixed $post_type
    */
    public function quick_edit( $column_type, $post_type ) {
        
        if ( $column_type != 'name' ) {
            return;
        }     
    
        require_once APOLLO_ADMIN_DIR. '/views/'.$this->type.'/html-quick-edit.php';
    }
  
    /**
     * Quick and Bulk saving
     * 
     * @access public
     * @param mixed $post_id
     * @param mixed $post
     */
    public function bulk_and_quick_edit_save_post( $post_id, $post ) {
       
        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}
        
        // Don't save revisions and autosaves
		if ( wp_is_post_revision( $post_id ) || wp_is_post_autosave( $post_id ) ) {
			return $post_id;
		}
        
        if ( $post->post_type != $this->type ) {
            return $post_id;
        }
        
        // Check user permission
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return $post_id;
		}
        
        $nonce = 'apollo_'.$this->type.'_bulk_edit_nonce';
        if ( isset( $_REQUEST[$nonce] ) && ! wp_verify_nonce( $_REQUEST[$nonce], $nonce ) ) {
			return $post_id;
		}
        
        $post = get_venue( $post );
        
        if ( ! empty( $_REQUEST['apollo_'.$this->type.'_quick_edit'] ) ) {
           
			$this->quick_edit_save( $post_id, $post );
		} else {
			$this->bulk_edit_save( $post_id, $post );
		}

		return $post_id;
    }
    
    public function bulk_edit_save() {
        
    }
    
    /**
	 * Change the columns shown in admin.
	 */
	public function edit_columns( $existing_columns ) {

		if ( empty( $existing_columns ) && ! is_array( $existing_columns ) ) {
            $existing_columns = array();
        }

		unset( $existing_columns['title'], $existing_columns['comments'], $existing_columns['date'] );

		$columns = array();
		$columns["cb"] = "<input type=\"checkbox\" />";
		$columns['D'] = __( 'Dup', 'apollo' );
		$columns["name"] = __( 'Title', 'apollo' );
        $columns["phone"] = __( 'Phone', 'apollo' );
        $columns["city"] = __( 'City', 'apollo' );
        $columns["state"] = __( 'State', 'apollo' );
        $columns["zip"] = __( 'Zip', 'apollo' );
        $columns["organization-type"] = __( 'Organization Category(s)', 'apollo' );
        $columns['username'] = __( 'Associated User(s)', 'apollo' );

        if (Apollo_App::is_avaiable_module(Apollo_DB_Schema::_AGENCY_PT)) {
            $columns['agency'] = __( 'Agency', 'apollo' );
        }

        $columns["date"] = __( 'Posted date', 'apollo' );
        if(of_get_option(Apollo_DB_Schema::_ORGANIZATION_ACTIVE_MEMBER_FIELD)){
            $columns['member'] = __( 'Member', 'apollo' );
        }



		return array_merge( $columns, $existing_columns );
	}
    
    /**
	 * Define our custom columns shown in admin.
	 * @param  string $column
	 */
	public function custom_columns( $column ) {
        global $post, $the_org;
        
        if ( empty( $the_org ) || $the_org->id != $post->ID ) {
            
            $the_org = get_org( $post );
        }
       
        
        switch ( $column ) {

            case "D" :
                $this->render_duplicate_icon($post, 'org');
            break;
                
            case "name" :
				$edit_link = get_edit_post_link( $post->ID );
				$title = _draft_or_post_title();
				$post_type_object = get_post_type_object( $post->post_type );
				$can_edit_post = current_user_can( $post_type_object->cap->edit_post, $post->ID );

				echo '<strong><a class="row-title" href="' . esc_url( $edit_link ) .'">' . $title.'</a>';

				_post_states( $post );

				echo '</strong>';

				if ( $post->post_parent > 0 )
					echo '&nbsp;&nbsp;&larr; <a href="'. get_edit_post_link($post->post_parent) .'">'. get_the_title($post->post_parent) .'</a>';

				// Excerpt view
				if (isset($_GET['mode']) && $_GET['mode']=='excerpt') echo apply_filters('the_excerpt', $post->post_excerpt);

				// Get actions
				$actions = array();

				$actions['id'] = 'ID: ' . $post->ID;

				if ( $can_edit_post && 'trash' != $post->post_status ) {
					$actions['edit'] = '<a href="' . get_edit_post_link( $post->ID, true ) . '" title="' . esc_attr( __( 'Edit this item', 'apollo' ) ) . '">' . __( 'Edit', 'apollo' ) . '</a>';
					$actions['inline hide-if-no-js'] = '<a href="#" class="editinline" title="' . esc_attr( __( 'Edit this item inline', 'apollo' ) ) . '">' . __( 'Quick&nbsp;Edit', 'apollo' ) . '</a>';
				}
				if ( current_user_can( $post_type_object->cap->delete_post, $post->ID ) ) {
					if ( 'trash' == $post->post_status )
						$actions['untrash'] = "<a title='" . esc_attr( __( 'Restore this item from the Trash', 'apollo' ) ) . "' href='" . wp_nonce_url( admin_url( sprintf( $post_type_object->_edit_link . '&amp;action=untrash', $post->ID ) ), 'untrash-post_' . $post->ID ) . "'>" . __( 'Restore', 'apollo' ) . "</a>";
					elseif ( EMPTY_TRASH_DAYS )
						$actions['trash'] = "<a class='submitdelete' title='" . esc_attr( __( 'Move this item to the Trash', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID ) . "'>" . __( 'Trash', 'apollo' ) . "</a>";
					if ( 'trash' == $post->post_status || !EMPTY_TRASH_DAYS )
						$actions['delete'] = "<a class='submitdelete' title='" . esc_attr( __( 'Delete this item permanently', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID, '', true ) . "'>" . __( 'Delete Permanently', 'apollo' ) . "</a>";
				}
				if ( $post_type_object->public ) {
					if ( in_array( $post->post_status, array( 'pending', 'draft', 'future' ) ) ) {
						if ( $can_edit_post )
							$actions['view'] = '<a href="' . esc_url( add_query_arg( 'preview', 'true', get_permalink( $post->ID ) ) ) . '" title="' . esc_attr( sprintf( __( 'Preview &#8220;%s&#8221;', 'apollo' ), $title ) ) . '" rel="permalink">' . __( 'Preview', 'apollo' ) . '</a>';
					} elseif ( 'trash' != $post->post_status ) {
						$actions['view'] = '<a href="' . get_permalink( $post->ID ) . '" title="' . esc_attr( sprintf( __( 'View &#8220;%s&#8221;', 'apollo' ), $title ) ) . '" rel="permalink">' . __( 'View', 'apollo' ) . '</a>';
					}
				}

				$actions = apply_filters( 'post_row_actions', $actions, $post );

				echo '<div class="row-actions">';

				$i = 0;
				$action_count = sizeof($actions);

				foreach ( $actions as $action => $link ) {
					++$i;
					( $i == $action_count ) ? $sep = '' : $sep = ' | ';
					echo "<span class='$action'>$link$sep</span>";
				}
				echo '</div>';
                get_inline_data( $post );
                    
                $_a_data = array(
                    Apollo_DB_Schema::_ORG_PHONE,
                    Apollo_DB_Schema::_ORG_FAX,
                    Apollo_DB_Schema::_ORG_EMAIL,
                    Apollo_DB_Schema::_ORG_WEBSITE_URL,
                );
                
                $_a_address = array(
                    Apollo_DB_Schema::_ORG_ADDRESS1,
                    Apollo_DB_Schema::_ORG_ADDRESS2,
                    Apollo_DB_Schema::_ORG_CITY,
                    Apollo_DB_Schema::_ORG_STATE,
                    Apollo_DB_Schema::_ORG_ZIP,
                    Apollo_DB_Schema::_ORG_REGION,
                );
                $commuicate_js_str = '';
                foreach ( $_a_data as $k ) {
                    $commuicate_js_str .= '<div class="'.Apollo_DB_Schema::_APL_ORG_DATA.'['.$k.']">' . $the_org->get_meta_data( Apollo_DB_Schema::_APL_ORG_DATA. '['. $k .']' ) . '</div>';
                }
                
                foreach ( $_a_address as $k ) {
                    $commuicate_js_str .= '<div class="'.Apollo_DB_Schema::_APL_ORG_ADDRESS.'['.$k.']">' . $the_org->get_meta_data( Apollo_DB_Schema::_APL_ORG_ADDRESS. '['. $k .']' ) . '</div>';
                }
                
                echo '
					<div class="hidden" id="_inline_' . $post->ID . '">
						<div class="menu_order">' . $post->menu_order . '</div>
						'.$commuicate_js_str.'
					</div>
				';
			break;
           
            case 'phone':
                $val = Apollo_App::apollo_get_meta_data( $the_org->id, Apollo_DB_Schema::_ORG_PHONE, Apollo_DB_Schema::_APL_ORG_DATA );
                echo $val ? $val : '-';
            break;
        
            case 'city':
                $val = Apollo_App::apollo_get_meta_data( $the_org->id, Apollo_DB_Schema::_ORG_CITY, Apollo_DB_Schema::_APL_ORG_ADDRESS );
                echo $val ? $val : '-';
            break;
        
            case 'state':
                $val = Apollo_App::apollo_get_meta_data( $the_org->id, Apollo_DB_Schema::_ORG_STATE, Apollo_DB_Schema::_APL_ORG_ADDRESS );
                echo $val ? $val : '-';
            break;
        
            case 'zip':
                $val = Apollo_App::apollo_get_meta_data( $the_org->id, Apollo_DB_Schema::_ORG_ZIP, Apollo_DB_Schema::_APL_ORG_ADDRESS );
                echo $val ? $val : '-';
            break;
        
            case "organization-type" :
				if ( ! $terms = get_the_terms( $post->ID, $column ) ) {
					echo '<span class="na">&ndash;</span>';
				} else {
                    $termlist = array();
                    
					foreach ( $terms as $term ) {
                        if ( $term->parent ) continue;  // Not display sub menu in the list event page
                        
						$termlist[] = '<a href="' . admin_url( 'edit.php?' . $column . '=' . $term->slug . '&post_type='.$this->type.'' ) . ' ">' . $term->name . '</a>';
					}
                    
					echo $termlist ? implode( ', ', $termlist ) : '<span class="na">&ndash;</span>';
				}
			break;
            
            case 'username':
                $this->associationUserModuleListComp->render(array(
                    'id' => $post->ID,
                    'post_type' => 'organization',
                ));
            break;

            case 'agency':
                $this->associationAgencyModuleComp->render(array(
                    'id' => $post->ID,
                    'post_type' => 'organization',
                ));
                break;

            case 'member' :
                $isMember = Apollo_App::apollo_get_meta_data( $the_org->id, Apollo_DB_Schema::_ORG_MEMBER_ONLY, Apollo_DB_Schema::_APL_ORG_DATA );
                if($isMember == '' || strtolower($isMember) == 'no'){
                    echo __('No','apollo');
                }else{
                    echo __('Yes','apollo');
                }
                break;
        }
        
    }
    
    /**
	 * Make columns sortable
	 *
	 * @access public
	 * @param mixed $columns
	 * @return array
	 */
	public function custom_columns_sort( $columns ) {
		$custom = array(
		    'name'  => 'title',
			'phone'               => 'phone',
            'city'               => 'city',
            'state'               => 'state',
			'zip'               => 'zip',
		);
		return wp_parse_args( $custom, $columns );
	}
    
    private function quick_edit_save( $post_id, $post ) {
         
        $org = get_org( $post );
        $data = Apollo_App::unserialize($org->get_meta_data(Apollo_DB_Schema::_APL_ORG_DATA));
        $input = $_POST[Apollo_DB_Schema::_APL_ORG_DATA];
        $data = $this->setQuickEditData($input, $data);
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_ORG_DATA, serialize( $data ) );
        update_apollo_meta($post_id, Apollo_DB_Schema::_ORG_PHONE, !empty($data[Apollo_DB_Schema::_ORG_PHONE]) ? $data[Apollo_DB_Schema::_ORG_PHONE] : '');

        do_action( 'apollo_'.$this->type.'_quick_edit_save', $post );
    }

    public function custom_query_posts($clauses) {
	    global $wpdb;
        if( !empty($_GET['orderby'])) {
            $order = !empty($_GET['order']) ? $_GET['order'] : 'ASC';
            if ($_GET['orderby'] == 'city' || $_GET['orderby'] == 'state' || $_GET['orderby'] == 'zip' ) {
                switch ($_GET['orderby']) {
                    case 'city' :
                        $sort = Apollo_DB_Schema::_ORG_CITY;
                        break;
                    case 'state' :
                        $sort = Apollo_DB_Schema::_ORG_STATE;
                        break;
                    case 'zip' :
                        $sort = Apollo_DB_Schema::_ORG_ZIP;
                        break;
                    default :
                        $sort = Apollo_DB_Schema::_ORG_CITY;
                        break;
                }
                $clauses['join'] = " LEFT JOIN " . $this->meta_tbl . " AS mt ON mt.apollo_organization_id=".$wpdb->posts.".ID AND mt.meta_key='" . $sort . "' ";
                $clauses['orderby'] = " mt.meta_value " . $order ;
            }
            if ($_GET['orderby'] == 'phone') {
                $clauses['join'] = " LEFT JOIN " . $this->meta_tbl . " AS mt ON mt.apollo_organization_id=".$wpdb->posts.".ID AND mt.meta_key='" . Apollo_DB_Schema::_ORG_PHONE . "' ";
                $clauses['orderby'] = " mt.meta_value " . $order ;
            }
        }
        return $clauses;
    }
    
}    
    
endif;    

return new Apollo_Admin_Org();
