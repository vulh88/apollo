<?php
/**
 * Admin functions for the photo slider
 *
 * @author 		vulh
 * @category 	admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Apollo_Admin_CPT' ) ) {
    include( 'class-apollo-admin-cpt.php' );
}

if ( ! class_exists( 'Apollo_Admin_Photo_Slider' ) ) :

    class Apollo_Admin_Photo_Slider extends Apollo_Admin_CPT {

        public function __construct() {
            global $typenow, $pagenow, $wpdb;


            $this->type             = Apollo_DB_Schema::_PHOTO_SLIDER_PT;
            $this->meta_tbl  = $wpdb->{Apollo_Tables::_APL_PHOTO_SLIDER_META};

            $this->requireds = array(
                'post_title'    => array(
                    'label'     => __( 'The name', 'apollo' ),
                    'validate'  => 'required',
                ),
                APL_Photo_Slider_Const::NUMBER_IMAGES_ORG    => array(
                    'label'     => __( 'The number of images pulled form Organization', 'apollo' ),
                    'validate'  => 'number',
                ),
                APL_Photo_Slider_Const::NUMBER_IMAGES_ARTIST   => array(
                    'label'     => __( 'The number of images pulled form Artist', 'apollo' ),
                    'validate'  => 'number',
                ),
                APL_Photo_Slider_Const::NUMBER_IMAGES_PUBLIC_ART    => array(
                    'label'     => __( 'The number of images pulled form Public Art', 'apollo' ),
                    'validate'  => 'number',
                ),
                APL_Photo_Slider_Const::NUMBER_IMAGES_PUBLIC_EVENT    => array(
                    'label'     => __( 'The number of images pulled form Event', 'apollo' ),
                    'validate'  => 'number',
                ),
                APL_Photo_Slider_Const::NUMBER_IMAGES_PUBLIC_VENUE    => array(
                    'label'     => __( 'The number of images pulled form Venue', 'apollo' ),
                    'validate'  => 'number',
                ),
                APL_Photo_Slider_Const::NUMBER_IMAGES_PUBLIC_CLASSIFIED    => array(
                    'label'     => __( 'The number of images pulled form Classified', 'apollo' ),
                    'validate'  => 'number',
                ),
                APL_Photo_Slider_Const::NUMBER_IMAGES_PUBLIC_EDUCATION    => array(
                    'label'     => __( 'The number of images pulled form Education', 'apollo' ),
                    'validate'  => 'number',
                ),
            ); // TODO: add any required fields in here if needed.

            // Admin column
            add_filter( 'manage_edit-'.$this->type.'_columns', array( $this, 'edit_columns' ) );
            add_action( 'manage_'.$this->type.'_posts_custom_column', array( $this, 'custom_columns' ), 2 );
            add_filter( 'manage_edit-'.$this->type.'_sortable_columns', array( $this, 'custom_columns_sort' ) );

            // remove meta-box of Publishing Post in only this screen edit post type syndication
            // remove_meta_box('submitdiv',Apollo_DB_Schema::_SYNDICATION_PT,'side');

            add_action('admin_head-post.php', array($this, 'custom_publishing_layout'),99);
            add_action('admin_head-post-new.php', array($this, 'custom_publishing_layout'),99);

            /// Event filtering
            add_action( 'restrict_manage_posts', array( $this, '_filters' ) );



            // Update query
            if ( $typenow == $this->type && $pagenow == 'edit.php'
                && ( ( isset( $_GET['post_status'] ) && $_GET['post_status'] == 'publish' )  ) || ! isset( $_GET['post_status'] ) ) {
                add_filter( 'posts_join' , array( $this, 'filter_meta_join' ) );
                add_filter( 'posts_where', array( $this, 'posts_where' ) );
                add_filter( 'posts_orderby', array( $this, 'change_order_by' ) );
            }


            parent::__construct();

        }





        public function custom_publishing_layout(){
            global $post;
            if($post->post_type == Apollo_DB_Schema::_PHOTO_SLIDER_PT){
                echo '
                <style type="text/css">
                    #submitdiv #misc-publishing-actions,
                    #submitdiv #minor-publishing-actions,
                    #submitdiv .handlediv,
                     #titlediv .inside #edit-slug-box{
                        display:none;
                    }
                    #submitdiv{
                        position: fixed;
                    }
                    #publishing-action {
                        float: left;
                    }
                    #publishing-action .spinner {
                        float: right !important;
                    }
                    #publishing-action input[type=submit]{
                        width: 120px;
                    }
                    #submitdiv #delete-action{
                        float: right !important;
                    }
                  
                </style>
            ';
            }
        }


        /**
         * Custom post join
         *
         * @access public
         * @return string
         */
        public function filter_meta_join( $join ) {
            return $join;
        }

        /**
         * Replace meta table
         *
         * @access public
         * @return string
         */
        public function replace_meta_table( $str ) {
            /*global $wpdb;
            return str_replace( $wpdb->postmeta, $wpdb->apollo_artistmeta, $str);*/
            // TODO: handle this later because at present, syndication is storing data under table wordpress default post_meta
            return $str;
        }

        /**
         * Custom post where
         *
         * @access public
         * @return string
         */
        public function posts_where( $where ) {
            return $this->replace_meta_table( $where );
        }

        /**
         * Change order by
         *
         * @access public
         * @return string
         */
        public function change_order_by( $order_by ) {

            global $wpdb;

            $order_by = $wpdb->posts . ".post_title ASC";

            return $order_by;
        }

        public function _filters() {
            global $typenow;

            if ( $typenow !== $this->type ) {
                return ;
            }
        }



        // Define header columns
        public function edit_columns( $existing_columns ) {

            if ( empty( $existing_columns ) && ! is_array( $existing_columns ) ) {
                $existing_columns = array();
            }

            unset( $existing_columns['title'], $existing_columns['comments'], $existing_columns['date'] );

            $columns = array();
            $columns["cb"] = "<input type=\"checkbox\" />";
            $columns["id"] = __( 'ID', 'apollo' );
            $columns["name"] = __( 'Name', 'apollo' );
            $columns["short-code"] = __( 'Short code', 'apollo' );


            return array_merge( $columns, $existing_columns );
        }

        /**
         * Define our custom columns shown in admin.
         * @param  string $column
         */
        public function custom_columns( $column ) {
            global $post;

            switch ( $column ) {
                case "id":
                    echo "<b>" . $post->ID . "</b>";
                    break;
                case "name" :
                    $edit_link = get_edit_post_link( $post->ID );
                    $title = $post->post_title;
                    $post_type_object = get_post_type_object( $post->post_type );
                    $can_edit_post = current_user_can( $post_type_object->cap->edit_post, $post->ID );

                    echo '<strong><a class="row-title" href="' . esc_url( $edit_link ) .'">' . $title.'</a>';
                    _post_states( $post );
                    echo '</strong>';

                    // Get actions
                    $actions = array();

                    $actions['id'] = 'ID: ' . $post->ID;

                    if ( $can_edit_post && 'trash' != $post->post_status ) {
                        $actions['edit'] = '<a href="' . get_edit_post_link( $post->ID, true ) . '" title="' . esc_attr( __( 'Edit this item', 'apollo' ) ) . '">' . __( 'Edit', 'apollo' ) . '</a>';
                    }
                    if ( current_user_can( $post_type_object->cap->delete_post, $post->ID ) ) {
                        if ( 'trash' == $post->post_status )
                            $actions['untrash'] = "<a title='" . esc_attr( __( 'Restore this item from the Trash', 'apollo' ) ) . "' href='" . wp_nonce_url( admin_url( sprintf( $post_type_object->_edit_link . '&amp;action=untrash', $post->ID ) ), 'untrash-post_' . $post->ID ) . "'>" . __( 'Restore', 'apollo' ) . "</a>";
                        elseif ( EMPTY_TRASH_DAYS )
                            $actions['trash'] = "<a class='submitdelete' title='" . esc_attr( __( 'Move this item to the Trash', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID ) . "'>" . __( 'Trash', 'apollo' ) . "</a>";
                        if ( 'trash' == $post->post_status || !EMPTY_TRASH_DAYS )
                            $actions['delete'] = "<a class='submitdelete' title='" . esc_attr( __( 'Delete this item permanently', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID, '', true ) . "'>" . __( 'Delete Permanently', 'apollo' ) . "</a>";
                    }

                    $actions = apply_filters( 'post_row_actions', $actions, $post );

                    echo '<div class="row-actions">';

                    $i = 0;
                    $action_count = sizeof($actions);

                    foreach ( $actions as $action => $link ) {
                        ++$i;
                        ( $i == $action_count ) ? $sep = '' : $sep = ' | ';
                        echo "<span class='$action'>$link$sep</span>";
                    }
                    echo '</div>';
                    break;

                case 'short-code':
                    $val = Apollo_App::apollo_get_meta_data( $post->ID, APL_Photo_Slider_Const::PHOTO_SLIDER_SHORT_CODE );
                    $result = $val ? $val : '-';
                    echo  '<span>'.esc_attr($result).'</span>';
                    break;


            }
        }

        /**
         * Make columns sortable
         *
         * @access public
         * @param mixed $columns
         * @return array
         */
        public function custom_columns_sort( $columns ) {
            $custom = array(
                'name'  => 'post_title'
            );
            return wp_parse_args( $custom, $columns );
        }

    }

endif;

new Apollo_Admin_Photo_Slider();


