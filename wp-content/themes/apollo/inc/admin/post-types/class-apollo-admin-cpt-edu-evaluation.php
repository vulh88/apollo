<?php
/**
 * Admin functions for the venue post type
 *
 * @author 		vulh
 * @category 	admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Apollo_Admin_CPT' ) ) {
    include( 'class-apollo-admin-cpt.php' );
}

if ( ! class_exists( 'Apollo_Admin_Edu_Evaluation' ) ) :
    
class Apollo_Admin_Edu_Evaluation extends Apollo_Admin_CPT {
    
    public function __construct() {
        global $wpdb;
        $this->type = Apollo_DB_Schema::_EDU_EVALUATION;
        $this->meta_tbl  = $wpdb->{Apollo_Tables::_APL_EDU_EVAL_META};
        
        // Admin column
        add_filter( 'manage_edit-'.$this->type.'_columns', array( $this, 'edit_columns' ) );
        add_action( 'manage_'.$this->type.'_posts_custom_column', array( $this, 'custom_columns' ), 2 );
        add_filter( 'manage_edit-'.$this->type.'_sortable_columns', array( $this, 'custom_columns_sort' ) );

        
        /// Event filtering
        add_action( 'restrict_manage_posts', array( $this, '_filters' ) );
        
        add_action( 'delete_post', array( $this, 'delete_post' ) );
        
        $this->requireds = array(
            'district' => array( 
                'label'     => __( 'Your School District', 'apollo' ), 
                'validate'  => 'required' 
            ),
            'school' => array( 
                'label'     => __( 'Your School Name', 'apollo' ), 
                'validate'  => 'required' 
            ),
            Apollo_DB_Schema::_APL_EDU_EVAL_EDUCATOR => array( 
                'label'     => __( 'Cultural Organization', 'apollo' ), 
                'validate'  => 'required' 
            ),
            Apollo_DB_Schema::_APL_EDU_EVAL_PROG => array( 
                'label'     => __( 'Cultural Program Provided', 'apollo' ), 
                'validate'  => 'required' 
            ),
        );
        
        /**
         * filter join
         */
        add_filter('posts_join', array($this, 'filter_meta_join'));
        add_filter('posts_where', array($this, 'filter_meta_where'));
        
        parent::__construct();
    }
    
    public function filter_meta_join($join) {
        global $wpdb;
        
        if ( isset( $_GET['educator_id'] ) && $_GET['educator_id'] ) {
            $join .= "
                INNER JOIN {$this->meta_tbl} mt_edu ON mt_edu.apollo_edu_evaluation_id = ". $wpdb->posts .".ID
            ";
        }
        
        if ( isset( $_GET['program_id'] ) && $_GET['program_id'] ) {
            $join .= "
                INNER JOIN {$this->meta_tbl} mt_prog ON mt_prog.apollo_edu_evaluation_id = ". $wpdb->posts .".ID   
            ";
        }    
        
        return $join;
    }
    
    public function filter_meta_where($where) {
        
        if ( isset( $_GET['educator_id'] ) && $_GET['educator_id'] ) {
            $where .= "
                AND mt_edu.meta_key = '".Apollo_DB_Schema::_APL_EDU_EVAL_EDUCATOR."'
                AND mt_edu.meta_value = ".$_GET['educator_id']."    
            ";
        }
        
        if ( isset( $_GET['program_id'] ) && $_GET['program_id'] ) {
            $where .= "
                AND mt_prog.meta_key = '".Apollo_DB_Schema::_APL_EDU_EVAL_PROG."'
                AND mt_prog.meta_value = ".$_GET['program_id']."    
            ";
        }
        
        return $where;
    }
    
    /**
     * Replace meta table 
     * 
     * @access public
     * @return string
     */
    public function replace_meta_table( $str ) {
        global $wpdb;
        return str_replace( $wpdb->postmeta, $wpdb->edu_evaluationmeta, $str);
    }
    
    public function delete_post( $post_id ) {
        
    }
    
    public function _filters() {
        global $typenow;
        
        if ( $typenow !== $this->type ) {
            return ;
        }
    }
    
    /**
	 * Change the columns shown in admin.
	 */
	public function edit_columns( $existing_columns ) {
       
		if ( empty( $existing_columns ) && ! is_array( $existing_columns ) ) {
            $existing_columns = array();
        }

		unset( $existing_columns['title'], $existing_columns['comments'], $existing_columns['date'] );

		$columns = array();
		$columns["cb"] = "<input type=\"checkbox\" />";
        
         // Get cus field title
        $cus_title = get_option('apl_field_listing_'.$this->type, false);
        if ($cus_title) {
            $field = Apollo_Custom_Field::get_field_by_id($cus_title, $this->type);
            $columns['title'] = $field->label;
        } else {
            $columns["title"] = __( 'Title', 'apollo' );
        }
        //TriLm add column
        $columns['submit_user'] = __( 'Submitted User', 'apollo' );
        $columns["date"] = __( 'Date', 'apollo' );
		return array_merge( $columns, $existing_columns );
	}
    
    /**
	 * Define our custom columns shown in admin.
	 * @param  string $column
	 */
	public function custom_columns( $column ) {
        global $post, $the_org;
        
        if ( empty( $the_org ) || $the_org->id != $post->ID ) {
            
            $the_org = get_org( $post );
        }
        
        $eduEvalObj = get_edu_evaluation($post->ID);
        
        switch ( $column ) {
            
            case "title" :
				$edit_link = get_edit_post_link( $post->ID );
				$post_type_object = get_post_type_object( $post->post_type );
				$can_edit_post = current_user_can( $post_type_object->cap->edit_post, $post->ID );
                
                $cus_title = get_option('apl_field_listing_'.$this->type, false);
                $field = Apollo_Custom_Field::get_field_by_id($cus_title, $this->type);
                
                $title = '--';
                if ( $field ) {
                    $title = $eduEvalObj->get_meta_data($field->name, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA, '--');
                    if ( Apollo_Custom_Field::get_data_source($field) ) {
                        $p = get_post($title);
                        
                        if ( $p ) { // Get from post
                            $title = $p->post_title;
                        } else { // Get from taxanomy
                            $term = get_term($title, 'district-school');
                            $title = $term ? $term->name : '--';
                        }
                        
                    }
                }
                
				echo '<strong><a class="row-title" href="' . esc_url( $edit_link ) .'">' . $title.'</a>';

				_post_states( $post );

				echo '</strong>';

				if ( $post->post_parent > 0 )
					echo '&nbsp;&nbsp;&larr; <a href="'. get_edit_post_link($post->post_parent) .'">'. get_the_title($post->post_parent) .'</a>';

				// Excerpt view
				if (isset($_GET['mode']) && $_GET['mode']=='excerpt') echo apply_filters('the_excerpt', $post->post_excerpt);

				// Get actions
				$actions = array();

				$actions['id'] = 'ID: ' . $post->ID;

				if ( $can_edit_post && 'trash' != $post->post_status ) {
					$actions['edit'] = '<a href="' . get_edit_post_link( $post->ID, true ) . '" title="' . esc_attr( __( 'Edit this item', 'apollo' ) ) . '">' . __( 'Edit', 'apollo' ) . '</a>';
					$actions['inline hide-if-no-js'] = '<a href="#" class="editinline" title="' . esc_attr( __( 'Edit this item inline', 'apollo' ) ) . '">' . __( 'Quick&nbsp;Edit', 'apollo' ) . '</a>';
				}
				if ( current_user_can( $post_type_object->cap->delete_post, $post->ID ) ) {
					if ( 'trash' == $post->post_status )
						$actions['untrash'] = "<a title='" . esc_attr( __( 'Restore this item from the Trash', 'apollo' ) ) . "' href='" . wp_nonce_url( admin_url( sprintf( $post_type_object->_edit_link . '&amp;action=untrash', $post->ID ) ), 'untrash-post_' . $post->ID ) . "'>" . __( 'Restore', 'apollo' ) . "</a>";
					elseif ( EMPTY_TRASH_DAYS )
						$actions['trash'] = "<a class='submitdelete' title='" . esc_attr( __( 'Move this item to the Trash', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID ) . "'>" . __( 'Trash', 'apollo' ) . "</a>";
					if ( 'trash' == $post->post_status || !EMPTY_TRASH_DAYS )
						$actions['delete'] = "<a class='submitdelete' title='" . esc_attr( __( 'Delete this item permanently', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID, '', true ) . "'>" . __( 'Delete Permanently', 'apollo' ) . "</a>";
				}
				if ( $post_type_object->public ) {
					if ( in_array( $post->post_status, array( 'pending', 'draft', 'future' ) ) ) {
						if ( $can_edit_post )
							$actions['view'] = '<a href="' . esc_url( add_query_arg( 'preview', 'true', get_permalink( $post->ID ) ) ) . '" title="' . esc_attr( sprintf( __( 'Preview &#8220;%s&#8221;', 'apollo' ), $title ) ) . '" rel="permalink">' . __( 'Preview', 'apollo' ) . '</a>';
					} elseif ( 'trash' != $post->post_status ) {
						$actions['view'] = '<a href="' . get_permalink( $post->ID ) . '" title="' . esc_attr( sprintf( __( 'View &#8220;%s&#8221;', 'apollo' ), $title ) ) . '" rel="permalink">' . __( 'View', 'apollo' ) . '</a>';
					}
				}

				$actions = apply_filters( 'post_row_actions', $actions, $post );

				echo '<div class="row-actions">';

				$i = 0;
				$action_count = sizeof($actions);

				foreach ( $actions as $action => $link ) {
					++$i;
					( $i == $action_count ) ? $sep = '' : $sep = ' | ';
					echo "<span class='$action'>$link$sep</span>";
				}
				echo '</div>';
                get_inline_data( $post );
                
				echo '
					<div class="hidden" id="_inline_' . $post->ID . '">
						<div class="menu_order">' . $post->menu_order . '</div>
						<div class="_address1">' . $the_org->get_meta_data( Apollo_DB_Schema::_APL_ORG_ADDRESS. '['. Apollo_DB_Schema::_ORG_ADDRESS1 .']' ) . '</div>
                        <div class="_address2">' . $the_org->get_meta_data( Apollo_DB_Schema::_APL_ORG_ADDRESS. '['. Apollo_DB_Schema::_ORG_ADDRESS2 .']' ) . '</div>
                        <div class="_city">' . $the_org->get_meta_data( Apollo_DB_Schema::_APL_ORG_ADDRESS. '['. Apollo_DB_Schema::_ORG_CITY .']' ) . '</div>
                        <div class="_state">' . $the_org->get_meta_data( Apollo_DB_Schema::_APL_ORG_ADDRESS. '['. Apollo_DB_Schema::_ORG_STATE .']' ) . '</div>
                        <div class="_zip">' . $the_org->get_meta_data( Apollo_DB_Schema::_APL_ORG_ADDRESS. '['. Apollo_DB_Schema::_ORG_ZIP .']' ) . '</div>
                            
                        <div class="_phone">' . $the_org->get_meta_data( Apollo_DB_Schema::_APL_ORG_DATA. '['. Apollo_DB_Schema::_ORG_PHONE .']' ) . '</div>
                        <div class="_fax">' . $the_org->get_meta_data( Apollo_DB_Schema::_APL_ORG_DATA. '['. Apollo_DB_Schema::_ORG_FAX .']' ) . '</div>
                        <div class="_email">' . $the_org->get_meta_data( Apollo_DB_Schema::_APL_ORG_DATA. '['. Apollo_DB_Schema::_ORG_EMAIL .']' ) . '</div>
                        <div class="_website_url">' . $the_org->get_meta_data( Apollo_DB_Schema::_APL_ORG_DATA. '['. Apollo_DB_Schema::_ORG_WEBSITE_URL .']' ) . '</div>
					</div>
				';
                
			break;
           
            case 'school_name':
                $school = $eduEvalObj->get_school();
				echo $school ? $school->name : '-';
            break;
            
            case 'school_district':
                $district = $eduEvalObj->get_district();
				echo $district ? $district->name : '-';
            break;
        
            case 'educator':
                $val = Apollo_App::apollo_get_meta_data( $the_org->id, Apollo_DB_Schema::_APL_EDU_EVAL_EDUCATOR );
                $edu = get_post($val);
                echo $edu ? '<a href="'.get_edit_post_link($val).'">'.$edu->post_title. '</a>' : '-';
            break;
        
            case 'program':
                $val = Apollo_App::apollo_get_meta_data( $the_org->id, Apollo_DB_Schema::_APL_EDU_EVAL_PROG );
                $prog = get_post($val);
                echo $prog ? '<a href="'.get_edit_post_link($val).'">'.$prog->post_title. '</a>' : '-';
            break;

            //Tri LM -- submit_user column data
            case "submit_user":
                $author  = $post->post_author;
                if($author){
                    $authorName = get_the_author_meta( 'user_nicename' , $author );
                    $authorLink = get_edit_user_link($author);
                    echo '<a href="'.$authorLink.'">'.$authorName.'</a>';
                }

                break;
        }
        
    }
    
    /**
	 * Make columns sortable
	 *
	 * @access public
	 * @param mixed $columns
	 * @return array
	 */
	public function custom_columns_sort( $columns ) {
		$custom = array(
			'name'               => 'name',
			'submit_user'               => 'submit_user'
		);
		return wp_parse_args( $custom, $columns );
	}

    
}    
    
endif;    

return new Apollo_Admin_Edu_Evaluation();