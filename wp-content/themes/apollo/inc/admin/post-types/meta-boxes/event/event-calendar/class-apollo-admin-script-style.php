<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/8/2015
 * Time: 10:08 AM
 */

class Apollo_Admin_Script_Style{

    public function __construct() {
        add_action( 'admin_enqueue_scripts', array( $this, 'apollo_add_admin_script_style' ) );
    }
    //import script to admin
    function apollo_add_admin_script(){
        $scripts = array(
            'apollo-js-lightbox'          => '/inc/admin/assets/js/lightbox.min.js',
            'apollo-js-global'          => '/inc/admin/assets/js/event-calendar/global.js',
            'apollo-angular-js'          => 'https://ajax.googleapis.com/ajax/libs/angularjs/1.4.2/angular.min.js',
            'apollo-angular-calendar-js'          => '/inc/admin/assets/js/event-calendar/angular-apl-calendar.js',
            'apollo-angular-global-js'          => '/inc/admin/assets/js/event-calendar/global.js',
            'apollo-angular-moment-js'          => '/inc/admin/assets/js/event-calendar/moment.min.js',
            'apollo-angular-block-js'          => '/inc/admin/assets/js/event-calendar/jquery.blockUI.js',
            'apollo-angular-ex-plugin-js'          => '/inc/admin/assets/js/event-calendar/ex-plugins.js',
            'apollo-angular-state'          => '/inc/admin/assets/js/state.js',
            'apollo-angular-route'          => '/inc/admin/assets/js/angular-route.js',
            'apollo-angular-pagination'          => '/inc/admin/assets/js/angular-pagination.js',
        );

        //include script
        foreach($scripts as $k => $script){
            $prefix = $k == 'apollo-angular-js' ?
                '' : get_stylesheet_directory_uri();
            wp_enqueue_script($k , $prefix. $script, array('jquery'), null, true);
        }
        wp_localize_script( 'apollo-js-global' , 'APL', Apollo_App::jsCommunityWithClient());

    }

    //import style to admin
    function apollo_add_admin_style(){

        $styles = array(
            'apollo-admin-css-style' => '/inc/admin/assets/css/event-calendar/style.css',
            'apollo-admin-css-dialog' => '/inc/admin/assets/css/event-calendar/dialog.css',
            'apollo-admin-css-lightbox' => '/inc/admin/assets/css/lightbox.min.css',
        );
        //include style
        foreach($styles as $k => $style){
            wp_register_style($k , get_stylesheet_directory_uri().$style);
            wp_enqueue_style($k);
        }


        // Include bootstrap network admin
        if (isset($_GET['page']) && $_GET['page'] == 'apl-manage-states-cities') {
            wp_register_style('apl-bootstrap-network-admin' , 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css');
            wp_enqueue_style('apl-bootstrap-network-admin');
        }

    }

    //hook scripts and style to admin
    function apollo_add_admin_script_style(){
        self::apollo_add_admin_style();
        self::apollo_add_admin_script();
    }

}
new Apollo_Admin_Script_Style();