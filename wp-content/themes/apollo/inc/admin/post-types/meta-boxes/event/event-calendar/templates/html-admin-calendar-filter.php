<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/8/2015
 * Time: 10:36 AM
 */
?>
<div class="option_group calendar-filter-content">
    <div class=" filter">
        <div class="event-contact"><?php _e('Times', 'apollo') ?></div>
        <div class="time-filter">
            <div class="start">
                <label>&nbsp; <?php _e('Start time', 'apollo') ?> *</label>
                <select class="time _hour1">
                    <option value="01">01</option>
                    <option value="02">02</option>
                    <option value="03">03</option>
                    <option value="04">04</option>
                    <option value="05">05</option>
                    <option value="06">06</option>
                    <option value="07">07</option>
                    <option value="08">08</option>
                    <option value="09">09</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                </select>
                <select class="time mins _minute1">
                    <option value="00">00</option>
                    <option value="05">05</option>
                    <option value="10">10</option>
                    <option value="15">15</option>
                    <option value="20">20</option>
                    <option value="25">25</option>
                    <option value="30">30</option>
                    <option value="35">35</option>
                    <option value="40">40</option>
                    <option value="45">45</option>
                    <option value="50">50</option>
                    <option value="55">55</option>
                </select><span class="arrowM"></span>
                <select class="time ap _ampm1">
                    <option value="AM"><?php _e('AM', 'apollo') ?></option>
                    <option selected value="PM"><?php _e('PM', 'apollo') ?></option>
                </select>
            </div>
            <div class="end">
                <label>&nbsp; <?php _e('End time', 'apollo') ?></label>
                <select class="time _hour2">
                    <option value="--">--</option>
                    <option value="01">01</option>
                    <option value="02">02</option>
                    <option value="03">03</option>
                    <option value="04">04</option>
                    <option value="05">05</option>
                    <option value="06">06</option>
                    <option value="07">07</option>
                    <option value="08">08</option>
                    <option value="09">09</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                </select>
                <select class="time mins _minute2">
                    <option value="--">--</option>
                    <option value="00">00</option>
                    <option value="05">05</option>
                    <option value="10">10</option>
                    <option value="15">15</option>
                    <option value="20">20</option>
                    <option value="25">25</option>
                    <option value="30">30</option>
                    <option value="35">35</option>
                    <option value="40">40</option>
                    <option value="45">45</option>
                    <option value="50">50</option>
                    <option value="55">55</option>
                </select><span class="arrowM"> </span>
                <select class="time ap _ampm2">
                    <option value="--">--</option>
                    <option value="AM"><?php _e('AM', 'apollo') ?></option>
                    <option value="PM"><?php _e('PM', 'apollo') ?></option>
                </select>
            </div>
        </div>
        <div class="right width-40 text-right">
            <div class="clear-all clear-all-admin">
                <a href="javascript:void(0);" id="calendar_clear_all">
                    <span><?php _e('CLEAR ALL', 'apollo') ?> </span>
                </a>
            </div>
            <div class="apply-all apply-all-admin">
                <a href="javascript:void(0);" id="calendar_choice_all">
                    <span><?php _e('APPLY TO ALL', 'apollo') ?></span>
                </a>
            </div>
        </div>

    </div>
</div>