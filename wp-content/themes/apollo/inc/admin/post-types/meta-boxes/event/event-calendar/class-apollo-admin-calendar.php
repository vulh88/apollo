<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/8/2015
 * Time: 10:21 AM
 */


class Apollo_Admin_Calendar{

    public static function getAllDayInWeek(){
        $arrayDayOfWeek = array(
            '0' => 'Sun',
            '1' => 'Mon',
            '2' => 'Tue',
            '3' => 'Wed',
            '4' => 'Thu',
            '5' => 'Fri',
            '6' => 'Sat',
        );
        return $arrayDayOfWeek;
    }
    public static function getDaySelectedInWeek($thepostid = ''){
        $arrayDay  =   unserialize(Apollo_App::apollo_get_meta_data( $thepostid,  Apollo_DB_Schema::_APOLLO_EVENT_DAYS_OF_WEEK));

        return $arrayDay;
    }
    public static function getCalendarConfigForPost($postId = '',$config = array()){
        $starDate = Apollo_App::apollo_get_meta_data($postId,Apollo_DB_Schema::_APOLLO_EVENT_START_DATE);
        $endDate= Apollo_App::apollo_get_meta_data($postId,Apollo_DB_Schema::_APOLLO_EVENT_END_DATE);
        $daysInWeek = self::getDayInWeekForCalendar($postId);
        if(isset($config['start_date']))
            $starDate = $config['start_date'];
        if(isset($config['end_date']))
            $endDate = $config['end_date'];
        if(isset($config['day_in_week']))
            $daysInWeek = $config['day_in_week'];

        $d1 = date_create($starDate);
        $d2 = date_create($endDate);
        $dta = date_diff($d2, $d1);
        
        $_month = (strtotime($endDate) - strtotime($starDate)) / (24 * 60 * 60 * 30);

        $calendarType = 2;
        //calendar rule mont > 6, use calendar style 2
        if($_month < Apollo_App::configDateRange()){
            $calendarType = 1;
        }
        if($starDate == '')
            $starDate = '';
        if($endDate == '')
            $endDate = '';
//            $endDate = date('Y-m-d',time()+(29*86400));


        $arrConfig = array(
            'start_date' => $starDate ,
            'end_date' => $endDate,
            'calendar_type' => $calendarType,
            'post_id' => $postId,
            'day_in_week' =>$daysInWeek
        );
        return $arrConfig;
    }
    public static function getDayInWeekForCalendar($postId =''){
        $arrayDayInWeek = self::getDaySelectedInWeek($postId);
        $str = '';

        if (!is_array($arrayDayInWeek)) {
            $arrayDayInWeek = array();
        }

        for($i= 0 ;$i <= count($arrayDayInWeek)-2; $i++){
            $str .= $arrayDayInWeek[$i].',';
        }
        $str .= isset($arrayDayInWeek[count($arrayDayInWeek)-1]) ? $arrayDayInWeek[count($arrayDayInWeek)-1] : '';
        return $str;
    }
    public static function render_calendar($postId = '',$config = array()){
        //get config for calendar
        $calendarConfig = self::getCalendarConfigForPost($postId,$config);
        if(is_array($calendarConfig)){
            $daysInWeek = $calendarConfig['day_in_week'];
            $startDay = $calendarConfig['start_date'];
            $endDay = $calendarConfig['end_date'];
            $postId = $calendarConfig['post_id'];
            $calendarType = $calendarConfig['calendar_type'];
            //render admin calendar event here
            if($startDay != '' || $startDay != ''){
                echo '<div id="content_calendar_ajax">';
                include_once('templates/html-admin-calendar-style-'.$calendarType.'.php');
                include_once('templates/html-admin-calendar-view-more.php');
                echo '</div>';
            }
        }
    }
    public static function render_day_in_week($thepostid = ''){
        $arrayDayOfWeek = self::getAllDayInWeek();
        $arrayDayOfWeekSelected = self::getDaySelectedInWeek($thepostid);
        include('templates/html-admin-calendar-chose-day-in-week.php');

    }
    public static function render_filter(){
        include_once('templates/html-admin-calendar-filter.php');
    }
    public static function open_calendar($thepostid = ''){
        $allEvent = Apollo_Calendar::static_get_all_event($thepostid);
        $disableDay = Apollo_Calendar::static_get_disable_cell($thepostid);
        echo '<div data-all-event=\''.json_encode($allEvent).'\' data-disable-day=\''.json_encode($disableDay).'\'  class="events ng-scope" id="add-event-calendar" ng-app="dashboard">';
    }
    public static function close_calendar(){
        echo '</div>';
    }
    public static function render_update_calendar_button(){
        include_once('templates/html-admin-calendar-update-calendar-button.php');
    }
    public static function render_chose_date($thepostid = ''){
        $config = self::getCalendarConfigForPost($thepostid);
        include_once('templates/html-admin-calendar-chose-start-end-date.php');
    }
    public static function render_admin_calendar($thepostid = ''){
        self::render_chose_date($thepostid);
        self::open_calendar($thepostid);
        self::render_day_in_week($thepostid);

        $calendarConfig = self::getCalendarConfigForPost($thepostid);
        $activeCalendar = $calendarConfig['start_date'] != '' || $calendarConfig['end_date'] != '';
        if($activeCalendar){
            self::render_update_calendar_button();
            self::render_filter();
            self::render_calendar($thepostid);
        }
        self::close_calendar();

        if ($activeCalendar) {
            echo '<div class="options_group event-box" style="float: left; width: 100%;">';
                // Description
                apollo_wp_editor(  array(
                    'id' => Apollo_DB_Schema::_APOLLO_EVENT_DATA. Apollo_DB_Schema::_APL_EVENT_ADDITIONAL_TIME,
                    'name'          => Apollo_DB_Schema::_APOLLO_EVENT_DATA.'['. Apollo_DB_Schema::_APL_EVENT_ADDITIONAL_TIME .']',
                    'label' => __( 'Additional Time Info', 'apollo' ),
                    'desc_tip'      => 'true',
                    'description' => "",
                    'class'         => 'apl-editor',
                ) );
            echo '</div>';
        }
    }

}