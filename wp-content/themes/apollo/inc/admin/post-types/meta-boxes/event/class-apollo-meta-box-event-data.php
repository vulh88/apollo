<?php
/**
 * Event Data
 *
 * Displays the event data box, tabbed, with several panels covering price, stock etc.
 *
 * @author 		vulh
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Event_Data
 */
class Apollo_Meta_Box_Event_Data {

    public static function outputSearch() {
        ?>
        <div class="options_group event-search-box">
            <input placeholder="<?php _e('Keyword') ?>" type="text" id="apollo-post-search-input" value="">
            <input data-url="/wp-admin/edit.php?s=s&post_status=all&post_type=<?php echo Apollo_DB_Schema::_EVENT_PT ?>" 
                   type="button" class="button" id="apollo-event-search-submit" value="<?php _e('Submit', 'apollo') ?>"> 
        </div>
        <?php
    }
    
	/**
	 * Output the metabox
	 */
	public static function output( $_post ) {

        global $post, $wpdb, $thepostid;
        $post = $_post;
		wp_nonce_field( 'apollo_event_meta_nonce', 'apollo_event_meta_nonce' );
        $thepostid = $post->ID;
        ?>
        <div id="datetime-event-data" class="panel apollo_options_panel">
            <?php
                /***
                 * @author 		TriLM
                 * Add new Admin calendar function
                 *
                 */
              Apollo_Admin_Calendar::render_admin_calendar($thepostid);
            ?>

        </div>
        <div class="clear"></div>
		<?php
	}
    
    public static function output_admiss($_post) {
        global $post, $wpdb, $thepostid;
        $post = $_post;
		wp_nonce_field( 'apollo_event_meta_nonce', 'apollo_event_meta_nonce' );
        $thepostid = $post->ID;

        /** @Ticket #13392 */
        $free_event = of_get_option(Apollo_DB_Schema::_EVENT_ENABLE_FREE_EVENT, false);
        ?>
        <div id="admission-event-data" class="panel apollo_options_panel">
            <?php 
                echo '<div class="options_group">';

                apollo_wp_editor(  array(
                    'id' => Apollo_DB_Schema::_APOLLO_EVENT_DATA. Apollo_DB_Schema::_ADMISSION_DETAIL,
                    'name' => Apollo_DB_Schema::_APOLLO_EVENT_DATA. '['. Apollo_DB_Schema::_ADMISSION_DETAIL .']',
                    'label' => __( 'Detail Admission', 'apollo' ),
                    'desc_tip'      => 'true',
                    'description' => "",
                    'class'         => 'apl-editor',
                ) );

                echo '</div>';

                echo '<div class="options_group">';

                 apollo_wp_text_input(  array( 
                        'id' => Apollo_DB_Schema::_APOLLO_EVENT_DATA. '['. Apollo_DB_Schema::_ADMISSION_PHONE .']', 
                        'label' => __( 'Phone Admission', 'apollo' ), 
                        'desc_tip' => 'true', 
                        'description' => '', 
                        'class' => 'full',
                        'type'  => 'text',
                        'data_parent'   => '.admission_options',
                    ) );

                echo '</div>';

                echo '<div class="options_group">';

                 apollo_wp_text_input(  array( 
                        'id' => Apollo_DB_Schema::_APOLLO_EVENT_DATA. '['. Apollo_DB_Schema::_ADMISSION_TICKET_URL .']', 
                        'label' => __( 'Ticket URL', 'apollo' ), 
                        'desc_tip' => 'true', 
                        'description' => '', 
                        'class' => 'full apollo_input_url',
                        'type'  => 'text',
                        'data_parent'   => '.admission_options',
                    ) );

                echo '</div>';

                echo '<div class="options_group">';

                 apollo_wp_text_input(  array( 
                        'id' => Apollo_DB_Schema::_APOLLO_EVENT_DATA. '['. Apollo_DB_Schema::_ADMISSION_TICKET_EMAIL .']', 
                        'label' => __( 'Ticket Email', 'apollo' ), 
                        'desc_tip' => 'true', 
                        'description' => '', 
                        'class' => 'full apollo_input_email',
                        'type'  => 'text',
                        'data_parent'   => '.admission_options',
                    ) );

                echo '</div>';

                echo '<div class="options_group">';

                 apollo_wp_text_input(  array( 
                        'id' => Apollo_DB_Schema::_APOLLO_EVENT_DATA. '['. Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL .']', 
                        'label' => __( 'Discount URL', 'apollo' ), 
                        'desc_tip' => 'true', 
                        'description' => '', 
                        'class' => 'full apollo_input_url',
                        'type'  => 'text',
                        'data_parent'   => '.admission_options',
                    ) );

                echo '</div>';

                echo '<div class="options_group '. (!$free_event ? 'hidden' : '') .'">';
                    // Description
                    apollo_wp_checkbox(  array(
                        'id' => Apollo_DB_Schema::_APOLLO_EVENT_FREE_ADMISSION,
                        'label' => __( 'Free Admission', 'apollo' ),
                        'desc_tip' => 'true',
                        'description' => "",
                        'type' => 'checkbox', ) );
                echo '</div>';

                /*@ticket 17346 */
                if(of_get_option(Apollo_DB_Schema::_EVENT_FORM_ENABLE_DISCOUNT_TEXT, 0)) {
                    echo '<div class="options_group">';

                    apollo_wp_text_input(array(
                        'id' => Apollo_DB_Schema::_APOLLO_EVENT_DATA . '[' . Apollo_DB_Schema::_ADMISSION_DISCOUNT_TEXT . ']',
                        'label' => __('Discount Text', 'apollo'),
                        'desc_tip' => 'true',
                        'description' => '',
                        'class' => 'full',
                        'type' => 'text',
                        'data_parent' => '.admission_options',
                    ));

                    echo '</div>';
                }
            ?>
        </div>
            
			<div class="clear"></div>
		<?php
    }

    public static function output_video($_post) {
        global $post, $wpdb, $thepostid;
        $post = $_post; // Keep global post because this form apply remote data in select dropdown, after get_post('Venue ID or ORG ID') the global post is reseted
		wp_nonce_field( 'apollo_event_meta_nonce', 'apollo_event_meta_nonce' );
        $thepostid = $post->ID;
        ?>
        <!-- Video event data section -->
        <div id="video-event-data" class="panel apollo_options_panel">

            <div id="video-event-data">
                <div class="video-wrapper">
                    <?php
                    $event_data = maybe_unserialize( get_apollo_meta( $thepostid, Apollo_DB_Schema::_APOLLO_EVENT_DATA, TRUE ) );

                    $videos = isset( $event_data[Apollo_DB_Schema::_VIDEO] ) ? $event_data[Apollo_DB_Schema::_VIDEO] : '';

                    if ( ! $videos ) {
                        $videos = array();
                        $videos[] = array( 'embed' => '', 'desc' => '' );
                    }

                    $i = 0;
                    foreach( $videos as $index => $v ):
                        $prefix_id = $i != 0 ? '-'.$i : '';

                        if (empty($v['embed']) && $index) continue;

                        ?>
                        <div class="count video-list<?php echo $prefix_id; ?>">

                            <div class="options_group">
                                <p class="form-field video_embed_field ">
                                    <label for="video_embed"><?php _e( 'YouTube or Vimeo URL', 'apollo' ) ?></label>
                                    <input data-parent=".video_options" name="video_embed[]" type="text" value="<?php echo isset($v['embed']) ? $v['embed'] : '' ?>" class="full apollo_input_url" />
                                </p>
                            </div>

                            <div class="options_group">
                                <p class="form-field video_desc_field ">
                                    <label for="video_embed"><?php _e( 'Video Description', 'apollo' ) ?></label>
                                    <textarea class="full" name="video_desc[]" placeholder="" rows="2" cols="20"><?php echo isset($v['desc']) ? $v['desc'] : '' ?></textarea>
                                </p>
                            </div>

                            <div <?php if ( $i == 0 ) echo 'style="display: none;"' ?> data-confirm="<?php _e( 'Are you sure to remove this video ?', 'apollo' ) ?>" class="del"><i class="fa fa-times"></i></div>

                        </div>
                        <?php $i++; endforeach; ?>
                </div>
                <input type="hidden" name="<?php echo Apollo_DB_Schema::_APOLLO_EVENT_DATA ?>[<?php echo Apollo_DB_Schema::_VIDEO ?>]" />
                <input type="button" class="button button-primary button-large" id="apl-add-video" value="<?php _e( 'Add more links', 'apollo' ) ?>" />
            </div>
        </div>
        <div class="clear"></div>
		<?php
    }

    /**
	 * Output the metabox
	 */
	public static function output_contact_info( $_post ) {
        global $post, $thepostid;
        $post = $_post;
		wp_nonce_field( 'apollo_event_meta_nonce', 'apollo_event_meta_nonce' );
        $thepostid = $post->ID;
        
        echo '<div class="options_group event-box">';
            
            // Description
            apollo_wp_text_input(  array( 
                'id' => Apollo_DB_Schema::_APOLLO_EVENT_DATA.'['. Apollo_DB_Schema::_E_CONTACT_NAME .']',
                'label' => __( 'Contact Name', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'type' => 'text',
            ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => Apollo_DB_Schema::_APOLLO_EVENT_DATA.'['. Apollo_DB_Schema::_E_CONTACT_EMAIL .']', 
                'label' => __( 'Contact Email', 'apollo' ), 
                'type' => 'text',
                'class' => 'apollo_input_email', 
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => Apollo_DB_Schema::_APOLLO_EVENT_DATA.'['. Apollo_DB_Schema::_E_CONTACT_PHONE .']', 
                'label' => __( 'Contact Phone', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'type' => 'text',
            ) );
        echo '</div>';
        
	}
    
    /**
     * Output Accessibility
     */
    public static function output_acb( $_post ) {
        global $post;
        $post = $_post;
        $event = get_event( $post ); 
        global $apollo_event_access;
        $_arr = $apollo_event_access;
        
        echo '<div class="options_group event-box"><ul id="info-access-ul">';
        ksort( $_arr );
        $_acb = $event->get_meta_data( Apollo_DB_Schema::_E_CUS_ACB, Apollo_DB_Schema::_APOLLO_EVENT_DATA );
        $eventACBInfo = $event->get_meta_data( Apollo_DB_Schema::_E_ACB_INFO, Apollo_DB_Schema::_APOLLO_EVENT_DATA );

        foreach ( $_arr as $img => $label ):
            $checked = $_acb && in_array( $img, $_acb ) ? 'checked' : '';
            echo '<li>
            <input '.$checked.' type="checkbox" name="'.Apollo_DB_Schema::_APOLLO_EVENT_DATA.'['. Apollo_DB_Schema::_E_CUS_ACB .'][]" value="'.$img.'" />
            <img src="'. get_template_directory_uri().'/assets/images/event-accessibility/'.$img.'.png" /> <span>'.$label.'</span></li>';
        endforeach;
        
        echo '</ul>';

        echo '<div class="options_group event-box" style="float: left; width: 100%;">';
        // Description
        apollo_wp_editor(  array(
            'id' => Apollo_DB_Schema::_APOLLO_EVENT_DATA. Apollo_DB_Schema::_E_ACB_INFO,
            'name' => Apollo_DB_Schema::_APOLLO_EVENT_DATA.'['. Apollo_DB_Schema::_E_ACB_INFO .']',
            'label' => __( 'Additional accessibility info', 'apollo' ),
            'desc_tip'      => 'true',
            'description' => "",
            'class'         => 'apl-editor',
        ) );
        echo '</div>';

        echo '</div>';
    }
    
    /**
	 * Save meta box data
	 */
	public static function save( $post_id, $post ) {

        self::_save($post_id);

        /** Vandd - @Ticket #12584 - add action hook for plugin "copy-to-mini-site" */
        global $apollo_mini_submit_sites;
        if ($apollo_mini_submit_sites) {
            foreach($apollo_mini_submit_sites as $mini_site_id => $info){
                $mini_post_id = isset($info["mini_post_id"]) ? $info["mini_post_id"] : null;
                if($mini_post_id && $mini_site_id){
                    if(switch_to_blog($mini_site_id)){
                        Apollo_Mini_Site_App::insertIclTranslations($mini_post_id, 'event');
                        self::_save($mini_post_id, true);
                        restore_current_blog();
                    }
                    //update featured image
                    Apollo_Mini_Site_App::updateAttachment($mini_site_id, $post_id, $mini_post_id);

                    //update tags to mini site
                    Apollo_Mini_Site_App::copyTagsToMiniSite($post_id, $mini_site_id, $mini_post_id, 'event-tag');

                    //update calendars
                    Apollo_Event_Copy_To_Mini_Site::copyEventCalendars($post_id, $mini_site_id, $mini_post_id);
                }
            }
        }
        //do_action('updated_eventmeta', $post_id);
	}

	private static function _save($post_id, $shouldRemovePrivate = false){
        $_apollo_event_data = $_POST[Apollo_DB_Schema::_APOLLO_EVENT_DATA];
        $_apollo_day_of_week =$_POST[Apollo_DB_Schema::_APOLLO_EVENT_DATA][Apollo_DB_Schema::_APOLLO_EVENT_DAYS_OF_WEEK];

        $video_embed = isset( $_POST['video_embed'] ) ? $_POST['video_embed'] : '';
        $video_desc = isset( $_POST['video_desc'] ) ? $_POST['video_desc'] : '';


        $_arr_video = array();
        if ( $video_embed ) {
            $i = 0;
            foreach ( $video_embed as $v ) {
                if ( ! $v ) continue;
                $_arr_video[] = array( 'embed' => $v, 'desc' => isset( $video_desc[$i] ) ?  $video_desc[$i] : '' );
                $i++;
            }
        }

        $_apollo_event_data[Apollo_DB_Schema::_VIDEO] = $_arr_video;


        if ( isset( $_POST[Apollo_DB_Schema::_APOLLO_EVENT_START_DATE] ) ) {
            update_apollo_meta( $post_id, Apollo_DB_Schema::_APOLLO_EVENT_START_DATE, stripslashes( sanitize_text_field( $_POST[Apollo_DB_Schema::_APOLLO_EVENT_START_DATE] ) ) );
        }

        if ( isset( $_POST[Apollo_DB_Schema::_APOLLO_EVENT_END_DATE] ) ) {
            update_apollo_meta( $post_id, Apollo_DB_Schema::_APOLLO_EVENT_END_DATE, stripslashes( sanitize_text_field( $_POST[Apollo_DB_Schema::_APOLLO_EVENT_END_DATE] ) ) );
        }



        Apollo_Admin_Event_Meta_Boxes::get_url_data( Apollo_DB_Schema::_ADMISSION_TICKET_URL, $_apollo_event_data, __( 'Admission ticket', 'apollo' ) );
        Apollo_Admin_Event_Meta_Boxes::get_email_data( Apollo_DB_Schema::_ADMISSION_TICKET_EMAIL, $_apollo_event_data, __( 'Admission', 'apollo' ) );
        Apollo_Admin_Event_Meta_Boxes::get_url_data( Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL, $_apollo_event_data, __( 'Admission', 'apollo' ) );
        Apollo_Admin_Event_Meta_Boxes::get_url_data( Apollo_DB_Schema::_WEBSITE_URL, $_apollo_event_data, __( 'Website', 'apollo' ) );
        Apollo_Admin_Event_Meta_Boxes::get_simple_data( Apollo_DB_Schema::_ADMISSION_TICKET_INFO, $_apollo_event_data );

        /**
         * Contact info
         */
        Apollo_Admin_Event_Meta_Boxes::get_email_data( Apollo_DB_Schema::_E_CONTACT_EMAIL, $_apollo_event_data, __( 'Contact', 'apollo' ) );
        Apollo_Admin_Event_Meta_Boxes::get_simple_data( Apollo_DB_Schema::_E_CONTACT_NAME, $_apollo_event_data );
        Apollo_Admin_Event_Meta_Boxes::get_simple_data( Apollo_DB_Schema::_E_CONTACT_PHONE, $_apollo_event_data );

        // Remove private for the child site
        if ($shouldRemovePrivate) {
            if (isset($_POST[Apollo_DB_Schema::_APOLLO_EVENT_DATA][Apollo_DB_Schema::_E_PRIVATE])) {
                unset($_POST[Apollo_DB_Schema::_APOLLO_EVENT_DATA][Apollo_DB_Schema::_E_PRIVATE]);
            }

            if (isset($_apollo_event_data[Apollo_DB_Schema::_E_PRIVATE])) {
                unset($_apollo_event_data[Apollo_DB_Schema::_E_PRIVATE]);
            }
        }

        /** @Ticket #13396 */
        if (isset( $_POST[Apollo_DB_Schema::_APOLLO_EVENT_DATA][Apollo_DB_Schema::_APOLLO_EVENT_CUSTOM_ACCESSIBILITY])) {
            update_apollo_meta( $post_id, Apollo_DB_Schema::_APOLLO_EVENT_CUSTOM_ACCESSIBILITY, serialize( Apollo_App::clean_array_data($_POST[Apollo_DB_Schema::_APOLLO_EVENT_DATA][Apollo_DB_Schema::_APOLLO_EVENT_CUSTOM_ACCESSIBILITY])));
        }

        update_apollo_meta( $post_id, Apollo_DB_Schema::_APOLLO_EVENT_DATA, serialize( Apollo_App::clean_array_data($_apollo_event_data) ) );
        // Update discount for searching
        update_apollo_meta( $post_id, Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL, Apollo_App::clean_data_request($_apollo_event_data[Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL]) );


        $free_admission = isset( $_POST[Apollo_DB_Schema::_APOLLO_EVENT_FREE_ADMISSION] ) ? $_POST[Apollo_DB_Schema::_APOLLO_EVENT_FREE_ADMISSION] : 'no';
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APOLLO_EVENT_FREE_ADMISSION, stripslashes( sanitize_text_field( $free_admission ) ) );

        self::checkOldDateInWeek($post_id);
        update_apollo_meta($post_id,Apollo_DB_Schema::_APOLLO_EVENT_DAYS_OF_WEEK,serialize($_apollo_day_of_week));


        // Update save to mini site
        update_apollo_meta($post_id, Apollo_DB_Schema::_APL_EVENT_MINI_SITE, implode(',', isset( $_POST['mini_sites'] ) ? $_POST['mini_sites'] : array() ));
        // Update save to mini site

        $isDoNotDisplay =  '';
        if(isset($_POST[Apollo_DB_Schema::_APOLLO_EVENT_DATA][Apollo_DB_Schema::_E_PRIVATE])
            && $_POST[Apollo_DB_Schema::_APOLLO_EVENT_DATA][Apollo_DB_Schema::_E_PRIVATE] == 'yes'
        ){
            $isDoNotDisplay = 'yes';

        }

        $preferredSearch =  0;
        if(isset($_POST[Apollo_DB_Schema::_APOLLO_EVENT_DATA][Apollo_DB_Schema::_E_PREFERRED_SEARCH])
            && $_POST[Apollo_DB_Schema::_APOLLO_EVENT_DATA][Apollo_DB_Schema::_E_PREFERRED_SEARCH] == 'yes'
        ){
            $preferredSearch = 1;

        }
        update_apollo_meta($post_id, Apollo_DB_Schema::_E_PREFERRED_SEARCH, $preferredSearch);
        update_apollo_meta($post_id, Apollo_DB_Schema::_E_PRIVATE, $isDoNotDisplay);


        /**
         * @Ticket #15089
         * Keep private events in option
        */
        $privateEventIDs = maybe_unserialize(get_option('apollo_private_events'));

        if (!is_array($privateEventIDs)) {
            $privateEventIDs = array();
        }

        if ($isDoNotDisplay) {
            $privateEventIDs[] = $post_id;
        }
        else {
            $key = array_search($post_id, $privateEventIDs);
            if($key!==false){
                unset($privateEventIDs[$key]);
            }
        }

        $privateEventIDs = array_unique($privateEventIDs);
        update_option('apollo_private_events', maybe_serialize($privateEventIDs), 'no');
    }

    public static function getUserAssociate($post){
        global $post;
        if($post){
            $listUsers = array();
            if (!$user = get_user_by( 'id', $post->post_author)) {
                return false;
            }

            $listUsers[$user->ID] = $user->user_nicename;

            if(current_user_can('manage_options')){
                echo '<div class="current-user">';
                echo '<br/>';
                echo '<a target="_BLANK" href="'.  get_edit_user_link($post->post_author).'" >'.$user->user_nicename.'</a>';
                echo '<div id="publishing-action">';
                echo '<div class="associte_user_loading">';
                    echo '<span class="spinner is-active"></span>';
                echo '</div>';
                echo '<a href="javascript:void(0)" class="button-primary" id="change_user" >' .__("Change User","apollo"). '</a>';
                echo '</div>';
                echo '<br/>';
                echo '</div>';

                echo '<div class="options_group event-box select-user" style="display: none">';
                apollo_wp_select(  array(
                    'id'            => Apollo_DB_Schema::_APOLLO_EVENT_ASSOCIATED_USER,
                    'name'          => Apollo_DB_Schema::_APOLLO_EVENT_ASSOCIATED_USER,
                    'label'         => '',
                    'desc_tip'      => 'true',
                    'description'   => "",
                    'attr' => array('append'=>0),
                    'class'         => 'assoc_user_select truong',
                    'options'       => $listUsers,
                    'first_option'  => __('Select user', 'apollo'),
                    'value'         => $user->ID,
                ) );

                echo '<a href="javascript:void(0)" class="button-primary" data-message-confirm="'.__("Are you sure you want to change this user?","apollo").'"
            data-user-id="'.$user->ID.'" data-post-id="'.$post->ID.'" id="confirm_change_user" >' .__("Change","apollo"). '</a>';
                echo '<a href="javascript:void(0)" class="button-primary" data-message-warning="'.__("Please select the different user to change","apollo").'"
                        id="cancel_change_user" >' .__("Cancel","apollo"). '</a>';
                echo '</div>';

                echo '<div class="clear"></div>';
            } else{
                echo '<a target="_BLANK" href="'.  get_edit_user_link($post->post_author).'" >'.$user->user_nicename.'</a>';
            }


        }
    }

    public static function checkOldDateInWeek($post_id){
        $old_dows =  get_apollo_meta($post_id, Apollo_DB_Schema::_APOLLO_EVENT_DAYS_OF_WEEK,Apollo_DB_Schema::_APOLLO_EVENT_DATA ) ;
        $old_dows = maybe_unserialize($old_dows);
        $new_dows = $_POST[Apollo_DB_Schema::_APOLLO_EVENT_DATA][Apollo_DB_Schema::_APOLLO_EVENT_DAYS_OF_WEEK];
        $arrayDelete = array();

        if(is_array($new_dows) && $old_dows){
            foreach($old_dows as $item){
                if(!in_array($item,$new_dows)){
                    $arrayDelete[] = $item;
                }
            }
        }
        
            
        if ( $arrayDelete != null ) {
            
            // Keep log update day in weeks
            $logObj = new Apollo_Logs("Updated day in week event_id = $post_id, arr_day = ".implode(',', $arrayDelete)." ", __FUNCTION__);
            $logObj->logs();

            $arrdate_event = array();
            foreach ( $arrayDelete as $val_date ) {
                $event_date = Apollo_Calendar::$arrWeeks[$val_date];
                $arrdate_event = array_merge( $arrdate_event, Apollo_Calendar::getStepsDateByDay($post_id, $event_date, $event_date) ) ;
            }

            if ($arrdate_event != null) {
                Apollo_Calendar::deleteEventByListDate($post_id , implode(',', $arrdate_event) );
                $logObj = new Apollo_Logs("Delete the list event date/time of event_id = $post_id, arr_date ".implode(',', $arrdate_event)." ", __FUNCTION__);
                $logObj->logs();
            }
        }




    }

    /***
     * @author: Trilm
     * @param $post
     */
    public static function outputDisplay( $post ) {

        global $post, $thepostid;
        wp_nonce_field( 'apollo_event_meta_nonce', 'apollo_event_meta_nonce' );
        $thepostid = $post->ID;
        echo '<div >';
        apollo_wp_checkbox(  array(
            'id' => Apollo_DB_Schema::_APOLLO_EVENT_DATA . '['. Apollo_DB_Schema::_E_PRIVATE .']',
            'label' => __( 'Private', 'apollo' ),
        ) );
        echo '</div>';

        self::outputPreferredSearch($post);
    }

    /**
     * @author: Trilm
     * @param $post
     */
    public static function outputPreferredSearch($post){
        global $post, $thepostid;
        wp_nonce_field( 'apollo_event_meta_nonce', 'apollo_event_meta_nonce' );
        $thepostid = $post->ID;
        echo '<div >';
        apollo_wp_checkbox(  array(
            'id' => Apollo_DB_Schema::_APOLLO_EVENT_DATA . '['. Apollo_DB_Schema::_E_PREFERRED_SEARCH .']',
            'label' => __( 'Preferred Search', 'apollo' ),
        ) );
        echo '</div>';
    }
}