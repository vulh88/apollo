<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/8/2015
 * Time: 10:36 AM
 */
?>
    <div class="option_group admin-calendar left full-width">
        <div class="calendar-container" ng-controller="calendar">
            <calendar data-dws="<?php echo $daysInWeek ?>" selected="day" data-type="1" data-eid="<?php echo $postId ?>" data-sd="<?php echo $startDay ?>" data-ed="<?php echo $endDay ?>"></calendar>
        </div>
    </div>