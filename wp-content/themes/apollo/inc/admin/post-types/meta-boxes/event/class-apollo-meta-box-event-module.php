<?php
/**
 * Event Meta box
 *
 * @author 		vulh
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Event
 */
class Apollo_Meta_Box_Event_Module {

    public static $post_type   = 'venue';
    public static $key         = Apollo_DB_Schema::_APOLLO_EVENT_VENUE;

    protected static function init( $key, $post_type ) {
        self::$key      = $key;
        self::$post_type = $post_type;
    }

    /**
	 * Output the metabox
	 */
	protected static function output_html( $post ) {
        global $wpdb;
        $status = array( 'publish','pending' );
        $artistId =  Apollo_App::apollo_get_meta_data( $post->ID, self::$key);
        $artists = $wpdb->get_results( "
            SELECT *
            FROM $wpdb->posts AS p
            WHERE post_type = '".Apollo_DB_Schema::_ARTIST_PT."' AND post_status IN (\"".  implode('","', $status)."\") AND p.ID = {$artistId}" );

        $attr = sprintf('data-enable-remote="%s" data-post-type="%s" data-source-url="%s"', 1,Apollo_DB_Schema::_ARTIST_PT, 'apollo_get_remote_data_to_select2_box' );

        echo '<div class="options_group event-box">';
        // Description
        apollo_wp_select(  array(
            'id'            => self::$key,
            'name'          => self::$key,
            'label'         => '',
            'desc_tip'      => 'true',
            'description'   => "",
            'attr'         => $attr,
            'options' =>$artists,
            'value'         => $artistId,
            'class'         => 'apl_select2',
            'first_option'  => __('Select Artist', 'apollo'),
            'is_post_type_list' => true,
        ) );
        echo '</div>';
    }

    private static function _get_options( $post, $select_label = '',$post_status = array('publish') ) {
        global $post, $wpdb, $thepostid;
		wp_nonce_field( 'apollo_event_meta_nonce', 'apollo_event_meta_nonce' );
        $thepostid = $post->ID;

        $options = array();

        $data = Apollo_App::get_list_post_type_items( self::$post_type,true,$post_status);

        if ( ! $data ) {
            return;
        }

        $options[0] = $select_label ? $select_label : self::get_select_label();

        foreach ( $data as $d ) {
            $options[$d->ID] = $d->post_status != 'publish'?  $d->post_title.__('*','apollo') : $d->post_title;
        }
        return $options;
    }

    private static function get_select_label() {
        global $apollo_modules;
        if ( isset( $apollo_modules[self::$post_type] ) ) {
            return __( 'Select '. $apollo_modules[self::$post_type]['sing'], 'apollo' );
        }

        return __( 'Select', 'apollo' );
    }

    public static function save_data( $post_id, $post ) {
        return self::_save_data($post_id);
    }

    private static function _save_data($post_id){

        $value = isset( $_POST[self::$key] ) ? stripslashes( sanitize_text_field( $_POST[self::$key] ) ) : '';
        update_apollo_meta( $post_id, self::$key, $value );
        return $value;
    }

    public static function save_venue( $post_id, $post ) {

        // Save tmp venue
        self::init( Apollo_DB_Schema::_APL_EVENT_TMP_VENUE, Apollo_DB_Schema::_EVENT_PT );
        foreach($_POST[self::$key]  as $index => $value){
            if ( !empty($value ) && $index != Apollo_DB_Schema::_VENUE_WEBSITE_URL ) {
                $_POST[self::$key][$index] = stripslashes( sanitize_text_field( $value));
            }
        }
        $value = isset( $_POST[self::$key] ) ? $_POST[self::$key] : '';

        if ( isset( $value[Apollo_DB_Schema::_VENUE_WEBSITE_URL] ) && ! filter_var( $value[Apollo_DB_Schema::_VENUE_WEBSITE_URL], FILTER_VALIDATE_URL ) ) {
            $value[Apollo_DB_Schema::_VENUE_WEBSITE_URL] = '';
        }
        if(!isset($value[Apollo_DB_Schema::_VENUE_REGION])){
            $value[Apollo_DB_Schema::_VENUE_REGION] = Apollo_App::apollo_get_meta_data($post_id, Apollo_DB_Schema::_VENUE_REGION, Apollo_DB_Schema::_APL_EVENT_TMP_VENUE );
        }

        if(!isset($value[Apollo_DB_Schema::_VENUE_ZIP])){
            $value[Apollo_DB_Schema::_VENUE_ZIP] = Apollo_App::apollo_get_meta_data($post_id, Apollo_DB_Schema::_VENUE_ZIP, Apollo_DB_Schema::_APL_EVENT_TMP_VENUE );
        }

        update_apollo_meta( $post_id, self::$key, serialize( $value ) );


        // Save primary venue
        $avaiable_modules = Apollo_App::get_avaiable_modules();
        if ( Apollo_App::is_avaiable_module( 'venue', $avaiable_modules ) ) {
            self::init( Apollo_DB_Schema::_APOLLO_EVENT_VENUE, Apollo_DB_Schema::_VENUE_PT );
            $venueID = self::save_data( $post_id, $post );

            /** Update venue to mini site */
            self::_minisite_save_venue($post_id, $venueID, $value);

        }
    }

    /**
     * Handle copy venue to mini site
     * @param $post_id
     * @param $venueID
     * @param bool $tmpVenue
     */
    private static function _minisite_save_venue($post_id, $venueID, $tmpVenue = false)
    {
        global $apollo_mini_submit_sites, $wpdb;

        if ($apollo_mini_submit_sites) {
            foreach($apollo_mini_submit_sites as $mini_site_id => $info){
                $mini_post_id = isset($info["mini_post_id"]) ? $info["mini_post_id"] : null;
                if($mini_post_id && $mini_site_id) {

                    $metaValuesVenue = get_meta_data_venue($venueID);
                    $copyVenue = get_venue_xref_data($venueID);
                    $venuePost = $venueID ? get_post($venueID, ARRAY_A) : false;
                    $parent_gallery_ids = get_apollo_meta($venueID, Apollo_DB_Schema::_APL_VENUE_IMAGE_GALLERY);
                    $parent_gallery_ids = $parent_gallery_ids ? explode(',', $parent_gallery_ids[0]) : '';
                    if(switch_to_blog($mini_site_id)) {

                        // save tmp venue
                        if ($tmpVenue) {
                            update_apollo_meta( $mini_post_id, Apollo_DB_Schema::_APL_EVENT_TMP_VENUE, serialize( $tmpVenue ) );
                        }


                        // Handle venue meta
                        $miniSiteVenueID = count($copyVenue) ? $copyVenue[0]->mini_venueID : 0;

                        // Ignore a draft, trash org
                        $venue = get_post($miniSiteVenueID);
                        if (($venue->post_status != 'publish' && $venue->post_status != 'pending') || $venue->post_type != 'venue') {
                            $miniSiteVenueID = false;
                        }

                        if ($venuePost) {
                            if (!$miniSiteVenueID) {
                                //Check Venue by name
                                $insertVenueID = Apollo_Mini_Site_App::checkPostByName(Apollo_DB_Schema::_VENUE_PT, $venuePost['post_title']);
                                if(!$insertVenueID){
                                    $venuePost['ID'] = '';
                                    $venuePost['post_status'] = 'publish';

                                    $insertVenueID = wp_insert_post($venuePost);
                                }

                                // copy venue meta
                                Apollo_Mini_Site_App::copyMetaData($insertVenueID, $metaValuesVenue);

                                // Update the venue IDs here before returning to current blog
                                $table = $wpdb->prefix . "apollo_eventmeta";
                                $data = array('meta_value' => $insertVenueID);
                                $where = array('apollo_event_id' => $mini_post_id, 'meta_key' => '_apollo_event_venue');
                                copy_meta_data_with_where($table, $data, $where);
                            }
                            else {
                                // Update the venue IDs here before returning to current blog
                                $wpdb->get_results("update $wpdb->apollo_eventmeta set meta_value = $miniSiteVenueID where meta_key = '_apollo_event_venue' and apollo_event_id = $mini_post_id");
                                $insertVenueID = $miniSiteVenueID;
                            }
                        }
                        else {
                            $insertVenueID = 0;
                        }

                        Apollo_Mini_Site_App::insertIclTranslations($insertVenueID, 'venue');

                        // Store event venue
                        update_apollo_meta( $mini_post_id, Apollo_DB_Schema::_APOLLO_EVENT_VENUE, intval($insertVenueID) );

                        restore_current_blog();

                        if(($insertVenueID)){
                            //update thumbnail
                            Apollo_Mini_Site_App::updateAttachment($mini_site_id, $venueID, $insertVenueID);
                            //update gallery
                            Apollo_Mini_Site_App::updateGalleryImagesToMiniSite($insertVenueID, $parent_gallery_ids, $mini_site_id, Apollo_DB_Schema::_APL_VENUE_IMAGE_GALLERY);

                            //copy category
                            $cat_type = Apollo_DB_Schema::_VENUE_PT . '-type';
                            Apollo_Mini_Site_App::copyCategoryToMiniSite($venueID, $mini_site_id, $insertVenueID, 0, $cat_type, '', '');
                        }

                        // Update xref data
                        update_xref_data([
                            'parent_post_id'    => $post_id,
                            'venueID'   => $venueID,
                            'mini_venueID'  => $insertVenueID,
                            'mini_site_id' => $mini_site_id
                        ]);

                    }
                } // End update venue to mini site
            }
        }

    }

    public static function save_organization( $post_id, $post ) {

        self::init( Apollo_DB_Schema::_APOLLO_EVENT_ORGANIZATION, Apollo_DB_Schema::_ORGANIZATION_PT );

        // Save org in event org table
        $apl_query  = new Apl_Query( Apollo_Tables::_APL_EVENT_ORG );

        $org_field  = Apollo_DB_Schema::_APL_E_ORG_ID;

        $apl_query->delete( " post_id = {$post_id}" );

        $orgDataID = array();
        for ( $i = 0; $i < 6; $i++ ):
            if ( ! isset( $_POST[self::$key.$i] ) ) continue;

            $org_id = $_POST[self::$key.$i];
            if ( ! $org_id || ! get_post_status( $org_id ) ) {
                $j = $i + 1;
                $apl_query->delete( " post_id = {$post_id} AND ".Apollo_DB_Schema::_APL_E_ORG_ORDERING." = {$j} " );
                continue;
            }

            $apl_query->insert( array(
                'post_id'  => $post_id,
                $org_field => $org_id,
                Apollo_DB_Schema::_APL_E_ORG_IS_MAIN => $i == 0 ? 'yes' : 'no',
                Apollo_DB_Schema::_APL_E_ORG_ORDERING => $i + 1,
            ));
            $orgDataID[] = $org_id;
        endfor;

        /**
         * Copy to mini site
         */
        self::_minisite_save_org($post_id, $orgDataID);


        // Save tmp org
        self::init( Apollo_DB_Schema::_APL_EVENT_TMP_ORG, Apollo_DB_Schema::_EVENT_PT );
        self::save_data( $post_id, $post );

    }

    /**
     * Handle ORGs to mini-site
     *
     * @param $post_id
     * @param $orgDataID
     */
    private static function _minisite_save_org($post_id, $orgDataID)
    {
        global $apollo_mini_submit_sites, $wpdb;

        if ($apollo_mini_submit_sites) {
            foreach($apollo_mini_submit_sites as $mini_site_id => $info) {
                $mini_post_id = isset($info["mini_post_id"]) ? $info["mini_post_id"] : null;
                if ($mini_post_id && $mini_site_id) {

                    // Prepare org data
                    $orgData = array();

                    foreach ($orgDataID as $key => $orgID) {

                        // Check has copied ORG
                        $copyCoOrgID = Apollo_Mini_Site_App::getOrgByParentOrgID($mini_site_id, $orgID);

                        $org = get_post($orgID, ARRAY_A);
                        $parent_gallery_ids = get_apollo_meta($orgID, Apollo_DB_Schema::_APL_ORG_IMAGE_GALLERY);
                        $parent_gallery_ids = $parent_gallery_ids ? explode(',', $parent_gallery_ids[0]) : '';
                        $orgData[] = [
                            'data' => $org,
                            'meta' => get_meta_data_org($orgID),
                            'copied_id' => $copyCoOrgID,
                            'parent_gallery_ids' => $parent_gallery_ids
                        ];

                    }

                    // Switch to mini site
                    $xrefData = array();
                    if (switch_to_blog($mini_site_id)) {

                        // Clear current data
                        $table = $wpdb->prefix . Apollo_Tables::_APL_EVENT_ORG;
                        $wpdb->get_results("DELETE FROM $table WHERE post_id = $mini_post_id");
                        if (!empty($orgData)) {
                            $insert_array = array();
                            foreach ($orgData as $key => $orgArr) {
                                $org = $orgArr['data'];
                                $mainOrgID = $org['ID'];
                                $metaValueOrgs = $orgArr['meta'];
                                $insertOrgID = $orgArr['copied_id'];

                                $isMain = !$key ? 'yes' : 'no';
                                $order = $key + 1;

                                if (!$insertOrgID) {
                                    //Check Org by name
                                    $insertOrgID = Apollo_Mini_Site_App::checkPostByName(Apollo_DB_Schema::_ORGANIZATION_PT, $org['post_title']);
                                    if (!$insertOrgID) {
                                        $org['ID'] = '';
                                        $org['post_status'] = 'publish';
                                        $insertOrgID = wp_insert_post($org);
                                    }
                                }

                                // copy org meta
                                Apollo_Mini_Site_App::copyMetaData($insertOrgID, $metaValueOrgs);

                                //update thumbnail, gallery
                                $insert_array[] = array(
                                    'mini_org_id' => $insertOrgID,
                                    'parent_org_id' => $mainOrgID,
                                    'parent_gallery_ids' => $orgArr['parent_gallery_ids']
                                );

                                // Add org to the apollo_event_org table
                                $table = $wpdb->prefix . "apollo_event_org";
                                $data = array('org_id' => $insertOrgID, 'org_ordering' => $order, 'is_main' => $isMain, 'post_id' => $mini_post_id);
                                copy_meta_data($table, $data);


                                // Update xref for the main ORG to avoid add new data at the next time
                                if (!$key) {
                                    $xrefData[] = array(
                                        'parent_post_id' => $post_id,
                                        'orgID' => $mainOrgID,
                                        'mini_orgID' => $insertOrgID,
                                        'mini_site_id' => $mini_site_id,
                                    );
                                } else {
                                    $xrefData[] = array(
                                        'mini_site_id' => $mini_site_id,
                                        'org_id' => $mainOrgID,
                                        'mini_org_id' => $insertOrgID
                                    );
                                }

                                Apollo_Mini_Site_App::insertIclTranslations($insertOrgID, 'organization');

                            }
                        }
                        restore_current_blog();
                    }

                    //update featured image
                    if (isset($insert_array) && $insert_array) {
                        $cat_type = Apollo_DB_Schema::_ORGANIZATION_PT . '-type';
                        foreach ($insert_array as $insert_org) {
                            //update thumbnail
                            Apollo_Mini_Site_App::updateAttachment($mini_site_id, $insert_org['parent_org_id'], $insert_org['mini_org_id']);
                            //update gallery
                            Apollo_Mini_Site_App::updateGalleryImagesToMiniSite($insert_org['mini_org_id'], $insert_org['parent_gallery_ids'], $mini_site_id, Apollo_DB_Schema::_APL_ORG_IMAGE_GALLERY);

                            //copy category
                            Apollo_Mini_Site_App::copyCategoryToMiniSite($insert_org['parent_org_id'], $mini_site_id, $insert_org['mini_org_id'], 0, $cat_type, '', '');
                        }
                    }

                    // update xref data
                    if ($xrefData) {

                        foreach ($xrefData as $key => $data) {

                            // Main org
                            if (!$key) {
                                update_xref_data($data);
                                continue;
                            }

                            // Co-presenter org
                            insert_mini_site_org($data);
                        }
                    }


                } // End copy to mini site


                // Save TMP ORG
                if ($mini_post_id && $mini_site_id) {
                    if (switch_to_blog($mini_site_id)) {
                        $key = Apollo_DB_Schema::_APL_EVENT_TMP_ORG;
                        $value = isset($_POST[$key]) ? stripslashes(sanitize_text_field($_POST[$key])) : '';
                        update_apollo_meta($mini_post_id, $key, $value);

                        restore_current_blog();
                    }
                } // End save TMP ORG
            }
        }
    }

    /*
     * @Ticket 14456
    */
    public static function save_artists($event_id){

        $list_artist_checked = isset($_POST['apl-event-checked-list-art']) ? $_POST['apl-event-checked-list-art'] : '';
        $list_artist_unchecked = isset($_POST['apl-event-unchecked-list-art']) ? $_POST['apl-event-unchecked-list-art'] : '';

        APL_Artist_Function::saveArtistEvent($event_id, $list_artist_checked, $list_artist_unchecked);
    }

    public static function output_organization( $post ) {
        self::init( Apollo_DB_Schema::_APOLLO_EVENT_ORGANIZATION, Apollo_DB_Schema::_ORGANIZATION_PT );

        $venueSelect2 = apl_instance('APL_Lib_Helpers_Select2', array(
            'post_type' => Apollo_DB_Schema::_ORGANIZATION_PT,
        ));

        if ($venueSelect2->getEnableRemote()) {
            self::output_org_remote_data_html( $post, $venueSelect2 );
        } else {
            self::output_org_html( $post );
        }

    }

    /*
     * @Ticket #14456
     */
    public static function output_artist( $post ) {
        if (Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ARTIST_PT)) {
            $artists = APL_Artist_Function::getListArtistForEvent(0, '', $post->ID);
            self::renderMultiSelectArtist($post->ID, $artists);
        }
    }

    private static function renderMultiSelectArtist($post_id, $artists){
        echo '<div id="artist-list" post_id="' . $post_id . '">';
        echo '<div class="group-search"><input id="search-artists-for-event" placeholder="'.__('Search by Artist name', 'apollo').'"/><span id="remove-search-text" class="remove-search hidden"><i class="fa fa-times"></i></span></div>';
        echo '<ul id="apollo-event-artist-list-in-frm">';
        echo(APL_Artist_Function::renderArtistsForEvent($artists['data']));
        echo '</ul>';
        echo '<input type="hidden" name="apl-event-checked-list-art" id="apl-checked-list-art" value=""/>';
        echo '<input type="hidden" name="apl-event-unchecked-list-art" id="apl-unchecked-list-art" value=""/>';
        echo '</div>';
        ?>
        <div class="wrap-box-load-more-artist <?php echo $artists['have_more'] ? '' : 'hidden' ?>">
        <div class="load-more b-btn wp-ct-event">
            <a href="javascript:void(0);"
               data-container="#apollo-event-artist-list-in-frm"
               data-ride="ap-more"
               data-holder="#apollo-event-artist-list-in-frm>:last"
               data-sourcetype="url"
               data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_get_more_artists&module=event&post_id=' . $post_id . '&offset='.count($artists['data']) ) ?>"
               data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
               data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
               class="btn-b arw ct-btn-sm-event"><?php _e('SHOW MORE', 'apollo') ?>
            </a>
        </div>
        </div>
        <?php
    }

    public static function output_quick_edit_select_option( $post_type, $label ) {

        self::$post_type = $post_type;

        $data = Apollo_App::get_list_post_type_items( self::$post_type );
        if ( ! $data ) {
            return;
        }
        ?>
        <label>
            <span class="title"><?php echo $label; ?></span>
            <span class="input-text-wrap">
                <select name="_apollo_event_<?php echo $post_type; ?>" >
                    <option><?php echo self::get_select_label(); ?></option>
                <?php foreach ( $data as $item ): ?>
                <option value="<?php echo $item->ID ?>" ><?php echo $item->post_title ?></option>
                <?php endforeach; ?>
                </select>
            </span>
        </label>
        <br class="clear" />
        <?php
    }

    /**
     * Get org ID for current org field in admin
     *
     * @param integer $current_loop
     * @param array $event_orgs
     *
     * @ticket #11353
     * @return integer
     */
    private static function getOrgIDForCurrentField($current_loop, $event_orgs)
    {
        /**
         * In table wp_apollo_event_org, we have meaning of "org_ordering" column as follow
         *
         * org_ordering = 1 => Register Organization
         * org_ordering = 2 => Co-Presenter 1
         * org_ordering = 3 => Co-Presenter 2
         * org_ordering = 4 => Co-Presenter 3
         * org_ordering = 5 => Co-Presenter 4
         * org_ordering = 6 => Co-Presenter 5
         *
         * @ticket #11353
         */
        $val = '';
        if ( ($total = count($event_orgs)) > 0 ) {
            for($j = 0; $j < $total; $j++) {
                if ( $current_loop == ($event_orgs[$j]->{Apollo_DB_Schema::_APL_E_ORG_ORDERING} - 1) ) {
                    $val = $event_orgs[$j]->{Apollo_DB_Schema::_APL_E_ORG_ID};
                    break;
                }
            }
        }

        return $val;
    }

    public static function output_org_remote_data_html($post, $Select2) {
        // Get event org
        $apl_query  = new Apl_Query(Apollo_Tables::_APL_EVENT_ORG);
        $columns    = sprintf('%s, %s', Apollo_DB_Schema::_APL_E_ORG_ID, Apollo_DB_Schema::_APL_E_ORG_ORDERING);
        $event_orgs = $apl_query->get_where(" post_id = {$post->ID}", $columns);

        echo '<div class="options_group event-box event-tmp-org">';

        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_EVENT_TMP_ORG,
            'label' => __( 'Temp Organization', 'apollo' ),
            'desc_tip' => 'true',
            'description' => '',
            'type' => 'text',
         //   'required' => ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ORGANIZATION_PT ) || ! $event_orgs
        ) );

         /*@ticket #18047 Adds the ability to register a temp org in the event form, as the venue section does*/ ?>
        <p>
            <input data-post="<?php echo $post->ID ?>" data-add-text="<?php _e( 'Add new', 'apollo' ) ?>"
                   data-adding-text="<?php _e( 'Adding', 'apollo' ) ?>"
                   id="apl-event-new-org" type="button" value="<?php _e( 'Add new', 'apollo' ) ?>"
                   class="button button-primary button-large" />
        </p> <?php

        echo '</div>';

        $attr = sprintf('data-enable-remote="%s" data-post-type="%s" data-source-url="%s"',
            $Select2->getEnableRemote(),
            Apollo_DB_Schema::_ORGANIZATION_PT,
            'apollo_get_remote_data_to_select2_box'
        );

        for( $i = 0; $i < 6; $i++ ):
            $label = '&nbsp;';
            if ( ! $i ) {
                $label  = __( 'Register Organization', 'apollo' );
            } else {
                $label  = __( 'Co-Presenter'. ' '. $i, 'apollo' );
            }

            $val = self::getOrgIDForCurrentField($i, $event_orgs);

            $options = $Select2->getItemsInPostTypeToDropDownList(array(
                'post_status' => array('publish','pending'),
                'selected_item' => $val
            ));
            echo '<div class="options_group event-box edit-item-wrap">';
            // Description
            apollo_wp_select(  array(
                'id'            => self::$key. $i,
                'name'          => self::$key. $i,
                'label'         => '',
                'desc_tip'      => 'true',
                'description'   => "",
                'label'         => $label,
                'options'       => $options,
                'value'         => $val,
                'attr'          => $attr,
                'class'         => 'apl_select2 edit-item-select',
              //  'required' => $i == 0 && $event_orgs && Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ORGANIZATION_PT ),
                'first_option'  => __('Select Organization', 'apollo'),
                'is_post_type_list' => true,
                'edit_item' => true
            ) );
            echo '</div>';

        endfor;

?>
<?php
    }

    public static function output_org_html( $post ) {

        // Get event org
        $apl_query  = new Apl_Query( Apollo_Tables::_APL_EVENT_ORG );
        $columns    = sprintf('%s, %s', Apollo_DB_Schema::_APL_E_ORG_ID, Apollo_DB_Schema::_APL_E_ORG_ORDERING);
        $event_orgs = $apl_query->get_where( " post_id = {$post->ID}", $columns );

        $options = self::_get_options( $post,'',array('publish','pending') );

         echo '<div class="options_group event-box ">';

            apollo_wp_text_input(  array(
                'id' => Apollo_DB_Schema::_APL_EVENT_TMP_ORG,
                'label' => __( 'Temp Organization', 'apollo' ),
                'desc_tip' => 'true',
                'description' => '',
                'type' => 'text',
             //   'required' => ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ORGANIZATION_PT ) || ! $event_orgs
            ) );

        echo '</div>';


        for( $i = 0; $i < 6; $i++ ):
            $label = '&nbsp;';
            if ( ! $i ) {
                $label  = __( 'Register Organization', 'apollo' );
            } else {
                $label  = __( 'Co-Presenter'. ' '. $i, 'apollo' );
            }

            $style =  ! $options || count( $options ) - 1 <= $i ? 'style="display: none;"' : '';
            echo '<div '. $style .' class="options_group event-box edit-item-wrap">';
                // Description
                apollo_wp_select(  array(
                    'id'            => self::$key. $i,
                    'name'          => self::$key. $i,
                    'label'         => '',
                    'desc_tip'      => 'true',
                    'description'   => "",
                    'label'         => $label,
                    'options'       => $options,
                    'value'         => self::getOrgIDForCurrentField($i, $event_orgs),
               //     'required' => $i == 0 && $event_orgs && Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ORGANIZATION_PT ),
                    'class' => 'apl_select2 edit-item-select',
                    'edit_item' => true,
                    'remote' => true
                ) );
            echo '</div>';

        endfor;

    }

    public static function output_venue( $_post ) {
        global $post;
        $post = $_post;
        self::init( Apollo_DB_Schema::_APOLLO_EVENT_VENUE, Apollo_DB_Schema::_VENUE_PT );
        $is_avaiable = Apollo_App::is_avaiable_module( Apollo_DB_Schema::_VENUE_PT );
        $event = get_event($post->ID);
        $venueId = $event->get_meta_data(self::$key);
        $venueSelect2 = apl_instance('APL_Lib_Helpers_Select2', array(
            'post_type' => Apollo_DB_Schema::_VENUE_PT,
            'selected_item' => $venueId,
        ));
        $options = $venueSelect2->getItemsInPostTypeToDropDownList();

        if ( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_VENUE_PT ) ) :

            $attr = sprintf('data-enable-remote="%s" data-post-type="%s" data-source-url="%s"',
                $venueSelect2->getEnableRemote(),
                Apollo_DB_Schema::_VENUE_PT,
                'apollo_get_remote_data_to_select2_box'
            );

            echo '<div class="options_group event-box edit-item-wrap">';
                // Description
                apollo_wp_select(  array(
                    'id'            => self::$key,
                    'name'          => self::$key,
                    'label'         => '',
                    'desc_tip'      => 'true',
                    'description'   => "",
                    'label'         => __( 'Register Venue', 'apollo' ),
                    'class'         => 'apl_select2 edit-item-select',
                    'options'       => $options,
                    'first_option'  => __('Select Venue', 'apollo'),
                    'is_post_type_list' => true,
                    'attr'  => $attr,
                    'value'   => $venueId,
                    'edit_item' => true,
                    'remote' => true
                ) );
            echo '</div>';
        endif;

        $event = get_event( $post );
        $venue = $event->get_meta_data( Apollo_DB_Schema::_APOLLO_EVENT_VENUE );
        $tmp_venue = unserialize( $event->get_meta_data( Apollo_DB_Schema::_APL_EVENT_TMP_VENUE ) );
        $venueState = isset($tmp_venue[Apollo_DB_Schema::_VENUE_STATE]) ? $tmp_venue[Apollo_DB_Schema::_VENUE_STATE] : '';
        $neighborhoodOptions = (isset($tmp_venue[Apollo_DB_Schema::_VENUE_CITY]) && isset($tmp_venue[Apollo_DB_Schema::_VENUE_STATE])) ?
            APL_Lib_Territory_Neighborhood::getNeighborhoodByCity($tmp_venue[Apollo_DB_Schema::_VENUE_CITY], $tmp_venue[Apollo_DB_Schema::_VENUE_STATE]) : array();
        if ( $is_avaiable ):

            echo '<div class="tmp-venue-wrapper" >';

            echo '<h4>'.__( 'Temporary Venue Address Information', 'apollo' ) .'</h4>';

            echo '<div class="options_group event-box">';
                // Description
                apollo_wp_text_input(  array(
                    'id' => ''. Apollo_DB_Schema::_APL_EVENT_TMP_VENUE.'['. Apollo_DB_Schema::_VENUE_NAME .']',
                    'label' => __( 'Venue name', 'apollo' ),
                    'desc_tip' => 'true',
                    'description' => "",
                    'required'  => ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_VENUE_PT ),
                ) );
            echo '</div>';

            echo '<div class="options_group event-box">';
                // Description
                apollo_wp_text_input(  array(
                    'id' => ''. Apollo_DB_Schema::_APL_EVENT_TMP_VENUE.'['. Apollo_DB_Schema::_VENUE_ADDRESS1 .']',
                    'label' => __( 'Address 1', 'apollo' ),
                    'desc_tip' => 'true',
                    'description' => ""
                ) );
            echo '</div>';

            echo '<div class="options_group event-box">';
                // Description
                apollo_wp_text_input(  array(
                    'id' => Apollo_DB_Schema::_APL_EVENT_TMP_VENUE. '['. Apollo_DB_Schema::_VENUE_ADDRESS2 .']',
                    'label' => __( 'Address 2', 'apollo' ),
                    'desc_tip' => 'true',
                    'description' => ""
                ) );
            echo '</div>';

            echo '<div class="options_group event-box">';
            // Description
            apollo_wp_select(  array(
                'id' => ''. Apollo_DB_Schema::_APL_EVENT_TMP_VENUE .'['. Apollo_DB_Schema::_VENUE_STATE .']',
                'label' => __( 'State', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'options'   => Apollo_App::getStateByTerritory(),
                'class' => 'apl-territory-state',
            ) );
            echo '</div>';

            echo '<div class="options_group event-box">';
            apollo_wp_select(  array(
                'id' => ''. Apollo_DB_Schema::_APL_EVENT_TMP_VENUE .'['. Apollo_DB_Schema::_VENUE_CITY .']',
                'label' => __( 'City', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'options'   => Apollo_App::getCityByTerritory(false, $venueState),
                'class' => 'apl-territory-city',
            ) );
            echo '</div>';

            echo '<div class="options_group event-box neighborhood-box '. (empty($neighborhoodOptions) ? 'hidden' : '') .'">';
            apollo_wp_select(  array(
                'id' => ''. Apollo_DB_Schema::_APL_EVENT_TMP_VENUE .'['. Apollo_DB_Schema::_VENUE_NEIGHBORHOOD .']',
                'label' => __( 'Neighborhood', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'class' => 'apl-territory-neighborhood ',
                'first_option' => __('Select Neighborhood', 'apollo'),
                'options'   => $neighborhoodOptions,
                'value' => isset($tmp_venue[Apollo_DB_Schema::_VENUE_NEIGHBORHOOD]) ? $tmp_venue[Apollo_DB_Schema::_VENUE_NEIGHBORHOOD] : 0,
            ) );
            echo '</div>';

            echo '<div class="options_group event-box">';
            // Description
            apollo_wp_select(  array(
                'id' => ''. Apollo_DB_Schema::_APL_EVENT_TMP_VENUE .'['. Apollo_DB_Schema::_VENUE_ZIP .']',
                'label' => __( 'Zip', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'options'   => Apollo_App::getZipByTerritory(),
                'class' => 'apl-territory-zipcode',
            ) );
            echo '</div>';

            echo '<div class="options_group event-box">';
            apollo_wp_select(  array(
                'id' => ''. Apollo_DB_Schema::_APL_EVENT_TMP_VENUE .'['. Apollo_DB_Schema::_VENUE_REGION .']',
                'label' => __( 'Region', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'options'   => Apollo_App::get_regions(),
                'display' => Apollo_App::showRegion() && !Apollo_App::enableMappingRegionZipSelection(),
            ) );
            echo '</div>';

            echo '<div class="options_group event-box">';
                // Description
                apollo_wp_text_input(  array(
                    'id' => Apollo_DB_Schema::_APL_EVENT_TMP_VENUE. '['. Apollo_DB_Schema::_VENUE_WEBSITE_URL .']',
                    'label' => __( 'URL', 'apollo' ),
                    'desc_tip' => 'true',
                    'description' => "",
                    'class' => 'apollo_input_url',
                ) );
            echo '</div>';

            /*@ticket #16898: [CF] 20180730 - [Event admin] Please add latitude/longitude data to the 'temp' venue - Item 4*/
            //latitude
            echo '<div class="options_group event-box">';
            apollo_wp_text_input(  array(
                'id' => Apollo_DB_Schema::_APL_EVENT_TMP_VENUE. '[' . Apollo_DB_Schema::_VENUE_LATITUDE . ']',
                'label' => __( 'Latitude', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
            ) );
            echo '</div>';

            //longitude
            echo '<div class="options_group event-box">';
            apollo_wp_text_input(  array(
                'id' => Apollo_DB_Schema::_APL_EVENT_TMP_VENUE. '[' . Apollo_DB_Schema::_VENUE_LONGITUDE . ']',
                'label' => __( 'Longitude', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
            ) );
            echo '</div>';

            echo '<div class="options_group event-box">';
            $browserKey = of_get_option(Apollo_DB_Schema::_GOOGLE_API_KEY_BROWSER);
            apollo_wp_google_map(array(
                'id' => 'apl-get-lat-long',
                'label' => __('Get latitude and longitude', 'apollo'),
                'attr' => array(
                    'data-address-id = "'.Apollo_DB_Schema::_APL_EVENT_TMP_VENUE .'['. Apollo_DB_Schema::_VENUE_ADDRESS1 .']'.'"',
                    'data-address2-id = "'.Apollo_DB_Schema::_APL_EVENT_TMP_VENUE .'['. Apollo_DB_Schema::_VENUE_ADDRESS2 .']'.'"',
                    'data-state-id = "'.Apollo_DB_Schema::_APL_EVENT_TMP_VENUE .'['. Apollo_DB_Schema::_VENUE_STATE .']'.'"',
                    'data-city-id = "'.Apollo_DB_Schema::_APL_EVENT_TMP_VENUE .'['. Apollo_DB_Schema::_VENUE_CITY .']'.'"',
                    'data-zip-id = "'.Apollo_DB_Schema::_APL_EVENT_TMP_VENUE .'['. Apollo_DB_Schema::_VENUE_ZIP .']'.'"',
                    'data-latitude-id = "'.Apollo_DB_Schema::_APL_EVENT_TMP_VENUE .'['. Apollo_DB_Schema::_VENUE_LATITUDE .']'.'"',
                    'data-longitude-id = "'.Apollo_DB_Schema::_APL_EVENT_TMP_VENUE .'['. Apollo_DB_Schema::_VENUE_LONGITUDE .']'.'"',
                    'data-google-api-key = "'. $browserKey .'"'
                )
            ));
            echo '</div>';

            ?>
                <p>
                    <label></label>
                    <input data-post="<?php echo $post->ID ?>" data-add-text="<?php _e( 'Add new', 'apollo' ) ?>" data-adding-text="<?php _e( 'Adding', 'apollo' ) ?>" id="apl-event-new-venue" type="button" value="<?php _e( 'Add new', 'apollo' ) ?>" class="button button-primary button-large" />
                </p>
            <?php
            echo '</div>';
        endif;

    }

}
