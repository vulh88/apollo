<?php
/**
 * Event Images
 *
 * Display the event images meta box.
 *
 * @author 		vulh
 * @category 	Admin
 * @package 	inc/admin/post-types/meta-boxes
 * 
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/**
 * Apollo_Meta_Box_Event_Images
 */
class Apollo_Meta_Box_Event_Images {

	/**
	 * Output the metabox
	 */
	public static function output( $post ) {
		?>
		<div id="event_images_container" data-title="<?php _e('Select images','apollo');?>" data-text="<?php _e('Use selected images','apollo');?>">
			<ul class="event_images">
				<?php
			
                    $event_image_gallery = get_apollo_meta( $post->ID, Apollo_DB_Schema::_APOLLO_EVENT_IMAGE_GALLERY, true );
					$attachments = array_filter( explode( ',', $event_image_gallery ) );

					if ( $attachments )
						foreach ( $attachments as $attachment_id ) {
							echo '<li class="image" data-attachment_id="' . esc_attr( $attachment_id ) . '">
								' . wp_get_attachment_image( $attachment_id, 'thumbnail' ) . '
								<ul class="actions">
									<li><a href="#" class="delete tips" data-tip="' . __( 'Delete image', 'apollo' ) . '">' . __( 'Delete', 'apollo' ) . '</a></li>
								</ul>
							</li>';
						}
				?>
			</ul>

			<input type="hidden" id="event_image_gallery" name="event_image_gallery" value="<?php echo esc_attr( $event_image_gallery ); ?>" />

		</div>
		<p class="add_event_images hide-if-no-js">
			<a href="#" data-choose="<?php _e( 'Add Images to Event Gallery', 'apollo' ); ?>" data-update="<?php _e( 'Add to gallery', 'apollo' ); ?>" data-delete="<?php _e( 'Delete image', 'apollo' ); ?>" data-text="<?php _e( 'Delete', 'apollo' ); ?>"><?php _e( 'Add event gallery images', 'apollo' ); ?></a>
		</p>
		<?php
	}

	/**
	 * Save meta box data
	 */
	public static function save( $post_id, $post ) {
		$attachment_ids = array_filter( explode( ',', sanitize_text_field( $_POST['event_image_gallery'] ) ) );
		update_apollo_meta( $post_id, Apollo_DB_Schema::_APOLLO_EVENT_IMAGE_GALLERY, implode( ',', $attachment_ids ) );

		/** Update gallery to mini site*/
        global $apollo_mini_submit_sites;

        if ($apollo_mini_submit_sites) {
            foreach($apollo_mini_submit_sites as $mini_site_id => $info) {
                $mini_post_id = isset($info["mini_post_id"]) ? $info["mini_post_id"] : null;
                if ($mini_post_id && $mini_site_id) {

                    Apollo_Mini_Site_App::updateGalleryImagesToMiniSite($mini_post_id, $attachment_ids, $mini_site_id, Apollo_DB_Schema::_APOLLO_EVENT_IMAGE_GALLERY);

                }
            }
        }
    }
}