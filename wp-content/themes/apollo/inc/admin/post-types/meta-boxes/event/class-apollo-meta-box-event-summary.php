<?php
/**
 * Event Summary
 *
 * Replaces the standard excerpt box.
 *
 * @author 		vulh
 * @category 	Admin
 * @package 	inc/admin/post-type/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Event_Summary
 */
class Apollo_Meta_Box_Event_Summary {

	/**
	 * Output the metabox
	 */
	public static function output( $post ) {
//		$settings = array(
//			'textarea_name'	=> 'excerpt',
//			'quicktags' 	=> array( 'buttons' => 'em,strong,link' ),
//			'tinymce' 	=> array(
//				'theme_advanced_buttons1' => 'bold,italic,strikethrough,separator,bullist,numlist,separator,blockquote,separator,justifyleft,justifycenter,justifyright,separator,link,unlink,separator,undo,redo,separator',
//				'theme_advanced_buttons2' => '',
//			),
//			'editor_css'	=> '<style>#wp-excerpt-editor-container .wp-editor-area{height:175px; width:100%;}</style>'
//		);
//
//		wp_editor( htmlspecialchars_decode( $post->post_excerpt ), 'excerpt', apply_filters( 'apollo_event_summary_editor_settings', $settings ) );
//                
                $maxNumSum = of_get_option(Apollo_DB_Schema::_APL_EVENT_CHARACTERS_SUM, 250);
                ?>  
                <textarea maxlength="<?php echo $maxNumSum ?>" 
                          class="full h-100" name="excerpt" placeholder=""><?php echo $post->post_excerpt ?></textarea>
                <div><?php echo sprintf(__( 'Event summary may not exceed %s characters, including spaces', 'apollo' ), $maxNumSum) ?></div>          
                <?php    
                
	}

}
