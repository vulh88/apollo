<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/8/2015
 * Time: 2:42 PM
 */
?>


<div id="button-update-calendar" class="option-group">
    <p class="submit">
        <input id="admin_event_save_date" 
               data-alert="<?php _e('Each date should have at least one start-time', 'apollo') ?>"
               type="button" class="button button-primary" value="<?php echo strtoupper(__('Edit dates','apollo')) ?>">
    </p>
</div>
<input type="hidden" id="list-msg-calendar"
       data-today ="<?php _e('TODAY','apollo') ?>"
       data-mon ="<?php _e('MON','apollo') ?>"
       data-tue ="<?php _e('TUE','apollo') ?>"
       data-wed ="<?php _e('WED','apollo') ?>"
       data-thu ="<?php _e('THU','apollo') ?>"
       data-fri ="<?php _e('FRI','apollo') ?>"
       data-sat ="<?php _e('SAT','apollo') ?>"
       data-sun ="<?php _e('SUN','apollo') ?>"
       data-am  ="<?php _e('AM','apollo') ?>"
       data-pm  ="<?php _e('PM','apollo') ?>"
       data-3more  ="<?php _e('[+3] More','apollo') ?>"
       data-edit="<?php _e('EDIT','apollo') ?>"
       data-del="<?php _e('DELETE','apollo') ?>"
       data-add-new="<?php _e('ADD NEW TIME','apollo') ?>"
       data-start-time  ="<?php _e('Start time','apollo') ?>"
       data-end-time  ="<?php _e('End time','apollo') ?>"
       data-missing-error  ="<?php _e('Something missing. Please try again!','apollo') ?>"
       data-unknow-error  ="<?php _e('An unknown error occurred.','apollo') ?>"
       data-remove-date = "<?php _e('Are you sure? This action will delete all times in this date.', 'apollo'); ?>"
       data-remove-date-events ="<?php _e('Are you sure? This action will delete all times of the event.','apollo') ?>"
       data-end-larger-start="<?php _e('End time must larger than start time!','apollo') ?>"
       data-invalid-time="<?php _e('Invalid time. Please choice another time','apollo') ?>"
       data-end-start-greater-today ="<?php _e(' The end date must be greater than the start date and one of them must be greater than or equal today','apollo') ?>"
       data-reset-msg="<?php _e('Editing dates will cause your times to be reset. Would you like to proceed ?','apollo') ?>"



    />