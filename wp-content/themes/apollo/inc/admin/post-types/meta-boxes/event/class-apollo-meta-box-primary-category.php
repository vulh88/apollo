<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of class-apollo-meta-box-primary-category
 *
 * @author vulh
 */
class Apollo_Meta_Box_Primary_Category {
    private static $_key = Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID;
    
    public static function output( $_post ) {
       
        global $post, $thepostid;
        $post = $_post;

        $type = Apollo_DB_Schema::_EVENT_PT. '-type';
        $terms = get_terms( $type, array( 'menu_order' => 'asc', 'orderby' => 'name', 'hide_empty' => false, 'parent' => false ) );
        $option = array();
        $option[] = __( 'Select primary category' );
        
        if ( $terms ) :
            foreach( $terms as $term ):
                $themeTool = new Apollo_Theme_Tool($term->term_id);
                if ($themeTool->isThemeTool()) continue;
                
                $option[$term->term_id] = $term->name;
            endforeach;
        endif;
        
        
        $terms_post = get_the_terms( $post->ID , $type );
        
        $primary_id = -1;
        if ( $terms_post ):
            $is_primary = get_apollo_meta( $post->ID, self::$_key, TRUE );
            foreach ( $terms_post as $term ):
                if ( $is_primary == $term->term_id ):
                    $primary_id = $term->term_id;
                    break;
                endif;
            endforeach;
        endif;
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_select(  array( 
                'id'            => self::$_key, 
                'label'         => '', 
                'desc_tip'      => 'true', 
                'description'   => "",
                'options'       => $option,
                'value'         => $primary_id,
                'class'         => 'apl-primary-category',
                'required'      => true,
            ) );
        echo '</div>';
        
        echo '<span style="display: none" class="error">'.__( 'Please select the primary category', 'apollo' ).'</span>';
    }
    
    public static function save( $post_id, $post ) {
        $term_id = isset( $_REQUEST[self::$_key] ) ? $_REQUEST[self::$_key] : false;
       
        $terms = wp_get_post_terms( $post_id, Apollo_DB_Schema::_EVENT_PT. '-type' );
        $_arr_term = array();
        $_arr_term[] = $term_id;
        
        if ( $terms ):
            foreach ( $terms as $t ):
                $_arr_term[] = $t->term_id;
            endforeach;
        endif;
        
        wp_set_post_terms( $post_id, $_arr_term , Apollo_DB_Schema::_EVENT_PT. '-type' );
        update_apollo_meta( $post_id, self::$_key, $term_id );

        // Copy primary category ID meta data to mini site
//        self::_minisiteCopyTermsAndPrimaryCat($post_id, $term_id);
        global $apollo_mini_submit_sites;

        if ($apollo_mini_submit_sites) {
            foreach($apollo_mini_submit_sites as $mini_site_id => $info) {
                $mini_post_id = isset($info["mini_post_id"]) ? $info["mini_post_id"] : null;
                $mini_site_config_post_id = isset($info["mini_site_config_post_id"]) ? $info["mini_site_config_post_id"] : null;
                if ($mini_post_id && $mini_site_id && $mini_site_config_post_id) {
                    Apollo_Mini_Site_App::copyCategoryToMiniSite($post_id, $mini_site_id, $mini_post_id, $mini_site_config_post_id, 'event-type', 'meta-catsel', 'meta-type', $term_id);
                }
            }
        }
    }

    /**
     * Copy primary category ID to meta data of the mini site
     * @param $parentPostId
     * @param $parentPrimaryCatID
     */
    private static function _minisiteCopyTermsAndPrimaryCat($parentPostId, $parentPrimaryCatID)
    {

        $parentTerms = wp_get_post_terms($parentPostId, 'event-type');

        // Copy primary ID to mini-site
        global $apollo_mini_submit_sites, $wpdb;

        foreach($apollo_mini_submit_sites as $mini_site_id => $info) {
            $mini_post_id = isset($info["mini_post_id"]) ? $info["mini_post_id"] : null;
            $mini_site_config_post_id = isset($info["mini_site_config_post_id"]) ? $info["mini_site_config_post_id"] : null;
            if ($mini_post_id && $mini_site_id) {

                // get setting
                $isAllCat = get_post_meta($mini_site_config_post_id, 'meta-catsel');

                if ($isAllCat && $isAllCat[0] == 'on') {
                    $selectedCats = get_post_meta($mini_site_config_post_id, 'meta-type');
                    $selectedCats = $selectedCats && $selectedCats[0] ? $selectedCats[0] : array();
                }

                // Parent Term metas
                $parentTermMetas = array();
                if ($parentTerms) {
                    foreach ($parentTerms as $parentTerm) {
                        // Get all term meta
                        $table = $wpdb->prefix . Apollo_Tables::_APOLLO_TERM_META;
                        $result = $wpdb->get_results($wpdb->prepare("select * from $table where apollo_term_id = %s", $parentTerm->term_id));
                        $parentTermMetas[$parentTerm->term_id] = $result;
                    }
                }


                if (switch_to_blog($mini_site_id)) {

                    $childTerms = array();
                    $childPrimaryTermID = '';
                    if ($parentTerms) {
                        foreach ($parentTerms as $parentTerm) {

                            if (isset($selectedCats) && $selectedCats && !in_array($parentTerm->term_id, $selectedCats)) {
                                continue;
                            }

                            // Get child term by name
                            $term = get_term_by('name', $parentTerm->name, 'event-type');

                            $termID = $term ? $term->term_id : 0;

                            if (!$termID) {
                                // Add new category
                                $newTerm = array(
                                    'ID' => '',
                                    'cat_name' => $parentTerm->name,
                                    'category_description' => $parentTerm->description,
                                    'taxonomy' => 'event-type',
                                );

                                $termID = wp_insert_category($newTerm);
                            }

                            // Update term meta
                            if (isset($parentTermMetas[$parentTerm->term_id]) && $parentTermMetas[$parentTerm->term_id]) {
                                foreach ($parentTermMetas[$parentTerm->term_id] as $mt) {
                                    update_apollo_term_meta($termID, $mt->meta_key, $mt->meta_value);
                                }
                            }

                            $childTerms[] = $termID;

                            // Get child primary term ID
                            if ($parentPrimaryCatID == $parentTerm->term_id) {
                                $childPrimaryTermID = $termID;
                            }
                        }

                        update_apollo_meta($mini_post_id, Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID, $childPrimaryTermID);
                    }

                    if ($childTerms) {
                        wp_set_post_terms($mini_post_id, $childTerms, 'event-type');
                    }
                }
                restore_current_blog();
            }
        }



    }
}
