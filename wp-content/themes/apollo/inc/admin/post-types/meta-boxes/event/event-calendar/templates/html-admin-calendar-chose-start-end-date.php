<?php
echo '<input id="hidden-postID" type ="hidden" value="'.$thepostid.'">';
echo '<div class="options_group">';
if ( ((strtotime($config['end_date']) - strtotime($config['start_date'])) / (24 * 3600 * 30)) >= Apollo_App::configDateRange() ) {
    echo '<p class="ongoing">'.__('Ongoing', 'apollo').'</p>';
}

apollo_wp_text_input(  array(
    'id' => Apollo_DB_Schema::_APOLLO_EVENT_START_DATE,
    'label' => __( 'Start date', 'apollo' ),
    'desc_tip' => 'true',
    'description' => '',
    'type' => 'text',
    'required' => true,
    'data_parent'   => '.datetime_options',
    'value' => $config['start_date']
) );
echo '</div>';
?>
<?php
echo '<div class="options_group">';
apollo_wp_text_input(  array(
    'id' => Apollo_DB_Schema::_APOLLO_EVENT_END_DATE,
    'label' => __( 'End date', 'apollo' ),
    'desc_tip' => 'true',
    'description' => '',
    'type' => 'text',
    'required' => true,
    'data_compare'  => Apollo_DB_Schema::_APOLLO_EVENT_START_DATE,
    //'class' => 'apollo-date-greater short',
    'class' => 'short',
    'data_parent'   => '.datetime_options',
    'value' => $config['end_date'],
    'data_elm'  => sprintf('data-today="%s"', current_time('Y-m-d')),
) );
echo '</div>';
echo '<input type = "hidden" id="current_sd" value="'.$config['start_date'].'" >';
echo '<input type = "hidden" id="current_ed" value="'.$config['end_date'].'" >';
?>