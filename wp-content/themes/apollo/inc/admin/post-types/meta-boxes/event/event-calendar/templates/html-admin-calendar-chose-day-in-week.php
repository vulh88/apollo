<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/8/2015
 * Time: 2:42 PM
 */
$class = of_get_option(Apollo_DB_Schema::DISABLE_REQUIREMENT_EVENT_DATE_TIME) ? 'disable-requirement' : '';
?>
<div class="option_group selected_day <?php  echo $class;?>">
    <p class="form-field _apollo_event_days_of_week_field ">
        <label class="calendar-day-in-week-label" for=""><?php echo __('Days of the week(*)','apollo') ?></label>
        <div class="calendar-day-in-week-item-group">
        <?php

            foreach($arrayDayOfWeek as  $k => $day){
                $checked = '';
                if(is_array($arrayDayOfWeekSelected)){
                    if(in_array($k,$arrayDayOfWeekSelected)){
                        $checked = 'checked';
                    }
                }
                echo '<div class="calendar-day-group">'
                ?>
                <input class="cmb-day-in-week" <?php echo $checked; ?> type="checkbox" name="<?php echo Apollo_DB_Schema::_APOLLO_EVENT_DATA.'['. Apollo_DB_Schema::_APOLLO_EVENT_DAYS_OF_WEEK .'][]'; ?>" value="<?php echo $k; ?>">
            <?php
                echo '<span class="cmb-day-in-week-span">'.$day.'</span>';
                echo '</div>';
            }
        ?>
        </div>
    </p>
</div>