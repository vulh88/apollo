<?php
/**
 * Event Images
 *
 * Display the event images meta box.
 *
 * @author 		vulh
 * @category 	Admin
 * @package 	inc/admin/post-types/meta-boxes
 * 
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/**
 * Apollo_Meta_Box_Home_Spotlight_Event
 */
class Apollo_Meta_Box_Home_Spotlight_Event_Image {

	/**
	 * Output the metabox
	 */
	public static function output( $post ) {
        $attachment_id = esc_attr( get_apollo_meta( $post->ID, Apollo_DB_Schema::_APOLLO_EVENT_HOME_SPOTLIGHT_IMAGE, true ) );
		?>
        <input type="hidden" id="<?php echo Apollo_DB_Schema::_APOLLO_EVENT_HOME_SPOTLIGHT_IMAGE ?>" name="<?php echo Apollo_DB_Schema::_APOLLO_EVENT_HOME_SPOTLIGHT_IMAGE ?>"
               value="<?php echo $attachment_id; ?>" />

		<p class="hide-if-no-js">
            <p class="image-container"><?php echo wp_get_attachment_image( $attachment_id, 'thumbnail' )  ?></p>
			<a href="#" data-choose="<?php _e( 'Set Home spotlight image', 'apollo' ); ?>" 
               data-update="<?php _e( 'Add Home Spotlight Image', 'apollo' ); ?>" 
               data-delete="<?php _e( 'Delete image', 'apollo' ); ?>"
               data-text="<?php _e( 'Delete', 'apollo' ); ?>"><?php _e( 'Select Home spotlight image', 'apollo' ); ?></a>
		</p>
		<?php
	}

	/**
	 * Save meta box data
	 */
	public static function save( $post_id, $post ) {
        
        if ( isset( $_REQUEST[Apollo_DB_Schema::_APOLLO_EVENT_HOME_SPOTLIGHT_IMAGE] ) ) {
            update_apollo_meta( $post_id, Apollo_DB_Schema::_APOLLO_EVENT_HOME_SPOTLIGHT_IMAGE, $_REQUEST[Apollo_DB_Schema::_APOLLO_EVENT_HOME_SPOTLIGHT_IMAGE] );
        }
	}
}