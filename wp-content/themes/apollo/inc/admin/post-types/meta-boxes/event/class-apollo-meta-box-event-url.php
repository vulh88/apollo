<?php
/**
 * Event URL
 *
 * Displays the event URL
 *
 * @author 		vulh
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Event_URL
 */
class Apollo_Meta_Box_Event_URL {

	/**
	 * Output the metabox
	 */
	public static function output( $_post ) {
        
        global $post, $wpdb, $thepostid;
		$post = $_post;
		wp_nonce_field( 'apollo_event_meta_nonce', 'apollo_event_meta_nonce' );
        $thepostid = $post->ID;
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => Apollo_DB_Schema::_APOLLO_EVENT_DATA. '['. Apollo_DB_Schema::_WEBSITE_URL .']', 
                'label' => __( 'Official Website', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'class' => 'apollo_input_url',
            ) );
        echo '</div>';  
        
	}

	/**
	 * Save meta box data
	 */
	public static function save( $post_id, $post ) {
        
        // Save it in event data
	}

}
