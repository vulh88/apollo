<?php
/**
 * Created by PhpStorm.
 * User: TriSatria
 * Date: 5/8/2015
 * Time: 10:36 AM
 */
?>
<div class="calendar-container sevenday left full-width">
    <calendar data-dws="<?php echo $daysInWeek ?>" selected="day" data-type="2" data-eid = "<?php echo $postId ?>" data-sd="<?php echo $startDay ?>" data-ed = "<?php echo $endDay ?>"></calendar>
</div>