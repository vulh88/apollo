<?php

/**
 * Apollo business Meta Boxes
 *
 * @author 		Elisoft
 * @category 	Admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

include 'class-apl-ad-module-meta-boxes.php';

/**
 * Apollo_Admin_Business_Meta_Boxes
 */
class Apollo_Admin_Business_Meta_Boxes extends Apl_Ad_Module_Meta_Boxes {

    /**
     * Constructor
     */
    public function __construct() {

        parent::__construct( Apollo_DB_Schema::_BUSINESS_PT);

        $this->require_meta_boxes();

        add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 30 );

        add_action( 'save_post', array( $this, 'save_meta_boxes' ), 1, 2 );

        // Save Meta Boxes
        add_action( 'pre_post_update', array($this, 'save_meta_box_custom_field'), 10);
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Business_Data::save', 10, 2 );

        // Error handling (for showing errors from meta boxes on next page load)
        add_action( 'admin_notices', array( $this, 'output_errors' ) );
        add_action( 'shutdown', array( $this, 'save_errors' ) );


        /**
         * Hide the "Move to Trash" link, Set 'status', Set 'Visibility', 'add new' button
         * Disable this feature from @Ticket #14399
        */
        //add_action('admin_head', array( $this, 'hide_publishing_actions' ));

        add_action( 'pending_to_publish', 'Apollo_Meta_Box_Business_Data::enableBusinessCheckbox', 10, 1 );

        // Only copy to mini site when not switched to the child site yet
        global $switched;

        add_action('apollo_copy_to_mini_sites', array( $this, 'apollo_copy_to_mini_sites' ), 10, 1);
    }

    public static function apollo_copy_to_mini_sites($post_id)
    {
        global $apollo_mini_submit_sites;

        if ($apollo_mini_submit_sites) {
            $businessCopyMiniSite = new Apollo_Business_Copy_To_Mini_Site($post_id);
            foreach($apollo_mini_submit_sites as $siteId => $miniConfigPostId){
                $site_item = $businessCopyMiniSite->getMiniPostByParentPost($post_id, $siteId);
                $site_item = count($site_item) ? $site_item[0] : [];

                if($site_item->mini_site_id && $site_item->mini_post_id && $site_item->mini_options_id){

                    $metaValuesBusiness = $businessCopyMiniSite->getMetaData($post_id);

                    //don't update _apl_business_org
                    foreach($metaValuesBusiness as $index => $value ){
                        if (!strcmp($value->meta_key, Apollo_DB_Schema::_APL_BUSINESS_ORG)){
                            unset($metaValuesBusiness[$index]);
                        }
                    }

                    $metaValuesBusiness = array_values($metaValuesBusiness);

                    if(switch_to_blog($site_item->mini_site_id)){
                        Apollo_Mini_Site_App::insertIclTranslations($site_item->mini_post_id, 'business');
                        Apollo_Mini_Site_App::copyMetaData($site_item->mini_post_id, $metaValuesBusiness);
                        restore_current_blog();
                    }

                    Apollo_Mini_Site_App::copyCategoryToMiniSite($post_id, $site_item->mini_site_id, $site_item->mini_post_id, $site_item->mini_options_id , 'business-type', 'business-meta-catsel', 'business-meta-type');
                }
            }

        }
    }

    public function save_meta_box_custom_field($post_id){
        global $post;
        // verify if this is an auto save routine.
        // If it is our form has not been submitted, so we dont want to do anything
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){
            return;
        }
        // Thienld : save business features meta data to individual meta-key in order to serve for searching
        $aplQuery = new Apl_Query(Apollo_Tables::_APOLLO_BUSINESS_META);
        $metaBSIDKey = "apollo_" . Apollo_DB_Schema::_BUSINESS_PT . "_id";
        $aplQuery->delete(" ".$metaBSIDKey. " = '" .$post_id . "' AND meta_key LIKE '" . Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY . '_%\'');
    }

    public function require_meta_boxes() {
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/business/class-apollo-meta-box-business-data.php';

    }

    /**
     * Add Apollo Meta boxes
     */
    public function add_meta_boxes() {

        add_meta_box( 'apollo-business-customfield', __( 'More Business Info', 'apollo' ),
            'Apollo_Meta_Box_Customfield::output', $this->type, 'normal', 'high' );

        add_meta_box( 'apollo-business-information', __( 'More Info', 'apollo' ),
            'Apollo_Meta_Box_Business_Data::output_location_info', $this->type, 'normal', 'high' );

        if ( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ORGANIZATION_PT ) ) {

            add_meta_box('apollo-address-org-data', __('Associated Organization  Information', 'apollo'),
                'Apollo_Meta_Box_Business_Data::outputOrgData', $this->type, 'normal', 'high');


            add_meta_box('apollo-business-org', __('Associated Organization', 'apollo'),
                'Apollo_Meta_Box_Business_Data::output_org', $this->type, 'side', 'high');
        }




    }
}

new Apollo_Admin_Business_Meta_Boxes();
