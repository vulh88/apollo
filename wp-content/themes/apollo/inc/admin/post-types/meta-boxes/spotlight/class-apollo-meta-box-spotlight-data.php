<?php
/**
 * Spotlight
 *
 * @author 		elinext
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Spitlight_Data
 */

class Apollo_Meta_Box_Spitlight_Data {

	/**
	 * Output the metabox
	 */
	public static function output( $post ) { 
        
        global $post, $thepostid;
		wp_nonce_field( 'apollo_spotlight_meta_nonce', 'apollo_spotlight_meta_nonce' );
        $thepostid = $post->ID;
        $data = get_post_meta( $thepostid, Apollo_DB_Schema::_APL_SPOT_LINK );
        $value = $data && $data[0] ? $data[0] : '';

        $arr_open_style = get_post_meta( $thepostid, Apollo_DB_Schema::_APL_SPOT_LINK_OPEN_STYLE);
        $val_open_style = $arr_open_style && $arr_open_style[0] ? $arr_open_style[0] : '';

        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => Apollo_DB_Schema::_APL_SPOT_LINK, 
                'label' => '', 
                'desc_tip' => 'true', 
                'description' => "",
                'class' => 'apollo_input_url',
                'value' => $value,
            ) );
        echo '</div>';

        echo '<div class="options_group event-box italic">';
        // Description
        echo '<input type="checkbox" '.checked($val_open_style, true, false).' name="'. Apollo_DB_Schema::_APL_SPOT_LINK_OPEN_STYLE .'" />' . "Is open new page ?";
        echo '</div>';

    }

    /**
     * @Ticket #13274
     * @param $post
     */
    public static function date_range_output( $post ) {

        global $post, $thepostid;
        wp_nonce_field( 'apollo_spotlight_meta_nonce', 'apollo_spotlight_meta_nonce' );
        $thepostid = $post->ID;
        $start_date = get_post_meta( $thepostid, Apollo_DB_Schema::_APL_SPOT_LIGHT_START_DATE );
        $start_date = ($start_date && $start_date[0]) ? $start_date[0] : '';
        $exp_date = get_post_meta( $thepostid, Apollo_DB_Schema::_APL_SPOT_LIGHT_END_DATE );
        $exp_date = ($exp_date && $exp_date[0]) ? $exp_date[0] : '';

        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_SPOT_LIGHT_START_DATE,
            'name' => Apollo_DB_Schema::_APL_SPOT_LIGHT_START_DATE,
            'label' => __( 'Start date', 'apollo' ),
            'desc_tip' => 'true',
            'description' => '',
            'type' => 'text',
            'value' => $start_date,
            'data_parent'   => '.datetime_options',
            'class' => 'apollo_input_datepicker short start-date',
        ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_SPOT_LIGHT_END_DATE,
            'name' => Apollo_DB_Schema::_APL_SPOT_LIGHT_END_DATE,
            'label' => __( 'End date', 'apollo' ),
            'desc_tip' => 'true',
            'description' => '',
            'type' => 'text',
            'value' => $exp_date,
            'data_compare'  => Apollo_DB_Schema::_APL_SPOT_LIGHT_START_DATE,
            'data_parent'   => '.datetime_options',
            'class' => 'apollo_input_datepicker short end-date apollo-date-greater-than-now',
        ) );
        echo '</div>';
    }

    public static  function images_box($post) {
        ?>
        <div id="event_images_container" data-title="<?php _e('Select images','apollo');?>" data-text="<?php _e('Use selected images','apollo');?>">
            <ul class="event_images">
                <?php

                $spot_image_gallery = get_post_meta( $post->ID, Apollo_DB_Schema::_APL_SPOT_LIGHT_GALLERY, true );
                $attachments = array_filter( explode( ',', $spot_image_gallery ) );

                if ( $attachments )
                    foreach ( $attachments as $attachment_id ) {
                        echo '<li class="image" data-attachment_id="' . esc_attr( $attachment_id ) . '">
								' . wp_get_attachment_image( $attachment_id, 'thumbnail' ) . '
								<ul class="actions">
									<li><a href="#" class="delete tips" data-tip="' . __( 'Delete image', 'apollo' ) . '">' . __( 'Delete', 'apollo' ) . '</a></li>
								</ul>
							</li>';
                    }
                ?>
            </ul>

            <input type="hidden" id="event_image_gallery" name="spot_image_gallery" value="<?php echo esc_attr( $spot_image_gallery ); ?>" />

        </div>
        <p class="add_spotlight_images hide-if-no-js">
            <a href="#" data-choose="<?php _e( 'Add Images to Spot light Gallery', 'apollo' ); ?>" data-update="<?php _e( 'Add to gallery', 'apollo' ); ?>" data-delete="<?php _e( 'Delete image', 'apollo' ); ?>" data-text="<?php _e( 'Delete', 'apollo' ); ?>"><?php _e( 'Add Spot light gallery images', 'apollo' ); ?></a>
        </p>
        <?php
    }

    public static function color_fields($post){
        global $post, $thepostid, $wpdb;
        $thepostid = $post->ID;
        $spot_color_left_arrow= get_post_meta( $post->ID, Apollo_DB_Schema::_APL_SPOT_LIGHT_COLOR_LEFT_ARROW , true);
        echo '<div class="options_group event-box clear">';
        apollo_wp_color_field(  array(
            'id' => Apollo_DB_Schema::_APL_SPOT_LIGHT_COLOR_LEFT_ARROW,
            'label' => __( 'Background-color left arrow', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => '',
            'std' => '#666666',
            'value' =>$spot_color_left_arrow,
        ) );
        echo '</div>';

        $spot_color_right_arrow= get_post_meta( $post->ID, Apollo_DB_Schema::_APL_SPOT_LIGHT_COLOR_RIGHT_ARROW, true );

        echo '<div class="options_group event-box clear">';
        apollo_wp_color_field(  array(
            'id' => Apollo_DB_Schema::_APL_SPOT_LIGHT_COLOR_RIGHT_ARROW,
            'label' => __( 'Background-color Right arrow', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => '',
            'std' => '#666666',
            'value' =>$spot_color_right_arrow,
        ) );
        echo '</div>';

    }

	/**
	 * Save meta box data
	 */
	public static function save( $post_id, $post ) {
        $val = $_POST[Apollo_DB_Schema::_APL_SPOT_LINK];;
        if ( filter_var( $val, FILTER_VALIDATE_URL ) ) {
            $val = $_POST[Apollo_DB_Schema::_APL_SPOT_LINK];
        } else {
            $val = '';
        }
        update_post_meta( $post_id, Apollo_DB_Schema::_APL_SPOT_LINK, Apollo_App::clean_data_request( $val ) );

        /** @Ticket #13274 */
        $val_start_date = isset($_POST[Apollo_DB_Schema::_APL_SPOT_LIGHT_START_DATE]) ? $_POST[Apollo_DB_Schema::_APL_SPOT_LIGHT_START_DATE] : '';
        $val_end_date = isset($_POST[Apollo_DB_Schema::_APL_SPOT_LIGHT_END_DATE]) ? $_POST[Apollo_DB_Schema::_APL_SPOT_LIGHT_END_DATE] : '';
        update_post_meta( $post_id, Apollo_DB_Schema::_APL_SPOT_LIGHT_START_DATE, Apollo_App::clean_data_request( $val_start_date ) );
        update_post_meta( $post_id, Apollo_DB_Schema::_APL_SPOT_LIGHT_END_DATE, Apollo_App::clean_data_request( $val_end_date ) );

        $currentTheme = function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '';

        if ( $currentTheme == APL_SM_DIR_THEME_NAME) {

            if (isset($_POST[Apollo_DB_Schema::_APL_SPOT_LIGHT_COLOR_LEFT_ARROW]) && !empty($_POST[Apollo_DB_Schema::_APL_SPOT_LIGHT_COLOR_LEFT_ARROW])) {
                update_post_meta($post_id, Apollo_DB_Schema::_APL_SPOT_LIGHT_COLOR_LEFT_ARROW, Apollo_App::clean_data_request($_POST[Apollo_DB_Schema::_APL_SPOT_LIGHT_COLOR_LEFT_ARROW]));
            }
            if (isset($_POST[Apollo_DB_Schema::_APL_SPOT_LIGHT_COLOR_RIGHT_ARROW]) && !empty($_POST[Apollo_DB_Schema::_APL_SPOT_LIGHT_COLOR_RIGHT_ARROW])) {
                update_post_meta($post_id, Apollo_DB_Schema::_APL_SPOT_LIGHT_COLOR_RIGHT_ARROW, Apollo_App::clean_data_request($_POST[Apollo_DB_Schema::_APL_SPOT_LIGHT_COLOR_RIGHT_ARROW]));

            }
            $attachment_ids = array_filter(explode(',', sanitize_text_field($_POST['spot_image_gallery'])));
            update_post_meta($post_id, Apollo_DB_Schema::_APL_SPOT_LIGHT_GALLERY, implode(',', $attachment_ids));
        }

        $val_open_style = isset($_POST[Apollo_DB_Schema::_APL_SPOT_LINK_OPEN_STYLE]) ?  $_POST[Apollo_DB_Schema::_APL_SPOT_LINK_OPEN_STYLE] : false;
        $val_open_style = !!$val_open_style;

        update_post_meta( $post_id, Apollo_DB_Schema::_APL_SPOT_LINK_OPEN_STYLE, Apollo_App::clean_data_request( $val_open_style ) );

        /*@ticket #17212 */
        $displayTitle =  isset($_POST[Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_TITLE]) ? $_POST[Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_TITLE] : 'off';
        update_post_meta($post_id,Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_TITLE ,$displayTitle);

        /*@ticket 17249 Octave Theme - [Spotlight] Add new button to the homepage spotlight - item 2*/
        if($currentTheme === "octave-child"){
            $displaySemiTransparent =  isset($_POST[Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_SEMI_TRANSPARENT]) ? $_POST[Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_SEMI_TRANSPARENT] : 'off';
            update_post_meta($post_id,Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_SEMI_TRANSPARENT ,$displaySemiTransparent);

            $displayActionButton =  isset($_POST[Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_ACTION_BUTTON]) ? $_POST[Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_ACTION_BUTTON] : '';
            update_post_meta($post_id,Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_ACTION_BUTTON ,$displayActionButton);

            $displaySetting =  isset($_POST[Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_SETTING]) ? $_POST[Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_SETTING] : '';
            update_post_meta($post_id,Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_SETTING ,$displaySetting);
        }
    }

    /**
     * @ticket #17212 Octave Theme - [CF] 20180821 - Masthead background color & separating spotlight title options
     * @param $post
     */
    public static function spotlight_display_options($post){
        $displayTitle = get_post_meta($post->ID,Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_TITLE,true);
        $titleValue = ($displayTitle === 'on' || !$displayTitle) ? 'checked' : '';

        ?>
        <p><input type="checkbox" name="<?php echo Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_TITLE ?>" <?php echo $titleValue?>/>
            <label><?php echo __('Display Title')?></label></p>
        <?php

        /*@ticket 17249 Octave Theme - [Spotlight] Add new button to the homepage spotlight - item 2*/
        $currentTheme = function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '';

        if($currentTheme === "octave-child"){
            $displaySemiTransparent = get_post_meta($post->ID,Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_SEMI_TRANSPARENT,true);
            $semiValue= ($displaySemiTransparent === 'on' || !$displaySemiTransparent) ? 'checked' : '';;

            $displayActionButton = get_post_meta($post->ID,Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_ACTION_BUTTON,true);
            $actionValue = ($displayActionButton === 'on') ? 'checked' : '';

            ?>
            <p><input type="checkbox" name="<?php echo Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_SEMI_TRANSPARENT ?>" <?php echo $semiValue?>/>
                <label><?php echo __('Display Semi-transparent')?></label></p>

            <p><input type="checkbox" name="<?php echo Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_ACTION_BUTTON ?>" <?php echo $actionValue?>/>
                <label><?php echo __('Display Action button')?></label></p>
            <?php
        }
    }

    /**
     *  @ticket 17249 Octave Theme - [Spotlight] Add new button to the homepage spotlight - item 2
     * @param $post
     */
    public static function spotlight_display_setting($post){

        $displaySetting = get_post_meta($post->ID,Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_SETTING,true);

        if (!$displaySetting) {
            $displaySetting = array();
        }

        $displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_TEXT] = isset($displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_TEXT]) ? $displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_TEXT] : __('Call to action', 'apollo');
        $displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_TRANSPARENT_LEVEL] = isset($displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_TRANSPARENT_LEVEL]) ? $displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_TRANSPARENT_LEVEL] : 75;
        $displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_BACKGROUND] = isset($displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_BACKGROUND]) ? $displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_BACKGROUND] : '#429ad4';
        $displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_WIDTH] = isset($displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_WIDTH]) ? $displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_WIDTH] : '200';
        $displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_HEIGHT] = isset($displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_HEIGHT]) ? $displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_HEIGHT] : '50';

        ?>
        <div class="spotlight-display-setting">
        <p>
            <label><?php echo __('Text')?></label>
            <input type="text"
                  name="<?php echo Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_SETTING . '[' . Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_TEXT . ']'?>"
                  value ="<?php echo $displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_TEXT] ?>"/>
        </p>

        <p>
            <label><?php echo __('Transparent level')?></label>
            <input type="number"
                  name="<?php echo Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_SETTING . '[' . Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_TRANSPARENT_LEVEL . ']'?>"
                  value ="<?php echo $displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_TRANSPARENT_LEVEL]?>"
                   min="1" max="100"/>
        </p>

        <p>
            <label><?php echo __('Background color')?></label>
            <input type="text" class="spotlight-display-setting-background"
                  name="<?php echo Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_SETTING . '[' . Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_BACKGROUND . ']'?>"
                  value ="<?php echo $displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_BACKGROUND]?>"/>
        </p>

        <p>
            <label><?php echo __('Width')?></label>
            <input type="text" class="apollo_input_number "
                  name="<?php echo Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_SETTING . '[' . Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_WIDTH . ']'?>"
                  value ="<?php echo  $displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_WIDTH]?>"/>
        </p>

        <p>
            <label><?php echo __('Height')?></label>
            <input type="text" class="apollo_input_number "
                  name="<?php echo Apollo_DB_Schema::_APL_SPOT_LIGHT_DISPLAY_SETTING . '[' . Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_HEIGHT . ']'?>"
                  value ="<?php echo $displaySetting[Apollo_DB_Schema::_APL_SPOT_LIGHT_SETTING_HEIGHT]?>"/>
        </p>
        </div>
        <?php
    }
}