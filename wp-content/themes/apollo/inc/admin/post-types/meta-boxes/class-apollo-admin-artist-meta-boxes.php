<?php
/**
 * Apollo Artist Meta Boxes
 *
 * Sets up the write panels used by venues (custom post types)
 *
 * @author 		vulh
 * @category 	Admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

include 'class-apl-ad-module-meta-boxes.php';

/**
 * Apollo_Admin_Artist_Meta_Boxes
 */
class Apollo_Admin_Artist_Meta_Boxes extends Apl_Ad_Module_Meta_Boxes {
    
	/**
	 * Constructor
	 */
	public function __construct() {
        
        parent::__construct( Apollo_DB_Schema::_ARTIST_PT );
        
        $this->require_meta_boxes();
     
		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 30 );

		add_action( 'save_post', array( $this, 'save_meta_boxes' ), 1, 2 );
        
        // Save Meta Boxes
		add_action( $this->process_data_action, 'Apollo_Meta_Box_Artist_Data::save', 10, 2 );
		add_action( $this->process_data_action, 'Apollo_Meta_Box_Artist_Timeline::save', 10, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Artist_Question::save', 10, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Artist_Address::save', 10, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Artist_Images::save', 20, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_UploadPDF::save', 20, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Artist_Video::save', 10, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Common::updateChangeAuthor', 10, 2 );

        
        // Error handling (for showing errors from meta boxes on next page load)
		add_action( 'admin_notices', array( $this, 'output_errors' ) );
        add_action( 'shutdown', array( $this, 'save_errors' ) );
        
	}
    
    public function require_meta_boxes() {
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/artist/class-apollo-meta-box-artist-data.php';
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/artist/class-apollo-meta-box-artist-option.php';
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/artist/class-apollo-meta-box-artist-address.php';
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/artist/class-apollo-meta-box-artist-images.php';
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/artist/class-apollo-meta-box-artist-video.php';
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/artist/class-apollo-meta-box-artist-timeline.php';
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/artist/class-apollo-meta-box-artist-social.php';
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/artist/class-apollo-meta-box-artist-question.php';
    }

	/**
	 * Add Apollo Meta boxes
	 */
	public function add_meta_boxes() {
        $enable_pbm = apply_filters('apl_pbm_module_active', null);

        add_meta_box( 'apollo-artist-data', __( 'Artist Data', 'apollo' ),
            'Apollo_Meta_Box_Artist_Data::output', Apollo_DB_Schema::_ARTIST_PT, 'normal' );
        
        add_meta_box( 'apollo-artist-options', __( 'Artist Options', 'apollo' ), 
            'Apollo_Meta_Box_Artist_Options::output', Apollo_DB_Schema::_ARTIST_PT, 'normal' );
        
        add_meta_box( 'apollo-artist-address', __( 'Address', 'apollo' ), 
            'Apollo_Meta_Box_Artist_Address::output', Apollo_DB_Schema::_ARTIST_PT, 'normal' );
        
        add_meta_box( 'apollo-event-data', __( 'Artist Video', 'apollo' ),
            'Apollo_Meta_Box_Artist_Video::output', Apollo_DB_Schema::_ARTIST_PT, 'normal' );
        
        add_meta_box( 'apollo-artist-social', __( 'Social Network URL', 'apollo' ), 
            'Apollo_Meta_Box_Artist_Social::output', Apollo_DB_Schema::_ARTIST_PT, 'normal' );
        
        add_meta_box( 'apollo-artist-question', __( 'Questions', 'apollo' ), 
            'Apollo_Meta_Box_Artist_Question::output', Apollo_DB_Schema::_ARTIST_PT, 'normal' );
        
        add_meta_box( 'apollo-artist-customfield', __( 'More Info', 'apollo' ), 
            'Apollo_Meta_Box_Customfield::output', Apollo_DB_Schema::_ARTIST_PT, 'normal' );
        
        add_meta_box( 'apollo-artist-images', __( 'Artist Gallery', 'apollo' ),
            'Apollo_Meta_Box_Artist_Images::output', Apollo_DB_Schema::_ARTIST_PT, $enable_pbm ? 'normal' : 'side');

        add_meta_box( 'apollo-artist-timeline', __( 'Add To TimeLine', 'apollo' ),
            'Apollo_Meta_Box_Artist_Timeline::output', Apollo_DB_Schema::_ARTIST_PT, 'normal' );

        add_meta_box( 'apollo-artist-uploadpdf', __( 'Documents (PDF)', 'apollo' ),
            'Apollo_Meta_Box_UploadPDF::output', Apollo_DB_Schema::_ARTIST_PT, 'normal' );
        
        add_meta_box( 'apollo-icons', __( 'Icons', 'apollo' ), 
            'Apollo_Meta_Box_Common::output_icons', $this->type, 'normal' );

        add_meta_box( 'apollo-artist-user', __( 'Associated User(s)', 'apollo' ),
            'Apollo_Meta_Box_Common::output_associated_users', Apollo_DB_Schema::_ARTIST_PT, 'side' );

        if (Apollo_App::is_avaiable_module(Apollo_DB_Schema::_AGENCY_PT)) {
            add_meta_box('apollo-artist-agency', __('Associated Agencies', 'apollo'),
                'Apollo_Meta_Box_Common::output_associated_agencies', Apollo_DB_Schema::_ARTIST_PT, 'side');
        }
        /** @Ticket #16209 */
        $currentUser = get_current_user_id();
        if (is_super_admin($currentUser) || user_can($currentUser, 'manage_options')) {
            add_meta_box( 'apollo-venue-author', __( 'Change Author', 'apollo' ),
                'Apollo_Meta_Box_Common::outputChangeAuthor', $this->type, 'side' );
        }

        /** @Ticket #17029 Artist membership */
        $checkIsMemberActivated = of_get_option(Apollo_DB_Schema::_ARTIST_ACTIVE_MEMBER_FIELD, false);
        if($checkIsMemberActivated){
            add_meta_box( 'apollo-artist-member', __( 'Member', 'apollo' ),
                'Apollo_Meta_Box_Artist_Data::outputIsMember', $this->type, 'side', 'high' );
        }

        /*@ticket #18310: 0002510: Artist Directory Plugin > New Artist option*/
        $enableOptionNewArtist = of_get_option(APL_Theme_Option_Site_Config_SubTab::_ARTIST_ENABLE_OPTION_NEW_ARTIST, false);
        if($enableOptionNewArtist){
            add_meta_box( 'apollo-new-artist', __( 'New Artist', 'apollo' ),
                'Apollo_Meta_Box_Artist_Data::outputIsNewArtist', $this->type, 'side', 'high' );
        }
	}
}

new Apollo_Admin_Artist_Meta_Boxes();
