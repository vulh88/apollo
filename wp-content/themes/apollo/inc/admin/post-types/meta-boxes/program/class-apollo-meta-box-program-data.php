<?php
/**
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Program_Data
 */
class Apollo_Meta_Box_Program_Data {

	/**
	 * Output the metabox
	 */
	public static function output( $post ) { 
        
        global $post, $thepostid;
		wp_nonce_field( 'apollo_program_meta_nonce', 'apollo_program_meta_nonce' );
        $thepostid = $post->ID;
    
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_PROGRAM_DATA .'['. Apollo_DB_Schema::_APL_PROGRAM_CNAME .']', 
                'label' => __( 'Contact Name', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_PROGRAM_DATA .'['. Apollo_DB_Schema::_APL_PROGRAM_PHONE .']', 
                'label' => __( 'Phone', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_PROGRAM_DATA .'['. Apollo_DB_Schema::_APL_PROGRAM_EMAIL .']', 
                'label' => __( 'Email', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'class' => 'apollo_input_email',
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_PROGRAM_DATA .'['. Apollo_DB_Schema::_APL_PROGRAM_URL .']', 
                'label' => __( 'Website', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'class' => 'apollo_input_url',
            ) );
        echo '</div>';
        
	}
    
    public static function output_data() {
        
        global $post, $wpdb, $thepostid;
		wp_nonce_field( 'apollo_event_meta_nonce', 'apollo_event_meta_nonce' );
        $thepostid = $post->ID;
        ?>
        <div class="panel-wrap product_data">
            
			<div class="apollo-tabs-back"></div>

			<ul class="product_data_tabs apollo-tabs" style="display:none;">
				<?php
					$product_data_tabs = apply_filters( 'apollo_event_data_tabs', array(
                        'general-info' => array(
							'label'  => __( 'General', 'apollo' ),
							'target' => 'general-prog-data',
							'class'  => array( 'active' ),
						),
						'video' => array(
							'label'  => __( 'Video', 'apollo' ),
							'target' => 'video-event-data',
							'class'  => array( '' ),
						),
                        'datetime' => array(
							'label'  => __( 'Date range', 'apollo' ),
							'target' => 'datetime-event-data',
							'class'  => array(),
						),
						
					) );

					foreach ( $product_data_tabs as $key => $tab ) {
						?><li class="<?php echo $key; ?>_options <?php echo $key; ?>_tab <?php echo implode( ' ' , $tab['class'] ); ?>">
							<a href="#<?php echo $tab['target']; ?>"><?php echo esc_html( $tab['label'] ); ?></a>
						</li><?php
					}

					do_action( 'apollo_product_write_panel_tabs' );
				?>
			</ul>
            
            <div id="general-prog-data" class="panel apollo_options_panel">
                <?php 
                     echo '<div class="options_group">';
                        apollo_wp_text_input(  array( 
                            'id' => ''. Apollo_DB_Schema::_APL_PROGRAM_DATA .'['. Apollo_DB_Schema::_APL_PROGRAM_MAX_STUDENTS .']', 
                            'label' => __( 'Max Number of Students', 'apollo' ), 
                            'desc_tip' => 'true', 
                            'description' => __( 'Must be greater than 0' , 'apollo'), 
                            'type' => 'text', 
                            'data_elm'  => 'data-min="0"',
                            'class' => 'apollo_input_number apollo-number-min short',
                            'required' => of_get_option(APL_Theme_Option_Site_Config_SubTab::_PROGRAM_ENABLE_REQUIREMENT_MAX_STUDENTS, 0) ? true : false
                        ) );
                    echo '</div>';
                    
                    echo '<div class="options_group">';
                        apollo_wp_text_input(  array( 
                            'id' => ''. Apollo_DB_Schema::_APL_PROGRAM_DATA .'['. Apollo_DB_Schema::_APL_PROGRAM_LENGTH_PROGRAM .']',
                            'label' => __( 'Length of Program', 'apollo' ), 
                            'desc_tip' => 'true', 
                            'description' => __( 'Min / hours per session', 'apollo' ), 
                            'type' => 'text', 
                        ) );
                    echo '</div>';
                ?>
                
			</div>
            
            <div id="video-event-data" class="panel apollo_options_panel">
                <div class="video-wrapper">
                    <?php 
                        $event_data = @unserialize( get_apollo_meta( $thepostid, Apollo_DB_Schema::_APL_PROGRAM_DATA, TRUE ) );
                        
                        $video_embed = maybe_unserialize(get_apollo_meta( $thepostid, Apollo_DB_Schema::_APL_PROGRAM_VIDEO_EMBED, TRUE ));
                        $video_desc = maybe_unserialize(get_apollo_meta( $thepostid, Apollo_DB_Schema::_APL_PROGRAM_VIDEO_DESC, TRUE ));

                        $videos = array();
                        if ( $video_embed ):
                            foreach($video_embed as $i =>  $v) {
                                $videos[] = array( 'embed' => $v, 'desc' => isset($video_desc[$i]) ? $video_desc[$i] : '' );
                            }
                        endif;
                      
                        if ( ! $videos ) {
                            $videos[] = array( 'embed' => '', 'desc' => '' );
                        }
                        
                        $i = 0;
                        foreach( $videos as $v ):
                            $prefix_id = $i != 0 ? '-'.$i : '';
                    ?>
                    <div class="count video-list<?php echo $prefix_id; ?>">
                        
                        <div class="options_group">
                            <p class="form-field video_embed_field ">
                                <label for="video_embed"><?php _e( 'YouTube or Vimeo URL', 'apollo' ) ?></label>
                                <input name="video_embed[]" type="text" value="<?php echo $v['embed'] ?>" class="full apollo_input_url" />
                            </p>
                        </div>
                        
                        <div class="options_group">
                            <p class="form-field video_desc_field ">
                                <label for="video_embed"><?php _e( 'Video Description', 'apollo' ) ?></label>
                                <textarea class="full" name="video_desc[]" placeholder="" rows="2" cols="20"><?php echo $v['desc'] ?></textarea> 
                            </p>
                        </div>
                     
                        <div <?php if ( $i == 0 ) echo 'style="display: none;"' ?> data-confirm="<?php _e( 'Are you sure to remove this video ?', 'apollo' ) ?>" class="del"><i class="fa fa-times"></i></div>
                     
                    </div>
                    <?php $i++; endforeach; ?>
                </div>
                <input type="hidden" name="<?php echo Apollo_DB_Schema::_APL_PROGRAM_DATA ?>[<?php echo Apollo_DB_Schema::_APL_PROGRAM_VIDEO ?>]" />
                <input type="button" class="button button-primary button-large" id="apl-add-video" value="<?php _e( 'Add more links', 'apollo' ) ?>" />
			</div>
            
            <div id="datetime-event-data" class="panel apollo_options_panel">
                <?php 
                     echo '<div class="options_group">';

                        apollo_wp_text_input(  array( 
                            'id' => Apollo_DB_Schema::_APL_PROGRAM_STARTD, 
                            'label' => __( 'Start date', 'apollo' ), 
                            'desc_tip' => 'true', 
                            'description' => '', 
                            'type' => 'text', 
                            'data_parent'   => '.datetime_options',
                        ) );

                    echo '</div>';
                    echo '<div class="options_group">';

                        apollo_wp_text_input(  array( 
                            'id' => Apollo_DB_Schema::_APL_PROGRAM_ENDD, 
                            'label' => __( 'End date', 'apollo' ), 
                            'desc_tip' => 'true', 
                            'description' => '', 
                            'type' => 'text', 
                            'data_compare'  => Apollo_DB_Schema::_APL_PROGRAM_STARTD,
                            'class' => 'apollo-date-greater short',
                            'data_parent'   => '.datetime_options',
                        ) );

                    echo '</div>';
                    echo '<div class="options_group">';

                        apollo_wp_text_input(  array(
                            'id' => Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND,
                            'label' => __( 'Expiration date', 'apollo' ),
                            'desc_tip' => 'true',
                            'description' => '',
                            'type' => 'text',
                            'class' => 'apollo-date-greater short',
                            'data_parent'   => '.datetime_options',
                        ) );

                    echo '</div>';
                ?>
			</div>
            <div class="clear"></div>
        </div>    
        <?php
    }


    public static function output_daterange($post) {
        
        global $post, $thepostid;
		wp_nonce_field( 'apollo_program_meta_nonce', 'apollo_program_meta_nonce' );
        $thepostid = $post->ID;
        
        echo '<div class="options_group event-box">';

           apollo_wp_text_input(  array( 
                'id' => Apollo_DB_Schema::_APL_PROGRAM_STARTD, 
                'label' => __( 'Start date', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => '', 
                'type' => 'text', 
           ) );

        echo '</div>';

        echo '<div class="options_group event-box">';

           apollo_wp_text_input(  array( 
                'id' => Apollo_DB_Schema::_APL_PROGRAM_ENDD, 
                'label' => __( 'End date', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => '', 
                'type' => 'text', 
           ) );

        echo '</div>';
    }
    
    public static function output_options() {
        global $post, $thepostid;
		wp_nonce_field( 'apollo_program_meta_nonce', 'apollo_program_meta_nonce' );
        $thepostid = $post->ID;

        /** @Task #12782 */
        $bilingual_option = of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_BILINGUAL, true);
        $free_option = of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_FREE, true);

        echo '<div class="options_group">';
            // Description
            apollo_wp_checkbox(  array(
                'id' => Apollo_DB_Schema::_APL_PROGRAM_IS_BILINGUAL,
                'wrapper_class' => !$bilingual_option ? 'on-off-theme-options' : '',
                'label' => __( 'Bilingual', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'type' => 'checkbox', )
            );

            /** @Task #12788*/
            apollo_wp_checkbox(  array(
                'id' => Apollo_DB_Schema::_APL_PROGRAM_IS_FREE,
                'wrapper_class' => !$free_option ? 'on-off-theme-options' : '',
                'label' => __( 'Free', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'type' => 'checkbox', )
            );

        echo '</div>';
    }
    
    public static function output_teks_guidelines() {
        global $post, $thepostid;
		wp_nonce_field( 'apollo_program_meta_nonce', 'apollo_program_meta_nonce' );
        $thepostid = $post->ID;


        $label = of_get_option(Apollo_DB_Schema::_EDUCATOR_PROGRAM_STANDARD_LABEL, __('Standards are customized for every classroom visit', 'apollo'));
        echo '<div class="options_group">';
            // Description
            apollo_wp_checkbox(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_PROGRAM_DATA .'['. Apollo_DB_Schema::_APL_PROGRAM_TEKS_GUIDELINES .']', 
                'label' => $label,
                'desc_tip' => 'true', 
                'description' => "",
                'type' => 'checkbox', ) );
        echo '</div>';
    }
    
    public static function output_cancel_policy() {
        global $post, $thepostid;
		wp_nonce_field( 'apollo_program_meta_nonce', 'apollo_program_meta_nonce' );
        $thepostid = $post->ID;
      
        echo '<div class="options_group">';
            // Description
            apollo_wp_editor(  array( 
                'name' => ''. Apollo_DB_Schema::_APL_PROGRAM_DATA .'['. Apollo_DB_Schema::_APL_PROGRAM_CANCEL_POLICY .']', 
                'id' => Apollo_DB_Schema::_APL_PROGRAM_CANCEL_POLICY, 
                'label' => '', 
                'desc_tip' => 'true', 
                'description' => "",
            ) );
        echo '</div>';
    }
    
    public static function output_preferences() {
        global $post, $thepostid;
		wp_nonce_field( 'apollo_program_meta_nonce', 'apollo_program_meta_nonce' );
        $thepostid = $post->ID;
      
        echo '<div class="options_group">';
            // Description
            apollo_wp_editor(  array( 
                'name' => ''. Apollo_DB_Schema::_APL_PROGRAM_DATA .'['. Apollo_DB_Schema::_APL_PROGRAM_REFERENCES .']', 
                'id' => Apollo_DB_Schema::_APL_PROGRAM_REFERENCES, 
                'label' => '', 
                'desc_tip' => 'true', 
                'description' => "",
            ) );
        echo '</div>';

        /*@ticket #18391: Arts Education Customizations - FE Program Form - The PDF files and text do NOT display on the admin program form nor the FE program detail page - item 1, 2*/
        echo '<div class="apl-program-references">';
        Apollo_Meta_Box_UploadPDF::output($post, array('args' => array('suffix' => "_references")));
        echo '</div>';
    }
    
    public static function output_others() {
        global $post, $thepostid;
		wp_nonce_field( 'apollo_program_meta_nonce', 'apollo_program_meta_nonce' );
        $thepostid = $post->ID;
      
        echo '<div class="options_group">';
            // Description
            apollo_wp_editor(  array( 
                'name' => ''. Apollo_DB_Schema::_APL_PROGRAM_DATA .'['. Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_DATE .']', 
                'id' => Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_DATE, 
                'label' => __( 'Available Dates', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
            ) );
        echo '</div>';
        
        echo '<div class="options_group">';
            // Description
            apollo_wp_editor(  array( 
                'name' => ''. Apollo_DB_Schema::_APL_PROGRAM_DATA .'['. Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_TIME .']', 
                'id' => Apollo_DB_Schema::_APL_PROGRAM_AVAIABLE_TIME, 
                'label' => __( 'Available Times', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
            ) );
        echo '</div>';
        
        echo '<div class="options_group">';
            // Description
            apollo_wp_editor(  array( 
                'name' => ''. Apollo_DB_Schema::_APL_PROGRAM_DATA .'['. Apollo_DB_Schema::_APL_PROGRAM_SPACE_TECHNICAL .']', 
                'id' => Apollo_DB_Schema::_APL_PROGRAM_SPACE_TECHNICAL, 
                'label' => __( 'Space / Technical Requirements', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
            ) );
        echo '</div>';
        
        echo '<div class="options_group">';
            // Description
            apollo_wp_editor(  array( 
                'name' => ''. Apollo_DB_Schema::_APL_PROGRAM_DATA .'['. Apollo_DB_Schema::_APL_PROGRAM_LOCATION .']', 
                'id' => Apollo_DB_Schema::_APL_PROGRAM_LOCATION, 
                'label' => __( 'Location(s)', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
            ) );
        echo '</div>';
        
        echo '<div class="options_group">';
            // Description
            apollo_wp_editor(  array( 
                'name' => ''. Apollo_DB_Schema::_APL_PROGRAM_DATA .'['. Apollo_DB_Schema::_APL_PROGRAM_FEE .']', 
                'id' => Apollo_DB_Schema::_APL_PROGRAM_FEE, 
                'label' => __( 'Fees / Ticketing', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
            ) );
        echo '</div>';
        if(of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_CORE_FIELD)) {
            echo '<div class="options_group">';
            $programCoreLabel = of_get_option(Apollo_DB_Schema::_EDUCATOR_PROGRAM_CORE_LABEL) ? of_get_option(Apollo_DB_Schema::_EDUCATOR_PROGRAM_CORE_LABEL) : 'Program Core';
            // Description
            apollo_wp_editor(array(
                'name' => '' . Apollo_DB_Schema::_APL_PROGRAM_DATA . '[' . Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_CORE . ']',
                'id' => Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_CORE,
                'label' => __($programCoreLabel, 'apollo'),
                'desc_tip' => 'true',
                'description' => "",
            ));
            echo '</div>';
        }
        if(of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_ESSENTIALS_FIELD)) {
            echo '<div class="options_group">';
            $programEssentialsLabel = of_get_option(Apollo_DB_Schema::_EDUCATOR_PROGRAM_ESSENTIALS_LABEL) ? of_get_option(Apollo_DB_Schema::_EDUCATOR_PROGRAM_ESSENTIALS_LABEL) : 'Program Essentials';
            // Description
            apollo_wp_editor(array(
                'name' => '' . Apollo_DB_Schema::_APL_PROGRAM_DATA . '[' . Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_ESSENTIAL . ']',
                'id' => Apollo_DB_Schema::_APL_PROGRAM_PROGRAM_ESSENTIAL,
                'label' => __($programEssentialsLabel, 'apollo'),
                'desc_tip' => 'true',
                'description' => "",
            ));
            echo '</div>';
        }
        
    }
    
    
    public static function output_educator() {
        global $post, $thepostid;
		wp_nonce_field( 'apollo_program_meta_nonce', 'apollo_program_meta_nonce' );
        $thepostid = $post->ID;
       
        $apl_query = new Apl_Query( Apollo_Tables::_APL_PROGRAM_EDUCATOR );
        $prod_edu = $apl_query->get_row( "prog_id = $thepostid" );
        //select2
        $Select2 = apl_instance('APL_Lib_Helpers_Select2', array(
            'post_type' => Apollo_DB_Schema::_EDUCATOR_PT,
        ));

//        $educators = get_posts( array(
//            'post_type'     => Apollo_DB_Schema::_EDUCATOR_PT,
//            'post_status'   => 'publish',
//            'posts_per_page' => -1,
//            'orderby'   => 'post_title',
//            'order' => 'ASC',
//        ) );
//
//        $option = array();
//        $option[] = __( 'Select An Educator' );
//
//        if ( $educators ) {
//            foreach ( $educators as $e ) {
//                $option[$e->ID] = $e->post_title;
//            }
//        }
        if ($Select2->getEnableRemote()) {
            self::output_educator_remote_data_html($prod_edu, $Select2);
        } else {
            self::output_educator_html($prod_edu);
        }
//        $attr = sprintf('data-enable-remote="%s" data-post-type="%s" data-source-url="%s"',
//            true,
//            Apollo_DB_Schema::_EDUCATOR_PT,
//            'apollo_get_remote_data_to_select2_box'
//        );
//        $options = $Select2->getItemsInPostTypeToDropDownList(array(
//            'post_status' => array('publish'),
//            'selected_item' => $prod_edu ? $prod_edu->edu_id : ""
//        ));
//
//
//        echo '<div class="options_group event-box wrap-select2">';
//            // Description
//            apollo_wp_select(  array(
//                'id'            => 'edu_id',
//                'label'         => '',
//                'desc_tip'      => 'true',
//                'description'   => "",
//                'options'       => $options,
//                'value'         => $prod_edu ? $prod_edu->edu_id : 0,
//                'attr'          => $attr,
//                'class'         => 'apl_select2 apl-primary-category',
//                'required'      => true,
//                'first_option'  => __('Select An Educator', 'apollo'),
//                'is_post_type_list' => true
//            ) );
//        echo '</div>';
    }

    public static function output_educator_remote_data_html($prod_edu, $Select2){
        $attr = sprintf('data-enable-remote="%s" data-post-type="%s" data-source-url="%s"',
            true,
            Apollo_DB_Schema::_EDUCATOR_PT,
            'apollo_get_remote_data_to_select2_box'
        );
        $options = $Select2->getItemsInPostTypeToDropDownList(array(
            'post_status' => array('publish'),
            'selected_item' => $prod_edu ? $prod_edu->edu_id : ""
        ));


        echo '<div class="options_group event-box wrap-select2">';
        // Description
        apollo_wp_select(  array(
            'id'            => 'edu_id',
            'label'         => '',
            'desc_tip'      => 'true',
            'description'   => "",
            'options'       => $options,
            'value'         => $prod_edu ? $prod_edu->edu_id : 0,
            'attr'          => $attr,
            'class'         => 'apl_select2 apl-primary-category',
            'required'      => true,
            'first_option'  => __('Select An Educator', 'apollo'),
            'is_post_type_list' => true
        ) );
        echo '</div>';
    }

    public static function output_educator_html($prod_edu){
        $educators = get_posts( array(
            'post_type'     => Apollo_DB_Schema::_EDUCATOR_PT,
            'post_status'   => 'publish',
            'posts_per_page' => -1,
            'orderby'   => 'post_title',
            'order' => 'ASC',
        ) );

        $options = array();
        $options[] = __( 'Select An Educator' );

        if ( $educators ) {
            foreach ( $educators as $e ) {
                $options[$e->ID] = $e->post_title;
            }
        }

        echo '<div class="options_group event-box wrap-select2">';
        // Description
        apollo_wp_select(  array(
            'id'            => 'edu_id',
            'label'         => '',
            'desc_tip'      => 'true',
            'description'   => "",
            'options'       => $options,
            'value'         => $prod_edu ? $prod_edu->edu_id : 0,
            'class'         => 'apl_select2 apl-primary-category',
            'required'      => true,
        ) );
        echo '</div>';

    }
    
	/**
	 * Save meta box data
	 */
	public static function save( $post_id, $post ) {
        
        $_data = $_POST[Apollo_DB_Schema::_APL_PROGRAM_DATA];
        
        // Save educator
        $edu_id = isset( $_POST['edu_id'] ) ? $_POST['edu_id'] : '';
        
        $video_embed = isset( $_POST['video_embed'] ) ? $_POST['video_embed'] : array();
        $video_desc = isset( $_POST['video_desc'] ) ? $_POST['video_desc'] : array();

        update_apollo_meta($post_id, Apollo_DB_Schema::_APL_PROGRAM_VIDEO_EMBED, array_diff( $video_embed , array( '' )) );
        update_apollo_meta($post_id, Apollo_DB_Schema::_APL_PROGRAM_VIDEO_DESC, array_diff( $video_desc , array( '' )) );

        $email_field = Apollo_DB_Schema::_APL_PROGRAM_EMAIL;
        if ( isset( $_data[$email_field] ) && $_data[$email_field] ):
            if ( is_email( $_data[$email_field] ) ) {
                $_data[$email_field] = Apollo_App::clean_data_request( $_data[$email_field] );
            } 
        endif;
        
        $url_check = array(
            Apollo_DB_Schema::_APL_PROGRAM_URL => __( 'Website', 'apollo' ),
        );
        
        foreach( $url_check as $url_field => $l ) {
            if ( isset( $_data[$url_field] ) && $_data[$url_field] ):
                if ( filter_var( $_data[$url_field], FILTER_VALIDATE_URL ) ) {
                    $_data[$url_field] = Apollo_App::clean_data_request( $_data[$url_field] );
                }
            endif;
        }
        
        $_data[Apollo_DB_Schema::_PROGRAM_EDU_ID] = $edu_id;
        update_apollo_meta( $post_id, Apollo_DB_Schema::_PROGRAM_EDU_ID, $edu_id );
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_PROGRAM_DATA, serialize( Apollo_App::clean_array_data( $_data ) ) );
        
        // Update event date range
        $start_d = Apollo_DB_Schema::_APL_PROGRAM_STARTD;
        $end_d = Apollo_DB_Schema::_APL_PROGRAM_ENDD;
        $expiration_d = Apollo_DB_Schema::_APL_PROGRAM_EXPIRATIOND;
        update_apollo_meta( $post_id, $start_d, stripslashes( sanitize_text_field( $_POST[$start_d] ) ) );
        update_apollo_meta( $post_id, $end_d, stripslashes( sanitize_text_field( $_POST[$end_d] ) ) );
        update_apollo_meta( $post_id, $expiration_d, stripslashes( sanitize_text_field( $_POST[$expiration_d] ) ) );

        
        // Update event options
        $val = isset( $_POST[Apollo_DB_Schema::_APL_PROGRAM_IS_BILINGUAL] ) ? $_POST[Apollo_DB_Schema::_APL_PROGRAM_IS_BILINGUAL] : 'no';
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_PROGRAM_IS_BILINGUAL, stripslashes( sanitize_text_field( $val ) ) );
        /** @Task #12788*/
        $free_val = isset( $_POST[Apollo_DB_Schema::_APL_PROGRAM_IS_FREE] ) ? $_POST[Apollo_DB_Schema::_APL_PROGRAM_IS_FREE] : 'no';
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_PROGRAM_IS_FREE, stripslashes( sanitize_text_field( $free_val ) ) );
        
        // Save educator
        if ( $edu_id ) {
            $apl_query = new Apl_Query( Apollo_Tables::_APL_PROGRAM_EDUCATOR );
           
            $post_id = intval( $post_id );
            $edu_id  = intval( $edu_id );
            
            $apl_query->delete( "prog_id = $post_id" );
       
            $apl_query->insert( array(
                'prog_id'   => $post_id,
                'edu_id'    => $edu_id,
            ) );
        }

        /*@ticket #18391: Arts Education Customizations - FE Program Form - The PDF files and text do NOT display on the admin program form nor the FE program detail page - item 1, 2*/
        Apollo_Meta_Box_UploadPDF::save($post_id, $post, "_related_materials");
        Apollo_Meta_Box_UploadPDF::save($post_id, $post, "_references");
	}
}