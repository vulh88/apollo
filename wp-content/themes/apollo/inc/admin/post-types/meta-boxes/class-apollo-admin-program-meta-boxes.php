<?php
/**
 * Apollo Educator Meta Boxes
 *
 * @author 		Elisoft
 * @category 	Admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

include 'class-apl-ad-module-meta-boxes.php';

/**
 * Apollo_Admin_Program_Meta_Boxes
 */
class Apollo_Admin_Program_Meta_Boxes extends Apl_Ad_Module_Meta_Boxes {
    
	/**
	 * Constructor
	 */
	public function __construct() {
     
        parent::__construct( Apollo_DB_Schema::_PROGRAM_PT );
        
        $this->require_meta_boxes();

        add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 30 );
		add_action( 'save_post', array( $this, 'save_meta_boxes' ), 1, 2 );
        
        // Save Meta Boxes
		add_action( $this->process_data_action, 'Apollo_Meta_Box_Program_Data::save', 10, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Program_Images::save', 10, 2 );
        
        // Error handling (for showing errors from meta boxes on next page load)
		add_action( 'admin_notices', array( $this, 'output_errors' ) );
        add_action( 'shutdown', array( $this, 'save_errors' ) );
        
        add_action( 'delete_post', array( $this, 'delete_post' ) );
        
	}
    
    public function delete_post( $post_id ) {
        // Delete all data from programs educator
        $apl_query = new Apl_Query( Apollo_Tables::_APL_PROGRAM_EDUCATOR );
        $apl_query->delete( "prog_id = $post_id" );
    }
    
    public function require_meta_boxes() {
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/program/class-apollo-meta-box-program-data.php';
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/program/class-apollo-meta-box-program-images.php';
    }

	/**
	 * Add Apollo Meta boxes
	 */
	public function add_meta_boxes() {
        add_meta_box( 'apollo-program-contact', __( 'Contact Info', 'apollo' ), 
            'Apollo_Meta_Box_Program_Data::output', Apollo_DB_Schema::_PROGRAM_PT, 'normal' );
        
        add_meta_box( 'apollo-program-options', __( 'Program Options', 'apollo' ), 
            'Apollo_Meta_Box_Program_Data::output_options', Apollo_DB_Schema::_PROGRAM_PT, 'normal' );
        
        add_meta_box( 'apollo-program-teks-guidelines', __( 'Education Standards', 'apollo' ), 
            'Apollo_Meta_Box_Program_Data::output_teks_guidelines', Apollo_DB_Schema::_PROGRAM_PT, 'normal' );
        
        add_meta_box( 'apollo-program-cancellation-policy', __( 'Cancellation Policy', 'apollo' ), 
            'Apollo_Meta_Box_Program_Data::output_cancel_policy', Apollo_DB_Schema::_PROGRAM_PT, 'normal' );
        
        add_meta_box( 'apollo-program-references', __( 'References', 'apollo' ), 
            'Apollo_Meta_Box_Program_Data::output_preferences', Apollo_DB_Schema::_PROGRAM_PT, 'normal' );
        
        add_meta_box( 'apollo-program-other-fields', __( 'Other Fields', 'apollo' ), 
            'Apollo_Meta_Box_Program_Data::output_others', Apollo_DB_Schema::_PROGRAM_PT, 'normal' );
        
        add_meta_box( 'apollo-event-data', __( 'Program Data', 'apollo' ), 
            'Apollo_Meta_Box_Program_Data::output_data', Apollo_DB_Schema::_PROGRAM_PT, 'normal' );
      
        add_meta_box( 'apollo-program-educator' , __( 'Educator', 'apollo' ),
            'Apollo_Meta_Box_Program_Data::output_educator', Apollo_DB_Schema::_PROGRAM_PT, 'side' );
        
        add_meta_box( 'apollo-program-images', __( 'Gallery', 'apollo' ), 
            'Apollo_Meta_Box_Program_Images::output', Apollo_DB_Schema::_PROGRAM_PT, 'side' );
        
        add_meta_box( 'apollo-program-customfield', __( 'More Info', 'apollo' ), 
            'Apollo_Meta_Box_Customfield::output', Apollo_DB_Schema::_PROGRAM_PT, 'normal' );
        
        add_meta_box( 'apollo-program-pdf', __( 'Related Materials Documents', 'apollo' ), 
            'Apollo_Meta_Box_UploadPDF::output', Apollo_DB_Schema::_PROGRAM_PT, 'normal', 'default', array('suffix' => '_related_materials') );
	}
}

new Apollo_Admin_Program_Meta_Boxes();
