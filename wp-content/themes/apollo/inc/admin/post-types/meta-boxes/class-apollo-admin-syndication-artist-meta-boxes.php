<?php
/**
 * Apollo Artist Meta Boxes
 *
 * Sets up the write panels used by venues (custom post types)
 *
 * @author 		vulh
 * @category 	Admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

require_once 'class-apl-ad-module-meta-boxes.php';
/**
 * Apollo_Admin_Syndication_Artist_Meta_Boxes
 */
class Apollo_Admin_Syndication_Artist_Meta_Boxes extends Apl_Ad_Module_Meta_Boxes {
    
	/**
	 * Constructor
	 */
	public function __construct() {
        
        parent::__construct( Apollo_DB_Schema::_SYNDICATION_ARTIST_PT );
        
        $this->require_meta_boxes();
     
		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 30 );
		add_action( 'save_post', array( $this, 'save_meta_boxes' ), 1, 2 );

        // Save Meta Boxes
		add_action( $this->process_data_action, 'Apollo_Meta_Box_Syndication_Artist_Info::save', 10, 2 );
		add_action( $this->process_data_action, 'Apollo_Meta_Box_Syndication_Artist_Settings::save', 10, 2 );
		add_action( $this->process_data_action, 'Apollo_Meta_Box_Syndication_Artist_Xml_Generation::save', 10, 2 );

        // Error handling (for showing errors from meta boxes on next page load)
		add_action( 'admin_notices', array( $this, 'output_errors' ) );
        add_action( 'shutdown', array( $this, 'save_errors' ) );
        
	}

    public static function getMetaDataByKey($post_id, $key, $default = ''){
        if(isset($_POST[$key])){
            return $_POST[$key];
        }
        $metaVal = get_post_meta($post_id,$key,true);
        if(is_serialized($metaVal)){
            $metaVal = maybe_unserialize($metaVal);
        }

        return $metaVal != null && $metaVal != ''  ? $metaVal : $default;

    }

    public function require_meta_boxes() {
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/syndication/artist/class-apollo-meta-box-syndication-artist-info.php';
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/syndication/artist/class-apollo-meta-box-syndication-artist-settings.php';
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/syndication/artist/class-apollo-meta-box-syndication-artist-xml-generation.php';
    }

	/**
	 * Add Apollo Meta boxes
	 */
	public function add_meta_boxes() {
        add_meta_box( 'apollo-syndication-artist-info', __( 'Artist Info', 'apollo' ),
            'Apollo_Meta_Box_Syndication_Artist_Info::output', Apollo_DB_Schema::_SYNDICATION_ARTIST_PT, 'normal' );

        add_meta_box( 'apollo-syndication-artist-settings', __( 'Artist Settings', 'apollo' ),
            'Apollo_Meta_Box_Syndication_Artist_Settings::output', Apollo_DB_Schema::_SYNDICATION_ARTIST_PT, 'normal' );

        add_meta_box( 'apollo-syndication-artist-xml-generation', __( 'XML Generation', 'apollo' ),
            'Apollo_Meta_Box_Syndication_Artist_Xml_Generation::output', Apollo_DB_Schema::_SYNDICATION_ARTIST_PT, 'normal' );
	}
}

new Apollo_Admin_Syndication_Artist_Meta_Boxes();
