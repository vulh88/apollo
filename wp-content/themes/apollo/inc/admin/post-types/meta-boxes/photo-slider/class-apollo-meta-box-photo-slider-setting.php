<?php
/**
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Photo_Slider_Setting
 */
class Apollo_Meta_Box_Photo_Slider_Setting {

    /**
     * Output the metabox
     */
    public static function output( $post ) {
        global $post, $thepostid, $wpdb;
        wp_nonce_field( 'apollo_photo-slider_meta_nonce', 'apollo_photo-slider_meta_nonce' );
        $thepostid = $post->ID;
        $available_modules = Apollo_App::getModulesForPhotoSlider();

        echo '<div class="options_group clear wrap-blk ct-full-width">';
        if( !empty($available_modules)) {
            foreach ($available_modules as $module => $index) {
                $data = Apollo_App::getDataFromModulePulled($module, $thepostid, true);
                $hasData = !empty($data) && count($data) > 0 ? true : false ;
                echo '<div class="options_group clear event-box ps-box-label">';
                // Account ID
                apollo_wp_info_field(  array(
                    'desc_tip' => 'true',
                    'description' => "",
                    'class' => 'noticed_info',
                    'text' => '<h2>'.Apollo_App::formatModuleTitle($module ). '</h2>'
                ) );
                echo '</div>';

                echo '<div class="options_group clear ct-selection-box">';
                apollo_wp_radio(array(
                    'id' => 'enable_images_' . $module,
                    'label' => sprintf(__('Pull Images From %s', 'apollo'), Apollo_App::formatModuleTitle($module )),
                    'class' => '',
                    'value' =>  $hasData ? Apollo_Admin_Photo_Slider_Meta_Boxes::getDataByKey($thepostid, 'enable_images_' .$module, 'OFF') : 'OFF',
                    'disable' => !$hasData,
                    'options' => array('ON' => 'ON',
                        'OFF' => 'OFF'
                    )
                ));
                echo '</div>';
                if ( !$hasData ){
                    echo '<div class="options_group clear event-box">';
                    // Account ID
                    apollo_wp_info_field(  array(
                        'desc_tip' => 'true',
                        'description' => "",
                        'class' => 'noticed_info',
                        'text' => sprintf(__('The module %s does NOT have any instances OR the existed instances are NOT set the featured image.','apollo'), Apollo_App::formatModuleTitle($module )),
                    ) );
                    echo '</div>';
                } else {
                    echo '<div class="options_group event-box">';
                    apollo_wp_text_input(array(
                        'id' => 'number_images_' . $module,
                        'label' => __('The Number of images', 'apollo'),
                        'value' => Apollo_Admin_Photo_Slider_Meta_Boxes::getDataByKey($thepostid, 'number_images_' . $module , 4),
                        'class' => 'apollo_input_number'
                    ));
                    echo '</div>';
                }

            }
        }

        echo '</div>';

        echo '<br/>';

        echo '<div class="options_group clear wrap-blk ct-full-width">';

        echo '<div class="options_group clear event-box ps-box-label-second">';
        // Account ID
        apollo_wp_info_field(  array(
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'noticed_info',
            'text' => '<h2>'. __('Photo Slider Settings ', 'apollo').'</h2>'
        ) );
        echo '</div>';
            echo '<div class="options_group clear ct-selection-box">';
            apollo_wp_radio(  array(
                'id' => APL_Photo_Slider_Const::ENABLE_TITLE_SLIDER,
                'label' => __( 'Enable display title slider', 'apollo' ),
                'class' => '',
                'value' => Apollo_Admin_Photo_Slider_Meta_Boxes::getDataByKey($thepostid,APL_Photo_Slider_Const::ENABLE_TITLE_SLIDER, 'OFF'),
                'options' => array('ON' => 'ON',
                    'OFF' => 'OFF'
                )
            ) );
            echo '</div>';

            echo '<div class="options_group clear ct-selection-box">';
            apollo_wp_radio(  array(
                'id' => APL_Photo_Slider_Const::ENABLE_DESC_SLIDER,
                'label' => __( 'Enable display description slider', 'apollo' ),
                'class' => '',
                'value' => Apollo_Admin_Photo_Slider_Meta_Boxes::getDataByKey($thepostid,APL_Photo_Slider_Const::ENABLE_DESC_SLIDER, 'OFF'),
                'options' => array('ON' => 'ON',
                    'OFF' => 'OFF'
                )
            ) );
            echo '</div>';

            echo '<div class="options_group clear ct-selection-box">';
            apollo_wp_radio(  array(
                'id' => APL_Photo_Slider_Const::ENABLE_TITLE_SLIDER_ITEMS,
                'label' => __( 'Enable display title slider items', 'apollo' ),
                'class' => '',
                'value' => Apollo_Admin_Photo_Slider_Meta_Boxes::getDataByKey($thepostid,APL_Photo_Slider_Const::ENABLE_TITLE_SLIDER_ITEMS, 'OFF'),
                'options' => array('ON' => 'ON',
                    'OFF' => 'OFF'
                )
            ) );
            echo '</div>';
        echo '</div>';



    }

    /**
     * Save meta box data
     */
    public static function save( $post_id, $post ) {
        global $post, $wpdb;
        // Checks save status
        $is_autosave = wp_is_post_autosave( $post_id );
        $is_revision = wp_is_post_revision( $post_id );
        $is_valid_nonce = ( isset( $_POST[ 'apollo_photo-slider_meta_nonce' ] ) && wp_verify_nonce( $_POST[ 'apollo_photo-slider_meta_nonce' ], 'apollo_photo-slider_meta_nonce' ) ) ? true : false;

        // Exits script depending on save status
        if ( $is_autosave || $is_revision || ! $is_valid_nonce ) {
            return;
        }

        // Checks for input $k_choicand sanitizes/saves if needed

        $available_modules = Apollo_App::getModulesForPhotoSlider();

        if( !empty($available_modules)) {
            foreach ($available_modules as $module => $index) {
                $data = Apollo_App::getDataFromModulePulled($module, $post_id, true);
                if ( !empty($data) && count($data)  > 0 ) {
                    if (isset($_POST['enable_images_' . $module])) {
                        update_apollo_meta($post_id, 'enable_images_' . $module, $_POST['enable_images_' . $module]);
                    }
                    if (isset($_POST['number_images_' . $module])) {
                        update_apollo_meta($post_id, 'number_images_' . $module, $_POST['number_images_' . $module]);
                    }
                }
            }
        }

        if( isset( $_POST[ APL_Photo_Slider_Const::ENABLE_TITLE_SLIDER ] ) ) {
            update_apollo_meta( $post_id, APL_Photo_Slider_Const::ENABLE_TITLE_SLIDER , $_POST[ APL_Photo_Slider_Const::ENABLE_TITLE_SLIDER ]  );
        }
        if( isset( $_POST[ APL_Photo_Slider_Const::ENABLE_DESC_SLIDER ] ) ) {
            update_apollo_meta( $post_id, APL_Photo_Slider_Const::ENABLE_DESC_SLIDER ,  $_POST[ APL_Photo_Slider_Const::ENABLE_DESC_SLIDER ]  );
        }

        if( isset( $_POST[ APL_Photo_Slider_Const::ENABLE_TITLE_SLIDER_ITEMS ] ) ) {
            update_apollo_meta( $post_id, APL_Photo_Slider_Const::ENABLE_TITLE_SLIDER_ITEMS, $_POST[ APL_Photo_Slider_Const::ENABLE_TITLE_SLIDER_ITEMS ] );
        }


    }
}
