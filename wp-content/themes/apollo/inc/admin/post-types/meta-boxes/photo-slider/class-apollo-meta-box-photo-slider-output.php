<?php
/**
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Photo_Slider_Output
 */
class Apollo_Meta_Box_Photo_Slider_Output {


    /**
     * Output the metabox
     */
    public static function output( $post ) {
        global $post, $thepostid, $wpdb;
        wp_nonce_field( 'apollo_photo-slider_meta_nonce', 'apollo_photo-slider_meta_nonce' );
        echo '<div class="options_group clear ct-selection-box">';
        apollo_wp_info_field(  array(
            'id' => APL_Photo_Slider_Const::PHOTO_SLIDER_SHORT_CODE,
            'label' => __( 'Short Code', 'apollo' ),
            'text' => sprintf('[apollo_photo_slider photo_slider_id=%s]',$thepostid ),
        ) );
        echo '</div>';

    }


    /**
     * Save meta box data
     */
    public static function save( $post_id, $post ) {
        global $post, $wpdb;

        // Checks save status
        $is_autosave = wp_is_post_autosave( $post_id );
        $is_revision = wp_is_post_revision( $post_id );
        $is_valid_nonce = ( isset( $_POST[ 'apollo_photo-slider_meta_nonce' ] ) && wp_verify_nonce( $_POST[ 'apollo_photo-slider_meta_nonce' ], 'apollo_photo-slider_meta_nonce' ) ) ? true : false;

        // Exits script depending on save status
        if ( $is_autosave || $is_revision || ! $is_valid_nonce ) {
            return;
        }
        if ($post_id){
            update_apollo_meta( $post_id, APL_Photo_Slider_Const::PHOTO_SLIDER_SHORT_CODE, sprintf('[apollo_photo_slider photo_slider_id=%s]',$post_id ));

        }


    }

}
