<?php
/**
 * Apollo Classified Meta Boxes
 * @author 		Hong
 * @category 	Admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

include 'class-apl-ad-module-meta-boxes.php';

/**
 * Apollo_Admin_Classified_Meta_Boxes
 */
class Apollo_Admin_Classified_Meta_Boxes extends Apl_Ad_Module_Meta_Boxes {

    /**
     * Constructor
     */
    public function __construct() {

        parent::__construct( Apollo_DB_Schema::_CLASSIFIED );

        $this->require_meta_boxes();

        add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 30 );

        add_action( 'save_post', array( $this, 'save_meta_boxes' ), 1, 2 );

        // Save Classified Meta Boxes
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Classified_Data::save', 10, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Classified_Address::save', 10, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Classified_Images::save', 10, 2 );

        /*@ticket #16639:  Upload PDF files to the FE and Admin Classified forms.*/
        add_action( $this->process_data_action, 'Apollo_Meta_Box_UploadPDF::save', 20, 2 );

        // Error handling (for showing errors from meta boxes on next page load)
        add_action( 'admin_notices', array( $this, 'output_errors' ) );
        add_action( 'shutdown', array( $this, 'save_errors' ) );

    }

    public function require_meta_boxes() {
        require_once 'classified/class-apollo-meta-box-classified-address.php';
        require_once 'classified/class-apollo-meta-box-classified-data.php';
        require_once 'classified/class-apollo-meta-box-classified-images.php';
    }

    /**
     * Add Apollo Meta boxes
     */
    public function add_meta_boxes() {
        $enable_pbm = apply_filters('apl_pbm_module_active', null);

        add_meta_box( 'apollo-classified-address', __( 'Address', 'apollo' ),
            'Apollo_Meta_Box_Classified_Address::output', $this->type, 'normal' );

        add_meta_box( 'apollo-classified-data-top', __( 'Classified Data', 'apollo' ),
            'Apollo_Meta_Box_Classified_Data::output', $this->type, 'normal' );

        add_meta_box( 'apollo-classified-data-link', __( 'Own website and/or blog?', 'apollo' ),
            'Apollo_Meta_Box_Classified_Data::outputLink', $this->type, 'normal' );

        add_meta_box( 'apollo-classified-data-contact', __( 'Contact info', 'apollo' ),
            'Apollo_Meta_Box_Classified_Data::outputContactInfo', $this->type, 'normal' );

        add_meta_box( 'apollo-classified-organization', __( 'Classified Organization ', 'apollo' ),
            'Apollo_Meta_Box_Classified_Data::outputOrganization', $this->type, 'side' );

        add_meta_box( 'apollo-artist-customfield', __( 'More Info', 'apollo' ),
            'Apollo_Meta_Box_Customfield::output', Apollo_DB_Schema::_CLASSIFIED, 'normal' );

        add_meta_box( 'apollo-icons', __( 'Icons', 'apollo' ),
            'Apollo_Meta_Box_Common::output_icons', $this->type, 'normal','low' );

        add_meta_box( 'apollo-classified-gallery', __( 'Classified gallery ', 'apollo' ),
            'Apollo_Meta_Box_Classified_Images::output', $this->type, $enable_pbm ? 'normal' : 'side');

        /*@ticket #16639:  Upload PDF files to the FE and Admin Classified forms.*/
        add_meta_box( 'apollo-classified-uploadpdf', __( 'Documents (PDF)', 'apollo' ),
            'Apollo_Meta_Box_UploadPDF::output', Apollo_DB_Schema::_CLASSIFIED, 'normal' );

        /**
         * @ticket #19641: [CF] 20190402 - [Classified] Add the Associated User(s) field to main admin list and detail form - Item 5
         */
        add_meta_box( 'apollo-classified-user', __( 'Associated User(s)', 'apollo' ),
            'Apollo_Meta_Box_Common::output_associated_users', Apollo_DB_Schema::_CLASSIFIED, 'side' );


    }
}

new Apollo_Admin_Classified_Meta_Boxes();
