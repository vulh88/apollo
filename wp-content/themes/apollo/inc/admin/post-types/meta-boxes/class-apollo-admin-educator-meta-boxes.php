<?php
/**
 * Apollo Educator Meta Boxes
 *
 * @author 		Elisoft
 * @category 	Admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

include 'class-apl-ad-module-meta-boxes.php';

/**
 * Apollo_Admin_Educator_Meta_Boxes
 */
class Apollo_Admin_Educator_Meta_Boxes extends Apl_Ad_Module_Meta_Boxes {
    
	/**
	 * Constructor
	 */
	public function __construct() {
     
        parent::__construct( Apollo_DB_Schema::_EDUCATOR_PT );
        
        $this->require_meta_boxes();
        
		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 30 );

		add_action( 'save_post', array( $this, 'save_meta_boxes' ), 1, 2 );
        
        // Save Meta Boxes
		add_action( $this->process_data_action, 'Apollo_Meta_Box_Educator_Data::save', 10, 2 );
        
        // Error handling (for showing errors from meta boxes on next page load)
		add_action( 'admin_notices', array( $this, 'output_errors' ) );
        add_action( 'shutdown', array( $this, 'save_errors' ) );
        
	}
    
    public function require_meta_boxes() {
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/educator/class-apollo-meta-box-educator-data.php';
    }

	/**
	 * Add Apollo Meta boxes
	 */
	public function add_meta_boxes() {
        add_meta_box( 'apollo-educator-data', __( 'Educator Data', 'apollo' ), 
            'Apollo_Meta_Box_Educator_Data::output', $this->type, 'normal' );
        
        add_meta_box( 'apollo-icons', __( 'Icons', 'apollo' ), 
            'Apollo_Meta_Box_Common::output_icons', $this->type, 'normal' );
        
        add_meta_box( 'apollo-educator-social', __( 'Social Network URL', 'apollo' ), 
            'Apollo_Meta_Box_Educator_Data::output_social_network', $this->type, 'normal' );
        
        add_meta_box( 'apollo-educator-program', __( 'Associated Program(s)', 'apollo' ), 
            'Apollo_Meta_Box_Educator_Data::output_programs', $this->type, 'normal' );
        
        add_meta_box( 'apollo-educator-customfield', __( 'More Info', 'apollo' ), 
            'Apollo_Meta_Box_Customfield::output', $this->type, 'normal' );
        
        add_meta_box( 'apollo-educator-user', __( 'Associated User(s)', 'apollo' ), 
            'Apollo_Meta_Box_Common::output_associated_users', $this->type, 'side' );

        if (Apollo_App::is_avaiable_module(Apollo_DB_Schema::_AGENCY_PT)) {
            add_meta_box('apollo-educator-agency', __('Associated Agencies', 'apollo'),
                'Apollo_Meta_Box_Common::output_associated_agencies', $this->type, 'side');
        }
	}
}

new Apollo_Admin_Educator_Meta_Boxes();
