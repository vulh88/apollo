<?php
/**
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Educator_Data
 */
class Apollo_Meta_Box_Educator_Data {

	/**
	 * Output the metabox
	 */
	public static function output( $post ) { 
        
        global $post, $thepostid;
		wp_nonce_field( 'apollo_educator_meta_nonce', 'apollo_educator_meta_nonce' );
        $thepostid = $post->ID;
        $stateVal = Apollo_App::apollo_get_meta_data( $thepostid, Apollo_DB_Schema::_APL_EDUCATOR_DATA .'['. Apollo_DB_Schema::_APL_EDUCATOR_STATE .']' );
        $cityVal = Apollo_App::apollo_get_meta_data( $thepostid, Apollo_DB_Schema::_APL_EDUCATOR_DATA .'['. Apollo_DB_Schema::_APL_EDUCATOR_CITY .']' );

        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_EDUCATOR_DATA .'['. Apollo_DB_Schema::_APL_EDUCATOR_ADD1 .']', 
                'label' => __( 'Address 1', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_EDUCATOR_DATA .'['. Apollo_DB_Schema::_APL_EDUCATOR_ADD2 .']', 
                'label' => __( 'Address 2', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
            ) );
        echo '</div>';

        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_select(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_EDUCATOR_DATA .'['. Apollo_DB_Schema::_APL_EDUCATOR_STATE .']', 
                'label' => __( 'State', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'options'   => Apollo_App::getStateByTerritory(),
                'class' => 'apl-territory-state',
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_select(  array(
                'id' => ''. Apollo_DB_Schema::_APL_EDUCATOR_DATA .'['. Apollo_DB_Schema::_APL_EDUCATOR_CITY .']',
                'label' => __( 'City', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'options'   => Apollo_App::getCityByTerritory(FALSE, $stateVal, true),
                'class' => 'apl-territory-city',
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_select(  array(
                'id' => ''. Apollo_DB_Schema::_APL_EDUCATOR_DATA .'['. Apollo_DB_Schema::_APL_EDUCATOR_ZIP .']',
                'label' => __( 'Zip', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'options'   => Apollo_App::getZipByTerritory(FALSE, $stateVal, $cityVal, true),
                'class' => 'apl-territory-zipcode',
            ) );
        echo '</div>';

        /** @Ticket #19640 */
        echo '<div class="tmp-address-wrapper" >';

        echo '<h4>'.__( 'Other Address', 'apollo' ) .'</h4>';

        echo '<div class="options_group event-box">';
        // Description
        apollo_wp_text_input(  array(
            'id' => ''. Apollo_DB_Schema::_APL_EDUCATOR_DATA .'['. Apollo_DB_Schema::_APL_EDUCATOR_TMP_STATE .']',
            'label' => __( 'Other State', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
        ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
        // Description
        apollo_wp_text_input(  array(
            'id' => ''. Apollo_DB_Schema::_APL_EDUCATOR_DATA .'['. Apollo_DB_Schema::_APL_EDUCATOR_TMP_CITY .']',
            'label' => __( 'Other City', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
        ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
        // Description
        apollo_wp_text_input(  array(
            'id' => ''. Apollo_DB_Schema::_APL_EDUCATOR_DATA .'['. Apollo_DB_Schema::_APL_EDUCATOR_TMP_ZIP .']',
            'label' => __( 'Other Zip Code', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
        ) );
        echo '</div>';
        echo '</div>';

        // region
        echo '<div class="options_group event-box">';
        apollo_wp_select(  array(
            'id' => ''. Apollo_DB_Schema::_APL_EDUCATOR_DATA .'['. Apollo_DB_Schema::_APL_EDUCATOR_REGION .']',
            'label' => __( 'Region', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'options'   => Apollo_App::get_regions(),
            'display' => Apollo_App::showRegion(),
        ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_EDUCATOR_DATA .'['. Apollo_DB_Schema::_APL_EDUCATOR_COUNTY .']', 
                'label' => __( 'County', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
            ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_EDUCATOR_DATA .'['. Apollo_DB_Schema::_APL_EDUCATOR_PHONE1 .']', 
                'label' => __( 'Phone', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_EDUCATOR_DATA .'['. Apollo_DB_Schema::_APL_EDUCATOR_FAX .']', 
                'label' => __( 'Fax', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_EDUCATOR_DATA .'['. Apollo_DB_Schema::_APL_EDUCATOR_EMAIL .']', 
                'label' => __( 'Email', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'class' => 'apollo_input_email',
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_EDUCATOR_DATA .'['. Apollo_DB_Schema::_APL_EDUCATOR_URL .']', 
                'label' => __( 'Website', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'class' => 'apollo_input_url',
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_EDUCATOR_DATA .'['. Apollo_DB_Schema::_APL_EDUCATOR_BLOG .']', 
                'label' => __( 'Blog URL', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'class' => 'apollo_input_url',
            ) );
        echo '</div>';
        
	}

    public static function output_programs( $post ) {
        global $post, $thepostid;
		wp_nonce_field( 'apollo_educator_meta_nonce', 'apollo_educator_meta_nonce' );
        $thepostid = $post->ID;
        
        // Get selected programs
        $edu = get_educator( $post );
        $selected_programs = $edu->get_programs();
      
        ?>
        <div class="postbox apl-custom-postbox venues">
            
            <div id="message-educator" class="error below-h2 hidden">
                <p><?php echo _e( 'There are some educators are not published.', 'apollo' )  ?></p>
            </div>
            
            <div class=' section wrap-multiselect'>
                <?php
                    $selected_ul_str = '';
                    $has_not_published = false;
                    foreach ( $selected_programs as $pe ):
                        
                        if ( ! $pe->prog_id ) continue;
                        
                        $pv = get_post($pe->prog_id);
                        if ( ! $pv ) continue;
                        $in_trash = $pv->post_status != 'publish' ? 'style="color: #dd3d36"' : '';
                        $selected_ul_str .= '<li data-value="'. $pv->ID .'"><a '.$in_trash.' href="'.  get_edit_post_link( $pv->ID, true ).'">'. $pv->post_title .'</a><i class="fa fa-remove"></i></li>';
                        if ( ! $has_not_published && $in_trash ) $has_not_published = true;
                    endforeach;
                
                    $programs = get_posts( array( 
                        'post_type'         => Apollo_DB_Schema::_PROGRAM_PT, 
                        'post_status'       => 'publish', 
                        'posts_per_page'    => -1,
                        'orderby'           => 'post_title', 
                        'order'             => 'ASC'
                    ) );
                    if ( $programs ):
              
                if ( $has_not_published ): ?>    
                
                <script>
                    jQuery( function() {
                        var _elm = '#message-educator';
                        jQuery( _elm ).show();
                        jQuery( _elm ).parent().css('height', '415px');
                    });
                </script>
                
                <?php endif; ?>
                
                <select name="programs[]" class="hidden hidden-multiselect" multiple="multiple" >
                    <?php 
                        $opts_str = '';
                        foreach ( $programs as $v ):
                            $prog = get_program( $v->ID );
                            
                            if ( $prog->has_educator() ) {
                                $select_op = 'selected="selected"';
                                $style = 'style="opacity: 0.3;"';
                            } else {
                                $select_op = '';
                                $style = '';;
                            }
                            $opts_str .= '<option data-allow="'.$allow.'" data-url="'.get_edit_post_link( $v->ID ).'" '.$style.' '.$select_op.' class="level-0" value="'.$v->ID.'">'.$v->post_title.'</option>';
                        endforeach;
                        echo $opts_str
                    ?>
                </select>
                    <?php 
                endif; ?>
                <select class="multiselect" multiple="multiple">
                    <?php echo $opts_str ?>
                </select>
                    <button type="button" class="add-multi-select-btn button"><?php _e( 'Add', 'apollo' ); ?></button>
                <div class="selected-event">
                    <ol><?php echo $selected_ul_str; ?></ol>
                </div>
            </div> 
        </div>
        <?php
    }
    
    /**
	 * Output the metabox
	 */
	public static function output_social_network( $post ) { 
        
        global $post, $thepostid;
		wp_nonce_field( 'apollo_educator_meta_nonce', 'apollo_educator_meta_nonce' );
        $thepostid = $post->ID;
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_EDUCATOR_DATA .'['. Apollo_DB_Schema::_APL_EDUCATOR_FB .']', 
                'label' => __( 'Facebook', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'class' => 'apollo_input_url',
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_EDUCATOR_DATA .'['. Apollo_DB_Schema::_APL_EDUCATOR_TW .']', 
                'label' => __( 'Twitter', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'class' => 'apollo_input_url',
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_EDUCATOR_DATA .'['. Apollo_DB_Schema::_APL_EDUCATOR_YT .']', 
                'label' => __( 'Youtube', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'class' => 'apollo_input_url',
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_EDUCATOR_DATA .'['. Apollo_DB_Schema::_APL_EDUCATOR_INS .']', 
                'label' => __( 'Instagram', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'class' => 'apollo_input_url',
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_EDUCATOR_DATA .'['. Apollo_DB_Schema::_APL_EDUCATOR_LK .']', 
                'label' => __( 'LinkedIn', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'class' => 'apollo_input_url',
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_EDUCATOR_DATA .'['. Apollo_DB_Schema::_APL_EDUCATOR_PTR .']', 
                'label' => __( 'Pinterest', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'class' => 'apollo_input_url',
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_EDUCATOR_DATA .'['. Apollo_DB_Schema::_APL_EDUCATOR_FLI .']', 
                'label' => __( 'Flickr', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'class' => 'apollo_input_url',
            ) );
        echo '</div>';
        
	}
    
    /**
	 * Save meta box data
	 */
	public static function save( $post_id, $post ) {
        
        $_custom_data_search = isset( $_POST[Apollo_DB_Schema::_APL_EDUCATOR_CUSTOM_FIELD_SEARCH] ) ? $_POST[Apollo_DB_Schema::_APL_EDUCATOR_CUSTOM_FIELD_SEARCH] : array();
        $_data = $_POST[Apollo_DB_Schema::_APL_EDUCATOR_DATA];
        if(!isset($_data[Apollo_DB_Schema::_APL_EDUCATOR_REGION])){
            $_data[Apollo_DB_Schema::_APL_EDUCATOR_REGION] = Apollo_App::apollo_get_meta_data($post_id, Apollo_DB_Schema::_APL_EDUCATOR_REGION, Apollo_DB_Schema::_APL_EDUCATOR_DATA );
        }
        $email_field = Apollo_DB_Schema::_APL_EDUCATOR_EMAIL;
        if ( isset( $_data[$email_field] ) && $_data[$email_field] ):
            if ( is_email( $_data[$email_field] ) ) {
                $_data[$email_field] = Apollo_App::clean_data_request( $_data[$email_field] );
            } 
        endif;
        
        $url_check = array(
            Apollo_DB_Schema::_APL_EDUCATOR_URL => __( 'Website', 'apollo' ),
        );
        
        foreach( $url_check as $url_field => $l ) {
            if ( isset( $_data[$url_field] ) && $_data[$url_field] ):
                if ( filter_var( $_data[$url_field], FILTER_VALIDATE_URL ) ) {
                    $_data[$url_field] = Apollo_App::clean_data_request( $_data[$url_field] );
                }
            endif;
        }
        
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_EDUCATOR_DATA, serialize( Apollo_App::clean_array_data(array_merge( $_data, $_custom_data_search ) ) ) );
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_EDUCATOR_CUSTOM_FIELD_SEARCH, serialize( Apollo_App::clean_array_data( $_custom_data_search ) ) );

        /** @Ticket #13028 */
        update_apollo_meta($post_id, Apollo_DB_Schema::_APL_EDUCATOR_CITY, !empty($_data[Apollo_DB_Schema::_APL_EDUCATOR_CITY]) ? $_data[Apollo_DB_Schema::_APL_EDUCATOR_CITY] : '');
        update_apollo_meta($post_id, Apollo_DB_Schema::_APL_EDUCATOR_STATE, !empty($_data[Apollo_DB_Schema::_APL_EDUCATOR_STATE]) ? $_data[Apollo_DB_Schema::_APL_EDUCATOR_STATE] : '');
        update_apollo_meta($post_id, Apollo_DB_Schema::_APL_EDUCATOR_ZIP, !empty($_data[Apollo_DB_Schema::_APL_EDUCATOR_ZIP]) ? $_data[Apollo_DB_Schema::_APL_EDUCATOR_ZIP] : '');

        /**
         * Save selected programs
         */ 
        $programs = isset( $_POST['programs'] ) ? $_POST['programs'] : '';
        $apl_query = new Apl_Query( Apollo_Tables::_APL_PROGRAM_EDUCATOR );
        $apl_query->delete( "edu_id = $post_id" );
        if ( $programs ) {
            foreach ( $programs as $prog_id ) {
                $prog = get_program( $prog_id );
                if ( $prog->has_educator() ) continue;
                $apl_query->insert( array( 'prog_id' => $prog_id, 'edu_id'  => $post_id ) );
            }
        }
    }    
}
