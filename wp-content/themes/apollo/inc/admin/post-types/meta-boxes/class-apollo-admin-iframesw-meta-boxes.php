<?php
/**
 * Apollo Artist Meta Boxes
 *
 * Sets up the write panels used by venues (custom post types)
 *
 * @author 		vulh
 * @category 	Admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

include 'class-apl-ad-module-meta-boxes.php';

/**
 * Apollo_Admin_Iframe_Search_Widget_Meta_Boxes
 */
class Apollo_Admin_Iframe_Search_Widget_Meta_Boxes extends Apl_Ad_Module_Meta_Boxes {

    /**
     * Constructor
     */
    public function __construct() {

        parent::__construct( Apollo_DB_Schema::_IFRAME_SEARCH_WIDGET_PT );

        $this->require_meta_boxes();

        add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 30 );
        add_action( 'save_post', array( $this, 'save_meta_boxes' ), 1, 2 );

        // Save Meta Boxes
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Iframe_Search_Widget_Design::save', 10, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Iframe_Search_Widget_Setting::save', 10, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Iframe_Search_Widget_Output::save', 10, 2 );

        // Error handling (for showing errors from meta boxes on next page load)
        add_action( 'admin_notices', array( $this, 'output_errors' ) );
        add_action( 'shutdown', array( $this, 'save_errors' ) );

    }

    public static function getIFrameSearchWidgetDataByKey($post_id, $key, $default = ''){
        $val = Apollo_App::apollo_get_meta_data( $post_id, $key );
        $val = is_string($val) ? trim($val) : $val;
        $return  = $val && !empty($val) ? $val : $default;
        return $return;
    }

    public function require_meta_boxes() {
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/iframe-search-widget/class-apollo-meta-box-iframe-search-widget-design.php';
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/iframe-search-widget/class-apollo-meta-box-iframe-search-widget-setting.php';
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/iframe-search-widget/class-apollo-meta-box-iframe-search-widget-output.php';
    }

    /**
     * Add Apollo Meta boxes
     */
    public function add_meta_boxes() {
        add_meta_box( 'apollo-iframe-search-widget-design', __( 'Design', 'apollo' ),
            'Apollo_Meta_Box_Iframe_Search_Widget_Design::output', Apollo_DB_Schema::_IFRAME_SEARCH_WIDGET_PT, 'normal' );

        add_meta_box( 'apollo-iframe-search-widget-setting', __( ' Settings', 'apollo' ),
            'Apollo_Meta_Box_Iframe_Search_Widget_Setting::output', Apollo_DB_Schema::_IFRAME_SEARCH_WIDGET_PT, 'normal' );

        add_meta_box( 'apollo-iframe-search-widget-output', __( ' Results', 'apollo' ),
            'Apollo_Meta_Box_Iframe_Search_Widget_Output::output', Apollo_DB_Schema::_IFRAME_SEARCH_WIDGET_PT, 'normal' );
    }
}

new Apollo_Admin_Iframe_Search_Widget_Meta_Boxes();
