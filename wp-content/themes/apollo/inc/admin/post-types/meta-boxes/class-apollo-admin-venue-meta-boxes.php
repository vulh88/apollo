<?php
/**
 * Apollo Venue Meta Boxes
 *
 * Sets up the write panels used by venues (custom post types)
 *
 * @author 		vulh
 * @category 	Admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

include 'class-apl-ad-module-meta-boxes.php';

/**
 * Apollo_Admin_Venue_Meta_Boxes
 */
class Apollo_Admin_Venue_Meta_Boxes extends Apl_Ad_Module_Meta_Boxes {

    /**
     * Constructor
     */
    public function __construct() {

        parent::__construct( Apollo_DB_Schema::_VENUE_PT );

        $this->require_meta_boxes();

        add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 30 );

        add_action( 'save_post', array( $this, 'save_meta_boxes' ), 1, 2 );

        // Save Venue Meta Boxes
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Venue_Data::save', 10, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Venue_Address::save', 10, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Venue_Images::save', 10, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Venue_Data_Video::save', 10, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Common::updateChangeAuthor', 10, 2 );

        // Error handling (for showing errors from meta boxes on next page load)
        add_action( 'admin_notices', array( $this, 'output_errors' ) );
        add_action( 'shutdown', array( $this, 'save_errors' ) );

        add_action('apollo_copy_to_mini_sites', array( $this, 'apollo_copy_to_mini_sites' ), 10, 1);
    }

    public static function apollo_copy_to_mini_sites($post_id)
    {
        global $apollo_mini_submit_sites;

        if ($apollo_mini_submit_sites) {

            foreach($apollo_mini_submit_sites as $siteId => $miniConfigPostId){
                $site_item = Apollo_Venue_Copy_To_Mini_Site::getMiniPostByParentPost($post_id, $siteId);
                $site_item = count($site_item) ? $site_item[0] : [];

                if($site_item->mini_site_id && $site_item->mini_post_id && $miniConfigPostId){
                    $parent_gallery_ids = get_apollo_meta($post_id, Apollo_DB_Schema::_APL_VENUE_IMAGE_GALLERY);
                    $parent_gallery_ids = $parent_gallery_ids ? explode(',', $parent_gallery_ids[0]) : '';
                    $metaValuesVenue = get_meta_data_venue($post_id);

                    if(switch_to_blog($site_item->mini_site_id)){
                        Apollo_Mini_Site_App::insertIclTranslations($site_item->mini_post_id, 'venue');
                        Apollo_Mini_Site_App::copyMetaData($site_item->mini_post_id, $metaValuesVenue);
                        restore_current_blog();
                    }

                    Apollo_Mini_Site_App::updateAttachment($site_item->mini_site_id, $post_id, $site_item->mini_post_id);

                    Apollo_Mini_Site_App::updateGalleryImagesToMiniSite($site_item->mini_post_id, $parent_gallery_ids, $site_item->mini_site_id, Apollo_DB_Schema::_APL_VENUE_IMAGE_GALLERY);
                    Apollo_Mini_Site_App::copyCategoryToMiniSite($post_id, $site_item->mini_site_id, $site_item->mini_post_id, $miniConfigPostId , 'venue-type', 'venue-meta-catsel', 'venue-meta-type');
                }
            }
        }
    }

    public function require_meta_boxes() {
        require_once 'venue/class-apollo-meta-box-venue-address.php';
        require_once 'venue/class-apollo-meta-box-venue-data.php';
        require_once 'venue/class-apollo-meta-box-venue-images.php';
        require_once 'venue/class-apollo-meta-box-venue-data-video.php';
    }

    /**
     * Add Apollo Meta boxes
     */
    public function add_meta_boxes() {

        add_meta_box( 'apollo-venue-address', __( 'Address', 'apollo' ),
            'Apollo_Meta_Box_Venue_Address::output', $this->type, 'normal' );

        add_meta_box( 'apollo-venue-data-top', __( 'Venue Data', 'apollo' ),
            'Apollo_Meta_Box_Venue_Data::output', $this->type, 'normal' );

        add_meta_box( 'apollo-venue-data-link', __( 'Own website and/or blog?', 'apollo' ),
            'Apollo_Meta_Box_Venue_Data::outputLink', $this->type, 'normal' );

        add_meta_box( 'apollo-venue-data-contact', __( 'Contact info', 'apollo' ),
            'Apollo_Meta_Box_Venue_Data::outputContactInfo', $this->type, 'normal' );

        add_meta_box( 'apollo-venue-acb', __( 'Accessibility Info', 'apollo' ),
            'Apollo_Meta_Box_Venue_Data::output_acb', $this->type, 'normal' );

        add_meta_box( 'apollo-event-data', __( 'Venue videos', 'apollo' ),
            'Apollo_Meta_Box_Venue_Data_Video::output', $this->type, 'normal' );

        add_meta_box( 'apollo-venue-user', __( 'Associated User(s)', 'apollo' ),
            'Apollo_Meta_Box_Common::output_associated_users', $this->type, 'side' );

        if (Apollo_App::is_avaiable_module(Apollo_DB_Schema::_AGENCY_PT)) {
            add_meta_box('apollo-venue-agency', __('Associated Agencies', 'apollo'),
                'Apollo_Meta_Box_Common::output_associated_agencies', $this->type, 'side');
        }

        add_meta_box( 'apollo-artist-customfield', __( 'More Info', 'apollo' ),
            'Apollo_Meta_Box_Customfield::output', Apollo_DB_Schema::_VENUE_PT, 'normal' );

        add_meta_box( 'apollo-icons', __( 'Icons', 'apollo' ),
            'Apollo_Meta_Box_Common::output_icons', $this->type, 'normal','low' );

        add_meta_box( 'apollo-venue-gallery', __( 'Venue  gallery ', 'apollo' ),
            'Apollo_Meta_Box_Venue_Images::output', $this->type, 'side' );

        /** @Ticket #16209 */
        $currentUser = get_current_user_id();
        if (is_super_admin($currentUser) || user_can($currentUser, 'manage_options')) {
            add_meta_box( 'apollo-venue-author', __( 'Change Author', 'apollo' ),
                'Apollo_Meta_Box_Common::outputChangeAuthor', $this->type, 'side' );
        }
    }
}

new Apollo_Admin_Venue_Meta_Boxes();
