<?php
/**
 * Apollo Event Meta Boxes
 *
 * Sets up the write panels used by events (custom post types)
 *
 * @author 		vulh
 * @category 	Admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

include 'class-apl-ad-module-meta-boxes.php';

/**
 * Apollo_Admin_Event_Meta_Boxes
 */
class Apollo_Admin_Event_Meta_Boxes extends Apl_Ad_Module_Meta_Boxes {
    
	/**
	 * Constructor
	 */
	public function __construct() {
        parent::__construct( Apollo_DB_Schema::_EVENT_PT );
//        $this->type = Apollo_DB_Schema::_EVENT_PT;
        $this->require_meta_boxes();

		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 30 );
		add_action( 'save_post', array( $this, 'save_meta_boxes' ), 1, 2 );
        
        // Save Event Meta Boxes
       
		add_action( 'apollo_process_event_meta', 'Apollo_Meta_Box_Event_Data::save', 10, 2 );
        add_action( 'apollo_process_event_meta', 'Apollo_Meta_Box_Event_URL::save', 10, 2 );
		add_action( 'apollo_process_event_meta', 'Apollo_Meta_Box_Event_Images::save', 20, 2 );
        add_action( 'apollo_process_event_meta', 'Apollo_Meta_Box_Primary_Category::save', 20, 2 );
        //add_action( 'apollo_process_event_meta', 'Apollo_Meta_Box_Home_Spotlight_Event_Image::save', 10, 2 );
        
        // Error handling (for showing errors from meta boxes on next page load)
		add_action( 'admin_notices', array( $this, 'output_errors' ) );
        add_action( 'shutdown', array( $this, 'save_errors' ) );

        /*@ticket #17235 - [Admin] Official tag for event and post */
        $currentTheme = function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '';
        if ( $currentTheme == 'octave-child') {
            add_action( 'apollo_process_event_meta', 'Apollo_Meta_Box_Official_Tags::save', 20, 2 );
            require_once APOLLO_ADMIN_DIR . '/post-types/meta-boxes/common/class-apollo-meta-box-official-tags.php';
        }

        // Add metabox modules action
        $this->add_meta_boxes_modules_action();
        
	}

    
    public function add_meta_boxes_modules_action() {
        
        $avaiable_modules = Apollo_App::get_avaiable_modules();
        
        
        add_action( 'apollo_process_event_meta', 'Apollo_Meta_Box_Event_Module::save_venue', 20, 2 );
        
        add_action( 'apollo_process_event_meta', 'Apollo_Meta_Box_Event_Module::save_organization', 20, 2 );
        
        if ( Apollo_App::is_avaiable_module( 'artist', $avaiable_modules ) ) {
            add_action( 'apollo_process_event_meta', 'Apollo_Meta_Box_Event_Module::save_artists', 20, 2 );
        }
        
    }
    
    public function require_meta_boxes() {
        require_once 'event/class-apollo-meta-box-primary-category.php';
        require_once 'event/class-apollo-meta-box-event-data.php';
        require_once 'event/class-apollo-meta-box-event-url.php';

        require_once 'event/class-apollo-meta-box-event-summary.php';
        require_once 'event/class-apollo-meta-box-event-images.php';
        
        require_once 'event/class-apollo-meta-box-event-module.php';
        require_once 'event/class-apollo-meta-box-home-spotlight-event.php';
    }
    
	/**
	 * Add Apollo Meta boxes
	 */
	public function add_meta_boxes() {
        $avaiable_modules = Apollo_App::get_avaiable_modules();
        
		// events
        add_meta_box( 'apollo-event-quick-search', __( 'Search Events', 'apollo' ), 'Apollo_Meta_Box_Event_Data::outputSearch', 'event', 'side', 'high' );

        add_meta_box( 'apollo-event-quick-display', __( 'Display', 'apollo' ), 'Apollo_Meta_Box_Event_Data::outputDisplay', 'event', 'side', 'high' );

        add_meta_box( 'postexcerpt', __( 'Summary', 'apollo' ), 'Apollo_Meta_Box_Event_Summary::output', 'event', 'normal', 'high' );
        add_meta_box( 'apollo-event-url', __( 'Event URL', 'apollo' ), 'Apollo_Meta_Box_Event_URL::output', 'event', 'normal', 'high' );
        
        add_meta_box( 'apollo-event-data-datetime', __( 'Dates/Times', 'apollo' ), 'Apollo_Meta_Box_Event_Data::output', 'event', 'normal', 'high' );
        add_meta_box( 'apollo-event-data-admiss', __( 'Admission', 'apollo' ), 'Apollo_Meta_Box_Event_Data::output_admiss', 'event', 'normal', 'high' );
        add_meta_box( 'apollo-event-data-video', __( 'Video', 'apollo' ), 'Apollo_Meta_Box_Event_Data::output_video', 'event', 'normal', 'high' );
        
        add_meta_box( 'apollo-event-contact-info', __( 'Contact Info', 'apollo' ), 'Apollo_Meta_Box_Event_Data::output_contact_info', 'event', 'normal', 'high' );
        add_meta_box( 'apollo-event-acb', __( 'Accessibility Info', 'apollo' ), 'Apollo_Meta_Box_Event_Data::output_acb', 'event', 'normal', 'high' );
        
        add_meta_box( 'apollo-event-venue', __( 'Venue', 'apollo' ), 'Apollo_Meta_Box_Event_Module::output_venue', 'event', 'normal' );
        add_meta_box( 'apollo-event-organization', __( 'Organization', 'apollo' ), 'Apollo_Meta_Box_Event_Module::output_organization', 'event', 'normal', 'high' );

        /** @Ticket - #14085 - Additional fields*/
        add_meta_box( 'apollo-event-customfield', __( 'More Info', 'apollo' ),
            'Apollo_Meta_Box_Customfield::output', Apollo_DB_Schema::_EVENT_PT, 'normal' );

        /*@ticket #17235 - [Admin] Official tag for event and post */
        $currentTheme = function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '';
        if ( $currentTheme == 'octave-child') {
            add_meta_box( 'apollo-event-official-tags', __( 'Official tags', 'apollo' ), 'Apollo_Meta_Box_Official_Tags::output', 'event', 'side' );
        }

        add_meta_box( 'apollo-event-primary-category', __( 'Primary Category', 'apollo' ), 'Apollo_Meta_Box_Primary_Category::output', 'event', 'side' );
        
		add_meta_box( 'apollo-event-images', __( 'Event Gallery', 'apollo' ), 'Apollo_Meta_Box_Event_Images::output', 'event', 'side' );
        
        if ( Apollo_App::is_avaiable_module( 'artist', $avaiable_modules ) ) {
            add_meta_box( 'apollo-event-artist', __( 'Artist', 'apollo' ), 'Apollo_Meta_Box_Event_Module::output_artist', 'event', 'side', 'default' );
        }
        add_meta_box( 'apollo-event-user', __( 'Associated User', 'apollo' ),'Apollo_Meta_Box_Event_Data::getUserAssociate', $this->type, 'side' );

        add_meta_box( 'apollo-icons', __( 'Icons', 'apollo' ),
            'Apollo_Meta_Box_Common::output_icons', $this->type, 'normal','low' );
	}

	/**
	 * Check if we're saving, the trigger an action based on the post type
	 *
	 * @param  int $post_id
	 * @param  object $post
	 */
	public function save_meta_boxes( $post_id, $post ) {
        
		// $post_id and $post are required
		if ( empty( $post_id ) || empty( $post ) ) {
			return;
		}

		// Dont' save meta boxes for revisions or autosaves
		if ( defined( 'DOING_AUTOSAVE' ) || is_int( wp_is_post_revision( $post ) ) || is_int( wp_is_post_autosave( $post ) ) ) {
			return;
		}
		
		// Check the nonce
		if ( empty( $_POST['apollo_event_meta_nonce'] ) || ! wp_verify_nonce( $_POST['apollo_event_meta_nonce'], 'apollo_event_meta_nonce' ) ) {
			return;
		} 
       
		// Check the post being saved == the $post_id to prevent triggering this call for other save_post events
		if ( empty( $_POST['post_ID'] ) || $_POST['post_ID'] != $post_id ) {
			return;
		}
 
		// Check user has permission to edit
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
        
		do_action( 'apollo_process_' . $post->post_type . '_meta', $post_id, $post );
	}
    
    public static function get_simple_data( $key, &$obj ) {
        
        if ( isset( $obj[$key] ) ) {
            $obj[$key] = Apollo_App::clean_data_request( $obj[$key] );
        }
    }
    
    public static function get_email_data( $key, &$obj, $field_name ) {
        
        if ( isset( $obj[$key] ) ) {
            $email = $obj[$key];
            if ( is_email( $email ) ) {
                $obj[$key] = Apollo_App::clean_data_request( $email );
            } else {
                if ( $email ) {
                    //Apollo_Admin_Event_Meta_Boxes::add_error(sprintf( __( '%s Email is invalid format', 'apollo' ), $field_name ) );
                }
                $obj[$key] = '';
            }
        }
    }
    
    public static function get_url_data( $key, &$obj, $field_name ) {
        
        if ( isset( $obj[$key] ) ) {
            $url = $obj[$key];
            if ( filter_var( $url, FILTER_VALIDATE_URL ) ) {
                $obj[$key] = esc_url_raw($url);
            } else {
                if ( $url ) {
                    //Apollo_Admin_Event_Meta_Boxes::add_error(sprintf( __( '%s URL is invalid format', 'apollo' ), $field_name ) );
                }
                $obj[$key] = '';
            }
            
        }
    }
}

new Apollo_Admin_Event_Meta_Boxes();
