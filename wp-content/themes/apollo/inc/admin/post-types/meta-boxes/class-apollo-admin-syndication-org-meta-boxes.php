<?php

if (!defined('ABSPATH')) {
    exit;
}

require_once 'class-apl-ad-module-meta-boxes.php';

/**
 * Apollo_Admin_Syndication_Organization_Meta_Boxes
 */
class Apollo_Admin_Syndication_Organization_Meta_Boxes extends Apl_Ad_Module_Meta_Boxes
{

    /**
     * Constructor
     */
    public function __construct()
    {

        parent::__construct(Apollo_DB_Schema::_SYNDICATION_ORG_PT);

        $this->require_meta_boxes();

        add_action('add_meta_boxes', array($this, 'add_meta_boxes'), 30);
        add_action('save_post', array($this, 'save_meta_boxes'), 1, 2);

        // Save Meta Boxes
        add_action($this->process_data_action, 'Apollo_Meta_Box_Syndication_Org_Info::save', 10, 2);
        add_action($this->process_data_action, 'Apollo_Meta_Box_Syndication_Org_Xml_Generation::save', 10, 2);


        // Error handling (for showing errors from meta boxes on next page load)
        add_action('admin_notices', array($this, 'output_errors'));
        add_action('shutdown', array($this, 'save_errors'));

    }

    public static function getMetaDataByKey($post_id, $key, $default = '')
    {
        if (isset($_POST[$key])) {
            return $_POST[$key];
        }
        $metaVal = get_post_meta($post_id, $key, true);
        if (is_serialized($metaVal)) {
            $metaVal = maybe_unserialize($metaVal);
        }

        return $metaVal != null && $metaVal != '' ? $metaVal : $default;

    }

    public function require_meta_boxes()
    {
        require_once APOLLO_ADMIN_DIR . '/post-types/meta-boxes/syndication/org/class-apollo-meta-box-syndication-org-info.php';
        require_once APOLLO_ADMIN_DIR . '/post-types/meta-boxes/syndication/org/class-apollo-meta-box-syndication-org-xml-generation.php';
    }

    /**
     * Add Apollo Meta boxes
     */
    public function add_meta_boxes()
    {
        add_meta_box('apollo-syndication-org-info', __('Syndication Org Info', 'apollo'),
            'Apollo_Meta_Box_Syndication_Org_Info::output', Apollo_DB_Schema::_SYNDICATION_ORG_PT, 'normal');

        add_meta_box('apollo-syndication-org-xml-generation', __('XML Generation', 'apollo'),
            'Apollo_Meta_Box_Syndication_Org_Xml_Generation::output', Apollo_DB_Schema::_SYNDICATION_ORG_PT, 'normal');

    }
}

new Apollo_Admin_Syndication_Organization_Meta_Boxes();
