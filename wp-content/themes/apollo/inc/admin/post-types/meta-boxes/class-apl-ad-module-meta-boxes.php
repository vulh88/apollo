<?php
/**
 * Apollo Admin Meta Boxes
 *
 * @author 		vulh
 * @category 	Admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Apl_Ad_Module_Meta_Boxes {
    protected $type;
    protected static $meta_box_errors = array();
    protected $process_data_action = '';
    protected $nonce_param  = '';


    public function __construct( $type ) {
        $this->type = $type;
        $this->process_data_action = 'apollo_process_'.$this->type.'_meta';
        $this->nonce_param = 'apollo_'.$this->type.'_meta_nonce';
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/common/class-apollo-meta-box-customfield.php';
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/common/class-apollo-meta-box-uploadpdf.php';
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/common/class-apollo-meta-box-common.php';
        
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Customfield::save', 20, 2 );
		add_action( 'add_meta_boxes', array( $this, 'remove_meta_boxes' ), 30 );
    }
    
    /**
	 * Add an error message
	 * @param string $text
	 */
	public static function add_error( $text ) {
		self::$meta_box_errors[] = $text;
	}


    /**
	 * Save errors to an option
	 */
	public function save_errors() {
		update_option( 'apollo_'.$this->type.'_meta_box_errors', self::$meta_box_errors, 'no' );
	}

	/**
	 * Show any stored error messages.
	 */
	public function output_errors() {
		$errors = maybe_unserialize( get_option( 'apollo_'.$this->type.'_meta_box_errors' ) );

		if ( ! empty( $errors ) ) {

			echo '<div id="apollo-'.$this->type.'-errors" class="error fade">';
			foreach ( $errors as $error ) {
				echo '<p>' . esc_html( $error ) . '</p>';
			}
			echo '</div>';

			// Clear
			delete_option( 'apollo_'.$this->type.'_meta_box_errors' );
		}
	}
    
    /**
	 * Check if we're saving, the trigger an action based on the post type
	 *
	 * @param  int $post_id
	 * @param  object $post
	 */
	public function save_meta_boxes( $post_id, $post ) {
        
		// $post_id and $post are required
		if ( empty( $post_id ) || empty( $post ) ) {
			return;
		}

		// Dont' save meta boxes for revisions or autosaves
		if ( defined( 'DOING_AUTOSAVE' ) || is_int( wp_is_post_revision( $post ) ) || is_int( wp_is_post_autosave( $post ) ) ) {
			return;
		}
		
		// Check the nonce
		if ( empty( $_POST[$this->nonce_param] ) || ! wp_verify_nonce( $_POST[$this->nonce_param], $this->nonce_param ) ) {
			return;
		} 
       
		// Check the post being saved == the $post_id to prevent triggering this call for other save_post venues
		if ( empty( $_POST['post_ID'] ) || $_POST['post_ID'] != $post_id ) {
			return;
		}
 
		// Check user has permission to edit
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
        
		do_action( $this->process_data_action, $post_id, $post );
	}
    
    public function remove_meta_boxes() {
        $port = $this->type;
      //  if ( $port == Apollo_DB_Schema::_EVENT_PT ) return;
        
        //remove_meta_box( 'authordiv', $port, 'normal' ); // Author Metabox
        remove_meta_box( 'pageparentdiv', $port, 'side' ); // Attributes Metabox
        //remove_meta_box( 'commentstatusdiv', $port, 'normal' ); // Comments Status Metabox
        //remove_meta_box( 'commentsdiv', $port, 'normal' ); // Comments Metabox
        remove_meta_box( 'postcustom', $port, 'normal' ); // Custom Fields Metabox
        //remove_meta_box( 'postexcerpt', $port, 'normal' ); // Excerpt Metabox
      //  remove_meta_box( 'revisionsdiv', $port, 'normal' ); // Revisions Metabox
        remove_meta_box( 'slugdiv', $port, 'normal' ); // Slug Metabox
        remove_meta_box( 'trackbacksdiv', $port, 'normal' ); // Trackback Metabox
    }
}
