<?php
/**
 * Post Summary
 *
 * Add more summary meta box.
 *
 * @author 		Truonghn
 * @category 	Admin
 * @package 	inc/admin/post-type/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Post_Summary
 */
class Apollo_Meta_Box_Post_Summary {

    /**
     * Output the metabox
     */
    public static function output( $post ) {
        $postSummaryValue = get_post_meta($post->ID,'post-summary',true);
        ?>
        <textarea class="full h-100" name="post-summary" placeholder=""><?php echo $postSummaryValue; ?></textarea>
        <div><?php echo __( 'Post summary are optional hand-crafted summaries of your content that can be used in your theme', 'apollo' ) ?></div>
        <?php
    }

    public static function save( $post_id, $post ){
        $postSummary = isset($_POST['post-summary']) ? $_POST['post-summary'] : '';

        update_post_meta($post_id,'post-summary',$postSummary);
    }
}