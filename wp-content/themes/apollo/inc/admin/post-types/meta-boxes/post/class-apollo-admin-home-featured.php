<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


class Apollo_Admin_Home_Featured {

    private $homeFeaturedOptions = array(
        Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_TYPE,
        Apollo_DB_Schema::_HOME_FEATURED_ARTICLES_TITLE,
        Apollo_DB_Schema::_ACTIVE_HOME_FEATURED_ARTICLES,
        Apollo_DB_Schema::_HOME_CMS_PAGE_BLOG_FEATURED,
        Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_LARGE_LEFT,
        Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_LARGE_RIGHT,
        Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_TOP,
        Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_MIDDLE,
        Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_BOTTOM,
        Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_POSTION
    );

    /**
     * @Ticket #19528 update options for the Home Featured Articles.
     */
    public function saveHomeFeaturedPostsOptions() {
        $optionId = of_get_option_id();
        $apolloOptions = get_option($optionId);

        foreach ($this->homeFeaturedOptions as $key) {
            if (isset($_POST[$optionId][$key])) {
                $apolloOptions[$key] = $_POST[$optionId][$key];
            } else {
                $apolloOptions[$key] = '';
            }
        }

        update_option($optionId, $apolloOptions);
        /** Empty cache */
        $cacheManagementFilePath = APOLLO_INCLUDES_DIR. '/admin/tools/cache/Inc/apollo-sites-caching-management.php';
        if(file_exists($cacheManagementFilePath)){
            require_once $cacheManagementFilePath;
            if(class_exists('APLC_Site_Caching_Management')){
                $currentSiteID = get_current_blog_id();
                $cleanCachedSite = new APLC_Site_Caching_Management($currentSiteID);
                $cleanCachedSite->showAdminMessage = false;
                $cleanCachedSite->emptyAllLocalCaches();
            }
        }

        add_action( 'admin_notices', array($this, 'home_featured_admin_notice_success') );
    }

    public function home_featured_admin_notice_success() { ?>
        <div class="notice notice-success is-dismissible">
            <p><?php _e('Options updated.', 'apollo') ?></p>
        </div>
<?php
    }

}
