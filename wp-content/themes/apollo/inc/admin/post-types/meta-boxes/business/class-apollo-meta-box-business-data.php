<?php
/**
 * @category    admin
 * @package    inc/admin/post-types/meta-boxes
 */

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Apollo_Meta_Box_Business_Data
 */
class Apollo_Meta_Box_Business_Data {
    
    /**
     * Output the metabox
     */

    public  static  function enableBusinessCheckbox($post){
        $business = get_business($post->ID);
        $org = $business->get_org();
        $metaData = get_apollo_meta($org->ID, Apollo_DB_Schema::_APL_ORG_DATA, true);
        $metaData = Apollo_App::unserialize($metaData);
        $arrBusiness = array(Apollo_DB_Schema::_ORG_BUSINESS=>'yes');
        $metaData = array_merge($arrBusiness,$metaData);
        update_apollo_meta( $org->ID, Apollo_DB_Schema::_APL_ORG_DATA, serialize( Apollo_App::clean_array_data($metaData) ) );

    }
    public static function outputOrgData($post) {

        global $thepostid, $typenow;
        $business = get_business($post->ID);
        $post = $business->get_org();
        if ($post) {
            wp_nonce_field('apollo_organization_meta_nonce', 'apollo_organization_meta_nonce');
            $thepostid = $post->ID;

            echo '<h4>' . __('Address Infomation', 'apollo') . '</h4>';

            $business->getOrgInfo($thepostid,Apollo_DB_Schema::_APL_ORG_ADDRESS);

            $arr = array(Apollo_DB_Schema::_ORG_BUSINESS, Apollo_DB_Schema::_ORG_RESTAURANT,Apollo_DB_Schema::_ORG_DO_NOT_DISPLAY);
            $business->getOrgInfo($thepostid,Apollo_DB_Schema::_APL_ORG_DATA,$arr,Apollo_DB_Schema::_ORG_WEBSITE_URL);

            $valueLa = Apollo_App::apollo_get_meta_data($thepostid, Apollo_DB_Schema::_ORG_LATITUDE);
            echo '<div class="options_group event-box org-text-fields">';
            echo '<div> <label for="' . esc_attr($valueLa) . '">' . __('Latitude', 'apollo') . '</label> </div>';
            echo '<div class = text-org> ' . $valueLa . ' </div>';
            echo '</div>';

            $valueLong = Apollo_App::apollo_get_meta_data($thepostid, Apollo_DB_Schema::_ORG_LONGITUDE);
            echo '<div class="options_group event-box org-text-fields">';
            echo '<div> <label for="' . esc_attr($valueLong) . '">' . __('Longitude', 'apollo') . '</label> </div>';
            echo '<div class = text-org> ' . $valueLong . ' </div>';
            echo '</div>';

            echo '<hr>';

            echo '<h4>' . __('Own website and/or blog', 'apollo') . '</h4>';

            $arr = array(Apollo_DB_Schema::_ORG_BUSINESS,Apollo_DB_Schema::_ORG_RESTAURANT,Apollo_DB_Schema::_ORG_DO_NOT_DISPLAY, Apollo_DB_Schema::_ORG_EMAIL, Apollo_DB_Schema::_ORG_PHONE,Apollo_DB_Schema::_APL_ORG_POST_ICONS,
                        Apollo_DB_Schema::_ORG_FAX,Apollo_DB_Schema::_ORG_CONTACT_EMAIL,Apollo_DB_Schema::_ORG_CONTACT_NAME, Apollo_DB_Schema::_ORG_CONTACT_PHONE);
            $business->getOrgInfo($thepostid,Apollo_DB_Schema::_APL_ORG_DATA,$arr);

            echo '<hr>';

            echo '<h4>' . __('Contact Info', 'apollo') . '</h4>';

            $arr = array(Apollo_DB_Schema::_ORG_CONTACT_EMAIL, Apollo_DB_Schema::_ORG_CONTACT_NAME,Apollo_DB_Schema::_ORG_CONTACT_PHONE);
            $business->getOrgInfo($thepostid,Apollo_DB_Schema::_APL_ORG_DATA,$arr,'',1);

            echo '<hr>';

            echo '<h4>' . __('More Organization Info', 'apollo') . '</h4>';

            $org = get_org($thepostid);
            $org->render_cf_detail();

            echo '<hr>';

            echo '<h4>' . __('Organization gallery', 'apollo') . '</h4>';

            ?>
            <div id="event_images_container" data-title="<?php _e('Select images', 'apollo'); ?>"
                 data-text="<?php _e('Use selected images', 'apollo'); ?>">
                <ul class="event_images">
                    <?php
                    $org_image_gallery = get_apollo_meta($post->ID, Apollo_DB_Schema::_APL_ORG_IMAGE_GALLERY, true);
                    $attachments = array_filter(explode(',', $org_image_gallery));
                    if ($attachments)
                        foreach ($attachments as $attachment_id) {
                            echo '<li class="image" data-attachment_id="' . esc_attr($attachment_id) . '">
								' . wp_get_attachment_image($attachment_id, 'thumbnail') . '</li>';
                        }
                    ?>
                </ul>
                <input type="hidden" id="event_image_gallery" name="org_image_gallery"
                       value="<?php echo esc_attr($org_image_gallery); ?>"/>
            </div>

            <?php

            echo '<hr>';

            $videos = @unserialize(get_apollo_meta($thepostid, Apollo_DB_Schema::_APL_ORG_VIDEO, TRUE));
            $audios = @unserialize(get_apollo_meta($thepostid, Apollo_DB_Schema::_APL_ORG_AUDIO, TRUE));

            if (!$videos) {
                $videos[] = array('video_embed' => '', 'video_desc' => '');
            }

            $i = 0;
            echo '<h4>' . __('Organization Video', 'apollo') . '</h4>';
            foreach ($videos as $v):

                $oldVideoEmbed = isset($v['embed']) ? $v['embed'] : '';
                $oldVideoDesc = isset($v['desc']) ? $v['desc'] : '';
                $videoEmbed = isset($v['video_embed']) ? $v['video_embed'] : $oldVideoEmbed;
                $videoDesc = isset($v['video_desc']) ? $v['video_desc'] : $oldVideoDesc;

                echo '<div class="options_group event-box org-text-fields">';
                echo '<div> <label for="' . esc_attr($videoEmbed) . '">' . __('Youtube or Vimeo video', 'apollo') . '</label> </div>';
                echo '<div class = text-org> ' . $videoEmbed . ' </div>';
                echo '</div>';

                echo '<div class="options_group event-box org-text-fields">';
                echo '<div> <label for="' . esc_attr($videoDesc) . '">' . __('Description', 'apollo') . '</label> </div>';
                echo '<div class = text-org> ' . esc_attr(base64_decode($videoDesc)) . ' </div>';
                echo '</div>';

            endforeach;
            echo '<hr>';

            echo '<h4>' . __('Organization  Audio', 'apollo') . '</h4>';
            if (!$audios) {
                $audios[] = array('embed' => '', 'desc' => '');
            }

            $i = 0;
            foreach ($audios as $v):
                $prefix_id = $i != 0 ? '-' . $i : '';
                $embed = str_replace('\\', '', base64_decode($v['embed']));

                echo '<div class="options_group event-box org-text-fields-audio">';
                echo '<div> <label for="' . esc_attr($embed) . '">' . __('Audio Link, Iframe, Object embed', 'apollo') . '</label> </div>';
                echo '<div class = text-org> ' . $embed . ' </div>';
                echo '</div>';
                echo '<div class="clear"></div>';
                echo '</br>';

                echo '<div class="options_group event-box org-text-fields">';
                echo '<div> <label for="' . esc_attr($v['desc']) . '">' . __('Description', 'apollo') . '</label> </div>';
                echo '<div class = text-org> ' . esc_attr(base64_decode($v['desc'])) . ' </div>';
                echo '</div>';

                $i++; endforeach;

            echo '<hr>';

            echo '<h4>' . __('Organization icons', 'apollo') . '</h4>';


            switch ($typenow) {
                case Apollo_DB_Schema::_BUSINESS_PT:
                    $opt_name = Apollo_DB_Schema::_APL_ORG_ICONS;
                    $meta_key = Apollo_DB_Schema::_APL_ORG_POST_ICONS;
                    $_data_k = Apollo_DB_Schema::_APL_ORG_DATA;
                    break;

                default:
                    $opt_name = Apollo_DB_Schema::_APL_EDUCATORS_ICONS;
                    $meta_key = Apollo_DB_Schema::_APL_EDUCATOR_POST_ICONS;
                    $_data_k = Apollo_DB_Schema::_APL_EDUCATOR_DATA;
            }

            $_meta_data = maybe_unserialize(get_apollo_meta($thepostid, $_data_k, true));
            $post_icon_ids = isset($_meta_data[$meta_key]) ? $_meta_data[$meta_key] : array();
            $icon_fields = @unserialize(get_option($opt_name));
            if (!$icon_fields) {
                $icon_fields = array(array('icon_id' => '', 'educator_id' => '', 'icon_desc' => ''));
            }
            echo '<div id="apollo-icons">';
            foreach ($icon_fields as $k => $icon) {
                if (!$icon['icon_id']) continue;
                $desc = explode(' ', $icon_fields[$k]['icon_desc']);

                echo '<div class="options_group">';
                // Description
                apollo_wp_checkbox(array(
                    'id' => 'educator_icon_' . $k,
                    'name' => '' . $_data_k . '[' . $meta_key . '][]',
                    'label' => wp_get_attachment_image($icon['icon_id'], 'thumbnail') . ' ' . implode(' ', array_slice($desc, 0, 15)),
                    'desc_tip' => 'true',
                    'description' => "",
                    'class' => "disable-text-fields",
                    'value' => $post_icon_ids && in_array($icon['icon_id'], $post_icon_ids) ? $icon['icon_id'] : '',
                    'cbvalue' => $icon['icon_id'],
                    'type' => 'checkbox',));
                echo '</div>';
            }
            echo '</div>';
            echo '<hr>';


        }
    }

    public static function output_org($post)
    {
        $business = get_business($post);
        $org = $business->get_org();
        echo $org ? '<a href="' . get_edit_post_link($org->ID) . '">' . $org->post_title . '</a>' : '--';
    }

    /**
     * @param $post
     * @param array $args
     * Vandd metabox location
     */
    public static function output_location_info($post){
        global $post, $thepostid;
        wp_nonce_field( 'apollo_business_meta_nonce', 'apollo_business_meta_nonce' );
        $thepostid = $post->ID;
        $_meta_data = maybe_unserialize(get_apollo_meta($thepostid, Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO, true));
        $hours = '';
        $reservations = '';
        $other_info = '';
        if(!empty($_meta_data)){
            $hours = isset($_meta_data[Apollo_DB_Schema::_APL_BUSINESS_HOURS]) ? $_meta_data[Apollo_DB_Schema::_APL_BUSINESS_HOURS] : "";
            $reservations = isset($_meta_data[Apollo_DB_Schema::_APL_BUSINESS_RESERVATION]) ? $_meta_data[Apollo_DB_Schema::_APL_BUSINESS_RESERVATION] : "";
            $other_info = isset($_meta_data[Apollo_DB_Schema::_APL_BUSINESS_OTHER_INFO]) ? $_meta_data[Apollo_DB_Schema::_APL_BUSINESS_OTHER_INFO] : "";
        }
        echo '<div class="options_group">';
        // Hours
        apollo_wp_editor(  array(
            'name' => Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO . '[' . Apollo_DB_Schema::_APL_BUSINESS_HOURS . ']',
            'id' => Apollo_DB_Schema::_APL_BUSINESS_HOURS,
            'label' => __('Hours', 'apollo'),
            'value' => isset($_POST[Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO][Apollo_DB_Schema::_APL_BUSINESS_HOURS]) ? $_POST[Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO][Apollo_DB_Schema::_APL_BUSINESS_HOURS] : $hours,
            'desc_tip' => 'true',
            'description' => "",
            'class'         => 'apl-editor',
        ) );
        echo '</div>';

        echo '<div class="options_group">';
        // Reservations
        apollo_wp_editor(  array(
            'name' => Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO . '[' . Apollo_DB_Schema::_APL_BUSINESS_RESERVATION . ']',
            'id' => Apollo_DB_Schema::_APL_BUSINESS_RESERVATION,
            'label' => __('Reservations', 'apollo'),
            'value' => isset($_POST[Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO][Apollo_DB_Schema::_APL_BUSINESS_RESERVATION]) ? $_POST[Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO][Apollo_DB_Schema::_APL_BUSINESS_RESERVATION] : $reservations,
            'desc_tip' => 'true',
            'description' => "",
            'class'         => 'apl-editor',
        ) );
        echo '</div>';

        echo '<div class="options_group">';
        // Other info
        apollo_wp_editor(  array(
            'name' => Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO . '[' . Apollo_DB_Schema::_APL_BUSINESS_OTHER_INFO .']',
            'id' => Apollo_DB_Schema::_APL_BUSINESS_OTHER_INFO,
            'label' => __('Other info', 'apollo'),
            'value' => isset($_POST[Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO][Apollo_DB_Schema::_APL_BUSINESS_OTHER_INFO]) ? $_POST[Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO][Apollo_DB_Schema::_APL_BUSINESS_OTHER_INFO] : $other_info,
            'desc_tip' => 'true',
            'description' => "",
            'class'         => 'apl-editor',
        ) );
        echo '</div>';


    }

    /**
     * Save meta box data
     */
    public static function save( $post_id, $post ) {
        global $wpdb;
        $_location_info = isset( $_POST[Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO] ) ? $_POST[Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO] : array();
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO, serialize( Apollo_App::clean_array_data( $_location_info ) ) );

        // ThienLD: Proceed to delete all current BS Fetures before inserting/updating
        $deleteSQL = "DELETE FROM " . $wpdb->{Apollo_Tables::_APOLLO_BUSINESS_META} . " WHERE apollo_business_id = '". (int) $post_id ."' AND meta_key LIKE '".Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY."_%' ";
        $wpdb->query($deleteSQL);
        if ( isset($_POST[Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA][Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY])
            && !empty($_POST[Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA][Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY])
        ) {
            // Start to inserting / updating
            $selectedBSFeatures = $_POST[Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA][Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY];
            if(is_array($selectedBSFeatures)){
                foreach($selectedBSFeatures as $f){
                    update_apollo_meta($post_id, Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY . '_' . $f, $f);
                }
            } else {
                update_apollo_meta($post_id, Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY . '_' . $selectedBSFeatures, $selectedBSFeatures);
            }
        }
    }



    /**
     * Render all business form fields in another module as kind of metabox
     * @date Oct 9th 2018 - serve for ticket #17843
     * @author ThienLD
     * @param $post
     * @param array $args
     */
    public static function output_business_form_fields($post, $args = []){
        global $post, $thepostid;

        $bsPostID = isset($args['args']['associated-business-id']) ? (int) $args['args']['associated-business-id'] : -1;
        if ($bsPostID < 0){
            return;
        }

        $bsPostObject = get_business($bsPostID);

        $metaboxAddedByModule = isset($args['args']['added-by-module']) ? $args['args']['added-by-module'] : '';
        if ($metaboxAddedByModule == Apollo_DB_Schema::_ORGANIZATION_PT){
            wp_nonce_field( 'apollo_organization_meta_nonce', 'apollo_organization_meta_nonce' );
        }

        apollo_wp_hidden_input(array(
            'id'            => Apollo_DB_Schema::_APL_BUSINESS_ID,
            'name'          => 'apl_business_admin_form['.Apollo_DB_Schema::_APL_BUSINESS_ID.']',
            'value'         => $bsPostObject->get_id()
        ));

        echo '<div class="options_group">';
        // Title
        $bsTitle = isset($_POST['apl_business_admin_form'][Apollo_DB_Schema::_APL_BUSINESS_TITLE]) ? $_POST['apl_business_admin_form'][Apollo_DB_Schema::_APL_BUSINESS_TITLE] : $bsPostObject->get_title();
        apollo_wp_text_input(  array(
            'name' => 'apl_business_admin_form[' . Apollo_DB_Schema::_APL_BUSINESS_TITLE . ']',
            'id' => Apollo_DB_Schema::_APL_BUSINESS_TITLE,
            'label' => __( 'Title', 'apollo' ),
            'value' => $bsTitle,
            'desc_tip' => 'true',
            'description' => "",
            'class' => '' )
        );
        echo '</div>';

        echo '<div class="options_group">';
        // Post Content
        $bsContent = isset($_POST['apl_business_admin_form'][Apollo_DB_Schema::_APL_BUSINESS_DESCRIPTION]) ? $_POST['apl_business_admin_form'][Apollo_DB_Schema::_APL_BUSINESS_DESCRIPTION] : $bsPostObject->get_post_content();
        apollo_wp_editor(  array(
            'name' => 'apl_business_admin_form[' . Apollo_DB_Schema::_APL_BUSINESS_DESCRIPTION . ']',
            'id' => Apollo_DB_Schema::_APL_BUSINESS_DESCRIPTION,
            'label' => __('Business Description', 'apollo'),
            'value' => $bsContent,
            'desc_tip' => 'true',
            'description' => "",
            'class'         => 'apl-editor apl-business-desc',
        ) );
        echo '</div>';

        $bsType = $bsPostObject->getListOfBusinessTypesMultiLevels();
        if(!empty($bsType)) {
            echo '<div id="business-type-area-selection" class="options_group clear">';
                echo '<label class="apl-multi-choice">'. __('Categories', 'apollo') .'</label>';
                echo '<div class="wrap-cz">';
                    echo '<div class="wrap-cz-child">';
                    $index = 0;
                    $numCol = 1;
                    $itemPerCol = intval(count($bsType) / $numCol);
                    $selectedValue = isset($_POST['apl_business_admin_form'][Apollo_DB_Schema::_APL_BUSINESS_CATEGORIES]) ? $_POST['apl_business_admin_form'][Apollo_DB_Schema::_APL_BUSINESS_CATEGORIES] : $bsPostObject->getListOfSelectedBusinessTypes();
                        foreach ( $bsType as $k_choice => $choice ):
                            apollo_wp_multi_checkbox(  array(
                                'id'            => ''. Apollo_DB_Schema::_APL_BUSINESS_CATEGORIES . '-' .$index ,
                                'name'          => 'apl_business_admin_form[' . Apollo_DB_Schema::_APL_BUSINESS_CATEGORIES . '][]',
                                'label'         => isset($choice['name']) ? $choice['name'] : '',
                                'class'         => isset($choice['cat-level']) && (int) $choice['cat-level'] > 0 ? 'multi-cb item-child-lv1' : '' ,
                                'desc_tip'      => 'true',
                                'description'   => '',
                                'type'          => 'checkbox',
                                'default'       => false,
                                'value'			=> $selectedValue,
                                'cbvalue'       => $k_choice ) );
                            $index += 1;
                            if($itemPerCol > 0 && $index % $itemPerCol === 0){
                                echo '</div>';
                                echo '<div class="wrap-cz-child">';
                            }
                        endforeach;
                    echo '</div>';

                echo '</div>';
            echo '</div>';
        }


        // render location metabox

        $_meta_data = maybe_unserialize(get_apollo_meta($bsPostID, Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO, true));
        $hours = '';
        $reservations = '';
        $other_info = '';
        if(!empty($_meta_data)){
            $hours = isset($_meta_data[Apollo_DB_Schema::_APL_BUSINESS_HOURS]) ? $_meta_data[Apollo_DB_Schema::_APL_BUSINESS_HOURS] : "";
            $reservations = isset($_meta_data[Apollo_DB_Schema::_APL_BUSINESS_RESERVATION]) ? $_meta_data[Apollo_DB_Schema::_APL_BUSINESS_RESERVATION] : "";
            $other_info = isset($_meta_data[Apollo_DB_Schema::_APL_BUSINESS_OTHER_INFO]) ? $_meta_data[Apollo_DB_Schema::_APL_BUSINESS_OTHER_INFO] : "";
        }
        echo '<div class="options_group">';
        // Hours
        apollo_wp_editor(  array(
            'name' => Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO . '[' . Apollo_DB_Schema::_APL_BUSINESS_HOURS . ']',
            'id' => Apollo_DB_Schema::_APL_BUSINESS_HOURS,
            'label' => __('Hours', 'apollo'),
            'value' => isset($_POST[Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO][Apollo_DB_Schema::_APL_BUSINESS_HOURS]) ? $_POST[Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO][Apollo_DB_Schema::_APL_BUSINESS_HOURS] : $hours,
            'desc_tip' => 'true',
            'description' => "",
            'class'         => 'apl-editor',
        ) );
        echo '</div>';

        echo '<div class="options_group">';
        // Reservations
        apollo_wp_editor(  array(
            'name' => Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO . '[' . Apollo_DB_Schema::_APL_BUSINESS_RESERVATION . ']',
            'id' => Apollo_DB_Schema::_APL_BUSINESS_RESERVATION,
            'label' => __('Reservations', 'apollo'),
            'value' => isset($_POST[Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO][Apollo_DB_Schema::_APL_BUSINESS_RESERVATION]) ? $_POST[Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO][Apollo_DB_Schema::_APL_BUSINESS_RESERVATION] : $reservations,
            'desc_tip' => 'true',
            'description' => "",
            'class'         => 'apl-editor',
        ) );
        echo '</div>';

        echo '<div class="options_group">';
        // Other info
        apollo_wp_editor(  array(
            'name' => Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO . '[' . Apollo_DB_Schema::_APL_BUSINESS_OTHER_INFO .']',
            'id' => Apollo_DB_Schema::_APL_BUSINESS_OTHER_INFO,
            'label' => __('Other info', 'apollo'),
            'value' => isset($_POST[Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO][Apollo_DB_Schema::_APL_BUSINESS_OTHER_INFO]) ? $_POST[Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO][Apollo_DB_Schema::_APL_BUSINESS_OTHER_INFO] : $other_info,
            'desc_tip' => 'true',
            'description' => "",
            'class'         => 'apl-editor',
        ) );
        echo '</div>';


    }


}
