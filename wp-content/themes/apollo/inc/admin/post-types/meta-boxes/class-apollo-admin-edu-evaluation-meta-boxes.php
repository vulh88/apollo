<?php
/**
 * Apollo Artist Meta Boxes
 *
 * Sets up the write panels used by venues (custom post types)
 *
 * @author 		vulh
 * @category 	Admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

include 'class-apl-ad-module-meta-boxes.php';

/**
 * Apollo_Admin_Edu_Evaluation_Meta_Boxes
 */
class Apollo_Admin_Edu_Evaluation_Meta_Boxes extends Apl_Ad_Module_Meta_Boxes {
    
	/**
	 * Constructor
	 */
	public function __construct() {
      
        parent::__construct( Apollo_DB_Schema::_EDU_EVALUATION );
        
        $this->require_meta_boxes();
     
		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 30 );
		add_action( 'save_post', array( $this, 'save_meta_boxes' ), 1, 2 );
      
        // Error handling (for showing errors from meta boxes on next page load)
		add_action( 'admin_notices', array( $this, 'output_errors' ) );
        add_action( 'shutdown', array( $this, 'save_errors' ) );
        
        // Save Meta Boxes
		add_action( $this->process_data_action, 'Apollo_Meta_Box_Edu_Evaluation_Data::save', 10, 2 );
        
	}
    
    public function require_meta_boxes() {
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/edu-evaluation/class-apollo-meta-box-edu-evaluation-data.php';
    }

	/**
	 * Add Apollo Meta boxes
	 */
	public function add_meta_boxes() {
       
        add_meta_box( 'apollo-edu-evaluation-customfield', __( 'More Info', 'apollo' ), 
            'Apollo_Meta_Box_Customfield::output', $this->type, 'normal' );
	}
}

new Apollo_Admin_Edu_Evaluation_Meta_Boxes();
