<?php
/**
 * Apollo Artist Meta Boxes
 *
 * Sets up the write panels used by venues (custom post types)
 *
 * @author 		vulh
 * @category 	Admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

require_once 'class-apl-ad-module-meta-boxes.php';
/**
 * Apollo_Admin_Syndication_Meta_Boxes
 */
class Apollo_Admin_Syndication_Meta_Boxes extends Apl_Ad_Module_Meta_Boxes {
    
	/**
	 * Constructor
	 */
	public function __construct() {
        
        parent::__construct( Apollo_DB_Schema::_SYNDICATION_PT );
        
        $this->require_meta_boxes();
     
		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 30 );
		add_action( 'save_post', array( $this, 'save_meta_boxes' ), 1, 2 );

        // Save Meta Boxes
		add_action( $this->process_data_action, 'Apollo_Meta_Box_Syndication_Info::save', 10, 2 );
		add_action( $this->process_data_action, 'Apollo_Meta_Box_Syndication_Filter::save', 10, 2 );
		add_action( $this->process_data_action, 'Apollo_Meta_Box_Syndication_Result::save', 10, 2 );

        // Error handling (for showing errors from meta boxes on next page load)
		add_action( 'admin_notices', array( $this, 'output_errors' ) );
        add_action( 'shutdown', array( $this, 'save_errors' ) );
        
	}

    public static function getSyndicationDataByKey($post_id, $key, $default = ''){
        if(isset($_POST[$key])){
            return $_POST[$key];
        }
        $metaVal = get_post_meta($post_id,$key,true);
        if(is_serialized($metaVal)){
            $metaVal = maybe_unserialize($metaVal);
        }

        return $metaVal != null && $metaVal != ''  ? $metaVal : $default;

    }

    public function require_meta_boxes() {
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/syndication/class-apollo-meta-box-syndication-filter.php';
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/syndication/class-apollo-meta-box-syndication-result.php';
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/syndication/class-apollo-meta-box-syndication-info.php';
    }

	/**
	 * Add Apollo Meta boxes
	 */
	public function add_meta_boxes() {
        add_meta_box( 'apollo-syndication-info', __( 'Syndication Info', 'apollo' ),
            'Apollo_Meta_Box_Syndication_Info::output', Apollo_DB_Schema::_SYNDICATION_PT, 'normal' );

        add_meta_box( 'apollo-syndication-filter', __( 'Syndication Settings', 'apollo' ),
            'Apollo_Meta_Box_Syndication_Filter::output', Apollo_DB_Schema::_SYNDICATION_PT, 'normal' );
        
        add_meta_box( 'apollo-syndication-result', __( 'Syndication Results', 'apollo' ),
            'Apollo_Meta_Box_Syndication_Result::output', Apollo_DB_Schema::_SYNDICATION_PT, 'normal' );
	}
}

new Apollo_Admin_Syndication_Meta_Boxes();
