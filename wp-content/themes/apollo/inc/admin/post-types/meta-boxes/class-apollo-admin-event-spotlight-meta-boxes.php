<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Apl_Ad_Module_Meta_Boxes' ) ) {
    include 'class-apl-ad-module-meta-boxes.php';
}

/**
 * Apollo Event Spotlight Meta Boxes
 *
 * @category 	Admin
 * @package 	inc/admin/post-types
 * @ticket      #13565
 */
class Apollo_Admin_Event_Spotlight_Meta_Boxes extends Apl_Ad_Module_Meta_Boxes
{
    public function __construct()
    {
        parent::__construct( Apollo_DB_Schema::_EVENT_SPOTLIGHT_PT );

        $this->require_meta_boxes();

        add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) , 30 );
        add_action( 'save_post'     , array( $this, 'save_meta_boxes' ), 1, 2 );

        // Save Meta Boxes;
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Event_Spotlight_Data::save', 10, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Event_Spotlight_Type::save', 10, 2 );

        // Error handling (for showing errors from meta boxes on next page load)
        add_action( 'admin_notices', array( $this, 'output_errors' ) );
        add_action( 'shutdown'     , array( $this, 'save_errors'   ) );
    }

    public function require_meta_boxes()
    {
        require_once 'event-spotlight/class-apollo-meta-box-event-spotlight-data.php';
        require_once 'event-spotlight/class-apollo-meta-box-event-type.php';
    }

    public function add_meta_boxes()
    {
        // add meta box Link
        add_meta_box( 'apollo-event-spotlight-link', __( 'Link', 'apollo' ),
            'Apollo_Meta_Box_Event_Spotlight_Data::output', $this->type, 'normal', 'high');

        add_meta_box( 'apollo-event-spotlight-type', __( 'Event Types', 'apollo' ),
            'Apollo_Meta_Box_Event_Spotlight_Type::output', $this->type, 'side' );
    }
}

new Apollo_Admin_Event_Spotlight_Meta_Boxes();
