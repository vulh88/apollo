<?php
/**
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Edu_Evaluation_Data
 */
class Apollo_Meta_Box_Edu_Evaluation_Data {
    
    
    public static function output_school_district($post) {
        global $post, $thepostid;
		wp_nonce_field( 'apollo_edu_evaluation_nonce', 'apollo_edu_evaluation_nonce' );
        $thepostid = $post->ID;
        
        $school_districts   = get_terms('district-school', array('hide_empty' => false));
        $districts          = array();
        $schools            = array();
        $districts[] = $schools[] = __( '--Select--', 'apollo' );
        
        $district   = '';
        $school     = '';
        $terms = get_the_terms($post, 'district-school');
        if ( $terms ) {
            foreach ( $terms as $term ) {
                if ( $term->parent ) {
                    $school = $term->term_id;
                } else {
                    $district = $term->term_id;
                }
            }
        }
        
        
        if ($school_districts) {
            foreach ( $school_districts as $sd ) {
                if ( $sd->parent ) {
                    $schools[$sd->term_id] = $sd->name; 
                } else {
                    $districts[$sd->term_id] = $sd->name;
                }
            }
        }
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_select(  array( 
                'id'            => 'district', 
                'label'         => __('Your School District', 'apollo'), 
                'desc_tip'      => 'true', 
                'description'   => "",
                'options'       => $districts,
                'value'         => $district,
                'class'         => 'apl-primary-category',
                'required'      => true,
                'attr'          => 'data-ride="ap-refer-select" data-target="#school" data-action="apollo_get_school_district"',
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_select(  array( 
                'id'            => 'school', 
                'label'         => __('Your School Name', 'apollo'), 
                'desc_tip'      => 'true', 
                'description'   => "",
                'options'       => $schools,
                'value'         => $school,
                'class'         => 'apl-primary-category',
                'required'      => true,
            ) );
        echo '</div>';
        
    }
    
	/**
	 * Output the metabox
	 */
	public static function output_educator_program( $post ) { 
        
        global $post, $thepostid;
		wp_nonce_field( 'apollo_edu_evaluation_nonce', 'apollo_edu_evaluation_nonce' );
        $thepostid = $post->ID;
        
        $edu_id = get_apollo_meta($thepostid, Apollo_DB_Schema::_APL_EDU_EVAL_EDUCATOR, TRUE);
        $prog_id = get_apollo_meta($thepostid, Apollo_DB_Schema::_APL_EDU_EVAL_PROG, TRUE);
        
        $educators = get_posts( array(
            'post_type'         => Apollo_DB_Schema::_EDUCATOR_PT,
            'post_status'       => 'publish',
            'posts_per_page'    => -1,
            'orderby'           => 'post_title',
            'order'             => 'ASC',
        ) );
        
        $option = array();
        $option[] = __( '--Select--', 'apollo' );
        
        if ( $educators ) {
            foreach ( $educators as $e ) {
                $option[$e->ID] = $e->post_title; 
            }
        }
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_select(  array( 
                'id'            => Apollo_DB_Schema::_APL_EDU_EVAL_EDUCATOR, 
                'label'         => __('Cultural Organization', 'apollo'), 
                'desc_tip'      => 'true', 
                'description'   => "",
                'options'       => $option,
                'value'         => $edu_id,
                'class'         => 'apl-primary-category',
                'required'      => true,
                'attr'          => 'data-ride="ap-refer-select" data-target="#_apl_edu_eval_prog" data-action="apollo_get_programs_educator"',
            ) );
        echo '</div>';
                
        
        $option = array();
        $option[] = __( '--Select--', 'apollo' );
        
        if ( $edu_id ) {
            $educatorObj = get_educator($edu_id);
            $progs = $educatorObj->get_programs();
            if ($progs) {
                foreach ( $progs as $prog ) {
                    $_prog = get_post($prog->prog_id);
                    $option[$_prog->ID] = $_prog->post_title; 
                }
            }
        } else {
            
            $progs = get_posts( array(
                'post_type'         => Apollo_DB_Schema::_PROGRAM_PT,
                'post_status'       => 'publish',
                'posts_per_page'    => -1,
                'orderby'           => 'post_title',
                'order'             => 'ASC',
            ) );
            if ( $progs ) {
                foreach ( $progs as $e ) {
                    $option[$e->ID] = $e->post_title; 
                }
            }
        }
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_select(  array( 
                'id'            => Apollo_DB_Schema::_APL_EDU_EVAL_PROG, 
                'label'         => __('Cultural Program Provided', 'apollo'),
                'desc_tip'      => 'true', 
                'description'   => "",
                'options'       => $option,
                'value'         => $prog_id,
                'class'         => 'apl-primary-category',
                'required'      => true,
            ) );
        echo '</div>';
	}
    
    /**
	 * Save meta box data
	 */
	public static function save( $post_id, $post ) {
        update_apollo_meta($post_id, Apollo_DB_Schema::_APL_EDU_EVAL_EDUCATOR, isset($_POST[Apollo_DB_Schema::_APL_EDU_EVAL_EDUCATOR]) ? $_POST[Apollo_DB_Schema::_APL_EDU_EVAL_EDUCATOR] : '');
        update_apollo_meta($post_id, Apollo_DB_Schema::_APL_EDU_EVAL_PROG, isset($_POST[Apollo_DB_Schema::_APL_EDU_EVAL_PROG]) ? $_POST[Apollo_DB_Schema::_APL_EDU_EVAL_PROG] : '');
        
        /**
         * Save terms
         */ 
        $_arr_term = array();
        if ( isset( $_POST['district'] ) && $_POST['district'] ) {
            $_arr_term[] = $_POST['district'];
        }
        
        if ( isset( $_POST['school'] ) && $_POST['school'] ) {
            $_arr_term[] = $_POST['school'];
        }
        if ( $_arr_term ) {
            wp_set_post_terms( $post_id, $_arr_term , 'district-school' );
        }
    }    
}
