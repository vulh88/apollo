<?php
/**
 * Venue URL
 *
 * Displays the org Location
 *
 * @author 		vulh
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Org_Data
 */
class Apollo_Meta_Box_Org_Data {

	/**
	 * Output the metabox
	 */
	public static function output( $post ) { 
        
        global $post, $thepostid;
		wp_nonce_field( 'apollo_organization_meta_nonce', 'apollo_organization_meta_nonce' );
        $thepostid = $post->ID;

        /** @Ticket #13517 */
        echo '<div class="options_group event-box">';
            // web site url
            apollo_wp_text_input(  array( 
                'id' => Apollo_DB_Schema::_APL_ORG_DATA. '['. Apollo_DB_Schema::_ORG_DISCOUNT_URL .']',
                'label' => __( 'Discount URL', 'apollo' ),
                'desc_tip' => 'true', 
                'description' => "",
                'class' => 'apollo_input_url',
            ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
            // web site url
            apollo_wp_text_input(  array(
                'id' => Apollo_DB_Schema::_APL_ORG_DATA. '['. Apollo_DB_Schema::_ORG_WEBSITE_URL .']',
                'label' => __( 'Website URL', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'class' => 'apollo_input_url',
            ) );
        echo '</div>';

        echo '<div class="options_group event-box">';

        // blog url
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_ORG_DATA. '['. Apollo_DB_Schema::_ORG_BLOG_URL .']',
            'label' => __( 'Blog url', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apollo_input_url',
        ) );
        echo '</div>';

        // Instagram url
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_ORG_DATA. '['. Apollo_DB_Schema::_ORG_INSTAGRAM_URL .']',
            'label' => __( 'Instagram url', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apollo_input_url',
        ) );
        echo '</div>';

        // Twitter url
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_ORG_DATA. '['. Apollo_DB_Schema::_ORG_TWITTER_URL .']',
            'label' => __( 'Twitter url', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apollo_input_url',
        ) );
        echo '</div>';

        //  Pinterest  url
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_ORG_DATA. '['. Apollo_DB_Schema::_ORG_PINTEREST_URL .']',
            'label' => __( 'Pinterest url', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apollo_input_url',
        ) );
        echo '</div>';

        // Facebook  url
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_ORG_DATA. '['. Apollo_DB_Schema::_ORG_FACEBOOK_URL .']',
            'label' => __( 'Facebook url', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apollo_input_url',
        ) );
        echo '</div>';

        // Linkedin  url
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_ORG_DATA. '['. Apollo_DB_Schema::_ORG_LINKED_IN_URL .']',
            'label' => __( 'LinkedIn url', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apollo_input_url',
        ) );
        echo '</div>';

        // Donate  url
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_ORG_DATA. '['. Apollo_DB_Schema::_ORG_DONATE_URL .']',
            'label' => __( 'Donate', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apollo_input_url',
        ) );
        echo '</div>';
    }

    /**
     * output contact info
     *
     */
    public static function outputContactInfo( $post ) {

        global $post, $thepostid;
        wp_nonce_field( 'apollo_organization_meta_nonce', 'apollo_organization_meta_nonce' );
        $thepostid = $post->ID;

        // contact name
        echo '<div class="options_group event-box">';

        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_ORG_DATA. '['. Apollo_DB_Schema::_ORG_CONTACT_NAME .']',
            'label' => __( 'Contact name', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
        ) );
        echo '</div>';

        // contact email
        echo '<div class="options_group event-box">';
        // web site url
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_ORG_DATA. '['. Apollo_DB_Schema::_ORG_CONTACT_EMAIL .']',
            'label' => __( 'Contact email', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apollo_input_email',
        ) );
        echo '</div>';

        // contact phone
        echo '<div class="options_group event-box">';
        // web site url
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_ORG_DATA. '['. Apollo_DB_Schema::_ORG_CONTACT_PHONE .']',
            'label' => __( 'Contact phone', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
        ) );
        echo '</div>';

    }


    public static function outputBusiness( $post ) {

        global $post, $thepostid;
        wp_nonce_field( 'apollo_organization_meta_nonce', 'apollo_organization_meta_nonce' );
        $thepostid = $post->ID;


        echo '<div class="org-business-box">';

        /** @Ticket #13424 */
        $is_business = Apollo_App::apollo_get_meta_data( $thepostid, Apollo_DB_Schema::_APL_ORG_DATA. '['. Apollo_DB_Schema::_ORG_BUSINESS .']');

        /*@ticket #18087: [CF] 20181030 - [ORG][Admin form] Preview Changes button for the business - item 4, 5*/
        if($is_business == 'yes'){
            $businessId = Apollo_App::apollo_get_meta_data( $thepostid, Apollo_DB_Schema::_APL_ORG_BUSINESS);
            $previewBusinessText = __('Preview Business Changes', 'apollo');
            echo "<div><a class='preview-business' data-post-id='$businessId'>$previewBusinessText</a></div>";
        }

        apollo_wp_checkbox(  array(
            'id' => Apollo_DB_Schema::_APL_ORG_DATA. '['. Apollo_DB_Schema::_ORG_BUSINESS .']',
            'label' => __( 'Is Business', 'apollo' ),
            'class' => 'org-is-business'

        ) );

        if ($is_business == 'yes') {
            $business_link = get_edit_post_link($businessId);
            echo '<small><a class="apl-org-business-edit-link" target="_blank" href="'. $business_link .'">'. __( "Edit Business", "apollo" ) .'</a></small>';
        }
        echo '</div>';


        echo '<div >';

        apollo_wp_checkbox(  array(
            'id' => Apollo_DB_Schema::_APL_ORG_DATA. '['. Apollo_DB_Schema::_ORG_RESTAURANT .']',
            'label' => __( 'If you are a RESTAURANT please check this box', 'apollo' ),
        ) );
        echo '</div>';

    }

    public static function outputDisplay( $post ) {

        global $post, $thepostid;
        wp_nonce_field( 'apollo_organization_meta_nonce', 'apollo_organization_meta_nonce' );
        $thepostid = $post->ID;

        echo '<div >';

        apollo_wp_checkbox(  array(
            'id' => Apollo_DB_Schema::_APL_ORG_DATA. '['. Apollo_DB_Schema::_ORG_DO_NOT_DISPLAY .']',
            'label' => __( 'Do not display the main directory listings page', 'apollo' ),
        ) );
        echo '</div>';

    }

    public static function outputIsMember( $post ) {
            global $post, $thepostid;
            wp_nonce_field( 'apollo_organization_meta_nonce', 'apollo_organization_meta_nonce' );
            $thepostid = $post->ID;
            echo '<div >';
            apollo_wp_checkbox(  array(
                'id' => Apollo_DB_Schema::_APL_ORG_DATA. '['. Apollo_DB_Schema::_ORG_MEMBER_ONLY .']',
                'label' => __( 'This item will appear in the FE event submission form\'s Organization drop-down menu', 'apollo' ),
            ) );
            echo '</div>';

    }

    private static function _isExistBusiness($id) {
        global  $wpdb;
        $Tbl = $wpdb->{Apollo_Tables::_APL_ORG_META};
        $sql = "SELECT  meta_value from $Tbl WHERE meta_key ='" . Apollo_DB_Schema::_APL_ORG_BUSINESS. "'
                AND  apollo_organization_id = '$id'  ";
        $rows = $wpdb->get_row($sql);
        if( !$rows )
            return false;
        return $rows->meta_value;
    }

    public static function on_all_status_transitions( $new_status, $old_status,$post ){
        if ( $new_status != $old_status ) {
            $id_business = self::_isExistBusiness($post->ID);
            if( $id_business) {
                self::saveBusinessPost($id_business,$id_business,$new_status);
            }
        }
    }

    public static function deleteBusiness($post_id){
        $id_business = self::_isExistBusiness($post_id);
        global  $wpdb;
        if( $id_business) {
            $wpdb->delete($wpdb->posts,array(ID => $id_business));
        }
    }

    /**
     * Clone org data to business.
     * @param $bsID
     * @param $orgID
     * @param $status
     * @param bool $updateOnly
     * @return int|WP_Error
     */
    public static function saveBusinessPost($bsID, $orgID, $status, $updateOnly = false){
        try{
            if (get_post($bsID)){ // updating business post
                $post_arr = array(
                    'ID' => $bsID ,
                    'post_status' => $status,
                );
                $bsID = wp_update_post($post_arr);

            } else if (!$updateOnly) { // inserting new business post with org data
                $orgObj = get_org($orgID);
                $post_arr = array(
                    'post_title' => $orgObj->get_title(), // in case : $post_id is orgID
                    'post_content' => $orgObj->get_full_content(),
                    'post_status' => $status,
                    'post_type' => Apollo_DB_Schema::_BUSINESS_PT,
                );
                $bsID = wp_insert_post($post_arr);
            }

            // inserting  row into icl_translations table once plugin WPML existed
            if(function_exists('icl_object_id')){
                global $sitepress, $wpdb;
                $table = new Apl_Query( "{$wpdb->prefix}icl_translations");
                if( intval($bsID) && !$table->get_row("element_id =$bsID")) {
                    $def_trid = $sitepress->get_element_trid($bsID);
                    $sitepress->set_element_language_details($bsID, "post_business", $def_trid, ICL_LANGUAGE_CODE);
                }
            }

            return $bsID;
        }catch (Exception $ex){
            aplDebugFile($ex->getMessage(), "Apollo_Meta_Box_Org_Data:saveBusinessPost");
        }
        return 0;
    }
        /**
	 * Save meta box data
	 */
	public static function save( $post_id, $post ) {
        global $wpdb;
        $_data = $_POST[Apollo_DB_Schema::_APL_ORG_DATA];
        $email_field = Apollo_DB_Schema::_ORG_EMAIL;
        if ( isset( $_data[$email_field] ) ):
            if ( is_email( $_data[$email_field] ) ) {
                $_data[$email_field] = Apollo_App::clean_data_request( $_data[$email_field] );
            } else {
                if ( $_data[$email_field] ) {
                    Apollo_Admin_Org_Meta_Boxes::add_error( __( 'Email is invalid format', 'apollo' ) );
                }
                $_data[$email_field] = '';
            }
        endif;
//        $url_field = Apollo_DB_Schema::_ORG_WEBSITE_URL;
//        if ( isset( $_data[$url_field] ) ):
//            if ( filter_var( $_data[$url_field], FILTER_VALIDATE_URL ) ) {
//                $_data[$url_field] = Apollo_App::clean_data_request( $_data[$url_field] );
//            } else {
//                if ( $_data[$url_field] ) {
//                //    Apollo_Admin_Org_Meta_Boxes::add_error( __( 'Website url is invalid format', 'apollo' ) );
//                }
//                $_data[$url_field] = '';
//            }
//        endif;
        /** @Ticket #13517 */
        if (isset($_data[Apollo_DB_Schema::_ORG_DISCOUNT_URL])) {
            update_apollo_meta($post_id, Apollo_DB_Schema::_ORG_DISCOUNT_URL, $_data[Apollo_DB_Schema::_ORG_DISCOUNT_URL]);
        }
        $isBusiness = isset($_data[Apollo_DB_Schema::_ORG_BUSINESS]) ? 'yes' :'';
        update_apollo_meta($post_id, Apollo_DB_Schema::_ORG_BUSINESS, $isBusiness);
        $isRestaurant = isset($_data[Apollo_DB_Schema::_ORG_RESTAURANT]) ? 'yes' :'';
        update_apollo_meta($post_id, Apollo_DB_Schema::_ORG_RESTAURANT, $isRestaurant);
        $isDoNotDisplay = isset($_data[Apollo_DB_Schema::_ORG_DO_NOT_DISPLAY]) ? 'yes' :'';
        update_apollo_meta( $post_id, Apollo_DB_Schema::_ORG_DO_NOT_DISPLAY, $isDoNotDisplay );

        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_ORG_DATA, serialize( Apollo_App::clean_array_data($_data) ) );

        /* TruongHN: clone org post data to business post */
        $id_business = self::_isExistBusiness($post_id); // get org meta data : org_business_id. FALSE if not existing
        $id_business = !$id_business ? -1 : $id_business;
        $business_field = Apollo_DB_Schema::_ORG_BUSINESS ;
        if (isset($_data[$business_field]) && $id_business) {
            // TruongHN: at the first time, when org is checked as org_business
            // Handle to clone org data to business.
            $id_business = self::saveBusinessPost($id_business, $post_id, $post->post_status); // insert new post
        } else {
            $id_business = self::saveBusinessPost($id_business, $post_id, 'pending', true); // update status post
        }
        if(intval($id_business) > 0){
            update_apollo_meta($post_id, Apollo_DB_Schema::_APL_ORG_BUSINESS, $id_business);
            update_apollo_meta($id_business, Apollo_DB_Schema::_APL_BUSINESS_ORG, $post_id);
        }
        /* TruongHN: End clone org post data to business post */

        /** @Ticket #13028 */
        update_apollo_meta($post_id, Apollo_DB_Schema::_ORG_PHONE, !empty($_data[Apollo_DB_Schema::_ORG_PHONE]) ? $_data[Apollo_DB_Schema::_ORG_PHONE] : '');
	}


    /**
     * This function serves for saving data from Business Metabox instead of doing directly in Business admin form
     * @Ticket #17843
     * @author ThienLD
     * @param $post_id
     * @param $post
     */
    public static function saveBusinessMetaboxData($post_id, $post ) {
        global $wpdb;
        if ( ! isset($_POST[Apollo_DB_Schema::_APL_ORG_DATA][Apollo_DB_Schema::_ORG_BUSINESS])){
            return; // handle for case of uncheck is_business checkbox
        }

        $postedGeneralData = isset($_POST['apl_business_admin_form']) ? $_POST['apl_business_admin_form'] : array();
        $id = isset($postedGeneralData[Apollo_DB_Schema::_APL_BUSINESS_ID]) ? $postedGeneralData[Apollo_DB_Schema::_APL_BUSINESS_ID] : -1;
        $title = isset($postedGeneralData[Apollo_DB_Schema::_APL_BUSINESS_TITLE]) ? $postedGeneralData[Apollo_DB_Schema::_APL_BUSINESS_TITLE] : '';
        $postContent = isset($postedGeneralData[Apollo_DB_Schema::_APL_BUSINESS_DESCRIPTION]) ? $postedGeneralData[Apollo_DB_Schema::_APL_BUSINESS_DESCRIPTION] : '';
        $selectedCategories = isset($postedGeneralData[Apollo_DB_Schema::_APL_BUSINESS_CATEGORIES]) ? $postedGeneralData[Apollo_DB_Schema::_APL_BUSINESS_CATEGORIES] : array();
        $postedLocationData = isset( $_POST[Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO] ) ? $_POST[Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO] : array();

        if( (int) $id > 0){
            $dataForUpdating = array(
                'ID'            => $id,
                'post_title'    => $title,
                'post_content'  => $postContent,
                'post_type'     => Apollo_DB_Schema::_BUSINESS_PT
            );
            wp_update_post($dataForUpdating);
            wp_set_post_terms($id,$selectedCategories,Apollo_DB_Schema::_BUSINESS_PT . '-type');
            update_apollo_meta( $id, Apollo_DB_Schema::_APL_BUSINESS_LOCATION_INFO, serialize( Apollo_App::clean_array_data( $postedLocationData ) ) );
        }

        // save custom field Business Feature as multiple metadata within post_meta
        // ThienLD: Proceed to delete all current BS Fetures before inserting/updating
        $deleteSQL = "DELETE FROM " . $wpdb->{Apollo_Tables::_APOLLO_BUSINESS_META} . " WHERE apollo_business_id = '". (int) $id ."' AND meta_key LIKE '".Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY."_%' ";
        $wpdb->query($deleteSQL);
        if ( isset($_POST[Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA][Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY])
            && !empty($_POST[Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA][Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY])
        ) {
            // Start to inserting / updating
            $selectedBSFeatures = $_POST[Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA][Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY];
            if (is_array($selectedBSFeatures)){
                foreach($selectedBSFeatures as $f){
                    update_apollo_meta($id, Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY . '_' . $f, $f);
                }
            } else {
                update_apollo_meta($id, Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY . '_' . $selectedBSFeatures, $selectedBSFeatures);
            }
        }
    }


    /**
     * This function serves for saving data from Business Metabox instead of doing directly in Business admin form
     * @Ticket #17843
     * @author ThienLD
     * @param $post_id
     * @param $post
     */
    public static function saveBusinessCustomFieldsData($post_id, $post ) {
        if ( ! isset($_POST[Apollo_DB_Schema::_APL_ORG_DATA][Apollo_DB_Schema::_ORG_BUSINESS])){
            return; // handle for case of uncheck is_business checkbox
        }
        $postedGeneralData = isset($_POST['apl_business_admin_form']) ? $_POST['apl_business_admin_form'] : array();
        $id = isset($postedGeneralData[Apollo_DB_Schema::_APL_BUSINESS_ID]) ? $postedGeneralData[Apollo_DB_Schema::_APL_BUSINESS_ID] : -1;
        $parentKey = Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA;

        if( (int) $id > 0){
            $currentPostObj = get_business($id);
            $currentPostObj = !empty($currentPostObj) ? $currentPostObj->post : array();
            $businessGroupFields = Apollo_Custom_Field::get_group_fields(Apollo_DB_Schema::_BUSINESS_PT);
            $postedBSCFData = array();
            if (!empty($businessGroupFields)){
                foreach( $businessGroupFields as $gf ) {
                    foreach ($gf['fields'] as $field){
                        if(isset($_POST[$parentKey][$field->name])){
                            $postedBSCFData[$parentKey][$field->name] = $_POST[$parentKey][$field->name];
                        }
                    }
                }
            }
            Apollo_Meta_Box_Customfield::saveByPassingPostedData(array(
                'post' => $currentPostObj,
                'post_id' => $id,
                'posted_data' => $postedBSCFData
            ));
        }
    }


}