<?php
/**
 * Event Data
 *
 * Displays the event data box, tabbed, with several panels covering price, stock etc.
 *
 * @author 		vulh
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Event_Data
 */
class Apollo_Meta_Box_Org_Data_Audio_Video {

	/**
	 * Output the metabox
	 */
	public static function output( $post ) { 
        
        global $post, $wpdb, $thepostid;
		wp_nonce_field( 'apollo_event_meta_nonce', 'apollo_event_meta_nonce' );
        $thepostid = $post->ID;
        $videos = @unserialize( get_apollo_meta( $thepostid, Apollo_DB_Schema::_APL_ORG_VIDEO, TRUE ) );
        $audios = @unserialize( get_apollo_meta( $thepostid, Apollo_DB_Schema::_APL_ORG_AUDIO, TRUE ) );

        ?>
            
        <div class="panel-wrap product_data">
			<div class="apollo-tabs-back"></div>
			<ul class="product_data_tabs apollo-tabs" style="display:none;">
				<?php
					$product_data_tabs = apply_filters( 'apollo_org_data_tabs', array(
                        'video' => array(
							'label'  => __( 'Video', 'apollo' ),
							'target' => 'video-org-data',
							'class'  => array('active'),
						),
                        'audio' => array(
                            'label'  => __( 'Audio', 'apollo' ),
                            'target' => 'audio-org-data',
                            'class'  => array(),
                        ),
					) );
					foreach ( $product_data_tabs as $key => $tab ) {
						?><li class="<?php echo $key; ?>_options <?php echo $key; ?>_tab <?php echo implode( ' ' , $tab['class'] ); ?>">
							<a href="#<?php echo $tab['target']; ?>"><?php echo esc_html( $tab['label'] ); ?></a>
						</li><?php
					}
					do_action( 'apollo_product_write_panel_tabs' );
				?>
			</ul>
            <!--Tri start data video -->
            <div id="video-org-data" class="panel apollo_options_panel">
                <div class="video-wrapper">
                    <?php
                        if ( ! $videos ) {
                            $videos[] = array( 'video_embed' => '', 'video_desc' => '' );
                        }

                        $i = 0;
                        foreach( $videos as $v ):
                            $tags_html = '';
                            if(has_filter('apl_pbm_admin_render_select_tags')) {
                                $tags_html = apply_filters('apl_pbm_admin_render_select_tags', [
                                    'post_id' => $post->ID,
                                    'tags_name' => "apl_pbm_selected_tags_video[$i]",
                                    'type' => 'video',
                                    'media_id' => $i,
                                ]);
                            }

                            $prefix_id = $i != 0 ? '-'.$i : '';
                            $oldVideoEmbed = isset($v['embed'])?$v['embed']:'';
                            $oldVideoDesc = isset($v['desc'])?$v['desc']:'';
                            $videoEmbed = isset($v['video_embed'] )?$v['video_embed'] :$oldVideoEmbed;
                            $videoDesc = isset($v['video_desc'] )?$v['video_desc'] :$oldVideoDesc;
                    ?>
                    <div class="count video-list<?php echo $prefix_id; ?>">

                        <div class="options_group">
                            <p class="form-field video_embed_field ">
                                <label for="video_embed"><?php _e( 'YouTube or Vimeo URL', 'apollo' ) ?></label>
                                <input data-parent=".video_options" name="video_embed[]" type="text" value="<?php echo $videoEmbed ?>" class="full apollo_input_url" />
                            </p>
                        </div>

                        <div class="options_group">
                            <p class="form-field video_desc_field ">
                                <label for="video_embed"><?php _e( 'Video Description', 'apollo' ) ?></label>
                                <textarea class="full" name="video_desc[]" placeholder="" rows="2" cols="20"><?php echo esc_attr(base64_decode($videoDesc)) ?></textarea>
                            </p>
                        </div>

                        <?php if($tags_html): ?>
                            <div class="options_group">
                                <p class="form-field video_desc_field left apl-pbm-label-video-select-tags">
                                    <label for="video_embed "><?php _e( 'Select tags', 'apollo' ) ?></label>
                                </p>
                                <div class="wrap-select-tags left">
                                    <?php echo $tags_html ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <hr>
                        <?php endif; ?>

                        <div <?php if ( $i == 0 ) echo 'style="display: none;"' ?> data-confirm="<?php _e( 'Are you sure to remove this video ?', 'apollo' ) ?>" class="del"><i class="fa fa-times"></i></div>

                    </div>
                    <?php $i++; endforeach; ?>

                    <?php
                        $tags_html_default = '';
                        if(has_filter('apl_pbm_admin_render_select_tags')) {
                            $tags_html_default = apply_filters('apl_pbm_admin_render_select_tags', [
                                'post_id' => $post->ID,
                                'tags_name' => 'apl_pbm_selected_tags_video',
                                'type' => 'video',
                                'media_id' => '',
                                'wrap_html' => array(
                                    'first' => '<input type="hidden" name="pbm_number_video" value="' . $i . '"><div id="container-select-video-tags-field" class="hidden">',
                                    'last' => '</div>'
                                )
                            ]);
                        }

                        echo $tags_html_default;

                    ?>

                </div>
<!--                <input type="hidden" name="--><?php //echo Apollo_DB_Schema::_APOLLO_EVENT_DATA ?><!--[--><?php //echo Apollo_DB_Schema::_VIDEO ?><!--]" />-->
                <input type="button" class="button button-primary button-large" id="apl-add-video" value="<?php _e( 'Add more links', 'apollo' ) ?>" />
			</div>
            <!--Tri start data audio -->
            <div id="audio-org-data" class="panel apollo_options_panel">
                <div class="audio-wrapper" >
                    <?php
                    if ( ! $audios ) {
                        $audios[] = array( 'embed' => '', 'desc' => '' );
                    }

                    $i = 0;
                    foreach( $audios as $v ):
                        $prefix_id = $i != 0 ? '-'.$i : '';
                        $embed = str_replace('\\','',base64_decode($v['embed']));

                        $tags_html = '';
                        if(has_filter('apl_pbm_admin_render_select_tags')) {
                            $tags_html = apply_filters('apl_pbm_admin_render_select_tags', [
                                'post_id' => $post->ID,
                                'tags_name' => "apl_pbm_selected_tags_audio[$i]",
                                'type' => 'audio',
                                'media_id' => $i,
                            ]);
                        }

                        ?>
                        <div class="count audio-list<?php echo $prefix_id; ?>">

                            <div class="options_group">
                                <p class="form-field video_embed_field ">
                                    <label for="audio_embed"><?php _e( 'Audio Link, Iframe, Object embed', 'apollo' ) ?></label>
                                    <textarea class="audio_embed" data-parent=".audio_options" name="audio_embed[]"><?php echo $embed ?></textarea>
                                </p>
                            </div>

                            <div class="options_group">
                                <p class="form-field video_desc_field ">
                                    <label for="audio_embed"><?php _e( 'Description', 'apollo' ) ?></label>
                                    <textarea style="  width: 95%" class="full" name="audio_desc[]" placeholder="" rows="2" cols="20"><?php echo esc_attr(base64_decode($v['desc'])) ?></textarea>
                                </p>
                            </div>

                            <?php if($tags_html): ?>
                                <div class="options_group">
                                    <p class="form-field video_desc_field left apl-pbm-label-video-select-tags">
                                        <label><?php _e( 'Select tags', 'apollo' ) ?></label>
                                    </p>
                                    <div class="wrap-select-tags left">
                                        <?php echo $tags_html ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                            <?php endif; ?>


                            <div <?php if ( $i == 0 ) echo 'style="display: none;"' ?> data-confirm="<?php _e( 'Are you sure to remove this video ?', 'apollo' ) ?>" class="del"><i class="fa fa-times"></i></div>

                        </div>
                        <?php $i++; endforeach; ?>

                    <?php
                    //@Ticket 15263
                    $tags_audio_html_default = '';
                    if(has_filter('apl_pbm_admin_render_select_tags')) {
                        $tags_audio_html_default = apply_filters('apl_pbm_admin_render_select_tags', [
                            'post_id' => $post->ID,
                            'tags_name' => 'apl_pbm_selected_tags_audio',
                            'type' => 'audio',
                            'media_id' => '',
                            'wrap_html' => array(
                                'first' => '<div id="container-select-audio-tags-field" class="hidden">',
                                'last' => '</div>'
                            )
                        ]);
                    }

                    echo $tags_audio_html_default;

                    ?>

                </div>
                <!--                <input type="hidden" name="--><?php //echo Apollo_DB_Schema::_APOLLO_EVENT_DATA ?><!--[--><?php //echo Apollo_DB_Schema::_VIDEO ?><!--]" />-->
                <input type="button" class="button button-primary button-large" id="apl-add-audio" value="<?php _e( 'Add audio', 'apollo' ) ?>" />
            </div>

			<div class="clear"></div>
		</div>
		
		<?php
	}
    

    
	/**
	 * Save meta box data
	 */
	public static function save( $post_id, $post ) {


        //save video data
        $video_embed = isset( $_POST['video_embed'] ) ? $_POST['video_embed'] : '';
        $video_desc = isset( $_POST['video_desc'] ) ? $_POST['video_desc'] :'';
        $_arr_video = array();
        if ( $video_embed ) {
            $i = 0;
            foreach ( $video_embed as $v ) {
                if ( ! $v ) continue;
                $_arr_video[] = array( 'video_embed' => $v, 'video_desc' => isset( $video_desc[$i] ) ?  base64_encode(Apollo_App::clean_data_request($video_desc[$i])) : '' );
                $i++;
            }
        }
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_ORG_VIDEO, serialize($_arr_video) );
        //end save video data

        //save autio data
        $audio_embed = isset( $_POST['audio_embed'] ) ? $_POST['audio_embed'] : '';
        $audio_desc = isset( $_POST['audio_desc'] ) ? $_POST['audio_desc'] :'';

        $_arr_audio = array();
        if ( $audio_embed ) {
            $i = 0;
            foreach ( $audio_embed as $v ) {
                if ( ! $v ) continue;
                $_arr_audio[] = array(
                    'embed' =>  base64_encode($v),
                    'desc' => isset( $audio_embed[$i] ) ?  base64_encode(Apollo_App::clean_data_request($audio_desc[$i])) : ''
                );
                $i++;
            }
        }

        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_ORG_AUDIO, serialize($_arr_audio) );
        //end save video data
	}
    
}