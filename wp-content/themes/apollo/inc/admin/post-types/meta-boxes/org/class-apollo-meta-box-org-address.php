<?php
/**
 * Venue URL
 *
 * Displays the org address
 *
 * @author 		vulh
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Org_Address
 */

class Apollo_Meta_Box_Org_Address {

	/**
	 * Output the metabox
	 */
	public static function output( $post ) {

        global $post, $thepostid,$pagenow;
		wp_nonce_field( 'apollo_organization_meta_nonce', 'apollo_organization_meta_nonce' );
        $thepostid = $post->ID;
        $stateVal = '';
        $cityVal = '';

        if(  $pagenow == 'post.php' ){//edit
            $stateVal = Apollo_App::apollo_get_meta_data( $thepostid, Apollo_DB_Schema::_APL_ORG_ADDRESS .'['. Apollo_DB_Schema::_ORG_STATE .']' );
            $cityVal = Apollo_App::apollo_get_meta_data( $thepostid, Apollo_DB_Schema::_APL_ORG_ADDRESS .'['. Apollo_DB_Schema::_ORG_CITY .']' );
        } else {
            $list_states = Apollo_App::getListState();
            $default_states =  of_get_option(Apollo_DB_Schema::_APL_DEFAULT_STATE);
            $cityVal = of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY);
            if( $default_states && in_array($default_states,$list_states) ) {
                $stateVal = $default_states;
            } else if(count($list_states) == 1) {
                $stateVal = key($list_states);
            }
        }

        /** @Ticket #13643 */
        $requiredEmail = of_get_option(Apollo_DB_Schema::_ORGANIZATION_ENABLE_EMAIL_REQUIREMENT, 0);
        $requiredPhone = of_get_option(Apollo_DB_Schema::_ORGANIZATION_ENABLE_PHONE_REQUIREMENT, 0);
        $requiredAddress = of_get_option(Apollo_DB_Schema::_ORGANIZATION_ENABLE_ADDRESS_REQUIREMENT, 0);
        $requiredState = of_get_option(Apollo_DB_Schema::_ORGANIZATION_ENABLE_STATE_REQUIREMENT, 0);
        $requiredCity = of_get_option(Apollo_DB_Schema::_ORGANIZATION_ENABLE_CITY_REQUIREMENT, 0);
        $requiredZip = of_get_option(Apollo_DB_Schema::_ORGANIZATION_ENABLE_ZIP_REQUIREMENT, 0);

        echo '<div class="options_group event-box">';
            // Address 1
            apollo_wp_text_input(  array(
                'id' => ''. Apollo_DB_Schema::_APL_ORG_ADDRESS .'['. Apollo_DB_Schema::_ORG_ADDRESS1 .']',
                'label' => __( 'Address 1', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'class' => $requiredAddress ? 'required' : '',
                'required' => $requiredAddress ? true : false
            ) );
        echo '</div>';

        //Address 2
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array(
                'id' => Apollo_DB_Schema::_APL_ORG_ADDRESS. '['. Apollo_DB_Schema::_ORG_ADDRESS2 .']',
                'label' => __( 'Address 2', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",

            ) );
        echo '</div>';

        echo '<div class="apl-first-org-address">';
        // State
        echo '<div class="options_group event-box">';
            apollo_wp_select(  array(
                'id' => ''. Apollo_DB_Schema::_APL_ORG_ADDRESS .'['. Apollo_DB_Schema::_ORG_STATE .']',
                'label' => __( 'State', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'options'   => Apollo_App::getStateByTerritory(),
                'class' => sprintf('apl-territory-state %s', $requiredState ? 'required' : ''),
                'required' => $requiredState ? true : false,
                'value' => $stateVal,
                'attr' => "data-wrapper = '.apl-first-org-address'"
            ) );
        echo '</div>';
        // City
        echo '<div class="options_group event-box">';
            apollo_wp_select(  array(
                'id' => ''. Apollo_DB_Schema::_APL_ORG_ADDRESS .'['. Apollo_DB_Schema::_ORG_CITY .']',
                'label' => __( 'City', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'options'   => Apollo_App::getCityByTerritory(FALSE, $stateVal, true),
                'class' => sprintf('apl-territory-city %s', $requiredCity ? 'required' : ''),
                'required' => $requiredCity ? true : false,
                'value' => $cityVal,
                'attr' => "data-wrapper = '.apl-first-org-address'"
            ) );
        echo '</div>';

        // Zip
        echo '<div class="options_group event-box">';
            apollo_wp_select(  array(
                'id' => ''. Apollo_DB_Schema::_APL_ORG_ADDRESS .'['. Apollo_DB_Schema::_ORG_ZIP .']',
                'label' => __( 'Zip', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'options'   => Apollo_App::getZipByTerritory(FALSE, $stateVal, $cityVal, true),
                'class' => sprintf('apl-territory-zipcode %s', $requiredZip ? 'required' : ''),
                'required' => $requiredZip ? true : false,
                'attr' => "data-wrapper = '.apl-first-org-address'"
            ) );
        echo '</div>';
        echo '</div>';

        /*Begin @ticket #18223: Turn on the secondary org address*/
        $enableSecondaryOrgAddress = of_get_option(Apollo_DB_Schema::_APL_ENABLE_SECONDARY_ORG_ADDRESS, 0) ? '' : 'hidden';
        $secondary_address = Apollo_App::apollo_get_meta_data( $thepostid, Apollo_DB_Schema::_APL_SECONDARY_ORG_ADDRESS  );
        if(!is_array($secondary_address)){
            $secondary_address = maybe_unserialize($secondary_address);
        }
        $secondaryState = isset($secondary_address[Apollo_DB_Schema::_SECONDARY_ORG_STATE]) ? $secondary_address[Apollo_DB_Schema::_SECONDARY_ORG_STATE] : '';
        $secondaryCity = isset($secondary_address[Apollo_DB_Schema::_SECONDARY_ORG_CITY] ) ? $secondary_address[Apollo_DB_Schema::_SECONDARY_ORG_CITY] : '';

        echo "<div class='options_group event-box $enableSecondaryOrgAddress'>";
            // Address 1
            apollo_wp_text_input(  array(
                'id' => ''. Apollo_DB_Schema::_APL_SECONDARY_ORG_ADDRESS .'['. Apollo_DB_Schema::_SECONDARY_ORG_ADDRESS1 .']',
                'label' => __( 'Secondary Address 1', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'value' => isset($secondary_address[Apollo_DB_Schema::_SECONDARY_ORG_ADDRESS1]) ? $secondary_address[ Apollo_DB_Schema::_SECONDARY_ORG_ADDRESS1] : ''
            ) );
        echo '</div>';

        //Address 2
        echo "<div class='options_group event-box $enableSecondaryOrgAddress'>";
            // Description
            apollo_wp_text_input(  array(
                'id' => Apollo_DB_Schema::_APL_SECONDARY_ORG_ADDRESS. '['. Apollo_DB_Schema::_SECONDARY_ORG_ADDRESS2 .']',
                'label' => __( 'Secondary Address 2', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'value' => isset($secondary_address[Apollo_DB_Schema::_SECONDARY_ORG_ADDRESS2] ) ? $secondary_address[Apollo_DB_Schema::_SECONDARY_ORG_ADDRESS2] : ''

            ) );
        echo '</div>';

        echo '<div class="apl-secondary-org-address">';
        // State
        echo "<div class='options_group event-box $enableSecondaryOrgAddress'>";
            apollo_wp_select(  array(
                'id' => ''. Apollo_DB_Schema::_APL_SECONDARY_ORG_ADDRESS .'['. Apollo_DB_Schema::_SECONDARY_ORG_STATE .']',
                'label' => __( 'Secondary State', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'options'   => Apollo_App::getStateByTerritory(),
                'class' => 'apl-territory-state',
                'value' => $secondaryState,
                'attr' => "data-wrapper = '.apl-secondary-org-address'"
            ) );
        echo '</div>';
        // City
        echo "<div class='options_group event-box $enableSecondaryOrgAddress'>";
            apollo_wp_select(  array(
                'id' => ''. Apollo_DB_Schema::_APL_SECONDARY_ORG_ADDRESS .'['. Apollo_DB_Schema::_SECONDARY_ORG_CITY .']',
                'label' => __( 'Secondary City', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'options'   => Apollo_App::getCityByTerritory(FALSE, $secondaryState, true),
                'class' => 'apl-territory-city',
                'value' => $secondaryCity,
                'attr' => "data-wrapper = '.apl-secondary-org-address'"

            ) );
        echo '</div>';

        // Zip
        echo "<div class='options_group event-box $enableSecondaryOrgAddress'>";
            apollo_wp_select(  array(
                'id' => ''. Apollo_DB_Schema::_APL_SECONDARY_ORG_ADDRESS .'['. Apollo_DB_Schema::_SECONDARY_ORG_ZIP .']',
                'label' => __( 'Secondary Zip', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'options'   => Apollo_App::getZipByTerritory(FALSE, $secondaryState, $secondaryCity, true),
                'class' => 'apl-territory-zipcode',
                'attr' => "data-wrapper = '.apl-secondary-org-address'"
            ) );
        echo '</div>';
        echo '</div>';

        /*end @ticket #18223: Turn on the secondary org address*/

        /** @Ticket #19640 */
        echo '<div class="tmp-address-wrapper" >';

        echo '<h4>'.__( 'Other Address', 'apollo' ) .'</h4>';

        echo '<div class="options_group event-box">';
        // Description
        apollo_wp_text_input(  array(
            'id' => ''. Apollo_DB_Schema::_APL_ORG_ADDRESS .'['. Apollo_DB_Schema::_APL_ORG_TMP_STATE .']',
            'label' => __( 'Other State', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
        ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
        // Description
        apollo_wp_text_input(  array(
            'id' => ''. Apollo_DB_Schema::_APL_ORG_ADDRESS .'['. Apollo_DB_Schema::_APL_ORG_TMP_CITY .']',
            'label' => __( 'Other City', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
        ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
        // Description
        apollo_wp_text_input(  array(
            'id' => ''. Apollo_DB_Schema::_APL_ORG_ADDRESS .'['. Apollo_DB_Schema::_APL_ORG_TMP_ZIP .']',
            'label' => __( 'Other Zip Code', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
        ) );
        echo '</div>';

        //Tri start move form meta-box-org-data
        // Phone
        echo '<div class="options_group event-box">';

        apollo_wp_text_input(  array(
            'id' => ''. Apollo_DB_Schema::_APL_ORG_DATA .'['. Apollo_DB_Schema::_ORG_PHONE .']',
            'label' => __( 'Phone', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => $requiredPhone ? 'required' : '',
            'required' => $requiredPhone ? true : false
        ) );
        echo '</div>';
        echo '</div>';

        // region
        echo '<div class="options_group event-box">';
        apollo_wp_select(  array(
            'id' => ''. Apollo_DB_Schema::_APL_ORG_ADDRESS .'['. Apollo_DB_Schema::_ORG_REGION .']',
            'label' => __( 'Region', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'options'   => Apollo_App::get_regions(),
            'display' => Apollo_App::showRegion() && ! Apollo_App::enableMappingRegionZipSelection(),
        ) );
        echo '</div>';

        // Fax
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_ORG_DATA. '['. Apollo_DB_Schema::_ORG_FAX .']',
            'label' => __( 'Fax', 'apollo' ),
            'desc_tip' => 'true',
            'description' => ""
        ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_ORG_LATITUDE,
            'label' => __( 'Latitude', 'apollo' ),
            'desc_tip' => 'true',
            'description' => ""
        ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_ORG_LONGITUDE,
            'label' => __( 'Longitude', 'apollo' ),
            'desc_tip' => 'true',
            'description' => ""
        ) );
        echo '</div>';

        // Email
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_ORG_DATA. '['. Apollo_DB_Schema::_ORG_EMAIL .']',
            'label' => __( 'Email', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => sprintf('apollo_input_email %s', $requiredEmail ? 'required' : ''),
            'required' => $requiredEmail ? true : false
        ) );
        echo '</div>';
        //Tri end move form meta-box-org-data

        /** @Ticket #15810 - Show map */
        echo '<div class="options_group event-box">';
        $browserKey = of_get_option(Apollo_DB_Schema::_GOOGLE_API_KEY_BROWSER);
        apollo_wp_google_map(array(
            'id' => 'apl-get-lat-long',
            'label' => __('Get latitude and longitude', 'apollo'),
            'attr' => array(
                'data-address-id = "'.Apollo_DB_Schema::_APL_ORG_ADDRESS .'['. Apollo_DB_Schema::_ORG_ADDRESS1 .']'.'"',
                'data-address2-id = "'.Apollo_DB_Schema::_APL_ORG_ADDRESS .'['. Apollo_DB_Schema::_ORG_ADDRESS2 .']'.'"',
                'data-state-id = "'.Apollo_DB_Schema::_APL_ORG_ADDRESS .'['. Apollo_DB_Schema::_ORG_STATE .']'.'"',
                'data-city-id = "'.Apollo_DB_Schema::_APL_ORG_ADDRESS .'['. Apollo_DB_Schema::_ORG_CITY .']'.'"',
                'data-zip-id = "'.Apollo_DB_Schema::_APL_ORG_ADDRESS .'['. Apollo_DB_Schema::_ORG_ZIP .']'.'"',
                'data-latitude-id = "'.Apollo_DB_Schema::_ORG_LATITUDE.'"',
                'data-longitude-id = "'.Apollo_DB_Schema::_ORG_LONGITUDE.'"',
                'data-google-api-key = "'. $browserKey .'"'
            )
        ));
        echo '</div>';
    }

	/**
	 * Save meta box data
	 */
	public static function save( $post_id, $post ) {

        $_data = $_POST[Apollo_DB_Schema::_APL_ORG_ADDRESS];
        /*@ticket #18223: Turn on the secondary org address*/
        $_data2 = $_POST[Apollo_DB_Schema::_APL_SECONDARY_ORG_ADDRESS];
        if(!isset($_data[Apollo_DB_Schema::_ORG_REGION])){
            $_data[Apollo_DB_Schema::_ORG_REGION] = Apollo_App::apollo_get_meta_data($post_id, Apollo_DB_Schema::_ORG_REGION, Apollo_DB_Schema::_APL_ORG_ADDRESS );
        }

        $city = isset($_data[Apollo_DB_Schema::_ORG_CITY]) ? $_data[Apollo_DB_Schema::_ORG_CITY] : '';
        $zip = isset($_data[Apollo_DB_Schema::_ORG_CITY]) ? $_data[Apollo_DB_Schema::_ORG_ZIP] : '';
        $region = isset($_data[Apollo_DB_Schema::_ORG_REGION]) ? $_data[Apollo_DB_Schema::_ORG_REGION] : '';

        $postLat = Apollo_App::clean_data_request($_REQUEST[Apollo_DB_Schema::_ORG_LATITUDE]);
        $postLng = Apollo_App::clean_data_request($_REQUEST[Apollo_DB_Schema::_ORG_LONGITUDE]);

        if ($org = get_org($post_id)) {
            if (!$postLat && !$postLng && $address = $org->get_full_address()) {
                $arrCoor = Apollo_Google_Coor_Cache::getCoorForVenue($address);
                $postLat = @$arrCoor['lat'];
                $postLng = @$arrCoor['lng'];
            }
        }

        update_apollo_meta( $post_id, Apollo_DB_Schema::_ORG_LATITUDE, $postLat );
        update_apollo_meta( $post_id, Apollo_DB_Schema::_ORG_LONGITUDE, $postLng );

        // Update latitude and longitude from ORG
        $org = get_org($post->ID);
        if( $businessID = $org->get_meta_data(Apollo_DB_Schema::_APL_ORG_BUSINESS)) {
            update_apollo_meta( $businessID, Apollo_DB_Schema::_BUSINESS_LATITUDE, $postLat );
            update_apollo_meta( $businessID, Apollo_DB_Schema::_BUSINESS_LONGITUDE, $postLng );
            update_apollo_meta( $businessID, Apollo_DB_Schema::_BUSINESS_CITY, $city );
            update_apollo_meta( $businessID, Apollo_DB_Schema::_BUSINESS_ZIP, $zip );
            update_apollo_meta( $businessID, Apollo_DB_Schema::_BUSINESS_REGION, $region );
        }

        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_ORG_ADDRESS, serialize( Apollo_App::clean_array_data($_data) ) );
        /*@ticket #18223: Turn on the secondary org address*/
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_SECONDARY_ORG_ADDRESS, serialize( Apollo_App::clean_array_data($_data2) ) );

        update_apollo_meta( $post_id, Apollo_DB_Schema::_ORG_ZIP, isset($_data[Apollo_DB_Schema::_ORG_ZIP]) ? $_data[Apollo_DB_Schema::_ORG_ZIP] : '' );
        update_apollo_meta( $post_id, Apollo_DB_Schema::_ORG_CITY, $city );
        update_apollo_meta( $post_id, Apollo_DB_Schema::_ORG_STATE, isset($_data[Apollo_DB_Schema::_ORG_STATE]) ? $_data[Apollo_DB_Schema::_ORG_STATE] : '' );

	}
}