<?php
/**
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Artist_Social
 */
class Apollo_Meta_Box_Artist_Social {

	/**
	 * Output the metabox
	 */
	public static function output( $post ) { 
        
        global $post, $thepostid;
		wp_nonce_field( 'apollo_artist_meta_nonce', 'apollo_artist_meta_nonce' );
        $thepostid = $post->ID;
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_ARTIST_DATA .'['. Apollo_DB_Schema::_APL_ARTIST_FB .']', 
                'label' => __( 'Facebook', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'class' => 'apollo_input_url',
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_ARTIST_DATA .'['. Apollo_DB_Schema::_APL_ARTIST_TW .']', 
                'label' => __( 'Twitter', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'class' => 'apollo_input_url',
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_ARTIST_DATA .'['. Apollo_DB_Schema::_APL_ARTIST_INS .']', 
                'label' => __( 'Instagram', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'class' => 'apollo_input_url',
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_ARTIST_DATA .'['. Apollo_DB_Schema::_APL_ARTIST_LK .']', 
                'label' => __( 'LinkedIn', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'class' => 'apollo_input_url',
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_ARTIST_DATA .'['. Apollo_DB_Schema::_APL_ARTIST_PR .']', 
                'label' => __( 'Pinterest', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'class' => 'apollo_input_url',
            ) );
        echo '</div>';
        
	}

}