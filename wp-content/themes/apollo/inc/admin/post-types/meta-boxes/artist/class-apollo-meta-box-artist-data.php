<?php
/**
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Artist_Data
 */
class Apollo_Meta_Box_Artist_Data {

	/**
	 * Output the metabox
	 */
	public static function output( $post ) {

        global $post, $thepostid, $wpdb;
		wp_nonce_field( 'apollo_artist_meta_nonce', 'apollo_artist_meta_nonce' );
        $thepostid = $post->ID;

        echo '<div class="options_group event-box">';
        // Description
        apollo_wp_text_input(  array(
            'id' => ''. Apollo_DB_Schema::_APL_ARTIST_DATA .'['. Apollo_DB_Schema::_APL_ARTIST_FNAME .']',
            'label' => __( 'First name', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
        ) );

        echo '</div>';

        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array(
                'id' => ''. Apollo_DB_Schema::_APL_ARTIST_DATA .'['. Apollo_DB_Schema::_APL_ARTIST_LNAME .']',
                'label' => __( 'Last name', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'required' => true,
            ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array(
                'id' => ''. Apollo_DB_Schema::_APL_ARTIST_DATA .'['. Apollo_DB_Schema::_APL_ARTIST_PHONE .']',
                'label' => __( 'Phone', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
            ) );
        echo '</div>';

        echo '<div class="options_group apl-cbbox-child">';
            // Description
            apollo_wp_checkbox(  array(
                'id' => ''. Apollo_DB_Schema::_APL_ARTIST_DATA .'['. Apollo_DB_Schema::_APL_ARTIST_DISPLAY_PHONE .']',
                'label' => __( 'Check here if you DO NOT want your Phone Number to appear on your Artist Profile page.', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'type' => 'checkbox', ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array(
                'id' => ''. Apollo_DB_Schema::_APL_ARTIST_DATA .'['. Apollo_DB_Schema::_APL_ARTIST_EMAIL .']',
                'label' => __( 'Email', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'class' => 'apollo_input_email',
            ) );
        echo '</div>';

        echo '<div class="options_group apl-cbbox-child">';
            // Description
            apollo_wp_checkbox(  array(
                'id' => ''. Apollo_DB_Schema::_APL_ARTIST_DATA .'['. Apollo_DB_Schema::_APL_ARTIST_DISPLAY_EMAIL .']',
                'label' => __( 'Check here if you DO NOT want your Email to appear on your Artist Profile page.', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'type' => 'checkbox', ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array(
                'id' => ''. Apollo_DB_Schema::_APL_ARTIST_DATA .'['. Apollo_DB_Schema::_APL_ARTIST_BLOGURL .']',
                'label' => __( 'Blog Url', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'class' => 'apollo_input_url',
            ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array(
                'id' => ''. Apollo_DB_Schema::_APL_ARTIST_DATA .'['. Apollo_DB_Schema::_APL_ARTIST_WEBURL .']',
                'label' => __( 'Web Url', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'class' => 'apollo_input_url',
            ) );
        echo '</div>';

        echo '<div class="options_group">';
        // Description
        apollo_wp_checkbox(  array(
            'id' => ''. Apollo_DB_Schema::_APL_ARTIST_DISPLAY_PUBLIC,
            'label' => __( 'Check this to show Public Artists in right column of Public Art form using for association tool purpose.', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'type' => 'checkbox', ) );
        echo '</div>';

        echo '<div class="options_group">';
        // Description
        apollo_wp_checkbox(  array(
            'id' => ''. Apollo_DB_Schema::_APL_ARTIST_DO_NOT_LINKED,
            'label' => __( 'Check this to hide Artist in Artist Directory.', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'type' => 'checkbox', ) );
        echo '</div>';



        $c_year = 1991;
        $end_y = date('Y') + 6;

        $opts = array();
        for ( $i = $c_year; $i < $end_y; $i++ ) {
            $opts[$i] = $i;
        }



        ?>
        <script>
            jQuery(function(){
                jQuery('#title').attr('disabled', 'true');
            });
        </script>

        <?php
	}

    /**
     * @Ticket @17029
     * @param $post
     */
    public static function outputIsMember( $post ) {
        global $post, $thepostid;
        wp_nonce_field( 'apollo_artist_meta_nonce', 'apollo_artist_meta_nonce' );
        $thepostid = $post->ID;
        echo '<div >';
        apollo_wp_checkbox(  array(
            'id' => Apollo_DB_Schema::_ARTIST_MEMBER_ONLY,
            'label' => __( 'If this option is enabled, all fields are available from the FE form. In contrast, only first name, last name, email, phone, city, artist category types, and primary image are available', 'apollo' ),
        ) );
        echo '</div>';

    }


    /**
     * @ticket #18310: 0002510: Artist Directory Plugin > New Artist option
     */
    public static function outputIsNewArtist() {
        echo '<div >';
        apollo_wp_checkbox(  array(
            'id' => Apollo_DB_Schema::_ARTIST_IS_NEW,
            'label' => __( 'Enable new artist', 'apollo' ),
        ) );
        echo '</div>';
    }

	/**
	 * Save meta box data
	 */
	public static function save( $post_id, $post ) {

        $_data = $_POST[Apollo_DB_Schema::_APL_ARTIST_DATA];

        $video_embed = isset( $_POST['video_embed'] ) ? $_POST['video_embed'] : '';
        $video_desc = isset( $_POST['video_desc'] ) ? $_POST['video_desc'] : '';

        $_arr_video = array();
        if ( $video_embed ) {
            $i = 0;
            foreach ( $video_embed as $v ) {
                if ( ! $v ) continue;
                $_arr_video[] = array( 'embed' => $v, 'desc' => isset( $video_desc[$i] ) ?  $video_desc[$i] : '' );
                $i++;
            }
        }
        $_data[Apollo_DB_Schema::_APL_ARTIST_VIDEO] = $_arr_video;

        $email_field = Apollo_DB_Schema::_APL_ARTIST_EMAIL;
        if ( isset( $_data[$email_field] ) && $_data[$email_field] ):
            if ( is_email( $_data[$email_field] ) ) {
                $_data[$email_field] = Apollo_App::clean_data_request( $_data[$email_field] );
            }
        endif;

        $url_check = array(
            Apollo_DB_Schema::_APL_ARTIST_BLOGURL => __( 'Blog URL', 'apollo' ),
            Apollo_DB_Schema::_APL_ARTIST_WEBURL => __( 'Website', 'apollo' ),
            Apollo_DB_Schema::_APL_ARTIST_FB => __( 'Facebook', 'apollo' ),
            Apollo_DB_Schema::_APL_ARTIST_LK => __( 'LinkedIn', 'apollo' ),
            Apollo_DB_Schema::_APL_ARTIST_TW => __( 'Twitter', 'apollo' ),
            Apollo_DB_Schema::_APL_ARTIST_INS => __( 'Instagram', 'apollo' ),
            Apollo_DB_Schema::_APL_ARTIST_PR => __( 'Pinterest', 'apollo' ),
        );

        foreach( $url_check as $url_field => $l ) {
            if ( isset( $_data[$url_field] ) && $_data[$url_field] ):
                if ( filter_var( $_data[$url_field], FILTER_VALIDATE_URL ) ) {
                    $_data[$url_field] = Apollo_App::clean_data_request( $_data[$url_field] );
                }
            endif;
        }

        global $wpdb;
        $name = sanitize_text_field(stripcslashes( $_data[Apollo_DB_Schema::_APL_ARTIST_FNAME] ) . ' ' . stripslashes( $_data[Apollo_DB_Schema::_APL_ARTIST_LNAME] ) );
        $wpdb->update($wpdb->posts, array(
                'post_title' => $name ? $name : __( 'Auto Draft' )
            ),
            array(
                'ID' => $post_id
            )
        );

        $otherKeys = array(
            Apollo_DB_Schema::_APL_ARTIST_LNAME =>  isset($_data[Apollo_DB_Schema::_APL_ARTIST_LNAME]) ? $_data[Apollo_DB_Schema::_APL_ARTIST_LNAME] : '',
            Apollo_DB_Schema::_APL_ARTIST_DATA  => Apollo_App::serialize( Apollo_App::clean_array_data( $_data ) ),
            Apollo_DB_Schema::_APL_ARTIST_DISPLAY_PUBLIC    => isset($_POST[Apollo_DB_Schema::_APL_ARTIST_DISPLAY_PUBLIC]) ? Apollo_App::clean_data_request($_POST[Apollo_DB_Schema::_APL_ARTIST_DISPLAY_PUBLIC]) : '',
            Apollo_DB_Schema::_APL_ARTIST_DO_NOT_LINKED => isset($_POST[Apollo_DB_Schema::_APL_ARTIST_DO_NOT_LINKED]) ? Apollo_App::clean_data_request($_POST[Apollo_DB_Schema::_APL_ARTIST_DO_NOT_LINKED]) : '',

            /*@ticket #17079: 0002366: wpdev19 - [Artist] Separate member artists and normal artists in two tabs*/
            Apollo_DB_Schema::_ARTIST_MEMBER_ONLY   => isset($_POST[Apollo_DB_Schema::_ARTIST_MEMBER_ONLY]) ? Apollo_App::clean_data_request($_POST[Apollo_DB_Schema::_ARTIST_MEMBER_ONLY]) : '',

            /*@ticket #18310: 0002510: Artist Directory Plugin > New Artist option*/
            Apollo_DB_Schema::_ARTIST_IS_NEW   => isset($_POST[Apollo_DB_Schema::_ARTIST_IS_NEW]) ? Apollo_App::clean_data_request($_POST[Apollo_DB_Schema::_ARTIST_IS_NEW]) : '',
        );
        Apollo_App::storeMultipleMetaData($post_id, $otherKeys);
    }
}
