<?php
/**
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Artist_Question
 */
class Apollo_Meta_Box_Artist_Question {

	/**
	 * Output the metabox
	 */
	public static function output( $post ) { 
        
        global $post, $thepostid;
		wp_nonce_field( 'apollo_artist_meta_nonce', 'apollo_artist_meta_nonce' );
        $thepostid = $post->ID;
        $ques = @unserialize(get_option( Apollo_DB_Schema::_APL_ARTIST_OPTIONS ));
        $answer = @unserialize( get_apollo_meta( $thepostid, Apollo_DB_Schema::_APL_ARTIST_QUESTIONS, TRUE ) );
        

        if ( $ques ):
            $i = 0;
            foreach ( $ques as $k => $q ):
            if ( ! $q ) continue;
            $i++;
            $pre_label = sprintf( __( 'Q%s. ', 'apollo' ), $i ); 
            echo '<div class="options_group event-box q-full">';
            ?>
                <p class="form-field">
                    <label><?php echo $pre_label. $q ?></label>
                    <textarea class="short" name="<?php echo Apollo_DB_Schema::_APL_ARTIST_QUESTIONS ?>[]"rows="2" cols="20"><?php echo $answer && isset( $answer[$i-1] ) ? $answer[$i-1] : '' ?></textarea> 
                </p>
            <?php
            echo '</div>';

            endforeach;
        endif;
	}
    
    public static function save($post_id, $post) {
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_ARTIST_QUESTIONS, serialize( Apollo_App::clean_array_data( $_POST[Apollo_DB_Schema::_APL_ARTIST_QUESTIONS] ) ) );
    }
}