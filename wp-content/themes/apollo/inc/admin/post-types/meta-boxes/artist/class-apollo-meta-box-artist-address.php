<?php
/**
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Artist_Address
 */
class Apollo_Meta_Box_Artist_Address {

	/**
	 * Output the metabox
	 */
	public static function output( $post ) { 
        
        global $post, $thepostid,$pagenow;
		wp_nonce_field( 'apollo_artist_meta_nonce', 'apollo_artist_meta_nonce' );
        $thepostid = $post->ID;
        $stateVal = '';
        $cityVal = '';
        if( $pagenow == 'post.php' ){
            $stateVal = Apollo_App::apollo_get_meta_data( $thepostid, Apollo_DB_Schema::_APL_ARTIST_ADDRESS .'['. Apollo_DB_Schema::_APL_ARTIST_STATE .']' );
            $cityVal = Apollo_App::apollo_get_meta_data( $thepostid, Apollo_DB_Schema::_APL_ARTIST_ADDRESS .'['. Apollo_DB_Schema::_APL_ARTIST_CITY .']' );
        } else {
            $list_states = Apollo_App::getListState();
            $default_states =  of_get_option(Apollo_DB_Schema::_APL_DEFAULT_STATE);
            $cityVal = of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY);
            if( $default_states && in_array($default_states,$list_states) ) {
                $stateVal = $default_states;
            } else if(count($list_states) == 1) {
                $stateVal = key($list_states);
            }
        }

        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_ARTIST_ADDRESS .'['. Apollo_DB_Schema::_APL_ARTIST_STREET .']', 
                'label' => __( 'Street', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
            ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_select(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_ARTIST_ADDRESS .'['. Apollo_DB_Schema::_APL_ARTIST_STATE .']', 
                'label' => __( 'State', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'options'   => Apollo_App::getStateByTerritory(),
                'class' => 'apl-territory-state',
                'value' => $stateVal,
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';

            apollo_wp_select(  array(
                'id' => ''. Apollo_DB_Schema::_APL_ARTIST_ADDRESS .'['. Apollo_DB_Schema::_APL_ARTIST_CITY .']',
                'label' => __( 'City', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'options'   => Apollo_App::getCityByTerritory(false, $stateVal, true),
                'class' => 'apl-territory-city',
                'value' => $cityVal,
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_select(  array(
                'id' => ''. Apollo_DB_Schema::_APL_ARTIST_ADDRESS .'['. Apollo_DB_Schema::_APL_ARTIST_ZIP .']',
                'label' => __( 'Zip', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'options'   => Apollo_App::getZipByTerritory(FALSE, $stateVal, $cityVal, true),
                'class' => 'apl-territory-zipcode'
            ) );
        echo '</div>';

        /** @Ticket #19640 */
        echo '<div class="tmp-address-wrapper" >';

        echo '<h4>'.__( 'Other Address', 'apollo' ) .'</h4>';
        echo '<div class="options_group event-box">';
        // Description
        apollo_wp_text_input(  array(
            'id' => ''. Apollo_DB_Schema::_APL_ARTIST_ADDRESS .'['. Apollo_DB_Schema::_APL_ARTIST_TMP_STATE .']',
            'label' => __( 'Other State', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
        ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
        // Description
        apollo_wp_text_input(  array(
            'id' => ''. Apollo_DB_Schema::_APL_ARTIST_ADDRESS .'['. Apollo_DB_Schema::_APL_ARTIST_TMP_CITY .']',
            'label' => __( 'Other City', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
        ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
        // Description
        apollo_wp_text_input(  array(
            'id' => ''. Apollo_DB_Schema::_APL_ARTIST_ADDRESS .'['. Apollo_DB_Schema::_APL_ARTIST_TMP_ZIP .']',
            'label' => __( 'Other Zip Code', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
        ) );
        echo '</div>';
        echo '</div>';

        // region
        echo '<div class="options_group event-box">';
        apollo_wp_select(  array(
            'id' => ''. Apollo_DB_Schema::_APL_ARTIST_ADDRESS .'['. Apollo_DB_Schema::_APL_ARTIST_REGION .']',
            'label' => __( 'Region', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'options'   => Apollo_App::get_regions(),
            'display' => Apollo_App::showRegion(),
        ) );
        echo '</div>';

        echo '<div class="options_group  apl-cbbox-child">';
            // Description
            apollo_wp_checkbox(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_ARTIST_DATA .'['. Apollo_DB_Schema::_APL_ARTIST_DISPLAY_ADDRESS .']', 
                'label' => __( 'Check here if you DO NOT want your Address to appear on your Artist Profile page.', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'type' => 'checkbox', ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
        // Description
        apollo_wp_text_input(  array(
                'id' => ''. Apollo_DB_Schema::_APL_ARTIST_ADDRESS .'['. Apollo_DB_Schema::_APL_ARTIST_COUNTRY .']',
                'label' => __( 'County', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'required'  => of_get_option(Apollo_DB_Schema::_ARTIST_ENABLE_COUNTY_REQUIREMENT) ? true : '',
            ) );
        echo '</div>';

	}

	/**
	 * Save meta box data
	 */
	public static function save( $post_id, $post ) {
      
        $_data = $_POST[Apollo_DB_Schema::_APL_ARTIST_ADDRESS];
        if(!isset($_data[Apollo_DB_Schema::_APL_ARTIST_REGION])){
            $_data[Apollo_DB_Schema::_APL_ARTIST_REGION] = Apollo_App::apollo_get_meta_data($post_id, Apollo_DB_Schema::_APL_ARTIST_REGION, Apollo_DB_Schema::_APL_ARTIST_ADDRESS );
        }
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_ARTIST_CITY, $_data[Apollo_DB_Schema::_APL_ARTIST_CITY] );
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_ARTIST_ZIP, $_data[Apollo_DB_Schema::_APL_ARTIST_ZIP] );
        /** @Ticket #13028 */
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_ARTIST_STATE, $_data[Apollo_DB_Schema::_APL_ARTIST_STATE] );

        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_ARTIST_ADDRESS, serialize( Apollo_App::clean_array_data( $_data ) ) );
	}
}