<?php
/**
 * Artist Images
 *
 * Display the artist images meta box.
 *
 * @author 		vulh
 * @category 	Admin
 * @package 	inc/admin/post-types/meta-boxes
 * 
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/**
 * Apollo_Meta_Box_Artist_Images
 */
class Apollo_Meta_Box_Artist_Images {

	/**
	 * Output the metabox
	 */
	public static function output( $post ) {
        $tags_html_default = '';
        if(has_filter('apl_pbm_admin_render_select_tags')) {
            $tags_html_default = apply_filters('apl_pbm_admin_render_select_tags', [
                'post_id' => -1,
                'tags_name' => 'input-select-tag-template',
                'type' => 'post',
                'media_id' => '',
                'wrap_html' => array(
                    'first' => '<div class="hidden" data-tag-name="apl_pbm_selected_tags_gallery" id="container-select-tags-field">',
                    'last' => '</div>'
                )
            ]);
        }
        ?>
		<div id="event_images_container" class="<?php echo $tags_html_default ? 'pbm-gallery' : '' ?>" data-title="<?php _e('Select images','apollo');?>" data-text="<?php _e('Use selected images','apollo');?>">
			<ul class="event_images">
				<?php

                    $image_gallery = get_apollo_meta( $post->ID, Apollo_DB_Schema::_APL_ARTIST_IMAGE_GALLERY, true );
					$attachments = array_filter( explode( ',', $image_gallery ) );

					if ( $attachments )
						foreach ( $attachments as $attachment_id ) {
                            $tags_html = '';
                            if(has_filter('apl_pbm_admin_render_select_tags')) {
                                $tags_html = apply_filters('apl_pbm_admin_render_select_tags', [
                                    'post_id' => $post->ID,
                                    'tags_name' => "apl_pbm_selected_tags_gallery[$attachment_id]",
                                    'type' => 'photo',
                                    'media_id' => $attachment_id,
                                ]);
                            }

							echo '<li class="image" data-attachment_id="' . esc_attr( $attachment_id ) . '">
								' . wp_get_attachment_image( $attachment_id, 'thumbnail' ) . '
								<ul class="actions">
									<li><a href="#" class="delete tips" data-tip="' . __( 'Delete image', 'apollo' ) . '">' . __( 'Delete', 'apollo' ) . '</a></li>
								</ul>' .$tags_html . '
							</li>';
						}
				?>
			</ul>

			<input type="hidden" id="event_image_gallery" name="image_gallery" value="<?php echo esc_attr( $image_gallery ); ?>" />

            <?php echo $tags_html_default ?>

		</div>
		<p class="add_event_images hide-if-no-js">
			<a href="#" data-choose="<?php _e( 'Add Images to Gallery', 'apollo' ); ?>" data-update="<?php _e( 'Add to gallery', 'apollo' ); ?>" data-delete="<?php _e( 'Delete image', 'apollo' ); ?>" data-text="<?php _e( 'Delete', 'apollo' ); ?>"><?php _e( 'Add gallery images', 'apollo' ); ?></a>
		</p>
		<?php
	}

	/**
	 * Save meta box data
	 */
	public static function save( $post_id, $post ) {
		$attachment_ids = array_filter( explode( ',', sanitize_text_field( $_POST['image_gallery'] ) ) );
		update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_ARTIST_IMAGE_GALLERY, implode( ',', $attachment_ids ) );
	}
}