<?php
/**
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Artist_Gallery
 */
class Apollo_Meta_Box_Artist_Timeline {

	/**
	 * Output the metabox
	 */
	public static function output( $post ) { 
        
        global $post, $thepostid;
		wp_nonce_field( 'apollo_artist_meta_nonce', 'apollo_artist_meta_nonce' );
        $thepostid = $post->ID;
        ?>
        <div id="timeline-event-data" >
            <div class="timeline-wrapper">
                <?php
                // DEFAULT DATA
                $current_year = date('Y');
                $arr_year = array_combine(range($current_year, 1980), range($current_year, 1980));
                $arr_year = array('' => __('Select Year', 'apollo')) + $arr_year;

                $arr_award = array(
                );

                ?>

                <script type="text/plain" id="timeline-default-template">
                    <div class="count apollo-artist-timeline">
                    <div class="options_group event-box">
                        <p class="form-field timeline_embed_field ">
                            <label><?php _e( 'Year', 'apollo' ) ?></label>
                                <select name="year_award[]" data-mygroup='year_award' data-ride="unique-check" data-alert = "<?php _e('Look like you have already choice this year!','apollo') ?>" >
                                    <?php foreach($arr_year as $y => $year ): ?>
                                        <option value="<?php echo $y ?>" ><?php echo $year ?></option>
                                    <?php endforeach; ?>
                                </select>

                        </p>
                    </div>

                    <div class="options_group event-box artist-timeline-desc">
                        <p class="form-field timeline_desc_field ">
                            <label><?php _e( 'Award Category', 'apollo' ) ?></label>
                                <textarea class="wpc-75" name="award_category[]" placeholder="" rows="3" cols="20"></textarea>
                        </p>
                    </div>
                    <div style="display: block;" data-confirm="<?php _e('Are you sure to remove this record ?','apollo');?>" class="del"><i class="fa fa-times"></i></div>
                    </div>
                </script>
                <div class="apollo-artist-timeline-list">
                <?php

                // GET DATA - override $arr_award
                global $wpdb;
                $sql = "select year_award, award_category from {$wpdb->{Apollo_Tables::_APL_TIMELINE}} where p_id = '%s' and type = 'artist' order by year_award ASC";
                $arr_results = $wpdb->get_results($wpdb->prepare($sql, array(
                            (int)$thepostid
                        )));

                if(!empty($arr_results)) {
                    $arr_award = $arr_results;
                }
                // FETCH DATA
                if(!empty($arr_award)):
                $i = 0;
                foreach($arr_award as $aw):

                ?>


                    <div class="count apollo-artist-timeline">

                        <div class="options_group event-box">
                            <p class="form-field timeline_embed_field ">
                                <label><?php _e( 'Year', 'apollo' ) ?></label>
                                <select name="year_award[]" data-mygroup='year_award' data-ride="unique-check" data-alert = "<?php _e('Look like you have already choice this year!','apollo') ?>">
                                    <?php foreach($arr_year as $y => $year ): ?>
                                        <option value="<?php echo $y ?>" <?php selected($aw->year_award, $y) ?>><?php echo $year ?></option>
                                    <?php endforeach; ?>
                                </select>

                            </p>
                        </div>

                        <div class="options_group event-box artist-timeline-desc">
                            <p class="form-field timeline_desc_field ">
                                <label><?php _e( 'Award Category', 'apollo' ) ?></label>
                                <textarea class="wpc-75" name="award_category[]" placeholder="" rows="3" cols="20"><?php echo $aw->award_category ?></textarea>

                            </p>
                        </div>
                        <div style="display: block;" data-confirm="Are you sure to remove this record ?" class="del"><i class="fa fa-times"></i></div>


                </div>

                <?php
                    $i++;
                    endforeach;
                    endif;
                    ?>
                </div><!-- end apollo-artist-timeline-list -->
            </div>

            <input type="button" class="button button-primary button-large apollo-btn-right"
                   data-ride="shadow-man"
                   data-template="#timeline-default-template"
                   data-append-to = ".apollo-artist-timeline-list:append"
                   value="<?php _e( 'Add More', 'apollo' ) ?>" />

            
            <br/><br/>
        </div>
    
        <?php 
	}

    public static function save( $post_id, $post ) {

        $arr_year_award = isset($_POST['year_award']) ? $_POST['year_award'] : array();
        $arr_year_award = array_filter($arr_year_award);


        global $wpdb;
        $arr_award_category = isset($_POST['award_category']) ? $_POST['award_category'] : array();

        // UPDATE TO DB
        // build data for insert
        if(!empty($arr_year_award)) {
            $arr_param_for_query = array();
            $value_for_query = array();
            foreach ($arr_year_award as $index => $year) {
                $year = trim($year);
                if (empty($year)) continue;

                $arr_param_for_query[] = $post_id;
                $arr_param_for_query[] = $year;
                $arr_param_for_query[] = $arr_award_category[$index];
                $arr_param_for_query[] = Apollo_DB_Schema::_ARTIST_PT;

                $value_for_query[] = '(%s, %s, %s, %s)';
            }

            if (!empty($arr_param_for_query)) {
                $sql = "insert into {$wpdb->{Apollo_Tables::_APL_TIMELINE}} (p_id, year_award, award_category, type)" . ' VALUES ' . implode(
                        ", ",
                        $value_for_query
                    ) . ' on duplicate key update award_category = VALUES(award_category) ;';

                $result1 = $wpdb->query($wpdb->prepare($sql, $arr_param_for_query));
                if (self::processError($result1)) {
                    return;
                }
            }
        }

        /* Delete other year */
        $condition = array(
            "p_id = '%s'"
        );
        $condition[] = 'type = "'.Apollo_DB_Schema::_ARTIST_PT. '"';
        $arr_params = array($post_id);

        if(!empty($arr_year_award))  {
            $arr_year_award = array_filter($arr_year_award); // clean it up
            $condition[] = 'year_award not in (' . implode(", ",array_fill(0, count($arr_year_award), '%s')) .')';
            $arr_params = array_merge(array($post_id), $arr_year_award);
        }

        $sql = "delete from {$wpdb->{Apollo_Tables::_APL_TIMELINE}} where " . implode(' AND ', $condition) . " ;";


        $result2 = $wpdb->query($wpdb->prepare($sql, $arr_params));

        if(self::processError($result2)) {
            return;
        }


    }

    private static  function processError($result2) {
        if($result2 === false || $result2 === null) { // there are some error need notice
            $msg = __('There are something wrong. Cannot save artist award', 'apollo');
            global $wpdb;
            if(isset($wpdb->last_error)) {
                $msg = $wpdb->last_error;
            }
            $_SESSION['messages']['artist-timeline'][] = array(
                'message' =>$msg,
                'error' => true
            );

            return true;
        }

        return false;
    }
}