<?php
/**
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Artist_Address
 */
class Apollo_Meta_Box_Artist_Options {

	/**
	 * Output the metabox
	 */
	public static function output( $post ) { 
        
        global $post, $thepostid;
		wp_nonce_field( 'apollo_artist_meta_nonce', 'apollo_artist_meta_nonce' );
        $thepostid = $post->ID;
        
        echo '<div class="options_group">';

            apollo_wp_text_input(  array( 
               'id' => Apollo_DB_Schema::_APL_ARTIST_DATA. '['. Apollo_DB_Schema::_APL_ARTIST_ANOTHER_CAT .']', 
               'label' => __( 'Other type not in Artist Category', 'apollo' ), 
               'desc_tip' => 'true', 
               'description' => '', 
               'class' => 'full',
               'type'  => 'text'
           ) );
        echo '</div>';
        
        echo '<div class="options_group">';
            apollo_wp_text_input(  array( 
               'id' => Apollo_DB_Schema::_APL_ARTIST_DATA. '['. Apollo_DB_Schema::_APL_ARTIST_ANOTHER_STYLE .']', 
               'label' => __( 'Other type not in Artist style', 'apollo' ), 
               'desc_tip' => 'true', 
               'description' => '', 
               'class' => 'full',
               'type'  => 'text'
           ) );
        echo '</div>';
        
        echo '<div class="options_group">';
            apollo_wp_text_input(  array( 
               'id' => Apollo_DB_Schema::_APL_ARTIST_DATA. '['. Apollo_DB_Schema::_APL_ARTIST_ANOTHER_MEDIUM .']', 
               'label' => __( 'Other type not in Artist medium', 'apollo' ), 
               'desc_tip' => 'true', 
               'description' => '', 
               'class' => 'full',
               'type'  => 'text'
           ) );
        echo '</div>';
        
	}

}