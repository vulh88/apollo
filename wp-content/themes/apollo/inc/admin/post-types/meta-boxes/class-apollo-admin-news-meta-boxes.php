<?php
/**
 * Apollo Org Meta Boxes
 *
 * Sets up the write panels used by orgs (custom post types)
 *
 * @author 		vulh
 * @category 	Admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

include 'class-apl-ad-module-meta-boxes.php';

/**
 * Apollo_Admin_News_Meta_Boxes
 */
class Apollo_Admin_News_Meta_Boxes extends Apl_Ad_Module_Meta_Boxes {

	/**
	 * Constructor
	 */
	public function __construct() {

        parent::__construct( Apollo_DB_Schema::_NEWS_PT );

        /**
         * if remove slug box, the permalink of post cant change.
         */
        add_action( 'add_meta_boxes', array( $this, 'remove_meta_boxes'), 30 );
        add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 30 );
		add_action( 'save_post', array( $this, 'save_meta_boxes' ), 1, 2 );

        /**
         * @ticket #19232: News Module - Sponsored Content of the news
         */
        require_once APOLLO_ADMIN_DIR . '/post-types/meta-boxes/common/class-apollo-meta-box-post-sponsored-content.php';
        add_action('save_post', "Apollo_Meta_Box_Post_Sponsored_Content::save", 10, 2);
        if (defined('APOLLO_BLOG_NEWS_SITE_ENABLE') && APOLLO_BLOG_NEWS_SITE_ENABLE == get_current_blog_id()) {
            add_action('save_post', "Apollo_Meta_Box_Common::updateCopyToAnotherSite", 99, 2);
        }

        add_action( $this->process_data_action, 'Apollo_Meta_Box_Common::updateChangeAuthor', 10, 2 );
    }

	/**
	 * Add Apollo Meta boxes
	 */
	public function add_meta_boxes() {

        add_meta_box( 'apollo-news-meta-nonce', 'Meta nonce',
            'Apollo_Admin_News_Meta_Boxes::addMetaNonce', $this->type, 'side' );


	    /*@ticket #18377:  Add the option to change and remove Author*/
        $currentUser = get_current_user_id();
        if (is_super_admin($currentUser) || user_can($currentUser, 'manage_options')) {
            add_meta_box( 'apollo-news-author', __( 'Change Author', 'apollo' ),
                'Apollo_Meta_Box_Common::outputChangeAuthor', $this->type, 'side' );
        }

        if (defined('APOLLO_BLOG_NEWS_SITE_ENABLE') && APOLLO_BLOG_NEWS_SITE_ENABLE == get_current_blog_id()) {
            add_meta_box('apollo-news-data-to-another-site', __('Copy Data', 'apollo'), 'Apollo_Meta_Box_Common::outputCopyToAnotherSite', 'news', 'normal', 'high');
        }

        /**
         * @ticket #19232: News Module - Sponsored Content of the news
         */
        add_meta_box('apollo-sponsored-content', __('Sponsored Content', 'apollo'), 'Apollo_Meta_Box_Post_Sponsored_Content::output', 'news', 'normal', 'high');
    }

    public static function addMetaNonce(){
        wp_nonce_field( 'apollo_news_meta_nonce', 'apollo_news_meta_nonce' );
    }
}

new Apollo_Admin_News_Meta_Boxes();
