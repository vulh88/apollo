<?php
/**
 * Apollo Spotlight Meta Boxes
 *
 * Sets up the write panels used by venues (custom post types)
 *
 * @author 		vulh
 * @category 	Admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

include 'class-apl-ad-module-meta-boxes.php';

/**
 * Apollo_Admin_Venue_Meta_Boxes
 */
class Apollo_Admin_Spotlight_Meta_Boxes extends Apl_Ad_Module_Meta_Boxes {
    
	/**
	 * Constructor
	 */
	public function __construct() {
        
        parent::__construct( Apollo_DB_Schema::_SPOT_PT );
        
        $this->require_meta_boxes();
        
		add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 30 );

		add_action( 'save_post', array( $this, 'save_meta_boxes' ), 1, 2 );
        
        // Save Meta Boxes
		add_action( $this->process_data_action, 'Apollo_Meta_Box_Spitlight_Data::save', 10, 2 );
        
        // Error handling (for showing errors from meta boxes on next page load)
		add_action( 'admin_notices', array( $this, 'output_errors' ) );
        add_action( 'shutdown', array( $this, 'save_errors' ) );
        
	}
    
    public function require_meta_boxes() {
        require_once 'spotlight/class-apollo-meta-box-spotlight-data.php';
    }

	/**
	 * Add Apollo Meta boxes
	 */
	public function add_meta_boxes() {
        
        add_meta_box( 'apollo-spotlight-link', __( 'Link', 'apollo' ),
            'Apollo_Meta_Box_Spitlight_Data::output', $this->type, 'normal', 'high' );

        /** @Ticket #13247 */
        add_meta_box( 'apollo-spotlight-date', __( 'Date Range', 'apollo' ),
            'Apollo_Meta_Box_Spitlight_Data::date_range_output', $this->type, 'normal', 'high' );

        /** @Ticket #17212 */
        add_meta_box( 'apollo-spotlight-display-options', __( 'Display option', 'apollo' ),
            'Apollo_Meta_Box_Spitlight_Data::spotlight_display_options', $this->type, 'side', 'high' );

        $currentTheme = function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '';

        /** @Ticket #17249 */
        if($currentTheme === "octave-child"){
            add_meta_box( 'apollo-spotlight-display-setting', __( 'Display setting', 'apollo' ),
                'Apollo_Meta_Box_Spitlight_Data::spotlight_display_setting', $this->type, 'side', 'high' );
        }

        if ( $currentTheme == APL_SM_DIR_THEME_NAME) {
            add_meta_box( 'apollo-spotlight-color-picker', __( 'Arrows button', 'apollo' ),
                'Apollo_Meta_Box_Spitlight_Data::color_fields', $this->type, 'normal', 'high' );
            add_meta_box( 'apollo-spotlight-data', __( 'Gallery', 'apollo' ),
                'Apollo_Meta_Box_Spitlight_Data::images_box', $this->type, 'side', 'low' );
        }

    }
}

new Apollo_Admin_Spotlight_Meta_Boxes();
