<?php
/**
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_IFrame_Search_Widget_Setting
 */
class Apollo_Meta_Box_IFrame_Search_Widget_Setting {

    const INDIVIDUAL_EVENTS_PAGESIZE = 40;

    /**
     * Output the metabox
     */
    public static function output( $post ) {
        global $post, $thepostid, $wpdb;
        wp_nonce_field( 'apollo_iframe_search_widget_settings_meta_nonce', 'apollo_iframe_search_widget_settings_meta_nonce' );
        $thepostid = $post->ID;
        $currentDomainName = get_site_url();
        $isEnabledVenueModule = Apollo_App::check_avaiable_module(Apollo_DB_Schema::_VENUE_PT);
        if($isEnabledVenueModule){
            echo '<div class="options_group clear wrap-blk ct-full-width">';
                echo '<div class="options_group clear ct-selection-box">';
                apollo_wp_radio(  array(
                    'id' => APL_Iframe_Search_Widget_Const::META_ENABLE_VENUE,
                    'label' => __( 'Enable Display Venue', 'apollo' ),
                    'class' => 'iframe-search-widget-enable',
                    'value' => Apollo_Admin_Iframe_Search_Widget_Meta_Boxes::getIFrameSearchWidgetDataByKey($thepostid,APL_Iframe_Search_Widget_Const::META_ENABLE_VENUE, 'ON'),
                    'options' => array('ON' => 'ON',
                        'OFF' => 'OFF'
                    )
                ) );
                echo '</div>';
            $class_venues =  Apollo_Admin_Iframe_Search_Widget_Meta_Boxes::getIFrameSearchWidgetDataByKey($thepostid,APL_Iframe_Search_Widget_Const::META_ENABLE_VENUE) == 'OFF'
                ? 'style=" display:none; " ' : "";
            $venues = self::getVenues();
            $allVenues = Apollo_App::get_list_post_type_items( Apollo_DB_Schema::_VENUE_PT );
            $allVenuesIDArr = array();
            if ($allVenues) {
                foreach ($allVenues as $v ){
                    $allVenuesIDArr[] = $v->ID;
                }
            }
            $totalVenues = count($allVenues);

            if (!empty($venues)) {
                echo '<div id="venue-area-selection" class="options_group clear '.APL_Iframe_Search_Widget_Const::META_ENABLE_VENUE.' " '.$class_venues.' >';
                    echo '<div class="options_group ">';
                    apollo_wp_checkbox(  array(
                        'id' => APL_Iframe_Search_Widget_Const::META_VENUE.'_all',
                        'class' => 'meta_checkbox_all',
                        'label' => __( 'Select All Venues', 'apollo' ),
                        'desc_tip' => 'true',
                        'description' => "",
                        'name' => APL_Iframe_Search_Widget_Const::META_VENUE,
                        'type' => 'checkbox', ) );
                    echo '</div>';
                    echo '<div class="wrap-ddl">';
                        echo '<label class="apl-multi-choice ct-lb-mtc">'. __('Venues','apollo') .'</label>';
                        echo '<div class="wrap-ddl-blk venue-section-iframesw">';
                        $selectedValue = Apollo_Admin_Iframe_Search_Widget_Meta_Boxes::getIFrameSearchWidgetDataByKey($thepostid,APL_Iframe_Search_Widget_Const::META_VENUE,array());
                        if ( $selectedValue ) {
                            $_selectedVenues = !is_array($selectedValue) ? maybe_unserialize($selectedValue) : $selectedValue;
                        }else{
                            $_selectedVenues = array();
                        }
                        $finalSelectedVenues = !empty($_selectedVenues) ? implode(',',$_selectedVenues) : '';
                        $allVenuesIDArr = !empty($allVenuesIDArr) ? implode(',',$allVenuesIDArr) : '';
                        echo ' <input  id="iframesw-venues"  name="'.APL_Iframe_Search_Widget_Const::META_VENUES .'" type="hidden" value="'.$finalSelectedVenues.'" />';
                        echo ' <input  id="all_venues_id"   type="hidden" value="'.$allVenuesIDArr.'" />';
                        $index = 0;
                        foreach ( $venues as $k_choice => $choice ):
                            // Description
                            apollo_wp_multi_checkbox(  array(
                                'id'            => ''. APL_Iframe_Search_Widget_Const::META_VENUE . '-' .$index ,
                                'name'          => ''. APL_Iframe_Search_Widget_Const::META_VENUE . '[]',
                                'label'         => $choice,
                                'desc_tip'      => 'true',
                                'description'   => '',
                                'type'          => 'checkbox',
                                'default'       => false,
                                'value'			=> $_selectedVenues,
                                'cbvalue'       => $k_choice ) );
                            $index += 1;
                        endforeach;
                        echo '</div>';
                        echo '<div style="text-align: center;">';
                            echo '<span class="spinner" style=" float: none; margin-top: 10px; visibility: visible; display: none;"></span>';
                        echo '</div>';
                    echo '</div>';
                    echo ' <input  id="iframesw-show-more-venues" type="hidden" data-total-venue="'.$totalVenues .'"  data-selected-venues="'.$finalSelectedVenues.'" />';
                echo '</div>';
            }
            echo '</div>';
            echo "<hr/>";
        }

        if($isEnabledVenueModule){
            echo '<div class="options_group clear wrap-blk ct-full-width">';
                echo '<div class="options_group clear ct-selection-box">';
                apollo_wp_radio(  array(
                    'id' => APL_Iframe_Search_Widget_Const::META_ENABLE_CITIES,
                    'class' => 'iframe-search-widget-enable',
                    'label' => __( 'Enable Display Cites', 'apollo' ),
                    'value' => Apollo_Admin_Iframe_Search_Widget_Meta_Boxes::getIFrameSearchWidgetDataByKey($thepostid,APL_Iframe_Search_Widget_Const::META_ENABLE_CITIES,'ON'),
                    'options' => array('ON' => 'ON',
                        'OFF' => 'OFF'
                    )
                ) );
                echo '</div>';

            $class_cities =  Apollo_Admin_Iframe_Search_Widget_Meta_Boxes::getIFrameSearchWidgetDataByKey($thepostid,APL_Iframe_Search_Widget_Const::META_ENABLE_CITIES) == 'OFF'
                ? 'style=" display:none; " ' : "";
            $cities = self::getCities();
            if ( !empty($cities) ){

                echo '<div id="city-area-selection" class="options_group clear '.APL_Iframe_Search_Widget_Const::META_ENABLE_CITIES.'" '.$class_cities.'>';
                    echo '<div class="options_group ">';
                    apollo_wp_checkbox(  array(
                        'id' => APL_Iframe_Search_Widget_Const::META_CITIES.'_all',
                        'class' => 'meta_checkbox_all',
                        'label' => __( 'Select All Cities', 'apollo' ),
                        'desc_tip' => 'true',
                        'description' => "",
                        'name' => APL_Iframe_Search_Widget_Const::META_CITIES,
                        'type' => 'checkbox', ) );
                    echo '</div>';
                    echo '<div class="wrap-ddl">';
                        echo '<label class="apl-multi-choice ct-lb-mtc">'. __('Cities','apollo') .'</label>';
                        echo '<div class="wrap-ddl-blk">';
                        $index = 0;
                        $selectedValue = Apollo_Admin_Iframe_Search_Widget_Meta_Boxes::getIFrameSearchWidgetDataByKey($thepostid,APL_Iframe_Search_Widget_Const::META_CITIES,array());
                        foreach ( $cities as $k_choice => $choice ):
                            // Description
                            apollo_wp_multi_checkbox(  array(
                                'id'            => ''. APL_Iframe_Search_Widget_Const::META_CITIES . '-' .$index ,
                                'name'          => ''. APL_Iframe_Search_Widget_Const::META_CITIES . '[]',
                                'label'         => $choice,
                                'desc_tip'      => 'true',
                                'description'   => '',
                                'type'          => 'checkbox',
                                'default'       => false,
                                'value'			=> $selectedValue,
                                'cbvalue'       => $k_choice ) );
                            $index += 1;
                        endforeach;
                        echo '</div>';
                    echo '</div>';
                    echo '<div class="options_group clear event-box '.APL_Iframe_Search_Widget_Const::META_ENABLE_CITIES.'" '.$class_cities.'>';
                    apollo_wp_select(  array(
                        'id' =>  APL_Iframe_Search_Widget_Const::META_DEFAULT_CITY ,
                        'label' => __( 'Select default city', 'apollo' ),
                        'desc_tip' => 'true',
                        'description' => "",
                        'options'   => Apollo_App::getCityByTerritory(false,false,true),
                        'class' => 'apl-territory-state',
                        'value' => Apollo_Admin_Iframe_Search_Widget_Meta_Boxes::getIFrameSearchWidgetDataByKey($thepostid,APL_Iframe_Search_Widget_Const::META_DEFAULT_CITY),
                    ) );
                    echo '</div>';
                echo '</div>';
            }
            echo '</div>';
            echo "<hr/>";
        }

        if($isEnabledVenueModule){
            echo '<div class="options_group clear wrap-blk ct-full-width">';
                echo '<div class="options_group clear ct-selection-box">';
                apollo_wp_radio(  array(
                    'id' => APL_Iframe_Search_Widget_Const::META_ENABLE_REGION,
                    'label' => __( 'Enable Display Region', 'apollo' ),
                    'class' => 'iframe-search-widget-enable',
                    'value' => Apollo_Admin_Iframe_Search_Widget_Meta_Boxes::getIFrameSearchWidgetDataByKey($thepostid,APL_Iframe_Search_Widget_Const::META_ENABLE_REGION,'ON'),
                    'options' => array('ON' => 'ON',
                        'OFF' => 'OFF'
                    )
                ) );
                echo '</div>';

            $class_regions =  Apollo_Admin_Iframe_Search_Widget_Meta_Boxes::getIFrameSearchWidgetDataByKey($thepostid,APL_Iframe_Search_Widget_Const::META_ENABLE_REGION) == 'OFF'
                ? 'style=" display:none " ' : "";
            $regions = self::getRegions();


            if ( !empty($regions) ){

                echo '<div id="city-area-selection" class="options_group clear '.APL_Iframe_Search_Widget_Const::META_ENABLE_REGION.' " '.$class_regions.' >';
                    echo '<div class="options_group ">';
                    apollo_wp_checkbox(  array(
                        'id' => APL_Iframe_Search_Widget_Const::META_REGION.'_all',
                        'class' => 'meta_checkbox_all',
                        'label' => __( 'Select All Regions', 'apollo' ),
                        'desc_tip' => 'true',
                        'description' => "",
                        'name' => APL_Iframe_Search_Widget_Const::META_REGION,
                        'type' => 'checkbox', ) );
                    echo '</div>';
                    echo '<label class="apl-multi-choice ct-lb-mtc">'. __('Regions','apollo') .'</label>';
                    echo '<div class="wrap-cz">';
                        echo '<div class="wrap-cz-child">';
                        $index = 0;
                        $numCol = 4;
                        $itemPerCol = intval(count($regions) / $numCol);
                        $selectedValue = Apollo_Admin_Iframe_Search_Widget_Meta_Boxes::getIFrameSearchWidgetDataByKey($thepostid,APL_Iframe_Search_Widget_Const::META_REGION,array());
                        foreach ( $regions as $k_choice => $choice):
                            // Description
                            apollo_wp_multi_checkbox(  array(
                                'id'            => ''. APL_Iframe_Search_Widget_Const::META_REGION . '-' .$index ,
                                'name'          => ''. APL_Iframe_Search_Widget_Const::META_REGION . '[]',
                                'label'         => $choice,
                                'desc_tip'      => 'true',
                                'class'         => 'iframesw_multicb',
                                'description'   => '',
                                'type'          => 'checkbox',
                                'default'       => false,
                                'value'			=> $selectedValue,
                                'cbvalue'       => $k_choice ) );
                            $index += 1;
                            if($itemPerCol > 0 && $index % $itemPerCol === 0){
                                echo '</div>';
                                echo '<div class="wrap-cz-child">';
                            }
                        endforeach;
                        echo '</div>';
                    echo '</div>';
                echo '</div>';

            }
            echo '</div>';
            echo "<hr/>";
        }

        echo '<div class="options_group clear wrap-blk ct-full-width">';
            echo '<div class="options_group clear ct-selection-box">';
            apollo_wp_radio(  array(
                'id' => APL_Iframe_Search_Widget_Const::META_ENABLE_CATEGORY,
                'label' => __( 'Enable Display Categories', 'apollo' ),
                'class' => 'iframe-search-widget-enable',
                'value' => Apollo_Admin_Iframe_Search_Widget_Meta_Boxes::getIFrameSearchWidgetDataByKey($thepostid,APL_Iframe_Search_Widget_Const::META_ENABLE_CATEGORY,'ON'),
                'options' => array('ON' => 'ON',
                    'OFF' => 'OFF'
                )
            ) );
            echo '</div>';

        $class_categories =  Apollo_Admin_Iframe_Search_Widget_Meta_Boxes::getIFrameSearchWidgetDataByKey($thepostid,APL_Iframe_Search_Widget_Const::META_ENABLE_CATEGORY) == 'OFF'
            ? 'style=" display:none " ' : "";

        $categories = self::getEventTypes();


        if ( !empty($categories) ){

            echo '<div id="categories-area-selection" class="options_group clear '.APL_Iframe_Search_Widget_Const::META_ENABLE_CATEGORY.' " '.$class_categories.'>';
                echo '<div class="options_group ">';
                apollo_wp_checkbox(  array(
                    'id' => APL_Iframe_Search_Widget_Const::META_CATEGORY.'_all',
                    'class' => 'meta_checkbox_all',
                    'label' => __( 'Select All Categories', 'apollo' ),
                    'desc_tip' => 'true',
                    'description' => "",
                    'name' => APL_Iframe_Search_Widget_Const::META_CATEGORY,
                    'type' => 'checkbox', ) );
                echo '</div>';
                echo '<div class="wrap-ddl">';
                    echo '<label class="apl-multi-choice ct-lb-mtc">'. __('Categories','apollo') .'</label>';
                    echo '<div class="wrap-ddl-blk">';
                    $index = 0;
                    $selectedValue = Apollo_Admin_Iframe_Search_Widget_Meta_Boxes::getIFrameSearchWidgetDataByKey($thepostid,APL_Iframe_Search_Widget_Const::META_CATEGORY,array());
                    foreach ( $categories as $k_choice => $choice ):
                        // Description
                        apollo_wp_multi_checkbox(  array(
                                'id'            => ''. APL_Iframe_Search_Widget_Const::META_CATEGORY . '-' .$index ,
                                'name'          => ''. APL_Iframe_Search_Widget_Const::META_CATEGORY. '[]',
                                'label'         => $choice['name'],
                                'desc_tip'      => 'true',
                                'class'         => intval($choice['parent']) === 0 ? 'checkbox parent_'.$k_choice : 'is_child_of_'.$choice['parent'].' checkbox',
                                'description'   => '',
                                'type'          => 'checkbox',
                                'default'       => false,
                                'value'			=> $selectedValue,
                                'cbvalue'       => $k_choice,
                                'data-attributes' => array('data-parent-id='.$choice['parent'])
                            )
                        );
                        $index += 1;
                    endforeach;
                    echo '</div>';
                echo '</div>';
            echo '</div>';
        }

        echo '</div>';
        echo "<hr/>";
    }



    public static function getVenues(){
        $resultsData = array();
        $venues = Apollo_App::getListPostItemWithPagination( array(
            'post_type' => Apollo_DB_Schema::_VENUE_PT,
            'posts_per_page' => Apollo_DB_Schema::_APL_MAX_VENUE_PER_PAGE
        ) );
        if ($venues) {
            foreach ($venues as $v ){
                $resultsData[$v->ID] = $v->post_title;
            }
        }
        return $resultsData;
    }


    public static function getCities(){
        $_terrData = of_get_option(Apollo_DB_Schema::_TERR_DATA);
        $_cities_str = array();
        if ($_terrData) {
            foreach($_terrData as $state => $cities) {
                if (!$state) continue;
                if ( $cities ) {
                    foreach ( $cities as $c => $zips ) {
                        $_cities_str[$c] = $c. ' ('.$state.')';
                    }
                }
            }
        }
        natcasesort($_cities_str);
        return $_cities_str;
    }

    public static function getZips(){
        $zips = Apollo_App::getZipByTerritory(false,'',false,false);
        $resultsData = $zips;
        return $resultsData;
    }
    public static function getRegions(){
        $regions = Apollo_App::get_regions(false,true);
        $resultsData = $regions;
        if(!empty($resultsData)){
            unset($resultsData[0]);
            natcasesort($resultsData);
        }
        return $resultsData;
    }

    public static function getEventTypes(){
        $resultsData = array();
        $args = array(
            'orderby'                => 'name',
            'order'                  => 'ASC',
            'hide_empty'             => false,
            'fields'                 => 'all',
            'hierarchical'           => true,
            'parent'				 => 0,
        );
        $eventTypes = get_terms('event-type', $args);
        if(!empty($eventTypes)){
            foreach($eventTypes as $itemParent){
                $resultsData[$itemParent->term_id] = array(
                    'name' => $itemParent->name,
                    'parent' => 0
                );
                $termsChild = get_terms('event-type', array(
                    'orderby'                => 'name',
                    'order'                  => 'ASC',
                    'hide_empty'             => false,
                    'fields'                 => 'all',
                    'hierarchical'           => true,
                    'parent'				 => $itemParent->term_id,
                ));
                if(!empty($termsChild)){
                    foreach($termsChild as $tc){
                        $resultsData[$tc->term_id] = array(
                            'name' => '—' . $tc->name,
                            'parent' => $itemParent->term_id
                        );
                    }
                }
            }
        }
        return $resultsData;
    }

    /**
     * Save meta box data
     */
    public static function save( $post_id, $post ) {
        global $post, $wpdb;

        // Checks save status
        $is_autosave = wp_is_post_autosave( $post_id );
        $is_revision = wp_is_post_revision( $post_id );
        $is_valid_nonce = ( isset( $_POST[ 'apollo_iframe_search_widget_settings_meta_nonce' ] ) && wp_verify_nonce( $_POST[ 'apollo_iframe_search_widget_settings_meta_nonce' ], 'apollo_iframe_search_widget_settings_meta_nonce' ) ) ? true : false;

        // Exits script depending on save status
        if ( $is_autosave || $is_revision || ! $is_valid_nonce ) {
            return;
        }

        if( isset( $_POST[ APL_Iframe_Search_Widget_Const::META_ENABLE_VENUE ] ) ) {
            update_apollo_meta( $post_id,  APL_Iframe_Search_Widget_Const::META_ENABLE_VENUE,  $_POST[ APL_Iframe_Search_Widget_Const::META_ENABLE_VENUE] );
        } else {
            update_apollo_meta( $post_id,  APL_Iframe_Search_Widget_Const::META_ENABLE_VENUE,  'OFF' );
        }
        if( isset( $_POST[ APL_Iframe_Search_Widget_Const::META_ENABLE_CITIES ] ) ) {
            update_apollo_meta( $post_id,  APL_Iframe_Search_Widget_Const::META_ENABLE_CITIES,  $_POST[ APL_Iframe_Search_Widget_Const::META_ENABLE_CITIES] );
        } else {
            update_apollo_meta( $post_id,  APL_Iframe_Search_Widget_Const::META_ENABLE_CITIES,  'OFF' );
        }
        if( isset( $_POST[ APL_Iframe_Search_Widget_Const::META_ENABLE_CATEGORY ] ) ) {
            update_apollo_meta( $post_id,  APL_Iframe_Search_Widget_Const::META_ENABLE_CATEGORY,  $_POST[ APL_Iframe_Search_Widget_Const::META_ENABLE_CATEGORY ] );
        } else {
            update_apollo_meta( $post_id,  APL_Iframe_Search_Widget_Const::META_ENABLE_CATEGORY,  'OFF' );
        }
        if( isset( $_POST[ APL_Iframe_Search_Widget_Const::META_ENABLE_REGION ] ) ) {
            update_apollo_meta( $post_id,  APL_Iframe_Search_Widget_Const::META_ENABLE_REGION,  $_POST[ APL_Iframe_Search_Widget_Const::META_ENABLE_REGION ] );
        } else {
            update_apollo_meta( $post_id,  APL_Iframe_Search_Widget_Const::META_ENABLE_REGION,  'OFF' );
        }
        if( isset( $_POST[ APL_Iframe_Search_Widget_Const::META_DEFAULT_CITY ] ) ) {
            update_apollo_meta( $post_id,  APL_Iframe_Search_Widget_Const::META_DEFAULT_CITY,  $_POST[ APL_Iframe_Search_Widget_Const::META_DEFAULT_CITY ] );
        } else {
            update_apollo_meta( $post_id,  APL_Iframe_Search_Widget_Const::META_DEFAULT_CITY,  '' );
        }

        if( isset( $_POST[APL_Iframe_Search_Widget_Const::META_VENUES ] ) ) {
            update_apollo_meta( $post_id, APL_Iframe_Search_Widget_Const::META_VENUE, explode(",",$_POST[APL_Iframe_Search_Widget_Const::META_VENUES ]) );
        } else {
            update_apollo_meta( $post_id, APL_Iframe_Search_Widget_Const::META_VENUE, array() );
        }
        if( isset( $_POST[APL_Iframe_Search_Widget_Const::META_CATEGORY ] ) ) {
            update_apollo_meta( $post_id,APL_Iframe_Search_Widget_Const::META_CATEGORY, $_POST[APL_Iframe_Search_Widget_Const::META_CATEGORY ] );
        } else {
            update_apollo_meta( $post_id,APL_Iframe_Search_Widget_Const::META_CATEGORY, array() );
        }

        if( isset( $_POST[APL_Iframe_Search_Widget_Const::META_CITIES ] ) ) {
            update_apollo_meta( $post_id, APL_Iframe_Search_Widget_Const::META_CITIES, $_POST[ APL_Iframe_Search_Widget_Const::META_CITIES ] );
        } else {
            update_apollo_meta( $post_id, APL_Iframe_Search_Widget_Const::META_CITIES, array() );
        }

        if( isset( $_POST[ APL_Iframe_Search_Widget_Const::META_REGION] ) ) {
            update_apollo_meta( $post_id, APL_Iframe_Search_Widget_Const::META_REGION  , $_POST[ APL_Iframe_Search_Widget_Const::META_REGION  ] );
        } else {
            update_apollo_meta( $post_id, APL_Iframe_Search_Widget_Const::META_REGION  , array() );
        }

    }


}
