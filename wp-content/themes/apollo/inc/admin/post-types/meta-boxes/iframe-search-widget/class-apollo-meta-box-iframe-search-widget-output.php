<?php
/**
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_IFrame_Search_Widget_Output
 */
class Apollo_Meta_Box_IFrame_Search_Widget_Output {


    /**
     * Output the metabox
     */
    public static function output( $post ) {
        global $post, $thepostid, $wpdb;
        wp_nonce_field( 'apollo_iframesw_meta_nonce', 'apollo_iframesw_meta_nonce' );
        $thepostid = $post->ID;
        $currentDomainName = get_site_url();
        $iframeUrl = $currentDomainName . '?'._APL_IFRAME_SEARCH_WIDGET_FLAG.'&ifswid='.$thepostid.'';
        echo '<div class="options_group clear ct-selection-box">';
        apollo_wp_text_input(  array(
            'id' => 'meta-iframe-url',
            'label' => __( 'iFrame Widget URL', 'apollo' ),
            'value' => $iframeUrl,
        ) );
        echo '</div>';
        // generate first html for iframe on add new iframe post type
        $mainIframeColor = isset($_POST[ APL_Iframe_Search_Widget_Const::META_COLOR ]) ? $_POST[ APL_Iframe_Search_Widget_Const::META_COLOR ] : Apollo_Admin_Iframe_Search_Widget_Meta_Boxes::getIFrameSearchWidgetDataByKey($thepostid,APL_Iframe_Search_Widget_Const::META_COLOR,'#fcb234');
        echo '<div class="options_group clear ct-selection-box">';
        $iframeCode = '
            <div id="apl-iframe" data-url="'.site_url().'" data-width="400" data-height="1000" data-flag="'._APL_IFRAME_SEARCH_WIDGET_FLAG.'" data-ifswid="'.$thepostid.'" data-primary-color="'.$mainIframeColor.'" ></div>
             <script>(function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = "'.site_url().'/custom-iframe-handler.js";
              fjs.parentNode.insertBefore(js, fjs);
             }(document, "script", "apl-elinext-jssdk"));</script>
        ';

        apollo_wp_textarea_input(  array(
            'id' => 'meta-iframe-code',
            'label' => __( 'iFrame Widget Code', 'apollo' ),
            'value' => Apollo_Admin_Iframe_Search_Widget_Meta_Boxes::getIFrameSearchWidgetDataByKey($thepostid,APL_Iframe_Search_Widget_Const::META_IFRAME_CODE,$iframeCode),
            'num-row' => 10,
        ) );

        apollo_wp_hidden_input(  array(
            'id' => APL_Iframe_Search_Widget_Const::META_COLOR_BEFORE_SAVE,
            'std' => '',
            'value' => Apollo_Admin_Iframe_Search_Widget_Meta_Boxes::getIFrameSearchWidgetDataByKey($thepostid,APL_Iframe_Search_Widget_Const::META_COLOR,'#fcb234')
        ) );

        echo '</div>';
        }

    /**
     * Save meta box data
     */
    public static function save( $post_id, $post ) {
        global $post, $wpdb;

        // Checks save status
        $is_autosave = wp_is_post_autosave( $post_id );
        $is_revision = wp_is_post_revision( $post_id );
        $is_valid_nonce = ( isset( $_POST[ 'apollo_iframesw_meta_nonce' ] ) && wp_verify_nonce( $_POST[ 'apollo_iframesw_meta_nonce' ], 'apollo_iframesw_meta_nonce' ) ) ? true : false;

        // Exits script depending on save status
        if ( $is_autosave || $is_revision || ! $is_valid_nonce ) {
            return;
        }
        if( isset( $_POST[ 'meta-iframe-url' ] ) ) {
            update_apollo_meta( $post_id, 'meta-iframe-url', $_POST[ 'meta-iframe-url' ] );
        }
        if( isset( $_POST[ 'meta-iframe-code' ] ) ) {
            $mainIframeColor = isset($_POST[ APL_Iframe_Search_Widget_Const::META_COLOR ]) ? $_POST[ APL_Iframe_Search_Widget_Const::META_COLOR ] : '#fcb234';
            $mainColorBeforeSave = isset($_POST[ APL_Iframe_Search_Widget_Const::META_COLOR_BEFORE_SAVE ]) ? $_POST[ APL_Iframe_Search_Widget_Const::META_COLOR_BEFORE_SAVE ] : '#fcb234';
            $iframeCode = $_POST['meta-iframe-code'];
            if(strpos($iframeCode,"data-primary-color") !== false){
                $iframeCode = str_replace($mainColorBeforeSave,$mainIframeColor,$iframeCode);
            } else {
                $iframeCode = str_replace('data-ifswid','data-primary-color="'.$mainIframeColor.'" data-ifswid',$iframeCode);
            }
            update_apollo_meta( $post_id, 'meta-iframe-code', $iframeCode );
        }

    }

}
