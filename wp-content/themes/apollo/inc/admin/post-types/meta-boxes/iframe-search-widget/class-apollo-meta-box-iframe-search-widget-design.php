<?php
/**
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Iframe_Search_Widget_Design
 */
class Apollo_Meta_Box_Iframe_Search_Widget_Design {

    /**
     * Output the metabox
     */
    public static function output( $post ) {
        global $post, $thepostid, $wpdb;
        wp_nonce_field( 'apollo_iframe-search-widget_design_meta_nonce', 'apollo_iframe-search-widget_design_meta_nonce' );
        $thepostid = $post->ID;
        echo '<div class="options_group event-box clear">';

        apollo_wp_color_field(  array(
            'id' => APL_Iframe_Search_Widget_Const::META_COLOR,
            'label' => __( 'Color', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => '',
            'std' => '#666666',
            'value' =>Apollo_Admin_Iframe_Search_Widget_Meta_Boxes::getIFrameSearchWidgetDataByKey($thepostid,APL_Iframe_Search_Widget_Const::META_COLOR,'')
        ) );
        echo '</div>';

        echo '<br/>';
        echo '<div class="options_group event-box clear">';
        apollo_wp_editor(  array(
            'name' => APL_Iframe_Search_Widget_Const::META_HTML_BEFORE,
            'id' =>  APL_Iframe_Search_Widget_Const::META_HTML_BEFORE,
            'label' => '',
            'desc_tip' => 'true',
            'description' => __('Custom HTML above widget','apollo').':',
        ) );
        echo '</div>';

        echo '<br/>';

        echo '<div class="options_group event-box clear">';
        apollo_wp_editor(  array(
            'name' => APL_Iframe_Search_Widget_Const::META_HTML_AFTER,
            'id' =>  APL_Iframe_Search_Widget_Const::META_HTML_AFTER,
            'label' => '',
            'desc_tip' => 'true',
            'description' =>__('Custom HTML below widget','apollo').':',
        ) );
        echo '</div>';



    }

    /**
     * Save meta box data
     */
    public static function save( $post_id, $post ) {
        global $post, $wpdb;
        // Checks save status
        $is_autosave = wp_is_post_autosave( $post_id );
        $is_revision = wp_is_post_revision( $post_id );
        $is_valid_nonce = ( isset( $_POST[ 'apollo_iframe-search-widget_design_meta_nonce' ] ) && wp_verify_nonce( $_POST[ 'apollo_iframe-search-widget_design_meta_nonce' ], 'apollo_iframe-search-widget_design_meta_nonce' ) ) ? true : false;

        // Exits script depending on save status
        if ( $is_autosave || $is_revision || ! $is_valid_nonce ) {
            return;
        }

        // Checks for input $k_choicand sanitizes/saves if needed
        // TODO : replace string-key = APL_Syndication_Const
        if( isset( $_POST[ APL_Iframe_Search_Widget_Const::META_HTML_BEFORE ] ) ) {
            update_apollo_meta( $post_id, APL_Iframe_Search_Widget_Const::META_HTML_BEFORE , $_POST[ APL_Iframe_Search_Widget_Const::META_HTML_BEFORE ]  );
        }
        if( isset( $_POST[ APL_Iframe_Search_Widget_Const::META_HTML_AFTER ] ) ) {
            update_apollo_meta( $post_id, APL_Iframe_Search_Widget_Const::META_HTML_AFTER ,  $_POST[ APL_Iframe_Search_Widget_Const::META_HTML_AFTER ]  );
        }

        if( isset( $_POST[ APL_Iframe_Search_Widget_Const::META_COLOR ] ) ) {
            update_apollo_meta( $post_id, APL_Iframe_Search_Widget_Const::META_COLOR, $_POST[ APL_Iframe_Search_Widget_Const::META_COLOR ] );
        }


    }
}
