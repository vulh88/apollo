<?php
/**
 * Apollo Artist Meta Boxes
 *
 * Sets up the write panels used by venues (custom post types)
 *
 * @author 		vulh
 * @category 	Admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Admin_Page_Meta_Boxes
 */
class Apollo_Admin_Page_Meta_Boxes {

    /**
     * Constructor
     */
    public function __construct() {

        $currentTheme =  !empty(wp_get_theme()) ? wp_get_theme()->stylesheet : '';
        if ( $currentTheme == 'apollo') {
            /*@ticket #18192: 0002504: Arts Education Customizations - Adjust the landing page style - item 4*/
            add_action('add_meta_boxes', array($this, 'add_meta_boxes'), 30);
            add_action('save_post', "Apollo_Admin_Page_Meta_Boxes::saveCustomBox", 10, 2);
        }
    }

    /**
     *
     */
    public function add_meta_boxes()
    {
        add_meta_box('apl_add_search_box_custom', __('Search Global Custom Box', 'apollo'),
            'Apollo_Admin_Page_Meta_Boxes::add_search_global_custom_box', 'page', 'side');

        /*@ticket #18313 Enable education spotlight*/
        add_meta_box('apl_config_for_education_page', __('Options', 'apollo'),
            'Apollo_Admin_Page_Meta_Boxes::apl_config_for_education_page', 'page', 'side');

    }

    public static function add_search_global_custom_box($post){

        $enableGlobalSearch = get_post_meta($post->ID,'page-global-search',true);
        $enableGlobalSearchValue = ($enableGlobalSearch && $enableGlobalSearch === 'on') ? 'checked' : '';
        $directModule = get_post_meta($post->ID,'direct-to-module',true);
        $avaiableModules = Apollo_App::get_avaiable_modules();
        $listModules = array('' => __('Select module', 'apollo'));

        foreach($avaiableModules as $module){
            if(in_array($module, array('agency', 'syndication', 'photo-slider', 'iframesw', 'post'))){
                continue;
            }
            $listModules[$module] = ucfirst($module);
        }

        ?>
        <div>
            <p>
                <input type="checkbox" name="page-global-search" id="page-global-search"  <?php echo $enableGlobalSearchValue?> >
                <label for="page-global-search"><?php echo __('Enable the global search bar below the navigation', 'apollo')?></label>
            </p>
        </div>
        <?php

        echo '<div>';
        apollo_wp_select(  array(
            'id'            => 'apl-direct-module',
            'name'          => 'direct-to-module',
            'label'         => 'Direct to module',
            'desc_tip'      => 'true',
            'options'       => $listModules,
            'value'         => $directModule,
            'class'         => 'apl-direct-module'
        ) );
        echo '</div>';
    }

    /**
     * @param $post
     */
    public static function apl_config_for_education_page($post){
        /*@ticket #18313 Enable education spotlight*/
        $enableEducationSpotlight = get_post_meta($post->ID,'enable-education-spotlight',true);
        $enableEducationSpotlight = $enableEducationSpotlight ? $enableEducationSpotlight : 'yes';
        echo '<div >';
        apollo_wp_checkbox(  array(
            'id' => 'enable-education-spotlight',
            'label' => __( 'Enable education spotlight', 'apollo' ),
            'value' => $enableEducationSpotlight
        ) );
        echo '</div>';

        /*@ticket #18331: add checkbox is education full page*/
        //Default is Yes
        $isFullTemplate = get_post_meta($post->ID,'education-full-template',true);
        echo '<div >';
        apollo_wp_checkbox(  array(
            'id' => 'education-full-template',
            'label' => __( 'Display in wide-screen', 'apollo' ),
            'value' => $isFullTemplate
        ) );
        echo '</div>';

        /*@ticket #18345: Add option hide/show social button on "educator landing page"*/
        $showSocialBtn = get_post_meta($post->ID,'education-show-social-btns',true);
        echo '<div >';
        apollo_wp_checkbox(  array(
            'id' => 'education-show-social-btns',
            'label' => __( 'Show social buttons', 'apollo' ),
            'value' => $showSocialBtn
        ) );
        echo '</div>';
    }

    /**
     * @param $post_id
     * @param $post
     */
    public static function saveCustomBox($post_id){

        $enableSearchGlobal = isset($_POST['page-global-search']) ? $_POST['page-global-search'] : '';
        $directModule = isset($_POST['direct-to-module']) ? $_POST['direct-to-module'] : '';

        update_post_meta($post_id,'page-global-search', $enableSearchGlobal);
        update_post_meta($post_id,'direct-to-module', $directModule);

        /*@ticket #18313 Enable education spotlight*/
        $enableEducationSpotlight = isset($_POST['enable-education-spotlight']) ? $_POST['enable-education-spotlight'] : 'no';
        update_post_meta($post_id,'enable-education-spotlight', $enableEducationSpotlight);

        /*@ticket #18331 save checkbox is education full page*/
        $isFullTemplate = isset($_POST['education-full-template']) ? $_POST['education-full-template'] : 'no';
        update_post_meta($post_id,'education-full-template', $isFullTemplate);

        /*@ticket #18345: Add option hide/show social button on "educator landing page"*/
        $showSocialBtn = isset($_POST['education-show-social-btns']) ? $_POST['education-show-social-btns'] : 'no';
        update_post_meta($post_id,'education-show-social-btns', $showSocialBtn);
    }
}

new Apollo_Admin_Page_Meta_Boxes();