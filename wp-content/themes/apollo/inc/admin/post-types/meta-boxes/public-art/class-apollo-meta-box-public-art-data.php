<?php
/**
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Program_Data
 */
class Apollo_Meta_Box_Public_Art_Data {

	/**
	 * Output the metabox
	 */
    public static function output( $post ) {

        global $post, $thepostid;
        wp_nonce_field( 'apollo_public_art_meta_nonce', 'apollo_public_art_meta_nonce' );
        $thepostid = $post->ID;

        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => ''. Apollo_DB_Schema::_APL_PUBLIC_ART_DATA .'['. Apollo_DB_Schema::_PUBLIC_ART_DIMENSION .']',
            'label' => __( 'Dimensions', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
        ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => ''. Apollo_DB_Schema::_APL_PUBLIC_ART_DATA .'['. Apollo_DB_Schema::_PUBLIC_ART_CREATED_DATE .']',
            'label' => __( 'Date created', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
        ) );
        echo '</div>';



    }

    /***
     * OutputContactInfo the metabox
     */
    public static function outputContactInfo( $post ) {

        global $post, $thepostid;
        wp_nonce_field( 'apollo_public_art_meta_nonce', 'apollo_public_art_meta_nonce' );
        $thepostid = $post->ID;
        // contact name
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_PUBLIC_ART_DATA. '['. Apollo_DB_Schema::_PUBLIC_ART_CONTACT_NAME .']',
            'label' => __( 'Contact name', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
        ) );
        echo '</div>';

        // contact email
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_PUBLIC_ART_DATA. '['. Apollo_DB_Schema::_PUBLIC_ART_CONTACT_EMAIL .']',
            'label' => __( 'Contact email', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apollo_input_email',
        ) );
        echo '</div>';

        // contact phone
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_PUBLIC_ART_DATA. '['. Apollo_DB_Schema::_PUBLIC_ART_CONTACT_PHONE .']',
            'label' => __( 'Contact phone', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
        ) );
        echo '</div>';

        //website
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => ''. Apollo_DB_Schema::_APL_PUBLIC_ART_DATA .'['. Apollo_DB_Schema::_PUBLIC_ART_WEBSITE_URL .']',
            'label' => __( 'Website', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apollo_input_url',
        ) );
        echo '</div>';

    }

    /**
     * Output Artist
     */
    public static function output_artist( $post ) {
        $thepostid = $post->ID;

        $public_art = get_public_art($post);
        $artistsPublicArt = $public_art->getAssociatedArtist();
        $artistIdsPublicArt = $public_art->getAssociatedArtistIDs($artistsPublicArt);

        $checked_list = empty($artistIdsPublicArt) ? "" : join(",", $artistIdsPublicArt);
        $artists = APL_Artist_Function::getListArtists();

        echo '<div id="artist-list" post_id="' . $thepostid . '">';
        echo '<div class="group-search"><input id="search-text" placeholder="'.__('Search by Artist name', 'apollo').'"/><span id="remove-search-text" class="remove-search hidden"><i class="fa fa-times"></i></span></div>';
        echo '<ul id="apollo-artist-list-in-frm">';
        echo(Apollo_App::renderArtists($artists['data'], $artistIdsPublicArt));
        echo '</ul>';
        echo '<input type="hidden" name="' . Apollo_DB_Schema::_APL_PUBLIC_ART_ARTIST .'" id="apl-checked-list-art" value="' . $checked_list . '"/>';
        echo '<div><p>'. __('Selected Artists: ', 'apollo');
        echo APL_Artist_Function::renderArtistsSelected($artistsPublicArt);
        echo'</div></p>';
        echo '</div>';
        ?>
        <div class="wrap-box-load-more-artist <?php echo $artists['have_more'] ? '' : 'hidden' ?>">
        <div class="load-more b-btn wp-ct-event">
            <a href="javascript:void(0);"
               data-container="#apollo-artist-list-in-frm"
               data-ride="ap-more"
               data-holder="#apollo-artist-list-in-frm>:last"
               data-sourcetype="url"
               data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_get_more_artists&module=public-art&post_id=' . $thepostid . '&offset='.count($artists['data']) ) ?>"
               data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
               data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'
               class="btn-b arw ct-btn-sm-event"><?php _e('SHOW MORE', 'apollo') ?>
            </a>
        </div>
        </div>
        <?php
    }

    public static function save_artist_public_art($arr_art_pub = array(),$post_id){
        $apl_query  = new Apl_Query( Apollo_Tables::_APL_ARTIST_PUBLIC_ART );

        $art_field  = Apollo_DB_Schema::_APL_A_ART_ID;

        $apl_query->delete( " post_id = {$post_id}" );
        $convert_data = array();
        if(!is_array($arr_art_pub)){
            if(!empty($arr_art_pub)){
                $convert_data = mb_split(",", $arr_art_pub);
            }
        }else{
            $convert_data = $arr_art_pub;
        }
        if ($convert_data){
            foreach ($convert_data as $k => $v) {
                if($v) {
                    $apl_query->insert(array(
                        'post_id' => $post_id,
                        $art_field => $v,
                        Apollo_DB_Schema::_APL_A_ORDERING => $k,
                    ));
                }
            }
        }
    }

    /**
     * Save meta box data
     */
    public static function save( $post_id, $post ) {

        $_data          = $_POST[Apollo_DB_Schema::_APL_PUBLIC_ART_DATA];
        $convert_data   = @$_POST[Apollo_DB_Schema::_APL_PUBLIC_ART_ARTIST];
        /* Convert string to array*/
        $_artist_list = array();
        if(!is_array($convert_data)){
            if(!empty($convert_data)){
                $_artist_list = mb_split(",", $convert_data);
            }
        }else{
            $_artist_list = $convert_data;
        }
        $contact_email_field = Apollo_DB_Schema::_PUBLIC_ART_CONTACT_EMAIL;
       
        if ( isset( $_data[$contact_email_field] ) && $_data[$contact_email_field] ):
            if ( is_email( $_data[$contact_email_field] ) ) {
                $_data[$contact_email_field] = Apollo_App::clean_data_request( $_data[$contact_email_field] );
            } else {
                $_data[$contact_email_field] = '';
                Apollo_Admin_Public_Art_Meta_Boxes::add_error( __( 'Contact email is invalid format', 'apollo' ) );
            }
        endif;

        $url_field = Apollo_DB_Schema::_PUBLIC_ART_WEBSITE_URL;

        if ( isset( $_data[$url_field] ) && $_data[$url_field] ):
            if ( filter_var( $_data[$url_field], FILTER_VALIDATE_URL ) ) {
                $_data[$url_field] = Apollo_App::clean_data_request( $_data[$url_field] );
            } else {
                $_data[$url_field] = '';
                Apollo_Admin_Public_Art_Meta_Boxes::add_error( __( 'Website url is invalid format', 'apollo' ) );
            }
        endif;

        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_PUBLIC_ART_DATA, serialize( Apollo_App::clean_array_data($_data) ) );
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_PUBLIC_ART_ARTIST, Apollo_App::clean_array_data($_artist_list) );

        self::save_artist_public_art($_artist_list,$post_id);

        // Save it for searching eg "s32e"
        update_apollo_meta($post_id, Apollo_DB_Schema::_APL_PUBLIC_ART_ARTIST_SEARCHING, 's'. implode('e, s', $_artist_list). 'e');
        
    }

}