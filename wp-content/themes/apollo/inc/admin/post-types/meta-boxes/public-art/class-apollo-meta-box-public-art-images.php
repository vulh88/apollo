<?php
/**
 * Public Art Images
 *
 * Display the Public Art images meta box.
 *
 * @author 		hong
 * @category 	Admin
 * @package 	inc/admin/post-types/meta-boxes
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Apollo_Meta_Box_Public_Art_Images
 */
class Apollo_Meta_Box_Public_Art_Images {

	/**
	 * Output the metabox
	 */
	public static function output( $post ) {
		?>
		<div id="event_images_container" data-title="<?php _e('Select images','apollo');?>" data-text="<?php _e('Use selected images','apollo');?>">
			<ul class="event_images">
				<?php
				$public_art_image_gallery = get_apollo_meta( $post->ID, Apollo_DB_Schema::_APL_PUBLIC_ART_IMAGE_GALLERY, true );
				$attachments = array_filter( explode( ',', $public_art_image_gallery ) );
				if ( $attachments )
					foreach ( $attachments as $attachment_id ) {
						echo '<li class="image" data-attachment_id="' . esc_attr( $attachment_id ) . '">
								' . wp_get_attachment_image( $attachment_id, 'thumbnail' ) . '
								<ul class="actions">
									<li><a href="#" class="delete tips" data-tip="' . __( 'Delete image', 'apollo' ) . '">' . __( 'Delete', 'apollo' ) . '</a></li>
								</ul>
							</li>';
					}
				?>
			</ul>

			<input type="hidden" id="event_image_gallery" name="public_art_image_gallery" value="<?php echo esc_attr( $public_art_image_gallery ); ?>" />

		</div>
		<p class="add_event_images hide-if-no-js">
			<a href="#" data-choose="<?php _e( 'Add Images to Public Art  Gallery', 'apollo' ); ?>" data-update="<?php _e( 'Add to gallery', 'apollo' ); ?>" data-delete="<?php _e( 'Delete image', 'apollo' ); ?>" data-text="<?php _e( 'Delete', 'apollo' ); ?>"><?php _e( 'Add Public Art gallery images', 'apollo' ); ?></a>
		</p>
		<?php
	}

	/**
	 * image map icon
	 *
	 */

	public static function outputMapIcon( $post ) {
		?>

		<div class="public_art_image_icon_content">
				<?php
				$public_art_image_map_icon = get_apollo_meta( $post->ID, Apollo_DB_Schema::_APL_PUBLIC_ART_GOOGLE_ICON, true );
				$actionRemoveButtonStyle = 'display:none';
				$actionAddButtonStyle = 'display:block';

				//$attachments = array_filter( explode( ',', $public_art_image_gallery ) );
				if ( $public_art_image_map_icon )

					//foreach ( $attachments as $attachment_id ) {
						$imageLink = wp_get_attachment_url( $public_art_image_map_icon, 'thumbnail' ) ;
						$imageId = esc_attr( $public_art_image_map_icon );

						if(empty($imageLink)){
							$defaultIcon =	$imageLink = of_get_option( Apollo_DB_Schema::_GOOGLE_MAP_MARKER_ICON );
						}

						if(!empty($imageLink)){
							echo '
								<a class="">
									<img width="266" height="206" src="'.$imageLink.'" class="attachment-266x266" alt="classified-featured-admin-'.$imageId.'"></a>
							';

							$actionRemoveButtonStyle = 'display:block';
							$actionAddButtonStyle = 'display:none';
						}
				?>
		</div>

		<input type="hidden" id="public_art_google_icon" name="public_art_google_icon" value="<?php echo esc_attr( $public_art_image_map_icon ); ?>" />

		<p class="add_public_art_icon" style="<?php echo $actionAddButtonStyle ?>">
			<a  class=""><?php echo __( 'Add google map icon', 'apollo' ) ?></a>
		</p>
		<p class="remove_google_map_icon" style="<?php echo $actionRemoveButtonStyle ?>">
			<a  class=""><?php echo __( 'Remove google map icon', 'apollo' ) ?></a>
		</p>

		<?php

	}

	/**
	 * Save meta box data
	 */
	public static function save( $post_id, $post ) {

		$attachment_ids = array_filter( explode( ',', sanitize_text_field( $_POST['public_art_image_gallery'] ) ) );
		$public_art_google_icon = isset($_POST['public_art_google_icon'])?$_POST['public_art_google_icon']:'';

		update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_PUBLIC_ART_IMAGE_GALLERY, implode( ',', $attachment_ids ) );
		update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_PUBLIC_ART_GOOGLE_ICON, $public_art_google_icon  );

	}
}