<?php
/**
 * Displays the Public Art address
 *
 * @author 		Hong
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Public_art_Address
 */

class Apollo_Meta_Box_Public_art_Address {

    /**
     * Output the metabox
     */
    public static function output( $post ) {

        global $post, $thepostid,$pagenow;
        wp_nonce_field( 'apollo_public_art_meta_nonce', 'apollo_public_art_meta_nonce' );
        $thepostid = $post->ID;
        $stateVal = '';
        $cityVal = '';
        if( $pagenow == 'post.php' ){
            $stateVal = Apollo_App::apollo_get_meta_data( $thepostid, Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS .'['. Apollo_DB_Schema::_PUBLIC_ART_STATE .']' );
            $cityVal = Apollo_App::apollo_get_meta_data( $thepostid, Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS .'['. Apollo_DB_Schema::_PUBLIC_ART_CITY .']' );
        } else {
            $list_states = Apollo_App::getListState();
            $default_states =  of_get_option(Apollo_DB_Schema::_APL_DEFAULT_STATE);
            $cityVal = of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY);
            if( $default_states && in_array($default_states,$list_states) ) {
                $stateVal = $default_states;
            } else if(count($list_states) == 1) {
                $stateVal = key($list_states);
            }
        }
        //address
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => ''. Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS .'['. Apollo_DB_Schema::_PUBLIC_ART_ADDRESS .']',
            'label' => __( 'Address', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
        ) );
        echo '</div>';

        //state
        echo '<div class="options_group event-box">';
        apollo_wp_select(  array(
            'id' => ''. Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS .'['. Apollo_DB_Schema::_PUBLIC_ART_STATE .']',
            'label' => __( 'State', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'options'   => Apollo_App::getStateByTerritory(),
            'class' => 'apl-territory-state',
            'value' => $stateVal,
        ) );
        echo '</div>';
        
        //city
        echo '<div class="options_group event-box">';
        apollo_wp_select(  array(
            'id' => ''. Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS .'['. Apollo_DB_Schema::_PUBLIC_ART_CITY .']',
            'label' => __( 'City', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'options'   => Apollo_App::getCityByTerritory(FALSE, $stateVal, true),
            'class' => 'apl-territory-city',
            'value' => $cityVal,
        ) );
        echo '</div>';

        //Zip
        echo '<div class="options_group event-box">';
        apollo_wp_select(  array(
            'id' => ''. Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS .'['. Apollo_DB_Schema::_PUBLIC_ART_ZIP .']',
            'label' => __( 'Zip', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'options'   => Apollo_App::getZipByTerritory(FALSE, $stateVal, $cityVal, true),
            'class' => 'apl-territory-zipcode',

        ) );
        echo '</div>';

        // region
        echo '<div class="options_group event-box">';
        apollo_wp_select(  array(
            'id' => ''. Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS .'['. Apollo_DB_Schema::_PUBLIC_ART_REGION .']',
            'label' => __( 'Region', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'options'   => Apollo_App::get_regions(),
            'display' => Apollo_App::showRegion(),
        ) );
        echo '</div>';


        //latitude
        if (! $lat = Apollo_App::apollo_get_meta_data( $thepostid, Apollo_DB_Schema::_PUBLIC_ART_LATITUDE, Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS )) {
            $lat = Apollo_App::apollo_get_meta_data( $thepostid, Apollo_DB_Schema::_APL_PUBLIC_ART_LATITUDE );
        }
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_PUBLIC_ART_LATITUDE,
            'name' => Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS. '['.Apollo_DB_Schema::_PUBLIC_ART_LATITUDE.']',
            'label' => __( 'Latitude', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'data_compare'  => Apollo_DB_Schema::_PUBLIC_ART_LONGITUDE,
            'class' => 'lat-lng-related',
            'value' => $lat,
        ) );
        echo '</div>';

        //longitude
        if (! $lng = Apollo_App::apollo_get_meta_data( $thepostid, Apollo_DB_Schema::_PUBLIC_ART_LONGITUDE, Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS )) {
            $lng = Apollo_App::apollo_get_meta_data( $thepostid, Apollo_DB_Schema::_APL_PUBLIC_ART_LONGITUDE );
        }
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_PUBLIC_ART_LONGITUDE,
            'name' => Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS. '['.Apollo_DB_Schema::_PUBLIC_ART_LONGITUDE.']',
            'label' => __( 'Longitude', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'data_compare'  => Apollo_DB_Schema::_PUBLIC_ART_LATITUDE,
            'class' => 'lat-lng-related',
            'value' => $lng,
        ) );
        echo '</div>';

        /** @Ticket #15810 - Show map */
        echo '<div class="options_group event-box">';
        $browserKey = of_get_option(Apollo_DB_Schema::_GOOGLE_API_KEY_BROWSER);
        apollo_wp_google_map(array(
            'id' => 'apl-get-lat-long',
            'label' => __('Get latitude and longitude', 'apollo'),
            'attr' => array(
                'data-address-id = "'.Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS .'['. Apollo_DB_Schema::_PUBLIC_ART_ADDRESS .']'.'"',
                'data-state-id = "'.Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS .'['. Apollo_DB_Schema::_PUBLIC_ART_STATE .']'.'"',
                'data-city-id = "'.Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS .'['. Apollo_DB_Schema::_PUBLIC_ART_CITY .']'.'"',
                'data-zip-id = "'.Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS .'['. Apollo_DB_Schema::_PUBLIC_ART_ZIP .']'.'"',
                'data-latitude-id = "'.Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS .'['. Apollo_DB_Schema::_PUBLIC_ART_LATITUDE .']'.'"',
                'data-longitude-id = "'.Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS .'['. Apollo_DB_Schema::_PUBLIC_ART_LONGITUDE .']'.'"',
                'data-google-api-key = "'. $browserKey .'"'
            )
        ));
        echo '</div>';
    }

    static function create_full_address($address) {

        $config = array(
            Apollo_DB_Schema::_PUBLIC_ART_ADDRESS,
            Apollo_DB_Schema::_PUBLIC_ART_CITY,
            Apollo_DB_Schema::_PUBLIC_ART_STATE,
            Apollo_DB_Schema::_PUBLIC_ART_ZIP,
        );


        if ( ! $address ) return false;

        $_add_arr = array();
        foreach ( $config as $c ) {
            if ( isset($address[$c]) && !empty($address[$c]) ) {
                $_add_arr[] = $address[$c] ;
            }
        }

        return $_add_arr ? implode( ', ' , $_add_arr ) : false;
    }

    /**
     * Save meta box data
     */
    public static function save( $post_id, $post ) {
        $_data = $_POST[Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS];

        if(!isset($_data[Apollo_DB_Schema::_PUBLIC_ART_REGION])){
            $_data[Apollo_DB_Schema::_PUBLIC_ART_REGION] = Apollo_App::apollo_get_meta_data($post_id, Apollo_DB_Schema::_PUBLIC_ART_REGION, Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS );
        }

        update_apollo_meta( $post_id, Apollo_DB_Schema::_PUBLIC_ART_CITY, isset($_data[Apollo_DB_Schema::_PUBLIC_ART_CITY]) ? $_data[Apollo_DB_Schema::_PUBLIC_ART_CITY] : '');
        update_apollo_meta( $post_id, Apollo_DB_Schema::_PUBLIC_ART_ZIP, isset($_data[Apollo_DB_Schema::_PUBLIC_ART_ZIP]) ? $_data[Apollo_DB_Schema::_PUBLIC_ART_ZIP] : '');

        /** @Ticket #13028 */
        update_apollo_meta( $post_id, Apollo_DB_Schema::_PUBLIC_ART_STATE, isset($_data[Apollo_DB_Schema::_PUBLIC_ART_STATE]) ? $_data[Apollo_DB_Schema::_PUBLIC_ART_STATE] : '');

        $_latitude = $_data[Apollo_DB_Schema::_PUBLIC_ART_LATITUDE];
        $_longitude = $_data[Apollo_DB_Schema::_PUBLIC_ART_LONGITUDE];

        $arrCoor = array();
        $address = self::create_full_address($_data);

        if (!$_latitude && !$_longitude && !empty($address)){
            $arrCoor = Apollo_Google_Coor_Cache::getCoorForVenue($address);
            $_latitude = @$arrCoor['lat'];
            $_longitude = @$arrCoor['lng'];
        }
        $_data[Apollo_DB_Schema::_PUBLIC_ART_LATITUDE] = $_latitude;
        $_data[Apollo_DB_Schema::_PUBLIC_ART_LONGITUDE] = $_longitude;

        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_PUBLIC_ART_ADDRESS, serialize( Apollo_App::clean_array_data($_data) ));

        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_PUBLIC_ART_LATITUDE, ( Apollo_App::clean_data_request($_latitude) ));
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_PUBLIC_ART_LONGITUDE, ( Apollo_App::clean_data_request($_longitude) ));
    }
}

