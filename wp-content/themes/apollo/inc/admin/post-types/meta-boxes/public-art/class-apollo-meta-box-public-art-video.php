<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


class Apollo_Meta_Box_Public_Art_Data_Video {

    /**
     * Output the metabox
     */
    public static function output( $post ) {

        global $post, $wpdb, $thepostid;
        wp_nonce_field( 'apollo_public_art_meta_nonce', 'apollo_public_art_meta_nonce' );
        $thepostid = $post->ID;
        $videos = @unserialize( get_apollo_meta( $thepostid, Apollo_DB_Schema::_APL_PUBLIC_ART_VIDEO, TRUE ) );
        ?>

        <div class="panel-wrap product_data">
            <div class="apollo-tabs-back"></div>
            <ul class="product_data_tabs apollo-tabs" style="display:none;">
                <?php
                $product_data_tabs = apply_filters( 'apollo_org_data_tabs', array(
                    'video' => array(
                        'label'  => __( 'Videos', 'apollo' ),
                        'target' => 'video-org-data',
                        'class'  => array('active'),
                    ),
                ) );
                foreach ( $product_data_tabs as $key => $tab ) {
                    ?><li class="<?php echo $key; ?>_options <?php echo $key; ?>_tab <?php echo implode( ' ' , $tab['class'] ); ?>">
                    <a href="#<?php echo $tab['target']; ?>"><?php echo esc_html( $tab['label'] ); ?></a>
                    </li><?php
                }
                do_action( 'apollo_product_write_panel_tabs' );
                ?>
            </ul>
            <!--Tri start data video -->
            <div id="video-org-data" class="panel apollo_options_panel">
                <div class="video-wrapper">
                    <?php
                    if ( ! $videos ) {
                        $videos[] = array( 'video_embed' => '', 'video_desc' => '' );
                    }

                    $i = 0;
                    foreach( $videos as $v ):
                        $prefix_id = $i != 0 ? '-'.$i : '';
                        $oldVideoEmbed = isset($v['embed'])?$v['embed']:'';
                        $oldVideoDesc = isset($v['desc'])?$v['desc']:'';
                        $videoEmbed = isset($v['video_embed'] )?$v['video_embed'] :$oldVideoEmbed;
                        $videoDesc = isset($v['video_desc'] )?$v['video_desc'] :$oldVideoDesc;
                        ?>
                        <div class="count video-list<?php echo $prefix_id; ?>">

                            <div class="options_group">
                                <p class="form-field video_embed_field ">
                                    <label for="video_embed"><?php _e( 'YouTube or Vimeo URL', 'apollo' ) ?></label>
                                    <input data-parent=".video_options" name="video_embed[]" type="text" value="<?php echo $videoEmbed ?>" class="full apollo_input_url" />
                                </p>
                            </div>

                            <div class="options_group">
                                <p class="form-field video_desc_field ">
                                    <label for="video_embed"><?php _e( 'Video Description', 'apollo' ) ?></label>
                                    <textarea class="full" name="video_desc[]" placeholder="" rows="2" cols="20"><?php echo esc_attr(base64_decode($videoDesc)) ?></textarea>
                                </p>
                            </div>

                            <div <?php if ( $i == 0 ) echo 'style="display: none;"' ?> data-confirm="<?php _e( 'Are you sure to remove this video ?', 'apollo' ) ?>" class="del"><i class="fa fa-times"></i></div>

                        </div>
                        <?php $i++; endforeach; ?>
                </div>
                <!--                <input type="hidden" name="--><?php //echo Apollo_DB_Schema::_APOLLO_EVENT_DATA ?><!--[--><?php //echo Apollo_DB_Schema::_VIDEO ?><!--]" />-->
                <input type="button" class="button button-primary button-large" id="apl-add-video" value="<?php _e( 'Add more links', 'apollo' ) ?>" />
            </div>
            <div class="clear"></div>
        </div>

        <?php
    }



    /**
     * Save meta box data
     */
    public static function save( $post_id, $post ) {


        //save video data
        $video_embed = isset( $_POST['video_embed'] ) ? $_POST['video_embed'] : '';
        $video_desc = isset( $_POST['video_desc'] ) ? $_POST['video_desc'] :'';
        $_arr_video = array();
        if ( $video_embed ) {
            $i = 0;
            foreach ( $video_embed as $v ) {
                if ( ! $v ) continue;
                $_arr_video[] = array( 'video_embed' => $v, 'video_desc' => isset( $video_desc[$i] ) ?  base64_encode(Apollo_App::clean_data_request($video_desc[$i])) : '' );
                $i++;
            }
        }
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_PUBLIC_ART_VIDEO, serialize($_arr_video) );
        //end save video data

    }

}