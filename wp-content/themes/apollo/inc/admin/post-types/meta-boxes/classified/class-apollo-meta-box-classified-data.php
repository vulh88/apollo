<?php
/**
 * Displays the classified Data
 *
 * @author 		Hong
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Apollo_Meta_Box_Classified_Data {

    /**
     * Output the metabox
     */
    public static function output( $post ) {

        global $post, $thepostid;
        wp_nonce_field( 'apollo_classified_meta_nonce', 'apollo_classified_meta_nonce' );
        $thepostid = $post->ID;
        $email_required = of_get_option(Apollo_DB_Schema::_CLASSIFIED_ENABLE_REQUIREMENTS_EMAIL, false);
        $email_class = 'apollo_input_email'  . ($email_required ? ' required' : '');
        echo '<div class="options_group event-box">';
        // Phone
        apollo_wp_text_input(  array(
            'id' => ''. Apollo_DB_Schema::_APL_CLASSIFIED_DATA .'['. Apollo_DB_Schema::_CLASSIFIED_NAME .']',
            'label' => __( 'Name', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
        ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
        // Phone
        apollo_wp_text_input(  array(
            'id' => ''. Apollo_DB_Schema::_APL_CLASSIFIED_DATA .'['. Apollo_DB_Schema::_CLASSIFIED_PHONE .']',
            'label' => __( 'Phone', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
        ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
        // Fax
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_CLASSIFIED_DATA. '['. Apollo_DB_Schema::_CLASSIFIED_FAX .']',
            'label' => __( 'Fax', 'apollo' ),
            'desc_tip' => 'true',
            'description' => ""
        ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
        // Email
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_CLASSIFIED_DATA. '['. Apollo_DB_Schema::_CLASSIFIED_EMAIL .']',
            'label' => __( 'Email', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => $email_class,
            'required' => $email_required,

        ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_CLASSIFIED_EXP_DATE,
            'name' => Apollo_DB_Schema::_APL_CLASSIFIED_DATA. '['. Apollo_DB_Schema::_CLASSIFIED_EXP_DATE .']',
            'label' => __( 'Expiry date', 'apollo' ),
            'desc_tip' => 'true',
            'description' => '',
            'type' => 'text',
            'data_parent'   => '.datetime_options',
            'required' => true,
            'class' => 'apollo_input_datepicker short expired-date apollo-date-greater-than-now required',
        ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_CLASSIFIED_DEADLINE_DATE,
            'name' => Apollo_DB_Schema::_APL_CLASSIFIED_DATA. '['. Apollo_DB_Schema::_CLASSIFIED_DEADLINE_DATE .']',
            'label' => __( 'Deadline date', 'apollo' ),
            'desc_tip' => 'true',
            'description' => '',
            'type' => 'text',
            'data_parent'   => '.datetime_options',
            'class' => 'apollo_input_datepicker short expired-date apollo-date-greater-than-now ',
        ) );
        echo '</div>';


    }
    /**
     * OutputLink the metabox
     */
    public static function outputLink($post){
        global $post, $thepostid;
        wp_nonce_field( 'apollo_classified_meta_nonce', 'apollo_classified_meta_nonce' );
        $thepostid = $post->ID;

        echo '<div class="options_group event-box">';
        // web site url
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_CLASSIFIED_DATA. '['. Apollo_DB_Schema::_CLASSIFIED_WEBSITE_URL .']',
            'label' => __( 'Website url', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apollo_input_url',
        ) );
        echo '</div>';

    }

    /***
     * OutputContactInfo the metabox
     */
    public static function outputContactInfo( $post ) {

        global $post, $thepostid;
        wp_nonce_field( 'apollo_organization_meta_nonce', 'apollo_organization_meta_nonce' );
        $thepostid = $post->ID;

        // contact name
        echo '<div class="options_group event-box">';

        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_CLASSIFIED_DATA. '['. Apollo_DB_Schema::_CLASSIFIED_CONTACT_NAME .']',
            'label' => __( 'Contact name', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => '',
        ) );
        echo '</div>';

        // contact email
        echo '<div class="options_group event-box">';

        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_CLASSIFIED_DATA. '['. Apollo_DB_Schema::_CLASSIFIED_CONTACT_EMAIL .']',
            'label' => __( 'Contact email', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apollo_input_email',
        ) );

        echo '</div>';

        // contact phone
        echo '<div class="options_group event-box">';

        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_CLASSIFIED_DATA. '['. Apollo_DB_Schema::_CLASSIFIED_CONTACT_PHONE .']',
            'label' => __( 'Contact phone', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",

        ) );
        echo '</div>';

    }

    /**
     * Print out drop down select box for Organization section (Classified edit admin page)
     *
     * @ticket: #11069
     * @since: after ticket #11069, this select box can use ajax to load Organization items, not load all items as before.
     */
    public static function outputOrganization()
    {
        global $post, $thepostid;
        $thepostid = $post->ID;

        // get selected Organization item and format it to <option> tag
        $metaOrg            = get_apollo_meta($thepostid, Apollo_DB_Schema::_APOLLO_CLASSIFIED_ORGANIZATION, true);
        $selectedOptionItem = sprintf('<option selected="selected" value="">%s</option>', __('Select Organization', 'apollo')); // default selected option
        if ( !empty($metaOrg) ) {
            $selectedOrg         = get_post($metaOrg);
            $selectedOptionItem  = sprintf('<option value="">%s</option>', __('Select Organization', 'apollo'));
            $selectedOptionItem .= sprintf('<option selected="selected" value="%s">%s</option>', $selectedOrg->ID, $selectedOrg->post_title);
        }

        // Organization section
        echo '<p>' . _e( 'Organization', 'apollo' ) . '</p>';
        echo '<div class="options_group event-box">';
        echo "
            <select name='" . Apollo_DB_Schema::_APOLLO_CLASSIFIED_ORGANIZATION . "'
                    id='" . Apollo_DB_Schema::_APOLLO_CLASSIFIED_ORGANIZATION . "'
                    data-value=''
                    data-enable-remote='1'
                    data-post-type='" . Apollo_DB_Schema::_ORGANIZATION_PT . "'
                    data-source-url='apollo_get_remote_associate_data_to_select2_box'
                    class='w-200 select apl_select2 apl-primary-category'>"
                . $selectedOptionItem . "
            </select>
            <br/><br/>
        ";
        echo '</div>';

        // Organization temp section
        echo '<div class="options_group event-box">';
        echo '<p>' . _e( 'Organization temp', 'apollo' ) . '</p>';
        apollo_wp_text_input( array(
            'id'          => Apollo_DB_Schema::_APL_CLASSIFIED_TMP_ORG,
            'label'       => '',
            'desc_tip'    => 'true',
            'description' => "",
            'class'       => '',
        ) );
        echo '</div>';
    }

    /**
     * Save meta box data
     */
    public static function save( $post_id, $post ) {

        self::_save($post_id);
        $_org           = Apollo_App::clean_data_request($_POST[Apollo_DB_Schema::_APOLLO_CLASSIFIED_ORGANIZATION]);
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APOLLO_CLASSIFIED_ORGANIZATION, $_org );

        /**Vandd - @Ticket #12623 */
        self::_save_mini_sites_data($post_id, $_org);
    }

    /**
     * Store data for mini-sites
     * @param $post_id
     * @param $_org
     */
    private static function _save_mini_sites_data($post_id, $_org)
    {
        global $mini_submit_sites;
        if ($mini_submit_sites) {
            if ($miniData = Apollo_Classified_Copy_To_Mini_Site::getAllMiniPostByParentPost($post_id, $mini_submit_sites)) {

                foreach ($miniData as $site_item){
                    if($site_item->mini_site_id && $site_item->mini_post_id && $site_item->mini_options_id){
                        $mini_org_id = '';
                        if(!empty($_org)){
                            $mini_org_id = Apollo_Classified_Copy_To_Mini_Site::copyOrgToMiniSite($site_item->mini_site_id, $_org);
                        }
                        //insert term

                        if(switch_to_blog($site_item->mini_site_id)){
                            Apollo_Mini_Site_App::insertIclTranslations($site_item->mini_post_id, 'classified');
                            Apollo_Mini_Site_App::insertIclTranslations($mini_org_id, 'organization');
                            self::_save($site_item->mini_post_id);
                            update_apollo_meta($site_item->mini_post_id, Apollo_DB_Schema::_APOLLO_CLASSIFIED_ORGANIZATION, $mini_org_id);
                            restore_current_blog();
                        }

                        //update attachment
                        Apollo_Mini_Site_App::updateAttachment($site_item->mini_site_id, $post_id, $site_item->mini_post_id);

                        //copy category
//                        Apollo_Mini_Site_App::updatePostTerm($post_id, 'classified-type', $site_item->mini_site_id, $site_item->mini_post_id, $site_item->mini_options_id);
                        Apollo_Mini_Site_App::copyCategoryToMiniSite($post_id, $site_item->mini_site_id, $site_item->mini_post_id, $site_item->mini_options_id, 'classified-type', 'meta-classified-catsel', 'classified-meta-type');
                    }
                }
            }
        }
    }

    public static function _save( $post_id) {

        $_data          = $_POST[Apollo_DB_Schema::_APL_CLASSIFIED_DATA];
        $_temp_org      = $_POST[Apollo_DB_Schema::_APL_CLASSIFIED_TMP_ORG];
        //expired date use to compare to current date
        $expired_date   = $_data[Apollo_DB_Schema::_CLASSIFIED_EXP_DATE];
        $deadline_date   = $_data[Apollo_DB_Schema::_CLASSIFIED_DEADLINE_DATE];
        $email_field = Apollo_DB_Schema::_CLASSIFIED_EMAIL;
        $email_required = of_get_option(Apollo_DB_Schema::_CLASSIFIED_ENABLE_REQUIREMENTS_EMAIL, false);

        if ($email_required && isset( $_data[$email_field] ) && $_data[$email_field] ):
            if ( is_email( $_data[$email_field] ) ) {
                $_data[$email_field] = Apollo_App::clean_data_request( $_data[$email_field] );
            } else {
                $_data[$email_field] = '';
                Apollo_Admin_Classified_Meta_Boxes::add_error( __( 'Email is invalid format', 'apollo' ) );
            }
        endif;

        $contact_email_field = Apollo_DB_Schema::_CLASSIFIED_CONTACT_EMAIL;

        if ( isset( $_data[$contact_email_field] ) && $_data[$contact_email_field] ):
            if ( is_email( $_data[$contact_email_field] ) ) {
                $_data[$contact_email_field] = Apollo_App::clean_data_request( $_data[$contact_email_field] );
            } else {
                $_data[$contact_email_field] = '';
                Apollo_Admin_Classified_Meta_Boxes::add_error( __( 'Contact email is invalid format', 'apollo' ) );
            }
        endif;

        $url_field = Apollo_DB_Schema::_CLASSIFIED_WEBSITE_URL;

        if ( isset( $_data[$url_field] ) && $_data[$url_field] ):
            if ( filter_var( $_data[$url_field], FILTER_VALIDATE_URL ) ) {
                $_data[$url_field] = Apollo_App::clean_data_request( $_data[$url_field] );
            } else {
                $_data[$url_field] = '';
                Apollo_Admin_Classified_Meta_Boxes::add_error( __( 'Website url is invalid format', 'apollo' ) );
            }
        endif;

        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_CLASSIFIED_DATA, serialize( Apollo_App::clean_array_data($_data) ) );
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_CLASSIFIED_TMP_ORG, Apollo_App::clean_data_request($_temp_org) ) ;

        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_CLASSIFIED_EXP_DATE, Apollo_App::clean_data_request($expired_date) ) ;
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_CLASSIFIED_DEADLINE_DATE, Apollo_App::clean_data_request($deadline_date) ) ;

        /** @Ticket #13028 */
        update_apollo_meta($post_id, Apollo_DB_Schema::_CLASSIFIED_PHONE, !empty($_data[Apollo_DB_Schema::_CLASSIFIED_PHONE]) ? $_data[Apollo_DB_Schema::_CLASSIFIED_PHONE] : '');
        update_apollo_meta($post_id, Apollo_DB_Schema::_CLASSIFIED_NAME, !empty($_data[Apollo_DB_Schema::_CLASSIFIED_NAME]) ? $_data[Apollo_DB_Schema::_CLASSIFIED_NAME] : '');


    }

}