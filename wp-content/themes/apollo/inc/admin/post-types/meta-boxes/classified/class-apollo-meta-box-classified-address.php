<?php
/**
 * Displays the Classified address
 *
 * @author 		Hong
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Classified_Address
 */

class Apollo_Meta_Box_Classified_Address {

    /**
     * Output the metabox
     */
    public static function output( $post ) {

        global $post, $thepostid,$pagenow;
        wp_nonce_field( 'apollo_classified_meta_nonce', 'apollo_classified_meta_nonce' );
        $thepostid = $post->ID;
        $stateVal = '';
        $cityVal = '';
        $requirementState = of_get_option(Apollo_DB_Schema::_CLASSIFIED_ENABLE_REQUIREMENTS_STATE, 1);
        $requirementCity = of_get_option(Apollo_DB_Schema::_CLASSIFIED_ENABLE_REQUIREMENTS_CITY, 1);
        $requirementZip = of_get_option(Apollo_DB_Schema::_CLASSIFIED_ENABLE_REQUIREMENTS_ZIP, 0);
        if( $pagenow == 'post.php' ){
            $stateVal = Apollo_App::apollo_get_meta_data( $thepostid, Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS .'['. Apollo_DB_Schema::_CLASSIFIED_STATE .']' );
            $cityVal = Apollo_App::apollo_get_meta_data( $thepostid, Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS .'['. Apollo_DB_Schema::_CLASSIFIED_CITY .']' );
        } else {
            $list_states = Apollo_App::getListState();
            $default_states =  of_get_option(Apollo_DB_Schema::_APL_DEFAULT_STATE);
            $cityVal = of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY);
            if( $default_states && in_array($default_states,$list_states) ) {
                $stateVal = $default_states;
            } else if(count($list_states) == 1) {
                $stateVal = key($list_states);
            }
        }
        echo '<div class="options_group event-box">';
        // Description
        apollo_wp_text_input(  array(
            'id' => ''. Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS .'['. Apollo_DB_Schema::_CLASSIFIED_ADDRESS .']',
            'label' => __( 'Address', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
        ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
        // Description
        apollo_wp_select(  array(
            'id' => ''. Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS .'['. Apollo_DB_Schema::_CLASSIFIED_STATE .']',
            'label' => __( 'State', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'required' => $requirementState ? true : false,
            'options'   => Apollo_App::getStateByTerritory( $requirementState ? true : false),
            'class' => 'apl-territory-state',
            'value' => $stateVal
        ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
        apollo_wp_select(  array(
            'id' => ''. Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS .'['. Apollo_DB_Schema::_CLASSIFIED_CITY .']',
            'label' => __( 'City', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'required' =>$requirementCity ? true : false,
            'options'   =>  Apollo_App::getCityByTerritory( $requirementCity ? true : false, $stateVal, true),
            'class' => 'apl-territory-city',
            'value' => $cityVal,
        ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
        // Description
        apollo_wp_select(  array(
            'id' => ''. Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS .'['. Apollo_DB_Schema::_CLASSIFIED_ZIP .']',
            'label' => __( 'Zip', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => sprintf('apl-territory-zipcode %s', $requirementZip ? 'required' : ''),
            'required' => $requirementZip ? true : false,
            'options'   => Apollo_App::getZipByTerritory(false, $stateVal, $cityVal, true),
        ) );
        echo '</div>';

        echo '<div class="tmp-address-wrapper" >';

        echo '<h4>'.__( 'Other Address', 'apollo' ) .'</h4>';

        // Add tmp state input field for entering exception state
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => ''. Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS .'['. Apollo_DB_Schema::_CLASSIFIED_TMP_STATE .']',
            'label' => __( 'Other State', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apl-territory-state-tmp',
        ) );
        echo '</div>';

        // Add tmp city input field for entering exception cities
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => ''. Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS .'['. Apollo_DB_Schema::_CLASSIFIED_TMP_CITY .']',
            'label' => __( 'Other City', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apl-territory-city-tmp',
        ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => ''. Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS .'['. Apollo_DB_Schema::_CLASSIFIED_TMP_ZIP .']',
            'label' => __( 'Other Zip Code', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apl-territory-city-tmp',
        ) );
        echo '</div>';
        echo '</div>';

        // region
        echo '<div class="options_group event-box">';
        apollo_wp_select(  array(
            'id' => ''. Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS .'['. Apollo_DB_Schema::_CLASSIFIED_REGION .']',
            'label' => __( 'Region', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'options'   => Apollo_App::get_regions(),
            'display' => Apollo_App::showRegion(),
        ) );
        echo '</div>';

    }

    /**
     * Save meta box data
     */
    public static function save( $post_id, $post ) {

        $_data = $_POST[Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS];
        if(!isset($_data[Apollo_DB_Schema::_CLASSIFIED_REGION])){
            $_data[Apollo_DB_Schema::_CLASSIFIED_REGION] = Apollo_App::apollo_get_meta_data($post_id, Apollo_DB_Schema::_CLASSIFIED_REGION, Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS );
        }
        self::_save($post_id, $_data);

        /** Vandd - @Ticket - #12623 - store data for mini-sites */
        self::_save_mini_sites_data($post_id, $_data);

    }

    /**
     * Store mini site data
     * @param $post_id
     * @param $_data
     */
    private static function _save_mini_sites_data($post_id, $_data)
    {
        global $mini_submit_sites;
        if ($mini_submit_sites) {
            if ($miniData = Apollo_Classified_Copy_To_Mini_Site::getAllMiniPostByParentPost($post_id, $mini_submit_sites)) {

                foreach ($miniData as $site_item){
                    if(switch_to_blog($site_item->mini_site_id)){
                        self::_save($site_item->mini_post_id, $_data);
                        restore_current_blog();
                        if(Apollo_App::get_network_manage_states_cities()){
                            Apollo_Mini_Site_App::updateStateZipToMiniSite($site_item->mini_site_id, $_data);
                        }
                    }
                }
            }
        }
    }

    private static function _save($post_id, $_data){
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_CLASSIFIED_ADDRESS, serialize( Apollo_App::clean_array_data($_data) ) );

        $city = '';
        if(isset($_data[Apollo_DB_Schema::_CLASSIFIED_CITY]) && !empty($_data[Apollo_DB_Schema::_CLASSIFIED_CITY])){
            $city = $_data[Apollo_DB_Schema::_CLASSIFIED_CITY];

        } elseif(isset($_data[Apollo_DB_Schema::_CLASSIFIED_TMP_CITY]) && !empty($_data[Apollo_DB_Schema::_CLASSIFIED_TMP_CITY])){
            $city = $_data[Apollo_DB_Schema::_CLASSIFIED_TMP_CITY];

        }
        update_apollo_meta($post_id, Apollo_DB_Schema::_APL_CLASSIFIED_CITY, $city);

        /** @Ticket #13028 */
        $classifiedCity = '';
        if(isset($_data[Apollo_DB_Schema::_CLASSIFIED_STATE]) && !empty($_data[Apollo_DB_Schema::_CLASSIFIED_STATE])){
            $classifiedCity = $_data[Apollo_DB_Schema::_CLASSIFIED_STATE];

        } elseif(isset($_data[Apollo_DB_Schema::_CLASSIFIED_TMP_STATE]) && !empty($_data[Apollo_DB_Schema::_CLASSIFIED_TMP_STATE])){
            $classifiedCity = $_data[Apollo_DB_Schema::_CLASSIFIED_TMP_STATE];
        }
        update_apollo_meta($post_id, Apollo_DB_Schema::_APL_CLASSIFIED_STATE, $classifiedCity);

    }
}