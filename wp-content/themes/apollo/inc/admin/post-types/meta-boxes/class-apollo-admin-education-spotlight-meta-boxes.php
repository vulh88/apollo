<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Apl_Ad_Module_Meta_Boxes' ) ) {
    include 'class-apl-ad-module-meta-boxes.php';
}

/**
 * Apollo_Admin_Education_Spotlight_Meta_Boxes
 * @category 	Admin
 * @package 	inc/admin/post-types
 * @ticket      #11186
 */
class Apollo_Admin_Education_Spotlight_Meta_Boxes extends Apl_Ad_Module_Meta_Boxes
{
    /**
     * Apollo_Admin_Education_Spotlight_Meta_Boxes constructor.
     */
    public function __construct()
    {
        parent::__construct( Apollo_DB_Schema::_EDUCATION_SPOTLIGHT_PT );

        add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) , 30 );

        add_action( 'save_post'     , array( $this, 'save_meta_boxes' ), 1, 2 );

        add_action( $this->process_data_action, 'Apollo_Admin_Education_Spotlight_Meta_Boxes::save_custom_box', 10, 2 );
    }

    /**
     *
     */
    public function add_meta_boxes()
    {
        // @ticket #18320: 0002504: Arts Education Customizations - add hyperlinks to the landing page slider images (Education spotlights) - item 4
        add_meta_box( 'apollo-business-spotlight-link', __( 'Link', 'apollo' ),
            'Apollo_Admin_Education_Spotlight_Meta_Boxes::spotlight_link', $this->type, 'normal', 'high');
    }

    /**
     * @ticket #18320: 0002504: Arts Education Customizations - add hyperlinks to the landing page slider images (Education spotlights) - item 4*
     * @param $post
     */
    public static function spotlight_link($post){
        wp_nonce_field( 'apollo_education-spotlight_meta_nonce', 'apollo_education-spotlight_meta_nonce' );
        $link           = get_post_meta( $post->ID, Apollo_DB_Schema::_APL_EDUCATION_SPOTLIGHT_LINK, true );
        $newTab = get_post_meta( $post->ID, Apollo_DB_Schema::_APL_EDUCATION_SPOTLIGHT_LINK_OPEN_NEW_TAB, true );
        $newTab = $newTab == 'on' ? 'checked' : '';
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(array(
            'id'          => Apollo_DB_Schema::_APL_EDUCATION_SPOTLIGHT_LINK,
            'label'       => '',
            'desc_tip'    => 'true',
            'class'       => 'apollo_input_url',
            'value'       => $link,
        ) );
        echo '</div>';
        echo '<div class="options_group event-box italic">';
        echo '<input type="checkbox" ' . $newTab . ' name="' . Apollo_DB_Schema::_APL_EDUCATION_SPOTLIGHT_LINK_OPEN_NEW_TAB . '" />' . "Is open new page ?";
        echo '</div>';
    }

    /**
     * @param $post_id
     */
    public static function save_custom_box($post_id )
    {
        /*@ticket #18320: 0002504: Arts Education Customizations - add hyperlinks to the landing page slider images (Education spotlights) - item 4*/
        $link = '';
        if ( filter_var( $_POST[Apollo_DB_Schema::_APL_EDUCATION_SPOTLIGHT_LINK], FILTER_VALIDATE_URL ) ) {
            $link = $_POST[Apollo_DB_Schema::_APL_EDUCATION_SPOTLIGHT_LINK];
        }
        update_post_meta($post_id, Apollo_DB_Schema::_APL_EDUCATION_SPOTLIGHT_LINK, Apollo_App::clean_data_request( $link ));

        $newTab = isset($_POST[Apollo_DB_Schema::_APL_EDUCATION_SPOTLIGHT_LINK_OPEN_NEW_TAB]) ? $_POST[Apollo_DB_Schema::_APL_EDUCATION_SPOTLIGHT_LINK_OPEN_NEW_TAB] : false;
        update_post_meta($post_id, Apollo_DB_Schema::_APL_EDUCATION_SPOTLIGHT_LINK_OPEN_NEW_TAB, Apollo_App::clean_data_request( $newTab ));
    }
}

new Apollo_Admin_Education_Spotlight_Meta_Boxes();
