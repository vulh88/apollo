<?php
/**
 * Apollo Org Meta Boxes
 *
 * Sets up the write panels used by orgs (custom post types)
 *
 * @author 		vulh
 * @category 	Admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

include 'class-apl-ad-module-meta-boxes.php';

/**
 * Apollo_Admin_Venue_Meta_Boxes
 */
class Apollo_Admin_Org_Meta_Boxes extends Apl_Ad_Module_Meta_Boxes {

    protected $organizationID;

    protected $organization;

    protected $organizationData;

    protected $isOrgBusiness;

	/**
	 * Constructor
	 */
	public function __construct() {

        parent::__construct( Apollo_DB_Schema::_ORGANIZATION_PT );

        if (isset($_GET['post']) && (int) $_GET['post'] > 0){
            $this->organizationID = (int) $_GET['post'];
            $this->organization = get_org($this->organizationID);
            $this->organizationData = $this->organization->getOrgData();
            $this->isOrgBusiness = $this->organizationData[Apollo_DB_Schema::_ORG_BUSINESS]; // boolean
        } else {
            $this->organizationID = 0;
            $this->organization = null;
            $this->organizationData = null;
            $this->isOrgBusiness = false; // boolean
        }

        $this->require_meta_boxes();

        /**
         * if remove slug box, the permalink of post cant change.
         */
        add_action( 'add_meta_boxes', array( $this, 'remove_meta_boxes'), 30 );
        add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 30 );
		add_action( 'save_post', array( $this, 'save_meta_boxes' ), 1, 2 );

        // update status of business when change status
        add_action( 'transition_post_status', 'Apollo_Meta_Box_Org_Data::on_all_status_transitions',10, 3 );

        // delete post of business when delete org
        add_action('before_delete_post', 'Apollo_Meta_Box_Org_Data::deleteBusiness', 10, 2);

        add_action(  'publish_post',  'on_post_publish', 10, 2 );

        // Save Venue Meta Boxes
		add_action( $this->process_data_action, 'Apollo_Meta_Box_Org_Data::save', 10, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Org_Address::save', 10, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Org_Images::save', 10, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Org_Data_Audio_Video::save', 10, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Common::updateChangeAuthor', 10, 2 );
        /**
         * Author: ThienLD
         * @Ticket #17843
         */
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Org_Data::saveBusinessMetaboxData', 10, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Org_Data::saveBusinessCustomFieldsData', 10, 2 );


        // Error handling (for showing errors from meta boxes on next page load)
		add_action( 'admin_notices', array( $this, 'output_errors' ) );
        add_action( 'shutdown', array( $this, 'save_errors' ) );

        // Hide the except box, Set post custom box ,slug box
        add_action('admin_head', array( $this, 'hide_meta_boxes' ));

        add_action('apollo_copy_to_mini_sites', array( $this, 'apollo_copy_to_mini_sites' ), 10, 1);

        /*@ticket #18087: [CF] 20181030 - [ORG][Admin form] Preview Changes button for the business - item 4, 5*/
        add_action( 'edit_form_after_title', array($this,'add_label_for_org_description') );
    }

    /**
     * @ticket #18087: [CF] 20181030 - [ORG][Admin form] Preview Changes button for the business - item 4, 5
     */
    public function add_label_for_org_description() {
        $text = _('Organization Description');
        echo "<div class='apl-org-desc'><label>$text</label></div>";
    }

    public static function apollo_copy_to_mini_sites($post_id)
    {
        global $apollo_mini_submit_sites;

        $businessId = Apollo_App::apollo_get_meta_data( $post_id, Apollo_DB_Schema::_APL_ORG_BUSINESS);

        if ($apollo_mini_submit_sites) {
            $orgCopyMiniSite = new Apollo_Org_Copy_To_Mini_Site($post_id);

            foreach($apollo_mini_submit_sites as $siteID => $miniConfigPostId){
                $site_item = $orgCopyMiniSite->getMiniPostByParentPost($post_id, $siteID);
                $site_item = count($site_item) ? $site_item[0] : [];

                if($site_item->mini_site_id && $site_item->mini_post_id && $miniConfigPostId){

                    if(!empty($businessId)){
                        Apollo_Org_Copy_To_Mini_Site::copyBusinessToMiniSite($site_item->mini_site_id, $businessId,$site_item->mini_post_id );
                    }

                    $metaValuesOrg = $orgCopyMiniSite->getMetaData($post_id);
                    $parent_gallery_ids = get_apollo_meta($post_id, Apollo_DB_Schema::_APL_ORG_IMAGE_GALLERY);
                    $parent_gallery_ids = $parent_gallery_ids ? explode(',', $parent_gallery_ids[0]) : '';

                    //don't update _apollo_org_business
                    foreach($metaValuesOrg as $index => $value ){
                        if (!strcmp($value->meta_key, Apollo_DB_Schema::_APL_ORG_BUSINESS)){
                            unset($metaValuesOrg[$index]);
                        }
                    }

                    $metaValuesOrg = array_values($metaValuesOrg);

                    if(switch_to_blog($site_item->mini_site_id)){
                        Apollo_Mini_Site_App::insertIclTranslations($site_item->mini_post_id, 'organization');
                        Apollo_Mini_Site_App::copyMetaData($site_item->mini_post_id, $metaValuesOrg);
                        restore_current_blog();
                    }

                    Apollo_Mini_Site_App::updateAttachment($site_item->mini_site_id, $post_id, $site_item->mini_post_id);
                    Apollo_Mini_Site_App::updateGalleryImagesToMiniSite($site_item->mini_post_id, $parent_gallery_ids, $site_item->mini_site_id, Apollo_DB_Schema::_APL_ORG_IMAGE_GALLERY);
                    Apollo_Mini_Site_App::copyCategoryToMiniSite($post_id, $site_item->mini_site_id, $site_item->mini_post_id, $miniConfigPostId , 'organization-type', 'org-meta-catsel', 'org-meta-type');
                }
            }
        }
    }

    public  function hide_meta_boxes(){
        global $post;
        if($post && $post->post_type == Apollo_DB_Schema::_ORGANIZATION_PT ){
            echo '
                <style type="text/css">
                    #postexcerpt,#postcustom,#slugdiv{
                        display:none;
                    }
                </style>
            ';

            /*@ticket #18015: [CF] 20181022 - [ORG] - Add 'on/off' switch for FE Org types on org/business form - item 3*/
            if (!of_get_option(Apollo_DB_Schema::_ORGANIZATION_ENABLE_TYPES, 1)){
                echo '
                <style type="text/css">
                    #organization-typediv{
                        display:none;
                    }
                </style>
            ';
            }
        }
    }
    
    public function require_meta_boxes() {
        require_once 'org/class-apollo-meta-box-org-address.php';
        require_once 'org/class-apollo-meta-box-org-data.php';
        require_once 'org/class-apollo-meta-box-org-images.php';
        require_once 'org/class-apollo-meta-box-org-data-video-audio.php';

        if($this->isOrgBusiness && $this->is_enabled_apollo_business_module()){
            /**
             * Author: ThienLD
             * @Ticket #17843
             */
            require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/business/class-apollo-meta-box-business-data.php';
        }
    }


    /**
     * Check if business module is already active
     * @return bool
     */
    public function is_enabled_apollo_business_module(){
	    return Apollo_App::is_avaiable_module( Apollo_DB_Schema::_BUSINESS_PT );
    }


	/**
	 * Add Apollo Meta boxes
	 */
	public function add_meta_boxes() {
        $enable_pbm = apply_filters('apl_pbm_module_active', null);

        add_meta_box( 'apollo-org-address', __( 'Information', 'apollo' ),
            'Apollo_Meta_Box_Org_Address::output', $this->type, 'normal', 'high' );
        
        add_meta_box( 'apollo-org-data', __( 'Own website and/or blog?', 'apollo' ),
            'Apollo_Meta_Box_Org_Data::output', $this->type, 'normal', 'high' );

        add_meta_box( 'apollo-org-contact-info', __( 'Contact info', 'apollo' ),
            'Apollo_Meta_Box_Org_Data::outputContactInfo', $this->type, 'normal', 'high' );

        $checkIsMemberActived = of_get_option(Apollo_DB_Schema::_ORGANIZATION_ACTIVE_MEMBER_FIELD);
        if($checkIsMemberActived){
            add_meta_box( 'apollo-org-member', __( 'Member', 'apollo' ),
                'Apollo_Meta_Box_Org_Data::outputIsMember', $this->type, 'side', 'high' );
        }

        add_meta_box( 'apollo-org-Display', __( 'Display', 'apollo' ),
            'Apollo_Meta_Box_Org_Data::outputDisPlay', $this->type, 'side', 'high' );

        if(Apollo_App::is_avaiable_module( Apollo_DB_Schema::_BUSINESS_PT )) {
            add_meta_box('apollo-org-business', __('Business', 'apollo'),
                'Apollo_Meta_Box_Org_Data::outputBusiness', $this->type, 'side', 'high');
        }

        add_meta_box( 'apollo-event-data', __( 'Organization Video Audio', 'apollo' ),
            'Apollo_Meta_Box_Org_Data_Audio_Video::output', $this->type, 'normal', 'low' );

        add_meta_box( 'apollo-icons', __( 'Icons', 'apollo' ),
            'Apollo_Meta_Box_Common::output_icons', $this->type, 'normal','low' );

        add_meta_box( 'apollo-artist-customfield', __( 'More Info', 'apollo' ),
            'Apollo_Meta_Box_Customfield::output', Apollo_DB_Schema::_ORGANIZATION_PT, 'normal' );

        add_meta_box( 'apollo-organization-user', __( 'Associated User(s)', 'apollo' ),
            'Apollo_Meta_Box_Common::output_associated_users', Apollo_DB_Schema::_ORGANIZATION_PT, 'side' );

        if (Apollo_App::is_avaiable_module(Apollo_DB_Schema::_AGENCY_PT)) {
            add_meta_box('apollo-organization-agency', __('Associated Agencies', 'apollo'),
                'Apollo_Meta_Box_Common::output_associated_agencies', Apollo_DB_Schema::_ORGANIZATION_PT, 'side');
        }

        add_meta_box( 'apollo-venue-gallery', __( 'Organization gallery ', 'apollo' ),
            'Apollo_Meta_Box_Org_Images::output', $this->type, $enable_pbm ? 'normal' : 'side');

        /** @Ticket #16209 */
        $currentUser = get_current_user_id();
        if (is_super_admin($currentUser) || user_can($currentUser, 'manage_options')) {
            add_meta_box( 'apollo-venue-author', __( 'Change Author', 'apollo' ),
                'Apollo_Meta_Box_Common::outputChangeAuthor', $this->type, 'side' );
        }

        /**
         * Author: ThienLD
         * @Ticket #17843
         */
        if($this->isOrgBusiness && $this->is_enabled_apollo_business_module()){

            $associatedBusinessID = $this->organizationData[Apollo_DB_Schema::_APL_ORG_BUSINESS];

            add_meta_box( 'apollo-business-customfield', __( 'More Business Info', 'apollo' ),
                'Apollo_Meta_Box_Customfield::output', $this->type, 'normal', 'high',
                array( 'render-type' => Apollo_Const::_APL_ADMIN_METABOX_RENDER_TYPE_IN_OTHER_MODULE,
                    'associated-business-id' => $associatedBusinessID,
                    'added-by-module' => Apollo_DB_Schema::_ORGANIZATION_PT,
                    'added-by-post-id' => $this->organizationID
                ));

            add_meta_box( 'apollo-business-information', __( 'Business', 'apollo' ),
                'Apollo_Meta_Box_Business_Data::output_business_form_fields', $this->type, 'normal', 'high',
                array( 'render-type' => Apollo_Const::_APL_ADMIN_METABOX_RENDER_TYPE_IN_OTHER_MODULE,
                       'associated-business-id' => $associatedBusinessID,
                       'added-by-module' => Apollo_DB_Schema::_ORGANIZATION_PT
                )
            );
        }

    }
}

new Apollo_Admin_Org_Meta_Boxes();
