<?php

/*@ticket #17235 - [Admin] Official tag for event and post */

class Apollo_Meta_Box_Official_Tags {

    protected $postType = '';
    protected $metaKey = '';
    protected $metaOfficialKey = '';

    public static function setMetaKey($postType){
        switch($postType){
            case "event":
               return array(
                   'tag' => Apollo_DB_Schema::_APL_EVENT_TAG_OFFICIAL,
                   'meta_key' => Apollo_DB_Schema::_APL_EVENT_OFFICIAL_TAGS,
               );

            case "post":
                return array(
                    'tag' => Apollo_DB_Schema::_APL_POST_TAG_OFFICIAL,
                    'meta_key' => Apollo_DB_Schema::_APL_POST_OFFICIAL_TAGS,
                );
            default:
                return array();

        }
    }
    public static function output( $post ) {
       
        global $wpdb;

        $config = self::setMetaKey($post->post_type);

        $officialTerms = $wpdb->get_results($wpdb->prepare("select * from {$wpdb->terms} term join {$wpdb->{Apollo_Tables::_APOLLO_TERM_META}} 
                                      on (term_id = apollo_term_id and meta_key=%s and meta_value = %s)", $config['tag'], 1));

        $option = array();
        $option[] = __( 'Select official tags' );
        
        if ( $officialTerms ) :
            foreach( $officialTerms as $official ):
                $option[$official->term_id] = $official->name;
            endforeach;
        endif;

        $officialSelected = get_apollo_meta($post->ID, $config['meta_key'],  true);
        $officialSelected = explode('e', $officialSelected) ;

        if (!empty($officialSelected)) {
            $selectedHtml = '<div class="apl-official-tags-selected">';
            foreach($officialSelected as $official) {
                if (!$official || $official === '' || $official === ',') continue;
                $term = get_term( $official );
                if($term){
                    $selectedHtml .= sprintf('<span id=%s><button class="apl-official-tag-delete"><i class="fa fa-times"></i></button>%s
                                                   <input type="hidden" value=%s name="'. $config['meta_key'] . '[]"></span>',
                        'apl-official-tag-'.$official, $term->name, $official);
                }
            }
            $selectedHtml .= "</div>";
        } else {
            $selectedHtml = '<div class="apl-official-tags-selected"></div>';
        }

        echo '<div class="apl-official-tag">';
            apollo_wp_select(  array( 
                'id'            => '',
                'label'         => '', 
                'desc_tip'      => 'true', 
                'description'   => "",
                'options'       => $option,
                'value'         => '',
                'class'         => 'apl-select-official-tag',
            ) );
        echo '<input type="button" post-type="' . $post->post_type . '" class="button add-official-tag" value="Add"></div>';

        echo $selectedHtml;
    }
    
    public static function save( $post_id) {
        global $post;
        if(isset($post->post_type) && in_array($post->post_type, array('post', 'event'))){
            $config = self::setMetaKey($post->post_type);
            $terms = isset( $_REQUEST[$config['meta_key']] ) ? $_REQUEST[$config['meta_key']] : '';
            if($terms !== ''){
                $terms = 'e' . implode('e,e', $terms) . 'e';
            }

            update_apollo_meta( $post_id, $config['meta_key'], $terms );
        }
    }
}
