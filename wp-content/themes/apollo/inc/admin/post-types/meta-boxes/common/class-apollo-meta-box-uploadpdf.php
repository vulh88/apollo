<?php
/**
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_UploadPDF
 */
class Apollo_Meta_Box_UploadPDF {

    /**
     * @param $post
     * @param $argMetaBox: parameter of metaboxes
     */
    public static function output($post, $argMetaBox ) {
        
        global $post, $thepostid;
        wp_nonce_field( 'apollo_'.$post->post_type.'_meta_nonce', 'apollo_'.$post->post_type.'_meta_nonce' );
        $thepostid = $post->ID;
        /** @Ticket 16287 */
        if ($post->post_type == Apollo_DB_Schema::_ARTIST_PT ) {
            $maxUploadPDF = of_get_option(Apollo_DB_Schema::_MAX_UPLOAD_PDF_ARTIST, Apollo_Display_Config::_MAX_UPLOAD_PDF_DEFAULT);
        }
        else if($post->post_type == Apollo_DB_Schema::_CLASSIFIED){
            $maxUploadPDF = of_get_option(Apollo_DB_Schema::_MAX_UPLOAD_PDF_CLASSIFIED, Apollo_Display_Config::_MAX_UPLOAD_PDF_DEFAULT);
        }
        else {
            $maxUploadPDF = of_get_option(Apollo_DB_Schema::_MAX_UPLOAD_PDF_PROGRAM, Apollo_Display_Config::_MAX_UPLOAD_PDF_DEFAULT);
        }

        /*@ticket #18391:  Arts Education Customizations - FE Program Form - The PDF files and text do NOT display on the admin program form nor the FE program detail page - item 1, 2*/
        $suffix = '';
        if ( $post->post_type == Apollo_DB_Schema::_PROGRAM_PT ) {
            $suffix = (isset($argMetaBox['args']) && isset($argMetaBox['args']['suffix'])) ? $argMetaBox['args']['suffix']  : '';
        }
        ?>
        <div id="upload_pdf-event-data<?php echo $suffix ?>" >
            <div class="upload_pdf-wrapper">
                <?php
                // DEFAULT DATA

                $arr_items = array(
                );



                ?>

                <script type="text/plain" id="upload_pdf-default-template">
                    <div class="count apollo-artist-upload_pdf pos-relative">
                        <div class="options_group event-box">
                            <p class="form-field upload_pdf_embed_field ">
                                <input type="button"
                                   class="apl-select-from-library" data-media-popup-title="<?php _e('Choose file', 'apollo'); ?>"
                                   data-media-button-title="<?php _e('Select', 'apollo'); ?>" readonly="true"
                                    value="<?php _e('Choose from media', 'apollo');?>"/>
                                <input type="hidden" class="apl-choose-pdf-from-media" name="upload_pdf_from_media<?php echo $suffix?>[]" />
                                <input type="file" class="apl-admin-upload-pdf" name="upload_pdf<?php echo $suffix?>[]" data-ride="upload_file"
                                data-alert="<?php _e('File too large. Maximum size allow is %s MB','apollo') ?>"
                                data-maxfilesize="<?php echo wp_max_upload_size(); ?>"
                                accept=".pdf"
                                 />
                                 <span class="apl-pdf-file-name"><?php _e('No file chosen', 'apollo'); ?></span>
                            </p>
                        </div>

                        <div class="options_group event-box artist-upload_pdf-desc">
                            <p class="form-field upload_pdf_desc_field ">
                                <label><?php _e( 'Description', 'apollo' ) ?></label>
                                <textarea class="wpc-75" name="upload_pdf_label<?php echo $suffix?>[]" placeholder="" rows="3" cols="20"></textarea>
                                <textarea class="apl-desc-pdf-from-media hidden" name="upload_pdf_label_from_media<?php echo $suffix?>[]" placeholder="" rows="3" cols="20"></textarea>
                            </p>
                        </div>
                        <div style="display: block;" data-target="parent" data-ride="delete-relation"  data-confirm="<?php _e( 'Are you sure to remove this record ?', 'apollo' ) ?>" class="del right_corner_abs mr-50"><i class="fa fa-times"></i></div>
                    </div>
                </script>

                <span id="_center_info_html" data-max-upload-file="<?php echo of_get_option(Apollo_DB_Schema::_MAX_UPLOAD_PDF_ARTIST, Apollo_Display_Config::_MAX_UPLOAD_PDF_DEFAULT) ?>" data-max_upload_size="<?php wp_max_upload_size(); ?> data-uploaded="0" />


                <div class="apollo-artist-upload_pdf-list">

                    <?php

                    // GET DATA - override $arr_award
                    global $wpdb;
                    
                    $arr_upload_pdf_info_id = maybe_unserialize(get_apollo_meta($thepostid, 'upload_pdf'. $suffix, true));
                   
                    if(!empty($arr_upload_pdf_info_id)) {

                        // build $arr_items
                        foreach($arr_upload_pdf_info_id as $id) {
                            array_push($arr_items, get_post($id)) ;
                        }
                    }
                    // FETCH DATA
                    if(!empty($arr_items)):
                        $i = 0;
                        foreach($arr_items as $item):
                            if ( ! $item ) continue;
                            ?>

                    <div class="count apollo-artist-upload_pdf pos-relative">

                        <div class="options_group event-box">
                            <p class="form-field upload_pdf_embed_field ">
                                <?php echo $item->post_title ?>
                            </p>
                        </div>

                        <div class="options_group event-box artist-upload_pdf-desc">
                            <p class="form-field upload_pdf_desc_field ">
                                <label><?php _e( 'Description', 'apollo' ) ?></label>
                                <textarea class="wpc-75" name="upload_pdf_label_old<?php echo $suffix?>[]" placeholder="" rows="3" cols="20"><?php echo $item->post_excerpt ?></textarea>
                            </p>
                        </div>
                        <div style="display: block;" data-ride="delete-relation" data-target="parent"
                             data-action="apollo_remove_upload_pdf"
                             data-action_data = '<?php echo $thepostid ?>, <?php echo $item->ID ?>'
                             data-confirm="Are you sure to remove this record ?" class="del right_corner_abs mr-50"><i class="fa fa-times"></i></div>
                    </div>
                            <?php
                            $i++;
                        endforeach;
                    endif;
                    ?>
                </div><!-- end apollo-artist-upload_pdf-list -->
            </div>

            <input type="button" class="button button-primary button-large apollo-btn-right"
                   data-ride="shadow-man"
                   data-template="#upload_pdf-default-template"
                   data-append-to = ".apollo-artist-upload_pdf-list:append"
                   data-wrapper="#upload_pdf-event-data<?php echo $suffix ?>"
                   data-maxtimes="<?php echo $maxUploadPDF ?>"
                   data-bornname=".apollo-artist-upload_pdf"
                   data-alert = "<?php _e('Max %s item allow!','apollo') ?>"
                   value="<?php _e( 'Add More', 'apollo' ) ?>" />

            <br/><br/>
        </div>

    <?php
    }

    public static function save( $post_id, $post, $suffix = '' ) {
       
        $arr_attachment_ids = array();
        
        $arr_description = array_map('sanitize_text_field', $_POST['upload_pdf_label'.$suffix]);
        $arr_description_media = array_map('sanitize_text_field', $_POST['upload_pdf_label_from_media'.$suffix]);
        $old_attachment_ids = maybe_unserialize(get_apollo_meta($post_id, 'upload_pdf'. $suffix, true));
        if(empty($old_attachment_ids)) $old_attachment_ids = array();
        $arrError = array();

        if(!empty($_FILES['upload_pdf'.$suffix]['name'])) {
            foreach($_FILES['upload_pdf'.$suffix]['name'] as $i => $name) {

                if(empty($name)) continue; /* Silent and continue */

                if ($_FILES['upload_pdf'.$suffix]['error'][$i] !== UPLOAD_ERR_OK) {
                    $arrError[] = array(
                        'message' => 'Error upload file: ' . $name,
                        'error' => true
                    );
                    continue;

                }

                // check filesize
                if($_FILES['upload_pdf'.$suffix]['size'][$i] > wp_max_upload_size()) {
                    $arrError[] = array(
                        'message' => 'File too large. Max upload size allowed is ' . number_format(wp_max_upload_size()/ (1024 * 1024), 2) . ' MB',
                        'error' => true
                    );
                    continue;
                }

                // copy to upload directory
                if ( ! ( ( $uploads = wp_upload_dir(  ) ) && false === $uploads['error'] ) ) {

                    $arrError[] = array(
                        'message' => 'Missing upload dir for file ' . $name,
                        'error' => true
                    );
                    continue;
                }
                $wp_filetype = wp_check_filetype_and_ext($_FILES['upload_pdf'.$suffix]['tmp_name'][$i], $name );
                $type = empty( $wp_filetype['type'] ) ? '' : $wp_filetype['type'];

                if($type !== 'application/pdf') {
                    $arrError[] = array(
                        'message' => 'File  '. $name .' is invalid. Please upload pdf file only!',
                        'error' => true
                    );
                    continue;
                }

                $unique_name = wp_unique_filename( $uploads['path'], $name );
                $filename = $uploads['path']. '/' . $unique_name;

                $move_new_file = @ move_uploaded_file( $_FILES['upload_pdf'.$suffix]['tmp_name'][$i], $filename );

                if ( false === $move_new_file ) {
                    $arrError[] = array(
                        'message' => 'Cannot move to directory: ' . $uploads['path'],
                        'error' => true
                    );
                    continue;
                }


                $url = $uploads['url'] .'/'. $unique_name;

                /* Save into post attachment */
                $attachment = array(
                    'post_mime_type' => $type,
                    'guid' => $url,
                    'post_title' => $unique_name,
                    'post_content' => '',
                    'post_excerpt' => isset($arr_description[$i]) ? Apollo_App::clean_data_request($arr_description[$i]) : '',
                );

                $attachment_id = wp_insert_attachment($attachment, $filename, $post_id);
                if ( !is_wp_error($attachment_id) ) {
                    array_push($arr_attachment_ids, $attachment_id);
                }
            }
        }

        /* Error mechasin */
        $_SESSION['messages']['artist-upload_pdf'] = $arrError;

        /* Update old part */
        $_POST['upload_pdf_label_old'.$suffix] = isset($_POST['upload_pdf_label_old'.$suffix]) ? $_POST['upload_pdf_label_old'.$suffix] : array();
        $arr_description_old = array_map('sanitize_text_field', $_POST['upload_pdf_label_old'.$suffix]);
        $actual_old_attachment_ids = array();
        foreach($old_attachment_ids as $i => $id) {
            $pdf_post = array(
                'ID'           => $id,
                'post_excerpt' => isset($arr_description_old[$i]) ? Apollo_App::clean_data_request($arr_description_old[$i]) : '',
            );
            if (isset($arr_description_old[$i])) {
                array_push($actual_old_attachment_ids, $id);
            }
            wp_update_post( $pdf_post );
        }

        /** @Ticket #16288 - Upload from media library */
        if (!empty($_POST['upload_pdf_from_media'.$suffix])) {
            $descMediaIndex = 0;
            foreach ($_POST['upload_pdf_from_media'.$suffix] as $attId) {
                if (!empty($attId)) {
                    $pdf_post = array(
                        'ID'           => $attId,
                        'post_excerpt' => $arr_description_media[$descMediaIndex]
                    );
                    wp_update_post($pdf_post);
                    array_push($actual_old_attachment_ids, intval($attId));
                }
                $descMediaIndex += 1;
            }
        }

        // Merge upload result
        if(!empty($arr_attachment_ids)) {
            $arr_attachment_ids = array_merge($actual_old_attachment_ids, $arr_attachment_ids);
        } else {
            $arr_attachment_ids = $actual_old_attachment_ids;
        }

        update_apollo_meta($post_id, 'upload_pdf'. $suffix, $arr_attachment_ids);
    }

    private static  function processError($result2) {
        if($result2 === false || $result2 === null) { // there are some error need notice
            $msg = __('There are something wrong. Cannot save data award', 'apollo');
            global $wpdb;
            if(isset($wpdb->last_error)) {
                $msg = $wpdb->last_error;
            }
            $_SESSION['messages']['artist-upload_pdf'][] = array(
                'message' =>$msg,
                'error' => true
            );

            return true;
        }

        return false;
    }
}