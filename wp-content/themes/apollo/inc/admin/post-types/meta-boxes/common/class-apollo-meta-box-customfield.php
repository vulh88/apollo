<?php
/**
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Customfield
 */
if( ! class_exists('Apollo_Meta_Box_Customfield')) {
    class Apollo_Meta_Box_Customfield {

        public function __construct()
        {
            add_action( 'admin_print_scripts', array($this,'additional_admin_scripts') , 99);
        }


        public function additional_admin_scripts(){
            echo "<script type='text/x-custom-template' id='apl_af_gallery_item_script_template'>\n";
            Apollo_App::getTemplatePartCustom(APOLLO_ADMIN_DIR .'/post-types/meta-boxes/common/templates/gallery_fields_part.php',
                array('image' => array("{{image}}"),
                    'targetLink' => "",
                    'id' => "{{id}}",
                    'index' => "{{index}}",
                    'memberName' => "",
                    'title' => "",
                    'name' => "{{name}}"),false);
            echo "\n</script>";
        }

        /**
         * Output the metabox
         * @param $post
         * @param array $args
         */
        public static function output( $post, $args = array() ) {

            global $post, $thepostid, $pagenow, $typenow;

            /**
             * ThienLD: @ticket #17843
             */
            // For all the default cases in all modules
            $currentPost = $post;
            if(isset($args['args']['render-type']) && $args['args']['render-type'] == Apollo_Const::_APL_ADMIN_METABOX_RENDER_TYPE_IN_OTHER_MODULE) {
                $bsPostID = isset($args['args']['associated-business-id']) ? (int) $args['args']['associated-business-id'] : -1;
                $bsPostObject = get_business($bsPostID);
                if($bsPostID > 0 && !empty($bsPostObject) && isset($bsPostObject->post)){
                    $currentPost = $bsPostObject->post;
                    $typenow = $currentPost->post_type;
                    // overwrite post object and thepostid for rendering custom fields usage
                    $post = $currentPost;
                    $thepostid = $bsPostID;
                }
            }

            wp_nonce_field( 'apollo_'.$currentPost->post_type.'_meta_nonce', 'apollo_'.$currentPost->post_type.'_meta_nonce' );
            $currentPostID = $currentPost->ID;
            $parent_key = Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA;
            $group_fields = Apollo_Custom_Field::get_group_fields($currentPost->post_type);

            if ( $group_fields ):
                foreach( $group_fields as $gf ):
                    echo '<fieldset class="apl-group-fields">';
                    echo '<legend>'.($gf['group']->label ? $gf['group']->label : __('No name', 'apollo')).'</legend>';

                    foreach( $gf['fields'] as $field ):

                        $meta = maybe_unserialize( $field->meta_data );
                        $locations = maybe_unserialize( $field->location );

                        switch( $field->cf_type ):
                            case 'text':
                                $validation = Apollo_Custom_Field::get_validation($field);
                                $max_length = Apollo_Custom_Field::get_character_limit($field);
                                $data_attr = $max_length ? 'maxlength="'.$max_length.'"' : '';
                                echo '<div class="options_group event-box">';
                                // Description
                                apollo_wp_text_input(  array(
                                    'id' => ''. $parent_key. $field->name,
                                    'name' => ''. $parent_key .'['. $field->name .']',
                                    'label' => $field->label,
                                    'desc_tip' => 'true',
                                    'description' => isset($meta['desc']) ? $meta['desc'] : '',
                                    'class' => 'apollo_input_'. $validation,
                                    'required'   => $field->required == 1,
                                    'default'   => $pagenow == 'post-new.php' && isset( $meta['default_value'] ) ? $meta['default_value'] : '',
                                    'data_elm'  => $data_attr,
                                ) );
                                echo '<div class="clear"></div>';
                                echo '</div>';

                                break;

                            case 'select':
                                $options = Apollo_Custom_Field::get_choice( $field );
                                $display_style = Apollo_Custom_Field::get_display_style($field);

                                if ( ! $options ) continue;

                                $data_attr = '';
                                if ( $actionReference = Apollo_Custom_Field::get_action_refer_to($field, $currentPost->post_type) ) {
                                    $data_attr = sprintf( 'data-action="'.$actionReference.'" data-ride="ap-refer-select" data-target="#%s"', Apollo_Custom_Field::get_refer_to($field));
                                }

                                echo '<div class="options_group event-box '.($display_style == 'right_label' ? 'apl_cf_w_8_2' : '').'">';
                                // Description
                                apollo_wp_select(  array(
                                    'name'          => ''. $parent_key .'['. $field->name .']',
                                    'id'            => $field->name,
                                    'label'         => esc_attr($field->label),
                                    'desc_tip'      => 'true',
                                    'description'   => isset($meta['desc']) ? $meta['desc'] : '',
                                    'required'      => $field->required == 1,
                                    'options'       => $options,
                                    'attr'          => $data_attr,
                                    'default'       => $pagenow == 'post-new.php' && isset( $meta['default_value'] ) ? $meta['default_value'] : '',
                                ) );

                                if ( Apollo_Custom_Field::has_explain_field($field) ) {
                                    // Description
                                    apollo_wp_textarea_input(  array(
                                        'id' => ''. $parent_key .'['. $field->name .'_explain]',
                                        'desc_tip' => 'true',
                                        'description' => "",
                                        'label_class'   => 'full',
                                    ) );
                                }

                                echo '<div class="clear"></div>';
                                echo '</div>';
                                break;

                            case 'textarea':
                                $max_length = Apollo_Custom_Field::get_character_limit($field);
                                $data_attr = $max_length ? 'maxlength="'.$max_length.'"' : '';
                                echo '<div class="options_group event-box">';
                                // Description
                                apollo_wp_textarea_input(  array(
                                    'id' => ''. $parent_key .'['. $field->name .']',
                                    'label' => $field->label,
                                    'desc_tip' => 'true',
                                    'description' => isset($meta['desc']) ? $meta['desc'] : '',
                                    'label_class'   => 'full',
                                    'required'   => $field->required == 1,
                                    'default'   => $pagenow == 'post-new.php' && isset( $meta['default_value'] ) ? $meta['default_value'] : '',
                                    'data_elm'  => $data_attr,
                                ) );

                                echo '</div>';
                                break;

                            case 'multi_checkbox':

                                $choices = Apollo_Custom_Field::get_choice( $field );
                                if ( ! $choices ) continue;
                                echo '<div class="options_group apollo-checkbox clear">';
                                echo '<label class="apl-multi-choice">'.$field->label.' '.apollo_the_required_field( array( 'required' => $field->required == 1 ) ).'</label>';
                                /*@ticket #18221: 0002504: Arts Education Customizations - Add a checkbox option for "Add Menu Arrow" - item 1*/
                                if(isset($meta["add_menu_arrow"]) && $meta['add_menu_arrow']){
                                    echo '<span class="expander" data-toggle-class="show"></span>';
                                }
                                echo '<span class="description">' . wp_kses_post( $meta['desc'] ) . '</span>';
                                echo '<ul>';
                                foreach ( $choices as $k_choice => $choice ):
                                    /** @Ticket #18222 Render label */
                                    if (strpos($choice, '[label]') > -1) : ?>
                                        <label class="apl-item-none-checkbox"><?php echo trim(str_replace('[label]', '', $choice)); ?></label>
                                    <?php
                                    else :
                                        // Description
                                        apollo_wp_multi_checkbox(  array(
                                                'id'            => ''. $parent_key. $k_choice .'['. $field->name .']',
                                                'name'          => ''. $parent_key. '['. $field->name .'][]',
                                                'label'         => $choice,
                                                'desc_tip'      => 'true',
                                                'description'   => '',
                                                'type'          => 'checkbox',
                                                'default'       => $pagenow == 'post-new.php' &&  Apollo_Custom_Field::get_default_value_choice( $meta ),
                                                'cbvalue'       => $k_choice )
                                        );
                                    endif;
                                endforeach;
                                echo '</ul>';

                                apollo_wp_hidden_input(array(
                                    'id'            => ''. $parent_key. '['. $field->name .']',
                                    'name'          => 'apl-cf-multi-checkbox-hidden',
                                    'extend_name'   => ''. $parent_key. '['. $field->name .']',
                                    'required'      => $field->required == 1,
                                    'value'         => Apollo_App::apollo_get_meta_data( $currentPostID, $parent_key.'['.$field->name. ']' ) ? 1 : ''
                                ));

                                echo '</div>';

                                break;

                            case 'checkbox':

                                if (is_array($locations) && in_array( 'rsb' , $locations ) ) {
                                    $p_key = Apollo_DB_Schema::_APL_POST_TYPE_CF_SEARCH;
                                } else {
                                    $p_key = $parent_key;
                                }

                                $choices = Apollo_Custom_Field::get_choice( $field );

                                echo '<div class="options_group clear">';
                                apollo_wp_checkbox(  array(
                                    'id'            => ''. $p_key. '['. $field->name .']',
                                    'label'         => $field->label,
                                    'desc_tip'      => 'true',
                                    'description'   => isset($meta['desc']) ? $meta['desc'] : '',
                                    'type'          => 'checkbox',
                                    'default'       => $pagenow == 'post-new.php' && isset( $meta['default_value'] ) ? $meta['default_value'] : '',
                                    'cbvalue'       => $field->name ) );

                                echo '</div>';

                                break;


                            case 'radio':

                                $has_other_choice = Apollo_Custom_Field::has_other_choice($field);
                                $options = Apollo_Custom_Field::get_choice( $field );

                                if ($has_other_choice) {
                                    $options['other_choice'] = __("Other", "apollo");
                                }

                                if ( ! $options ) continue;
                                echo '<div class="options_group clear">';

                                apollo_wp_radio( array(
                                    'id' => ''. $parent_key .'['. $field->name .']',
                                    'label' => $field->label,
                                    'desc_tip' => 'true',
                                    'description' => isset($meta['desc']) ? $meta['desc'] : '',
                                    'class'         => $has_other_choice ? 'radio-other-choice' : '',
                                    'options' => $options,
                                    'default'   => $pagenow == 'post-new.php' && isset( $meta['default_value'] ) ? $meta['default_value'] : '',
                                    'required'   => $field->required == 1,
                                ) );

                                apollo_wp_hidden_input(array(
                                    'id'            => ''. $parent_key. '['. $field->name .']',
                                    'name'          => 'apl-cf-multi-radio-hidden',
                                    'extend_name'   => ''. $parent_key. '['. $field->name .']',
                                    'required'      => $field->required == 1,
                                    'value'         => Apollo_App::apollo_get_meta_data( $currentPostID, $parent_key.'['.$field->name. ']' ) ? 1 : ''
                                ));

                                if ( $has_other_choice ) {
                                    $val = Apollo_App::apollo_get_meta_data( $currentPostID, ''. $parent_key .'['. $field->name .']' );
                                    // Description
                                    apollo_wp_textarea_input(  array(
                                        'label' => '',
                                        'id' => ''. $parent_key .'['. $field->name .'_other_choice]',
                                        'desc_tip' => 'true',
                                        'description' => "",
                                        'label_class'   => 'full',
                                        'class' => ' '.($val != "other_choice" ? "hidden" : "").' other-choice',
                                    ) );
                                    echo '<div class="clear"></div>';
                                }

                                echo '</div>';
                                break;

                            case 'wysiwyg':
                                // Description
                                apollo_wp_editor(  array(
                                    'id'            => $field->name,
                                    'name'          => ''. $parent_key. '['. $field->name .']',
                                    'label'         => $field->label,
                                    'desc_tip'      => 'true',
                                    'description'   => $meta['desc'],
                                    'class'         => 'apl-editor',
                                    'required'      => $field->required == 1,
                                    'settings'      => array(
                                        'media_buttons' => FALSE
                                    )
                                ) );
                                break;
                            case 'gallery':
                                $galleriesData = Apollo_App::getMetaDataForGallery($currentPost->ID, $typenow, $field->name);
                                $label = Apollo_App::getMetaDataForGallery($currentPostID, $typenow, 'apl_'.$field->name.'_label');
                                $label = !empty($label) ? $label : $field->label;
                                $topDesc = Apollo_App::getMetaDataForGallery($currentPostID, $typenow, 'apl_'.$field->name.'_desc');
                                $topDesc =  !empty($topDesc) ? $topDesc : '';
                                $bottomDesc = Apollo_App::getMetaDataForGallery($currentPostID, $typenow, 'apl_'.$field->name.'_bottom_desc');
                                $bottomDesc =  !empty($bottomDesc) ? $bottomDesc : '';
                                ?>
                                <table class="form-table gallery-metabox" data-name="<?php echo $field->name ?>">
                                    <tr>
                                        <?php
                                        echo '<div class="options_group event-box">';
                                        apollo_wp_text_input(  array(
                                            'name' => 'apl_'.$field->name.'_label',
                                            'label' => __('Title','apollo'),
                                            'class' => 'apollo_input',
                                            'required'   => false,
                                            'default'   => $label,
                                        ) );

                                        echo '</div>';

                                        echo '<div class="clear"></div>';
                                        echo '<div class="options_group event-box">';
                                        apollo_wp_textarea_input(  array(
                                            'label' => __('Top description','apollo'),
                                            'id' => 'apl_'.$field->name.'_desc',
                                            'class' => 'apl_cf_gallery_desc',
                                            'wrapper_class' => 'apl_cf_gallery_class',
                                            'num-row' => 5,
                                            'description' => __('The top description will be displayed on the top of gallery photos','apollo'),
                                            'value' => $topDesc,
                                        ) );
                                        echo  '</div>';

                                        echo '<div class="options_group event-box">';
                                        apollo_wp_textarea_input(  array(
                                            'label' => __('Bottom description','apollo'),
                                            'id' => 'apl_'.$field->name.'_bottom_desc',
                                            'class' => 'apl_cf_gallery_desc',
                                            'wrapper_class' => 'apl_cf_gallery_class',
                                            'num-row' => 5,
                                            'description' => __('The bottom description will be displayed on the bottom of gallery photos','apollo'),
                                            'value' => $bottomDesc,
                                        ) );
                                        echo  '</div>';
                                        ?>
                                        <td>
                                            <a class="gallery-add button" href="#"
                                               data-uploader-title="Add image(s) to gallery"
                                               data-uploader-button-text="Add image(s)"><?php _e('Add image(s)','apollo'); ?></a>
                                            <ul class="gallery-metabox-list">
                                                <?php if (!empty($galleriesData)) :
                                                    foreach ($galleriesData as  $index => $gItem) :
                                                        if (empty($gItem)) {
                                                            continue;
                                                        }
                                                        $image = wp_get_attachment_image_src($gItem['id']);
                                                        $targetLink = $gItem['url'];
                                                        $id = $gItem['id'];
                                                        $title = $gItem['title'];
                                                        $memberName = $gItem['name'];

                                                        Apollo_App::getTemplatePartCustom(APOLLO_ADMIN_DIR . '/post-types/meta-boxes/common/templates/gallery_fields_part.php',
                                                            array('image' => $image,
                                                                'targetLink' => $targetLink,
                                                                'id' => $id,
                                                                'index' => $index,
                                                                'memberName' => $memberName,
                                                                'title' => $title,
                                                                'name' => $field->name), false);
                                                    endforeach;endif; ?>
                                            </ul>

                                        </td>
                                    </tr>
                                </table>
                                <?php
                                break;
                        endswitch;

                    endforeach; // End loop fields
                    echo '</fieldset>'; // End apl-group-field
                endforeach;
            endif;


            /**
             * Below is to revert post & thepostid & typenow as globally for the default custom fields of current post type using
             * ThienLD: @ticket #17843
             */
            if(isset($args['args']['render-type']) && $args['args']['render-type'] == Apollo_Const::_APL_ADMIN_METABOX_RENDER_TYPE_IN_OTHER_MODULE) {
                $addedByModule = isset($args['args']['added-by-module']) ? $args['args']['added-by-module'] : '';
                $addedByPostID = isset($args['args']['added-by-post-id']) ? (int) $args['args']['added-by-post-id'] : -1;
                if($addedByPostID > 0) {
                    switch ($addedByModule) {
                        case Apollo_DB_Schema::_ORGANIZATION_PT:
                            $postObjectData = get_org($addedByPostID);
                            if(!empty($postObjectData)){
                                $post = $postObjectData->post;
                                $thepostid = $addedByPostID;
                                $typenow  = $postObjectData->post->post_type;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        public static function save( $post_id, $post ) {

            /**
             * ThienLD: revise due to @ticket #17843
             * Return if save custom fields data of another post-type within the current submitted one
             * For save async for all custom fields of another post-type within the current submitted one then use self::saveByPassingPostedData
             */
            if( isset($_POST['post_ID']) && (int) $_POST['post_ID'] !== (int) $post_id ){
                return;
            }

            /** @Ticket #13482 */
            if ( isset($_POST[Apollo_DB_Schema::_APL_POST_TYPE_CF_SEARCH]) ) {
                $qualis = $_POST[Apollo_DB_Schema::_APL_POST_TYPE_CF_SEARCH];
                /** @Ticket - #14085 */
                update_apollo_meta($post_id, Apollo_DB_Schema::_APL_POST_TYPE_CF_SEARCH, maybe_serialize( $qualis ) );
            } else {
                $qualis = '';
            }

            if ( isset($_POST[Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA] ) ) {
                $custom_fields = !empty($qualis) ? array_merge($qualis, $_POST[Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA]) : $_POST[Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA];
            } else {
                $custom_fields = $qualis;
            }
            update_apollo_meta($post_id, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA, maybe_serialize( $custom_fields ));

            $fields = Apollo_Custom_Field::get_fields_by_type($post->post_type, 'gallery');

                $cfGalleryFields = array();
                if (!empty($fields)){
                    foreach ($fields as $field){
                        $cfGalleryFields[] = $field->name;
                        if(isset($_POST['apl_'.$field->name.'_label']) ) {
                            Apollo_App::saveMetaDataForGallery($post_id, $post->post_type, $_POST['apl_'.$field->name.'_label'],'apl_'.$field->name.'_label' );
                        }
                        if(isset($_POST['apl_'.$field->name.'_desc']) ) {
                            Apollo_App::saveMetaDataForGallery($post_id, $post->post_type, $_POST['apl_'.$field->name.'_desc'], 'apl_'.$field->name.'_desc' );
                        }
                        if(isset($_POST['apl_'.$field->name.'_bottom_desc']) ) {
                            Apollo_App::saveMetaDataForGallery($post_id, $post->post_type, $_POST['apl_'.$field->name.'_bottom_desc'], 'apl_'.$field->name.'_bottom_desc' );
                        }
                    }
                }
                $posGalleryFields = array();

            if(isset($_POST['apl_gallery_fields']) && !empty($_POST['apl_gallery_fields'])) {
                foreach ( $_POST['apl_gallery_fields']  as $fieldName => $gItems){
                    $posGalleryFields[] = $fieldName;
                    $arr = array();
                    if ( !empty($gItems)){
                        foreach ( $gItems as $items) {
                            if ( !empty($items)){
                                $arr[] = $items;
                            }
                        }
                    }
                    Apollo_App::saveMetaDataForGallery($post_id, $post->post_type, $arr, $fieldName );
                }
                $fieldsBeDeleted = array_diff($cfGalleryFields,$posGalleryFields);
                if (!empty($fieldsBeDeleted)) {
                    foreach ($fieldsBeDeleted as $name ) {
                        delete_post_meta( $post_id, $name );
                    }
                }


            }

            // Only copy to mini site when not switched to the child site yet
            global $switched;
            if (!$switched && get_the_ID() === $post_id) {
                do_action('apollo_copy_to_mini_sites', $post_id);
            }
        }


        /**
         * Save custom fields data by passing posted-data instead of getting directly from $_POST
         * Just use for the special cases which looks like AJAX request for example
         * @author ThienLD
         * @ticket #17843
         * @param array $args
         */
        public static function saveByPassingPostedData($args = array() ) {

            $post = isset($args['post']) ? $args['post'] : array();
            $post_id = isset($args['post_id']) ? $args['post_id'] : -1;
            $posted_data = isset($args['posted_data']) ? $args['posted_data'] : array();

            if(empty($post) || empty($post_id)){
                return;
            }

            /** @Ticket #13482 */
            if ( isset($posted_data[Apollo_DB_Schema::_APL_POST_TYPE_CF_SEARCH]) ) {
                $qualis = $posted_data[Apollo_DB_Schema::_APL_POST_TYPE_CF_SEARCH];
                /** @Ticket - #14085 */
                update_apollo_meta($post_id, Apollo_DB_Schema::_APL_POST_TYPE_CF_SEARCH, maybe_serialize( $qualis ) );
            } else {
                $qualis = '';
            }

            if ( isset($posted_data[Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA] ) ) {
                $custom_fields = !empty($qualis) ? array_merge($qualis, $posted_data[Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA]) : $posted_data[Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA];
            } else {
                $custom_fields = $qualis;
            }
            update_apollo_meta($post_id, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA, maybe_serialize( $custom_fields ));

            $fields = Apollo_Custom_Field::get_fields_by_type($post->post_type, 'gallery');

            $cfGalleryFields = array();
            if (!empty($fields)){
                foreach ($fields as $field){
                    $cfGalleryFields[] = $field->name;
                    if(isset($posted_data['apl_'.$field->name.'_label']) ) {
                        Apollo_App::saveMetaDataForGallery($post_id, $post->post_type, $posted_data['apl_'.$field->name.'_label'],'apl_'.$field->name.'_label' );
                    }
                    if(isset($posted_data['apl_'.$field->name.'_desc']) ) {
                        Apollo_App::saveMetaDataForGallery($post_id, $post->post_type, $posted_data['apl_'.$field->name.'_desc'], 'apl_'.$field->name.'_desc' );
                    }
                    if(isset($posted_data['apl_'.$field->name.'_bottom_desc']) ) {
                        Apollo_App::saveMetaDataForGallery($post_id, $post->post_type, $posted_data['apl_'.$field->name.'_bottom_desc'], 'apl_'.$field->name.'_bottom_desc' );
                    }
                }
            }
            $posGalleryFields = array();

            if(isset($posted_data['apl_gallery_fields']) && !empty($posted_data['apl_gallery_fields'])) {
                foreach ( $posted_data['apl_gallery_fields']  as $fieldName => $gItems){
                    $posGalleryFields[] = $fieldName;
                    $arr = array();
                    if ( !empty($gItems)){
                        foreach ( $gItems as $items) {
                            if ( !empty($items)){
                                $arr[] = $items;
                            }
                        }
                    }
                    Apollo_App::saveMetaDataForGallery($post_id, $post->post_type, $arr, $fieldName );
                }
                $fieldsBeDeleted = array_diff($cfGalleryFields,$posGalleryFields);
                if (!empty($fieldsBeDeleted)) {
                    foreach ($fieldsBeDeleted as $name ) {
                        delete_post_meta( $post_id, $name );
                    }
                }


            }

            // Only copy to mini site when not switched to the child site yet
            global $switched;
            if (!$switched && get_the_ID() === $post_id) {
                do_action('apollo_copy_to_mini_sites', $post_id);
            }
        }
    }

    new Apollo_Meta_Box_Customfield();
}



