
<?php
$id = isset($template_args['id']) && !empty($template_args['id']) ? $template_args['id'] : '';
$image = isset($template_args['image']) && !empty($template_args['image']) ? $template_args['image'][0] : '';
$targetLink = isset($template_args['targetLink']) && !empty($template_args['targetLink']) ? $template_args['targetLink'] : '';
$memberName = isset($template_args['memberName']) && !empty($template_args['memberName']) ? $template_args['memberName'] : '';
$title = isset($template_args['title']) && !empty($template_args['title']) ? $template_args['title'] : '';
$name = isset($template_args['name']) && !empty($template_args['name']) ? $template_args['name'] : '';
$index    = isset($template_args['index'])  ? $template_args['index'] : '';
?>

<li>
    <input type="hidden" name="apl_gallery_fields[<?php echo $name?>][<?php echo $index?>][id]"
           value="<?php echo $id; ?>">
    <img class="image-preview" src="<?php echo $image; ?>">
    <a class="change-image button button-small" href="#"
       data-uploader-title="Change image"
       data-uploader-button-text="Change image"><?php _e('Change image','apollo') ?></a><br>
    <input name="apl_gallery_fields[<?php echo $name?>][<?php echo $index?>][url]" class="target-link" placeholder="<?php _e('Target Link ','apollo'); ?>" value="<?php echo $targetLink; ?>"/>
    <input name="apl_gallery_fields[<?php echo $name?>][<?php echo $index?>][name]" class="target-link" placeholder="<?php _e('Name ','apollo'); ?>" value="<?php echo $memberName; ?>"/>
    <input name="apl_gallery_fields[<?php echo $name?>][<?php echo $index?>][title]" class="target-link" placeholder="<?php _e('Title','apollo'); ?>" value="<?php echo $title; ?>"/>
    <small><a class="remove-image" href="#"><?php _e('Remove image','apollo') ?></a></small>
</li>

