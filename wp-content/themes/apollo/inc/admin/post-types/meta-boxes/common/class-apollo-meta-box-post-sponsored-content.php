<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Post_Sponsored_Content
 * @ticket #16768: 0002309: BLOG - Sponsored Content of the blog
 */
class Apollo_Meta_Box_Post_Sponsored_Content {

    /**
     * Output the metabox
     */
    public static function output( $post ) {
        $sponsoredContent = get_post_meta($post->ID,'post-sponsored-content',true);
        $sponsoredValue = ($sponsoredContent && $sponsoredContent === 'on') ? 'checked' : '';

        /*@ticket #17756:  BLOG - Sponsored Content - New Page checkbox and URL field textbox*/
        $newPage = get_post_meta($post->ID,'post-new-page',true);
        $newPageValue = ($newPage && $newPage === 'on') ? 'checked' : '';

        $url = get_post_meta($post->ID,'post-new-url',true);
        $urlValue = $url ? $url : '';

        ?>
        <div>
            <p>
                <input type="checkbox" name="post-sponsored-content" id="post-sponsored-content"
                    <?php echo $sponsoredValue?>/>
                <label for="post-sponsored-content"><?php echo __('Sponsored Content')?></label>
            </p>
            <p>
                <input type="checkbox" name="post-new-page" class="post-new-page" <?php echo $newPageValue?>/>
                <label><?php echo __('New Page')?></label>
            </p>
            <p>
                <label><?php echo __('URL')?></label>
                <input type="text" name="post-new-url" class="post-new-url apollo_input_url"  value="<?php echo $urlValue?>"/>
            </p>
        </div>
        <?php
    }

    public static function save( $post_id, $post ){

        if($post && isset($post->post_type) && in_array($post->post_type, array('post', 'news'))){
            $sponsoredContent =  isset($_POST['post-sponsored-content']) ? $_POST['post-sponsored-content'] : '';
            update_post_meta($post_id,'post-sponsored-content',$sponsoredContent);

            /*@ticket #177560:  BLOG - Sponsored Content - New Page checkbox and URL field textbox*/
            $newPage =  isset($_POST['post-new-page']) ? $_POST['post-new-page'] : '';
            update_post_meta($post_id,'post-new-page', $newPage);

            $newURL =  isset($_POST['post-new-url']) ? $_POST['post-new-url'] : '';
            if ( !filter_var( $newURL, FILTER_VALIDATE_URL) ) {
                $newURL = '';
            }
            update_post_meta($post_id,'post-new-url',$newURL);
        }

    }
}