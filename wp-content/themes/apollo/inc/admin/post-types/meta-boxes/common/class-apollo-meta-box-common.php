<?php
/**
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Common
 */
class Apollo_Meta_Box_Common {

    public static function output_users($post) {
        global $post, $thepostid;
        $type = strtolower( $post->post_type );
		wp_nonce_field( 'apollo_'.$type.'_meta_nonce', 'apollo_'.$type.'_meta_nonce' );
        $thepostid = $post->ID;
        
        if ( $post->post_type == Apollo_DB_Schema::_ORGANIZATION_PT ) {
            $id = 'org_id';
        } else {
            $id = strtolower(str_replace('-', '_', $post->post_type) ). '_id';
        }
        $blog_id = get_current_blog_id();
        $apl_q = new Apl_Query( Apollo_App::getBlogUserTable(), true );
        $results = $apl_q->get_where( "$id = $post->ID AND blog_id = $blog_id " );

        if ( $results ) {
            echo '<ul>';
            foreach ( $results as $r ) {
                $user = get_user_by( 'id', $r->user_id);
                if ( ! $user ) continue;
                echo '<li><a target="_BLANK" href="'.  get_edit_user_link($r->user_id).'">'.$user->user_nicename.'</a></li>';
            }
            echo '</ul>';
        }
    }

    /**
     * Display list of associated users in module (artist, organization, venue, educator) detail page
     *
     * @param $post
     * @ticket #15226 - [CF] 20180126 - [2049#c12311][Admin] Selected association users on the organization, venue, educator, artist detail
     */
    public static function output_associated_users($post)
    {
        $associationUserModuleListComp = apl_instance('APL_Lib_Association_Components_AssociationUsersModuleList');
        $associationUserModuleListComp->render(array(
            'id'            => $post->ID,
            'post_type'     => $post->post_type,
            'page'          => 'detail',
            'display_title' => false,
        ));
    }

    /**
     * Display list of associated agencies in module (artist, organization, venue, educator) detail page
     *
     * @param $post
     * @ticket #15226 - [CF] 20180126 - [2049#c12311][Admin] Selected association users on the organization, venue, educator, artist detail
     */
    public static function output_associated_agencies($post)
    {
        if (Apollo_App::is_avaiable_module(Apollo_DB_Schema::_AGENCY_PT)) {
            $associationAgencyModuleListComp = apl_instance('APL_Lib_Association_Components_AssociationAgenciesModuleList');
            $associationAgencyModuleListComp->render(array(
                'id'            => $post->ID,
                'post_type'     => $post->post_type,
                'page'          => 'detail',
                'display_title' => false,
            ));
        }
    }
    
    public static function output_icons($post) {
        
        global $post, $thepostid, $typenow;
		wp_nonce_field( 'apollo_'.$post->post_type.'_meta_nonce', 'apollo_'.$post->post_type.'_meta_nonce' );
        $thepostid = $post->ID;

        switch( $typenow ) {
            case Apollo_DB_Schema::_EDUCATOR_PT:
                $opt_name = Apollo_DB_Schema::_APL_EDUCATORS_ICONS;
                $meta_key = Apollo_DB_Schema::_APL_EDUCATOR_POST_ICONS;
                $_data_k = Apollo_DB_Schema::_APL_EDUCATOR_DATA;
                break;
            case Apollo_DB_Schema::_ARTIST_PT:
                $opt_name = Apollo_DB_Schema::_APL_ARTISTS_ICONS;
                $meta_key = Apollo_DB_Schema::_APL_ARTIST_POST_ICONS;
                $_data_k = Apollo_DB_Schema::_APL_ARTIST_DATA;
                break;
            case Apollo_DB_Schema::_ORGANIZATION_PT:
                $opt_name = Apollo_DB_Schema::_APL_ORG_ICONS;
                $meta_key = Apollo_DB_Schema::_APL_ORG_POST_ICONS;
                $_data_k = Apollo_DB_Schema::_APL_ORG_DATA;
                break;
            case Apollo_DB_Schema::_VENUE_PT:
                $opt_name = Apollo_DB_Schema::_APL_VENUE_ICONS;
                $meta_key = Apollo_DB_Schema::_APL_VENUE_POST_ICONS;
                $_data_k = Apollo_DB_Schema::_APL_VENUE_DATA;
                break;
            case Apollo_DB_Schema::_CLASSIFIED:
                $opt_name = Apollo_DB_Schema::_APL_CLASSIFIED_ICONS;
                $meta_key = Apollo_DB_Schema::_APL_CLASSIFIED_POST_ICONS;
                $_data_k = Apollo_DB_Schema::_APL_CLASSIFIED_DATA;
                break;
            case Apollo_DB_Schema::_PUBLIC_ART_PT:
                $opt_name = Apollo_DB_Schema::_APL_PUBLIC_ART_ICONS;
                $meta_key = Apollo_DB_Schema::_APL_PUBLIC_ART_POST_ICONS;
                $_data_k = Apollo_DB_Schema::_APL_PUBLIC_ART_DATA;
                break;
            case Apollo_DB_Schema::_EVENT_PT:
                $opt_name = Apollo_DB_Schema::_APL_EVENT_ICONS;
                $meta_key = Apollo_DB_Schema::_APL_EVENT_POST_ICONS;
                $_data_k = Apollo_DB_Schema::_APOLLO_EVENT_DATA;
                break;
            default:
                $opt_name = Apollo_DB_Schema::_APL_EDUCATORS_ICONS;
                $meta_key = Apollo_DB_Schema::_APL_EDUCATOR_POST_ICONS;
                $_data_k = Apollo_DB_Schema::_APL_EDUCATOR_DATA;
        }
        
        $_meta_data = maybe_unserialize(get_apollo_meta($thepostid, $_data_k, true));
  
        $post_icon_ids = isset( $_meta_data[$meta_key] ) ? $_meta_data[$meta_key] : array();


        $icon_fields = @unserialize( get_option($opt_name) );

        if ( ! $icon_fields ) {
            $icon_fields = array( array( 'icon_id' => '', 'educator_id'   => '', 'icon_desc' => '' ) );   
        }

        if (isset($_GET['vu'])) {
            var_dump($post_icon_ids);
        }

        foreach ( $icon_fields as $k => $icon ) {
            if ( ! $icon['icon_id'] )                continue;
            $desc = explode( ' ' , $icon_fields[$k]['icon_desc'] );

            $value = !empty($icon['icon_key']) ? $icon['icon_key'] : $icon['icon_id'];

            echo '<div class="options_group">';
                // Description
                apollo_wp_checkbox(  array(
                    'id' => 'educator_icon_'. $k,
                    'name' => ''.$_data_k.'['.$meta_key.'][]',
                    'label' => wp_get_attachment_image( $icon['icon_id'], 'thumbnail' ). ' '. implode( ' ' , array_slice( $desc , 0, 15 )),
                    'desc_tip' => 'true',
                    'description' => "",
                    'value' => $post_icon_ids && in_array( $value, $post_icon_ids ) ? $value : '',
                    'cbvalue'   => $value,
                    'type' => 'checkbox', ) );
            echo '</div>';
        }
    }

    /**
     * @Ticket #16209 - Render user info
     * @param $post
     */
    public static function outputChangeAuthor($post) {

        $author = get_userdata($post->post_author);
        echo '<select
                data-enable-remote="1"
                data-source-url="apollo_get_remote_user_data_to_select2_box"
                id="apl-change-author"
                class="apl_select2"
                name="apl-change-author"
            >
            <option value="">'.__("Select author", "apollo").'</option>';
        if (!empty($author)) {
            $text = $author->data->display_name . '('. $author->data->user_email . ')';
            echo sprintf('<option selected="selected" value="%s">%s</option>', $author->ID, $text);
        }
        echo '</select>';
    }

    /**
     * @Ticket #16209 - Change author
     * @param $post_id
     * @param $post
     */
    public static function updateChangeAuthor($post_id, $post) {
        if (isset($_POST['apl-change-author'])) {
            if (empty($_POST['apl-change-author'])) {
                /*@ticket #16642: [CF] 20180713 - Remove the associated author when do not select an any author.*/
                $authorId = '';
            } else {
                $authorId = intval($_POST['apl-change-author']);
            }
            global $wpdb;
            $dataUpdate = array(
                'post_author' => $authorId
            );
            $where = array(
                'ID' => $post_id
            );
            $wpdb->update($wpdb->posts, $dataUpdate, $where);
        }
    }

    /** @Ticket #19481 migrate data (new blog posts and news posts)*/
    public static function outputCopyToAnotherSite($post) {
        $postType = get_post_type($post);
        $copyValue = get_post_meta($post->ID, 'apl_copy_to_another_site', true);
        $checked = !empty($copyValue) ? 'checked' : '';
        echo '<input type="checkbox" name="apl_copy_'.$postType.'_to_another_site" value="' . APOLLO_BLOG_NEWS_DESTINATION_SITE_ID . '" ' . $checked . ' >' . APOLLO_BLOG_NEWS_DESTINATION_SITE_DOMAIN . '<br>';
    }

    public static function updateCopyToAnotherSite($post_id, $post) {
        $postType = get_post_type($post_id);
        if (!empty($_POST['apl_copy_'.$postType.'_to_another_site'])) {
            update_post_meta($post_id, 'apl_copy_to_another_site', $_POST['apl_copy_'.$postType.'_to_another_site']);
            require_once APOLLO_INCLUDES_DIR . '/Lib/Helpers/SyncBlogNewsData.php';
            $copyToAnotherSite = new APL_Lib_Helpers_Sync_Blog_News_Data();
            $copyToAnotherSite->copyDataToDestinationSite($post_id, APOLLO_BLOG_NEWS_DESTINATION_SITE_ID, $post->post_type);
        } else {
            update_post_meta($post_id, 'apl_copy_to_another_site', '');
        }
    }
}
