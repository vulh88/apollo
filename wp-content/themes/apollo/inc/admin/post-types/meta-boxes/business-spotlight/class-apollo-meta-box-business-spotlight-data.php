<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo Business Spotlight custom post type meta data
 *
 * @category    Admin
 * @package     inc/admin/post-types/meta-boxes
 * @ticket      #11186
 */
class Apollo_Meta_Box_Business_Spotlight_Data
{
    /**
     * Output the Link meta box
     *
     * @param $post
     */
    public static function output( $post )
    {
        wp_nonce_field( 'apollo_business_spotlight_meta_nonce', 'apollo_business_spotlight_meta_nonce' );

        global $post, $thepostid;
        $thepostid      = $post->ID;
        $data           = get_post_meta( $thepostid, Apollo_DB_Schema::_APL_BUSINESS_SPOTLIGHT_LINK );
        $value          = $data && $data[0] ? $data[0] : '';
        $arr_open_style = get_post_meta( $thepostid, Apollo_DB_Schema::_APL_BUSINESS_SPOTLIGHT_LINK_OPEN_STYLE );
        $val_open_style = $arr_open_style && $arr_open_style[0] ? $arr_open_style[0] : '';

        echo '<div class="options_group event-box">';
        apollo_wp_text_input(array(
            'id'          => Apollo_DB_Schema::_APL_BUSINESS_SPOTLIGHT_LINK,
            'label'       => '',
            'desc_tip'    => 'true',
            'description' => '',
            'class'       => 'apollo_input_url',
            'value'       => $value,
        ) );
        echo '</div>';
        echo '<div class="options_group event-box italic">';
        echo '<input type="checkbox" ' . checked( $val_open_style, true, false ) . ' name="' . Apollo_DB_Schema::_APL_BUSINESS_SPOTLIGHT_LINK_OPEN_STYLE . '" />' . "Is open new page ?";
        echo '</div>';
    }

    /**
     * Save meta box data
     *
     * @param $post_id
     */
    public static function save( $post_id )
    {
        // save Link URL
        $link = '';
        if ( filter_var( $_POST[Apollo_DB_Schema::_APL_BUSINESS_SPOTLIGHT_LINK], FILTER_VALIDATE_URL ) ) {
            $link = $_POST[Apollo_DB_Schema::_APL_BUSINESS_SPOTLIGHT_LINK];
        }
        update_post_meta(
            $post_id,
            Apollo_DB_Schema::_APL_BUSINESS_SPOTLIGHT_LINK,
            Apollo_App::clean_data_request( $link )
        );

        // save Link "Is open new page ?" style
        $val_open_style = isset($_POST[Apollo_DB_Schema::_APL_BUSINESS_SPOTLIGHT_LINK_OPEN_STYLE])
                        ? $_POST[Apollo_DB_Schema::_APL_BUSINESS_SPOTLIGHT_LINK_OPEN_STYLE]
                        : false;
        $val_open_style = !!$val_open_style;
        update_post_meta(
            $post_id,
            Apollo_DB_Schema::_APL_BUSINESS_SPOTLIGHT_LINK_OPEN_STYLE,
            Apollo_App::clean_data_request( $val_open_style )
        );
    }
}
