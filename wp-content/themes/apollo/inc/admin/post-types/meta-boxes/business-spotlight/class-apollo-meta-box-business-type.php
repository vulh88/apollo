<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo Business Spotlight custom post type meta data
 *
 * @category    Admin
 * @package     inc/admin/post-types/meta-boxes
 * @ticket      #11186
 */
class Apollo_Meta_Box_Business_Spotlight_Type
{

    private static $_key = Apollo_DB_Schema::_APL_BUSINESS_SPOT_TYPES_PRIMARY_ID;
    /**
     * Output the Link meta box
     *
     * @param $post
     */
    public static function output( $post )
    {
        wp_nonce_field( 'apollo_business_spotlight_meta_nonce', 'apollo_business_spotlight_meta_nonce' );

        global $post, $thepostid;
        $thepostid      = $post->ID;
        $data           = get_post_meta( $thepostid,self::$_key );
        $value          = $data && $data[0] ? $data[0] : '';
        apollo_dropdown_categories('business-type', array('hide_empty' => 0, 'auto_complete' => true, 'value' => 'id'), '', '', '', '', $value);
    }

    /**
     * Save meta box data
     *
     * @param $post_id
     */
    public static function save( $post_id )
    {
        update_post_meta($post_id, self::$_key, Apollo_App::clean_data_request( $_POST['business-type'] ));
    }
}
