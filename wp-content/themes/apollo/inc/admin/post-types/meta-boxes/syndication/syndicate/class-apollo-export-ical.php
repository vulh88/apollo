<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Apollo_Syndicate_Export_ICAL extends Apollo_Syndicate_Export_Abstract
{
	public function __construct()
	{
		$this->cacheFile = 'syndication-ical';
		$this->output = 'txt';
		parent::__construct();
	}

	public function handleRequest($echo = true)
	{
		try{
			global $wpdb;

			// Get from cache
			$syndicationMeta = get_post_meta($this->apID);
			if ($cacheContent = $this->getCache($syndicationMeta)) {
				echo $cacheContent;
				return;
			}


			$dataDisplayedEFields = self::getDisplayEventFieldsList($this->apID);
			$dataQueryArgs = $this->dataQueryArgs;
			if(is_array($dataDisplayedEFields) && is_array($dataQueryArgs)){
				$data = array_merge($dataQueryArgs, $dataDisplayedEFields);
			} else {
				$data = $dataDisplayedEFields;
			}

			if(empty($syndicationMeta)){
				_e('Account ID or Output Type is invalid. Process generating is failed.');
				exit;
			}

			if(isset($syndicationMeta['meta-mode']) && $syndicationMeta['meta-mode'][0] === 'YES'){
				$getEventMode = 'individual';
			} else {
				$getEventMode = 'event-type';
			}

			if($getEventMode === 'individual'){
				$events = $this->getEventsBySelectedIndividualEventIDs($syndicationMeta);
			} else {
				$events = $this->getEventsBySelectedEventType($syndicationMeta);
			}

            /**
             * ThienLD : refactor code to be clearer and handle ticket http://redmine.elidev.info/issues/12655
             */
            $blacklist = self::getMetaDataByKey('meta-blacklist', $syndicationMeta);
            $blacklist = self::getUnSerializeValueForQuery($blacklist);
            $emStartDate = self::getMetaDataByKey('meta-dateStart', $syndicationMeta);
            $emEndDate = self::getMetaDataByKey('meta-dateEnd', $syndicationMeta);
            $emDateRange = self::getMetaDataByKey('meta-range', $syndicationMeta);
            $emFilterDate = self::getEventStartDateEndDateForQuery($emDateRange, $emStartDate, $emEndDate);
            $types = self::getMetaDataByKey('meta-type', $syndicationMeta);
            $types = self::getUnSerializeValueForQuery($types);
			$timezone = Apollo_Ical_calendar::getTimeZone();


			/** @Ticket #14733 */
			$blog_name = get_bloginfo('name');
			$blog_url = get_bloginfo('url');
			$content = "BEGIN:VCALENDAR\r
VERSION:2.0\r
PRODID:-//$blog_name//NONSGML v1.0//EN\r
X-WR-CALNAME:{$blog_name}\r
X-WR-TIMEZONE:$timezone\r
X-ORIGINAL-URL:{$blog_url}\r
X-WR-CALDESC:Blog posts from {$blog_name}\r
CALSCALE:GREGORIAN\r
METHOD:PUBLISH\r
";
			$eventdet = '';
			foreach ($events as $eventID) {

				if (!empty($eventID->ID)) {
					$eventID = $eventID->ID;
				}

                /**
                 * ThienLD : handle issue in ticket #12654 and refactor code to be easier maintain in the future
                 */

                if ($this->isExpiredTime($eventID)) continue;

				$row = self::get_all_details($eventID);
				if (empty($row)) {
					continue;
				}

                $eventType = self::getValByKey($row,'_apl_event_term_primary_id');

                if ($eventType == '') continue;

                if ( self::isExistedInEventBlacklistType($row, $blacklist) ) continue;

                if ( ! self::isExistedInEventType($row, $types, $syndicationMeta)) continue;

                $type_name = get_term($eventType)->name;

                /**
                 * ThienLD : [END] - handle issue in ticket #12654 and refactor code to be easier maintain in the future
                 */


				$summary = $row['post']->post_title;
				$summary = $this-> ical_split('SUMMARY:', $summary);
				$desc = strip_tags($row['post']->post_content);
				$replace = '&lt;p&gt;';
				$desc = preg_replace('/$replace/',' ',$desc);
				$desc = str_replace(",","\,",$desc);
				$desc = $this-> ical_split('DESCRIPTION:', $desc);

				$permalink = get_permalink($eventID);
				$permalink = $this-> ical_split('URL;VALUE=URI:', $permalink);

				$startd = $row['_apollo_event_start_date'];
				$stimestr = "SELECT time_from,time_to from $wpdb->apollo_event_calendar WHERE date_event = '".$startd."' AND event_id = $eventID";
				$stimerslt = $wpdb->get_row($stimestr, OBJECT);

				//print_r($stimerslt);

				$endd = $row['_apollo_event_end_date'];
				$etimestr = "SELECT time_from,time_to from $wpdb->apollo_event_calendar WHERE date_event = '".$endd."' AND event_id = $eventID";
				$etimerslt = $wpdb->get_row($etimestr, OBJECT);

				//print_r($etimerslt->time_from);
				$stime = !empty($stimerslt) ? str_replace(":","",$stimerslt->time_from)."00" : "000000";
				$start_date = date_create($row['_apollo_event_start_date']);
				$start_date = date_format($start_date, 'Ymd')."T$stime";

				$dtstamp = date('Ymd')."T000000";

				$etime =  !empty($etimerslt) ? str_replace(":","",$etimerslt->time_from)."00" : "000000";
				$end_date = date_create($row['_apollo_event_end_date']);
				$end_date = date_format($end_date, 'Ymd')."T$etime";

				if (empty($row['_org_name'])){
					$tmp_org = $wpdb->get_results("SELECT meta_value from $wpdb->apollo_eventmeta where apollo_event_id = $eventID and meta_key = '_apl_event_tmp_org'");
					$tmp_org = $tmp_org ? $tmp_org[0]->meta_value : '';
					$row['_org_name'] = $tmp_org;
				}

				$org = $row['_org_name'];

				if (empty($row['_venue_name'])){
					$tmp_venue = $wpdb->get_results("SELECT meta_value from $wpdb->apollo_eventmeta where apollo_event_id = $eventID and meta_key = '_apl_event_tmp_venue'");
					$tmp_venue = $tmp_venue ? $tmp_venue[0] : array();
					$tmp_venue = $tmp_venue ? maybe_unserialize(maybe_unserialize($tmp_venue->meta_value)) : array();
					$row['_venue_name'] = !empty($tmp_venue) ? $tmp_venue['_venue_name'] : '';
				} else {
					$row['_venue_name'] = '';
				}

				$location = $row['_venue_address1']."\, ".$row['_venue_city']."\, ".$row['_venue_state']."\, ".$row['_venue_zip'];

				$venue = $row['_venue_name'];
				/** @Ticket #14733 */
				$desc = trim($desc);
//print_r($row['image']);exit;
				$offset = strpos($row['image'],"img data-original=")+18;
				$end = strpos($row['image'],'"',$offset+1)-1;
				$img_str = substr($row['image'],20,$end - $offset);
				$img_str = str_replace("-200x200","",$img_str);
				$img_str = str_replace("-150x150","",$img_str);
				$img_str = $this-> ical_split('ATTACH;FMTTYPE=image/jpeg:', $img_str);


				$contact = str_replace(' ', '_', $row['_org_name']);
				$content .= "BEGIN:VEVENT\r
UID:$eventID\r
DTSTAMP:$dtstamp\r
DTSTART:$start_date\r
DTEND:$end_date\r
SUMMARY:$summary\r
DESCRIPTION:$desc\r
CATEGORY:$type_name\r
ORGANIZER:$contact\r
LOCATION:$location\r
URL:$permalink\r
ATTACH;FMTTYPE=image/jpeg:$img_str
END:VEVENT\r
";

			}

			$blog_name = get_bloginfo('name');
			$blog_url = get_bloginfo('url');

			$content .= "END:VCALENDAR\r";

			if (!$echo) {
				return array(
					'string'	=> $content
				);
			}

			echo $content;
			// Store cache
			$this->addCache($content);

		}catch (Exception $ex){

			if (!$echo) {
				return array(
					'string'	=> ''
				);
			}

			aplDebugFile($ex->getMessage(),'Syndicate export with type as ICAL or ICAL File');
			wp_safe_redirect('/404');
		}
	}

function ical_split($preamble, $value) {
  $value = trim($value);
  $value = strip_tags($value);
  $value = preg_replace('/\n+/', ' ', $value);
  $value = preg_replace('/\s{2,}/', ' ', $value);
  $preamble_len = strlen($preamble);
  $lines = array();
  while (strlen($value)>(75-$preamble_len)) {
    $space = (75-$preamble_len);
    $mbcc = $space;
    while ($mbcc) {
      $line = mb_substr($value, 0, $mbcc);
      $oct = strlen($line);
      if ($oct > $space) {
        $mbcc -= $oct-$space;
      }
      else {
        $lines[] = $line;
        $preamble_len = 1; // Still take the tab into account
        $value = mb_substr($value, $mbcc);
        break;
      }
    }
  }
  if (!empty($value)) {
    $lines[] = $value;
  }
  return join($lines, "\n");
}

	/**
	 * Abstract whether set store transient cache or not
	 * @author vulh
	 * @return boolean
	 */
	protected function setEnableStoreTransientCache()
	{
		return true;
	}
}