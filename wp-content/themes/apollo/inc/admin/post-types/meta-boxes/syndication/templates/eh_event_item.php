<?php
$event_id = $template_args['event_id'];
$display_fields = $template_args['display_fields'];
$e = get_event($event_id);
$objectClass = $template_args['objectClass'];
$event_end_date = $e->get_meta_data(Apollo_DB_Schema::_APOLLO_EVENT_END_DATE);
$summary = $e->get_full_excerpt();
$admission_detail  = $e->getMetaInMainData( Apollo_DB_Schema::_ADMISSION_DETAIL );

if (!$objectClass->isExpiredTime($event_id)) :
    $event_name = !empty($e) ? $e->post->post_title : '';
    $event_link = get_permalink($event_id);

    $event_start_date = $e->get_meta_data(Apollo_DB_Schema::_APOLLO_EVENT_START_DATE);

    $event_date =  $objectClass->renderDateTime($event_start_date, $event_end_date, $event_id);
    ?>

<div class="syndication-expanded-html">
    <a target="<?php echo apply_filters('linkest', '') ?>" href="<?php echo $event_link; ?>"><b><?php echo $event_name; ?></b></a><br />
    <?php echo $summary ?>
    <?php echo $e->renderOrgVenueHtml(true, false, true) ?><br/>
    <?php echo Apollo_App::convertContentEditorToHtml($admission_detail); ?>
    <?php echo sprintf("%s",$event_date)?> <br /><br />
</div>
<?php endif;
