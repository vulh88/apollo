<tr {class}>
    <td class="thumbnail"><?php if($enableEventImage != 'NO') : ?><div class="syndication-thumbnail">{event_image}</div> <?php endif; ?></td>
    <td class="event_name">
        <div class="eli-event-tooltip-item-wrapper" data-tooltip-content="#tooltip-content_{event_id}">
            <a <?php echo apply_filters('linkest', '') ?>  href="{1url}">{1name}</a>
            <?php include (APOLLO_ADMIN_SYNDICATE_IFRAME_DIR . '/views/partial/_tooltip.php') ?>
        </div>
    </td>
	<td class="organization">{2name}</td>
	<td class="dates">{3name}</td>
	<td class="city">{4name}</td>
</tr>
