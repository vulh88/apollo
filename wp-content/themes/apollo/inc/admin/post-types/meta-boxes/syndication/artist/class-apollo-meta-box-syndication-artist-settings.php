<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Apollo_Meta_Box_Syndication_Artist_Xml_Generation
 */
class Apollo_Meta_Box_Syndication_Artist_Settings {

	/**
	 * Output the metabox
	 */
	public static function output( $post ) {
		global $wpdb;
		wp_nonce_field( 'apollo_syndication-artist_settings_meta_nonce', 'apollo_syndication-artist_settings_meta_nonce' );

        echo '<div class="options_group clear ct-selection-box">';
        apollo_wp_text_input(array(
            'id' => APL_Syndication_Const::_SA_META_ARTIST_LIMIT,
            'label' => __('Limit', 'apollo'),
            'value' => Apollo_Admin_Syndication_Artist_Meta_Boxes::getMetaDataByKey($post->ID, APL_Syndication_Const::_SA_META_ARTIST_LIMIT, 1000),
            'class' => 'apollo_input_number'

        ));
        echo '</div>';
        echo '<div class="options_group clear wrap-blk ct-full-width">';
        echo '<label class="ct-lb-mtc">'. __('Enable file caching','apollo') .'</label>';
        echo '<div class="options_group clear ct-selection-box">';
        $val = Apollo_Admin_Syndication_Artist_Meta_Boxes::getMetaDataByKey($post->ID, APL_Syndication_Const::_SA_META_ARTIST_ENABLE_CACHE, 0);
        apollo_wp_radio(  array(
            'id' => APL_Syndication_Const::_SA_META_ARTIST_ENABLE_CACHE,
            'label' => __( '', 'apollo' ),
            'value' => $val,
            'class' => 'syndication-enable-caching',
            'options' => array(
                1 => __("Yes",'apollo'),
                0 => __("No",'apollo')
            )
        ) );
        echo '</div>';
        echo '<button '.($val != "1" ? "disabled" : "").' id="syn-clear-cache" data-type="artist" data-post-id="'.$post->ID.'" type="button" data-loading="'.__("Processing", "apollo").'" data-text="'.__("Clear Cache", "apollo").'">'.__("Clear Cache", "apollo").'</button>';
        echo '</div>';

        echo '<div class="options_group clear ct-selection-box">';
        apollo_wp_radio(array(
            'id' => APL_Syndication_Const::_SA_META_ARTIST_ORDER_BY,
            'label' => __('Order by', 'apollo'),
            'value' => Apollo_Admin_Syndication_Artist_Meta_Boxes::getMetaDataByKey($post->ID, APL_Syndication_Const::_SA_META_ARTIST_ORDER_BY, 'post_title'),
            'options' => array(
                'post_title' => __('Artist name', 'apollo'),
                'post_date' => __('Posted date', 'apollo'),
            ),
        ));
        echo '</div>';
        echo '<div class="options_group clear ct-selection-box">';
        apollo_wp_radio(array(
            'id' => APL_Syndication_Const::_SA_META_ARTIST_ORDER,
            'label' => __('Sort order', 'apollo'),
            'value' => Apollo_Admin_Syndication_Artist_Meta_Boxes::getMetaDataByKey($post->ID, APL_Syndication_Const::_SA_META_ARTIST_ORDER, 'asc'),
            'options' => array(
                'asc' => __('ASC', 'apollo'),
                'desc' => __('DESC', 'apollo'),
            ),
        ));
        echo '</div>';
        echo '<div class="options_group clear ct-selection-box">';
        echo '</div>';
	}

	/**
	 * Save meta box data
	 */
	public static function save( $post_id, $post ) {
		global $post, $wpdb;

		// Checks save status
		$is_autosave = wp_is_post_autosave( $post_id );
		$is_revision = wp_is_post_revision( $post_id );
		$is_valid_nonce = ( isset( $_POST[ 'apollo_syndication-artist_settings_meta_nonce' ] ) && wp_verify_nonce( $_POST[ 'apollo_syndication-artist_settings_meta_nonce' ], 'apollo_syndication-artist_settings_meta_nonce' ) ) ? true : false;

		// Exits script depending on save status
		if ( $is_autosave || $is_revision || ! $is_valid_nonce ) {
			return;
		}

        if( isset( $_POST[ APL_Syndication_Const::_SA_META_ARTIST_LIMIT ] ) ) {
            update_post_meta( $post_id, APL_Syndication_Const::_SA_META_ARTIST_LIMIT, $_POST[ APL_Syndication_Const::_SA_META_ARTIST_LIMIT ] );
        }

        if( isset( $_POST[ APL_Syndication_Const::_SA_META_ARTIST_ENABLE_CACHE ] ) ) {
            update_post_meta( $post_id, APL_Syndication_Const::_SA_META_ARTIST_ENABLE_CACHE, $_POST[ APL_Syndication_Const::_SA_META_ARTIST_ENABLE_CACHE ] );
        }

        if( isset( $_POST[ APL_Syndication_Const::_SA_META_ARTIST_ORDER_BY ] ) ) {
            update_post_meta( $post_id, APL_Syndication_Const::_SA_META_ARTIST_ORDER_BY, $_POST[ APL_Syndication_Const::_SA_META_ARTIST_ORDER_BY ] );
        }

        if( isset( $_POST[ APL_Syndication_Const::_SA_META_ARTIST_ORDER ] ) ) {
            update_post_meta( $post_id, APL_Syndication_Const::_SA_META_ARTIST_ORDER, $_POST[ APL_Syndication_Const::_SA_META_ARTIST_ORDER ] );
        }

	}
}
