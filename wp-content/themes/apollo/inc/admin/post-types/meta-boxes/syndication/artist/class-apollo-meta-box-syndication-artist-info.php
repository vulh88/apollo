<?php
/**
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Apollo_Meta_Box_Syndication_Artist_Info
 */
class Apollo_Meta_Box_Syndication_Artist_Info {

	/**
	 * Output the metabox
	 */
	public static function output( $post ) {
		wp_nonce_field( 'apollo_syndication-artist_info_meta_nonce', 'apollo_syndication-artist_info_meta_nonce' );
		$listID = array();
        require_once APOLLO_ADMIN_DIR . '/post-types/meta-boxes/syndication/artist/APL_Artist_Syndication_Table.php';

        $tableView = new APL_Artist_Syndication_Table();
        $tableView->setListArtistSelected($listID);
        if (isset($_GET['paged'])) {
            $tableView->setOffset(intval($_GET['paged']));
        }
        $selectAll = Apollo_Admin_Syndication_Artist_Meta_Boxes::getMetaDataByKey($post->ID, APL_Syndication_Const::_SA_META_ARTIST_ALL, '1');

        echo '<div class="options_group clear ct-selection-box select-all-artist">';
        apollo_wp_radio(array(
            'id' => APL_Syndication_Const::_SA_META_ARTIST_ALL,
            'label' => '',
            'value' => Apollo_Admin_Syndication_Artist_Meta_Boxes::getMetaDataByKey($post->ID, APL_Syndication_Const::_SA_META_ARTIST_ALL, '1'),
            'options' => array(
                '1' => __('All', 'apollo'),
                '0' => __('Select Artist', 'apollo'),
            )
        ));
        echo '</div>';

        $tableView->prepare_items();
        echo '<div class="wrap-select-artist '. ($selectAll ? 'disable-content' : '') .'">';
        echo '</div>';

        echo '<div class="options_group clear ct-selection-box">';
        apollo_wp_hidden_input(array(
            'id' => APL_Syndication_Const::_SA_META_ARTIST_SELECTED,
            'value' => Apollo_Admin_Syndication_Artist_Meta_Boxes::getMetaDataByKey($post->ID, APL_Syndication_Const::_SA_META_ARTIST_SELECTED, ''),
        ));
        echo '</div>';
	}

	/**
	 * Save meta box data, APL_Syndication_Const::_SA_META_ARTIST_SELECTED, true
	 */
	public static function save( $post_id, $post ) {
		global $post, $wpdb;

		// Checks save status
		$is_autosave = wp_is_post_autosave( $post_id );
		$is_revision = wp_is_post_revision( $post_id );
		$is_valid_nonce = ( isset( $_POST[ 'apollo_syndication-artist_info_meta_nonce' ] ) && wp_verify_nonce( $_POST[ 'apollo_syndication-artist_info_meta_nonce' ], 'apollo_syndication-artist_info_meta_nonce' ) ) ? true : false;

		// Exits script depending on save status
		if ( $is_autosave || $is_revision || ! $is_valid_nonce ) {
			return;
		}

		if (isset($_POST[APL_Syndication_Const::_SA_META_ARTIST_ALL])) {
            update_post_meta( $post_id, APL_Syndication_Const::_SA_META_ARTIST_ALL, $_POST[ APL_Syndication_Const::_SA_META_ARTIST_ALL ] );
        }

        if (isset($_POST[ APL_Syndication_Const::_SA_META_ARTIST_SELECTED ])) {
            update_post_meta( $post_id, APL_Syndication_Const::_SA_META_ARTIST_SELECTED, $_POST[ APL_Syndication_Const::_SA_META_ARTIST_SELECTED ] );
        }
	}
}
