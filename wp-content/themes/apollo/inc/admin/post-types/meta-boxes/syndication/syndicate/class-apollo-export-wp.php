<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Apollo_Syndicate_Export_WP extends Apollo_Syndicate_Export_Abstract {

	public function __construct(){
		$this->cacheFile = 'syndication-wp';
		$this->output = 'xml';
		parent::__construct();
	}

	public function handleRequest($echo = true){

		if(!empty($this->apType)){
			switch($this->apType){
				case 'event':
					return $this->process_wp_request_event($echo);
					break;
				case 'cat':
					return $this->process_wp_request_cat();
					break;
			}
		}
		return false;
	}

	private function process_wp_request_cat(){
		try {
			global $wpdb;

			$dataDisplayedEFields = self::getDisplayEventFieldsList($this->apID);
			$dataQueryArgs = $this->dataQueryArgs;
			if (is_array($dataDisplayedEFields) && is_array($dataQueryArgs)) {
				$data = array_merge($dataQueryArgs, $dataDisplayedEFields);
			} else {
				$data = $dataDisplayedEFields;
			}


			$post_id = $this->apID;
			$types = get_post_meta($post_id, 'meta-type');
			$types = $types[0];
			$_types = $types;

			$xml = "<cat_subcat>\n";

			$cats = get_terms('event-type', [
			    'orderby' => 'ID',
                'order' => 'ASC',
                'hide_empty' => 0
            ]);

			// Auto add parent category to list event type if it was not selected
			if ($types) {
				foreach ($types as $type) {
					$term = get_term($type, 'event-type');
					if ($term->parent && !in_array($term->parent, $types)) {
						$types[] = $term->parent;
					}
				}
			}

			$cat_id = array();
			if (!empty($cats) && is_array($cats)) {
				foreach ($cats as $cat) {
					$key = $cat->term_id;
					$val = $cat->name;
					$parent = $cat->parent;

					if (is_array($types) && !in_array($key, $types) && !in_array($parent, $types)) continue;

					if ($parent == 0) {
						$cat_id[$key] = $val;
					}

				}
			}


			ksort($cat_id);

			foreach ($cat_id as $key => $val) {

				$xml .= "<cat_id>" . $key . "_0_" . $val . "</cat_id>\n";

				$active = is_array($_types) && in_array($key, $_types) ? 1 : 0;

				// Active == 1 => This term is enable for the feed
				$xml .= "<active>" . $active . "</active>\n";

				$xml .= "<subcat_ids>\n";

				foreach ($cats as $dog) {
					if ($key == $dog->parent && is_array($types) && in_array($dog->term_id, $types)) {
						$xml .= "<subcat_id>" . $dog->parent . "_" . $dog->term_id . "_" . $dog->name . "</subcat_id>\n";
					}
				}

				$xml .= "</subcat_ids>\n";

			}

			$xml .= "</cat_subcat>\n";
			echo $xml;
		} catch (Exception $ex) {
			aplDebugFile($ex->getMessage(), 'Syndicate export with type as WORDPRESS - EVENT CATEGORIES');
			wp_safe_redirect('/404');
		}
	}

	public function process_wp_request_event($echo = true, $output = 'xml', $eID = false){

		try{
			global $wpdb;

			$syndicationMeta = get_post_meta($this->apID);

			if(empty($syndicationMeta)){
				_e('Account ID or Output Type is invalid. Process generating is failed.');
				exit;
			}

			// Get from cache, ignore cache once getting an event detail
			if (!$eID && $cacheContent = $this->getCache($syndicationMeta)) {
				echo $cacheContent;
				return;
			}

			$data = self::getDisplayEventFieldsList($this->apID);

			$post_id = $this->apID;

			$types =  get_post_meta( $post_id, 'meta-type');
			$types = !empty($types) ? $types[0] : array();

			if(isset($syndicationMeta['meta-mode']) && $syndicationMeta['meta-mode'][0] === 'YES'){
				$getEventMode = 'individual';
			} else {
				$getEventMode = 'event-type';
			}

			// Get by event ID
			if ($eID) {
                $syndicationMeta['eventID'] = $eID;
            }

			if($getEventMode === 'individual'){
				$events = $this->getEventsBySelectedIndividualEventIDs($syndicationMeta);
			} else {
				$events = $this->getEventsBySelectedEventType($syndicationMeta);
			}

			$cats = get_terms('event-type', [
                'orderby' => 'name',
                'order' => 'ASC',
                'hide_empty' => 0
            ]);

			$category_type = array();
			$lookup_type = array();
			if(!empty($cats) && is_array($cats)){
				foreach ($cats as $cat){

					if ( is_array($types) && in_array($cat->term_id, $types)){
						$key = $cat->term_id;
						$val = $cat->name;
						$category_type[$key] = $val;

						if ($cat->parent == 0){
							$val = $cat->term_id."_".$cat->parent."|".$cat->name;
						} else {
							$val = $cat->parent."_".$cat->term_id."|".$cat->name;
						}

						$lookup_type[$key] = $val;
					}
				}
			}

			// Export cat ids
			$catIds = array_keys($category_type);

            /**
             * ThienLD : refactor code to be clearer and handle ticket http://redmine.elidev.info/issues/12655
             */
            $blacklist = self::getMetaDataByKey('meta-blacklist', $syndicationMeta);
            $blacklist = self::getUnSerializeValueForQuery($blacklist);
            $emStartDate = self::getMetaDataByKey('meta-dateStart', $syndicationMeta);
            $emEndDate = self::getMetaDataByKey('meta-dateEnd', $syndicationMeta);
            $emDateRange = self::getMetaDataByKey('meta-range', $syndicationMeta);
            $emFilterDate = self::getEventStartDateEndDateForQuery($emDateRange, $emStartDate, $emEndDate);
            $types = self::getMetaDataByKey('meta-type', $syndicationMeta);
            $types = self::getUnSerializeValueForQuery($types);

			/** @Ticket #14105 - Get Alternate info */
			$alternateFields = Apollo_Custom_Field::getAlternateFields();

            $xml = "";
			if (!$eID) {
                $xml = "<events>\n";
            }

			$results = array();

			foreach ($events as $eventID){

				if (!empty($eventID->ID)) {
					$eventID = $eventID->ID;
				}

                /**
                 * ThienLD : handle issue in ticket #12654 and refactor code to be easier maintain in the future
                 */
                if ($this->isExpiredTime($eventID)) continue;

				$row = self::get_all_details($eventID, array(
					'img_size'   => 'large',
					'org_img'    => true,
				));

				if (empty($row)) {
					continue;
				}

                $eventType = self::getValByKey($row,'_apl_event_term_primary_id');

                if ($eventType == '') continue;

                if ( self::isExistedInEventBlacklistType($row, $blacklist) ) continue;

                if ( ! self::isExistedInEventType($row, $types, $syndicationMeta)) continue;

                /**
                 * ThienLD : [END] - handle issue in ticket #12654 and refactor code to be easier maintain in the future
                 */

				$eventUrl = get_permalink( $eventID );
				$websiteUrl = !empty($row[Apollo_DB_Schema::_WEBSITE_URL]) ? $row[Apollo_DB_Schema::_WEBSITE_URL] : $eventUrl;
				$startDate = date("m-d-Y", strtotime($row['_apollo_event_start_date']));
				$endDate = date("m-d-Y", strtotime($row['_apollo_event_end_date']));
				$ongoing = isset($row['ongoing']) ? $row['ongoing'] : 'N';
				$eventDaysList = $row['_apollo_event_days_of_week'];
				$eventDaysAssoc = array(0 => 'Sunday',1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday');
				$eventDays = '';
				if($eventDaysList){
					foreach ($eventDaysList as $day) {
						$eventDays .= $eventDaysAssoc[$day]. ",";
					}
				}


				$eventDays = substr($eventDays,0, -1);

				// Keep existing ORG Images for the next loop
				$orgImage = isset($row['org_img']) ? $row['org_img'] : '';

				// Set event image is org image if this value is empty
				if (! $eventimage = $row['image'] ) {
					$eventimage = $orgImage;
				}


				$_results = array(
					'eventID'	=> $eventID,
					'eventImage'	=> $eventimage,
					'eventName'	=> $row['post']->post_title,
					'eventUrl'	=> $eventUrl,
					'eventDateBegin'	=> $startDate,
					'eventDateEnd'	=> $endDate,
					'eventDays'	=> $eventDays,
					'ongoing'	=> $ongoing,
					'websiteUrl'	=> $websiteUrl,
				);

				$store_type = array();
				$store_cat = array();
				if (in_array($eventType, $catIds)) {
					$store_type[] = isset($category_type[$eventType]) ? html_entity_decode($category_type[$eventType]) : '';
					$store_cat[] = $eventType."_0";
				}

				$source = $_SERVER['SERVER_NAME'];
				$_results['source']	= $source;

                if ($data['eventSummary']) {
                    $_results['eventSummary'] = $row['post']->post_excerpt;
                }

				if ($data['eventDescription']) {
					$_results['eventDescription'] = $row['post']->post_content;
				}

				if ($data['eventDatesTimes']) {
					$dates_times = $this->get_dates_times($eventID,self::getValByKey($emFilterDate,'end-date',''), false, false, $this->output);
					$_results['eventDatesTimes'] = $dates_times;
				}


				if ($data['orgName']) {

					if (empty($row['_org_name'])){
						$this->getOrgTmpByEventId($row, $eventID);
					}
					$_results['orgName'] = $row['_org_name'];
				}

                /** @Ticket #13899 */
                $_results['orgID'] = isset($row['_org_id']) ? $row['_org_id'] : '';

				if ($data['orgImage']) {
					$_results['orgImage'] = $orgImage;
				}

				if ($data['venue']){
					if (empty($row['_venue_name'])){
						$this->getVenueTmpByEventId($row, $eventID);
					}

                    /** @Ticket #13926 */
                    $venue_neighborhood_name = '';
                    if (!empty($row[Apollo_DB_Schema::_VENUE_NEIGHBORHOOD])) {
                        $venue_neighborhood_name = APL_Lib_Territory_Neighborhood::getNameById($row[Apollo_DB_Schema::_VENUE_NEIGHBORHOOD]);
                    }

					$_results['venueID'] = isset($row['_venue_id']) ? $row['_venue_id'] : '';
					$_results['venueName'] = $row['_venue_name'];
					$_results['venueAddress1'] = $row['_venue_address1'];
					$_results['venueCity'] = $row['_venue_city'];
					$_results['venueState'] = $row['_venue_state'];
					$_results['venueZip'] = $row['_venue_zip'];
					$_results['venueLatitude'] = self::getValByKey($row, Apollo_DB_Schema::_VENUE_LATITUDE);
					$_results['venueLongitude'] = self::getValByKey($row, Apollo_DB_Schema::_VENUE_LONGITUDE);

                    $_results['venueNeighborhood'] = $venue_neighborhood_name;
					/** @Ticket #13899 */
					$_results['venueParking'] = self::getValByKey($row, Apollo_DB_Schema::_VENUE_PARKING_INFO);
				}

				$feat_spot = $wpdb->get_results("SELECT _is_category_spotlight, _is_category_feature from $wpdb->apollo_post_term where post_id = $eventID");

				$secondary_types = $row['secondary_types'];

				foreach ($secondary_types as $secondary_type){

					if ( is_array($types) && !in_array($secondary_type, $types)) continue;
					if ($secondary_type == $eventType) continue;
					if ( is_array($blacklist) && in_array($secondary_type, $blacklist)) continue;

					if (!empty($category_type[$secondary_type])) {
						$store_type[] = $category_type[$secondary_type];
					}

					if( isset($lookup_type[$secondary_type]) ) {
						list($cat,$sub) = explode("|", $lookup_type[$secondary_type]);
						$store_cat[] = $cat;
					}
				}
				$store_type = implode(', ', $store_type);
				$store_cat = implode(', ', $store_cat);


				if ($feat_spot && $feat_spot[0]->_is_category_spotlight == 'yes') {$spotlight = 'true';} else {$spotlight = 'false';}
				if ($feat_spot && $feat_spot[0]->_is_category_feature == 'yes') {$featured = 'true';} else {$featured = 'false';}

				if (isset($data['spotlight']) && $data['spotlight'])	$_results['spotlight'] = $spotlight;
				if (isset($data['featured']) && $data['featured'])		$_results['featured'] = $featured;

				$_results['categories'] = $store_cat;
				$_results['tags'] = $store_type;

				if (isset($data['eventTicketInfo']) && $data['eventTicketInfo']) {
					$_results['eventTicketInfo'] = self::getValByKey($row,'_admission_detail');
				}

				if (isset($data['eventPhone1']) && $data['eventPhone1']) {
					$_results['eventPhone1'] = self::getValByKey($row,'_admission_phone');
				}

				if (isset($data['ticketPhone']) && $data['ticketPhone']) {
					$_results['ticketPhone'] = self::getValByKey($row,'_admission_phone');
				}

				if (isset($data['eventEmail']) && $data['eventEmail']) {
					$_results['eventEmail'] = self::getValByKey($row,'_admission_ticket_email');
				}
				if (isset($data['eventTicketUrl']) && $data['eventTicketUrl']) {
					$_results['eventTicketUrl'] = self::getValByKey($row,'_admission_ticket_url');
				}
				if (isset($data['discountUrl']) && $data['discountUrl']) {
					$_results['discountUrl'] = self::getValByKey($row,'_admission_discount_url');
				}
				if (isset($data['eventStartTime']) && $data['eventStartTime']) {
                    $dateTimeInfo = self::getValByKey($row, Apollo_DB_Schema::_APL_EVENT_ADDITIONAL_TIME);
					$_results['eventStartTime'] = $dateTimeInfo;
				}
				if (isset($data['datePosted']) && $data['datePosted']) {
					$_results['datePosted'] = $row['post']->post_date;
				}
				if (isset($data['eventLink']) && $data['eventLink']) {
					$_results['link'] = $eventUrl;
				}
				if (isset($data['eventGuid']) && $data['eventGuid']) {
					$_results['guid'] = $eventUrl;
				}
				/** @Ticket #14105 - Alternate fields */
				if ($alternateFields) {
					$alternateInfo = $this->renderAlternateFields($eventID, $alternateFields, false);
					if (!empty($alternateInfo)) {
						$_results = array_merge($_results, $alternateInfo);
					}
				}

				// Output
				if ($output == 'xml' && $_results) {
					$xml .=  "<event>\n";

					foreach($_results as $k => $v) {
						if ($echo && $k == 'eventDatesTimes')  {
							$xml .=  sprintf("<%s>%s</%s>\n", $k, $v, $k);
						} else {
							$xml .=  sprintf("<%s><![CDATA[%s]]></%s>\n", $k, $v, $k);
						}

					}

					$xml .=  "</event>\n";

				} else {
					$results[] = $_results;
				}
			}

            if (!$eID) {
                $xml .= "</events>\n";
            }

			if (!$echo) {
                if ($eID) {
                    return !empty($results) ? $results[0] : array();
                }

				return array(
					'data'  => $results,
					'hasMore'  => $this->haveMore(),
					'count'	=> $this->totalEventsFeed,
					'string'	=> $xml
				);
			}

			echo $xml;

			// Store cache
            if (!$eID) {
                $this->addCache($xml);
            }

		} catch (Exception $ex){

			if (!$echo) {

				return array(
					'string'	=> ''
				);
			}


			aplDebugFile($ex->getMessage(),'Syndicate export with type as WORDPRESS - EVENT');
			wp_safe_redirect('/404');
		}
	}


	protected function get_dates_times($postID, $syndicationEndDate = '', $offset = false, $limit = false, $output='xml'){
		$event = $this->getEventCalendar([
		    'postID' => $postID,
            'offset' => $offset,
            'limit' => $limit,
            'endDate' => $syndicationEndDate
        ]);
		$retevent = '';
		$results = array();
		if (count($event) > 0 ){

			foreach ($event as $evt){
              $item = $this->outputExportResultItem($postID,$evt);
              $mydate = isset($item['date'])?$item['date']:'';
              $mytime = isset($item['time'])?$item['time']:'';
              $unix_time = isset($item['timestamp'])?$item['timestamp']:'';
              $date_filter = isset($item['date_filter'])?$item['date_filter']:'';
              $myend =  isset($item['end_time'])?$item['end_time']:'';
              $duration = isset($item['duration'])?$item['duration']:'';
				if ($output == 'xml') {
					$retevent .= "<datetime>\n<date>".$mydate."</date><time>".$mytime."</time>\n<timestamp>".$unix_time."</timestamp>\n<date_filter>".$date_filter."</date_filter>\n<end_time>".$myend."</end_time>\n<duration>".$duration."</duration>\n</datetime>\n";
				}
				else {
					$results[] = array(
						'date'	=> $mydate,
						'time'	=> $mytime,
						'timestamp'	=> $unix_time,
						'date_filter'	=> $date_filter,
						'end_time'	=> $myend,
						'duration'	=> $duration,
					);
				}


			} // end foreach

		} else {

			if ($output == 'xml') {
				$retevent = "<datetime>\n<date>\n</date>\n<time>\n</time>\n<timestamp>\n</timestamp>\n<date_filter>\n</date_filter>\n<duration>\n</duration>\n</datetime>\n";
			}
			else {
				$results = array(
					'date'	=> '',
					'time'	=> '',
					'timestamp'	=> '',
					'date_filter'	=> '',
					'duration'	=> '',
				);
			}
		}

		return $output == 'xml' ? $retevent : $results;

	}

	/**
	 * Abstract whether set store transient cache or not
	 * @author vulh
	 * @return boolean
	 */
	protected function setEnableStoreTransientCache()
	{
		return true;
	}
}