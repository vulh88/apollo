<?php
/**
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Apollo_Meta_Box_Syndication_Info
 */
class Apollo_Meta_Box_Syndication_Info {

	/**
	 * Output the metabox
	 */
	public static function output( $post ) {
		global $wpdb;
		wp_nonce_field( 'apollo_syndication_info_meta_nonce', 'apollo_syndication_info_meta_nonce' );
		$thepostid = $post->ID;
		$tokenKeyLength = 40;
		$tokenKey = Apollo_App::apl_getToken($tokenKeyLength);

		if (isset($_GET['post'])) {
			$_token = Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_TOKEN_KEY,$tokenKey);
		} else {
			$_token = $tokenKey.'_'. $thepostid;
		}


		echo '<div class="options_group event-box">';
		apollo_wp_text_input(  array(
			'id' => APL_Syndication_Const::META_TOKEN_KEY,
			'label' => __( 'API Token Key', 'apollo' ),
			'desc_tip' => 'true',
			'value' => $_token
		) );
		echo '<div style="text-align: center;">';
		echo '<span class="token spinner" style=" float: none; margin-top: 10px; visibility: visible; display: none;"></span>';
		echo '</div>';

		echo '<a data-app-id="'.$thepostid.'" data-confirm="'.__('Are you sure you want to refresh the token? All the API clients that are using this token will be broken.', 'apollo').'" class="button button-small"  data-token-key-length="'.$tokenKeyLength.'" id="refresh-token-key" onclick="" >'.__("Refresh Token Key ","apollo").'</a>';

		echo '</div>';
		echo '<hr/>';


		echo '<div class="options_group event-box">';
		// Account ID
		apollo_wp_info_field(  array(
			'label' => __( 'Account ID', 'apollo' ),
			'desc_tip' => 'true',
			'description' => "",
			'class' => '',
			'text' => $thepostid
		) );
		echo '</div>';

		echo '<div class="options_group event-box">';
		apollo_wp_editor(  array(
			'id' => APL_Syndication_Const::META_DESC,
			'label' => __( 'Account Description', 'apollo' ),
			'desc_tip' => 'true',
			'description' => "",
			'class' => 'syn-full-width',
			'value' => Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_DESC,'')
		) );
		echo '</div>';

		echo '<div class="options_group clear event-box">';
		apollo_wp_checkbox(  array(
			'id'    => APL_Syndication_Const::META_EXCLUDE_PRIVATE_EVENTS,
			'label' => __( 'Exclude Private Events', 'apollo' ),
			'value' => Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid, APL_Syndication_Const::META_EXCLUDE_PRIVATE_EVENTS, '')
		) );
		echo '</div>';

		echo '<div class="options_group clear ct-selection-box">';
		apollo_wp_radio(  array(
			'id' => APL_Syndication_Const::META_STATUS,
			'label' => __( 'Status', 'apollo' ),
			'value' => Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_STATUS,'OFF'),
			'options' => array('ON' => 'ON',
				'OFF' => 'OFF'
			)
		) );
		echo '</div>';

		echo '<div class="options_group clear ct-selection-box">';
		apollo_wp_radio(  array(
			'id' => APL_Syndication_Const::META_INCLUDE_PRI_CATE,
			'label' => __( 'Include Primary Category Type Segmentation', 'apollo' ),
			'value' => Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_INCLUDE_PRI_CATE,'OFF'),
			'options' => array('ON' => 'ON',
				'OFF' => 'OFF'
			)
		) );
		echo '</div>';

		echo '<div class="options_group clear ct-selection-box">';
		apollo_wp_radio(  array(
			'id' => APL_Syndication_Const::META_MODE,
			'label' => __( 'Individual Selection Mode', 'apollo' ),
			'value' => Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_MODE,'NO'),
			'options' => array('YES' => 'YES',
				'NO' => 'NO'
			)
		) );
		echo '</div>';

		echo '<div class="options_group clear ct-selection-box">';
		apollo_wp_radio(  array(
			'id' => APL_Syndication_Const::META_SFACTIVE,
			'label' => __( 'Enable Spotlight and Featured events for iframe', 'apollo' ),
			'value' => Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_SFACTIVE,'NO'),
			'options' => array('YES' => 'YES',
				'NO' => 'NO'
			)
		) );
		echo '</div>';

		echo '<div class="options_group clear event-box">';
		// Account ID
		apollo_wp_info_field(  array(
			'desc_tip' => 'true',
			'description' => "",
			'class' => 'noticed_info',
			'text' => __('After selecting the options please click the publish/update button so we can provide the correct form below.','apollo')
		) );
		echo '</div>';

	}

	/**
	 * Save meta box data
	 */
	public static function save( $post_id, $post ) {
		global $post, $wpdb;

		// Checks save status
		$is_autosave = wp_is_post_autosave( $post_id );
		$is_revision = wp_is_post_revision( $post_id );
		$is_valid_nonce = ( isset( $_POST[ 'apollo_syndication_info_meta_nonce' ] ) && wp_verify_nonce( $_POST[ 'apollo_syndication_info_meta_nonce' ], 'apollo_syndication_info_meta_nonce' ) ) ? true : false;

		// Exits script depending on save status
		if ( $is_autosave || $is_revision || ! $is_valid_nonce ) {
			return;
		}

		// Checks for input and sanitizes/saves if needed
		// TODO : replace string-key = APL_Syndication_Const
		if( isset( $_POST[ 'meta-token-key' ] ) ) {
			update_post_meta( $post_id, 'meta-token-key', sanitize_text_field( $_POST[ 'meta-token-key' ] ));
		}
		if( isset( $_POST[ 'meta-desc' ] ) ) {
			update_post_meta( $post_id, 'meta-desc', $_POST[ 'meta-desc' ] );
		}

		if( isset( $_POST[ 'meta-status' ] ) ) {
			update_post_meta( $post_id, 'meta-status', $_POST[ 'meta-status' ] );
		} else {
			update_post_meta( $post_id, 'meta-status', 'OFF' );
		}

		/* Thienld : save meta-include-primary-category meta data */
		if( isset( $_POST[ 'meta-include-primary-category' ] ) ) {
			update_post_meta( $post_id, 'meta-include-primary-category', $_POST[ 'meta-include-primary-category' ] );
		} else {
			update_post_meta( $post_id, 'meta-include-primary-category', 'OFF' );
		}

		if( isset( $_POST[ 'meta-mode' ] ) ) {
			update_post_meta( $post_id, 'meta-mode', $_POST[ 'meta-mode' ] );
		}

		if( isset( $_POST[ 'meta-sfactive' ] ) ) {
			update_post_meta( $post_id, 'meta-sfactive', $_POST[ 'meta-sfactive' ] );
		}

		// ticket #10998 : exclude private events in Syndication feeds
		$isExcludePrivateEvents = isset( $_POST[ APL_Syndication_Const::META_EXCLUDE_PRIVATE_EVENTS ] ) ? $_POST[ APL_Syndication_Const::META_EXCLUDE_PRIVATE_EVENTS ] : '';
		update_post_meta( $post_id, APL_Syndication_Const::META_EXCLUDE_PRIVATE_EVENTS, $isExcludePrivateEvents );

	}
}
