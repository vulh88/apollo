<?php
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Apollo_Meta_Box_Syndication_Org_Xml_Generation
 */
class Apollo_Meta_Box_Syndication_Org_Xml_Generation
{

    /**
     * @param $post
     */
    public static function output($post)
    {
        wp_nonce_field('apollo_syndication-org-xml-generation_meta_nonce', 'apollo_syndication-org-xml-generation_meta_nonce');
        $postId = !empty($post->ID) ? $post->ID : '';

        $currentDomainName = get_site_url();
        $xmlEngUrl = $currentDomainName . "/?syndicated_data&apid={$postId}&aplang=eng&apoutput=wp&aptype=" . Apollo_DB_Schema::_ORGANIZATION_PT;
        ?>
        <div class="wrap">
            <div class="options_group clear ct-selection-box">
                <?php apollo_wp_text_input(array(
                    'id' => APL_Syndication_Const::_SO_META_ORG_LIMIT,
                    'label' => __('Limit', 'apollo'),
                    'value' => Apollo_Admin_Syndication_Organization_Meta_Boxes::getMetaDataByKey($postId, APL_Syndication_Const::_SO_META_ORG_LIMIT, 1000),
                    'class' => 'apollo_input_number'

                )); ?>
            </div>
            <div class="options_group clear ct-selection-box">
                <?php apollo_wp_radio(array(
                    'id' => APL_Syndication_Const::_SO_META_ORG_ORDER_BY,
                    'label' => __('Order by', 'apollo'),
                    'value' => Apollo_Admin_Syndication_Organization_Meta_Boxes::getMetaDataByKey($postId, APL_Syndication_Const::_SO_META_ORG_ORDER_BY, 'post_title'),
                    'options' => array(
                        'post_title' => __('Post title', 'apollo'),
                        'post_date' => __('Post date', 'apollo'),
                    ),
                )); ?>
            </div>
            <div class="options_group clear ct-selection-box">
                <?php apollo_wp_radio(array(
                    'id' => APL_Syndication_Const::_SO_META_ORG_ORDER,
                    'label' => __('Sort order', 'apollo'),
                    'value' => Apollo_Admin_Syndication_Organization_Meta_Boxes::getMetaDataByKey($postId, APL_Syndication_Const::_SO_META_ORG_ORDER, 'asc'),
                    'options' => array(
                        'asc' => __('ASC', 'apollo'),
                        'desc' => __('DESC', 'apollo'),
                    ),
                )); ?>
            </div>
            <div class="options_group clear">
                <?php apollo_wp_text_input(array(
                    'id' => '_syndication_org_xml_english',
                    'label' => __('English', 'apollo'),
                    'value' => $xmlEngUrl,
                    'custom_attributes' => array(
                        'readonly' => true
                    ),
                )); ?>
                <a class="button button-small" href="<?php echo $xmlEngUrl ?>"
                   target=_blank> <?php echo __("TEST URL", "apollo") ?> </a>
            </div>
        </div> <?php
    }

    /**
     * @param $post_id
     */
    public static function save($post_id)
    {
        // Checks save status
        $is_autosave = wp_is_post_autosave($post_id);
        $is_revision = wp_is_post_revision($post_id);
        $is_valid_nonce = (isset($_POST['apollo_syndication-org-xml-generation_meta_nonce']) && wp_verify_nonce($_POST['apollo_syndication-org-xml-generation_meta_nonce'], 'apollo_syndication-org-xml-generation_meta_nonce')) ? true : false;

        // Exits script depending on save status
        if ($is_autosave || $is_revision || !$is_valid_nonce) {
            return;
        }

        if (isset($_POST[APL_Syndication_Const::_SO_META_ORG_LIMIT])) {
            update_post_meta($post_id, APL_Syndication_Const::_SO_META_ORG_LIMIT, $_POST[APL_Syndication_Const::_SO_META_ORG_LIMIT]);
        }
        if (isset($_POST[APL_Syndication_Const::_SO_META_ORG_ORDER_BY])) {
            update_post_meta($post_id, APL_Syndication_Const::_SO_META_ORG_ORDER_BY, $_POST[APL_Syndication_Const::_SO_META_ORG_ORDER_BY]);
        }
        if (isset($_POST[APL_Syndication_Const::_SO_META_ORG_ORDER])) {
            update_post_meta($post_id, APL_Syndication_Const::_SO_META_ORG_ORDER, $_POST[APL_Syndication_Const::_SO_META_ORG_ORDER]);
        }
    }

}
