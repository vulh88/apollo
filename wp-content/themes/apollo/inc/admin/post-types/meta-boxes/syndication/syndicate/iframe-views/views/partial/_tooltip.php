<div id="tooltip-content_{event_id}" class="tooltip_description">
    <div class="eli-event-tooltip-img">{event_image}</div>
    <div class="eli-event-tooltip-text">
        <p class="tt-event-name">{event_name}</p>
        <p class="tt-organization">{org_venue}</p>
        <p class="tt-date">{dates}</p>
        <p class="tt-description">{event_content}</p>
    </div>
</div>