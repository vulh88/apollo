<?php
/**
 * User: thienld
 * Date: 17/12/2015
 * Time: 18:07
 * Description: template of rendering one event item simple html
 */

$event_id = $template_args['event_id'];
$display_fields = $template_args['display_fields'];
$e = get_event($event_id);
$objectClass = $template_args['objectClass'];
$event_end_date = $e->get_meta_data(Apollo_DB_Schema::_APOLLO_EVENT_END_DATE);

if (!$objectClass->isExpiredTime($event_id)) :
    $event_name = !empty($e) ? $e->post->post_title : '';
    $event_link = get_permalink($event_id);
    $registered_org = $e->get_register_org();
    $org = !empty($registered_org) ? get_org($registered_org->org_id) : '';
    $org_term_name = $e->get_meta_data( Apollo_DB_Schema::_APL_EVENT_TMP_ORG );
    $org_term_name = $org_term_name !== false ? $org_term_name : __("", "apollo");
    $org_name = !empty($org) ? $org->get_full_title() : $org_term_name;
    $venue_id = $e->get_meta_data(Apollo_DB_Schema::_APOLLO_EVENT_VENUE);
    $venue = $venue_id !== false ? get_venue($venue_id) : '';
    $venue_name = !empty($venue) ? $venue->get_full_title() : __("", "apollo");

    if(empty($venue)){
    $tmp_venue = unserialize( $e->get_meta_data( Apollo_DB_Schema::_APL_EVENT_TMP_VENUE ) );
    if(!empty($tmp_venue[_venue_name]))$venue_name = $tmp_venue[_venue_name];
    }

    $event_start_date = $e->get_meta_data(Apollo_DB_Schema::_APOLLO_EVENT_START_DATE);

    $event_date = Apollo_App::apl_date('M d, Y',strtotime($event_start_date));
    if($event_start_date != $event_end_date && !empty($event_end_date)){
        $event_date .= ' - ' . Apollo_App::apl_date('M d, Y ',strtotime($event_end_date));
    }
    ?>

    <a target="<?php echo apply_filters('linkest', '') ?>" href="<?php echo $event_link; ?>"><b><?php echo $event_name; ?></b></a><br />
    <?php echo sprintf(__("Presented by %s at %s"), $org_name, $venue_name); ?><br/>
    <?php echo sprintf("%s",$event_date)?> <br /><br />
<?php endif;
