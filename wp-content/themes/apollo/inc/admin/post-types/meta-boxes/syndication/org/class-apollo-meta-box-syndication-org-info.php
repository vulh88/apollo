<?php
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Apollo_Meta_Box_Syndication_Org_Info
 */
class Apollo_Meta_Box_Syndication_Org_Info
{

    /**
     * @param $post
     */
    public static function output($post)
    {
        require_once APOLLO_ADMIN_DIR . '/post-types/meta-boxes/syndication/org/APL_Org_Syndication_Table.php';

        wp_nonce_field('apollo_syndication-org_meta_nonce', 'apollo_syndication-org_meta_nonce');
        $postId = !empty($post->ID) ? $post->ID : '';
        $listID = Apollo_Admin_Syndication_Organization_Meta_Boxes::getMetaDataByKey($postId, APL_Syndication_Const::_SO_META_ORG_SELECTED, '');
        $selectAll = Apollo_Admin_Syndication_Organization_Meta_Boxes::getMetaDataByKey($postId, APL_Syndication_Const::_SO_META_ORG_ALL, 1);

        $tableView = new APL_Org_Syndication_Table();
        $tableView->setListOrgSelected($listID);
        if (isset($_GET['paged'])) {
            $tableView->setOffset(intval($_GET['paged']));
        }

        ?>
        <div class="wrap">
            <div>
                <div class="ct-selection-box select-all-org">
                    <?php
                    apollo_wp_radio(array(
                        'id' => APL_Syndication_Const::_SO_META_ORG_ALL,
                        'label' => '',
                        'value' => $selectAll,
                        'options' => array(
                            1 => __('All', 'apollo'),
                            0 => __('Select Organization', 'apollo'),
                        )
                    ));
                    ?>
                </div>
                <div class="wrap-select-org <?php echo $selectAll ? 'disable-content' : '' ?>"></div>
                <?php
                apollo_wp_hidden_input(array(
                    'id' => APL_Syndication_Const::_SO_META_ORG_SELECTED,
                    'value' => $listID,
                ));
                ?>

            </div>
        </div> <?php
    }

    /**
     * @param $post_id
     */
    public static function save($post_id)
    {
        // Checks save status
        $is_autosave = wp_is_post_autosave($post_id);
        $is_revision = wp_is_post_revision($post_id);
        $is_valid_nonce = (isset($_POST['apollo_syndication-org_meta_nonce']) && wp_verify_nonce($_POST['apollo_syndication-org_meta_nonce'], 'apollo_syndication-org_meta_nonce')) ? true : false;

        // Exits script depending on save status
        if ($is_autosave || $is_revision || !$is_valid_nonce) {
            return;
        }

        if (isset($_POST[APL_Syndication_Const::_SO_META_ORG_ALL])) {
            update_post_meta($post_id, APL_Syndication_Const::_SO_META_ORG_ALL, $_POST[APL_Syndication_Const::_SO_META_ORG_ALL]);
        }

        if (isset($_POST[APL_Syndication_Const::_SO_META_ORG_SELECTED])) {
            update_post_meta($post_id, APL_Syndication_Const::_SO_META_ORG_SELECTED, $_POST[APL_Syndication_Const::_SO_META_ORG_SELECTED]);
        }
    }
}
