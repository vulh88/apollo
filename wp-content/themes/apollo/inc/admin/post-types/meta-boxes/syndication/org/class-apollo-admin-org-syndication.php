<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


class Apollo_Admin_Org_Syndication {


    public function getOrgSyndicationData() {
        $data = array(
            Apollo_DB_Schema::_SYNDICATION_ALL_ORGANIZATION => get_option(Apollo_DB_Schema::_SYNDICATION_ALL_ORGANIZATION, 0),
            Apollo_DB_Schema::_SYNDICATION_SELECTED_ORGANIZATION => get_option(Apollo_DB_Schema::_SYNDICATION_SELECTED_ORGANIZATION, ''),
            Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_LIMIT => get_option(Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_LIMIT, 1000),
            Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_ORDERBY => get_option(Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_ORDERBY, 'post_title'),
            Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_ORDER => get_option(Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_ORDER, 'asc'),
        );
        return $data;
    }


    public function saveOrgSyndicationData() {
        if (isset($_POST[Apollo_DB_Schema::_SYNDICATION_ALL_ORGANIZATION])) {
            update_option(Apollo_DB_Schema::_SYNDICATION_ALL_ORGANIZATION, $_POST[Apollo_DB_Schema::_SYNDICATION_ALL_ORGANIZATION], 'no');
        }
        if (isset($_POST[Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_LIMIT])) {
            update_option(Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_LIMIT, $_POST[Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_LIMIT], 'no');
        }
        if (isset($_POST[Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_ORDERBY])) {
            update_option(Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_ORDERBY, $_POST[Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_ORDERBY], 'no');
        }
        if (isset($_POST[Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_ORDER])) {
            update_option(Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_ORDER, $_POST[Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_ORDER], 'no');
        }
        if ( isset($_POST['org_ids']) && isset($_POST['current_org_ids'])) {
            $list_id = $_POST['current_org_ids'];
            $selected_ids = isset($_POST['org_selected_ids']) ? $_POST['org_selected_ids'] : array();
            if (!empty($list_id)) {
                $list_id = explode(',', $list_id);
            }
            $remove_ids = array_diff($_POST['org_ids'], $selected_ids);
            if (!empty($list_id)) {
                $new_ids = array_diff($selected_ids, $list_id);
                $list_id = array_merge($list_id, $new_ids);
                if (!empty($remove_ids)) {
                    $list_id = array_diff( $list_id, $remove_ids );
                }
            } else {
                $list_id = $selected_ids;
            }
            update_option(Apollo_DB_Schema::_SYNDICATION_SELECTED_ORGANIZATION, implode(',', $list_id), 'no');
        }
    }

}
