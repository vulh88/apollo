<?php

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

/**
 * Class APL_Org_Syndication_Table
 *
 * @ticket #11252
 */
class APL_Org_Syndication_Table extends WP_List_Table
{

    private $perPage = 20;
    private $totalItems;
    private $offset = 0;
    private $listOrgSelected;

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    /**
     * @return array
     */
    public function getListOrgSelected()
    {
        return $this->listOrgSelected;
    }

    /**
     * @param array $listOrgSelected
     */
    public function setListOrgSelected($listOrgSelected)
    {
        $this->listOrgSelected = $listOrgSelected;
    }

    public function prepare_items()
    {
        $hidden       = $this->get_hidden_columns();
        $sortable     = $this->get_sortable_columns();
        $currentPage  = $this->get_pagenum();
        $perPage      = $this->perPage;
        $this->offset = ($currentPage - 1) * $this->perPage;
        $s = isset($_GET['s']) ? $_GET['s'] : '';
        $orderby = isset($_GET['orderby']) ? $_GET['orderby'] : '';
        $order = isset($_GET['order']) ? $_GET['order'] : '';
        $data         = $this->table_data($s, $orderby, $order);
        $columns      = $this->get_columns();

        $this->set_pagination_args( array(
            'total_items' => $this->totalItems,
            'per_page'    => $perPage
        ) );
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = $data;
    }

    /**
     * @return array
     */
    public function get_columns()
    {
        $columns = array(
            'cb' => '<input type="checkbox" />',
            'title' => __( 'Title', 'apollo' ),
            'post_date' => __( 'Post Date', 'apollo')
        );
        return $columns;
    }

    /**
     * @return array
     */
    public function get_hidden_columns()
    {
        return array();
    }

    /**
     * @return array
     */
    public function get_sortable_columns()
    {
        return array(
            'title' => array( 'title', false ),
        );
    }

    /**
     * @param string $s
     * @param string $orderby
     * @param string $order
     * @return array
     */
    private function table_data($s = '', $orderby = '' , $order = '') {
        if (empty($orderby)) {
            $args = array(
                's' => $s,
                'post_type' => Apollo_DB_Schema::_ORGANIZATION_PT,
                'post_status' => 'publish',
                'posts_per_page' => $this->perPage,
                'offset' => $this->getOffset()
            );
        } else {
            $args = array(
                's' => $s,
                'post_type' => Apollo_DB_Schema::_ORGANIZATION_PT,
                'post_status' => 'publish',
                'posts_per_page' => $this->perPage,
                'orderby' => $orderby,
                'order' => $order,
                'offset' => $this->getOffset()
            );
        }
        $data = get_posts($args);

        $the_query = new WP_Query( $args );
        $this->totalItems = $the_query->found_posts;
        return $data;
    }

    /**
     * @param string $text
     * @param string $input_id
     */
    public function search_box($text, $input_id)
    {
        // get current selected organization
        ?>
        <div id="org-syndication-search">
            <p class="search-box">
                <label class="screen-reader-text" for="<?php echo esc_attr( $input_id ); ?>"><?php echo $text; ?>:</label>
                <input type="search" id="<?php echo esc_attr( $input_id ); ?>" name="s" value="<?php _admin_search_query(); ?>" />
                <a id="apl-ajax-wp-list-table-search" class="button" href="" ><?php _e('Search', 'apollo'); ?></a>
            </p>
        </div>
        <?php
    }

    /**
     * @param object $item
     * @param string $column_name
     * @return mixed
     */
    public function column_default( $item, $column_name )
    {
        switch( $column_name ) {
            case 'post_date':
            case 'title':
                return $item[ $column_name ];
            default:
                return print_r( $item, true ) ;
        }
    }

    /**
     * @param object $item
     * @return string
     */
    function column_cb($item) {
        $checked = in_array($item->ID, $this->getListOrgSelected()) ? 'checked' : '';
        return sprintf( '<input type="checkbox" name="_org_selected_ids[]" value="%s" %s/><input type="hidden" name="org_ids[]" value="%s"/>', $item->ID, $checked, $item->ID);
    }

    /**
     * @param $item
     * @return string
     */
    public function column_title($item)
    {
        return sprintf('<span >%s</span>', $item->post_title);
    }

    /**
     * @param $item
     * @return string
     */
    public function column_post_date($item) {

        return sprintf('<span>%s</span>', $item->post_date);
    }

}
