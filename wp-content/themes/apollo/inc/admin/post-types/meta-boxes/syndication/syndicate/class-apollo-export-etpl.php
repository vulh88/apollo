<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class Apollo_Syndicate_Export_ETPL extends Apollo_Syndicate_Export_Abstract
{

    public function __construct()
    {
        $aType = isset($_GET['aptype']) ? $_GET['aptype'] : '';
        $this->cacheFile = 'syndication-etpl-'. $aType;
        $this->output = 'html';
        parent::__construct();
    }

    public function handleRequest($echo = true)
    {
        $accIDValue = $this->apID;
        $syndicationMeta = get_post_meta($accIDValue);

        // Get from cache
        if ($cacheContent = $this->getCache($syndicationMeta)) {
            echo $cacheContent;
            return;
        }


        if(empty($syndicationMeta)){
            _e('Account ID or Generate Type is invalid. Process generating is failed.');
            exit;
        }

        if(isset($syndicationMeta['meta-mode']) && $syndicationMeta['meta-mode'][0] === 'YES'){
            $getEventMode = 'individual';
        } else {
            $getEventMode = 'event-type';
        }

        if($getEventMode === 'individual'){
            $eventData = $this->getEventsBySelectedIndividualEventIDs($syndicationMeta);
        } else {
            $eventData = $this->getEventsBySelectedEventType($syndicationMeta);
        }

        if(!empty($eventData)){
            if(isset($syndicationMeta['meta-include-primary-category']) && $syndicationMeta['meta-include-primary-category'][0] == 'ON'){
                $primaryCateSeparated = true;
            } else {
                $primaryCateSeparated = false;
            }

            if($primaryCateSeparated){
                $eventData = self::separateEventDataByPrimaryCategory($eventData);
            }
            if($this->apType === 'html'){
                $str = Apollo_App::getTemplatePartCustom( APOLLO_ADMIN_SYNDICATION_TEMPLATE_DIR . '/simple_html.php',array(
                    'objectClass' => $this,
                    'event_data' => $eventData,
                    'primary_category_separated' => $primaryCateSeparated,
                    'syndication_meta_data' => $syndicationMeta
                ));

            } else if($this->apType === 'e-html') {
                $str = Apollo_App::getTemplatePartCustom( APOLLO_ADMIN_SYNDICATION_TEMPLATE_DIR . '/expanded_html.php',array(
                    'objectClass' => $this,
                    'event_data' => $eventData,
                    'primary_category_separated' => $primaryCateSeparated,
                    'syndication_meta_data' => $syndicationMeta
                ));
            } else if($this->apType === 'e-html2') {
                $str = Apollo_App::getTemplatePartCustom( APOLLO_ADMIN_SYNDICATION_TEMPLATE_DIR . '/expanded_html2.php',array(
                    'objectClass' => $this,
                    'event_data' => $eventData,
                    'primary_category_separated' => $primaryCateSeparated,
                    'syndication_meta_data' => $syndicationMeta
                ));
            } else {
                $str = Apollo_App::getTemplatePartCustom( APOLLO_ADMIN_SYNDICATION_TEMPLATE_DIR . '/plain_text.php',array(
                    'objectClass' => $this,
                    'event_data' => $eventData,
                    'primary_category_separated' => $primaryCateSeparated,
                    'syndication_meta_data' => $syndicationMeta
                ));
            }

            if (!$echo) {
                return [
                    'string'    => $str
                ];
            }

            echo $str;

            // Store cache
            $this->addCache($str);
        }
        else {
            if (!$echo) {
                return array(
                    'string'    => ''
                );
            }

            _e('No event is available.','prfx-textdomain');
        }
    }

    /**
     * Abstract whether set store transient cache or not
     * @author vulh
     * @return boolean
     */
    protected function setEnableStoreTransientCache()
    {
        return false;
    }
}

