<div id="content">
    <div id="content_main_inner">
        <div id="description">
            <div id="spotlight_right">
                <div id="event_image" class="event_image_div" style="float: left; margin-right: 8px; border: 0px;"/>
                {image}
            </div>
            <h4 class="name"><?php echo  $venue['name'] ?></h4>

            <p><?php echo  $venue['description'] ?></p>
        </div>
        <br style="clear:both;"/>

        <div id="info">
            <?php if (!empty($venue['hours'])) : ?>
			<h3><span>Hours</span></h3>
			<?php echo  $venue['hours'] ?></p>
		<?php endif; ?>
            <?php if (!empty($venue['admission'])) : ?>
			<h3><span>Admission</span></h3>
			<?php echo  $venue['admission'] ?></p>
		<?php endif; ?>
            <h3><span>Location &amp; Map</span></h3>

            <p>
            <?php if (!empty($venue['address1'])): ?><p><?php echo  $venue['address1']; ?><br/><?php endif; ?>
            <?php if (!empty($venue['address2'])): ?><p><?php echo  $venue['address2']; ?><br/><?php endif; ?>
                <?php echo  $venue['city'] ?>, <?php echo  $venue['state'] ?> <?php echo  $venue['zip'] ?>
            </p>

            <?php if (!empty($lat)) : ?>
                <script src="http://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
                <script>
                    var map;
                    function initialize() {
                        var mapOptions = {
                            zoom: 15,
                            center: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $long; ?>),
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        };
                        map = new google.maps.Map(document.getElementById('map-canvas'),
                            mapOptions);

                        var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $long; ?>),
                            map: map,
                            title: '<?php echo $venue['name'] ?>'
                        });

                    }

                    google.maps.event.addDomListener(window, 'load', initialize);
                </script>
                <div id="map-canvas" style="height:300px;width:430px;"></div>
            <?php endif; ?>


            <!--/htdig_noindex-->
            <?php if (!empty($venue['parking'])): ?>
                <div id="parking_info">
                    <h3><span>Parking Info </span></h3>

                    <p><?php echo  $venue['parking'] ?></p>
                </div>
            <?php endif; ?>
            <?php if (!empty($access_available)): ?>
                <div id="accessibility_info">
                    <h3><span>Accessibility Info</span></h3>
                    <?php foreach ($access_available as $val) : ?>
                        <img src="/images/icon-accessibility-<?php echo  $val['image']; ?>.gif" width="100" height="33" alt=""/>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
            <?php if (0): ?>
                <div id="additional_info">
                    <!--htdig_noindex-->
                    <h3><span>Additional Info</span></h3>
                    <?php echo  $additionalInfoContainerBlock ?>
                    <!--htdig_noindex-->
                </div>
            <?php endif; ?>
            <!--htdig_noindex-->
            <p><br/></p>

            <p><strong>NOTE:</strong> We do our best to ensure all information is accurate, however it's a good idea to
                visit the website or call the venue to verify the information.</p>
            <!--htdig_noindex-->
        </div>
    </div>
    <!--/div-->
    <br/>

    <div class="advertisement"><?php echo  html_entity_decode($widlower) ?></div>
    <br/>
</div>

