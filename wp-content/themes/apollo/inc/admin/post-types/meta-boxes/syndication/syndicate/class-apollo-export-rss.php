<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Apollo_Syndicate_Export_RSS extends Apollo_Syndicate_Export_Abstract
{

    public function __construct()
    {
        $this->cacheFile = 'syndication-rss';
        $this->output = 'xml';
        parent::__construct();
    }

    public function handleRequest($echo = true)
    {
        try {
            global $wpdb;

            $syndicationMeta = get_post_meta($this->apID);

            // Store cache
            if ($cacheContent = $this->getCache($syndicationMeta)) {
                echo $cacheContent;
                return;
            }


            $dataDisplayedEFields = self::getDisplayEventFieldsList($this->apID);
            $dataQueryArgs = $this->dataQueryArgs;
            if (is_array($dataDisplayedEFields) && is_array($dataQueryArgs)) {
                $data = array_merge($dataQueryArgs, $dataDisplayedEFields);
            } else {
                $data = $dataDisplayedEFields;
            }

            $post_id = $this->apID;

            if(empty($syndicationMeta)){
                _e('Account ID or Output Type is invalid. Process generating is failed.');
                exit;
            }

            if(isset($syndicationMeta['meta-mode']) && $syndicationMeta['meta-mode'][0] === 'YES'){
                $getEventMode = 'individual';
            } else {
                $getEventMode = 'event-type';
            }

            if($getEventMode === 'individual'){
                $events = $this->getEventsBySelectedIndividualEventIDs($syndicationMeta);
            } else {
                $events = $this->getEventsBySelectedEventType($syndicationMeta);
            }

            /**
             * ThienLD : refactor code to be clearer and handle ticket http://redmine.elidev.info/issues/12655
             */
            $blacklist = self::getMetaDataByKey('meta-blacklist', $syndicationMeta);
            $blacklist = self::getUnSerializeValueForQuery($blacklist);
            $emStartDate = self::getMetaDataByKey('meta-dateStart', $syndicationMeta);
            $emEndDate = self::getMetaDataByKey('meta-dateEnd', $syndicationMeta);
            $emDateRange = self::getMetaDataByKey('meta-range', $syndicationMeta);
            $emFilterDate = self::getEventStartDateEndDateForQuery($emDateRange, $emStartDate, $emEndDate);
            $types = self::getMetaDataByKey('meta-type', $syndicationMeta);
            $types = self::getUnSerializeValueForQuery($types);
            $syndDescription = self::getMetaDataByKey('meta-desc',$syndicationMeta,'');

            $xml = "<rss>\n";
            $xml .= "<channel>\n";

            $xml .= "<title><![CDATA[ Events " . $syndDescription . " ]]></title>\n";
            $xml .= "<link><![CDATA[ http://" . $_SERVER['HTTP_HOST'] . " ]]></link>\n";
            $xml .= "<description><![CDATA[ " . $syndDescription . " ]]></description>\n";
            $xml .= "<language><![CDATA[ en-us ]]></language>\n";
            $xml .= "<pubDate><![CDATA[ " . date('D, d M Y H:i:s O') . " ]]></pubDate>\n";

            foreach ($events as $eventID) {

                if (!empty($eventID->ID)) {
                    $eventID = $eventID->ID;
                }

                /**
                 * ThienLD : handle issue in ticket #12654 and refactor code to be easier maintain in the future
                 */

                if ($this->isExpiredTime($eventID)) continue;

                $row = self::get_all_details($eventID, array(
                    'img_size'  => false
                ));
                if (empty($row)) {
                    continue;
                }

                $eventType = self::getValByKey($row,'_apl_event_term_primary_id');

                $admissionPhone = isset($row['_admission_phone']) && $row['_admission_phone'] ? self::getValByKey($row,'_admission_phone') : '';

                if ($eventType == '') continue;

                if ( self::isExistedInEventBlacklistType($row, $blacklist) ) continue;

                if ( ! self::isExistedInEventType($row, $types, $syndicationMeta)) continue;

                /**
                 * ThienLD : [END] - handle issue in ticket #12654 and refactor code to be easier maintain in the future
                 */

                $xml .= "<item>\n";

                $eventUrl = get_permalink($eventID);

                $xml .= "<title><![CDATA[ " . $row['post']->post_title . " ]]></title>\n";
                $xml .= "<link><![CDATA[ $eventUrl ]]></link>\n";
                $xml .= "<guid><![CDATA[ $eventUrl ]]></guid>\n";
                $xml .= "<eventPhone1><![CDATA[ $admissionPhone ]]></eventPhone1>\n";

                $xml .= "<description><![CDATA[ ".$row['post_content_200']." ]]></description>\n";
                $xml .= "<pubDate>" . date('D, d M Y H:i:s O', strtotime($row['_apollo_event_start_date'])) . "</pubDate>\n";
                $xml .= "</item>\n";
            }

            $xml .= "</channel>\n";
            $xml .= "</rss>";

            if (!$echo) {
                return array(
                    'string'	=> $xml
                );
            }

            echo $xml;

            $this->addCache($xml);
        }
        catch (Exception $ex) {
            if (!$echo) {
                return array(
                    'string'	=> ''
                );
            }

            aplDebugFile($ex->getMessage(), 'Syndicate export with type as RSS');
            wp_safe_redirect('/404');
        }
    }

    /**
     * Abstract whether set store transient cache or not
     * @author vulh
     * @return boolean
     */
    protected function setEnableStoreTransientCache()
    {
        return true;
    }
}