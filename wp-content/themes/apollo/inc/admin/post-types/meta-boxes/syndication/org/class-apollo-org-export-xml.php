<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Apollo_Org_Syndicate_Export_XML
{
    public function __construct()
    {

    }

    public function handleRequest($echo = true)
    {
        try {
            $xml = "<organizations>\n";
            $postId = isset($_GET['apid']) ? $_GET['apid'] : '';
            $allOrgs = self::getOrganization($postId);
            if (!empty($allOrgs)) {
                foreach ($allOrgs as $org) {

                    $org_address_meta_data = maybe_unserialize(Apollo_App::apollo_get_meta_data($org->ID, Apollo_DB_Schema::_APL_ORG_ADDRESS));
                    $org_meta_data = maybe_unserialize(Apollo_App::apollo_get_meta_data($org->ID, Apollo_DB_Schema::_APL_ORG_DATA));
                    $address1 = isset($org_address_meta_data[Apollo_DB_Schema::_ORG_ADDRESS1]) ? $org_address_meta_data[Apollo_DB_Schema::_ORG_ADDRESS1] : '';
                    $address2 = isset($org_address_meta_data[Apollo_DB_Schema::_ORG_ADDRESS2]) ? $org_address_meta_data[Apollo_DB_Schema::_ORG_ADDRESS2] : '';
                    $city = isset($org_address_meta_data[Apollo_DB_Schema::_ORG_CITY]) ? $org_address_meta_data[Apollo_DB_Schema::_ORG_CITY] : '';
                    $state = isset($org_address_meta_data[Apollo_DB_Schema::_ORG_STATE]) ? $org_address_meta_data[Apollo_DB_Schema::_ORG_STATE] : '';
                    $zip = isset($org_address_meta_data[Apollo_DB_Schema::_ORG_ZIP]) ? $org_address_meta_data[Apollo_DB_Schema::_ORG_ZIP] : '';
                    $longitude = Apollo_App::apollo_get_meta_data($org->ID, Apollo_DB_Schema::_ORG_LONGITUDE);
                    $latitude = Apollo_App::apollo_get_meta_data($org->ID, Apollo_DB_Schema::_ORG_LATITUDE);
                    $phone = isset($org_meta_data[Apollo_DB_Schema::_ORG_PHONE]) ? $org_meta_data[Apollo_DB_Schema::_ORG_PHONE] : '';
                    $email = isset($org_meta_data[Apollo_DB_Schema::_ORG_EMAIL]) ? $org_meta_data[Apollo_DB_Schema::_ORG_EMAIL] : '';
                    $website_url = isset($org_meta_data[Apollo_DB_Schema::_ORG_WEBSITE_URL]) ? $org_meta_data[Apollo_DB_Schema::_ORG_WEBSITE_URL] : '';
                    $discount_url = Apollo_App::apollo_get_meta_data($org->ID, Apollo_DB_Schema::_ORG_DISCOUNT_URL);
                    $org_type = self::getCategory($org->ID);
                    //check business
                    $business_type = '';
                    $business_id = Apollo_App::apollo_get_meta_data($org->ID, Apollo_DB_Schema::_APL_ORG_BUSINESS);
                    if (!empty($business_id)) {
                        $business_cats = self::getCategory($business_id, 'business-type');
                        if (!empty($business_cats)) {
                            $business_type = implode(', ', $business_cats);
                        }
                    }

                    $xml .= "<organization>\n";

                    $xml .= "<orgID><![CDATA[" . $org->ID . "]]></orgID>\n";

                    $xml .= "<orgImage><![CDATA[" . get_the_post_thumbnail_url($org->ID, 'full') . "]]></orgImage>\n";
                    $xml .= "<orgName><![CDATA[" . $org->post_title . "]]></orgName>\n";
                    $xml .= "<orgAddress1><![CDATA[" . $address1 . "]]></orgAddress1>\n";
                    $xml .= "<orgAddress2><![CDATA[" . $address2 . "]]></orgAddress2>\n";
                    $xml .= "<orgCity><![CDATA[" . $city . "]]></orgCity>\n";
                    $xml .= "<orgState><![CDATA[" . $state . "]]></orgState>\n";
                    $xml .= "<orgZip><![CDATA[" . $zip . "]]></orgZip>\n";
                    $xml .= "<orgLatitude><![CDATA[" . $longitude . "]]></orgLatitude>\n";
                    $xml .= "<orgLongitude><![CDATA[" . $latitude . "]]></orgLongitude>\n";
                    $xml .= "<orgPhone><![CDATA[" . $phone . "]]></orgPhone>\n";
                    $xml .= "<orgEmail><![CDATA[" . $email . "]]></orgEmail>\n";
                    $xml .= "<orgUrl><![CDATA[" . $website_url . "]]></orgUrl>\n";
                    $xml .= "<orgDiscountUrl><![CDATA[" . $discount_url . "]]></orgDiscountUrl>\n";
                    $xml .= "<orgType><![CDATA[" . (!empty($org_type) ? implode(', ', $org_type) : '') . "]]></orgType>\n";
                    $xml .= "<orgBusinessType><![CDATA[" . $business_type . "]]></orgBusinessType>\n";
                    $xml .= "<orgDescription><![CDATA[" . $org->post_content . "]]></orgDescription>\n";
                    $xml .= "<datePosted><![CDATA[" . $org->post_date . "]]></datePosted>\n";
                    $xml .= "<link><![CDATA[" . get_the_permalink($org->ID) . "]]></link>\n";
                    $xml .= "<guid><![CDATA[" . get_the_guid($org->ID) . "]]></guid>\n";
                    $xml .= "<source><![CDATA[" . $_SERVER['SERVER_NAME'] . "]]></source>\n";

                    $xml .= "</organization>\n";
                }
            }
            $xml .= "</organizations>\n";
            if (!$echo) {
                return array(
                    'string'	=> $xml
                );
            }

            echo trim($xml);

        } catch (Exception $ex) {

            if (!$echo) {
                return array(
                    'string'	=> ''
                );
            }

            aplDebugFile($ex->getMessage(), 'Syndicate export with type as XML');
            wp_safe_redirect('/404');
        }
    }

    /**
     * get Organization by option
     * @return array
     */
    private static function getOrganization($postID) {
        global $wpdb;

        if(!empty($postID)){
            $select_all = get_post_meta($postID,APL_Syndication_Const::_SO_META_ORG_ALL, true);
            $limit = get_post_meta($postID,APL_Syndication_Const::_SO_META_ORG_LIMIT, true);
            $orderby = get_post_meta($postID,APL_Syndication_Const::_SO_META_ORG_ORDER_BY, true);
            $order = get_post_meta($postID,APL_Syndication_Const::_SO_META_ORG_ORDER, true);
        }
        else{
            $select_all = !get_option(Apollo_DB_Schema::_SYNDICATION_ALL_ORGANIZATION, 0);
            $limit = get_option(Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_LIMIT, 1000);
            $orderby = get_option(Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_ORDERBY, 'post_title');
            $order = get_option(Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_ORDER, 'asc');
        }

        $sql = "SELECT ".$wpdb->posts.".ID, ".$wpdb->posts.".post_title, ".$wpdb->posts.".post_content, ".$wpdb->posts.".post_date 
                FROM " . $wpdb->posts ." WHERE post_type='". Apollo_DB_Schema::_ORGANIZATION_PT ."' AND post_status='publish'";

        if (!$select_all) {
            $org_ids = !empty($postID) ? get_post_meta($postID,APL_Syndication_Const::_SO_META_ORG_SELECTED, true)
                                        :  $org_ids = get_option(Apollo_DB_Schema::_SYNDICATION_SELECTED_ORGANIZATION, '');
            $sql .= " AND ID IN( " . (!empty($org_ids) ? $org_ids : '-1') . " )";
        }
        $sql .= " ORDER BY ". $orderby . " " . $order . " LIMIT " . $limit;
        return $wpdb->get_results($sql);

    }

    private static function getCategory( $org_id, $type = 'organization-type' ) {
        $all_cats = get_the_terms( $org_id , $type );
        $result = array();
        if ($all_cats) {
            foreach ($all_cats as $cat) {
                array_push($result, $cat->name);
            }
        }
        return $result;
    }

}