<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-type" content="text/html;charset=utf-8"/>
    <title>Syndication</title>

    {resetcss}
    {screencss}
    {customcss}
    {landingcss}
    {sortable}
    {customscript}
    {overridecss}

</head>
<body>

<div id="content">
    {image}
    <div id="search">
        <div id="basic_search" class="clearfix">

            <?php
            $domain = get_site_url();
            $calhome = $domain . "/?"._APL_SYNDICATED_DATA_HANDLER_FLAG."&apid={apid}&aplang=eng&apoutput=iframe";
            $calsearch = $domain . "/?"._APL_SYNDICATED_DATA_HANDLER_FLAG."&apid={apid}&aplang=eng&apoutput=iframe&search=Y";
            ?>



            <form name="searchForm" method="get" action="<?php echo $calsearch; ?>">
                <input  type="hidden" name="<?php echo _APL_SYNDICATED_DATA_HANDLER_FLAG ?>"/>
                <input  type="hidden" name="apid" value="{apid}"/>
                <input  type="hidden" name="aplang" value="eng"/>
                <input  type="hidden" name="apoutput" value="iframe"/>
                <input  type="hidden" name="search" value="Y"/>
                <input  type="hidden" name="apd" value="more"/>

                <table class="tabcol" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td colspan='4'>

                            <h4 class="calendarhomelink"><a href="<?php echo  isset($calhome) ? $calhome : ''; ?>">Calendar
                                    Home</a>
                                <?php if ($showsubmit == 'YES') : ?>
                                <span style="color:#FFF"> | </span><a id="submit_event_btn" target=_blank
                                                                      href="{submit}"> Submit Event</a>
        </div>
        <?php endif; ?>
        </h4>
        </td>
        </tr>
        <tr>
            <td colspan='4'><span style="font-size:12px; line-height:16px;margin-left:9px;"><a href="#" id="showquick"><?php _e('Quick Search','apollo') ?></a></span></td>
        </tr>


        <tr>
            <td style="width:10px">&nbsp;</td>
            <td colspan="3">
                <!--<label>Keyword:</label>-->
                <input placeholder="Keyword" type="text" name="eventName" class="textfield form-control"
                       id="simple_eventName" value="<?php echo !empty($_GET['eventName']) ? $_GET['eventName'] : ''?>" style='margin-right:5px; width:220px;'/>

                <input style="display:none;" type=submit name=submit value="All">



            </td>
        </tr>

        <tr>
            <td colspan="4">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td width="480">

                <div id="quick" style="display:{quick};">

                    <input type=submit name=submit class="submitButton btn" value="Today">
                    <input type=submit name=submit class="submitButton btn" value="Tomorrow">
                    <input type=submit name=submit class="submitButton btn" value="Weekend">

                    <strong>&nbsp;&nbsp;Next: </strong>

                    <input type=submit name=submit class="submitButton btn" value="7">
                    <input type=submit name=submit class="submitButton btn" value="14">
                    <input type=submit name=submit class="submitButton btn" value="30">
                    <input type=submit name=submit class="submitButton btn" id="submit-all-btn" value="All">

                </div>

            </td>

        </tr>

        <tr>
            <td colspan="4">
                <div id="advlink" style="display:{advlink};">
                    <br><span style="font-size:12px;line-height:16px;margin-left:9px;"><a id="showadv">Advanced Search</a></span>
                </div>
            </td>
        </tr>
        <tr>



        <tr>
            <td style="width:10px">&nbsp;</td>
            <td colspan="3">

                <div id="adv" style="display:{adv};">

                    <select name="eventType" id="catopt" class="form-control" style='margin-right:5px'>
                        <option value="">Category</option>
                        {catopts}
                    </select>

                    <select name="eventOrg" id="orgopt" class="form-control" style='margin-right:5px'>
                        <option value="">Organization</option>
                        {orgopts}
                    </select>

                    <?php
                        $venoptName = 'eventVenue';
                        $filterByName = '';
                        $filterByNameValue = '';
                    if(isset($_GET['eventCities'])){
                        $venoptName = 'eventCities';
                        $filterByName = 'eFilterByCityOrZip';
                        $filterByNameValue = 'CITY';
                    }
                    ?>
                    <select name="<?php echo $venoptName ?>" id="venopt" class="form-control" style='margin-right:5px'>
                        <option value="">Venue</option>
                        {venopts}
                    </select>
                    <input type="hidden" name="<?php echo $filterByName; ?>" value="<?php echo $filterByNameValue ?>" id="city_search"/>

                    <br><br>Start: <input style='line-height:22px; width:150px;' name="startDate" id="startDate" type="text" value="<?php echo !empty($_GET['startDate']) ? $_GET['startDate'] : ''?>">
                    &nbsp;&nbsp;&nbsp;End: <input style='line-height:22px; width:150px;' name="endDate" id="endDate" type="text" value="<?php echo !empty($_GET['endDate']) ? $_GET['endDate'] : ''?>">
                    &nbsp;&nbsp;&nbsp;<input type=submit name=submit class="submitButton btn" value="Search" id="search-advanced-btn">
                    &nbsp;&nbsp;&nbsp;<input type=submit name=submit class="submitButton btn" style="background:lightgrey; color:black" value="Reset" id="btn-reset">

                </div>


            </td>
        </tr>




        <td>&nbsp;</td>
        <td>
            <span id="calendar-container"></span>
            <!--<label>Search by date:</label>-->

            <!--strong>Start:</strong> <input name="startDate" id="startDate" type="text" value="<?php echo date("m/d/Y"); ?>"/-->

            <!--strong>End:</strong> <input name="endDate" id="endDate" type="text" value=""/-->

            <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
            <script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
            <link rel="stylesheet"
                  href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">


            <script type="text/javascript">
                $(document).ready(function () {
                    $(function () {
                        $("#startDate").datepicker({
                            defaultDate: null,
                            changeMonth: true,
                            numberOfMonths: 1,
                            dateFormat: "yy-mm-dd",
                            //maxDate:"-d",

                        });
                        $("#endDate").datepicker({
                            changeMonth: true,
                            dateFormat: "yy-mm-dd",
//                            onClose: function (selectedDate) {
//                                var d = selectedDate.split("/");
//                                var da = d[1];
//                                var mo = d[0];
//                                var yr = parseInt(d[2]);
//                                var nd = mo + "/" + da + "/" + yr;
//                                $("#startDate").datepicker("setDate", nd);
//                            }
                        });
                        $('#btn-reset').on('click',function(){
                           $('#simple_eventName').val('');
                           $('#catopt').val('');
                           $('#orgopt').val('');
                           $('#venopt').val('');
                           $('#startDate').val('');
                           $('#endDate').val('');
                           $('#submit-all-btn').trigger('click');
                           return false;
                        });

                        $('#search-advanced-btn').on('click',function(e){
                            var keyword = $('#simple_eventName').val();
                            var cateVal = $('#catopt').val();
                            var orgVal = $('#orgopt').val();
                            var venueVal = $('#venopt').val();
                            var startDateVal = $('#startDate').val();
                            var endDateVal = $('#endDate').val();
                            if(keyword.length > 0
                                || cateVal.length > 0
                                || orgVal.length > 0
                                || venueVal.length > 0
                                || startDateVal.length > 0
                                || endDateVal.length > 0
                            ){
                                return true;
                            }
                            $('#btn-reset').trigger('click');
                            return false;
                        });

                        $('#venopt').on('change',function(e){
                            var val = $(this).find('option:selected').val();
                            if($.isNumeric(val)){
                                $('#city_search').val('');
                                $('#city_search').attr('name','');
                                $('#venopt').attr('name','eventVenue');
                            }else{
                                $('#city_search').val('CITY');
                                $('#city_search').attr('name','eFilterByCityOrZip');
                                $('#venopt').attr('name','eventCities');
                            }
                        });

                    });
                });
            </script>


            <script>
                $(document).ready(function () {
                    $(function () {
                        $("#thisDate").datepicker({
                            defaultDate: "-d",
                            changeMonth: true,
                            numberOfMonths: 1,
                            dateFormat: "mm/dd/yy"

                        });

                    });
                });
            </script>

            <script>
                $(document).ready( function() {

                    $('#showadv').click( function() {
                        $('#quick').hide();
                        $('#advlink').hide();
                        $('#adv').show();
                    });

                    $('#showquick').click( function() {
                        $('#quick').show();
                        $('#advlink').show();
                        $('#adv').hide();
                        $('#catopt').val('');
                        $('#orgopt').val('');
                        $('#venopt').val('');
                        $('#startDate').val('');
                        $('#endDate').val('');
                    });

                });
            </script>




        </td>
        <td style="text-align: right">
            <!--input type=submit name=submit class="submitButton btn" value="Search" -->
        </td>
        <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                &nbsp;
            </td>
        </tr>
        </table>
        </form>
    </div>


    <div style="background-color:grey; color:white; font-size:14px;padding:2px;">{searchres}</div>

    <?php if ($showbanner) : ?>
        <br>
        <div id="promos"><?php echo  $widupper; ?>
        </div>
        <div id="widget_description"><?php echo  $widdesc; ?></div>
    <?php else : ?>
        <br>
    <?php endif; ?>
    {spotfeat}
    {paginate}

