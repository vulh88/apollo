<?php

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

/**
 * Class APL_Artist_Syndication_Table
 *
 */
class APL_Artist_Syndication_Table extends WP_List_Table
{

    private $perPage = 50;
    private $totalItems;
    private $offset = 0;
    private $listArtistSelected;

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    /**
     * @return array
     */
    public function getListArtistSelected()
    {
        return $this->listArtistSelected;
    }

    /**
     * @param array $listOrgSelected
     */
    public function setListArtistSelected($listOrgSelected)
    {
        $this->listArtistSelected = $listOrgSelected;
    }

    public function prepare_items()
    {
        $hidden       = $this->get_hidden_columns();
        $sortable     = $this->get_sortable_columns();
        $currentPage  = $this->get_pagenum();
        $perPage      = $this->perPage;
        $this->offset = ($currentPage - 1) * $this->perPage;
        $type = isset($_GET['artist-type']) ? $_GET['artist-type'] : '';
        $medium = isset($_GET['artist-medium']) ? $_GET['artist-medium'] : '';
        $s = isset($_GET['s']) ? $_GET['s'] : '';
        $orderby = isset($_GET['orderby']) ? $_GET['orderby'] : '';
        $order = isset($_GET['order']) ? $_GET['order'] : '';
        $data         = $this->table_data($s, $orderby, $order, $type, $medium);
        $columns      = $this->get_columns();

        $this->set_pagination_args( array(
            'total_items' => $this->totalItems,
            'per_page'    => $perPage
        ) );
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = $data;
    }

    /**
     * @return array
     */
    public function get_columns()
    {
        $columns = array(
            'cb' => '<input type="checkbox" />',
            'post_title' => __( 'Full Name', 'apollo' ),
            'last_name' => __( 'Last Name', 'apollo' ),
            'type'  =>  __('Category(s)', 'apollo'),
            'medium'  =>  __('Medium(s)', 'apollo'),
            'style'  =>  __('Style(s)', 'apollo'),
            'post_date' => __( 'Post Date', 'apollo')
        );
        return $columns;
    }

    /**
     * @return array
     */
    public function get_hidden_columns()
    {
        return array();
    }

    /**
     * @return array
     */
    public function get_sortable_columns()
    {
        return array(
            'post_title' => array( 'post_title', false ),
            'last_name' => array( 'last_name', false ),
        );
    }

    /**
     * @param string $s
     * @param string $orderby
     * @param string $order
     * @param string $type
     * @param string $medium
     * @return array
     */
    private function table_data($s = '', $orderby = '' , $order = '', $type = '', $medium = '') {
        global $wpdb;
        $postType = Apollo_DB_Schema::_ARTIST_PT;
        $lastNameField = Apollo_DB_Schema::_APL_ARTIST_LNAME;
        $sql = "SELECT SQL_CALC_FOUND_ROWS p.*, am.meta_value as last_name FROM {$wpdb->posts} p
                INNER JOIN {$wpdb->{Apollo_Tables::_APOLLO_ARTIST_META}} am ON p.ID = am.apollo_artist_id AND am.meta_key = '{$lastNameField}' ";
        if (!empty($type)) {
            $sql .= " INNER JOIN {$wpdb->term_relationships} trt ON p.ID = trt.object_id AND trt.term_taxonomy_id = {$type} ";
        }

        if (!empty($medium)) {
            $sql .= " INNER JOIN {$wpdb->term_relationships} trm ON p.ID = trm.object_id AND trm.term_taxonomy_id = {$medium} ";
        }

        $sql .= " WHERE post_type = '{$postType}' AND post_status = 'publish' ";

        if (!empty($s)) {
            $sql .= " AND (p.post_title LIKE '%{$s}%') ";
        }
        if (!empty($orderby) && !empty($order)) {
            if ($orderby == 'last_name') {
                $sql .= " ORDER BY am.meta_value {$order} " ;
            } else {
                $sql .= " ORDER BY p.{$orderby} {$order} ";
            }
        }
        $sql .= " LIMIT {$this->getOffset()}, {$this->perPage}";


        $data = $wpdb->get_results($sql);

        $this->totalItems = $wpdb->get_var("SELECT FOUND_ROWS()");
        return $data;
    }

    /**
     * @param string $text
     * @param string $input_id
     */
    public function search_box($text, $input_id)
    {
        // get current selected organization
        ?>
        <div id="artist-syndication-search">
            <p class="search-box">
                <label class="screen-reader-text" for="<?php echo esc_attr( $input_id ); ?>"><?php echo $text; ?>:</label>
                <input type="search" id="<?php echo esc_attr( $input_id ); ?>" name="s" value="<?php _admin_search_query(); ?>" />
                <a id="apl-ajax-wp-list-table-search" class="button" href="" ><?php _e('Search', 'apollo'); ?></a>
            </p>
        </div>
        <?php
    }

    /**
     * @param object $item
     * @param string $column_name
     * @return mixed
     */
    public function column_default( $item, $column_name )
    {
        switch( $column_name ) {
            case 'post_date':
            case 'post_title':
                return sprintf('<span >%s</span>', $item->post_title);
            case 'last_name':
                $lastName = $item->last_name;
                return sprintf('<span >%s</span>', $lastName);
            case 'type':
                return self::artistSyndicationRenderCate($item->ID, 'artist-type');
            case 'medium':
                return self::artistSyndicationRenderCate($item->ID, 'artist-medium', Apollo_DB_Schema::_APL_ARTIST_ANOTHER_MEDIUM);
            case 'style':
                return self::artistSyndicationRenderCate($item->ID, 'artist-style', Apollo_DB_Schema::_APL_ARTIST_ANOTHER_STYLE);
            default:
                return print_r( $item, true ) ;
        }
    }

    /**
     * @param object $item
     * @return string
     */
    function column_cb($item) {
        $checked = in_array($item->ID, $this->getListArtistSelected()) ? 'checked' : '';
        return sprintf( '<input type="checkbox" name="artist_selected_ids[]" value="%s" %s/><input type="hidden" name="artist_ids[]" value="%s"/>', $item->ID, $checked, $item->ID);
    }

    public function extra_tablenav($which)
    {
        if ( $which == "top" ){
            $typeSelected = isset($_GET['artist-type']) ? $_GET['artist-type'] : '';
            $mediumSelected = isset($_GET['artist-medium']) ? $_GET['artist-medium'] : '';
            apollo_dropdown_categories( Apollo_DB_Schema::_ARTIST_PT. '-type',
                array('hide_empty' => 0, 'value' => 'id', 'selected' => $typeSelected)
            );
            apollo_dropdown_categories( Apollo_DB_Schema::_ARTIST_PT. '-medium',
                array('hide_empty' => 0, 'value' => 'id', 'selected' => $mediumSelected), __('Select Medium', 'apollo')
            );
            ?>
            <a id="apl-ajax-wp-list-table-filter" class="button" href="" ><?php _e('Filter', 'apollo'); ?></a>
            <a id="apl-ajax-wp-list-table-reset" class="button" href="" ><?php _e('Reset', 'apollo'); ?></a>
            <?php

        }
    }

    /**
     * @param $item
     * @return string
     */
    public function column_post_date($item) {

        return sprintf('<span>%s</span>', $item->post_date);
    }

    private function artistSyndicationRenderCate($artistId, $column, $anotherCateField = Apollo_DB_Schema::_APL_ARTIST_ANOTHER_CAT) {
        $terms = get_the_terms( $artistId, $column );
        $cat_name = Apollo_App::apollo_get_meta_data( $artistId, $anotherCateField, Apollo_DB_Schema::_APL_ARTIST_DATA );

        $termlist = array();

        if ( $terms ):
            foreach ( $terms as $term ) {
                if ( $term->parent ) continue;  // Not display sub menu in the list event page

                $termlist[] = '<a href="' . admin_url( 'edit.php?' . $column . '=' . $term->slug . '&post_type='.$this->type.'' ) . ' ">' . $term->name . '</a>';
            }
        endif;

        if ( $cat_name ) $termlist[] = $cat_name;

        return $termlist ? implode( ', ', $termlist ) : '<span class="na">&ndash;</span>';
    }

}
