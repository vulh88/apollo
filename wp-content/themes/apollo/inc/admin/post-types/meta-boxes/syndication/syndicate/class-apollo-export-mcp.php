<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Apollo_Syndicate_Export_MCP extends Apollo_Syndicate_Export_Abstract
{

	public function __construct()
	{
		$this->cacheFile = 'syndication-mcp';
		$this->output = 'xml';
		parent::__construct();
	}

	public function handleRequest($echo = true)
	{
		try{
			global $wpdb;

			$syndicationMeta = get_post_meta($this->apID);

			// Store cache
			if ($cacheContent = $this->getCache($syndicationMeta)) {
				echo $cacheContent;
				return;
			}

			$dataDisplayedEFields = self::getDisplayEventFieldsList($this->apID);
			$dataQueryArgs = $this->dataQueryArgs;
			if(is_array($dataDisplayedEFields) && is_array($dataQueryArgs)){
				$data = array_merge($dataQueryArgs, $dataDisplayedEFields);
			} else {
				$data = $dataDisplayedEFields;
			}

			if(empty($syndicationMeta)){
				_e('Account ID or Output Type is invalid. Process generating is failed.');
				exit;
			}

			if(isset($syndicationMeta['meta-mode']) && $syndicationMeta['meta-mode'][0] === 'YES'){
				$getEventMode = 'individual';
			} else {
				$getEventMode = 'event-type';
			}

			if($getEventMode === 'individual'){
				$events = $this->getEventsBySelectedIndividualEventIDs($syndicationMeta);
			} else {
				$events = $this->getEventsBySelectedEventType($syndicationMeta);
			}


			$xml = "<rss>\n";
			$xml .= "<channel>\n";;

            /**
             * ThienLD : refactor code to be clearer and handle ticket http://redmine.elidev.info/issues/12655
             */
            $blacklist = self::getMetaDataByKey('meta-blacklist', $syndicationMeta);
            $blacklist = self::getUnSerializeValueForQuery($blacklist);
            $emStartDate = self::getMetaDataByKey('meta-dateStart', $syndicationMeta);
            $emEndDate = self::getMetaDataByKey('meta-dateEnd', $syndicationMeta);
            $emDateRange = self::getMetaDataByKey('meta-range', $syndicationMeta);
            $emFilterDate = self::getEventStartDateEndDateForQuery($emDateRange, $emStartDate, $emEndDate);
            $types = self::getMetaDataByKey('meta-type', $syndicationMeta);
            $types = self::getUnSerializeValueForQuery($types);
                        
			foreach ($events as $eventID){

				if (!empty($eventID->ID)) {
					$eventID = $eventID->ID;
				}

                /**
                 * ThienLD : handle issue in ticket #12654 and refactor code to be easier maintain in the future
                 */

                if ($this->isExpiredTime($eventID)) continue;

				$row = self::get_all_details($eventID, array(
					'img_size'   => 'large',
				));
				if (empty($row)) {
					continue;
				}

                $eventType = self::getValByKey($row,'_apl_event_term_primary_id');

                if ($eventType == '') continue;

                if ( self::isExistedInEventBlacklistType($row, $blacklist) ) continue;

                if ( ! self::isExistedInEventType($row, $types, $syndicationMeta)) continue;

                /**
                 * ThienLD : [END] - handle issue in ticket #12654 and refactor code to be easier maintain in the future
                 */
                                
				$xml .=  "<item>\n";

				$eventUrl = get_permalink( $eventID );
				$startDate = date("m/d/Y", strtotime($row['_apollo_event_start_date']));
				$endDate = date("m/d/Y", strtotime($row['_apollo_event_end_date']));

				if ($startDate != $endDate) $date = $startDate . " - ". $endDate;
				if ($startDate == $endDate) $date = $startDate;
				if ($endDate == '') $date = $startDate;

				$venue = $row['_venue_name'];
				if ($venue == '') $venue = $row['_venue_name'];

				$image = $row['image'];

				$xml .=  "<link><![CDATA[ $eventUrl ]]></link>\n";
				$xml .=  "<guid><![CDATA[ $eventUrl ]]></guid>\n";

				if ($image == '') {
					$xml .= "<enclosure url='' />";
				} else {
					$length = 0;
					$headers = get_headers($image, 1);
					$length = $headers['Content-Length'];
					$length = $length;
					$mime = $headers['Content-Type'];
					$mime = $mime;

					$xml .= "<enclosure url='$image' length='$length' type='".$mime."' />";
				}


				$xml .=  "<title><![CDATA[".$row['post']->post_title."]]></title>\n";
				$xml .=  "<description><![CDATA[<div class=\"date\">$date</div><div class=\"location\">$venue</div>]]></description>";

				$xml .=  "</item>\n";
			}

			$xml .= "</channel></rss>";

			if (!$echo) {
				return array(
					'string'	=> $xml
				);
			}

			echo $xml;

			// Store cache
			$this->addCache($xml);
		}
		catch (Exception $ex) {

			if (!$echo) {
				return array(
					'string'	=> ''
				);
			}

			aplDebugFile($ex->getMessage(),'Syndicate export with type as MailChimp');
			wp_safe_redirect('/404');
		}
	}

	/**
	 * Abstract whether set store transient cache or not
	 * @author vulh
	 * @return boolean
	 */
	protected function setEnableStoreTransientCache()
	{
		return true;
	}
}