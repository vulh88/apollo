<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}


class Apollo_Admin_Artist_Syndication {


    public function getArtistSyndicationData() {
        $data = array(
            APL_Syndication_Const::_SYNDICATION_ALL_ARTIST => get_option(APL_Syndication_Const::_SYNDICATION_ALL_ARTIST, 0),
            APL_Syndication_Const::_SYNDICATION_SELECTED_ARTIST => get_option(APL_Syndication_Const::_SYNDICATION_SELECTED_ARTIST, ''),
            APL_Syndication_Const::_SYNDICATION_ARTIST_LIMIT => get_option(APL_Syndication_Const::_SYNDICATION_ARTIST_LIMIT, 1000),
            APL_Syndication_Const::_SYNDICATION_ARTIST_ORDERBY => get_option(APL_Syndication_Const::_SYNDICATION_ARTIST_ORDERBY, 'post_title'),
            APL_Syndication_Const::_SYNDICATION_ARTIST_ORDER => get_option(APL_Syndication_Const::_SYNDICATION_ARTIST_ORDER, 'asc'),
            APL_Syndication_Const::_SYNDICATION_ARTIST_ENABLE_CACHE => get_option(APL_Syndication_Const::_SYNDICATION_ARTIST_ENABLE_CACHE, 0),
        );
        return $data;
    }


    public function saveArtistSyndicationData() {
        if (isset($_POST[APL_Syndication_Const::_SYNDICATION_ALL_ARTIST])) {
            update_option(APL_Syndication_Const::_SYNDICATION_ALL_ARTIST, $_POST[APL_Syndication_Const::_SYNDICATION_ALL_ARTIST], 'no');
        }
        if (isset($_POST[APL_Syndication_Const::_SYNDICATION_ARTIST_LIMIT])) {
            update_option(APL_Syndication_Const::_SYNDICATION_ARTIST_LIMIT, $_POST[APL_Syndication_Const::_SYNDICATION_ARTIST_LIMIT], 'no');
        }
        if (isset($_POST[APL_Syndication_Const::_SYNDICATION_ARTIST_ORDERBY])) {
            update_option(APL_Syndication_Const::_SYNDICATION_ARTIST_ORDERBY, $_POST[APL_Syndication_Const::_SYNDICATION_ARTIST_ORDERBY], 'no');
        }
        if (isset($_POST[APL_Syndication_Const::_SYNDICATION_ARTIST_ORDER])) {
            update_option(APL_Syndication_Const::_SYNDICATION_ARTIST_ORDER, $_POST[APL_Syndication_Const::_SYNDICATION_ARTIST_ORDER], 'no');
        }

        if (isset($_POST[APL_Syndication_Const::_SYNDICATION_ARTIST_ENABLE_CACHE])) {
            update_option(APL_Syndication_Const::_SYNDICATION_ARTIST_ENABLE_CACHE, $_POST[APL_Syndication_Const::_SYNDICATION_ARTIST_ENABLE_CACHE], 'no');
        }

        if ( isset($_POST['artist_ids']) && isset($_POST['current_artist_ids'])) {
            $list_id = $_POST['current_artist_ids'];
            $selected_ids = isset($_POST['artist_selected_ids']) ? $_POST['artist_selected_ids'] : array();
            if (!empty($list_id)) {
                $list_id = explode(',', $list_id);
            }
            $remove_ids = array_diff($_POST['artist_ids'], $selected_ids);
            if (!empty($list_id)) {
                $new_ids = array_diff($selected_ids, $list_id);
                $list_id = array_merge($list_id, $new_ids);
                if (!empty($remove_ids)) {
                    $list_id = array_diff( $list_id, $remove_ids );
                }
            } else {
                $list_id = $selected_ids;
            }
            update_option(APL_Syndication_Const::_SYNDICATION_SELECTED_ARTIST, implode(',', $list_id), 'no');
        }
    }

    public function artistSyndicationTemplate()
    {
        if (isset($_POST[APL_Syndication_Const::_SYNDICATION_ALL_ARTIST])) {
            $this->saveArtistSyndicationData();
        }

        $artistSyndicationData = $this->getArtistSyndicationData();
        $all_checked = '';
        $selected_checked = '';

        /**
         * Enable list table when search or filter Artist
         * Disable list table when submit with "All" mode
         */
        $isSelecting = !isset($_POST['artist-syndication-submit']) ? (isset($_GET['s']) || isset($_GET['orderby']) || isset($_GET['paged'])) : false;

        if ($artistSyndicationData[APL_Syndication_Const::_SYNDICATION_ALL_ARTIST] || $isSelecting) {
            $selected_checked = 'checked';
        } else {
            $all_checked = 'checked';
        }

        $listID = array();
        if (!empty($artistSyndicationData[APL_Syndication_Const::_SYNDICATION_SELECTED_ARTIST])) {
            $listID = explode(',', $artistSyndicationData[APL_Syndication_Const::_SYNDICATION_SELECTED_ARTIST]);
        }

        require_once APOLLO_ADMIN_DIR . '/post-types/meta-boxes/syndication/artist/APL_Artist_Syndication_Table.php';

        $tableView = new APL_Artist_Syndication_Table();
        $tableView->setListArtistSelected($listID);

        if (isset($_GET['paged'])) {
            $tableView->setOffset(intval($_GET['paged']));
        }

        $tableView->prepare_items();
        $currentDomainName = get_site_url();

        aplc_require_once(APLC. '/Inc/Files/Syndication/Artist/CacheFactory.php');
        $baseURL = $currentDomainName. '/?' . 'syndicated_data' . '&aplang=eng&apoutput=%s&type='. Apollo_DB_Schema::_ARTIST_PT;
        $xmlEngUrl = sprintf($baseURL, APLC_Inc_Files_Syndication_Artist_CacheFactory::ARTIST);
        $xmlCategoriesUrl = sprintf($baseURL, APLC_Inc_Files_Syndication_Artist_CacheFactory::CATEGORY);
        $xmlStylesUrl = sprintf($baseURL, APLC_Inc_Files_Syndication_Artist_CacheFactory::STYLE);
        $xmlMediumsUrl = sprintf($baseURL, APLC_Inc_Files_Syndication_Artist_CacheFactory::MEDIUM);
        $xmlCitiesUrl = sprintf($baseURL, APLC_Inc_Files_Syndication_Artist_CacheFactory::CITY);

        ?>
        <div class="wrap">
        <div id="icon-users" class="icon32"></div>
        <h2> <?php _e('Artist Syndication', 'apollo'); ?></h2>
        <div class="postbox" style="padding: 10px;">
            <?php $tableView->search_box(__('Search artist', 'apollo'), 'admin-org-syndication'); ?>
            <form id="frm-artist-syndication" method="post" action="">
                <div class="select-all-artist"><input type="radio" id="all-artist"  value="0"
                                                      name="<?php echo APL_Syndication_Const::_SYNDICATION_ALL_ARTIST ?>" <?php echo $all_checked ?> />
                    <label for="all-artist"> <?php _e('All', 'apollo') ?></label>
                    <input type="radio"
                           name="<?php echo APL_Syndication_Const::_SYNDICATION_ALL_ARTIST ?>" <?php echo $selected_checked ?>
                           value="1"/>
                    <label><?php echo __('Select Artist', 'apollo') ?></label>
                </div>

                <?php echo wp_nonce_field(); ?>
                <div class="wrap-select-artist">
                    <?php $tableView->display(); ?>
                </div>

                <div class="wrap-op-blk">
                    <div class="inside">
                        <h3><?php _e("XML Generation", "apollo") ?></h3>
                        <div class="options_group clear ct-selection-box">
                            <?php apollo_wp_text_input(array(
                                'id' => APL_Syndication_Const::_SYNDICATION_ARTIST_LIMIT,
                                'label' => __('Limit', 'apollo'),
                                'value' => $artistSyndicationData[APL_Syndication_Const::_SYNDICATION_ARTIST_LIMIT],
                                'class' => 'apollo_input_number'

                            ));
                            ?>
                        </div>
                        <?php
                        echo '<div class="options_group clear wrap-blk ct-full-width">';
                        echo '<label class="ct-lb-mtc">'. __('Enable file caching','apollo') .'</label>';
                        echo '<div class="options_group clear ct-selection-box">';
                        $val = get_option(APL_Syndication_Const::_SYNDICATION_ARTIST_ENABLE_CACHE, 0);
                        apollo_wp_radio(  array(
                            'id' => APL_Syndication_Const::_SYNDICATION_ARTIST_ENABLE_CACHE,
                            'label' => __( '', 'apollo' ),
                            'value' => $val,
                            'options' => array(
                                1 => __("Yes",'apollo'),
                                0 => __("No",'apollo')
                            )
                        ) );
                        echo '</div>';
                        echo '<button '.($val == "no" ? "disabled" : "").' id="syn-clear-cache" data-type="artist" type="button" data-loading="'.__("Processing", "apollo").'" data-text="'.__("Clear Cache", "apollo").'">'.__("Clear Cache", "apollo").'</button>';
                        echo '</div>';
                        ?>

                        <div class="options_group clear ct-selection-box">
                            <?php apollo_wp_radio(array(
                                'id' => APL_Syndication_Const::_SYNDICATION_ARTIST_ORDERBY,
                                'label' => __('Order by', 'apollo'),
                                'value' => isset($artistSyndicationData[APL_Syndication_Const::_SYNDICATION_ARTIST_ORDERBY]) ? $artistSyndicationData[APL_Syndication_Const::_SYNDICATION_ARTIST_ORDERBY] : 'post_title',
                                'options' => array(
                                    'post_title' => __('Artist name', 'apollo'),
                                    'post_date' => __('Posted date', 'apollo'),
                                ),
                            )); ?>
                        </div>
                        <div class="options_group clear ct-selection-box">
                            <?php apollo_wp_radio(array(
                                'id' => APL_Syndication_Const::_SYNDICATION_ARTIST_ORDER,
                                'label' => __('Sort order', 'apollo'),
                                'value' => isset($artistSyndicationData[APL_Syndication_Const::_SYNDICATION_ARTIST_ORDER]) ? $artistSyndicationData[APL_Syndication_Const::_SYNDICATION_ARTIST_ORDER] : 'asc',
                                'options' => array(
                                    'asc' => __('ASC', 'apollo'),
                                    'desc' => __('DESC', 'apollo'),
                                ),
                            )); ?>
                        </div>
                        <div class="options_group clear">
                            <?php apollo_wp_text_input(array(
                                'id' => '_syndication_artist_xml_english',
                                'label' => __('English', 'apollo'),
                                'value' => $xmlEngUrl,
                            )); ?>
                            <a class="button button-small" href="<?php echo $xmlEngUrl ?>"
                               target=_blank><?php _e("TEST URL", "apollo") ?></a>
                        </div>


                        <div class="options_group clear">
                            <?php apollo_wp_text_input(array(
                                'id' => '_syndication_artist_xml_categories',
                                'label' => __('Artist Categories', 'apollo'),
                                'value' => $xmlCategoriesUrl,
                            )); ?>
                            <a class="button button-small" href="<?php echo $xmlCategoriesUrl ?>"
                               target=_blank><?php _e("TEST URL", "apollo") ?></a>
                        </div>

                        <div class="options_group clear">
                            <?php apollo_wp_text_input(array(
                                'id' => '_syndication_artist_xml_mediums',
                                'label' => __('Artist Mediums', 'apollo'),
                                'value' => $xmlMediumsUrl,
                            )); ?>
                            <a class="button button-small" href="<?php echo $xmlMediumsUrl ?>"
                               target=_blank><?php _e("TEST URL", "apollo") ?></a>
                        </div>

                        <div class="options_group clear">
                            <?php apollo_wp_text_input(array(
                                'id' => '_syndication_artist_xml_styles',
                                'label' => __('Artist Styles', 'apollo'),
                                'value' => $xmlStylesUrl,
                            )); ?>
                            <a class="button button-small" href="<?php echo $xmlStylesUrl ?>"
                               target=_blank><?php _e("TEST URL", "apollo") ?></a>
                        </div>

                        <div class="options_group clear">
                            <?php apollo_wp_text_input(array(
                                'id' => '_syndication_artist_xml_cities',
                                'label' => __('Cities', 'apollo'),
                                'value' => $xmlCitiesUrl,
                            )); ?>
                            <a class="button button-small" href="<?php echo $xmlCitiesUrl ?>"
                               target=_blank><?php _e("TEST URL", "apollo") ?></a>
                        </div>

                        <div class="select-btn-update"><input class="button button-primary" type="submit"
                                                              name="artist-syndication-submit"
                                                              value="<?php _e('Update', 'apollo') ?>"/></div>
                        <input type="hidden" name="current_artist_ids"
                               value="<?php echo (!empty($tableView->getListArtistSelected()) ? implode(',', $tableView->getListArtistSelected()) : '') ?>"/>
            </form>
        </div>

        <?php
    }
}
