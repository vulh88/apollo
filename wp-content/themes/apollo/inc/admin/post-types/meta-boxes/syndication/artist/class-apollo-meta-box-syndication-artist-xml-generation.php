<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Apollo_Meta_Box_Syndication_Artist_Xml_Generation
 */
class Apollo_Meta_Box_Syndication_Artist_Xml_Generation {

	/**
	 * Output the metabox
	 */
	public static function output( $post ) {
		global $wpdb;
		wp_nonce_field( 'apollo_syndication-artist_meta_nonce', 'apollo_syndication-artist_meta_nonce' );

        $currentDomainName = get_site_url();

        aplc_require_once(APLC. '/Inc/Files/Syndication/Artist/CacheFactory.php');
        $baseURL = $currentDomainName. '/?' . 'syndicated_data' . '%s&aplang=eng&apoutput=%s&aptype='. Apollo_DB_Schema::_ARTIST_PT;
        $artistId = '&apid='.$post->ID;
        $xmlEngUrl = sprintf($baseURL, $artistId, 'wp');
        $xmlCategoriesUrl = sprintf($baseURL,'', APLC_Inc_Files_Syndication_Artist_CacheFactory::CATEGORY);
        $xmlStylesUrl = sprintf($baseURL, '', APLC_Inc_Files_Syndication_Artist_CacheFactory::STYLE);
        $xmlMediumsUrl = sprintf($baseURL, '', APLC_Inc_Files_Syndication_Artist_CacheFactory::MEDIUM);
        $xmlCitiesUrl = sprintf($baseURL, '', APLC_Inc_Files_Syndication_Artist_CacheFactory::CITY);

        echo '<div class="options_group clear">';
        apollo_wp_text_input(array(
            'id' => '_syndication_artist_xml_english',
            'label' => __('English', 'apollo'),
            'value' => $xmlEngUrl,
            'custom_attributes' => array(
                'readonly' => true
            ),
        ));
        echo '<a class="button button-small" href="'.$xmlEngUrl.'" target=_blank>' . __("TEST URL", "apollo") . '</a>';
        echo '</div>';


        echo '<div class="options_group clear">';
        apollo_wp_text_input(array(
            'id' => '_syndication_artist_xml_categories',
            'label' => __('Artist Categories', 'apollo'),
            'value' => $xmlCategoriesUrl,
            'custom_attributes' => array(
                'readonly' => true
            ),
        ));
        echo '<a class="button button-small" href="'.$xmlCategoriesUrl.'" target=_blank>'. __("TEST URL", "apollo") .'</a>';
        echo '</div>';

        echo '<div class="options_group clear">';
        apollo_wp_text_input(array(
            'id' => '_syndication_artist_xml_mediums',
            'label' => __('Artist Mediums', 'apollo'),
            'value' => $xmlMediumsUrl,
            'custom_attributes' => array(
                'readonly' => true
            ),
        ));
        echo '<a class="button button-small" href="'.$xmlMediumsUrl.'" target=_blank>'. __("TEST URL", "apollo") .'</a>';
        echo '</div>';

        echo '<div class="options_group clear">';
        apollo_wp_text_input(array(
            'id' => '_syndication_artist_xml_styles',
            'label' => __('Artist Styles', 'apollo'),
            'value' => $xmlStylesUrl,
            'custom_attributes' => array(
                'readonly' => true
            ),
        ));
        echo '<a class="button button-small" href="'.$xmlStylesUrl.'" target=_blank>'. __("TEST URL", "apollo") .'</a>';
        echo '</div>';

        echo '<div class="options_group clear">';
        apollo_wp_text_input(array(
            'id' => '_syndication_artist_xml_cities',
            'label' => __('Cities', 'apollo'),
            'value' => $xmlCitiesUrl,
            'custom_attributes' => array(
                'readonly' => true
            ),
        ));
        echo '<a class="button button-small" href="'.$xmlCitiesUrl.'" target=_blank>'. __("TEST URL", "apollo").'</a>';
        echo '</div>';
	}

	/**
	 * Save meta box data
	 */
	public static function save( $post_id, $post ) {

	}
}
