<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

include_once APOLLO_ADMIN_SYNDICATE_DIR. '/class-apollo-syndicate-abstract.php';

class Apollo_Syndicate_Caching_Handler extends Apollo_Syndicate_Export_Abstract
{
    public function handleCaching($account_id = '')
    {
        if(!empty($account_id) && intval($account_id) > 0){
            $this->setSyndicationDataCache($account_id);
        }
    }

    /**
     * Clear file cache
     */
    public static function clearCache()
    {
        require_once(APOLLO_ADMIN_DIR. '/tools/cache/Inc/Files/Syndication.php');
        $cacheIns = new APLC_Inc_Files_Syndication('');
        foreach (glob($cacheIns->getBaseDir(). "/syndication*.*") as $filename) {
            @unlink($filename);
        }
    }

    /**
     * Abstract whether set store transient cache or not
     * @author vulh
     * @return boolean
     */
    protected function setEnableStoreTransientCache()
    {
        return true;
    }
}

