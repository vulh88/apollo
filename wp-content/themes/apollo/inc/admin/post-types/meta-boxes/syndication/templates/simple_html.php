<?php
/**
 * User: Thienld
 * Date: 16/12/2015
 * Time: 17:39
 * Description: Rendering email content as simple html
 */

$primary_category_separated = isset($template_args['primary_category_separated']) ? $template_args['primary_category_separated'] : false;
$event_data = isset($template_args['event_data']) ? $template_args['event_data'] : array();
$syndication_meta_data = isset($template_args['syndication_meta_data']) ? $template_args['syndication_meta_data'] : array();
$display_fields = Apollo_Syndicate_Export_Abstract::getMetaDataByKey('meta-ownsel',$syndication_meta_data,'');
$display_fields = empty($display_fields) ? array() : maybe_unserialize(maybe_unserialize($display_fields));
?>
<?php if($primary_category_separated) : ?>
    <?php foreach($event_data as $pCateID => $listEvent) : ?>
        <?php
            $priCateItem = get_term($pCateID,'event-type');
            $termName = !empty($priCateItem) ? $priCateItem->name : '';
        ?>
        <h3><?php echo strtoupper($termName); ?></h3>
        <?php foreach($listEvent as $eID) : ?>
            <?php echo Apollo_App::getTemplatePartCustom( APOLLO_ADMIN_SYNDICATION_TEMPLATE_DIR . '/sh_event_item.php',array('event_id'=>$eID,
                'display_fields' => $display_fields,
                'objectClass' => $template_args['objectClass'],
            )); ?>
        <?php endforeach; ?>
    <?php endforeach; ?>
<?php else : ?>
    <?php foreach($event_data as $e) : ?>
        <?php
            $e = is_object($e) ? get_object_vars($e) : $e;
            $eventID = $e['ID'];
            if(!Apollo_Syndicate_Export_Abstract::hasSetEventPrimaryCategory($eventID)) { continue; }
            echo Apollo_App::getTemplatePartCustom( APOLLO_ADMIN_SYNDICATION_TEMPLATE_DIR . '/sh_event_item.php',array('event_id'=>$eventID,
                'display_fields' => $display_fields,
                'objectClass' => $template_args['objectClass'],
            )); ?>
    <?php endforeach; ?>
<?php endif; ?>
