<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
require_once APOLLO_ADMIN_SYNDICATION_DIR . '/class-apollo-meta-box-syndication-filter.php';



class Apollo_Syndicate_IFrame_Request_Handler extends Apollo_Syndicate_Export_Abstract
{
    protected $linkest = '';

    public function __construct()
    {

        //Trilm add config link your website, partner widget
        $this->addLinkConfig();
        parent::__construct();
    }

    public function addLinkConfig(){
        $post_id = $this->apID;
        $syndicationMeta = get_post_meta($post_id);
        $metaLinkest = isset($syndicationMeta['meta-linkdest'][0])?$syndicationMeta['meta-linkdest'][0]:'YW';
        $metaLinkest = strtolower($metaLinkest);
        $this->linkest = $metaLinkest;
        add_filter('linkest',array($this,'echoLinkest'));
    }

    public function handleRequest()
    {
        global $wpdb;
        $this->isFrameRequest = true; // mark flag to detect zone of iframe request
        $dataDisplayedEFields = self::getDisplayEventFieldsList($this->apID);
        $dataQueryArgs = $this->dataQueryArgs;

        $this->addLinkConfig();
        if (is_array($dataDisplayedEFields) && is_array($dataQueryArgs)) {
            $data = array_merge($dataQueryArgs, $dataDisplayedEFields);
        } else {
            $data = $dataDisplayedEFields;
        }

        $perpage = 75;

        $post_id = $this->apID;

        $apoff = isset($data['apoff']) ? $data['apoff'] : 0;
        $reqtype = "ifr" . $apoff;

        $iframe = '';
        $prfx_stored_meta = get_post_meta($post_id);

        $display_search_box = get_post_meta($post_id, 'meta-search', TRUE);

        $widupper = get_post_meta($post_id, 'meta-widupper', TRUE);
        $widdesc = get_post_meta($post_id, 'meta-widdesc', TRUE);
        $showsubmit = get_post_meta($post_id, 'meta-submit', TRUE);
        /** @Ticket #14430 */
        $enableEventImage = get_post_meta($post_id, APL_Syndication_Const::META_ENABLE_EVENT_IMAGE_FOR_IFRAME_WIDGET, TRUE);

        if (!empty($data['apd'])) {
            $showbanner = false;
        } else {
            $showbanner = true;
        }

        // Does this iframe require the search box?
        if ($display_search_box == 'YES') {
            ob_start();
            require_once(APOLLO_ADMIN_SYNDICATE_IFRAME_DIR . '/views/search_box.php');
            $iframe .= ob_get_contents();
            ob_end_clean();
        } else {
            ob_start();
            require_once(APOLLO_ADMIN_SYNDICATE_IFRAME_DIR . '/views/no_search_box.php');
            $iframe .= ob_get_contents();
            ob_end_clean();
        }


        // Replace some variables in the search box header code
        define('SYND_ROOT_URL', APOLLO_ADMIN_SYNDICATE_ASSETS_URL);
        $reset = SYND_ROOT_URL . "/css/reset.css";
        $css = SYND_ROOT_URL . "/css/screen.css";
        $landingcss = SYND_ROOT_URL . "/css/landing.css";
        $path = SYND_ROOT_URL;
        $iframe = str_replace('{path}', $path, $iframe);

        if (!empty($prfx_stored_meta['meta-custom-url'][0])) {
            $submit = $prfx_stored_meta['meta-custom-url'][0];
        } else {
            $domain = get_site_url();
            $submit = $domain . "/user/add-event/step-1/";

        }

        $customcss = get_post_meta($post_id, 'meta-widcss', TRUE);

        if  (isset($_GET['submit']) && $_GET['submit'] == 'Reset') {

            $_GET = array();
        }


        $iframe = str_replace('{submit}', $submit, $iframe);
        $iframe = str_replace('{sortable}', '<script src="' . SYND_ROOT_URL . '/js/sorttable.js"></script>', $iframe);
        $iframe = str_replace('{resetcss}', '<link  rel="stylesheet" href="' . $reset . '" type="text/css" media="screen, projection"/>', $iframe);
        $iframe = str_replace('{screencss}', '<link  rel="stylesheet" href="' . $css . '?v='.APOLLO_ADMIN_SYNDICATION_CSS_VERSION.'" type="text/css" media="screen, projection"/>', $iframe);

        $custom_css = '<link  rel="stylesheet" href="' . $customcss . '" type="text/css" media="screen, projection"/>';
        $custom_css .= '<link rel="stylesheet" href="' . SYND_ROOT_URL . '/tooltipster/dist/css/tooltipster.bundle.min.css" type="text/css" media="screen, projection"/>';
        $custom_css .= '<link rel="stylesheet" href="' . SYND_ROOT_URL . '/css/style.css" type="text/css" media="screen, projection"/>';
        $iframe = str_replace('{customcss}', $custom_css, $iframe);

        /** @Ticket #14429 - Add override css*/
        if ( of_get_option( Apollo_DB_Schema::_ACTIVATE_OVERRIDE_CSS ) && file_exists(get_override_css_abs_path() ) ) {
            $overrideCss = get_override_css_url();

        } else {
            $overrideCss = '';
        }
        $iframe = str_replace('{overridecss}', '<link  rel="stylesheet" href="' . $overrideCss . '" type="text/css" media="screen, projection"/>', $iframe);

        $custom_script = '<script src="' . includes_url('/js/jquery/jquery.js') . '"></script>';
        $custom_script .= '<script src="' . SYND_ROOT_URL . '/tooltipster/dist/js/tooltipster.bundle.min.js"></script>';
        $custom_script .= '<script src="' . SYND_ROOT_URL . '/js/script.js"></script>';

        $iframe = str_replace('{customscript}', $custom_script, $iframe);
        if (!empty($prfx_stored_meta['meta-image'][0])) {
            $image = wp_get_attachment_image($prfx_stored_meta['meta-image'][0], 'large');
            $iframe = str_replace('{image}', "$image<br>", $iframe);
        } else {
            $iframe = str_replace('{image}', '', $iframe);
        }

        $iframe = str_replace("{apid}", $post_id, $iframe);

        if (!empty($data['apd'])) {
            if ($data['apd'] == 'det')$iframe = str_replace('{paginate}', '', $iframe);
            $iframe = str_replace('{landingcss}', '', $iframe);
            if ($data['apd'] == 'css') $this->process_css_page($data, $iframe);
            if ($data['apd'] == 'det') $this->process_detail_page($data, $iframe);
            if ($data['apd'] == 'ven') $this->process_venue_page($data, $iframe);
            if ($data['apd'] == 'org') $this->process_org_page($data, $iframe);
            if ($data['apd'] == 'ser') $this->process_search_page($data, $iframe);
        }

        if (empty($data['apoff'])) {
            if (empty($data['apd'])) {
                if (isset($prfx_stored_meta['meta-landing'][0]) && $prfx_stored_meta['meta-landing'][0] == 'YES') {
                    $iframe = str_replace('{landingcss}', '<link  rel="stylesheet" href="' . $landingcss . '" type="text/css" media="screen, projection"/>', $iframe);
                    $this->process_iframe_landing($data, $iframe);
                }
            }
        }


        $iframe = str_replace('{landingcss}', '', $iframe);
        $iframe .= '<div id="event_listing">';
        $iframe .= '<table cellspacing="0" class="sortable">';
        $iframe .= '<caption style="background:#3886BC; color:white;line-height:24px">Events</caption>';

        // Add powered by link
        $url = get_site_url();
        $domain = substr($url, 7);
        // Set up template for line header
        ob_start();
        require_once(APOLLO_ADMIN_SYNDICATE_IFRAME_DIR . '/views/line_listing_hdr.php');
        $tablehdr = ob_get_contents();
        ob_end_clean();

        // Set up template for line listings
        ob_start();
        require_once(APOLLO_ADMIN_SYNDICATE_IFRAME_DIR . '/views/line_listing_item.php');
        $tablerow = ob_get_contents();
        ob_end_clean();

        if (isset($prfx_stored_meta['meta-orgcol'][0]) && $prfx_stored_meta['meta-orgcol'][0] == 'YES') {
            $org = 'Organization';
        } else {
            $org = ' ';
        }
        if (isset($prfx_stored_meta['meta-loccol'][0]) && $prfx_stored_meta['meta-loccol'][0] == 'YES') {
            $loc = 'Location';
        } else {
            $loc = ' ';
        }
        $find = array('{1name}', '{2name}', '{3name}', '{4name}');
        $replace = array('Name', $org, 'Dates', $loc);
        $iframe .= str_replace($find, $replace, $tablehdr);


        /* Thienld : get total feed events */
        $syndicationMeta = get_post_meta($post_id);


        if (empty($syndicationMeta)) {
            _e('Account ID or Output Type is invalid. Process generating is failed.');
            exit;
        }

        if (isset($syndicationMeta['meta-mode']) && $syndicationMeta['meta-mode'][0] === 'YES') {
            $getEventMode = 'individual';
        } else {
            $getEventMode = 'event-type';
        }
        $limit = $perpage;
        $curPage = intval($apoff) + 1;
        $this->curPage = $curPage;
        $this->postPerPage = $limit;

        $searchres = '';
        if (isset($dataQueryArgs['search']) && $dataQueryArgs['search'] == 'Y') {
            $syndicationMeta = $this->setSearchOptions($syndicationMeta);
            $searchres = $this->getresults();
        }

        $events = $this->getEventForIFrame($syndicationMeta);
        $postcnt = intval($this->totalEventsFeed);
        $pages = intval($postcnt);
        $pages = ($pages / $perpage);

        /** @Ticket #14429 */
        $paginate = "<div class='apl-iframe-paginate'><strong>Page:</strong> ";
        if(isset($_SERVER['QUERY_STRING'])){
            $queryArray = array();
            parse_str($_SERVER['QUERY_STRING'],$queryArray);
            if(isset($queryArray['submit']) && strtolower($queryArray['submit']) == 'reset' ){
                unset($queryArray['submit']);
            }
            if(isset($queryArray['apd']) && strtolower($queryArray['apd']) == 'more' ){
                unset($queryArray['apd']);
            }
            if(isset($queryArray['apoff'])){
                unset($queryArray['apoff']);
            }
            $additionalQS = http_build_query($queryArray);
        } else {
            $additionalQS = '';
        }
        if(empty($additionalQS)){
            $pagiBaseLink = $url . "/?" . _APL_SYNDICATED_DATA_HANDLER_FLAG . "&apid=" . $this->apID . "&aplang=eng&apoutput=iframe&apd=more";
        }else{
            $pagiBaseLink = $url . "/?" . $additionalQS . "&apd=more";
        }
        for ($i = 0; $i < $pages; $i++) {
            $pageno = $i + 1;
            $pagelink =  $pagiBaseLink . "&apoff=".$i."";
            if (!empty($data['apoff']) && $data['apoff'] == $i) {
                $paginate .= " <a href='$pagelink'><strong style='text-decoration:underline;'>$pageno</strong>&nbsp;</a>";
            } else {
                $paginate .= " <a href='$pagelink'>$pageno&nbsp;</a>";
            }
        }
        $paginate .= '</div>';
        /**
         * ThienLD : refactor code to be clearer and handle ticket http://redmine.elidev.info/issues/12655
         */
        $blacklist = self::getMetaDataByKey('meta-blacklist', $syndicationMeta);
        $blacklist = self::getUnSerializeValueForQuery($blacklist);
        $emStartDate = self::getMetaDataByKey('meta-dateStart', $syndicationMeta);
        $emEndDate = self::getMetaDataByKey('meta-dateEnd', $syndicationMeta);
        $emDateRange = self::getMetaDataByKey('meta-range', $syndicationMeta);
        $emFilterDate = self::getEventStartDateEndDateForQuery($emDateRange, $emStartDate, $emEndDate);
        $types = self::getMetaDataByKey('meta-type', $syndicationMeta);
        $types = self::getUnSerializeValueForQuery($types);

        $oddeven = 0;
        $iframe .= "<br>";


        // updated by QA's suggestion : always hidden this block spotlight and featured events on search page (search = Y)
        $iframe = str_replace('{spotfeat}', '', $iframe);

        foreach ($events as $event) {

            /**
             * ThienLD : handle issue in ticket #12654 and refactor code to be easier maintain in the future
             */

            if ($this->isExpiredTime($event->ID)) continue;


            $row = self::get_all_details_for_iframe($event->ID);

            if (empty($row) || $event->post_title == 'Auto Draft') {
                continue;
            }

            $eventType = self::getValByKey($row,'_apl_event_term_primary_id');

            if ($eventType == '') continue;

            if ( self::isExistedInEventBlacklistType($row, $blacklist) ) continue;

            if ( ! self::isExistedInEventType($row, $types, $syndicationMeta)) continue;

            /**
             * ThienLD : [END] - handle issue in ticket #12654 and refactor code to be easier maintain in the future
             */

            if ($_SERVER['REMOTE_ADDR'] == '73.252.6.1') {
                //if ($eventType == '') { echo "evt=$eventType title=$event->post_title ID=$event->ID <br>"; print_r($row);}
            }

            if (is_array($blacklist) && in_array($eventType, $blacklist)) continue;

            $oddeven++;
            if ($oddeven % 2 == 0) {
                $class = '';
            } else {
                $class = ' class="odd" ';
            }

            if (empty($row['_org_name'])) {
                $tmp_org = $wpdb->get_results("SELECT meta_value from $wpdb->apollo_eventmeta where apollo_event_id = $event->ID and meta_key = '_apl_event_tmp_org'");
                $tmp_org = !empty($tmp_org) ? $tmp_org[0]->meta_value : '';
                $row['_org_name'] = $tmp_org;
            }

            if (isset($prfx_stored_meta['meta-orgcol']) && $prfx_stored_meta['meta-orgcol'][0] == 'YES') {
                $org = self::getValByKey($row, '_org_name');
            } else {
                $org = ' ';
            }

            if (isset($prfx_stored_meta['meta-loccol'][0]) && $prfx_stored_meta['meta-loccol'][0] == 'YES') {

                if ($row['_venue_name'] != '') {
                    $loc = $row['_venue_name'] . ", " . $row['_venue_city'];
                } else {

                    if (empty($row['_venue_name'])) {
                        $this->getVenueTmpByEventId($row,$event->ID);
                    }

                    if($row['_venue_name'] == ''){
                        $loc = $row['_venue_city'];
                    }else{
                        $loc = $row['_venue_name'] . ", " . $row['_venue_city'];
                    }

                }

            } else {
                $loc = ' ';
            }
            $dates = date("m/d/Y", strtotime(self::getValByKey($row, '_apollo_event_start_date')));

            //if (self::getValByKey($row, '_apollo_event_end_date') == '2037-01-01') {
            if (Apollo_App::isOngoing(strtotime($row[Apollo_DB_Schema::_APOLLO_EVENT_START_DATE]), strtotime($row[Apollo_DB_Schema::_APOLLO_EVENT_END_DATE]))) {
                $dates = "Ongoing";
            } else {
                if ((!empty($row['_apollo_event_end_date'])) AND ($row['_apollo_event_start_date'] != $row['_apollo_event_end_date'])) $dates .= " - " . date("m/d/Y", strtotime($row['_apollo_event_end_date']));
            }

            $event_obj = get_event( $event );
            $find = array('{class}', '{1url}', '{1name}', '{2name}', '{3name}', '{4name}', '{event_name}', '{event_image}', '{event_content}', '{org_venue}', '{event_id}', '{dates}');

            if (isset($prfx_stored_meta['meta-linkdest'][0]) && $prfx_stored_meta['meta-linkdest'][0] == 'PW') {
                $link = $url . "/?" . _APL_SYNDICATED_DATA_HANDLER_FLAG . "&apid=" . $post_id . "&aplang=eng&apoutput=iframe&apd=det&apdid=" . $event->ID;
            } else {
                $link = get_permalink($event->ID);
            }

            //add link to image
            if(empty($row['image'])){
                $link_image = '';
            }else{
                $link_image = '<a href="' . $link . '">' . $row['image'] . '</a>';
            }
            $replace = array(
                $class,
                $link,
                $event->post_title,
                $org,
                $dates,
                $loc,
                $event->post_title,
                $link_image,
                wp_trim_words($event->post_content, 150),
                $event_obj->renderOrgVenueHtml(),
                $event->ID,
                APL::dateUnionDateShort(self::getValByKey($row, '_apollo_event_start_date'), self::getValByKey($row, '_apollo_event_end_date')));
            $iframe .= str_replace($find, $replace, $tablerow);

        }

        $widlower = get_post_meta($this->apID, 'meta-widlower', TRUE);

        $iframe .= "</table><br style=\"clear:both;\" /><div class=\"advertisement\">$widlower</div>{paginate}";

        $iframe = str_replace('{paginate}', $paginate, $iframe);

        $iframe = str_replace('{searchres}', $searchres, $iframe);
        $keyword = Apollo_Syndicate_Export_Abstract::getDataSearchFilter('eventName');

        if (!empty($keyword)){
            $eventname = ' value="'.$keyword.'"%%';
            $iframe = str_replace(' value=""%%', $eventname, $iframe);
        }

        $iframe = $this->setsearchcss($iframe);


        $options = $this->get_cat_options($post_id,  Apollo_Syndicate_Export_Abstract::getDataSearchFilter('eventType'));

        $iframe = str_replace('{catopts}', $options, $iframe);

        /** Apply cache file */
        $options = $this->get_ven_options($post_id,  Apollo_Syndicate_Export_Abstract::getDataSearchFilter('eventVenue'));

        $iframe = str_replace('{venopts}', $options, $iframe);

        $options = $this->get_org_options($post_id,  Apollo_Syndicate_Export_Abstract::getDataSearchFilter('eventOrg'));

        $iframe = str_replace('{orgopts}', $options, $iframe);

        $iframe = str_replace('{sdate}', Apollo_Syndicate_Export_Abstract::getDataSearchFilter('startDate'), $iframe);
        $iframe = str_replace('{edate}', Apollo_Syndicate_Export_Abstract::getDataSearchFilter('endDate'), $iframe);

        $iframe .= $this->get_analytics($post_id);


        echo $iframe;
    }

    private function process_iframe_landing($data, $iframe)
    {

        global $wpdb;

        $this->isFrameRequest = true;
        $id = isset($data['apdid']) ? $data['apdid'] : 0;
        $iframe = str_replace('{paginate}', '', $iframe);
        $mapimg = SYND_ROOT_URL . "/images/googlemap.png";
        $this->addLinkConfig();
        // Get posts
        $syndicationMeta = get_post_meta($this->apID);
        $syndicationMeta['meta-range'][0] = 0;
        if (isset($_REQUEST['search']) && $_REQUEST['search'] == 'Y') {
            $syndicationMeta = $this->setSearchOptions($syndicationMeta, FALSE);
        }

        if (empty($syndicationMeta)) {
            _e('Account ID or Output Type is invalid. Process generating is failed.');
            exit;
        }
        if (isset($syndicationMeta['meta-mode']) && $syndicationMeta['meta-mode'][0] === 'YES') {
            $getEventMode = 'individual';
        } else {
            $getEventMode = 'event-type';
        }
        $limit = 6;
        $curPage = 1;
        $this->curPage = $curPage;
        $this->postPerPage = $limit;

        $events = $this->getEventForIFrame($syndicationMeta);


        $myevents = array();
        $i = 0;

        foreach ($events as $event) {
            $row = self::get_all_details($event->ID);
            if ($event->post_title == 'Auto Draft') continue;

            $image = wp_get_attachment_image(get_post_thumbnail_id($event->ID), 'thumbnail');

            $image = str_replace('<img ', '<img class="event_image"', $image);
            $url = get_site_url();
            if ($syndicationMeta['meta-linkdest'][0] == 'PW') {
                $link = $url . "/?" . _APL_SYNDICATED_DATA_HANDLER_FLAG . "&apid=" . $this->apID . "&aplang=eng&apoutput=iframe&apd=det&apdid=" . $event->ID;
            } else {
                $link = get_permalink($event->ID);
            }
            $googleMapAddress = self::getValByKey($row, '_venue_address1') . " " . self::getValByKey($row, '_venue_city') . ", " . self::getValByKey($row, '_venue_state') . " " . self::getValByKey($row, '_venue_zip');
            if(isset($_SERVER['QUERY_STRING'])){
                $queryArray = array();
                parse_str($_SERVER['QUERY_STRING'],$queryArray);
                if(isset($queryArray['submit']) && strtolower($queryArray['submit']) == 'reset' ){
                    unset($queryArray['submit']);
                }
                if(isset($queryArray['apd']) && strtolower($queryArray['apd']) == 'more' ){
                    unset($queryArray['apd']);
                }
                $additionalQS = http_build_query($queryArray);
            } else {
                $additionalQS = '';
            }
            if(empty($additionalQS)){
                $calhome2 = $url . "/?" . _APL_SYNDICATED_DATA_HANDLER_FLAG . "&apid=" . $this->apID . "&aplang=eng&apoutput=iframe&apd=more";
            }else{
                $calhome2 = $url . "/?" . $additionalQS . "&apd=more";
            }


            $myevents[$i]['image'] = $image;
            $myevents[$i]['link'] = $link;
            $myevents[$i]['name'] = $event->post_title;
            $myevents[$i]['description'] = $event->post_content;
            $myevents[$i]['dateBegin'] = self::getValByKey($row, '_apollo_event_start_date');
            $myevents[$i]['dateEnd'] = self::getValByKey($row, '_apollo_event_end_date');
            $myevents[$i]['ticketURL'] = self::getValByKey($row, '_admission_ticket_url');
            $myevents[$i]['googleMapAddress'] = $googleMapAddress;
            $myevents[$i]['ID'] = $event->ID;

            $event_obj = get_event( $event );
            $myevents[$i]['org_venue'] = $event_obj->renderOrgVenueHtml();

            $i++;

        }

        $myevents = json_decode(json_encode($myevents), FALSE);


        ob_start();
        require_once(APOLLO_ADMIN_SYNDICATE_IFRAME_DIR . '/views/landing_page.php');
        $iframe .= ob_get_contents();
        ob_end_clean();

        $spotfeat = $this->get_spotfeat($syndicationMeta);
        $iframe = str_replace('{spotfeat}', $spotfeat, $iframe);

        $options = $this->get_cat_options($this->apID, Apollo_Syndicate_Export_Abstract::getDataSearchFilter('eventType'));
        $iframe = str_replace('{catopts}', $options, $iframe);

        $options = $this->get_ven_options($this->apID,  Apollo_Syndicate_Export_Abstract::getDataSearchFilter('eventVenue'));
        $iframe = str_replace('{venopts}', $options, $iframe);

        $options = $this->get_org_options($this->apID,  Apollo_Syndicate_Export_Abstract::getDataSearchFilter('eventOrg'));
        $iframe = str_replace('{orgopts}', $options, $iframe);

        $iframe = str_replace('{searchres}', '', $iframe);
        $iframe = $this->setsearchcss($iframe);

        echo $iframe;
        exit;


    }

    private function process_detail_page($data, $iframe)
    {

        global $wpdb;

        // Get post
        $id = isset($data['apdid']) ? $data['apdid'] : 0;

        $post_id = $data['apid'];

        $event = $wpdb->get_results("SELECT * FROM $wpdb->posts WHERE post_type = 'event' AND ID = $id");
        $event = $event[0];
        $row = self::get_all_details($data['apdid']);


        if (empty($row['_venue_name'])) {
            $tmp_venue = $wpdb->get_results("SELECT meta_value from $wpdb->apollo_eventmeta where apollo_event_id = $id and meta_key = '_apl_event_tmp_venue'");
            $tmp_venue = $tmp_venue[0];
            $tmp_venue = unserialize(unserialize($tmp_venue->meta_value));
            $row['_venue_name'] = $tmp_venue['_venue_name'];
            $row['_venue_address1'] = $tmp_venue['_venue_address1'];
            $row['_venue_city'] = $tmp_venue['_venue_city'];
            $row['_venue_state'] = $tmp_venue['_venue_state'];
            $row['_venue_zip'] = $tmp_venue['_venue_zip'];
        }


        $buytickets = $row['_admission_ticket_url'];
        $website = $row['_website_url'];
        $email = self::getValByKey($row,'_admission_ticket_email','');


        $url = get_site_url();
        $venurl = $url . "/?" . _APL_SYNDICATED_DATA_HANDLER_FLAG . "&apid=" . $this->apID . "&aplang=eng&apoutput=iframe&apd=ven&apdid=" . $row['_venue_id'];
        $orgurl = $url . "/?" . _APL_SYNDICATED_DATA_HANDLER_FLAG . "&apid=" . $this->apID . "&aplang=eng&apoutput=iframe&apd=org&apdid=" . $row['_org_id'];
        $googleMapAddress = $row['_venue_address1'] . " " . $row['_venue_city'] . ", " . $row['_venue_state'] . " " . $row['_venue_zip'];

        // Set google maps data
        $gmurl = 'http://maps.google.com/maps/api/geocode/json?address=';
        $gmurl .= $googleMapAddress;
        $gmurl .= '&sensor=false';
        $gmurl = str_replace(' ', '+', $gmurl);
        $geocode = file_get_contents($gmurl);
        $output = json_decode($geocode);
        $lat = !empty($output) && !empty($output->results) ? $output->results[0]->geometry->location->lat : '';
        $long = !empty($output) && !empty($output->results) ? $output->results[0]->geometry->location->lng : '';

        $widlower = get_post_meta($this->apID, 'meta-widlower', TRUE);

        ob_start();
        require_once(APOLLO_ADMIN_SYNDICATE_IFRAME_DIR . '/views/detail_page.php');
        $iframe .= ob_get_contents();
        ob_end_clean();

        // Fill in all the variables
        if (isset($baseurl)) {
            $baseurl .= get_bloginfo('template_directory');
        } else {
            $baseurl = get_bloginfo('template_directory');
        }
        $access = '';
        $types = self::getValByKey($row, '_custom_accessibility');
        if (!empty($types) && is_array($types)) {
            foreach ($types as $actype) {
                $access .= "<img title='" . $actype . "' width=30 height=30 src=" . $baseurl . "/assets/images/event-accessibility/" . $actype . ".png>&nbsp;&nbsp;&nbsp;&nbsp;";
            }
        }

        $sep = '';
        $nameven = '';
        if (!empty($row['_venue_name'])) {
            $nameven = $row['_venue_name'];
            $sep = ' at ';
        }
        if (!empty($row['_apl_event_temp_venue'])) {
            $nameven = $row['_apl_event_temp_venue'];
            $sep = ' at ';
        }

        $image = wp_get_attachment_image(get_post_thumbnail_id($id), 'thumbnail');

        $datestimes = $this->get_datetime($id);

        $orgdate = date("m/d/Y", strtotime($row['_apollo_event_start_date']));

        if (Apollo_App::isOngoing(strtotime($row[Apollo_DB_Schema::_APOLLO_EVENT_START_DATE]), strtotime($row[Apollo_DB_Schema::_APOLLO_EVENT_END_DATE]))) {
            $orgdate = "Ongoing";
        } else {
            if ((!empty($row['_apollo_event_end_date'])) AND ($row['_apollo_event_start_date'] != $row['_apollo_event_end_date'])) $orgdate .= " - " . date("m/d/Y", strtotime($row['_apollo_event_end_date']));
        }

        $addressven = $row['_venue_address1'] . "<br>" . $row['_venue_city'] . ", " . $row['_venue_state'] . " " . $row['_venue_zip'];

        if ($row['_venue_address1'] != '') {
            $mapit = $row['_venue_address1'] . "+" . $row['_venue_city'] . "+" . $row['_venue_state'] . "+" . $row['_venue_zip'];
            $mapit = '<a target="_blank" href="https://www.google.com/maps/place/' . $mapit . '">Map it on Google</a>';
        } else {
            $mapit = '';
        }

        if (empty($row['_org_name'])) {
            $tmp_org = $wpdb->get_results("SELECT meta_value from $wpdb->apollo_eventmeta where apollo_event_id = $event->ID and meta_key = '_apl_event_tmp_org'");
            $tmp_org = $tmp_org[0]->meta_value;
            $row['_org_name'] = $tmp_org;
        }

        $adinfo = '';
        $adDetail = self::getValByKey($row,'_admission_detail','');
        $adPhone = self::getValByKey($row,'_admission_phone','');
        if (!empty($adDetail)) {
            $adinfo .= "<h3><span>Admission Info</span></h3><p><strong>Tickets:</strong></p><p>{tickets}</p><br/>";
        }
        if (!empty($adPhone)) {
            $adinfo .= "<p><strong>Phone:</strong> {phone}</p><br/>";
        }
        $iframe =  str_replace('{adinfo}', $adinfo, $iframe);

        $find = array('{mapit}', '{image}', '{nameven}', '{nameorg}', '{title}', '{desc}', '{tickets}', '{phone}', '{email}', '{buytickets}', '{website}', '{orgdate}', '{addressven}', '{sep}', '{lat}', '{long}', '{access}', '{datestimes}');
        $replace = array($mapit, $image, $nameven, $row['_org_name'], $event->post_title, nl2br($event->post_content), $adDetail, $adPhone, $email, $row['_admission_ticket_url'], $row['_website_url'], $orgdate, $addressven, $sep, $lat, $long, $access, $datestimes);
        $iframe = str_replace($find, $replace, $iframe);

        $iframe = $this->setsearchcss($iframe);

        $iframe = str_replace('{searchres}', '', $iframe);
        $options = $this->get_cat_options($post_id, Apollo_Syndicate_Export_Abstract::getDataSearchFilter('eventType'));
        $iframe = str_replace('{catopts}', $options, $iframe);

        $options = $this->get_ven_options($post_id, Apollo_Syndicate_Export_Abstract::getDataSearchFilter('eventVenue'));
        $iframe = str_replace('{venopts}', $options, $iframe);

        $options = $this->get_org_options($post_id, Apollo_Syndicate_Export_Abstract::getDataSearchFilter('eventORg'));
        $iframe = str_replace('{orgopts}', $options, $iframe);

        $keyword = Apollo_Syndicate_Export_Abstract::getDataSearchFilter('eventName');
        if (!empty($keyword)){
            $eventname = ' value="'.$keyword.'"%%';
            $iframe = str_replace(' value=""%%', $eventname, $iframe);
        }

        $startDate = Apollo_Syndicate_Export_Abstract::getDataSearchFilter('startDate');
        if (!empty($startDate)){

            $iframe = str_replace('{sdate}',Apollo_Syndicate_Export_Abstract::getDataSearchFilter('startDate'), $iframe);
            $iframe = str_replace('{edate}', Apollo_Syndicate_Export_Abstract::getDataSearchFilter('endDate'), $iframe);

        } else {
            $formdate = date('m/d/Y');
            $iframe = str_replace('{sdate}', $formdate, $iframe);
            $iframe = str_replace('{edate}', $formdate, $iframe);
        }

        $iframe = str_replace('{spotfeat}', '', $iframe);

        $iframe .= $this->get_analytics($post_id);
        echo $iframe;
        exit;

    }

    private function process_venue_page($data, $iframe)
    {

        global $wpdb;

        // Get post
        $id = $data['apdid'];

        $ven = self::get_venue_details($id);

        $venue = array();
        $venue['name'] = self::getValByKey($ven, '_venue_name');
        $venue['description'] = self::getValByKey($ven, 'post_content');
        $venue['address1'] = self::getValByKey($ven, '_venue_address1');
        $venue['address2'] = self::getValByKey($ven, '_venue_address2');
        $venue['city'] = self::getValByKey($ven, '_venue_city');
        $venue['state'] = self::getValByKey($ven, '_venue_state');
        $venue['zip'] = self::getValByKey($ven, '_venue_zip');
        $long = self::getValByKey($ven, Apollo_DB_Schema::_VENUE_LONGITUDE);
        $lat = self::getValByKey($ven, Apollo_DB_Schema::_VENUE_LATITUDE);
        $venue['parking'] = '';

        $widlower = get_post_meta($this->apID, 'meta-widlower', TRUE);

        ob_start();
        require_once(APOLLO_ADMIN_SYNDICATE_IFRAME_DIR . '/views/venue_page.php');
        $iframe .= ob_get_contents();
        ob_end_clean();

        // Fill in all the variables
        $access = 'Wheelchair';

        $image = wp_get_attachment_image(get_post_thumbnail_id($id), 'thumbnail');
        $row = $ven;
        $sep = isset($sep) ? $sep : '';
        $orgdate = self::getValByKey($ven, '_apollo_event_start_date');
        $orgdate .= " - " . self::getValByKey($ven, '_apollo_event_end_date');
        $addressven = self::getValByKey($ven, '_venue_address1') . "<br>" . self::getValByKey($ven, '_venue_city') . ", " . self::getValByKey($ven, '_venue_state') . " " . self::getValByKey($ven, '_venue_zip');
        $long = self::getValByKey($ven, Apollo_DB_Schema::_VENUE_LONGITUDE);
        $lat = self::getValByKey($ven, Apollo_DB_Schema::_VENUE_LATITUDE);
        $find = array('{image}', '{nameven}', '{nameorg}', '{title}', '{desc}', '{tickets}', '{phone}', '{email}', '{buytickets}', '{website}', '{orgdate}', '{addressven}', '{sep}', '{lat}', '{long}', '{access}');
        $replace = array($image, self::getValByKey($venue, 'post_title'), self::getValByKey($venue, 'post_title'), self::getValByKey($venue, 'post_title'), self::getValByKey($venue, 'post_content'), self::getValByKey($row, '_admission_detail'), self::getValByKey($row, '_admission_phone'), self::getValByKey($row, '_contact_email'), self::getValByKey($row, '_admission_ticket_url'), self::getValByKey($row, '_website_url'), $orgdate, $addressven, $sep, $lat, $long, $access);
        $iframe = str_replace($find, $replace, $iframe);

        echo $iframe;
        exit;

    }

    private function process_org_page($data, $iframe)
    {

        global $wpdb;

        // Get post
        $id = $data['apdid'];

        $org = self::get_org_details($id);

        $org_url = self::getValByKey($org, '_org_website_url');
        $org_email = self::getValByKey($org, '_contact_email');
        $orgname = self::getValByKey($org, 'post_title');
        $org_description = self::getValByKey($org, 'post_content');
        $org_address1 = self::getValByKey($org, '_org_address1');
        $org_city = self::getValByKey($org, '_org_city');
        $org_state = self::getValByKey($org, '_org_state');
        $org_zip = self::getValByKey($org, '_org_zip');
        $org_phone = self::getValByKey($org, '_org_phone');
        $long = self::getValByKey($org, '_apl_oorg_lng');
        $lat = self::getValByKey($org, '_apl_org_lat');
        $media_gallery = "No gallery images exist";

        $widlower = get_post_meta($this->apID, 'meta-widlower', TRUE);

        ob_start();
        require_once(APOLLO_ADMIN_SYNDICATE_IFRAME_DIR . '/views/org_page.php');
        $iframe .= ob_get_contents();
        ob_end_clean();

        // Fill in all the variables
        $access = 'Wheelchair';

        $args = array('post_type' => 'attachment', 'numberposts' => -1, 'post_status' => null, 'post_parent' => $id);
        $attachments = get_posts($args);
        $image = '';
        if ($attachments) {
            foreach ($attachments as $attachment) {
                $image = wp_get_attachment_image($attachment->ID, 'thumbnail');
            }
        }
        $row = $org;
        $sep = isset($sep) ? $sep : '';
        $orgdate = self::getValByKey($org, '_apollo_event_start_date');
        $orgdate .= " - " . self::getValByKey($org, '_apollo_event_end_date');
        $addressven = self::getValByKey($org, '_org_address1') . "<br>" . self::getValByKey($org, '_org_city') . ", " . self::getValByKey($org, '_org_state') . " " . self::getValByKey($org, '_org_zip');
        $long = self::getValByKey($org, '_apl_oorg_lng');
        $lat = self::getValByKey($org, '_apl_org_lat');
        $find = array('{image}', '{nameven}', '{nameorg}', '{title}', '{desc}', '{tickets}', '{phone}', '{email}', '{buytickets}', '{website}', '{orgdate}', '{addressven}', '{sep}', '{lat}', '{long}', '{access}');
        $replace = array($image, self::getValByKey($org, 'post_title'), self::getValByKey($org, 'post_title'), self::getValByKey($org, 'post_title'), self::getValByKey($org, 'post_content'), self::getValByKey($row, '_admission_detail'), self::getValByKey($row, '_admission_phone'), self::getValByKey($row, '_contact_email'), self::getValByKey($row, '_admission_ticket_url'), self::getValByKey($row, '_website_url'), $orgdate, $addressven, $sep, $lat, $long, $access);
        $iframe = str_replace($find, $replace, $iframe);

        echo $iframe;
        exit;

    }

    private function process_css_page($data, $iframe)
    {

        ob_start();
        require_once(APOLLO_ADMIN_SYNDICATE_IFRAME_DIR . '/css/screen.css');
        if (isset($file)) {
            $file .= ob_get_contents();
        } else {
            $file = ob_get_contents();
        }
        ob_end_clean();
        $file = nl2br($file);
        echo $file;
        exit;

    }

    private function process_search_page($data, $iframe)
    {

        $keyword = $_GET['eventName'];

        ob_start();
        require_once(APOLLO_ADMIN_SYNDICATE_IFRAME_DIR . '/views/search_page.php');
        $iframe .= ob_get_contents();
        ob_end_clean();

        echo $iframe;
        exit;
    }

    private function get_datetime($postID)
    {

        global $wpdb;
        $retevent = '';

        $times = $wpdb->get_results("SELECT date_event, time_from FROM $wpdb->apollo_event_calendar where event_id = $postID");

        if (!empty($times)) {
            foreach ($times as $time) {

                if ($time->date_event < Date('Y-m-d', time()) ) continue;


                $mydate = date("M d, Y (D)", strtotime($time->date_event));
                $mytime = (date("g:i a", strtotime($time->time_from)));
                $retevent .= "$mydate at $mytime <br>";
            }
        }

        return $retevent;

    }

    private function get_cat_options($post_id, $id)
    {
        require_once(APOLLO_ADMIN_DIR. '/tools/cache/Inc/Files/Syndication/SyndicationIframeCategory.php');
        $catCacheInstance = new APLC_Inc_Files_Syndication_IframeCategory($this->apID, 'html');
        $catData = $catCacheInstance->get();
        if ($catData) {
            return $catData;
        }
        $event_types = get_post_meta($post_id, 'meta-type', TRUE);

        $args = array('orderby' => 'name', 'order' => 'ASC');
        $types = get_terms('event-type', $args);
        $retval = '';


        if (empty($event_types)){

            foreach ($types as $type) {

                if ($type->term_id == $id){$sel = 'selected';} else {$sel = '';}
                $retval .= "<option value='" . $type->term_id . "' $sel>" . $type->name . "</option>";

                $children = get_term_children($type->term_id, 'event-type');

                foreach ($children as $child) {
                    $name = get_term_by('id', absint($child), 'event-type');
                    if ($child == $id){$sel = 'selected';} else {$sel = '';}
                    $retval .= "<option value='" . $child . "' $sel>- " . $name->name . "</option>";
                }

            }

        } else {

            foreach ($types as $type) {

                if ( !empty($event_types) && is_array($event_types) && in_array($type->term_id, $event_types)) {


                    if ($type->term_id == $id){$sel = 'selected';} else {$sel = '';}
                    $retval .= "<option value='" . $type->term_id . "' $sel>" . $type->name . "</option>";

                    $children = get_term_children($type->term_id, 'event-type');

                    foreach ($children as $child) {
                        $name = get_term_by('id', absint($child), 'event-type');
                        if ($child == $id){$sel = 'selected';} else {$sel = '';}
                        $retval .= "<option value='" . $child . "' $sel>- " . $name->name . "</option>";
                    }
                }

            }

        }
        if ($retval) {
            $catCacheInstance->save($retval);
        }
        return $retval;

    }

    private function get_ven_options($post_id, $id)
    {
        require_once(APOLLO_ADMIN_DIR. '/tools/cache/Inc/Files/Syndication/SyndicationIframeVenue.php');
        $venueCacheInstance = new APLC_Inc_Files_Syndication_IframeVenue($this->apID, 'html');
        $venueData = $venueCacheInstance->get();
        if ($venueData) {
            return $venueData;
        }
        global $wpdb;

        $ven_types = get_post_meta($post_id, 'meta-venues', TRUE);

        $querystr = "SELECT ID, post_title FROM $wpdb->posts WHERE post_type = 'venue' order by post_title";
        $ven_names = $wpdb->get_results($querystr, OBJECT);


        $retval = '';

        if (!empty($ven_types)){

            foreach ($ven_names as $ven){

                if (!empty($ven_types) && is_array($ven_types) && in_array($ven->ID, $ven_types)) {
                    if ($ven->ID == $id){$sel = 'selected';} else {$sel='';}
                    $retval .= "<option value='" . $ven->ID . "' $sel>" . $ven->post_title . "</option>";
                }
            }
        }else{
            $retval =  $this->get_meta_from_event_list($post_id,'meta-venues');
        }
        if ($retval) {
            $venueCacheInstance->save($retval);
        }
        return $retval;
    }

    private function get_org_options($post_id, $id)
    {
        require_once(APOLLO_ADMIN_DIR. '/tools/cache/Inc/Files/Syndication/SyndicationIframeOrg.php');
        $orgCacheInstance = new APLC_Inc_Files_Syndication_IframeOrg($this->apID, 'html');
        $orgData = $orgCacheInstance->get();
        if ($orgData) {
            return $orgData;
        }
        global $wpdb;
        $org_types = get_post_meta($post_id, 'meta-orgs', TRUE);
        $retval = '';
        if (!empty($org_types)){
            $querystr = "SELECT ID, post_title FROM $wpdb->posts WHERE post_type = 'organization' order by post_title";
            $org_names = $wpdb->get_results($querystr, OBJECT);
            foreach ($org_names as $org){

                if (!empty($org_types) && is_array($org_types) && in_array($org->ID, $org_types)) {
                    if ($org->ID == $id) {$sel='selected';} else {$sel='';}
                    $retval .= "<option value='" . $org->ID . "' $sel>" . $org->post_title . "</option>";
                }
            }
        }
        else{
            $retval =  $this->get_meta_from_event_list($post_id,'meta-orgs');
        }
        if ($retval) {
            $orgCacheInstance->save($retval);
        }
        return $retval;

    }

    public function get_meta_from_event_list($post_id, $meta,$isEcho = true){
        $events  = Apollo_Meta_Box_Syndication_Filter::getEventByCurrentSyndSettingsWithPagination(array(
            'syndication_id' => $post_id,
        ));
        $returnArray = array();
        $retval = '';
        $selectedId = '';
        $events = isset($events['eventData'])?$events['eventData']:null;
        if(is_array($events) && count($events) > 0){
            foreach($events as $event){
                $eventObj  = get_event($event);
                $item  = array();
                if($meta ==   'meta-orgs') {
                    $selectedId = isset($_GET['eventOrg'])?$_GET['eventOrg']:'';
                    $org = $eventObj->get_register_org();

                    $orgInfo = get_post($org->org_id);

                    if($orgInfo->post_type == Apollo_DB_Schema::_ORGANIZATION_PT){
                        $item['title'] = $orgInfo->post_title;
                        $item['id'] = $orgInfo->ID;
                    }

                }
                elseif($meta == 'meta-venues' ){
                    $selectedId = isset($_GET['eventVenue'])?$_GET['eventVenue']:'';
                    if($selectedId == ''){
                        $selectedId = isset($_GET['eventCities'])?$_GET['eventCities']:'';
                    }
                    $venue = $eventObj->getVenueEvent();
                    $venue = $venue->get_post_data();


                    //$venueObj  = get_venue($venue);
                    $title = $venue->post_title;
                    //use primary venue
                    if($venue->post_type == Apollo_DB_Schema::_VENUE_PT) {
                        $city = Apollo_App::apollo_get_meta_data($venue->ID,Apollo_DB_Schema::_VENUE_CITY,Apollo_DB_Schema::_APL_VENUE_ADDRESS);
                        $item['title'] = $title.', '.$city;
                        $item['id'] = $venue->ID;
                    }
                    //use tmp venue
                    elseif($venue == ''){
                        $tmpCity = Apollo_App::apollo_get_meta_data($event->ID,Apollo_DB_Schema::_VENUE_CITY,Apollo_DB_Schema::_APL_EVENT_TMP_VENUE);
                        if($tmpCity){
                            $item['title'] = $tmpCity;
                            $item['id'] = $tmpCity;
                        }
                    }

                }
                if(!array_key_exists($item['id'],$returnArray) && $item['title']!= ''){
                    $returnArray[$item['id']] = $item;
                }
            }
        }

        if(!$isEcho ){
            return $returnArray;
        }

        $this->sksort($returnArray,'title',true);
        if (!empty($returnArray)){
            foreach($returnArray as $item){
                $selectedStr = '';
                if($selectedId == $item['id']){
                    $selectedStr = 'selected';
                }
                $retval .= sprintf('<option value=\'%s\' %s  >%s</option>',$item['id'],$selectedStr, $item['title']);
            }

        }
        return $retval;
    }

    function setSearchOptions($syndicationMeta){


        $syndicationMeta['meta-range'][0] = 0;
        if ($_GET['submit'] == 'Search') {
            $syndicationMeta['meta-range'][0] = '0';
            $syndicationMeta['meta-dateStart'][0] = date('Y-m-d', strtotime($_GET['startDate']));
            $syndicationMeta['meta-dateEnd'][0] = date('Y-m-d', strtotime($_GET['endDate']));
        }
        if ($_GET['submit'] == 'All') {
            $syndicationMeta['meta-range'][0] = 720;
            $syndicationMeta['meta-dateStart'][0] = '';
            $syndicationMeta['meta-dateEnd'][0] = '';
        }
        if ($_GET['submit'] == 'Today') {
            $today = current_time("Y-m-d");
            $syndicationMeta['meta-range'][0] = -2;
            $syndicationMeta['meta-dateStart'][0] = $today;
            $syndicationMeta['meta-dateEnd'][0] = $today;
        }
        if ($_GET['submit'] == 'Tomorrow') {
            $tomorrow = date("Y-m-d", strtotime("+1 day"));
            $syndicationMeta['meta-range'][0] = 1;
            $syndicationMeta['meta-dateStart'][0] = $tomorrow;
            $syndicationMeta['meta-dateEnd'][0] = $tomorrow;
        }

        if ($_GET['submit'] == 'Weekend') {
            $now = current_time("Y-m-d");
            $end_date = strtotime("+1 weeks");
            $ds = $de = '';
            while (date("Y-m-d", $now) != date("Y-m-d", $end_date)) {
                $day_index = date("w", $now);
                if ($day_index == 6) {
                    $ds = date("Y-m-d", $now);
                }
                if ($day_index == 0) {
                    $de = date("Y-m-d", $now);
                }
                $now = strtotime(date("Y-m-d", $now) . "+1 day");
            }
            $syndicationMeta['meta-range'][0] = -1000;
            $syndicationMeta['meta-dateStart'][0] = $ds;
            $syndicationMeta['meta-dateEnd'][0] = $de;
        }

        if ($_GET['submit'] == '7') {
            $syndicationMeta['meta-range'][0] = 7;
            $syndicationMeta['meta-dateStart'][0] = '';
            $syndicationMeta['meta-dateEnd'][0] = '';
        }

        if ($_GET['submit'] == '14') {
            $syndicationMeta['meta-range'][0] = 14;
            $syndicationMeta['meta-dateStart'][0] = '';
            $syndicationMeta['meta-dateEnd'][0] = '';
        }

        if ($_GET['submit'] == '30') {
            $syndicationMeta['meta-range'][0] = 30;
            $syndicationMeta['meta-dateStart'][0] = '';
            $syndicationMeta['meta-dateEnd'][0] = '';
        }

        return $syndicationMeta;
    }

    private function getresults(){

        global $wpdb;

        $searchres = '';
        $keyword = Apollo_Syndicate_Export_Abstract::getDataSearchFilter('eventName');

        if (!empty($keyword)){
            $searchres = "&nbsp;&nbsp;&nbsp;Search results for keyword ".$keyword;
        }

        if (!empty($_GET['submit'])){
            if (is_numeric($_GET['submit'])){
                $searchres = "&nbsp;&nbsp;&nbsp;Search results for ".$_GET['submit']." days";
                if (!empty($keyword)){ $searchres .= " - keyword ".$keyword;}

            } else {
                if ($_GET['submit'] == 'All'){
                    $searchres = "&nbsp;&nbsp;&nbsp;Search results for ".$_GET['submit']." days";
                    if (!empty($keyword)){ $searchres .= " - keyword ".$keyword;}
                } else {
                    $searchres = "&nbsp;&nbsp;&nbsp;Search results for ".$_GET['submit'];
                    if (!empty($keyword)){ $searchres .= " - keyword ".$keyword;}
                }
            }

            if ($_GET['submit'] == "This date only"){
                $searchres = "&nbsp;&nbsp;&nbsp;Search results for ".$_GET['submit']." ".date('m-d-Y', strtotime($_GET['startDate']));
                if (!empty($keyword)){ $searchres .= " - keyword ".$keyword;}

            }


            if ($_GET['submit'] == "Search"){
                $searchres = "&nbsp;&nbsp;&nbsp;Search results for dates ".$_GET['startDate']." thru ". $_GET['endDate'];
                if (!empty($_GET['eventType'])){

                    $type = $_GET['eventType'];
                    $querystr = "SELECT name FROM $wpdb->terms WHERE term_id = $type";
                    $type_name = $wpdb->get_results($querystr, OBJECT);
                    $searchres .= " - Category: ".$type_name[0]->name;

                }

                if (!empty($_GET['eventOrg'])){
                    $orgID =  $_GET['eventOrg'];
                    $querystr = "SELECT post_title FROM $wpdb->posts WHERE post_type = 'organization' and ID = $orgID";
                    $org_name = $wpdb->get_results($querystr, OBJECT);
                    $searchres .= " - Organization: ".$org_name[0]->post_title;
                }


                if (!empty($_GET['eventVenue'])){
                    $venueID =  $_GET['eventVenue'];
                    $querystr = "SELECT post_title FROM $wpdb->posts WHERE post_type = 'venue' and ID = $venueID";
                    $ven_name = $wpdb->get_results($querystr, OBJECT);
                    $searchres .= " - Venue: ".$ven_name[0]->post_title;
                }


                if (!empty($keyword)){ $searchres .= " - keyword ".$keyword;}
            }



        }

        return $searchres;

    }

    function setsearchcss($iframe){


        if ( isset($_GET['submit']) && ($_GET['submit'] == 'Search' OR $_GET['submit'] == 'search' ) ){
            $set = array('none','none','inline');
        } else {
            $set = array('inline','inline','none');
        }

        $replace = array('{quick}','{advlink}','{adv}');
        $iframe = str_replace($replace,$set,$iframe);

        return $iframe;


    }

    function get_analytics($post_id){

        $analytics = get_post_meta($post_id, 'meta-analytics', TRUE);
        $retval = "<!-- Google Analytics code goes here --><div style='height: 0px; visibility: hidden;'>$analytics</div><!-- End Google Analytics code -->";
        return $retval;

    }

    function get_spotfeat($prfx_stored_meta){

        $retval = "";

        // Set up template for spotlight
        ob_start();
        require_once(APOLLO_ADMIN_SYNDICATE_IFRAME_DIR . '/views/featured.php');
        $featview = ob_get_contents();
        ob_end_clean();


//if($_SERVER['REMOTE_ADDR'] == '73.252.6.1' ) {

        $spotlight = unserialize($prfx_stored_meta['meta-spot'][0]);
        $featcheck = unserialize($prfx_stored_meta['meta-feat'][0]);
        $featured = unserialize($prfx_stored_meta['meta-pos'][0]);

        if (is_array($featured)) {
            uasort($featured, array($this, 'customSortFeatured'));
        }

        if ($spotlight) {
            foreach ($spotlight as $spot){
                $row = self::get_all_details($spot);
                if ($this->isExpiredTime($spot)) {
                    continue;
                } else {

                    // Set up template for spotlight
                    ob_start();
                    require_once(APOLLO_ADMIN_SYNDICATE_IFRAME_DIR . '/views/spotlight.php');
                    $spotview = ob_get_contents();
                    ob_end_clean();
                    $retval .= $this->fill_spotfeat($spotview, $spot, $prfx_stored_meta, array(150,150));
                    break;
                }
            }
        }


        $limit = 0;
        if ($featured) {
            foreach ($featured as $feat){
                list ($pos, $postid) = explode('_', $feat);
                if ($postid == '') continue;
                if (!in_array($postid, $featcheck)) continue;
                $row = self::get_all_details($postid);
                if ($this->isExpiredTime($postid)) {
                    continue;
                } else {

                    $featviewx = $featview;
                    if ($limit == 0) {$featviewx = str_replace('{feat}', 'Featured', $featviewx); } else {$featviewx = str_replace('{feat}', '', $featviewx);}
                    $retval .= $this->fill_spotfeat($featviewx, $postid, $prfx_stored_meta, array(90, 90), $row );
                    $limit++;
                    if ($limit >= 10) break;
                }
            }
        }



//} // end if

        return $retval."<br><br>";
    }

    public function customSortFeatured($a, $b){
        $aInArray = explode('_',$a);
        $bInArray = explode('_',$b);
        $a = !empty($aInArray) ? intval($aInArray[0]) : 10000;
        $b = !empty($bInArray) ? intval($bInArray[0]) : 10000;
        if ($a == $b) {
            return 0;
        }
        return ($a < $b) ? -1 : 1;
    }

    function fill_spotfeat($view, $post, $prfx_stored_meta, $size){

        global $wpdb;

        $postdata = get_post($post);
        $row = self::get_all_details($post);
        $image = wp_get_attachment_image(get_post_thumbnail_id($post), $size);
        if ($prfx_stored_meta['meta-linkdest'][0] == 'YW'){
            $href = "<a target=_blank href=".$postdata->guid.">";
        } else {
            $href = "<a href=?syndicated_data&apid=".$this->apID."&aplang=eng&apoutput=iframe&apd=det&apdid=".$post.">";

        }


        if (empty($row['_org_name'])) {
            $tmp_org = $wpdb->get_results("SELECT meta_value from $wpdb->apollo_eventmeta where apollo_event_id = $post and meta_key = '_apl_event_tmp_org'");
            $tmp_org = !empty($tmp_org) ? $tmp_org[0]->meta_value : '';
            $row['_org_name'] = $tmp_org;
        }

        if (empty($row['_venue_name'])) {
            $tmp_ven = $wpdb->get_results("SELECT meta_value from $wpdb->apollo_eventmeta where apollo_event_id = $post and meta_key = '_apl_event_tmp_venue'");
            $tmp_ven = unserialize(unserialize($tmp_ven[0]->meta_value));
            $row['_venue_name'] = $tmp_ven['_venue_name'];
        }


        if ($row['_venue_name'] != '') { $sep = '<strong>at</strong>';} else {$sep = '';}

        $find = array('{image}','{title}','{nameorg}','{nameven}','{orgdate}','{desc}', '{href}', '{sep}');
        $replace = array($image, $postdata->post_title, $row['_org_name'], $row['_venue_name'], $orgdate, $postdata->post_content, $href, $sep );
        $view = str_replace($find, $replace, $view);
        return $view;

    }

    public function echoLinkest(){
        if($this->linkest == 'pw'){
            echo '';
        }else{
            echo ' target="_blank" ';
        }
    }

    public  function sksort(&$array, $subkey="id", $sort_ascending=false) {
        $temp_array = array();
        if (count($array))
            $temp_array[key($array)] = array_shift($array);

        foreach($array as $key => $val){
            $offset = 0;
            $found = false;
            foreach($temp_array as $tmp_key => $tmp_val)
            {
                if(!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey]))
                {
                    $temp_array = array_merge(    (array)array_slice($temp_array,0,$offset),
                        array($key => $val),
                        array_slice($temp_array,$offset)
                    );
                    $found = true;
                }
                $offset++;
            }
            if(!$found) $temp_array = array_merge($temp_array, array($key => $val));
        }

        if ($sort_ascending) $array = array_reverse($temp_array);

        else $array = $temp_array;
    }

    /**
     * Get event for iFrame
     *
     * @param array $args
     * @return array or boolean
    */
    public function getEventForIFrame($args = array())
    {
        // get list of event by current settings within its syndication
        $syndicationCachedData = $this->getSyndicationDataCache(false);
        if(empty($syndicationCachedData)){
            // in case it has no cache
            // proceed create cache

            // Store transient cache
            require_once APOLLO_ADMIN_SYNDICATE_DIR. '/class-apollo-syndication-caching-handler.php';
            $handleCaching = new Apollo_Syndicate_Caching_Handler();
            $handleCaching->setSyndicationDataCache($this->apID);

            // get again list id from cache in order to serve for iframe request and searching
            $syndicationCachedData = $this->getSyndicationDataCache(false);
        }

        $eTypes = self::getDataSearchFilter('eventType');
        $eStartDate = self::getDataSearchFilter('startDate');
        $eEndDate =  self::getDataSearchFilter('endDate');
        $dateRange = isset($args['meta-range'][0]) ? $args['meta-range'][0] : 0;
        $filterDate = self::getEventStartDateEndDateForQueryOnIFrameRequest( $dateRange, $eStartDate, $eEndDate);
        $eOrgs =  self::getDataSearchFilter('eventOrg');
        $eVenues = self::getDataSearchFilter('eventVenue');
        $sKeyword = self::getDataSearchFilter('eventName');
        $eCities = self::getDataSearchFilter('eventCities');
        $eFilterByCityOrZip = self::getDataSearchFilter('eFilterByCityOrZip');
        $eSort = self::getValByKey($args,'meta-sort',array('post_title'));
        // prevent updating cache on case iframe request on first time or on searching data on form submission
        $searchData = array(
            'eType' => $eTypes,
            'eStartDate' => $filterDate['start-date'],
            'eEndDate' => $filterDate['end-date'],
            'eOrganizations' => !empty($eOrgs) ? array($eOrgs) : '',
            'eVenues' => !empty($eVenues) ? array($eVenues) : '',
            'eIDs' => $syndicationCachedData,
            'eSort' => !empty($eSort[0]) ? $eSort[0] : 'post_title',
            'sKeyword' => $sKeyword,
            'eCities' => $eCities,
            'eFilterByCityOrZip' => $eFilterByCityOrZip,
        );

        $eResults = $this->getEventsByFilters($searchData);

        return $eResults;
    }

    /**
     * Abstract whether set store transient cache or not
     *
     * @author vulh
     * @return mixed
     */
    protected function setEnableStoreTransientCache()
    {
        return false; // Should not allow store cache on this class, because it caused some reasons regarding the limitation on query so the caching result will miss some events
    }
}