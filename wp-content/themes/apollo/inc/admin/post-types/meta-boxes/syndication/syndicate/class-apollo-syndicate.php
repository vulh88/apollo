<?php
/**
 * User: Thienld
 * Date: 16/12/2015
 * Time: 17:23
 * Description : This file is defined functions to get data for generating email template of syndication.
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

require_once APOLLO_ADMIN_SYNDICATE_DIR . '/class-apollo-syndicate-abstract.php';
require_once APOLLO_TEMPLATES_DIR. '/pages/lib/class-apollo-page-module.php';



$GLOBALS['eSydFiltersParams'] = null;

class Apollo_Syndicate_Export_Handler {

    static $controllers = array();


    public function __construct(){
        $debugContentType = isset($_GET['is_debugging']) ? 'text/html' : 'application/xml';
        $apType = isset($_GET['aptype']) ? $_GET['aptype'] : '';

        if(empty($apType)){
            $apType = isset($_GET['type']) ? $_GET['type'] : '';

        }
        switch ($apType){
            case Apollo_DB_Schema::_ARTIST_PT:
                self::$controllers = array(
                    /*@ticket #18228: 0002510: Artist Directory Plugin > Admin > Syndication - Artist configuration*/
                    'wp' => array(
                        'path' => APOLLO_ADMIN_DIR . '/post-types/meta-boxes/syndication/artist/class-apollo-artist-export-xml.php',
                        'class' => 'Apollo_Artist_Syndicate_Export_XML',
                        'header' => 'Content-Type: application/xml ; charset=utf-8',
                        'type'  => 'xml',
                    ),
                    'artist' => array(
                        'path' => APOLLO_ADMIN_DIR . '/post-types/meta-boxes/syndication/artist/class-apollo-artist-export-xml.php',
                        'class' => 'Apollo_Artist_Syndicate_Export_XML',
                        'header' => 'Content-Type: application/xml ; charset=utf-8',
                        'type'  => 'xml',
                    ),
                    'artist-categories' => array(
                        'path' => APOLLO_ADMIN_DIR . '/post-types/meta-boxes/syndication/artist/class-apollo-artist-export-xml.php',
                        'class' => 'Apollo_Artist_Syndicate_Export_XML',
                        'header' => 'Content-Type: application/xml ; charset=utf-8',
                        'type'  => 'xml',
                    ),
                    'artist-mediums' => array(
                        'path' => APOLLO_ADMIN_DIR . '/post-types/meta-boxes/syndication/artist/class-apollo-artist-export-xml.php',
                        'class' => 'Apollo_Artist_Syndicate_Export_XML',
                        'header' => 'Content-Type: application/xml ; charset=utf-8',
                        'type'  => 'xml',
                    ),
                    'artist-styles' => array(
                        'path' => APOLLO_ADMIN_DIR . '/post-types/meta-boxes/syndication/artist/class-apollo-artist-export-xml.php',
                        'class' => 'Apollo_Artist_Syndicate_Export_XML',
                        'header' => 'Content-Type: application/xml ; charset=utf-8',
                        'type'  => 'xml',
                    ),
                    'artist-cities' => array(
                        'path' => APOLLO_ADMIN_DIR . '/post-types/meta-boxes/syndication/artist/class-apollo-artist-export-xml.php',
                        'class' => 'Apollo_Artist_Syndicate_Export_XML',
                        'header' => 'Content-Type: application/xml ; charset=utf-8',
                        'type'  => 'xml',
                    )

                );
                break;
            case Apollo_DB_Schema::_ORGANIZATION_PT:
                self::$controllers = array(
                    /*@ticket #18228: 0002510: Artist Directory Plugin > Admin > Syndication - Artist configuration*/
                    'wp' => array(
                        'path' => APOLLO_ADMIN_DIR . '/post-types/meta-boxes/syndication/org/class-apollo-org-export-xml.php',
                        'class' => 'Apollo_Org_Syndicate_Export_XML',
                        'header' => 'Content-Type: application/xml ; charset=utf-8',
                        'type'  => 'xml',
                    ),
                    'orgxml' => array(
                        'path' => APOLLO_ADMIN_DIR . '/post-types/meta-boxes/syndication/org/class-apollo-org-export-xml.php',
                        'class' => 'Apollo_Org_Syndicate_Export_XML',
                        'header' => 'Content-Type: application/xml ; charset=utf-8',
                        'type'  => 'xml',
                    ),

                );
                break;
            default:
                self::$controllers = array(
                    'xml' => array(
                        'path' => APOLLO_ADMIN_SYNDICATE_DIR . '/class-apollo-export-xml.php',
                        'class' => 'Apollo_Syndicate_Export_XML',
                        'header' => 'Content-Type: '.$debugContentType.' ; charset=utf-8',
                        'type'  => 'xml',
                    ),
                    'wp' => array(
                        'path' => APOLLO_ADMIN_SYNDICATE_DIR . '/class-apollo-export-wp.php',
                        'class' => 'Apollo_Syndicate_Export_WP',
                        'header' => 'Content-Type: application/xml; charset=utf-8',
                        'type'  => 'xml',
                    ),
                    'rss' => array(
                        'path' => APOLLO_ADMIN_SYNDICATE_DIR . '/class-apollo-export-rss.php',
                        'class' => 'Apollo_Syndicate_Export_RSS',
                        'header' => 'Content-Type: application/xml; charset=utf-8',
                        'type'  => 'xml',
                    ),
                    'mcp' => array(
                        'path' => APOLLO_ADMIN_SYNDICATE_DIR . '/class-apollo-export-mcp.php',
                        'class' => 'Apollo_Syndicate_Export_MCP',
                        'header' => 'Content-Type: application/xml; charset=utf-8',
                        'type'  => 'xml',
                    ),
                    'ical' => array(
                        'path' => APOLLO_ADMIN_SYNDICATE_DIR . '/class-apollo-export-ical.php',
                        'class' => 'Apollo_Syndicate_Export_ICAL',
                        'header' => 'Content-Type: text/text; charset=utf-8',
                        'type'  => 'txt',
                    ),
                    'icalfile' => array(
                        'path' => APOLLO_ADMIN_SYNDICATE_DIR . '/class-apollo-export-ical.php',
                        'class' => 'Apollo_Syndicate_Export_ICAL',
                        'header' => 'Content-Type: application/text; charset=utf-8',
                        'type'  => 'txt',
                    ),
                    'etpl' => array(
                        'path' => APOLLO_ADMIN_SYNDICATE_DIR . '/class-apollo-export-etpl.php',
                        'class' => 'Apollo_Syndicate_Export_ETPL',
                        'header' => 'Content-Type: text/html; charset=utf-8',
                        'type'  => 'html',
                    ),
                    'iframe' => array(
                        'path' => APOLLO_ADMIN_SYNDICATE_DIR . '/class-apollo-handle-iframe-request.php',
                        'class' => 'Apollo_Syndicate_IFrame_Request_Handler',
                        'header' => 'Content-Type: text/html; charset=utf-8'
                    )
                );
        }
        $this->apollo_syndicate_export_handler();
    }

    public function apollo_syndicate_export_handler(){
        try{
            if( isset( $_GET[_APL_SYNDICATED_DATA_HANDLER_FLAG] )) {
                $qs = $_SERVER['QUERY_STRING'];
                $qsArr = Apollo_Page_Module::apolloGetAllQueryStringToArray($qs);
                if(isset($qsArr['apoutput']) && isset(self::$controllers[$qsArr['apoutput']])){
                    $controller = self::$controllers[$qsArr['apoutput']];
                    if(file_exists($controller['path'])){
                        require_once($controller['path']);
                        if(class_exists($controller['class'])){
                            $instance = new $controller['class']();
                            /** @Ticket #13583 */
                            if ( (!isset($qsArr['aptype'])
                                || (isset($qsArr['aptype']) && !in_array($qsArr['aptype'], array(Apollo_DB_Schema::_ARTIST_PT, Apollo_DB_Schema::_ORGANIZATION_PT))
                                ))
                                && !isset($qsArr['type'])) {
                                $instance->extractQueryString();

                                // Check account status
                                $accountStatus = get_post_meta($instance->apID,'meta-status',true);
                                if ($accountStatus == 'OFF') {
                                    wp_safe_redirect('/404');
                                }

                                if($instance->isAccountAvailable()){
                                    if(!empty($controller['header'])){
                                        @header($controller['header']);
                                    }

                                    $instance->handleRequest();
                                }
                            } else {
                                if(!empty($controller['header'])){
                                    @header($controller['header']);
                                }

                                $instance->handleRequest();
                            }
                        }
                    }
                }
                exit;
            }
        }catch (Exception $ex){
            aplDebugFile($ex->getMessage(),'Apollo_Syndicate_Export_Handler:apollo_syndicate_export_handler');
            wp_safe_redirect('/404');
        }
    }

}

new Apollo_Syndicate_Export_Handler();
