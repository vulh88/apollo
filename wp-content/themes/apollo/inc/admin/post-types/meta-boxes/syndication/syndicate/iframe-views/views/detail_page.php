<div id="spotlight_event" class="clearfix">
    <div id="spotlight_event">
        <div id="spotlight_top">
            <div id="spotlight_corner_top_left"></div>
            <div id="spotlight_corner_top_right"></div>
        </div>
        <div id="spotlight_middle">
            <div id="spotlight_left">
                <div id="spotlight_right">
                    <div id="event_image" class="event_image_div" style="float: left; margin-right: 8px; border: 0px;"/>
                    {image}
                </div>
                <h2>
                    <table>
                        <tr>
                            <td>{title}</td>

                            <td>&nbsp;</td>
                            <td>
                            </td>
                        </tr>
                    </table>

                </h2>

                <p class="org_date">
                    Presented by {nameorg} {sep} {nameven}</a>
                    <!-- Presented by <a href="<?php echo $orgurl; ?>">{nameorg}</a> {sep} <a href="<?php echo $venurl; ?>">{nameven}</a-->
                </p>


                <p class="org_date">{orgdate}</p>
                <br/>

                <p>

                <p>{desc}</p>
                </p>
                <br style="clear:both;"/></div>
        </div>
    </div>
    <div id="spotlight_bottom">
        <div id="spotlight_corner_bot_left"></div>
        <div id="spotlight_corner_bot_right"></div>
    </div>
</div>
</div>
<div id="event_info">
    {adinfo}

    <p>
        <?php if ($buytickets != '') : ?>
            <a href="{buytickets}" class="buy_tickets" target="_blank">Buy Tickets</a> |
        <?php endif; ?>

        <?php if ($website != '') : ?>
            <a href="{website}" class="official_web_site" target="_blank">Official Website</a>
        <?php endif; ?>

        <?php if ($email != '') : ?>
            | <a target="_blank" class="email" href="mailto:{email}">Email Contact</a>
        <?php endif; ?>
    </p>
    <br/>


    <h3><span>Upcoming Dates & Times</span></h3>

    <p><strong>Individual Dates &Times:</strong><br>{datestimes}<br><br></p>


    <h3><span>Venue Info</span></h3>

    <p><strong>{nameven}</strong></p>

    <p>

    <p>{addressven}</p>
    <br/>


    {mapit}


    <!--/htdig_noindex-->
    <?php if (!empty($org['supporters'])) : ?>
        <!--htdig_noindex-->
        <div id="sponsors_info">
            <h6><span>Sponsors</span></h6>
            <ul id="sponsors">
                <?php echo  $org['supporters']; ?>
            </ul>
        </div>
    <?php endif; ?>

    <?php if (!empty($media_gallery)): ?>
        <div id="additional_info">
            <!--htdig_noindex-->
            <h3><span>Additional Info</span></h3>
            <?php echo  $media_gallery ?>
            <!--htdig_noindex-->
        </div>
    <?php endif; ?>
</div>
</div>

<!--h3><span>Parking Info</span></h3>
<p><p>Free parking is available in the garage under the Center for Performing Arts. Some street parking is also available.</p></p-->
<div id="accessibility_info">
    <h3 class="htmlwidaccess"><span>Accessibility Info</span></h3>
    {access}
</div>
<p>&nbsp;</p>
<p>----------------------------------------------------</p>
<p><strong>NOTE: </strong>While we do our best to ensure that all information is accurate, we recommend that you visit
    the presenter's website or call the venue to verify this information.</p>
<p>&nbsp;</p>
<div class="advertisement"><?php echo  $widlower; ?></div>

</div>
