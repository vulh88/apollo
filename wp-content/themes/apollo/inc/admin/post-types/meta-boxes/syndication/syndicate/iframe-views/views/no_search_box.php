<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-type" content="text/html;charset=utf-8" />
	<title>Syndication</title>

	{resetcss}
	{screencss}
	{customcss}
	{landingcss}
	{sortable}
	{overridecss}

    {customscript}
	<script type="text/javascript"> jQuery.noConflict(); </script>

</head><body>
<div id="content">
	{image}

	<?php if ($showbanner) : ?>
		<br>
		<?php if (!empty($widupper)) : ?><div id="promos"><?php echo  $widupper; ?></div> <?php endif; ?>
		<?php if (!empty($widdesc)) : ?><div id="widget_description"><?php echo  $widdesc; ?></div> <?php endif;?>
	<?php else : ?>
		<br>
	<?php endif; ?>
	<?php
	$domain = get_site_url();
	$calhome = $domain . "/?"._APL_SYNDICATED_DATA_HANDLER_FLAG."&apid={apid}&aplang=eng&apoutput=iframe";
	?>
	<h4 class="calendarhomelink"><a href="<?php echo  $calhome; ?>">Calendar Home</a></h4><br>
	{spotfeat}
	{paginate}


