<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Apollo_Syndicate_Export_XML extends Apollo_Syndicate_Export_Abstract
{
    public function __construct()
    {
        $this->cacheFile = 'syndication-xml';
        $this->output = 'xml';
        parent::__construct();
    }

    public function handleRequest($echo = true)
    {
        try {
            global $wpdb;

            $syndicationMeta = get_post_meta($this->apID);

            // Get from cache
            if ($cacheContent = $this->getCache($syndicationMeta)) {
                echo $cacheContent;
                return;
            }

            $dataDisplayedEFields = self::getDisplayEventFieldsList($this->apID);
            $dataQueryArgs = $this->dataQueryArgs;
            if (is_array($dataDisplayedEFields) && is_array($dataQueryArgs)) {
                $data = array_merge($dataQueryArgs, $dataDisplayedEFields);
            } else {
                $data = $dataDisplayedEFields;
            }

            $post_id = $this->apID;

            if (empty($syndicationMeta)) {
                _e('Account ID or Output Type is invalid. Process generating is failed.');
                exit;
            }

            if (isset($syndicationMeta['meta-mode']) && $syndicationMeta['meta-mode'][0] === 'YES') {
                $getEventMode = 'individual';
            } else {
                $getEventMode = 'event-type';
            }

            if ($getEventMode === 'individual') {
                $events = $this->getEventsBySelectedIndividualEventIDs($syndicationMeta);
            } else {
                $events = $this->getEventsBySelectedEventType($syndicationMeta);
            }

            $catsList = get_terms('event-type', [
                'orderby' => 'name',
                'order' => 'ASC',
                'hide_empty' => 0,
            ]);

            $category_type = array();
            if(!empty($catsList)){
                foreach ($catsList as $cat){
                    $category_type[$cat->term_id] = $cat->name;
                }
            }

            /** @Ticket #14105 - Get Alternate info */
            $alternateFields = Apollo_Custom_Field::getAlternateFields();

            $xml = '';
            $xml .= "<events>\n";


            /**
             * ThienLD : refactor code to be clearer and handle ticket http://redmine.elidev.info/issues/12655
             */
            $blacklist = self::getMetaDataByKey('meta-blacklist', $syndicationMeta);
            $blacklist = self::getUnSerializeValueForQuery($blacklist);
            $emStartDate = self::getMetaDataByKey('meta-dateStart', $syndicationMeta);
            $emEndDate = self::getMetaDataByKey('meta-dateEnd', $syndicationMeta);
            $emDateRange = self::getMetaDataByKey('meta-range', $syndicationMeta);
            $emFilterDate = self::getEventStartDateEndDateForQuery($emDateRange, $emStartDate, $emEndDate);
            $types = self::getMetaDataByKey('meta-type', $syndicationMeta);
            $types = self::getUnSerializeValueForQuery($types);

            if (!empty($events)) {
                foreach ($events as $eventID) {

                    if (!empty($eventID->ID)) {
                        $eventID = $eventID->ID;
                    }

                    /**
                     * ThienLD : handle issue in ticket #12654 and refactor code to be easier maintain in the future
                     */

                    if ($this->isExpiredTime($eventID)) continue;

                    $row = self::get_all_details($eventID, array(
                        'img_size'   => 'large',
                        'org_img'    => true
                    ));
                    if (empty($row)) {
                        continue;
                    }

                    $eventType = self::getValByKey($row,'_apl_event_term_primary_id');

                    if ($eventType == '') continue;

                    if ( self::isExistedInEventBlacklistType($row, $blacklist) ) continue;

                    if ( ! self::isExistedInEventType($row, $types, $syndicationMeta)) continue;

                    /**
                     * ThienLD : [END] - handle issue in ticket #12654 and refactor code to be easier maintain in the future
                     */

                    $xml .= "<event>\n";

                    $eventUrl = get_permalink($eventID);
                    $webUrl = !empty($row[Apollo_DB_Schema::_WEBSITE_URL]) ? $row[Apollo_DB_Schema::_WEBSITE_URL] : '';
                    $startDate = date("m-d-Y", strtotime($row['_apollo_event_start_date']));
                    $endDate = date("m-d-Y", strtotime($row['_apollo_event_end_date']));

                    $time = time();
                    $stime = strtotime($row['_apollo_event_start_date']);
                    $etime = strtotime($row['_apollo_event_end_date']);

                    $newDate = strtotime('+3 month', $time);

                    if ($etime >= $newDate) {
                        $ongoing = 'Y';
                    } else {
                        $ongoing = 'N';
                    }

                    $ticketinfo = strip_tags(self::getValByKey($row,'_admission_detail'));
                    $starttime = self::getValByKey($row,'_apl_event_additional_time');

                    $eventDaysList = array();

                    //
                    // Here we determine the days of the week from the event calendar
                    //

                    $sql = "SELECT date_event FROM " . $wpdb->prefix . "apollo_event_calendar where event_id = $eventID";
                    $edays = $wpdb->get_results($sql);

                    foreach ($edays as $eday) {
                        $dayname = date("l", strtotime($eday->date_event));
                        $eventDaysList[$dayname] = $dayname;
                        if (count($eventDaysList) > 6) break;
                    }


                    $eventDays = '';
                    foreach ($eventDaysList as $day) {
                        $eventDays .= $day . ",";
                    }


                    $eventDays = substr($eventDays, 0, -1);

                    $eventimage = $row['image'];

                    $xml .= "<eventID><![CDATA[" . $eventID . "]]></eventID>\n";

                    $feat_spot = $wpdb->get_results("SELECT _is_category_spotlight, _is_category_feature from $wpdb->apollo_post_term where post_id = $eventID");

                    if ($feat_spot && $feat_spot[0]->_is_category_spotlight == 'yes') {
                        $spotlight = 'true';
                    } else {
                        $spotlight = 'false';
                    }
                    if ($feat_spot && $feat_spot[0]->_is_category_feature == 'yes') {
                        $featured = 'true';
                    } else {
                        $featured = 'false';
                    }


                    if (isset($data['spotlight']) && $data['spotlight']) $xml .= "<spotlight><![CDATA[" . $spotlight . "]]></spotlight>\n";
                    if (isset($data['featured']) && $data['featured']) $xml .= "<featured><![CDATA[" . $featured . "]]></featured>\n";

                    $orgimage = isset($row['org_img']) ? $row['org_img'] : '';
                    $xml .= "<orgImage><![CDATA[ $orgimage ]]></orgImage>\n";

                    $xml .= "<eventImage><![CDATA[" . $eventimage . "]]></eventImage>\n";
                    $xml .= "<eventName><![CDATA[" . $row['post']->post_title . "]]></eventName>\n";
                    $xml .= "<eventDateBegin><![CDATA[" . $startDate . "]]></eventDateBegin>\n";
                    $xml .= "<eventDateEnd><![CDATA[" . $endDate . "]]></eventDateEnd>\n";
                    $xml .= "<eventDays><![CDATA[" . $eventDays . "]]></eventDays>\n";
                    $xml .= "<ongoing><![CDATA[ " . $ongoing . " ]]></ongoing>\n";


                    if ($data['eventDatesTimes']) {
                        $dates_times = $this->get_dates_times($eventID,self::getValByKey($emFilterDate,'end-date',''));

                        $xml .= "<eventDatesTimes>\n";
                        //$xml .= "<datetime>";
                        $xml .= $dates_times;
                        //$xml .= "</datetime>";
                        $xml .= "</eventDatesTimes>\n";
                    }


                    //$xml .= "<eventType><![CDATA[" . $category_type[$eventType] . "]]></eventType>";
                    $source = $_SERVER['SERVER_NAME'];

                    $xml .= "<orgID><![CDATA[" . $row['_org_id'] . "]]></orgID>\n";

                    if ($data['orgName']) {

                        if (empty($row['_org_name'])) {
                            $this->getOrgTmpByEventId($row, $eventID);
                        }

                        $xml .= "<orgName><![CDATA[" . $row['_org_name'] . "]]></orgName>\n";
                    }

                    $xml .= "<eventUrl><![CDATA[ $eventUrl ]]></eventUrl>\n";
					$xml .= "<webUrl><![CDATA[ $webUrl ]]></webUrl>\n";

                    if ($data['venue']) {
                        if (empty($row['_venue_name'])) {
                            $this->getVenueTmpByEventId($row,$eventID);
                        }

                        /** @Ticket #13926 */
                        $venue_neighborhood_name = '';
                        if (!empty($row[Apollo_DB_Schema::_VENUE_NEIGHBORHOOD])) {
                            $venue_neighborhood_name = APL_Lib_Territory_Neighborhood::getNameById($row[Apollo_DB_Schema::_VENUE_NEIGHBORHOOD]);
                        }



                        $eVenueID = isset($row['_venue_id']) ? $row['_venue_id'] : '';
                        $xml .= "<venueID><![CDATA[" . $eVenueID. "]]></venueID>\n";

                        $xml .= "<venueName><![CDATA[" . $row['_venue_name'] . "]]></venueName>\n";
                        $xml .= "<venueAddress1><![CDATA[" . $row['_venue_address1'] . "]]></venueAddress1>\n";
                        $xml .= "<venueAddress2><![CDATA[" . $row['_venue_address2'] . "]]></venueAddress2>\n";
                        $xml .= "<venueCity><![CDATA[" . $row['_venue_city'] . "]]></venueCity>\n";
                        $xml .= "<venueState><![CDATA[" . $row['_venue_state'] . "]]></venueState>\n";
                        $xml .= "<venueZip><![CDATA[" . $row['_venue_zip'] . "]]></venueZip>\n";
                        $xml .= "<venueLatitude><![CDATA[" . self::getValByKey($row, Apollo_DB_Schema::_VENUE_LATITUDE) . "]]></venueLatitude>\n";
                        $xml .= "<venueLongitude><![CDATA[" . self::getValByKey($row, Apollo_DB_Schema::_VENUE_LONGITUDE) . "]]></venueLongitude>\n";

                        $xml .= "<venueNeighborhood><![CDATA[" . $venue_neighborhood_name . "]]></venueNeighborhood>\n";
                        $xml .= "<venueParking><![CDATA[" . self::getValByKey($row, Apollo_DB_Schema::_VENUE_PARKING_INFO) . "]]></venueParking>\n";


                    }

                    $startt = $wpdb->get_results("SELECT time_from FROM $wpdb->apollo_event_calendar where event_id = $eventID limit 1");
                    $startt = $startt ? date("g:i a", strtotime($startt[0]->time_from)) : '';

                    $secondary = '';
                    $secondary_types = $row['secondary_types'];
                    if (is_array($secondary_types)) {
                        foreach ($secondary_types as $secondary_type) {
                            if ($secondary_type == $eventType) continue;
                            if (is_array($blacklist) && in_array($secondary_type, $blacklist)) continue;
                            if (isset($category_type[$secondary_type])) {
                                $secondary .= $category_type[$secondary_type] . ", ";
                            }
                        }
                        $secondary = substr($secondary, 0, -2);
                    }


                    if (isset($data['eventTicketInfo']) && $data['eventTicketInfo']) $xml .= "<eventTicketInfo><![CDATA[ $ticketinfo ]]></eventTicketInfo>\n";
                    if (isset($data['eventPhone1']) && $data['eventPhone1']) $xml .= "<eventPhone1><![CDATA[" . self::getValByKey($row, '_admission_phone') . "]]></eventPhone1>\n";
                    if (isset($data['eventEmail']) && $data['eventEmail']) $xml .= "<eventEmail><![CDATA[" . self::getValByKey($row, '_admission_ticket_email') . "]]></eventEmail>\n";
                    if (isset($data['eventTicketUrl']) && $data['eventTicketUrl']) $xml .= "<eventTicketUrl><![CDATA[" . self::getValByKey($row, '_admission_ticket_url') . "]]></eventTicketUrl>\n";
                    if (isset($data['discountUrl']) && $data['discountUrl']) $xml .= "<discountUrl><![CDATA[" . self::getValByKey($row, '_admission_discount_url') . "]]></discountUrl>\n";
                    if (isset($data['eventStartTime']) && $data['eventStartTime']) $xml .= "<eventStartTime><![CDATA[" . $starttime . "]]></eventStartTime>\n";

                    // moved here as per request from Jeff
                    $xml .= "<eventType><![CDATA[" . $category_type[$eventType] . "]]></eventType>\n";

                    if (isset($data['secondaryType']) && $data['secondaryType']) $xml .= "<secondaryType><![CDATA[" . $secondary . "]]></secondaryType>\n";
                    $xml .= "<eventSummary><![CDATA[" . $row['post']->post_excerpt . "]]></eventSummary>\n";
                    if ($data['eventDescription']) $xml .= "<eventDescription><![CDATA[" . $row['post']->post_content . "]]></eventDescription>\n";


                    if (isset($data['datePosted']) && $data['datePosted']) $xml .= "<datePosted><![CDATA[" . $row['post']->post_date . "]]></datePosted>\n";
                    if (isset($data['eventLink']) && $data['eventLink']) $xml .= "<link><![CDATA[ $eventUrl ]]></link>\n";
                    if (isset($data['eventGuid']) && $data['eventGuid']) $xml .= "<guid><![CDATA[ $eventUrl ]]></guid>\n";

                    $xml .= "<source><![CDATA[$source]]></source>\n";

                    /** @Ticket #14105 - Alternate fields */
                    if ($alternateFields) {
                        $xml .= $this->renderAlternateFields($eventID, $alternateFields);
                    }

                    $xml .= "</event>\n";
                }
            }

            $xml .= "</events>\n";

            if (!$echo) {
                return array(
                    'string'	=> $xml
                );
            }

            echo trim($xml);

            // Store cache
            $this->addCache($xml);

        } catch (Exception $ex) {

            if (!$echo) {
                return array(
                    'string'	=> ''
                );
            }

            aplDebugFile($ex->getMessage(), 'Syndicate export with type as XML');
            wp_safe_redirect('/404');
        }
    }

    private function get_dates_times($postID, $syndicationEndDate = '', $offset = false, $limit = false) {
        $event = $this->getEventCalendar([
            'postID' => $postID,
            'offset' => $offset,
            'limit' => $limit,
            'endDate' => $syndicationEndDate
        ]);
        $retevent = '';
        if ($event && count($event) > 0) {

            foreach ($event as $evt) {
                $item = $this->outputExportResultItem($postID,$evt);
                $mydate = isset($item['date'])?$item['date']:'';
                $mytime = isset($item['time'])?$item['time']:'';
                $unix_time = isset($item['timestamp'])?$item['timestamp']:'';
                $date_filter = isset($item['date_filter'])?$item['date_filter']:'';
                $myend =  isset($item['end_time'])?$item['end_time']:'';
                $duration = isset($item['duration'])?$item['duration']:'';
                $retevent .= "<datetime>\n<date>" . $mydate . "</date><time>" . $mytime . "</time>\n<timestamp>" . $unix_time . "</timestamp>\n<date_filter>" . $date_filter . "</date_filter>\n<end_time>" . $myend . "</end_time>\n<duration>" . $duration . "</duration>\n</datetime>\n";
            } // end foreach
        } else {
            $retevent = "<datetime>\n<date>\n</date>\n<time>\n</time>\n<timestamp>\n</timestamp>\n<date_filter>\n</date_filter>\n<duration>\n</duration>\n</datetime>\n";
        }
        return $retevent;
    }

    /**
     * Abstract whether set store transient cache or not
     * @author vulh
     * @return boolean
     */
    protected function setEnableStoreTransientCache()
    {
        return true;
    }
}