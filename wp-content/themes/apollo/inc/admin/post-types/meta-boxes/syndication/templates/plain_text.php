<?php
/**
 * User: Thienld
 * Date: 16/12/2015
 * Time: 17:39
 * Description: Rendering email content as plain text
 */
$primary_category_separated = isset($template_args['primary_category_separated']) ? $template_args['primary_category_separated'] : false;
$event_data = isset($template_args['event_data']) ? $template_args['event_data'] : array();
$syndication_meta_data = isset($template_args['syndication_meta_data']) ? $template_args['syndication_meta_data'] : array();
$display_fields = Apollo_Syndicate_Export_Abstract::getMetaDataByKey('meta-ownsel',$syndication_meta_data,'');
$display_fields = empty($display_fields) ? array() : maybe_unserialize(maybe_unserialize($display_fields));
$plainText = "";
?>
<?php if($primary_category_separated) : ?>
    <?php foreach($event_data as $pCateID => $listEvent) : ?>
        <?php
        $priCateItem = get_term($pCateID,'event-type');
        $termName = !empty($priCateItem) ? $priCateItem->name : '';
        ?>
        <?php
            $displayTermName = "". strtoupper(trim($termName));
            $plainText .= trim($displayTermName)  . "\n";
        ?>
        <?php foreach($listEvent as $eID) : ?>
        <?php $plainText .= Apollo_App::getTemplatePartCustom( APOLLO_ADMIN_SYNDICATION_TEMPLATE_DIR . '/pt_event_item.php',array('event_id'=>$eID,
            'display_fields' => $display_fields,
            'objectClass' => $template_args['objectClass'],
        )); ?>
        <?php endforeach; ?>
        <?php $plainText .= "\n"; ?>
    <?php endforeach; ?>
<?php else : ?>
    <?php foreach($event_data as $e) :
        $e = is_object($e) ? get_object_vars($e) : $e;
        $eventID = $e['ID'];

        if(!Apollo_Syndicate_Export_Abstract::hasSetEventPrimaryCategory($eventID)) { continue; }
        $plainText .= Apollo_App::getTemplatePartCustom( APOLLO_ADMIN_SYNDICATION_TEMPLATE_DIR . '/pt_event_item.php',array('event_id'=>$eventID,
            'display_fields' => $display_fields,
            'objectClass' => $template_args['objectClass'],
        )); ?>
    <?php endforeach; ?>
<?php endif; ?>
<?php echo trim($plainText); ?>
