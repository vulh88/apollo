<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class Apollo_Artist_Syndicate_Export_XML
{

    private $cacheFactory;

    public function __construct()
    {
        $this->cacheFactory = aplc_instance('APLC_Inc_Files_Syndication_Artist_CacheFactory');
    }

    public function handleRequest($echo = true)
    {
        $output = isset($_GET['apoutput']) ? $_GET['apoutput'] : '';
        try {
            aplc_require_once(APLC. '/Inc/Files/Syndication/Artist/CacheFactory.php');
            switch ($output){
                case APLC_Inc_Files_Syndication_Artist_CacheFactory::CATEGORY:
                    $xml = $this->categoriesXML('artist-type', 'type', $output);
                    break;
                case APLC_Inc_Files_Syndication_Artist_CacheFactory::MEDIUM:
                    $xml = $this->categoriesXML('artist-medium', 'medium', $output);
                    break;
                case APLC_Inc_Files_Syndication_Artist_CacheFactory::STYLE:
                    $xml = $this->categoriesXML('artist-style', 'style', $output);
                    break;
                case APLC_Inc_Files_Syndication_Artist_CacheFactory::CITY:
                    $xml = $this->getTerritory();
                    break;
                case APLC_Inc_Files_Syndication_Artist_CacheFactory::ARTIST:
                default:
                    $xml = $this->artistXML();
            }

            if (!$echo) {
                return array(
                    'string'	=> $xml
                );
            }

            echo trim($xml);

        } catch (Exception $ex) {

            if (!$echo) {
                return array(
                    'string'	=> ''
                );
            }

            aplDebugFile($ex->getMessage(), 'Syndicate export with type as XML');
            wp_safe_redirect('/404');
        }
    }

    public function artistXML(){

        $apid = isset($_GET['apid']) ? $_GET['apid'] : '';
        if (!empty($apid)) {
            $isCaching = get_post_meta($apid, APL_Syndication_Const::_SA_META_ARTIST_ENABLE_CACHE, true);
        }
        else {
            $isCaching = get_option(APL_Syndication_Const::_SYNDICATION_ARTIST_ENABLE_CACHE, false);
        }
        if ($isCaching) {
            $xml = $this->cacheFactory->get(APLC_Inc_Files_Syndication_Artist_CacheFactory::ARTIST, $apid);
            if (!empty($xml)) {
                return $xml;
            }
        }

        $xml = "<artists>\n";

        $allArtist= $this->getArtist($apid);
        $enableStyle = of_get_option(APL_Theme_Option_Site_Config_SubTab::ARTIST_ENABLE_STYLE_SEARCH_WIDGET,1);
        $enableMedium = of_get_option(APL_Theme_Option_Site_Config_SubTab::ARTIST_ENABLE_MEDIUM_SEARCH_WIDGET, 1);
        if (!empty($allArtist)) {
            foreach ($allArtist as $artist) {


                if ($artist instanceof Apollo_Artist) {}

                $artistModel = get_artist($artist->ID);
                if (!$this->checkDisplayControl($artistModel)) {
                    continue;
                }
                $address = Apollo_App::unserialize(get_apollo_meta($artist->ID, Apollo_DB_Schema::_APL_ARTIST_ADDRESS, true));
                $artistData = Apollo_App::unserialize(get_apollo_meta($artist->ID, Apollo_DB_Schema::_APL_ARTIST_DATA, true));

                //artist Data
                $displayPhone = $this->getArtistData($artistData,Apollo_DB_Schema::_APL_ARTIST_DISPLAY_PHONE);
                $displayEmail = $this->getArtistData($artistData,Apollo_DB_Schema::_APL_ARTIST_DISPLAY_EMAIL);
                $displayAddress = $this->getArtistData($artistData,Apollo_DB_Schema::_APL_ARTIST_DISPLAY_ADDRESS);
                $summary = $artistModel->get_summary();

                $styleXML = '';
                $anotherStyle = '';
                $styleIds = '';
                $mediumXML = '';
                $mediumIds = '';
                $anotherMedium = '';

                if($enableStyle){
                    $style = $this->getCategories($artist->ID, 'artist-style', 'artistStyle');
                    $styleXML = $style['xml'];
                    $styleIds = !empty( $style['ids']) ? implode(', ',  $style['ids']) : '';
                    $anotherStyle = $this->getArtistData($artistData,Apollo_DB_Schema::_APL_ARTIST_ANOTHER_STYLE);
                }

                if($enableMedium){
                    $medium = $this->getCategories($artist->ID, 'artist-medium', 'artistMedium');
                    $mediumXML = $medium['xml'];
                    $mediumIds = !empty( $medium['ids']) ? implode(', ',  $medium['ids']) : '';
                    $anotherMedium = $this->getArtistData($artistData,Apollo_DB_Schema::_APL_ARTIST_ANOTHER_MEDIUM);
                }

                //videos, audio, galleries
                $videos =  $this->getArtistData($artistData, Apollo_DB_Schema::_APL_ARTIST_VIDEO);
                $audios =  $artistModel->getListAudios(Apollo_DB_Schema::_APL_ARTIST_AUDIO);

                $categories = $this->getCategories($artist->ID, 'artist-type', 'artistType');
                $categoriesXML = $categories['xml'];


                /*@ticket #18310: 0002510: Artist Directory Plugin > New Artist option*/
                $xmlElement = array(
                    "ID" => $artistModel->post->ID,
                    "name" => $artistModel->get_title($artistData),
                    "image_thumbnail" =>  $artistModel->get_image(),
                    "image_medium" =>  $artistModel->get_image('medium'),
                    "image_large" =>  $artistModel->get_image('large'),
                    "summary" => isset($summary['text']) ? $summary['text'] : '',
                    "description" => $artistModel->get_full_content(),
                    "link" => get_the_permalink($artistModel->post->ID),
                    "firstName" => $this->getArtistData($artistData, Apollo_DB_Schema::_APL_ARTIST_FNAME),
                    "lastName" => $this->getArtistData($artistData,Apollo_DB_Schema::_APL_ARTIST_LNAME),
                    "phone" => $displayPhone != 'yes' ? $this->getArtistData($artistData,Apollo_DB_Schema::_APL_ARTIST_PHONE) : '',
                    "email" => $displayEmail != 'yes' ? $this->getArtistData($artistData,Apollo_DB_Schema::_APL_ARTIST_EMAIL) : '',
                    "blogURL" => $this->getArtistData($artistData,Apollo_DB_Schema::_APL_ARTIST_BLOGURL),
                    "websiteURL" => $this->getArtistData($artistData,Apollo_DB_Schema::_APL_ARTIST_WEBURL),
                    "street" => $this->getArtistData($address,Apollo_DB_Schema::_APL_ARTIST_STREET),
                    "state" => $this->getArtistData($address, Apollo_DB_Schema::_APL_ARTIST_STATE),
                    "city" => $this->getArtistData($address, Apollo_DB_Schema::_APL_ARTIST_CITY),
                    "zip" => $this->getArtistData($address, Apollo_DB_Schema::_APL_ARTIST_ZIP),
                    "country" => $this->getArtistData($address,Apollo_DB_Schema::_APL_ARTIST_COUNTRY),
                    "region" => $this->getArtistData($address,Apollo_DB_Schema::_APL_ARTIST_REGION),
                    "fullAddress" => $displayAddress != 'yes' ? $artistModel->get_full_address() : '',
                    "facebookURL" => $this->getArtistData($artistData,Apollo_DB_Schema::_APL_ARTIST_FB),
                    "linkedinURL" => $this->getArtistData($artistData,Apollo_DB_Schema::_APL_ARTIST_TW),
                    "twitterURL" => $this->getArtistData($artistData,Apollo_DB_Schema::_APL_ARTIST_LK),
                    "pinterestURL" => $this->getArtistData($artistData,Apollo_DB_Schema::_APL_ARTIST_PR),
                    "instagramURL" => $this->getArtistData($artistData,Apollo_DB_Schema::_APL_ARTIST_INS),
                    "categories" => $categoriesXML,
                    "otherCategories" => $this->getArtistData($artistData,Apollo_DB_Schema::_APL_ARTIST_ANOTHER_CAT),
                    "categoryIDs" => !empty( $categories['ids']) ? implode(', ',  $categories['ids']) : '',
                    "styles" => $styleXML,
                    "otherStyles" => $anotherStyle,
                    "styleIDs" => $styleIds,
                    "mediums" => $mediumXML,
                    "otherMediums" => $anotherMedium,
                    "mediumIDs" => $mediumIds,
                    "galleries" =>  $this->getGalleries($artist->ID),
                    "audios" => $this->getListMedia($audios, 'audio'),
                    "videos" => $this->getListMedia($videos, 'artistVideo'),
                    "documents" => $this->getDocuments($artist->ID),
                    "publicArts" => $this->getPublicArts($artistModel),
                    "questions" => $this->getQuestions($artist->ID),
//                    "additionalFields" => $this->getAdditionalFields($artistModel),
                    "isNew" => get_apollo_meta($artist->ID, Apollo_DB_Schema::_ARTIST_IS_NEW, true) != 'yes' ? 'No' : 'Yes'
                );

                $xml .= "<artist>\n";

                foreach ($xmlElement as $tag => $data){

                    if(empty($data)){
                        continue;
                    }

                    if(in_array($tag, array("documents", "audios", "videos",
                        "publicArts", "questions", "additionalFields",
                        "categories", "styles", "mediums", "galleries"
                    ))){
                        $xml .= "<$tag>$data</$tag>\n";
                        continue;
                    }

                    $xml .= "<$tag><![CDATA[$data]]></$tag>\n";
                }

                $xml .= "</artist>\n";
            }
        }

        $xml .= "</artists>\n";
        if ($isCaching){
            $this->cacheFactory->save(APLC_Inc_Files_Syndication_Artist_CacheFactory::ARTIST, $xml, $apid);
        }

        return $xml;

    }

    private function categoriesXML($type, $typeName, $output){

        if ($xml = $this->cacheFactory->get($output)) {
            return $xml;
        }

        $terms = get_terms( $type, array('hide_empty' => false ));

        $xml = "<$typeName" ."s>\n";

        if (!empty($terms)) {

            foreach ($terms as $cat) {
                $xml .= "<$typeName>\n";
                $xml .= "<id><![CDATA[$cat->term_id]]></id>\n";
                $xml .= "<name><![CDATA[$cat->name]]></name>\n";
                $xml .= "<parentId><![CDATA[$cat->parent]]></parentId>\n";
                $xml .= "</$typeName>\n";
            }
        }

        $xml .= "</$typeName" ."s>\n";

        $this->cacheFactory->save($output, $xml);

        return $xml;
    }

    private function getTerritory() {

        if ($xml = $this->cacheFactory->get(APLC_Inc_Files_Syndication_Artist_CacheFactory::CITY)) {
            return $xml;
        }

        $cities = Apollo_App::getCityByTerritory(false, '', false);
        $xml = "<cities>\n";
        if(!empty($cities)) {
            foreach ($cities as $code => $name) {
                $xml .= "<city>\n";
                $xml .= "<code><![CDATA[$code]]></code>\n";
                $xml .= "<name><![CDATA[$name]]></name>\n";
                $xml .= "</city>\n";
            }
        }
        $xml .= "</cities>\n";

        $this->cacheFactory->save(APLC_Inc_Files_Syndication_Artist_CacheFactory::CITY, $xml);

        return $xml;
    }

    /**
     * @param $postID
     * @return array|null|object
     */
    private function getArtist($postID) {
        global $wpdb;
        if (!empty($postID)) {
            $select_all = get_post_meta($postID, APL_Syndication_Const::_SA_META_ARTIST_ALL, true);
            $limit = get_post_meta($postID, APL_Syndication_Const::_SA_META_ARTIST_LIMIT, true); //1000
            $orderby = get_post_meta($postID, APL_Syndication_Const::_SA_META_ARTIST_ORDER_BY, true);// 'post_title'
            $orderby = $orderby ? $orderby : 'post_title';
            $order = get_post_meta($postID, APL_Syndication_Const::_SA_META_ARTIST_ORDER, true);// 'asc'
            $order = $order ? $order : 'asc';
        } else {
            $select_all = !(get_option(APL_Syndication_Const::_SYNDICATION_ALL_ARTIST, 0));
            $limit = get_option(APL_Syndication_Const::_SYNDICATION_ARTIST_LIMIT, 1000);
            $orderby = get_option(APL_Syndication_Const::_SYNDICATION_ARTIST_ORDERBY, 'post_title');
            $order = get_option(APL_Syndication_Const::_SYNDICATION_ARTIST_ORDER, 'asc');
        }

        $sql = "SELECT ".$wpdb->posts.".ID 
                FROM " . $wpdb->posts ." WHERE post_type='". Apollo_DB_Schema::_ARTIST_PT ."' AND post_status='publish'";
        if (!$select_all) {
            /** Make sure old URL still working fine */
            if (!empty($postID)) {
                $artistIds = get_post_meta($postID, APL_Syndication_Const::_SA_META_ARTIST_SELECTED, true);
            } else {
                $artistIds = get_option(APL_Syndication_Const::_SYNDICATION_SELECTED_ARTIST, '');
            }
            $sql .= " AND ID IN( " . (!empty($artistIds) ? $artistIds : '-1') . " )";
        }
        $sql .= " ORDER BY ". $orderby . " " . $order . " LIMIT " . $limit;

        return $wpdb->get_results($sql);

    }


    /**
     * @param $artistId
     * @param $type
     * @return array
     */
    private function getCategories($artistId, $type, $typeName) {
        $all_cats = get_the_terms( $artistId , $type );
        $xml = '';
        $categoriesIds = array();

        if (!empty($all_cats)) {

            foreach ($all_cats as $cat) {

                if(!empty($cat)){
                    $xml .= "<$typeName>\n";
                    $link = get_term_link($cat->term_id);
                    $xml .= "<id><![CDATA[$cat->term_id]]></id>\n";
                    $xml .= "<name><![CDATA[$cat->name]]></name>\n";
                    $xml .= "<link><![CDATA[$link]]></link>\n";
                    $xml .= "</$typeName>\n";

                    $categoriesIds[] = $cat->term_id;
                }
            }
        }

        return array("xml" => $xml, "ids"=> $categoriesIds);
    }

    /**
     * @param $data
     * @param $key
     * @return string
     */
    private function getArtistData($data, $key){
        return isset($data[$key]) ? $data[$key] : '';
    }

    /**
     * @param $artistId
     * @return string
     */
    private function getDocuments($artistId){
        $upload_pdf = Apollo_App::unserialize(get_apollo_meta($artistId, 'upload_pdf', true));
        $xml = '';
        if ($upload_pdf) {
            foreach($upload_pdf as $attachment_id) {
                $attachment = get_post($attachment_id);
                if(!empty($attachment)){
                    $link = $attachment->guid;
                    $name = !empty($attachment->post_excerpt) ? $attachment->post_excerpt : substr($attachment->guid, strrpos($attachment->guid, '/') + 1, -4);
                    $xml .= "<document>\n";
                    $xml .= "<documentLink><![CDATA[$link]]></documentLink>\n";
                    $xml .= "<documentName><![CDATA[$name]]></documentName>\n";
                    $xml .= "</document>\n";
                }
            }
        }

        return $xml;
    }

    /**
     * @param $medias: video, audio
     * @param $tabName
     * @return string
     */
    private function getListMedia($medias, $tabName){
        $xml = "";
        if(!empty($medias)){
            foreach($medias as $media) {
                $xml .= "<$tabName>\n";
                if($tabName == 'audio'){
                    $link = isset($media["link"]) ? $media["link"] : '';
                    $type = isset($media["type"]) ? $media["type"] : '';
                    $xml .= "<type><![CDATA[$type]]></type>\n";
                }
                else{
                    $link = isset($media["embed"]) ? $media["embed"] : '';
                }

                $desc = isset($media["desc"]) ? $media["desc"] : '';
                $xml .= "<link><![CDATA[$link]]></link>\n";
                $xml .= "<desc><![CDATA[$desc]]></desc>\n";
                $xml .= "</$tabName>\n";
            }
        }
        return $xml;
    }

    /**
     * @param $artistId
     * @return string
     */
    private function getGalleries($artistId){
        $gallerys = Apollo_App::unserialize(get_apollo_meta($artistId, Apollo_DB_Schema::_APL_ARTIST_IMAGE_GALLERY, true));
        $xml = '';
        if(!empty($gallerys)){
            $gallerys = explode( ',' , $gallerys);
            foreach ($gallerys as $id){
                $post_img = get_post($id);
                if (!$post_img) {
                    continue;
                }
                $caption = !empty($post_img) ? $post_img->post_excerpt : '';
                $title = !empty($post_img) ? $post_img->post_title : '';

                $xml .= "<gallery>\n";
                if($caption || $title){
                    $xml .= "<name><![CDATA[$title]]></name>\n";
                    $xml .= "<caption><![CDATA[$caption]]></caption>\n";
                }

                $sizes = array(
                    'full' => $this->getImageSize($id, 'full'),
                    'large' => $this->getImageSize($id, 'large'),
                    'medium' => $this->getImageSize($id, 'medium'),
                    'thumbnail' => $this->getImageSize($id, 'thumbnail'),
                );

                $sizes = json_encode($sizes);

                $xml .= "<sizes><![CDATA[[$sizes]]]></sizes>\n";

                $xml .= "</gallery>\n";
            }
        }

        return $xml;
    }

    private function getImageSize($id, $size){
        $image = wp_get_attachment_image_src($id, $size);
        if(!empty($image)){
            return $image;
        }
        return array();
    }

    /**
     * @param $artistModel
     * @return string
     */
    private function getPublicArts($artistModel){

        $xml = '';
        if ($artistModel->is_public()){
            include_once APOLLO_TEMPLATES_DIR.'/public-art/list-public-arts.php';
            $list_pub_art = new List_Public_Art_Adapter( 1, Apollo_Display_Config::PAGESIZE_UPCOM );
            $list_pub_art->get_artist_public_art( $artistModel->id, false, true);
            $publicArts = $list_pub_art->getPublicArt();

            if(!empty($publicArts)) {
                foreach ($publicArts as $public) {
                    $xml .= "<publicArt>\n";
                    $title = get_the_title($public->ID);
                    $link = get_permalink($public->ID);
                    $image = get_the_post_thumbnail_url($public->ID, 'full');
                    $categories = $this->getCategories($public->ID, 'public-art-type', 'type')['xml'];

                    $xml .= "<title><![CDATA[$title]]></title>\n";
                    $xml .= "<link><![CDATA[$link]]></link>\n";
                    $xml .= "<image><![CDATA[$image]]></image>\n";

                    if(!empty($categories)){
                        $xml .= "<types>$categories</types>\n";
                    }

                    $xml .= "</publicArt>\n";
                }

                return $xml;
            }
        }

        return $xml;
    }

    /**
     * @param $artistId
     * @return string
     */
    private function getQuestions($artistId){

        $answers = Apollo_App::unserialize(get_apollo_meta($artistId, Apollo_DB_Schema::_APL_ARTIST_QUESTIONS, true));
        $questions = Apollo_App::unserialize( get_option( Apollo_DB_Schema::_APL_ARTIST_OPTIONS ) );
        $xml = '';
        if(!empty($answers)){
            foreach ($answers as $index => $answer){
                if(!empty($answer)){
                    $xml .= "<question>\n";
                    $xml .= "<name><![CDATA[$questions[$index]]]></name>\n";
                    $xml .= "<answer><![CDATA[$answer]]></answer>\n";
                    $xml .= "</question>\n";
                }
            }
        }

        return $xml;
    }

    /**
     * @param $artistModel
     * @return string
     */
    private function getAdditionalFields($artistModel){
        $custom_fields = Apollo_Custom_Field::get_group_fields( 'artist');
        $xml = '';
        $selectedValue = array();
        if ( $custom_fields ){
            foreach($custom_fields as $group){
                if(!empty($group['fields'])){
                    foreach (  $group['fields'] as $field ){
                        if ( ! Apollo_Custom_Field::can_display( $field, 'dp' ) ||
                            (!empty($unexpectedField) && $field->name == $unexpectedField)) continue;
                        $fieldValue = $artistModel->get_custom_field_value( $field );

                        $fieldValue = $field->cf_type == 'wysiwyg' ? Apollo_App::convertContentEditorToHtml($fieldValue) : $fieldValue;

                        if($fieldValue){
                            $selectedValue[$group['group']->id]["group_name"] = $group['group']->label ? $group['group']->label : '';
                            $selectedValue[$group['group']->id]["fields"][] = array($field->label, $fieldValue, $field->cf_type);
                        }
                    }
                }
            }
        }

        if(!empty($selectedValue)){
            foreach ($selectedValue as $group){
                $groupName = $group['group_name'];
                $xml .= "<group>\n";
                $xml .= "<groupName><![CDATA[$groupName]]></groupName>\n";
                $xml .= "<fields>\n";

                foreach($group["fields"] as $field){
                    $xml .= "<field>\n";
                    $xml .= "<name><![CDATA[$field[0]]]></name>\n";
                    $xml .= "<value><![CDATA[$field[1]]]></value>\n";
                    $xml .= "<type><![CDATA[$field[2]]]></type>\n";
                    $xml .= "</field>\n";
                }

                $xml .= "</fields>\n";
                $xml .= "</group>\n";
            }
        }

        return $xml;
    }

    /**
     * @Ticket #18559 - Check display control custom field
     * @param $artistModel
     * @return bool
     */
    private function checkDisplayControl($artistModel) {
        $custom_fields = Apollo_Custom_Field::get_group_fields( Apollo_DB_Schema::_ARTIST_PT);
        $result = true;
        if ($custom_fields) {
            if ( $custom_fields ){
                foreach($custom_fields as $group){
                    $groupData = isset($group['group']->meta_data) ? maybe_unserialize($group['group']->meta_data) : array();
                    if (!empty($groupData['group']['display_control']) && !empty($group['fields'])) {
                        foreach (  $group['fields'] as $field ){
                            if ($field->name == $groupData['group']['display_control']) {
                                if(empty($artistModel->get_custom_field_value( $field ))){
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }
        return $result;
    }
}