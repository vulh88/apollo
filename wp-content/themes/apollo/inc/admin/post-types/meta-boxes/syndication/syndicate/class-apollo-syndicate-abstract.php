<?php
/**
 * User: Thienld
 * Date: 16/12/2015
 * Time: 17:23
 * Description : Abstract class for handling Syndicate
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

abstract class Apollo_Syndicate_Export_Abstract
{

    protected $select = false;
    public $apLang;
    public $apOutput;
    public $apID;
    public $apType;
    public $dataQueryArgs;
    public $setSyndicationTransient = false;
    public $isUpdatingCache = true; // always equal "true", used to except case in iframe request and search
    public $totalEventsFeed = 0;
    public $isFrameRequest = false;
    public $postPerPage = 10;
    public $curPage = 1;
    public $hasPagination = false;
    public $searchParams = array();
    public $totalDateTime = 0;

    const MAX_FEEDS_PER_PAGE = 500;
    const MAX_SPOT_FEAT_PER_PAGE = 100;
    const SYNDICATION_TRANSIENT_KEY = 'syndication_caching_events';

    public $cacheFile = '';
    public $output = '';
    protected $cacheInstance = '';

    protected $enableStoreTransientCache = false;

    /**
     * Abstract whether set store transient cache or not
     * @author vulh
     * @return boolean
     */
    abstract protected function setEnableStoreTransientCache();

    /**
     * @var $_venues
     */
    private static $_venues;

    /**
     * @var $_orgs
     */
    private static $_orgs;


    public function __construct()
    {
        $this->enableStoreTransientCache = $this->setEnableStoreTransientCache();
        $this->reSetCacheFile();
    }

    /**
     * Re set cache file by connect the base name with syndication ID
    */
    private function reSetCacheFile()
    {
        $this->cacheFile .= '-'.(isset($_GET['apid']) ? $_GET['apid'] : '');
    }

    /**
     * Get cache content
     * @param $syndicationMeta
     * @return bool|string
     */
    public function getCache($syndicationMeta)
    {
        if ($this->cacheFile && $this->getMetaDataByKey(APL_Syndication_Const::ENABLE_FILE_CACHING, $syndicationMeta) == 'yes') {
            require_once(APOLLO_ADMIN_DIR. '/tools/cache/Inc/Files/Syndication.php');
            $this->cacheInstance = new APLC_Inc_Files_Syndication($this->cacheFile, $this->output);
            if (($cacheContent = $this->cacheInstance->get())) {
                return $cacheContent;
            }
        }
        return false;
    }

    /**
     * Add cache content
     * @param $content
     */
    public function addCache($content)
    {
        if (isset($this->cacheInstance) && $this->cacheInstance) {
            $this->cacheInstance->save($content);
        }
    }

    /**
     * Render Alternate info
     * @param $eventId
     * @param $alternateFields
     * @param bool $outputXml
     * @return string
     */
    public function renderAlternateFields($eventId, $alternateFields, $outputXml = true) {
        $xml = '';
        $result = array();
        $customFieldData = Apollo_App::unserialize( Apollo_App::apollo_get_meta_data( $eventId , Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA) );
        if ($customFieldData) {
            foreach ( $alternateFields as $altKey => $altItem ) {
                if (isset($customFieldData[$altKey])) {
                    if (is_array($customFieldData[$altKey])) {
                        $altMultipleValue = array();
                        foreach ($customFieldData[$altKey] as $altValue) {
                            if (isset($altItem['alternate_field'][$altValue])) {
                                array_push($altMultipleValue, $altItem['alternate_field'][$altValue]);
                            }
                        }
                        if (!empty($altMultipleValue)) {
                            if (!$outputXml) {
                                $result[$altItem['syn_field_name']] = !empty($altMultipleValue) ? implode(", ", $altMultipleValue) : '' ;
                            } else {
                                $xml .= "<" . $altItem['syn_field_name'] . "><![CDATA[";
                                $xml .= !empty($altMultipleValue) ? implode(", ", $altMultipleValue) : '';
                                $xml .= "]]></" . $altItem['syn_field_name'] . ">\n";
                            }
                        }
                    } else {
                        if (!empty($altItem['alternate_field'][$customFieldData[$altKey]])
                            || (isset($altItem['alternate_field'][$customFieldData[$altKey]]) && $altItem['alternate_field'][$customFieldData[$altKey]] == 0)) {
                            if (!$outputXml) {
                                $result[$altItem['syn_field_name']] = $altItem['alternate_field'][$customFieldData[$altKey]];
                            } else {
                                $xml .= "<" . $altItem['syn_field_name'] . "><![CDATA[";
                                $xml .= $altItem['alternate_field'][$customFieldData[$altKey]];
                                $xml .= "]]></" . $altItem['syn_field_name'] . ">\n";
                            }
                        }
                    }
                }
            }
        }
        if ($outputXml) {
            return $xml;
        } else {
            return $result;
        }
    }

    public function haveMore() {
        return ($this->curPage) * $this->postPerPage < intval( $this->totalEventsFeed );
    }

    // checking event expired date by current local time (get from time zone is set in backend > setting > general).
    public function isExpiredTime($event_id)
    {
        $apl_query = new Apl_Query( Apollo_Tables::_APL_EVENT_CALENDAR );
        $data = $apl_query->get_where( " event_id = ".intval($event_id)." ", "*", " LIMIT 1 ", " ORDER BY date_event DESC, time_to DESC " );
        if(empty($data)) return true; // force event expired if have error data
        $dateEvent = $data[0]->date_event;
        $timeTo = empty($data[0]->time_to) ? "23:59" : $data[0]->time_to; // set time_to = 23:59 if had no chosen end time for this event
        $unixTime = strtotime($dateEvent . ' ' . $timeTo);
        $currentTime = current_time('timestamp');
        return $unixTime < $currentTime;
    }

    public function extractQueryString(){
        try{
            $qs = $_SERVER['QUERY_STRING'];
            $this->dataQueryArgs = array();
            $qsArr = Apollo_Page_Module::apolloGetAllQueryStringToArray($qs);
            foreach($qsArr as $k => $v){
                switch ($k){
                    case 'aplang':
                        $this->apLang = $v;
                        break;
                    case 'apid':
                        $this->apID = $v;
                        break;
                    case 'apoutput':
                        $this->apOutput = $v;
                        break;
                    case 'aptype':
                        $this->apType = $v;
                        break;
                    default:
                        break;
                }
                $this->dataQueryArgs[$k] = $v;
            }
        }catch (Exception $ex){
            wp_safe_redirect('/404');
        }
    }

    /**
     * Set apID
     *
     * @author vulh
     * @param $apID
     */
    public function setApID($apID) {
        $this->apID = $apID;
    }

    /**
     * Set apID
     *
     * @author vulh
     * @param $aType
     */
    public function setAType($aType) {
        $this->apType = $aType;
    }

    /**
     * @param $syndicationID
     * @param $mod
     * @param $type
     * @param string $parentDir
     * @return string
     * @throws Exception
     */
    public function getCacheFilePath($syndicationID, $mod, $type, $parentDir = '')
    {
        if (!$parentDir) {
            $parentDir = Apollo_App::getUploadBaseInfo( 'syndication_dir' );
        }

        return sprintf('%s/%s_%s.%s', $parentDir, $syndicationID, $mod, $type);
    }

    /* Start of handle get event feed by filter which was implemented to email template generation */

    public static function getMetaDataByKey($key = '', $meta_data = array(), $default = array())
    {
        if (isset($meta_data[$key]) && isset($meta_data[$key][0])) {
            return $meta_data[$key][0];
        } elseif (isset($meta_data[$key])) {
            return $meta_data[$key];
        }
        return $default;
    }

    public static function getCitiesForQuery($serCitiesStr = '')
    {
        if (empty($serCitiesStr)) return '';
        $citiesArr = maybe_unserialize(maybe_unserialize($serCitiesStr));
        $rsCities = array();
        foreach ($citiesArr as $c) {
            $cSplit = explode('(', $c);
            if (!empty($cSplit)) {
                $rsCities[] = trim($cSplit[0]);
            }
        }
        return $rsCities;
    }

    public static function getUnSerializeValueForQuery($serializedString = '')
    {
        if (empty($serializedString)) return '';
        $rsArr = maybe_unserialize(maybe_unserialize($serializedString));
        return $rsArr;
    }

    public static function getEventStartDateEndDateForQuery($date_range = 0, $start_date = '', $end_date = '')
    {
        $today = date('Y-m-d', time());
        if (intval($date_range) === 0) {
            $startDate = !empty($start_date) ? $start_date : '';
            $endDate = !empty($end_date) ? $end_date : '';
        } elseif (intval($date_range) === 1) {
            $startDate = $endDate = $today;
        } elseif (intval($date_range) > 1) {
            $date = date_create($today);
            date_add($date, date_interval_create_from_date_string(absint($date_range) . " days"));
            $startDate = $today;
            $endDate = date_format($date, "Y-m-d");
        } else {
            $startDate = $endDate = $today;
        }
        return array(
            'start-date' => $startDate,
            'end-date' => $endDate
        );
    }

    public static function getEventStartDateEndDateForQueryOnIFrameRequest($date_range = 0, $start_date = '', $end_date = '')
    {
        $today = current_time("Y-m-d");
        if (intval($date_range) == -1000 ) { // filter by weekend
            $now = $today;
            $end_date = strtotime("+1 weeks");
            $ds = $de = '';
            while (date("Y-m-d", $now) != date("Y-m-d", $end_date)) {
                $day_index = date("w", $now);
                if ($day_index == 6) {
                    $ds = date("Y-m-d", $now);
                }
                if ($day_index == 0) {
                    $de = date("Y-m-d", $now);
                }

                $now = strtotime(date("Y-m-d", $now) . "+1 day");
            }
            $startDate = $ds;
            $endDate= $de;

        } elseif (intval($date_range) > 1) {
            $date = date_create($today);
            date_add($date, date_interval_create_from_date_string(absint($date_range) . " days"));
            $startDate = $today;
            $endDate = date_format($date, "Y-m-d");
        }  elseif (intval($date_range) === 1) {
            $tomorrow = current_time("Y-m-d", strtotime("+1 day"));
            $startDate = $endDate = $tomorrow;
        } elseif(!empty($start_date) || !empty($end_date)) {
            $startDate = $start_date;
            $endDate= $end_date;
        } elseif(intval($date_range) === -2){
            $startDate = $endDate = $today;
        } else {
            // default case :
            $startDate = '';
            $endDate = '';
        }
        return array(
            'start-date' => $startDate,
            'end-date' => $endDate
        );
    }

    public function getSyndicationTransientKey(){
        return self::SYNDICATION_TRANSIENT_KEY . '_' . $this->apID;
    }

    /**
     * Get syndication data and re format with a query
     *
     * @param boolean $reFormat
     * @return array
     */
    public function getSyndicationDataCache($reFormat = true){
        $transientKey = $this->getSyndicationTransientKey();
        $syndicationCachedData = get_transient($transientKey);

        // Return the list IDs
        if (!$reFormat) {
            return $syndicationCachedData;
        }

        if(!empty($syndicationCachedData)){
            $maxEvents = $this->getConfigMaxEvents();
            $args = array(
                'post_type' => Apollo_DB_Schema::_EVENT_PT,
                'posts_per_page' => intval($maxEvents),
                'post_status' => array('publish'),
                'post__in' => $syndicationCachedData,
                'orderby' => 'post__in'
            );
            if($this->hasPagination){
                $args['posts_per_page'] = $this->postPerPage;
                $args['paged'] = $this->curPage;
            }
            $result = query_posts($args);
            wp_reset_postdata();
            return $result;
        }
        return array();
    }

    public function setSyndicationDataCache($accountID = ''){
        $this->apID = $accountID;
        $syndicateSetting = get_post_meta($accountID);
        if(!empty($syndicateSetting)){
            $this->setSyndicationTransient = true;
            if (isset($syndicateSetting['meta-mode']) && $syndicateSetting['meta-mode'][0] === 'YES') {
                $this->getEventsBySelectedIndividualEventIDs($syndicateSetting);
            } else {
                $this->getEventsBySelectedEventType($syndicateSetting);
            }
        }
    }

    public function getSyndicationByArgs($args = array()){
        $syndicationID = isset($args['syndication_id']) ? $args['syndication_id'] : -1 ;
        $postPerPage = isset($args['post-per-page']) ? $args['post-per-page'] : Apollo_Syndicate_Export_Abstract::MAX_SPOT_FEAT_PER_PAGE ;
        $curPage = isset($args['cur-page']) ? $args['cur-page'] : 1 ;
        $this->apID = $syndicationID;
        $syndicateSetting = get_post_meta($syndicationID);
        $this->hasPagination = true;
        $this->postPerPage = $postPerPage;
        $this->curPage = $curPage;
        $result = array();
        if(!empty($syndicateSetting)){
            if (isset($syndicateSetting['meta-mode']) && $syndicateSetting['meta-mode'][0] === 'YES') {
                $result['eventData'] = $this->getEventsBySelectedIndividualEventIDs($syndicateSetting);
            } else {
                $result['eventData'] = $this->getEventsBySelectedEventType($syndicateSetting);
            }
            $result['totalEvent'] = $this->totalEventsFeed;
        }
        return $result;
    }

    public function getEventsBySelectedIndividualEventIDs($args = array())
    {
        if(!$this->setSyndicationTransient && empty($this->searchParams) && isset($this->apOutput) && $this->apOutput != 'etpl'){ // check if is only request for get feeds
            $result = $this->getSyndicationDataCache(false);
            if(!empty($result)){
                return $result;
            }
        }
        $eIDs = self::getMetaDataByKey('meta-eventID', $args);
        $eIDsArr = self::getUnSerializeValueForQuery($eIDs);
        $eSort = self::getMetaDataByKey('meta-sort', $args, 'post_title');
        $eIncPriCateSeg = self::getMetaDataByKey('meta-include-primary-category', $args, 'OFF');
        $eExcludePrivateEvents = self::getMetaDataByKey(APL_Syndication_Const::META_EXCLUDE_PRIVATE_EVENTS, $args, '');

        // Get event by ID
        if (!empty($args['eventID'])) {

            if (!in_array($args['eventID'], $eIDsArr)) {
                 return array();
            }
            $eIDsArr = array($args['eventID']);
        }

        $_args = array(
            'eIDs' => $eIDsArr,
            'eSort' => $eSort,
            'eIncludePrimaryCategory' => $eIncPriCateSeg,
            'eExcludePrivateEvents' => $eExcludePrivateEvents,
        );

        $eResults = $this->getEventsByFilters($_args, true);
        return $eResults;
    }

    private function setSyndicationTransient($eventData = array()){

        $syndicationSetting = get_post_meta($this->apID);
        if(!empty($syndicationSetting)){
            $cacheTimeValue =  of_get_option(Apollo_DB_Schema::_SYNDICATION_EXPIRED_TIME, Apollo_Display_Config::APL_SYNDICATION_EXPIRED_TIME);//self::getMetaDataByKey('meta-cache',$syndicationSetting); // hour
            // always clean current transient of current site and current account id for renewing transient each syndication data is updated.
            $transientKey = $this->getSyndicationTransientKey();
            delete_transient($transientKey);
            if(intval($cacheTimeValue) > 0){
                // get caching value in this account. if caching value > 0 -> is_caching = true
                $eIDs = array();
                foreach($eventData as $e){
                    $eIDs[] = $e->ID;
                }
                $timeCacheSet = intval($cacheTimeValue) * 60 * MINUTE_IN_SECONDS;
                set_transient($transientKey,$eIDs, $timeCacheSet );
            }
        }
    }

    public function getEventsBySelectedEventType($args = array())
    {

        if(empty($this->searchParams) && !$this->setSyndicationTransient && isset($this->apOutput) && $this->apOutput != 'etpl'){ // check if is only request for get feeds and don't get caches for outputType = Email Generate Template
            $result = $this->getSyndicationDataCache(false);
            if(!empty($result)){
                return $result;
            }
        }

        $eTypes = self::getMetaDataByKey('meta-type', $args);
        $eTypesArr = self::getUnSerializeValueForQuery($eTypes);
        $eBlacklistType = self::getMetaDataByKey('meta-blacklist', $args);
        $eBlacklistTypeArr = self::getUnSerializeValueForQuery($eBlacklistType);
        $eFinalType = !empty($eBlacklistTypeArr) ? array_diff($eTypesArr, $eBlacklistTypeArr) : $eTypesArr;
        $eStartDate = self::getMetaDataByKey('meta-dateStart', $args);
        $eEndDate = self::getMetaDataByKey('meta-dateEnd', $args);
        $eDateRange = self::getMetaDataByKey('meta-range', $args);
        $filterDate = self::getEventStartDateEndDateForQuery($eDateRange, $eStartDate, $eEndDate);
        $eOrgs = self::getMetaDataByKey('meta-orgs', $args);
        $eOrgsArr = self::getUnSerializeValueForQuery($eOrgs);
        $eVenues = self::getMetaDataByKey('meta-venues', $args);
        $eVenuesArr = self::getUnSerializeValueForQuery($eVenues);
        $eFilterByZipOrCity = self::getMetaDataByKey('meta-cityzip', $args, '');
        $eCity = self::getMetaDataByKey('meta-cities', $args);
        $eCityArr = self::getCitiesForQuery($eCity);
        $eZip = self::getMetaDataByKey('meta-zips', $args);
        $eZipArr = self::getUnSerializeValueForQuery($eZip);
        $eSort = self::getMetaDataByKey('meta-sort', $args, 'post_title');
        $eIncPriCateSeg = self::getMetaDataByKey('meta-include-primary-category', $args, 'OFF');
        $eExcludePrivateEvents = self::getMetaDataByKey(APL_Syndication_Const::META_EXCLUDE_PRIVATE_EVENTS, $args, '');

        $_args = array(
            'eType' => $eFinalType,
            'eExcludeType'  => $eBlacklistTypeArr,
            'eStartDate' => $filterDate['start-date'],
            'eEndDate' => $filterDate['end-date'],
            'eOrganizations' => $eOrgsArr,
            'eVenues' => $eVenuesArr,
            'eFilterByCityOrZip' => $eFilterByZipOrCity,
            'eCities' => $eCityArr,
            'eZips' => $eZipArr,
            'eSort' => $eSort,
            'eIncludePrimaryCategory' => $eIncPriCateSeg,
            'eExcludePrivateEvents' => $eExcludePrivateEvents,
        );

        // Get by event ID
        if (!empty($args['eventID'])) {
            $_args['eIDs'] = array($args['eventID']);
        }

        $eResults = $this->getEventsByFilters($_args);
        return $eResults;
    }

    function filterSearchParams($eSydFiltersParams) {

        if (!empty($this->searchParams['startDate']) && strtotime($this->searchParams['startDate']) > strtotime($eSydFiltersParams['eStartDate'])) {
            $eSydFiltersParams['eStartDate'] = $this->searchParams['startDate'];
        }

        if (!empty($this->searchParams['endDate']) && strtotime($this->searchParams['endDate']) < strtotime($eSydFiltersParams['eEndDate'])) {
            $eSydFiltersParams['eEndDate'] = $this->searchParams['endDate'];
        }

        // Get the the categories ID that already existed in the range of valid categories
        if (!empty($this->searchParams['categories'])) {
            $searchTypesArr = explode(',', $this->searchParams['categories']);
            $diffArr = array_intersect($searchTypesArr, $eSydFiltersParams['eType']);
            if (!$diffArr) $diffArr = array(-1);
            $eSydFiltersParams['eType'] = !empty($eSydFiltersParams['eType']) ? $diffArr : $searchTypesArr;
        }

        // Get the the venues ID that already existed in the range of valid venues
        if (!empty($this->searchParams['venues'])) {
            $searchTypesArr = explode(',', $this->searchParams['venues']);
            $eSydFiltersParams['eVenues'] = !empty($eSydFiltersParams['eVenues']) ? array_intersect($searchTypesArr, $eSydFiltersParams['eVenues']) : $searchTypesArr ;
        }

        if (!empty($this->searchParams['keyword'])) {
            $eSydFiltersParams['sKeyword'] = $this->searchParams['keyword'];
        }

        if (!empty($this->searchParams['page'])) {
            $this->curPage = intval($this->searchParams['page']);
        }

        $maxEvents = $this->getConfigMaxEvents();
        if (!empty($this->searchParams['limit']) && $this->searchParams['limit'] > 0 && $this->searchParams['limit'] <= $maxEvents) {
            $this->postPerPage = intval($this->searchParams['limit']);
            $this->hasPagination = true;
        }

        return $eSydFiltersParams;
    }

    /**
     * Get config max events
     * @return integer
    */
    public function getConfigMaxEvents()
    {
        $maxEvents = intval(get_post_meta($this->apID, APL_Syndication_Const::NUM_EVENTS, true));

        if (!$maxEvents) {
            $maxEvents = self::MAX_FEEDS_PER_PAGE;
        }
        return $maxEvents;
    }

    public function getEventsByFilters($params = array(), $allowCache = true)
    {
        global $eSydFiltersParams;

        $eSydFiltersParams = $this->filterSearchParams($params);

        $maxEvents = $this->getConfigMaxEvents();
        $arr_params = array(
            'post_type' => Apollo_DB_Schema::_EVENT_PT,
            'posts_per_page' => $this->hasPagination ? $this->postPerPage : intval($maxEvents),
            'post_status' => array('publish'),
        );

        if (isset($eSydFiltersParams['select']) && $eSydFiltersParams['select']) {
            $this->select = $eSydFiltersParams['select'];
        }

        // for getting pagination event feeds
        if($this->isFrameRequest){
            $arr_params = array(
                'post_type' => Apollo_DB_Schema::_EVENT_PT,
                'posts_per_page' => $this->postPerPage,
                //'posts_per_page' => -1,//Trilm fixed here
                'paged' => $this->curPage,
                'post_status' => array('publish'),
            );
        }

        if (!empty($eSydFiltersParams['eType'])) {
            $arr_params['tax_query'] = array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'event-type',
                    'field' => 'id',
                    'terms' => $eSydFiltersParams['eType'],
                    'include_children' => false,
                    'operator' => 'IN'
                )
            );
        }

        /**
         * @Ticket #14215
         * Only execute this query for API because this will speed down the query, API limits the number of events
        */
        if (!empty($eSydFiltersParams['eExcludeType']) && !$this->enableStoreTransientCache) {
            $arr_params['tax_query'] = array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'event-type',
                    'field' => 'id',
                    'terms' => $eSydFiltersParams['eExcludeType'],
                    'include_children' => false,
                    'operator' => 'NOT IN'
                )
            );
        }


        if (!empty($eSydFiltersParams['eIDs'])) {
            $arr_params['post__in'] = $eSydFiltersParams['eIDs'];
        }

        if($this->hasPagination){
            $arr_params['posts_per_page'] = $this->postPerPage;
            $arr_params['paged'] = $this->curPage;
        }

        // Re-set if missing for some cases
        if (empty($arr_params['posts_per_page'])) {
            $arr_params['posts_per_page'] = $maxEvents;
        }


        add_filter('posts_fields', array($this,'filter_select_event_tbl'), 10, 1);
        add_filter('posts_where', array($this,'filter_where_event_tbl'), 10, 1);
        add_filter('posts_join', array($this,'filter_join_event_tbl'), 10, 1);
        add_filter('posts_orderby', array($this,'filter_order_event_tbl'), 10, 1);
        add_filter('posts_groupby', array($this,'filter_groupby'), 10, 1);
        add_filter('posts_request', array($this,'dump_request'));

         if(!empty( $eSydFiltersParams['sKeyword'])){
             $arr_params['s'] =  $eSydFiltersParams['sKeyword'];
         }

        $result = query_posts($arr_params);
        //echo $GLOBALS['wp_query']->request;exit;
        $this->totalEventsFeed = $GLOBALS['wp_query']->found_posts;

        // set transient for syndication in order to serve optimizing caching on next request.
        // always set cache because option Include Primary Category is only affect to request get event feed of outputType = 'Event Generate Email Template'
        if($this->enableStoreTransientCache){
            $this->setSyndicationTransient($result); // caching is using for all output type including Iframe but exclude Event Generation Email Template.
        }

        remove_filter('posts_orderby', array($this,'filter_select_event_tbl'), 10);
        remove_filter('posts_orderby', array($this,'filter_order_event_tbl'), 10);
        remove_filter('posts_join', array($this,'filter_join_event_tbl'), 10);
        remove_filter('posts_where', array($this,'filter_where_event_tbl'), 10);
        remove_filter('posts_groupby', array($this,'filter_groupby'), 10);
        remove_filter('posts_request', array($this,'dump_request'));

        wp_reset_postdata();
        return $result;
    }

    public function filter_select_event_tbl($select)
    {
        if ($this->select) return $this->select;

        global $wpdb, $eSydFiltersParams;
        if ($this->apOutput == 'etpl' && isset($eSydFiltersParams['eIncludePrimaryCategory']) && $eSydFiltersParams['eIncludePrimaryCategory'] == 'ON') {
            $select .= ', ' . $wpdb->posts . '.ID AS event_id, primary_category_tbl.pc_primary_category_id AS primary_cate_id, primary_category_tbl.pc_primary_category_name AS primary_cate_name ';
        }
        return $select;
    }

    public function filter_groupby($groupby)
    {
        global $wpdb, $eSydFiltersParams;
        if ($this->apOutput == 'etpl' && isset($eSydFiltersParams['eIncludePrimaryCategory']) && $eSydFiltersParams['eIncludePrimaryCategory'] == 'ON') {
            return $wpdb->posts . ".ID, primary_category_tbl.pc_primary_category_id, primary_category_tbl.pc_primary_category_name ";
        }
        return "$wpdb->posts.ID";
    }

    public function dump_request($input)
    {
         // aplDebug($input);
        $debugContentType = isset($_GET['is_debugging']);
        if($debugContentType){
            aplDebug($input,1);
        }
        return $input;
    }

    public function filter_order_event_tbl($order)
    {
        global $eSydFiltersParams, $wpdb;

        if (!empty($eSydFiltersParams['eSort'])) {
            switch ($eSydFiltersParams['eSort']) {
                case 'post_title':
                    $order = " DATEDIFF( mt_end_d.meta_value, start_date.meta_value ) >= ".Apollo_App::orderDateRange() . " ASC, ". $wpdb->posts . ".post_title ASC ";
                    break;
                case 'em2.meta_value':
                    $order = " DATEDIFF( mt_end_d.meta_value, start_date.meta_value ) >= ".Apollo_App::orderDateRange() . " ASC, start_date.meta_value ASC ";
                    break;
                case 'em3.meta_value':
                    $order = " DATEDIFF( mt_end_d.meta_value, start_date.meta_value ) >= ".Apollo_App::orderDateRange() . " ASC,  mt_end_d.meta_value ASC ";
                    break;
                default:
                    $order = " DATEDIFF( mt_end_d.meta_value, start_date.meta_value ) >= ".Apollo_App::orderDateRange() . " ASC, ". $wpdb->posts . ".post_title ASC ";
                    break;
            }
        }
        if ($this->apOutput == 'etpl' && isset($eSydFiltersParams['eIncludePrimaryCategory']) && $eSydFiltersParams['eIncludePrimaryCategory'] == 'ON') {
            $order = " primary_category_tbl.pc_primary_category_name, " . $order;
        }
        return $order;
    }

    public function filter_join_event_tbl($join)
    {
        global $wpdb, $eSydFiltersParams;
        if (isset($eSydFiltersParams['eStartDate']) && isset($eSydFiltersParams['eEndDate'])) {
            $join .= " INNER JOIN {$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}} mt_end_d ON $wpdb->posts.ID = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '" . Apollo_DB_Schema::_APOLLO_EVENT_END_DATE . "'";
            $join .= ' INNER JOIN ' . $wpdb->{Apollo_Tables::_APOLLO_EVENT_META} . ' start_date ON ' .$wpdb->posts.'.ID = start_date.apollo_event_id AND start_date.meta_key = "' . Apollo_DB_Schema::_APOLLO_EVENT_START_DATE . '" ';

            $join .= " INNER JOIN (
                SELECT e_calendar.event_id as event_id, e_calendar.date_event as date_event
                FROM " . $wpdb->{Apollo_Tables::_APL_EVENT_CALENDAR} . " e_calendar
                INNER JOIN " . $wpdb->{Apollo_Tables::_APOLLO_EVENT_META} . " esd ON e_calendar.event_id = esd.apollo_event_id AND esd.meta_key = '" . Apollo_DB_Schema::_APOLLO_EVENT_START_DATE . "'
                INNER JOIN " . $wpdb->{Apollo_Tables::_APOLLO_EVENT_META} . " eed ON e_calendar.event_id = eed.apollo_event_id AND eed.meta_key = '" . Apollo_DB_Schema::_APOLLO_EVENT_END_DATE . "'
                WHERE e_calendar.date_event BETWEEN esd.meta_value AND eed.meta_value
            ) as ec ON $wpdb->posts.ID = ec.event_id ";
        } else {
            $today = current_time('Y-m-d');
            $join .= " INNER JOIN {$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}} mt_end_d ON $wpdb->posts.ID = mt_end_d.apollo_event_id AND mt_end_d.meta_key = '" . Apollo_DB_Schema::_APOLLO_EVENT_END_DATE . "' AND mt_end_d.meta_value >= '".$today."' ";
            $join .= ' INNER JOIN ' . $wpdb->{Apollo_Tables::_APOLLO_EVENT_META} . ' start_date ON ' .$wpdb->posts.'.ID = start_date.apollo_event_id AND start_date.meta_key = "' . Apollo_DB_Schema::_APOLLO_EVENT_START_DATE . '" ';
        }

        // eIncludePrimaryCategory
        if ($this->apOutput == 'etpl' && isset($eSydFiltersParams['eIncludePrimaryCategory']) && $eSydFiltersParams['eIncludePrimaryCategory'] == 'ON') {
            $join .= ' INNER JOIN (
                SELECT empc.apollo_event_id AS pc_event_id, empc.meta_value AS pc_primary_category_id, cq_evt.name AS pc_primary_category_name
                FROM ' . $wpdb->{Apollo_Tables::_APOLLO_EVENT_META} . ' empc
                INNER JOIN ' . $wpdb->terms . ' cq_evt ON empc.`meta_value` = cq_evt.`term_id`
                WHERE empc.meta_key = "' . Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID . '"
                GROUP BY empc.apollo_event_id, empc.meta_value, cq_evt.name
            ) AS primary_category_tbl
            ON ' . $wpdb->posts . '.ID = primary_category_tbl.pc_event_id
        ';
        }

        return $join;
    }

    public function filter_where_event_tbl($where)
    {
        global $eSydFiltersParams, $wpdb;

        $sqlString = " AND $wpdb->posts.post_title != 'Auto Draft' ";

        //query by venue
        if (isset($eSydFiltersParams['eVenues']) && !empty($eSydFiltersParams['eVenues'])) {
            $location = $eSydFiltersParams['eVenues'];
            $sqlString .= 'AND  ' . $wpdb->posts . '.ID IN (
                SELECT em.apollo_event_id
                FROM ' . $wpdb->{Apollo_Tables::_APOLLO_EVENT_META} . ' em
                WHERE em.apollo_event_id = ' . $wpdb->posts . '.ID
                    AND em.meta_value IN (' . implode(',', $location) . ')
                    AND em.meta_key = "' . Apollo_DB_Schema::_APOLLO_EVENT_VENUE . '"
            )';
        }

        //query by org
        if (isset($eSydFiltersParams['eOrganizations']) && !empty($eSydFiltersParams['eOrganizations'])) {
            $orgs = $eSydFiltersParams['eOrganizations'];
            $sqlString .= 'AND  ' . $wpdb->posts . '.ID IN (
                SELECT e_org.post_id
                FROM ' . $wpdb->{Apollo_Tables::_APL_EVENT_ORG} . ' e_org
                WHERE e_org.org_id IN (' . implode(',', $orgs) . ')
                GROUP BY (e_org.post_id)
            )';
        }



        //query by city
        if (isset($eSydFiltersParams['eFilterByCityOrZip'])
            && $eSydFiltersParams['eFilterByCityOrZip'] == 'CITY'
            && !empty($eSydFiltersParams['eCities'])
        ) {

            $city = $eSydFiltersParams['eCities'];
            $eventsByCity = self::getEventIDsByCityFilters($city);
            $sqlString .= ' AND (  ' . $wpdb->posts . '.ID IN (' . implode(",",$eventsByCity) . ') )';
        }



        //query by zip
        if (isset($eSydFiltersParams['eFilterByCityOrZip'])
            && $eSydFiltersParams['eFilterByCityOrZip'] == 'ZIP'
            && !empty($eSydFiltersParams['eZips'])
        ) {
            $zips = $eSydFiltersParams['eZips'];
            $eventsByZip = self::getEventIDsByZipFilters($zips);
            $sqlString .= ' AND ( ' . $wpdb->posts . '.ID IN ('.implode(",",$eventsByZip).') )';
        }

        if (isset($eSydFiltersParams['eStartDate']) && isset($eSydFiltersParams['eEndDate'])) {
            $eStartDate = $eSydFiltersParams['eStartDate'];
            $eEndDate = $eSydFiltersParams['eEndDate'];

            //logic query event from search start date and search end date
            //case1: search_start <= event_end <= search_end
            //case2: event_start <= search_start AND event_end = '2037-01-01'
            //case3: search_start <=  available event dates <= search_end
            //all case: (case1) OR (case2) OR (case3)


            if(!empty($eStartDate) || !empty($eEndDate)){
                $sqlStringCase1 = '';
                $sqlStringCase2 = '';
                $sqlStringCase3 = '';

                if($eStartDate != '' && $eEndDate != ''){
                    if( $eEndDate == $eStartDate){
                        $_eStartDate = self::getGreaterThanDate($eStartDate);
                        $sqlStringCase1 .= " mt_end_d.meta_value >= '".$_eStartDate."' AND start_date.meta_value <= '".$_eStartDate."' ";
                        $sqlStringCase3 .= " ec.date_event = '" . $eStartDate . "'  ";

                    } else {
                        $eStartDate = self::getGreaterThanDate($eStartDate);
                        $sqlStringCase1 .= " mt_end_d.meta_value BETWEEN '".$eStartDate."' AND '".$eEndDate."' ";
                        $sqlStringCase3 .= " ec.date_event BETWEEN '" . $eStartDate . "' AND '". $eEndDate ."' ";
                    }
                }
                elseif($eStartDate != '' && $eEndDate == ''){
                    $sqlStringCase1 .= " mt_end_d.meta_value <= '".self::getGreaterThanDate($eStartDate)."'  ";
                    $sqlStringCase3 .= " ec.date_event <= '" . $eStartDate . "'  ";
                }
                elseif($eEndDate != '' && $eStartDate == ''){
                    $sqlStringCase1 .= " mt_end_d.meta_value >= '".self::getGreaterThanDate($eEndDate)."' ";
                    $sqlStringCase3 .= " ec.date_event >=  '". $eEndDate ."' ";
                }

                //search by all case
                $sqlStringAllCase = '(' . $sqlStringCase1 . ') OR ('. $sqlStringCase3 .')';
                $sqlString .= ' AND ( ' . $sqlStringAllCase . ' ) ';
            }
        }

        // Filter available events
        $currentDate = date('Y-m-d', current_time('timestamp') - 86400);
        $sqlString .= " AND ( mt_end_d.meta_value >= '$currentDate' OR start_date.meta_value >= '$currentDate'   )";


        // ticket #10998 : exclude private events in Syndication feeds
        if ( isset($eSydFiltersParams['eExcludePrivateEvents']) && $eSydFiltersParams['eExcludePrivateEvents'] == 'yes' ) {
            $sqlString = Apollo_App::hidePrivateEvent($sqlString, $wpdb->posts);
        }

        //Trilm: disable _apl_event_term_primary_id in blacklist

        $where .= $sqlString;


        return $where;
    }

    public static function getGreaterThanDate($date) {
        $currentDate = current_time('Y-m-d');
        if (strtotime($date) < strtotime($currentDate)) return $currentDate;
        return $date;
    }

    public static function getEventIDsByCityFilters($filterCity = array()){
        global $wpdb;
        $tableEventMeta = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};
        $templeMetaKey = Apollo_DB_Schema::_APL_EVENT_TMP_VENUE;
        $criteriaFilterCityTmp = [];
        $resultTmpCityEventIDs = [];
        if(is_array($filterCity) && count($filterCity) > 0){
            foreach ($filterCity as $city){
                $cityLength =  strlen($city);
                $criteriaFilterCityTmp[] = " (  meta_value like '%\"_venue_city\";s:$cityLength:\"$city\"%'  ) ";
            }
        }

        if(!empty($criteriaFilterCityTmp)){
            $cityInTemp = implode(" OR ", $criteriaFilterCityTmp);
            $sqlCityTmpInEventMeta = 'SELECT mt.apollo_event_id as event_id
                             FROM '.$tableEventMeta.' mt
                             WHERE meta_key = \''.$templeMetaKey.'\' AND ('.$cityInTemp.')
                             GROUP BY mt.apollo_event_id ';

            $qrTmpCityInEventMeta = $wpdb->get_results($sqlCityTmpInEventMeta);
            if(!empty($qrTmpCityInEventMeta)){
                foreach($qrTmpCityInEventMeta as $item){
                    $resultTmpCityEventIDs[] = $item->event_id;
                }
            }
        }

        $sql = 'SELECT em.apollo_event_id as event_id
                FROM ' . $wpdb->{Apollo_Tables::_APOLLO_EVENT_META} . ' em INNER JOIN ' . $wpdb->{Apollo_Tables::_APL_VENUE_META} . ' mt_venue ON em.meta_value = mt_venue.apollo_venue_id
                WHERE em.meta_key = "' . Apollo_DB_Schema::_APOLLO_EVENT_VENUE . '"
                    AND mt_venue.meta_key = "' . Apollo_DB_Schema::_VENUE_CITY . '"
                    AND mt_venue.meta_value IN ("' . implode('","', $filterCity) . '") GROUP BY event_id ';
        $queriedResults = $wpdb->get_results($sql);
        $resultEventIDs = array(-1);
        if(!empty($queriedResults)){
            foreach($queriedResults as $item){
                $resultEventIDs[] = $item->event_id;
            }
        }

        if(!empty($resultTmpCityEventIDs)) {
            $resultEventIDs = array_merge($resultEventIDs, $resultTmpCityEventIDs);
        }

        return $resultEventIDs;
    }

    public static function getEventIDsByZipFilters($filterZip = array()){
        global $wpdb;
        $tableEventMeta = $wpdb->{Apollo_Tables::_APOLLO_EVENT_META};
        $templeMetaKey = Apollo_DB_Schema::_APL_EVENT_TMP_VENUE;
        $criteriaFilterZipTmp = [];
        $resultTmpZipEventIDs = [];
        if(is_array($filterZip) && count($filterZip) > 0){
            foreach ($filterZip as $zip){
                $zipLength =  strlen($zip);
                $criteriaFilterZipTmp[] = " (  meta_value like '%\"_venue_zip\";s:$zipLength:\"$zip\"%'  ) ";
            }
        }

        if(!empty($criteriaFilterZipTmp)){
            $zipInTemp = implode(" OR ", $criteriaFilterZipTmp);
            $sqlZipTmpInEventMeta = 'SELECT mt.apollo_event_id as event_id
                                     FROM '.$tableEventMeta.' mt
                                     WHERE meta_key = \''.$templeMetaKey.'\' AND ('.$zipInTemp.')
                                     GROUP BY mt.apollo_event_id ';

            $qrTmpZipInEventMeta = $wpdb->get_results($sqlZipTmpInEventMeta);
            if(!empty($qrTmpZipInEventMeta)){
                foreach($qrTmpZipInEventMeta as $item){
                    $resultTmpZipEventIDs[] = $item->event_id;
                }
            }
        }


        $sql = 'SELECT em.apollo_event_id as event_id
                FROM ' . $wpdb->{Apollo_Tables::_APOLLO_EVENT_META} . ' em INNER JOIN ' . $wpdb->{Apollo_Tables::_APL_VENUE_META} . ' mt_venue ON em.meta_value = mt_venue.apollo_venue_id
                WHERE  em.meta_key = "' . Apollo_DB_Schema::_APOLLO_EVENT_VENUE . '"
                    AND mt_venue.meta_key = "' . Apollo_DB_Schema::_VENUE_ZIP . '"
                    AND mt_venue.meta_value IN ("' . implode('","', $filterZip) . '")  GROUP BY event_id';
        $queriedResults = $wpdb->get_results($sql);
        $resultEventIDs = array(-1);
        if(!empty($queriedResults)){
            foreach($queriedResults as $item){
                $resultEventIDs[] = $item->event_id;
            }
        }

        if(!empty($resultTmpZipEventIDs)) {
            $resultEventIDs = array_merge($resultEventIDs, $resultTmpZipEventIDs);
        }

        return $resultEventIDs;
    }

    public static function separateEventDataByPrimaryCategory($eventData)
    {
        $result = array();
        foreach ($eventData as $pcEvent) {
            $pcEvent = is_object($pcEvent) ? get_object_vars($pcEvent) : $pcEvent;
            $eventTypeID = $pcEvent['primary_cate_id'];
            $eventID = $pcEvent['event_id'];
            $result[$eventTypeID][] = $eventID;
        }
        return $result;
    }

    public static function get_event($the_event = false)
    {
        return new Apollo_Event($the_event, Apollo_DB_Schema::_EVENT_PT);
    }

    public static function get_org($the_org = false)
    {
        return new Apollo_Org($the_org, Apollo_DB_Schema::_ORGANIZATION_PT);
    }

    public static function get_venue($the_venue = false)
    {
        return new Apollo_Org($the_venue, Apollo_DB_Schema::_VENUE_PT);
    }

    /* End of handle get event feed by filter which was implemented to email template generation */

    /**
     * @param $event
     * @param array $args
     * @return mixed
     */
    public static function get_all_details($event, $args = array(
        'img_size'   => 'thumbnail',
        'img_tag'  => true,
        'org_img'    => true,
    ))
    {
        // Read the meta data from eventmeta - plunk it into an arrray to access it
        $eventObj = get_event($event);

        if (!empty($eventObj->post->post_status) && $eventObj->post->post_status != 'publish') {
            return false;
        }


        $row1 = maybe_unserialize($eventObj->get_meta_data(Apollo_DB_Schema::_APOLLO_EVENT_DATA));

        $row1['_apl_event_term_primary_id'] = $eventObj->get_meta_data(Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID);
        $row1['_apollo_event_start_date'] = $eventObj->get_meta_data(Apollo_DB_Schema::_APOLLO_EVENT_START_DATE);
        $row1['_apollo_event_end_date'] = $eventObj->get_meta_data(Apollo_DB_Schema::_APOLLO_EVENT_END_DATE);

        $row1['ongoing'] = Apollo_App::isOngoing(strtotime($row1['_apollo_event_start_date']), strtotime($row1['_apollo_event_end_date'])) ? 'Y' : 'N';

        $types = wp_get_post_terms($event, 'event-type', array(
            'fields'    => 'ids'
        ));
        $row1['terms'] = $types;

        $row1['_apollo_event_days_of_week'] = $eventObj->get_meta_data(Apollo_DB_Schema::_APOLLO_EVENT_DAYS_OF_WEEK, Apollo_DB_Schema::_APOLLO_EVENT_DATA);

        $row1['secondary_types'] = $types;


        /**
         * Storing this ORG in an object for the next loop
         */

        $orgArr = $eventObj->get_register_org(); // Get the registered org

        $orgRow = array();
        if (!empty($orgArr->org_id)) {
            $orgID = $orgArr->org_id;

            if (!empty(self::$_orgs[$orgID])) {
                $orgRow = self::$_orgs[$orgID];
            }
            else if ($org = get_org($orgID)) {

                // Now add the org contact info to the array
                $orgRow['_org_name'] = $org->get_title();
                $orgRow['_org_id'] = $org->id;

                $orgMetaData = maybe_unserialize($org->get_meta_data(Apollo_DB_Schema::_APL_ORG_DATA));

                // Now add the organization address info to the array
                $orgAddMetaData = maybe_unserialize($org->get_meta_data(Apollo_DB_Schema::_APL_ORG_ADDRESS));
                if (empty($vnrow[Apollo_DB_Schema::_ORG_LATITUDE])) {
                    $orgRow[Apollo_DB_Schema::_ORG_LATITUDE] = empty($orgAddMetaData[Apollo_DB_Schema::_ORG_LATITUDE]) ? $org->get_meta_data(Apollo_DB_Schema::_ORG_LATITUDE) : $orgAddMetaData[Apollo_DB_Schema::_ORG_LATITUDE];
                }

                if (empty($vnrow[Apollo_DB_Schema::_ORG_LONGITUDE])) {
                    $orgRow[Apollo_DB_Schema::_ORG_LONGITUDE] = empty($orgAddMetaData[Apollo_DB_Schema::_ORG_LONGITUDE]) ? $org->get_meta_data(Apollo_DB_Schema::_ORG_LONGITUDE) : $orgAddMetaData[Apollo_DB_Schema::_ORG_LONGITUDE];
                }


                if (!empty($args['org_img']) && $orgImgData = wp_get_attachment_image_src( get_post_thumbnail_id( $orgID ), 'large') ) {
                    $orgRow['org_img'] = $orgImgData[0];
                }
                else {
                    $orgRow['org_img'] = '';
                }

                if (is_array($orgMetaData)) {
                    $orgRow = array_merge($orgRow, $orgMetaData);
                }

                self::$_orgs[$orgID] = $orgRow;
            }

            if ($orgRow) {
                $row1 = array_merge($row1, $orgRow);
            }
        }


        /**
         * Storing this ORG in an object for the next loop
         */

        $venueID = $eventObj->get_meta_data(Apollo_DB_Schema::_APOLLO_EVENT_VENUE); // Get the venue

        $vnrow = array();
        if (!empty(self::$_venues[$venueID])) {
            $vnrow = self::$_venues[$venueID];
        }
        else if ($venueID && $venue = get_venue($venueID)) {
            $vnrow['_venue_name'] = $venue->get_title();
            $vnrow['_venue_id'] = $venue->id;

            // Now add the venue contact info to the array
            $venueMetaData = maybe_unserialize($venue->get_meta_data(Apollo_DB_Schema::_APL_VENUE_DATA));
            if ($venueMetaData) {
                $vnrow = array_merge($vnrow, $venueMetaData);
            }

            // Now add the venue address info to the array
            $venueAddMetaData = maybe_unserialize($venue->get_meta_data(Apollo_DB_Schema::_APL_VENUE_ADDRESS));

            $vnrow = is_array($venueAddMetaData) ? array_merge($vnrow, $venueAddMetaData) : $vnrow;

            $vnrow[Apollo_DB_Schema::_VENUE_LATITUDE] = empty($venueAddMetaData[Apollo_DB_Schema::_VENUE_LATITUDE]) ?
                    $venue->get_meta_data(Apollo_DB_Schema::_VENUE_LATITUDE) : $venueAddMetaData[Apollo_DB_Schema::_VENUE_LATITUDE];

            $vnrow[Apollo_DB_Schema::_VENUE_LONGITUDE] = empty($venueAddMetaData[Apollo_DB_Schema::_VENUE_LONGITUDE]) ?
                $venue->get_meta_data(Apollo_DB_Schema::_VENUE_LONGITUDE) : $venueAddMetaData[Apollo_DB_Schema::_VENUE_LONGITUDE];


            self::$_venues[$venueID] = $vnrow;
        }

        if ($vnrow) {
            $row1 = array_merge($row1, $vnrow);
        }

        // Get image
        if ($imgSize = !empty($args['img_size']) ? $args['img_size'] : '') {
            if (!empty($args['img_tag'])) {
                $row1['image'] = $eventObj->get_image($imgSize, array(), array(), '', false);
            }
            else {
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $eventObj->id ), $imgSize );
                $row1['image'] = $image ? $image[0] : '';
            }
        }

        $row1['post'] = $eventObj->post;

        $data = $eventObj->get_content(200);
        $row1['post_content_200'] = isset($data['text']) ? $data['text'] : '';

        return $row1;

    }

    /**
     * Get details info for iframe
     * @param $event
     * @param array $args
     * @return mixed
     */
    public static function get_all_details_for_iframe($event, $args = array(
        'img_size'   => 'thumbnail',
        'img_tag'  => true,
        'org_img'    => true,
    ))
    {
        // Read the meta data from eventmeta - plunk it into an arrray to access it
        $eventObj = get_event($event);

        if (!empty($eventObj->post->post_status) && $eventObj->post->post_status != 'publish') {
            return false;
        }

        $row1['_apl_event_term_primary_id'] = $eventObj->get_meta_data(Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID);
        $row1['_apollo_event_start_date'] = $eventObj->get_meta_data(Apollo_DB_Schema::_APOLLO_EVENT_START_DATE);
        $row1['_apollo_event_end_date'] = $eventObj->get_meta_data(Apollo_DB_Schema::_APOLLO_EVENT_END_DATE);

        $row1['ongoing'] = Apollo_App::isOngoing(strtotime($row1['_apollo_event_start_date']), strtotime($row1['_apollo_event_end_date'])) ? 'Y' : 'N';

        $types = wp_get_post_terms($event, 'event-type', array(
            'fields'    => 'ids'
        ));
        $row1['terms'] = $types;

        $row1['_apollo_event_days_of_week'] = $eventObj->get_meta_data(Apollo_DB_Schema::_APOLLO_EVENT_DAYS_OF_WEEK, Apollo_DB_Schema::_APOLLO_EVENT_DATA);

        $row1['secondary_types'] = $types;


        /**
         * Storing this ORG in an object for the next loop
         */

        $orgArr = $eventObj->get_register_org(); // Get the registered org

        $orgRow = array();
        if (!empty($orgArr->org_id)) {
            $orgID = $orgArr->org_id;

            if (!empty(self::$_orgs[$orgID])) {
                $orgRow = self::$_orgs[$orgID];
            }
            else if ($org = get_org($orgID)) {
                // Now add the org contact info to the array
                $orgRow['_org_name'] = $org->get_title();
            }

            if ($orgRow) {
                $row1 = array_merge($row1, $orgRow);
            }
        }


        /**
         * Storing this ORG in an object for the next loop
         */

        $venueID = $eventObj->get_meta_data(Apollo_DB_Schema::_APOLLO_EVENT_VENUE); // Get the venue

        $vnrow = array();
        if (!empty(self::$_venues[$venueID])) {
            $vnrow = self::$_venues[$venueID];
        }
        else if ($venueID && $venue = get_venue($venueID)) {
            $vnrow['_venue_name'] = $venue->get_title();

            // Now add the venue address info to the array
            $venueAddMetaData = maybe_unserialize($venue->get_meta_data(Apollo_DB_Schema::_APL_VENUE_ADDRESS));

            $vnrow = is_array($venueAddMetaData) ? array_merge($vnrow, $venueAddMetaData) : $vnrow;

            self::$_venues[$venueID] = $vnrow;
        }

        if ($vnrow) {
            $row1 = array_merge($row1, $vnrow);
        }

        // Get image
        if ($imgSize = !empty($args['img_size']) ? $args['img_size'] : '') {
            if (!empty($args['img_tag'])) {
                $row1['image'] = $eventObj->get_image($imgSize, array(), array(), '', false);
            }
            else {
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $eventObj->id ), $imgSize );
                $row1['image'] = $image ? $image[0] : '';
            }
        }

        $row1['post'] = $eventObj->post;

        $data = $eventObj->get_content(200);
        $row1['post_content_200'] = isset($data['text']) ? $data['text'] : '';

        return $row1;

    }

    public static function get_org_details($org_id)
    {
        global $wpdb;
        $org = $wpdb->get_results("select * from $wpdb->posts where ID = " . $org_id . " AND post_type = '".Apollo_DB_Schema::_ORGANIZATION_PT."'");
        $row = !empty($org) && is_object($org[0]) ? get_object_vars($org[0]) : array();

        $orgAddressMetaData = $wpdb->get_results("select meta_key, meta_value from $wpdb->apollo_organizationmeta where meta_key ='_apl_org_address' and apollo_organization_id = " . $org_id);
        $row1 = !empty($orgAddressMetaData) ? maybe_unserialize(maybe_unserialize($orgAddressMetaData[0]->meta_value)) : array();
        $row = array_merge($row, $row1);

        $orgMetaData = $wpdb->get_results("select meta_key, meta_value from $wpdb->apollo_organizationmeta where meta_key ='_apl_org_data' and apollo_organization_id = " . $org_id);
        $row2 = !empty($orgMetaData) ? maybe_unserialize(maybe_unserialize($orgMetaData[0]->meta_value)) : array();
        $row = array_merge($row, $row2);
        return $row;

    }

    public static function get_venue_details($ven_id)
    {
        global $wpdb;
        $ven = $wpdb->get_results("select * from $wpdb->posts where ID = " . $ven_id . " AND post_type = '".Apollo_DB_Schema::_VENUE_PT."'");
        $row = !empty($ven) && is_object($ven[0]) ? get_object_vars($ven[0]) : array();

        $venAddressMetaData = $wpdb->get_results("select meta_key, meta_value from $wpdb->apollo_venuemeta where meta_key ='_apl_venue_address' and apollo_venue_id = " . $ven_id);
        $row1 = !empty($venAddressMetaData) ? maybe_unserialize(maybe_unserialize($venAddressMetaData[0]->meta_value)) : array();
        $row = array_merge($row, $row1);

        $venMetaData = $wpdb->get_results("select meta_key, meta_value from $wpdb->apollo_venuemeta where meta_key ='_apl_venue_data' and apollo_venue_id = " . $ven_id);
        $row2 = !empty($venMetaData) ? maybe_unserialize(maybe_unserialize($venMetaData[0]->meta_value)) : array();
        $row = array_merge($row, $row2);
        return $row;

    }

    public static function get_feed_events($apid, $limit = 0, $offset = 0)
    {
        global $wpdb;

        $meta_mode = get_post_meta($apid, 'meta-mode');

        // Are we in individuasl selection mode
        if ( $meta_mode && $meta_mode[0] == 'YES') {
            $events = get_post_meta($apid, 'meta-eventID');
            $eventlist = $events ? $events[0] : array();
            $inlist = '';
            if($eventlist){
                foreach ($eventlist as $event) {
                    $inlist .= "'" . $event . "',";
                }
            }

            $inlist = substr($inlist, 0, -1);
            $events = $wpdb->get_results("SELECT ID, post_content, post_date, post_title FROM $wpdb->posts WHERE post_status = 'publish' and post_type = 'event' and ID in ($inlist)");

            return $events;

        } else {

            $metadata = get_post_meta($apid);

            $sql = "select * from $wpdb->posts wp ";

            if (!empty($metadata['meta-venues'][0])) {
                // maybe_unserialize data to use
                $venuelist = maybe_unserialize($metadata['meta-venues'][0]);
                $venuefind = '';
                $venuejoin = "em1.meta_value = ";
                foreach ($venuelist as $myven) {
                    $venuefind .= $venuejoin . $myven . " or ";
                }
                $venuefind = substr($venuefind, 0, -3);
                $sql .= "join $wpdb->apollo_eventmeta em1 on em1.apollo_event_id = wp.ID and em1.meta_key = '_apollo_event_venue' and ($venuefind) ";

            }

            if (!empty($metadata['meta-orgs'][0])) {
                // maybe_unserialize data to use
                $orglist = maybe_unserialize($metadata['meta-orgs'][0]);
                $orgfind = '';
                $orgjoin = "org_id = ";
                foreach ($orglist as $myorg) {
                    $orgfind .= $orgjoin . $myorg . " or ";
                }
                $orgfind = substr($orgfind, 0, -3);

                $sql .= "join $wpdb->apollo_event_org on is_main = 'yes' and post_id = wp.ID and ($orgfind) ";

            }

            // set up blacklist
            $blacklist = maybe_unserialize($metadata['meta-blacklist'][0]);

            if (!empty($metadata['meta-type'][0])) {
                // unserialise data to use
                $termlist = maybe_unserialize($metadata['meta-type'][0]);

                $termfind = '';
                $termjoin = "term_taxonomy_id = ";
                foreach ($termlist as $myterm) {
                    if (!empty($blacklist) && is_array($blacklist) && in_array($myterm, $blacklist)) continue;
                    $termfind .= $termjoin . $myterm . " or ";
                }
                $termfind = substr($termfind, 0, -3);

                $sql .= "join $wpdb->term_relationships tr on tr.object_id = wp.ID and ($termfind) ";
            }


            /////////////////////////////
            /// Modify SQL for ongoing
            /////////////////////////////

            $union = false;
            $ongo = "";
            $part1 = $sql;

            // Date range been selected if 0
            if ( isset($metadata['meta-range']) && $metadata['meta-range'][0] != '' && intval($metadata['meta-range'][0]) === 0) {
                // use start end date
                $union = true;
                $sdate = $metadata['meta-dateStart'][0];
                $edate = $metadata['meta-dateEnd'][0];
                $sql .= "join $wpdb->apollo_eventmeta em2 on em2.apollo_event_id = wp.ID and em2.meta_key = '_apollo_event_end_date' and em2.meta_value >= '$sdate' ";
                $sql .= "join $wpdb->apollo_eventmeta em3 on em3.apollo_event_id = wp.ID and em3.meta_key = '_apollo_event_end_date' and em3.meta_value <= '$edate' ";

            }

            $srange = date('Y-m-d', (time()));
            if ( isset($metadata['meta-range']) && $metadata['meta-range'][0] != '' && intval($metadata['meta-range'][0]) > 0) {
                // calculate today plus number of days
                $union = true;
                $days = $metadata['meta-range'][0];
                $erange = date('Y-m-d', (time() + (86400 * $days)));
                $sql .= "join $wpdb->apollo_eventmeta em2 on em2.apollo_event_id = wp.ID and em2.meta_key = '_apollo_event_end_date' and em2.meta_value >= '$srange' ";
                $sql .= "join $wpdb->apollo_eventmeta em3 on em3.apollo_event_id = wp.ID and em3.meta_key = '_apollo_event_end_date' and em3.meta_value <= '$erange' ";
            }


            $ongo .= "join $wpdb->apollo_eventmeta em2 on em2.apollo_event_id = wp.ID and em2.meta_key = '_apollo_event_start_date' and em2.meta_value <= '$srange' ";
            $ongo .= "join $wpdb->apollo_eventmeta em3 on em3.apollo_event_id = wp.ID and em3.meta_key = '_apollo_event_end_date' and em3.meta_value = '2037-01-01' ";


            $part2 = '';


            if (!empty($metadata['meta-cityzip'][0]) && $metadata['meta-cityzip'][0] == 'CITY') {
                // unserialise data to use
                $citylist = maybe_unserialize($metadata['meta-cities'][0]);
                $cityfind = '';
                $cityjoin = "vm1.meta_value like ";
                foreach ($citylist as $mycity) {
                    list ($city, $state) = explode(' ', $mycity);
                    $citylen = strlen($city);
                    $cityfind .= $cityjoin . "'%_venue_city\";s:" . $citylen . ":\"$city%'" . " or ";
                }
                $cityfind = substr($cityfind, 0, -3);
                $sql .= "inner join $wpdb->apollo_venuemeta vm1 on $cityfind ";
                $part2 .= "inner join $wpdb->apollo_venuemeta vm1 on $cityfind ";

            }


            if (!empty($metadata['meta-cityzip'][0]) && $metadata['meta-cityzip'][0] == 'ZIP') {
                // unserialise data to use
                $ziplist = maybe_unserialize($metadata['meta-zips'][0]);
                $zipfind = '';
                $zipjoin = "vm2.meta_value like ";
                foreach ($ziplist as $myzip) {
                    $ziplen = strlen($myzip);
                    $zipfind .= $zipjoin . "'%_venue_zip\";s:" . $ziplen . ":\"$myzip%'" . " or ";
                }
                $zipfind = substr($zipfind, 0, -3);
                $sql .= "inner join $wpdb->apollo_venuemeta vm2 on $zipfind ";
                $part2 .= "inner join $wpdb->apollo_venuemeta vm2 on $zipfind ";
            }


            if ($union) {

                $sql = $sql . " where wp.post_status = 'publish' and wp.post_type = 'event' group by wp.ID union " . $part1 . $ongo . $part2 . " where wp.post_status = 'publish' and wp.post_type = 'event' group by wp.ID";

                $meta_sort = get_post_meta($apid, 'meta-sort');
                $meta_sort = $meta_sort ? $meta_sort[0] : '';


                ///////////////////////////////////////////////////////////////////////////////
                /// in here we need to find the column number depending on the options selected.
                //////////////////////////////////////////////////////////////////////////////////
                if ($meta_sort == '') $sql .= " order by 31";
                if ($meta_sort == 'em3.meta_value') $sql .= " order by 31";
                if ($meta_sort == 'em2.meta_value') $sql .= " order by 31";
                if ($meta_sort == 'post_title') $sql .= " order by 6";

                //echo "l=$limit o=$offset";


                if (($limit == 0) && ($offset == 0)) {
                    $sql .= "";
                } else {
                    $sql .= " limit $limit offset $offset";
                }


            } else {

                // Sort options from syndication admin panel
                $meta_sort = get_post_meta($apid, 'meta-sort');
                $meta_sort = $meta_sort ? $meta_sort[0] : '';
                if ($meta_sort == '') $meta_sort = 'em3.meta_value';

                if (($limit == 0) && ($offset == 0)) {
                    $sql .= " where wp.post_status = 'publish' and wp.post_type = 'event' group by wp.ID order by $meta_sort";
                } else {
                    $sql .= " where wp.post_status = 'publish' and wp.post_type = 'event' group by wp.ID order by $meta_sort limit $limit offset $offset";
                }

            }

            $events = $wpdb->get_results($sql);

            if ($_SERVER['REMOTE_ADDR'] == '73.252.6.1') {
                //		echo $sql . "<br><br>"; die;
            }
            return $events;
        }

    }

    public static function getDisplayEventFieldsList($apID = 0){
        $sel_list = get_post_meta($apID, APL_Syndication_Const::META_OWNSEL, TRUE);
        $sel_type = get_post_meta($apID, APL_Syndication_Const::META_ALLOW_DISPLAY_FIELD, TRUE);
        $result = array();
        $default_sel_list = array(
            'Event Summary' => 'eventSummary',
            'Event Description' => 'eventDescription',
            'Individual Dates & Times' => 'eventDatesTimes',
            'Organization Name' => 'orgName',
            'Organization Image' => 'orgImage',
            'Venue Details' => 'venue',
            'Indicator for event being spotlighted' => 'spotlight',
            'Indicator for event being featured' => 'featured',
            'Sub Category Names' => 'secondaryType',
            'Ticket Inoformation' => 'eventTicketInfo',
            'Phone number' => 'eventPhone1',
            'Contact Email' => 'eventEmail',
            'Ticket URL' => 'eventTicketUrl',
            'Discount URL' => 'discountUrl',
            'Ticket Phone' => 'ticketPhone',
            'Start Time' => 'eventStartTime',
            'Date event created' => 'datePosted',
            'URL to event' => 'eventLink',
            'Globally Unique Identifier' => 'eventGuid'
        );
        foreach ($default_sel_list as $selected){
            $result[$selected] = FALSE;
        }
        if ($sel_type == 'OWN' && !empty($sel_list)) {
            foreach ($sel_list as $selected){
                $result[$selected] = TRUE;
            }
        } else {
            foreach ($default_sel_list as $selected){
                $result[$selected] = TRUE;
            }
        }
        return $result;
    }

    public function isAccountAvailable(){

        return true;
    }

    public function isExistingAccount(){
        global $wpdb;
        $accID = intval($this->apID);
        $sql = "SELECT * FROM ".$wpdb->posts." WHERE ID = ".$accID." AND post_status = 'publish' AND post_type = '".Apollo_DB_Schema::_SYNDICATION_PT."' ";
        $result = $wpdb->get_results($sql);
        return !empty($result);
    }

    public function getVenueTmpByEventId(&$currentRow, $eventID = 0)
    {
        global $wpdb;
        $currentRow['_venue_name'] = '';
        $currentRow['_venue_address1'] = '';
        $currentRow['_venue_address2'] = '';
        $currentRow['_venue_city'] = '';
        $currentRow['_venue_state'] = '';
        $currentRow['_venue_zip'] = '';
        $currentRow[Apollo_DB_Schema::_VENUE_LATITUDE] = '';
        $currentRow[Apollo_DB_Schema::_VENUE_LONGITUDE] = '';
        /** @Ticket #13926 */
        $currentRow[Apollo_DB_Schema::_VENUE_NEIGHBORHOOD] = '';
        $tmp_venue = $wpdb->get_results("SELECT meta_value from $wpdb->apollo_eventmeta where apollo_event_id = $eventID and meta_key = '_apl_event_tmp_venue'");
        $tmp_venue = $tmp_venue ? $tmp_venue[0] : array();
        $tmp_venue = $tmp_venue ? maybe_unserialize(maybe_unserialize($tmp_venue->meta_value)) : array();
        if (!empty($tmp_venue) && is_array($tmp_venue)) {
            $currentRow = array_replace($currentRow, $tmp_venue);
        }
    }

    public function getOrgTmpByEventId( &$currentRow, $eventID = 0){
        global $wpdb;
        $tmp_org = $wpdb->get_results("SELECT meta_value from $wpdb->apollo_eventmeta where apollo_event_id = $eventID and meta_key = '_apl_event_tmp_org'");
        $tmp_org = !empty($tmp_org) ? $tmp_org[0]->meta_value : __("","apollo");
        $currentRow['_org_name'] = $tmp_org;
    }

    public static function getValByKey($row,$key, $default = ''){
        return !empty($row) && is_array($row) && isset($row[$key]) ? $row[$key] : $default;
    }

    public static function hasSetEventPrimaryCategory($event_id = 0){
        $pc = Apollo_App::apollo_get_meta_data($event_id,Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID);
        return (!empty($pc) && intval($pc) > 0);
    }

    public static  function getDataSearchFilter($key = ''){
        return isset($_GET[$key]) && !empty($_GET[$key]) ? $_GET[$key] : '';
    }

    /**
     * This function for export-xml  and export-wp use one filter event calendar function
     * @author Trilm
     * @param array $args
     * @return array|null|object
     * @ModifiedBy ThienLD
     * @ModifiedDescription limit render individual datetime for ongoing events to the range is configured in syndication setting
     */
    public function getEventCalendar($args = []){
        global $wpdb;
        $curDate = current_time('Y-m-d');
        $curTime = current_time('H:s');
        $postID = self::getValByKey($args,'postID',0);
        $offset = self::getValByKey($args,'offset',false);
        $limit = self::getValByKey($args,'limit',false);
        // end-date are configured and calculated from date-range of current setting in each specific Syndication account
        $endDate = self::getValByKey($args,'endDate',$curDate);

        $sql = "

            SELECT SQL_CALC_FOUND_ROWS
                date_event ,
                time_from ,
                time_to
            FROM $wpdb->apollo_event_calendar
            WHERE

            event_id = '%s'

            AND time_from <> ''

            AND (

                ( date_event > '%s' AND date_event <= '%s' )   

                OR
                (
                    date_event = '%s'
                    AND (
                        (time_to >= '%s' AND time_to <> '' and time_to is not null)
                        or
                        ( (time_to = '' or time_to is null) and (time_from <> '' and time_from is not null) )
                    )

                )
            )
        ";

        if (intval($offset) !== false && $limit) {
            $sql .= " LIMIT $offset, $limit";
        }

        $event = $wpdb->get_results($wpdb->prepare($sql, $postID, $curDate, $endDate, $curDate, $curTime, $curTime));

        $this->totalDateTime =$wpdb->get_var("SELECT FOUND_ROWS()");

        return $event;
    }

    /**
     * @author Trilm
     * @param $postID
     * @param $evt
     * @return array
     * this function for export-xml  and export-wp use one  output event calendar  function
     */
    public function outputExportResultItem($postID,$evt){

        $eventid = $postID;
        $event_time = $evt->time_from;
        $event_to = $evt->time_to;
        $event_day = substr($evt->date_event,8,2);
        $event_month = substr($evt->date_event,5,2);
        $event_year = substr($evt->date_event,0,4);
        $duration = '';
        $eventTimeTo = '23:59';
        if (!empty($event_to)){
            $time1 = strtotime($event_time);
            $time2 = strtotime($event_to);
            $eventTimeTo = $event_to;
            $time3 = ($time2 - $time1);
            if ($time3 > 0) {$duration = $time3;} else {$duration = '';}
        }

        $mytime = ( date("g:i a", strtotime("$event_time")) );

        $mydate = date('D-M-j-Y', strtotime($evt->date_event));

//				$date1 = date('Y-m-d', strtotime($evt->date_event));
//				$date2 = date('Y-m-d');
//				if ($date1 < $date2) continue;



        list ($dayname, $month, $day, $year) = explode('-', $mydate);

        $date_filter = $event_year.$event_month.$event_day;
        $hour = substr($event_time, 0,2);
        $min  = substr($event_time, 3,2);
        $unix_time = mktime($hour, $min, 00, $event_month, $event_day, $event_year);
        $myend = strtotime($eventTimeTo);
        $myend = ( date("g:i a", $myend) );

        if ($duration == '') $duration = 7200;

        return array(
            'date'	=> $mydate,
            'time'	=> $mytime,
            'timestamp'	=> $unix_time,
            'date_filter'	=> $date_filter,
            'end_time'	=> $myend,
            'duration'	=> $duration,
        );
    }

    /**
     * @Description Check the categories of an event exists or NOT in the selected event-types of a Syndication account
     * @param null $event
     * @param array $eventTypes
     * @param bool $syndicationMeta
     * @return bool
     * @CreatedBy ThienLD
     * @CreatedDate June 8th, 2017
     */
    protected static function isExistedInEventType($event = null, $eventTypes = [], $syndicationMeta = false){

        // No need to check with individual mode
        if ($syndicationMeta && isset($syndicationMeta['meta-mode']) && $syndicationMeta['meta-mode'][0] === 'YES') {
            return true;
        }

        if( empty($event) || empty($eventTypes)){
            return false;
        }

        $categoriesInEvent = self::getValByKey($event,'terms',[]);
        return (is_array($eventTypes) && array_intersect($categoriesInEvent, $eventTypes));
    }

    /**
     * @Description Check the categories of an event exists or NOT in the selected BLACKLIST event-types of a Syndication account
     * @param null $event
     * @param array $eventBlacklistTypes
     * @return bool
     * @CreatedBy ThienLD
     * @CreatedDate June 8th, 2017
     */
    protected static function isExistedInEventBlacklistType($event = null, $eventBlacklistTypes = []){
        if( empty($event) || empty($eventBlacklistTypes)){
            return false;
        }

        $categoriesInEvent = self::getValByKey($event,'terms',[]);
        return (is_array($eventBlacklistTypes) && array_intersect($categoriesInEvent, $eventBlacklistTypes));
    }

    /**
     * @ticket #17649: [CF] 20180921 - [Syndication] Add a new HTML feed
     * @param $startDate
     * @param $endDate
     * @param $eventId
     * @return string
     */
    public function renderDateTime($startDate, $endDate, $eventId){

        $apl_query = new Apl_Query( Apollo_Tables::_APL_EVENT_CALENDAR );

        $data = $apl_query->get_where("event_id=$eventId", 'distinct time_from', '', "order by time_from");

        $startTimes = "";
        if(!empty($data)){
            foreach ($data as $index => $time){
                if($index){
                    $startTimes .= ', ';
                }
                $startTimes .= date("h:i a", strtotime($time->time_from));
            }
        }

        if ($startDate == $endDate){
            $strDateTime = Apollo_App::apl_date('M d, Y',strtotime($startDate)) . " " .  __("at", "apollo") . " " . $startTimes;
        }
        else{
            $strDateTime = Apollo_App::apl_date('M d, Y',strtotime($startDate)) .  " - "
                . Apollo_App::apl_date('M d, Y',strtotime($endDate)) . " " .  __("at", "apollo") . " " . $startTimes;
        }

        return $strDateTime;
    }

}

