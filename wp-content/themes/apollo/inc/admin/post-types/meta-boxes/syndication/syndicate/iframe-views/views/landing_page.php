<link rel="stylesheet" href="<?php echo SYND_ROOT_URL ?>/tooltipster/dist/css/tooltipster.bundle.min.css" type="text/css" media="screen, projection"/>
<link rel="stylesheet" href="<?php echo SYND_ROOT_URL ?>/css/style.css" type="text/css" media="screen, projection"/>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<script src="<?php echo SYND_ROOT_URL ?>/tooltipster/dist/js/tooltipster.bundle.min.js"></script>
<script src="<?php echo SYND_ROOT_URL ?>/js/script.js"></script>
<script>
    function expandParentDiv(el) {

        var $collapsed = $(el).closest('div.collapsed_content');

        $collapsed.siblings('div.expanded_content').toggle();
        ;

        $collapsed.toggle();

    }


    function collapseParentDiv(el) {

        var $expanded = $(el).closest('div.expanded_content');

        $expanded.siblings('div.collapsed_content').toggle();

        $expanded.toggle();

    }
</script>
<div class="feature_events">
    <?php
    function get_Month($date)
    {
        return date("M", strtotime($date));
    }

    function get_Day($date)
    {
        $day = substr($date, 8, 2);
        return $day;
    }

    if (!empty($myevents)) {
        $countI = 0;
        $a_items = array();
        ob_start();
        include (APOLLO_ADMIN_SYNDICATE_IFRAME_DIR . '/views/partial/_tooltip.php');
        $content_tooltip = ob_get_contents();
        ob_end_clean();

        $template_tooltip = '<div class="template-tool">';
        foreach ($myevents as $eItem) {
            $countI++;

            $a_items[$countI]['img'] = $eItem->image;
            $a_items[$countI]['daysp'] = get_Day($eItem->dateBegin);
            $a_items[$countI]['monthSp'] = get_Month($eItem->dateBegin);
            $a_items[$countI]['name'] = $eItem->name;
            $a_items[$countI]['ticketURL'] = $eItem->ticketURL;
            $a_items[$countI]['description'] = $eItem->description;
            $a_items[$countI]['link'] = $eItem->link;
            $a_items[$countI]['googleMapAddress'] = $eItem->googleMapAddress;
            $a_items[$countI]['event_id'] = $eItem->ID;

            $template_tooltip .= str_replace(
                array('{event_name}', '{event_image}', '{event_content}', '{org_venue}', '{event_id}', '{dates}'),
                array($eItem->name, $eItem->image, wp_trim_words($eItem->description, 150), $eItem->org_venue, $eItem->ID, APL::dateUnionDateShort($eItem->dateBegin, $eItem->dateEnd)),
                $content_tooltip);
        }
        $template_tooltip .= '</div>';
        ?>
        <div class="event_container">
            <div class="header_event_row">
                <?php
                for ($j = 1; $j <= 3; $j++) :
                    if (isset($a_items[$j])):
                        $link = $a_items[$j]['link'];
                        ?>
                        <div class="imgdiv col">
                            <a <?php echo apply_filters('linkest', $link) ?> href="<?php echo $link; ?>  "><?php echo $a_items[$j]['img']; ?></a>

                            <div class="monthDate">

                                <div class="daySp"><?php echo $a_items[$j]['daysp'] ?></div>
                                <div class="monthSp"><?php echo $a_items[$j]['monthSp'] ?></div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endfor; ?>
            </div>

            <div class="event_title_row">
                <?php
                for ($j = 1; $j <= 3; $j++) :
                    if (isset($a_items[$j])):
                        $link = $a_items[$j]['link'];
                        ?>
                        <div class="col ">
                            <h5 class="eli-event-tooltip-item-wrapper event_name" data-tooltip-content="#tooltip-content_<?php echo $a_items[$j]['event_id'] ?>"><a <?php echo apply_filters('linkest', $link) ?>
                                    href="<?php echo $link; ?>  "
                                ><?php echo $a_items[$j]['name']; ?>  </a>
                            </h5>
                        </div>
                    <?php endif; ?>
                <?php endfor; ?>
            </div>

            <div class="description_row">
                <?php
                for ($j = 1; $j <= 3; $j++) :
                    if (isset($a_items[$j])):
                        ?>
                        <div class="col">
                            <div class="collapsed_content">
                                <?php echo  substr($a_items[$j]['description'], 0, 100); ?>
                                <?php if (strlen($a_items[$j]['description']) > 100) { ?>
                                <span class="see_all"><a href="javascript:void(0);" onClick="expandParentDiv(this);">(more)</a></span>
                            </div>
                            <div class="expanded_content" style="display:none">
                                <p>
                                    <?php echo $a_items[$j]['description']; ?>
                                    <br/>

                                <div class="see_all"><a href="javascript:void(0);" onClick="collapseParentDiv(this);">collapse
                                        [-]</a></div>
                                <?php } ?>
                                </p>
                            </div>
                            <br><a target="_blank"
                                   href="<?php echo  'http://maps.google.com/maps?f=q&hl=en&ie=UTF8om=1&q=' . urlencode($a_items[$j]['googleMapAddress']); ?>"><img
                                    title="Map it on google" src='<?php echo  $mapimg; ?>' width='20' height='20'/></a>&nbsp;
                            <?php
                            //echo do_shortcode('[feather_share skin="wheel" size="20" show="google_plus, twitter, facebook, pinterest" hide="mail, reddit, linkedin"]');
                            ?>
                        </div>
                    <?php endif; ?>
                <?php endfor; ?>
            </div>

        </div>
        <?php
        echo '<div style="border-bottom:2px solid #d6d6d6;height:0px;margin-bottom:50px;margin-top:50px;clear:both">&nbsp;</div>';
        ?>
        <div class="event_container">
            <div class="header_event_row">
                <?php
                for ($j = 4; $j <= 6; $j++) :
                    if (isset($a_items[$j])):
                        $link = $a_items[$j]['link'];
                        ?>
                        <div class="imgdiv col">
                            <a <?php echo apply_filters('linkest', $link) ?> href="<?php echo $link; ?>  "><?php echo $a_items[$j]['img']; ?></a>

                            <div class="monthDate">
                                <div class="daySp"><?php echo $a_items[$j]['daysp'] ?></div>
                                <div class="monthSp"><?php echo $a_items[$j]['monthSp'] ?></div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endfor; ?>
            </div>

            <div class="event_title_row">
                <?php
                for ($j = 4; $j <= 6; $j++) :
                    if (isset($a_items[$j])):

                        $link = $a_items[$j]['link'];
                        ?>
                        <div class="col">
                            <h5 class="eli-event-tooltip-item-wrapper event_name" data-tooltip-content="#tooltip-content_<?php echo $a_items[$j]['event_id'] ?>"><a <?php echo apply_filters('linkest', $link) ?>
                                        href="<?php echo $link; ?>  "
                                ><?php echo $a_items[$j]['name']; ?>  </a>
                            </h5>
                        </div>
                    <?php endif; ?>
                <?php endfor; ?>
            </div>

            <div class="description_row">
                <?php
                for ($j = 4; $j <= 6; $j++) :
                    if (isset($a_items[$j])):
                        ?>
                        <div class="col">
                            <div class="collapsed_content">
                                <?php echo  substr($a_items[$j]['description'], 0, 100); ?>
                                <?php if (strlen($a_items[$j]['description']) > 100) { ?>
                                <span class="see_all"><a href="javascript:void(0);" onClick="expandParentDiv(this);">(more)</a></span>
                            </div>
                            <div class="expanded_content" style="display:none">
                                <p>
                                    <?php echo $a_items[$j]['description']; ?>
                                    <br/>

                                <div class="see_all"><a href="javascript:void(0);" onClick="collapseParentDiv(this);">collapse
                                        [-]</a></div>
                                <?php } ?>
                                </p>
                            </div>
                            <br><a target="_blank"
                                   href="<?php echo  'http://maps.google.com/maps?f=q&hl=en&ie=UTF8om=1&q=' . urlencode($a_items[$j]['googleMapAddress']); ?>"><img
                                    title="Map it on google" src='<?php echo  $mapimg; ?>' width='20' height='20'/></a>&nbsp;
                            <?php
                            //echo do_shortcode('[feather_share skin="wheel" size="20" show="google_plus, twitter, facebook, pinterest" hide="mail, reddit, linkedin"]');
                            ?>
                        </div>
                    <?php endif; ?>
                <?php endfor; ?>
            </div>

        </div>
        <?php
    }
    ?>
</div>
<br/><br/>
<div class="viewmore">
    <a <?php
    $link = isset($calhome2) ? $calhome2 : '#';
    echo apply_filters('linkest', $link) ?> href="<?php echo $link; ?>"><?php echo __("View more events >", "apollo"); ?></a>
</div>

<?php echo $template_tooltip; ?>


