<?php
/**
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Syndication_Result
 */
class Apollo_Meta_Box_Syndication_Result {

	/**
	 * Output the metabox
	 */
	public static function output( $post ) {
        global $wpdb;
		wp_nonce_field( 'apollo_syndication_meta_nonce', 'apollo_syndication_meta_nonce' );
        $thepostid = $post->ID;
		$currentDomainName = get_site_url();
		echo '<div class="options_group event-box">';
		// Account ID
		apollo_wp_info_field(  array(
			'label' => __( '', 'apollo' ),
			'desc_tip' => 'true',
			'description' => "",
			'class' => '',
			'text' => '<p>'.__("To use the ","apollo").'<strong>'.__("TEST URL or TEST FILE link","apollo").'</strong>'.__(" please save your changes before clicking the link.","apollo").'</p>'
		) );
		echo '</div>';

		/* XML */
		echo '<div class="wrap-op-blk">';
			echo '<h3>'.__("XML Generation").'</h3>';
			echo '<div class="options_group clear">';
			$xmlEngUrl = $currentDomainName . '/?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid='.$thepostid.'&aplang=eng&apoutput=xml';
			apollo_wp_text_input(  array(
				'id' => 'op-xml-english',
				'label' => __( 'English', 'apollo' ),
				'value' => $xmlEngUrl,
			) );
			echo '<a class="button button-small"  href="'.$xmlEngUrl.'" target=_blank>'.__("TEST URL","apollo").'</a>';
			echo '</div>';
			if(Apollo_App::hasWPLM()){
				echo '<div class="options_group clear">';
				$xmlEspUrl = $currentDomainName . '/?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid='.$thepostid.'&aplang=esp&apoutput=xml';
				apollo_wp_text_input(  array(
					'id' => 'op-xml-esp-lang',
					'label' => __( 'Spanish', 'apollo' ),
					'value' => $xmlEspUrl,
				) );
				echo '<a class="button button-small"  href="'.$xmlEspUrl.'" target=_blank>'.__("TEST URL","apollo").'</a>';
				echo '</div>';
			}
		echo '</div>';
		echo '<hr/>';


		/* MAILCHIMP */
		echo '<div class="wrap-op-blk">';
			echo '<h3>'.__("MAILCHIMP Generation").'</h3>';
			echo '<div class="options_group clear">';
			$mcpEngUrl = $currentDomainName . '/?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid='.$thepostid.'&aplang=eng&apoutput=mcp';
			apollo_wp_text_input(  array(
				'id' => 'op-mailchimp-english',
				'label' => __( 'English', 'apollo' ),
				'value' => $mcpEngUrl,
			) );
			echo '<a class="button button-small"  href="'.$mcpEngUrl.'" target=_blank>'.__("TEST URL","apollo").'</a>';
			echo '</div>';
			if(Apollo_App::hasWPLM()){
				echo '<div class="options_group clear">';
				$mcpEspUrl = $currentDomainName . '/?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid='.$thepostid.'&aplang=esp&apoutput=mcp';
				apollo_wp_text_input(  array(
					'id' => 'op-mailchimp-esp-lang',
					'label' => __( 'Spanish', 'apollo' ),
					'value' => $mcpEspUrl,
				) );
				echo '<a class="button button-small"  href="'.$mcpEspUrl.'" target=_blank>'.__("TEST URL","apollo").'</a>';
				echo '</div>';
			}
		echo '</div>';
		echo '<hr/>';

		/* RSS */
		echo '<div class="wrap-op-blk">';
			echo '<h3>'.__("RSS Generation").'</h3>';
			echo '<div class="options_group clear">';
			$rssEngUrl = $currentDomainName . '/?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid='.$thepostid.'&aplang=eng&apoutput=rss';
			apollo_wp_text_input(  array(
				'id' => 'op-rss-english',
				'label' => __( 'English', 'apollo' ),
				'value' => $rssEngUrl,
			) );
			echo '<a class="button button-small"  href="'.$rssEngUrl.'" target=_blank>'.__("TEST URL","apollo").'</a>';
			echo '</div>';
			if(Apollo_App::hasWPLM()){
				echo '<div class="options_group clear">';
				$rssEspUrl = $currentDomainName . '/?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid='.$thepostid.'&aplang=esp&apoutput=rss';
				apollo_wp_text_input(  array(
					'id' => 'op-rss-esp-lang',
					'label' => __( 'Spanish', 'apollo' ),
					'value' => $rssEspUrl,
				) );
				echo '<a class="button button-small"  href="'.$rssEspUrl.'" target=_blank>'.__("TEST URL","apollo").'</a>';
				echo '</div>';
			}
		echo '</div>';
		echo '<hr/>';

		/* ICAL */
		echo '<div class="wrap-op-blk">';
			echo '<h3>'.__("ICAL Generation").'</h3>';
			echo '<div class="options_group clear">';
			$icalEngUrl = $currentDomainName . '/?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid='.$thepostid.'&aplang=eng&apoutput=ical';
			$icalFileEngUrl = $currentDomainName . '/?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid='.$thepostid.'&aplang=eng&apoutput=icalfile';
			apollo_wp_text_input(  array(
				'id' => 'op-ical-english',
				'label' => __( 'English', 'apollo' ),
				'value' => $icalEngUrl,
			) );
			echo '<a class="button button-small"  href="'.$icalEngUrl.'" target=_blank>'.__("TEST URL","apollo").'</a>';
			echo '|<a class="button button-small"  href="'.$icalFileEngUrl.'" target=_blank>'.__("TEST FILE","apollo").'</a>';
			echo '</div>';
			if(Apollo_App::hasWPLM()){
				echo '<div class="options_group clear">';
				$icalEspUrl = $currentDomainName . '/?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid='.$thepostid.'&aplang=esp&apoutput=ical';
				$icalFileEspUrl = $currentDomainName . '/?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid='.$thepostid.'&aplang=esp&apoutput=icalfile';
				apollo_wp_text_input(  array(
					'id' => 'op-ical-esp-lang',
					'label' => __( 'Spanish', 'apollo' ),
					'value' => $icalEspUrl,
				) );
				echo '<a class="button button-small"  href="'.$icalEspUrl.'" target=_blank>'.__("TEST URL","apollo").'</a>';
				echo '|<a class="button button-small"  href="'.$icalFileEspUrl.'" target=_blank>'.__("TEST FILE","apollo").'</a>';
				echo '</div>';
			}
		echo '</div>';
		echo '<hr/>';

		/* WORDPRESS */
		echo '<div class="wrap-op-blk">';
			echo '<h3>'.__("Wordpress Generation").'</h3>';
			echo '<div class="options_group clear">';
			$wpEventEngUrl = $currentDomainName . '/?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid='.$thepostid.'&aplang=eng&apoutput=wp&aptype=event';
			apollo_wp_text_input(  array(
				'id' => 'op-wp-event-english',
				'label' => __( 'Event - English', 'apollo' ),
				'value' => $wpEventEngUrl,
			) );
			echo '<a class="button button-small"  href="'.$wpEventEngUrl.'" target=_blank>'.__("TEST URL","apollo").'</a>';
			echo '</div>';
			echo '<div class="options_group clear">';
			$wpCategoryEngUrl = $currentDomainName . '/?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid='.$thepostid.'&aplang=eng&apoutput=wp&aptype=cat';
			apollo_wp_text_input(  array(
				'id' => 'op-wp-cat-english',
				'label' => __( 'Category - English', 'apollo' ),
				'value' => $wpCategoryEngUrl,
			) );
			echo '<a class="button button-small"  href="'.$wpCategoryEngUrl.'" target=_blank>'.__("TEST URL","apollo").'</a>';
			echo '</div>';
			if(Apollo_App::hasWPLM()){
				echo '<div class="options_group clear">';
				$wpEventEspUrl = $currentDomainName . '/?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid='.$thepostid.'&aplang=esp&apoutput=wp&aptype=event';
				apollo_wp_text_input(  array(
					'id' => 'op-wp-event-spanish',
					'label' => __( 'Event - Spanish', 'apollo' ),
					'value' => $wpEventEspUrl,
				) );
				echo '<a class="button button-small"  href="'.$wpEventEspUrl.'" target=_blank>'.__("TEST URL","apollo").'</a>';
				echo '</div>';
				echo '<div class="options_group clear">';
				$wpCategoryEspUrl = $currentDomainName . '/?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid='.$thepostid.'&aplang=esp&apoutput=wp&aptype=cat';
				apollo_wp_text_input(  array(
					'id' => 'op-wp-cat-spanish',
					'label' => __( 'Category - Spanish', 'apollo' ),
					'value' => $wpCategoryEspUrl,
				) );
				echo '<a class="button button-small"  href="'.$wpCategoryEspUrl.'" target=_blank>'.__("TEST URL","apollo").'</a>';
				echo '</div>';
			}
		echo '</div>';
		echo '<hr/>';

		/* Email Template */
		echo '<div class="wrap-op-blk">';
			echo '<h3>'.__("Email Template Generation").'</h3>';
			echo '<div class="options_group clear">';
			$emailTplSHEngUrl = $currentDomainName . '/?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid='.$thepostid.'&aplang=eng&apoutput=etpl&aptype=html';
			apollo_wp_text_input(  array(
				'id' => 'op-wp-email-tpl-sh-english',
				'label' => __( 'Simple HTML - English', 'apollo' ),
				'value' => $emailTplSHEngUrl,
			) );
			echo '<a class="button button-small"  href="'.$emailTplSHEngUrl.'" target=_blank>'.__("TEST URL","apollo").'</a>';
			echo '</div>';
			echo '<div class="options_group clear">';
			$emailTplPTEngUrl = $currentDomainName . '/?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid='.$thepostid.'&aplang=eng&apoutput=etpl&aptype=text';
			apollo_wp_text_input(  array(
				'id' => 'op-wp-email-tpl-pt-english',
				'label' => __( 'Plain Text - English', 'apollo' ),
				'value' => $emailTplPTEngUrl,
			) );
			echo '<a class="button button-small"  href="'.$emailTplPTEngUrl.'" target=_blank>'.__("TEST URL","apollo").'</a>';
			echo '</div>';
			echo '<div class="options_group clear">';
			$emailTplPTEngUrl = $currentDomainName . '/?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid='.$thepostid.'&aplang=eng&apoutput=etpl&aptype=e-html';
			apollo_wp_text_input(  array(
				'id' => 'op-wp-email-tpl-eh-english',
				'label' => __( 'Expanded HTML - English', 'apollo' ),
				'value' => $emailTplPTEngUrl,
			) );
			echo '<a class="button button-small"  href="'.$emailTplPTEngUrl.'" target=_blank>'.__("TEST URL","apollo").'</a>';
			echo '</div>';
			echo '<div class="options_group clear">';
			$emailTplPTEngUrl = $currentDomainName . '/?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid='.$thepostid.'&aplang=eng&apoutput=etpl&aptype=e-html2';
			apollo_wp_text_input(  array(
				'id' => 'op-wp-email-tpl-eh-english',
				'label' => __( 'Expanded HTML 2 - English', 'apollo' ),
				'value' => $emailTplPTEngUrl,
			) );
			echo '<a class="button button-small"  href="'.$emailTplPTEngUrl.'" target=_blank>'.__("TEST URL","apollo").'</a>';
			echo '</div>';
			if(Apollo_App::hasWPLM()){
				echo '<div class="options_group clear">';
				$emailTplSHEspUrl = $currentDomainName . '/?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid='.$thepostid.'&aplang=esp&apoutput=etpl&aptype=html';
				apollo_wp_text_input(  array(
					'id' => 'op-wp-email-tpl-sh-spanish',
					'label' => __( 'Simple HTML - Spanish', 'apollo' ),
					'value' => $emailTplSHEspUrl,
				) );
				echo '<a class="button button-small"  href="'.$emailTplSHEspUrl.'" target=_blank>'.__("TEST URL","apollo").'</a>';
				echo '</div>';
				echo '<div class="options_group clear">';
				$emailTplPTEspUrl = $currentDomainName . '/?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid='.$thepostid.'&aplang=esp&apoutput=etpl&aptype=text';
				apollo_wp_text_input(  array(
					'id' => 'op-wp-email-tpl-pt-spanish',
					'label' => __( 'Plain Text - Spanish', 'apollo' ),
					'value' => $emailTplPTEspUrl,
				) );
				echo '<a class="button button-small"  href="'.$emailTplPTEspUrl.'" target=_blank>'.__("TEST URL","apollo").'</a>';
				echo '</div>';
				echo '<div class="options_group clear">';
				$emailTplPTEspUrl = $currentDomainName . '/?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid='.$thepostid.'&aplang=esp&apoutput=etpl&aptype=e-html';
				apollo_wp_text_input(  array(
					'id' => 'op-wp-email-tpl-eh-spanish',
					'label' => __( 'Expanded HTML - Spanish', 'apollo' ),
					'value' => $emailTplPTEspUrl,
				) );
				echo '<a class="button button-small"  href="'.$emailTplPTEspUrl.'" target=_blank>'.__("TEST URL","apollo").'</a>';
				echo '</div>';
				echo '<div class="options_group clear">';
				$emailTplPTEspUrl = $currentDomainName . '/?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid='.$thepostid.'&aplang=esp&apoutput=etpl&aptype=e-html2';
				apollo_wp_text_input(  array(
					'id' => 'op-wp-email-tpl-eh-spanish',
					'label' => __( 'Expanded HTML - Spanish', 'apollo' ).' 2',
					'value' => $emailTplPTEspUrl,
				) );
				echo '<a class="button button-small"  href="'.$emailTplPTEspUrl.'" target=_blank>'.__("TEST URL","apollo").'</a>';
				echo '</div>';
			}
		echo '</div>';


	}

	/**
	 * Save meta box data
	 */
	public static function save( $post_id, $post ) {
       // only display for testing url, so not save any meta data
	}
}
