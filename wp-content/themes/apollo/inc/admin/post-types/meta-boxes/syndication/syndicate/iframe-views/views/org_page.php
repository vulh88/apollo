<div id="content">
    <div id="content_main_inner">
        <div id="description">
            <div id="event_image" class="event_image_div" style="float: left; margin-right: 8px; border: 0px;"/>
            {image}
        </div>
        <h4 class="name"><?php echo  $orgname; ?></h4>

        <p><?php echo  $org_description; ?></p>
    </div>

    <br style="clear:both;"/>

    <div id="info">
        <h3><span>Contact Info</span></h3>

        <p><?php echo  $orgname; ?></p>

        <p><?php echo  $org_address1; ?><br>
            <?php echo  $org_city; ?>, <?php echo  $org_state; ?> <?php echo  $org_zip; ?></p>
        <br/>
        <?php if (!empty($org_phone)) : ?><p><strong>Phone:</strong> <?php echo  $org_phone; ?> |<?php endif; ?>
            <?php if (!empty($org_email)) : ?><a href="mailto:<?php echo  $org_email; ?>"><strong>Email</strong></a> |<?php endif; ?>
            <?php if (!empty($org_url)) : ?><a href="<?php echo  $org_url; ?>" target="_blank"><strong>Official
                    Website</strong></a></p><?php endif; ?>

        <h3><span>Location &amp; Map</span></h3>


        <?php if (!empty($lat)) : ?>
            <script src="http://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
            <script>
                var map;
                function initialize() {
                    var mapOptions = {
                        zoom: 15,
                        center: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $long; ?>),
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    map = new google.maps.Map(document.getElementById('map-canvas'),
                        mapOptions);

                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(<?php echo $lat; ?>, <?php echo $long; ?>),
                        map: map,
                        title: '<?php echo $orgname; ?>'
                    });

                }

                google.maps.event.addDomListener(window, 'load', initialize);
            </script>
            <div id="map-canvas" style="height:300px;width:430px;"></div>
        <?php endif; ?>


        <!--/htdig_noindex-->
        <?php if (!empty($org_supporters)) : ?>
            <!--htdig_noindex-->
            <div id="sponsors_info">
                <h6><span>Sponsors</span></h6>
                <ul id="sponsors">
                    <?php echo  $org_supporters; ?>
                </ul>
            </div>
        <?php endif; ?>

        <?php if (!empty($media_gallery)): ?>
            <div id="additional_info">
                <!--htdig_noindex-->
                <h3><span>Additional Info</span></h3>
                <?php echo  $media_gallery ?>
                <!--htdig_noindex-->
            </div>
        <?php endif; ?>
    </div>
</div>
</div>
<br/>
<div class="advertisement"><?php echo  html_entity_decode($widlower) ?></div>
<br/>
</div>
