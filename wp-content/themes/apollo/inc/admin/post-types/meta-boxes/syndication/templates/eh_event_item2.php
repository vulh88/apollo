<?php
$event_id = $template_args['event_id'];
$display_fields = $template_args['display_fields'];
$e = get_event($event_id);
$objectClass = $template_args['objectClass'];
$event_end_date = $e->get_meta_data(Apollo_DB_Schema::_APOLLO_EVENT_END_DATE);
$summary = $e->get_full_content();
$admission_detail  = $e->getMetaInMainData( Apollo_DB_Schema::_ADMISSION_DETAIL );
$daw = $e->get_meta_data(Apollo_DB_Schema::_APOLLO_EVENT_DAYS_OF_WEEK, Apollo_DB_Schema::_APOLLO_EVENT_DATA);
$eventDaysAssoc = array(0 => 'Sunday',1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday');
$eventDays = '';
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $event_id ), "medium" );
$image_src = $image ? $image[0] : '';

if($daw){
foreach ($daw as $day) {
	$eventDays .= $eventDaysAssoc[$day]. ",";
			}
		}
$eventDays = substr($eventDays,0, -1);


if (!$objectClass->isExpiredTime($event_id)) :
    $event_name = !empty($e) ? $e->post->post_title : '';
    $event_link = get_permalink($event_id);

    $event_start_date = $e->get_meta_data(Apollo_DB_Schema::_APOLLO_EVENT_START_DATE);

    $event_date =  $objectClass->renderDateTime($event_start_date, $event_end_date, $event_id);
    ?>

<div class="syndication-expanded-html">
<img src="<?php echo $image_src;?>"><br />
    <a target="<?php echo apply_filters('linkest', '') ?>" href="<?php echo $event_link; ?>"><b><?php echo $event_name; ?></b></a><br />
     <?php echo $eventDays; ?> <br />
    <?php echo sprintf("%s",$event_date)?> <br />
    <?php echo Apollo_App::convertContentEditorToHtml($admission_detail); ?>
    <?php echo $e->renderOrgVenueHtml(true, false, true) ?><br/>
    <?php echo $summary; ?><br/>

</div>
<?php endif;
