<?php
$title = isset($template_args['title']) ? $template_args['title']: '';
$spot = isset($template_args['spot']) ? $template_args['spot']: '';
$feat_pos = isset($template_args['feat_pos']) ? $template_args['feat_pos'] : '';
$feat = isset($template_args['feat']) ? $template_args['feat'] : '';
$orgname = isset($template_args['orgname']) ? $template_args['orgname'] : '';
$venuename = isset($template_args['venuename']) ? $template_args['venuename'] : '';
$start = isset($template_args['start']) ? $template_args['start'] : '';
$end = isset($template_args['end']) ? $template_args['end']: '';
$istatus = isset($template_args['istatus']) ? $template_args['istatus'] : '';
$name = isset($template_args['type']) ? $template_args['type'] : '';
$id = isset($template_args['ID']) ? $template_args['ID'] : '';

?>

<tr>
    <td align=center><?php echo $istatus ?></td>
    <td align=center><input type=checkbox name='meta-spot[]' value='<?php echo $id ?>' <?php echo $spot ?>></td>
    <td align=center nowrap><input type=checkbox name='meta-feat[]' value='<?php echo $id ?>' <?php echo $feat ?>> <?php echo $feat_pos ?></td>
    <td align=center><?php echo $title ?></td>
    <td align=center><?php echo $name ?></td>
    <td align=center><?php echo $orgname ?></td>
    <td align=center><?php echo $venuename ?></td>
    <td align=center><?php echo $start ?></td>
    <td align=center><?php echo $end ?></td>
</tr>