<?php

$individualEvents = isset($template_args['individualEvents']) && !empty($template_args['individualEvents']) ? $template_args['individualEvents'] : array();
$synAccID = isset($template_args['accountID']) && !empty($template_args['accountID']) ? $template_args['accountID'] : '';

if(!empty($individualEvents)){
    $index = 0;
    $numCol = 4;
    $itemPerCol = intval(count($individualEvents) / $numCol);
    $totalItem = count($individualEvents);
    $selectedValue = Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($synAccID,APL_Syndication_Const::META_EVENT_IDS,array());
    echo '<div class="wrap-cz-wrap-line">';
        echo '<div class="wrap-cz-child">';
            foreach ( $individualEvents as $e ):

                $e = get_event($e);

                // Description
                apollo_wp_multi_checkbox(  array(
                    'id'            => '' ,
                    'name'          => ''. APL_Syndication_Const::META_EVENT_IDS . '[]',
                    'label'         => $e->get_title(),
                    'desc_tip'      => 'true',
                    'description'   => '',
                    'type'          => 'checkbox',
                    'default'       => false,
                    'value'			=> $selectedValue,
                    'cbvalue'       => $e->id ) );
                $index += 1;

                echo '<span class="sch-date">';
                echo $e->render_sch_date();
                echo '</span>';

                if($itemPerCol > 0 && $index < $totalItem && $index % $itemPerCol === 0){
                    echo '</div>';
                    echo '<div class="wrap-cz-child">';
                }
            endforeach;
        echo '</div>';
    echo '</div>';
}