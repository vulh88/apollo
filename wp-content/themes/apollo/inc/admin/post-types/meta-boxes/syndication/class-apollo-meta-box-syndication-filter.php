<?php
/**
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

require_once APOLLO_ADMIN_SYNDICATE_DIR . '/class-apollo-syndicate-abstract.php';
require_once APOLLO_ADMIN_SYNDICATE_DIR. '/class-apollo-syndication-helper.php';

/**
 * Apollo_Meta_Box_Syndication_Filter
 */
class Apollo_Meta_Box_Syndication_Filter {

	const INDIVIDUAL_EVENTS_PAGESIZE = 24;
	public static $syndicationHelper = null;
	public static $totalSpotFeatEvents = 0;
		/**
	 * Output the metabox
	 */
	public static function output( $post ) {
		global $wpdb;
		wp_nonce_field( 'apollo_syndication_settings_meta_nonce', 'apollo_syndication_settings_meta_nonce' );
		if(self::$syndicationHelper === null){
			self::$syndicationHelper = new Apollo_Syndicate_Helper();
		}
		$thepostid = $post->ID;
		$currentDomainName = get_site_url();
		echo '<h3>'.__("Filtering criteria").'</h3>';

		$isDisplayIndEvents = Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_MODE,'NO') == 'YES';

		if($isDisplayIndEvents) :
			/* Display list events for mode individual event selection */
			$individualEvents = self::getIndividualEvents();
			if ( !empty($individualEvents) ){
				echo '<div id="blk-individual-events" class="options_group clear wrap-blk ct-full-width">';
				echo '<label class="ct-lb-mtc">'. __('Select Individual Events from the list','apollo') .'</label>';
				echo '<div class="wrap-cz wrap-ind-events">';
				$indEventsHTML = Apollo_App::getTemplatePartCustom(APOLLO_ADMIN_DIR .'/post-types/meta-boxes/syndication/templates/individual-event-part.php',array(
					'individualEvents' => $individualEvents,
					'accountID' => $thepostid
				));
				echo $indEventsHTML;
				// TODO: implement main logic to show syndication email template list filtered events - Move code from plugin to apollo core.
				echo '</div>';
				$totalIndiEvents = self::getTotalIndividualEvents();
				if($totalIndiEvents > count($individualEvents)){
					echo '<div class="options_group apollo-checkbox clear wrap-sm-indi-events">';
					echo '<button type="button" data-total-events="'.$totalIndiEvents.'" class="show-more-individual-events" href="javascript:void(0);" >'.__("Show More Events","apollo").'</button>';
					echo '<span class="spinner"></span>';
					echo '</div>';
				}
				echo '</div>';
			}

		endif;



		/**
		 * Event type
		 * Only display with individual mode is disabled
		 */
		$eventTypes = self::getEventTypes();
		if ( !empty($eventTypes) ){
			echo '<div class="options_group apollo-checkbox clear '.($isDisplayIndEvents ? "force-hidden" : "").' ">';
			echo '<div class="wrap-ddl">';
			echo '<label class="apl-multi-choice ct-lb-mtc">'. __('Event Type','apollo') .'</label>';
			echo '<div class="wrap-ddl-blk">';
			$index = 0;
			$selectedValue = Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_TYPE,array());
			foreach ( $eventTypes as $k_choice => $choice ):
				// Description
				apollo_wp_multi_checkbox(  array(
					'id'            => ''. APL_Syndication_Const::META_TYPE . '-' .$index ,
					'name'          => ''. APL_Syndication_Const::META_TYPE . '[]',
					'label'         => $choice,
					'desc_tip'      => 'true',
					'description'   => '',
					'type'          => 'checkbox',
					'default'       => false,
					'value'			=> $selectedValue,
					'cbvalue'       => $k_choice ) );
				$index += 1;
			endforeach;
			echo '</div>';
			echo '</div>';
			echo '<div class="wrap-ddl">';
			echo '<label class="apl-multi-choice ct-lb-mtc">'. __('Blacklist Type','apollo') .'</label>';
			echo '<div class="wrap-ddl-blk">';
			$index = 0;
			$selectedValue = Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_BLACKLIST,array());
			foreach ( $eventTypes as $k_choice => $choice ):
				// Description
				apollo_wp_multi_checkbox(  array(
					'id'            => ''. APL_Syndication_Const::META_BLACKLIST . '-' .$index ,
					'name'          => ''. APL_Syndication_Const::META_BLACKLIST . '[]',
					'label'         => $choice,
					'desc_tip'      => 'true',
					'description'   => '',
					'type'          => 'checkbox',
					'default'       => false,
					'value'			=> $selectedValue,
					'cbvalue'       => $k_choice ) );
				$index += 1;
			endforeach;
			echo '</div>';
			echo '</div>';
			echo '</div>';
		}

		echo "<hr/>";
		echo '<div class="wrap-blk">';
		echo '<label class="ct-lb-mtc">'. __('Date Range','apollo') .'</label>';

		$rangeChecked = Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_RANGE,-1);
		$startDateVal = Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_DATE_START,'');
		$endDateVal = Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_DATE_END,'');
		$dateRangeChecked = intval($rangeChecked) === 0 ? 'checked' : '';
		$todayChecked = intval($rangeChecked) === 1 ? 'checked' : '';
		$sevenDayChecked = intval($rangeChecked) === 7 ? 'checked' : '';
		$fourteenDayChecked = intval($rangeChecked) === 14 ? 'checked' : '';
		$oneMonthChecked = intval($rangeChecked) === 30 ? 'checked' : '';
		$threeMonthChecked = intval($rangeChecked) === 90 ? 'checked' : '';
		$sixMonthChecked = intval($rangeChecked) === 180 ? 'checked' : '';
		$oneYearChecked = intval($rangeChecked) === 365 ? 'checked' : '';
		echo '  <table border="0">
					<tr>
						<td valign=top height=30>
							<input name="meta-range" '.$dateRangeChecked.' type="radio" class="radio" value="0" /> '.__("Use date range",'apollo').' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.__("Start Date:",'apollo').' <input type="text" name="meta-dateStart" id="dateStart" value="'.$startDateVal.'" />
							'.__("End Date:",'apollo').'<input type="text" name="meta-dateEnd" id="dateEnd" value="'.$endDateVal.'" />
						</td>
					</tr>
					<tr><td valign=top height=30><strong>'.__("OR",'apollo').'</strong> '.__("Select a fixed number of days",'apollo').'</td></tr>
					<tr>
						<td valign=top height=30>
							<input name="meta-range" '.$todayChecked.' type="radio" class="radio" value="1"/> '.__("Today",'apollo').' |
							<input name="meta-range" '.$sevenDayChecked.' type="radio" class="radio" value="7"/> '.__("7 days",'apollo').' |
							<input name="meta-range" '.$fourteenDayChecked.' type="radio" class="radio" value="14"/> '.__("14 days",'apollo').' |
							<input name="meta-range" '.$oneMonthChecked.' type="radio" class="radio" value="30"/> '.__("1 month (30 days)",'apollo').'  |
							<input name="meta-range" '.$threeMonthChecked.' type="radio" class="radio" value="90"/> '.__("3 months (90 days) ",'apollo').' |
							<input name="meta-range" '.$sixMonthChecked.' type="radio" class="radio" value="180"/> '.__("6 months (180 days)",'apollo').' |
							<input name="meta-range" '.$oneYearChecked.' type="radio" class="radio" value="365"/> '.__("1 year (365 days)",'apollo').'
						</td>
					</tr>
				</table>';
		echo "</div>";

		echo "<hr/>";

		echo '<div class="options_group apollo-checkbox clear">';

		$orgListing = apl_instance('APL_Lib_Helpers_PostTypeListing', array(
			'postType' => Apollo_DB_Schema::_ORGANIZATION_PT,
		));

		$orgListing->setLimit(100);

		$orgs = $orgListing->get();

		if ( !empty($orgs) ){
			$selectedValue = Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_ORGS,array());
			$sourceURL = admin_url(sprintf('admin-ajax.php?action=apollo_show_more_posts_type&post_type=%s&page=2&selected=%s&meta_name=%s', Apollo_DB_Schema::_ORGANIZATION_PT, implode(",", $selectedValue), 'meta-orgs'));

			echo '<div class="wrap-ddl">';
			echo '<label class="apl-multi-choice ct-lb-mtc">'. __('Organization','apollo') .'</label>';

			echo sprintf('<div class="wrap-ddl-blk"
			data-loading="<p class=\'form-field loading\'>%s</p>"
			data-ride="scroll-more" data-action="%s"
			data-has-more="%s" >', __('Loading ...', 'apollo'), $sourceURL, $orgListing->hasMore());
			echo sprintf('<input type="hidden" name="hidden-meta-orgs" class="hidden-meta" value="%s" />', implode(',', $selectedValue));
			$index = 0;

			foreach ( $orgs as $org ):
				// Description
				apollo_wp_multi_checkbox(  array(
					'id'            => ''. APL_Syndication_Const::META_ORGS . '-' .$org->ID ,
					'name'          => ''. APL_Syndication_Const::META_ORGS . '[]',
					'label'         => $org->post_title,
					'desc_tip'      => 'true',
					'description'   => '',
					'type'          => 'checkbox',
					'default'       => false,
					'value'			=> $selectedValue,
					'cbvalue'       => $org->ID ) );
				$index += 1;
			endforeach;

			echo '</div>';
			echo self::_renderSelectedPostType($selectedValue,'meta-orgs');
			echo '</div>';
		}

		$venueListing = apl_instance('APL_Lib_Helpers_PostTypeListing', array(
			'postType' => Apollo_DB_Schema::_VENUE_PT,
		));

		$venueListing->setLimit(100);

		$venues = $venueListing->get();
		if ( !empty($venues) ){
			echo '<div class="wrap-ddl">';
			echo '<label class="apl-multi-choice ct-lb-mtc">'. __('Venues','apollo') .'</label>';
			$selectedValue = Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_VENUE,array());
			$sourceURL = admin_url(sprintf('admin-ajax.php?action=apollo_show_more_posts_type&post_type=%s&page=2&selected=%s&meta_name=%s', Apollo_DB_Schema::_VENUE_PT, implode(",", $selectedValue), 'meta-venues'));

			echo sprintf('<div class="wrap-ddl-blk"
			data-loading="<p class=\'form-field loading\'>%s</p>"
			data-ride="scroll-more" data-action="%s"
			data-has-more="%s" >', __('Loading ...', 'apollo'), $sourceURL, $venueListing->hasMore());
			echo sprintf('<input type="hidden" name="hidden-meta-venues" class="hidden-meta" value="%s" />', implode(',', $selectedValue));
			$index = 0;
			foreach ( $venues as $venue ):
				// Description
				apollo_wp_multi_checkbox(  array(
					'id'            => ''. APL_Syndication_Const::META_VENUE . '-' .$venue->ID ,
					'name'          => ''. APL_Syndication_Const::META_VENUE . '[]',
					'label'         => $venue->post_title,
					'desc_tip'      => 'true',
					'description'   => '',
					'type'          => 'checkbox',
					'default'       => false,
					'value'			=> $selectedValue,
					'cbvalue'       => $venue->ID ) );
				$index += 1;
			endforeach;
			echo '</div>';
			echo self::_renderSelectedPostType($selectedValue);
			echo '</div>';
		}
		echo '</div>';

		echo "<hr/>";
		echo '<div class="options_group clear wrap-blk ct-full-width">';
		echo '<label class="ct-lb-mtc">'. __('Select City or Zip (optional)','apollo') .'</label>';
		echo '<div class="options_group clear ct-selection-box">';
		apollo_wp_radio(  array(
			'id' => APL_Syndication_Const::META_CITY_ZIP_OPTION,
			'label' => __( '', 'apollo' ),
			'value' => Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_CITY_ZIP_OPTION,''),
			'options' => array('CITY' => 'CITY',
				'ZIP' => 'ZIP',
				'' => 'RESET'
			)
		) );
		echo '</div>';
		$cities = self::getCities();
		if ( !empty($cities) ){
			echo '<div id="city-area-selection" class="options_group clear cz-area-selection">';
			echo '<label class="apl-multi-choice ct-lb-mtc">'. __('Cities','apollo') .'</label>';
			echo '<div class="wrap-cz">';
			echo '<div class="wrap-cz-child">';
			$index = 0;
			$numCol = 4;
			$itemPerCol = intval(count($cities) / $numCol);
			$selectedValue = Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_CITIES,array());
			foreach ( $cities as $k_choice => $choice ):
				// Description
				apollo_wp_multi_checkbox(  array(
					'id'            => ''. APL_Syndication_Const::META_CITIES . '-' .$index ,
					'name'          => ''. APL_Syndication_Const::META_CITIES . '[]',
					'label'         => $choice,
					'desc_tip'      => 'true',
					'description'   => '',
					'type'          => 'checkbox',
					'default'       => false,
					'value'			=> $selectedValue,
					'cbvalue'       => $k_choice ) );
				$index += 1;
				if($itemPerCol > 0 && $index % $itemPerCol === 0){
					echo '</div>';
					echo '<div class="wrap-cz-child">';
				}
			endforeach;
			echo '</div>';
			echo '</div>';
			echo '</div>';
		}
		$zips = self::getZips();
		if ( !empty($zips) ){
			echo '<div id="zip-area-selection" class="options_group clear cz-area-selection">';
			echo '<label class="apl-multi-choice ct-lb-mtc">'. __('Zips','apollo') .'</label>';
			echo '<div class="wrap-cz">';
			echo '<div class="wrap-cz-child">';
			$index = 0;
			$numCol = 4;
			$itemPerCol = intval(count($zips) / $numCol);
			$selectedValue = Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_ZIPS,array());
			foreach ( $zips as $k_choice => $choice ):
				// Description
				apollo_wp_multi_checkbox(  array(
					'id'            => ''. APL_Syndication_Const::META_ZIPS . '-' .$index ,
					'name'          => ''. APL_Syndication_Const::META_ZIPS . '[]',
					'label'         => $choice,
					'desc_tip'      => 'true',
					'description'   => '',
					'type'          => 'checkbox',
					'default'       => false,
					'value'			=> $selectedValue,
					'cbvalue'       => $k_choice ) );
				$index += 1;
				if($itemPerCol > 0 && $index % $itemPerCol === 0){
					echo '</div>';
					echo '<div class="wrap-cz-child">';
				}
			endforeach;
			echo '</div>';
			echo '</div>';
			echo '</div>';
		}
		echo "</div>";

		echo "<hr/>";
		// End for not individual mode



		// Stop here
		echo '<div class="options_group clear wrap-blk ct-full-width">';
		echo '<label class="ct-lb-mtc">'. __('Select all fields or Select your own fields to include (Does not apply to iframe version)','apollo') .'</label>';
		echo '<div class="options_group clear ct-selection-box">';
		apollo_wp_radio(  array(
			'id' => APL_Syndication_Const::META_ALLOW_DISPLAY_FIELD,
			'label' => __( '', 'apollo' ),
			'value' => Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_ALLOW_DISPLAY_FIELD,''),
			'options' => array('ALL' => __("All Fields",'apollo'),
				'OWN' => __("Select My Own Fields",'apollo')
			)
		) );
		echo '</div>';
		echo '<div id="all-fields-selection" class="options_group clear display-fields-area-selection">';
		echo '<p>' . __('You have selected to include all fields in your syndication','apollo') . '</p>';
		echo '</div>';
		echo '<div id="own-fields-selection" class="options_group clear display-fields-area-selection">';
		echo '<p>' . __('The basic information for the event is automatically included.<br>Below are some additional fields you can choose to include (*note: this does not apply to the mailchimp feed):','apollo').'</p>';
		echo '<div class="wrap-cz">';
		echo '<div class="wrap-cz-child">';
		$displayFields = array( 'eventDescription' => 'Event Description',
			'eventDatesTimes' => 'Individual Dates & Times',
			'orgName' => 'Organization Name',
			'orgImage' => 'Organization Image',
			'venue' => 'Venue Details',
			'spotlight' => 'Indicator for event being spotlighted',
			'featured' => 'Indicator for event being featured',
			'secondaryType' => 'Sub Category Names',
			'eventTicketInfo' => 'Ticket Information',
			'eventPhone1' => 'Phone number',
			'eventEmail' => 'Contact Email',
			'eventTicketUrl' => 'Ticket URL',
			'discountUrl' => 'Discount URL',
			'eventStartTime' => 'Start Time',
			'datePosted' => 'Date event created',
			'eventLink' => 'URL to event',
			'eventGuid' => 'Globally Unique Identifier'
		);
		$selectedValue = Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_OWNSEL,array());
		$index = 0;
		$numCol = 3;
		$itemPerCol = intval(count($displayFields) / $numCol);
		foreach ( $displayFields as $k_choice => $choice ):
			// Description
			apollo_wp_multi_checkbox(  array(
				'id'            => ''. APL_Syndication_Const::META_OWNSEL . '-' .$index ,
				'name'          => ''. APL_Syndication_Const::META_OWNSEL . '[]',
				'label'         => $choice,
				'desc_tip'      => 'true',
				'description'   => '',
				'type'          => 'checkbox',
				'default'       => false,
				'value'			=> $selectedValue,
				'cbvalue'       => $k_choice ) );
			$index += 1;
			if($index % $itemPerCol === 0){
				echo '</div>';
				echo '<div class="wrap-cz-child">';
			}
		endforeach;
		echo '</div>';
		echo '</div>';
		echo '</div>';
		echo '</div>';

		echo "<hr/>";

		echo '<div class="options_group clear wrap-blk ct-full-width">';
		echo '<label class="ct-lb-mtc">'. __('Create IFrame widget','apollo') .'</label>';
		echo '<div class="options_group clear ct-selection-box">';
		apollo_wp_radio(  array(
			'id' => APL_Syndication_Const::META_IFRAME,
			'label' => __( '', 'apollo' ),
			'value' => Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_IFRAME,'NO'),
			'options' => array('YES' => __("YES",'apollo'),
				'NO' => __("NO",'apollo')
			)
		) );
		echo '</div>';
		echo '<div id="create-iframe-ws-blk" class="wrap-cz create-iframe-ws">';
		echo '<div class="options_group clear ct-selection-box">';
		echo '<label class="ct-lb-mtc">'. __('Select display settings','apollo') .'</label><br/>';
		apollo_wp_radio(  array(
			'id' => APL_Syndication_Const::META_LANDING,
			'label' => __( 'Landing Page', 'apollo' ),
			'value' => Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_LANDING,''),
			'options' => array('YES' => __("YES",'apollo'),
				'NO' => __("NO",'apollo')
			)
		) );
		echo '</div>';
		echo '<div class="options_group clear ct-selection-box">';
		apollo_wp_radio(  array(
			'id' => APL_Syndication_Const::META_SEARCH,
			'label' => __( 'Search Bar Display', 'apollo' ),
			'value' => Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_SEARCH,''),
			'options' => array('YES' => __("YES",'apollo'),
				'NO' => __("NO",'apollo')
			)
		) );
		echo '</div>';
		echo '<div class="options_group clear ct-selection-box">';
		apollo_wp_radio(  array(
			'id' => APL_Syndication_Const::META_SUBMIT,
			'label' => __( 'Submit Event Link Display', 'apollo' ),
			'value' => Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_SUBMIT,''),
			'options' => array('YES' => __("YES",'apollo'),
				'NO' => __("NO",'apollo')
			)
		) );
		echo '</div>';
		echo '<div class="options_group clear ct-selection-box">';
		apollo_wp_text_input(  array(
			'id' => APL_Syndication_Const::META_CUSTOM_URL,
			'label' => __( 'Custom Submit Event URL', 'apollo' ),
			'value' => Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_CUSTOM_URL,''),
		) );
		echo '</div>';
		echo '<div class="options_group clear ct-selection-box">';
		apollo_wp_radio(  array(
			'id' => APL_Syndication_Const::META_ORG_COL,
			'label' => __( 'Org Column', 'apollo' ),
			'value' => Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_ORG_COL,''),
			'options' => array('YES' => __("YES",'apollo'),
				'NO' => __("NO",'apollo')
			)
		) );
		echo '</div>';
		echo '<div class="options_group clear ct-selection-box">';
		apollo_wp_radio(  array(
			'id' => APL_Syndication_Const::META_LOCATION_COL,
			'label' => __( 'Location Column', 'apollo' ),
			'value' => Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_LOCATION_COL,''),
			'options' => array('YES' => __("YES",'apollo'),
				'NO' => __("NO",'apollo')
			)
		) );
		echo '</div>';
		echo '<div class="options_group clear ct-selection-box">';
		apollo_wp_radio(  array(
			'id' => APL_Syndication_Const::META_LINK_DESTINATION,
			'label' => __( 'Detail Links Point To', 'apollo' ),
			'value' => Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_LINK_DESTINATION,''),
			'options' => array('PW' => __("Partner's Widget ",'apollo'),
				'YW' => __("Your Website",'apollo')
			)
		) );
		echo '</div>';

		$isSpotFeat = Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_SFACTIVE,'NO') == 'YES';


		if ($isSpotFeat == 'YES'){
			self::resetSpotFeatEventForIFrame($thepostid);
			$metadata = get_post_meta($thepostid);
			if(!empty($metadata['meta-spot'])){
				$curSelectedSpotLightEventIDs = unserialize($metadata['meta-spot'][0]);
				$curSelectedSpotLightEventIDs = !empty($curSelectedSpotLightEventIDs) ? implode(",", $curSelectedSpotLightEventIDs) : '';
			} else {
				$curSelectedSpotLightEventIDs = '';
			}

			if(!empty($metadata['meta-feat'])){
				$curSelectedFeaturedEventIDs = unserialize($metadata['meta-feat'][0]);
				$curSelectedFeaturedEventIDs = !empty($curSelectedFeaturedEventIDs) ? implode(",", $curSelectedFeaturedEventIDs) : '';
			} else {
				$curSelectedFeaturedEventIDs = '';
			}

			if(!empty($metadata['meta-pos'])){
				$curSelectedFeaturedPosEventIDs = unserialize($metadata['meta-pos'][0]);
				$curSelectedFeaturedPosEventIDs = !empty($curSelectedFeaturedPosEventIDs) ? implode(",", $curSelectedFeaturedPosEventIDs) : '';
			} else {
				$curSelectedFeaturedPosEventIDs = '';
			}

			// Display events for spotlight or feature
			echo '<div class="options_group clear ct-selection-box">';
			echo "	<strong>Spotlight / Featured Events</strong><br>";
			echo "	<div style=\"height:500px; width:100%; overflow:auto;\" class='spotlight-feature'>";
			echo "		<div style=\"height:300px;\">";
			echo "			<table border=1 bordercolor=lightgray cellpadding=3 width=980 style=\"border-collapse:collapse;\">";
			echo "				<tr>";
			echo "					<th>Image</th><th>Spotlight</th><th>Feature</th><th id='title_header'>Event Title</th><th>Category</th><th>Organization</th><th id='city_header'>City</th><th>Start Date</th><th>End Date</th>";
			echo "				</tr>";
			echo "				<tbody id='SpotFea'>";
									echo self::getHTMLSpotFeatEventsWithPagination(array(
										'syndication_id' => $thepostid,
									));
			echo "				</tbody>";
			echo "			</table>";
			echo '<div style="text-align: center;">';
			echo '<span class="spinner" style=" float: none; margin-top: 10px; visibility: visible; display: none;"></span>';
			echo '</div>';
			echo "		</div>";
			echo ' <input  id="selected-spotlight-event" type="hidden" data-id="'.$thepostid.'"  value="'.$curSelectedSpotLightEventIDs.'"  name="spot-syd"/>';
			echo ' <input  id="selected-feature-event" type="hidden"   value="'.$curSelectedFeaturedEventIDs.'" name="feature-syd" />';
			echo ' <input  id="selected-feature-pos" type="hidden"   value="'.$curSelectedFeaturedPosEventIDs.'" name="feature-pos-syd" />';
			echo ' <input  id="total-spot-feat-event" type="hidden"   value="'.self::$totalSpotFeatEvents.'" name="total-spot-feat-event" />';
			echo "	</div>";
			echo "</div>";

		}


		// TODO : implement field 'image-uploader' and add that to here
		echo '<div class="options_group clear ct-selection-box">';
		echo '<strong>Top Image (appears above search bar):</strong> <input type=file name="meta-image" id="meta-image"/><br>';
		$metaIFrameImageVal = Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_IMAGE,'');
		if(!empty($metaIFrameImageVal)){
			echo "<div style='border:1px dotted lightgray;'>";
			echo wp_get_attachment_image( $metaIFrameImageVal, 'medium' );
			echo "</div>";
		}
		echo '</div>';
		echo '<div class="options_group clear ct-selection-box">';
		apollo_wp_textarea_input(  array(
			'id' => APL_Syndication_Const::META_WID_UPPER,
			'label' => __( 'Upper Banner (appears below search bar)', 'apollo' ),
			'value' => Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_WID_UPPER,''),
		) );
		echo '</div>';
		echo '<div class="options_group clear ct-selection-box">';
		apollo_wp_textarea_input(  array(
			'id' => APL_Syndication_Const::META_WID_DESC,
			'label' => __( 'Description (appears below upper banner)', 'apollo' ),
			'value' => Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_WID_DESC,''),
		) );
		echo '</div>';
		echo '<div class="options_group clear ct-selection-box">';
		apollo_wp_textarea_input(  array(
			'id' => APL_Syndication_Const::META_WID_LOWER,
			'label' => __( 'Lower Banner (appears under event line listings)', 'apollo' ),
			'value' => Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_WID_LOWER,''),
		) );
		echo '</div>';

		echo '<div class="options_group clear ct-selection-box">';
		apollo_wp_textarea_input(  array(
			'id' => APL_Syndication_Const::META_ANALYTICS,
			'label' => __( 'Google Analytics Tracking Code', 'apollo' ),
			'value' => Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_ANALYTICS,''),
		) );
		echo '</div>';


		echo '<div class="options_group clear ct-selection-box">';
		apollo_wp_text_input(  array(
			'id' => 'iframe-widget-url',
			'label' => __( 'iFrame Widget URL', 'apollo' ),
			'value' => $currentDomainName . '?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid='.$thepostid.'&aplang=eng&apoutput=iframe',
		) );
		echo '</div>';
		echo '<div class="options_group clear ct-selection-box">';
		apollo_wp_text_input(  array(
			'id' => 'iframe-widget-code',
			'label' => __( 'iFrame Widget Code', 'apollo' ),
			'value' => '<iframe frameborder="0" width="620" height="900" src="'.$currentDomainName.'?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid='.$thepostid.'&aplang=eng&apoutput=iframe"></iframe>',
		) );
		echo '</div>';
		echo '<div class="options_group clear ct-selection-box">';
		apollo_wp_text_input(  array(
			'id' => APL_Syndication_Const::META_WID_CSS,
			'label' => __( 'iFrame Custom CSS URL', 'apollo' ),
			'value' => Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_WID_CSS,''),
		) );
		echo '<a target=_blank href="'.$currentDomainName.'/?'._APL_SYNDICATED_DATA_HANDLER_FLAG.'&apid=2721&aplang=eng&apoutput=iframe&apd=css">'.__("Download default css file","apollo").'</a></p>';
		echo '</div>';
		echo '<div class="options_group clear ct-selection-box">';
		apollo_wp_radio(  array(
			'id' => APL_Syndication_Const::META_ENABLE_EVENT_IMAGE_FOR_IFRAME_WIDGET,
			'label' => __( 'Enable event image', 'apollo' ),
			'value' => Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid, APL_Syndication_Const::META_ENABLE_EVENT_IMAGE_FOR_IFRAME_WIDGET,''),
			'default' => 'YES',
			'options' => array(
				'YES' => __("YES",'apollo'),
				'NO' => __("NO",'apollo')
			)
		) );
		echo '</div>';
		echo '</div>';
		echo '</div>';

		echo "<hr/>";
		echo '<div class="wrap-blk">';
		echo '<label class="ct-lb-mtc">'. __('Management Options','apollo') .'</label>';
		/* Output sort order get checked value */
		$outputSortOrderVal = Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::META_SORT,'');
		$pttChecked = $outputSortOrderVal == 'post_title' ? 'checked' : '';
		$dStartChecked = $outputSortOrderVal == 'em2.meta_value' ? 'checked' : '';
		$dEndChecked = $outputSortOrderVal == 'em3.meta_value' ? 'checked' : '';

		echo '<p>
			<strong>'.__("Output sort order:","apollo").'</strong>
			<input type=radio name=meta-sort value="post_title" '.$pttChecked.' > '.__("Alphabetical",'apollo').'
			<input type=radio name=meta-sort value="em2.meta_value" '.$dStartChecked.' > '.__("Start date ascending",'apollo').'
			<input type=radio name=meta-sort value="em3.meta_value" '.$dEndChecked.' > '.__("End date ascending",'apollo').'
			</p>';

		echo "</div>";


		echo '<div class="options_group clear wrap-blk ct-full-width">';
		echo '<label class="ct-lb-mtc">'. __('Enable file caching','apollo') .'</label>';
		echo '<div class="options_group clear ct-selection-box">';
		$val = Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::ENABLE_FILE_CACHING,'no');
		apollo_wp_radio(  array(
			'id' => APL_Syndication_Const::ENABLE_FILE_CACHING,
			'label' => __( '', 'apollo' ),
			'value' => $val,
			'options' => array('yes' => __("YES",'apollo'),
				'no' => __("NO",'apollo')
			)
		) );
		echo '</div>';
		echo '<button '.($val == "no" ? "disabled" : "").' id="syn-clear-cache" type="button" data-loading="'.__("Processing", "apollo").'" data-text="'.__("Clear Cache", "apollo").'">'.__("Clear Cache", "apollo").'</button>';
		echo '</div>';


		echo '<div class="options_group clear wrap-blk ct-full-width">';
		echo '<label class="ct-lb-mtc">'. __('Number of events per feed','apollo') .'</label>';
		$val = Apollo_Admin_Syndication_Meta_Boxes::getSyndicationDataByKey($thepostid,APL_Syndication_Const::NUM_EVENTS, 500);
		apollo_wp_text_input(  array(
			'id' => APL_Syndication_Const::NUM_EVENTS,
			'label' => __( '', 'apollo' ),
			'value' => $val,
			'type'	=> 'number',
			'custom_attributes'	=> array('max' => 1000, 'min'	=> 1)
		) );
		echo '</div>';

	}

	public static function getOrganization(){
		global $wpdb;
		$resultsData = array();
		$queryString = "SELECT post_title, ID
						FROM ".$wpdb->posts."
						WHERE ID IN (
								SELECT org_id
								FROM ".$wpdb->apollo_event_org."
								WHERE is_main = 'yes')
						";
		$orgs = $wpdb->get_results($queryString, OBJECT);
		wp_reset_query();
		if ($orgs) {
			foreach ($orgs as $v ){
				$resultsData[$v->ID] = $v->post_title;
			}
		}
		return $resultsData;
	}

	public static function getVenues(){
		$resultsData = array();
		$venues = Apollo_App::get_list_post_type_items( Apollo_DB_Schema::_VENUE_PT );
		if ($venues) {
			foreach ($venues as $v ){
				$resultsData[$v->ID] = $v->post_title;
			}
		}
		return $resultsData;
	}

	public static function getEventTypes(){
		$resultsData = array();
		$args = array(
			'orderby'                => 'name',
			'order'                  => 'ASC',
			'hide_empty'             => false,
			'fields'                 => 'all',
			'hierarchical'           => true,
			'parent'				 => 0,
		);
		$eventTypes = get_terms('event-type', $args);
		if(!empty($eventTypes)){
			foreach($eventTypes as $itemParent){
				$resultsData[$itemParent->term_id] = $itemParent->name;
				$termsChild = get_terms('event-type', array(
					'orderby'                => 'name',
					'order'                  => 'ASC',
					'hide_empty'             => false,
					'fields'                 => 'all',
					'hierarchical'           => true,
					'parent'				 => $itemParent->term_id,
				));
				if(!empty($termsChild)){
					foreach($termsChild as $tc){
						$resultsData[$tc->term_id] = '—' . $tc->name;
					}
				}
			}
		}
		return $resultsData;
	}

	public static function getCities(){
		$cities = Apollo_App::getCityByTerritory(false,'',false);
		$resultsData = $cities;
		return $resultsData;
	}

	public static function getZips(){
		$zips = Apollo_App::getZipByTerritory(false,'',false,false);
		$resultsData = $zips;
		return $resultsData;
	}


	/**
	 * Save meta box data
	 */
	public static function save( $post_id, $post ) {
		global $post, $wpdb;

		// Checks save status
		$is_autosave = wp_is_post_autosave( $post_id );
		$is_revision = wp_is_post_revision( $post_id );
		$is_valid_nonce = ( isset( $_POST[ 'apollo_syndication_settings_meta_nonce' ] ) && wp_verify_nonce( $_POST[ 'apollo_syndication_settings_meta_nonce' ], 'apollo_syndication_settings_meta_nonce' ) ) ? true : false;

		// Exits script depending on save status
		if ( $is_autosave || $is_revision || ! $is_valid_nonce ) {
			return;
		}

		// handle save all syndication meta data / settings
		// TODO : replace string-key = APL_Syndication_Const
		if( isset( $_POST[ 'meta-iframe' ] ) ) {
			update_post_meta( $post_id, 'meta-iframe', $_POST[ 'meta-iframe' ] );
		} else {
			update_post_meta( $post_id, 'meta-iframe', 'NO' );
		}

		if( isset( $_POST[ APL_Syndication_Const::ENABLE_FILE_CACHING ] ) ) {
			update_post_meta( $post_id, APL_Syndication_Const::ENABLE_FILE_CACHING, $_POST[ APL_Syndication_Const::ENABLE_FILE_CACHING ] );
		} else {
			update_post_meta( $post_id, APL_Syndication_Const::ENABLE_FILE_CACHING, 'no' );
		}

		if( isset( $_POST[ APL_Syndication_Const::NUM_EVENTS ] ) ) {
			update_post_meta( $post_id, APL_Syndication_Const::NUM_EVENTS, intval($_POST[ APL_Syndication_Const::NUM_EVENTS ]) );
		}

		if( isset( $_POST[ 'meta-eventID' ] ) ) {
			update_post_meta( $post_id, 'meta-eventID', $_POST[ 'meta-eventID' ] );
		}

		if( isset( $_POST[ 'meta-type' ] ) ) {
			update_post_meta( $post_id, 'meta-type', $_POST[ 'meta-type' ] );
		} else {
			update_post_meta( $post_id, 'meta-type', '' );
		}

		if( isset( $_POST[ 'meta-blacklist' ] ) ) {
			update_post_meta( $post_id, 'meta-blacklist', $_POST[ 'meta-blacklist' ] );
		} else {
			update_post_meta( $post_id, 'meta-blacklist', '' );
		}

		if( isset( $_POST[ 'meta-privacy' ] ) ) {
			update_post_meta( $post_id, 'meta-privacy', $_POST[ 'meta-privacy' ] );
		}

		if( isset( $_POST[ 'meta-dateStart' ] ) ) {
			update_post_meta( $post_id, 'meta-dateStart', $_POST[ 'meta-dateStart' ] );
		}

		if( isset( $_POST[ 'meta-dateEnd' ] ) ) {
			update_post_meta( $post_id, 'meta-dateEnd', $_POST[ 'meta-dateEnd' ] );
		}

		if( isset( $_POST[ 'meta-range' ] ) ) {
			update_post_meta( $post_id, 'meta-range', $_POST[ 'meta-range' ] );
		}

		if( !empty( $_POST[ 'current-meta-orgs-checked' ] ) ) {
			$selected = explode(',', $_POST['hidden-meta-orgs']);
			$metaOrgs = explode(',', $_POST[ 'current-meta-orgs-checked' ]);
			$orgsArr = array_filter(array_unique(array_merge($metaOrgs, $selected)));
			update_post_meta( $post_id, 'meta-orgs', $orgsArr);
		} else {
			update_post_meta( $post_id, 'meta-orgs', '' );
		}

		if( !empty( $_POST[ 'current-meta-venues-checked' ] ) ) {
			$selected = explode(',', $_POST['hidden-meta-venues']);
			$metaVenues = explode(',', $_POST[ 'current-meta-venues-checked' ]);
			$venuesArr = array_filter(array_unique(array_merge($metaVenues, $selected)));
			update_post_meta( $post_id, 'meta-venues', $venuesArr );
		} else {
			update_post_meta( $post_id, 'meta-venues', '');
		}

		if( isset( $_POST[ 'meta-privacy' ] ) ) {
			update_post_meta( $post_id, 'meta-privacy', $_POST[ 'meta-privacy' ] );
		}

		if( isset( $_POST[ 'meta-age' ] ) ) {
			update_post_meta( $post_id, 'meta-age', $_POST[ 'meta-age' ] );
		}

		if( isset( $_POST[ 'meta-cities' ] ) ) {
			update_post_meta( $post_id, 'meta-cities', $_POST[ 'meta-cities' ] );
		} else {
			update_post_meta( $post_id, 'meta-cities', '');
		}

		if( isset( $_POST[ 'meta-zips' ] ) ) {
			update_post_meta( $post_id, 'meta-zips', $_POST[ 'meta-zips' ] );
		} else {
			update_post_meta( $post_id, 'meta-zips', '' );
		}

		if( isset( $_POST[ 'meta-cityzip' ] ) ) {
			update_post_meta( $post_id, 'meta-cityzip', $_POST[ 'meta-cityzip' ] );
		} else {
			update_post_meta( $post_id, 'meta-cityzip', '' );
		}

		if( isset( $_POST[ 'meta-allown' ] ) ) {
			update_post_meta( $post_id, 'meta-allown', $_POST[ 'meta-allown' ] );
		}

		if( isset( $_POST[ 'meta-ownsel' ] ) ) {
			update_post_meta( $post_id, 'meta-ownsel', $_POST[ 'meta-ownsel' ] );
		}

		if( isset( $_POST[ 'meta-cache' ] ) ) {
			update_post_meta( $post_id, 'meta-cache', $_POST[ 'meta-cache' ] );
		}

		if( isset( $_POST[ 'meta-sort' ] ) ) {
			update_post_meta( $post_id, 'meta-sort', $_POST[ 'meta-sort' ] );
		}

		// additional fields for iframe widget
		if( isset( $_POST[ 'meta-landing' ] ) ) {
			update_post_meta( $post_id, 'meta-landing', $_POST[ 'meta-landing' ] );
		}
		if( isset( $_POST[ 'meta-search' ] ) ) {
			update_post_meta( $post_id, 'meta-search', $_POST[ 'meta-search' ] );
		}
		if( isset( $_POST[ 'meta-submit' ] ) ) {
			update_post_meta( $post_id, 'meta-submit', $_POST[ 'meta-submit' ] );
		}
		/** @Ticket $14430 */
		if( isset( $_POST[ APL_Syndication_Const::META_ENABLE_EVENT_IMAGE_FOR_IFRAME_WIDGET ] ) ) {
			update_post_meta( $post_id, APL_Syndication_Const::META_ENABLE_EVENT_IMAGE_FOR_IFRAME_WIDGET, $_POST[ APL_Syndication_Const::META_ENABLE_EVENT_IMAGE_FOR_IFRAME_WIDGET ] );
		}
		if( isset( $_POST[ 'meta-custom-url' ] ) ) {
			update_post_meta( $post_id, 'meta-custom-url', $_POST[ 'meta-custom-url' ] );
		}
		if( isset( $_POST[ 'meta-linkdest' ] ) ) {
			update_post_meta( $post_id, 'meta-linkdest', $_POST[ 'meta-linkdest' ] );
		}
		if( isset( $_POST[ 'meta-orgven' ] ) ) {
			update_post_meta( $post_id, 'meta-orgven', $_POST[ 'meta-orgven' ] );
		}
		if( isset( $_POST[ 'meta-orgcol' ] ) ) {
			update_post_meta( $post_id, 'meta-orgcol', $_POST[ 'meta-orgcol' ] );
		}
		if( isset( $_POST[ 'meta-loccol' ] ) ) {
			update_post_meta( $post_id, 'meta-loccol', $_POST[ 'meta-loccol' ] );
		}
		if( isset( $_POST[ 'meta-disman' ] ) ) {
			update_post_meta( $post_id, 'meta-disman', $_POST[ 'meta-disman' ] );
		}

		if( isset( $_POST[ 'meta-widupper' ] ) ) {
			update_post_meta( $post_id, 'meta-widupper', $_POST[ 'meta-widupper' ] );
		}
		if( isset( $_POST[ 'meta-widdesc' ] ) ) {
			update_post_meta( $post_id, 'meta-widdesc', $_POST[ 'meta-widdesc' ] );
		}
		if( isset( $_POST[ 'meta-widlower' ] ) ) {
			update_post_meta( $post_id, 'meta-widlower', $_POST[ 'meta-widlower' ] );
		}
		if( isset( $_POST[ 'meta-widcss' ] ) ) {
			update_post_meta( $post_id, 'meta-widcss', $_POST[ 'meta-widcss' ] );
		} else {
			update_post_meta( $post_id, 'meta-widcss', '' );
		}

		if( isset( $_POST[ 'spot-syd' ] ) ) {
			update_post_meta( $post_id, 'meta-spot', explode(",",$_POST['spot-syd']) );
		} else {
			update_post_meta( $post_id, 'meta-spot', '' );
		}

		if( isset( $_POST[ 'feature-syd' ] ) ) {
			update_post_meta( $post_id, 'meta-feat', explode(",",$_POST['feature-syd']) );
		} else {
			update_post_meta( $post_id, 'meta-feat', '' );
		}

		if( isset( $_POST[ 'feature-pos-syd' ] ) ) {
			update_post_meta( $post_id, 'meta-pos', explode(",",$_POST[ 'feature-pos-syd' ]) );
		} else {
			update_post_meta( $post_id, 'meta-pos', '' );
		}

		if( isset( $_POST[ 'meta-analytics' ] ) ) {
			update_post_meta( $post_id, 'meta-analytics', $_POST[ 'meta-analytics' ] );
		} else {
			update_post_meta( $post_id, 'meta-analytics', '' );
		}


//  Upload image to the gallery

		$filename = $_FILES['meta-image']['name'];

		if (!empty($filename)) {

			$wp_filetype = wp_check_filetype( basename($filename), null );
			$wp_upload_dir = wp_upload_dir();

			// Move the uploaded file into the WordPress uploads directory
			move_uploaded_file( $_FILES['meta-image']['tmp_name'], $wp_upload_dir['path']  . '/' . $filename );

			$attachment = array(
				'guid' => $wp_upload_dir['url'] . '/' . basename( $filename ),
				'post_mime_type' => $wp_filetype['type'],
				'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
				'post_content' => '',
				'post_status' => 'inherit'
			);

			$filename = $wp_upload_dir['path']  . '/' . $filename;

			$attach_id = wp_insert_attachment( $attachment, $filename, $post->ID );
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
			wp_update_attachment_metadata( $attach_id, $attach_data );

			update_post_meta( $post_id, 'meta-image', $attach_id);
		}

	}



	public static function getHTMLSpotFeatEventsWithPagination($args = array()){
		$resultHTML = "";
		if(self::$syndicationHelper === null){
			self::$syndicationHelper = new Apollo_Syndicate_Helper();
		}
		$resultEvents = self::getEventByCurrentSyndSettingsWithPagination($args);
		if(!empty($resultEvents)){
			$listEvents = $resultEvents['eventData'];

			self::$totalSpotFeatEvents = $resultEvents['totalEvent'];
			foreach($listEvents as $eItem){
				self::$syndicationHelper->getEventDetail($eItem,$args['syndication_id']);
				$eItem = is_object($eItem) ? get_object_vars($eItem) : $eItem;
				$resultHTML .= Apollo_App::getTemplatePartCustom(APOLLO_ADMIN_SYNDICATION_TEMPLATE_DIR . '/spotlight_featured_event_item.php',$eItem);
			}
		}
		return $resultHTML;
	}

	public static function getIndividualEvents($page = 1, $pagesize = self::INDIVIDUAL_EVENTS_PAGESIZE){
		global $wpdb;
		$sql = Apollo_App::getSQLIndividualEvents(false,$page,$pagesize, 'post_title');
		$listEvents = $wpdb->get_results($sql);
		return $listEvents;
	}



	public static function getTotalIndividualEvents(){
		global $wpdb;
		$sql = Apollo_App::getSQLIndividualEvents(true);
		$result = $wpdb->get_results($sql);
		return !empty($result) ? intval($result[0]->total) : 0;
	}


	private static function _renderSelectedPostType($selectedValue,$name = 'meta-venues') {
		$selectedHtml = '';
		if ($selectedValue) {
			foreach($selectedValue as $s) {
				if (!$s) continue;
				$selectedInput =  '<input type="checkbox" class="checkbox-hidden checkbox " name="'.$name.'[]" id="hidden'.$name.'-'.$s.'" value="'.$s.'" checked>';
				$selectedHtml .= sprintf('<li>%s<a target="_blank" href="%s">%s</a></li>',$selectedInput, get_edit_post_link($s), get_the_title($s));
			}
		} else {
			$selectedHtml = __('No data', 'apollo');
		}
		return sprintf('<div class="wrapper-selected"><i><strong>%s</strong></i><div class="wrap-ddl-blk h-100"><ul>%s</ul></div></div>', __('Selected Items', 'apollo'), $selectedHtml);
	}

	public static function getEventByCurrentSyndSettingsWithPagination($args = array()){
		if(self::$syndicationHelper === null){
			self::$syndicationHelper = new Apollo_Syndicate_Helper();
		}
		return self::$syndicationHelper->getEventBySyndicationSettings($args);
	}

	public static function resetSpotFeatEventForIFrame($syndication_id = 0){
		$checkReset = self::checkAndResetSpotFeatEventForIFrame($syndication_id);
		if($checkReset){
			update_post_meta($syndication_id,'meta-spot','');
			update_post_meta($syndication_id,'meta-feat','');
			update_post_meta($syndication_id,'meta-pos','');
		}
	}

	public static function checkAndResetSpotFeatEventForIFrame($syndication_id = 0){
		$syndTransientKey = Apollo_Syndicate_Export_Abstract::SYNDICATION_TRANSIENT_KEY . '_' . $syndication_id;
		$transientIDs = get_transient($syndTransientKey);
		if(!empty($transientIDs)){
			$metadata = get_post_meta($syndication_id);
			if(!empty($metadata['meta-spot'])){
				$curSelectedSpotLightEventIDs = unserialize($metadata['meta-spot'][0]);
				if ($curSelectedSpotLightEventIDs) {
					foreach ($curSelectedSpotLightEventIDs as $spotEventID){
						if(!in_array($spotEventID,$transientIDs)){
							return true; // force reset because have change filters in syndication setting
						}
					}
				}
			}

			if(!empty($metadata['meta-feat'])){
				$curSelectedFeaturedEventIDs = unserialize($metadata['meta-feat'][0]);

				if ($curSelectedFeaturedEventIDs) {
					foreach ($curSelectedFeaturedEventIDs as $featEventID){
						if(!in_array($featEventID,$transientIDs)){
							return true; // force reset because have change filters in syndication setting
						}
					}
				}
			}
		}
		return false;
	}

}
