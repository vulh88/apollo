<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class Apollo_Syndicate_Helper extends Apollo_Syndicate_Export_Abstract
{

    public function getEventBySyndicationSettings($args = array())
    {
        $result = array();
        $accountID = isset($args['syndication_id']) ? $args['syndication_id'] : -1 ;
        if(!empty($accountID) && intval($accountID) > 0){
            $result = $this->getSyndicationByArgs($args);
        }
        return $result;
    }

    public function getEventDetail(&$eventObj, $post_id){

        //
        global $wpdb, $post;

        $event = get_event($eventObj->ID);
        $eventObj->post_title = $event->post->post_title;
        $eventObj->post_content = $event->post->post_content;

        $metadata = get_post_meta($post_id);

        $type = array();
        $categories =  get_the_terms($eventObj->ID,'event-type') ;
        foreach($categories as $category){
            if( $category->name){
                $type[] = $category->name;
            }
        }
        $eventObj->type = !empty($type) ? implode(', ',$type) : '';

        $eventObj->istatus = 'NO';
        $image = wp_get_attachment_url( get_post_thumbnail_id($eventObj->ID) );
        if(!empty($image)){
            $eventObj->istatus = "<a href=\"javascript:void(0);\" onclick=\"window.open('$image', '_blank', 'width=150,height=180,scrollbars=yes,status=yes,resizable=yes,screenx=10,screeny=10');\">Yes</a>";
        }

        // Get the org name
        $orgid = '';
        $org_id = $wpdb->get_results("select org_id from $wpdb->apollo_event_org where is_main = 'yes' and post_id = $eventObj->ID limit 1");
        if( !empty($org_id)){
            $orgid = $org_id[0]->org_id;
        }
        if (!empty($orgid)){
            $eventObj->orgname = get_the_title($orgid);
        } else {
            $org_name = $wpdb->get_results("select meta_value from $wpdb->apollo_eventmeta where meta_key = '_apl_event_tmp_org' and apollo_event_id = $eventObj->ID");
            $eventObj->orgname = $org_name[0]->meta_value;
        }


        // Get venue name
        $venuename = '';
        $venue_id = $wpdb->get_results("SELECT meta_value FROM $wpdb->apollo_eventmeta WHERE meta_key = '_apollo_event_venue' AND apollo_event_id = $eventObj->ID");
        $venueid = $venue_id[0]->meta_value;
        if (!empty($venueid)){
            $ven_loc = $wpdb->get_results("SELECT meta_value FROM $wpdb->apollo_venuemeta WHERE meta_key = '_apl_venue_address' AND apollo_venue_id = $venueid");
            $venloc = unserialize(unserialize($ven_loc[0]->meta_value));
            $eventObj->venuename = $venloc['_venue_city'];

        } else {
            $ven_loc = $wpdb->get_results("select meta_value from $wpdb->apollo_eventmeta where meta_key = '_apl_event_tmp_venue' and apollo_event_id = $eventObj->ID");
            $venloc = unserialize(unserialize($ven_loc[0]->meta_value));
            $eventObj->venuename = $venloc['_venue_city'];
        }



        $eventObj->start = date('m/d/Y', strtotime(Apollo_App::apollo_get_meta_data($eventObj->ID, Apollo_DB_Schema::_APOLLO_EVENT_START_DATE)));
        $eventObj->end = date('m/d/Y', strtotime(Apollo_App::apollo_get_meta_data($eventObj->ID, Apollo_DB_Schema::_APOLLO_EVENT_END_DATE)));
        $eTitle = "<a href='%s' target=_blank>%s</a>";
        $eventObj->title = sprintf($eTitle,get_edit_post_link($eventObj->ID),$eventObj->post_title);

        $spotlight = '';
        if (!empty($metadata['meta-spot'][0])) {
            $spotlight = unserialize($metadata['meta-spot'][0]);
        }

        $feature = '';
        if (!empty($metadata['meta-feat'][0])) {
            $feature = unserialize($metadata['meta-feat'][0]);
        }
        $position = '';
        if (!empty($metadata['meta-pos'][0])) {
            $position = unserialize($metadata['meta-pos'][0]);
        }

        $currentSelection = 0;
        $eventObj->feat_pos = "<select data-current-selected=%currentSelection% name=meta-pos[]>
				<option value=''>-</option>";
        for ($i = 1; $i < 11; $i++) {
            $testval = $i . '_' . $eventObj->ID;
            if (is_array($position) && in_array($testval, $position)) {
                $currentSelection = $testval;
                $pos = ' selected ';
            } else {
                $pos = '';
            }
            $eventObj->feat_pos .= "<option value='$testval' $pos>$i</option>";
        }
        $eventObj->feat_pos .= "</select>";
        $eventObj->feat_pos = str_replace('%currentSelection%',$currentSelection,$eventObj->feat_pos);

        if (is_array($spotlight) && in_array($eventObj->ID, $spotlight)) {$eventObj->spot = ' checked '; } else {$eventObj->spot = '';}
        if (is_array($feature) && in_array($eventObj->ID, $feature)) {$eventObj->feat = ' checked '; } else {$eventObj->feat = '';}
    }


    /**
     * Abstract whether set store transient cache or not
     * @author vulh
     * @return boolean
     */
    protected function setEnableStoreTransientCache()
    {
        // TODO: Implement setEnableStoreTransientCache() method.
    }
}

