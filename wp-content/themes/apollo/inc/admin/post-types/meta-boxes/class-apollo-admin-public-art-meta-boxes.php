<?php
/**
 * Apollo Public Art Meta Boxes
 * @author 		Hong
 * @category 	Admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

include 'class-apl-ad-module-meta-boxes.php';

/**
 * Apollo_Admin_Public_Art_Meta_Boxes
 */
class Apollo_Admin_Public_Art_Meta_Boxes extends Apl_Ad_Module_Meta_Boxes {

    /**
     * Constructor
     */
    public function __construct() {

        parent::__construct( Apollo_DB_Schema::_PUBLIC_ART_PT );

        $this->require_meta_boxes();

        add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 30 );

        add_action( 'save_post', array( $this, 'save_meta_boxes' ), 1, 2 );

        // Save Public Art Meta Boxes
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Public_Art_Data::save', 10, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Public_Art_Address::save', 10, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Public_Art_Images::save', 10, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Public_Art_Data_Video::save', 10, 2 );


        // Error handling (for showing errors from meta boxes on next page load)
        add_action( 'admin_notices', array( $this, 'output_errors' ) );
        add_action( 'shutdown', array( $this, 'save_errors' ) );

    }

    public function require_meta_boxes() {

        require_once 'public-art/class-apollo-meta-box-public-art-address.php';
        require_once 'public-art/class-apollo-meta-box-public-art-data.php';
        require_once 'public-art/class-apollo-meta-box-public-art-images.php';
        require_once 'public-art/class-apollo-meta-box-public-art-video.php';
    }

    /**
     * Add Apollo Meta boxes
     */
    public function add_meta_boxes() {

        add_meta_box( 'apollo-public-art-data-top', __( 'Public Data', 'apollo' ),
            'Apollo_Meta_Box_Public_Art_Data::output', $this->type, 'normal' );

        add_meta_box( 'apollo-public-art-address', __( 'Address', 'apollo' ),
            'Apollo_Meta_Box_Public_art_Address::output', $this->type, 'normal' );

        add_meta_box( 'apollo-public-art-data-contact', __( 'Contact info', 'apollo' ),
            'Apollo_Meta_Box_Public_Art_Data::outputContactInfo', $this->type, 'normal' );

        add_meta_box( 'apollo-event-data', __( 'Public Art videos', 'apollo' ),
            'Apollo_Meta_Box_Public_Art_Data_Video::output', $this->type, 'normal' );

        add_meta_box( 'apollo-artist-customfield', __( 'More Info', 'apollo' ),
            'Apollo_Meta_Box_Customfield::output', Apollo_DB_Schema::_PUBLIC_ART_PT, 'normal' );

        add_meta_box( 'apollo-icons', __( 'Icons', 'apollo' ),
            'Apollo_Meta_Box_Common::output_icons', $this->type, 'normal','low' );

        add_meta_box( 'apollo-public-art-data-artist', __( 'Artists', 'apollo' ),
            'Apollo_Meta_Box_Public_Art_Data::output_artist', $this->type, 'side' );

        add_meta_box( 'apollo-public-art-gallery', __( 'Public Art gallery ', 'apollo' ),
            'Apollo_Meta_Box_Public_Art_Images::output', $this->type, 'side' );

        add_meta_box( 'apollo-public-art-gallery-map-icon', __( 'Public Art Google icon ', 'apollo' ),
            'Apollo_Meta_Box_Public_Art_Images::outputMapIcon', $this->type, 'side' );

    }
}

new Apollo_Admin_Public_Art_Meta_Boxes();
