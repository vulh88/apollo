<?php
/**
 * Apollo Artist Meta Boxes
 *
 * Sets up the write panels used by venues (custom post types)
 *
 * @author 		truonghn
 * @category 	Admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

include 'class-apl-ad-module-meta-boxes.php';

/**
 * Apollo_Admin_Photo_Slider_Meta_Boxes
 */
class Apollo_Admin_Photo_Slider_Meta_Boxes extends Apl_Ad_Module_Meta_Boxes {

    /**
     * Constructor
     */
    public function __construct() {

        parent::__construct( Apollo_DB_Schema::_PHOTO_SLIDER_PT );

        $this->require_meta_boxes();

        add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 30 );
        add_action( 'save_post', array( $this, 'save_meta_boxes' ), 1, 2 );

        // Save Meta Boxes
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Photo_Slider_Setting::save', 10, 2 );
        add_action( $this->process_data_action, 'Apollo_Meta_Box_Photo_Slider_Output::save', 10, 2 );

        // Error handling (for showing errors from meta boxes on next page load)
        add_action( 'admin_notices', array( $this, 'output_errors' ) );
        add_action( 'shutdown', array( $this, 'save_errors' ) );

    }

    public static function getDataByKey($post_id, $key, $default = ''){
        $val = Apollo_App::apollo_get_meta_data( $post_id, $key );
        $val = is_string($val) ? trim($val) : $val;
        $return  = $val && !empty($val) ? $val : $default;
        return $return;
    }


    public function require_meta_boxes() {

        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/photo-slider/class-apollo-meta-box-photo-slider-setting.php';
        require_once APOLLO_ADMIN_DIR .'/post-types/meta-boxes/photo-slider/class-apollo-meta-box-photo-slider-output.php';
    }

    /**
     * Add Apollo Meta boxes
     */
    public function add_meta_boxes() {
        add_meta_box( 'apollo-photo-slider-setting', __( 'Setting', 'apollo' ),
            'Apollo_Meta_Box_Photo_Slider_Setting::output', Apollo_DB_Schema::_PHOTO_SLIDER_PT, 'normal' );


        add_meta_box( 'apollo-photo-slider-output', __( ' Results', 'apollo' ),
            'Apollo_Meta_Box_Photo_Slider_Output::output', Apollo_DB_Schema::_PHOTO_SLIDER_PT, 'normal' );
    }
}

new Apollo_Admin_Photo_Slider_Meta_Boxes();
