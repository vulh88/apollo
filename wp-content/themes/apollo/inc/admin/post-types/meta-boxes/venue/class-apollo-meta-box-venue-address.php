<?php
/**
 * Venue URL
 *
 * Displays the venue address
 *
 * @author 		vulh
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Venue_Address
 */

class Apollo_Meta_Box_Venue_Address {

	/**
	 * Output the metabox
	 */
	public static function output( $post ) { 
        
        global $post, $thepostid,$pagenow;
		wp_nonce_field( 'apollo_venue_meta_nonce', 'apollo_venue_meta_nonce' );
        $thepostid = $post->ID;
        $stateVal = '';
        $cityVal = '';
        $neighborhoodVal = '';
        if( $pagenow == 'post.php' ){
            $stateVal = Apollo_App::apollo_get_meta_data( $thepostid, Apollo_DB_Schema::_APL_VENUE_ADDRESS .'['. Apollo_DB_Schema::_VENUE_STATE .']' );
            $cityVal = Apollo_App::apollo_get_meta_data( $thepostid, Apollo_DB_Schema::_APL_VENUE_ADDRESS .'['. Apollo_DB_Schema::_VENUE_CITY .']' );
            /** @Ticket #13856 */
            $neighborhoodVal = Apollo_App::apollo_get_meta_data( $thepostid, Apollo_DB_Schema::_APL_VENUE_ADDRESS .'['. Apollo_DB_Schema::_VENUE_NEIGHBORHOOD .']' );
        } else {
            $list_states = Apollo_App::getListState();
            $default_states =  of_get_option(Apollo_DB_Schema::_APL_DEFAULT_STATE);
            $cityVal = of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY);
            if( $default_states && in_array($default_states,$list_states) ) {
                $stateVal = $default_states;
            } else if(count($list_states) == 1) {
                $stateVal = key($list_states);
            }
        }

        /** @Ticket #13643 */
        $requiredAddress = of_get_option(Apollo_DB_Schema::_VENUE_ENABLE_ADDRESS_REQUIREMENT, 0);
        $requiredState = of_get_option(Apollo_DB_Schema::_VENUE_ENABLE_STATE_REQUIREMENT, 0);
        $requiredCity = of_get_option(Apollo_DB_Schema::_VENUE_ENABLE_CITY_REQUIREMENT, 0);
        $requiredZip = of_get_option(Apollo_DB_Schema::_VENUE_ENABLE_ZIP_REQUIREMENT, 0);

        /** @Ticket #13856 */
        $neighborhoodOptions = APL_Lib_Territory_Neighborhood::getNeighborhoodByCity($cityVal, $stateVal);

        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_VENUE_ADDRESS .'['. Apollo_DB_Schema::_VENUE_ADDRESS1 .']', 
                'label' => __( 'Address 1', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'class' => $requiredAddress ? 'required' : '',
                'required' => $requiredAddress ? true : false
            ) );
        echo '</div>';  
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_text_input(  array( 
                'id' => Apollo_DB_Schema::_APL_VENUE_ADDRESS. '['. Apollo_DB_Schema::_VENUE_ADDRESS2 .']', 
                'label' => __( 'Address 2', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => ""
            ) );
        echo '</div>';


        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_select(  array( 
                'id' => ''. Apollo_DB_Schema::_APL_VENUE_ADDRESS .'['. Apollo_DB_Schema::_VENUE_STATE .']', 
                'label' => __( 'State', 'apollo' ), 
                'desc_tip' => 'true', 
                'description' => "",
                'class' => sprintf('apl-territory-state %s', $requiredState ? 'required' : ''),
                'required' => $requiredState ? true : false,
                'options'   => Apollo_App::getStateByTerritory(),
                'value' => $stateVal,
            ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            apollo_wp_select(  array(
                'id' => ''. Apollo_DB_Schema::_APL_VENUE_ADDRESS .'['. Apollo_DB_Schema::_VENUE_CITY .']',
                'label' => __( 'City', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'class' => sprintf('apl-territory-city %s', $requiredCity ? 'required' : ''),
                'required' => $requiredCity ? true : false,
                'options'   => Apollo_App::getCityByTerritory(false, $stateVal, true),
                'value' => $cityVal,
            ) );
        echo '</div>';

        echo '<div class="options_group event-box neighborhood-box '. (empty($neighborhoodOptions) ? 'hidden' : '') .'">';
        apollo_wp_select(  array(
            'id' => ''. Apollo_DB_Schema::_APL_VENUE_ADDRESS .'['. Apollo_DB_Schema::_VENUE_NEIGHBORHOOD .']',
            'label' => __( 'Neighborhood', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apl-territory-neighborhood ',
            'first_option' => __('Select Neighborhood', 'apollo'),
            'options'   => $neighborhoodOptions,
            'value' => $neighborhoodVal,
        ) );
        echo '</div>';
        
        echo '<div class="options_group event-box">';
            // Description
            apollo_wp_select(  array(
                'id' => ''. Apollo_DB_Schema::_APL_VENUE_ADDRESS .'['. Apollo_DB_Schema::_VENUE_ZIP .']',
                'label' => __( 'Zip', 'apollo' ),
                'desc_tip' => 'true',
                'description' => "",
                'class' => sprintf('apl-territory-zipcode %s', $requiredZip ? 'required' : ''),
                'required' => $requiredZip ? true : false,
                'options'   => Apollo_App::getZipByTerritory(false, $stateVal, $cityVal, true),
            ) );
        echo '</div>';

        // region
        echo '<div class="options_group event-box">';
        apollo_wp_select(  array(
            'id' => ''. Apollo_DB_Schema::_APL_VENUE_ADDRESS .'['. Apollo_DB_Schema::_VENUE_REGION .']',
            'label' => __( 'Region', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'options'   => Apollo_App::get_regions(),
            'display' => Apollo_App::showRegion() && !Apollo_App::enableMappingRegionZipSelection(),
        ) );
        echo '</div>';

        //latitude
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_VENUE_LATITUDE,
            'name' => Apollo_DB_Schema::_VENUE_LATITUDE,
            'label' => __( 'Latitude', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
        ) );
        echo '</div>';

        //longitude
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_VENUE_LONGITUDE,
            'name' => Apollo_DB_Schema::_VENUE_LONGITUDE,
            'label' => __( 'Longitude', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
        ) );
        echo '</div>';

        /** @Ticket #15810 - Show map */
        echo '<div class="options_group event-box">';
        $browserKey = of_get_option(Apollo_DB_Schema::_GOOGLE_API_KEY_BROWSER);
        apollo_wp_google_map(array(
            'id' => 'apl-get-lat-long',
            'label' => __('Get latitude and longitude', 'apollo'),
            'attr' => array(
                'data-address-id = "'.Apollo_DB_Schema::_APL_VENUE_ADDRESS .'['. Apollo_DB_Schema::_VENUE_ADDRESS1 .']'.'"',
                'data-address2-id = "'.Apollo_DB_Schema::_APL_VENUE_ADDRESS .'['. Apollo_DB_Schema::_VENUE_ADDRESS2 .']'.'"',
                'data-state-id = "'.Apollo_DB_Schema::_APL_VENUE_ADDRESS .'['. Apollo_DB_Schema::_VENUE_STATE .']'.'"',
                'data-city-id = "'.Apollo_DB_Schema::_APL_VENUE_ADDRESS .'['. Apollo_DB_Schema::_VENUE_CITY .']'.'"',
                'data-zip-id = "'.Apollo_DB_Schema::_APL_VENUE_ADDRESS .'['. Apollo_DB_Schema::_VENUE_ZIP .']'.'"',
                'data-latitude-id = "'.Apollo_DB_Schema::_VENUE_LATITUDE.'"',
                'data-longitude-id = "'.Apollo_DB_Schema::_VENUE_LONGITUDE.'"',
                'data-google-api-key = "'. $browserKey .'"'
            )
        ));
        echo '</div>';
	}

	/**
	 * Save meta box data
	 */
	public static function save( $post_id, $post ) {
      
        $_data = $_POST[Apollo_DB_Schema::_APL_VENUE_ADDRESS];

        $postLat = isset($_POST[Apollo_DB_Schema::_VENUE_LATITUDE]) ? $_POST[Apollo_DB_Schema::_VENUE_LATITUDE] : '';
        $postLng = isset($_POST[Apollo_DB_Schema::_VENUE_LONGITUDE]) ? $_POST[Apollo_DB_Schema::_VENUE_LONGITUDE] : '';

        if ($venue = get_venue($post_id)) {
            if (!$postLat && !$postLng && $address = $venue->get_full_address()) {
                $arrCoor = Apollo_Google_Coor_Cache::getCoorForVenue($address);
                $postLat = @$arrCoor['lat'];
                $postLng = @$arrCoor['lng'];
            }
        }

        update_apollo_meta( $post_id, Apollo_DB_Schema::_VENUE_LATITUDE, $postLat );
        update_apollo_meta( $post_id, Apollo_DB_Schema::_VENUE_LONGITUDE, $postLng );
        
        unset($_data[Apollo_DB_Schema::_VENUE_LATITUDE]);
        unset($_data[Apollo_DB_Schema::_VENUE_LONGITUDE]);
        
        if(!isset($_data[Apollo_DB_Schema::_VENUE_REGION])){
            $_data[Apollo_DB_Schema::_VENUE_REGION] = Apollo_App::apollo_get_meta_data($post_id, Apollo_DB_Schema::_VENUE_REGION, Apollo_DB_Schema::_APL_VENUE_ADDRESS );
        }
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_VENUE_ADDRESS, serialize( Apollo_App::clean_array_data($_data) ) );
        update_apollo_meta( $post_id, Apollo_DB_Schema::_VENUE_ZIP, isset($_data[Apollo_DB_Schema::_VENUE_ZIP]) ? $_data[Apollo_DB_Schema::_VENUE_ZIP] : '' );
        update_apollo_meta( $post_id, Apollo_DB_Schema::_VENUE_CITY, isset($_data[Apollo_DB_Schema::_VENUE_CITY]) ? $_data[Apollo_DB_Schema::_VENUE_CITY] : '' );
        update_apollo_meta( $post_id, Apollo_DB_Schema::_VENUE_STATE, isset($_data[Apollo_DB_Schema::_VENUE_STATE]) ? $_data[Apollo_DB_Schema::_VENUE_STATE] : '' );
        update_apollo_meta( $post_id, Apollo_DB_Schema::_VENUE_REGION, isset($_data[Apollo_DB_Schema::_VENUE_REGION]) ? $_data[Apollo_DB_Schema::_VENUE_REGION] : '' );
        /** @Ticket #13874 */
        update_apollo_meta( $post_id, Apollo_DB_Schema::_VENUE_NEIGHBORHOOD, isset($_data[Apollo_DB_Schema::_VENUE_NEIGHBORHOOD]) ? $_data[Apollo_DB_Schema::_VENUE_NEIGHBORHOOD] : '' );
	}
}