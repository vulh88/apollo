<?php
/**
 * Venue URL
 *
 * Displays the venue Location
 *
 * @author 		vulh
 * @category 	admin
 * @package 	inc/admin/post-types/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Apollo_Meta_Box_Venue_Data
 */
class Apollo_Meta_Box_Venue_Data {

    /**
     * Output the metabox
     */
    public static function output( $post ) {

        global $post, $thepostid;
        wp_nonce_field( 'apollo_venue_meta_nonce', 'apollo_venue_meta_nonce' );
        $thepostid = $post->ID;

        /** @Ticket #13643 */
        $requiredEmail = of_get_option(Apollo_DB_Schema::_VENUE_ENABLE_EMAIL_REQUIREMENT, 0);
        $requiredPhone = of_get_option(Apollo_DB_Schema::_VENUE_ENABLE_PHONE_REQUIREMENT, 0);

        echo '<div class="options_group event-box">';
        // Phone
        apollo_wp_text_input(  array(
            'id' => ''. Apollo_DB_Schema::_APL_VENUE_DATA .'['. Apollo_DB_Schema::_VENUE_PHONE .']',
            'label' => __( 'Phone', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => $requiredPhone ? 'required' : '',
            'required' => $requiredPhone ? true : false
        ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
        // Fax
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_VENUE_DATA. '['. Apollo_DB_Schema::_VENUE_FAX .']',
            'label' => __( 'Fax', 'apollo' ),
            'desc_tip' => 'true',
            'description' => ""
        ) );
        echo '</div>';

        echo '<div class="options_group event-box">';
        // Email
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_VENUE_DATA. '['. Apollo_DB_Schema::_VENUE_EMAIL .']',
            'label' => $requiredEmail ? __( 'Email (*)', 'apollo' ) : __( 'Email', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => sprintf('apollo_input_email %s', $requiredEmail ? 'required' : ''),
            'required' => $requiredEmail ? true : false
        ) );
        echo '</div>';

        // Parking Info
        /** @Ticket #12975 - Switch textarea to text editor for parking info, public hours, public admission fee */

        echo '<div class="options_group">';
        apollo_wp_editor(  array(
            'id' => Apollo_DB_Schema::_APL_VENUE_DATA. Apollo_DB_Schema::_VENUE_PARKING_INFO,
            'name' => Apollo_DB_Schema::_APL_VENUE_DATA. '['. Apollo_DB_Schema::_VENUE_PARKING_INFO .']',
            'label' => __( 'Parking Info', 'apollo' ),
            'desc_tip'      => 'true',
            'description' => "",
            'class'         => 'apl-editor',
        ) );
        echo '</div>';

        //Public Hours (if applicable)
        echo '<div class="options_group">';
        apollo_wp_editor(  array(
            'id' => Apollo_DB_Schema::_APL_VENUE_DATA. Apollo_DB_Schema::_VENUE_PUBLIC_HOURS,
            'name' => Apollo_DB_Schema::_APL_VENUE_DATA. '['. Apollo_DB_Schema::_VENUE_PUBLIC_HOURS .']',
            'label' => __( 'Public Hours (if applicable)', 'apollo' ),
            'desc_tip'      => 'true',
            'description' => "",
            'class'         => 'apl-editor',
        ) );
        echo '</div>';

        //Public Admission Fee(s) (if applicable)
        echo '<div class="options_group">';
        apollo_wp_editor(  array(
            'id' => Apollo_DB_Schema::_APL_VENUE_DATA. Apollo_DB_Schema::_VENUE_PUBLIC_ADMISSION_FEEDS,
            'name' => Apollo_DB_Schema::_APL_VENUE_DATA. '['. Apollo_DB_Schema::_VENUE_PUBLIC_ADMISSION_FEEDS .']',
            'label' => __( 'Public Admission Fee(s)', 'apollo' ),
            'desc_tip'      => 'true',
            'description' => "",
            'class'         => 'apl-editor',
        ) );
        echo '</div>';

    }
    /**
     * OutputLink the metabox
     */
    public static function outputLink($post){
        global $post, $thepostid;
        wp_nonce_field( 'apollo_venue_meta_nonce', 'apollo_venue_meta_nonce' );
        $thepostid = $post->ID;

        echo '<div class="options_group event-box">';
        // web site url
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_VENUE_DATA. '['. Apollo_DB_Schema::_VENUE_WEBSITE_URL .']',
            'label' => __( 'Website url', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apollo_input_url',
        ) );
        echo '</div>';

        // blog url
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_VENUE_DATA. '['. Apollo_DB_Schema::_VENUE_PUBLIC_BLOG_URL .']',
            'label' => __( 'Blog url', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apollo_input_url',
        ) );
        echo '</div>';

        // Instagram url
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_VENUE_DATA. '['. Apollo_DB_Schema::_VENUE_INSTAGRAM_URL .']',
            'label' => __( 'Instagram url', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apollo_input_url',
        ) );
        echo '</div>';

        // Twitter url
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_VENUE_DATA. '['. Apollo_DB_Schema::_VENUE_TWITTER_URL .']',
            'label' => __( 'Twitter url', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apollo_input_url',
        ) );
        echo '</div>';

        //  Pinterest  url
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_VENUE_DATA. '['. Apollo_DB_Schema::_VENUE_PINTEREST_URL .']',
            'label' => __( 'Pinterest  url', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apollo_input_url',
        ) );
        echo '</div>';

        // Facebook  url
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_VENUE_DATA. '['. Apollo_DB_Schema::_VENUE_FACEBOOK_URL .']',
            'label' => __( 'Facebook  url', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apollo_input_url',
        ) );
        echo '</div>';

        // Linkedin  url
        echo '<div class="options_group event-box">';
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_VENUE_DATA. '['. Apollo_DB_Schema::_VENUE_LINKEDIN_URL .']',
            'label' => __( 'LinkedIn   url', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apollo_input_url',
        ) );
        echo '</div>';
    }

    /***
     * OutputContactInfo the metabox
     */
    public static function outputContactInfo( $post ) {

        global $post, $thepostid;
        wp_nonce_field( 'apollo_organization_meta_nonce', 'apollo_organization_meta_nonce' );
        $thepostid = $post->ID;

        // contact name
        echo '<div class="options_group event-box">';

        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_VENUE_DATA. '['. Apollo_DB_Schema::_VENUE_CONTACT_NAME .']',
            'label' => __( 'Contact name', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
        ) );
        echo '</div>';

        // contact email
        echo '<div class="options_group event-box">';
        // web site url
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_VENUE_DATA. '['. Apollo_DB_Schema::_VENUE_CONTACT_EMAIL .']',
            'label' => __( 'Contact email', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
            'class' => 'apollo_input_email',
        ) );
        echo '</div>';

        // contact phone
        echo '<div class="options_group event-box">';
        // web site url
        apollo_wp_text_input(  array(
            'id' => Apollo_DB_Schema::_APL_VENUE_DATA. '['. Apollo_DB_Schema::_VENUE_CONTACT_PHONE .']',
            'label' => __( 'Contact phone', 'apollo' ),
            'desc_tip' => 'true',
            'description' => "",
        ) );
        echo '</div>';

    }

    /**
     * Output Accessibility
     */
    public static function output_acb( $post ) {
        $venue = get_venue( $post );
        global $apollo_event_access;
        $_arr = $apollo_event_access;
        echo '<div class="options_group event-box"><ul id="info-access-ul">';
        ksort( $_arr );
        $_acb = $venue->get_meta_data( Apollo_DB_Schema::_E_CUS_ACB, Apollo_DB_Schema::_APL_VENUE_DATA );
//        echo '<pre>';
//        print_r($_acb);
//        echo '</pre>';
        foreach ( $_arr as $img => $label ):
            $checked = $_acb && in_array( $img, $_acb ) ? 'checked' : '';
            echo '<li>
            <input '.$checked.' type="checkbox" name="'.Apollo_DB_Schema::_APL_VENUE_DATA.'['. Apollo_DB_Schema::_E_CUS_ACB .'][]" value="'.$img.'" />
            <img src="'. get_template_directory_uri().'/assets/images/event-accessibility/'.$img.'.png" /> <span>'.$label.'</span></li>';
        endforeach;

        echo '</ul></div>';

        // Additional accessibility info
        /** @Ticket#12975 */
        echo '<div class="clearfix"></div>';

        echo '<div class="options_group">';
        apollo_wp_editor(  array(
            'id' => Apollo_DB_Schema::_APL_VENUE_DATA. Apollo_DB_Schema::_E_ACB_INFO,
            'name' => Apollo_DB_Schema::_APL_VENUE_DATA. '['. Apollo_DB_Schema::_E_ACB_INFO .']',
            'label' => __( 'Additional accessibility info', 'apollo' ),
            'desc_tip'      => 'true',
            'description' => "",
            'class'         => 'apl-editor',
        ) );
        echo '</div>';


    }

    /**
     * Save meta box data
     */
    public static function save( $post_id, $post ) {

        $_data = $_POST[Apollo_DB_Schema::_APL_VENUE_DATA];

        $email_field = Apollo_DB_Schema::_VENUE_EMAIL;

        if ( isset( $_data[$email_field] ) && $_data[$email_field] ):
            if ( is_email( $_data[$email_field] ) ) {
                $_data[$email_field] = Apollo_App::clean_data_request( $_data[$email_field] );
            } else {
                $_data[$email_field] = '';
                Apollo_Admin_Venue_Meta_Boxes::add_error( __( 'Email is invalid format', 'apollo' ) );
            }
        endif;


        $url_field = Apollo_DB_Schema::_VENUE_WEBSITE_URL;

        if ( isset( $_data[$url_field] ) && $_data[$url_field] ):
            if ( filter_var( $_data[$url_field], FILTER_VALIDATE_URL ) ) {
                $_data[$url_field] = Apollo_App::clean_data_request( $_data[$url_field] );
            } else {
                $_data[$url_field] = '';
                Apollo_Admin_Venue_Meta_Boxes::add_error( __( 'Website url is invalid format', 'apollo' ) );
            }
        endif;


        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_VENUE_DATA, serialize( Apollo_App::clean_array_data($_data) ) );


        //for search
       update_apollo_meta( $post_id, Apollo_DB_Schema::_VENUE_ACCESSIBILITY, serialize( Apollo_App::clean_array_data($_data[Apollo_DB_Schema::_E_CUS_ACB]) ) );

       /** @Ticket #13028 */
       update_apollo_meta($post_id, Apollo_DB_Schema::_VENUE_PHONE, !empty($_data[Apollo_DB_Schema::_VENUE_PHONE]) ? $_data[Apollo_DB_Schema::_VENUE_PHONE] : '');
    }
}