<?php
/**
 * Admin functions for the Artist type
 *
 * @author 		vulh
 * @category 	admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Apollo_Admin_CPT' ) ) {
    include( 'class-apollo-admin-cpt.php' );
}

if ( ! class_exists( 'Apollo_Admin_Educator' ) ) :
    
class Apollo_Admin_Educator extends Apollo_Admin_CPT {
    
    public function __construct() {
        global $typenow, $pagenow, $wpdb;
        
        $this->type             = Apollo_DB_Schema::_EDUCATOR_PT;
        $this->meta_tbl         = $wpdb->{Apollo_Tables::_APL_EDUCATOR_META};
        $this->post_term_tbl    = $wpdb->{Apollo_Tables::_POST_TERM};
        $this->associationUserModuleListComp = apl_instance('APL_Lib_Association_Components_AssociationUsersModuleList');
        $this->associationAgencyModuleComp = apl_instance('APL_Lib_Association_Components_AssociationAgenciesModuleList');

        $this->requireds = array(
        
            'post_title' => array( 
                'label'     => __( 'The name', 'apollo' ), 
                'validate'  => 'required' 
            ),
          
            'post_content'  => array(
                'label'     => __( 'The Description', 'apollo' ), 
                'validate'  => 'required_editor' 
            ),
            ''.Apollo_DB_Schema::_APL_EDUCATOR_DATA.'['.Apollo_DB_Schema::_APL_EDUCATOR_URL.']' => array(
                'label'     => __( 'The Website', 'apollo' ), 
                'validate'  => 'url' 
            ),
            ''.Apollo_DB_Schema::_APL_EDUCATOR_DATA.'['.Apollo_DB_Schema::_APL_EDUCATOR_BLOG.']' => array(
                'label'     => __( 'The Blog URL', 'apollo' ), 
                'validate'  => 'url' 
            ),
            
            ''.Apollo_DB_Schema::_APL_EDUCATOR_DATA.'['.Apollo_DB_Schema::_APL_EDUCATOR_FB.']' => array(
                'label'     => __( 'The Facebook', 'apollo' ), 
                'validate'  => 'url' 
            ),
            ''.Apollo_DB_Schema::_APL_EDUCATOR_DATA.'['.Apollo_DB_Schema::_APL_EDUCATOR_TW.']' => array(
                'label'     => __( 'The Twitter', 'apollo' ), 
                'validate'  => 'url' 
            ),
            ''.Apollo_DB_Schema::_APL_EDUCATOR_DATA.'['.Apollo_DB_Schema::_APL_EDUCATOR_LK.']' => array(
                'label'     => __( 'The LinkedIn', 'apollo' ), 
                'validate'  => 'url' 
            ),
            ''.Apollo_DB_Schema::_APL_EDUCATOR_DATA.'['.Apollo_DB_Schema::_APL_EDUCATOR_INS.']' => array(
                'label'     => __( 'The Instagram', 'apollo' ), 
                'validate'  => 'url' 
            ),
            ''.Apollo_DB_Schema::_APL_EDUCATOR_DATA.'['.Apollo_DB_Schema::_APL_EDUCATOR_YT.']' => array(
                'label'     => __( 'The youtube', 'apollo' ), 
                'validate'  => 'url' 
            ),
            ''.Apollo_DB_Schema::_APL_EDUCATOR_DATA.'['.Apollo_DB_Schema::_APL_EDUCATOR_PTR.']' => array(
                'label'     => __( 'The Pinterest', 'apollo' ), 
                'validate'  => 'url' 
            ),
            ''.Apollo_DB_Schema::_APL_EDUCATOR_DATA.'['.Apollo_DB_Schema::_APL_EDUCATOR_FLI.']' => array(
                'label'     => __( 'The Flickr', 'apollo' ), 
                'validate'  => 'url' 
            ),
        ); 
        
        // Admin column
        add_filter( 'manage_edit-'.$this->type.'_columns', array( $this, 'edit_columns' ) );
        add_action( 'manage_'.$this->type.'_posts_custom_column', array( $this, 'custom_columns' ), 2 );
        add_filter( 'manage_edit-'.$this->type.'_sortable_columns', array( $this, 'custom_columns_sort' ) );
        
        // Bulk / Quick edit
        /** @Ticket #14398 */
        /** @Ticket #14443 */
        add_action( 'quick_edit_custom_box', array( $this, 'quick_edit' ), 10, 2 );
        add_action( 'save_post', array( $this, 'bulk_and_quick_edit_save_post' ), 10, 2 );
        
        /// Event filtering
        add_action( 'restrict_manage_posts', array( $this, '_filters' ) );
        
        $this->meta_tbl  = $wpdb->{Apollo_Tables::_APL_EDUCATOR_META};

        // Update query
        if ( $typenow == $this->type && $pagenow == 'edit.php' ) {
//            add_filter( 'posts_join' , array( $this, 'filter_meta_join' ) );
//            add_filter( 'posts_where', array( $this, 'posts_where' ) );
//            add_filter( 'posts_orderby', array( $this, 'change_order_by' ) );  
        }
        
        add_action( 'delete_post', array( $this, 'delete_post' ) );
        
        parent::__construct();
    }
    
    public function delete_post( $post_id ) {
        // Delete data in program(s) educator
        $apl_query_pe = new Apl_Query( Apollo_Tables::_APL_PROGRAM_EDUCATOR );
        $apl_query_pe->delete( "edu_id = '$post_id'" );
        
        // Delete all data in agency educator
        $apl_query_ae = new Apl_Query( Apollo_Tables::_APL_AGENCY_EDUCATOR );
        $apl_query_ae->delete( "edu_id = $post_id" );
    }
    
    /**
     * Custom post join 
     * 
     * @access public
     * @return string
     */
    public function filter_meta_join( $join ) {
        global $wpdb;
        
        $join = str_replace( 'post_id', 'apollo_educator_id', $this->replace_meta_table( $join ) );
        
        // Join to filter event not exprise
        $join .= "
            LEFT JOIN {$this->meta_tbl} mt ON mt.apollo_educator_id = ". $wpdb->posts .".ID
        ";
        
        return $join;
    }
    
    /**
     * Replace meta table 
     * 
     * @access public
     * @return string
     */
    public function replace_meta_table( $str ) {
        global $wpdb;
        return str_replace( $wpdb->postmeta, $wpdb->apollo_educatormeta, $str);
    }
    
    /**
     * Custom post where 
     * 
     * @access public
     * @return string
     */
    public function posts_where( $where ) {
        
        return $where;
    }
    
    /**
     * Change order by
     * 
     * @access public
     * @return string
     */
    public function change_order_by( $str ) {
        
        $str = " mt.meta_value ASC ";
        
        return $str;
    }
    
    public function _filters() {
        global $typenow;
        
        if ( $typenow !== $this->type ) {
            return ;
        }

        // show drop down
        apollo_dropdown_categories( $this->type. '-type' );
    }
    
     /**
     * Custom Quick Edit form
     * 
     * @access public
     * @param mixed $column_type
	 * @param mixed $post_type
    */
    public function quick_edit( $column_type, $post_type ) {
        
        if ( $column_type == 'name' ) {
            require_once APOLLO_ADMIN_DIR. '/views/'.$this->type.'/html-quick-edit.php';
        }
    }

    public function bulk_edit_save( $post_id, $venue ) {
        
    }
    
    /**
     * Quick and Bulk saving
     * 
     * @access public
     * @param mixed $post_id
     * @param mixed $post
     */
    public function bulk_and_quick_edit_save_post( $post_id, $post ) {
       
        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}
        
        // Don't save revisions and autosaves
		if ( wp_is_post_revision( $post_id ) || wp_is_post_autosave( $post_id ) ) {
			return $post_id;
		}
        
        if ( $post->post_type != $this->type ) {
            return $post_id;
        }
        
        // Check user permission
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return $post_id;
		}
        
        $nonce = 'apollo_'.$this->type.'_bulk_edit_nonce';
        if ( isset( $_REQUEST[$nonce] ) && ! wp_verify_nonce( $_REQUEST[$nonce], $nonce ) ) {
			return $post_id;
		}
        
        $post = get_venue( $post );
        
        if ( ! empty( $_REQUEST['apollo_'.$this->type.'_quick_edit'] ) ) {
           
			$this->quick_edit_save( $post_id, $post );
		} else {
			$this->bulk_edit_save( $post_id, $post );
		}

		return $post_id;
    }
    
    /**
	 * Change the columns shown in admin.
	 */
	public function edit_columns( $existing_columns ) {
       
		if ( empty( $existing_columns ) && ! is_array( $existing_columns ) ) {
            $existing_columns = array();
        }

		unset( $existing_columns['title'], $existing_columns['comments'], $existing_columns['date'] );

		$columns = array();
		$columns["cb"] = "<input type=\"checkbox\" />";
		$columns["name"] = __( 'Name', 'apollo' );
        $columns["state"] = __( 'State', 'apollo' );
        $columns["city"] = __( 'City', 'apollo' );
        $columns["zip"] = __( 'Zip', 'apollo' );
        $columns["educator-type"] = __( 'Educator Type(s)', 'apollo' );
        $columns['program'] = __( 'Associated Program(s)', 'apollo' );
        $columns['username'] = __( 'Associated User(s)', 'apollo' );

        if (Apollo_App::is_avaiable_module(Apollo_DB_Schema::_AGENCY_PT)) {
            $columns['agency'] = __( 'Agency', 'apollo' );
        }

		return array_merge( $columns, $existing_columns );
	}
    
    /**
	 * Define our custom columns shown in admin.
	 * @param  string $column
	 */
	public function custom_columns( $column ) {
        global $post, $the_educator;
        
        if ( empty( $the_educator ) || $the_educator->id != $post->ID ) {
            
            $the_educator = get_educator( $post );
        }
       
        
        switch ( $column ) {
            
            case "name" :
				$edit_link = get_edit_post_link( $post->ID );
				$title = _draft_or_post_title();
				$post_type_object = get_post_type_object( $post->post_type );
				$can_edit_post = current_user_can( $post_type_object->cap->edit_post, $post->ID );

				echo '<strong><a class="row-title" href="' . esc_url( $edit_link ) .'">' . $title.'</a>';

				_post_states( $post );

				echo '</strong>';

				if ( $post->post_parent > 0 )
					echo '&nbsp;&nbsp;&larr; <a href="'. get_edit_post_link($post->post_parent) .'">'. get_the_title($post->post_parent) .'</a>';

				// Excerpt view
				if (isset($_GET['mode']) && $_GET['mode']=='excerpt') echo apply_filters('the_excerpt', $post->post_excerpt);

				// Get actions
				$actions = array();

				$actions['id'] = 'ID: ' . $post->ID;

				if ( $can_edit_post && 'trash' != $post->post_status ) {
					$actions['edit'] = '<a href="' . get_edit_post_link( $post->ID, true ) . '" title="' . esc_attr( __( 'Edit this item', 'apollo' ) ) . '">' . __( 'Edit', 'apollo' ) . '</a>';
					$actions['inline hide-if-no-js'] = '<a href="#" class="editinline" title="' . esc_attr( __( 'Edit this item inline', 'apollo' ) ) . '">' . __( 'Quick&nbsp;Edit', 'apollo' ) . '</a>';
				}
				if ( current_user_can( $post_type_object->cap->delete_post, $post->ID ) ) {
					if ( 'trash' == $post->post_status )
						$actions['untrash'] = "<a title='" . esc_attr( __( 'Restore this item from the Trash', 'apollo' ) ) . "' href='" . wp_nonce_url( admin_url( sprintf( $post_type_object->_edit_link . '&amp;action=untrash', $post->ID ) ), 'untrash-post_' . $post->ID ) . "'>" . __( 'Restore', 'apollo' ) . "</a>";
					elseif ( EMPTY_TRASH_DAYS )
						$actions['trash'] = "<a class='submitdelete' title='" . esc_attr( __( 'Move this item to the Trash', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID ) . "'>" . __( 'Trash', 'apollo' ) . "</a>";
					if ( 'trash' == $post->post_status || !EMPTY_TRASH_DAYS )
						$actions['delete'] = "<a class='submitdelete' title='" . esc_attr( __( 'Delete this item permanently', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID, '', true ) . "'>" . __( 'Delete Permanently', 'apollo' ) . "</a>";
				}
				if ( $post_type_object->public ) {
					if ( in_array( $post->post_status, array( 'pending', 'draft', 'future' ) ) ) {
						if ( $can_edit_post )
							$actions['view'] = '<a href="' . esc_url( add_query_arg( 'preview', 'true', get_permalink( $post->ID ) ) ) . '" title="' . esc_attr( sprintf( __( 'Preview &#8220;%s&#8221;', 'apollo' ), $title ) ) . '" rel="permalink">' . __( 'Preview', 'apollo' ) . '</a>';
					} elseif ( 'trash' != $post->post_status ) {
						$actions['view'] = '<a href="' . get_permalink( $post->ID ) . '" title="' . esc_attr( sprintf( __( 'View &#8220;%s&#8221;', 'apollo' ), $title ) ) . '" rel="permalink">' . __( 'View', 'apollo' ) . '</a>';
					}
				}
                
				$actions = apply_filters( 'post_row_actions', $actions, $post );

				echo '<div class="row-actions">';

				$i = 0;
				$action_count = sizeof($actions);

				foreach ( $actions as $action => $link ) {
					++$i;
					( $i == $action_count ) ? $sep = '' : $sep = ' | ';
					echo "<span class='$action'>$link$sep</span>";
				}
				echo '</div>';
                get_inline_data( $post );
                
                $commuicate_js_str = '';
                $_a_data = array(
                    Apollo_DB_Schema::_APL_EDUCATOR_ADD1,
                    Apollo_DB_Schema::_APL_EDUCATOR_ADD2,
                    Apollo_DB_Schema::_APL_EDUCATOR_CITY,
                    Apollo_DB_Schema::_APL_EDUCATOR_STATE,
                    Apollo_DB_Schema::_APL_EDUCATOR_ZIP,
                    Apollo_DB_Schema::_APL_EDUCATOR_COUNTY,
                    Apollo_DB_Schema::_APL_EDUCATOR_PHONE1,
                    Apollo_DB_Schema::_APL_EDUCATOR_EMAIL,
                    Apollo_DB_Schema::_APL_EDUCATOR_URL,
                    Apollo_DB_Schema::_APL_EDUCATOR_FAX,
                    Apollo_DB_Schema::_APL_EDUCATOR_REGION,
                );
                
                foreach ( $_a_data as $k ) {
                    $commuicate_js_str .= '<div class="'.Apollo_DB_Schema::_APL_EDUCATOR_DATA.'['.$k.']">' . $the_educator->get_meta_data( Apollo_DB_Schema::_APL_EDUCATOR_DATA. '['. $k .']' ) . '</div>';
                }
                
				echo '
					<div class="hidden" id="_inline_' . $post->ID . '">
						<div class="menu_order">' . $post->menu_order . '</div>
						'.$commuicate_js_str.'
					</div>
				';
                
			break;
           
        
            case 'city':
                $val = Apollo_App::apollo_get_meta_data( $the_educator->id, Apollo_DB_Schema::_APL_EDUCATOR_CITY, Apollo_DB_Schema::_APL_EDUCATOR_DATA );
                echo $val ? $val : '-';
            break;
        
            case 'state':
                $val = Apollo_App::apollo_get_meta_data( $the_educator->id, Apollo_DB_Schema::_APL_EDUCATOR_STATE, Apollo_DB_Schema::_APL_EDUCATOR_DATA );
                echo $val ? $val : '-';
            break;
        
            case 'zip':
                $val = Apollo_App::apollo_get_meta_data( $the_educator->id, Apollo_DB_Schema::_APL_EDUCATOR_ZIP, Apollo_DB_Schema::_APL_EDUCATOR_DATA );
                echo $val ? $val : '-';
            break;
            case "educator-type" :
                
                $terms = get_the_terms( $post->ID, $column );
           
                $termlist = array();
                
                if ( $terms ):
                    foreach ( $terms as $term ) {
                        if ( $term->parent ) continue;  // Not display sub menu in the list event page

                        $termlist[] = '<a href="' . admin_url( 'edit.php?' . $column . '=' . $term->slug . '&post_type='.$this->type.'' ) . ' ">' . $term->name . '</a>';
                    }
                endif;
                
                echo $termlist ? implode( ', ', $termlist ) : '<span class="na">&ndash;</span>';

			break;
            
            case 'username':
                $this->associationUserModuleListComp->render(array(
                    'id' => $post->ID,
                    'post_type' => 'educator',
                ));
            break;

            case 'agency':
                $this->associationAgencyModuleComp->render(array(
                    'id' => $post->ID,
                    'post_type' => 'educator',
                ));
                break;

            case 'program':
                
                $apl_query = new Apl_Query( Apollo_Tables::_APL_PROGRAM_EDUCATOR );
                $programs = $apl_query->get_where( " edu_id = $post->ID " );
                if ( $programs ):
                ?>
                <a href="#TB_inline?height=100&amp;width=400&amp;inlineId=educator-programs-<?php echo $post->ID ?>" title="<?php echo _draft_or_post_title() ?>" class="thickbox" ><?php _e( 'View Programs', 'apollo' ) ?></a>
                <div id="educator-programs-<?php echo $post->ID ?>" style="display:none">
                    <ul>
                        <?php foreach ( $programs as $p ): 
                            $p = get_post( $p->prog_id );
                        ?>
                        <li><a href="<?php echo get_edit_post_link( $p->ID, true ) ?>"><?php  echo $p->post_title ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <?php
                else:
                    echo '-';
                endif;
            break;
        }
        
    }
    
    /**
	 * Make columns sortable
	 *
	 * @access public
	 * @param mixed $columns
	 * @return array
	 */
	public function custom_columns_sort( $columns ) {
		$custom = array(
			'name'  => 'name'
		);
		return wp_parse_args( $custom, $columns );
	}
    
    private function quick_edit_save( $post_id, $post ) {
        
        $educator = get_educator( $post );
        $data = Apollo_App::unserialize($educator->get_meta_data(Apollo_DB_Schema::_APL_EDUCATOR_DATA));


        $inputData = $_POST[Apollo_DB_Schema::_APL_EDUCATOR_DATA];

        if ($inputData) {
            foreach($inputData as $key => $value) {
                if (isset($data[$key])) {
                    $data[$key] = $value;
                }
            }
        }

        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_EDUCATOR_DATA, serialize($data) );

        update_apollo_meta($post_id, Apollo_DB_Schema::_APL_EDUCATOR_CITY, !empty($_data[Apollo_DB_Schema::_APL_EDUCATOR_CITY]) ? $_data[Apollo_DB_Schema::_APL_EDUCATOR_CITY] : '');
        update_apollo_meta($post_id, Apollo_DB_Schema::_APL_EDUCATOR_STATE, !empty($_data[Apollo_DB_Schema::_APL_EDUCATOR_STATE]) ? $_data[Apollo_DB_Schema::_APL_EDUCATOR_STATE] : '');
        update_apollo_meta($post_id, Apollo_DB_Schema::_APL_EDUCATOR_ZIP, !empty($_data[Apollo_DB_Schema::_APL_EDUCATOR_ZIP]) ? $_data[Apollo_DB_Schema::_APL_EDUCATOR_ZIP] : '');
      
        do_action( 'apollo_'.$this->type.'_quick_edit_save', $post );
    }
    
}    
    
endif;    

new Apollo_Admin_Educator();
