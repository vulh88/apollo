<?php

if ( !defined( 'ABSPATH' ) ) {
    return;
}

if ( !class_exists( 'Apollo_Admin_Advanced_Search' ) ):
    
class Apollo_Admin_Advanced_Search {

    private $_data;
    private $_template;

    public function __construct( $post_type ) {
       
        switch ( $post_type ) {
            case 'event':
                $this->_template = APOLLO_ADMIN_DIR. '/views/event/html-advanced-search.php';
                $this->handle_search_event();
                break;
        }
        
    }
    
    public function handle_search_event() {
        
        if ( isset( $_REQUEST['search'] ) ) {
            $this->search_event( $_REQUEST );
        }
        
        $orgs = $this->get_orgs_selected_by_events();
        $this->render_html( array( 'orgs' => $orgs ) );
    }
    
    public function render_html( $data ) {
        ob_start();
        include $this->_template;
        echo ob_get_clean();
    }
    
    public function search_event( $data ) {
        global $wpdb;
        $org_id     = $_REQUEST['apollo_event_org'];
        $start_date = $_REQUEST['start_date'];
        $end_date   = $_REQUEST['end_date'];
        
        $_join = array();
        $_where_join = array();
        
        if ( $org_id ) {
            $_alias_meta_org = 'mt_org';
            $_join[] = "INNER JOIN {$wpdb->apollo_eventmeta} {$_alias_meta_org} ON {$_alias_meta_org}.apollo_event_id = p.ID";
            $_where_join[] = "
                AND {$_alias_meta_org}.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_ORGANIZATION ."'
                AND CAST( {$_alias_meta_org}.meta_value AS CHAR ) = '{$org_id}'
            ";
        }
        
        if ( $start_date ) {
            $_alias_meta_start_date = 'mt_start_date';
            
            $_join[] = "INNER JOIN {$wpdb->apollo_eventmeta} {$_alias_meta_start_date}
                        ON {$_alias_meta_start_date}.apollo_event_id = p.ID";
            $_where_join[] = "
                AND {$_alias_meta_start_date}.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
                AND CAST( {$_alias_meta_start_date}.meta_value AS DATE ) >= '{$start_date}'
            ";
        }
        
        if ( $end_date ) {
            $_alias_meta_end_date = 'mt_end_date';
            
            $_join[] = "INNER JOIN {$wpdb->apollo_eventmeta} {$_alias_meta_end_date}
                        ON {$_alias_meta_end_date}.apollo_event_id = p.ID";
            $_where_join[] = "
                AND {$_alias_meta_end_date}.meta_key = '". Apollo_DB_Schema::_APOLLO_EVENT_START_DATE ."'
                AND CAST( {$_alias_meta_end_date}.meta_value AS DATE ) >= '{$start_date}'
            ";
        }
       
        $join_str  = implode( ' ', $_join);
        $where_str = implode( ' ', $_where_join );

        $sql = " 
            SELECT SQL_CALC_FOUND_ROWS p.*
            FROM {$wpdb->posts} p
            {$join_str}    
            WHERE p.post_type = '". Apollo_DB_Schema::_EVENT_PT ."' AND ( p.post_status = 'publish' or p.post_status = 'private' )    
            {$where_str}    
        ";
       
        $this->_data = $wpdb->get_results( $sql );
    }
    
    public function get_orgs_selected_by_events( $offset = 0, $limit = 2000 ) {
        
        global $wpdb;
        
        $sql = "SELECT SQL_CALC_FOUND_ROWS p_org.* 

            FROM {$wpdb->posts} p_org

            JOIN {$wpdb->posts} p_event ON p_event.post_type =  '". Apollo_DB_Schema::_EVENT_PT ."'
            AND ( p_event.post_status = 'publish' OR p_event.post_status = 'private' )
            
            JOIN {$wpdb->apollo_eventmeta} e_mt ON e_mt.apollo_event_id = p_event.ID 
            AND e_mt.meta_value = p_org.ID

            WHERE p_org.post_type =  'organization'
            AND ( p_org.post_status =  'publish' OR p_org.post_status =  'private'
            )

            AND e_mt.meta_key =  '". Apollo_DB_Schema::_APOLLO_EVENT_ORGANIZATION ."'

            GROUP BY p_org.ID
            LIMIT {$offset} , {$limit}
        ";
            
        return $wpdb->get_results( $sql );    
    }
}    
    
endif;