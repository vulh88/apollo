<?php
/**
 * Admin functions for the venue post type
 *
 * @author 		vulh
 * @category 	admin
 * @package 	inc/admin/post-types
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Apollo_Admin_CPT' ) ) {
    include( 'class-apollo-admin-cpt.php' );
}

if ( ! class_exists( 'Apollo_Admin_Edu_Evaluation' ) ) :
    
class Apollo_Admin_Grant_Education extends Apollo_Admin_CPT {
    
    public function __construct() {
        global $wpdb;
        $this->type = Apollo_DB_Schema::_GRANT_EDUCATION;
        $this->meta_tbl  = $wpdb->{Apollo_Tables::_APL_GRANT_EDU_META};
        
        // Admin column
        add_filter( 'manage_edit-'.$this->type.'_columns', array( $this, 'edit_columns' ) );
        add_action( 'manage_'.$this->type.'_posts_custom_column', array( $this, 'custom_columns' ), 2 );
        add_filter( 'manage_edit-'.$this->type.'_sortable_columns', array( $this, 'custom_columns_sort' ) );
        
        // Bulk / Quick edit
        /** @Ticket #14398 */
        
        /// Event filtering
        add_action( 'restrict_manage_posts', array( $this, '_filters' ) );
        
        add_action( 'delete_post', array( $this, 'delete_post' ) );
        
        $this->requireds = array(
      
            Apollo_DB_Schema::_APL_EDU_EVAL_EDUCATOR => array( 
                'label'     => __( 'Program Organization', 'apollo' ), 
                'validate'  => 'required' 
            ),
            
            Apollo_DB_Schema::_APL_EDU_EVAL_PROG => array( 
                'label'     => __( 'Program ', 'apollo' ), 
                'validate'  => 'required' 
            ),
        );
        
        /**
         * filter join
         */
        add_filter('posts_join', array($this, 'filter_meta_join'));
        add_filter('posts_where', array($this, 'filter_meta_where'));
        
        parent::__construct();
    }
    
    public function filter_meta_join($join) {
        global $wpdb;
        
        if ( isset( $_GET['educator_id'] ) && $_GET['educator_id'] ) {
            $join .= "
                INNER JOIN {$this->meta_tbl} mt_edu ON mt_edu.apollo_grant_education_id = ". $wpdb->posts .".ID
            ";
        }
        
        if ( isset( $_GET['program_id'] ) && $_GET['program_id'] ) {
            $join .= "
                INNER JOIN {$this->meta_tbl} mt_prog ON mt_prog.apollo_grant_education_id = ". $wpdb->posts .".ID   
            ";
        }    
        
        return $join;
    }
    
    public function filter_meta_where($where) {
        
        if ( isset( $_GET['educator_id'] ) && $_GET['educator_id'] ) {
            $where .= "
                AND mt_edu.meta_key = '".Apollo_DB_Schema::_APL_EDU_EVAL_EDUCATOR."'
                AND mt_edu.meta_value = ".$_GET['educator_id']."    
            ";
        }
        
        if ( isset( $_GET['program_id'] ) && $_GET['program_id'] ) {
            $where .= "
                AND mt_prog.meta_key = '".Apollo_DB_Schema::_APL_EDU_EVAL_PROG."'
                AND mt_prog.meta_value = ".$_GET['program_id']."    
            ";
        }
        
        return $where;
    }
    
    /**
     * Replace meta table 
     * 
     * @access public
     * @return string
     */
    public function replace_meta_table( $str ) {
        global $wpdb;
        return str_replace( $wpdb->postmeta, $wpdb->edu_evaluationmeta, $str);
    }
    
    public function delete_post( $post_id ) {
        
    }
    
    public function _filters() {
        global $typenow;
        
        if ( $typenow !== $this->type ) {
            return ;
        }
    }
    
    /**
	 * Change the columns shown in admin.
	 */
	public function edit_columns( $existing_columns ) {
       
		if ( empty( $existing_columns ) && ! is_array( $existing_columns ) ) {
            $existing_columns = array();
        }

		unset( $existing_columns['title'], $existing_columns['comments'] );

		$columns = array();
		$columns["cb"] = "<input type=\"checkbox\" />";
        
         // Get cus field title
        $cus_title = get_option('apl_field_listing_'.$this->type, false);
        if ($cus_title) {
            $field = Apollo_Custom_Field::get_field_by_id($cus_title, $this->type);
            $columns['title'] = $field->label;
        } else {
            $columns["title"] = __( 'Title', 'apollo' );
        }

        $moreFields = get_option('apl_field_more_'.$this->type, false);
    
        if ($moreFields) {
            foreach( $moreFields as $fieldId ) {
                $_fieldData = Apollo_Custom_Field::get_field_by_id($fieldId, $this->type);
                $columns[$_fieldData->name] = $_fieldData->label;
            }
        }
        
        //TriLm add column
        $columns['submit_user'] = __( 'Submitted User', 'apollo' );

		return array_merge( $columns, $existing_columns );
	}
    
    /**
	 * Define our custom columns shown in admin.
	 * @param  string $column
	 */
	public function custom_columns( $column ) {
        global $post, $the_org;
        
        if ( empty( $the_org ) || $the_org->id != $post->ID ) {
            
            $the_org = get_org( $post );
        }
        
        $eduEvalObj = get_edu_evaluation($post->ID);
      
        switch ( $column ) {
            
            case "title" :
               
                $cus_title = get_option('apl_field_listing_'.$this->type, false);
                $field = Apollo_Custom_Field::get_field_by_id($cus_title, $this->type);
             
                $title = '-';
                if ( $field ) {
                    $title = $eduEvalObj->get_meta_data($field->name, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA, '--');
                    if ( Apollo_Custom_Field::get_data_source($field) ) {
                        $p = get_post($title);
                        $title = $p ? $p->post_title : '';
                    }
                }
             
				$edit_link = get_edit_post_link( $post->ID );
				
				$post_type_object = get_post_type_object( $post->post_type );
				$can_edit_post = current_user_can( $post_type_object->cap->edit_post, $post->ID );

				echo '<strong><a class="row-title" href="' . esc_url( $edit_link ) .'">' . $title.'</a>';

				_post_states( $post );

				echo '</strong>';

				if ( $post->post_parent > 0 )
					echo '&nbsp;&nbsp;&larr; <a href="'. get_edit_post_link($post->post_parent) .'">'. get_the_title($post->post_parent) .'</a>';

				// Excerpt view
				if (isset($_GET['mode']) && $_GET['mode']=='excerpt') echo apply_filters('the_excerpt', $post->post_excerpt);

				// Get actions
				$actions = array();

				$actions['id'] = 'ID: ' . $post->ID;

				if ( $can_edit_post && 'trash' != $post->post_status ) {
					$actions['edit'] = '<a href="' . get_edit_post_link( $post->ID, true ) . '" title="' . esc_attr( __( 'Edit this item', 'apollo' ) ) . '">' . __( 'Edit', 'apollo' ) . '</a>';
					$actions['inline hide-if-no-js'] = '<a href="#" class="editinline" title="' . esc_attr( __( 'Edit this item inline', 'apollo' ) ) . '">' . __( 'Quick&nbsp;Edit', 'apollo' ) . '</a>';
				}
				if ( current_user_can( $post_type_object->cap->delete_post, $post->ID ) ) {
					if ( 'trash' == $post->post_status )
						$actions['untrash'] = "<a title='" . esc_attr( __( 'Restore this item from the Trash', 'apollo' ) ) . "' href='" . wp_nonce_url( admin_url( sprintf( $post_type_object->_edit_link . '&amp;action=untrash', $post->ID ) ), 'untrash-post_' . $post->ID ) . "'>" . __( 'Restore', 'apollo' ) . "</a>";
					elseif ( EMPTY_TRASH_DAYS )
						$actions['trash'] = "<a class='submitdelete' title='" . esc_attr( __( 'Move this item to the Trash', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID ) . "'>" . __( 'Trash', 'apollo' ) . "</a>";
					if ( 'trash' == $post->post_status || !EMPTY_TRASH_DAYS )
						$actions['delete'] = "<a class='submitdelete' title='" . esc_attr( __( 'Delete this item permanently', 'apollo' ) ) . "' href='" . get_delete_post_link( $post->ID, '', true ) . "'>" . __( 'Delete Permanently', 'apollo' ) . "</a>";
				}
				if ( $post_type_object->public ) {
					if ( in_array( $post->post_status, array( 'pending', 'draft', 'future' ) ) ) {
						if ( $can_edit_post )
							$actions['view'] = '<a href="' . esc_url( add_query_arg( 'preview', 'true', get_permalink( $post->ID ) ) ) . '" title="' . esc_attr( sprintf( __( 'Preview &#8220;%s&#8221;', 'apollo' ), $title ) ) . '" rel="permalink">' . __( 'Preview', 'apollo' ) . '</a>';
					} elseif ( 'trash' != $post->post_status ) {
						$actions['view'] = '<a href="' . get_permalink( $post->ID ) . '" title="' . esc_attr( sprintf( __( 'View &#8220;%s&#8221;', 'apollo' ), $title ) ) . '" rel="permalink">' . __( 'View', 'apollo' ) . '</a>';
					}
				}

				$actions = apply_filters( 'post_row_actions', $actions, $post );

				echo '<div class="row-actions">';

				$i = 0;
				$action_count = sizeof($actions);

				foreach ( $actions as $action => $link ) {
					++$i;
					( $i == $action_count ) ? $sep = '' : $sep = ' | ';
					echo "<span class='$action'>$link$sep</span>";
				}
				echo '</div>';
                get_inline_data( $post );
                
				echo '
					<div class="hidden" id="_inline_' . $post->ID . '">
						<div class="menu_order">' . $post->menu_order . '</div>
					</div>
				';
                
			break;

            //Tri LM -- submit_user column data
            case "submit_user":
                $author  = $post->post_author;
                if($author){
                    $authorName = get_the_author_meta( 'user_nicename' , $author );
                    $authorLink = get_edit_user_link($author);
                    echo '<a href="'.$authorLink.'">'.$authorName.'</a>';
                }

                break;
                
            default:
                echo $eduEvalObj->get_meta_data($column, Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA, '--');
                
        }
        
    }
    
    /**
	 * Make columns sortable
	 *
	 * @access public
	 * @param mixed $columns
	 * @return array
	 */
	public function custom_columns_sort( $columns ) {
        $custom = array(
            'submit_user'               => 'submit_user'
        );
		return wp_parse_args( $custom, $columns );
	}
    
    private function quick_edit_save( $post_id, $post ) {
         
        $org = get_org( $post );
        
        // Check and save event data
        $_address = $_POST[Apollo_DB_Schema::_APL_ORG_ADDRESS];
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_ORG_ADDRESS, serialize( $_address ) );
        
        $_data = $_POST[Apollo_DB_Schema::_APL_ORG_DATA];
        
        if ( isset( $_data[Apollo_DB_Schema::_ORG_WEBSITE_URL] ) 
            && ! filter_var( $_data[Apollo_DB_Schema::_ORG_WEBSITE_URL], FILTER_VALIDATE_URL ) ) {
            $_data[Apollo_DB_Schema::_ORG_WEBSITE_URL] = $org->get_meta_data( Apollo_DB_Schema::_ORG_WEBSITE_URL, Apollo_DB_Schema::_APL_ORG_DATA );
        }
        
        if ( isset( $_data[Apollo_DB_Schema::_ORG_EMAIL] ) 
            && !is_email( $_data[Apollo_DB_Schema::_ORG_EMAIL] ) ) {
            $_data[Apollo_DB_Schema::_ORG_EMAIL] = $org->get_meta_data( Apollo_DB_Schema::_ORG_EMAIL, Apollo_DB_Schema::_APL_ORG_DATA );
        }
        
        update_apollo_meta( $post_id, Apollo_DB_Schema::_APL_ORG_DATA, serialize( $_data ) );
        
        do_action( 'apollo_'.$this->type.'_quick_edit_save', $post );
    }
    
}    
    
endif;    

return new Apollo_Admin_Grant_Education();