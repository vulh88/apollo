<?php
/**
 * Admin functions for page post type
 *
 * @author 	    TriLM
 * @category 	admin
 * @package 	inc/admin/post-types
 */
if ( ! class_exists( 'Apollo_Admin_CPT' ) ) {
    include( 'class-apollo-admin-cpt.php' );
}

class Apollo_Admin_Page extends Apollo_Admin_CPT {
    protected $globalPages ;

    public function __construct() {
        global $wpdb;

        require_once APOLLO_INCLUDES_DIR. '/admin/post-types/meta-boxes/class-apollo-admin-page-meta-boxes.php';
        $this->globalPages = Apollo_Page_Creator::getPagesDefinition();
        $this->type = 'page';
        add_filter( 'manage_edit-'.$this->type.'_columns', array( $this, 'edit_columns' ) );
        add_action( 'manage_'.$this->type.'_posts_custom_column', array( $this, 'custom_columns' ), 2 );

        add_action( 'restrict_manage_posts', array( $this, 'global_page_filters' ) );
        add_filter( 'parse_query', array( $this, 'global_page_filters_query' ) );

        parent::__construct();
    }
    


    
     /**
     * Custom Quick Edit form
     * 
     * @access public
     * @param mixed $column_type
	 * @param mixed $post_type
    */
    public function quick_edit( $column_type, $post_type ) {
        
        if ( $column_type != 'name' ) {
            return;
        }


    }

    public function bulk_edit_save( $post_id, $venue ) {
        
    }
    
    /**
     * Quick and Bulk saving
     * 
     * @access public
     * @param mixed $post_id
     * @param mixed $post
     */
    public function bulk_and_quick_edit_save_post( $post_id, $post ) {
       

    }
    
    /**
	 * Change the columns shown in admin.
	 */
	public function edit_columns( $existing_columns ) {
        $mergePos = 2;
        $merge = array_slice($existing_columns, 0, $mergePos, true) +
            array("global_page" => __(  'Global page', 'apollo' )) +
            array_slice($existing_columns, $mergePos, count($existing_columns) - 1, true) ;



        return $merge;
    }
    
    /**
	 * Define our custom columns shown in admin.
	 * @param  string $column
	 */
	public function custom_columns( $column ) {
        global $post;
        switch ( $column ) {
            case 'global_page' :
                echo $this->checkGlobalPage($post);
                break;
        }
        
    }
    function global_page_filters() {
         global $typenow;
         if($typenow == $this->type){
             $isGlobalPage = array(
                 'no' => __('No','apollo'),
                 'yes' => __('Yes','apollo'),
             );
            echo "<select name='global_page_filters'>";
            echo "<option value=''>".__('Global page','apollo')."</option>";
            foreach($isGlobalPage as $k => $item){
                $selected = '';
                if($_GET['global_page_filters'] == $k){
                     $selected = 'selected';
                }
                echo "<option value='{$k}' {$selected}  >{$item}</option>";
            }

            echo "</select>";
         }
    }

    protected function checkGlobalPage( $post ) {
        $pages =  $this->globalPages;
        $isGlobalPage = isset($pages[$post->post_name])?true:false;


        if($isGlobalPage){
            update_post_meta($post->ID,Apollo_DB_Schema::_IS_GLOBAL_PAGE,'yes');
            return 'Yes';
        }
        update_post_meta($post->ID,Apollo_DB_Schema::_IS_GLOBAL_PAGE,'no');
        return 'No';

    }

    function global_page_filters_query($query){
        global $typenow;
        if($typenow == $this->type && $query->is_main_query()){
            if(isset($_GET['global_page_filters']) && $_GET['global_page_filters'] ){
             //query global page
             $query->query_vars['meta_key'] = Apollo_DB_Schema::_IS_GLOBAL_PAGE;
             $query->query_vars['meta_value'] = $_GET['global_page_filters'];
            }
        }
    }
    
}    
    


return new Apollo_Admin_Page();