<?php

/*@ticket #17234 0002398 - Recommendation Widgets - [Admin] Official tag for event and post*/
class Apollo_Admin_Post_Taxonomies
{
    /**
     * Apollo_Admin_Post_Taxonomies constructor.
     */
    public function __construct()
    {
        add_action( 'post_tag_add_form_fields', array( $this, 'add_post_tag_fields' ) );
        add_action( 'post_tag_edit_form_fields', array( $this, 'edit_post_tag_fields' ), 10, 2 );

        add_filter( 'manage_edit-post_tag_columns', array( $this, 'post_tag_columns' ) );
        add_filter( 'manage_post_tag_custom_column', array( $this, 'post_tag_column' ), 10, 3 );

        add_action( "create_term", array( $this, 'save_post_tag_official' ), 5, 3 );
        add_action( 'created_term', array( $this, 'save_post_tag_official' ), 10, 3 );
        add_action( 'edit_term', array( $this, 'save_post_tag_official' ), 10, 3 );
    }

    /**
     * @param $columns
     * @return array
     */
    public function post_tag_columns($columns ) {
        $new_columns          = array();
        $new_columns['official'] = __( 'Official', 'apollo' );
        return array_merge( $columns, $new_columns );
    }

    /**
     * @param $columns
     * @param $column
     * @param $id
     * @return string
     */
    public function post_tag_column($columns, $column, $id ) {

        if ( $column == 'official' ) {
            $official = get_apollo_term_meta( $id, Apollo_DB_Schema::_APL_POST_TAG_OFFICIAL, true );

            $columns .= $official ? __('Yes', 'apollo') : __('No', 'apollo');
        }

        return $columns;
    }

    /**
     *
     */
    public function add_post_tag_fields(){
        ?>
        <div class="form-field">
            <span for="post-tag-official"><?php _e( 'Official', 'apollo' ) ?>&nbsp;&nbsp;</span>
            <input name="post-tag-official" id="apl-post-tag-official" autocomplete="off" type="checkbox" value="" />
        </div>
        <?php
    }

    /**
     * @param $term
     * @param $taxonomy
     */
    public function edit_post_tag_fields($term, $taxonomy){
        $official = get_apollo_term_meta( $term->term_id, Apollo_DB_Schema::_APL_POST_TAG_OFFICIAL, true );
        ?>
        <tr class="form-field form-required">
            <th scope="row"><label for="post-tag-official"><?php _e('Official', 'apollo') ?></label></th>
            <td><input name="post-tag-official" id="apl-post-tag-official" type="checkbox" value="1" size="40"
                       autocomplete="off" <?php echo $official == 1 ? 'checked' : '' ?> ></td>
        </tr>
        <?php
    }


    /**
     * @param $term_id
     * @param $tt_id
     * @param $taxonomy
     */
    public function save_post_tag_official($term_id, $tt_id, $taxonomy) {
        if ( $taxonomy == 'post_tag' && !isset($_POST['_inline_edit']) ) { // In edit page
            $official = isset($_POST['post-tag-official']) ? 1 : 0;
            update_apollo_term_meta($term_id, Apollo_DB_Schema::_APL_POST_TAG_OFFICIAL, $official);
        }
    }

}

new Apollo_Admin_Post_Taxonomies();