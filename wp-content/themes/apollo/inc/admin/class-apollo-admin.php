<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Apollo Admin.
 *
 * @class 		Apollo_Admin 
 * @author 		vulh
 * @package 	inc/admin
 */
class Apollo_Admin {

	/**
	 * Constructor
	 */
	public function __construct() {
        
        if ( defined( 'DOING_AJAX' ) ) {
            $this->ajax_includes();
            /** @Ticket #14085 - Frontend ajax */
            $headers = getallheaders();
            if ( $headers && !empty($headers['X-Apl-Frontend']) ) {
                return;
            }
        }


        /** @Ticket #14212 */
        add_filter('upload_mimes', array($this, 'my_myme_types'), 1, 1);

        add_action( 'init', array( $this, 'includes' ) );
     
        // unregister the default activity widget
        add_action('wp_dashboard_setup', array( $this, 'remove_dashboard_widgets' ) );
        
        // register your custom activity widget
        add_action('wp_dashboard_setup', array( $this, 'add_custom_dashboard_activity' ) );
        
        // Not apply crop for post or
        add_filter('intermediate_image_sizes_advanced', array( $this, 'sgr_filter_image_sizes' ) );
        add_action( 'save_post', array( $this, 'set_type_post' ));

        add_action( 'post_updated', array( $this, 'clear_featured_blog_home_page_cache' ));

        // Show message at admin panel
        add_action('admin_notices', array( $this, 'showAdminMessages' ));
        
        add_action('admin_menu', array( $this, 'remove_default_post_type' ));

        /**
         * Thienld : if the business module is enabled then Remove sub-menu 'Add' new business inside Admin menu business
         **/
        if( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_BUSINESS_PT ) ) {
            add_action('admin_menu', array( $this, 'remove_add_submenu_business' ),99);
        }

        add_action('admin_init', array( $this, 'handle_admin_initialization' ));
         
        add_action('pre_current_active_plugins', array( $this, 'hidePlugins' ));

        // Not re-sort categories
        add_filter( 'wp_terms_checklist_args', array( $this, 'taxonomy_checklist_checked_ontop_filter' ));

        /** @Ticket #14817 */
        if ( defined('APOLLO_ADMIN_NOTICE') && APOLLO_ADMIN_NOTICE &&
        defined('APOLLO_ADMIN_NOTICE_BLOG_ID') && APOLLO_ADMIN_NOTICE_BLOG_ID == get_current_blog_id()) {
            add_action( 'admin_notices', array($this, 'apollo_show_admin_notice') );
        }
    }

    /**
     * Add new file type
     * @param $mime_types
     * @return mixed
     */
    public function my_myme_types($mime_types){
        $mime_types['eot'] = 'eot';
        $mime_types['svg'] = 'svg';
        $mime_types['ttf'] = 'ttf';
        $mime_types['woff'] = 'woff';
        $mime_types['woff2'] = 'woff2';
        return $mime_types;
    }

    public function apollo_show_admin_notice() {
        $message = __('Our website is currently undergoing scheduled maintenance. Please do not add, delete, modify any data. Sorry for the inconvenience', 'apollo');
        printf('<div class="notice notice-error apl-custom-notice"><h1>%s</h1></div>', esc_html($message));
    }

    function taxonomy_checklist_checked_ontop_filter ($args) {
        $args['checked_ontop'] = false;
        return $args;
    }

    public function remove_add_submenu_business(){
        remove_submenu_page("edit.php?post_type=".Apollo_DB_Schema::_BUSINESS_PT."","post-new.php?post_type=".Apollo_DB_Schema::_BUSINESS_PT."");
    }


    function remove_default_post_type() {
        global $pagenow, $typenow;
     
        if ( ! Apollo_App::is_avaiable_module( 'post' ) ) {
            remove_menu_page('edit.php');
            if ( ( $pagenow == 'post-new.php' || $pagenow == 'edit.php' ) && $typenow == ''  ) {
                wp_safe_redirect( admin_url() );
                exit;
            }
        }
    }

    
    public function showAdminMessages() {
        if (current_user_can('manage_options')) {
            if(isset($_SESSION['messages'])) {
                foreach($_SESSION['messages'] as $key => $arrError) {
                    foreach($arrError as $arrInfo) {
                        $this->showMessage($arrInfo['message'], $arrInfo['error']);
                    }

                }
            }

            unset($_SESSION['messages']);
        }
    }

    private function showMessage($message, $errormsg = false)
    {
        if ($errormsg) {
            echo '<div id="message" class="error">';
        }
        else {
            echo '<div id="message" class="updated fade">';
        }

        echo "<p><strong>$message</strong></p></div>";
    }

    public function sgr_filter_image_sizes( $sizes ) {

        if ( isset( $_SESSION['apl_admin']['post_type'] ) 
            && ( $_SESSION['apl_admin']['post_type'] == 'post' || $_SESSION['apl_admin']['post_type'] == 'page' )  ) {
            $sizes['thumbnail']['crop'] = false;
            $sizes['thumbnail']['width'] = '';
            $sizes['thumbnail']['height'] = '';
        }

        return $sizes;
    }

    public function set_type_post() {
     
        global $typenow;
        if ( $typenow ) {
            $_SESSION['apl_admin']['post_type'] = $typenow;
        }
    }

    /**
     * @Ticket #15013 - Clear Featured blog when update post
     * @param $postID
     */
    public function clear_featured_blog_home_page_cache ($postID) {
        global $typenow;
        if( $typenow === 'post' && Apollo_App::get_network_local_cache(get_current_blog_id())) {
            $smallArticles = array(of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_TOP,0),
                of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_MIDDLE,0),
                of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_BOTTOM,0)
            );
            $largeArticles = array(
                of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_LARGE_LEFT,0),
                of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_LARGE_RIGHT,0)
            );
            $homeFeaturedPost = array_merge($smallArticles, $largeArticles);
            if (in_array($postID, $homeFeaturedPost)) {
                $fileCache = aplc_instance('APLC_Inc_Files_HomeFeaturedBlog');
                $fileCache->remove();
            }
        }
    }
    
    /**
	 * Include any classes we need within admin.
	 */
	public function includes() {
        
        $arr = array(

            APOLLO_ADMIN_DIR .'/class-apollo-admin-config.php',
            APOLLO_ADMIN_DIR .'/class-apollo-install-theme.php',
            APOLLO_ADMIN_DIR. '/apollo-meta-box-function.php',
            APOLLO_ADMIN_DIR. '/class-apollo-admin-post-types.php',
            APOLLO_ADMIN_DIR .'/tools/custom-fields/class-apollo-admin-tool-custom-field.php',
            APOLLO_ADMIN_DIR. '/inc/class-apollo-admin-event-page-list-table.php',
            APOLLO_ADMIN_DIR. '/inc/class-apollo-admin-import-event-file.php',
            !Apollo_App::is_ajax() ? APOLLO_ADMIN_DIR. '/class-apollo-admin-assets.php' : '',
            APOLLO_ADMIN_DIR. '/class-apollo-admin-taxonomies.php',
            is_network_admin() ? APOLLO_ADMIN_DIR. '/network/class-apollo-admin-network.php' : '',

            (is_network_admin() && defined('APL_USER_API_BASE_URL')) && (!defined('APL_USER_API_HIDDEN') || isset($_GET['is_super_admin'])) ? APOLLO_ADMIN_DIR. '/network/sync-users/class-apollo-admin-network-sync-users.php' : '',

            is_network_admin() || Apollo_App::get_network_manage_states_cities() ? 
                APOLLO_ADMIN_DIR. '/network/class-apollo-admin-network-state.php' : '',
            
            APOLLO_ADMIN_DIR. '/class-apollo-admin-image.php',
            /** @Ticket #13856 */
            APOLLO_ADMIN_DIR. '/theme-options/includes/territory/neighborhood/class-apollo-options-territory-neighborhood-option.php',
            APOLLO_ADMIN_DIR. '/apollo-admin-ajax.php',
            APOLLO_ADMIN_SRC. '/user/inc/apollo-admin-account.php',
            APOLLO_ADMIN_DIR. '/custom-pages/class-apollo-admin-agency.php',
            APOLLO_ADMIN_DIR. '/post-types/meta-boxes/event/event-calendar/class-apollo-admin-calendar.php',
            
            // including script serve for users actions in the admin
            APOLLO_ADMIN_DIR. '/class-apollo-admin-users-action.php',

            APOLLO_ADMIN_DIR. '/tools/cache/class-apollo-admin-cache.php',
            APOLLO_ADMIN_DIR. '/tools/clean-duplicated-images/APL_Admin_Clean_Duplicated_images.php'
        );

        foreach( $arr as $path ) {
            if ($path) require_once $path;
        }

        add_thickbox();
    }
    
    
    /**
	 * Include required ajax files.
	 */
	public function ajax_includes() {

	    $module = isset($_REQUEST['module']) ? $_REQUEST['module'] : '';
        switch ($module) {
            case 'event':
                require_once APOLLO_INCLUDES_DIR. '/src/event/inc/ApolloEventAjax.php';
                break;
            case 'artist':
                require_once APOLLO_INCLUDES_DIR. '/src/artist/inc/ApolloArtistAjax.php';
                break;
            case 'organization':
                require_once APOLLO_INCLUDES_DIR. '/src/org/inc/ApolloOrganizationAjax.php';
                break;
        }

		require_once APOLLO_INCLUDES_DIR. '/class-apollo-ajax.php';
	}
    
    // unregister the default activity widget
    function remove_dashboard_widgets() {

        global $wp_meta_boxes;
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);

    }
    
    /**
     * Override activity dashboard
     */
    function add_custom_dashboard_activity() {
        
        wp_add_dashboard_widget('custom_dashboard_activity', 'Activity', array( $this, 'custom_wp_dashboard_site_activity' ) );
    }
    
    function custom_wp_dashboard_site_activity() {

        echo '<div id="activity-widget">';

        $future_posts = wp_dashboard_recent_posts( array(
            'display' => 5,
            'max'     => 10,
            'status'  => 'future',
            'order'   => 'ASC',
            'title'   => __( 'Publishing Soon' ),
            'id'      => 'future-posts',
        ) );

        $recent_posts = wp_dashboard_recent_posts( array(
            'display' => 5,
            'max'     => 10,
            'status'  => 'publish',
            'order'   => 'DESC',
            'title'   => __( 'Recently Published' ),
            'id'      => 'published-posts',
        ) );
        
        // Override the comment in admin dashboard
        
        if ( $avaible_modules = Apollo_App::get_avaiable_modules() ) {
            
            $_str_avaiable_modules = "'". implode( "','", $avaible_modules ). "'";
            global $wpdb;
            $sql = " SELECT * FROM $wpdb->comments LEFT OUTER JOIN $wpdb->posts "
                . " ON ($wpdb->comments.comment_post_ID = $wpdb->posts.ID) "
                . " WHERE  comment_type = '' AND post_password = '' AND post_type IN ({$_str_avaiable_modules}) "
                . " ORDER BY comment_date_gmt DESC LIMIT 10";
            
            $recent_comments = $wpdb->get_results( $sql );

            echo '<div id="latest-comments" class="activity-block">';
            echo '<h4>' . __( 'Comments' ) . '</h4>';
            echo '<div id="the-comment-list" data-wp-lists="list:comment">';

            foreach ( $recent_comments as $comment ) {
                _wp_dashboard_recent_comments_row( $comment );
            }
            echo '</div>';

            if ( current_user_can('edit_posts') ) {
                _get_list_table('WP_Comments_List_Table')->views();
            }

            wp_comment_reply( -1, false, 'dashboard', false );
            wp_comment_trashnotice();

            echo '</div>';


            //$recent_comments = wp_dashboard_recent_comments( 10 );

            if ( !$future_posts && !$recent_posts && !$recent_comments ) {
                echo '<div class="no-activity">';
                echo '<p class="smiley"></p>';
                echo '<p>' . __( 'No activity yet!', 'apollo' ) . '</p>';
                echo '</div>';
            }
        }
        echo '</div>';
    }
    
    
    /**
     * Deactivate plugins that is set on the network admin for each site
     * @access public
     */ 
    function handle_admin_initialization() {
        
        if (! is_network_admin() ) {
            
            // Get already active plugins
            $activePlugins = maybe_unserialize(get_option('active_plugins'));
            
            if ( $activePlugins ) {
                
                // Get disabled plugin that set on the admin network for each site
                $disabledPlugins = Apollo_App::get_disabled_plugins();
                if ( $disabledPlugins ) {
                    foreach ( $activePlugins as $i => $activePlugin ) {
                        $dirData = explode('/', $activePlugin);

                        // If the current action plugin in disabledPlugins
                        if (!in_array($dirData[0], $disabledPlugins) ) {
                            unset($activePlugins[$i]);
                        }
                    }

                    // Deactive plugins
                    if ( $activePlugins ) {
                        deactivate_plugins(array_values($activePlugins) );
                    }
                }
            }
            // Thienld : remove featured image meta box inside edit business post type screen.
            remove_post_type_support( Apollo_DB_Schema::_BUSINESS_PT, 'thumbnail' );
        }


    } // End deactive_plugins function
    
    /**
     * Hide the disabled plugins
     * @access public
     * 
     */
    function hidePlugins() {
        global $wp_list_table;

        $myplugins = $wp_list_table->items;

        $disabledPlugins = Apollo_App::get_disabled_plugins();
        if ( $disabledPlugins ) {
            foreach ($myplugins as $key => $val) {
                $dataKeys = explode('/', $key);

                if (in_array($dataKeys[0], $disabledPlugins) ) {
                    unset($wp_list_table->items[$key]);
                }
            }
        }
    }
    
}

/**
 * Disable the emoji's
 */
function apl_disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );	
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );	
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'apl_disable_emojis_tinymce' );
}
add_action( 'init', 'apl_disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 * 
 * @param    array  $plugins  
 * @return   array             Difference betwen the two arrays
 */
function apl_disable_emojis_tinymce( $plugins ) {
    
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} 
    return array();
	
}

add_action( 'admin_init', 'posts_order_wpse_91866' );

function posts_order_wpse_91866()
{
    add_post_type_support( 'post', 'page-attributes' );
}

/**
 * #14833 [CF] 20180122 - Performance issue - Apply cache for Primary navigation
 * @author hieulc
 */
add_action('wp_update_nav_menu', 'clear_cache_primary_navigation');
function clear_cache_primary_navigation() {
    if(intval(Apollo_App::get_network_local_cache(get_current_blog_id())) == 1) {
        $cacheFileClass = aplc_instance('APLC_Inc_Files_PrimaryNavigation');
        $cacheFileClass->remove();
    }
}

/**
 * @author Trilm
 * Class pasteAsPlainText
 *
 */

class pasteAsPlainText {

    function __construct() {
        add_action( 'admin_init', array( $this, 'init' ) );

    }

    function init() {

        add_filter( 'tiny_mce_before_init', array( $this, 'forcePasteAsPlainText' ) );
        add_filter( 'teeny_mce_before_init', array( $this, 'forcePasteAsPlainText' ) );
        add_filter( 'teeny_mce_plugins', array( $this, 'loadPasteInTeeny' ) );
        add_filter( 'mce_buttons_2', array( $this, 'removePasteAsPlainTextButton' ) );
        /* Add the button/option in second row */
        add_filter( 'mce_buttons_2', array( $this, 'addBackgroundColorButton' ) );

    }

    function forcePasteAsPlainText( $mceInit ) {

        global $tinymce_version;

        if ( $tinymce_version[0] < 4 ) {
            $mceInit[ 'paste_text_sticky' ] = true;
            $mceInit[ 'paste_text_sticky_default' ] = true;
        } else {
            $mceInit[ 'paste_as_text' ] = true;
        }

        return $mceInit;
    }

    function loadPasteInTeeny( $plugins ) {

        return array_merge( $plugins, (array) 'paste' );

    }

    function removePasteAsPlainTextButton( $buttons ) {

        if( ( $key = array_search( 'pastetext', $buttons ) ) !== false ) {
            unset( $buttons[ $key ] );
        }

        return $buttons;

    }

    /**
     * Modify 2nd Row in TinyMCE and Add Background Color After Text Color Option
     * @link https://shellcreeper.com/?p=1339
     */
    function addBackgroundColorButton( $buttons ){

        /* Add the button/option after 4th item */
        array_splice( $buttons, 4, 0, 'backcolor' );

        return $buttons;
    }

}

new pasteAsPlainText();

return new Apollo_Admin();



