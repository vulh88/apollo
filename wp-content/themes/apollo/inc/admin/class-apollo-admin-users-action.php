<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Apollo Admin.
 *
 * @class 		Apollo_Admin 
 * @author 		vulh
 * @package 	inc/admin
 */
class Apollo_Admin_Users_Action {

	/**
	 * Constructor
	 */
	public function __construct() {
        if(!is_network_admin()){
            add_action('pre_user_query',array($this,'pre_user_search_function'));
            add_action( 'admin_footer-users.php', array($this, 'bulk_footer_toggle_users_import_tool') );
            add_action( 'admin_action_enable_import_event_tool', array($this,'apollo_bulk_users_enable_import_event_tool') );
            add_action( 'admin_action_disable_import_event_tool', array($this,'apollo_bulk_users_disable_import_event_tool') );
            add_filter( 'manage_users_columns', array($this, 'apollo_modify_user_table') );
            add_filter( 'manage_users_custom_column', array($this, 'apollo_modify_user_table_row'), 10, 3 );
            add_filter( 'manage_users_sortable_columns', array($this,'apollo_modify_users_sortable_columns'), 10, 3);
            add_action('pre_user_query', array($this, 'apollo_modify_users_query'));
            add_filter( 'admin_init', array($this, 'set_user_metaboxes'), 10, 3 );

        }

    }

    /**
     * @Ticket #15071 - custom sortable columns
     * @param $columns
     * @return array
     */
    public function apollo_modify_users_sortable_columns( $columns ) {
        $custom_columns = array(
            'name' => 'first_name',
            'role' => 'role',
            'created_date' => 'user_registered'
        );
        return wp_parse_args( $custom_columns, $columns );
    }

    /**
     * #Ticket #15071 - Custom sort with column name
     * @param $userQuery
     */
    public function apollo_modify_users_query( $userQuery ) {
        global $wpdb;
        if ($userQuery->query_vars['orderby'] == 'role') {
            $userQuery->query_from .= " INNER JOIN {$wpdb->usermeta} m1 ON {$wpdb->users}.ID=m1.user_id AND (m1.meta_key='{$wpdb->prefix}capabilities')";
            $userQuery->query_orderby = " ORDER BY REPLACE( m1.meta_value, SUBSTRING_INDEX( m1.meta_value,'\"' , 1 ), '' ) {$userQuery->query_vars['order']}";
        }
        if ($userQuery->query_vars['orderby'] == 'first_name') {
            $userQuery->query_from .= " INNER JOIN {$wpdb->usermeta} m1 ON {$wpdb->users}.ID=m1.user_id AND (m1.meta_key='first_name')";
            $userQuery->query_orderby = "ORDER BY m1.meta_value {$userQuery->query_vars['order']}";
        }
    }

    public function apollo_modify_user_table( $column ) {
        if (Apollo_App::get_network_event_import_tool()) {
            $column['toggle_eit'] = __('Event Import Tool','apollo');
        }
        $column['created_date'] = __('User Registered','apollo');
        
        return $column;
    }


    public function apollo_modify_user_table_row( $val, $column_name, $user_id ) {
        $return = "";
        switch ($column_name) {
            case 'toggle_eit' :
                $return = self::apollo_check_can_use_import_event_tool($user_id) ? __("Enabled","apollo") : __("Disabled","apollo");
                break;
            case 'created_date' :
                $return = self::getUserRegisteredDate($user_id);
                break;
            default:
                break;
        }
        return $return;
    }

    public function bulk_footer_toggle_users_import_tool(){
        if (! Apollo_App::get_network_event_import_tool()) return;
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $('<option>').val('enable_import_event_tool').text('<?php _e('Enable Import Event Tool','apollo'); ?>')
                    .appendTo("select[name='action']");
                $('<option>').val('disable_import_event_tool').text('<?php _e('Disable Import Event Tool','apollo'); ?>')
                    .appendTo("select[name='action']");
            });
        </script>
    <?php
    }

    public function apollo_bulk_users_enable_import_event_tool(){
        if(empty($_REQUEST['users'])){
            return;
        }
        self::apollo_enable_import_event_tool_by_array_user_ids(!is_array($_REQUEST['users']) ? array($_REQUEST['users']) : $_REQUEST['users']);
    }

    public function apollo_bulk_users_disable_import_event_tool(){
        if(empty($_REQUEST['users'])){
            return;
        }
        self::apollo_disable_import_event_tool_by_array_user_ids(!is_array($_REQUEST['users']) ? array($_REQUEST['users']) : $_REQUEST['users']);
    }

    public static function apollo_enable_import_event_tool_by_array_user_ids($listUserIDsEnabled=array()){
        if(empty($listUserIDsEnabled)){
            return;
        }
        $listOfUserCan = get_site_option(Apollo_Tables::_APL_TOGGLE_EVENT_IMPORT_TOOL, array());
        $listOfUserCan = empty($listOfUserCan) ? $listUserIDsEnabled : array_merge($listOfUserCan, $listUserIDsEnabled);
        self::apollo_update_users_can_use_import_event_tool($listOfUserCan);
        
        global $wp_rewrite;
        $wp_rewrite->init();
        $wp_rewrite->flush_rules();
    }

    /*
     * @todo this function currently store array user in site_option, it should bt store in user_meta
     * */
    public static function apollo_disable_import_event_tool_by_array_user_ids($listUserIDsDisabled=array()){
        if(empty($listUserIDsDisabled)){
            return;
        }
        $listOfUserCan = get_site_option(Apollo_Tables::_APL_TOGGLE_EVENT_IMPORT_TOOL, array());
        $listOfUserCan = array_diff($listOfUserCan, $listUserIDsDisabled);
        self::apollo_update_users_can_use_import_event_tool($listOfUserCan);
    }

    public static function  apollo_update_users_can_use_import_event_tool($userIDs = array()){
        try{
            $resultUpdate = update_site_option(Apollo_Tables::_APL_TOGGLE_EVENT_IMPORT_TOOL, $userIDs);
            return $resultUpdate;
        }catch (Exception $ex){
            return false;
        }
    }

    public static function apollo_enable_bypass_pending_approval_by_array_user_ids($listUserIDsEnabled=array()){
        if(empty($listUserIDsEnabled)){
            return;
        }
        $listOfUserCan = get_site_option(Apollo_Tables::_APL_BYPASS_PENDING_APPROVAL, array());
        $listOfUserCan = empty($listOfUserCan) ? $listUserIDsEnabled : array_merge($listOfUserCan, $listUserIDsEnabled);
        self::apollo_update_users_can_bypass_pending_approval($listOfUserCan);

        global $wp_rewrite;
        $wp_rewrite->init();
        $wp_rewrite->flush_rules();
    }

    public static function apollo_disable_bypass_pending_approval_by_array_user_ids($listUserIDsDisabled=array()){
        if(empty($listUserIDsDisabled)){
            return;
        }
        $listOfUserCan = get_site_option(Apollo_Tables::_APL_BYPASS_PENDING_APPROVAL, array());
        $listOfUserCan = array_diff($listOfUserCan, $listUserIDsDisabled);
        self::apollo_update_users_can_bypass_pending_approval($listOfUserCan);
    }

    public static function  apollo_update_users_can_bypass_pending_approval($userIDs = array()){
        try{
            $resultUpdate = update_site_option(Apollo_Tables::_APL_BYPASS_PENDING_APPROVAL, $userIDs);
            return $resultUpdate;
        }catch (Exception $ex){
            return false;
        }
    }

    public static function apollo_check_can_use_import_event_tool($user_id){
        $listOfUserCan = get_site_option(Apollo_Tables::_APL_TOGGLE_EVENT_IMPORT_TOOL, array());
        return in_array($user_id, $listOfUserCan);
    }

    static function set_user_metaboxes($user_id=NULL) {

        if (is_super_admin()) {
            return true;
        }
       
        // These are the metakeys we will need to update
        $meta_key['order'] = 'meta-box-order_post';
        $meta_key['hidden'] = 'metaboxhidden_post';

        // So this can be used without hooking into user_register
        if ( ! $user_id) {
            $user_id = get_current_user_id();
        }

        // Set the default order if it has not been set yet
        if ( ! get_user_meta( $user_id, $meta_key['order'], true) ) {

            $meta_value = array(
                'side' => 'submitdiv,formatdiv,categorydiv,postimagediv,tagsdiv-post_tag',
                'normal' => 'postexcerpt,commentstatusdiv,commentsdiv,trackbacksdiv,authordiv,revisionsdiv',
                'advanced' => '',
            );
            update_user_meta( $user_id, $meta_key['order'], $meta_value );
        }

        //Set the default hiddens if it has not been set yet

        if ( ! get_user_meta( $user_id, $meta_key['hidden'], true) ) {
            $meta_value = array('trackbacksdiv', 'postcustom', 'slugdiv');
            update_user_meta( $user_id, $meta_key['hidden'], $meta_value );
        }
    }

    public static function getUserRegisteredDate($userId){
        $return = '';
        if($userId){
            $user = get_userdata( $userId );
            if($user){
                $date = $user->user_registered;
                $return = date("Y-m-d", strtotime($date));
            }
        }
        return  $return;
    }

    /**
     * @author Trilm
     * @param $userSearch
     * Custom admin user search
     */
    public function pre_user_search_function($userSearch){
        global $wpdb;
        //custom strQuery where
        $keyWord = isset($userSearch->query_vars['search'])?str_replace('*','%',$userSearch->query_vars['search']):'';

        if($keyWord!=''){
            $keyWordMeta = explode(' ',$keyWord);
            $metaQuery = array();
            if(is_array($keyWordMeta) && count($keyWord)){
                foreach ($keyWordMeta as $k => $metaValue){
                    $metaQuery[] = " ( mt%_meta_pos_%.meta_value LIKE \"$metaValue\" AND mt%_meta_pos_%.meta_key=\"%_meta_key_%\"  )";
                }
            }
            $metaQuery[] = " ( mt%_meta_pos_%.meta_value LIKE \"$keyWord\" AND mt%_meta_pos_%.meta_key=\"%_meta_key_%\"  )";

            $metaQuery = implode(' OR ',$metaQuery);

            $metaValue = str_replace('%_meta_pos_%','',$metaQuery);
            $metaValue = str_replace('%_meta_key_%','first_name',$metaValue);

            $meta1Value = str_replace('%_meta_pos_%','1',$metaQuery);
            $meta1Value = str_replace('%_meta_key_%','last_name',$meta1Value);

            $queryUserByKeyWord = array(
                " user_login LIKE '$keyWord' ",
                " user_url LIKE '$keyWord'",
                " user_email LIKE '$keyWord' ",
                " user_nicename LIKE '$keyWord' ",
                " display_name LIKE '$keyWord' ",
                "  {$wpdb->users}.ID IN
                     ( 
                        SELECT mt.user_id
                        FROM {$wpdb->usermeta} mt
                        WHERE $metaValue
                        GROUP BY mt.user_id
                     ) 
                  ",
                 "  {$wpdb->users}.ID IN
                     ( 
                        SELECT mt1.user_id
                        FROM {$wpdb->usermeta} mt1
                        WHERE $meta1Value
                        GROUP BY mt1.user_id
                     ) 
                  ",

            );
            $wpCapabilitiesMeta = 'wp_capabilities';
            if(get_current_blog_id() != 1){
                $blogId = get_current_blog_id();
                $wpCapabilitiesMeta = "wp_{$blogId}_capabilities";
            }
            $queryUserByKeyWord = implode('OR',$queryUserByKeyWord);
            $where = "  WHERE 1=1 AND ( wp_usermeta.meta_key = '$wpCapabilitiesMeta' ) AND ( $queryUserByKeyWord ) ";
            $userSearch->query_where = $where;
            $userSearch->query_groupby = " {$wpdb->users}.ID ";
        }
    }
}

return new Apollo_Admin_Users_Action();
