<?php
    ob_start();
?>
<h1 class="title-page"><?php _e( 'New Account', 'apollo' ) ?></h1>
<div class="p-sub-ct">
    <p>New to <a href="<?php echo home_url(); ?>"><?php echo Apollo_App::apollo_get_current_domain(); ?></a> ? Don’t have a login? Sign up now… It’s FREE and anyone can join!</p>
    <div class="benef">
        <p> <b>Bookmark </b> your favorite events, organizations and venues</p>
        <p> <b>Post </b> reviews on events</p>
        <p> <b>Create </b> and update your profile</p></div>
    [apollo_registration]
    <p class="warn"><span>IMPORTANT! </span> Anyone UNDER 18 must have their parents approval to participate in this service.
        <a href="<?php echo home_url(); ?>"><?php echo Apollo_App::apollo_get_current_domain(); ?></a> reserves the right to edit submissions for grammar, style and accuracy. We may also choose to omit any profile at our discretion. </p>
</div>
<?php return ob_get_clean(); ?>