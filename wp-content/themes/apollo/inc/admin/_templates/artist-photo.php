<?php
ob_start();
$max_upload_size_in_b = wp_max_upload_size();
$max_upload_size_in_mb = number_format($max_upload_size_in_b / (1024 * 1024), 2);
?>
    <div class="dsb-welc">
        <h1>Images</h1>
        <p><?php echo sprintf(__("All images must be %s format only. Each image should not exceed a file size of %s megabyte (mb). Image file names must NOT contain spaces between words, question marks, exclamation points, apostrophes, quotation marks, or most other symbols (hyphens and underscore marks are OK). Our system automatically rejects images with bad file names.", "apollo"), '.jpg, .png, .jpeg', $max_upload_size_in_mb); ?></p>
        <form id="artist-frm" method="" action="">
            <div class="vertical-light"></div>
            <div class="artist-blk"><span class="it">To submit your primary image:</span>
                <ul class="image-step">
                    <li>
                        <div class="step-num">1</div>
                        <div class="step-des">Use the <strong>"Upload Image" </strong>button below to locate the image on your computer.</div>
                    </li>
                    <li>
                        <div class="step-num">2</div>
                        <div class="step-des">Select your image and then click <strong>"Open"</strong></div>
                    </li>
                    <li>
                        <div class="step-num">3</div>
                        <div class="step-des">Once you have selected your image click <strong>"Submit Record" </strong>below.</div>
                    </li>
                </ul>
            </div>
            <div class="artist-blk" id="artist_center_info">
                <nav class="nav-tab">
                    <ul class="tab-image-list">
                        <li class="selected"><a href="javascript:void(0);" data-id="1">PRIMARY IMAGE</a></li>
                        <li><a href="javascript:void(0);" data-id="2">GALLERY</a></li>
                    </ul>
                </nav>

                [apollo_upload_and_drop target='artist']
                [apollo_upload_gallery target='artist']


            </div>
            <div class="artist-blk" id="apollo_submit_artist_photo" data-center_info="#artist_center_info">
                <div class="submit-blk"><a href="javascript:void(0);" class="submit-btn">SUBMIT PHOTO</a></div>
            </div>
        </form>
    </div>
<?php return ob_get_clean(); ?>