<?php
ob_start();
?>
    <div class="dsb-welc">
        <h1>Welcome to your dashboard {username}!</h1>
        <p>You can manage your individual artist profile, organization or business profile, post and edit events, bookmark your favorite listings, and more. To return to your dashboard at any time, click "My Account" in the upper right corner.  </p>
        <p>If this is your first time submitting an event, you will need to create an organization or business profile, or if you are an artist, you will need to create an artist profile. You can do all of that using the links in the tool bar above.</p>
        <p>Please be sure to read the Submission Guidelines in each section first. Happy sharing! </p>
    </div><a {create_event_btn} href="<?php echo home_url() ?>/user/add-event/step-1/" class="dsb-create btn btn-l"> <i class="fa fa-pencil"></i>CREATE NEW EVENT</a>
    <p class="dsb-support">If you need assistance, please contact: <a href="mailto:contact email here" >contact email here</a></p>
<?php return ob_get_clean(); ?>