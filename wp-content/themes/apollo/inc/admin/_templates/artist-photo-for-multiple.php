<?php
ob_start();
?>
    <div class="dsb-welc custom">
        <h1>Images</h1>
        <p>All images must be .jpg, .png, .jpeg format only. Each image should not exceed a file size of 1.5 megabyte (mb). Image file names must NOT contain spaces between words, question marks, exclamation points, apostrophes, quotation marks, or most other symbols (hyphens and underscore marks are OK). Our system automatically rejects images with bad file names.</p>
        <div class="vertical-light"></div>
        <div class="artist-blk"><span class="it">To submit your primary image:</span>
            <ul class="image-step">
                <li>
                    <div class="step-num">1</div>
                    <div class="step-des">Use the <strong>"Upload Image" </strong>button below to locate the image on your computer.</div>
                </li>
                <li>
                    <div class="step-num">2</div>
                    <div class="step-des">Select your image and then click <strong>"Open"</strong></div>
                </li>
                <li>
                    <div class="step-num">3</div>
                    <div class="step-des">Once you have selected your image click <strong>"Submit Record" </strong>below.</div>
                </li>
            </ul>
        </div>
    </div>
<?php return ob_get_clean(); ?>