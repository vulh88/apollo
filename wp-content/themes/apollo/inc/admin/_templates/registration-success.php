<?php
ob_start();
?>
<div id="apollo-registered-page"><?php _e( 'Thanks for your registration, please check your email and', 'apollo' ) ?><a href="<?php echo get_bloginfo('url') ?>/login"> <?php _e( 'click here', 'apollo' ) ?></a> <?php _e( 'to login', 'apollo' ) ?>.</div>
<?php return ob_get_clean(); ?>