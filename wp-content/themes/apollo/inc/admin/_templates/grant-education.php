<?php 
    ob_start();
?>
<div class="blog-bkl">
    <h2>Grant Application</h2>
    <div class="el-blk edu-detail">
      <div class="edu-detail-desc"> 
        <p>Teachers - thank you for your interest in exposing our youth to creative experiences. If you have identified a program or organization you want your students to experience then you have come to the right place. Please fill out the application below to request funds to help pay for the creative venture, the transportation, materials, or all of the above. Our team will review the application and get back in touch with you in the next two weeks. You can apply for up to $1,000 per teacher, per classroom.</p>
      </div>
    </div>
</div>

[apollo-grant-education-form]
<?php 

return ob_get_clean(); ?>