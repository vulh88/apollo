<?php
ob_start();
?>
<div class="submit-event-success">
    <div class="evt-blk">
        <div class="event-tt"><?php _e('SUCCESS!', 'apollo') ?></div>
    </div>
    <div class="evt-blk">
        <p>
            <?php _e('Thank you for posting your classified. We will review your submission and let you know if we have any questions.', 'apollo') ?>
        </p>
    </div>
    <div class="evt-blk">

        <a href="<?php echo home_url() ?>/user/published-classifieds/">
            <div id="arrow-3" style=""><span><?php _e('RETURN TO THE LIST', 'apollo') ?></span></div>
        </a>
    </div>
</div>
<?php return ob_get_clean(); ?>