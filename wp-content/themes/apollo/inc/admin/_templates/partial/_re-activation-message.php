<div class="update-nag" style="padding: 10px; font-size: 14px;border-left: 4px solid red;">
    <img src="/wp-content/themes/apollo/inc/admin/assets/images/aflag-icon.png" style="width: 17px;vertical-align: middle;padding-right: 5px;">
    <?php _e("IMPORTANT: Artsopolis theme update required!", 'apollo') ?>
    <a href="/wp-admin/themes.php?page=re-active-theme"><?php _e('Please update now.', 'apollo') ?></a>
</div>
