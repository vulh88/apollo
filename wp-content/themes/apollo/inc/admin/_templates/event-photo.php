<?php
ob_start();
?>
    <p>
        <?php echo sprintf(__( 'You can upload a primary event image and up to %d additional gallery images below - the primary image will be the default image displayed with your event.
        The gallery images will appear on the event detail page under the &#8220;Media Gallery&#8221; section.', 'apollo' ), of_get_option(Apollo_DB_Schema::_MAX_UPLOAD_GALLERY_IMG,Apollo_Display_Config::MAX_UPLOAD_GALLERY_IMG) ); ?>
    </p>
    <p><?php echo sprintf(__( 'All images must be %s format only and each image should be smaller than %s MB in size.', 'apollo' ), '.jpg, .png, .jpeg', $max_upload_size_in_mb); ?></p>
<?php return ob_get_clean(); ?>