<?php
ob_start();
?>
    <h1 class="title-page"><?php _e('Forgot Password', 'apollo') ?></h1>
    <div class="p-sub-ct">
        <p><?php _e('Please enter your email address. You will receive a link to create a  new password via email.', 'apollo') ?></p>
        [apollo_reset_password]
    </div>
<?php return ob_get_clean(); ?>