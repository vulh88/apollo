<?php
ob_start();
?>
<p>Thank for your information. <a href="<?php echo get_permalink(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_TEACHER_EVALUATION_FORM)) ?>">Click here</a> to submit another Teacher Evaluation</p>
<?php return ob_get_clean(); ?>