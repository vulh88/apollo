<?php
ob_start();
?>
    <h1 class="title-page"><?php _e('Enter New Password', 'apollo') ?></h1>
    <div class="p-sub-ct">
        <p><?php _e('Enter your new password below.', 'apollo') ?></p>
        [apollo_change_my_password]
    </div>
<?php return ob_get_clean(); ?>