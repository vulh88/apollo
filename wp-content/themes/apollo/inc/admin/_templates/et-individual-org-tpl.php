<?php
/**
 * @ticket #11493: [CF] 20170310 - [Event Theme page] Add a new Venues and ORGs listing
 */

$individualOrgs = isset($template_args['individualOrgs']) && !empty($template_args['individualOrgs']) ? $template_args['individualOrgs'] : array();
$selectedOrgs   = isset($template_args['selectedOrgs'])   && !empty($template_args['selectedOrgs'])   ? $template_args['selectedOrgs']   : array();

if( !empty($individualOrgs) ) :
    foreach ( $individualOrgs as $e ):
        $org = get_org($e);
        ?>
        <label>
            <input type="checkbox" name="associated_orgs[]"
                   value="<?php echo $org->post->ID ?>"
                   <?php echo $selectedOrgs && in_array($org->post->ID, $selectedOrgs) ? 'checked' : '' ?> />
            <?php echo $org->get_title(true) ?>
            <a style="float: right" href="<?php echo get_edit_post_link($org->post->ID) ?>"><?php _e('Edit', 'apollo') ?></a>
        </label>
        <br>
    <?php endforeach; ?>
<?php endif; ?>
