<?php 
    ob_start();
?>
<div class="blog-bkl">
    <h2>Teacher Evaluation</h2>
    <div class="el-blk edu-detail">
      <div class="edu-detail-desc"> 
            <p>Dear Teacher, we’re glad your students attended this program. Please return this evaluation immediately (cultural organizations are not paid without evaluations). We value your opinion! – United Arts</p>
      </div>
    </div>
</div>

[apollo-teacher-evaluation-form]

<?php 
//success-page='submit-teacher-evaluation-success'
return ob_get_clean(); ?>