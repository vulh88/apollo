<?php
ob_start();
?>
    <div class="dsb-welc custom">
        <h1><?php _e('Images','apollo'); ?></h1>
        <p><?php _e('All images must be .jpg, .png, .jpeg format only. Each image should not exceed a file size of 1.46 megabyte (mb). Image file names must NOT contain spaces between words, question marks, exclamation points, apostrophes, quotation marks, or most other symbols (hyphens and underscore marks are OK). Our system automatically rejects images with bad file names.','apollo'); ?></p>
    </div>
<?php return ob_get_clean(); ?>