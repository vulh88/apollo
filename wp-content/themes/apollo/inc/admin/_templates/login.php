<?php
    ob_start();
    $signup_url = get_the_permalink(Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_REGISTER_PAGE));
?>
<h1 class="title-page"><?php _e( 'Account Login', 'apollo' ) ?></h1>
<div class="p-sub-ct">
    <p><?php _e( 'Already have an account? Enter your username and password below to login', 'apollo' ) ?>: </p>
    <div class="login-wrp">
        <div class="log-frm">
            [apollo_login]
        </div>

            [apollo_login_fb]
    </div>
    <p>New to <a href="<?php echo home_url(); ?>"><?php echo Apollo_App::apollo_get_current_domain(); ?></a>? Don’t have a login? <a href="<?php echo $signup_url ?>">Sign up </a> now… It’s FREE and anyone can join! </p>
</div>
<?php
    return ob_get_clean();
?>