<?php
/**
 * @ticket #11493: [CF] 20170310 - [Event Theme page] Add a new Venues and ORGs listing
 */

$individualVenues = isset($template_args['individualVenues']) && !empty($template_args['individualVenues']) ? $template_args['individualVenues'] : array();
$selectedVenues   = isset($template_args['selectedVenues'])   && !empty($template_args['selectedVenues'])   ? $template_args['selectedVenues']   : array();

if( !empty($individualVenues) ) :
    foreach ( $individualVenues as $e ):
        $venue = get_venue($e);
        ?>
        <label>
            <input type="checkbox" name="associated_venues[]"
                   value="<?php echo $venue->post->ID ?>"
                   <?php echo $selectedVenues && in_array($venue->post->ID, $selectedVenues) ? 'checked' : '' ?> />
            <?php echo $venue->get_title(true) ?>
            <a style="float: right" href="<?php echo get_edit_post_link($venue->post->ID) ?>"><?php _e('Edit', 'apollo') ?></a>
        </label>
        <br>
    <?php endforeach; ?>
<?php endif; ?>
