<?php

$individualEvents = isset($template_args['individualEvents']) && !empty($template_args['individualEvents']) ? $template_args['individualEvents'] : array();
$selectedEvents = isset($template_args['selectedEvents']) && !empty($template_args['selectedEvents']) ? $template_args['selectedEvents'] : array();
if(!empty($individualEvents)) :
    foreach ( $individualEvents as $e ):
        $event = get_event($e);
        $org = $event->event_org_links();
        ?>
        <label class="event-item">
            <input <?php echo $selectedEvents && in_array($event->post->ID, $selectedEvents) ? 'checked' : '' ?> value="<?php echo $event->post->ID ?>" type="checkbox" name="events[]">
            <?php echo $event->get_title(true) ?> <i style="font-size: 12px">( <?php _e('Presented By: ') ?><?php echo $org ?> - <?php echo $event->render_sch_date() ?> )</i>
            <a style="float: right" href="<?php echo get_edit_post_link($event->post->ID) ?>"><?php _e('Edit', 'apollo') ?></a>
        </label><br>
<?php endforeach; endif;
