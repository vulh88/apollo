<?php
ob_start();
?>

        <h1>Tell A Friend</h1>
        <p>Keep your friends and family in the loop by emailing this page!
            <Tell>them about all the exciting events, organizations, venues, and more that you\'re finding on {site_name}</br></br></Tell>
        </p>
            <p>If you have any questions, please contact: <a href="mailto:{support_email}">{support_email}</a></p>
        <div class="btn">
            <a class="btn-b" href="{link_to_friend}">SEND MESSAGE</a>
        </div>
        <p class="note">This allow you to send via your own address book using MS Outlook or other local email client.</p>

<?php return ob_get_clean(); ?>