<?php

    /* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}    

if (!class_exists('APLC_Inc_File')):
    
class APLC_Inc_File
{
    protected $filename = '';
    protected $extension = '';
    protected $baseDir = '';
    protected $content = '';
    const APLCF_EXPIRED_CACHING_TIME = '+1 day';
    public $localSiteCacheEnabled = false;

    public function __construct() {
        $uploadDir      = wp_upload_dir();
        $this->baseDir  = $uploadDir['cache'];

        // determine cache is enabled for current site (ticket #10738)
        $currentBlogID     = get_current_blog_id();
        $optionCachedValue = Apollo_App::get_network_local_cache($currentBlogID);
        if ( intval($optionCachedValue) === 1 ) {
            $this->localSiteCacheEnabled = true;
        }
    }

    /**
     * Save cache file
     * @param string $expireTime
     */
    public function add($expireTime = '') {
        if ($this->extension == 'json') {
            $this->content = json_encode($this->content, true);
        }

        update_option($this->getOptionKey(), date('Y-m-d H:i:s', $expireTime), 'no');
        file_put_contents($this->getFilePath(), $this->content);
    }

    /**
     * Save cache file by expire time and cached content
     * @param string $filename
     * @param string $content
     * @param string $expireTime
     */
    public function addCache($filename = '', $content = '', $expireTime = '') {
        try{
            if(!empty($content) && !empty($filename)){
                $expireTimeCached = !empty($expireTime) ? strtotime($expireTime) : strtotime(self::APLCF_EXPIRED_CACHING_TIME);
                $expireTimeCached = date('Y-m-d H:i:s', $expireTimeCached);
                update_option($this->getOptionKeyByFileName($filename), $expireTimeCached, 'no');
                @file_put_contents($this->getFilePathByFilename($filename), $content);
            }
        }catch (Exception $ex){
            aplDebugFile($ex->getMessage(),'APLC_Inc_File Caching problems');
        }
    }

    public function getOptionKey() {
        return 'apl_expire_time_'. $this->filename;
    }

    public function getOptionKeyByFileName($filename) {
        return 'apl_expire_time_'. basename($filename);
    }
    
    /**
     * Get cache content
     * 
     * @return string
     */
    public function get() {
        $data = false;
        if ( $this->localSiteCacheEnabled ) {
            $file = $this->getFilePath();
            $data = file_exists($file) ? @file_get_contents($file) : '';

            if (! $data || strtotime(get_option($this->getOptionKey())) < current_time('timestamp') ) return false;
        }


        if ($this->extension == 'json') {
            return json_decode($data, true);
        }

        return $data;
    }

    /**
     * Get cache content
     *
     * @param bool $stopCache
     * @return string
     */
    public function forceGet($stopCache = false) {

        if ($stopCache) return '';

        $file = $this->getFilePath();
        $data = file_exists($file) ? @file_get_contents($file) : '';

        if (! $data || strtotime(get_option($this->getOptionKey())) < current_time('timestamp') ) return false;

        if ($this->extension == 'json') {
            return json_decode($data, true);
        }

        return $data;
    }

    /**
     * Get cache content by filename
     *
     * @return string
     */
    public function getCache($filename) {
        try{
            $pathToFile = $this->getFilePathByFilename($filename);
            if(!file_exists($pathToFile) || !is_file($pathToFile)) return false;
            $expiredTime = strtotime(get_option($this->getOptionKeyByFileName($filename)));

            $data = @file_get_contents($pathToFile);
            if(empty($data)) return false;

            if($expiredTime < current_time('timestamp')){
                @unlink($pathToFile);
                return false;
            }

            return $data;
        }catch (Exception $ex){
            return false;
        }
    }

    public function getFilePath() {

        if ($this->extension == 'none') {
            $ext = '';
        }
        else {
            $ext = $this->extension ? '.'.$this->extension : '.html';
        }

        return sprintf('%s/%s%s', $this->baseDir, $this->filename, $ext);
    }

    public function getFilePathByFilename($filename) {
        $filenameSplit = explode('.',basename($filename));
        $ext = end($filenameSplit);
        $formattedFilename = basename($filename, '.'.$ext);
        return sprintf('%s/%s', $this->baseDir, $formattedFilename) . '.html';
    }


    public function flush() {

        if (!is_admin()) {die;}

        $files = glob($this->baseDir.'/*');

        if ($files) {

            foreach($files as $file){ // iterate files
                if(is_file($file))
                    unlink($file); // delete file
            }
        }
    }

    /**
     * Remove cache file
     * @author vulh
     * @return mixed
     */
    public function remove() {
        $file = $this->getFilePath();
        if (file_exists($file)) {
            unlink($file);
        }
    }

    /**
     * Get base dir
     * @return string
     */
    public function getBaseDir()
    {
        return $this->baseDir;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @param string $extension
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
    }
}
    
endif;

