<?php

aplc_require_once(APLC. '/Inc/File.php');
aplc_require_once(APLC. '/Inc/Interface/File.php');

class APLC_Inc_Files_Syndication extends APLC_Inc_File implements APLC_Inc_Interface_File
{
    public function __construct($filename = '', $extension = 'xml')
    {
        parent::__construct();
        $this->filename = $filename;
        $this->extension = $extension;
    }

    public function save($content, $endDate = false)
    {
        if (!$this->filename) {
            return false;
        }

        $this->content = $content;
        $this->add(time() + 8 * 60 * 60);
    }

    /**
     * Get cache content base on server time
     *
     * @return string
     */
    public function get() {
        $data = false;
        if ( $this->localSiteCacheEnabled ) {
            $file = $this->getFilePath();
            $data = file_exists($file) ? @file_get_contents($file) : '';
            if (! $data || strtotime(get_option($this->getOptionKey())) < time() ) return false;
        }
        return $data;
    }
}