<?php

/**
 * Created by Thienld.
 * Date: 04/04/2016
 * Time: 16:15
 */
interface APLC_Interface_File_Cache_Class
{
    public function output();

    public function setCache($content);

    public function getCache();

    public function getPath();

    public function getFileName();

    public static function deleteCache($delete_args = array());
}