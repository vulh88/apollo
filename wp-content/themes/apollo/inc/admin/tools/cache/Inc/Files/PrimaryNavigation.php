<?php

aplc_require_once(APLC. '/Inc/File.php');
aplc_require_once(APLC. '/Inc/Interface/File.php');

class APLC_Inc_Files_PrimaryNavigation extends APLC_Inc_File implements APLC_Inc_Interface_File
{
    public $filename = '';

    public function __construct() {
        parent::__construct();
        $this->filename = 'primary_navigation';
    }

    public function save($content, $expiredTime = false) {

        $localSiteCacheEnabled = false;
        $currentBlogID = get_current_blog_id();
        $optionCachedValue = Apollo_App::get_network_local_cache($currentBlogID);
        if(intval($optionCachedValue) === 1){
            $localSiteCacheEnabled = true;
        }
        if($localSiteCacheEnabled){
            $this->content = $content;
            $this->add(strtotime(date('Y-m-d'). ' +30 days'));
        }
    }

    public function getContent() {

        /** @Ticket #15362 */
        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        $htmlCachedNavigation = is_plugin_active('menu-swapper/menu-swapper.php') ? '' : $this->get();

        if (!$htmlCachedNavigation) {
            ob_start();
            include_once(APOLLO_TEMPLATES_DIR. '/navigation/primary.php');
            $htmlCachedNavigation = ob_get_contents();
            ob_end_clean();

            // Ignore cache if the swapper plugin is activated
            if (!is_plugin_active('menu-swapper/menu-swapper.php')) {
                $this->save($htmlCachedNavigation);
            }
        }
        return $htmlCachedNavigation;
    }
}