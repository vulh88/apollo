<?php

aplc_require_once(APLC. '/Inc/File.php');
aplc_require_once(APLC. '/Inc/Interface/File.php');

class APLC_Inc_Files_EventIFrameSearchWidget extends APLC_Inc_File implements APLC_Inc_Interface_File
{
    public function __construct($params) {
        parent::__construct();

        if (empty($params['ifsw_id'])) {
            return;
        }
        $ifswID = $params['ifsw_id'];
        $this->filename = 'search_widget/_apl_iframe_search_widget_cached_'. $ifswID;
        $this->extension = 'none';
    }


    public function save($content, $expiredTime = false) {

    }

    /**
     * Get cache content
     *
     * @return string
     */
    public function get() {
        $data = false;
        if ( $this->localSiteCacheEnabled ) {
            $file = $this->getFilePath();
            $data = file_exists($file) ? @file_get_contents($file) : '';
            if (! $data) return false;
        }

        return $data;
    }
}