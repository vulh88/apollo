<?php

    /* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}    

if (!class_exists('APLC_Site_Caching_Management')):
    
class APLC_Site_Caching_Management
{

    private $siteID;
    private $uploadDir;
    private $uploadSiteDir;
    private $cachedDir;
    private $curSite;
    public $showAdminMessage; // to detect when user admin click link Empty All Apollo Cache on admin header bar.

    const UPLOAD_PATH = 'upload_path';

    public function __construct($site_id = 0)
    {
        $this->siteID = $site_id;
        $this->uploadDir = $this->getUploadBaseDir();
        $this->getSiteUploadDir();
        $this->showAdminMessage = true;
    }

    protected function getUploadBaseDir(){
        $uploadBaseDir = WP_CONTENT_DIR . '/uploads/sites';
        $curSiteUploadPath = get_option(self::UPLOAD_PATH, false);
        if(!empty($curSiteUploadPath)){
            if(class_exists('Class_Upload_Path_Custom')){
                $uploadBaseDir = Class_Upload_Path_Custom::uploadDirRule($curSiteUploadPath);
                $uploadBaseDir = rtrim($uploadBaseDir,'/') . '/sites';
            }
        }
        return $uploadBaseDir;
    }

    protected function getSiteUploadDir(){
        $this->curSite = get_blog_details($this->siteID);
        if(empty($this->curSite)){
            wp_die(__("Have some problem in process of clean caching in this site. Please try again."),"apollo");
        }
        $domain = $this->curSite->domain;
        $this->uploadSiteDir = $this->uploadDir . '/' . $domain;
        $this->cachedDir = $this->uploadDir . '/' . $domain . '/cache';
    }

    protected function addAdminSuccessMessage($message = ''){
        if(!empty($message)){
            $adminNotices = array(
                array(
                    'message' => $message,
                    'error' => false
                ));
            if(isset($_SESSION['messages'])){
                $_SESSION['messages'][] = $adminNotices;
            } else {
                $_SESSION['messages'] = array($adminNotices);
            }
        }
    }

    protected function addAdminErrorMessage($message = ''){
        if(!empty($message)){
            $adminNotices = array(
                array(
                    'message' => '',
                    'error' => $message
                ));
            if(isset($_SESSION['messages'])){
                $_SESSION['messages'][] = $adminNotices;
            } else {
                $_SESSION['messages'] = array($adminNotices);
            }
        }
    }

    public function emptyAllLocalCaches(){
        if (!is_admin() || !is_user_logged_in() || is_network_admin()) {
            wp_die(__("Clearing all local cached files are only executed in admin zone but not in network admin zone. Please log as administrator or move to admin zone for proceeding."),"apollo");
        }

        if(empty($this->curSite)){
            wp_die(__("The site doesn't exist and process of empty caching was stopped. Please try again."),"apollo");
        }

        try{
            // handle everything for cleaning all caches
            $this->emptyAllCachedFiles();
            if($this->showAdminMessage){
                $this->addAdminSuccessMessage(__("All caches is cleared on this site.","apollo"));
                wp_redirect(admin_url());
                exit;
            }
        } catch (Exception $ex) {
            aplDebugFile($ex->getMessage(),'APLC_Site_Caching_Management:emptyAllLocalCaches');
        }
    }

    protected function emptyAllCachedFiles(){
        if(file_exists($this->cachedDir) && is_dir($this->cachedDir)){
            $this->deleteAllFilesAndSubFolders($this->cachedDir);
        }
    }

    private function deleteAllFilesAndSubFolders($str) {
        //It it's a file.
        if (is_file($str)) {
            //Attempt to delete it.
            return unlink($str);
        }
        //If it's a directory.
        elseif (is_dir($str)) {
            //Get a list of the files in this directory.
            $scan = glob(rtrim($str,'/').'/*');
            //Loop through the list of files.
            foreach($scan as $index=>$path) {
                //Call our recursive function.
                $this->deleteAllFilesAndSubFolders($path);
            }
            //Remove the directory itself.
            return @rmdir($str);
        }
    }

    public function emptyCachedFile($filename){
        $file = $this->cachedDir . '/' . $filename . '.html';
        if(file_exists($this->cachedDir) && is_dir($this->cachedDir) && file_exists($file) && is_file($file)){
            @unlink($file); // delete file
        }

    }

    public function cacheThemeOptions($themeOptData){
        try{
            if(!empty($this->cachedDir) && file_exists($this->cachedDir) && is_dir($this->cachedDir)){
                @file_put_contents($this->cachedDir. '/'. of_get_option_id(). '.json', json_encode($themeOptData), LOCK_EX);
            }
        }catch (Exception $ex){
            aplDebugFile($ex->getMessage(),'APLC_Site_Caching_Management:cacheThemeOptions');
        }
    }

}

endif;