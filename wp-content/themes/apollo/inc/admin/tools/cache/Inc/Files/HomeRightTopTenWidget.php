<?php

aplc_require_once(APLC. '/Inc/File.php');
aplc_require_once(APLC. '/Inc/Interface/File.php');

class APLC_Inc_Files_HomeRightTopTenWidget extends APLC_Inc_File implements APLC_Inc_Interface_File
{
    public $filename = '';

    public function __construct() {
        parent::__construct();
        $this->filename = 'home/home-right-top-ten-widget';
    }


    public function save($content, $expiredTime = false) {
        $localSiteCacheEnabled = false;
        $currentBlogID = get_current_blog_id();
        $optionCachedValue = Apollo_App::get_network_local_cache($currentBlogID);
        if(intval($optionCachedValue) === 1){
            $localSiteCacheEnabled = true;
        }
        if($localSiteCacheEnabled){
            $this->content = $content;
            $this->add(strtotime(current_time('Y-m-d H:i:s') . '+6 hours'));
        }
    }
}