<?php

/**
 * Created by PhpStorm.
 * User: vulh
 * Date: 12/8/16
 * Time: 11:24 AM
 */

aplc_require_once(APLC. '/Inc/File.php');
aplc_require_once(APLC. '/Inc/Interface/File.php');

class APLC_Inc_Files_HomeFeaturedBlog extends APLC_Inc_File implements APLC_Inc_Interface_File
{
    public $filename = '';

    public function __construct() {
        parent::__construct();
        $this->filename = 'home/home-featured-blog';
    }


    public function save($content, $expiredTime = false) {
        $localSiteCacheEnabled = false;
        $currentBlogID = get_current_blog_id();
        $optionCachedValue = Apollo_App::get_network_local_cache($currentBlogID);
        if(intval($optionCachedValue) === 1){
            $localSiteCacheEnabled = true;
        }
        if($localSiteCacheEnabled){
            $this->content = $content;
            $this->add(strtotime(date('Y-m-d'). ' +10 days'));
        }
    }
}