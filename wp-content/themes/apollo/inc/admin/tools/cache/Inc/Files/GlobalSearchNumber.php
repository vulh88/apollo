<?php
aplc_require_once(APLC. '/Inc/File.php');
aplc_require_once(APLC. '/Inc/Interface/File.php');

class APLC_Inc_Files_GlobalSearchNumber extends APLC_Inc_File implements APLC_Inc_Interface_File
{
    public function __construct() {
        parent::__construct();
        $this->filename = 'global-search-number';
        $this->extension = 'json';
    }


    public function save($content, $endDate = false) {
        $localSiteCacheEnabled = false;
        $currentBlogID = get_current_blog_id();
        $optionCachedValue = Apollo_App::get_network_local_cache($currentBlogID);
        if(intval($optionCachedValue) === 1){
            $localSiteCacheEnabled = true;
        }
        if($localSiteCacheEnabled){
            $this->content = $content;

            $time = strtotime(date('Y-m-d H:i:s'). ' +3 hours');

            $this->add($time);
        }
    }
}