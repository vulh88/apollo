<?php

aplc_require_once(APLC. '/Inc/Interface/File.php');

interface APLC_Inc_Files_Syndication_Artist_CacheInterface extends APLC_Inc_Interface_File {

    /**
     * Set file name
     * @param $type
     * @param $postID
     * @return
     */
    public function setFileName($type, $postID = '');
}
