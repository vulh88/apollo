<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}    

if (!class_exists('APLC_Inc_Event_Category_Cache')):
    
class APLC_Inc_Event_Category_Cache
{
    // caching time
    const EVENT_CATEGORY_CACHING_HOURS = 168; // 1 week
    // event caching types
    const EVENT_SPOTLIGHT_CACHE = 'event_spotlight_caching';
    const EVENT_SPOTLIGHT_CACHE_END_DATE = 'event_spotlight_caching_end_date';
    const EVENT_FEATURED_CACHE = 'event_featured_caching';
    const EVENT_FEATURED_CACHE_END_DATE = 'event_featured_caching_end_date';
    const EVENT_OTHER_CACHE = 'event_other_caching';

    protected $current_site_id;
    protected $transient_time_cache_set;
    protected $isEnableLocalCache;
    protected $currentEventCategoryID = 0;

    public static function getCacheKey($site_id = ''){
        return '_apollo_event_category_caching_' . $site_id;
    }

    public function __construct()
    {
        $this->current_site_id = get_current_blog_id();
        $this->transient_time_cache_set = intval(self::EVENT_CATEGORY_CACHING_HOURS) * 60 * MINUTE_IN_SECONDS;
        $this->isEnableLocalCache = false;
        $optionCachedValue = Apollo_App::get_network_local_cache($this->current_site_id);
        if(intval($optionCachedValue) === 1){
            $this->isEnableLocalCache = true;
        }

        add_filter('apl_event_cache_check_local_cache_enable',array($this,'checkLocalCacheEnable'),1,1);
        add_filter('apl_event_cache_get_cache_data',array($this,'getEventCategoryCacheData'),1,1);
        add_action('apl_event_cache_set_data_cache',array($this,'setDataCache'),1,2);
        add_action('apl_event_cache_empty_all_cache',array($this,'emptyAllEventCategoryCaches'));

        // hook Wordpress trash event post type in both of FrontEnd and Backend in order to clear all of event category caching
        add_action('trashed_post',array($this,'handleEventPostTypeTrashed'),99,1);

    }

    public function handleEventPostTypeTrashed($post_id){
        $post = get_post($post_id);
        $postStatusBeforeDoingTrash = get_post_meta($post_id,'_wp_trash_meta_status',true);
        if($this->isEnabledLocalCache() && $post->post_type == Apollo_DB_Schema::_EVENT_PT && $postStatusBeforeDoingTrash == 'publish'){
            $this->emptyAllEventCategoryCaches();
        }
    }

    public function getEventCategoryCacheData($current_term_id = ''){
        $result = array(
            self::EVENT_SPOTLIGHT_CACHE => array(),
            self::EVENT_FEATURED_CACHE => array()
        );
        if($this->isEnabledLocalCache()){
            $this->setCurrentEventCategoryID($current_term_id);
            $result = array(
                self::EVENT_SPOTLIGHT_CACHE => $this->getCache(self::EVENT_SPOTLIGHT_CACHE),
                self::EVENT_FEATURED_CACHE => $this->getCache(self::EVENT_FEATURED_CACHE)
            );
        }
        return $result;
    }

    public function checkLocalCacheEnable($tmp){
        return $this->isEnabledLocalCache();
    }

    public function setDataCache($cache_type = '', $data_cache = ''){
        if($this->isEnabledLocalCache()){
            $this->setCache($cache_type,$data_cache);
        }
    }

    public function setCurrentEventCategoryID($term_id){
        $this->currentEventCategoryID = intval($term_id);
    }

    public function getCache($event_cache_type = ''){
        if(empty($event_cache_type)) return '';
        switch ($event_cache_type) {
            case self::EVENT_SPOTLIGHT_CACHE:
                $cache = $this->getEventSpotLightCache();
                break;
            case self::EVENT_FEATURED_CACHE:
                $cache = $this->getEventFeaturedCache();
                break;
            default:
                $cache = '';
                break;
        }

        // Delete transient cache if there is at least one expired event
        // # Ticket #11432
        if (isset($cache['endDates']) && $cache['endDates']) {
            foreach ($cache['endDates'] as $endDate) {
                if ((strtotime($endDate) + 24 * 60 * 60) < current_time('timestamp')) {
                    do_action('apl_event_cache_empty_all_cache');
                    return false;
                    break;
                }
            }
        }

        return $cache;
    }

    public static function getListIDFromArrayEventObjects($objectPosts = array()){
        if(empty($objectPosts)) return array();
        $resultEventsIDs = array();
        foreach($objectPosts as $event){
            $resultEventsIDs[] = $event->ID;
        }
        return $resultEventsIDs;
    }

    /**
     * Get list event IDs and end dates. TicketID: #11426
     *
     * @param array $objectPosts
     * @return array
     * @author vulh
     *
     */
    public static function getListIDAndEndDateFromArrayEventObjects($objectPosts = array()){
        if(empty($objectPosts)) return array();
        $resultEventsIDs = array();
        $resultEventsEndDates = array();
        foreach($objectPosts as $event){
            $endDate = get_apollo_meta($event->ID, Apollo_DB_Schema::_APOLLO_EVENT_END_DATE, true);
            $resultEventsEndDates[] = $endDate;
            $resultEventsIDs[] = $event->ID;
        }
        return [
            'eventIDs'   => $resultEventsIDs,
            'endDates'  => $resultEventsEndDates
        ];
    }

    protected function getEventSpotLightCache(){
        $eventCatCacheData = $this->getEventCategoryCachingBySiteId($this->current_site_id);
        $eventIDs = !empty($eventCatCacheData) && isset($eventCatCacheData[self::EVENT_SPOTLIGHT_CACHE][$this->currentEventCategoryID]) ? $eventCatCacheData[self::EVENT_SPOTLIGHT_CACHE][$this->currentEventCategoryID] : array();
        $endDates = !empty($eventCatCacheData) && isset($eventCatCacheData[self::EVENT_SPOTLIGHT_CACHE_END_DATE][$this->currentEventCategoryID]) ? $eventCatCacheData[self::EVENT_SPOTLIGHT_CACHE_END_DATE][$this->currentEventCategoryID] : array();
        return !empty($eventIDs) ? array('event_ids' => $eventIDs, 'endDates' => $endDates) : array();
    }

    protected function getEventFeaturedCache(){
        $eventCatCacheData = $this->getEventCategoryCachingBySiteId($this->current_site_id);
        $eventIDs = !empty($eventCatCacheData) && isset($eventCatCacheData[self::EVENT_FEATURED_CACHE][$this->currentEventCategoryID]) ? $eventCatCacheData[self::EVENT_FEATURED_CACHE][$this->currentEventCategoryID] : array();
        $endDates = !empty($eventCatCacheData) && isset($eventCatCacheData[self::EVENT_FEATURED_CACHE_END_DATE][$this->currentEventCategoryID]) ? $eventCatCacheData[self::EVENT_FEATURED_CACHE_END_DATE][$this->currentEventCategoryID] : array();
        return !empty($eventIDs) ? array('event_ids' => $eventIDs, 'endDates' => $endDates) : array();
    }

    public function setCache($event_cache_type = '', $data_cache = ''){
        if(!empty($event_cache_type)){
            switch ($event_cache_type) {
                case self::EVENT_SPOTLIGHT_CACHE:
                    $data_cache = $this->getListIDAndEndDateFromArrayEventObjects($data_cache);
                    $this->setEventSpotLightCache($data_cache);
                    break;
                case self::EVENT_FEATURED_CACHE:
                    $data_cache = $this->getListIDAndEndDateFromArrayEventObjects($data_cache);
                    $this->setEventFeaturedCache($data_cache);
                    break;
                default:
                    break;
            }
        }
    }

    public function setEventSpotLightCache($data_cache = ''){
        $eventCatCacheData = $this->getEventCategoryCachingBySiteId($this->current_site_id);
        $eventCatCacheData[self::EVENT_SPOTLIGHT_CACHE][$this->currentEventCategoryID] = isset($data_cache['eventIDs']) ? $data_cache['eventIDs'] : array();
        $eventCatCacheData[self::EVENT_SPOTLIGHT_CACHE_END_DATE][$this->currentEventCategoryID] = isset($data_cache['endDates']) ? $data_cache['endDates'] : array();
        set_site_transient(self::getCacheKey($this->current_site_id), $eventCatCacheData, $this->transient_time_cache_set);
    }

    public function setEventFeaturedCache($data_cache = ''){
        $eventCatCacheData = $this->getEventCategoryCachingBySiteId($this->current_site_id);
        $eventCatCacheData[self::EVENT_FEATURED_CACHE][$this->currentEventCategoryID] = isset($data_cache['eventIDs']) ? $data_cache['eventIDs'] : array();
        $eventCatCacheData[self::EVENT_FEATURED_CACHE_END_DATE][$this->currentEventCategoryID] = isset($data_cache['endDates']) ? $data_cache['endDates'] : array();
        set_site_transient(self::getCacheKey($this->current_site_id), $eventCatCacheData, $this->transient_time_cache_set);
    }

    public function emptyAllEventCategoryCaches(){
        if($this->isEnabledLocalCache()){
            delete_site_transient(self::getCacheKey($this->current_site_id));
        }
    }

    public function isEnabledLocalCache(){
        return $this->isEnableLocalCache;
    }

    public function getEventCategoryCachingBySiteId($site_id = ''){
        return get_site_transient(self::getCacheKey($site_id));
    }
}

new APLC_Inc_Event_Category_Cache();

endif;