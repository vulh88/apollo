<?php

aplc_require_once(APLC. '/Inc/File.php');
aplc_require_once(APLC. '/Inc/Interface/File.php');

class APLC_Inc_Files_Syndication_IframeCategory extends APLC_Inc_File implements APLC_Inc_Interface_File
{
    public function __construct($syndicationID, $extension)
    {
        parent::__construct();
        $this->filename = '/syndication-iframe-category-'. $syndicationID;
        $this->extension = $extension;
    }

    public function save($content, $endDate = false)
    {
        if (!$this->filename) {
            return false;
        }

        $this->content = $content;
        $this->add(strtotime(date('Y-m-d'). ' +3 days'));
    }
}