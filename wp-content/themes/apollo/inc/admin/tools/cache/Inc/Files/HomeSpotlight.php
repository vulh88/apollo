<?php

/**
 * Created by PhpStorm.
 * User: vulh
 * Date: 12/8/16
 * Time: 11:24 AM
 */

aplc_require_once(APLC. '/Inc/File.php');
aplc_require_once(APLC. '/Inc/Interface/File.php');

class APLC_Inc_Files_HomeSpotlight extends APLC_Inc_File implements APLC_Inc_Interface_File
{
    public $filename = '';

    public function __construct() {
        parent::__construct();
        $this->filename = 'home/home-spotlight';
    }


    public function save($content, $expiredTime = false) {
        $localSiteCacheEnabled = false;
        $currentBlogID = get_current_blog_id();
        $optionCachedValue = Apollo_App::get_network_local_cache($currentBlogID);
        if(intval($optionCachedValue) === 1){
            $localSiteCacheEnabled = true;
        }
        if($localSiteCacheEnabled){
            $this->content = $content;
            $t = current_time('Y-m-d 23:59:59');
            $this->add(strtotime($t));
        }
    }
}