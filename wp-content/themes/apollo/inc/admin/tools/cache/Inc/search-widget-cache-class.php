<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}    

if (!class_exists('APLC_Search_Widget_Cache_Class')):

require_once(APOLLO_ADMIN_DIR. '/tools/cache/Inc/abstract-cache-class.php');

class APLC_Search_Widget_Cache_Class extends APLC_Abstract_Cache_Class implements APLC_Interface_File_Cache_Class
{

    const _SEARCH_WIDGET_FILE_NAME = '_apl_iframe_search_widget_cached_';
    const _CACHE_META_KEY = '_apl_search_widget_cache_status';
    private $_ifswID = null;
    private $_fileTemplate = '';
    private $_parseArguments = array();

    public function __construct($args = array()) {
        parent::__construct($args);
        $this->_fileTemplate = $this->getArgValByKey('file','');
        $this->_parseArguments = $this->getArgValByKey('template_args',array());
        $this->_ifswID = isset($this->_parseArguments['ifswid']) ? $this->_parseArguments['ifswid'] : 0;
    }

    public function output() {
        // TODO: Implement output() method.
        $cacheStatus = get_apollo_meta($this->_ifswID,self::_CACHE_META_KEY,true);
        $cacheData = $this->getCache();
        if($this->isCached()&& intval($cacheStatus) === 1 && !empty($cacheData)){
            return $cacheData;
        }
        if(empty($this->_fileTemplate)) return '';
        $outputData = self::generateTemplateContentByArgs($this->_fileTemplate,$this->_parseArguments);
        if(empty($outputData)) return '';
        if($this->isCached()){
            $this->setCache($outputData);
        }
        return $outputData;
    }

    public function setCache($content) {
        // TODO: Implement setCache() method.
        try{
            global $wpdb;
            $file = $this->getPath();
            if(file_exists($file)){
                @unlink($file);
            }
            @file_put_contents($file,$content);
            update_apollo_meta($this->_ifswID,self::_CACHE_META_KEY,1);
        }catch (Exception $ex){
            aplDebugFile($ex->getMessage(),'APLC_Search_Widget_Cache_Class');
        }
    }

    public function getCache() {
        // TODO: Implement getCache() method.
        $file = $this->getPath();
        return @file_get_contents($file);
    }

    public function getPath()
    {
        // TODO: Implement getPath() method.
        $cacheFolder = $this->_cacheDir . '/search_widget';
        if(!file_exists($cacheFolder) || !is_dir($cacheFolder)){
            @mkdir($cacheFolder,0777);
        }
        if(file_exists($cacheFolder) && is_dir($cacheFolder)){
            return $cacheFolder . '/' . $this->getFileName();
        }
        return $this->_cacheDir . '/' . $this->getFileName();
    }

    public function getFileName()
    {
        // TODO: Implement getFileName() method.
        return self::_SEARCH_WIDGET_FILE_NAME . $this->_ifswID;
    }

    public static function deleteCache($delete_args = array())
    {
        // TODO: Implement deleteCache() method.
        try{
            $ifswID = isset($delete_args['ifswid']) ? intval($delete_args['ifswid']) : '';
            if(empty($ifswID)) return false;
            $instance = new self(array('template_args' => array('ifswid' => $ifswID)));
            @unlink($instance->getPath());
            update_apollo_meta($ifswID, self::_CACHE_META_KEY,0);
        }catch (Exception $ex){
            aplDebugFile($ex->getMessage(),'APLC_Search_Widget_Cache_Class');
        }
    }
}

endif;