<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}    

if (!class_exists('APLC_Abstract_Cache_Class')):

require_once(APOLLO_ADMIN_DIR. '/tools/cache/Inc/interface_file_cache_class.php');
    
abstract class APLC_Abstract_Cache_Class
{

    private $_args = array();
    protected $_uploadDir = array();
    protected $_cacheDir = '';


    public function __construct($args = array())
    {
        $this->_args = $args;
        $uploadDir      = wp_upload_dir();
        $cacheDir = $uploadDir['cache'];
        $cacheDir = $cacheDir[strlen($cacheDir) - 1] === DIRECTORY_SEPARATOR ? substr($cacheDir,0,strlen($cacheDir) - 1) : $cacheDir;
        $this->_cacheDir  = $cacheDir;
    }

    protected function isCached(){
        return (bool) $this->getArgValByKey('cached',false);
    }

    protected function isEnabledApolloLocalCache(){
        $localSiteCacheEnabled = false;
        $currentBlogID = get_current_blog_id();
        $optionCachedValue = Apollo_App::get_network_local_cache($currentBlogID);
        if(intval($optionCachedValue) === 1){
            $localSiteCacheEnabled = true;
        }
        return $localSiteCacheEnabled;
    }

    protected function getArgValByKey($key ,$default = ''){
        return isset($this->_args[$key]) ? $this->_args[$key] : $default;
    }

    protected function setArgValByKey($key, $set_val = ''){
        $this->_args[$key] = $set_val;
    }

    protected static function generateTemplateContentByArgs($file,$template_args = array()){
        $template_args = wp_parse_args( $template_args );
        $data = '';
        if(file_exists($file) && is_file($file)){
            ob_start();
            require( $file );
            $data = ob_get_clean();
        }
        return $data;
    }
}

endif;