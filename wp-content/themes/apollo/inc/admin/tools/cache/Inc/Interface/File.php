<?php

interface APLC_Inc_Interface_File
{
    /**
     * @param string $content
     * @param string $expiredTime
     * @return mixed
     */
    public function save($content, $expiredTime = false);
}