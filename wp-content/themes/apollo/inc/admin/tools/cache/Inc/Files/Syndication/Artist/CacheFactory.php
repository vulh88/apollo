<?php

class APLC_Inc_Files_Syndication_Artist_CacheFactory {
    const ARTIST = 'artist';
    const CATEGORY = 'artist-categories';
    const MEDIUM = 'artist-mediums';
    const STYLE = 'artist-styles';
    const CITY = 'artist-cities';

    public $postID;

    /**
     * @param $type
     * @param $content
     * @param $postID
     * @return bool
     */
    public function save($type, $content, $postID = '') {

        $cache = aplc_instance('APLC_Inc_Files_Syndication_Artist_Cache');
        if ($cache instanceof APLC_Inc_Files_Syndication_Artist_Cache) {
            $cache->setFileName($type, $postID);
            $cache->save($content);
            return true;
        }
        return false;
    }

    /**
     * Get cache content
     * @param $type
     * @param $postID
     * @return string
     */
    public function get($type, $postID = '') {

        $cache = aplc_instance('APLC_Inc_Files_Syndication_Artist_Cache');
        if ($cache instanceof APLC_Inc_Files_Syndication_Artist_Cache) {
            $cache->setFileName($type, $postID);
            return $cache->forceGet();
        }
        return '';
    }

    public function clear($type, $postID = '') {
        $cache = aplc_instance('APLC_Inc_Files_Syndication_Artist_Cache');
        if ($cache instanceof APLC_Inc_Files_Syndication_Artist_Cache) {};
        $cache->setFileName($type, $postID);
        $cache->remove();
    }

    public function setPostID($postID) {
        $this->postID = $postID;
    }

    public function getPostID() {
        return $this->postID;
    }
}