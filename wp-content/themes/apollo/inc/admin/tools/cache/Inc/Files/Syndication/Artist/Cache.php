<?php

aplc_require_once(APLC. '/Inc/File.php');
aplc_require_once(APLC. '/Inc/Interface/File.php');
aplc_require_once(APLC. '/Inc/Files/Syndication/Artist/CacheInterface.php');

class APLC_Inc_Files_Syndication_Artist_Cache extends APLC_Inc_File implements APLC_Inc_Files_Syndication_Artist_CacheInterface
{
    public function __construct()
    {
        parent::__construct();
        $this->extension = 'xml';
    }

    public function save($content, $endDate = false)
    {
        if (!$this->filename) {
            return false;
        }

        $this->content = $content;
        $this->add(strtotime(date('Y-m-d'). ' +3 days'));
    }

    /**
     * Set type
     * @param string $type
     * @param $postID
     */
    public function setFileName($type, $postID = '')
    {
        $this->filename = 'syndication/artist/'.sprintf('syndication%s%s', ($postID ? '-'.$postID : ''), ($type ? '-'. $type : ''));
    }
}