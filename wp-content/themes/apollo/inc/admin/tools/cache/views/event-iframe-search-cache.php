<div id="apl-cache-event-search" class="postbox apl-custom-postbox hidden">
    <button type="button" class="handlediv button-link" aria-expanded="true">
        <span class="screen-reader-text"><?php _e('Toggle panel: Event IFrame Search Widget', 'apollo'); ?></span>
        <span class="toggle-indicator" aria-hidden="true"></span>
    </button>
    <h3 class="hndle apl-custom-title"><?php _e('Event IFrame Search Widget','apollo') ?></h3>
    <div class="apl-admin-cache-block inside">
        <?php if (!empty($ifSearchWgs)) :?>
            <ul>
                <?php foreach ($ifSearchWgs as $p) :
                    $ifSearchWgCacheClass = aplc_instance('APLC_Inc_Files_EventIFrameSearchWidget', array(
                        'ifsw_id'  => $p->ID
                    ));
                    $isCache = $ifSearchWgCacheClass->get();

                    ?>
                    <li data-class-name="<?php echo $p->ID; ?>,APLC_Inc_Files_EventIFrameSearchWidget" class="<?php echo !$isCache ? 'disabled' : ''; ?>" data-params="<?php echo $p->ID; ?>">
                        <input type="checkbox" class="apl-cache-checkbox" name="event_ifsw_clear_cache[]" value="<?php echo $p->ID; ?>,APLC_Inc_Files_EventIFrameSearchWidget"/>
                        <span><?php echo $p->post_title; ?> </span>
                        <a href="#" class="apl-cache-remove"><?php _e('Purge cache', 'apollo'); ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php else:
            _e("Not have any cache",'apollo');
        endif; ?>
    </div>
</div>