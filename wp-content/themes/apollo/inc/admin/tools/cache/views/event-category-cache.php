<div id="apl-cache-event-category" class="postbox apl-custom-postbox hidden">
    <button type="button" class="handlediv button-link" aria-expanded="true">
        <span class="screen-reader-text"><?php _e('Toggle panel: Event category page', 'apollo'); ?></span>
        <span class="toggle-indicator" aria-hidden="true"></span>
    </button>
    <h3 class="hndle apl-custom-title"><?php _e('Event category page','apollo') ?></h3>
    <div class="apl-admin-cache-block inside">
        <?php
        /** Get all categories */
        $eventCats = get_terms(array(
            'taxonomy' => Apollo_DB_Schema::_EVENT_PT.'-type',
            'hide_empty' => false
        ))
        ?>
        <?php if (! empty( $eventCats ) && ! is_wp_error( $eventCats ) && !empty($eventCacheFiles)) :
            ?>
            <?php foreach ($eventCats as $item ) : ?>
            <ul>
                <p class="apl-admin-cache-cate-name"><?php echo $item->name; ?> (<?php _e('#ID:', 'apollo')?> <?php echo $item->term_id; ?>)</p>
                <?php foreach ($eventCacheFiles as $key => $value) :
                    $homePath = $instancePath . $key;
                    $fileCache = aplc_instance($homePath, $item->term_id, true);
                    $is_cache = false;
                    switch ($key) {
                        case 'EventCategoryFeaturedBlock' :
                            if ($fileCache->get()) {
                                $is_cache = true;
                            }
                            break;
                        case 'EventCategorySpotlightBlock' :
                            if ($fileCache->get()) {
                                $is_cache = true;
                            }
                            break;

                        default :
                            $is_cache = false;
                            break;
                    }
                    ?>
                    <li data-class-name="<?php echo $homePath; ?>" data-params="<?php echo $item->term_id; ?>"
                        data-file-type="event" class="<?php echo !$is_cache ? 'disabled' : ''; ?>" data-event-transient-name="<?php echo $key; ?>">
                        <input type="checkbox" class="apl-cache-checkbox" name="apl-admin-event-cache-clear[]" value="<?php echo $item->term_id . ',' .$homePath . ',' .$key; ?>"/>
                        <span><?php echo $value; ?> </span>
                        <?php if($is_cache) : ?>
                            <span class="apl-admin-cache-exp">( <?php _e('Expiration date: ', 'apollo'); echo get_option($fileCache->getOptionKey()); ?> -
                               - <?php echo Apollo_Admin_Cache::formatExpiredDate(get_option($fileCache->getOptionKey()), current_time('Y-m-d h:i:s'));?>) - </span>
                        <?php endif; ?>
                        <a href="#" class="apl-cache-remove"><?php _e('Purge cache', 'apollo'); ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endforeach;?>
        <?php endif; ?>
    </div>
</div>