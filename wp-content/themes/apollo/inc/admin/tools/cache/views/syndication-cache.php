<div id="apl-cache-syndication" class="postbox apl-custom-postbox hidden">
    <button type="button" class="handlediv button-link" aria-expanded="true">
        <span class="screen-reader-text"><?php _e('Toggle panel: Syndication', 'apollo'); ?></span>
        <span class="toggle-indicator" aria-hidden="true"></span>
    </button>
    <h3 class="hndle apl-custom-title"><?php _e('Syndication','apollo') ?></h3>
    <div class="apl-admin-cache-block inside">
        <?php if (!empty($syndicationCacheFiles)) : ?>
            <?php
            global $wpdb;
            $synPostType = Apollo_DB_Schema::_SYNDICATION_PT;
            $listSyndication = $wpdb->get_results("SELECT * FROM ".$wpdb->posts." WHERE post_status = 'publish' AND post_type = '{$synPostType}'");
            if ($listSyndication) {
                $className = $instancePath.'Syndication';
                require_once APOLLO_ADMIN_SYNDICATE_DIR . '/class-apollo-syndicate-abstract.php';
                require_once APOLLO_ADMIN_SYNDICATE_DIR . '/class-apollo-export-xml.php';
                $syndicationTransient = new Apollo_Syndicate_Export_XML();
                foreach ($listSyndication as $item) :
                    $syndicationTransient->setApID($item->ID);
                    $eventTransient = $syndicationTransient->getSyndicationDataCache(false);
                    $transientKey = $syndicationTransient->getSyndicationTransientKey();
                    ?>
                    <ul>
                        <p class="apl-admin-cache-cate-name"><?php echo $item->post_title; ?> (<?php _e('#ID:', 'apollo')?> <?php echo $item->ID; ?>)</p>
                        <li data-class-name="<?php echo $className; ?>" data-params="<?php echo $item->ID; ?>" class="<?php echo empty($eventTransient) ? 'disabled' : ''; ?>">
                            <input type="checkbox" class="apl-cache-checkbox" name="apl-admin-syndication-transient-cache[]" value="<?php echo $item->ID; ?>"/>
                            <span><?php _e('Transient cache', 'apollo'); ?> </span>
                            <?php if (!empty($eventTransient)) :
                                $transientTimeout = get_option('_transient_timeout_'.$transientKey);
                                $transientTimeout = $transientTimeout ? date('Y-m-d H:i:s', $transientTimeout) : '';
                                ?>
                                <span class="apl-admin-cache-exp">( <?php _e('Expiration date: ', 'apollo'); echo $transientTimeout; ?>
                                   - <?php echo $transientTimeout ? Apollo_Admin_Cache::formatExpiredDate($transientTimeout, current_time('Y-m-d h:i:s')) : '' ;?>) - </span>
                            <?php endif; ?>
                            <a href="#" class="apl-cache-remove transient-cache"><?php _e('Purge cache', 'apollo'); ?></a>
                        </li>
                        <?php
                        foreach ($syndicationCacheFiles as $key => $value) :
                            $fileName = $key . '-' . $item->ID;
                            $synCache = aplc_instance($className);
                            $synCache->setFilename($fileName);
                            $synCache->setExtension($value['extension']);
                            $is_cache = $synCache->get();
                            ?>
                            <li data-class-name="<?php echo $className; ?>" data-params="<?php echo $fileName; ?>"
                                data-extension="<?php echo $value['extension']; ?>" class="<?php echo !$is_cache ? 'disabled' : ''; ?>">
                                <input type="checkbox" class="apl-cache-checkbox" name="apl-admin-syndication-cache-clear[]" value="<?php echo ($fileName . ',' . $value['extension']); ?>"/>
                                <span><?php echo $value['display_name']; ?> </span>
                                <?php if($is_cache) : ?>
                                    <span class="apl-admin-cache-exp">( <?php _e('Expiration date: ', 'apollo'); echo get_option($synCache->getOptionKey()); ?> - <?php echo Apollo_Admin_Cache::formatExpiredDate(get_option($synCache->getOptionKey()), current_time('Y-m-d h:i:s'));?> ) - </span>
                                <?php endif; ?>
                                <a href="#" class="apl-cache-remove"><?php _e('Purge cache', 'apollo'); ?></a>
                            </li>
                            <?php
                        endforeach;
                        ?>
                    </ul>
                    <?php
                endforeach;
            }
            ?>
        <?php endif; ?>
    </div>
</div>