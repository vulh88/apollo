<?php $instancePath = 'APLC_Inc_Files_';?>
<div class="wrap">
    <form id="apl-admin-cache" action="admin.php?page=apl_admin_cache" method="post">
        <h1 class="agency-left-title"> <?php _e('Apollo Caching Management', 'apollo'); ?></h1>
        <h2 class="nav-tab-wrapper">
            <a class="nav-tab nav-tab-active" data-tab-id="apl-cache-general" title="<?php _e('General', 'apollo'); ?>" href="#"><?php _e('General', 'apollo'); ?></a>
            <a class="nav-tab" data-tab-id="apl-cache-home" title="<?php _e('Home Page', 'apollo'); ?>" href="#"><?php _e('Home Page', 'apollo'); ?></a>
            <a class="nav-tab" data-tab-id="apl-cache-event-search" title="<?php _e('Event Search Widget', 'apollo'); ?>" href="#"><?php _e('Event Search Widget', 'apollo'); ?></a>
            <a class="nav-tab" data-tab-id="apl-cache-event-category" title="<?php _e('Event Category', 'apollo'); ?>" href="#"><?php _e('Event Category', 'apollo'); ?></a>
            <a class="nav-tab" data-tab-id="apl-cache-syndication" title="<?php _e('Syndication', 'apollo'); ?>" href="#"><?php _e('Syndication', 'apollo'); ?></a>
            <a class="nav-tab" data-tab-id="apl-cache-territory" title="<?php _e('Territory', 'apollo'); ?>" href="#"><?php _e('Territory', 'apollo'); ?></a>
        </h2>
        <div class="apl-cache-wrap">
            <div class="apl-cache-control">
                <p class="submit">
                    <input type="submit" name="submit" id="submit" class="apl-admin-cache-submit button button-primary" value="<?php _e( 'Purge cache', 'apollo' ); ?>">
                </p>
            </div>
            <div class="apl-cache-content">
                <?php
                include APLC . '/views/general-cache.php';
                include APLC . '/views/home-cache.php';
                include APLC . '/views/event-iframe-search-cache.php';
                include APLC . '/views/event-category-cache.php';
                include APLC . '/views/syndication-cache.php';
                include APLC . '/views/territory-cache.php';
                ?>
            </div>
            <div class="apl-cache-control bottom">
                <p class="submit">
                    <input type="submit" name="submit" class="apl-admin-cache-submit button button-primary" value="<?php _e( 'Purge cache', 'apollo' ); ?>">
                </p>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($){$(".if-js-closed").removeClass("if-js-closed").addClass("closed");
        postboxes.add_postbox_toggles( 'nav-menus');
    });
</script>