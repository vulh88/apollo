<div id="apl-cache-territory" class="postbox apl-custom-postbox hidden">
    <button type="button" class="handlediv button-link" aria-expanded="true">
        <span class="screen-reader-text"><?php _e('Toggle panel: Territory API', 'apollo'); ?></span>
        <span class="toggle-indicator" aria-hidden="true"></span>
    </button>
    <h3 class="hndle apl-custom-title"><?php _e('Territory API','apollo') ?></h3>
    <div class="apl-admin-cache-block inside">
        <?php if (!empty($territoryCacheFile)) : ?>
            <ul>
                <?php foreach ($territoryCacheFile as $key => $value) :
                    $homePath = $instancePath . $key;
                    $fileCache = aplc_instance($homePath);
                    $is_cache = $fileCache->get();
                    ?>
                    <li data-class-name="<?php echo $homePath; ?>" class="<?php echo !$is_cache ? 'disabled' : ''; ?>">
                        <span><?php echo $value; ?> </span>
                        <?php if($is_cache) : ?>
                            <span class="apl-admin-cache-exp">( <?php _e('Expiration date: ', 'apollo'); echo get_option($fileCache->getOptionKey());?>
                               - <?php echo Apollo_Admin_Cache::formatExpiredDate(get_option($fileCache->getOptionKey()), current_time('Y-m-d h:i:s'));?> )</span>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </div>
</div>