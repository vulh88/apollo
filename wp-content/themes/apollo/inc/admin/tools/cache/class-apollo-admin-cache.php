<?php
/**
 * Created by PhpStorm.
 * User: pc92-vu
 * Date: 21/01/2016
 * Time: 11:33
 */

if (!class_exists('Apollo_Admin_Cache')):
    class Apollo_Admin_Cache {

        protected $page = 'apl_cache_dashboard';
        private $enableCache = false;

        public function __construct()
        {
            $this->enableCache = Apollo_App::get_network_local_cache(get_current_blog_id());

            add_action( 'network_admin_menu', array($this, 'dm_network_pages') );
            add_action( 'admin_bar_menu', array($this, 'add_menu_caching_on_admin_bar'), 99);
            if (!empty($_POST['apl-admin-cache-clear']) ||
                !empty($_POST['apl-admin-event-cache-clear']) ||
                !empty($_POST['event_ifsw_clear_cache']) ||
                !empty($_POST['apl-admin-syndication-cache-clear']) ||
                !empty($_POST['apl-admin-syndication-transient-cache']) ||
                (!empty($_GET['clear_site_caches']) && $_GET['clear_site_caches'] == 'yes')
            ) {
                $this->apollo_clear_cache_files();
            }
            if(!is_network_admin() && $this->enableCache) {
                add_menu_page(
                    __('Apollo Cache', 'apollo'),
                    __('Apollo Cache', 'apollo'),
                    'manage_options',
                    'apl_admin_cache',
                    array($this, 'apollo_admin_cache'),
                    ''. home_url().'/wp-content/themes/apollo/inc/admin/assets/images/aflag-icon.png'
                );
            }
            add_action('admin_enqueue_scripts', array($this, 'enqueue_postbox_script'));
        }

        public function enqueue_postbox_script() {
            wp_enqueue_script('postbox');
//            wp_enqueue_style('apl-syndication-page',home_url(). '/wp-admin/css/common.min.css');
        }

        function dm_network_pages() {
            add_menu_page(__('Apollo Cache', 'apollo'), __('Apollo Cache', 'apollo'), 'manage_options', 'apl_cache_dashboard', array($this, 'dashboard'), ''. home_url().'/wp-content/themes/apollo/inc/admin/assets/images/aflag-icon.png');
        }

        function dashboard() {
            switch($this->page):
                case 'apl_cache_dashboard':
                    $optionPage = aplc_instance('APLC_UI_DashboardAdminView');
                    $optionPage->view();
                    if (isset($_POST['aplc_flush_all'])) {
                        $fileCache = aplc_instance('APLC_Inc_File');
                        $fileCache->flush();
                    }
            endswitch;
        }

        /**
         * @Ticket #15006 - Render cache info
         */
        function apollo_admin_cache() {
            $homeCacheFiles = array(
                'HomeFeatured' =>  __('Featured event', 'apollo'),
                'HomeFeaturedBlog' => __('Featured blog', 'apollo'),
                'HomeRightTopTenWidget' => __('Right top ten widget', 'apollo'),
                'HomeSpotlight' => __('Spotlight', 'apollo')
            );
            $eventCacheFiles = array(
                'EventCategorySpotlightBlock' => __('Category spotlight', 'apollo'),
                'EventCategoryFeaturedBlock' => __('Category feature', 'apollo')
            );
            $generalCacheFiles = array(
                'PrimaryNavigation' => __('Primary navigation', 'apollo'),
                'EventWidgetSearchCategory' => __( 'Search Widget - Event category dropdown', 'apollo' )
            );

            $ifSearchWgs = get_posts(array(
                'post_type' => 'iframesw',
                'post_status'   => 'publish'
            ));

            $territoryCacheFile = array(
                'Territory' => __('Territory API', 'apollo')
            );

            $syndicationCacheFiles = array(
                'syndication-xml'       => array(
                    'display_name' => __('Syndication XML file', 'apollo'),
                    'extension' => 'xml'
                ),
                'syndication-mcp'       => array(
                    'display_name' => __('Syndication MAILCHIMP file', 'apollo'),
                    'extension' => 'xml'
                ),
                'syndication-rss'       => array(
                    'display_name' => __('Syndication RSS file', 'apollo'),
                    'extension' => 'xml'
                ),
                'syndication-ical'      => array(
                    'display_name' => __('Syndication ICAL file', 'apollo'),
                    'extension' => 'txt'
                ),
                'syndication-wp'        => array(
                    'display_name' => __('Syndication WP file', 'apollo'),
                    'extension' => 'xml'
                ),
                'syndication-etpl-html' => array(
                    'display_name' => __('Syndication Email html file', 'apollo'),
                    'extension' => 'html'
                ),
                'syndication-etpl-text' => array(
                    'display_name' => __('Syndication Email plain text file', 'apollo'),
                    'extension' => 'html'
                ),
            );

            include 'views/cache-list.php';
        }

        /**
         * @Ticket #15013 - Clear cache
         */
        function apollo_clear_cache_files() {

            if (!empty($_POST['event_ifsw_clear_cache'])) {
                foreach ($_POST['event_ifsw_clear_cache'] as $file) {
                    $data = explode(',', $file);
                    list($id, $swCacheClass) = $data;
                    $fileCache = aplc_instance($swCacheClass, array('ifsw_id' => $id));
                    $fileCache->remove();
                }
            }

            if (!empty($_POST['apl-admin-cache-clear'])) {
                foreach ($_POST['apl-admin-cache-clear'] as $file) {
                    $fileCache = aplc_instance($file);
                    $fileCache->remove();
                }
            }
            /** Event cache */
            if (!empty($_POST['apl-admin-event-cache-clear'])) {
                $eventCaching = new APLC_Inc_Event_Category_Cache();
                foreach ($_POST['apl-admin-event-cache-clear'] as $event) {
                    $eventCache = explode(',', $event);
                    if (!empty($eventCache[0]) && !empty($eventCache[1]) && !empty($eventCache[2])) {
                        $eventFile = aplc_instance($eventCache[1], $eventCache[0], true);
                        $eventFile->remove();
                        
                        $eventCaching->setCurrentEventCategoryID($eventCache[0]);
                        switch ($eventCache) {
                            case 'EventCategoryFeaturedBlock' :
                                $eventCaching->setEventFeaturedCache('');
                                break;
                            case 'EventCategorySpotlightBlock' :
                                $eventCaching->setEventSpotLightCache('');
                                break;
                            default :
                                break;
                        }
                    }
                }
            }

            /** Syndication files cache */
            if (!empty($_POST['apl-admin-syndication-cache-clear'])) {
                foreach ($_POST['apl-admin-syndication-cache-clear'] as $syndication) {
                    $synParams = explode(",", $syndication);
                    if (!empty($synParams[0]) && !empty($synParams[1])) {
                        /** Clear files cache */
                        $synFile = aplc_instance('APLC_Inc_Files_Syndication');
                        $synFile->setFilename($synParams[0]);
                        $synFile->setExtension($synParams[1]);
                        $synFile->remove();
                    }
                }
            }

            /** Syndication transient cache*/
            if (!empty($_POST['apl-admin-syndication-transient-cache'])) {
                foreach ($_POST['apl-admin-syndication-transient-cache'] as $synId) {
                    self::admin_clear_syndication_transient_cache($synId);
                }
            }

            /**
             * Clear all cache
             * @ticket #13093
             */
            if(isset($_GET['clear_site_caches']) && $_GET['clear_site_caches'] == 'yes'){

                $action = !empty($_GET['action']) ? $_GET['action'] : '';
                switch($action) {

                    default:
                        $cacheManagementFilePath = APLC. '/Inc/apollo-sites-caching-management.php';
                        if(file_exists($cacheManagementFilePath)){
                            require_once $cacheManagementFilePath;
                            if(class_exists('APLC_Site_Caching_Management')){
                                $currentSiteID = get_current_blog_id();
                                $cleanCachedSite = new APLC_Site_Caching_Management($currentSiteID);
                                $cleanCachedSite->emptyAllLocalCaches();
                            }
                        }
                        break;
                }

            }

            add_action('admin_notices', array($this, 'admin_cache_notices'));
        }

        public function admin_cache_notices() {
            $class = 'notice notice-success is-dismissible';
            $message = __( 'Success!', 'apollo' );

            printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
        }

        function add_menu_caching_on_admin_bar(){

            if(!is_admin() || !is_admin_bar_showing() || is_network_admin() || !$this->enableCache) :
                return;
            endif;

            global $wp_admin_bar;

            /* Add the Admin Bar item */
            $wp_admin_bar->add_menu(array(
                'id' => 'clear_site_cache',
                'title' => '<span class="ab-icon"></span><span class="ab-label">'._x( 'Empty all Apollo cache', 'apollo' ).'</span>',
                'href' => admin_url('/admin.php?page=apl_admin_cache&clear_site_caches=yes'),
                'meta'  => array(
                    'title' => __('Empty all Apollo cache','apollo'),
                )
            ));

        }

        /**
         * Clear transient cache
         * @param $syndication
         */
        public static function admin_clear_syndication_transient_cache($syndication) {
            /** Clear transient cache */
            $filePath = APOLLO_ADMIN_SYNDICATE_DIR . '/class-apollo-export-xml.php';
            if(!class_exists('Apollo_Syndicate_Export_XML')){
                if(file_exists($filePath)){
                    require_once APOLLO_ADMIN_SYNDICATE_DIR . '/class-apollo-syndicate-abstract.php';
                    require_once $filePath;
                }
            }
            $transientCache = new Apollo_Syndicate_Export_XML();
            $transientCache->setApID($syndication);
            $transientKey = $transientCache->getSyndicationTransientKey();
            delete_transient($transientKey);
        }

        /**
         * Format Expired date.
         * @param $date1
         * @param $date2
         * @return string
         */
        public static function formatExpiredDate($date1, $date2) {
            $date1 = new DateTime($date1);
            $date2 = new DateTime($date2);
            $interval = $date1->diff($date2);
            $str = __('expired in ', 'apollo');
            if ($interval->format('%m')) {
                $str .= $interval->format('%m') . __(' month ', 'apollo');
            }
            if ($interval->format('%d')) {
                $str .= $interval->format('%d') . __(' days ', 'apollo');
            }
            if ($interval->format('%h')) {
                $str .= $interval->format('%h') . __(' hours ', 'apollo');
            }
            if(!$interval->format('%m') && !$interval->format('%d') && !$interval->format('%h')){
                $str = __('expired in less than 1 hour');
            }
            return $str;
        }

    }
    new Apollo_Admin_Cache();
endif;
