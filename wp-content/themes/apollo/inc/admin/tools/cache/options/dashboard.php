<?php if (!defined('APLC')) die(); ?>

<form id="w3tc_dashboard" action="admin.php?page=apl_cache_dashboard" method="post">
    <p>
        Perform a
        <?php echo $this->nonce_field('w3tc'); ?>
        <input id="flush_all" class="button" type="submit" name="aplc_flush_all" value="<?php _e('empty all caches', 'apollo') ?>" />
    </p>
</form>
