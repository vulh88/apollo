<?php

if (!defined('APLC')) die;

aplc_require_once(APLC. '/UI/PluginView.php');

class APLC_UI_DashboardAdminView extends APLC_UI_PluginView{

    function view() {
        include APLC .'/options/dashboard.php';
    }
}
