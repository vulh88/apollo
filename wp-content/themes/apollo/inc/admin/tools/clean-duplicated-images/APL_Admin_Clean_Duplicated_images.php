<?php

class APL_Admin_Clean_Duplicated_images
{
    /**
     * Page hook for the options screen
     *
     * @type string
     */
    protected $options_screen = null;

    /**
     * Save notice info
     * @type string
     */
    protected $notice = null;

    protected $should_update_newoptions = false;
    protected $newoptions = array();


    public function __construct() {
        add_action( 'admin_menu', array( $this, 'add_menu' ) );
    }

    function add_menu() {
        /**
         * Menu position: https://developer.wordpress.org/reference/functions/add_menu_page/#menu-structure
         */
        add_menu_page(
            __('Clean Duplication Images', 'apollo'),
            __('Clean Duplication Images', 'apollo'),
            'administrator',
            'apollo-clean-duplication-images',
            array($this, 'table'),
            home_url() . '/wp-content/themes/apollo/inc/admin/assets/images/aflag-icon.png',
            41 // add this menu before Appearance menu
        );
    }

    function table()
    {
        global $wpdb;


        if( ! class_exists( 'APL_Admin_Clean_Duplicated_Images_Table' ) ) {
            require_once( APOLLO_ADMIN_DIR . '/tools/clean-duplicated-images/views/APL_Admin_Clean_Duplicated_Images_Table.php' );
        }

        $tableView = new APL_Admin_Clean_Duplicated_Images_Table();

        if (isset($_POST['action'])) {
            switch ($tableView->current_action()) {
                case 'double_check':
                    break;
                case 'delete':
                    $tableView->clean();
                    break;
            }
        }

        $tableView->prepare_items();


        ?>

        <style>
            .img.column-img img {
                width: 60px;
                height: auto !important;
            }
        </style>

        <div class="wrap">
            <div id="icon-users" class="icon32"></div>
            <h2>
                <?php _e('Clean Duplication Images', 'apollo') ?>
            </h2>


            <?php

            if ($tableView->getRespectResult()) {
                $tableView->errorMessage = "Some images are using for other items. Please check the Reference column for more detail";
                echo sprintf('<div class="error notice"><p>%s: %s</p></div>', __('Error', 'apollo'), $tableView->errorMessage);
            }

            ?>
                <form id="apollo-clean-duplication-images" method="post" action="/wp-admin/admin.php?page=apollo-clean-duplication-images">
                    <input type="hidden" name="page" value="apollo-clean-duplication-images" />
                    <?php
                    wp_nonce_field();
                    $tableView->display();
                    ?>
                </form>

        </div>
        <?php
    }
}

new APL_Admin_Clean_Duplicated_images;