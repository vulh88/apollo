<?php

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class APL_Admin_Clean_Duplicated_Images_Table extends WP_List_Table
{
    private $perPage = 20;
    private $totalItems;
    private $offset = 0;

    public $grandTotalOfficialWebsiteClicks = 0;
    public $grandTotalBuyTicketClicks       = 0;
    public $grandTotalDiscountTicketClicks  = 0;

    protected $respectResult = array();


    public $errorMessage = '';

    /**
     * Prepare the items for the table to process
     *
     * @return Void
     */
    public function prepare_items()
    {
        $hidden       = $this->get_hidden_columns();
        $sortable     = $this->get_sortable_columns();
        $currentPage  = $this->get_pagenum();
        $perPage      = $this->perPage;
        $this->offset = ($currentPage - 1) * $this->perPage;

        $data         = $this->table_data();
        $columns      = $this->get_columns();

        $this->set_pagination_args( array(
            'total_items' => $this->totalItems,
            'per_page'    => $perPage
        ) );
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = $data;
    }


    /**
     * Override the parent columns method. Defines the columns to use in your listing table
     *
     * @return Array
     */
    public function get_columns()
    {
        $columns = array(
            'cb'    => true,
            'item_id'                => __('ID', 'apollo'),
            'img'                => __('image', 'apollo'),
            'title'                  => __('Name', 'apollo'),
            'status'                  => __('Reference', 'apollo'),
        );

        return $columns;
    }

    /**
     * Define which columns are hidden
     *
     * @return Array
     */
    public function get_hidden_columns()
    {
        return array();
    }

    /**
     * Define the sortable columns
     *
     * @return Array
     */
    public function get_sortable_columns()
    {
        return array(
            'item_id'   => array( 'item_id'  , false ),
            'img'   => array( 'img'  , false ),
            'title'      => array( 'title'     , false ),
        );
    }

    /**
     * Get the table data
     *
     * @return Array
     */
    private function table_data()
    {
        global $wpdb, $avaiable_modules;

        $likeCondArr = array();
        if ($avaiable_modules) {
            foreach($avaiable_modules as $m) {
                $likeCondArr[] = sprintf("dup_img_tbl.guid LIKE '%s%s-featured-%s'", '%', $m, '%');
            }
        }

        $likeCondStr = implode(' OR ', $likeCondArr);

        $sql = "
            SELECT dup_img_tbl.ID, dup_img_tbl.post_title, dup_img_tbl.guid, featured_img.meta_id, featured_img.meta_value  from $wpdb->posts dup_img_tbl
            LEFT JOIN $wpdb->postmeta featured_img ON featured_img.meta_value = dup_img_tbl.ID AND featured_img.meta_key = '_thumbnail_id'
            WHERE dup_img_tbl.post_type = 'attachment' AND featured_img.meta_id IS NULL AND ($likeCondStr)
            ORDER BY dup_img_tbl.ID ASC
            LIMIT $this->offset, $this->perPage
        ";

        return $wpdb->get_results($sql);
    }

    /**
     * Define what data to show on each column of the table
     *
     * @param  Array $item        Data
     * @param  String $column_name - Current column name
     *
     * @return Mixed
     */
    public function column_default( $item, $column_name )
    {
        switch( $column_name ) {
            case 'item_checkbox':
            case 'img':
            case 'item_id':
            case 'title':
            default:
                return print_r( $item, true ) ;
        }
    }

    /**
     * Display title as an anchor tag
     *
     * @param array $item Item was stored in MongoDB
     *
     * @return string
     */
    public function column_img($item)
    {
        return wp_get_attachment_image($item->ID, 'thumbnail');
    }

    function column_cb($item) {
        return sprintf(
            '<input type="checkbox" name="id[]" value="%s" />', $item->ID
        );
    }

    /**
     * Display item ID as an anchor tag
     *
     * @param array $item Item was stored in MongoDB
     *
     * @return string
     */
    public function column_item_checkbox($item)
    {
        $isUsing = !empty($this->respectResult) && in_array($item->ID, $this->respectResult);
        return $isUsing ? '':'<input type="checkbox" name="id[]" value="'.$item->ID.'" >';
    }

    /**
     * Display item ID as an anchor tag
     *
     * @param array $item Item was stored in MongoDB
     *
     * @return string
     */
    public function column_item_id($item)
    {

        $editLink = get_edit_post_link($item->ID);
        $title    = $item->ID;

        return sprintf('<a target="_blank" href="%s">%s</a>', esc_url($editLink), $title);
    }

    /**
     * Display title as an anchor tag
     *
     * @param array $item Item was stored in MongoDB
     *
     * @return string
     */
    public function column_title($item)
    {
        $editLink = get_edit_post_link($item->ID);
        $title    = $item->post_title;

        return sprintf('<a target="_blank" href="%s">%s</a>', esc_url($editLink), $title);
    }

    /**
     * Display title as an anchor tag
     *
     * @param array $item Item was stored in MongoDB
     *
     * @return string
     */
    public function column_status($item)
    {
        if (!$imgIds = array_keys($this->respectResult)) {
            return '';
        }

        $isUsing = !empty($imgIds) && in_array($item->ID, $imgIds);

        $usedData = isset($this->respectResult[$item->ID]) ? $this->respectResult[$item->ID] : '';
        $links = array();
        if ($usedData) {
            foreach ($usedData as $p) {
                $links[$p['post_id']] = sprintf('<a href="%s" target="_blank">%s</a>', get_edit_post_link($p['post_id']), get_the_title($p['post_id']));
            }
        }

        $linksStr = $links ? implode(', ', $links) : '';
        return $isUsing ? $linksStr : 'Can delete';
    }

    /**
     *
     * @return array
     */
    protected function get_bulk_actions() {
        $actions = array();
        $actions['delete'] = __( 'Delete' );

        return $actions;
    }

    public function clean()
    {
        $data = $_POST['id'];
        $respectResult = array();

        $checkingModules = array(
            'event' => array(
                'table' => Apollo_Tables::_APOLLO_EVENT_META,
                'gallery_field' => Apollo_DB_Schema::_APOLLO_EVENT_IMAGE_GALLERY,
            ),

            'venue' => array(
                'table' => Apollo_Tables::_APL_VENUE_META,
                'gallery_field' => Apollo_DB_Schema::_APL_VENUE_IMAGE_GALLERY,
            ),

            'artist' => array(
                'table' => Apollo_Tables::_APOLLO_ARTIST_META,
                'gallery_field' => Apollo_DB_Schema::_APL_ARTIST_IMAGE_GALLERY,
            ),
            'classified' => array(
                'table' => Apollo_Tables::_APL_CLASSIFIED_META,
                'gallery_field' => Apollo_DB_Schema::_APL_CLASSIFIED_IMAGE_GALLERY,
            ),
            'organization' => array(
                'table' => Apollo_Tables::_APL_ORG_META,
                'gallery_field' => Apollo_DB_Schema::_APL_ORG_IMAGE_GALLERY,
            ),
            'program' => array(
                'table' => Apollo_Tables::_APL_PROGRAM_META,
                'gallery_field' => Apollo_DB_Schema::_APL_PROGRAM_IMAGE_GALLERY,
            ),
            'public_art' => array(
                'table' => Apollo_Tables::_APL_PUBLIC_ART_META,
                'gallery_field' => Apollo_DB_Schema::_APL_PUBLIC_ART_IMAGE_GALLERY,
            ),
            'public_art' => array(
                'table' => "postmeta",
                'gallery_field' => Apollo_DB_Schema::_APL_SPOT_LIGHT_GALLERY,
            ),

        );

        if ($data) {
            foreach($data as $imgId) {

                foreach($checkingModules as $key => $arrVal) {
                    if ($r = $this->isUsingForGalleryImage($imgId, $arrVal['table'], $arrVal['gallery_field'], $key)) {
                        $respectResult[$imgId] = $r;
                        break;
                    }

                    // Delete img
                    //wp_delete_attachment($imgId);
                }

            }
        }
        $this->respectResult = $respectResult;
    }

    /**
     * Whether checking if this image is using for any gallery
     * @param $id
     * @param $mtTbl
     * @param $metaKey
     * @param $key
     * @return bool
     */
    private function isUsingForGalleryImage($id, $mtTbl, $metaKey, $key)
    {
        global $wpdb;
        $mtTbl = $wpdb->{$mtTbl};
        $post_key = 'apollo_'. $key. '_id';
        $sql = "
            SELECT meta_value, $post_key AS post_id FROM $mtTbl WHERE meta_key = '$metaKey' AND meta_value LIKE '%$id% GROUP BY $post_key '
        ";
        $result = $wpdb->get_results($sql, ARRAY_A);

        if (!$result || !count($result)) {
            return false;
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getRespectResult() {
        return $this->respectResult;
    }
}