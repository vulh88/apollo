<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of Apollo_Custom_Field
 *
 * @author vulh
 */
class Apollo_Admin_Tool_Custom_Field extends Apollo_Admin_Post_Types {

    function __construct() {
        add_action( 'admin_menu', array( $this, 'post_type_menu' ) );
    }
    
    function post_type_menu() {
        global $apollo_modules;

        $active_modules = Apollo_App::get_avaiable_modules();
        $active_modules[] = 'page';
        if ( $active_modules ) {
            foreach ( $active_modules as $am ) {
                
                $_ac_mods = array();
                if ( isset( $apollo_modules[$am]['childs'] ) ) $_ac_mods = $apollo_modules[$am]['childs'];
                else $_ac_mods[] = $am;

                if( $_ac_mods ):
                    foreach ( $_ac_mods as $k => $m ):
                        $mod = is_integer( $k ) ? $m :  $k;
                        if (in_array($mod, Apollo_Admin_Config::additionalFieldMods()) ) {
                            add_submenu_page("edit.php?post_type={$mod}", __( 'Additional Fields', 'apollo' ), __( 'Additional Fields', 'apollo' ), 'manage_options', "".$mod."_custom_fields", array( $this, 'apollo_custom_fields' ));
                        }
                        if( $mod == 'page' && has_action('_apl_add_additionfields_for_page')){
                            do_action('_apl_add_additionfields_for_page', $mod, array( $this, 'apollo_custom_fields' ));
                        }
                    endforeach;
                endif;
                
            }
        }
    }

    public function apollo_custom_fields() {
        
        $post_type = isset( $_GET['post_type'] ) ? $_GET['post_type'] : '';
        if ( ! $post_type ) wp_safe_redirect(admin_url() );
        ?>
        <div class="wrap apollo-artist-custom-fields">
            <ul class="apl-admin-tabs-link">
                <li <?php if(isset($_GET['field']) || (!isset($_GET['field_listing_page'])) && !isset($_GET['question']) ) echo 'class="active"'; ?>><a href="<?php echo $this->get_permalink($post_type) ?>"><?php _e( 'Groups', 'apollo' ) ?></a></li>
                
                <?php
                    if ($post_type == Apollo_DB_Schema::_EDU_EVALUATION || $post_type == Apollo_DB_Schema::_GRANT_EDUCATION) {
                ?>
                <li <?php if( isset($_GET['field_listing_page']) ) echo 'class="active"'; ?>><a href="<?php echo $this->get_permalink($post_type) ?>&field_listing_page=1"><?php _e( 'Additional Field Settings', 'apollo' ) ?></a></li>
                    <?php } ?>
                
                    <?php 
                    if ( $post_type == Apollo_DB_Schema::_ARTIST_PT ):
                ?>
                <li <?php if( isset($_GET['question']) ) echo 'class="active"'; ?>><a href="<?php echo $this->get_permalink($post_type) ?>&question=1"><?php _e( 'Questions', 'apollo' ) ?></a></li>
                <?php endif; ?>
                
            </ul>
            
            <?php
                if ( isset( $_GET['field'] ) ) {
                    $this->custom_fields_form();
                } else if ( isset( $_GET['question'] ) && $post_type == Apollo_DB_Schema::_ARTIST_PT ) {
                    $this->questions();
                } else if (isset( $_GET['field_listing_page'] ) && (
                    $post_type == Apollo_DB_Schema::_EDU_EVALUATION || $post_type == Apollo_DB_Schema::_GRANT_EDUCATION )) {
                    $this->_cf_field_listing_setting();
                } else {
                    $this->_cf_group();
                }
            ?>
        </div>
        <?php
    }
    
    private function custom_fields_form() {
        global $typenow;
        $group_name = '';
        
        $group = false;
        if ( isset($_POST['submit']) ) {
            $this->_save_cf();
            $group_name = $_POST['group_name'];
        } else {
            if ( isset( $_GET['group_id'] ) ) { 
                $apl_query = new Apl_Query( Apollo_Tables::_APL_CUSTOM_FIELD );
                $group = $apl_query->get_row(" id = ".$_GET['group_id']." " );
                $group_name = $group ? $group->label : '';
            }
            else {
                $group_name = isset( $_GET['group_name'] ) ? $_GET['group_name'] : '';
            }
        }
        
        $group_meta = Apollo_Custom_Field::get_group_meta($group);
        $displayControl = isset($group_meta['display_control']) ? $group_meta['display_control'] : '';
        ?>
        <div id="apollo-custom-notice" class="error hidden">
            <p><?php _e( 'There are some errors in your submitted data. Please check them again', 'apollo' ) ?></p>
        </div>

        <div id="aplcf-clone" class="hidden">
            <?php $this->_acf_simple_field(0, 'clone'); ?>
        </div>

        <form method="post" action="">
            <h2><?php _e( 'Manage Fields in Group', 'apollo' ) ?></h2>
            <input name="aplcf_remove_fields" value="" type="hidden" />
            <div id="post-body-content">
                <div id="titlediv">
                    <div id="titlewrap">
                        <label class="screen-reader-text" id="title-prompt-text" for="title"><?php _e( 'Enter group name', 'apollo' ) ?></label>
                        <input value="<?php echo esc_attr($group_name); ?>" type="text" id="title" name="group_name" size="30" placeholder="<?php _e( 'Group name', 'apollo' ) ?>" autocomplete="off">
                    </div>
                    <div class="inside"></div>
                    
                </div><!-- /titlediv -->
            </div>
            
            <div id="post-body-content">
                <div class="postbox aplcf">
                    <input class="new-field-label" type="hidden" value="<?php _e('New Field', 'apollo') ?>" />
                    <h3 class="hndle"><span><?php _e( 'Fields', 'apollo' ) ?></span></h3>

                    <div class="inside">
                        <div class="fields_header">
                            <table class="aplcf widefat">
                                <thead>
                                    <tr>
                                        <th class="field_order"><?php _e( 'Order', 'apollo' ) ?></th>
                                        <th class="field_label"><?php _e( 'Label', 'apollo' ) ?></th>
                                        <th class="field_required"><?php _e( 'Required', 'apollo' ) ?></th>
                                        <th class="field_type"><?php _e( 'Type', 'apollo' ) ?></th>
                                        <th class="field_display_control"><?php _e( 'Display control', 'apollo' ) ?></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        
                        <!-- Fields -->
                        <div class="fields">
                            <input type="hidden" class="post_type" name="post_type" value="<?php echo isset($_GET['post_type']) ? $_GET['post_type'] : '' ?>" />
                            <?php echo $this->_cf_each_field($displayControl) ?>
                        </div><!-- End Fields -->
                        
                        <div class="table_footer">
                            <a href="javascript:;" id="add-field" class="button button-primary button-large">+ <?php _e('Add Field', 'apollo') ?></a>
                            <br/>
                            <br/>
                        </div>
                        
                    </div>
                </div>
                
                <!-- Group Options --> 
                <div id="aplcf-options" class="postbox ">
                    <div class="handlediv" title="Click to toggle"><br></div><h3 class="hndle"><span><?php _e( 'Group Options', 'apollo' ) ?></span></h3>
                    <div class="inside ">
                        <table class="aplcf_input widefat aplcf_field_form_table" id="aplcf-options">
                            <tbody>
                                <tr>
                                    <td class="label">
                                        <label for=""><?php _e( 'Prepend html tag for label', 'apollo' ) ?></label>
                                        <p class="description"><?php _e( 'Appears before the label in the Frontend form (h4 tag by default)', 'apollo' ) ?></p>
                                    </td>
                                    <td>
                                        <div class="aplcf-input-wrap"><input value="<?php echo $group_meta && isset($group_meta['prepend']) ? $group_meta['prepend'] : '' ?>" name="group_meta[group][prepend]" type="text" class="label" /></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <label for=""><?php _e( 'Append html tag for label', 'apollo' ) ?></label>
                                        <p class="description"><?php _e( "Appears after the label in the Frontend form (close tag of h4 by default)", 'apollo' ) ?></p>
                                    </td>
                                    <td>
                                        <div class="aplcf-input-wrap"><input value="<?php echo $group_meta && isset($group_meta['append']) ? $group_meta['append'] : '' ?>" name="group_meta[group][append]" type="text" class="label" /></div>
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                </div><!-- End Group Options --> 
                
                <input data-confirm="<?php _e( 'Are you sure you want to commit your change?', 'apollo' ) ?>" id="apl-cf-save-btn" type="submit" name="submit" class="button button-primary button-large"  value="<?php _e( 'Save update', 'apollo' ) ?>">            
            </div>
            </form>
        <?php
    }
    
    private function _cf_each_field($displayControl = '') {
        $fields = array('clone');
        $post_type = isset($_GET['post_type']) ? $_GET['post_type'] : '';
        if ( isset($_GET['group_id']) && $_GET['group_id'] ) {
            $gid = $_GET['group_id'];
            $apl_query = new Apl_Query( Apollo_Tables::_APL_CUSTOM_FIELD );
           
            $results = $apl_query->get_where( "parent=$gid AND post_type = '$post_type'", '*', '', ' ORDER BY cf_order ASC');
            if ( $results ) $fields = $results;
        }
     

        if ($fields):
            foreach( $fields as $k => $v ):
                $this->_acf_simple_field($k, $v, $displayControl);
            endforeach;
        endif;
    }
    
    private function _acf_simple_field($k, $v, $displayControl = '') {
        $post_type = isset($_GET['post_type']) ? $_GET['post_type'] : '';
        $hide_meta = true;
        if ( $v == 'clone' ) {
            $key = 'field_0';
            $type = 'text';
            $label = __('New Field', 'apollo');
            $name = '';
            $order = 0;
            $meta = '';
            $required = 0;
            $hide_meta = false;
        } else {
            $key = 'field_'. $k;
            $type = $v->cf_type;
            $label = Apollo_App::clean_data_request(esc_attr($v->label));
            $name = $v->name;
            $order = $v->cf_order;
            $meta = maybe_unserialize( $v->meta_data );
            $required = $v->required ? $v->required : 0;
        }

        $orderField = isset($v->cf_order) && $v->cf_order ? $v->cf_order : 0;
        $current_type       = isset($v->cf_type) ? $v->cf_type : 'text';
        /*@ticket #18221: 0002504: Arts Education Customizations - Add a checkbox option for "Add Menu Arrow" - item 1*/
        $addMenuArrow = isset($meta['add_menu_arrow']) ? $meta['add_menu_arrow'] : 0;
        $current_location   = maybe_unserialize( isset($v->location) ? $v->location : '' );
        $types              = Apollo_Custom_Field::types();
        $displayControlChecked = (!empty($displayControl) && isset($v->name) && $displayControl == $v->name) ? 'checked' : '';
        $hiddenDisplayControl = $type == 'checkbox' ? '' : 'checkbox-hidden';
        
        ?>
        <div class="field form_open" data-type="text" data-id="<?php echo $key ?>">
            <div class="field_meta">
                <input type="hidden" class="id" name="fields[<?php echo $key ?>][id]" value="<?php echo isset($v->id) ? $v->id : '' ?>" />
                <table class="aplcf widefat">
                    <tbody><tr>
                        <td class="field_order"><span class="circle"><?php echo $k+1 ?></span></td>
                        <td class="field_label">
                            <strong>
                                <a class="aplcf_edit_field row-title" title="Edit this Field" href="javascript:;"><?php echo $label ?></a>
                            </strong>
                            <div class="row_options">
                                <span><a class="aplcf_edit_field" title="<?php _e('Edit this Field', 'apollo') ?>" href="javascript:;"><?php _e('Edit', 'apollo') ?></a> | </span>
                                <span><a data-confirm="<?php _e("Are you sure to remove this field? All the relative data will be deleted", 'apollo') ?>" class="aplcf_delete_field" title="<?php _e('Delete this Field', 'apollo') ?>" href="javascript:;"><?php _e('Delete', 'apollo') ?></a></span>
                            </div>
                        </td>
                        <td class="field_required"><?php $required == 1 ? _e( 'Yes', 'apollo' ) : _e( 'No', 'apollo' ) ?></td>
                        <td class="field_type"><?php echo isset( $types[$type] ) ? $types[$type] : ''; ?></td>
                        <td class="field_display_control">
                            <input type="checkbox"
                                   class="<?php echo $hiddenDisplayControl; ?> apl-cf-display-control"
                                   name="fields[<?php echo $key ?>][display_control]"
                                   value="<?php echo !empty($name) ? $name : $key; ?>" <?php echo $displayControlChecked; ?>
                            />
                        </td>
                    </tr>
                </tbody></table>
            </div>
            
            <div class="field_form_mask <?php echo $hide_meta ? 'hidden' : '' ?>">
                <div class="field_form">
                    <table class="aplcf_input widefat aplcf_field_form_table">
                        <tbody>
                            
                        <?php
                            $this->_render_label_field($key, $label);
                        /*@ticket #18221: 0002504: Arts Education Customizations - Add a checkbox option for "Add Menu Arrow" - item 1*/
                            $this->_render_menu_arrow($key, $current_type, $addMenuArrow);
                            $this->_render_order_field($key,  $current_type, $orderField);
                            $this->_render_type_field($key, $current_type);
                            $this->_render_data_manual($key, $current_type, $meta);
                            $this->_render_data_source($key, $current_type, $post_type, $meta);
                            $this->_render_choice_option_field($key, $current_type, $meta);
                            if ($post_type == Apollo_DB_Schema::_EVENT_PT) {
                                $this->_render_alternate_values_field($key, $current_type, $meta);
                            }
                            $this->_render_refer_to($key, $type, $post_type, $name, $meta);
                            
                            // for select box
                            $this->_render_display_style_option_field($key, $type, $meta);   
                            
                            $this->_render_location_field($key, $type, $post_type, $current_location);
                            $this->_render_required_field($key, $required);
                            $this->_render_validation_option_field($key, $type, $meta);
                
                            $this->_render_default_value_fields($key, $current_type, $meta); 
                            $this->_render_explain_option_field($key, $current_type, $meta);
                            $this->_render_character_limit_option_field($key, $current_type, $meta);
                            $this->_render_desc_field($key, $type, $meta);
                        ?>
                            
                        </tbody>
                    </table>
                </div>
            </div> <!-- End field_form_mask -->    
        </div>
        <?php
    }
    
    private function _render_label_field($key, $default_label) {
        ?>
        <tr class="field_label">
            <td class="label">
                <label><?php _e('Label', 'apollo') ?><span class="required">*</span></label>
                <p class="description"><?php _e( 'This is the name which will appear on the page', 'apollo' ) ?></p>
            </td>
            <td>
                <div class="aplcf-input-wrap">
                    <input type="text" class="label" data-required name="fields[<?php echo $key ?>][label]" value="<?php echo $default_label; ?>" placeholder="">
                </div>
            </td>
        </tr>
        <?php
    }

    /**
     * *@ticket #18221: 0002504: Arts Education Customizations - Add a checkbox option for "Add Menu Arrow" - item 1
     * @param $key
     * @param $currentType
     * @param $value
     */
    private function _render_menu_arrow($key, $currentType, $value) {

        $hidden = $currentType == "multi_checkbox" ? '' : 'hidden';
        ?>
        <tr class="field_label <?php echo $hidden?> apl-custom-field-menu-arrow">
            <td class="add-menu-arrow">
                <label><?php _e('Add menu arrow', 'apollo') ?></label>
            </td>
            <td>
                <div class="aplcf-input-wrap">
                    <input type="checkbox" class="add-menu-arrow"  name="fields[<?php echo $key ?>][add_menu_arrow]" <?php echo $value ? 'checked' : ''?> >
                    <label><?php _e('Add menu arrow', 'apollo') ?></label>
                </div>
            </td>
        </tr>
        <?php
    }

    private function _render_order_field($key, $type, $value = 0 ) {
        ?>
        <tr class="field_option field_option_gallery" id="order-field" <?php if ( $type != 'gallery' ) echo 'style="display: none;"' ?>>
            <td class="order">
                <label><?php _e('Order', 'apollo') ?></label>
                <p class="description"><?php _e( 'This is the order to appear on the page ', 'apollo' ) ?></p>
            </td>
            <td>
                <div class="aplcf-input-wrap">
                    <input type="text" class="order"  name="fields[<?php echo $key ?>][order]" value="<?php echo $value; ?>" placeholder="">
                </div>
            </td>
        </tr>
        <?php
    }


    
    private function _render_validation_option_field($key, $type, $meta) {
        $validations = array(
            'email'    => __( 'Email', 'apollo' ),
            'number'   => __( 'Number', 'apollo' ),
            'url'      => __( 'Url', 'apollo' ),
            'datepicker'      => __( 'Datepicker', 'apollo' ),
        );
        $validation_types = array('text');
        
        $current_value = $meta && isset($meta['validation']) ? (string) $meta['validation'] : '';
        
        foreach( $validation_types as $_v_type ):
        ?>
        <tr class="field_option field_option_<?php echo $_v_type; ?>" <?php if ( $type != $_v_type ) echo 'style="display: none;"' ?>>
            <td class="label">
                <label><?php _e( 'Validation', 'apollo' ) ?> </label>
            </td>
            <td>
                <select class="<?php echo $_v_type ?> field_option-validation" name="fields[<?php echo $key ?>][meta_<?php echo $_v_type ?>][validation]">
                    <option value="0"><?php _e('--Select--', 'apollo') ?></option>
                    <?php
                    foreach ( $validations as $_k => $_v ): ?>
                        <option value="<?php echo $_k ?>" <?php selected( $_k, $current_value ) ?> ><?php echo $_v ?></option>
                    <?php endforeach; ?>
                </select>
                
            </td>
        </tr>
        <?php
        endforeach;
    }
    
    
    private function _render_display_style_option_field($key, $type, $meta) {
        
        $types = array(
            'select' => array(
                'full_width'    => __( 'Full width (Default)', 'apollo' ),
                'right_label'    => __( 'On the right of label', 'apollo' ),
            ),
            'textarea' => array(
                'has_label'    => __( 'Full width with the label (Default)', 'apollo' ),
                'no_label'    => __( 'Full width with no label', 'apollo' ),
            ),
        );
        $current_value = $meta && isset($meta['display_style']) ? (string) $meta['display_style'] : 'full_width';
        
        foreach( $types as $_v_type => $display_type ):
        ?>
        <tr class="field_option field_option_<?php echo $_v_type; ?>" <?php if ( $type != $_v_type ) echo 'style="display: none;"' ?>>
            <td class="label">
                <label><?php _e( 'Display style', 'apollo' ) ?> </label>
            </td>
            <td>
                <select class="<?php echo $_v_type ?> field_option-display_style" name="fields[<?php echo $key ?>][meta_<?php echo $_v_type ?>][display_style]">
                    <?php
                    foreach ( $display_type as $_k => $_v ): ?>
                        <option value="<?php echo $_k ?>" <?php selected( $_k, $current_value ) ?> ><?php echo $_v ?></option>
                    <?php endforeach; ?>
                </select>
                
            </td>
        </tr>
        <?php
        endforeach;
    }
    
    private function _render_required_field($key, $required) {
        ?>
        <tr class="required">
            <td class="label"><label><?php _e( 'Required', 'apollo' ) ?></label></td>
            <td>
                <ul class="aplcf-radio-list radio horizontal">
                    <li>
                        <label><input class="required" type="radio" name="fields[<?php echo $key ?>][required]" <?php echo $required == 1 ? 'checked' : '' ?> value="1"><?php _e( 'Yes', 'apollo' ) ?></label></li>
                    <li>
                        <label><input class="required" type="radio" name="fields[<?php echo $key ?>][required]" <?php echo $required == 0 ? 'checked' : '' ?> value="0" ><?php _e( 'No', 'apollo' ) ?></label>
                    </li>
                </ul>							
            </td>
        </tr>
        <?php
    }
    
    private function _render_location_field($key, $type, $post_type, $current_location) {

        $locations = array(
            'dp'      => __( 'Detail Page', 'apollo' ),
            'ff'    => __( 'Front end Form', 'apollo' ),
            'rsb'  => __( 'Right Sidebar', 'apollo' ),
            'bf'  => __( 'Backend Form', 'apollo' )
        );

        /** @Ticket #13237 */
        $allowSearchWidgetMods = array(
            Apollo_DB_Schema::_BUSINESS_PT,
            Apollo_DB_Schema::_ARTIST_PT
        );
        if (in_array($post_type, $allowSearchWidgetMods)) {
            $locations['sw']  = __( 'Search Widget', 'apollo' );
        }
        
        if (in_array( $post_type , array(
            Apollo_DB_Schema::_GRANT_EDUCATION, 
            Apollo_DB_Schema::_EDU_EVALUATION ) 
        ) ) {
            unset($locations['dp']);
        }

        if ( $post_type != Apollo_DB_Schema::_EDUCATOR_PT ) {
            unset($locations['rsb']);
        }

        /**
         * Allow display on right sidebar #13823
         */
        $searchWidgetTypes = array(
            'select', 'checkbox', 'multi_checkbox'
        );

        ?>
        <tr class="field_location">
            <td class="label">
                <label><?php _e( 'Location', 'apollo' ) ?> </label>
            </td>
            <td>
                <ul>
                    <?php
                    foreach ( $locations as $_k => $_v ): ?>
                        <li <?php echo ($type != 'checkbox' && $_k == 'rsb') || ($type == 'gallery' && $_k != 'bf') || ( $type != 'gallery' && $_k == 'bf') || ( !in_array($type, $searchWidgetTypes) && $_k == 'sw') ? 'style="display: none"':'' ?>>
                            <input <?php echo $_k == 'sw' ? 'data-sw="'.implode(',', $searchWidgetTypes).'"' : '' ?> class="location" value="<?php echo $_k ?>" <?php echo is_array( $current_location ) && in_array( $_k , $current_location ) ? 'checked' : '' ?>
                                   <?php echo $_k == 'bf' ? 'checked disabled' : '' ?> type="checkbox" name="fields[<?php echo $key ?>][location][]" >
                            <label onclick="checkCBBefore(this)"><?php echo $_v ?></label>
                        </li>
                    <?php endforeach; ?>
                </ul>

            </td>
        </tr>
        <?php
    }
    
    private function _render_type_field($key, $current_type) {
        $types = Apollo_Custom_Field::types();
        ?>
        <tr class="field_type">
            <td class="label">
                <label><?php _e( 'Type', 'apollo' ) ?><span class="required">*</span></label>
            </td>
            <td>
                <select  class="type" name="fields[<?php echo $key ?>][type]">
                    <?php
                    foreach ( $types as $_k => $_v ): ?>
                        <option value="<?php echo $_k ?>" <?php selected( $_k, $current_type ) ?> ><?php echo $_v ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
        </tr>
        <?php
    }
    
    private function _render_desc_field($key, $type, $meta) {
        ?>
        <tr class="field_desc">
            <td class="label">
                <label><?php _e('Description', 'apollo') ?></label>
            </td>
            <td>
                <div class="aplcf-input-wrap">
                    <textarea class="<?php echo $type ?> field_option-desc" name="fields[<?php echo $key ?>][meta_<?php echo $type ?>][desc]" placeholder="" rows="8"><?php if(isset($meta['desc'])) echo $meta['desc'] ?></textarea>
                </div>
            </td>
        </tr>
        <?php
    }
    
    private function _render_data_manual($key, $current_type, $meta = '') {
        
        $currentManual = 'choice';
        if ( $meta && isset($meta['data_manual']) && $meta['data_manual'] ) {
            $currentManual = $meta['data_manual'];
        }
       
        $dataSourceTypes = array( 'select' );
     
        foreach($dataSourceTypes as $dataSourceType):
        ?>
        <tr class="field_option field_option_<?php echo $dataSourceType ?> <?php if ( $current_type != $dataSourceType ) echo 'hidden' ?>">
            <td class="label">
                <label for=""><?php _e( 'Data Option', 'apollo' ) ?></label>
            </td>
            <td>
                <select class="<?php echo $dataSourceType ?> field_option-data_manual" name="fields[<?php echo $key ?>][meta_<?php echo $dataSourceType ?>][data_manual]">
                    <option value=""><?php _e( '--Select--', 'apollo' ) ?></option>
                    <option <?php echo $currentManual == 'choice' ? 'selected' : '' ?> 
                        value="choice"><?php _e( 'Choice', 'apollo' ) ?></option>
                    <option <?php echo $currentManual == 'source' ? 'selected' : '' ?> 
                        value="source"><?php _e( 'Data source', 'apollo' ) ?></option>
                </select>
            </td>
        </tr>
        <?php endforeach;
    }
    
    private function _render_data_source($key, $current_type, $post_type, $meta = '') {
      
        $manualData = $meta && isset( $meta['data_manual'] ) ? $meta['data_manual'] : 'choice';
        
        $avaiableModules = Apollo_App::get_avaiable_modules();
        $dataSourceTypes = array( 'select'  );
        
        if ( $post_type == Apollo_DB_Schema::_EDU_EVALUATION ) {
            $avaiableModules[] = 'district';
            $avaiableModules[] = 'school';
        }
        
        foreach($dataSourceTypes as $dataSourceType):
        ?>
        <tr class="field_option field_option_<?php echo $dataSourceType ?> <?php if ( $current_type != $dataSourceType || $manualData != 'source' ) echo 'hidden' ?>">
            <td class="label">
                <label for=""><?php _e( 'Data Source', 'apollo' ) ?></label>
                <p><?php _e( 'Get dropdown data from the datasouce', 'apollo' ); ?></p>
            </td>
            <td>
                <select class="<?php echo $dataSourceType ?> field_option-data_source" name="fields[<?php echo $key ?>][meta_<?php echo $dataSourceType ?>][data_source]">
                    <option value=""><?php _e( '--Select--', 'apollo' ) ?></option>
                    <?php
                        $this->_renderDataSourceOptions($avaiableModules, $post_type, $meta);
                    ?>
                </select>
            </td>
        </tr>
        <?php endforeach;
    }
    
    private function _render_refer_to($key, $current_type, $post_type, $name,$meta = '') {
        
        $types = array( 'select' );
        $selectFields = Apollo_Custom_Field::get_fields_by_type( $post_type,'select');
        $manualData = $meta && isset( $meta['data_manual'] ) ? $meta['data_manual'] : 'choice';
        if ( ! $selectFields ) return;
        
        foreach($types as $type):
        ?>
        <tr class="field_option field_option_<?php echo $type ?> <?php if ( $current_type != $type || $manualData != 'source' ) echo 'hidden' ?>">
            <td class="label">
                <label for=""><?php _e( 'Reference to', 'apollo' ) ?></label>
                <p><?php _e( 'Auto fill data to reference field when select current one', 'apollo' ); ?></p>
            </td>
            <td>
                <select class="<?php echo $type ?> field_option-refer_to" name="fields[<?php echo $key ?>][meta_<?php echo $type ?>][refer_to]">
                    <option value=""><?php _e( '--Select--', 'apollo' ) ?></option>
                    <?php
                        foreach( $selectFields as $selectField ) {
                            if ( $selectField->name == $name ) continue;
                            echo '<option '.($meta && isset( $meta['refer_to'] ) && $meta['refer_to'] == $selectField->name ? "selected" : "").' value="'.$selectField->name.'">'.$selectField->label.'</option>';
                        }
                    ?>
                </select>
            </td>
        </tr>
        <?php endforeach;
    }
    
    private function _renderDataSourceOptions($avaiableModules, $post_type, $meta, $modules_data = '') {
        
        global $apollo_modules;
        
        if ( ! $modules_data ) $modules_data = $apollo_modules;
      
        if ( $post_type == Apollo_DB_Schema::_EDU_EVALUATION ) {
            
            $modules_data['district'] = array(
                'sing'  => __('District', 'apollo')
            );
            
            $modules_data['school'] = array(
                'sing'  => __('School', 'apollo')
            );
        }
        
        foreach ($avaiableModules as $m) {
            
            if ( $m == 'post' ) continue;
            
            if ( isset( $modules_data[$m]['childs'] ) && $modules_data[$m]['childs'] ) {
                $this->_renderDataSourceOptions (array_keys($modules_data[$m]['childs']), $post_type, $meta, $modules_data[$m]['childs']);
            } else {
                echo '<option '.(isset($meta['data_source']) && $meta['data_source'] == $m ? "selected" : "").' value="'.$m.'">'.$modules_data[$m]['sing'].'</option>';
            }
        }
        
    }

    /**
     * Only apply for Event module.
     * @Ticket #14076
     * @param $key
     * @param $current_type
     * @param string $meta
     */
    private function _render_alternate_values_field($key, $current_type, $meta = '') {
        $choice_types = array( 'multi_checkbox', 'select');

        $manualData = $meta && isset( $meta['data_manual'] ) ? $meta['data_manual'] : 'choice';

        foreach($choice_types as $choice):

            ?>
            <tr class="field_option field_option_<?php echo $choice ?>
            <?php if ( $current_type != $choice || ($manualData != 'choice' && $choice == 'select') ) echo 'hidden' ?>">
                <td class="label">
                    <label for=""><?php _e( 'Alternate values', 'apollo' ) ?></label>
                    <p>
                        <?php _e( 'Enter each alternate value on a new line its value should be the same to the bellow "Choices" field.', 'apollo' ) ?><br>
                        <?php _e( 'This field will be displayed on the syndication XML feed', 'apollo' );?>
                    </p>
                    <p><?php _e( 'key1', 'apollo' ) ?> : <?php _e( 'Alternate Value 1', 'apollo' ) ?><br><?php _e( 'key2', 'apollo' ) ?> : <?php _e( 'Alternate Value 2', 'apollo' ) ?></p>
                </td>
                <td>
                    <textarea class="<?php echo $choice ?> field_option-choice syn_alternate" name="fields[<?php echo $key ?>][meta_<?php echo $choice ?>][alternate_values]" placeholder="" rows="8"><?php if(isset($meta['alternate_values'])) echo $meta['alternate_values'] ?></textarea>
                </td>
            </tr>
        <?php endforeach;
    }
    
    private function _render_choice_option_field($key, $current_type, $meta = '') {
        $choice_types = array( 'multi_checkbox', 'select', 'radio' );
        
        $manualData = $meta && isset( $meta['data_manual'] ) ? $meta['data_manual'] : 'choice';
        
        foreach($choice_types as $choice):
        ?>
        <tr class="field_option field_option_<?php echo $choice ?> 
            <?php if ( $current_type != $choice || ($manualData != 'choice' && $choice == 'select') ) echo 'hidden' ?>">
            <td class="label">
                <label for=""><?php _e( 'Choices', 'apollo' ) ?></label>
                <p><?php _e( 'Enter each choice on a new line', 'apollo' ) ?></p>
                <p><?php _e( 'key1', 'apollo' ) ?> : <?php _e( 'Value 1', 'apollo' ) ?><br><?php _e( 'key2', 'apollo' ) ?> : <?php _e( 'Value 2', 'apollo' ) ?></p>
            </td>
            <td>
                <textarea class="<?php echo $choice ?> field_option-choice" name="fields[<?php echo $key ?>][meta_<?php echo $choice ?>][choice]" placeholder="" rows="8"><?php if(isset($meta['choice'])) echo $meta['choice'] ?></textarea>	
                <?php 
                if ( $choice == 'radio' ) {
                ?>
                <div class="<?php echo $choice ?> field_option-other_choice">
                    <ul class="acf-checkbox-list true_false">
                        <li>
                            <label><input 
                                    <?php echo $meta && isset($meta['other_choice']) && $meta['other_choice'] == 1 ? 'checked="checked"' : '' ?> class="<?php echo $choice ?> field_option-other_choice" 
                                    type="checkbox" name="fields[<?php echo $key ?>][meta_radio][other_choice]" 
                                    value="1"><?php _e("Add 'other' choice to allow for custom values", 'apollo') ?></label>
                        </li>
                    </ul>				
                </div>
                <?php } ?>
            </td>
        </tr>
        <?php endforeach;
    }
    
    private function _render_character_limit_option_field($key, $current_type, $meta = '') {
        $limitTypes = array( 'text', 'textarea' );
        
        foreach($limitTypes as $limitType):
        ?>
        <tr class="field_option field_option_<?php echo $limitType ?> <?php if ( $current_type != $limitType ) echo 'hidden' ?>">
            <td class="label">
                <label for=""><?php _e( 'Character Limit', 'apollo' ) ?></label>
            </td>
            <td>
                <input class="<?php echo $limitType ?> field_option-character_limit" name="fields[<?php echo $key ?>][meta_<?php echo $limitType ?>][character_limit]" placeholder="" value="<?php if(isset($meta['character_limit'])) echo $meta['character_limit'] ?>" type="number" />	
            </td>
        </tr>
        <?php endforeach;
    }
    
    private function _render_explain_option_field($key, $current_type, $meta = '') {
        $explain_fields = array( 'select' );
        foreach($explain_fields as $explain_field ):
        ?>
        <tr class="field_option field_option_<?php echo $explain_field ?> <?php if ( $current_type != $explain_field ) echo 'hidden' ?>">
            <td class="label">
                <label for=""><?php _e( 'Has Explain Textarea', 'apollo' ) ?></label>
                <p><?php _e( 'If you want to allow the user explain about the answer', 'apollo' ) ?></p>
            </td>
            <td>
                <input class="<?php echo $explain_field ?> field_option-explain" <?php echo $meta && isset($meta['explain']) && $meta['explain'] == 1 ? 'checked' : '' ?> type="checkbox" 
                        name="fields[<?php echo $key ?>][meta_<?php echo $explain_field ?>][explain]" value="1" />
            </td>
        </tr>
        <?php endforeach;
    }
    
    private function _render_listing_page_option_field($key, $current_type, $meta = '') {
        $explain_fields = array( 'text', '' );
        foreach($explain_fields as $explain_field ):
        ?>
        <tr class="field_option field_option_<?php echo $explain_field ?> <?php if ( $current_type != $explain_field ) echo 'hidden' ?>">
            <td class="label">
                <label for=""><?php _e( 'Has Explain Textarea', 'apollo' ) ?></label>
                <p><?php _e( 'If you want to allow the user explain about the answer', 'apollo' ) ?></p>
            </td>
            <td>
                <input class="<?php echo $explain_field ?> field_option-explain" <?php echo $meta && isset($meta['explain']) && $meta['explain'] == 1 ? 'checked' : '' ?> type="checkbox" 
                        name="fields[<?php echo $key ?>][meta_<?php echo $explain_field ?>][explain]" value="1" />
            </td>
        </tr>
        <?php endforeach;
    }
    
    private function _render_search_autocomplete_option_field($key, $current_type, $meta = '') {
        $explain_fields = array( 'select' );
        foreach($explain_fields as $explain_field ):
        ?>
        <tr class="field_option field_option_<?php echo $explain_field ?> <?php if ( $current_type != $explain_field ) echo 'hidden' ?>">
            <td class="label">
                <label for=""><?php _e( 'Allow Autocomplete Search', 'apollo' ) ?></label>
            </td>
            <td>
                <input class="<?php echo $explain_field ?> field_option-autocomplete" <?php echo $meta && isset($meta['autocomplete']) && $meta['autocomplete'] == 1 ? 'checked' : '' ?> type="checkbox" 
                        name="fields[<?php echo $key ?>][meta_<?php echo $explain_field ?>][autocomplete]" value="1" />
            </td>
        </tr>
        <?php endforeach;
    }
    
    private function _render_default_value_fields($key, $current_type, $meta) {
        return false;
            $default_value_types = array( 'text', 'textarea', 'radio', 'checkbox', 'multi_checkbox', 'select' );
            foreach( $default_value_types as $_v_type ):
        ?>
        <tr class="hidden field_option field_option_<?php echo $_v_type ?>  <?php if ( $current_type != $_v_type ) echo 'hidden' ?>" >
            <td class="label">
                <label><?php _e( 'Default Value', 'apollo' ) ?></label>
            </td>
            <td>
                <div class="acf-input-wrap">
                    <?php
                        switch($_v_type):
                            case 'text':
                            case 'radio':
                            case 'select':    
                            ?>
                                <input type="text" id="" class="<?php echo $_v_type ?> field_option-default-value" 
                                name="fields[<?php echo $key ?>][meta_<?php echo $_v_type ?>][default_value]" 
                                value="<?php echo isset( $meta['default_value'] ) ? $meta['default_value'] : '' ?>" placeholder="">
                            <?php
                            break;
                        
                            case 'textarea':
                            case 'multi_checkbox':
                            ?>
                                <textarea id="" class="<?php echo $_v_type ?> field_option-default-value" 
                                name="fields[<?php echo $key ?>][meta_<?php echo $_v_type ?>][default_value]" 
                                placeholder=""><?php echo isset( $meta['default_value'] ) ? $meta['default_value'] : '' ?></textarea>
                                    
                            <?php
                            break;
                            
                            case 'checkbox':
                            ?>
                                 <input value="yes" type="checkbox" id="" class="<?php echo $_v_type ?> field_option-default-value" 
                                name="fields[<?php echo $key ?>][meta_<?php echo $_v_type ?>][default_value]" 
                                <?php echo checked( 'yes', isset($meta['default_value']) ? $meta['default_value'] : '' ) ?> placeholder="">
                                    
                            <?php
                            break;
                            
                        endswitch;
                    ?>
                </div>	
            </td>
        </tr>
        <?php endforeach;
    }
    
    private function _cf_group() {
        if ( !isset( $_GET['post_type'] ) || ! $_GET['post_type'] ) {
            wp_safe_redirect (admin_url () );
        }
        $post_type = $_GET['post_type'];
        
        $apl_query = new Apl_Query( Apollo_Tables::_APL_CUSTOM_FIELD );
        
        if ( isset( $_GET['del_id'] ) && $_GET['del_id'] ) {
           
            $apl_query->delete( "id = ".$_GET['del_id']." " );
            $apl_query->delete( "parent = ".$_GET['del_id']." " );
        }
        
        $groups = $apl_query->get_where('parent IS NULL AND post_type = "'.$post_type.'" ', '*', '', 'ORDER BY cf_order ASC');
        
        $permalink = $this->get_permalink( $post_type );
        
        if ( isset( $_POST['submit'] ) ) {
            $group_name = urlencode( $_POST['group_name'] );    
            wp_safe_redirect( $permalink. '&field=1&add_new=1&group_name='. $group_name);
        }

        ?>
        <form method="post" action="" >
            <div id="apl-cf-group" class="wrap clear"><style>input[type='text'] { width:200px; padding:4px; } </style>
                <input type="hidden" id="artsopolis-calendar-selected-events" name="artsopolis-calendar-selected-events" value="" />
                <fieldset>
                    <legend><h2><?php _e( 'Manage Group', 'apollo' ) ?></h2></legend>
                    <div class="group-wraper">
                        <input type="text" name="group_name" placeholder="<?php _e( 'Group name', 'apollo' ) ?>" value="" />
                        <input type="submit" name="submit" id="submit" class="button button-primary" value="<?php _e( 'Add New Group', 'apollo' ) ?>">
                    </div>
                    <hr/>

                    <table class="apl-admin-datatable" cellpadding="0" cellspacing="0" border="0" class="display" id="tbl-apl-custom-field" width="100%">
                        <thead>
                            <tr>
                                <th class="hidden" style="width: 100px;"><?php _e( 'Order', 'apollo' ) ?></th>
                                <th style="text-align: left;"><?php _e( 'Title', 'apollo' ) ?></th>
                                <th style="width: 140px;"></th>
                                <th style="width: 140px;"></th>
                                <th class="hidden"></th>    
                            </tr>
                        </thead>
                        <?php
                        if ( $groups ):
                            foreach ( $groups as $group ):
                        ?>
                                <tr class="order odd gradeX">
                                    <td class="hidden"><?php echo $group->cf_order + 1 ?></td>
                                    <td><a href="<?php echo $permalink. '&field=1&group_id='. $group->id.'&group_name='. $group->label ?>"><?php echo $group->label ? $group->label :  __('No name', 'apollo') ?></a></td>
                                    <td><a href="<?php echo $permalink. '&field=1&group_id='. $group->id.'&group_name='. $group->label ?>"><?php _e('Edit', 'apollo') ?></a></td>
                                    <td><a onclick="return confirm('<?php _e('Are you sure to remove this group? All the fields and the relative data will be deleted', 'apollo') ?>');" href="<?php echo $permalink ?>&del_id=<?php echo $group->id ?>"><?php _e( 'Remove', 'apollo' ) ?></a></td>
                                    <td class="hidden"><input class="id" type="hidden" value="<?php echo $group->id ?>" /></td>
                                </tr>
                        <?php 
                            endforeach;
                        endif; 
                        ?>
                        <tbody></tbody>        

                    </table>
                </fieldset>
            </div>
        </form>
        <?php
    }
    
    private function _cf_field_listing_setting() {
        
        $post_type = isset($_GET['post_type']) && $_GET['post_type'] ? $_GET['post_type'] : '';
        $groupFields = Apollo_Custom_Field::get_group_fields($post_type);
  
        $keyField = 'apl_field_listing_'. $post_type;
        $moreField = 'apl_field_more_'. $post_type;
        if (isset($_POST['submit']) && strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
            update_option($keyField, $_POST['field_listing'], 'no');
            update_option($moreField, $_POST['more_fields'], 'no');
            $current = $_POST['field_listing'];
            $currentMore = $_POST['more_fields'];
        } else {
            $current = get_option($keyField);
            $currentMore = get_option($moreField);
        }
      
        ?>
        <form method="post" action="" >
            <div class="wrap clear"><style>input[type='text'] { width:200px; padding:4px; } </style>
                <div class="postbox apl-custom-postbox">
                    
                    <div class="row">
                        <h3 class="hndle"><span><?php _e('Field as Main Title Listing Page', 'apollo') ?></span></h3>
                        <select name="field_listing">
                            <option value=''>--<?php _e('Select Field', 'apollo') ?>--</option>
                            <?php
                                if ( $groupFields ) {
                                    foreach($groupFields as $gf) {

                                            if ( $gf['fields'] ):
                                            ?>   
                                                <optgroup label="<?php echo $gf['group']->label ?>">
                                            <?php
                                                foreach( $gf['fields'] as $field ):
                                                    if ( ! in_array($field->cf_type, array('text', 'textarea', 'select')) ) continue;
                                            ?>
                                                    <option <?php selected($field->id, $current) ?> value="<?php echo $field->id ?>"><?php echo $field->label ?></option>
                                            <?php 
                                                endforeach;
                                            endif; ?>
                                                </optgroup>    
                                        <?php
                                    }
                                }
                            ?>
                        </select>
                        <i class="aplcf-note"><?php _e( 'Only allow text, textarea, select', 'apollo' ) ?></i>
                    </div>
                    
                    <div class="row">
                        <h3 class="hndle"><span><?php _e('Add more columns', 'apollo') ?></span></h3>
                        <select multiple="true" name="more_fields[]">
                            <option value=''>--<?php _e('Select Field', 'apollo') ?>--</option>
                            <?php
                                if ( $groupFields ) {
                                    foreach($groupFields as $gf) {

                                            if ( $gf['fields'] ):
                                            ?>   
                                                <optgroup label="<?php echo $gf['group']->label ?>">
                                            <?php
                                                foreach( $gf['fields'] as $field ):
                                                    if ( ! in_array($field->cf_type, array('text', 'textarea', 'select')) ) continue;
                                            ?>
                                                    <option <?php echo $currentMore && in_array($field->id, $currentMore) ? 'selected' : '' ?> value="<?php echo $field->id ?>"><?php echo $field->label ?></option>
                                            <?php 
                                                endforeach;
                                            endif; ?>
                                                </optgroup>    
                                        <?php
                                    }
                                }
                            ?>
                        </select>
                        <i class="aplcf-note"><?php _e( 'Only allow text, textarea, select', 'apollo' ) ?></i>
                    </div>
                </div>
                
                
                <input type="submit" name="submit" 
                       class="button button-primary button-large" value="<?php _e('Save Update', 'apollo') ?>">
            </div>
        </form>
        <?php
    }
    
    private function _save_cf() {
        global $wpdb;
        $group_label = Apollo_App::clean_data_request($_POST['group_name']);
        $wpdb->escape_by_ref($label);
        $apl_query = new Apl_Query( Apollo_Tables::_APL_CUSTOM_FIELD );
        $post_type = isset( $_GET['post_type'] ) ? $_GET['post_type'] : '';
        
        if ( ! $post_type ) return false;
        
        /**
         * Save group
         */
        $group_meta = isset($_POST['group_meta']) ? $_POST['group_meta'] : '';
     
        $group_meta['group']['append'] = htmlspecialchars($group_meta['group']['append']);
        $group_meta['group']['prepend'] = htmlspecialchars($group_meta['group']['prepend']);
       
        $group_data = array(
            'label'     => $group_label,
            'post_type' => $post_type,
            'meta_data' => maybe_serialize($group_meta),
        );
        if ( isset($_GET['group_id']) && $_GET['group_id'] ) {

            $gid = $_GET['group_id'];
            $wpdb->escape_by_ref( $gid );
            
            $apl_query->update( $group_data , array( 'id' => $gid ) );
            
        } else {
            
            $apl_query->insert( $group_data );
            $gid = $apl_query->get_bottom();
        }
        
        if ( ! $gid ) return false;
        
        /**
         * Save fields
         */
        $fields = $_POST['fields'];
        
        $deleteFields = explode(',', $_POST['aplcf_remove_fields']);
        if ( $deleteFields ) {
            foreach ( $deleteFields as $deleteField ) {
                if ( !$deleteField ) continue;
                $apl_query->delete( "id=".trim($deleteField)."" );
            }
        }
        
        $order_index = 0;
        $displayControlField = '';
        foreach( $fields as $k => $f ) {
           
            $id             = $f['id'];
            $wpdb->escape_by_ref( $id );
            
            $label      = Apollo_App::clean_data_request($f['label']);
            if ( ! $label ) continue;
            $name       = 'cf_field_'. time(). Apollo_App::generateRandomString(5);
            if( $f['type'] == 'gallery'){
                $name = 'cf_gallery_field_'.current_time('timestamp');
                $order_index = !empty($f['order']) ?  $f['order'] : 0 ;
            }
            $type       = $f['type'];
            $required   = $f['required'];
            $location   = $f['location'];
            $meta       = isset( $f['meta_'.$type] ) ? $f['meta_'.$type] : '';

            /*@ticket #18221: 0002504: Arts Education Customizations - Add a checkbox option for "Add Menu Arrow" - item 1*/
            if($type === 'multi_checkbox'){
                $meta['add_menu_arrow']  = isset($f['add_menu_arrow']) && $f['add_menu_arrow'] == 'on' ? 1 : 0;
            }

            $wpdb->escape_by_ref( $type );
            $wpdb->escape_by_ref( $required );
            
            $location = $location ? maybe_serialize( $wpdb->escape( $location ) ) : '';

            /** @Ticket #14076 - Syndication field name */
            if ($post_type == Apollo_DB_Schema::_EVENT_PT) {
                $syn_field_name = strtolower(trim($label));
                $syn_field_name = preg_replace('/[^a-z0-9-]/', '_', $syn_field_name);
                $meta['syn_field_name'] = $syn_field_name;
            }

            $meta     = $meta ? maybe_serialize( Apollo_App::clean_array_data($meta) ) : '';
            $data = array(
                'label'     => $label,
                'parent'    => $gid,
                'cf_order'  => $order_index,
                'name'      => $name,
                'post_type' => $post_type,
                'cf_type'   => $type,
                'required'  => $required,
                'location'  => $location,
                'meta_data' => $meta,
            );
            $order_index++;
            if ( $id ) {
                unset($data['name']);
                $apl_query->update( $data, array( 'id' => $id ) );
            } else {
                $apl_query->insert( $data );
            }
            if (!empty($f['display_control'])) {
                /** The display control value is the name of a field in the list */
                if ($id) {
                    $displayControlField = $f['display_control'];
                } else {
                    $displayControlField = $name;
                }
            }
        }
        /** Update display control field name for group if exist */
        if (!empty($displayControlField)) {
            $group_meta['group']['display_control'] = $displayControlField;
            $apl_query->update( array('meta_data' => maybe_serialize($group_meta)) , array( 'id' => $gid ) );
        }


        wp_safe_redirect( $this->get_permalink($post_type). '&field=1&group_id='.$gid. '&group_name='. $group_label );
    }
    
    private function _renderTransGroup() {
        if (Apollo_App::hasWPLM()) {
			
		} 
        
        global $sitepress;
        $languages = $sitepress ? $sitepress->get_active_languages() : false;
        if (empty($languages) || (isset($languages) && !$languages[0]) ) {
            return '';
        }
        
        $output = '';
        foreach( $languages as $lang => $langVal ):
            if ($lang == 'en') continue;
            ?>
            <input value="<?php echo esc_attr($group_name); ?>" type="text" 
               name="group_name" size="30" placeholder="<?php _e( 'Group name', 'apollo' ) ?>" autocomplete="off">
            <?php
        endforeach;
        
        return '<fieldset class="apl-lang-tool"><a class="trans">'.__("Translations", "apollo").'</a><div class="container hidden">'.$output.'</div></fieldset>';
    }
    
}

new Apollo_Admin_Tool_Custom_Field();
