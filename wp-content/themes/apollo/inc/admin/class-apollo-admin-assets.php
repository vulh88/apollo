<?php
/**
 * Load assets.
 *
 * @author 		vulh
 * @category 	Admin
 * @package 	inc/admin
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( ! class_exists( 'Apollo_Admin_Assets' ) ) :

/**
 * Apollo_Admin_Assets Class
 */
class Apollo_Admin_Assets {

	public function __construct() {
        
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_styles' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );
	}
    
    public function admin_scripts() {
        global $typenow;
        
        $screen = get_current_screen();
        
        // Add wordpress media frame
        if (in_array( $screen->id, array( 'edit-organization' ) ) ) {
            wp_enqueue_media();
        }

        wp_register_script( 'apl-admin-js-select2', get_stylesheet_directory_uri(). '/assets/js/plugins/select2.js', array( 'jquery' ) );

        wp_register_script( 'apl-admin-js', APOLLO_ADMIN_JS_URL. '/admin.js', array( 'jquery' ), APOLLO_ADMIN_JS_VERSION );
        /** @Ticket #15217 */
        wp_register_script( 'apl-admin-event-js', APOLLO_ADMIN_JS_URL. '/event.js', array( 'jquery' ), APOLLO_ADMIN_JS_VERSION );
        wp_register_script( 'apollo-custom-fields', APOLLO_ADMIN_JS_URL. '/apollo-custom-fields.js', array( 'jquery' ), APOLLO_ADMIN_JS_VERSION );

        /** @Ticket #18553 */
        wp_register_script( 'apl-admin-syndication-js', APOLLO_ADMIN_JS_URL. '/syndication.js', array( 'jquery' ), APOLLO_ADMIN_JS_VERSION );

        /** @Ticket #17080 */
        wp_register_script( 'apl-admin-custom-ninja-form-plugin', APOLLO_ADMIN_JS_URL. '/admin-custom-ninja-form-plugin.js', array( 'jquery' ), APOLLO_ADMIN_JS_VERSION );

        /*@ticket #18087: [CF] 20181030 - [ORG][Admin form] Preview Changes button for the business - item 4, 5*/
        wp_register_script( 'apl-admin-org-js', APOLLO_ADMIN_JS_URL. '/organization.js', array( 'jquery' ), APOLLO_ADMIN_JS_VERSION );

        // page admin js
        wp_register_script( 'apl-admin-page-js', APOLLO_ADMIN_JS_URL. '/page.js', array( 'jquery' ), APOLLO_ADMIN_JS_VERSION );

        if( Apollo_App::get_network_enable_reports( get_current_blog_id() ) == 1) {
            wp_register_script( 'apl-admin-report-event', APOLLO_SRC_URL. '/report/admin/assets/js/report-event.js', array( 'jquery' ), APOLLO_ADMIN_JS_VERSION );
            wp_enqueue_script( 'apl-admin-report-event' );
        }

        wp_register_script( 'apl-gallery-metabox-js', APOLLO_ADMIN_JS_URL. '/gallery-metabox.js', array('jquery', 'jquery-ui-sortable'));

        wp_register_script( 'apl-admin-integration-js', APOLLO_ADMIN_JS_URL. '/admin-integration.js', array( 'jquery' ) );

        wp_register_script( 'meta-boxes-js', APOLLO_ADMIN_JS_URL. '/meta-boxes.js', array( 'jquery' ), APOLLO_ADMIN_JS_VERSION );
        wp_register_script( 'jquery-tip-js', APOLLO_ADMIN_JS_URL. '/jquery-tipTip.js', array( 'jquery' ) );
        wp_register_script( 'taxonomies-js' , APOLLO_ADMIN_JS_URL. '/taxonomies.js', array( 'jquery' ));
        wp_register_script( 'advanced-search-js' , APOLLO_ADMIN_JS_URL. '/advanced-search.js', array( 'jquery' ));
        wp_register_script( 'jquery-data-table-js' , APOLLO_ADMIN_JS_URL. '/jquery.dataTables.js', array( 'jquery' ));
        wp_register_script( 'apl-admin-user-js', APOLLO_ADMIN_JS_URL. '/user.js', array( 'jquery' ) );

        if ( is_network_admin() ) {
            wp_register_script( 'network-admin-js', APOLLO_ADMIN_JS_URL. '/network.js', array( 'jquery' ) );
            wp_enqueue_script( 'network-admin-js' );
        }
        
        wp_enqueue_script( 'jquery-ui-datepicker' );
        wp_enqueue_script( 'apl-admin-js-select2' );
        wp_enqueue_script( 'apl-admin-js' );
        wp_enqueue_script( 'apl-admin-event-js' );
        wp_enqueue_script( 'apl-admin-org-js' );
        wp_enqueue_script( 'apl-admin-page-js' );
        wp_enqueue_script( 'apl-admin-syndication-js' );
        wp_enqueue_script( 'apl-admin-custom-ninja-form-plugin' );
        wp_enqueue_script( 'apollo-custom-fields' );

        wp_enqueue_script( 'apl-gallery-metabox-js' );
        wp_enqueue_script( 'apl-admin-integration-js' );

        wp_enqueue_script( 'meta-boxes-js' );
        wp_enqueue_script( 'jquery-tip-js' );
        wp_enqueue_script( 'taxonomies-js' );
        wp_enqueue_script( 'advanced-search-js' );
        wp_enqueue_script( 'apl-admin-user-js' );

        wp_enqueue_script( 'jquery-data-table-js' );
        
        wp_localize_script( 'apl-admin-js', 'apollo_admin_obj', Apollo_App::admin_js_communicate_client() );
    
        wp_register_script( 'apollo-admin-quick-edit-js', APOLLO_ADMIN_JS_URL. '/apollo-admin-quick-edit.js', array( 'jquery' ) );
        wp_enqueue_script( 'apollo-admin-quick-edit-js' );
        wp_enqueue_media();
        
        global $typenow;
        if ($typenow == Apollo_DB_Schema::_EVENT_PT) {
            wp_register_script( 'event-quick-edit-js', APOLLO_ADMIN_JS_URL. '/event-quick-edit.js', array( 'jquery' ) );
            wp_enqueue_script( 'event-quick-edit-js' );
        }
        
        // For multi select and and order items in theme option
        global $typenow, $taxnow;
        if ( ( ( isset( $_REQUEST['page'] ) 
            && ( $_REQUEST['page'] == 'options-framework' || $_REQUEST['page'] == ''.$typenow.'_custom_fields' ) ) || get_post_type() == Apollo_DB_Schema::_SPOT_PT ) || $taxnow == 'population-served' ) {
            wp_enqueue_script('jquery-ui-sortable'); //load sortable
           
        }

    }
    
    public function admin_styles() {
        wp_enqueue_style('apl-admin-css-select2', get_stylesheet_directory_uri(). '/assets/css/select2.min.css');
        wp_enqueue_style('apl-admin-css', APOLLO_ADMIN_CSS_URL. '/admin.css', array(), APOLLO_ADMIN_CSS_VERSION);
        wp_enqueue_style('apl-gallery-metabox-css', APOLLO_ADMIN_CSS_URL. '/gallery-metabox.css');
        wp_enqueue_style('jquery-data-table-css', APOLLO_ADMIN_CSS_URL. '/jquery.dataTables.css');
        
    }


        
}
endif;

return new Apollo_Admin_Assets();