<?php
/**
 * Post Types Admin
 *
 * @author 		vulh
 * @category 	admin
 * @package 	inc/admin
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Apollo_Admin_Post_Types' ) ) :

/**
 * Apollo_Admin_Post_Types Class
 */
class Apollo_Admin_Post_Types {

    protected $subMenuEventImportFile = '';
    protected $subMenuEventPage = '';
    protected $aplAIEFClass = null;
    protected $currentModule = "";
	/**
	 * Constructor
	 */
	public function __construct() {
		add_action( 'admin_init', array( $this, 'include_post_type_handlers' ) );
		add_filter( 'post_updated_messages', array( $this, 'post_updated_messages' ) );

		// Status transitions
		add_action( 'delete_post', array( $this, 'delete_post' ) );
		//add_action( 'wp_trash_post', array( $this, 'trash_post' ) );
		//add_action( 'untrash_post', array( $this, 'untrash_post' ) );
        
        add_action( 'admin_menu', array( $this, 'post_type_menu' ) );
        
        // Add post type to $wpdb
        add_action( 'init', array( $this, 'apollo_post_type_metadata_wpdb' ) );

        //add summary meta box
        add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes'), 10 );
        // Truonghn: #7509 save data summary meta box to post admin form
        add_action( 'save_post', "Apollo_Meta_Box_Post_Summary::save", 10, 2 );

        /*@ticket #17235 - [Admin] Official tag for event and post */
        $currentTheme = function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '';
        if ( $currentTheme == 'octave-child') {
            add_action( 'save_post', "Apollo_Meta_Box_Official_Tags::save", 10, 2 );
            require_once APOLLO_ADMIN_DIR . '/post-types/meta-boxes/common/class-apollo-meta-box-official-tags.php';
        }

        // Do not allow calling save custom field in quick edit
        if (!Apollo_App::isQuickEdit()) {
            add_action( 'save_post', "Apollo_Meta_Box_Customfield::save", 10, 2 );
        }

        // move  meta box position `after_title` to above content editor
        add_action( 'edit_form_after_title', array( $this, 'after_title_meta_boxes') );

        // Truonghn: #7509 add summary meta box to post admin form
        @include 'post-types/meta-boxes/post/class-apollo-meta-box-post-summary.php';
        require_once 'post-types/meta-boxes/common/class-apollo-meta-box-customfield.php';

        /** @Ticket #13583 */
        require_once APOLLO_ADMIN_DIR . '/post-types/meta-boxes/syndication/org/class-apollo-admin-org-syndication.php';
        /* @ticket #18228: 0002510: Artist Directory Plugin > Admin > Syndication - Artist configuration */
        require_once APOLLO_ADMIN_DIR . '/post-types/meta-boxes/syndication/artist/class-apollo-admin-artist-syndication.php';

        /** @Ticket #19528 */
        require_once APOLLO_ADMIN_DIR . '/post-types/meta-boxes/post/class-apollo-admin-home-featured.php';

        add_filter( 'handle_bulk_actions-edit-event', array($this, 'handleBulkAction'), 10, 3 );
	}

    /**
     * Add the excerpt meta box back in with a custom screen location
     *
     * @param  string $post_type
     * @return null
     */
    function add_meta_boxes( $post_type) {

        if ( in_array( $post_type, array( 'post' ) ) ) {
            add_meta_box('postsummary', __( 'Summary', 'apollo' ), 'Apollo_Meta_Box_Post_Summary::output', $post_type, 'after_title', 'high');

            /*@ticket #17235 - [Admin] Official tag for event and post */
            $currentTheme = function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '';
            if ( $currentTheme == 'octave-child') {
                add_meta_box( 'apollo-post-official-tags', __( 'Official tags', 'apollo' ), 'Apollo_Meta_Box_Official_Tags::output', $post_type, 'side' );
            }
        }
        if ( in_array( $post_type, array( 'page' ) ) && Apollo_Custom_Field::get_fields_by_type($post_type, 'gallery') &&  get_page_template_slug(get_the_ID() ) == 'page_custom.php'  )  {
             add_meta_box('gallery', __( 'Gallery', 'apollo' ), 'Apollo_Meta_Box_Customfield::output', $post_type, 'normal', 'high');
        }
    }


    /**
     * You can't actually add meta boxes after the title by default in WP sowe're being cheeky. We've registered our own meta box position
     * `after_title` onto which we've regiestered our new meta boxes and are now calling them in the `edit_form_after_title` hook which is run
     * after the post tile box is displayed.
     * @return null
     */

    function after_title_meta_boxes() {
        global $post, $wp_meta_boxes;
        # Output the `below_title` meta boxes:
        do_meta_boxes( get_current_screen(), 'after_title', $post );
    }

    
    public function apollo_post_type_metadata_wpdb() {
  
        global $wpdb;
        $avaiable_modules = Apollo_App::get_avaiable_modules();
        
        if ( ! $avaiable_modules ) {
            return false;
        }
        
        foreach ( $avaiable_modules as $am ) {
            // Thienld : skip if module is 'syndication' to make correct data with client's plugin syndicate
            if($am == Apollo_DB_Schema::_SYNDICATION_PT || $am == Apollo_DB_Schema::_SYNDICATION_ARTIST_PT){
                continue;
            }
            $am = str_replace('-', '_', $am);
            $table = 'apollo_'. $am. 'meta';
            $wpdb->$table = $wpdb->prefix . 'apollo_'. $am. 'meta';
            
            if ( $wpdb->tables && ! in_array( $table , $wpdb->tables ) ) {
                $wpdb->tables[] = $table;
            }
        }
    }


    /**
	 * Conditonally load classes and functions only needed when viewing a post type.
	 */
	public function include_post_type_handlers() {
        global $typenow, $apollo_modules;

        $post_id = isset($_GET['post']) ? $_GET['post'] : ( isset( $_POST['post_ID'] ) ? $_POST['post_ID'] : '' );
        $post = get_post( $post_id );

        // Init pages
        if (($post && $post->post_type == 'page') || $typenow == 'page') {
            require_once APOLLO_INCLUDES_DIR. '/class-apollo-creator.php';
            require_once APOLLO_INCLUDES_DIR. '/admin/post-types/class-apollo-admin-page.php';
        }

        $ac_modules = Apollo_App::get_avaiable_modules();
        if ( ! $ac_modules ) {
            $ac_modules = array( Apollo_DB_Schema::_SPOT_PT );
        } else {
            $ac_modules[] = Apollo_DB_Schema::_SPOT_PT;

            /**
             * Load Business Spotlight meta box
             * @ticket #11186
             */
            if ( in_array( Apollo_DB_Schema::_BUSINESS_PT, $ac_modules ) ) {
                $ac_modules[] = Apollo_DB_Schema::_BUSINESS_SPOTLIGHT_PT;
            }

            /**
             * Load Event Spotlight meta box
             * @ticket #13565
             */
            if ( in_array( Apollo_DB_Schema::_EVENT_PT, $ac_modules ) ) {
                $ac_modules[] = Apollo_DB_Schema::_EVENT_SPOTLIGHT_PT;
            }

            /** @Ticket #19008 */
            if (in_array( Apollo_DB_Schema::_ARTIST_PT, $ac_modules)) {
                $ac_modules[] = Apollo_DB_Schema::_SYNDICATION_ARTIST_PT;
            }

            /*@ticket #18320: 0002504: Arts Education Customizations - add hyperlinks to the landing page slider images (Education spotlights) - item 4*/
            if ( in_array( Apollo_DB_Schema::_EDUCATION, $ac_modules ) ) {
                $ac_modules[] = Apollo_DB_Schema::_EDUCATION_SPOTLIGHT_PT;
            }

            /**
             * @ticket #19069: [Organization] Syndication form
             */
            if( in_array( Apollo_DB_Schema::_ORGANIZATION_PT, $ac_modules )){
                $ac_modules[] = Apollo_DB_Schema::_SYNDICATION_ORG_PT;
            }
        }

        global $apollo_modules;
        $_current_mod = '';
        foreach( $ac_modules as $k ) {
            if ( isset( $apollo_modules[$k]['childs'] ) ) $_mods = array_keys( $apollo_modules[$k]['childs'] );
            else $_mods = array($k);
            foreach( $_mods as $k ) {

                if ( ( $typenow == $k ) || ( $post && $post->post_type == $k ) ) {
                    $_current_mod = $k;
                    break;
                }
            }
            if ( $_current_mod ) break;
        }
        
        if ( ! $_current_mod ) return;
        
        $_current_mod = str_replace('_', '-', $_current_mod);
       
        @include 'post-types/class-apollo-admin-cpt-'. strtolower( $_current_mod ).'.php';

        // Do not save meta box meta data for Quick edit
        if (!Apollo_App::isQuickEdit()) {
            @include 'post-types/meta-boxes/class-apollo-admin-'.strtolower( $_current_mod ).'-meta-boxes.php';
        }

    }
    
	/**
	 * Change messages when a post type is updated.
	 *
	 * @param  array $messages
	 * @return array
	 */
	public function post_updated_messages( $messages ) {
		return $messages;
	}
    
    public function search_content() {
        
        // Get org 
        
        
        ?>
        <div class="wrap clear hidden"><style>input[type='text'] { width:200px; padding:4px;  } </style>
            
            <fieldset>
            <legend><h2>Event Advanced Search</h2></legend>
            <hr/>
                <form method="post" >
                    
                    <div class="tablenav top">
                        <div id="apollo-admin-event-adf" class="alignleft actions">
                            <input name="ads-apollo-event-start-date" id="ads-apollo-event-start-date" type="text" placeholder="<?php _e( 'Start Date', 'apollo' ) ?>" />
                            <input name="ads-apollo-event-end-date" id="ads-apollo-event-end-date" type="text" placeholder="<?php _e( 'End Date', 'apollo' ) ?>" />
                            <select name="m" id="ads-apollo-event-org">
                                <option selected="selected" value="0"><?php _e( 'Select an organization' ) ?></option>
                                <option value="201411">November 2014</option>
                            </select>
                            <input value="<?php _e( 'Seach', 'apollo' ) ?>" type="submit" />
                            <br class="clear">
                        </div>
                    </div>
                        
                    <table class="wp-list-table widefat fixed posts">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th style="width: 200px;">Date Range</th>
                                <th>Presenting Org Name</th>
                                <th style="width: 90px;">City Location</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="status-publish hentry alternate iedit author-self level-0">
                                <th></th>
                                <td>aaaa</td>
                                <td>ccc</td>
                                <td>saaa</td>
                                <td>gggg</td>
                            </tr>
                            <tr class="status-publish has-post-thumbnail hentry iedit author-self">
                                <th></th>
                                <td>aaaa</td>
                                <td>ccc</td>
                                <td>saaa</td>
                                <td>gggg</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th style="width: 200px;">Date Range</th>
                                <th>Presenting Org Name</th>
                                <th style="width: 90px;">City Location</th>
                            </tr>
                        </tfoot>
                    </table>
                    <p class="submit">
                        <input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes">
                    </p>

                <form>
            </fieldset>
        </div>

        <?php
    }
    
    /**
	 * Add menu for activated modules
	 *
	 */
    public function post_type_menu() {

        global $apollo_modules;
    
        $active_modules = Apollo_App::get_avaiable_modules();

        if ( $active_modules ) {
            foreach ( $active_modules as $am ) {
                
                $_ac_mods = array();
                if ( isset( $apollo_modules[$am]['childs'] ) ) $_ac_mods = $apollo_modules[$am]['childs'];
                else $_ac_mods[] = $am;

                if( $_ac_mods ):
                    foreach ( $_ac_mods as $k => $m ):
                    
                        $mod = is_integer( $k ) ? $m :  $k;

                        if( $mod != Apollo_DB_Schema::_SYNDICATION_PT &&
                            $mod != Apollo_DB_Schema::_EVENT_PT &&
                            $mod != Apollo_DB_Schema::_CLASSIFIED &&
                            $mod != Apollo_DB_Schema::_IFRAME_SEARCH_WIDGET_PT &&
                            $mod != Apollo_DB_Schema::_PHOTO_SLIDER_PT ){
                            add_submenu_page("edit.php?post_type={$mod}", 'search', __( 'Advanced Search', 'apollo' ), 'manage_options', "{$mod}_manage_type", array( $this, 'search_content' ));
                        }
                        
                        if ( $mod == Apollo_DB_Schema::_ARTIST_PT ) {
                            add_submenu_page("edit.php?post_type={$mod}", __( 'List Award Artist', 'apollo' ), __( 'List Award Artist', 'apollo' ), 'manage_options', "list_award_artist", array( $this, 'list_award_artist' ));
                        }
                        
                        if (in_array($mod, Apollo_Admin_Config::iconMods())) {
                            add_submenu_page("edit.php?post_type={$mod}", __( 'Icons', 'apollo' ), __( 'Icons', 'apollo' ), 'manage_options', "".$mod."_icons", array( $this, 'apollo_icons' ));
                        }
                        if ( $mod == Apollo_DB_Schema::_EVENT_PT && Apollo_App::get_network_event_import_tool() ) {
                            $this->currentModule = $mod;
                            $this->subMenuEventImportFile = $hook_sub_page_name = add_submenu_page("edit.php?post_type={$mod}", 'import', __( 'Import Events', 'apollo' ), 'manage_options', "{$mod}_import", array( $this, 'event_import' ));
                            add_action( "load-".$hook_sub_page_name, array($this, 'add_screen_option_event_import_file') );
                            add_filter( 'set-screen-option', array(__CLASS__, 'set_screen'), 10, 3 );
                        }


                        /**
                         * Author: Vandd
                         * Add Themes to submenu
                         */
                        if ( $mod == Apollo_DB_Schema::_EVENT_PT) {
                            $this->currentModule = $mod;
                            $this->subMenuEventPage = $hook_sub_page_name = add_submenu_page("edit.php?post_type={$mod}", 'themes', __( 'Themes', 'apollo' ), 'manage_options', "{$mod}_themes", array( $this, 'themeTool' ));
                            add_action( "load-".$hook_sub_page_name, array($this, 'add_screen_option_event_page') );
                            add_filter( 'set-screen-option', array(__CLASS__, 'set_screen'), 10, 3 );
                        }

                        /** @Ticket #13583 */
                        if ($mod == Apollo_DB_Schema::_SYNDICATION_PT) {

                            global $submenu;

                            /** @Ticket #19036 - Custom test on admin navigation */
                            $syndicationSubmenu = isset($submenu['edit.php?post_type='. Apollo_DB_Schema::_SYNDICATION_PT]) ? $submenu['edit.php?post_type='. Apollo_DB_Schema::_SYNDICATION_PT] : array();
                            if (!empty($syndicationSubmenu)) {
                                foreach ($syndicationSubmenu as $key => $synSub) {
                                    if ((isset($synSub[0]) && $synSub[0] == 'Syndication')){
                                        $submenu['edit.php?post_type='. Apollo_DB_Schema::_SYNDICATION_PT][$key][0] = __('Event', 'apollo');
                                    }
                                    if (isset($synSub[0]) && $synSub[0] == 'Add') {
                                        unset($submenu['edit.php?post_type='. Apollo_DB_Schema::_SYNDICATION_PT][$key]);
                                    }
                                }
                            }

                            if (in_array(Apollo_DB_Schema::_ARTIST_PT, $active_modules)) {
                                $artistSyndication = Apollo_DB_Schema::_SYNDICATION_ARTIST_PT;
                                $url ="edit.php?post_type={$artistSyndication}";
                                $submenu['edit.php?post_type=' . $mod][] = array('Artist', 'manage_options', $url);
                            }

                            /**
                             * @ticket #19061: [Organization] Syndication form
                             */
                            if (in_array(Apollo_DB_Schema::_ORGANIZATION_PT, $active_modules)) {
                                $orgSyndication = Apollo_DB_Schema::_SYNDICATION_ORG_PT;
                                $url = "edit.php?post_type={$orgSyndication}";
                                $submenu["edit.php?post_type={$mod}"][] = array('Organization', 'manage_options', $url);
                            }
                        }

                        if ( $mod == Apollo_DB_Schema::_BLOG_POST_PT) {
                            add_submenu_page("edit.php", __( 'Home Featured Posts', 'apollo' ), __( 'Home Featured Posts', 'apollo' ), 'manage_options', "home-featured-posts", array( $this, 'blog_home_featured'));
                        }

                    endforeach;
                endif; 
                
            }
        }
    }

    public static function blog_home_featured() {
        if (isset($_POST['_apollo_theme_options'])) {
            $homeFeaturedPostOptions = new Apollo_Admin_Home_Featured();
            $homeFeaturedPostOptions->saveHomeFeaturedPostsOptions();
            do_action( 'admin_notices' );
        }

        require APOLLO_ADMIN_DIR . '/theme-options/includes/fields/field.php';
        require APOLLO_ADMIN_DIR . '/theme-options/includes/fields/text.php';
        require_once APOLLO_ADMIN_DIR . '/theme-options/includes/class-options-interface.php';

        echo '<div class="wrap home-featured-wrap">
            <div id="icon-users" class="icon32"></div>
            <h2>';
        _e('Home Featured Posts', 'apollo');
        echo '</h2>';
        echo '<div  class="postbox home-featured-posts-wrap" style="padding: 10px;">';

        echo '<div class="inside">';
        echo '<form id="apl-home-featured-posts" method="post" action="">';

        $options[] = array(
            'name'        => __( 'Home Featured Articles', 'apollo' ),
            'type'        => 'heading'
        );

        $options[] = array(
            'name'       => __( 'Type', 'apollo' ),
            'id'        => Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_TYPE,
            'std'       => 'full',
            'type'      => 'radio',
            'options'   => array(
                'full' => __('Full Width','apollo'),
                'left' => __('Left Column Only','apollo'),
            ),
            'class'     => 'no-border inline',
        );

        $options[] = array(
            'name'     => __( 'Block title', 'apollo' ),
            'id'   => Apollo_DB_Schema::_HOME_FEATURED_ARTICLES_TITLE,
            'std'  => __('Featured Stories', 'apollo'),
            'type'     => 'text',
            'class' => 'no-border inline',
            'wplm'  => true
        );

        $options[] = array(
            'name'      => __( 'Activate Home Featured Articles', 'apollo' ),
            'id'        => Apollo_DB_Schema::_ACTIVE_HOME_FEATURED_ARTICLES,
            'std'       => 1,
            'type'      => 'radio',
            'options'   => array(
                1           => __( 'Yes', 'apollo' ),
                0           => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border ',
        );

        // Event home featured CMS page
        $options[] = array(
            'name'        => __( 'Enable Blog Featured Content', 'apollo' ),
            'desc'        => __( '', 'apollo' ),
            'id'      => Apollo_DB_Schema::_HOME_CMS_PAGE_BLOG_FEATURED,
            'std'     => 0,
            'type'        => 'radio',
            'options'   => array(
                1  => __( 'Yes', 'apollo' ),
                0  => __( 'No', 'apollo' ),
            ),
            'class'     => 'no-border'
        );

        $blogPosts[0] = __( 'Select Blog Post', 'apollo' );
        $homePostFeaturedAttr = '
            data-enable-remote="1"
            data-post-type="post"
            data-source-url="apollo_get_remote_associate_data_to_select2_box"
        ';

        $postId = of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_LARGE_LEFT);
        $post = get_post($postId);
        $options[] = array(
            'name'      => __(' Position Large Left ', 'apollo'),
            'id'        => Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_LARGE_LEFT,
            'std'       => '0',
            'type'      => 'select',
            'options'   => $blogPosts,
            'class'     => 'no-border inline',
            'select-class'  => 'select apl_select2',
            'selectedItem'  => array(
                'id'    => $post ? $post->ID : '',
                'label' => $post ? $post->post_title : ''
            ),
            'attr'  => $homePostFeaturedAttr
        );

        $postId = of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_LARGE_RIGHT);
        $post = get_post($postId);
        $options[] = array(
            'name'       => __( ' Position Large Right ', 'apollo' ),
            'id'        => Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_LARGE_RIGHT,
            'std'       => '0',
            'type'      => 'select',
            'options'   => $blogPosts,
            'class'     => 'no-border inline',
            'select-class'  => 'select apl_select2',
            'selectedItem'  => array(
                'id'    => $post ? $post->ID : '',
                'label' => $post ? $post->post_title : ''
            ),
            'attr'  => $homePostFeaturedAttr
        );

        $postId = of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_TOP);
        $post = get_post($postId);
        $options[] = array(
            'name'       => __( ' Position Right Top ', 'apollo' ),
            'id'        => Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_TOP,
            'std'       => '0',
            'type'      => 'select',
            'options'   => $blogPosts,
            'class'     => 'no-border inline',
            'select-class'  => 'select apl_select2',
            'selectedItem'  => array(
                'id'    => $post ? $post->ID : '',
                'label' => $post ? $post->post_title : ''
            ),
            'attr'  => $homePostFeaturedAttr
        );

        $postId = of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_MIDDLE);
        $post = get_post($postId);
        $options[] = array(
            'name'       => __( ' Position Right Middle ', 'apollo' ),
            'id'        => Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_MIDDLE,
            'std'       => '0',
            'type'      => 'select',
            'options'   => $blogPosts,
            'class'     => 'no-border inline',
            'select-class'  => 'select apl_select2',
            'selectedItem'  => array(
                'id'    => $post ? $post->ID : '',
                'label' => $post ? $post->post_title : ''
            ),
            'attr'  => $homePostFeaturedAttr
        );

        $postId = of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_BOTTOM);
        $post = get_post($postId);
        $options[] = array(
            'name'       => __( ' Position Right Bottom ', 'apollo' ),
            'id'        => Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_BOTTOM,
            'std'       => '0',
            'type'      => 'select',
            'options'   => $blogPosts,
            'class'     => 'no-border inline',
            'select-class'  => 'select apl_select2',
            'selectedItem'  => array(
                'id'    => $post ? $post->ID : '',
                'label' => $post ? $post->post_title : ''
            ),
            'attr'  => $homePostFeaturedAttr
        );
        $options[] = array(
            'name'       => __( 'Position', 'apollo' ),
            'id'        => Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_POSTION,
            'std'       => Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_POSTION_TOP,
            'type'      => 'select',
            'options'   => array(
                Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_POSTION_TOP => __('Top','apollo'),
                Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_POSTION_BOTTOM => __('Bottom','apollo'),
            ),
            'class'     => 'no-border inline',
        );
        ob_start();
        Options_Framework_Interface::optionsframework_fields($options);
        $htmlRender = ob_get_clean();
        echo $htmlRender;

        echo '<div class="featured-posts-submit"><input class="button button-primary" type="submit" name="featured-posts-submit" value="'. __( 'Update', 'apollo' ) .'" /></div>';
        echo '</form>';
        echo '</div>';
        echo '</div>';
        echo '</div>';
    }

    /** @Ticket #13583 */
    public static function organization_syndication() {

        $org_syndication = new Apollo_Admin_Org_Syndication();
        if (isset($_POST[Apollo_DB_Schema::_SYNDICATION_ALL_ORGANIZATION])) {
            $org_syndication->saveOrgSyndicationData();
        }
        $org_syndication_data = $org_syndication->getOrgSyndicationData();
        $all_checked = '';
        $selected_checked = '';

        /**
         * Enable list table when search or filter ORG
         * Disable list table when submit with "All" mode
         */
        $isSelecting = !isset($_POST['org-syndication-submit']) ? (isset($_GET['s']) || isset($_GET['orderby']) || isset($_GET['paged'])) : false;

        if ($org_syndication_data[Apollo_DB_Schema::_SYNDICATION_ALL_ORGANIZATION] || $isSelecting) {
            $selected_checked = 'checked';
        } else {
            $all_checked = 'checked';
        }

        require_once APOLLO_ADMIN_DIR . '/post-types/meta-boxes/syndication/org/APL_Org_Syndication_Table.php';

        $listID = array();
        if (!empty($org_syndication_data[Apollo_DB_Schema::_SYNDICATION_SELECTED_ORGANIZATION])) {
            $listID = explode(',', $org_syndication_data[Apollo_DB_Schema::_SYNDICATION_SELECTED_ORGANIZATION]);
        }
        $tableView = new APL_Org_Syndication_Table();
        $tableView->setListOrgSelected($listID);
        if (isset($_GET['paged'])) {
            $tableView->setOffset(intval($_GET['paged']));
        }

        $tableView->prepare_items();

        $currentDomainName = get_site_url();
        $xmlEngUrl = $currentDomainName . '/?'. 'syndicated_data' . '&aplang=eng&apoutput=orgxml&type=' . Apollo_DB_Schema::_ORGANIZATION_PT;

        echo '<div class="wrap">
            <div id="icon-users" class="icon32"></div>
            <h2>';
                _e('Organization Syndication', 'apollo');
        echo '</h2>';
        echo '<div  class="postbox" style="padding: 10px;">';
        $tableView->search_box(__('Search Organization', 'apollo'), 'admin-org-syndication');
        echo '<form id="frm-org-syndication" method="post" action="">';
                echo '<div class="select-all-org"><input type="radio" id="all-org" name="'. Apollo_DB_Schema::_SYNDICATION_ALL_ORGANIZATION .'" '. $all_checked .' value="0"/>
                <label for="all-org">'. __( 'All', 'apollo' ) .'</label>
                <input type="radio" id="list-org" name="'.Apollo_DB_Schema::_SYNDICATION_ALL_ORGANIZATION.'" '. $selected_checked .' value="1" />
                <label for="list-org">'. __( 'Select Organization', 'apollo' ) .'</label>
                </div>';
                wp_nonce_field();
                echo '<div class="wrap-select-org">';
                $tableView->display();
                echo '</div>';
                /* XML */
                echo '<div class="wrap-op-blk">';
                echo '<div class="inside">';
                echo '<h3>'.__("XML Generation").'</h3>';
                echo '<div class="options_group clear ct-selection-box">';
                apollo_wp_text_input( array(
                    'id' => Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_LIMIT,
                    'label' => __( 'Limit','apollo' ),
                    'value' => $org_syndication_data[Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_LIMIT],
                    'class' => 'apollo_input_number'

                ));
                echo '</div>';
                echo '<div class="options_group clear ct-selection-box">';
                apollo_wp_radio( array(
                    'id' => Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_ORDERBY,
                    'label' => __( 'Order by','apollo' ),
                    'value' => $org_syndication_data[Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_ORDERBY],
                    'options' => array(
                        'post_title' => __( 'Post title', 'apollo' ),
                        'post_date' => __( 'Post date', 'apollo' ),
                    ),
                ));
                echo '</div>';
                echo '<div class="options_group clear ct-selection-box">';
                apollo_wp_radio( array(
                    'id' => Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_ORDER,
                    'label' => __( 'Sort order','apollo' ),
                    'value' => $org_syndication_data[Apollo_DB_Schema::_SYNDICATION_ORGANIZATION_ORDER],
                    'options' => array(
                        'asc' => __( 'ASC', 'apollo' ),
                        'desc' => __( 'DESC', 'apollo' ),
                    ),
                ));
                echo '</div>';
                echo '<div class="options_group clear">';
                apollo_wp_text_input(  array(
                    'id' => '_syndication_org_xml_english',
                    'label' => __( 'English', 'apollo' ),
                    'value' => $xmlEngUrl,
                ) );
                echo '<a class="button button-small"  href="'.$xmlEngUrl.'" target=_blank>'.__("TEST URL","apollo").'</a>';
                echo '</div>';
                echo '<div class="select-btn-update"><input class="button button-primary" type="submit" name="org-syndication-submit" value="'. __( 'Update', 'apollo' ) .'" /></div>';
                echo '<input type="hidden" name="current_org_ids" value="'. (!empty($tableView->getListOrgSelected()) ? implode(',',$tableView->getListOrgSelected()) : '') .'" />';
            echo '</form>
        </div>';

    }

    /**
     * @ticket #18228: 0002510: Artist Directory Plugin > Admin > Syndication - Artist configuration
     */
    public static function artist_syndication() {
        $artistSyndication = new Apollo_Admin_Artist_Syndication();
        $artistSyndication->artistSyndicationTemplate();
    }

    /**
     * Screen options
     */

    public static function set_screen( $status, $option, $value ) {
        return $value;
    }

    public function add_screen_option_event_import_file() {
        $option = 'per_page';
        $args   = array(
            'label'   => 'Number of items per page:',
            'default' => Apollo_Event_Import_File::_APOLLO_IEF_NUM_NUM_PER_PAGE,
            'option'  => 'event_per_page'
        );
        add_screen_option( $option, $args );
        if(class_exists('Apollo_Admin_Import_Event_File')){
            $args = array(
                'hookSubMenuPage' => $this->subMenuEventImportFile,
                'page' => $this->currentModule . '_import',
                'postType' => $this->currentModule
            );
            $this->aplAIEFClass = new Apollo_Admin_Import_Event_File($args);
        }
    }

    public function event_import() {
        // New class handle import here
        if(!empty($this->aplAIEFClass)){
            $postType = $this->currentModule;
            $pageType = $this->currentModule . '_import';
            if(isset($_SESSION['action_completed'])){
                switch ($_SESSION['action_completed']) {
                    case 'delete':
                        echo '<div class="updated"><p>'.__('All selected files are deleted successfully !','apollo').'</p></div>';
                        break;
                    default:
                        break;
                }
                unset($_SESSION['action_completed']);
            }
            echo '<form id="posts-filter" class="apollo-import-event-files" method="get" action="">
                    <input type="hidden" name="post_type" value="'.$postType.'" />
                    <input type="hidden" name="page" value="'.$pageType.'" />
            ';
            $this->aplAIEFClass->prepare_items();
            $this->aplAIEFClass->display();
            echo '</form>';
        } else {
            // take user to admin dashboard page.
        }
    }

    public function add_screen_option_event_page() {
        $option = 'per_page';
        $args   = array(
            'label'   => 'Number of items per page:',
            'default' => Apollo_Event_Import_File::_APOLLO_IEF_NUM_NUM_PER_PAGE,
            'option'  => 'event_per_page'
        );
        add_screen_option( $option, $args );
        if(class_exists('Apollo_Admin_Event_Page_List_Table')){
            $args = array(
                'hookSubMenuPage' => $this->subMenuEventPage,
                'page' => $this->currentModule . '_themes',
                'postType' => $this->currentModule
            );
            $this->aplAIEFClass = new Apollo_Admin_Event_Page_List_Table($args);
        }
    }

    public function themeTool() {
        // New class handle import here
        if(!empty($this->aplAIEFClass)){
            $permalink = $this->aplAIEFClass->getPermalink();

            if ( isset( $_POST['submitnew'] ) ) {
                $slug = urlencode( $_POST['event-type'] );
                wp_safe_redirect( $permalink. '&add_new=1&slug='. $slug);
            }

            if(isset($_GET['add_new'])){
                $this->aplAIEFClass->addTheme();
            }else{
                $postType = $this->currentModule;
                $pageType = $this->currentModule . '_themes';
                if ( isset($_GET['id']) && $_GET['id'] && isset($_GET['isDel']) && $_GET['isDel'] ) {
                    $this->aplAIEFClass->removeTheme(array($_GET['id']));
                    wp_safe_redirect( $this->aplAIEFClass->getPermalink('', false, $_GET) );
                }
                if ( isset( $_POST['search-submit'] ) ) {
                    $s = urlencode( $_POST['s'] );
                    $paged = urlencode( $_POST['paged']);
                    wp_safe_redirect( $permalink. '&s='. $s . '&paged=' . $paged);
                }
                $search = '';
                if(isset($_GET['s']) && !empty($_GET['s'])){
                    $search = $_GET['s'];
                }
                echo '<form method="post" action="" id="frm-list-theme-tool">
                    <div id="apl-cf-group" class="wrap clear">
                        <input type="hidden" name="post_type" value="'.$postType.'" />
                        <input type="hidden" name="page" value="'.$pageType.'" />
                        <input type="hidden" id="artsopolis-calendar-selected-events" name="artsopolis-calendar-selected-events" value="" />
                        <fieldset>
                            <legend><h2>' . __( "Themes", "apollo" ) . '</h2></legend>
                            <div class="group-wraper">';
                                apollo_dropdown_categories("event-type", array("hide_empty" => 0));
                                echo '<input type="button" name="submitnew" id="submitnew" add_new_link="'.$permalink.'" class="button button-primary" value="'. __( "Add New", "apollo" ) .'">
                            </div>
                        </fieldset>
                        <p class="search-box">
                        <label class="screen-reader-text" for="search_id-search-input">search:</label> 
                        <input id="search_id-search-input" type="text" name="s" value="'.$search.'" />
                        <input id="search-submit" class="button" type="submit" name="search-submit" value="search" />
                        </p>
                    </div>
                ';
                $this->aplAIEFClass->prepare_items();
                $this->aplAIEFClass->display();
                echo '</form>';
                //Disable option
                echo '<input id="them-tool-lists" type="hidden" value="'. $this->aplAIEFClass->getCategoriesSelected() .'" />';

            }

        } else {
            // take user to admin dashboard page.
        }
    }
    
    public function list_award_artist() {
        global $wpdb;
        $current_year = date('Y');
        $query_year = isset($_POST['year']) ? (int)$_POST['year'] : date('Y');
        $type = isset($_GET['type']) ? (int)$_GET['type'] : Apollo_DB_Schema::_ARTIST_PT;


        $sql = "select * from {$wpdb->{Apollo_Tables::_APL_TIMELINE}} where year_award = '%s' and type = '%s' order by type, year_award";
        $results = $wpdb->get_results($wpdb->prepare($sql, array(
                    $query_year,
                    $type
                )));

        ?>
        <div class="wrap apollo-artist-timeline-management">
            <h2><?php _e( 'Award Artist', 'apollo' ) ?></h2>

            <div id="post-body-content">

                <div id="apollo-artist-question" class="postbox">
                    <form action="<?php echo $_SERVER['REQUEST_URI'] ?>" name="artist_timeline" method="POST">
                    <div class="inside">
                        <label for="artist_timeline_year">Years:</label>
                        <?php
                        $arr_year = array_combine(range(1980, $current_year), range(1980, $current_year));
                        ?>
                        <select name="year" id="artist_timeline_year" onchange="this.form.submit();">
                            <?php foreach($arr_year as $key => $year ): ?>
                                <option value="<?php echo $key ?>" <?php selected($query_year, $key) ?>><?php echo $year ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="inside">
                        <?php if(!empty($results)): ?>
                            <?php foreach($results as $arrObj):
                                $post = get_post($arrObj->p_id);
                                ?>
                                <a href="<?php echo get_edit_post_link($arrObj->p_id) ?>"><?php echo $post->post_title ?></a>
                                <br/>
                            <?php endforeach; ?>
                            <?php else: ?>
                                <?php _e('No results!') ?>
                            <?php endif; ?>

                    </div>
                    </form>
                </div>
            </div>
        </div>
        <?php
    }

    public function get_permalink($post_type) {
        return admin_url().'edit.php?post_type='.$post_type.'&page='.$post_type.'_custom_fields';
    }
    
    public function apollo_icons() {

        global $typenow;
        $opt_name = '';
        switch( $typenow ) {
            case Apollo_DB_Schema::_EDUCATOR_PT:
                $opt_name = Apollo_DB_Schema::_APL_EDUCATORS_ICONS;
                break;
            case Apollo_DB_Schema::_ARTIST_PT:
                $opt_name = Apollo_DB_Schema::_APL_ARTISTS_ICONS;
                break;
            //trilm icons
            case Apollo_DB_Schema::_ORGANIZATION_PT:
                $opt_name = Apollo_DB_Schema::_APL_ORG_ICONS;
                break;
            case Apollo_DB_Schema::_VENUE_PT:
                $opt_name = Apollo_DB_Schema::_APL_VENUE_ICONS;
                break;
            case Apollo_DB_Schema::_CLASSIFIED:
                $opt_name = Apollo_DB_Schema::_APL_CLASSIFIED_ICONS;
                break;
            case Apollo_DB_Schema::_PUBLIC_ART_PT:
                $opt_name = Apollo_DB_Schema::_APL_PUBLIC_ART_ICONS;
                break;
            case Apollo_DB_Schema::_EVENT_PT:
                $opt_name = Apollo_DB_Schema::_APL_EVENT_ICONS;
                break;
        }
        
        //kv-upload.php 
        if( 'POST' == $_SERVER['REQUEST_METHOD']  ) {
            $_arr_icon = array();

            delete_option( $opt_name );
            
            // Save icons
            if ( $_FILES ) { 
                
                $files = $_FILES["file_icon"];  

                $desc = $_POST['icon_desc'];
                $ids = $_POST['icon_ids'];
                $keys = $_POST['icon_keys'];
                $urls = $_POST['icon_url'];
                $open_new_windows = $_POST['open_new_window'];

                foreach ( $files['name'] as $key => $value) {

                    if ($files['name'][$key]) { 

                        $file = array( 
                            'name'      => $files['name'][$key],
                            'type'      => $files['type'][$key], 
                            'tmp_name'  => $files['tmp_name'][$key], 
                            'error'     => $files['error'][$key],
                            'size'      => $files['size'][$key]
                        ); 

                        $_FILES = array ("file_icon" => $file); 

                        foreach ($_FILES as $file => $array) {			

                            $icon_id = self::handle_attachment($file);

                            $uniqueKey = !empty($keys[$key]) ? $keys[$key] : $icon_id;
                            $_arr_icon[$uniqueKey] = array(
                                'icon_key'  => $uniqueKey,
                                'icon_id'       => $icon_id,
                                'icon_desc'     => $desc[$key], //Apollo_App::clean_data_request($desc[$key]),
                                'icon_url'     => Apollo_App::clean_data_request( $urls[$key]),
                                'open_new_window'     => $open_new_windows[$key],
                            );
                            
                        }

                    } else if( isset( $ids[$key] ) && $ids[$key] ) {

                        $_arr_icon[$keys[$key]] = array(
                            'icon_key'  => $keys[$key],
                            'icon_id'       => $ids[$key],
                            'icon_desc'     => $desc[$key], //Apollo_App::clean_data_request($desc[$key]),
                            'icon_url'     => Apollo_App::clean_data_request( $urls[$key]),
                            'open_new_window'     => $open_new_windows[$key],
                        );
                    } 
                }
            }
            update_option( $opt_name , @serialize( $_arr_icon ) );
        }
        
        $icons = @unserialize( get_option( $opt_name ) );


        if ( ! $icons ) {
            $icons = array( array( 'icon_id' => '', 'educator_id'   => '', 'icon_desc' => '' ) );   
        } 
        
        ?>
        <div class="wrap icon-manager event-icon-setting-box">
            <form action="" method="post" enctype="multipart/form-data" name="front_end_upload" >
                <p class="submit">
                    <input type="submit" name="submit" id="submit" class="button button-primary" value="<?php _e( 'Update Icon(s)', 'apollo' ) ?>">
                    <br/>    
                </p>
            <div class="postbox apl-custom-postbox icons">
                    <div id="video-event-data">
                        <div class="video-wrapper">
                            <?php 
                                $i = 0;
                                foreach( $icons as $k => $icon ):
                                   
                            ?>
                            <div data-id="<?php echo 'icon_desc_' . $k ?>" data-old-id="<?php echo 'icon_desc_' . $k ?>" class="count video-list<?php echo $i > 0 ? '-'. $i : '' ?>">
                                <div class="options_group event-box">
                                    <p class="form-field icon-img ">
                                        <label for="file_icon"><?php _e( 'Image', 'apollo' ) ?></label>
                                        <?php if( $icon['icon_id'] ) echo wp_get_attachment_image( $icon['icon_id'], 'thumbnail' ) ?>
                                        <input type="file" id="file_icon" name="file_icon[]" >
                                        <input type="hidden" name="icon_ids[]" value="<?php echo $icon['icon_id'] ?>" />
                                        <input type="hidden" name="icon_keys[]" value="<?php echo !empty($icon['icon_key']) ? $icon['icon_key'] : $icon['icon_id'] ?>" />
                                    </p>
                                </div>
                                <div class="options_group event-box">
                                    <p class="form-field icon-img ">
                                        <label for="icon_url_<?php echo $k ?>"><?php _e( 'URL', 'apollo' ) ?></label>
                                        <input type="text" class="apollo_input_url" id="icon_url_<?php echo $k ?>" name="icon_url[]" value="<?php echo isset($icon['icon_url']) ? $icon['icon_url'] : '' ?>">
                                    </p>
                                </div>
                                <div class="options_group event-box">
                                    <p class="form-field icon-img ">
                                        <label for="icon_url_<?php echo $k ?>"><?php _e( 'Open new window', 'apollo' ) ?></label>
                                        <input type="checkbox" class="apollo_input_open_new_window" id="open_new_window_<?php echo $i ?>" <?php echo (isset( $icon['open_new_window']) && $icon['open_new_window'] == 1) ? "checked" : "" ?> name="open_new_window[<?php echo $i ?>]" value="1">
                                    </p>
                                </div>
                                <div class="options_group event-box artist-video-desc">
                                    <p class="form-field video_desc_field ">
                                        <label for="icon_desc"><?php _e( 'Description', 'apollo' ) ?></label>
                                        <?php
                                        $quicktags_settings = array( 'buttons' => 'strong,em,link,block,del,code,close,more' );
                                        $settings = array('wpautop' => true,'tinymce'       => array(
                                            'toolbar1'      => 'bold,italic,underline,separator,alignleft,aligncenter,alignright,separator,link,unlink,undo,redo',
                                            'toolbar2'      => 'strikethrough,hr, forecolor, pastetext, removeformat, charmap, outdent,indent',
                                        ), 'media_buttons' => false, 'quicktags' => $quicktags_settings, 'textarea_rows' => '5', 'textarea_name' => 'icon_desc[]');
                                        wp_editor(html_entity_decode( htmlentities( stripslashes($icon['icon_desc']))), 'icon_desc_' . $k, $settings);
                                        ?>
                                    </p>
                                </div>
                                <div style="display: block;" data-confirm="<?php _e( 'Are you sure to remove this video ?', 'apollo' ) ?>" class="del"><i class="fa fa-times"></i></div>

                            </div>
                            <?php
                                $i++; endforeach;
                            ?>
                            <script>
                                var index_checkbox_open_new_window = <?php echo $i; ?>
                            </script>

                        </div>
                        <input type="button" class="button button-primary button-large" id="apl-add-video" value="<?php _e( 'Add Icon', 'apollo' ) ?>">
                        <br/><br/>
                    </div>
            </div>
            <p class="submit">
                <input type="submit" name="submit" id="submit" class="button button-primary" value="<?php _e( 'Update Icon(s)', 'apollo' ) ?>">
            </p>
        </form>
        </div>
        
        <?php
    }
    
    public function handle_attachment($file_handler) {
            // check to make sure its a successful upload
        if ($_FILES[$file_handler]['error'] !== UPLOAD_ERR_OK) __return_false();

        require_once(ABSPATH . "wp-admin" . '/includes/image.php');
        require_once(ABSPATH . "wp-admin" . '/includes/file.php');
        require_once(ABSPATH . "wp-admin" . '/includes/media.php');

        $attach_id = media_handle_upload( $file_handler, '' );
        return $attach_id;
    }
    
    protected function questions() {
        
        if ( $_POST ):
            // Update questions
            if ( isset( $_POST[Apollo_DB_Schema::_APL_ARTIST_OPTIONS] ) ) {
                $val = serialize( array_filter( Apollo_App::clean_array_data( $_POST[Apollo_DB_Schema::_APL_ARTIST_OPTIONS] ), 'strlen') );
                
            } else {
                $val = '';
            }
            update_option( Apollo_DB_Schema::_APL_ARTIST_OPTIONS, $val );
        endif;
        
        
        // Get questions
        $data = @unserialize( get_option( Apollo_DB_Schema::_APL_ARTIST_OPTIONS ) );  
        
        if ( ! $data ) {
            $data[] = '';
        }
        
        ?>
        <form action="" method="post">
        <div id="apollo-artist-question" class="postbox clear">
                    
            <div style="display: none" class="warning"><?php _e( 'Do not forget to click Save button to update your change', 'apollo' ) ?></div>

            <h3 class="hndle"><span><?php _e( 'Update Questions', 'apollo' ) ?></span></h3>

            <div class="inside">
                <input type="hidden" id="apollo_artist_meta_nonce" name="apollo_artist_meta_nonce" value="6c5d004976"><input type="hidden" name="_wp_http_referer" value="/wp-admin/post-new.php?post_type=artist">        
                <div id="video-event-data">
                <input name="commit_changed" value="0" type="hidden" />    
                <div class="artist-question-wrapper">

                    <?php 
                        $i = 0;
                        foreach ( $data as $val ):
                        if ( ! $val && count( $data ) > 1 ) continue;
                        ?>
                    <div <?php if ( ! $val ) echo 'style="display: none"'; ?> class="count artist-question-list-<?php echo $i ?>">
                        <div class="options_group event-box">
                            <p class="form-field">
                                <label data-label="<?php _e( 'Question', 'apollo' ) ?>"><?php echo sprintf( __( 'Question %s', 'apollo' ), $i + 1 ) ?></label>
                                <input placeholder="<?php _e( 'The name of question here (*)', 'apollo' ) ?>" name="<?php echo Apollo_DB_Schema::_APL_ARTIST_OPTIONS ?>[]" type="text" value="<?php echo $val ?>" class="full">
                                <a data-should-confirm="1" data-confirm="<?php _e( 'Are you sure to delete this question ?',  'apollo' ) ?>" href="#" class="remove"><i class="fa fa-remove"></i></a>
                            </p>
                        </div>
                    </div>
                    <?php $i++; endforeach; ?>

                </div>

                <input type="hidden" name="_apl_artist_data[_apl_artist_video]">
                <input type="button" class="button button-primary button-large" id="apl-add-artist-question" value="<?php _e( 'Add Question', 'apollo' ) ?>">

                <br><br>
                </div>  
            </div>
        </div>
        <input data-confirm="<?php _e( 'Are you sure you want to commit your change?', 'apollo' ) ?>" id="apl-artist-save-questions" type="submit" name="submit" class="button button-primary button-large"  value="<?php _e( 'Save update', 'apollo' ) ?>">            
        </form>
        <?php 

    }
    
    /**
     * Handle function when we delete post
     * 
     * @access public
     * @param int $post_id
     */
    public function delete_post( $post_id ) {
        global $wpdb;
        
        $post_type = get_post_type( $post_id );
        switch ( $post_type ) :
            
            case Apollo_DB_Schema::_VENUE_PT:
                $wpdb->query( " DELETE FROM {$wpdb->{Apollo_Tables::_APL_VENUE_META}} WHERE apollo_venue_id = $post_id" );
            break;
        
            case Apollo_DB_Schema::_ORGANIZATION_PT:
                $wpdb->query( " DELETE FROM {$wpdb->{Apollo_Tables::_APL_ORG_META}} WHERE apollo_organization_id = $post_id" );
            break;
            
            case Apollo_DB_Schema::_EVENT_PT:
                global $typenow;
        
                    if ( $typenow !== Apollo_DB_Schema::_EVENT_PT ) {
                        return;
                    }

                    if ( ! ( int ) $post_id ) {
                        return false;
                    }

                    global $wpdb;

                    // Delete event in event meta
                    $wpdb->query( " DELETE FROM {$wpdb->{Apollo_Tables::_APOLLO_EVENT_META}} WHERE apollo_event_id = $post_id" );
                    
                    // Delete event in post term
                    $wpdb->query( " DELETE FROM {$wpdb->{Apollo_Tables::_POST_TERM}} WHERE post_id = $post_id" );
            break;        
            
        endswitch;
    }

    /**
     * @param $redirect
     * @param $action
     * @param $objectIDs
     * @return mixed
     */
    public function handleBulkAction($redirect, $action, $objectIDs) {
        global $post_type;
        switch($post_type) {
            case Apollo_DB_Schema::_EVENT_PT:
                if ($action == 'apl_reviewed') {
                    if ( !empty( $objectIDs) ) {
                        $currentListReview = get_option( Apollo_DB_Schema::_APL_EVENT_REVIEW_EDIT, '');
                        if (!empty($currentListReview)) {
                            $currentListReview = explode( ",", $currentListReview );
                            $result = array_diff($currentListReview, $objectIDs);
                            $result = empty($result) ? '' : implode(',', $result);
                            update_option( Apollo_DB_Schema::_APL_EVENT_REVIEW_EDIT, $result);
                        }
                    }
                }
                break;
        }
        return $redirect;
    }
}
        
endif;

return new Apollo_Admin_Post_Types();
