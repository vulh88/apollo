<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Apollo Admin.
 *
 * @class 		Apollo_Admin 
 * @author 		vulh
 * @package 	inc/admin
 */
class Apollo_Admin_Image {
    public function __construct() {
        add_action( 'admin_footer-post-new.php', array(__CLASS__, 'denyUploadImage') );
        add_action( 'admin_footer-post.php', array(__CLASS__, 'denyUploadImage') );

    }

    public static function denyUploadImage()
    {
        global $typenow, $sonoma_available_modules;
        
        $hasRetina = of_get_option( Apollo_DB_Schema::_ENABLE_RETINA_DISPLAY );
        $duplicateNum = $hasRetina ? 2 : 1;
        $template = function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '';
        
        if($typenow === 'spotlight' ||
            ( ( $typenow == 'page' ||  $typenow == 'post' ) && $template == APL_SM_DIR_THEME_NAME )) {
            $w = 2 * get_option('large_size_w');
            $h = 2* get_option('large_size_h');

            self::denyUploadImageMustLarge($w, $h);
        } elseif ( $template == APL_SM_DIR_THEME_NAME
            && isset($sonoma_available_modules)
            && in_array($typenow, $sonoma_available_modules)
        ) {
            $w = $duplicateNum * get_option('circle_image_size_w');
            $h = $duplicateNum * get_option('circle_image_size_h');
            self::denyUploadImageMustLarge($w, $h);
        } else {
            $w = $duplicateNum * get_option('medium_size_w');
            $h = $duplicateNum * get_option('medium_size_h');

            self::denyUploadImageMustLarge($w, $h);
        }

    }

    public static function denyUploadImageMustLarge($w, $h) {
        $template = function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '';
        $apl_sm_dir_theme_name = APL_SM_DIR_THEME_NAME;
            ?>
        <script type="text/javascript">
            if(typeof wp.media !== 'undefined'){
                var originalButton = wp.media.view.Button;
                wp.media.view.Button = originalButton.extend({

                    click: function( event ) {
                        if ( '#' === this.attributes.href ) {
                            event.preventDefault();
                        }

                        var _model = false;
                        var _image = false;

                        try {
                            _model =  wp.media.editor.get().state().get('selection').models[0];
                            _image = wp.media.featuredImage.frame().state().get('selection').models[0];
                        } catch ($e) {}

                        /* normal flow */
                        if(wp.media.featuredImage.frame().state() === undefined  && (wp.media.editor.get() === undefined  || (_model && _model === undefined))) {
                            if ( this.options.click && ! this.model.get('disabled') ) {
                                this.options.click.apply( this, arguments );
                            }
                            return true;
                        }

                        var limitLargeW = parseInt('<?php echo $w ?>', 10);
                        var limitLargeH = parseInt('<?php echo $h ?>', 10);
                        var _template = '<?php echo $template?>';
                        var _aplSMDirThemeName = '<?php echo $apl_sm_dir_theme_name;?>';

                        var _arrInfo = '';

                        if( wp.media.editor.get() !== undefined  && typeof _model !== 'undefined' && _model
                            && _model.attributes && _model.attributes.type === 'image' ) {
                            _arrInfo = wp.media.editor.get().state().get('selection').models[0].attributes.sizes.full;
                        }
                        else if ( wp.media.featuredImage.frame().state() != undefined && _image !== undefined ) {
                            if (wp.media.featuredImage.frame().state().get('selection').models[0] !== undefined)
                                _arrInfo = wp.media.featuredImage.frame().state().get('selection').models[0].attributes.sizes.full;
                        }


                        var addMediaClicked = false;
                        if (jQuery('#insert-media-button').length) {
                            addMediaClicked = jQuery('#insert-media-button').attr('data-click');
                        }

                        try {
                            // Only apply this rule to feature update img
                            var selection = wp.media.featuredImage.frame().state().get('selection');
                        }
                        catch (e) {
                            var selection = false;
                        }

                        if ( _template == _aplSMDirThemeName){
                            if(  addMediaClicked == 'false' && wp.media.featuredImage.frame().state() != undefined
                                && (_arrInfo == undefined ||_arrInfo.width < limitLargeW || _arrInfo.height < limitLargeH)) {
                                alert('<?php _e('Image must larger than '.$w.'x'.$h, 'apollo') ?>');
                                /** Vandd -@Task #12759 remove selection*/
                                if(selection && selection.first()){
                                    selection.remove(selection.first());
                                }
                                return;
                            }
                        } else {
                            if( !jQuery('body').hasClass("post-type-post") && addMediaClicked == 'false' && wp.media.featuredImage.frame().state() != undefined
                                && (_arrInfo == undefined ||_arrInfo.width < limitLargeW || _arrInfo.height < limitLargeH)) {
                                alert('<?php _e('Image must larger than '.$w.'x'.$h, 'apollo') ?>');
                                /** Vandd -@Task #12759 remove selection*/
                                if(selection && selection.first()){
                                    selection.remove(selection.first());
                                }
                                return;
                            }
                        }

                        if ( this.options.click && ! this.model.get('disabled') ) {
                            this.options.click.apply( this, arguments );
                        }

                    }
                });
            }
        </script>
        <?php
        }

}
    
return new Apollo_Admin_Image();