<?php

    require_once APOLLO_ADMIN_SRC. '/user/model/ApolloAdminUserSingleSelectionModel.php';
    $model = new ApolloAdminUserSingleSelectionModel($user);
    $selects = array(
        'artist_id'    => array(
            'label'     => __('Artist', 'apollo'),
            'data'      => $model->getArtist(),
            'post_type'      => Apollo_DB_Schema::_ARTIST_PT,
        ),
        'educator_id'    => array(
            'label'     => __('Educator', 'apollo'),
            'data'      => $model->getEducator(),
            'post_type'      => Apollo_DB_Schema::_EDUCATION,
        ),
        'org_id'       => array(
            'label'     => __('Organization', 'apollo'),
            'data'      => $model->getOrg(),
            'post_type'      => Apollo_DB_Schema::_ORGANIZATION_PT,
        ),
        'venue_id'     => array(
            'label'     => __('Venue', 'apollo'),
            'data'      => $model->getVenue(),
            'post_type'      => Apollo_DB_Schema::_VENUE_PT,
        ),
        'classified_id'     => array(
            'label'     => __('Classified', 'apollo'),
            'data'      => $model->getClassified(),
            'post_type'      => Apollo_DB_Schema::_CLASSIFIED,
        ),
    );

?>


<table <?php echo $none_style ?> id="apl-user-asociation-mod" class="form-table">
    <?php
    foreach ($selects as $k => $f):

        // Enable remote auto complete search
        $remote = 1;

        $postType = isset($f['post_type']) ? $f['post_type']:'';
        if ( ! Apollo_App::is_avaiable_module( $f['post_type'] )) continue;
        $id = '';
        $name = '';
        if (!empty($f['data'])) {
            $data = $f['data'];
            $id = $data->ID;
            $name = $data->post_title;
            $name .= ($data->post_status == 'pending' ? ' (*)':'');
        }
        ?>
        <tr class="form-field">
            <th scope="row"><label><?php echo $f['label'] ?> </label></th>
            <input name="uma" value="<?php echo isset( $_GET['uma']) || (isset($GLOBALS['pagenow']) && $GLOBALS['pagenow'] === 'user-new.php' ) ?>" type="hidden" >
            <td>
                <select
                    data-value="<?php echo $id ?>"
                    data-enable-remote="<?php echo $remote ?>"
                    data-post-type="<?php echo $postType == Apollo_DB_Schema::_EDUCATION ? Apollo_DB_Schema::_EDUCATOR_PT : $postType; ?>"
                    data-source-url="apollo_get_remote_associate_data_to_select2_box"
                    data-status="publish, pending"
                    id="<?php echo $k; ?>"
                    class="w-200 select apl_select2"
                    name="<?php echo $k ?>"
                >
                    <option value=""><?php _e( '-- Select --', 'apollo' ) ?></option>
                    <?php
                    if ($id) {
                        echo "<option selected  value='{$id}'>{$name}</option>";
                    }
                    ?>
                </select>
            </td>
        </tr>
        <?php
    endforeach; ?>
</table>