<div <?php echo $none_style ?> id="apl-user-asociation-mod" class="">
    <input name="uma" value="<?php echo isset( $_REQUEST['uma']) || (isset($GLOBALS['pagenow']) && $GLOBALS['pagenow'] === 'user-new.php' ) ?>" type="hidden" >
    <?php
    $agencyList = ApolloAssociationFunction::getAssociatedModules($user_id, Apollo_DB_Schema::_AGENCY_PT);
    ?>

    <?php

    $associationCheckboxesComp = apl_instance('APL_Lib_Association_Components_AssociationCheckboxes');

    if ($agencyEnable){
        $associationCheckboxesComp->render(array(
            'post_type'     => Apollo_DB_Schema::_AGENCY_PT,
            'label'         => __( 'Agency', 'apollo' ),
            'associated_id' => 'agency-id',
            'wrap_class'    => 'agency',
            'user_id' => $user_id,
            'admin_type' => 'user',
            'agency_list' => $agencyList,

        ));
    }
    if (Apollo_App::is_avaiable_module(Apollo_DB_Schema::_ARTIST_PT)){
        $associationCheckboxesComp->render(array(
            'post_type'     => Apollo_DB_Schema::_ARTIST_PT,
            'label'         => __( 'Artist', 'apollo' ),
            'associated_id' => 'artist-id',
            'wrap_class'    => 'user-association artist',
            'user_id' => $user_id,
            'admin_type' => 'user',
            'agency_list' => $agencyList,
        ));
    }
    if (Apollo_App::is_avaiable_module(Apollo_DB_Schema::_EDUCATION)){

        if (isset( $_POST['educator_id'] )) {
            $id = $_POST['educator_id'];
        }
        else {
            $id = Apollo_User::getCurrentAssociatedID(Apollo_DB_Schema::_EDUCATOR_PT, $user_id);
        }

        $educatorData = get_post($id);

        $associationCheckboxesComp->renderSingleEducator(array(
            'post_type'     => Apollo_DB_Schema::_EDUCATOR_PT,
            'label'         => __( 'Educator', 'apollo' ),
            'associated_id' => 'educator-id',
            'wrap_class'    => 'user-association educator',
            'user_id' => $user_id,
            'admin_type' => 'user',
            'agency_list' => $agencyList,
            'id'          => $id,
            'data'        => $educatorData
        ));
    }
    if (Apollo_App::is_avaiable_module(Apollo_DB_Schema::_ORGANIZATION_PT)){
        $associationCheckboxesComp->render(array(
            'post_type'     => Apollo_DB_Schema::_ORGANIZATION_PT,
            'label'         => __( 'Organization', 'apollo' ),
            'associated_id' => 'organization-id',
            'wrap_class'    => 'user-association organization',
            'user_id' => $user_id,
            'admin_type' => 'user',
            'agency_list' => $agencyList,
        ));
    }
    if (Apollo_App::is_avaiable_module(Apollo_DB_Schema::_VENUE_PT)){
        $associationCheckboxesComp->render(array(
            'post_type'     => Apollo_DB_Schema::_VENUE_PT,
            'label'         => __( 'Venue', 'apollo' ),
            'associated_id' => 'venue-id',
            'wrap_class'    => 'user-association venue',
            'user_id' => $user_id,
            'admin_type' => 'user',
            'agency_list' => $agencyList,
        ));
    }
    /**
     * @ticket #19641: [CF] 20190402 - [Classified] Add the Associated User(s) field to main admin list and detail form - Item 5
     */
    if (Apollo_App::is_avaiable_module(Apollo_DB_Schema::_CLASSIFIED)){
        $associationCheckboxesComp->render(array(
            'post_type'     => Apollo_DB_Schema::_CLASSIFIED,
            'label'         => __( 'Classified', 'apollo' ),
            'associated_id' => 'classified-id',
            'wrap_class'    => 'user-association classified',
            'user_id' => $user_id,
            'admin_type' => 'user',
            'agency_list' => $agencyList,
        ));
    }
    ?>
</div>