<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class Apollo_Admin_Account {

    private $confirm = true;

    public function __construct() {

        $arrFilterDefinition = array(
            'user_contactmethods' => array(
                'priority'  => 10,
                'num_param' => 2
            ),

            // Send notification when add new user
            'wpmu_signup_user_notification_email' => array(
                'priority'  => 11,
                'num_param' => 4
            ),

            // Send the notification email
            'wpmu_welcome_user_notification' => array(
                'priority'  => 10,
                'num_param' => 3
            ),
        );

        foreach($arrFilterDefinition as $filter_name => $arr) {
            add_filter($filter_name, array($this, $filter_name), $arr['priority'], $arr['num_param']);
        }

        $arr_actions = array(
            'user_new_form' => 'asociate_modules_form',
            'show_user_profile' => 'asociate_modules_form',
            'edit_user_profile' => 'asociate_modules_form',

            'edit_user_profile_update'  => 'edit_user_profile_update',
            'user_register' => 'edit_user_profile_update',
            'profile_update'    => 'edit_user_profile_update',
        );

        foreach ($arr_actions as $action_name => $fn) {
            add_action($action_name, array( $this, $fn ));
        }

        /**
         * Allow administrator can edit user
         */
        add_filter( 'map_meta_cap', array($this, 'mc_admin_users_caps'), 1, 4 );
        remove_all_filters( 'enable_edit_any_user_configuration' );
        add_filter( 'enable_edit_any_user_configuration', '__return_true');
        add_filter( 'admin_head', array($this, 'mc_edit_permission_check'), 1, 4 );

    }

    function wpmu_welcome_user_notification($user_id, $password, $meta='') {
        $user       = new WP_User($user_id);

        if ( isset( $_SESSION['apl_new_user_pass'] ) && $_SESSION['apl_new_user_pass'] ) {
            $plaintext_pass = $_SESSION['apl_new_user_pass'];
            unset($_SESSION['apl_new_user_pass']);
        }

        $first_name = $user->first_name;
        $last_name  = $user->last_name;
        $user_login = stripslashes($user->user_login);
        $user_email = stripslashes($user->user_email);
        do_action( 'apollo_email_registration', $first_name, $last_name, $user_login, $plaintext_pass, $user_email);
    }

    public function user_contactmethods($method, $user) {
        return array_merge($method, array(
                'zip_code' => __( 'Zip Code', 'apollo' )
            ));
    }

    /**
     * Override the content of sign up email
     */
    public function wpmu_signup_user_notification_email($content, $username, $email, $activateKey) {

        if ( isset($_SESSION['apl_noconfirm']) ) {
            unset($_SESSION['apl_noconfirm']);
        }

        // Send email confirmation
        if ( isset( $_POST['noconfirmation'] ) && !is_super_admin() ) {
            global $wpdb;
            $_SESSION['apl_noconfirm'] = true;
            $password = $_POST['pass1'];
            $first_name = $_POST['first_name'];
            $last_name = $_POST['last_name'];
            $wpdb->escape_by_ref($password);
            $wpdb->escape_by_ref($first_name);
            $wpdb->escape_by_ref($last_name);

            $userdata = array(
                'user_login'        => $username,
                'user_pass'         => $password,
                'user_nicename'     => $username,
                'user_email'        => $email,
                'user_url'          => '#',
                'user_regristerd'   => date('c'),
                'display_name'      => $username,
                'first_name'        => $first_name,
                'last_name'         => $last_name,
            );
            $user_id = wp_insert_user($userdata);

            $agency_id      = isset( $_POST['agency_id'] ) ? $_POST['agency_id'] : '';
            $artist_id      = isset( $_POST['artist_id'] ) ? $_POST['artist_id'] : '';
            $educator_id    = isset( $_POST['educator_id'] ) ? $_POST['educator_id'] : '';
            $org_id         = isset( $_POST['org_id'] ) ? $_POST['org_id'] : '';
            $venue_id       = isset( $_POST['venue_id'] ) ? $_POST['venue_id'] : '';
            $wpdb->escape_by_ref($agency_id);
            $wpdb->escape_by_ref($artist_id);
            $wpdb->escape_by_ref($educator_id);
            $wpdb->escape_by_ref($org_id);
            $wpdb->escape_by_ref($venue_id);

            $apl_query = new Apl_Query( $wpdb->{Apollo_Tables::_APL_AGENCY} );
            $apl_query->insert( array(
                'blog_id'       => get_current_blog_id(),
                'user_id'       => $user_id,
                'agency_id'     => $agency_id,
                'artist_id'     => $artist_id,
                'educator_id'    => $educator_id,
                'org_id'        => $org_id,
                'venue_id'      => $venue_id,
            ) );

            do_action( 'apollo_email_registration', $first_name, $last_name, $username, $password, $email);

            return false;
        }

        do_action( 'apollo_email_confirm_regis_email', $username, $activateKey, $email );
        return false;
    }

    public function asociate_modules_form($user) {
        if(defined('WP_NETWORK_ADMIN') && WP_NETWORK_ADMIN === true) {
            return false;
        }

        /** @Ticket #18744 */
        $this->showCountryField($user);

        $this->show_registered_date_fields($user);
        // add upload profile image fide
        $this->show_profile_image_field($user);

        $disabled = $user === 'add-new-user' ? 'disabled' : '';

        global $wpdb;
        $user_id = isset( $_POST['user_id'] ) ? $_POST['user_id'] : ( $user && is_object( $user ) ? $user->ID : '' );



        $enableEventImport = Apollo_App::get_network_event_import_tool();

        ?>
        <script>

            jQuery(function($) {

            <?php if ( isset( $_SESSION['apl_noconfirm'] ) && $_SESSION['apl_noconfirm'] ) {
             ?>
                    $('#message').hide();
            <?php
            } ?>


            var firstNameField = '<tr class="form-field cus-first_name">\n\
                    <th scope="row"><label for="first_name"><?php _e('First name'); ?> </label></th>\n\
                    <td>\n\
                        <input class="hidden" value=" " /><!-- #24364 workaround -->\n\
                        <input <?php if ( !isset( $_POST['noconfirmation'] ) ) echo 'disabled' ?> name="first_name" type="text" value="<?php if (isset($_POST['first_name'])) echo $_POST['first_name'] ?>" id="first_name" autocomplete="off" />\n\
                    </td>\n\
                </tr>'

             var lastNameField = '<tr class="form-field cus-last_name">\n\
                    <th scope="row"><label for="last_name"><?php _e('Last name'); ?> </label></th>\n\
                    <td>\n\
                        <input class="hidden" value=" " /><!-- #24364 workaround -->\n\
                        <input <?php if ( !isset( $_POST['noconfirmation'] ) ) echo 'disabled' ?> name="last_name" type="text" value="<?php if (isset($_POST['last_name'])) echo $_POST['last_name'] ?>" id="last_name" autocomplete="off" />\n\
                    </td>\n\
                </tr>'

                if ( ! $('#noconfirmation').length ) {
                    $('#createuser table').first().children('tbody').append('<tr>\n\
                        <th scope="row"><label for="noconfirmation"><?php _e('Skip Confirmation Email', 'apollo') ?></label></th>\n\
                        <td><label for="noconfirmation"><input <?php if ( isset( $_POST['noconfirmation'] ) ) echo 'checked' ?> type="checkbox" name="noconfirmation" id="noconfirmation" value="1"> \n\
                        <?php _e( 'Add the user without sending an email that requires their confirmation.', 'apollo' ) ?></label></td>\n\
                    </tr>\n\
                    ');
                }


                if ( ! $('#noconfirmation').length ) {
                    $('.form-field.cus-password').hide();
                    $('.form-field.cus-repassword').hide();
                }


                if ( ! $('#createuser table tr.user-pass1-wrap').length ) {
                    $('#createuser table').first().children('tbody').append('\n\
                    <tr id="password" class="user-pass1-wrap">\n\
                        <th><label for="pass1"><?php _e( 'New Password' ); ?></label></th>\n\
                        <td>\n\
                            <input class="hidden" value=" " />\n\
                            <button <?php if ( !isset( $_POST['noconfirmation'] ) ) echo 'disabled' ?> type="button" class="button button-secondary wp-generate-pw hide-if-no-js"><?php _e( 'Generate Password' ); ?></button>\n\
                            <div class="wp-pwd hide-if-js">\n\
                                <span class="password-input-wrapper">\n\
                                    <input type="password" name="pass1" id="pass1" class="regular-text" value="" autocomplete="off" data-pw="<?php echo esc_attr( wp_generate_password( 24 ) ); ?>" aria-describedby="pass-strength-result" />\n\
                                </span>\n\
                                <button type="button" class="button button-secondary wp-hide-pw hide-if-no-js" data-toggle="0" aria-label="<?php esc_attr_e( 'Hide password' ); ?>">\n\
                                    <span class="dashicons dashicons-hidden"></span>\n\
                                    <span class="text"><?php _e( 'Hide' ); ?></span>\n\
                                </button>\n\
                                <button type="button" class="button button-secondary wp-cancel-pw hide-if-no-js" data-toggle="0" aria-label="<?php esc_attr_e( 'Cancel password change' ); ?>">\n\
                                    <span class="text"><?php _e( 'Cancel' ); ?></span>\n\
                                </button>\n\
                                <div style="display:none" id="pass-strength-result" aria-live="polite"></div>\n\
                            </div>\n\
                        </td>\n\
                    </tr>');
                }

                 if ( ! $('#createuser table tr.pw-weak').length ) {
                    $('#createuser table').first().children('tbody').append('\n\
                    <tr class="pw-weak">\n\
                        <th><?php _e( 'Confirm Password' ); ?></th>\n\
                        <td>\n\
                            <label>\n\
                                <input type="checkbox" name="pw_weak" class="pw-checkbox" />\n\
                                <?php _e( 'Confirm use of weak password' ); ?>\n\
                            </label>\n\
                        </td>\n\
                    </tr>');
                }


                if(! $('#createuser table tr.cus-first_name').length){
                    $('#createuser table').first().children('tbody').append(firstNameField);
                }
                if(! $('#createuser table tr.cus-last_name').length){

                    $('#createuser table').first().children('tbody').append(lastNameField);
                }


                $( 'form #noconfirmation' ).click(function() {
                    var $this = $(this);

                    if ( $this.attr('checked') != 'checked' ) {
                        $('.wp-generate-pw').attr('disabled', 'disabled');
                        $('#first_name').attr('disabled', 'disabled');
                        $('#last_name').attr('disabled', 'disabled');

                        $('#first_name').val('');
                        $('#last_name').val('');

                        $('#apl-user-asociation-mod select').attr('disabled', 'disabled');
                        $('#apl-user-asociation-mod select').val('');

                        $('.wp-pwd .wp-cancel-pw').trigger('click');

                    } else {
                         $('.wp-generate-pw').removeAttr('disabled');
                        $('#first_name').removeAttr('disabled');
                        $('#last_name').removeAttr('disabled');

                        $('#apl-user-asociation-mod select').removeAttr('disabled');
                    }
                });

                if ( $( '#noconfirmation' ).attr('checked') == 'checked' ) {
                    $('#apl-user-asociation-mod select').removeAttr('disabled');
                    $('.cus-password').addClass('form-required');
                    $('.cus-repassword').addClass('form-required');
                    $('.cus-password input').removeAttr('disabled');
                    $('.cus-repassword input').removeAttr('disabled');
                }

            });

            <?php
                $general_active = ! isset( $_REQUEST['uma'] ) && ! isset( $_REQUEST['apollo_config'] ) ? 'class="active"' : '';
                $asociation_active = isset( $_REQUEST['uma'] ) ? 'class="active"' : '';
                $apollo_configuration_active = isset( $_REQUEST['apollo_config'] ) ? 'class="active"' : '';
            ?>
                jQuery( '#profile-page h1' ).after( '<ul><li <?php echo $general_active ?> >\n\
                <a href="<?php echo admin_url() ?>user-edit.php?user_id=<?php echo $user_id ?>"><?php _e( 'General Infomation', 'apollo' ) ?></a></li>\n\
                <li <?php echo $asociation_active ?> ><a href="<?php echo admin_url() ?>user-edit.php?user_id=<?php echo $user_id ?>&uma=true"><?php _e( 'User Association', 'apollo' ) ?></a></li>\n\
                <li  <?php echo $apollo_configuration_active ?> ><a href="<?php echo admin_url() ?>user-edit.php?user_id=<?php echo $user_id ?>&apollo_config=true"><?php _e( 'Apollo Config', 'apollo' ) ?></a></li>\n\
                    </ul>' );

            <?php
                if ( isset( $_REQUEST['uma'] ) ) { ?>
                    jQuery('form#your-profile').children().hide();
                    jQuery('#apl-user-asociation-mod').show();
            <?php
                }
                if ( isset( $_REQUEST['apollo_config'] ) ) { ?>
                    jQuery('form#your-profile').children().hide();
                    jQuery('#apl-user-apollo-config').show();
            <?php
                }
            ?>
        </script>
        <?php $none_style = $user_id && ! isset( $_REQUEST['uma'] ) ? 'style="display: none"' : '' ?>
        <h3 <?php echo $none_style ?> ><?php _e( 'User Association', 'apollo' ) ?></h3>

        <?php
        $agencyEnable = Apollo_App::is_avaiable_module(Apollo_DB_Schema::_AGENCY_PT);
        if ($agencyEnable) :
            include APOLLO_ADMIN_SRC. '/user/view/form/multiple-selections.php';
        else :
            include APOLLO_ADMIN_SRC. '/user/view/form/single-selection.php';
        endif; ?>


        <!-- tab content Apollo Config -->
        <?php $aplConfig_none_style = $user_id && ! isset( $_REQUEST['apollo_config'] ) ? 'style="display: none"' : '' ?>

        <h3 <?php echo $aplConfig_none_style ?> ><?php _e( 'Apollo Config', 'apollo' ) ?></h3>
        <table <?php echo $aplConfig_none_style ?> id="apl-user-apollo-config" class="form-table">
            <input name="apollo_config" value="<?php echo (isset( $_GET['apollo_config']) || (isset($GLOBALS['pagenow']) && $GLOBALS['pagenow'] === 'user-new.php' )); ?>" type="hidden" >
            <?php if ($enableEventImport): ?>
            <tr class="form-field">
                <th scope="row"><label><?php _e('Enable/Disable Import Event Tool','apollo'); ?> </label></th>
                <td>
                    <?php
                    $listOfUserCan = get_site_option(Apollo_Tables::_APL_TOGGLE_EVENT_IMPORT_TOOL, array());
                    $currentUserId = (isset($_GET['user_id']) && !empty($_GET['user_id'])) ? $_GET['user_id'] : -1;
                    $canUseEITool = in_array($currentUserId, $listOfUserCan);
                    ?>
                    <input type="checkbox" value="1" name="user-can-use-event-import-tool" <?php echo $canUseEITool ? 'checked="checked"' : ''; ?> />
                </td>
            </tr>
            <?php endif; ?>
            <tr class="form-field">
                <th scope="row"><label><?php _e('Bypass Pending Approval','apollo'); ?> </label></th>
                <td>
                    <?php
                    $listOfUserCan = get_site_option(Apollo_Tables::_APL_BYPASS_PENDING_APPROVAL, array());
                    $currentUserId = (isset($_GET['user_id']) && !empty($_GET['user_id'])) ? $_GET['user_id'] : -1;
                    $canByPassPendingApproval = in_array($currentUserId, $listOfUserCan);
                    ?>
                    <input type="checkbox" value="1" name="user-can-bypass-pending-approval" <?php echo $canByPassPendingApproval ? 'checked="checked"' : ''; ?> />
                </td>
            </tr>
        </table>
        <!-- end tab content Apollo Config -->

    <?php }

    public function edit_user_profile_update( $user_id ) {
        // add new and edit user profile is in here .....
        // update normally with user id for all extra options of user.
        if (defined('WP_NETWORK_ADMIN') && WP_NETWORK_ADMIN === true) {
            return false;
        }

        if (isset($_POST['country']) && $_POST['country'] != '')
            update_user_meta($user_id, 'country', $_POST['country']);

        // save user profile image (Using the_author_meta('image') to get image)
        $this->save_profile_image_field($user_id);

        if ((!isset($_REQUEST['uma']) || $_REQUEST['uma'] !== '1') && (!isset($_REQUEST['apollo_config']) || $_REQUEST['apollo_config'] !== '1')) {
            return false;
        }

        if (!current_user_can('edit_user', $user_id)) {
            return false;
        }


        /* update user option for enable/disable of event import tool */
        if (isset($_POST['user-can-use-event-import-tool']) && !empty($_POST['user-can-use-event-import-tool'])) {
            Apollo_Admin_Users_Action::apollo_enable_import_event_tool_by_array_user_ids(array($user_id));
        } else {
            Apollo_Admin_Users_Action::apollo_disable_import_event_tool_by_array_user_ids(array($user_id));
        }

        if (isset($_POST['user-can-bypass-pending-approval']) && !empty($_POST['user-can-bypass-pending-approval'])) {
            Apollo_Admin_Users_Action::apollo_enable_bypass_pending_approval_by_array_user_ids(array($user_id));
        } else {
            Apollo_Admin_Users_Action::apollo_disable_bypass_pending_approval_by_array_user_ids(array($user_id));
        }

        global $wpdb;
        $_user_id = isset($_POST['user_id']) ? $_POST['user_id'] : $user_id;


        /** @Ticket #14913 */
        /** Update usermeta */
        if (Apollo_App::is_avaiable_module(Apollo_DB_Schema::_AGENCY_PT)) {
            if (isset($_POST['agency-id-checked']) && isset($_POST['agency-id-unchecked'])) {
                $this->updateAssociation($_user_id, Apollo_DB_Schema::_AGENCY_PT, $_POST['agency-id-checked'], $_POST['agency-id-unchecked']);
            }
            if (isset($_POST['artist-id-checked']) && isset($_POST['artist-id-unchecked'])) {
                $this->updateAssociation($_user_id, Apollo_DB_Schema::_ARTIST_PT, $_POST['artist-id-checked'], $_POST['artist-id-unchecked']);
            }
//            if (isset($_POST['educator-id-checked']) && isset($_POST['educator-id-unchecked'])) {
//                $this->updateAssociation($_user_id, Apollo_DB_Schema::_EDUCATOR_PT, $_POST['educator-id-checked'], $_POST['educator-id-unchecked']);
//            }
            if (isset($_POST['educator_id'])) {
                self::updateUserBLogEducator($_user_id);
            }
            if (isset($_POST['organization-id-checked']) && isset($_POST['organization-id-unchecked'])) {
                $this->updateAssociation($_user_id, Apollo_DB_Schema::_ORGANIZATION_PT, $_POST['organization-id-checked'], $_POST['organization-id-unchecked']);
            }
            if (isset($_POST['venue-id-checked']) && isset($_POST['venue-id-unchecked'])) {
                $this->updateAssociation($_user_id, Apollo_DB_Schema::_VENUE_PT, $_POST['venue-id-checked'], $_POST['venue-id-unchecked']);
            }
            /**
             * @ticket #19641: [CF] 20190402 - [Classified] Add the Associated User(s) field to main admin list and detail form - Item 5
             */
            if (isset($_POST['classified-id-checked']) && isset($_POST['classified-id-unchecked'])) {
                $this->updateAssociation($_user_id, Apollo_DB_Schema::_CLASSIFIED, $_POST['classified-id-checked'], $_POST['classified-id-unchecked']);
            }
        } else {
            /* Update for single */
            self::updateSingleAssociation($_user_id);
        }


        /** @Ticket #13029 */

        // remote the wp_signups
        if (isset($_POST['noconfirmation']) && $_POST['noconfirmation'] == 1) {
            $userInfo = get_user_by('id', $user_id);
            $apl_query = new Apl_query($wpdb->base_prefix . 'signups', true);
            $apl_query->delete("user_email = '" . $userInfo->data->user_email . "'");
        }

        global $pagenow;
        if ($pagenow == 'user-new.php' && isset($_POST['pass1']) && $_POST['pass1'] && isset($_POST['noconfirmation']) && $_POST['noconfirmation'] == 1) {
            $pass = $_POST['pass1'];
            $wpdb->escape_by_ref($pass);
            wp_set_password($pass, $user_id);
        }

        //update user first name, user last name
        if (isset($_POST['last_name']) && $_POST['last_name'] != '')
            update_user_meta($user_id, 'last_name', $_POST['last_name']);
        if (isset($_POST['first_name']) && $_POST['first_name'] != '')
            update_user_meta($user_id, 'first_name', $_POST['first_name']);


        if (isset($_POST['uma']) && $_POST['uma'] == '1'){
            Apollo_App::safeRedirect(admin_url() . 'user-edit.php?user_id=' . $user_id . '&uma=true&updated=1');
            exit;
        }
        if (isset($_POST['apollo_config']) && ($_POST['apollo_config'] == '1')) {
            Apollo_App::safeRedirect(admin_url() . 'user-edit.php?user_id=' . $user_id . '&apollo_config=true&updated=1');
            exit;
        }


    }

    /**
     * Update association for user
     * @param $userID
     * @param $post_type
     * @param $listChecked
     * @param $listUnchecked
     */
    private function updateAssociation($userID, $post_type, $listChecked, $listUnchecked) {
        if (empty($listChecked) && empty($listUnchecked)) {
            return;
        }
        $listUnchecked = !empty($listUnchecked) ? explode(",", $listUnchecked) : array();
        $listChecked = !empty($listChecked) ? explode(",", $listChecked) : array();
        $listRemove = array_merge($listUnchecked, $listChecked);
        $apl_query = new Apl_Query(Apollo_Tables::_APL_USER_MODULES);

        if (!empty($listRemove)) {
            $listIDs = implode(",", $listRemove);
            $apl_query->delete(" user_id = {$userID} AND post_id IN ( {$listIDs} ) AND  post_type = '{$post_type}'");
        }
        if (!empty($listChecked)) {
            foreach ($listChecked as $item) {
                $apl_query->insert(array(
                    'user_id' => $userID,
                    'post_id' => $item,
                    'post_type' => $post_type
                ));
            }
        }

    }

    /**
     * Update data for wp_apollo_user_modules table when disable agency module
     * @param $userID
     */
    private function updateSingleAssociation ( $userID ) {
        global $wpdb;
        $agency_id = isset($_POST['agency_id']) ? $_POST['agency_id'] : '';
        $artist_id = isset($_POST['artist_id']) ? $_POST['artist_id'] : '';
        $educator_id = isset($_POST['educator_id']) ? $_POST['educator_id'] : '';
        $org_id = isset($_POST['org_id']) ? $_POST['org_id'] : '';
        $venue_id = isset($_POST['venue_id']) ? $_POST['venue_id'] : '';
        $classified_id = isset($_POST['classified_id']) ? $_POST['classified_id'] : '';
        $wpdb->escape_by_ref($agency_id);
        $wpdb->escape_by_ref($artist_id);
        $wpdb->escape_by_ref($educator_id);
        $wpdb->escape_by_ref($org_id);
        $wpdb->escape_by_ref($venue_id);
        $wpdb->escape_by_ref($classified_id);
        $data = array(
            array(
                'post_id' => $agency_id,
                'post_type' => Apollo_DB_Schema::_AGENCY_PT
            ),
            array(
                'post_id' => $artist_id,
                'post_type' => Apollo_DB_Schema::_ARTIST_PT
            ),
            array(
                'post_id' => $educator_id,
                'post_type' => Apollo_DB_Schema::_EDUCATOR_PT
            ),
            array(
                'post_id' => $org_id,
                'post_type' => Apollo_DB_Schema::_ORGANIZATION_PT
            ),
            array(
                'post_id' => $venue_id,
                'post_type' => Apollo_DB_Schema::_VENUE_PT
            ),
            array(
                'post_id' => $classified_id,
                'post_type' => Apollo_DB_Schema::_CLASSIFIED
            ),
        );

        $apl_query = new Apl_Query(Apollo_Tables::_APL_USER_MODULES);
        $apl_query->delete(" user_id={$userID} ");
        foreach ($data as $item) {
            if ($item['post_id']) {
                $apl_query->insert(array(
                    'user_id' => $userID,
                    'post_id' => $item['post_id'],
                    'post_type' => $item['post_type']
                ));
            }
        }
    }

    /**
     * Update educator for wp_apollo_blog_user table when disable agency module
     * @param $user_id
     */
    private function updateUserBLogEducator($user_id) {
        global $wpdb;
        $educator_id = isset($_POST['educator_id']) ? $_POST['educator_id'] : '';
        $wpdb->escape_by_ref($educator_id);
        $apl_query = new Apl_Query(Apollo_Tables::_APL_USER_MODULES);
        $post_type = Apollo_DB_Schema::_EDUCATOR_PT;
        $apl_query->delete(" user_id = {$user_id} AND post_type = '{$post_type}' ");
        if ($educator_id) {
            $apl_query->insert(array(
                'user_id' => $user_id,
                'post_id' => $educator_id,
                'post_type' => Apollo_DB_Schema::_EDUCATOR_PT
            ));
        }

    }

    function mc_admin_users_caps( $caps, $cap, $user_id, $args ){

        foreach( $caps as $key => $capability ){

            if( $capability != 'do_not_allow' )
                continue;

            switch( $cap ) {
                case 'edit_user':
                case 'edit_users':
                    $caps[$key] = 'edit_users';
                    break;
                case 'delete_user':
                case 'delete_users':
                    $caps[$key] = 'delete_users';
                    break;
//                case 'create_users':
//                    $caps[$key] = $cap;
//                    break;
            }
        }

        return $caps;
    }

    /**
    * Checks that both the editing user and the user being edited are
    * members of the blog and prevents the super admin being edited.
    */
    function mc_edit_permission_check() {
        global $current_user, $profileuser;

        $screen = get_current_screen();

        wp_get_current_user();

        if( ! is_super_admin( $current_user->ID ) && in_array( $screen->base, array( 'user-edit', 'user-edit-network' ) ) ) { // editing a user profile
            if ( is_super_admin( $profileuser->ID ) ) { // trying to edit a superadmin while less than a superadmin
                wp_die( '<p id="apl-error-page">'. __( 'You do not have permission to edit this user.', 'apollo' ). '</p>' );
            } elseif ( ! ( is_user_member_of_blog( $profileuser->ID, get_current_blog_id() ) && is_user_member_of_blog( $current_user->ID, get_current_blog_id() ) )) { // editing user and edited user aren't members of the same blog
                wp_die( '<p id="apl-error-page">'. __( 'You do not have permission to edit this user.', 'apollo' ). '</p>' );
            }
        }

    }

    private function show_profile_image_field($user) { ?>

        <h3><?php _e( 'Profile Image', 'apollo' ); ?></h3>
        <table id="user_images_container" data-title="<?php _e('Select images','apollo');?>" data-text="<?php _e('Use selected images','apollo');?>">
            <tr>
                <th>
                </th>
                <td class="event_images">
                    <?php
                    if (isset($user->ID)) :
                        $attachment_id = get_user_meta( $user->ID, 'image', true );

                        if ( $attachment_id )
                            echo '<div class="image" data-attachment_id="' . esc_attr($attachment_id) . '">
                                        ' . wp_get_attachment_image($attachment_id, 'large') . '
                                         <p><a href="#" class="delete " title="' . __('Delete image', 'apollo') . '" data-tip="' . __('Delete image', 'apollo') . '">' . __('Delete', 'apollo') . '</a></p>
                                    </div>';
                        ?>
                    <?php endif; ?>
                </td>

                <input type="hidden" id="user_image" name="user_image" value="<?php echo isset($attachment_id) ? esc_attr( $attachment_id ) : ''; ?>" />
                <p class="user_image hide-if-no-js">
                    <a href="#" class="add-image" data-choose="<?php _e( 'Add User Image', 'apollo' ); ?>" data-update="<?php _e( 'Set Image', 'apollo' ); ?>" data-delete="<?php _e( 'Delete image', 'apollo' ); ?>" data-text="<?php _e( 'Delete', 'apollo' ); ?>"><?php _e( 'Add  image', 'apollo' ); ?></a>
                </p>
            </tr>

        </table>

    <?php }

    private function showCountryField($user) {
        $countriesList = Apollo_App::getAllCountries();
        if (!empty($user->ID)) {
            $userMeta = get_user_meta($user->ID, 'country', true);
        }
        $selectedCountry = !empty($userMeta) ? $userMeta : 'US'; ?>
        <table class="form-table">
            <tr class="form-field cus-country">
                <th ><label for="country"><?php _e('Country'); ?></label><span class="description"><?php _e('(required)', 'apollo'); ?></span></th>
                <td>
                    <input class="hidden" value=" " />
                    <?php echo Apollo_App::renderCountryHtml($countriesList, $selectedCountry); ?>
                </td>
            </tr>
        </table>
        <?php
    }

    private function show_registered_date_fields($user){
        if($user && isset($user->ID)){
        ?>
        <table class="form-table">
            <tr>
                <th>
                    <label><?php _e('User Registered','apollo') ?></label>
                </th>
                <td class="">
                    <?php echo Apollo_Admin_Users_Action::getUserRegisteredDate($user->ID); ?>
                </td>
            </tr>

        </table>
        <?php
        }
    }

    private  function save_profile_image_field($user_id ) {
        if ( !current_user_can( 'edit_user', $user_id ) )
        {
            return false;
        }
        update_user_meta( $user_id, 'image', $_POST[ 'user_image' ] );
    }

}

new Apollo_Admin_Account();
