<?php


class ApolloAdminUserSingleSelectionModel {

    protected $artist;
    protected $venue;
    protected $org;
    protected $educator;
    protected $classified;

    public function __construct($user)
    {
        $user_id = isset( $_POST['user_id'] ) ? $_POST['user_id'] : ( $user && is_object( $user ) ? $user->ID : '' );
        $artistID = Apollo_User::getCurrentAssociatedID(Apollo_DB_Schema::_ARTIST_PT, $user_id);
        $venueID = Apollo_User::getCurrentAssociatedID(Apollo_DB_Schema::_VENUE_PT, $user_id);
        $orgID = Apollo_User::getCurrentAssociatedID(Apollo_DB_Schema::_ORGANIZATION_PT, $user_id);
        $educatorID = Apollo_User::getCurrentAssociatedID(Apollo_DB_Schema::_EDUCATOR_PT, $user_id);
        /**
         * @ticket #19641: [CF] 20190402 - [Classified] Add the Associated User(s) field to main admin list and detail form - Item 5
         */
        $classifiedID = Apollo_User::getCurrentAssociatedID(Apollo_DB_Schema::_CLASSIFIED, $user_id);

        $this->setArtist(get_post($artistID));
        $this->setVenue(get_post($venueID));
        $this->setOrg(get_post($orgID));
        $this->setEducator(get_post($educatorID));
        $this->setClassified(get_post($classifiedID));
    }

    /**
     * @return mixed
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * @param mixed $artist
     */
    public function setArtist($artist)
    {
        $this->artist = $artist;
    }

    /**
     * @return mixed
     */
    public function getVenue()
    {
        return $this->venue;
    }

    /**
     * @param mixed $venue
     */
    public function setVenue($venue)
    {
        $this->venue = $venue;
    }

    /**
     * @return mixed
     */
    public function getOrg()
    {
        return $this->org;
    }

    /**
     * @param mixed $org
     */
    public function setOrg($org)
    {
        $this->org = $org;
    }

    /**
     * @return mixed
     */
    public function getEducator()
    {
        return $this->educator;
    }

    /**
     * @param mixed $educator
     */
    public function setEducator($educator)
    {
        $this->educator = $educator;
    }

    /**
     * @return mixed
     */
    public function getClassified()
    {
        return $this->classified;
    }

    /**
     * @param mixed $venue
     */
    public function setClassified($classified)
    {
        $this->classified = $classified;
    }
}