<?php
/**
* Apollo Meta Box Functions
*
* @author      vulh
* @category    Core
* @package     inc/admin/Functions
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Output a info field box.
 *
 * @access public
 * @param array $field
 * @return void
 */
function apollo_wp_info_field( $field ) {
	global $thepostid, $post, $apollo;

	$thepostid              = !empty( $thepostid ) ? $thepostid : 0;
	if(intval($thepostid) === 0 && !empty($post) && is_object($post)){
		$thepostid = $post->ID;
	}
	$field['class']         = isset( $field['class'] ) ? $field['class'] : 'short';
	$field['wrapper_class'] = isset( $field['wrapper_class'] ) ? $field['wrapper_class'] : '';
	$field['text']         = isset( $field['text'] ) ? $field['text'] : '';
	$field['label']         = isset( $field['label'] ) ? $field['label'] : '';
	$field['description']         = isset( $field['description'] ) ? $field['description'] : '';
	$field['id'] = isset( $field['id'] ) ? $field['id'] : '';

	if(isset($field['label'])){
		$text = '<label for="' . esc_attr( $field['id'] ) . '">' . wp_kses_post( $field['label'] ) . '</label>';
	} else {
		$text = '';
	}
	$text .= '<span class="'.$field['class'].'"> '.$field['text'].' </span>';

	echo $text;

	if ( ! empty( $field['description'] ) ) {
		echo '<br><span class="description">' . wp_kses_post( $field['description'] ) . '</span>';
	}
}

function apollo_wp_color_field( $field ) {
	global $thepostid, $post, $apollo;
	$default_color = '';
	$val        = isset( $field['value'] ) ? $field['value'] : Apollo_App::apollo_get_meta_data( $thepostid, $field['id'] );
	if(!$val){
		$val = $field['std'];
	}
	if ( isset($value['std']) ) {
		if ( $val !=  $value['std'] )
			$default_color = ' data-default-color="' .$field['std'] . '" ';
	}
	if(isset($field['label'])){
		$text = '<label for="' . esc_attr( $field['id'] ) . '">' . wp_kses_post( $field['label'] ) . '</label>';
	} else {
		$text = '';
	}
	$text .= '<input name="' . $field['id']  . '" id="' . esc_attr( $field['id'] ) . '" class="of-color"  type="text" value="' . esc_attr( $val ) . '"' . $default_color .' />';
	echo $text;
}



/**
 * Output a text input box.
 *
 * @access public
 * @param array $field
 * @return void
 */
function apollo_wp_text_input( $field ) {
	global $thepostid, $post, $apollo;

	$thepostid              = !empty( $thepostid ) ? $thepostid : 0;
	if(intval($thepostid) === 0 && !empty($post) && is_object($post)){
		$thepostid = $post->ID;
	}
	$field['placeholder']   = isset( $field['placeholder'] ) ? $field['placeholder'] : '';
	$field['class']         = isset( $field['class'] ) ? $field['class'] : 'short';
	$field['wrapper_class'] = isset( $field['wrapper_class'] ) ? $field['wrapper_class'] : '';
    $field['name']          = isset( $field['name'] ) ? $field['name'] : $field['id'];
	$field['value']         = isset( $field['value'] ) ? $field['value'] : trim(Apollo_App::apollo_get_meta_data( $thepostid, $field['name'] ));
	$field['type']          = isset( $field['type'] ) ? $field['type'] : 'text';
    $field['data_elm']      = isset( $field['data_elm'] ) ? $field['data_elm'] : 'data_elm';
	$data_type              = empty( $field['data_type'] ) ? '' : $field['data_type'];

    if ( ! $field['value'] && isset( $field['default'] ) ) $field['value'] = $field['default'];

	switch ( $data_type ) {
		case 'price' :
			$field['class'] .= ' apollo_input_price';
			$field['value']  = Apollo_App::apollo_format_localized_price( $field['value'] );
		break;
		case 'decimal' :
			$field['class'] .= ' apollo_input_decimal';
			$field['value']  = Apollo_App::apollo_format_localized_decimal( $field['value'] );
		break;
	}

	// Custom attribute handling
	$custom_attributes = array();

	if ( ! empty( $field['custom_attributes'] ) && is_array( $field['custom_attributes'] ) )
		foreach ( $field['custom_attributes'] as $attribute => $value )
			$custom_attributes[] = esc_attr( $attribute ) . '="' . esc_attr( $value ) . '"';

    $require_class = isset( $field['required'] ) && $field['required'] == 'yes' ? 'required' : '';

	echo '<p class="form-field ' . esc_attr( $field['id'] ) . '_field ' . esc_attr( $field['wrapper_class'] ) . '">';

    echo '<label for="' . esc_attr( $field['id'] ) . '">' . wp_kses_post( $field['label'] ) . '</label>&nbsp;'
        . '<input '.$field['data_elm'].' data-parent="'.(isset( $field["data_parent"] ) ? $field["data_parent"] : "").'"'
        . ' data-compare="'.(isset( $field["data_compare"] ) ? $field["data_compare"] : "").'" type="' . esc_attr( $field['type'] ) . '" class=" '.$require_class.' ' . esc_attr( $field['class'] ) . '" name="' . esc_attr( $field['name'] ) . '" id="' . esc_attr( $field['id'] ) . '" value="' . esc_attr( $field['value'] ) . '" placeholder="' . esc_attr( $field['placeholder'] ) . '" ' . implode( ' ', $custom_attributes ) . ' /> ';
    apollo_the_required_field( $field );
	if ( ! empty( $field['description'] ) ) {
		echo '<br><span class="description">' . wp_kses_post( $field['description'] ) . '</span>';
	}
	echo '</p>';
}

function apollo_the_required_field( $field = '', $return = false ) {

    if ( ! $field || ( isset( $field['required'] ) && $field['required'] ) ) {
        $error = '<span class="re-star">*</span>';
        if ( $return ) return $error;
        echo $error;
    }
}

/**
 * Output a hidden input box.
 *
 * @access public
 * @param array $field
 * @return void
 */
function apollo_wp_hidden_input( $field ) {
	global $thepostid, $post;

	$thepostid              = !empty( $thepostid ) ? $thepostid : 0;
	if(intval($thepostid) === 0 && !empty($post) && is_object($post)){
		$thepostid = $post->ID;
	}
	$field['value'] = isset( $field['value'] ) ? $field['value'] : get_apollo_meta( $thepostid, $field['id'], true );
	$field['class'] = isset( $field['class'] ) ? $field['class'] : '';
    $require_class = isset( $field['required'] ) && $field['required'] == 'yes' ? 'required' : '';
    $name = isset( $field['name'] ) ? esc_attr( $field['name'] ) : esc_attr( $field['id'] );
    $extend_name = isset( $field['extend_name'] ) ? $field['extend_name'] : '';
	echo '<input data-name="'.$extend_name.'" type="hidden" class="'.$require_class.'  ' . esc_attr( $field['class'] ) . '" name="' . $name . '" id="' . esc_attr( $field['id'] ) . '" value="' . esc_attr( $field['value'] ) .  '" /> ';
}

/**
 * Output a textarea input box.
 *
 * @access public
 * @param array $field
 * @return void
 */
function apollo_wp_textarea_input( $field ) {
	global $thepostid, $post, $apollo;

	$thepostid              = !empty( $thepostid ) ? $thepostid : 0;
	if(intval($thepostid) === 0 && !empty($post) && is_object($post)){
		$thepostid = $post->ID;
	}
	$field['placeholder'] 	= isset( $field['placeholder'] ) ? $field['placeholder'] : '';
	$field['class'] 		= isset( $field['class'] ) ? $field['class'] : 'short';
	$field['wrapper_class'] = isset( $field['wrapper_class'] ) ? $field['wrapper_class'] : '';
	$field['value'] 		= isset( $field['value'] ) ? $field['value'] : Apollo_App::apollo_get_meta_data( $thepostid, $field['id'] );
    $field['data_elm']      = isset( $field['data_elm'] ) ? $field['data_elm'] : 'data_elm';
	$field['num-row']          = isset( $field['num-row'] ) ? $field['num-row'] : 2;
	$field['num-col']          = isset( $field['num-col'] ) ? $field['num-col'] : 20;

    $require_class = isset( $field['required'] ) && $field['required'] == 'yes' ? 'required' : '';

    if ( ! $field['value'] && isset( $field['default'] ) ) $field['value'] = $field['default'];

	echo '<p class="form-field ' . esc_attr( $field['id'] ) . '_field ' . esc_attr( $field['wrapper_class'] ) . '"><label class="'.(isset($field['label_class']) ? $field['label_class'] : '').'" for="' . esc_attr( $field['id'] ) . '">' . wp_kses_post( isset($field['label']) ? $field['label'] : '' ) . '</label>';

	echo '<textarea '.$field["data_elm"].' class="'.$require_class.' ' . esc_attr( $field['class'] ) . '" name="' . esc_attr( $field['id'] ) . '" id="' . esc_attr( $field['id'] ) . '" placeholder="' . esc_attr( $field['placeholder'] ) . '" rows="' . esc_attr( $field['num-row'] ) . '" cols="' . esc_attr( $field['num-col'] ) . '">' . esc_textarea( $field['value'] ) . '</textarea> ';
    if ( ! empty( $field['description'] ) ) {
        echo '<span class="description">' . wp_kses_post( $field['description'] ) . '</span>';
    }
    apollo_the_required_field( $field );
	echo '</p>';
}

/**
 * Output a textarea input box.
 *
 * @access public
 * @param array $field
 * @return void
 */
function apollo_wp_editor( $field ) {
	global $thepostid, $post;

	$thepostid              = !empty( $thepostid ) ? $thepostid : 0;
	if(intval($thepostid) === 0 && !empty($post) && is_object($post)){
		$thepostid = $post->ID;
	}
	$field['placeholder'] 	= isset( $field['placeholder'] ) ? $field['placeholder'] : '';
	$field['class'] 		= isset( $field['class'] ) ? $field['class'] : 'short';
	$field['wrapper_class'] = isset( $field['wrapper_class'] ) ? $field['wrapper_class'] : '';
	$field['value'] 		= isset( $field['value'] ) ? $field['value'] : Apollo_App::apollo_get_meta_data( $thepostid, $field['name'] );
    $field['name']          = isset( $field['name'] ) ? $field['name'] : $field['id'];

	echo '<div class=" '.$field["class"].' '.(isset($field["required"]) && $field["required"] ? "required-editor" : "").' form-field ' . esc_attr( $field['id'] ) . '_field ' . esc_attr( $field['wrapper_class'] ) . '"><label for="' . esc_attr( $field['id'] ) . '">' . wp_kses_post( $field['label'] ) . ''
        . ' '.(isset($field["required"]) && $field["required"] ? '<span class="re-star">*</span>' : '').'</label>';
	if ( ! empty( $field['description'] ) ) {
		echo '<span class="description">' . wp_kses_post( $field['description'] ) . '</span>';
	}
    $default_editor_settings = array(
        'textarea_name' => $field['name'],
        'media_buttons' => true,
        'editor_height' => 150,
    );
    $editor_settings = array();

    if ( isset( $field['settings'] ) ) {
        $editor_settings = $field['settings'];
    }

    $editor_settings = array_merge( $default_editor_settings, $editor_settings );

    wp_editor( $field['value'], $field['id'], $editor_settings );

	echo '</div>';
}

/**
 * Output a checkbox input box.
 *
 * @access public
 * @param array $field
 * @return void
 */
function apollo_wp_checkbox( $field ) {
	global $thepostid, $post;

	$thepostid              = !empty( $thepostid ) ? $thepostid : 0;
	if(intval($thepostid) === 0 && !empty($post) && is_object($post)){
		$thepostid = $post->ID;
	}
	$field['class']         = isset( $field['class'] ) ? $field['class'] : 'checkbox';
	$field['wrapper_class'] = isset( $field['wrapper_class'] ) ? $field['wrapper_class'] : '';
	$field['value']         = isset( $field['value'] ) ? $field['value'] : Apollo_App::apollo_get_meta_data( $thepostid, $field['id'] );
	$field['cbvalue']       = isset( $field['cbvalue'] ) ? $field['cbvalue'] : 'yes';
	$field['name']          = isset( $field['name'] ) ? $field['name'] : $field['id'];
	$field['data-attributes']          = isset( $field['data-attributes'] ) ? $field['data-attributes'] : "";
	$field['data-attributes'] = is_array($field['data-attributes']) && !empty($field['data-attributes']) ? implode(' ',$field['data-attributes']) : $field['data-attributes'];

    if ( ! $field['value'] && isset( $field['default'] ) ) $field['value'] = $field['default'];
	$value = $field['value'] == '1' ? '1' : 'yes';

    if ( isset( $field['default'] ) && $field['default'] == 'yes' ) $field['cbvalue'] = 'yes';
	echo '<p class="form-field ' . esc_attr( $field['id'] ) . '_field ' . esc_attr( $field['wrapper_class'] ) . '">';
	echo '<input '.$field['data-attributes'].' type="checkbox" class="' . esc_attr( $field['class'] ) . '" name="' . esc_attr( $field['name'] ) . '" id="' . esc_attr( $field['id'] ) . '" value="' . esc_attr( $field['cbvalue'] ) . '" '
        . '' . checked( $field['value'], $value , false ) . '' . checked( $field['value'], $field['cbvalue'], false ) . ' /> ';

	echo '<label for="' . esc_attr( $field['id'] ) . '">' . wp_kses_post( $field['label'] ) . '</label>';
	if ( ! empty( $field['description'] ) ) {
		echo '<br><span class="description">' . wp_kses_post( $field['description'] ) . '</span>';
	}
	echo '</p>';

}

function apollo_wp_multi_checkbox( $field ) {
    global $thepostid, $post;

    $thepostid              = !empty( $thepostid ) ? $thepostid : 0;
	if(intval($thepostid) === 0 && !empty($post) && is_object($post)){
		$thepostid = $post->ID;
	}
    $field['class']         = isset( $field['class'] ) ? $field['class'] : 'checkbox';
    $field['wrapper_class'] = isset( $field['wrapper_class'] ) ? $field['wrapper_class'] : '';
    $field['value']         = isset( $field['value'] ) ? $field['value'] : Apollo_App::apollo_get_meta_data( $thepostid, isset($field['name']) ? $field['name'] : $field['id'] );
    $field['cbvalue']       = isset( $field['cbvalue'] ) ? $field['cbvalue'] : 'yes';
    $field['name']          = isset( $field['name'] ) ? $field['name'] : $field['id'];
    $field['id']            = isset( $field['id'] ) ? $field['id'] : $field['name'];
	$field['data-attributes']          = isset( $field['data-attributes'] ) ? $field['data-attributes'] : "";
	$field['data-attributes'] = is_array($field['data-attributes']) && !empty($field['data-attributes']) ? implode(' ',$field['data-attributes']) : $field['data-attributes'];

	if ( ! $field['value'] && isset( $field['default'] ) ) $field['value'] = $field['default'];

    $require_class = isset( $field['required'] ) && $field['required'] ? 'required' : '';

    if ( is_array( $field['value'] ) && $field['value']  ) {
        $checked = in_array( $field['cbvalue'] , $field['value']) ? 'checked' : '';
    } else {
        $checked = checked( $field['value'], $field['cbvalue'], false );
    }

	echo '<p class="form-field ' . esc_attr( $field['id'] ) . '_field ' . esc_attr( $field['wrapper_class'] ) . '"><input '.$field['data-attributes'].' type="checkbox" class="' . esc_attr( $field['class'] ) . ' '.$require_class.'" name="' . esc_attr( $field['name'] ) . '" id="' . esc_attr( $field['id'] ) . '" value="' . esc_attr( $field['cbvalue'] ) . '" ' . $checked . ' /> ';

	if ( ! empty( $field['description'] ) ) echo '<span class="description">' . wp_kses_post( $field['description'] ) . '</span>';

	echo '<label title="'.wp_kses_post( $field['label'] ).'" for="' . esc_attr( $field['id'] ) . '">' . wp_kses_post( $field['label'] ) . '</label></p>';
}

/**
 * Output a select input box.
 *
 * @access public
 * @param array $field
 * @return void
 */
function apollo_wp_select( $field ) {
    global $thepostid, $post, $apollo;
    $display    = isset( $field['display'] ) && !$field['display'] ? "apl-disabled" : true;

	$thepostid              = !empty( $thepostid ) ? $thepostid : 0;
	if(intval($thepostid) === 0 && !empty($post) && is_object($post)){
		$thepostid = $post->ID;
	}
    $field['class']         = isset( $field['class'] ) ? $field['class'] : 'select short';
    $field['wrapper_class'] = isset( $field['wrapper_class'] ) ? $field['wrapper_class'] : '';
    $field['attr']          = isset( $field['attr'] ) ? $field['attr'] : '';
    $field['value'] 	    = isset( $field['value'] ) ? $field['value'] : Apollo_App::apollo_get_meta_data( $thepostid, isset($field['name']) ? $field['name'] : $field['id'] );

    $_name = isset( $field['name'] ) ? $field['name'] : $field['id'];

    $require_class = isset( $field['required'] ) && $field['required'] == 'yes' ? 'required' : '';

    echo '<p class="form-field ' . esc_attr( $field['id'] ) . '_field ' . esc_attr( $field['wrapper_class'] ) . ' '.$display.'">';

    echo '<label for="' . $_name . '">' . wp_kses_post( $field['label'] ) . '</label><select data-value="'.$field['value'].'" '.(isset($field["attr"]) ? $field["attr"] : "").' id="' . esc_attr( $field['id'] ) . '" name="' . $_name . '" class=" '.$require_class.' ' . esc_attr( $field['class'] ) . '">';

   // echo '<option value="0">'.__("--Select--", "apollo").'</option>';

	if (isset($field['first_option'])) {
		echo '<option value="0">'.$field['first_option'].'</option>';
	}


	if (!empty($field['options'])) {
		if (isset($field['is_post_type_list']) && $field['is_post_type_list']) {
			//foreach ( $field['options'] as $post ) { // Never use $post here it will break the global $post
			foreach ( $field['options'] as $p ) {
			    if(!empty($field['remote'])){
                    echo '<option value="' . esc_attr( $p->ID ) . '" ' . selected( strtolower(esc_attr( $field['value']) ), strtolower(esc_attr( $p->ID )), false ) . ' edit_link="' . get_edit_post_link($p->ID, "") . '" >' . esc_html( $p->post_title ) . '</option>';
                }else{
                    echo '<option value="' . esc_attr( $p->ID ) . '" ' . selected( strtolower(esc_attr( $field['value']) ), strtolower(esc_attr( $p->ID )), false ) . '>' . esc_html( $p->post_title ) . '</option>';
                }
			}

		} else {
			foreach ( $field['options'] as $key => $value ) {
			    if(!empty($field['remote'])){
                    echo '<option value="' . esc_attr( $key ) . '" ' . selected( strtolower(esc_attr( $field['value']) ), strtolower(esc_attr( $key )), false ) . ' edit_link="' . get_edit_post_link($key, "") . '" >' . esc_html( $value ) . '</option>';
                }else{
                    echo '<option value="' . esc_attr( $key ) . '" ' . selected( strtolower(esc_attr( $field['value']) ), strtolower(esc_attr( $key )), false ) . '>' . esc_html( $value ) . '</option>';
                }
			}
		}

	}
    //Link edit org
    $link_edit = '';
	if(!empty($field["value"])){
	    $link_edit = get_edit_post_link($field["value"], '');
    }

    echo '</select> ';
    if(!empty($field['edit_item'])) {
        echo '<a class="edit-item ' . (empty($field["value"]) ? "hidden" : "") . '" target="_blank" href="' . $link_edit .'">' . __( 'Edit', 'apollo' ) .'</a>';
    }
    apollo_the_required_field( $field );
    if ( ! empty( $field['description'] ) ) {
            echo '<br><span class="description">' . wp_kses_post( $field['description'] ) . '</span>';
    }
    echo '</p>';
}

/**
 * Output a radio input box.
 *
 * @access public
 * @param array $field
 * @return void
 */
function apollo_wp_radio( $field ) {
	global $thepostid, $post, $apollo;

	$thepostid              = !empty( $thepostid ) ? $thepostid : 0;
	if(intval($thepostid) === 0 && !empty($post) && is_object($post)){
		$thepostid = $post->ID;
	}
	$field['class'] 		= isset( $field['class'] ) ? $field['class'] : 'select short';
	$field['wrapper_class'] = isset( $field['wrapper_class'] ) ? $field['wrapper_class'] : '';
	$field['value'] 		= isset( $field['value'] ) ? $field['value'] : Apollo_App::apollo_get_meta_data( $thepostid, $field['id'] );
	$field['name']          = isset( $field['name'] ) ? $field['name'] : $field['id'];
    $disable                = isset( $field['disable'] ) && $field['disable']  ? 'disabled' : '';

    echo '<label>'.apollo_the_required_field( $field, TRUE ).' ' . wp_kses_post( $field['label'] ) . ' </label>';
	if ( ! empty( $field['description'] ) ) {
		echo '<span class="description">' . wp_kses_post( $field['description'] ) . '</span>';
	}
	echo '<ul class="apollo-radios '.$field['class'].'">';

    $require_class = isset( $field['required'] ) && $field['required'] ? 'required' : '';

    if ( ! $field['value'] && isset( $field['default'] ) ) $field['value'] = $field['default'];

    foreach ( $field['options'] as $key => $value ) {
		echo '<li><label><input
        		name="' . esc_attr( $field['name'] ) . '"
        		value="' . esc_attr( $key ) . '"
        		type="radio"
        		'.$disable.'
        		class="'.$require_class.' ' . esc_attr( $field['class'] ) . '"
        		' . checked( esc_attr( $field['value'] ), esc_attr( $key ), false ) . '
        		/> ' . esc_html( $value ) . '</label>
    	</li>';
	}
    echo '</ul>';

}

function apollo_is_complex_key( $key ) {
    return strpos( $key, '[' ) && strpos( $key, ']' );
}

function apollo_wp_google_map( $field ) {

    $field['class']         = isset( $field['class'] ) ? $field['class'] : '';
    $field['wrapper_class'] = isset( $field['wrapper_class'] ) ? $field['wrapper_class'] : '';
    $field['attr']          = isset( $field['attr'] ) ? $field['attr'] : '';
    $field['label']         = isset( $field['label'] ) ? $field['label'] : '';

    $_name = isset( $field['name'] ) ? $field['name'] : $field['id'];
    $attr = !empty($field['attr']) ? implode(' ', $field['attr']) : '';
    echo '<div class="apl-admin-wrap-map" >';
        echo '<p class="form-field ' . esc_attr( $field['id'] ) . '_field " >';
            echo '<input type="button" id="'.$_name.'" class="button button-large" '.$attr.' value="'.$field['label'].'" />';
        echo '</p>';
        echo '<div id="apl-admin-google-map" class="hidden"></div>';
    echo '</div>';
}
