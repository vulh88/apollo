<?php

if ( !class_exists( 'Apollo_Admin_Network' ) ):

class Apollo_Admin_Network {

    private $_blog_id;
    
    public function __construct() {

        // Add fields to form
        add_action( 'admin_footer', array( $this, 'ms_custom_site_options' ) );
        
        // Hook into saving settings action
        add_action( 'wpmu_update_blog_options', array( $this, 'wpmu_update_blog_options' ) );
        
        // Hook to in edit form 
        add_action( 'wpmueditblogaction', array( $this, 'ms_update_site' ) );
       
        // Hook to action saving add new site
        add_action( 'wpmu_new_blog', array( $this, 'wpmu_new_blog' ) );

        // Hook to add custom column
        add_filter( 'wpmu_blogs_columns', array( $this, 'custom_column' ) );

        // hook to sortable custom column
        add_filter("manage_sites-network_sortable_columns", array( $this, 'sortable_columns' ));

        // add content for Apollo cache column
        add_action('manage_sites_custom_column', array( $this, 'column_apollo_cache'),10,2);



    }

    function custom_column ( $sites_columns ) {

        $sites_columns = array(
            'cb'          => '<input type="checkbox" />',
            'blogname'    => __( 'URL', 'apollo' ),
            'apollo-cache'    => __( 'Apollo Cache', 'apollo' ),
            'solr'    => __( 'Solr search engine', 'apollo' ),
            'lastupdated' => __( 'Last Updated', 'apollo' ),
            'registered'  => _x( 'Registered', 'site', 'apollo' ),
            'users'       => __( 'Users', 'apollo' ),
        );

        return $sites_columns;
    }
     function sortable_columns() {
        return array(
            'blogname'    => 'blogname',
            'lastupdated' => 'lastupdated',
            'registered'  => 'blog_id',
            'apollo-cache' => 'apollo-cache',
            'solr' => 'solr',
        );
    }

     function column_apollo_cache( $column_name, $blog_id ) {
        if($column_name == 'apollo-cache'){
            if ( Apollo_App::get_network_local_cache($blog_id) ) {
                $cache = __( 'Enabled', 'apollo' );
            } else {
                $cache = __( 'Disabled', 'apollo' );
            }
            echo $cache;
        }

         if($column_name == 'solr'){

             if ( Apollo_App::is_enable_solr_search($blog_id) ) {
                 $cache = __( 'Enabled', 'apollo' );
             } else {
                 $cache = __( 'Disabled', 'apollo' );
             }
             echo $cache;
         }
    }


    /**
     * Handle add new site
     * @param int $blog_id
     * 
     */ 
    public function wpmu_new_blog( $blog_id ) {
       
        $this->_blog_id = $blog_id;
        $this->_save_option();
    }

    /**
     * Add options to settings form
     */
    public function ms_custom_site_options() {
        
        global $pagenow;
        
        // Force other pages
        if ( $pagenow !== 'site-settings.php' && $pagenow !== 'site-new.php' ) {
            return false;
        }
      
        if ( 'site-settings.php' === $pagenow ) { 
            $this->_blog_id = $_REQUEST['id'];
        }
       
        echo $this->_render();
    }

    /**
     * Render template
     * @return html
     * @internal param int $post_id
     */
    private function _render() {

        global $apollo_modules, $pagenow;
        $available_modules = Apollo_App::get_avaiable_modules($this->_blog_id);

        $modules = $pagenow == 'site-new.php' ? array() : Apollo_App::get_avaiable_modules( $this->_blog_id );
        $plugins = $pagenow == 'site-new.php' ? array() : Apollo_App::get_disabled_plugins( $this->_blog_id );
        /** @Ticket #14280 */
        /***
         * ThienLD: custom site configs with kinds of Yes/No radio button by using Array and dispatch the respective WP Hook Filters so we can hook
         * to custom in another scenario, specially in Modules
         */
        $site_configs = [
            '_apollo_manage_states_cities' => [
                'label' => __('Customize States/Cities', 'apollo'),
                'value' => $pagenow == 'site-new.php' ? 0 : Apollo_App::get_network_manage_states_cities($this->_blog_id),
                'type'  => 'radio',
            ],
            '_apollo_enable_event_import_tool' => [
                'label' => __('Enable Event Import Tool', 'apollo'),
                'value' => $pagenow == 'site-new.php' ? 0 : Apollo_App::get_network_event_import_tool($this->_blog_id),
                'type'  => 'radio',
            ],
            '_apollo_enable_local_cache' => [
                'label' => __('Enable Apollo Cache', 'apollo'),
                'value' => $pagenow == 'site-new.php' ? 0 : Apollo_App::get_network_local_cache($this->_blog_id),
                'type'  => 'radio',
            ],
            '_apollo_enable_solr_search' => [
                'label' => __('Enable Apollo Solr Search', 'apollo'),
                'value' => $pagenow == 'site-new.php' ? 0 : Apollo_App::is_enable_solr_search($this->_blog_id),
                'type'  => 'radio',
            ],
            '_apollo_enable_photo_slider' => [
                'label' => __('Enable Apollo Photo Slider ShortCode', 'apollo'),
                'value' => Apollo_App::is_avaiable_module(Apollo_DB_Schema::_PHOTO_SLIDER_PT, $available_modules ) ? 1 : 0,
                'type'  => 'radio',
            ],
            '_apollo_enable_iframe_sw' => [
                'label' => __('Enable Apollo iFrame Search Widget', 'apollo'),
                'value' => Apollo_App::is_avaiable_module(Apollo_DB_Schema::_IFRAME_SEARCH_WIDGET_PT, $available_modules ),
                'type'  => 'radio',
            ],
            '_apollo_enable_report_module' => [
                'label' => __('Enable Reports Module', 'apollo'),
                'value' => $pagenow == 'site-new.php' ? 0 : Apollo_App::get_network_enable_reports($this->_blog_id),
                'type'  => 'radio',
            ],
        ];

        $site_configs = apply_filters('apl_network_adding_configs', [
            'site_configs' => $site_configs,
            'page_now' => $pagenow,
            'current_blog_id' => $this->_blog_id
        ]);

        // Force config maintenance mode always is bottom.
        $mm = Apollo_App::isMaintenanceMode($this->_blog_id, true);
        $site_configs['_apollo_mm_active'] = [
            'label' => __('Maintenance mode', 'apollo'),
            'value' => $pagenow == 'site-new.php' ? 0 : $mm['active'],
            'type'  => 'radio'
        ];

        $max = (int)(ini_get('upload_max_filesize'));
        $site_configs['_apollo_admin_max_upload_file_size'] = [
            'label' => __('Max upload file size for admin panel (MB)', 'apollo'),
            'value' => Apollo_App::maxUploadFileSize('mb', true, $this->_blog_id),
            'type'  => 'number',
            'min'   => 1,
            'max'   => $max,
            'desc'  => sprintf(__('Maximum is %d MB', 'apollo'), $max)
        ];

        /*@ticket #17020: [CF] 20180803 - [FE Forms] - Add the option to increase the file size upload - Item 1*/
        $max = (int)(ini_get('upload_max_filesize'));
        $site_configs['_apollo_frontend_max_upload_file_size'] = [
            'label' => __('Max upload file size for front end (MB)', 'apollo'),
            'value' => Apollo_App::maxUploadFileSize('mb', false, $this->_blog_id),
            'type'  => 'number',
            'min'   => 1,
            'max'   => $max,
            'desc'  => sprintf(__('Maximum is %d MB', 'apollo'), $max)
        ];

        $site_configs_not_in_sonoma = [
            '_apollo_enable_event_import_tool',
            '_apollo_enable_local_cache',
            '_apollo_enable_solr_search'
        ];

        $site_configs_not_in_sonoma = apply_filters('apl_network_list_configs_not_in_sonoma', $site_configs_not_in_sonoma);

        ob_start();
        include dirname( __FILE__ )   . '/template.php';
        return ob_get_clean();
    }
    
    /**
     * Handle action in update settings form of a site
     */
    public function ms_update_site() {
        
    }

    /**
     * Create sorl core
     *
     * @param integer $blog_id
     * @return boolean
     */
    private function _createSolrCore($blog_id) {

        if (! Apollo_App::is_enable_solr_search($blog_id)) {
            return false;
        }

        try {

            $blog = get_blog_details($blog_id);
            $coreName = Apollo_App::formatSolrCoreName($blog->domain);
            $dest = APL_SOLR_DATA_DIR. "/". $coreName;

            if (!is_dir($dest)) {

                mkdir($dest, 0775);

                $src = APOLLO_PARENT_DIR. '/inc/admin/network/solr/conf';
                shell_exec("
                    cp -R $src $dest
                ");
            }

            // create curl resource
            $ch = curl_init();

            // set url
            $solrHost = defined('APL_SOLR_HOST') && APL_SOLR_HOST ? APL_SOLR_HOST : '127.0.0.1';
            $solrPort = defined('APL_SOLR_PORT') && APL_SOLR_PORT ? APL_SOLR_PORT : '8983';

            curl_setopt($ch, CURLOPT_URL, sprintf("http://%s:%s/solr/admin/cores?action=CREATE&name=%s&instanceDir=%s", $solrHost, $solrPort, $coreName, $coreName));

            //return the transfer as a string
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            // $output contains the output string
            $output = curl_exec($ch);

            // close curl resource to free up system resources
            curl_close($ch);

        }
        catch(Exception $e) {
            return false;
        }

        return true;
    }
    
    /**
     * Save handle option in settings tab
     */
    public function wpmu_update_blog_options() {
        global $blog_id;
        $this->_blog_id = $blog_id;
        $this->_save_option();
        $this->_createSolrCore($blog_id);
    }
    
    private function _save_option() {

        if ( isset($_POST['blog']['_apollo_enable_photo_slider']) && $_POST['blog']['_apollo_enable_photo_slider'] == 1 ) {
            $_POST['blog']['_apollo_module'][] = Apollo_DB_Schema::_PHOTO_SLIDER_PT;
        }

        if ( isset($_POST['blog']['_apollo_enable_iframe_sw']) && $_POST['blog']['_apollo_enable_iframe_sw'] == 1 ) {
            $_POST['blog']['_apollo_module'][] = Apollo_DB_Schema::_IFRAME_SEARCH_WIDGET_PT;
        }

        if ( ! isset( $_POST['blog']['_apollo_module'] ) ) {
            $modules = '';
        } else {
            $modules = $_POST['blog']['_apollo_module'];
        }
        
        if ( ! isset( $_POST['blog']['_apollo_plugins'] ) ) {
            $plugins = '';
        } else {
            $plugins = $_POST['blog']['_apollo_plugins'];
        }

        /** @Ticket #14280 */
        if ( $_POST['blog']['_apollo_mm_content'] ) {
            $maintenance_mode_content = $_POST['blog']['_apollo_mm_content'];
        } else {
            $maintenance_mode_content = render_php_to_string(APOLLO_ADMIN_DIR . '/network/templates/default-maintenance-content.html');
        }
        $maintenance_mode_active = $_POST['blog']['_apollo_mm_active'] ? $_POST['blog']['_apollo_mm_active'] : '';

        if ( ! isset( $_POST['blog']['_apollo_manage_states_cities'] ) ) {
            $manage_states_cities = 0;
        } else {
            $manage_states_cities = $_POST['blog']['_apollo_manage_states_cities'];
        }

        if ( ! isset( $_POST['blog']['_apollo_enable_local_cache'] ) ) {
            $local_cache = 0;
        } else {
            $local_cache = $_POST['blog']['_apollo_enable_local_cache'];
        }

        if ( ! isset( $_POST['blog']['_apollo_enable_report_module'] ) ) {
            $enable_reports = 0;
        } else {
            $enable_reports = $_POST['blog']['_apollo_enable_report_module'];
        }

        //TriLM Solr-search
        if ( ! isset( $_POST['blog']['_apollo_enable_solr_search'] ) ) {
            $solr_search = 0;
        } else {
            $solr_search = $_POST['blog']['_apollo_enable_solr_search'];
        }
        //end TrilM solr-search

        // Admin max upload file size config
        $adminMaxUploadFileSize = ! isset( $_POST['blog']['_apollo_admin_max_upload_file_size'] ) ? '' : $_POST['blog']['_apollo_admin_max_upload_file_size'];
        $frontEndMaxUploadFileSize = ! isset( $_POST['blog']['_apollo_frontend_max_upload_file_size'] ) ? '' : $_POST['blog']['_apollo_frontend_max_upload_file_size'];


        $event_import_tool = $_POST['blog']['_apollo_enable_event_import_tool'];
        if( in_array(Apollo_DB_Schema::_BUSINESS_PT, $modules) && !in_array(Apollo_DB_Schema::_ORGANIZATION_PT, $modules)){
            $key = array_search(Apollo_DB_Schema::_BUSINESS_PT, $modules);
            unset($modules[$key]);
        }


        $arrayOptions = array(
            '_apollo_module'    =>     serialize($modules),
            '_apollo_plugins'    =>     serialize($plugins),
            '_apollo_mm_content'    =>     $maintenance_mode_content,
            '_apollo_mm_active'    =>     $maintenance_mode_active,
            '_apollo_manage_states_cities'    =>     $manage_states_cities,
            '_apollo_enable_event_import_tool'    =>     $event_import_tool,
            '_apollo_enable_local_cache'    =>     $local_cache,
            '_apollo_enable_reports'    =>     $enable_reports,
            '_apollo_enable_solr_search'    =>     $solr_search,
            '_apollo_admin_max_upload_file_size'    =>  $adminMaxUploadFileSize,
            '_apollo_frontend_max_upload_file_size' => $frontEndMaxUploadFileSize
        );

        foreach ($arrayOptions as $key => $val) {
            update_blog_option($this->_blog_id, $key, $val);
        }

        do_action('apl_network_after_save', $this->_blog_id);

        $this->_create_postmeta_modules( $modules );
        
        $this->_create_widget_modules( $modules );
        
        global $wp_rewrite;
        $wp_rewrite->init();
        $wp_rewrite->flush_rules();
        
        //$this->_remove_sidebar_widgets($arr_remove_module);
    }

    private function _remove_sidebar_widgets($arr_remove_module) {
        if(empty($arr_remove_module)) {
           return;
        }

        $sidebar_options = get_option('sidebars_widgets');
        foreach($arr_remove_module as $module) {
            $sidebar_options[Apollo_DB_Schema::_SIDEBAR_PREFIX . '-'.$module] = array();
        }

        update_option('sidebars_widgets', $sidebar_options);
    }
    
    private function _create_widget_modules( $modules ) {
        $install = new Apollo_Install_Theme();
        
        // Add widget for each module
        if ( $modules ) {
            foreach ( $modules as $am ) {
                if ( $am == Apollo_DB_Schema::_EVENT_PT ) continue;
                
                // Create default data
                if ( $am == Apollo_DB_Schema::_EDUCATION ) $this->_default_educator_qualifications();
                if ( $am == Apollo_DB_Schema::_BUSINESS_PT ) $this->_default_business_cf();
                
                
                $sidebar_options = get_option('sidebars_widgets');
                
                $am = str_replace('-', '_', $am);
                
                $sidebar = isset($sidebar_options[Apollo_DB_Schema::_SIDEBAR_PREFIX . '-'.$am]) ? $sidebar_options[Apollo_DB_Schema::_SIDEBAR_PREFIX . '-'.$am] : array();
                
                /**
                 * Check if search widget already exist
                 */

                $should_add_search_wg = true;

                if ( !empty($sidebar) ) {

                    foreach ( $sidebar as $s ) {
                        $sidebar_arr = explode( '-' , $s ); /* will be format like  apollo_search_event_widget-10*/
                        if ( isset( $sidebar_arr[0] ) && $sidebar_arr[0] == 'apollo_search_'. $am .'_widget') {
                            $should_add_search_wg = false;
                            break;
                        }
                    }

                }

                if ( $should_add_search_wg) {
                    $install->add_widget( 'apollo_search_'.$am.'_widget', Apollo_DB_Schema::_SIDEBAR_PREFIX . '-'. $am );
                }
            }
        }
    }
    
    private function _default_educator_qualifications() {
        
        $apl_query = new Apl_Query( Apollo_Tables::_APL_CUSTOM_FIELD );
        
        $default_data = array(
            __( 'Conducts educational programming for 2 or more years', 'apollo' ),
            __( 'Performs criminal background checks on staff with youth contact', 'apollo' ),
            __( 'Maintains general liability insurance', 'apollo' ),
            __( 'Three letters of recommendation / references available', 'apollo' ),
            __( 'Provides study guides for teachers and or students', 'apollo' ),
            __( 'Connects to State and or Common Core Curriculum Standards', 'apollo' ),
            __( 'Provides tools to assess student learning (workshops and residencies)', 'apollo' ),
            __( 'Provides scholarship and reduced fees', 'apollo' ),
            __( 'Conducts ongoing assessments of program quality', 'apollo' ),
        );

        if ( ! Apollo_Custom_Field::get_field_parent_by_name(Apollo_DB_Schema::_EDUCATOR_QUALIFICATION_GROUP_FIELD, Apollo_DB_Schema::_EDUCATOR_PT) ) {
            $apl_query->insert( array(
                'label' => __( 'Qualifications', 'apollo' ),
                'name' => Apollo_DB_Schema::_EDUCATOR_QUALIFICATION_GROUP_FIELD,
                'cf_order'  => 1,
                'post_type' => Apollo_DB_Schema::_EDUCATOR_PT
            ) );
            $parent = $apl_query->get_bottom();
            $order = 1;
            foreach ( $default_data as $k => $f ) {
                $_data_fields['checkbox_'.$k.''] = array(
                    'label' => $f,
                    'location'  => 'af_ff_dp_rsb',
                    'require'   => 'no',
                );
                
                $apl_query->insert( array(
                    'label'     => $f,
                    'cf_order'  => $order,
                    'name'      => 'checkbox_'.$k.'',
                    'post_type' => Apollo_DB_Schema::_EDUCATOR_PT,
                    'location'  => maybe_serialize( array( 'ff', 'dp', 'rsb' ) ),
                    'parent'    => $parent,
                    'cf_type'   => 'checkbox',
                ) );
                $order++;
            }
            
        }
    }
    
    private function _default_business_cf() {
        $apl_query = new Apl_Query( Apollo_Tables::_APL_CUSTOM_FIELD );
        $default_data = array(
            'bar-lounge : ' . __('Bar/Lounge', 'apollo'),
            'coffee : ' . __('Coffee', 'apollo'),
            'dinner : ' . __('Dinner', 'apollo'),
            'happy-hour : ' . __('Happy Hour', 'apollo'),
            'lunch : ' . __('Lunch', 'apollo'),
            'private-events-room : ' . __('Private Events/Room', 'apollo'),
            'brunch : ' . __('Brunch', 'apollo'),
            'delivery : ' . __('Delivery', 'apollo'),
            'fast-food : ' . __('Fast Food', 'apollo'),
            'kid-friendly : ' . __('Kid-Friendly', 'apollo'),
            'open-late : ' . __('Open Late', 'apollo'),
            'take-out : ' . __('Take-Out', 'apollo'),
            'catering : ' . __('Catering', 'apollo'),
            'diner : ' . __('Diner', 'apollo'),
            'fine-dining : ' . __('Fine Dining', 'apollo'),
            'live-music : ' . __('Live Music', 'apollo'),
            'patio-dining : ' . __('Patio Dining', 'apollo'),
        );
        $meta = maybe_serialize(array(
            'choice' => implode("\n",$default_data),
            'desc' => ''
        ));
        $postType = Apollo_DB_Schema::_BUSINESS_PT;
        if ( ! Apollo_Custom_Field::get_field_parent_by_name(Apollo_DB_Schema::_BUSINESS_FEATURES_GROUP_FIELD, $postType) ) {
            $apl_query->insert( array(
                'label' => __( 'Features', 'apollo' ),
                'name' => Apollo_DB_Schema::_BUSINESS_FEATURES_GROUP_FIELD,
                'cf_order'  => 1,
                'post_type' => $postType
            ) );

            $parent = $apl_query->get_bottom();

            $apl_query->insert( array(
                'label'     => __( 'Features', 'apollo' ),
                'cf_order'  => 1,
                'name'      => Apollo_DB_Schema::_APOLLO_BS_FEATURES_META_KEY,
                'post_type' => $postType,
                'location'  => maybe_serialize( array( 'ff', 'dp', 'rsb' ) ),
                'parent'    => $parent,
                'cf_type'   => 'multi_checkbox',
                'meta_data' => $meta
            ) );
        }
    }
    
    private function _create_postmeta_modules( $avaiable_mods ) {
        global $apollo_modules;
        
        if ( ! $avaiable_mods ) {
            return;
        }
     
        global $wpdb;
        
		$wpdb->hide_errors();

		$collate = '';

		if ( $wpdb->has_cap( 'collation' ) ) {
			if ( ! empty($wpdb->charset ) ) {
				$collate .= "DEFAULT CHARACTER SET $wpdb->charset";
			}
			if ( ! empty($wpdb->collate ) ) {
				$collate .= " COLLATE $wpdb->collate";
			}
		}

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        $query_post_type_meta_table = '';
        
        foreach ( $avaiable_mods as $mkey ) {
            $_ac_mods = array();
            if($mkey == Apollo_DB_Schema::_SYNDICATION_PT){
                // Thienld : skipped create syndication meta data table because this module is using wp_postmeta default of wordpress
                continue;
            }
            if ( isset( $apollo_modules[$mkey]['childs'] ) ) $_ac_mods = array_keys( $apollo_modules[$mkey]['childs'] );
            else $_ac_mods = array( $mkey );
            
            if ( $_ac_mods ):
                foreach( $_ac_mods as $key ):
                    $key = str_replace('-', '_', $key);
                    $id = 'apollo_'. $key. '_id';
                        $query_post_type_meta_table .= "CREATE TABLE {$wpdb->prefix}apollo_{$key}meta (
                            meta_id bigint(20) NOT NULL auto_increment,
                            {$id} bigint(20) NOT NULL,
                            meta_key varchar(255) NULL,
                            meta_value longtext NULL,
                            PRIMARY KEY  (meta_id),
                            KEY {$id} ({$id}),
                            KEY meta_key (meta_key)
                          ) $collate;";
                endforeach;
            endif;
            
        }
        
        if ( $query_post_type_meta_table ) {
            dbDelta( $query_post_type_meta_table );
        }
    }

    public static function customQueryFilterSearchEngine($s = ''){
        return '%' . $s . '%';
    }

    public function getStyleSheet(){
        global $wpdb;
        $id = is_main_site( $this->_blog_id ) ? '' : $this->_blog_id.'_';
        $table = $wpdb->prefix.''.$id.'options';
        $apl_query = new Apl_Query($table,true);
        return  $apl_query->get_row("option_name = 'stylesheet' ", 'option_value' );
    }
}

endif;

return new Apollo_Admin_Network();
