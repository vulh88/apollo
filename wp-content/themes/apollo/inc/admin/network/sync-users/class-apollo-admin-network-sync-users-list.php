<?php

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

/**
 * Class APL_Org_Syndication_Table
 *
 * @ticket #11252
 */
class Apollo_Admin_Network_Sync_User_List extends WP_List_Table
{

    private $perPage = 100;
    private $totalItems;
    private $offset = 0;
    private $userIds = array();

    public function __construct($args = array())
    {
        parent::__construct($args);
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    public function prepare_items()
    {
        $hidden       = $this->get_hidden_columns();
        $sortable     = $this->get_sortable_columns();
        $currentPage  = $this->get_pagenum();
        $perPage      = $this->perPage;
        $this->offset = ($currentPage - 1) * $this->perPage;
        $columns      = $this->get_columns();
        $data         = $this->table_data();

        $this->set_pagination_args( array(
            'total_items' => $this->totalItems,
            'per_page'    => $perPage
        ) );
        if (!isset($_GET['users-filter']) || (isset($_GET['users-filter']) && $_GET['users-filter'] != 'all_site')) {
            unset($columns['reference_site']);
        }
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = $data;
    }

    /**
     * @return array
     */
    public function get_columns()
    {
        $columns = array(
            'cb' => '<input type="checkbox" />',
            'id' => __( 'User ID', 'apollo' ),
            'name' => __( 'Source User Name', 'apollo'),
            'email' => __( 'Source User Email', 'apollo'),
            'site_name' => __( 'Source Site', 'apollo'),
            'reference_site' => __( 'Destination site', 'apollo' ),
            'site_id' => __( 'Site ID', 'apollo'),
            'move_to' => __( 'Move to', 'apollo'),
        );
        return $columns;
    }

    protected function column_cb($item)
    {
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            'user_ids',  // Let's simply repurpose the table's singular label ("movie").
            $item['ID']                // The value of the checkbox should be the record's ID.
        );
    }

    /**
     * @return array
     */
    public function get_hidden_columns()
    {
        return array();
    }

    /**
     * @return array
     */
    public function get_sortable_columns()
    {
        return array();
    }

    /**
     * @return array
     */
    private function table_data() {
        apl_require_once(APOLLO_INCLUDES_DIR. '/Lib/Helpers/SyncUsers.php');
        $usersFilter = isset($_GET['users-filter']) ? $_GET['users-filter'] : '';
        $blogFilter = isset($_GET['blog-id']) ? $_GET['blog-id'] : '';
        $data = APL_Lib_Helpers_Sync_Users::getUsersData(APL_USER_API_GET_FROM_USER_ID, $this->perPage, $this->getOffset(), $usersFilter, $blogFilter);
        $userIds = array();
        if (!empty($_POST['user_ids'])) {
            $userIds = $_POST['user_ids'];
        }
        if (isset($_POST['user_id'])) {
            $userIds[] = $_POST['user_id'];
        }
        if (!empty($userIds)) {
            foreach ($userIds as $userID) {
                if (!empty($data['users'][$userID])) {
                    $transfer = APL_Lib_Helpers_Sync_Users::transferUser($data['users'][$userID]);
                    if ($transfer) {
                        $data['total'] -= 1;
                        $users = $data['users'];
                        unset($users[$userID]);
                        $data['users'] = $users;
                    }
                }
            }
        }
        $this->totalItems = $data['total'];
        return $data['users'];
    }

    /**
     * @param object $item
     * @param string $column_name
     * @return mixed
     */
    public function column_default( $item, $column_name )
    {
        switch( $column_name ) {
            case 'id':
                $this->userIds[] = $item['ID'];
                return $item[ 'ID' ];
            case 'site_name':
                return isset($item['user_meta']['source_domain'][0]) ? $item['user_meta']['source_domain'][0] : '';
            case 'reference_site' :
                return isset($item['reference_source_domain']) ? $item['reference_source_domain'] : '';
            default:
                return print_r( $item, true ) ;
        }
    }

    public function column_name($item) {
        if (!empty($item['edit_link'])) {
            return '<a href="'. $item['edit_link'] .'" target="_blank">'. $item['user_nicename'] .'</a>';
        } else {
            return $item['user_nicename'];
        }
    }

    public function column_email ($item) {
        if (isset($item['reference_email']) && $item['user_email'] == $item['reference_email']) {
            return '<span class="conflict-user">'.$item['user_email'].'</span>';
        } else {
            return $item['user_email'];
        }
    }

    public function column_site_id ($item) {
        if (isset($item['reference_site_id'])) {
            $siteIDInfo = __('Source siteID: ', 'apollo' ) . $item['user_meta']['primary_blog'][0] . '</br>' .
                __('Destination siteID: ') . $item['reference_site_id'];
        } else {
            $siteIDInfo = isset($item['user_meta']['primary_blog'][0]) ? $item['user_meta']['primary_blog'][0] : '';
        }
        return $siteIDInfo;
    }

    /**
     * @param $item
     * @return string
     */
    public function column_move_to($item)
    {
        if (isset($item['reference_site_id'])) {
            return $item['move_to'];
        } else {
            $dataMessage = __( sprintf('Do you want move %s to %s?', $item['user_nicename'], $item['move_to']), 'apollo' );
            $result = '<form id="transfer-user-to" method="post">
                            <input type="hidden" name="user_id" value="'. $item['ID'] .'"/>
                            <input type="submit" class="button confirm-transfer-user" data-message="'. $dataMessage .'" value="' . $item['move_to'] . '"/>
                        </form>';
            return $result;
        }
    }

    public function extra_tablenav($which)
    {
        $userFilterSelected = isset($_GET['users-filter']) ? $_GET['users-filter'] : '';
        $blogSelected = isset($_GET['blog-id']) ? $_GET['blog-id'] : '';
        $listBlog = APL_Lib_Helpers_Sync_Users::getAllBlogIds();
        ?>
        <form>
            <div class="alignleft actions" id="sync-users-actions" style="">
                <input type="hidden" name="page" value="sync_users"/>
                <select name="users-filter">
                    <option
                        value="current_site" <?php echo ($userFilterSelected == 'current_site') ? 'selected' : ''; ?>><?php _e('Users only exist on source site', 'apollo'); ?></option>
                    <option
                        value="reference_site" <?php echo ($userFilterSelected == 'reference_site') ? 'selected' : ''; ?>><?php _e('Users only exist from the destination server', 'apollo'); ?></option>
                    <option
                        value="all_site" <?php echo ($userFilterSelected == 'all_site') ? 'selected' : ''; ?>><?php _e('Users exist on both servers (Same userID, but the info is different)', 'apollo'); ?></option>
                </select>
                <input type="submit" id="filter-users-action" class="button" value="<?php _e('Filter', 'apollo'); ?>"/>
                <select name="blog-id">
                    <option value=""><?php _e('All', 'apollo'); ?></option>
                    <?php
                    if (!empty($listBlog)) :
                        foreach ($listBlog as $blog) :
                            $selected = ($blog['blog_id'] == $blogSelected) ? 'selected' : '';
                            ?>
                            <option value="<?php echo $blog['blog_id']; ?>" <?php echo $selected; ?> ><?php echo $blog['blog_id']; ?></option>
                        <?php
                        endforeach;
                    endif;
                    ?>
                </select>
            </div>
        </form>
        <form id="transfer-multiple-user" method="post">
            <div class="apl-multiple_users">
                <?php $dataMessage = __( 'Are you sure?', 'apollo' ); ?>
                <input type="submit" id="apl-move-multiple-users" class="button" data-message="<?php echo $dataMessage; ?>" value="<?php _e('Move multiple', 'apollo'); ?>"/>
            </div>
        </form>
        <?php
    }

    public function render_userIds () {
        if ($this->userIds) : ?>
            <div class="list-userids">
                <p><span><?php echo $this->userIds ? ( __('All userIds: ', 'apollo') . implode(",", $this->userIds)) : ''; ?> </span></p>
            </div>
        <?php
        endif;
    }

}
