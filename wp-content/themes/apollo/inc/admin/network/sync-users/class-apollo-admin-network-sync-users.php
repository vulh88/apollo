<?php

class Apollo_Admin_Network_Sync_Users
{

    public function __construct($args = array())
    {
        $this->hooks();
    }

    public function hooks() {
        if( is_multisite() ) {
            // Add network menu item
            add_action( 'network_admin_menu', array( &$this, 'add_admin_menu' ) );
        }
    }

    public function add_admin_menu() {
        add_submenu_page(
            'users.php',
            __( 'Sync Users', 'apollo' ),
            __( 'Sync Users', 'apollo' ),
            'manage_options',
            'sync_users',
            array( &$this, 'render' )
        );
    }

    public function render() {
        include_once APOLLO_ADMIN_DIR . '/network/sync-users/class-apollo-admin-network-sync-users-list.php';
        $syncUser = new Apollo_Admin_Network_Sync_User_List();
        $syncUser->prepare_items();
        $syncUser->display();
        $syncUser->render_userIds();
    }

}

return new Apollo_Admin_Network_Sync_Users();