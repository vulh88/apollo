<?php
/**
 * Created by .
 * User: vulh
 * Date: 9/23/15
 * Time: 11:02 AM
 */

if (! class_exists('Apollo_Admin_Network_State')) {

    class Apollo_Admin_Network_State
    {
        protected $page;
        protected $icon;

        public function __construct()
        {
            // init data
            $this->page = 'apl-manage-states-cities';
            $this->icon = home_url() . '/wp-content/themes/apollo/inc/admin/assets/images/aflag-icon.png';

            // add menu
            add_action( 'network_admin_menu', array( $this, 'dm_network_pages' ) );
            add_action( 'admin_menu'        , array( $this, 'each_site_page'   ) );
        }

        function dm_network_pages()
        {
            add_menu_page(
                __('State & City', 'apollo'),  // page title
                __('State & City', 'apollo'),  // menu title
                'manage_options',              // capability
                $this->page,                   // menu slug
                array($this, 'dm_admin_page'), // callable function
                $this->icon                    // icon url
            );
        }

        function each_site_page() {
            add_options_page( __('State & City', 'apollo'), __('States & Cities', 'apollo'), 'manage_options', $this->page, array( $this, 'dm_admin_page' ) );
        }

        function dm_admin_page() {
            ob_start();
            include dirname(__FILE__) . '/templates/state.php';
            echo ob_get_clean();
        }
    }

    return new Apollo_Admin_Network_State();

}