<table class="form-table" style="display: none">
    <tbody>
        <tr class="apollo-custom-ms-options" id="apollo-custom-modules-options">
            <th scope="row"><?php _e( 'Modules', 'apollo' ) ?></th>
            <td>
                <label><input <?php echo is_array( $modules ) && count( $apollo_modules ) == count( $modules )-2 ? 'checked' : '' ?> value="1" id="checkall" type="checkbox" ><?php _e( 'All', 'apollo' ) ?></label>
                <br/>

                <?php
                $stylesheet = $this->getStyleSheet();
                if ( $apollo_modules ):
                    /* ThienLD : [START] remove 2 modules in here because they're using under settings in network admin on each site */
                    unset($apollo_modules[Apollo_DB_Schema::_PHOTO_SLIDER_PT]);
                    unset($apollo_modules[Apollo_DB_Schema::_IFRAME_SEARCH_WIDGET_PT]);
                    /* ThienLD : [END] remove 2 modules in here because they're using under settings in network admin on each site */
                    $apollo_modules['post'] = array(
                        'sing' => __('Blog',"apollo"),
                        'plural' => __('Blogs',"apollo")
                    );
                    $apollo_modules['agency'] = array(
                        'sing' => __('Agency',"apollo"),
                        'plural' => __('Agencies',"apollo")
                    );
                    function  sortByValue($a, $b) {
                        return strcmp($a["sing"], $b["sing"]);
                    }
                    if( !empty($stylesheet) && $stylesheet->option_value == APL_SM_DIR_THEME_NAME ){
                        unset($apollo_modules['agency'],$apollo_modules['business'],$apollo_modules['education'],$apollo_modules['syndication'],$apollo_modules['iframesw']);
                    }
                    uasort($apollo_modules,'sortByValue');
                    foreach ( $apollo_modules as $key => $label ): ?>
                        <label for="<?php echo $key; ?>">
                            <input <?php echo is_array( $modules ) && in_array( $key, $modules ) ? 'checked' : '' ?> value="<?php echo $key; ?>" type="checkbox" name="blog[_apollo_module][]" id="<?php echo $key; ?>" />
                            <?php echo $label['sing']; ?>
                        </label><br/>
                        
                <?php 
                    endforeach; 
                endif;
                ?>
            </td>
        </tr>
    </tbody>
</table>
<?php
if (!empty($site_configs)) :
    $hiddenConfigs = !empty($site_configs_not_in_sonoma) ? $site_configs_not_in_sonoma : [];
    foreach ($site_configs as $sKey => $sItem):
        $type = !empty($sItem['type']) ? $sItem['type'] : '';
        switch ($type) :
            case 'number':
                $min = !empty($sItem['min']) ? 'min='. $sItem['min'] : '';
                $max = !empty($sItem['max']) ? 'max='. $sItem['max'] : '';
                ?>
                <table class="form-table" style="display: none;">
                    <tbody>
                    <tr class="apollo-custom-ms-options">
                        <th scope="row"><?php echo isset($sItem['label']) ? $sItem['label'] : '' ?></th>
                        <td>
                            <input <?php echo $min ?> <?php echo $max ?> name="blog[<?php echo $sKey ?>]" value="<?php echo isset($sItem['value']) ? $sItem['value'] : 0 ?>" type="number" class="all-options" size="10"/>
                            <br/><small><?php echo isset($sItem['desc']) ? $sItem['desc'] : '' ?></small>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <?php
                break;

            case 'radio': // radio
                if( !empty($stylesheet)
                    && $stylesheet->option_value == APL_SM_DIR_THEME_NAME
                    && in_array($sKey, $hiddenConfigs)
                ) {
                    continue;
                }
?>
                <table class="form-table" style="display: none;">
                    <tbody>
                    <tr class="apollo-custom-ms-options">
                        <th scope="row"><?php echo $sItem['label']; ?></th>
                        <td>
                            <input <?php echo $sItem['value'] == 1 ? 'checked' : '' ?> class="of-input of-radio" type="radio" name="blog[<?php echo $sKey; ?>]" value="1">
                            <label><?php _e('Yes', 'apollo') ?></label>

                            <input <?php echo $sItem['value'] == 0 ? 'checked' : '' ?> class="of-input of-radio" type="radio" name="blog[<?php echo $sKey; ?>]" value="0">
                            <label><?php _e('No', 'apollo') ?></label>
                        </td>
                    </tr>


                    <?php if ($sKey == '_apollo_mm_active') {?>
                        <tr class="apollo-custom-ms-options">
                            <th scope="row"><?php _e( 'Maintenance content', 'apollo' ) ?></th>
                            <td>
                                <?php
                                $mm = Apollo_App::isMaintenanceMode($this->_blog_id, true);
                                if (!empty($mm['content'])) {
                                    $maintenance_value = Apollo_App::stripSlash($mm['content']);
                                } else {
                                    $maintenance_value = render_php_to_string(APOLLO_ADMIN_DIR . '/network/templates/default-maintenance-content.html');
                                }
                                ?>
                                <?php
                                wp_editor($maintenance_value, "_apollo_mm_content", array(
                                        'textarea_name' => 'blog[_apollo_mm_content]',
                                        'editor_height' => 250,
                                    )
                                ); ?>
                            </td>
                        </tr>
                    <?php } // end specific maintenance_mode field ?>

                    </tbody>
                </table>
            <?php break; ?>
        <?php endswitch; ?>
    <?php endforeach; ?>
<?php endif; ?>

<table class="form-table" style="display: none">
    <tbody>
        
        <tr class="apollo-custom-ms-options">
            <th scope="row"><?php _e( 'Disabled Plugins', 'apollo' ) ?></th>
            <td>
                <div id="apl-network-disabled-plugin" class="apl-list-panel">
                <?php 
                $directories = scandir(WP_PLUGIN_DIR);
              
                if ( $directories ):
                    foreach ( $directories as $directory ): 
                    
                        if($directory=='.' or $directory=='..' ) continue;
                            
                        
                        if(!is_dir(WP_PLUGIN_DIR.'/'.$directory)) continue;
                    ?>
                    
                        <label for="<?php echo $directory; ?>">
                            <input <?php echo is_array( $plugins ) && in_array( $directory, $plugins ) ? 'checked' : '' ?>  value="<?php echo $directory; ?>" type="checkbox" name="blog[_apollo_plugins][]" id="<?php echo $directory; ?>" />
                            <?php echo $directory; ?>
                        </label><br/>    
                <?php 
                    endforeach; 
                endif;
                ?>
                </div>
            </td>
        </tr>

    </tbody>
</table>

