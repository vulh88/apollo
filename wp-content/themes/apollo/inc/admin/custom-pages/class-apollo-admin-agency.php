<?php

class Apollo_Admin_Agency {
    private $forms = array();
    private $errors = array();
    private $total = 0;
    private $pagesize = 10;
    private $id = '';
    
    public function __construct() {
        $actions = array(
            'admin_menu'    => 'agency_menu',
        );
     
        foreach ( $actions as $k => $v ) {
            add_action( $k, array( $this, $v ) );
        }
        
        $this->forms = array(
            'name' => array( 'label' => __( 'Name', 'apollo' ), 'validate' => '' ),
            'address' => array( 'label' => __( 'Address', 'apollo' ), 'validate' => '' ),
            'phone' => array( 'label' => __( 'Phone', 'apollo' ), 'validate' => '' ),
            'email' => array( 'label' => __( 'Email', 'apollo' ), 'validate' => 'email', 'class' => 'apollo_input_email' ),
            'url' => array( 'label' => __( 'Url', 'apollo' ), 'validate'   => 'url', 'class'   => 'apollo_input_url' ),
            
        );
    }
    
    public function agency_menu() {
        
        if ( ! Apollo_App::is_avaiable_module( Apollo_DB_Schema::_AGENCY_PT ) ) return false;
    
        add_menu_page( __('Agencies', 'apollo'), __('Agencies', 'apollo'), 'administrator', 'admin-apollo-agency-management', array( $this, 'list_agency' ), ''. home_url().'/wp-content/themes/apollo/inc/admin/assets/images/aflag-icon.png', 37);
        
        // Add a submenu of admin menu:
        add_submenu_page('admin-apollo-agency-management', __( 'Add New', 'apollo' ), __( 'Add New', 'apollo' ), 'administrator', 'admin-apollo-agency-form', array( $this, 'agency_form' ));
    }
    
    public function list_agency() {
        
        $fields = array(
            'name'  => __( 'Name', 'apollo' ),
            'user'  => __( 'User', 'apollo' ),
            'educator'  => __( 'Educator(s)', 'apollo' ),
            'org'  => __( 'Org(s)', 'apollo' ),
            'venue'  => __( 'Venue(s)', 'apollo' ),
            'url'  => __( 'Agency Website', 'apollo' ),
            'email'  => __( 'Email', 'apollo' ),
        );
        global $wpdb;
        $this->pagesize = get_option( 'posts_per_page' );
        $apl_query = new Apl_Query( $wpdb->{Apollo_Tables::_APL_AGENCY} );
        
        // Delete one item
        $is_deletting = false;
        if ( isset( $_GET['action'] ) && $_GET['action'] == 'remove' && isset( $_GET['id'] ) && $_GET['id'] ) {
            $id = $_GET['id'];
            $apl_query->delete( "agencyID=$id" );
            $is_deletting = true;
        }
        
        if ( isset( $_GET['action'] ) && $_GET['action'] == 'removechecked' && isset( $_POST['agency'] ) && $_POST['agency'] ) {
            foreach ( $_POST['agency'] as $id ) {
                $apl_query->delete( "agencyID=$id" );
            }
            $is_deletting = true;
        }
        
        // Get result after delete
        $results = $apl_query->get_where( '1=1', '*', 'LIMIT '.$this->get_offset().', '.$this->pagesize.' ' );
        
        // Check redirect after deletting
        if ( ! $results && $is_deletting ) {
            $page = isset( $_GET['paged'] ) && $_GET['paged'] > 1 ? $_GET['paged'] - 1: 1;
            Apollo_App::safeRedirect( admin_url(). 'admin.php?page=admin-apollo-agency-management&paged='. $page );
        }
        
        if ( ! $this->total ) {
            $this->total = $apl_query->get_total();
        }

        $associationUserModuleListComp = apl_instance('APL_Lib_Association_Components_AssociationUsersModuleList');

        ?>
        <div class="wrap clear">
            
            <fieldset>
            <legend>
                <h2><?php _e( 'Agencies', 'apollo' ) ?><a href="admin.php?page=admin-apollo-agency-form" class="add-new-h2">Add New</a></h2>
            </legend>
            <hr/>
           
                    <div class="tablenav top">
                        <form method="post" action="<?php echo admin_url() ?>admin.php?page=admin-apollo-agency-management&action=removechecked&paged=<?php echo isset( $_GET['paged'] ) ? $_GET['paged'] : 1 ?>">
                            <div class="alignleft actions bulkactions">
                                <label for="bulk-action-selector-top" class="screen-reader-text"><?php _e( 'Select bulk action', 'apollo' ) ?></label><select name="action" id="bulk-action-selector-top">
                                <option value="-1" selected="selected"><?php _e( 'Bulk Actions', 'apollo' ) ?></option>
                                    <option value="remove"><?php _e( 'Remove', 'apollo' ) ?></option>
                                </select>
                                <input data-confirm="<?php _e( 'Are you sure to remove selected agencies ?', 'apollo' ) ?>" 
                                       type="button" name="" id="apl-doaction" class="button action" 
                                       value="<?php _e( 'Apply', 'apollo' ) ?>">
                            </div>
                        <div class="tablenav-pages one-page">
                            <span class="displaying-num"><?php echo sprintf( __( '%d items', 'apollo' ), $this->total ) ?></span>
                        </div>    
                        <br class="clear">
                    </div>
                    
                    <table class="wp-list-table widefat fixed posts apl-list-custom-table">
                        <thead>
                            <tr>
                                <th style="width: 20px">
                                    <input class="apl-cb-select-all" type="checkbox">
                                </th>
                                <?php
                                    $title = '';
                                    foreach( $fields as $v ):
                                        $title .= '<th>'.$v.'</th>';
                                    endforeach; 
                                    echo $title;
                                ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if ( $results ):
                            foreach ( $results as $k => $r ): ?>
                            <tr class="status-publish hentry <?php echo $k % 2 ? 'alternate' : '' ?> iedit author-self level-0">
                                <th>
                                    <input type="checkbox" name="agency[]" id="user_<?php echo $r->agencyID ?>" class="subscriber" value="<?php echo $r->agencyID ?>">
                                </th>
                                <td>
                                    <a href="<?php echo admin_url() ?>admin.php?page=admin-apollo-agency-form&action=edit&id=<?php echo $r->agencyID ?>"><?php echo $r->name ? $r->name : '--' ?></a>
                                    <div class="row-actions">
                                        <span class="id"><?php _e( 'ID', 'apollo' ) ?>: <?php echo $r->agencyID ?> | </span>
                                        <span class="edit">
                                            <a href="<?php echo admin_url() ?>admin.php?page=admin-apollo-agency-form&action=edit&id=<?php echo $r->agencyID ?>" title="<?php _e( 'Edit this item' ) ?>"><?php _e( 'Edit', 'apollo' ) ?></a>
                                        | </span>
                                        
                                        <span data-confirm="<?php _e( 'Are you sure to remove this agency ?', 'apollo' ) ?>" class="remove">
                                            <a href="<?php echo admin_url() ?>admin.php?page=admin-apollo-agency-management&action=remove&id=<?php echo $r->agencyID ?>&paged=<?php echo isset( $_GET['paged'] ) && $_GET['paged'] ? intval( $_GET['paged'] ) : 1 ?>" title="<?php echo $r->name ?>" rel="permalink"><?php _e( 'Remove', 'apollo' ) ?></a>
                                            
                                        </span>
                                    </div>
                                </td>

                                <td><?php echo $associationUserModuleListComp->render(array(
                                        'id' => $r->agencyID,
                                        'post_type' => 'agency',
                                    )); ?></td>
                                
                                <?php 
                                    $apl_edu_query = new Apl_Query( Apollo_Tables::_APL_AGENCY_EDUCATOR );
                                    $educators = $apl_edu_query->get_where( "agency_id = $r->agencyID" );
                                ?>
                                <td>
                                    <?php if ($educators): ?>
                                    <a href="#TB_inline?height=100&amp;width=400&amp;inlineId=educators-list-<?php echo $r->agencyID ?>" title="<?php echo $r->name ?>" class="thickbox" ><?php _e( 'View Educators', 'apollo' ) ?></a>
                                    <div id="educators-list-<?php echo $r->agencyID ?>" style="display:none">
                                        <ul>
                                            <?php foreach ( $educators as $educator ): 
                                                $p = get_post( $educator->edu_id );
                                            ?>
                                            <li><a href="<?php echo get_edit_post_link( $p->ID, true ) ?>"><?php  echo $p->post_title ?></a></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                    <?php else:
                                        echo '--';
                                    endif; ?>
                                </td>
                                
                                <?php 
                                    $apl_orgs_query = new Apl_Query( Apollo_Tables::_APL_AGENCY_ORG );
                                    $orgs = $apl_orgs_query->get_where( "agency_id = $r->agencyID" );
                                ?>
                                <td>
                                    <?php if ($orgs): ?>
                                    <a href="#TB_inline?height=100&amp;width=400&amp;inlineId=orgs-list-<?php echo $r->agencyID ?>" title="<?php echo $r->name ?>" class="thickbox" ><?php _e( 'View Organizations', 'apollo' ) ?></a>
                                    <div id="orgs-list-<?php echo $r->agencyID ?>" style="display:none">
                                        <ul>
                                            <?php foreach ( $orgs as $org ): 
                                                $p = get_post( $org->org_id );
                                                if ( ! $p ) continue;
                                            ?>
                                            <li><a href="<?php echo get_edit_post_link( $p->ID, true ) ?>"><?php  echo $p->post_title ?></a></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                    <?php else:
                                        echo '--';
                                    endif; ?>
                                </td>
                                
                                <?php 
                                    $apl_venues_query = new Apl_Query( Apollo_Tables::_APL_AGENCY_VENUE );
                                    $venues = $apl_venues_query->get_where( "agency_id = $r->agencyID" );
                                ?>
                                <td>
                                    <?php if ($venues): ?>
                                    <a href="#TB_inline?height=100&amp;width=400&amp;inlineId=venues-list-<?php echo $r->agencyID ?>" title="<?php echo $r->name ?>" class="thickbox" ><?php _e( 'View Venues', 'apollo' ) ?></a>
                                    <div id="venues-list-<?php echo $r->agencyID ?>" style="display:none">
                                        <ul>
                                            <?php foreach ( $venues as $venue ): 
                                                $p = get_post( $venue->venue_id );
                                            ?>
                                            <?php if(isset($p)) { ?>
                                            <li><a href="<?php echo get_edit_post_link( $p->ID, true ) ?>"><?php  echo $p->post_title ?></a></li>
                                            <?php } ?>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                    <?php else:
                                        echo '--';
                                    endif; ?>
                                </td>
                                
                                <td><?php if ( $r->url ) { ?><a target="_blank" href="<?php echo $r->url ?>"><?php echo $r->url ?></a> <?php } else { echo '--'; } ?></td>
                                <td><?php echo $r->email ?></td>
                            </tr>
                            <?php endforeach; 
                                else:
                                    echo '<tr class="no-items"><td class="colspanchange" colspan="7">'.__('No agencies found', 'apollo').'</td></tr>';
                                endif;
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th style="width: 20px">
                                    <input class="apl-cb-select-all" type="checkbox">
                                </th>
                                <?php echo $title; ?>
                            </tr>
                        </tfoot>
                    </table>
               
                    <div class="tablenav bottom">
                        <div class="tablenav-pages">
                            <?php 
                                $pagenum = isset( $_GET['paged'] ) ? absint( $_GET['paged'] ) : 1;
                                $num_of_pages = ceil( $this->total / $this->pagesize );
                                $page_links = paginate_links( array(
                                    'base' => add_query_arg( 'paged', '%#%' ),
                                    'format' => '',
                                    'prev_text' => __( '&laquo;', 'apollo' ),
                                    'next_text' => __( '&raquo;', 'apollo' ),
                                    'total' => $num_of_pages,
                                    'current' => $pagenum
                                ) );
                                echo $page_links;
                                
                            ?>
                        </div>
                        <br class="clear">
                    </div>
                    
                <form>
            </fieldset>
        </div>
        <?php     
    }
    
    public function save_associated( $configs  ) {
        
        $venues = $_POST[$configs['post_type'].'s'];
    
        if ( $venues ) {
            $agency_id = $configs['agency_id'];
            $apl_query = new Apl_Query( $configs['table'] );
            $apl_query->delete( "agency_id=$agency_id" );
            foreach ( $venues as $vid ):
                $apl_query->insert( array( 'agency_id' => $agency_id, $configs['associated_id'] => $vid ) );
            endforeach;
        } 
    }

    /**
     * Update association for agency
     * @param $agencyID
     * @param $postType
     * @param $listChecked
     * @param $listUnchecked
     */
    private function updateAssociation($agencyID, $postType, $listChecked, $listUnchecked) {
        if (empty($listChecked) && empty($listUnchecked)) {
            return;
        }
        $listUnchecked = !empty($listUnchecked) ? explode(",", $listUnchecked) : array();
        $listChecked = !empty($listChecked) ? explode(",", $listChecked) : array();
        $listRemove = array_merge($listUnchecked, $listChecked);
        global $wpdb;
        switch ($postType) {
            case Apollo_DB_Schema::_EDUCATOR_PT :
                $tableMeta = Apollo_Tables::_APL_AGENCY_EDUCATOR;
                $keyMeta = 'edu_id';
                break;
            case Apollo_DB_Schema::_ORGANIZATION_PT :
                $tableMeta = Apollo_Tables::_APL_AGENCY_ORG;
                $keyMeta = 'org_id';
                break;
            case Apollo_DB_Schema::_VENUE_PT :
                $tableMeta = Apollo_Tables::_APL_AGENCY_VENUE;
                $keyMeta = 'venue_id';
                break;
            case Apollo_DB_Schema::_ARTIST_PT :
                $tableMeta = Apollo_Tables::_APL_AGENCY_ARTIST;
                $keyMeta = 'artist_id';
                break;
            case 'user' :
                $tableMeta = Apollo_Tables::_APL_USER_MODULES;
                $keyMeta = 'user_id';
                break;
            default :
                $tableMeta = '';
                $keyMeta = '';
                break;
        }
        $apl_query = new Apl_Query($wpdb->prefix . $tableMeta);

        if (!empty($listRemove)) {
            $listIDs = implode(",", $listRemove);
            if ($postType == 'user') {
                $apl_query->delete(" post_id = {$agencyID} AND {$keyMeta} IN ( {$listIDs} ) AND  post_type = '". Apollo_DB_Schema::_AGENCY_PT ."'");
            } else {
                $apl_query->delete(" agency_id = {$agencyID} AND {$keyMeta} IN ( {$listIDs} ) ");
            }
        }
        if (!empty($listChecked)) {
            foreach ($listChecked as $item) {
                if ($postType == 'user') {
                    $apl_query->insert(array(
                        'user_id' => $item,
                        'post_id' => $agencyID,
                        'post_type' => Apollo_DB_Schema::_AGENCY_PT
                    ));
                } else {
                    $apl_query->insert(array(
                        'agency_id' => $agencyID,
                        $keyMeta => $item
                    ));
                }
            }
        }

    }
    
    public function agency_form() {
        
        $is_submitting = isset( $_POST['submit'] );
     
        $row = false;
        if ( isset( $_POST['submit'] ) ) {

            $this->set_errors();
            
            if ( ! $this->errors ) {
                $this->save();
                Apollo_App::safeRedirect( admin_url() . 'admin.php?&page=admin-apollo-agency-form&msg=true&action=edit&id=' . $this->id );
            }
        } else {
            if ( isset( $_GET['id'] ) && $_GET['id'] && isset( $_GET['action'] ) && $_GET['action'] == 'edit' ) {
                global $wpdb;
                $id = $_GET['id'];
                $wpdb->escape_by_ref($id);
                $apl_query = new Apl_Query( $wpdb->{Apollo_Tables::_APL_AGENCY} );
                $row = $apl_query->get_row( "agencyID = $id" );
            }
        }
        ?>
        <div class="wrap">
            <form method="post" action="">
            <h2 class="agency-left-title"> <?php isset( $_GET['id'] ) && $_GET['id'] ? _e( 'Edit Agency', 'apollo' ) : _e( 'Add New Agency', 'apollo' ) ?></h2>
            <p class="submit">
                <input type="submit" name="submit" id="submit" class="apl-agency-submit button button-primary" value="<?php 
                echo ( isset( $_GET['id'] ) && $_GET['id'] ) ? __( 'Save Agency', 'apollo' ) : __( 'Add New', 'apollo' ) ?>">
            </p>
            <br/>
            <div class="postbox apl-custom-postbox">
                <h3 class="hndle apl-custom-title"><span><?php _e( 'General Information', 'apollo' ) ?>  <i style='font-weight: normal'><?php if ( isset( $_GET['id'] ) && $_GET['id'] ) echo '('.sprintf( 'ID: %d', $_GET['id'] ). ')'; ?></i></span></h3>   
                
                <?php 
                if ( isset( $_GET['msg'] ) ): 
                ?>
                
                <div id="message" class="updated below-h2"><p>
                    <?php _e( 'Agency saved ', 'apollo' ) ?>
                </p></div>
                <?php endif; ?>
                
                
                    <table class="form-table apl-custom-table">
                        <tbody>
                            <?php
                                foreach ( $this->forms as $k => $f ):
                            ?>
                            <tr class="form-field">
                                <th scope="row"><label><?php 
                                    echo $f['label']; 
                                    if ( isset( $f['validate'] )  ) {
                                        $valid_arr = explode( ',', $f['validate'] );
                                        if ( in_array( 'required' , $valid_arr ) ) echo '<span class="error"> *</span>'; 
                                    }
                                    
                                    ?></label>
                                </th>
                                <td>
                                    <?php 
                                        $error = $this->get_error_field( $k );
                                    ?>
                                    
                                    <input name="<?php echo $k ?>" 
                                           value="<?php echo $is_submitting ? $_POST[$k] : ( is_object( $row ) && $row ? $row->{$k} : '' ) ?>" 
                                           type="text" class="<?php echo $error ? 'error ' : '';  echo isset( $f['class'] ) ? $f['class'] : '' ?>"><br/> <?php
                                        
                                        echo $error;
                                    ?>
                                    
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                   
              
            </div>
            
            <?php
            $associationCheckboxesComp = apl_instance('APL_Lib_Association_Components_AssociationCheckboxes');
            if ( Apollo_App::is_avaiable_module(Apollo_DB_Schema::_EDUCATION) ) {
                $associationCheckboxesComp->render(array(
                    'post_type'     => Apollo_DB_Schema::_EDUCATOR_PT,
                    'label'         => __( 'Educator', 'apollo' ),
                    'associated_id' => 'educator-id',
                    'wrap_class'    => 'user-association educator',
                    'admin_type' => Apollo_DB_Schema::_AGENCY_PT,
                    'agency_id' => isset($id) ? $id : 0,
                ));
            }
            
            if ( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ORGANIZATION_PT ) ) {
                $associationCheckboxesComp->render(array(
                    'post_type'     => Apollo_DB_Schema::_ORGANIZATION_PT,
                    'label'         => __( 'Organization', 'apollo' ),
                    'associated_id' => 'organization-id',
                    'wrap_class'    => 'user-association organization',
                    'admin_type' => Apollo_DB_Schema::_AGENCY_PT,
                    'agency_id' => isset($id) ? $id : 0,
                ));
            }
            
            if ( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_VENUE_PT ) ) {
                $associationCheckboxesComp->render(array(
                    'post_type'     => Apollo_DB_Schema::_VENUE_PT,
                    'label'         => __( 'Venue', 'apollo' ),
                    'associated_id' => 'venue-id',
                    'wrap_class'    => 'user-association venue',
                    'admin_type' => Apollo_DB_Schema::_AGENCY_PT,
                    'agency_id' => isset($id) ? $id : 0,
                ));
            }

            /** @Ticket #14988 */
            if ( Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ARTIST_PT ) ) {
                $associationCheckboxesComp->render(array(
                    'post_type'     => Apollo_DB_Schema::_ARTIST_PT,
                    'label'         => __( 'Artist', 'apollo' ),
                    'associated_id' => 'artist-id',
                    'wrap_class'    => 'user-association artist',
                    'admin_type' => Apollo_DB_Schema::_AGENCY_PT,
                    'agency_id' => isset($id) ? $id : 0,
                ));
            }

            $associationCheckboxesComp->render(array(
                'post_type'     => 'user',
                'label'         => __( 'User', 'apollo' ),
                'associated_id' => 'user-id',
                'wrap_class'    => 'user-association user',
                'admin_type' => Apollo_DB_Schema::_AGENCY_PT,
                'agency_id' => isset($id) ? $id : 0,
            ));
            
            ?>
            
            <p class="submit">
                <input type="submit" name="submit" id="submit" class="apl-agency-submit button button-primary" value="<?php 
                echo ( isset( $_GET['id'] ) && $_GET['id'] ) ? __( 'Save Agency', 'apollo' ) : __( 'Add New', 'apollo' ) ?>">
            </p>
              </form>
        </div>   
        <?php
    }

    public function save()
    {
        global $wpdb;

        $name    = isset( $_POST['name'] )    ? $_POST['name']    : '';
        $address = isset( $_POST['address'] ) ? $_POST['address'] : '';
        $phone   = isset( $_POST['phone'] )   ? $_POST['phone']   : '';
        $email   = isset( $_POST['email'] )   ? $_POST['email']   : '';
        $url     = isset( $_POST['url'] )     ? $_POST['url']     : '';

        $wpdb->escape_by_ref( $name    );
        $wpdb->escape_by_ref( $address );
        $wpdb->escape_by_ref( $phone   );
        $wpdb->escape_by_ref( $email   );
        $wpdb->escape_by_ref( $url     );
        $name = str_replace('\\', '', $name);

        $apl_query = new Apl_Query( $wpdb->{Apollo_Tables::_APL_AGENCY} );
        if ( isset( $_GET['action'] ) && $_GET['action'] == 'edit' && isset( $_GET['id'] ) && $_GET['id'] ) {
            $this->id = intval( $_GET['id'] );
            $apl_query->update( array(
                'name'    => $name,
                'address' => $address,
                'phone'   => $phone,
                'email'   => $email,
                'url'     => $url,
            ), array( 'agencyID' => $this->id ) );
        } else {
            $apl_query->insert( array( 
                'name'    => $name,
                'address' => $address,
                'phone'   => $phone,
                'email'   => $email,
                'url'     => $url,
            ) );
        }

        // Get lastest
        $lastest = $wpdb->get_row( " SELECT * FROM {$wpdb->{Apollo_Tables::_APL_AGENCY}} WHERE 1=1 AND name='$name' AND email='$email' "
            . "AND phone = '$phone' AND url='$url' AND address='$address' ORDER BY agencyID desc LIMIT 1 " );
        if ( $lastest ) {
            $this->id = $lastest->agencyID;
        } else if ( is_null( $lastest ) && !$this->id ) {
            $this->id = $wpdb->insert_id;
        }

        if (isset($_POST['organization-id-checked']) && isset($_POST['organization-id-unchecked'])) {
            self::updateAssociation($this->id, Apollo_DB_Schema::_ORGANIZATION_PT, $_POST['organization-id-checked'], $_POST['organization-id-unchecked']);
        }

        if (isset($_POST['educator-id-checked']) && isset($_POST['educator-id-unchecked'])) {
            self::updateAssociation($this->id, Apollo_DB_Schema::_EDUCATOR_PT, $_POST['educator-id-checked'], $_POST['educator-id-unchecked']);
        }

        if (isset($_POST['venue-id-checked']) && isset($_POST['venue-id-unchecked'])) {
            self::updateAssociation($this->id, Apollo_DB_Schema::_VENUE_PT, $_POST['venue-id-checked'], $_POST['venue-id-unchecked']);
        }

        if (isset($_POST['artist-id-checked']) && isset($_POST['artist-id-unchecked'])) {
            self::updateAssociation($this->id, Apollo_DB_Schema::_ARTIST_PT, $_POST['artist-id-checked'], $_POST['artist-id-unchecked']);
        }

        if (isset($_POST['user-id-checked']) && isset($_POST['user-id-unchecked'])) {
            self::updateAssociation($this->id, 'user', $_POST['user-id-checked'], $_POST['user-id-unchecked']);
        }

    }
    
    public function set_errors() {
        $require = __( '%s is required', 'apollo' );
        $valid   = __( '%s is invalid', 'apollo' );
        $p = $_POST;
        foreach ( $this->forms as $k => $e ) {
            $valids = explode( ',', $e['validate'] );
            $val = isset( $_POST[$k] ) ? $_POST[$k] : '';
            
            foreach ( $valids as $v ) {
                switch ( $v ):
                    case 'required':
                        if ( ! $val )
                            $this->errors[$k] = $require;
                        break;
                    
                    case 'email':
                        if ( $val && !is_email( $val ) )
                            $this->errors[$k] = $valid;
                        break;
                        
                    case 'url':
                        if ( $val && ! filter_var( $val, FILTER_VALIDATE_URL ) )
                            $this->errors[$k] = $valid;
                        break;    
                endswitch;
            }
        }
    }
    
    private function get_error_field( $fieldname ) {
        
        $field = $this->forms[$fieldname];
      
        if ( isset( $this->errors[$fieldname] ) ) {
            return '<span class="error">'.( sprintf( $this->errors[$fieldname], $field['label'] ) ).'</span>';
        }
        return '';
    }
    
    private function get_offset() {
        $pagenum = isset( $_GET['paged'] ) ? absint( $_GET['paged'] ) : 1;
        return ( $pagenum - 1 ) * $this->pagesize;
    }


}
new Apollo_Admin_Agency();