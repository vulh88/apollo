<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Apollo_Admin_Config' ) ) :
    class Apollo_Admin_Config {
    
        public static function additionalFieldMods() {
            return array(
                Apollo_DB_Schema::_ARTIST_PT,  
                Apollo_DB_Schema::_EDUCATOR_PT,
                Apollo_DB_Schema::_PROGRAM_PT,  
                Apollo_DB_Schema::_EDU_EVALUATION,
                Apollo_DB_Schema::_GRANT_EDUCATION,
                Apollo_DB_Schema::_ORGANIZATION_PT,
                Apollo_DB_Schema::_VENUE_PT,
                Apollo_DB_Schema::_CLASSIFIED,
                Apollo_DB_Schema::_PUBLIC_ART_PT,
                Apollo_DB_Schema::_BUSINESS_PT,
                Apollo_DB_Schema::_EVENT_PT
            );
        }
        
        public static function iconMods() {
            return array(
                Apollo_DB_Schema::_ORGANIZATION_PT,
                Apollo_DB_Schema::_VENUE_PT,
                Apollo_DB_Schema::_CLASSIFIED,
                Apollo_DB_Schema::_PUBLIC_ART_PT,
                Apollo_DB_Schema::_EDUCATOR_PT,
                Apollo_DB_Schema::_ARTIST_PT,
                Apollo_DB_Schema::_EVENT_PT
            );
        }
    }
endif;

