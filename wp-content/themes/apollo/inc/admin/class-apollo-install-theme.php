<?php

/**
 * Options Framework Handle front end function
 *
 * @class 		Apollo_Install_Theme
 * @package		inc/Admin
 * @category	Class
 * @author 		vulh
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Apollo_Install_Theme {

    private $_lock_dir = '_apollo_lock.theme.install';
    const _DO_NOT_RESET_DATA_WITH_THEMES = array('Apollo Theme', 'Octave Theme');

    function init() {

        // Handle after activate theme to set default template
        add_action( 'after_switch_theme', array( $this, 'install' ), 10, 1 );
        add_action( 'after_switch_theme', array( $this, 'add_widgets' ), 10, 1 );
        add_action( 'after_switch_theme', array( $this, 'apollo_active_theme_default_template' ), 10, 1 );
        add_action( 'after_switch_theme', array( $this, 'oc_update_sidebars_widgets' ), 10, 1 );
        add_action("switch_theme", array($this, 'cleanup'), 10 , 3);
    }

    /**
     * Backup sidebars widgets when switch theme
     * @return bool
     */
    private function octaveBackupSidebarsWidgets() {
        $currentSidebarsWidgets = get_option('sidebars_widgets', array());

        if(empty($currentSidebarsWidgets)) {
            return true;
        }
        update_option('apollo_sidebars_widgets_backup', $currentSidebarsWidgets);
    }

    /**
     * Restore sidebars widgets when switch theme
     * @return bool
     */
    private function octaveRestoreSidebarsWidgets() {
        $ocSidebarsWidgets = get_option('apollo_sidebars_widgets_backup', array());

        if(empty($ocSidebarsWidgets)) {
            return true;
        }
        update_option('sidebars_widgets', $ocSidebarsWidgets);
    }

    /**
     * @param $oldTheme
     * @param $newTheme
     * @return bool
     */
    private function checkThemeDoNotResetData($oldTheme, $newTheme) {
        return (
            in_array($oldTheme, Apollo_Install_Theme::_DO_NOT_RESET_DATA_WITH_THEMES) &&
            in_array($newTheme, Apollo_Install_Theme::_DO_NOT_RESET_DATA_WITH_THEMES)
        );
    }

    public function oc_update_sidebars_widgets($oldTheme = '') {
        if($this->checkThemeDoNotResetData($oldTheme, (string)(wp_get_theme()->name))){
            $this->octaveRestoreSidebarsWidgets();
        }
    }

    function remove_primary_and_add_default_module_widget(&$sidebar_options) {
        $sidebar_options[Apollo_DB_Schema::_SIDEBAR_PREFIX . '-primary'] = array ();
        
        $avaiable_modules = Apollo_App::get_avaiable_modules();
        if( $avaiable_modules ) {
            foreach( $avaiable_modules as $am ) {
                if ( $am == Apollo_DB_Schema::_EVENT_PT ) continue;
                
                $am = str_replace('-', '_', $am);
                
                $sidebar_options[Apollo_DB_Schema::_SIDEBAR_PREFIX . '-' . $am] = array();

                $sidebar_options[Apollo_DB_Schema::_SIDEBAR_PREFIX . '-' . $am] = array(
                    '_multiwidget' => 1,
                );

                $widget_name = 'apollo_search_' . $am .'_widget';
                $widget = get_option('widget_' . $widget_name, array());
                $widget[2] = array(
                    'title' => '',
                );

                $sidebar_options[Apollo_DB_Schema::_SIDEBAR_PREFIX . '-' . $am][] = $widget_name.'-2';

                update_option('widget_'.$widget_name,$widget);
            }
        }

        update_option('sidebars_widgets', $sidebar_options);
    }
    
    /**
    * Add default widgets
    * These functions are meant to update options that contain header, footer, siteinfo.
   /**
    * @return void
    */
    function add_widgets($oldTheme = '') {
        if ($this->checkThemeDoNotResetData($oldTheme, (string)(wp_get_theme()->name))){
            return;
        }
        
        $sidebar_options = get_option('sidebars_widgets');

        $this->remove_primary_and_add_default_module_widget($sidebar_options);

        /* Add to apl-sidebar-primary  */
        $this->add_widget( 'apollo_search_event_widget' );
        $this->add_widget( 'apollo_custom_widget', Apollo_DB_Schema::_SIDEBAR_PREFIX . '-primary', array(
            'title'     => '',
            'content'   => '<a href="#" target="_blank"><img src="/wp-content/themes/apollo/assets/uploads/sgl.jpg" alt="sale"></a>'
        ) );
        $this->add_widget( 'apollo_most_reviews_widget' );
        $this->add_widget( 'apollo_subscribe_widget' );
        $this->add_widget( 'apollo_custom_widget', Apollo_DB_Schema::_SIDEBAR_PREFIX . '-primary', array(
            'title'     => 'GET YOUR FREE',
            'content'   => '<a href="#" target="_blank"><img src="/wp-content/themes/apollo/assets/uploads/sale.jpg" alt="sale"></a>'
        ) );

        /* Add to some sidebar depend on */

    }
    
    function add_widget( $widget_name = 'apollo_custom_widget', $add_to_sidebar = '', $settings = array() ) {

        if($add_to_sidebar === '') {
            $add_to_sidebar = Apollo_DB_Schema::_SIDEBAR_PREFIX .'-'. 'primary';
        }
        // Reset empty sidebar-primary
        $sidebar_options = get_option('sidebars_widgets');

        if(!isset($sidebar_options[$add_to_sidebar])){
            $sidebar_options[$add_to_sidebar] = array('_multiwidget'=>1);
        }
        
        $widget = get_option('widget_'.$widget_name);
        
        if( ! is_array($widget ) ) {
            $widget = array();
        }
        
        $count = count($widget) + 1;
        
        // add first widget to sidebar:
        $sidebar_options[$add_to_sidebar][] = $widget_name.'-'.$count;
        
        $widget[$count] = $settings;
        
        update_option('sidebars_widgets',$sidebar_options);
        update_option('widget_'.$widget_name,$widget);
    }

    function apollo_active_theme_default_template($oldTheme = '') {
        if ($this->checkThemeDoNotResetData($oldTheme, (string)(wp_get_theme()->name))){
            return;
        }
        require_once APOLLO_ADMIN_DIR. '/theme-options/includes/class-options-framework-admin.php';
        
        /**
         * Author : ThienLD
         * Custom to allow custom default template for static HTML under child theme
        */
        $defaultTemplates = Apollo_Theme_Options_Default_Template::$themeOptsDefaultTemplates;
        
        $defaultTemplates = apply_filters('_apl_custom_theme_options_default_templates', $defaultTemplates);

        if(!empty($defaultTemplates)){
            foreach ( $defaultTemplates as $tplKey => $tplPath ){
                Options_Framework_Admin::update_html_file( render_php_to_string( $tplPath ) , $tplKey );
            }
        }

        $this->create_default_email_templates();
    }
    
    /**
     * Create default email templates
     */
    public function create_default_email_templates() {
        // Create default email templates 
        global $apl_email_templates;
        if ( $apl_email_templates ) {
            foreach ( $apl_email_templates as $key => $val ) {
                $fileTo = get_html_file_abs_path( $key );
                if( !file_exists($fileTo)) {
                    @copy(APOLLO_DEFAULT_EMAIL_TEMPLATES_DIR. '/'.$key. '.php', $fileTo);
                }
            }
        }
    }
    
    public function install( $stylesheet = '' ) {

        /* Return immediately when is ajax or cron*/
        if(defined('DOING_CRON') || (defined('DOING_AJAX') && DOING_AJAX)) {
            return;
        }

        /* be sure this theme onlywork from theme.php */
        if(isset($GLOBALS['pagenow']) && 'themes.php' !== $GLOBALS['pagenow']) {
            return;
        }
        /* Manual lock by dir */
        $path =WP_CONTENT_DIR.'/uploads/'.$this->_lock_dir;
        if(true !== is_writable(WP_CONTENT_DIR.'/uploads')) {
            throw new Exception("Please set write permission for ".WP_CONTENT_DIR.'/uploads'." directory!");
        }

        if((file_exists($path) && (time() - filectime($path) <= (0.5 * MINUTE_IN_SECONDS) )) ) {
            wp_die('Please wating half minute');
        };

        @mkdir($path);

        // Create tables
        $this->create_tables();

        // Create Roles
//        $this->create_roles();

        $this->create_account_manager_role();

        // Create default page register - Register page,
        $this->create_default_pages();
        
        // Insert default data
        $this->insert_default_data();

        /**
         * Init activation version option
         * @ticket #11493
         */
        $this->updateThemeVersion();

        rmdir(WP_CONTENT_DIR.'/uploads/'.$this->_lock_dir);
    }

    public function insert_default_data()
    {
        $newblog = new Apollo_Blog($GLOBALS['blog_id']);
        $newblog->createData();
        
        // Create default edu evaluation additional fields
        $this->_insert_cf_edu_evaluation();
        
        // Create default grant education additional fields
        $this->_insert_cf_grant_education();

        // auto fill data for table post at lang column
        $newblog->autoFillLangCodePostTerm();
    }
  
    
    /**
     * Insert default value to edu evaluation
     */
    private function _insert_cf_grant_education() {
        
        $apl_query = new Apl_Query( Apollo_Tables::_APL_CUSTOM_FIELD );

        $total = $apl_query->get_total("post_type = '".Apollo_DB_Schema::_GRANT_EDUCATION."'");

        if ( $total ) return false;
        
        $groupFields = array(
            __( 'CULTURAL PROGRAM INFORMATION', 'apollo' ) => array(
                array(
                    'label'     => __("Cultural Organization", 'apollo'),
                    'cf_type'   => 'select',
                    'required'  => true,
                    'meta'      => array('data_source'   => Apollo_DB_Schema::_EDUCATOR_PT, 'refer_to' => '', 'display_style' => ''  ),
                ),
                array(
                    'label'     => __('Cultural Program Provided', 'apollo'),
                    'cf_type'   => 'select',
                    'required'  => true,
                    'meta'      => array('data_source'   => Apollo_DB_Schema::_PROGRAM_PT, 'display_style' => ''  ),
                ),
            ),
            
            __( 'Teacher/School Information', 'apollo' ) => array(
                array(
                    'label'     => __("Teacher's Name", 'apollo'),
                    'cf_type'   => 'text',
                    'required'  => true,
                ),
                array(
                    'label'     => __('Teacher Contact Information', 'apollo'),
                    'cf_type'   => 'text',
                    'required'  => true,
                ),
                array(
                    'label'     => __('School Name', 'apollo'),
                    'cf_type'   => 'text',
                    'required'  => true,
                ),
                array(
                    'label'     => __('District', 'apollo'),
                    'cf_type'   => 'text',
                    'required'  => true,
                ),
                array(
                    'label'     => __('Grand', 'apollo'),
                    'cf_type'   => 'text',
                    'required'  => true,
                ),
                array(
                    'label'     => __('Number of students', 'apollo'),
                    'cf_type'   => 'text',
                    'required'  => true,
                    'meta'      => array('validation'   => 'number'),
                ),
            ), // End Program Datetime
            
            __( 'Arts Program Information', 'apollo' ) => array(
                array(
                    'label'     => __('If not listed above, please enter other organization here', 'apollo'),
                    'cf_type'   => 'text',
                    'required'  => false,
                ),
                array(
                    'label'     => __('What is the name of the program you are applying for? (50 words max)', 'apollo'),
                    'cf_type'   => 'text',
                    'meta'      => array(
                        'character_limit'   => 50,
                    )
                ),
                
                array(
                    'label'     => __('The grant is primaryly for: (select one)', 'apollo'),
                    'cf_type'   => 'radio',
                    'required'  => false,
                    'meta'      => array(
                        'other_choice'  => 1,
                        'choice' => "1 : Field trip/admission fees
2 : Artist fees
3 : Transportation
4 : Materials/supplies",
                    )
                ),
                array(
                    'label'     => __('Date of event or program', 'apollo'),
                    'cf_type'   => 'text',
                    'required'  => false,
                    'meta'      => array( 'validation' => 'datepicker' )
                ),
                array(
                    'label'     => __('Date when funds are needed', 'apollo'),
                    'cf_type'   => 'text',
                    'required'  => false,
                    'meta'      => array( 'validation' => 'datepicker' )
                ),
                array(
                    'label'     => __('I have permission from my principal to apply for this grant', 'apollo'),
                    'cf_type'   => 'checkbox',
                    'required'  => false,
                ),
            ),
            
            __( 'BUDGET INFORMATION', 'apollo' ) => array(
                array(
                    'label'     => __('Total amount requested (up to $1,000 per teacher)(*)', 'apollo'),
                    'cf_type'   => 'text',
                    'required'  => true,
                ),
                array(
                    'label'     => __('Transportation($)', 'apollo'),
                    'cf_type'   => 'text',
                    'required'  => false,
                ),
                array(
                    'label'     => __('Ticket/ admission fees($)', 'apollo'),
                    'cf_type'   => 'text',
                    'required'  => false,
                ),
                array(
                    'label'     => __('Artist fees($)', 'apollo'),
                    'cf_type'   => 'text',
                    'required'  => false,
                ),
                array(
                    'label'     => __('Materials/ supplies($)', 'apollo'),
                    'cf_type'   => 'text',
                    'required'  => false,
                ),
                array(
                    'label'     => __('Other($)', 'apollo'),
                    'cf_type'   => 'text',
                    'required'  => false,
                ),
            ),
            
            __( 'NARRATIVE QUESTIONS', 'apollo' ) => array(
                array(
                    'label'     => __('What is the purpose of the program/activity? (150 words max)', 'apollo'),
                    'cf_type'   => 'textarea',
                    'required'  => true,
                    'meta'      => array(
                        'character_limit'   => 150,
                    )
                ),
                array(
                    'label'     => __('How does this relate to your curriculum and what in-class activities will be '
                        . 'planned to reinforce the learning? (150 words max)', 'apollo'),
                    'cf_type'   => 'textarea',
                    'required'  => true,
                    'meta'      => array(
                        'character_limit'   => 150,
                    )
                ),
                array(
                    'label'     => __('How will students benefit from this experience? (150 words max)', 'apollo'),
                    'cf_type'   => 'textarea',
                    'required'  => true,
                    'meta'      => array(
                        'character_limit'   => 150,
                    )
                ),
            ),
        );
        $gOrder = 0;
        foreach ( $groupFields as $group => $fields ):
            // Insert group
            $group_meta = isset($fields['group_meta']) ? $fields['group_meta'] : '';
            $apl_query->insert( array(
                'label'         => $group,
                'post_type'     => Apollo_DB_Schema::_GRANT_EDUCATION,
                'meta_data'     => maybe_serialize($group_meta),
                'cf_order'      => $gOrder++,
            ) );
            $gid = $apl_query->get_bottom();
            
            foreach( $fields as $i => $field ):
                // Insert fields
                if ( $i === 'group_meta' ) continue;

                $meta = isset( $field['meta'] ) ? Apollo_App::clean_array_data( $field['meta'] ) : '';
            
                $data = array(
                    'label'     => $field['label'],
                    'parent'    => $gid,
                    'cf_order'  => $i + 1,
                    'name'      => 'cf_field_'. time(). Apollo_App::generateRandomString(5),
                    'post_type' => Apollo_DB_Schema::_GRANT_EDUCATION,
                    'cf_type'   => $field['cf_type'],
                    'required'  => isset($field['required']) && $field['required']  ? 1 : 0,
                    'location'  => maybe_serialize( array('dp', 'ff') ),
                    'meta_data' => $meta ? maybe_serialize( $meta ) : '',
                );
                $apl_query->insert( $data );
            endforeach;
            
        endforeach;
    }
    
    /**
     * Insert default value to edu evaluation
     */
    private function _insert_cf_edu_evaluation() {
        
        $apl_query = new Apl_Query( Apollo_Tables::_APL_CUSTOM_FIELD );
        
        $total = $apl_query->get_total("post_type = '".Apollo_DB_Schema::_EDU_EVALUATION."'");
        
        if ( $total ) return false;
        
        $groupFields = array(
            __( 'SCHOOL INFORMATION', 'apollo' ) => array(
                array(
                    'label'     => __('Your School District', 'apollo'),
                    'cf_type'   => 'select',
                    'required'  => true,
                    'meta'      => array('data_source'   => 'district', 'display_style' => ''  ),
                ),
                array(
                    'label'     => __("Your School Name", 'apollo'),
                    'cf_type'   => 'select',
                    'required'  => true,
                    'meta'      => array('data_source'   => 'school', 'refer_to' => '', 'display_style' => ''  ),
                ),
            ),
            
            __( 'CULTURAL PROGRAM INFORMATION', 'apollo' ) => array(
                array(
                    'label'     => __("Cultural Organization", 'apollo'),
                    'cf_type'   => 'select',
                    'required'  => true,
                    'meta'      => array(
                                        'data_source'   => Apollo_DB_Schema::_EDUCATOR_PT, 
                                        'refer_to' => '',
                                        'display_style' => '',
                                    ),
                ),
                array(
                    'label'     => __('Cultural Program Provided', 'apollo'),
                    'cf_type'   => 'select',
                    'required'  => true,
                    'meta'      => array(
                                        'data_source'   => Apollo_DB_Schema::_PROGRAM_PT, 
                                        'display_style' => '' 
                                    ),
                ),
            ),
            
            __( 'Program Datetime', 'apollo' ) => array(
                array(
                    'label'     => __('Program Date(s)', 'apollo'),
                    'cf_type'   => 'text',
                    'required'  => true,
                ),
                array(
                    'label'     => __('Program Time(s)', 'apollo'),
                    'cf_type'   => 'text',
                    'required'  => true,
                ),
                array(
                    'label'     => __('Lesson Plans Received', 'apollo'),
                    'cf_type'   => 'checkbox',
                    'required'  => false,
                ),
            ), // End Program Datetime
            
            __( 'Participation', 'apollo' ) => array(
                array(
                    'label'     => __('# Classes', 'apollo'),
                    'cf_type'   => 'text',
                    'required'  => true,
                ),
                array(
                    'label'     => __('# Students', 'apollo'),
                    'cf_type'   => 'text',
                    'required'  => true,
                ),
                array(
                    'label'     => __('Grade Levels', 'apollo'),
                    'cf_type'   => 'text',
                    'required'  => true,
                ), // End Participation
                'group_meta'    => array(
                    'group' => array(
                                'prepend'   => htmlspecialchars('<h2>'),
                                'append'    => htmlspecialchars('</h2>'),
                            ),
                )
            ),
            
            __( 'Contact Information (Optional)', 'apollo' ) => array(
                array(
                    'label'     => __('Teacher name/Filled out by', 'apollo'),
                    'cf_type'   => 'text',
                    'required'  => false,
                ),
                array(
                    'label'     => __('Contact Phone', 'apollo'),
                    'cf_type'   => 'text',
                    'required'  => false,
                ),
                array(
                    'label'     => __('Email Address', 'apollo'),
                    'cf_type'   => 'text',
                    'required'  => false,
                    'meta'    => array( 'validation' => 'email' )
                ),
                
                array(
                    'label'     => __('1. The lesson plans were adequate to prepare students. (If not received, leave blank.)', 'apollo'),
                    'cf_type'   => 'select',
                    'required'  => false,
                ),
                array(
                    'label'     => __('2. The program correlates to the Florida Sunshine State Standards.', 'apollo'),
                    'cf_type'   => 'select',
                    'required'  => false,
                ),
                array(
                    'label'     => __('3. The program was appropriate for the students (content, length, etc)', 'apollo'),
                    'cf_type'   => 'select',
                    'required'  => false,
                ),
                array(
                    'label'     => __('4. The presenter engaged the students, helping them learn.', 'apollo'),
                    'cf_type'   => 'select',
                    'required'  => false,
                ),
                array(
                    'label'     => __('5. How likely are you to return to this program with another group?', 'apollo'),
                    'cf_type'   => 'select',
                    'required'  => false,
                ),
                array(
                    'label'     => __('6. How valuable was this experience for your students? Explain below', 'apollo'),
                    'cf_type'   => 'select',
                    'required'  => false,
                    'meta'      => array( 'explain' => 1 ),
                ),
                array(
                    'label'     => __('7. How will you integrate this learning experience into your classroom lesson planning?', 'apollo'),
                    'cf_type'   => 'textarea',
                    'required'  => false,
                    'meta'      => array( 'desc' => '<p>  Help available for...<br>Orange County School District teachers: 
                      Scott Evans, Fine Arts Coordinator, at 407-317-3200 x2002769 or scott.evans@ocps.net<br><br>Osceola County School District teachers: 
                      Debbie Fahmie, Fine &amp; Performing Arts Specialist, at 407-346-2444 or fahmied@osceola.k12.fl.us<br><br>Lake County School District teachers: 
                      <laura>Woodham, Program Specialist, at 352-742-6909 or woodhaml@lake.k12.fl.us</laura><br><br>Seminole County School District teachers: 
                      Mary Lane, Curriculum Specialist, at 407-320-0192 or Mary_Lane@scps.k12.fl.us
                    </p>',
                    'display_style' => 'has_label',
                    ),
                ),
                array(
                    'label'     => __('8. How did the booking process go and do you have any suggestions for how to make it easier for teachers?', 'apollo'),
                    'cf_type'   => 'textarea',
                    'required'  => false,
                ),
                array(
                    'label'     => __('9. Additional comments/suggestions?', 'apollo'),
                    'cf_type'   => 'textarea',
                    'required'  => false,
                ),
                // End Contact Information (Optional)
            )
        );
        $gOrder = 0;
        foreach ( $groupFields as $group => $fields ):
            // Insert group
            $group_meta = isset($fields['group_meta']) ? $fields['group_meta'] : '';
            $apl_query->insert( array( 
                'label'         => $group,
                'post_type'     => Apollo_DB_Schema::_EDU_EVALUATION,
                'meta_data'    => maybe_serialize($group_meta),
                'cf_order'      => $gOrder++,
            ) );
            $gid = $apl_query->get_bottom();
         
            foreach( $fields as $i => $field ):
                // Insert fields
                if ( $i === 'group_meta' ) continue;
                
                $meta = isset( $field['meta'] ) ? Apollo_App::clean_array_data( $field['meta'] ) : array();
                
                if ( $field['cf_type'] == 'select' && !isset($meta['display_style']) ) {
                    $meta['choice'] = "1 : 1
2 : 2
3 : 3
4 : 4
5 : 5";
                    if ( !isset($meta['display_style']) ) {
                        $meta['display_style'] = 'right_label';
                    }
                }
            
                $data = array(
                    'label'     => $field['label'],
                    'parent'    => $gid,
                    'cf_order'  => $i + 1,
                    'name'      => 'cf_field_'. time(). Apollo_App::generateRandomString(5),
                    'post_type' => Apollo_DB_Schema::_EDU_EVALUATION,
                    'cf_type'   => $field['cf_type'],
                    'required'  => $field['required'] ? 1 : 0,
                    'location'  => maybe_serialize( array('dp', 'ff') ),
                    'meta_data' => $meta ? maybe_serialize( $meta ) : '',
                );
                $apl_query->insert( $data );
            endforeach;
            
        endforeach;
    }
    
    public function create_roles() {
        global $wp_roles;
       
        if ( class_exists( 'WP_Roles' ) ) {
            if ( ! isset( $wp_roles ) ) {
                $wp_roles = new WP_Roles;
            }
        }
        
        if (is_object( $wp_roles ) ) {
            $capabilities = $this->get_core_capabilities();
            foreach ($capabilities as $type => $arr_cap) {
                add_role( $type . '_manager', __( ucfirst($type) . ' Manager', 'apollo' ), $arr_cap );
            }

            $arr_roles = Apollo_Role::getRoles();

            foreach($arr_roles as $role => $child_role) {
                // get cap child roles
                foreach($child_role as $r) {
                    $objr = get_role($r);

                    if(is_null($objr)) continue;

                    foreach($objr->capabilities as $cap => $grant) {
                        $wp_roles->add_cap($role, $cap, $grant);
                    }
                }
            }
        }
    }

    /**
     * Create account manager role
     * @author quocpt
     */
    public function create_account_manager_role() {
        global $wp_roles;
        if ( ! isset( $wp_roles ) )
            $wp_roles = new WP_Roles();

        $editor = $wp_roles->get_role('editor');
        $apl_caps = Apollo_Role::getCapabilities();
        $manage_user_caps = $apl_caps['manage_user_accounts'];
        $caps = array_merge($editor->capabilities, $manage_user_caps);

        //Adding 'Account Manager' with all Editor roles caps
        $wp_roles->add_role('apl_account_manager', __('Account Manager', 'apollo'), $caps);
    }
    
    /**
	 * Get capabilities for WooCommerce - these are assigned to admin/shop manager during installation or reset
	 *
	 * @access public
	 * @return array
	 */
	public function get_core_capabilities() {
		$capabilities = array();
        
		$capability_types = array( 'event', 'organization', 'venue', 'artist' );
        
		foreach ( $capability_types as $capability_type ) {

			$capabilities[ $capability_type ] = array(
				// Post type
				"edit_{$capability_type}" => true,
				"read_{$capability_type}" => true,
				"delete_{$capability_type}" => true,
				"edit_{$capability_type}s" => true,
				"edit_others_{$capability_type}s" => true,
				"publish_{$capability_type}s" => true,
				"read_private_{$capability_type}s" => true,
				"delete_{$capability_type}s" => true,
				"delete_private_{$capability_type}s" => true,
				"delete_published_{$capability_type}s" => true,
				"delete_others_{$capability_type}s" => true,
				"edit_private_{$capability_type}s" => true,
				"edit_published_{$capability_type}s" => true,

				// Terms
				"manage_{$capability_type}_terms" => true,
				"edit_{$capability_type}_terms" => true,
				"delete_{$capability_type}_terms" => true,
				"assign_{$capability_type}_terms" => true,
			);
		}

		return $capabilities;
	}
    
    public function create_tables() {
        global $wpdb;
     
		$wpdb->hide_errors();

		$collate = '';

		if ( $wpdb->has_cap( 'collation' ) ) {
			if ( ! empty($wpdb->charset ) ) {
				$collate .= "DEFAULT CHARACTER SET $wpdb->charset";
			}
			if ( ! empty($wpdb->collate ) ) {
				$collate .= " COLLATE $wpdb->collate";
			}
		}

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		/**
		 * Update schemas before DBDELTA
		 *
		 * Before updating, remove any primary keys which could be modified due to schema updates
		 */
		
        // Create post type meta table for event
        $query_post_type_meta_table = "CREATE TABLE {$wpdb->prefix}". Apollo_Tables::_APOLLO_EVENT_META ." (
                    meta_id bigint(20) NOT NULL auto_increment,
                    apollo_event_id bigint(20) NOT NULL,
                    meta_key varchar(255) NULL,
                    meta_value longtext NULL,
                    PRIMARY KEY  (meta_id),
                    KEY apollo_event_id (apollo_event_id),
                    KEY meta_key (meta_key)
                  ) $collate;";
        
		$query_user_bookmark_table = " CREATE TABLE {$wpdb->prefix}". Apollo_Tables::_BOOKMARK ." (
                    bookmark_id  bigint(20) NOT NULL auto_increment,
                    user_id bigint(20) NOT NULL,
                    post_id bigint(20) NOT NULL,
                    post_type varchar(255) NOT NULL,
                    bookmark_date date NOT NULL,
                    PRIMARY KEY  (bookmark_id),
                    KEY user_id (user_id),
                    KEY post_id (post_id)
                  ) $collate;";
        
        $apollo_post_term = " CREATE TABLE {$wpdb->prefix}". Apollo_Tables::_POST_TERM ." (
                    post_term_id  bigint(20) NOT NULL auto_increment,
                    post_id bigint(20) NOT NULL,
                    term_id bigint(20) NOT NULL,
                    ".Apollo_DB_Schema::_TERM_LANGUAGE." varchar(64) DEFAULT NULL,
                    ".Apollo_DB_Schema::_IS_CATEGORY_SPOTLIGHT." enum('yes','no') DEFAULT NULL,
                    ".Apollo_DB_Schema::_IS_CATEGORY_FEATURE." enum('yes','no') DEFAULT NULL,
                    ".Apollo_DB_Schema::_IS_HOME_SPOTLIGHT." enum('yes','no') DEFAULT NULL,    
                    ".Apollo_DB_Schema::_TAB_LOCATION." varchar(255),
                    PRIMARY KEY  (post_term_id),
                    KEY post_id (post_id)
                  ) $collate;";

        $aplBlogUser = Apollo_App::getBlogUserTable();
        $apollo_user_blog = " CREATE TABLE ". $aplBlogUser ." (
                    user_id  bigint(20) NOT NULL,
                    blog_id bigint(20) NOT NULL,
                    artist_id bigint(20),
                    org_id bigint(20),
                    venue_id bigint(20),
                    agency_id bigint(20),
                    educator_id bigint(20),
                    PRIMARY KEY  (user_id, blog_id)
                  ) $collate;";
        
        $apollo_program_educator = " CREATE TABLE {$wpdb->prefix}". Apollo_Tables::_APL_PROGRAM_EDUCATOR ." (
                    prog_id  bigint(20) NOT NULL,
                    edu_id bigint(20) NOT NULL,
                    PRIMARY KEY  (prog_id, edu_id)
                  ) $collate;";
        
        $apollo_agency_educator = " CREATE TABLE {$wpdb->prefix}". Apollo_Tables::_APL_AGENCY_EDUCATOR ." (
                    agency_id  bigint(20) NOT NULL,
                    edu_id bigint(20) NOT NULL,
                    PRIMARY KEY  (agency_id, edu_id)
                  ) $collate;";
        
        $apollo_agency_org = " CREATE TABLE {$wpdb->prefix}". Apollo_Tables::_APL_AGENCY_ORG ." (
                    agency_id  bigint(20) NOT NULL,
                    org_id bigint(20) NOT NULL,
                    PRIMARY KEY  (agency_id, org_id)
                  ) $collate;";
        
        $apollo_agency_venue = " CREATE TABLE {$wpdb->prefix}". Apollo_Tables::_APL_AGENCY_VENUE ." (
                    agency_id  bigint(20) NOT NULL,
                    venue_id bigint(20) NOT NULL,
                    PRIMARY KEY  (agency_id, venue_id)
                  ) $collate;";

        /** @Ticket #14988 */
        $apollo_agency_artist = " CREATE TABLE {$wpdb->prefix}". Apollo_Tables::_APL_AGENCY_ARTIST ." (
                    agency_id  bigint(20) NOT NULL,
                    artist_id bigint(20) NOT NULL,
                    PRIMARY KEY  (agency_id, artist_id)
                  ) $collate;";

        $apollo_coordinate_cache = " CREATE TABLE {$wpdb->base_prefix}".Apollo_Tables::_APL_COOR_CACHE." (
                    key_address varchar (128) NOT NULL,
                    coordinate VARCHAR (50),
                    PRIMARY KEY  (key_address)
                  ) $collate;";
        
        $apollo_event_org = "
            CREATE TABLE {$wpdb->prefix}". Apollo_Tables::_APL_EVENT_ORG ." (
                id bigint(20) NOT NULL auto_increment,
                ". Apollo_DB_Schema::_APL_E_ORG_ID ." bigint(20) NOT NULL,
                ". Apollo_DB_Schema::_APL_E_ORG_ORDERING ." bigint(20) DEFAULT NULL,    
                ". Apollo_DB_Schema::_APL_E_ORG_IS_MAIN." enum('yes','no') DEFAULT NULL, 
                post_id bigint(20) NOT NULL,
                PRIMARY KEY ( id )    
        ) $collate; ";

        // #13856 - Neighborhood table
        $apollo_neighborhood = "
            CREATE TABLE {$wpdb->prefix}". Apollo_Tables::_APL_NEIGHBORHOOD ." (
                id bigint(20) NOT NULL auto_increment,
                name VARCHAR(255) NOT NULL,
                parent_id bigint(11) NOT NULL,
                type VARCHAR(255) DEFAULT 'city',
                PRIMARY KEY ( id )
        ) $collate; ";

        $apollo_activity = "
            CREATE TABLE {$wpdb->prefix}". Apollo_Tables::_APL_USER_ACTIVITY ." (
                  user_id BIGINT(20) NOT NULL,
                  item_id INT NOT NULL COMMENT 'Item id such as event_id',
                  item_type VARCHAR(20) COMMENT 'Item type such as post_type',
                  activity VARCHAR(255) NOT NULL COMMENT 'Activity of user such as: comment, like, select, click, share, twitter',
                  timestamp TIMESTAMP NOT NULL COMMENT 'Log time for user activity',
                  ip VARCHAR(16) COMMENT 'Remote address',
                  url VARCHAR(255) NULL COMMENT 'Url permalink of item',
                  title text not null comment 'Title of host object',
                  exdata longtext null comment 'Extra data such as content comment vv',
                  PRIMARY KEY (user_id, item_id, activity)
            ) $collate; ";

        $apollo_event_rating = "
            CREATE TABLE {$wpdb->prefix}". Apollo_Tables::_APL_EVENT_RATING ." (
                  item_id INT NOT NULL COMMENT 'Item id such as event_id',
                  item_type VARCHAR(20) COMMENT 'Item type such as post_type',
                  count int(11) not null default 0,
                  PRIMARY KEY (item_id, item_type)
            ) $collate; ";


        $apollo_event_calendar = "
                  CREATE TABLE {$wpdb->prefix}". Apollo_Tables::_APL_EVENT_CALENDAR ." (
                  event_id BIGINT(20) NOT NULL,
                  date_event date not null,
                  time_from varchar(8) not null,
                  time_to varchar(8) not null,
                  ticket_url varchar(255) DEFAULT NULL,
                  booth_override tinyint(1) DEFAULT NULL,
                  KEY e_c_id (event_id),
                  KEY e_c_date_event (date_event),
                  PRIMARY KEY (event_id, date_event, time_from, time_to)
                ) $collate; ";

        $apollo_visit = "
                  CREATE TABLE {$wpdb->prefix}". Apollo_Tables::_APL_USER_VISIT ." (
                  item_id int(11) NOT NULL,
                  item_type varchar(255) NOT NULL DEFAULT '',
                  count int(11) not null  default 0,
                  PRIMARY KEY (item_id)
                ) $collate; ";

        $apollo_topten = " CREATE TABLE {$wpdb->base_prefix}". Apollo_Tables::_APL_TOPTEN ." (
                    item_id int(11) NOT NULL,
                    item_type varchar(255) NOT NULL DEFAULT '',
                    count int(11) not null  default 0,
                    blog_id int(11) NOT NULL,
                    latest_visitation datetime,
                    PRIMARY KEY (item_id, blog_id)
                ) $collate;";

        $apollo_agency = "
                CREATE TABLE {$wpdb->prefix}". Apollo_Tables::_APL_AGENCY ." (
                    agencyID bigint(20) NOT NULL AUTO_INCREMENT,
                    name varchar(70) NOT NULL,
                    phone varchar(50) NOT NULL,
                    address varchar(100) NOT NULL,
                    email varchar(200) NOT NULL,
                    url varchar(200) NOT NULL,
                    PRIMARY KEY (agencyID)
                ) $collate; ";           


        $apollo_artist_laureate_award = "
                  CREATE TABLE {$wpdb->prefix}". Apollo_Tables::_APL_TIMELINE ." (
                  p_id bigint(11) NOT NULL,
                  year_award varchar(4) NOT NULL DEFAULT '1991',
                  award_category varchar(255) not null default '',
                  type varchar(20) not null default 'artist',
                  PRIMARY KEY (p_id, year_award, type)
                ) $collate; ";
                  
        $apollo_custom_field = "
            CREATE TABLE {$wpdb->prefix}".Apollo_Tables::_APL_CUSTOM_FIELD." (
                id bigint(11) NOT NULL auto_increment,
                name varchar(255) NOT NULL,
                label varchar(255) NULL,
                cf_order bigint(11) NOT NULL,
                cf_type varchar(255) NOT NULL,
                location varchar(255) NOT NULL,
                parent bigint(11) NULL,
                required tinyint(1) NULL default 0,
                post_type varchar(255) NOT NULL, 
                meta_data text NULL,
                PRIMARY KEY (id)
            ) $collate; ";

        $apollo_artist_public_art = "
            CREATE TABLE {$wpdb->prefix}". Apollo_Tables::_APL_ARTIST_PUBLIC_ART ." (
                id bigint(20) NOT NULL auto_increment,
                ". Apollo_DB_Schema::_APL_A_ART_ID ." bigint(20) NOT NULL,
                ". Apollo_DB_Schema::_APL_A_ORDERING ." bigint(20) DEFAULT NULL,
                post_id bigint(20) NOT NULL,
                PRIMARY KEY (id)
        ) $collate; ";
            
        $apollo_theme_tool = "
            CREATE TABLE {$wpdb->prefix}".Apollo_Tables::_APL_THEME_TOOL." (
                id bigint(11) NOT NULL auto_increment,
                tt_order varchar(255) NULL,
                post_type varchar(255) NOT NULL, 
                top_desc text NULL,
                bottom_desc text NULL,
                cat_id bigint(11) NOT NULL,
                entity_type varchar(255) NULL,
                events text NULL,
                location text NULL,
                location_type tinyint(1) NULL,
                location_data text NULL,
                leave_as_normal tinyint(1) default 0,
                associated_orgs text NULL,
                associated_venues text NULL,
                associated_orgs_desc text NULL,
                associated_venues_desc text NULL,
                PRIMARY KEY (id)
            ) $collate; ";


        $statesips = "
            CREATE TABLE {$wpdb->prefix}".Apollo_Tables::_APL_STATE_ZIP." (
                id bigint(11) NOT NULL auto_increment,
                code varchar(255) NOT NULL,
                name varchar(255) NULL,
                type enum('state','city','zip') NOT NULL,
                parent bigint(11) DEFAULT NULL,
                source varchar(255) DEFAULT NULL,
                PRIMARY KEY (id)
            ) $collate; ";
//Fields table: (id, file_name, user_id, import_date)
            
        $eventImport = "
            CREATE TABLE {$wpdb->prefix}".Apollo_Tables::_APL_EVENT_IMPORT." (
                id bigint(11) NOT NULL auto_increment,
                file_name varchar(255) NOT NULL,
                user_id bigint(11) NOT NULL,
                import_date date NULL,
                imported tinyint(1) DEFAULT NULL,
                PRIMARY KEY (id)
            ) $collate; ";     

        // Thienld : syndication create table 'cache' serve for improving retrieve syndication filter data
        $eventImport = "
            CREATE TABLE {$wpdb->prefix}".Apollo_Tables::_APL_EVENT_IMPORT." (
                id bigint(11) NOT NULL auto_increment,
                file_name varchar(255) NOT NULL,
                user_id bigint(11) NOT NULL,
                import_date date NULL,
                imported tinyint(1) DEFAULT NULL,
                PRIMARY KEY (id)
            ) $collate; ";


        /**
         * @Ticket 14015
        */
        $eventViewForExporting = "
            CREATE TABLE {$wpdb->prefix}".Apollo_Tables::_APL_EVENT_VIEW_FOR_EXPORTING." (
                event_id bigint(11) NOT NULL,
                event_title varchar(255) NOT NULL,
                org_name varchar(255) DEFAULT NULL,
                start_date date DEFAULT NULL,
                end_date date DEFAULT NULL,
                date_posted datetime DEFAULT NULL,
                venue_name varchar(255) DEFAULT NULL,
                post_author varchar(255) DEFAULT NULL,
                contact_name varchar(255) DEFAULT NULL,
                contact_email varchar(255) DEFAULT NULL,
                PRIMARY KEY (event_id)
            ) $collate; ";

        /**
         * @Ticket 14456
         */
        $artistsEvent = "
            CREATE TABLE {$wpdb->prefix}".Apollo_Tables::_APL_ARTIST_EVENT." (
                id bigint (11) NOT NULL auto_increment,
                artist_id bigint (11) NOT NULL,
                event_id bigint (11) NOT NULL,
                artist_ordering bigint (11),
                PRIMARY KEY (id)
            ) $collate; ";

        /**
         * @Ticket #14913
         */
        $userModules = "
            CREATE TABLE {$wpdb->prefix}".Apollo_Tables::_APL_USER_MODULES." (
                id bigint (11) NOT NULL auto_increment,
                user_id bigint (11) NOT NULL,
                post_id bigint (11) NOT NULL,
                post_type varchar (50),
                PRIMARY KEY (id)
            ) $collate; ";

        // Apollo Tables term meta
		$apollo_tables = "
            CREATE TABLE {$wpdb->prefix}". Apollo_Tables::_APOLLO_TERM_META ." (
              meta_id bigint(20) NOT NULL auto_increment,
              apollo_term_id bigint(20) NOT NULL,
              meta_key varchar(255) NULL,
              meta_value longtext NULL,
              PRIMARY KEY  (meta_id),
              KEY apollo_term_id (apollo_term_id),
              KEY meta_key (meta_key)
            ) $collate; "
            . " $query_post_type_meta_table "
            . " $query_user_bookmark_table "
            . " $apollo_post_term "
            . " $apollo_user_blog "
            . " $apollo_activity "
            . " $apollo_visit "
            . " $apollo_neighborhood "
            . " $apollo_coordinate_cache "
            . " $apollo_event_calendar "
            . " $apollo_event_rating "
            . " $apollo_artist_laureate_award "
            . " $apollo_event_org "   
            . " $apollo_agency "        
            . " $apollo_program_educator "
            . " $apollo_agency_educator "
            . " $apollo_agency_org "
            . " $apollo_custom_field "
            . " $apollo_artist_public_art "
            . " $apollo_agency_venue "
            . " $apollo_agency_artist "
            . " $statesips "
            . " $apollo_theme_tool "
            . " $eventImport "
            . " $apollo_topten"
            . "$eventViewForExporting"
            . "$artistsEvent"
            . "$userModules
        ";
		dbDelta( $apollo_tables );
    }

    public function create_default_pages() {
        Apollo_Page_Creator::createDefaultPage();
    }

    /**
     * From 'sidebar' to 'apl-sidebar' only
     * Don't support merge apl-sidebar. Must reactive theme immediately when change the name.
     */
    public function backward_widget_sidebar_name() {
        $old_sidebar_prefix = 'sidebar';
        $new_sidebar_prefix = Apollo_DB_Schema::_SIDEBAR_PREFIX;

        if($old_sidebar_prefix === $new_sidebar_prefix) return true; // ok

        $current_sidebar_widgets = get_option('sidebars_widgets', array());

        if(empty($current_sidebar_widgets)) {
            return true; /* Done! */
        }

        $arrNeedCopy = array();
        $listNameNeedRemove = array();
        foreach($current_sidebar_widgets as $sidebar_name => $arrValue) {
            if(!is_array($arrValue)) continue;
            if(strpos($sidebar_name, $old_sidebar_prefix ) !== 0) continue;

            // Convert name
            $_sidebar_name = substr_replace($sidebar_name, $new_sidebar_prefix, 0, strlen($old_sidebar_prefix));
            $arrNeedCopy[$_sidebar_name] = $arrValue;
            $listNameNeedRemove[] = $sidebar_name;
        }

        if(empty($arrNeedCopy)) return true;

        //BACKUP BEFORE DO ANYTHING
        update_option('_bk_sidebars_widgets', $current_sidebar_widgets);

        // REMOVE
        foreach($listNameNeedRemove as $sidebar_name) {
            unset($current_sidebar_widgets[$sidebar_name]);
        }

        // MERGE
        $current_sidebar_widgets = array_merge($current_sidebar_widgets, $arrNeedCopy);

        return update_option('sidebars_widgets', $current_sidebar_widgets);
    }


    /* Some function must do*/
    public function cleanup($newname = '', $newtheme = '', $oldTheme = '') {

        // Clean up page has created
        if($this->checkThemeDoNotResetData($oldTheme->name, $newtheme->name)) {
            $this->octaveBackupSidebarsWidgets();
            return true;
        }
        $pid_group = Apollo_Page_Creator::ID_GROUP_PAGE;
        $options = get_option(Apollo_App::getIncodeOptionName());
        if(FALSE === $options) return false;

        foreach ($options[$pid_group] as $k => $arrpid) {
            wp_delete_post($arrpid['id'], true);
            unset($options[$pid_group][$k]);
        }

        update_option(Apollo_App::getIncodeOptionName(), $options);

        // widget

    }

    /**
     * Update theme version if the theme has more features
     *
     * @ticket #11493
     */
    public function updateThemeVersion()
    {
        if ( Apollo_App::checkReactivation() ) {
            update_option(Apollo_DB_Schema::_APL_RE_ACTIVATION_THEME_VERSION, APOLLO_CURRENT_REACTIVE_THEME_VERSION, 'no');
        }
    }

}

$default_template_class = new Apollo_Install_Theme();
$default_template_class->init();
