<input type="hidden" name="apollo_<?php echo $this->type; ?>_quick_edit" value="1" />

<input type="hidden" name="apollo_<?php echo $this->type; ?>_quick_edit_nonce" value="<?php echo wp_create_nonce( 'apollo_'.$this->type.'_quick_edit_nonce' ); ?>" />

<div class="clear"></div>


<fieldset class="inline-edit-col-left">
    <div id="apollo-fields" class="inline-edit-col">

        <h4><?php _e( 'Public Art data', 'apollo' ); ?></h4>

        <label>
            <span class="title"><?php _e( 'Date created', 'apollo' ); ?></span>
            <span class="input-text-wrap">
                <input type="text" name="<?php echo Apollo_DB_Schema::_APL_PUBLIC_ART_DATA; ?>[<?php echo Apollo_DB_Schema::_PUBLIC_ART_CREATED_DATE; ?>]" class="text" value="">
            </span>
        </label>
        <br class="clear" />

        <label>
            <span class="title"><?php _e( 'Dimension', 'apollo' ); ?></span>
            <span class="input-text-wrap">
                <input type="text" name="<?php echo Apollo_DB_Schema::_APL_PUBLIC_ART_DATA; ?>[<?php echo Apollo_DB_Schema::_PUBLIC_ART_DIMENSION; ?>]" class="text" value="">
            </span>
        </label>
        <br class="clear" />

        <label>
            <span class="title"><?php _e( 'Website url', 'apollo' ); ?></span>
            <span class="input-text-wrap">
                <input class="apollo_input_url" type="text" name="<?php echo Apollo_DB_Schema::_APL_PUBLIC_ART_DATA; ?>[<?php echo Apollo_DB_Schema::_PUBLIC_ART_WEBSITE_URL; ?>]" class="text" value="">
            </span>
        </label>
        <br class="clear" />

    </div>
    <br class="clear" />
</fieldset>

<fieldset class="inline-edit-col-left">
    <div id="apollo-fields" class="inline-edit-col">

        <h4><?php _e( 'Contact info', 'apollo' ); ?></h4>

        <label>
            <span class="title"><?php _e( 'Contact Name', 'apollo' ); ?></span>
            <span class="input-text-wrap">
                <input type="text" name="<?php echo Apollo_DB_Schema::_APL_PUBLIC_ART_DATA; ?>[<?php echo Apollo_DB_Schema::_PUBLIC_ART_CONTACT_NAME; ?>]" class="text" value="">
            </span>
        </label>
        <br class="clear" />

        <label>
            <span class="title"><?php _e( 'Contact Phone', 'apollo' ); ?></span>
            <span class="input-text-wrap">
                <input type="text" name="<?php echo Apollo_DB_Schema::_APL_PUBLIC_ART_DATA; ?>[<?php echo Apollo_DB_Schema::_PUBLIC_ART_CONTACT_PHONE; ?>]" class="text" value="">
            </span>
        </label>
        <br class="clear" />

        <label>
            <span class="title"><?php _e( 'Contact Email', 'apollo' ); ?></span>
            <span class="input-text-wrap">
                <input class="apollo_input_email" type="text" name="<?php echo Apollo_DB_Schema::_APL_PUBLIC_ART_DATA; ?>[<?php echo Apollo_DB_Schema::_PUBLIC_ART_CONTACT_EMAIL; ?>]" class="text" value="">
            </span>
        </label>
        <br class="clear" />

    </div>
</fieldset>
