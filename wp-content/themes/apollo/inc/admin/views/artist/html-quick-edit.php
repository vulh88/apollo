<input type="hidden" name="apollo_<?php echo $this->type; ?>_quick_edit" value="1" />
<input type="hidden" name="apollo_<?php echo $this->type; ?>_quick_edit" value="1" />

<input type="hidden" name="apollo_<?php echo $this->type; ?>_quick_edit_nonce" value="<?php echo wp_create_nonce( 'apollo_'.$this->type.'_quick_edit_nonce' ); ?>" />

<div class="clear"></div>

<fieldset class="inline-edit-col-left">
	<div id="apollo-fields" class="inline-edit-col">
        <h4><?php _e( 'Artist data', 'apollo' ); ?></h4>
        <?php 
            $_a_data = array(
                Apollo_DB_Schema::_APL_ARTIST_FNAME => array( 'label' => __( 'First name','apollo' ) ),
                Apollo_DB_Schema::_APL_ARTIST_LNAME => array( 'label' => __( 'Last name' ),'apollo' ),
                Apollo_DB_Schema::_APL_ARTIST_PHONE => array( 'label' => __( 'Phone' ),'apollo' ),
                Apollo_DB_Schema::_APL_ARTIST_EMAIL => array( 'label' => __( 'Email','apollo' ), 'class' => 'apollo_input_email' ),
                Apollo_DB_Schema::_APL_ARTIST_BLOGURL => array( 'label' => __( 'Blog URL','apollo' ), 'class' => 'apollo_input_url' ),
                Apollo_DB_Schema::_APL_ARTIST_WEBURL => array( 'label' => __( 'Website','apollo' ), 'class' => 'apollo_input_url' ),
            );
            
            foreach ( $_a_data as $k => $l ):
        ?>
        <label>
            <span class="title"><?php echo $l['label'] ?></span>
            <span class="input-text-wrap">
                <input type="text" name="<?php echo Apollo_DB_Schema::_APL_ARTIST_DATA; ?>[<?php echo $k; ?>]" 
                       class="text <?php echo isset( $l['class'] ) ? $l['class'] : '' ?>" value="">
            </span>
        </label>    
        <br class="clear" />
        <?php endforeach; ?>
    </div>
</fieldset>

<fieldset class="inline-edit-col-left">
	<div id="apollo-fields" class="inline-edit-col">
        <h4><?php _e( 'Social Network URL', 'apollo' ); ?></h4>
        <?php 
            $_a_sn = array(
                Apollo_DB_Schema::_APL_ARTIST_FB => array( 'label' => __( 'Facebook', 'apollo' ), 'class' => 'apollo_input_url'  ),
                Apollo_DB_Schema::_APL_ARTIST_LK => array( 'label' => __( 'LinkedIn', 'apollo' ), 'class' => 'apollo_input_url'  ),
                Apollo_DB_Schema::_APL_ARTIST_TW => array( 'label' => __( 'Twitter', 'apollo' ), 'class' => 'apollo_input_url'  ),
                Apollo_DB_Schema::_APL_ARTIST_INS => array( 'label' => __( 'Instagram', 'apollo' ), 'class' => 'apollo_input_url'  ),
                Apollo_DB_Schema::_APL_ARTIST_PR => array( 'label' => __( 'Pinterest', 'apollo' ), 'class' => 'apollo_input_url'  ),
            );
            
            foreach ( $_a_sn as $k => $l ):
        ?>
        <label>
            <span class="title"><?php echo $l['label'] ?></span>
            <span class="input-text-wrap">
                <input type="text" name="<?php echo Apollo_DB_Schema::_APL_ARTIST_DATA; ?>[<?php echo $k; ?>]" 
                       class="text <?php echo isset( $l['class'] ) ? $l['class'] : '' ?>" value="">
            </span>
        </label>    
        <br class="clear" />
        <?php endforeach; ?>
    </div>
</fieldset>
