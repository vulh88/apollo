<input type="hidden" name="apollo_<?php echo $this->type; ?>_quick_edit" value="1" />

<input type="hidden" name="apollo_<?php echo $this->type; ?>_quick_edit_nonce" value="<?php echo wp_create_nonce( 'apollo_'.$this->type.'_quick_edit_nonce' ); ?>" />

<div class="clear"></div>

<fieldset class="inline-edit-col-left">
	<div id="apollo-fields" class="inline-edit-col">
        <h4><?php _e( 'Educator data', 'apollo' ); ?></h4>
        <?php 
            $_a_data = array(
                Apollo_DB_Schema::_APL_EDUCATOR_ADD1 => array( 'label' => __( 'Address 1','apollo' ) ),
                Apollo_DB_Schema::_APL_EDUCATOR_ADD2 => array( 'label' => __( 'Address 2' ),'apollo' ),
                Apollo_DB_Schema::_APL_EDUCATOR_STATE => array( 'label' => __( 'State','apollo' ) ),
                Apollo_DB_Schema::_APL_EDUCATOR_CITY => array( 'label' => __( 'City' ),'apollo' ),
                Apollo_DB_Schema::_APL_EDUCATOR_ZIP => array( 'label' => __( 'Zip','apollo' ) ),
                Apollo_DB_Schema::_APL_EDUCATOR_REGION => array( 'label' => __( 'State','apollo' ) ),
                Apollo_DB_Schema::_APL_EDUCATOR_COUNTY => array( 'label' => __( 'County','apollo' ) ),
                Apollo_DB_Schema::_APL_EDUCATOR_PHONE1 => array( 'label' => __( 'Phone','apollo' ) ),
                Apollo_DB_Schema::_APL_EDUCATOR_FAX => array( 'label' => __( 'Fax','apollo' ) ),
                Apollo_DB_Schema::_APL_EDUCATOR_EMAIL => array( 'label' => __( 'Email','apollo' ), 'class' => 'apollo_input_email' ),
                Apollo_DB_Schema::_APL_EDUCATOR_URL => array( 'label' => __( 'Website','apollo' ), 'class' => 'apollo_input_url' ),
            );
            
            foreach ( $_a_data as $k => $l ):
                if ( $k == Apollo_DB_Schema::_APL_EDUCATOR_STATE || $k == Apollo_DB_Schema::_APL_EDUCATOR_CITY || $k == Apollo_DB_Schema::_APL_EDUCATOR_ZIP  || $k == Apollo_DB_Schema::_APL_EDUCATOR_REGION):
                    if ($k == Apollo_DB_Schema::_APL_EDUCATOR_STATE) {
                        $_arr_terr = Apollo_App::render_us_states_option();
                        $class = "apl-territory-state";
                    }
                    else if($k == Apollo_DB_Schema::_APL_EDUCATOR_CITY) {
                        $_arr_terr = Apollo_App::render_us_city_option();
                        $class = "apl-territory-city";
                    }
                    else if($k == Apollo_DB_Schema::_APL_EDUCATOR_REGION) {
                        $_arr_terr = Apollo_App::render_regions_option();
                        $class = "";
                    }
                    else {
                        $_arr_terr = Apollo_App::render_us_zip_option();
                        $class = "apl-territory-zipcode";
                    }
                    if ( $_arr_terr ):?>
                        <label>
                            <span class="title"><?php echo $l['label'] ?></span>
                            <span class="input-text-wrap">
                                <select class="<?php echo $class ?>" data-wrapper="fieldset" name="<?php echo Apollo_DB_Schema::_APL_EDUCATOR_DATA; ?>[<?php echo $k; ?>]">
                                    <?php echo $_arr_terr ?>
                                </select>
                            </span>
                        </label>
                        <br class="clear" />
                    <?php endif; // End $us_states ?>
                <?php else : ?>
                    <label>
                        <span class="title"><?php echo $l['label'] ?></span>
                        <span class="input-text-wrap">
                            <input type="text" name="<?php echo Apollo_DB_Schema::_APL_EDUCATOR_DATA; ?>[<?php echo $k; ?>]"
                                   class="text <?php echo isset( $l['class'] ) ? $l['class'] : '' ?>" value="">
                        </span>
                    </label>
                    <br class="clear" />

                <?php endif; ?>

        <?php endforeach; ?>
    </div>
</fieldset>

