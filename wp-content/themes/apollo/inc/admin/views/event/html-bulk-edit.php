<fieldset class="inline-edit-col-right">
	<div id="apollo-event-fields-bulk" class="inline-edit-col">

		<h4><?php _e( 'Event Data', 'apollo' ); ?></h4>

		<?php do_action( 'apollo_event_bulk_edit_start' ); ?>

		<div class="inline-edit-group">
            <label class="alignleft">
                <span class="title"><?php _e( 'Admission Phone', 'apollo' ); ?></span>
                <span class="input-text-wrap">
                    <select class="change-to" name="change_admission_phone">
                    <?php
                        $options = array(
                            '' 	=> __( '— No Change —', 'apollo' ),
                            '1' => __( 'Change to:', 'apollo' )
                        );
                        foreach ($options as $key => $value) {
                            echo '<option value="' . esc_attr( $key ) . '">'. $value .'</option>';
                        }
                    ?>
                    </select>
                </span>
            </label>
            
            <label class="alignright">
                <input type="text" name="<?php echo Apollo_DB_Schema::_ADMISSION_PHONE; ?>" class="text" value="">
            </label>
        </div>
        
        <div class="inline-edit-group">
            <label class="alignleft">
                <span class="title"><?php _e( 'Admission Ticket URL', 'apollo' ); ?></span>
                <span class="input-text-wrap">
                    <select class="change-to" name="change_admission_ticket_url">
                    <?php
                        $options = array(
                            '' 	=> __( '— No Change —', 'apollo' ),
                            '1' => __( 'Change to:', 'apollo' )
                        );
                        foreach ($options as $key => $value) {
                            echo '<option value="' . esc_attr( $key ) . '">'. $value .'</option>';
                        }
                    ?>
                    </select>
                </span>
            </label>
            
            <label class="alignright">
                <input type="text" name="<?php echo Apollo_DB_Schema::_ADMISSION_TICKET_URL; ?>" class="text" value="">
            </label>
        </div>
        
    </div>
    
    <input type="hidden" name="apollo_event_bulk_edit" value="1" />
    <input type="hidden" name="apollo_bulk_edit_nonce" value="<?php echo wp_create_nonce( 'apollo_event_bulk_edit_nonce' ); ?>" />
        
</fieldset>    