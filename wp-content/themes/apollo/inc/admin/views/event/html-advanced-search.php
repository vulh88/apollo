<div class="wrap clear"><style>input[type='text'] { width:200px; padding:4px;  } </style>
          
    <fieldset>
    <legend><h2>Event Advanced Search</h2></legend>
    <hr/>
    
    <form method="get" action="#" >

        <div class="tablenav top">
            <div id="apollo-admin-event-adf" class="alignleft actions">
                <input name="post_type" value="event" type="hidden" />
                <input name="page" value="event_manage_type" type="hidden" />
                
                
                <input id="ads-apollo-event-start-date" value="<?php echo isset( $_REQUEST['start_date'] ) ? $_REQUEST['start_date'] : '' ?>" name="start_date" type="text" placeholder="<?php _e( 'Start Date', 'apollo' ) ?>" />
                <input id="ads-apollo-event-end-date" value="<?php echo isset( $_REQUEST['end_date'] ) ? $_REQUEST['start_date'] : '' ?>" name="end_date" type="text" placeholder="<?php _e( 'End Date', 'apollo' ) ?>" />
                
                <?php 

                if ( isset( $data['orgs'] ) && $data['orgs'] ): ?>
                <select name="apollo_event_org" id="ads-apollo-event-org">
                    <option selected="selected" value="0"><?php _e( 'Select an organization' ) ?></option>
                    <?php foreach ( $data['orgs'] as $org ): ?>
                    <option <?php echo isset( $_REQUEST['apollo_event_org'] ) && $_REQUEST['apollo_event_org'] == $org->ID ? 'selected' : ''  ?> 
                        value="<?php echo $org->ID ?>"><?php echo $org->post_title ?></option>
                    <?php endforeach; ?>
                </select>
                <?php endif; ?>

                <input class="button" name="search" value="<?php _e( 'Seach', 'apollo' ) ?>" type="submit" />
                <br class="clear">
            </div>
        </div>
    </form>
    <hr/>
    
    <table id="apollo-admin-ads-event-table" class="wp-list-table widefat fixed posts">
        <thead>
            <tr>
                <th><?php _e( 'Image' ) ?></th>
                <th><?php _e( 'Name' ) ?></th>
                <th><?php _e( 'Spotlight', 'apollo' ) ?></th>
                <th><?php _e( 'Featured', 'apollo' ) ?></th>
                <th><?php _e( 'Date' ) ?></th>
            </tr>
        </thead>
        <tbody>
            <?php if ( $this->_data ): ?>
            <?php foreach( $this->_data as $event ): 
                $event = get_event( $event );
            ?>
                <tr>
                    <td class="thumb column-thumb"><?php echo $event->get_image() ?></td>
                    <td><?php echo $event->get_title() ?></td>
                    <td>saaa</td>
                    <td>gggg</td>
                    <td>gggg</td>
                </tr>
            <?php endforeach; ?>
            <?php endif; ?>
    
        <tfoot>
            <tr>
                <th><?php _e( 'Image' ) ?></th>
                <th><?php _e( 'Name' ) ?></th>
                <th><?php _e( 'Spotlight', 'apollo' ) ?></th>
                <th><?php _e( 'Featured', 'apollo' ) ?></th>
                <th><?php _e( 'Date' ) ?></th>
            </tr>
        </tfoot>
    </table>
  
    </fieldset>
</div>