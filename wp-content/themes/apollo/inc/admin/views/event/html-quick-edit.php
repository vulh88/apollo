<?php 
    function apl_quickedit_input( $field ) { ?>
        <label>
            <span class="title"><?php echo $field['label'] ?> <?php echo isset( $field['require'] ) && $field['require'] ? '<i class="required">*</i>' : '' ?></span>
            <span class="input-text-wrap">
                <input type="<?php echo ! isset( $field['type'] ) || ! $field['type'] ? 'text' : $field['type'] ?>" name="<?php echo $field['name'] ?>" 
                       class="<?php echo ! isset( $field['type'] ) || ! $field['type'] ? 'text' : '' ?> <?php echo isset( $field['class'] ) ? $field['class'] : '' ?>" 
                       value="<?php echo isset( $field['value'] ) ? $field['value'] : '' ?>">
            </span>
        </label>
        <br class="clear" />
    <?php
    
    }
    
    function apl_quickedit_textarea( $field ) { ?>
        <label>
            <span class="title"><?php echo $field['label'] ?> <?php echo isset( $field['require'] ) && $field['require'] ? '<i class="required">*</i>' : '' ?></span>
            <span class="input-text-wrap">
                <textarea class="<?php echo isset( $field['class'] ) ? $field['class'] : '' ?>" name="<?php echo $field['name'] ?>"></textarea>
            </span>
        </label>
        <br class="clear" />
    <?php
    
    }
?>

<div class="clear"></div>
<fieldset class="inline-edit-col-left">
	<div id="apollo-fields" class="inline-edit-col">

		<h4><?php _e( 'Event Data', 'apollo' ); ?></h4>
        
		<?php do_action( 'apollo_event_quick_edit_start' ); ?>
        
        <?php 
        
        Apollo_Meta_Box_Event_Module::output_quick_edit_select_option( 'organization', __( 'Organization', 'apollo' ) );
        Apollo_Meta_Box_Event_Module::output_quick_edit_select_option( 'venue', __( 'Venue', 'apollo' ) ); 
        Apollo_Meta_Box_Event_Module::output_quick_edit_select_option( 'artist', __( 'Artist', 'apollo' ) );
        Apollo_Meta_Box_Event_Module::output_quick_edit_select_option( 'classified', __( 'Classified', 'apollo' ) );

        Apollo_Meta_Box_Event_Module::output_quick_edit_select_option( 'public_art', __( 'Public Art', 'apollo' ) );
        Apollo_Meta_Box_Event_Module::output_quick_edit_select_option( 'education', __( 'education', 'apollo' ) );
        Apollo_Meta_Box_Event_Module::output_quick_edit_select_option( 'business', __( 'Business', 'apollo' ) );
    
     
        $e_d_key = Apollo_DB_Schema::_APOLLO_EVENT_DATA; 
        apl_quickedit_input( array(
            'label'     => __( 'Start date', 'apollo' ),
            'name'      => Apollo_DB_Schema::_APOLLO_EVENT_START_DATE,
            'require'   => true,
            'class'     => 'apollo-datepicker'
        ) );
        
        apl_quickedit_input( array(
            'label'     => __( 'End date', 'apollo' ),
            'name'      => Apollo_DB_Schema::_APOLLO_EVENT_END_DATE,
            'require'   => true,
            'class'     => 'apollo-datepicker'
        ) );
        
        apl_quickedit_input( array(
            'label'     => __( 'Admission Phone', 'apollo' ),
            'name'      => $e_d_key.'['.Apollo_DB_Schema::_ADMISSION_PHONE. ']',
            'require'   => true,
        ) );
        
        apl_quickedit_input( array(
            'label'     => __( 'Admission Ticket URL', 'apollo' ),
            'name'      => $e_d_key.'['.Apollo_DB_Schema::_ADMISSION_TICKET_URL. ']',
            'class'     => 'apollo_input_url',
        ) );
        
        apl_quickedit_input( array(
            'label'     => __( 'Admission Ticket Email', 'apollo' ),
            'name'      => $e_d_key.'['.Apollo_DB_Schema::_ADMISSION_TICKET_EMAIL. ']',
            'class'     => 'apollo_input_email',
        ) );
        
        apl_quickedit_input( array(
            'label'     => __( 'Admission Discount URL', 'apollo' ),
            'name'      => $e_d_key.'['.Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL. ']',
            'class'     => 'apollo_input_url',
        ) );

        /*@ticket 17346 */
        if(of_get_option(Apollo_DB_Schema::_EVENT_FORM_ENABLE_DISCOUNT_TEXT, 0)) {
            apl_quickedit_input( array(
                'label'     => __( 'Admission Discount Text', 'apollo' ),
                'name'      => $e_d_key.'['.Apollo_DB_Schema::_ADMISSION_DISCOUNT_TEXT. ']'
            ) );
        }

        apl_quickedit_input( array(
            'label'     => __( 'Free Admission', 'apollo' ),
            'name'      => Apollo_DB_Schema::_APOLLO_EVENT_FREE_ADMISSION,
            'type'      => 'checkbox',
            'value'     => 'on',
        ) );

        ?>
   
    </div>
    <input type="hidden" name="apollo_event_quick_edit" value="1" />
    <?php 
        if ( $this->_is_home_display_management_page()) {
            echo '<input type="hidden" name="dm" value="true" />';
        } 
    ?>
    <input type="hidden" name="apollo_event_quick_edit_nonce" value="<?php echo wp_create_nonce( 'apollo_event_quick_edit_nonce' ); ?>" />

</fieldset>

<fieldset class="inline-edit-col-left">
	<div id="apollo-fields" class="inline-edit-col">

		<h4><?php _e( 'Contact info', 'apollo' ); ?></h4>
        
        <?php 
            apl_quickedit_input( array(
                'label'     => __( 'Contact Name', 'apollo' ),
                'name'      => $e_d_key.'['.Apollo_DB_Schema::_E_CONTACT_NAME. ']',
                'require'   => true,
            ) );
            
            apl_quickedit_input( array(
                'label'     => __( 'Contact Email', 'apollo' ),
                'name'      => $e_d_key.'['.Apollo_DB_Schema::_E_CONTACT_EMAIL. ']',
                'require'   => true,
                'class'     => 'apollo_input_email',
            ) );
            
            apl_quickedit_input( array(
                'label'     => __( 'Contact Phone', 'apollo' ),
                'name'      => $e_d_key.'['.Apollo_DB_Schema::_E_CONTACT_PHONE. ']',
            ) );
        ?>
 
    </div>
</fieldset>

<fieldset class="inline-edit-col-left">
	<div id="apollo-fields" class="inline-edit-col">

		<h4><?php _e( 'Event URL', 'apollo' ); ?></h4>
        
        <?php 
            apl_quickedit_input( array(
                'label'     => __( 'Official Website', 'apollo' ),
                'name'      => $e_d_key.'['.Apollo_DB_Schema::_WEBSITE_URL. ']',
                'require'   => true,
                'class'     => 'apollo_input_url',
            ) );
        ?>
    </div>
</fieldset>  
