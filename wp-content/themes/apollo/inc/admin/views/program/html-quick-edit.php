<input type="hidden" name="apollo_<?php echo $this->type; ?>_quick_edit" value="1" />

<input type="hidden" name="apollo_<?php echo $this->type; ?>_quick_edit_nonce" value="<?php echo wp_create_nonce( 'apollo_'.$this->type.'_quick_edit_nonce' ); ?>" />

<div class="clear"></div>

<fieldset class="inline-edit-col-left">
	<div id="apollo-fields" class="inline-edit-col">
        <h4><?php _e( 'Contact Info', 'apollo' ); ?></h4>
        <?php 
            $_a_data = array(
             
                Apollo_DB_Schema::_APL_PROGRAM_CNAME => array( 'label' => __( 'Contact Name','apollo' ) ),
                Apollo_DB_Schema::_APL_PROGRAM_PHONE => array( 'label' => __( 'Phone','apollo' ) ),
                Apollo_DB_Schema::_APL_PROGRAM_EMAIL => array( 'label' => __( 'Email','apollo' ), 'class' => 'apollo_input_email' ),
                Apollo_DB_Schema::_APL_PROGRAM_URL => array( 'label' => __( 'Website','apollo' ), 'class' => 'apollo_input_url' ),
            );
            
            foreach ( $_a_data as $k => $l ):
        ?>
        <label>
            <span class="title"><?php echo $l['label'] ?></span>
            <span class="input-text-wrap">
                <input type="text" name="<?php echo Apollo_DB_Schema::_APL_PROGRAM_DATA; ?>[<?php echo $k; ?>]" 
                       class="text <?php echo isset( $l['class'] ) ? $l['class'] : '' ?>" value="">
            </span>
        </label>    
        <br class="clear" />
        <?php endforeach; ?>
    </div>
</fieldset>

<fieldset class="inline-edit-col-left">
	<div id="apollo-fields" class="inline-edit-col">
        <h4><?php _e( 'Date Range', 'apollo' ); ?></h4>
        <?php 
            $_a_data = array(
                Apollo_DB_Schema::_APL_PROGRAM_STARTD => array( 'label' => __( 'Start Date','apollo' ) ),
                Apollo_DB_Schema::_APL_PROGRAM_ENDD => array( 'label' => __( 'End Date' ),'apollo' ),
            );
            
            foreach ( $_a_data as $k => $l ):
        ?>
        <label>
            <span class="title"><?php echo $l['label'] ?></span>
            <span class="input-text-wrap">
                <input type="text" name="<?php echo $k; ?>" 
                       class="text <?php echo isset( $l['class'] ) ? $l['class'] : '' ?> apollo-datepicker" value="">
            </span>
        </label>    
        <br class="clear" />
        <?php endforeach; ?>
    </div>
</fieldset>





