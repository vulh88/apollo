<?php

/**
 * Author: Thienld.
 * Date: 13/05/2016
 * Time: 17:18
 * Description: Custom workflow of Apollo RSS Feeds
 */
if(!class_exists('Apollo_RSS_Feed')){
    class Apollo_RSS_Feed
    {

        const NUMBER_OF_RECENT_POST_ITEMS = 10;

        public function __construct(){
            remove_all_actions( 'do_feed_rss2' );
            add_action( 'do_feed_rss2', array($this,'apollo_custom_rss2_feed'), 10, 1 );
            add_filter('feed_content_type', array($this, 'custom_rss2_header_content_type'), 99,2);

            // Remove feed on the single page
            remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
        }

        public function custom_rss2_header_content_type($content_type, $type){
            if(in_array($type, array('rss','rss2'))){
                return 'application/xml';
            }
            return $content_type;
        }

        public function apollo_custom_rss2_feed( $for_comments ) {
            try{
                $currentPostType = get_query_var( 'post_type' );
                $arrayAppliedCustomRSS2Feed = array(
                    Apollo_DB_Schema::_ARTIST_PT,
                    Apollo_DB_Schema::_ORGANIZATION_PT,
                    Apollo_DB_Schema::_VENUE_PT,
                    Apollo_DB_Schema::_CLASSIFIED,
                    Apollo_DB_Schema::_BUSINESS_PT,
                    Apollo_DB_Schema::_EDUCATOR_PT,
                    Apollo_DB_Schema::_PUBLIC_ART_PT
                );
                if( in_array($currentPostType,$arrayAppliedCustomRSS2Feed) ){
                    $this->handleCustomTemplateRSSFeed($currentPostType);
                } else {
                    do_feed_rss2( $for_comments ); // Call default function
                }
            } catch (Exception $ex) {
                echo 'Feed is empty !';
            }
        }

        private function handleCustomTemplateRSSFeed($post_type = ''){
            $args = array();
            $isDisplayedRecentPublishedPost = false;
            if(isset($_GET['recent_publish'])){
                $isDisplayedRecentPublishedPost = true;
            }
            $args['isRecentPublished'] = $isDisplayedRecentPublishedPost;

            $posts_per_page = $isDisplayedRecentPublishedPost ? self::NUMBER_OF_RECENT_POST_ITEMS : -1 ;
            $orderBy  = $isDisplayedRecentPublishedPost ? 'post_date ' : 'title' ;
            $order = $isDisplayedRecentPublishedPost ? 'DESC ' : 'ASC';
            $posts =  $this->getListPostItem(array(
                'posts_per_page'    => $posts_per_page,
                'post_type'         => $post_type,
                'order_by'         => $orderBy,
                'order'            => $order,
            ));

            $args['havePosts'] = !empty($posts);

            Apollo_App::getTemplatePartCustom(APOLLO_INCLUDES_DIR . '/feeds/templates/apollo-rss2-feed-template.php',$args,false);

            wp_reset_postdata();
        }

        private function getListPostItem( $args = array()  ) {

            $post_type = isset($args['post_type']) ? $args['post_type'] : '';
            $need_check_available = isset($args['need_check_available']) ? $args['need_check_available'] : true;
            $post_status = isset($args['post_status']) ? $args['post_status'] : array('publish');
            $posts_per_page = isset($args['posts_per_page']) ? $args['posts_per_page'] : -1;
            $orderBy = isset($args['order_by']) ? $args['order_by'] : 'title';
            $order = isset($args['order']) ? $args['order'] : 'ASC';

            // handle special logic checking module available for relationship between EDUCATOR post type and EDUCATION module
            $postTypeForCheckModuleAvailable = $post_type == Apollo_DB_Schema::_EDUCATOR_PT ? Apollo_DB_Schema::_EDUCATION : $post_type;
            if ( $need_check_available && ! Apollo_App::is_avaiable_module( $postTypeForCheckModuleAvailable ) ) {
                return array();
            }
            $args = array(
                'post_type'         => $post_type,
                'posts_per_page'    => $posts_per_page,
                'post_status'        => $post_status,
                'orderby'           => $orderBy,
                'order'             => $order,

            );

            add_filter( 'posts_request', array($this, 'dump_request') );

            return query_posts( $args );
        }

        public function dump_request( $input )
        {
            if(isset($_GET['is_debugging'])){
                aplDebug($input,1);
            }
            return $input;
        }
    }
}

new Apollo_RSS_Feed();