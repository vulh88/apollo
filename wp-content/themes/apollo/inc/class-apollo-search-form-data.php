<?php
/**
 * Created by PhpStorm.
 * User: pc92-vu
 * Date: 11/04/2016
 * Time: 11:20
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if (! class_exists('Apollo_Seach_Form_Data')) {
    class Apollo_Seach_Form_Data {

        /**
         * Get zipcode by region
         *
         * @param boolean $region
         * @return object
         */
        static function getRegionZipcodes($region) {

            $data = json_decode(of_get_option(Apollo_DB_Schema::_APL_FILTERING_BY_REGION_LISTING),true);

            if (isset($data[$region])) return $data[$region];

            return false;
        }

    }
}