<div class="r-blk  <?php echo !empty($args['templateLocation']) ? $args['templateLocation'] : '' ?>">
    <h3 class="r-ttl"><i class="fa fa-envelope-o fa-lg"></i><span><?php _e('E-Newsletter', 'apollo') ?></span></h3>

    <div class="r-blk-ct">
        <form id="newsletter" method="#" action="#" class="newsletter-frm">
            <input type="text" placeholder="<?php _e('Your email', 'apollo') ?>" class="ipt newsletter">
            <button type="button" class="btn btn-l"><?php _e('SUBSCRIBE NOW', 'apollo') ?></button>
        </form>
    </div>
</div>