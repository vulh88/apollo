<?php
require_once __DIR__. '/../Apollo_Widget.php';

class apollo_subscribe_widget extends Apollo_Widget {
    
    public function __construct() {
        $this->adminTemplatePath = dirname(__FILE__). '/admin.php';
        parent::__construct(
            
            // ID of widget
            'apollo_subscribe_widget', 

            // Widget name 
            __('Apollo Subscribe Widget', 'apollo'), 

            // Widget description
            array( 'description' => __( 'Apollo Subscribe Widget', 'apollo' ), ) 
        );

        add_shortcode('apollo_widgets', array($this, 'setLocation'));
    }
        

	
    // Updating widget replacing old instances with new
    public function childUpdate( $new_instance, $old_instance ) {
        
        $instance = array();
        $instance['title'] = ! empty( $new_instance['title'] ) ? strip_tags( $new_instance['title'] ) : '';
        $parentInstance = parent::update($new_instance, $old_instance);
        return array_merge($parentInstance,$instance);
    }
    
    /**
     * Creating widget front-end
     * This is where the action happens
     */
    public function childWiget( $args, $instance ) {
        ob_start();
        include dirname(__FILE__). '/frontend.php';
        $html = ob_get_contents();
        ob_end_clean();
        echo $html;
    }
    
}  