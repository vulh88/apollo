
<?php
    $title = isset( $instance['title'] ) ? $instance['title'] : '';
    $content = isset($instance['content']) ? $instance['content'] : '';
    /** @Ticket - #12995 */
    $custom_class = isset($instance['custom_class']) ? $instance['custom_class'] : '';

    /* @ticket #15241 */
    $doNotDisplayTitle = isset($instance['do_not_display_title']) ? (bool) $instance['do_not_display_title'] : false;
    /*@ticket #17316 */
    $advertise = isset($instance['advertise']) ? (bool) $instance['advertise'] : false;
?>
<input type="hidden" id="<?php echo $this->get_field_id('content'); ?>" name="<?php echo $this->get_field_name( 'content' ); ?>" value="<?php echo esc_attr($content); ?>">

<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title', 'apollo' ); ?>:</label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
</p>

<p>
    <input type="checkbox" id="<?php echo $this->get_field_id( 'do_not_display_title' ); ?>" name="<?php echo $this->get_field_name( 'do_not_display_title' ); ?>" <?php if ($doNotDisplayTitle) { echo 'checked'; } ?> />
    <label for="<?php echo $this->get_field_id( 'do_not_display_title' ); ?>"><?php _e( 'Do not display title', 'apollo' ); ?></label>
</p>

<p>
    <label for="<?php echo $this->get_field_id( 'custom_class' ); ?>"><?php _e( 'Custom class', 'apollo' ); ?>:</label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'custom_class' ); ?>" name="<?php echo $this->get_field_name( 'custom_class' ); ?>" type="text" value="<?php echo esc_attr($custom_class); ?>" />
</p>

<p>
    <a href="javascript:APLEditorWidget.showEditor('<?php echo $this->get_field_id( 'content' ); ?>');" class="button"><?php _e( 'Edit content', 'apollo' ) ?></a>
</p>

<p class="apl-advertise">
    <input class="widefat" id="<?php echo $this->get_field_id( 'advertise' ); ?>"
           name="<?php echo $this->get_field_name( 'advertise' ); ?>"
        <?php if ($advertise) { echo 'checked'; } ?>
           type="checkbox"/>
    <label for="<?php echo $this->get_field_id( 'advertise' ); ?>"><?php _e( 'Display within search results (mobile only)', 'apollo' ); ?>:</label>
</p>
