<?php 
    $bigClass = strpos($instance['content'], '[apollo_topten_viewed') !== false ? '' : 'big';

    /* @ticket #15241 */
    $doNotDisplayTitle = isset($instance['do_not_display_title']) ? (bool) $instance['do_not_display_title'] : false;
?>
<div class="r-blk <?php echo $bigClass ?> <?php echo !empty($instance['location']) ? $instance['location'] : '' ?> <?php echo isset($instance['custom_class']) ? $instance['custom_class'] : '' ; ?>">
    
    <?php if ( $has_title = isset( $instance['title'] ) && $instance['title'] && !$doNotDisplayTitle): ?>
    <h3 class="r-ttl"><span><?php _e("".$instance['title']."", 'apollo') ?></span></h3>
    <?php endif; ?>
    
    <div class="r-blk-ct apl-internal-content">
        <?php
        if ( isset($instance['content']) && $instance['content'] ) 
            echo  $instance['content'] ;    
        ?>  
    </div>
</div>
