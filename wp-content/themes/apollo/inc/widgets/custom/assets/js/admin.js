
APLEditorWidget = {

    /**
     * @var string
     */
    currentContentId: '',

    /**
     * @var string
     */
    currentEditorPage: '',

    /**
     * @var int
     */
    wpFullOverlayOriginalZIndex: 0,

    /**
     * Show the editor
     * @param string contentId
     */
    showEditor: function(contentId) {
        jQuery('#apl-editor-widget-backdrop').show();
        jQuery('#apl-editor-widget-container').show();

        this.currentContentId = contentId;
        this.currentEditorPage = ( jQuery('body').hasClass('wp-customizer') ? 'wp-customizer':'wp-widgets');

        if (this.currentEditorPage == "wp-customizer") {
            this.wpFullOverlayOriginalZIndex = parseInt(jQuery('.wp-full-overlay').css('zIndex'));
            jQuery('.wp-full-overlay').css({ zIndex: 49000 });
        }

        this.setEditorContent(contentId);
    },

    /**
     * Show event cats
     * @param string contentId
     */
    showEventCategories: function(contentId) {
        jQuery('#apl-event-cats').show();
        this.currentContentId = contentId;
        this.currentEditorPage = ( jQuery('body').hasClass('wp-customizer') ? 'wp-customizer':'wp-widgets');

        if (this.currentEditorPage == "wp-customizer") {
            this.wpFullOverlayOriginalZIndex = parseInt(jQuery('.wp-full-overlay').css('zIndex'));
            jQuery('.wp-full-overlay').css({ zIndex: 49000 });
        }

        this.setCategoriesContent(contentId);
    },

    /**
     * Set categories content
     */
    setCategoriesContent: function(contentId) {
        var content = jQuery('#'+ contentId).val().split('-');
        var displayTypes = eventCats = []
        try {
            var displayTypes = content[0].split(',');
            var eventCats = content[1].split(',');
        } catch(e) {

        }
        jQuery('#apl-event-cats').attr('data-widget',contentId);

        jQuery('.event-display-types').attr('checked', false);
        jQuery('.event-cats').attr('checked', false);

        jQuery.each(displayTypes, function(i, v) {
            jQuery('.event-display-types[value="'+v+'"]').attr('checked', 'checked');
        });

        jQuery.each(eventCats, function(i, v) {
            jQuery('.event-cats[value="'+v+'"]').attr('checked', 'checked');
        });
    },

    /**
     * Hide editor
     */
    hideEditor: function() {
        jQuery('#apl-editor-widget-backdrop').hide();
        jQuery('.apl-editor-widget-container').hide();

        if (this.currentEditorPage == "wp-customizer") {
            jQuery('.wp-full-overlay').css({ zIndex: this.wpFullOverlayOriginalZIndex });
        }
    },

    /**
     * Set editor content
     */
    setEditorContent: function(contentId) {
        var editor = tinyMCE.EditorManager.get('apollo_editor_customwidget');
        var content = jQuery('#'+ contentId).val();

        if (typeof editor == "object" && editor !== null) {
            editor.setContent(content);
        }
        jQuery('#apollo_editor_customwidget').val(content);
    },

    /**
     * Update widget and close the categories
     */
    updateWidgetAndCloseCategories: function() {

        this.hideEditor();
        var displayTypes = jQuery('input.event-display-types:checked'),
            contentDisplayTypes = [],
            eventCats = jQuery('input.event-cats:checked'),
            contentCventCats = []
            ;

        jQuery.each(displayTypes, function(i, v) {
            contentDisplayTypes.push(jQuery(v).val());
        });

        jQuery.each(eventCats, function(i, v) {
            contentCventCats.push(jQuery(v).val());
        });

        var content = contentDisplayTypes.join(',')+ '-' + contentCventCats.join(',');
        if (content == '-') content = '';
        jQuery('#'+ this.currentContentId).val(content);

        // customize.php
        if (this.currentEditorPage == "wp-customizer") {
            var widget_id = jQuery('#'+ this.currentContentId).closest('div.form').find('input.widget-id').val();
            var widget_form_control = wp.customize.Widgets.getWidgetFormControlForWidget( widget_id );
            widget_form_control.updateWidget();
        }

        // widgets.php
        else {
            wpWidgets.save(jQuery('#'+ this.currentContentId).closest('div.widget'), 0, 1, 0);
        }

    },

    /**
     * Update widget and close the editor
     */
    updateWidgetAndCloseEditor: function() {
        var editor = tinyMCE.EditorManager.get('apollo_editor_customwidget');

        if (typeof editor == "undefined" || editor == null || editor.isHidden()) {
            var content = jQuery('#apollo_editor_customwidget').val();
        }
        else {
            var content = editor.getContent();
        }

        jQuery('#'+ this.currentContentId).val(content);

        // customize.php
        if (this.currentEditorPage == "wp-customizer") {
            var widget_id = jQuery('#'+ this.currentContentId).closest('div.form').find('input.widget-id').val();
            var widget_form_control = wp.customize.Widgets.getWidgetFormControlForWidget( widget_id );
            widget_form_control.updateWidget();
        }

        // widgets.php
        else {
            wpWidgets.save(jQuery('#'+ this.currentContentId).closest('div.widget'), 0, 1, 0);
        }

        this.hideEditor();
    }

};

(function( $ ) {
    $(function () {

        /**
         * Handle click on the event module
         * */


        $('.widgets-holder-wrap').on('click', '.apl-mod-wg-child[value="event"]', function() {
            var $this = $(this);
            clearData($this);
        });

        /**
         * Select event menu if any specific page is selected
         * */
        $('#apl-event-cats').on('click', 'input[type="checkbox"]', function() {
            var root = jQuery('#apl-event-cats'),
                currentWidget = root.attr('data-widget');
            if (root.find('input[type="checkbox"]:checked').length) {
                var eventCb = $('#'+currentWidget).closest('.widget').find('.apl-mod-wg-child[value="event"]');
                if (!eventCb.is('checked')) {
                    eventCb.attr('checked', 'checked');
                }else{}
            }
        });

        /**
         * Display categories listing if click on the category display type
         * */
        $('ul.event-display-type li').on('click', 'input[value="category"]', function() {
            if ($(this).is(':checked')) {
                $('ul.event-terms li input[type="checkbox"]').prop('checked',true);
            }
            else {
                $('ul.event-terms li input[type="checkbox"]').prop('checked',false);
            }
        });

        $('ul.event-terms li input[type="checkbox"]').on('click',function () {
            var parentElement = $('input[value="category"]');
            if($(this).prop('checked') == true){
                if(parentElement.prop('checked') == false){
                    parentElement.prop('checked',true);
                }
            }
            else{
                var checked = false;
                $('ul.event-terms li input[type="checkbox"]').each(function () {
                    if($(this).prop('checked') == true){
                        checked = true;
                    }
                });
                parentElement.prop('checked',checked);
            }
        });

        $(document).bind('widget-added', function(event, widget){
            $(widget).find('.apl-wg-modules input[type="checkbox"]').attr('checked', false);
            if($(widget).closest('#sidebar-modules-area').length <= 0){
                $(widget).find('.apl-mods-sel-blk').remove();
            }
            hideCategoryListing($(widget));
        });

        $('#sidebar-modules-area').on('click','.apl-mod-wg-all',function (e) {
            var cbAll = $(e.currentTarget);
            var listContainer = $(e.currentTarget).closest('.apl-wg-modules');
            if(cbAll.prop('checked') === true){
                listContainer.find('.apl-mod-wg-child').prop('checked',true);
            } else {
                listContainer.find('.apl-mod-wg-child').prop('checked',false);
                var eventCheck = $(this).closest('.widget-content').find('.apl-mod-wg-child[value="event"]');
                clearData(eventCheck);
            }
        });

        $('#sidebar-modules-area').on('click','.apl-mod-wg-child',function (e) {
            var cbItem = $(e.currentTarget);
            var listContainer = cbItem.closest('.apl-wg-modules');
            var numOfCBItems = listContainer.find('.apl-mod-wg-child').length;
            if(listContainer.find('input[type="checkbox"][class="apl-mod-wg-child"]:checked').length == numOfCBItems){
                listContainer.find('.apl-mod-wg-all').prop('checked',true);
            } else {
                listContainer.find('.apl-mod-wg-all').prop('checked',false);
            }
        });

        removeModuleSelectionInIncorrectSidebarArea();

        hideCategoryListingAllWidget();
    });

    function clearData($this){
        var $root = $this.closest('.widget');
        if ($this.prop('checked') == false && $root.find('ul.event-specific-page').length >0  ) {
            var _confirm = confirm($this.attr('data-confirm'));
            if (_confirm == true){
                $root.find('ul.event-specific-page').remove();
                $root.find('#apl-event-cats input[type="checkbox"]:checked').attr('checked', false); // Un check specific page
                $root.find('input[type="hidden"][data-id="event-data"]').val(""); // Clean hidden data
            }else{
                $this.prop('checked',true);
            }
        }
    }

    var removeModuleSelectionInIncorrectSidebarArea = function () {
        $('.apl-mods-sel-blk').each(function (index, item) {
            var sidebarAreaId = $(item).closest('.widgets-sortables').attr('id');
            var isInWidgetList= $(item).closest('#widget-list').length > 0;
            if( !isInWidgetList && sidebarAreaId != 'sidebar-modules-area'){
                $(item).remove();
            }else if(!isInWidgetList){
                var numOfCBItems = $(item).find('.apl-mod-wg-child').length;
                if($(item).find('input[type="checkbox"][class="apl-mod-wg-child"]:checked').length == numOfCBItems){
                    $(item).find('.apl-mod-wg-all').prop('checked',true);
                }
            }


        });
    }

    /**
     * Hide the program type listing when add the Search Education to other modules
     * @param widget
     * @author TriLm
     */
    function hideCategoryListing(widget){
        var parentId = $(widget).parent().attr('id'),
            category =  widget.find('.category-listing');
        if(category.length > 0){
            var onlyInModule = category.attr('data-only-in-module');
            if(onlyInModule == 1){
                var showInModule = category.attr('data-show-in');
                console.log(showInModule);
                console.log(parentId);
                if( parentId != showInModule ){
                    category.remove();
                }
            }
        }
    }

    /**
     * Hide all program type listing in Search Education to other modules  when window loaded
     * @author TriLm
     */
    function hideCategoryListingAllWidget(){
        $('.widget-liquid-right').find('.widget').each(function () {
            var widget = $(this);
            hideCategoryListing(widget);
        })
    }
}) (jQuery);