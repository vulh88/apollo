<?php

require_once __DIR__. '/../Apollo_Widget.php';

class apollo_custom_widget extends Apollo_Widget {

    public function __construct() {

        $this->adminTemplatePath  = dirname(__FILE__). '/templates/admin.php';
        parent::__construct(

            // ID of widget
            'apollo_custom_widget', 

            // Widget name 
            __('Apollo Custom Widget', 'apollo'), 

            // Widget description
            array( 'description' => __( 'Apollo custom Widget', 'apollo' ), ) 
        );
        add_shortcode('apollo_widgets', array($this, 'setLocation'));
    }

    // Updating widget replacing old instances with new
    public function childUpdate( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ! empty( $new_instance['title'] ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['content'] = ! empty( $new_instance['content'] ) ? $new_instance['content'] : '';

        /* @ticket #15241 */
        $instance['do_not_display_title'] = isset($new_instance['do_not_display_title']) ? (bool) $new_instance['do_not_display_title'] : false;

        return $instance;
    }
    
    /**
     * Creating widget front-end
     * This is where the action happens
     */
    public function childWiget( $args, $instance ) {
        $content = apply_filters( 'apl_editor_custom_widget_content', $instance['content'] );
        $title = apply_filters( 'apl_editor_custom_widget_content', $instance['title'] );

        $str = strstr($content, "apollo_slideshow");
        if (!empty($str)) {
            $html = '<div class="r-blk black '.(!empty($args['templateLocation']) ? $args['templateLocation'] : '').' ">
                <h3 class="r-ttl"><span>'. $title.'</span><a class="btn btn-l fr" href="#">'.__( 'VIEW MORE', 'apollo' ).'</a></h3>';
            $htmlSlide = do_shortcode($content);
            $html .= $htmlSlide . '</div>';
            echo ($html);
        } else {
            ob_start();
            include dirname(__FILE__) . '/templates/frontend.php';
            $html = ob_get_contents();
            ob_end_clean();
            echo do_shortcode($html);
        }
    }

    public static function convertDataSpecificPageToArray($eventData,$type = 'event'){
        $eventDataArray = null;
        $dataTitle = array();
        if($eventData != ''){
            $eventDataArray = explode('-',$eventData);
            $title = isset($eventDataArray[0])?explode(',',$eventDataArray[0]):array();
            $category = isset($eventDataArray[1])?$eventDataArray[1]:array();
            foreach ($title as $text){
                $dataTitle[$text] = array('text' => ucfirst($text), 'items'=>array());
            }
            if(isset($dataTitle['category'])){
                $categoryArray = array();
                $category = explode(',',$category);
                if(is_array($category) && count($category)){
                    foreach ($category as $categoryId){
                        $term = get_term($categoryId,$type.'-type');
                        if($term){
                            $categoryArray[$term->term_id] = array(
                                'name' => $term->name,
                                'parent' => in_array($term->parent, $category) ? $term->parent : ''
                            );
                        }

                    }
                }
                $dataTitle['category'] = array(
                    'text' => __('Category'),
                    'items' => $categoryArray
                );
            }
        }
        return $dataTitle;
    }

    /**
     * Do not disable sidebar once widget is located on the blog footer
     * @param $args
     * @return bool
     */
    public function shouldDisableSidebar($args = array())
    {
        return isset($args['templateLocation']) && $args['templateLocation'] != 'blog_detail_footer';
    }
}  

if (is_admin()) {
    include 'apollo-custom-editor-widget.php';
}

