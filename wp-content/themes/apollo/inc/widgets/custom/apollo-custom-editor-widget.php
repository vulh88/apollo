<?php

if ( !defined( 'ABSPATH' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit();
}

/**
 * Apollo_Custom_Editor_Widget
 */
class Apollo_Custom_Editor_Widget {

	public function __construct() {

		add_action( 'load-widgets.php', array( $this, 'load_admin_assets' ) );
		add_action( 'load-customize.php', array( $this, 'load_admin_assets' ) );
		add_action( 'widgets_admin_page', array( $this, 'output_wp_editor_widget_html' ), 150 );
		add_action( 'widgets_admin_page', array( $this, 'output_event_categories' ), 150 );
		add_action( 'customize_controls_print_footer_scripts', array( $this, 'output_wp_editor_widget_html' ), 1 );
		add_action( 'customize_controls_print_footer_scripts', array( $this, 'output_event_categories' ), 1 );
		add_action( 'customize_controls_print_footer_scripts', array( $this, 'customize_controls_print_footer_scripts' ), 2 );
		
		add_filter( 'apl_editor_custom_widget_content', 'wptexturize' );
		add_filter( 'apl_editor_custom_widget_content', 'convert_smilies' );
		add_filter( 'apl_editor_custom_widget_content', 'convert_chars' );
        add_filter( 'apl_editor_custom_widget_content', 'wpautop' );

	} 

	public function load_admin_assets() {

		wp_register_script( 'apollo-editor-custom-widget-js', APOLLO_INCLUDES_URL.'/widgets/custom/assets/js/admin.js', array( 'jquery' ) );
		wp_enqueue_script( 'apollo-editor-custom-widget-js' );

		wp_register_style( 'apollo-editor-custom-widget-css', APOLLO_INCLUDES_URL.'/widgets/custom/assets/css/admin.css' );
		wp_enqueue_style( 'apollo-editor-custom-widget-css' );

	} 

	/**
	 * Action: widgets_admin_page
	 * Action: customize_controls_print_footer_scripts
	 */
	public function output_wp_editor_widget_html() {
		
		?>
		<div id="apl-editor-widget-container" style="display: none;" class="apl-editor-widget-container">
			<a class="close" href="javascript:APLEditorWidget.hideEditor();" title="<?php esc_attr_e( 'Close', 'apollo' ); ?>"><span class="icon"></span></a>
			<div class="editor">
				<?php
				$settings = array(
					'textarea_rows' => 20,
                    'tinymce' => array(
                        'height' => 250
                    )
				);
				wp_editor( '', 'apollo_editor_customwidget', $settings );
				?>
				<p>
					<a href="javascript:APLEditorWidget.updateWidgetAndCloseEditor(true);" class="button button-primary"><?php _e( 'Save and close', 'apollo' ); ?></a>
				</p>
			</div>
		</div>
		<div id="apl-editor-widget-backdrop" style="display: none;"></div>
		<?php
		
	}

	/**
	 * Action: widgets_admin_page
	 * Action: customize_controls_print_footer_scripts
	 */
	public function output_event_categories() {

		$terms = get_terms( 'event-type', array(
			'hide_empty' => false,
			'order'	=> 'ASC',
			'orderby'	=> 'name',
			'parent'	=> false,
		) );

		$parentMods = array(
			'listing'	=> __('Listing Page', 'apollo'),
			'detail'	=> __('Detail page', 'apollo'),
			'category'	=> __('Category page', 'apollo'),
		);

		?>
		<div id="apl-event-cats" style="display: none" class="apl-editor-widget-container">
			<div class="inner">
				<a class="close" href="javascript:APLEditorWidget.hideEditor();" title="<?php esc_attr_e( 'Close', 'apollo' ); ?>"><span class="icon"></span></a>
				<h2><?php _e('Event - Select pages or categories to display this widget') ?></h2>
				<ul class="event-display-type">
					<?php foreach($parentMods as $key => $val):

						?>
						<li><input class="event-display-types" type="checkbox" name="eventDisplayTypes[]" value="<?php echo $key ?>" /><?php echo $val ?></li>
					<?php endforeach; ?>
				</ul>


				<ul class="event-terms">
					<?php
					foreach ($terms as $term):
						$childs = get_terms('event-type', array(
								'hide_empty' => false,
								'order'	=> 'ASC',
								'orderby'	=> 'name',
								'parent'	=> $term->term_id,
							)
						);
						?>
						<li><input type="checkbox" class="event-cats" name="eventCats[]" value="<?php echo $term->term_id ?>" />
							<label><?php echo $term->name ?></label>

							<ul>
								<?php if ($childs): ?>
									<?php foreach($childs as $child): ?>
										<li>
											<input type="checkbox" class="event-cats" name="eventCats[]" value="<?php echo $child->term_id ?>" />
											<label><?php echo $child->name ?></label>
										</li>
									<?php endforeach; ?>
								<?php endif; ?>
							</ul>

						</li>
					<?php endforeach; ?>
				</ul>
			</div>

			<p><a href="javascript:APLEditorWidget.updateWidgetAndCloseCategories(true);" class="button button-primary"><?php _e( 'Save and close', 'apollo' ); ?></a></p>

		</div>
		<?php
	}

	public function customize_controls_print_footer_scripts() {
	
		// Because of https://core.trac.wordpress.org/ticket/27853
		// Which was fixed in 3.9.1 so we only need this on earlier versions
		$wp_version = get_bloginfo( 'version' );
		if ( version_compare( $wp_version, '3.9.1', '<' ) && class_exists( '_WP_Editors' ) ) {
			_WP_Editors::enqueue_scripts();
		}
		
	} 

} 

global $wp_editor_widget;
$wp_editor_widget = new Apollo_Custom_Editor_Widget;
