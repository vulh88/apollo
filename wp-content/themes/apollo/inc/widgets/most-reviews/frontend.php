<?php
    $title = !empty($instance['title']) ? $instance['title']:__('Most Reviews', 'apollo');
?>
<div class="r-blk <?php echo !empty($args['templateLocation']) ? $args['templateLocation'] : '' ?>">
    <h3 class="r-ttl"><i class="fa fa-eye fa-lg"></i><span><?php echo $title; ?></span></h3>

    <div class="r-blk-ct">
        <ul>
            <li><a href="#" class="rvw-itm">
                    <div class="rvw-pic"> <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/uploads/b1.jpg" alt="b 1"></div>
                    <h4><?php _e('This widget uses FontAwesome', 'apollo') ?></h4>
                    <p><?php _e('at', 'apollo') ?> <span><?php _e('St. Helena Library', 'apollo') ?></span></p></a></li>
            <li><a href="#" class="rvw-itm">
                    <div class="rvw-pic"> <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/uploads/b2.jpg" alt="b 1"></div>
                    <h4><?php _e('You can edit this home page using our free, drag and drop Page Builder', 'apollo') ?></h4>
                    <p><?php _e('at', 'apollo') ?> <span><?php _e('St. AR Center Park', 'apollo') ?>  </span></p></a></li>
            <li> <a href="#" class="rvw-itm">
                    <div class="rvw-pic"> <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/uploads/b3.jpg" alt="b 1"></div>
                    <h4><?php _e('Time is valuable. Don\'t waste it.', 'apollo') ?></h4>
                    <p><?php _e('at', 'apollo') ?> <span><?php _e('HOREU Restaurant', 'apollo') ?></span></p></a></li>
            <li> <a href="#" class="rvw-itm">
                    <div class="rvw-pic"> <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/uploads/b4.jpg" alt="b 1"></div>
                    <h4><?php _e('This widget uses FontAwesome', 'apollo') ?></h4>
                    <p><?php _e('at', 'apollo') ?> <span><?php _e('St. Helena Library', 'apollo') ?></span></p></a></li>
            <li> <a href="#" class="rvw-itm">
                    <div class="rvw-pic"> <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/uploads/b1.jpg" alt="b 1"></div>
                    <h4><?php _e('Great for testimonials.', 'apollo') ?></h4>
                    <p><?php _e('at', 'apollo') ?> <span><?php _e('St. Helena Library', 'apollo') ?></span></p></a></li>
        </ul>
    </div>
</div>