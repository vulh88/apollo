<?php
/**
 * Created by vulh.
 * User: vulh
 * Date: 9/14/15
 * Time: 2:31 PM
 */
require_once __DIR__. '/../Apollo_Widget.php';

class apollo_offer_dates_times_widget extends Apollo_Widget {

    public function __construct() {
        parent::__construct(

        // ID of widget
            'apollo_offer_dates_times_widget',

            // Widget name
            __('Apollo Offer Dates/Times Widget', 'apollo'),

            // Widget description
            array( 'description' => __( 'Apollo Offer Dates/Times Widget', 'apollo' ), )
        );

        add_shortcode('apollo_widgets', array($this, 'setLocation'));
    }



    // Updating widget replacing old instances with new
    public function childUpdate( $new_instance, $old_instance ) {

        $instance = array();
        $instance['title']                  = ! empty( $new_instance['title'] ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }

    /**
     * Creating widget front-end
     * This is where the action happens
     */
    public function childWiget( $args, $instance ) {
        global $post;

        if ( !is_single() || ! $post || $post->post_type != Apollo_DB_Schema::_EVENT_PT
        )
        {
            return;
        }

        include_once dirname(__FILE__). '/class-apollo-event-offer-dates-times.php';

        $offerObj = new Apollo_Event_Offer_Dates_Time($post->ID);
        echo $offerObj->renderDateTime();
    }


}