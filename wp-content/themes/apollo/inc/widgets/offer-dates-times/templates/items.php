<?php

$buyTicketTxt = of_get_option(Apollo_DB_Schema::_APL_EVENT_BUY_TICKET_TEXT, __('Buy Tickets', 'apollo'));
$checkDiscountTxt = of_get_option(Apollo_DB_Schema::_APL_EVENT_CHECK_DISCOUNTS_TEXT, __('Check Discounts', 'apollo'));
$boothOverrideText = of_get_option(Apollo_DB_Schema::_APL_EVENT_BOOTH_OVERRIDE, __('Booth Deal', 'apollo'));
$boothOverrideUrl = of_get_option(Apollo_DB_Schema::_APL_EVENT_BOOTH_OVERRIDE_URL, '');
foreach($this->results as $odt):
    if(!isset($event)) {
        $event = get_event($odt->event_id);
    }

    $offical = $event->get_meta_data(Apollo_DB_Schema::_ADMISSION_TICKET_URL, Apollo_DB_Schema::_APOLLO_EVENT_DATA);
    $venue = $event->getVenueEvent();
    $venueName = !$venue->isEmpty() ?  $venue->get_title() : $event->event_venue_link();
    
    ?>
    <div class="dt-row">
        <div class="l-dt">
            <div class="l-dt-tt"><?php echo date('l, M j', strtotime($odt->date_event)) ?></div>
            <div class="l-dt-txt">
                <p><?php echo sprintf(__('%s at %s', 'apollo'), date('g:i a', strtotime($odt->date_event. ' '. $odt->time_from) ), $venueName) ?></p>
            </div>
        </div>
        <div class="r-dt">
            <?php if ($offical): ?>
                <a target="_blank" href="<?php echo $offical ?>" class="btn btn-l dt" data-activity="click-buy-ticket"><?php echo $buyTicketTxt ?></a>
            <?php endif; ?>

            <?php if ( isset( $odt->booth_override ) && $odt->booth_override == 1 ): ?>
                
                <?php if ($boothOverrideUrl): ?>
                <a href="<?php echo $boothOverrideUrl ?>" 
                   class="btn btn-l dt pink"><?php echo $boothOverrideText ?></a>
                <?php endif; ?>   
                   
            <?php elseif ($odt->ticket_url): ?>
                <a target="_blank" href="<?php echo $odt->ticket_url ?>" class="btn btn-l dt" data-activity="click-discount"><?php echo $checkDiscountTxt ?></a>
            <?php endif; ?>
            
        </div>
    </div>
<?php endforeach; ?>