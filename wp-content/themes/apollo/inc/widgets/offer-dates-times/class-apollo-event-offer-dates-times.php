<?php

class Apollo_Event_Offer_Dates_Time {

    public $event_id;
    public $pageSize;
    public $offset = 0;
    public $page;
    public $total;
    public $results = '';

    function __construct($event_id, $page = 1) {
        $this->event_id = $event_id;
        $this->page = $page;
        $this->pageSize = Apollo_Display_Config::_NUMBER_OFFER_DATE_TIME;
        $this->offset = ($this->page-1) * $this->pageSize;
    }

    public function getOffers() {

        global $wpdb;
        
        if ($this->results) {
            return $this->results;
        }
        
        $calTbl = $wpdb->{Apollo_Tables::_APL_EVENT_CALENDAR};
        $filterType = of_get_option(Apollo_DB_Schema::_EVENT_OFFER_WIDGET_FILTER_TYPE, 'discount_only');

        // Get All datetime
        $whereDiscount = $filterType == 'discount_only' ? "ticket_url <> ''" : '1=1';
       
        $dateNow = current_time('Y-m-d');
        $sql = "
        SELECT SQL_CALC_FOUND_ROWS * FROM $calTbl
        WHERE $whereDiscount AND CAST(date_event AS DATE) >= '$dateNow'
        AND event_id = $this->event_id
        ORDER BY date_event ASC
        LIMIT $this->offset, $this->pageSize ";

        $this->results = $wpdb->get_results($sql);

        $this->total = (int) $wpdb->get_var("SELECT FOUND_ROWS()");
    }

    public function isShowMore() {

        $current_pos = ($this->page * $this->pageSize);
        return $current_pos < $this->total;
    }

    function getHtmlItem() {
        ob_start();
        include dirname(__FILE__). '/templates/items.php';
        $html =  ob_get_contents();
        ob_end_clean();
        return $html;
    }

    function isEmpty() {
        return !$this->total;
    }

    function renderDateTime( $wrapper = 'wrapper' ) {

        $this->getOffers();
        if ($this->isEmpty()) return;

        $htmlItems = $this->getHtmlItem();

        $data = array(
            'more' => $this->isShowMore(),
            'html'  => $htmlItems,
            'event_id'  => $this->event_id
        );

        ob_start();
        include dirname(__FILE__). '/frontend.php';
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }

}