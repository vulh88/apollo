<!-- Override your template here -->
<?php
$title = isset( $instance['title'] ) && $instance['title'] ? $instance['title'] : __('Dates and Times', 'apollo');
$loadMoreTxt = of_get_option(Apollo_DB_Schema::_APL_LOAD_MORE_TXT_OFFER_DATESTIME_WIDGET, __('Load More...', 'apollo'));
$wrapper = isset($wrapper) ? $wrapper : 'wrapper';
if ($data['html']):
?>

<div class="r-blk apl-offer-wrapper  <?php echo !empty($args['templateLocation']) ? $args['templateLocation'] : '' ?>">
    <h3 class="r-ttl datetime"> <span><?php echo $title ?></span></h3>
    <div class="r-blk-ct datetime">
        <div class="<?php echo $wrapper ?>">
            <?php echo $data['html'] ?>
        </div>


        <?php if ($data['more']): ?>
        <div class="load-mote-dt"><a class="btn btn-l"
                href="javascript:void(0);"
                data-ride="ap-more"
                data-holder=".r-blk-ct.datetime .<?php echo $wrapper ?>>:last"
                data-sourcetype="url"
                data-sourcedata="<?php echo admin_url('admin-ajax.php?action=apollo_get_more_offer_dates_times&page=2&event_id='. $data['event_id'] .'') ?>"
                data-blockuihtml="<?php echo esc_attr('<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>') ?>"
                data-blockuicss='<?php echo esc_attr(Apollo_App::getLoadingStyle()); ?>'>
                <?php echo $loadMoreTxt ?></a></div>
        <?php endif; ?>

    </div>
</div>
<?php endif; ?>