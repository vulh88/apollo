
<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
           name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"
           value="<?php echo esc_attr( isset( $instance['title'] ) ? $instance['title'] : '' ); ?>" />
</p>

<p>
    <label  for="<?php echo $this->get_field_id( 'number_show' ); ?>"><?php _e( 'Number of posts to show:','apollo' ); ?></label>
    <input  name="<?php echo $this->get_field_name( 'number_show' ); ?>" type="text" id="<?php echo $this->get_field_id( 'number_show' ); ?>"
            value="<?php echo  isset( $instance['number_show'] )  ? $instance['number_show'] : '5' ; ?>" size="3">
</p>

<p>
    <input class="checkbox" type="checkbox"  name="<?php echo $this->get_field_name( 'show_image' ); ?>"
        <?php echo  isset( $instance['show_image'] ) &&   $instance['show_image'] ? 'checked' : '' ; ?> id="<?php echo $this->get_field_id( 'show_image' ); ?>" >
    <label for="<?php echo $this->get_field_id( 'show_image' ); ?>"><?php _e( 'Do not display feature image?','apollo' ); ?></label>
</p>



