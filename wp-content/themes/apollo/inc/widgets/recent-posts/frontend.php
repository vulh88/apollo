<?php
$args = array(
    'post_type' => array('post'),
    'post_status' => 'publish',
    'showposts' => $instance['number_show'],
    'order'         => 'DESC',
    'orderby'       => 'post_date',

);
query_posts($args);
$link = Apollo_App::getCustomUrlByModuleName(Apollo_DB_Schema::_BLOG_POST_PT);
$title = $instance['title'];

if ( have_posts() ): ?>
  <div  class="r-blk big std recentpost widget_recent_post ">
      <h3 class="r-ttl"><span> <?php echo $title?></span></h3>
      <?php      while (have_posts() ) : the_post();
          $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID()),'apl_recent_posts' ); ?>
        <div class="apl-recent-post">
            <?php if ( !empty($image) && (!isset($instance['show_image']) || !$instance['show_image'])) : ?>
                    <div class="alignleft apl-recent-post-img-wrapper">
                        <a href="<?php  the_permalink();?>">
                            <img class="alignleft apl-recent-post-img "  src=" <?php echo $image[0]; ?>" alt=""  />
                        </a>
                    </div>
            <?php endif; ?>

            <div class="alignleft apl-recent-post-content">
                <span>
                    <a href="<?php  the_permalink();?>">  <h3><?php  the_title();?></h3></a>
                </span>
                <br/>
                <p class="post-author-link">
                    <span><?php  _e('By: ', 'apollo'); ?></span>
                    <?php echo the_author_posts_link(); ?>
                </p>
            </div>
        </div>

<?php endwhile;
      wp_reset_query()
            ?>
    <div class="recent-post-footer-link">
        <a href="<?php echo $link; ?>"><?php echo sprintf(__('View all %s ','apollo'),strtolower($title)); ?> &#187;</a>
    </div>
  </div>

<?php   endif;?>