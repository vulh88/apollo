<?php
require_once  APOLLO_INCLUDES_DIR. '/widgets/recommend-event/class-recommend-event-model.php';
global $wpdb, $post;

if ($post->post_type === 'event' && is_single()) {

    $title = $instance['recommend-title'] ? $instance['recommend-title'] : __('Recommended Events');
    $maxDisplays = $instance['max-displays'] ? $instance['max-displays'] : 50;

    $model = new Apollo_Recommend_Event_Model();
    $recommendEventID = $model->getRecommendPost($maxDisplays);

    if (!empty($recommendEventID)) { ?>

        <div class="r-blk big std recentpost widget_recent_post ">
            <h3 class="r-ttl"><span> <?php echo $title ?></span></h3>

            <?php foreach ($recommendEventID as $recommend) {
                if($post->ID ===  $recommend->ID) continue;
                $postID = $recommend->ID;
                $event = get_event($postID);
                ?>
                <div class="apl-recent-post">
                    <div class="alignleft apl-recent-post-img-wrapper">
                        <a href="<?php echo $event->get_permalink(); ?>">
                            <?php echo $event->get_image('apl_recent_posts', array(),
                                array(
                                    'aw' => true,
                                    'ah' => true,
                                ),
                                'normal'
                            ) ?>
                        </a>
                    </div>
                    <div class="apl-recent-post-content">
                        <span>
                            <a href="<?php echo $event->get_permalink(); ?>">  <h3><?php echo $event->get_title(); ?></h3></a>
                        </span>
                        <br/>
                        <p class="apl-recomment-event-date">
                            <a href="<?php echo $event->get_permalink() ?>"><?php $event->render_sch_date() ?></a>
                        </p>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php wp_reset_query();  };
}