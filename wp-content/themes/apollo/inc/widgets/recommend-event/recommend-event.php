<?php

require_once APOLLO_INCLUDES_DIR. '/widgets/Apollo_Widget.php';

class apollo_recommend_event_widget extends WP_Widget {
    private $adminTemplatePath = '';
    public function __construct() {
        $this->adminTemplatePath = dirname(__FILE__). '/admin.php';
        parent::__construct(

            // ID of widget
            'apollo_recommend_event_widget',

            // Widget name
            __('Recommended Events Widget', 'apollo'),

            // Widget description
            array( 'description' => __( 'Recommended Events Widget', 'apollo' ), )
        );
    }

    public function form($instance)
    {
        if (!file_exists($this->adminTemplatePath)) {
            return '';
        }

        ob_start();
        include $this->adminTemplatePath;
        $html = ob_get_contents();
        ob_end_clean();
        echo $html;
    }

    public function widget($args, $instance)
    {
        $fileName = dirname(__FILE__) . '/frontend-templates/recommends.php';
        if (!file_exists($fileName)) {
            return '';
        }
        ob_start();
        include $fileName;
        $html = ob_get_contents();
        ob_end_clean();
        echo $html;
    }

    public function update($new_instance, $old_instance)
    {
        $new_instance['max-displays'] = (int)$new_instance['max-displays'];
        if (!is_int($new_instance['max-displays']) || $new_instance['max-displays'] <= 0) {
            $new_instance['max-displays'] = '';
        }
        return parent::update($new_instance, $old_instance);
    }
}