<p>
    <label for="<?php echo $this->get_field_id( 'recommend-title' ); ?>"><?php _e( 'Title:' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'recommend-title' ); ?>"
           name="<?php echo $this->get_field_name( 'recommend-title' ); ?>" type="text"
           value="<?php echo esc_attr( isset( $instance['recommend-title'] ) ? $instance['recommend-title'] : '' ); ?>" />
</p>

<p>
    <label  for="<?php echo $this->get_field_id( 'max-displays' ); ?>"><?php _e( 'Display up to:','apollo' ); ?></label>
    <input  class="widefat"  name="<?php echo $this->get_field_name( 'max-displays' ); ?>" type="text"
            id="<?php echo $this->get_field_id( 'max-displays' ); ?>"
            value="<?php echo  isset( $instance['max-displays'] )  ? $instance['max-displays'] : 50 ; ?>">
    <span><?php echo __('Default value is 50', 'apollo') ?></span>
</p>