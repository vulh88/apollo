<?php

class Apollo_Recommend_Event_Model
{
    public function getRecommendPost($maxDisplays)
    {
        global $post;

        $currentTags = get_apollo_meta($post->ID, Apollo_DB_Schema::_APL_EVENT_OFFICIAL_TAGS, true);
        $currentTags = $currentTags ? $currentTags : "";

        if(!$currentTags){
            return array();
        }

        $currentTags = explode(',',$currentTags);
        $conditions = implode("%' or official_tags.meta_value like '%", $currentTags);
        $conditions = "official_tags.meta_value like '%". $conditions. "%'";

        require_once APOLLO_INCLUDES_DIR. '/src/event/inc/class-event-search.php';
        $objSearch = new Apollo_Event_Page(false, false, $conditions);
        $objSearch->search($maxDisplays);

        $recommentPostID = $objSearch->get_results();

        return $recommentPostID;
    }
}