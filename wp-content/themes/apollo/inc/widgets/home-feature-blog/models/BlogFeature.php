<?php


class Apollo_Home_Blog_Feature_Model {

    protected $largeArticles;

    protected $smallArticles;

    protected $maxWords;

    protected $featuredContent;

    protected $blogCategoryType;

    protected $length;

    protected $position;


    public function __construct()
    {
        $largeArticles = array(of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_LARGE_LEFT,0),of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_LARGE_RIGHT,0));
        $smallArticles = array(of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_TOP,0),of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_MIDDLE,0),
            of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_BOTTOM,0));

        /** Only show publish post */
        $largeItem = array();
        if (!empty($largeArticles)) {
            foreach ($largeArticles as $large) {
                if (get_post_status($large) == 'publish') {
                    $largeItem[] = $large;
                }
            }
        }
        $smallItem = array();
        if (!empty($smallArticles)) {
            foreach ($smallArticles as $small) {
                if (get_post_status($small) == 'publish') {
                    $smallItem[] = $small;
                }
            }
        }

        $this->setMaxWords(intval(of_get_option(Apollo_DB_Schema::_BLOG_NUM_OF_HEADLINE,75)));
        $this->setLargeArticles(array_filter($largeItem));
        $this->setSmallArticles(array_filter($smallItem));
        /*@ticket #16738: Octave Theme - [Home blog feature] Increase the max words of the description each blog item*/
        $this->setLength(apply_filters('apl_increase_max_word_for_description_of_home_blog_feature', 135));


        if (!empty($this->getLargeArticles()) && !empty($this->getSmallArticles())) {


            // Set page content
            if (of_get_option(Apollo_DB_Schema::_HOME_CMS_PAGE_BLOG_FEATURED) == 1) {
                $pageId = Apollo_Page_Creator::getPageId(Apollo_Page_Creator::ID_BLOG_HOME_FEATURED);
                if ($blogHomeFeatured = get_post($pageId)) {
                    $this->setFeaturedContent($blogHomeFeatured->post_content);
                }
            }

            // Set blog category type
            $blogType = of_get_option( Apollo_DB_Schema::_BLOG_CATEGORY_TYPES, 'parent-only');
            $this->setBlogCategoryType($blogType);
        }


        $pos = of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_POSTION, 'top');
        $this->setPosition($pos);
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return mixed
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param mixed $length
     */
    public function setLength($length)
    {
        $this->length = $length;
    }

    /**
     * @return mixed
     */
    public function getBlogCategoryType()
    {
        return $this->blogCategoryType;
    }

    /**
     * @param mixed $blogCategoryType
     */
    public function setBlogCategoryType($blogCategoryType)
    {
        $this->blogCategoryType = $blogCategoryType;
    }


    /**
     * @return mixed
     */
    public function getFeaturedContent()
    {
        return $this->featuredContent;
    }

    /**
     * @param mixed $featuredContent
     */
    public function setFeaturedContent($featuredContent)
    {
        $this->featuredContent = $featuredContent;
    }

    /**
     * @return mixed
     */
    public function getMaxWords()
    {
        return $this->maxWords;
    }

    /**
     * @param mixed $maxWords
     */
    public function setMaxWords($maxWords)
    {
        $this->maxWords = $maxWords;
    }


    /**
     * @return mixed
     */
    public function getLargeArticles()
    {
        return $this->largeArticles;
    }

    /**
     * @param mixed $largeArticles
     */
    public function setLargeArticles($largeArticles)
    {
        $this->largeArticles = $largeArticles;
    }

    /**
     * @return mixed
     */
    public function getSmallArticles()
    {
        return $this->smallArticles;
    }

    /**
     * @param mixed $smallArticles
     */
    public function setSmallArticles($smallArticles)
    {
        $this->smallArticles = $smallArticles;
    }
}