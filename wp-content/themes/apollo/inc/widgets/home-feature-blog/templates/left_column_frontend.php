<?php
require_once __DIR__.'/../models/BlogFeature.php';
$model = new Apollo_Home_Blog_Feature_Model();
$largeArticles = $model->getLargeArticles();
$smallArticles = $model->getSmallArticles();
$maxWords = $model->getMaxWords();

if (!$largeArticles && !$smallArticles) {
    return '';
}
?>
<section class="grid blk-fea-article clearfix featured-posts-widget <?php  echo $model->getPosition(); ?> left-column ">
    <h2 class="fea-ttl <?php echo isset($featureBlockTitleStyle) ? $featureBlockTitleStyle : ''?>"><span class="fea-ttl__text"><?php echo of_get_option(Apollo_DB_Schema::_HOME_FEATURED_ARTICLES_TITLE) ?></span></h2>
    <div class="row <?php echo isset($is_group_dashboard) && $is_group_dashboard ? '' : 'two-col' ?>">

        <?php if (!empty($largeArticles)): ?>

            <div class="v-line"></div>
            <div class="wc-l">
                <section class="list-blog">
                    <?php

                    foreach ($largeArticles as $article) {
                        if (isset($article) && $article) {
                            $blogPost = get_apl_blog($article);

                            $summary = $blogPost->get_summary_blog($model->getLength(), true);
                            $content = strip_tags($summary['text']);
                            $author_id = get_post($article)->post_author;
                            $url = wp_get_attachment_url(get_post_thumbnail_id($article));
                            $title = get_the_title($article);
                            $title = $blogPost->getStringByLength($title,$maxWords,true);
                            $title = strip_tags($title['text']);

                            /*@ticket #16896:  BLOG - Sponsored Content - Add the same type of label and background color to their home page featured blog posts*/
                            $sponsoredContent = get_post_meta($article,'post-sponsored-content',true);
                            /*@ticket #177560:  BLOG - Sponsored Content - New Page checkbox and URL field textbox*/
                            $urlField = $blogPost->renderURLBlog();

                            ?>
                            <article class="blog-itm <?php echo ($sponsoredContent && $sponsoredContent === 'on') ? 'post-sponsored-content-feature-blog' : ''?>">
                                <?php
                                if ($sponsoredContent && $sponsoredContent === 'on'){
                                    echo "<div  class='sponsored-content-text'>" .  __('SPONSORED CONTENT', 'apollo') . "</div>";
                                }
                                ?>
                                <?php if (has_post_thumbnail($article)) { ?>
                                    <a <?php echo $urlField ?>>
                                        <figure style="background-image: url('<?php echo $url ?>')" class="fea-img"></figure>
                                    </a>
                                <?php } else { ?>
                                    <a <?php echo $urlField ?>>
                                        <figure
                                                style="background-image: url('<?php echo Apollo_App::defaultBlogImage() ?>')"
                                                class="fea-img"></figure>
                                    </a>
                                <?php } ?>
                                <div class="fea-cat">
                                    <?php Apollo_App::apl_get_blog_categories($article, $model->getBlogCategoryType()) ?>
                                </div>
                                <a <?php echo $urlField ?>><h3><?php echo $title; ?></h3></a>
                                <div class="fea-author"><?php $blogPost->the_get_author_link() ?></div>
                                <div
                                        class="desc" data-ride="ap-clamp"
                                        data-n="5"> <?php echo $content ?>
                                </div>

                            </article>
                        <?php }
                    }
                    ?>
<?php
                    if (!empty($smallArticles)):
                    ?>

                    <div class="second-row">
                        <?php

                        foreach ($smallArticles as $article) {
                            if (isset($article) && $article) {
                                $author_id = get_post($article)->post_author;
                                $url = wp_get_attachment_url(get_post_thumbnail_id($article));
                                $maxWords = 56;
                                $title = get_the_title($article);
                                $blogPost = get_apl_blog($article);
                                $title = $blogPost->getStringByLength($title,$maxWords,true);
                                $title = strip_tags($title['text']);

                                /*@ticket #16896:  BLOG - Sponsored Content - Add the same type of label and background color to their home page featured blog posts*/
                                $sponsoredContent = get_post_meta($article,'post-sponsored-content',true);
                                /*@ticket #177560:  BLOG - Sponsored Content - New Page checkbox and URL field textbox*/
                                $urlField = $blogPost->renderURLBlog();
                                ?>
                                <div class="fea-itm clearfix <?php echo ($sponsoredContent && $sponsoredContent === 'on') ? 'post-sponsored-content-small-feature-blog' : ''?>">
                                    <a <?php echo $urlField ?>>
                                        <?php
                                        if ($sponsoredContent && $sponsoredContent === 'on'){
                                            echo "<div  class='sponsored-content-text'>" .  __('SPONSORED CONTENT', 'apollo') . "</div>";
                                        }
                                        ?>
                                        <?php if (has_post_thumbnail($article)) { ?>
                                            <figure style="background-image: url('<?php echo $url ?>')" class="fea-pic"></figure>
                                        <?php } else { ?>
                                            <figure
                                                    style="background-image: url('<?php echo Apollo_App::defaultBlogImage('small') ?>')"
                                                    class="fea-pic"></figure>
                                        <?php } ?>
                                    </a>
                                    <div class="fea-info">
                                        <div class="fea-cat">
                                            <?php Apollo_App::apl_get_blog_categories($article,$model->getBlogCategoryType())?>
                                        </div>
                                        <a <?php echo $urlField ?>><h3><?php echo $title; ?></h3></a>
                                        <div class="fea-author"><?php $blogPost->the_get_author_link() ?></div>
                                    </div>
                                </div>
                            <?php }
                        }
                        ?>
                        <!-- END : ITM-->
                    </div>
                    <?php endif; ?>

                </section>
            </div>
        <?php endif; ?>


    </div>

    <?php if ($content = $model->getFeaturedContent()): ?>
        <div class="home-blog-cms-page apl-internal-content"><p><?php echo $content ?></p></div>
    <?php endif ?>

</section>
<!-- END : FEATURE ARTICLE-->
