<?php

require_once __DIR__. '/../Apollo_Widget.php';

class Apollo_Home_Blog_Feature extends Apollo_Widget {

    public function __construct() {
        parent::__construct(
            'apollo_home_blog_feature_widget',

            __('Apollo Home Blog Feature Widget', 'apollo'),

            // Widget description
            array( 'description' => __( 'Apollo Home Blog Feature Widget', 'apollo' ), )
        );
    }

    /**
     * Creating widget front-end
     * This is where the action happens
     * @param $args
     * @param $instance
     * @return mixed|string
     */
    public function childWiget( $args, $instance ) {

        if (!$this->isAvailable($args)) {
            echo '';
            return false;
        }

        $cacheFileClass = aplc_instance('APLC_Inc_Files_HomeFeaturedBlog');
        $html = $cacheFileClass->get();
        /*@ticket #17250: Octave Theme - [Feature block] New feature block title - item 3, 4, 5, 6, 7*/
        $featureBlockTitleStyle = of_get_option(Apollo_DB_Schema::_FEATURE_BLOCK_TITLE_STYLE, '');

        if (!$html) {
            ob_start();

            if ($this->getWidgetType() =='full') {
                include(dirname(__FILE__) . '/templates/full_width_frontend.php');
            }
            else {
                include(dirname(__FILE__) . '/templates/left_column_frontend.php');
            }

            $html = ob_get_contents();
            ob_end_clean();
            $cacheFileClass->save($html);
        }

        echo $html;
    }

    /**
     * Check widget is available
     * @param $args
     * @return bool
     * @internal param string $position
     */
    private function isAvailable($args) {

        $position = !empty($args['position']) ? $args['position'] : 'top';
        $type = !empty($args['type']) ? $args['type'] : 'full';

        // Check the type
        if ($this->getWidgetType() != $type) {
            return false;
        }

        // Check the position
        if (of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_POSTION) != $position) {
            return false;
        }

        return Apollo_App::is_avaiable_module(Apollo_DB_Schema::_BLOG_POST_PT)
        && Apollo_App::is_homepage()

        && (of_get_option(Apollo_DB_Schema::_ACTIVE_HOME_FEATURED_ARTICLES,1)

            && (of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_LARGE_LEFT,0)
                ||  of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_LARGE_RIGHT,0)
                ||  of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_TOP,0)
                ||  of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_MIDDLE,0)
                ||  of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_RIGHT_BOTTOM,0))
        );
    }

    private function getWidgetType() {
        return of_get_option(Apollo_DB_Schema::_HOME_FEATURED_BLOG_POST_TYPE, 'full');
    }
}
