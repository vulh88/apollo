<?php

/**
 * Created by PhpStorm.
 * User: pc92-vu
 * Date: 18/03/2016
 * Time: 14:10
 */
class Apollo_Widget extends WP_Widget
{
    public $location;
    public $currentAplModule;
    protected $adminTemplatePath = '';

    public function __construct($widgetID, $widgetName, $widgetDesc)
    {
        parent::__construct($widgetID, $widgetName, $widgetDesc);
    }

    public function setLocation($args) {
        $this->location = $args['location'];
        $this->currentAplModule = isset($args['current_mod']) ? $args['current_mod'] : '';
        add_filter('dynamic_sidebar_params', function($params) {
            $params[0]['templateLocation'] = $this->location;
            $params[0]['templateCurrentMod'] = $this->currentAplModule;
            return $params;
        });
        dynamic_sidebar($args['key']);
    }

    /**
     * Whether allow to display shortcode or not
     *
     * @param  array $args
     * @param  array $instance
     * @return boolean
     */
    public function isAllowed($args, $instance) {

        // If this widget is not Custom Widget, only display it in the default location
        if (empty($instance['location'])) {
            return !isset($args['templateLocation']) || $args['templateLocation'] == 'std';
        }

        $widgetLocation = $instance['location'];
        switch($widgetLocation) {
            // Display widget in both Mobile and Desktop screen, handle hide/show by css
            case 'std_cus_pos':
                return true;
            break;

            // Mobile Only means it will be displayed in the right sidebar of mobile screen
            // Desktop Only means it will be displayed in the right sidebar of desktop screen
            case 'desktop_only':
            case 'mobile_only':
                $instance['location'] = 'std';
            break;

            // Only display this widget in the mobile custom location
            case 'mobile_only_cus_pos':
                if ($args['templateLocation'] == 'std_cus_pos') return true;
            break;
        }

        $templateLocation = $args['templateLocation'];
        return !($templateLocation && !empty($instance['location']) && $instance['location'] != $templateLocation);
    }

    /**
     * Whether allow to display shortcode or not in specify widget area Sidebar All Modules
     *
     * @param  array $args
     * @param  array $instance
     * @return boolean
     */
    public function isAllowedDisplayOnSidebarAllModuleSArea($args, $instance) {
        if(!isset($args['id']) || (isset($args['id']) && trim($args['id']) != 'sidebar-modules-area')){
            return true;
        }
        $currentMod = isset($args['templateCurrentMod']) ?  $args['templateCurrentMod'] : '';

        if (!empty($instance['eventData'])) {
            list($displayTypes, $cats) = explode('-', $instance['eventData']);
            $displayTypesArr = explode(',', $displayTypes);
            $catsArr = explode(',', $cats);

            /**
             * has set specific page
             */

            global $apl_current_event_term;
            // Detail page || Category page || Listing
            if (
                (is_single() && !in_array('detail', $displayTypesArr))
                || ($catsArr && $apl_current_event_term && !in_array($apl_current_event_term, $catsArr))
                || (get_query_var('_apollo_event_search') === 'true' && $displayTypesArr && !in_array('listing', $displayTypesArr))
            ) return false;

        }

        $selectedModsToDisplay = isset($instance['selected_apl_modules']) ? $instance['selected_apl_modules'] : array();
        return is_array($selectedModsToDisplay) && in_array($currentMod,$selectedModsToDisplay);
    }

    public function allowDisplayEducationSearchWidget($args, $instance) {

        global $apl_current_event_term;

        $exceptProgramTypes = isset($instance['selected_apl_prog_types']) ? $instance['selected_apl_prog_types'] : array();

        if ($exceptProgramTypes && in_array($apl_current_event_term, $exceptProgramTypes)) {
            return false;
        }
        return true;
    }

    /**
     * @author Trilm
     */
    public function form($instance)
    {

        if (!file_exists($this->adminTemplatePath)) {
            return '';
        }

        ob_start();
        include $this->adminTemplatePath;
        include(APOLLO_WIDGETS_DIR.'/global-template/module-listing.php');
        $html = ob_get_contents();
        ob_end_clean();
        echo $html;
    }

    /**
     * @author Trilm
     */
    public function update($new_instance, $old_instance)
    {
        $instance['location'] = ! empty( $new_instance['location'] ) ? $new_instance['location'] : '';
        $instance['eventData'] = ! empty( $new_instance['eventData'] ) ? strip_tags( $new_instance['eventData'] ) : '';
        $instance['selected_apl_modules'] = ! empty( $new_instance['selected_apl_modules'] ) ? $new_instance['selected_apl_modules'] : '';
        $childInstance = $this->childWiget($new_instance,$old_instance);
        $instance = array_merge($instance,$childInstance);
        return parent::update($new_instance,$old_instance);
    }

    /**
     * @author Trilm
     * @param $args
     * @param $instance
     * @return bool
     * All global allow rule here
     */
    public function checkAllow($args, $instance)
    {

        if(!$this->isAllowedDisplayOnSidebarAllModuleSArea($args, $instance)){
            return false;
        }

        if (!$this->isAllowed($args, $instance)) {
            return false;
        }

        return true;
    }

    /**
     *
     * @author Trilm
     * All global widget show/hide in frontend here
     *
     */
    public function widget($args, $instance)
    {
        $isAllow = $this->checkAllow($args,$instance);
        if($isAllow){
            /**
             * @ticket 9747
             * Trilm disble primary widget area or another module
             */
            if ($this->shouldDisableSidebar($args)) {
                Apollo_Static_Sidebar::disable_sidebar();
            }

            $this->childWiget($args, $instance);
        }else{
            return '';
        }

    }

    /**
     * @param array $args
     * @return bool
     */
    public function shouldDisableSidebar($args = array()) {
        return true;
    }

    /***
     * @author: Trilm
     * @param $args
     * @param $instance
     * @return mixed
     * Child widger frontend override here
     */
    public  function childWiget($args, $instance){}

    /**
     * @uthor: Trilm
     * All child update method override this function
     */
    public function childUpdate($new_instance, $old_instance){}


}