
<?php
$location = isset($instance['location']) ? $instance['location'] : '';
$enabledDisplayedModules = isset($instance['selected_apl_modules']) ?$instance['selected_apl_modules'] : array();
$eventData = isset($instance['eventData']) ? $instance['eventData'] : '';
$dataTitles =  apollo_custom_widget::convertDataSpecificPageToArray($eventData);
global $widget_locations, $apollo_widget_modules;

if ($widget_locations && $this->id_base == 'apollo_custom_widget'):

    /*@ticket #17681 */
    $customLocation = apply_filters('apl_add_custom_position_for_widget_location', array());
    if(!empty($customLocation)){
        $widget_locations = array_merge($widget_locations, $customLocation) ;
    }
    ?>
    <p>
        <label><?php _e('Location', 'apollo') ?></label>
        <select style="width: 100%;" class="w-100-per" name="<?php echo $this->get_field_name( 'location' ); ?>" id="<?php echo $this->get_field_id( 'location' ); ?>" >
            <?php foreach($widget_locations as $key => $widget_location): ?>
                <option <?php echo $location == $key ? 'selected' : '' ?> value="<?php echo $key ?>"><?php echo $widget_location ?></option>
            <?php endforeach; ?>
        </select>
    </p>
<?php endif;
?>
<input type="hidden" data-id="event-data" id="<?php echo $this->get_field_id('eventData'); ?>" name="<?php echo $this->get_field_name( 'eventData' ); ?>" value="<?php echo esc_attr($eventData); ?>">

<?php if ($apollo_widget_modules): ?>
    <div class="apl-mods-sel-blk">
        <label><?php _e('Select modules for this widget to display in:', 'apollo') ?></label>
        <ul class="apl-wg-modules">
            <li>
                <?php
                $cbAllChecked = '';
                if(count($apollo_widget_modules) === count($enabledDisplayedModules)){
                    $cbAllChecked = 'checked="checked"';
                }

                ?>
                <input type="checkbox" class="apl-mod-wg-all" value="all" name="apl-mod-wg-all" <?php echo $cbAllChecked; ?> />
                <label><?php _e('All','apollo'); ?></label>
            </li>
            <?php foreach($apollo_widget_modules as $key => $mod): ?>
                <li>
                    <input
                        <?php if ($key == Apollo_DB_Schema::_EVENT_PT) ?>data-confirm="<?php _e('If you uncheck this checkbox, all specific pages, categories will be uncheck as well. Are you sure?', 'apollo') ?>"
                        type="checkbox" class="apl-mod-wg-child"
                        value="<?php echo $key; ?>"
                        name="<?php echo $this->get_field_name( 'selected_apl_modules' ) . '[]'; ?>"
                        <?php echo in_array($key,$enabledDisplayedModules) ? 'checked="checked"' : ''; ?>
                    />
                    <label><?php echo $mod['label']; ?>

                        <?php if ($key == Apollo_DB_Schema::_EVENT_PT): ?>
                            - <a href="javascript:APLEditorWidget.showEventCategories('<?php echo $this->get_field_id( 'eventData' ); ?>');"><i>Select Specific Page</i></a>
                            <?php
                            if(is_array($dataTitles) && count($dataTitles)){
                                ?>
                                <ul class="event-specific-page">
                                    <?php
                                    foreach ($dataTitles as $k => $item) {
                                        $text = isset($item['text'])?$item['text']:'';
                                        $items= isset($item['items'])?$item['items']:array();
                                        if($text!=''){
                                            $subItem = '';
                                            if(count($items) > 0){

                                                foreach ($items as $subItemID => $subItemObj){
                                                    $subItemTitle = $subItemObj['name'];
                                                    if($subItemTitle != ''){
                                                        $isChild = $subItemObj['parent'] ? 'child' : '';
                                                        $subItem .=   '<li class="'.$isChild.'" id="'.$k.'_'.$subItemID.'" >'.$subItemTitle.'</li>';
                                                    }
                                                }
                                                if($subItem != ''){
                                                    $subItem = "<ul>$subItem</ul>";
                                                }
                                            }

                                            echo  '<li>'.$text.$subItem.'</li>';

                                        }
                                        ?>
                                        <?php
                                    }
                                    ?>
                                </ul>
                                <?php
                            }
                            ?>

                        <?php endif; ?>
                    </label>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>