<?php

class apollo_search_event_widget extends apollo_search_widget {

    protected $type = Apollo_DB_Schema::_EVENT_PT;

    /**
     * Display event category drop-down
     * @author vulh
     * @return string
    */
    public static function handleEventCategoryDropDown()
    {
        $cacheFileClass = aplc_instance('APLC_Inc_Files_EventWidgetSearchCategory');
        $htmlCategory = $cacheFileClass->get();

        if (!$htmlCategory) {
            $allHiddenCatIDs = Apollo_App::hiddenCategoryIDs();
            $eventCategories = Apollo_Event::getTreeEventCategories();

            ob_start();
            Apollo_Event::build_option_tree($eventCategories,0, Apollo_Display_Config::TREE_MAX_LEVEL, 'event-type', $allHiddenCatIDs);
            $htmlCategory = ob_get_clean();
            $cacheFileClass->save($htmlCategory);
        }
        return $htmlCategory;
    }
}