<?php
require_once __DIR__. '/../Apollo_Widget.php';

class apollo_search_widget extends Apollo_Widget {

    protected $type = Apollo_DB_Schema::_EVENT_PT;

    /**
     * Using for general fields except checkbox fields
     */
    const BASENAME = 'custom_field';

    /**
     * Using for general fields except checkbox fields, this field send field_name to the server side once submitting search form
     */
    const BASENAME_KEY = 'custom_field_key';


    public function __construct() {
        $this->adminTemplatePath = dirname(__FILE__). '/admin.php';
        parent::__construct(
            
            // ID of widget
            'apollo_search_' . str_replace('-', '_', $this->type) .'_widget',

            // Widget name 
            sprintf(__('Apollo Search %s Widget', 'apollo'), ucwords(str_replace("_", " ", $this->type))),

            // Widget description
            array( 'description' => sprintf(__( 'Apollo Search %s Widget', 'apollo' ), $this->type), )
        );
        add_shortcode('apollo_widgets', array($this, 'setLocation'));
    }

    // Updating widget replacing old instances with new
    public function childUpdate( $new_instance, $old_instance ) {
        aplDebug($new_instance);
        $instance = array();
        $instance['title'] = ! empty( $new_instance['title'] ) ? strip_tags( $new_instance['title'] ) : '';
        if($this->type == Apollo_DB_Schema::_EVENT_PT){
            $instance['selected_apl_modules'] = ! empty( $new_instance['selected_apl_modules'] ) ? $new_instance['selected_apl_modules'] : '';
        }
        if($this->type == Apollo_DB_Schema::_EDUCATION){
            $instance['selected_apl_prog_types'] = ! empty( $new_instance['selected_apl_prog_types'] ) ? $new_instance['selected_apl_prog_types'] : '';
        }
        return $instance;
    }

    protected  function get_template_name() {
        return $this->type;
    }

    public function childWiget($args, $instance)
    {
        $isAllowEducation = $this->allowDisplayEducationSearchWidget($args,$instance);
        if ($this->type == Apollo_DB_Schema::_EDUCATION && !$isAllowEducation) {
            return '';
        }
        ob_start();
        include dirname(__FILE__). '/templates/'.$this->get_template_name().'.php';
        $html = ob_get_contents();
        ob_end_clean();
        echo $html;
    }

    /**
     * @Ticket #13237 #13823
     * @param $type
     * @param $mtTbl
     * @return array
     */
    public static function renderFields($type, $mtTbl) {
        $fields = Apollo_Custom_Field::get_field_by_location($type, 'sw');
        $result = '';
        if ($fields) {
            $indexCheckboxField = 0;
            $indexOtherFields = 0;
            foreach ($fields as $field) {
                if ($fields) {

                    switch($field->cf_type) {
                        case 'select':
                            $result .= self::displayFieldSelect($field, $indexOtherFields, $mtTbl, $type);
                            $indexOtherFields++;
                            break;

                        case 'checkbox':
                            $result .= self::displayFieldCheckbox($field, $indexCheckboxField);
                            $indexCheckboxField++;
                            break;

                        case 'multi_checkbox':
                            $result .= self::displayFieldSelect($field, $indexOtherFields, $mtTbl, $type);
                            $indexOtherFields++;
                            break;
                    }

                }
            }
        }
        return $result;
    }

    /**
     * @Ticket #13237 #13823
     * @param $field
     * @param $index
     * @param $mtTbl
     * @param $postType
     * @return string
     */
    private static function displayFieldSelect($field, $index, $mtTbl, $postType) {

        $result = '';
        $result .= '<div class="select-bkl">';
        $result .= '<input name="'.self::BASENAME_KEY.'[]" value="'.$field->name.'" type="hidden" />';
        $result .= '<select name="'.self::BASENAME.'[]'.'" class="custom-field">';
        $result .= '<option value="">'. $field->label .'</option>';
        $options = Apollo_Custom_Field::get_choice($field);
        $dynamicOption = '';

        $selectedVal = isset($_GET[self::BASENAME][$index]) ? $_GET[self::BASENAME][$index] : '';
        if ($options) {
            foreach ($options as $key => $value) {
                if(empty($key)) continue;

                if (self::optionHasSelected($mtTbl, $key, $field->name, $postType) == 0) {
                    continue;
                }

                $selected = ($selectedVal == $key) ? 'selected' : '';
                $dynamicOption .= '<option '.$selected.' value="'. $key .'">'. $value .'</option>';
            }
        }

        $hide = !$dynamicOption ? 'style="display: none"':'';

        $result .= $dynamicOption;
        $result .= '</select><div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div></div>';

        $result = '<div class="el-blk" '.$hide.'>' . $result . '</div>';

        return $result;
    }

    /**
     * @Ticket #13823
     * Render checkbox
     * @param $field
     * @param $index
     * @return string
     */
    private static function displayFieldCheckbox($field, $index)
    {
        /**
         * We use different key between checkbox and other fields because checkbox field does not send request data to server side
         * when submit search form
        */
        $name = 'custom_field_cb';
        ob_start();
        ?>
        <div class="el-blk">
            <input name="custom_field_cb_key[]" value="" type="hidden" />
            <input name="<?php echo $name ?>[]" type="checkbox" class="free-event" value="<?php echo $field->name ?>" <?php echo !empty($_GET[$name][$index]) ? 'checked' : '' ; ?>/>
            <label for="free-event" class="free-evt"><?php echo $field->label; ?></label>
        </div>
        <?php
        $result = ob_get_contents();
        ob_end_clean();

        return $result;
    }

    /**
     * Whether checking an option has be selected or not
     * @param $tbl
     * @param $optVal
     * @param $fieldName
     * @param string $postType
     * @return bool
     */
    private static function optionHasSelected($tbl, $optVal, $fieldName, $postType = 'artist')
    {
        global $wpdb;
        $postIDName = "apollo_{$postType}_id";
        $mtField = Apollo_DB_Schema::_APL_POST_TYPE_CF_DATA;
        $exp = "meta_key = '$mtField' AND (meta_value REGEXP '.*\"%s\";s:[0-9]+:\"%s\".*' 
        OR meta_value REGEXP '.*\"%s\";a:[0-9]+:{.*.:\"%s\";.*}.*')";
        $conditions = sprintf($exp, $fieldName, $optVal, $fieldName, $optVal);

        $mtTable = $wpdb->{$tbl};
        $sql = "
            SELECT count(*) AS total FROM $mtTable mt INNER JOIN $wpdb->posts p ON p.ID = mt.$postIDName
            WHERE $conditions AND p.post_status = 'publish' AND p.post_type = '$postType' 
        ";


        if (isset($_GET['vulh'])) {
            var_dump($sql);exit;
        }

        $r = $wpdb->get_row($sql);
        return $r && $r->total > 0;
    }
}  