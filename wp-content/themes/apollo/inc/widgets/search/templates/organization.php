<!-- Override your template here -->
<?php
global $apollo_modules;
$module_name = isset( $apollo_modules[get_post_type()] ) ? $apollo_modules[get_post_type()]['sing'] : '';
if($module_name == ''){
    $module_name = Apollo_DB_Schema::_ORGANIZATION_PT;
}

$orgCategories = Apollo_Org::get_tree_event_style(Apollo_DB_Schema::_ORGANIZATION_PT);



$searchData = array(
    'keyword' => isset($_GET['keyword'])?Apollo_App::clean_data_request($_GET['keyword']):'',
    'term' => isset($_GET['term'])?Apollo_App::clean_data_request($_GET['term']):'',
    'state' => isset($_GET['state'])?Apollo_App::clean_data_request($_GET['state']):of_get_option(Apollo_DB_Schema::_APL_DEFAULT_STATE,''),
    'city' => isset($_GET['city'])? Apollo_App::clean_data_request($_GET['city']):of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY,''),
    'zip' => isset($_GET['zip'])?Apollo_App::clean_data_request($_GET['zip']):'',
    'region' => isset($_GET['region'])?Apollo_App::clean_data_request($_GET['region']):'',
);

$cityList = Apollo_App::getCityByTerritory(false, false, true);
$listState = Apollo_App::getStateByTerritory();
$zipList = Apollo_App::getZipByTerritory(false, false, false, true);
$regionList = Apollo_App::get_regions();
$listCityState = Apollo_App::getCityStateByTerritory();

$title = isset( $instance['title'] ) && $instance['title'] ? $instance['title'] : __('Find Organization','apollo');

$regionOption = of_get_option(Apollo_DB_Schema::_APL_ENABLE_ORGANIZATION_WIDGET_SEARCH_REGION, 1);
$stateOption = of_get_option(Apollo_DB_Schema::_APL_ENABLE_ORGANIZATION_WIDGET_SEARCH_STATE, 1);
$zipOption = of_get_option(Apollo_DB_Schema::_APL_ENABLE_ORGANIZATION_WIDGET_SEARCH_ZIP, 1);
?>

<div class="r-blk r-search widget-search">
    <h3 class="r-ttl"> <i class="fa fa-search fa-flip-horizontal fa-lg">     </i><span><?php echo $title; ?>   </span></h3>
    <form method="" id="search-organization" action="<?php echo home_url('/organization')?>" class="form-event">
        <div class="organization-search">
            <div class="el-blk full">
                <input type="text" name="keyword" placeholder="<?php _e('Search by Keyword','apollo'); ?>" class="inp inp-txt" value="<?php echo Apollo_App::clean_data_request(get_query_var('keyword'), true) ?>">
                <div class="show-tip"><?php _e('Search by Keyword','apollo') ?></div>
            </div>
            <!---Org categories -->
            <div class="el-blk">
                <div class="select-bkl">
                    <select name="term" class="chosen" >
                        <option value=""><?php _e('Organization Type','apollo') ?></option>
                        <?php
                        Apollo_Org::build_option_tree($orgCategories,$searchData['term']);
                        ?>
                    </select>
                    <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                </div>
            </div>
            <!-- end categories -->

            <!--state -->
            <?php if($stateOption) : ?>
            <div class="el-blk">
                <div class="select-bkl">
                    <select class="chosen" name="state">

                        <?php foreach($listState as $k => $val){
                            $selected = '';
                            if($k == $searchData['state'])
                                $selected = 'selected';
                            ?>
                            <option <?php echo $selected; ?> value="<?php echo $k ?>"><?php echo $val ?></option>
                        <?php } ?>
                    </select>
                    <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                </div>
            </div>
            <?php endif; ?>
            <!--end state -->

            <!--- start city-->
            <div class="el-blk">
                <div class="select-bkl">
                    <select class="chosen" name="city">
                        <?php echo Apollo_App::renderCities($listCityState, $searchData['city']); ?>
                    </select>
                    <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                </div>
            </div>
            <!---end city -->

            <?php if (!Apollo_App::enableMappingRegionZipSelection()): ?>
            <!--start zip -->
            <?php if($zipOption) : ?>
            <div class="el-blk">
                <div class="select-bkl">
                    <select class="chosen" name="zip">
                        <?php foreach($zipList as $k => $zipItem){
                            $selected = '';
                            if($k == $searchData['zip'])
                                $selected = 'selected';
                            ?>
                            <option <?php echo $selected; ?> value="<?php echo $k; ?>"><?php echo $zipItem; ?></option>
                        <?php } ?>


                    </select>
                    <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                </div>
            </div>
            <?php endif; ?>
            <!--End zip -->
            <?php endif; ?>

            <!--Region -->
            <?php if($regionOption && $regionList ): ?>
                <div class="el-blk">
                    <div class="select-bkl">
                        <select class="chosen" name="region" data-refer-to="select[data-role=zip]" data-role="region">
                            <?php foreach($regionList as $k => $val){
                                $selected = '';
                                if($k == $searchData['region'])
                                    $selected = 'selected';
                                if(!empty($val)) {
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $k ?>"><?php echo $val ?></option>
                            <?php } } ?>
                        </select>
                        <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="b-btn fl">
                <input type="submit" class="btn btn-b edu-btn" value="<?php _e('SEARCH','apollo') ?>" />
                <?php
                Apollo_App::includeTemplate(get_stylesheet_directory(). '/inc/widgets/search/includes/reset-button.php', ['idForm' => 'search-organization', 'typeResetButton' => 'input']);
                ?>
            </div>
        </div>
    </form>
</div>

