<!-- Override your template here -->
<?php
global $apollo_modules;
$module_name = isset( $apollo_modules[get_post_type()] ) ? $apollo_modules[get_post_type()]['sing'] : '';
if($module_name == ''){
    $module_name = Apollo_DB_Schema::_ORGANIZATION_PT;
}

$orgCategories = Apollo_Org::get_tree_event_style(Apollo_DB_Schema::_ORGANIZATION_PT);


$searchData = array(
    'keyword' => isset($_GET['keyword'])?Apollo_App::clean_data_request($_GET['keyword']):'',
    'term' => isset($_GET['term'])?Apollo_App::clean_data_request($_GET['term']):'',
    'state' => isset($_GET['state'])?Apollo_App::clean_data_request($_GET['state']):of_get_option(Apollo_DB_Schema::_APL_DEFAULT_STATE,''),
    'city' => isset($_GET['city'])? Apollo_App::clean_data_request($_GET['city']):of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY,''),
    'zip' => isset($_GET['zip'])?Apollo_App::clean_data_request($_GET['zip']):'',
    'region' => isset($_GET['region'])?Apollo_App::clean_data_request($_GET['region']):'',
);

$cityList = Apollo_App::getCityByTerritory(false, false, true);
$listState = Apollo_App::getStateByTerritory();
$zipList = Apollo_App::getZipByTerritory(false, false, false, true);
$regionList = Apollo_App::get_regions();
$listCityState = Apollo_App::getCityStateByTerritory();
$isTopSearchMobileVersion = isset($_GET['top-search-mobile']) && !empty($_GET['top-search-mobile']) ? 'ts-mobile' : '';

$regionOption = of_get_option(Apollo_DB_Schema::_APL_ENABLE_ORGANIZATION_WIDGET_SEARCH_REGION, 1);
$stateOption = of_get_option(Apollo_DB_Schema::_APL_ENABLE_ORGANIZATION_WIDGET_SEARCH_STATE, 1);
$zipOption = of_get_option(Apollo_DB_Schema::_APL_ENABLE_ORGANIZATION_WIDGET_SEARCH_ZIP, 1);
?>
<div class="top-search <?php echo $isTopSearchMobileVersion; ?>">
    <div class="inner">
        <div class="top-search-row organization-s-t-m">
            <form method="get" id="search-organization-m-t" action="<?php echo home_url('/organization')?>" class="form-event">
                <button type="button" class="btn btn-l s"><i class="fa fa-search fa-flip-horizontal fa-lg"></i></button>
                <input type="text" name="keyword" value="<?php echo Apollo_App::clean_data_request(get_query_var('keyword'), true) ?>" placeholder="<?php _e('Search by Keyword', 'apollo') ?>" class="inp inp-txt event-search event-search-custom">
                <input type="hidden" name="do_search" value="<?php echo isset($_GET['do_search']) && !empty($_GET['do_search']) ? 'yes' : 'no'; ?>" />

                <!---Org categories -->
                <div class="el-blk">
                    <div class="select-bkl">
                        <select name="term" class="chosen" id="org-type-select" >
                            <option value=""><?php _e('Organization Type','apollo') ?></option>
                            <?php
                            Apollo_Org::build_option_tree($orgCategories,$searchData['term']);
                            ?>
                        </select>
                        <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                    </div>
                </div>
                <!-- end categories -->

                <!--state -->
                <?php if($stateOption) : ?>
                <div class="el-blk">
                    <div class="select-bkl">
                        <select class="chosen" name="state" id="org-state-select">

                            <?php foreach($listState as $k => $val){
                                $selected = '';
                                if($k == $searchData['state'])
                                    $selected = 'selected';
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $k ?>"><?php echo $val ?></option>
                            <?php } ?>
                        </select>
                        <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                    </div>
                </div>
                <?php endif; ?>
                <!--end state -->

                <!--- start city-->
                <div class="el-blk">
                    <div class="select-bkl">
                        <select class="chosen" name="city" id="org-city-select">
                            <?php echo Apollo_App::renderCities($listCityState, $searchData['city']); ?>
                        </select>
                        <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                    </div>
                </div>
                <!---end city -->

                <!--start zip -->
                <?php if($zipOption) : ?>
                <div class="el-blk">
                    <div class="select-bkl">
                        <select class="chosen" name="zip" id="org-zip-select" >
                            <?php foreach($zipList as $k => $zipItem){
                                $selected = '';
                                if($k == $searchData['zip'])
                                    $selected = 'selected';
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $k; ?>"><?php echo $zipItem; ?></option>
                            <?php } ?>


                        </select>
                        <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                    </div>
                </div>
                <?php endif; ?>
                <!--End zip -->

                <!--Region -->
                <?php if($regionOption && $regionList ): ?>
                    <div class="el-blk">
                        <div class="select-bkl">
                            <select class="chosen" name="region" data-refer-to="select[data-role=zip]" data-role="region" >
                                <?php foreach($regionList as $k => $val){
                                    $selected = '';
                                    if($k == $searchData['region'])
                                        $selected = 'selected';
                                    if(!empty($val)) {
                                    ?>
                                    <option <?php echo $selected; ?> value="<?php echo $k ?>"><?php echo $val ?></option>
                                <?php } } ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="sub-blk">
                    <button type="submit" class="btn btn-l lgr fr f-w-btn-search"><?php _e('SEARCH', 'apollo') ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
