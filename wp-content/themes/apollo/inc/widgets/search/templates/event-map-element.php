<?php $currentTheme = function_exists('apl_sm_get_current_active_theme') ? apl_sm_get_current_active_theme() : '';
if ($currentTheme == 'octave-child') :
    $mapZoom = of_get_option(OC_Common_Const::_OC_GOOGLE_MAP_ZOOM, 12);
    $minLat = isset($_GET['min_lat'])?$_GET['min_lat']:'';
    $minLng = isset($_GET['min_lng'])?$_GET['min_lng']:'';
    $maxLat = isset($_GET['max_lat'])?$_GET['max_lat']:'';
    $maxLng = isset($_GET['max_lng'])?$_GET['max_lng']:'';
    ?>
    <div class="search-bkl wrap-search-map-event map-event-list hidden" style="opacity: 0; position: absolute; bottom: -10px">
        <nav class="search-map--event">
            <div id="map-Event-list" class="blk-map-content" data-map-zoom="<?php echo $mapZoom; ?>"></div>
            <div class="blk-maps-events-filter">
                <div class="maps-events-filter--view" data-min-lat="" data-max-lat="" data-min-lng=""
                     data-max-lng=""></div>
            </div>
        </nav>
    </div>
    <input type="hidden" value="<?php echo $minLat?>" name="min_lat">
    <input type="hidden" value="<?php echo $minLng?>" name="min_lng">
    <input type="hidden" value="<?php echo $maxLat?>" name="max_lat">
    <input type="hidden" value="<?php echo $maxLng?>" name="max_lng">
<?php endif; ?>