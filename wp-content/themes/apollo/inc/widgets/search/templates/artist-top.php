    <!-- Override your template here -->
<?php

$rq_artist_medium = isset($_GET['artist_medium']) ? $_GET['artist_medium'] : '';
$rq_artist_style = isset($_GET['artist_style']) ? $_GET['artist_style'] : '';


$rq_term = isset($_GET['term']) ? $_GET['term'] : '';
$rq_keyword = isset($_GET['keyword']) ?Apollo_App::clean_data_request( $_GET['keyword'] ) : '';

$title = isset( $instance['title'] ) && $instance['title'] ? $instance['title'] : __('Find An Artist', 'apollo');


$searchData = array(
    'city' => isset($_GET['city'])? urldecode($_GET['city']) : of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY,''),
    'zip' => isset($_GET['zip'])?urldecode($_GET['zip']):'',
);

$cityList = Apollo_App::getCityByTerritory(false, false, true);
$zipList = Apollo_App::getZipByTerritory(false, false, false, true);
$listCityState = Apollo_App::getCityStateByTerritory();

$isTopSearchMobileVersion = isset($_GET['top-search-mobile']) && !empty($_GET['top-search-mobile']) ? 'ts-mobile' : '';

$zipOption = of_get_option(Apollo_DB_Schema::_APL_ENABLE_ARTIST_WIDGET_SEARCH_ZIP, 1);

$enableSearchWdType = of_get_option(APL_Theme_Option_Site_Config_SubTab::ARTIST_ENABLE_TYPE_SEARCH_WIDGET, 1);
$enableSearchWdStyle = of_get_option(APL_Theme_Option_Site_Config_SubTab::ARTIST_ENABLE_STYLE_SEARCH_WIDGET,1);
$enableSearchWdMedium = of_get_option(APL_Theme_Option_Site_Config_SubTab::ARTIST_ENABLE_MEDIUM_SEARCH_WIDGET, 1);

$searchWdTypeLabel = of_get_option(APL_Theme_Option_Site_Config_SubTab::ARTIST_TYPE_SEARCH_WIDGET_LABEL, __("Artist Type", "apollo"));
$searchWdStyleLabel = of_get_option(APL_Theme_Option_Site_Config_SubTab::ARTIST_STYLE_SEARCH_WIDGET_LABEL, __("Artist Style", "apollo"));
$searchWdMediumLabel = of_get_option(APL_Theme_Option_Site_Config_SubTab::ARTIST_MEDIUM_SEARCH_WIDGET_LABEL, __("Artist Medium", "apollo"));
?>
<div class="top-search <?php echo $isTopSearchMobileVersion; ?>">
    <div class="inner">
        <div class="top-search-row artist-s-t-m">
            <form method="get" id="search-artist-m-t" action="<?php echo Apollo_App::getCustomUrlByModuleName(Apollo_DB_Schema::_ARTIST_PT); ?>" class="form-event">
                <button type="button" class="btn btn-l s"><i class="fa fa-search fa-flip-horizontal fa-lg"></i></button>
                <input type="text" name="keyword" value="<?php echo Apollo_App::clean_data_request(get_query_var('keyword'),  true) ?>" placeholder="<?php _e('Search by Keyword', 'apollo') ?>" class="inp inp-txt event-search event-search-custom">
                <input type="hidden" name="do_search" value="<?php echo isset($_GET['do_search']) && !empty($_GET['do_search']) ? 'yes' : 'no'; ?>" />
                <?php
                $artist_types = Apollo_Artist::get_tree_artist_type();

                if(!empty($artist_types) && $enableSearchWdType):
                    ?>
                    <div class="el-blk">
                        <div class="select-bkl">
                            <select name="term" class="chosen" id="artist-type-select">
                                <option value=""><?php echo $searchWdTypeLabel ?></option>
                                <?php Apollo_Artist::build_option_tree($artist_types, $rq_term) ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>
                <?php endif; ?>


                <?php
                $list_artist_style = Apollo_Artist::get_tree_artist_style();

                if(!empty($list_artist_style) && $enableSearchWdStyle):
                    ?>
                    <div class="el-blk">
                        <div class="select-bkl">
                            <select name="artist_style" class="chosen" id="artist-style-select">
                                <option value=""><?php echo $searchWdStyleLabel ?></option>
                                <?php Apollo_Artist::build_option_tree($list_artist_style, $rq_artist_style) ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php
                $list_artist_medium = Apollo_Artist::get_tree_artist_medium();

                if(!empty($list_artist_medium) && $enableSearchWdMedium):
                    ?>
                    <div class="el-blk">
                        <div class="select-bkl">
                            <select name="artist_medium" class="chosen" id="artist-medium-select">
                                <option value=""><?php echo $searchWdMediumLabel ?></option>
                                <?php Apollo_Artist::build_option_tree($list_artist_medium, $rq_artist_medium) ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php
                if(!empty($cityList)):
                    ?>
                    <div class="el-blk">
                        <div class="select-bkl">
                            <select name="city" class="chosen" id="artist-city-select">
                                <?php echo Apollo_App::renderCities($listCityState, $searchData['city']); ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php
                if($zipOption && !empty($zipList)):
                    ?>
                    <div class="el-blk">
                        <div class="select-bkl">
                            <select name="zip" class="chosen" id="artist-zip-select">
                                <?php foreach($zipList as $k => $zip ):
                                    if ( ! $zip ) continue;
                                    ?>
                                    <option value="<?php echo $k ?>" <?php selected($zip, $searchData['zip']) ?>><?php echo $zip ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php
                    /** @Ticket #13823 */
                    echo apollo_search_artist_widget::renderFields(Apollo_DB_Schema::_ARTIST_PT, Apollo_Tables::_APOLLO_ARTIST_META);
                ?>

                <div class="sub-blk">
                    <button type="submit" class="btn btn-l lgr fr f-w-btn-search"><?php _e('SEARCH', 'apollo') ?></button>
                </div>
                <div class="el-blk custom-el-blk"><span class="tt"><?php _e('Search by last name', 'apollo') ?>:</span>
                    <div class="character-board">
                        <?php
                        $cusModName = Apollo_App::getCustomUrlByModuleName(Apollo_DB_Schema::_ARTIST_PT);
                        $artistActiveMemberField = of_get_option(Apollo_DB_Schema::_ARTIST_ACTIVE_MEMBER_FIELD, false);
                        $artistMember = '';
                        if($artistActiveMemberField) {
                            $artistMember = !empty($_GET['artist_member']) ? $_GET['artist_member'] : 'premium';
                            $artistMember = '&artist_member=' . $artistMember;
                        }
                        for( $i = 97 ; $i <= 122; $i++): ?><a <?php if ( isset( $_REQUEST['last_name'] ) && strtolower( $_REQUEST['last_name'] ) == strtolower( chr($i) ) ) echo 'class="active"'; ?> href="<?php echo $cusModName; ?>?last_name=<?php echo chr($i) . $artistMember ?>&do_search=yes"><?php echo strtoupper(chr($i)) ?></a><?php endfor; ?>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>