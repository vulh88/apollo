<!-- Override your template here -->
<?php
    global $apollo_modules;
    $module_name = isset( $apollo_modules[get_post_type()] ) ? $apollo_modules[get_post_type()]['sing'] : '';
    $educators = get_posts( array(
        'post_type' => Apollo_DB_Schema::_EDUCATOR_PT,
        'posts_per_page' => -1,
        'orderby'   => 'post_title',
        'order'          => 'ASC',
    ));
    $title = isset( $instance['title'] ) && $instance['title'] ? $instance['title'] : __('Find Education Programs', 'apollo');
?>

<div class="r-blk r-search widget-search">
<h3 class="r-ttl"> <i class="fa fa-search fa-flip-horizontal fa-lg">     </i><span><?php echo $title; ?></span></h3>
    <form method="" id="search-education" action="<?php echo home_url('program') ?>" class="form-event search-education">

        <input name="view" type="hidden" value="<?php echo isset($_GET['view']) ? $_GET['view'] : '' ?>" />
        <div class="education-search">
            <div class="el-blk full">
                <input value="<?php echo Apollo_App::clean_data_request(get_query_var('keyword'), true) ?>" name="keyword" type="text" placeholder="<?php _e( 'Search by Keyword', 'apollo' ) ?>" class="inp inp-txt">
                <div class="show-tip"><?php _e( 'Search by Keyword', 'apollo' ) ?></div>
            </div>
            <div class="el-blk">
              <div class="select-bkl">
                <select id="educator-provider" name="edu" class="chosen">
                    <option value="0"><?php _e( 'Program Provider', 'apollo' ) ?></option>
                    <?php if ( $educators ):
                        foreach( $educators as $educator ):
                    ?>
                    <option <?php echo "value=".$educator->ID; if ( isset( $_GET['edu'] ) && $_GET['edu'] == $educator->ID ) echo ' selected' ?>><?php echo $educator->post_title ?></option>
                    <?php endforeach; endif; ?>
                </select>
                <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>

              </div>
            </div>

            <?php
                $args = array('hide_empty'   => false);
                $artistic_discipline_label = of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_ARTISTIC_DISCIPLINE_LABEL) ? of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_ARTISTIC_DISCIPLINE_LABEL) : 'Artistic Discipline';
                $taxes = array(
                    'program-type'  => __( 'Program Type', 'apollo' ),
                );

                /** @Task #12782 */
                $bilingual_option = of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_BILINGUAL, true);

                /** @Task #12788 */
                $free_option = of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_FREE, true);


                if (of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_POPULATION_SERVED_DROP) )
                    $taxes[ Apollo_DB_Schema::_APL_PROG_TAX_POP_SER] = __('Population Served', 'apollo');
                if (of_get_option( Apollo_DB_Schema::_EDUCATOR_ENABLE_SUBJECT_DROP) )
                    $taxes[Apollo_DB_Schema::_APL_PROG_TAX_SUBJECT] =  __('Subject', 'apollo');
                if (of_get_option( Apollo_DB_Schema::_EDUCATOR_ENABLE_ARTISTIC_DISCIPLINE_DROP) )
                    $taxes[Apollo_DB_Schema::_APL_PROG_TAX_ART_DESC] = __($artistic_discipline_label, 'apollo');
                if (of_get_option( Apollo_DB_Schema::_EDUCATOR_ENABLE_CULTURAL_ORIGIN_DROP) )
                    $taxes[Apollo_DB_Schema::_APL_PROG_TAX_CUL_ORIGIN] = __('Cultural Origin', 'apollo');
                if ( get_terms(Apollo_DB_Schema::_APL_EDU_TYPE,$args) )
                    $taxes[Apollo_DB_Schema::_APL_EDU_TYPE] = __('Educator Type','apollo');

                 foreach ( $taxes as $tax_key => $tax_label ):
                    $prefix = $tax_key == Apollo_DB_Schema::_APL_PROG_TAX_SUBJECT ? '_' : '';
                    $post_term_key = $prefix.str_replace('-', '_', $tax_key);
                    $default_value = isset( $_GET[$post_term_key] ) ? $_GET[$post_term_key] : '';
                    $tree_term = Apollo_Program::get_tree_terms( $tax_key );
            ?>

            <div class="el-blk">
                <div class="select-bkl">
                    <select name="<?php echo $post_term_key ?>" class="chosen">
                        <option value=""><?php echo $tax_label ?></option>
                        <?php Apollo_Program::build_option_tree($tree_term, $default_value) ?>
                    </select>
                    <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                </div>
            </div>
            <?php endforeach; ?>

            <div class="el-blk"><span class="tt"><?php _e( 'Date range', 'apollo' ) ?></span></div>
            <div class="date-range-blk">
              <label><?php _e( 'Start day', 'apollo' ) ?></label>
              <div class="date-picker">
                  <input value="<?php echo isset( $_GET['sd'] ) ? Apollo_App::clean_data_request($_GET['sd']) : '' ?>" name="sd" type="text" size="12" id="c-s" class="inp inp-txt caldr">
              </div>
            </div>
            <div class="date-range-blk">
              <label><?php _e( 'End day', 'apollo' ) ?></label>
              <div class="date-picker">
                <input value="<?php echo isset( $_GET['ed'] ) ? Apollo_App::clean_data_request($_GET['ed']) : '' ?>" name="ed" type="text" size="12" id="c-e" class="inp inp-txt caldr">
              </div>
            </div>

            <?php if($bilingual_option) : ?>
            <div class="el-blk ">
              <input type="checkbox" name="bil" value="yes" <?php if( isset( $_GET['bil'] ) && $_GET['bil'] == 'yes' ) echo 'checked' ?> />
              <label onclick="checkCBBefore(this)" class="biling"><?php _e( 'Bilingual', 'apollo' ) ?></label>
            </div>
            <?php endif; ?>


            <?php if($free_option) : ?>
            <div class="el-blk ">
              <input type="checkbox" name="free" value="yes" <?php if( isset( $_GET['free'] ) && $_GET['free'] == 'yes' ) echo 'checked' ?> />
              <label onclick="checkCBBefore(this)" class="biling"><?php _e( 'Free', 'apollo' ) ?></label>
            </div>
            <?php endif; ?>

            <?php
            $_have_check = false;
            $_other_fields_str = '';
                $custom_fields = Apollo_Custom_Field::get_field_by_location( Apollo_DB_Schema::_EDUCATOR_PT);
                if ( $custom_fields ):
                    foreach ( $custom_fields as $field ):

                        $k = $field->name;

                        $selected = isset( $_GET[$k] ) && $_GET[$k] == $k ? 'checked' : '';

                        if ( $selected && !$_have_check ) $_have_check = true;

                        $_other_fields_str .= '<li>
                      <input '.$selected.' value="'.$k.'" type="checkbox" name="'.$k.'">
                      <label onclick="checkCBBefore(this)" class="quali">'.$field->label.'</label>
                    </li>';
                    endforeach;
                endif;
                if ( $_other_fields_str ):
            ?>
            <div class="el-blk mrg"><a href="#" class="qualification <?php if ($_have_check) echo 'active' ?>"><?php _e( 'Qualifications', 'apollo' ) ?></a>
              <ul class="quali-list">
                  <?php echo $_other_fields_str ?>
              </ul>
            </div>
            <?php endif; ?>

            <div class="b-btn edu-search-group-button">
                <button type="submit" class="btn btn-b edu-btn"><?php _e( 'SEARCH', 'apollo' ) ?></button>
                <?php
                Apollo_App::includeTemplate(get_stylesheet_directory(). '/inc/widgets/search/includes/reset-button.php', ['idForm' => 'search-education', 'typeResetButton' => 'button']);
                ?>
            </div>
        </div>
    </form>
</div>

<?php
    if ( $_have_check ):
?>
<script>
    jQuery(function() {
        jQuery('.quali-list').show();
    });
</script>
<?php endif; ?>

