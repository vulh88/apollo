<?php

$classifiedCategories = Apollo_Org::get_tree_event_style(Apollo_DB_Schema::_CLASSIFIED);

$searchData = array(
    'keyword' => isset($_GET['keyword'])?Apollo_App::clean_data_request($_GET['keyword']):'',
    'term' => isset($_GET['term'])?Apollo_App::clean_data_request($_GET['term']):'',
    'state' => isset($_GET['state'])?$_GET['state'] : of_get_option(Apollo_DB_Schema::_APL_DEFAULT_STATE,''),
    'city' => isset($_GET['city'])? urldecode($_GET['city']) : of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY,''),
    'region' => isset($_GET['region'])?Apollo_App::clean_data_request($_GET['region']):'',
);

$cityList = Apollo_App::getCityByTerritory(false, false, true);
$stateList = Apollo_App::getStateByTerritory();
$listCityState = Apollo_App::getCityStateByTerritory();
$regionList = Apollo_App::get_regions();

$title = isset( $instance['title'] ) && $instance['title'] ? $instance['title'] : __('Find Classified ','apollo');
?>

<div class="r-blk r-search widget-search">
  <h3 class="r-ttl"> <i class="fa fa-search fa-flip-horizontal fa-lg">     </i><span><?php echo $title ?>  </span></h3>
  <form method="get" id="search-classified" action="<?php echo Apollo_App::getCustomUrlByModuleName(Apollo_DB_Schema::_CLASSIFIED); ?>" class="form-event">
    <div class="organization-search">
      <div class="el-blk full">
        <input type="text" name="keyword" value="<?php echo Apollo_App::clean_data_request(get_query_var('keyword'), true) ?>" placeholder="<?php _e('Search by Keyword ','apollo') ?>" class="inp inp-txt">
        <div class="show-tip"><?php _e('Search by Keyword ','apollo') ?></div>
      </div>

      <!--Categories -->
      <div class="el-blk">
        <div class="select-bkl">
          <select name="term" class="chosen">
            <option value=""><?php _e('Classified Type','apollo') ?></option>
            <?php
            Apollo_Org::build_option_tree($classifiedCategories,$searchData['term']);
            ?>
          </select>
          <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
        </div>
      </div>

       <!--State -->
      <div class="el-blk">
        <div class="select-bkl">
            <select class="chosen" name="state">
              <?php foreach($stateList as $k => $val){
                $selected = '';
                if($k == $searchData['state'])
                  $selected = 'selected';
                ?>
                <option <?php echo $selected; ?> value="<?php echo $k ?>"><?php echo $val ?></option>
              <?php } ?>
            </select>
            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
        </div>
      </div>

      <!--City -->
      <div class="el-blk">
        <div class="select-bkl">
          <select name="city" class="chosen">
              <?php echo Apollo_App::renderCities($listCityState, $searchData['city']); ?>
          </select>
          <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
        </div>
      </div>

        <!-- Author: thienld - add filter region via task #8158 -->
        <?php if( $regionList ): ?>
            <div class="el-blk">
                <div class="select-bkl">
                    <select class="chosen" name="region" data-refer-to="select[data-role=zip]" data-role="region">
                        <?php foreach($regionList as $k => $val){
                            $selected = '';
                            if($k == $searchData['region'])
                                $selected = 'selected';
                            if(!empty($val)) {
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $k ?>"><?php echo $val ?></option>
                            <?php } } ?>
                    </select>
                    <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                </div>
            </div>
        <?php endif; ?>
        <!-- end region -->

      <div class="b-btn bt">
        <input type="submit" class="btn btn-b edu-btn" value="<?php _e('SEARCH','apollo') ?>">
        <?php
          Apollo_App::includeTemplate(get_stylesheet_directory(). '/inc/widgets/search/includes/reset-button.php', ['idForm' => 'search-classified', 'typeResetButton' => 'input']);
        ?>
      </div>
    </div>
  </form>
</div>