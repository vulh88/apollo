<?php 
    
    $searchData = array(
        'keyword' => isset($_GET['keyword'])?Apollo_App::clean_data_request($_GET['keyword']):'',
        '_name' => isset($_GET['_name'])?Apollo_App::clean_data_request($_GET['_name']):'',
        'medium' => isset($_GET['medium'])?Apollo_App::clean_data_request($_GET['medium']):'',
        'term' => isset($_GET['term'])?Apollo_App::clean_data_request($_GET['term']):'',
        'location' => isset($_GET['location'])?Apollo_App::clean_data_request($_GET['location']):'',
        'collection' => isset($_GET['collection'])?Apollo_App::clean_data_request($_GET['collection']):'',
        'zip'  => isset($_GET['zip'])?Apollo_App::clean_data_request($_GET['zip']):'',  
        'city'  => isset($_GET['zip'])?Apollo_App::clean_data_request($_GET['city']):of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY,''),
        '_artist'  => isset($_GET['_artist'])?Apollo_App::clean_data_request($_GET['_artist']):'',
        '_alpha'  => isset($_GET['_alpha'])?Apollo_App::clean_data_request($_GET['_alpha']):'',
    );
    $pub_art = new Apollo_Public_Art( false, Apollo_DB_Schema::_PUBLIC_ART_PT);
    $artists = $pub_art->getArtistPublicArt();

    $cityList = Apollo_App::getCityByTerritory(false, '', true);
    $zipList = Apollo_App::getZipByTerritory(false, false, $searchData['city'], true);
    $listCityState = Apollo_App::getCityStateByTerritory();

    $title = isset( $instance['title'] ) && $instance['title'] ? $instance['title'] : __('Find Public Art','apollo');
    $zipOption = of_get_option(Apollo_DB_Schema::_APL_PUBLIC_ART_WIDGET_SEARCH_ENABLE_ZIP, 1);
?>

<div class="r-blk r-search widget-search">
    <h3 class="r-ttl"> <i class="fa fa-search fa-flip-horizontal fa-lg"></i>
        <span><?php echo $title; ?></span></h3>
    <form method="get" id="search-public-art" action="<?php echo home_url(Apollo_DB_Schema::_PUBLIC_ART_PT) ?>" class="form-event">
        <div class="artist-search">
            <div class="el-blk full">      
                <input value="<?php echo Apollo_App::clean_data_request(get_query_var('keyword'), true) ?>" type="text" name="keyword" placeholder="<?php _e('Search by Keyword', 'apollo') ?>" class="inp inp-txt">
                <div class="show-tip"><?php _e('Search by Keyword', 'apollo') ?></div>
            </div>
            
            <?php
                $artNames = $ar = Apollo_Public_Art::get_list_artnames();
                if ( $artNames ):
            ?>
            <div class="el-blk">
              <div class="select-bkl">
                <select name="_name" class="chosen">
                    <option value="0"><?php _e('Public Art Name', 'apollo') ?></option>
                    <?php foreach($artNames as $artName):
                            $selected = '';
                            if($artName['post_title'] == $searchData['_name']){
                                $selected = 'selected';
                            }
                        ?>
                    <option <?php echo $selected; ?> value="<?php echo $artName['post_title'] ?>"><?php echo $artName['post_title'] ?></option>
                    <?php endforeach; ?>
                </select>
                <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
              </div>
            </div>
            <?php endif; ?>

            <?php if ($publicArtTypes = Apollo_Public_Art::get_tree_event_style(Apollo_DB_Schema::_PUBLIC_ART_PT, true)): ?>
            <div class="el-blk">
              <div class="select-bkl">
                <select name="term" class="chosen">
                    <option value="0"><?php _e('Public Art Type', 'apollo') ?></option>
                    <?php
                        Apollo_Public_Art::build_option_tree($publicArtTypes,$searchData['term']);
                    ?>
                </select>
                <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
              </div>
            </div>
            <?php endif; ?>

            <?php if ($mediums = Apollo_Public_Art::get_tree_medium(true)): ?>
            <div class="el-blk">
              <div class="select-bkl">
                <select name="medium" class="chosen">
                    <option value="0"><?php _e('Medium', 'apollo') ?></option>
                    <?php
                        Apollo_Org::build_option_tree($mediums, isset($searchData['medium']) ? $searchData['medium'] : '');
                    ?>
                </select>
                <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
              </div>
            </div>
            <?php endif; ?>

            <?php 
            if ( $artists ):
            ?>
            <div class="el-blk">
              <div class="select-bkl">
                <select name="_artist" class="chosen">
                    <option value="0"><?php _e('Artist', 'apollo') ?></option>
                    <?php
                    foreach( $artists as $artist ) {
                    ?>
                    <option <?php selected($searchData['_artist'], $artist->ID) ?> value="<?php echo $artist->ID ?>"><?php echo $artist->post_title ?></option>
                    <?php } ?>
                </select>
                <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
              </div>
            </div>
            
            <?php
            endif;
            
            if(!empty($cityList)):
                ?>
                <div class="el-blk">
                    <div class="select-bkl">
                        <select name="city" class="chosen apl-territory-city">
                            <?php echo Apollo_App::renderCities($listCityState, $searchData['city']); ?>
                        </select>
                        <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                    </div>
                </div>
            <?php endif; ?>
            
            <?php
            if($zipOption && !empty($zipList)):
                ?>
                <div class="el-blk" id="public-art-zip-container">
                    <div class="select-bkl">
                        <select name="zip" class="chosen apl-territory-zipcode">
                            <?php foreach($zipList as $k => $zip ):
                               
                                ?>
                                <option value="<?php echo $k ?>" <?php selected($k,  $searchData['zip']) ?>><?php echo $zip ?></option>
                            <?php endforeach; ?>
                        </select>
                        <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                    </div>
                </div>
            <?php endif; ?>

            <?php
            if ($locations = Apollo_Public_Art::get_tree_location(true)):
            ?>
            <div class="el-blk">
              <div class="select-bkl">
                <select name="location">
                    <option value="0"><?php _e('Location', 'apollo') ?></option>
                    <?php
                        Apollo_Org::build_option_tree($locations, $searchData['location']);
                    ?>
                </select>
                <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
              </div>
            </div>
            <?php endif; ?>

            <?php
            if ($collection = Apollo_Public_Art::get_tree_collection(true)):
            ?>
            <div class="el-blk">
              <div class="select-bkl">
                <select name="collection">
                    <option value="0"><?php _e('Collection', 'apollo') ?></option>
                    <?php
                        Apollo_Org::build_option_tree($collection, $searchData['collection']);
                    ?>
                </select>
                <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
              </div>
            </div>
            <?php endif; ?>

            <div class="b-btn bt">
                <button type="submit" class="btn btn-b search-btn"><?php _e('SEARCH', 'apollo') ?></button>
                <?php
                Apollo_App::includeTemplate(get_stylesheet_directory(). '/inc/widgets/search/includes/reset-button.php', ['idForm' => 'search-public-art', 'typeResetButton' => 'button']);
                ?>
            </div>
            <div class="el-blk"><span class="tt"><?php _e('Search by alpha name:', 'apollo') ?></span>
                <div class="character-board">
                    <?php
                    for( $i = 97 ; $i <= 122; $i++): ?><a <?php if ( isset( $_REQUEST['_alpha'] ) && strtolower( $_REQUEST['_alpha'] ) == strtolower( chr($i) ) ) echo 'class="active"'; ?> href="<?php echo home_url(Apollo_DB_Schema::_PUBLIC_ART_PT) ?>?_alpha=<?php echo chr($i) ?>"><?php echo strtoupper(chr($i)) ?></a><?php endfor; ?>
                </div>
            </div>
        </div>
    </form>
  </div>