<!-- Override your template here -->
<?php
    global $apollo_modules;
    Apollo_Venue::updateAllVenueForSearch();
    $module_name = isset( $apollo_modules[get_post_type()] ) ? $apollo_modules[get_post_type()]['sing'] : '';
    $listState = Apollo_App::getStateByTerritory();
    $venueCategories = Apollo_Org::get_tree_event_style(Apollo_DB_Schema::_VENUE_PT);
    $searchData = array(
        'keyword' => isset($_GET['keyword'])?Apollo_App::clean_data_request($_GET['keyword']):'',
        'term' => isset($_GET['term'])?Apollo_App::clean_data_request($_GET['term']):'',
        'state' => isset($_GET['state'])?$_GET['state'] : of_get_option(Apollo_DB_Schema::_APL_DEFAULT_STATE,''),
        'city' => isset($_GET['city'])? urldecode($_GET['city']): of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY,''),
        'zip' => isset($_GET['zip'])?urldecode($_GET['zip']):'',
        'custom' => isset($_GET['custom'])?$_GET['custom']:array(),
        'region' => isset($_GET['region'])?urldecode($_GET['region']):'',
    );

    $cityList = Apollo_App::getCityByTerritory(false, false, true);
    $zipList = Apollo_App::getZipByTerritory(false,false, false, true);
    $regionList = Apollo_App::get_regions();
    
    $listCityState = Apollo_App::getCityStateByTerritory();

    $title = isset( $instance['title'] ) && $instance['title'] ? $instance['title'] : __('Find A Venue ','apollo');
    $regionOption = of_get_option(Apollo_DB_Schema::_APL_VENUE_WIDGET_SEARCH_ENABLE_REGION, 1);
    $stateOption = of_get_option(Apollo_DB_Schema::_APL_VENUE_WIDGET_SEARCH_ENABLE_STATE, 1);
    $zipOption = of_get_option(Apollo_DB_Schema::_APL_VENUE_WIDGET_SEARCH_ENABLE_ZIP, 1);

    /**
     * @author vulh
     * @ticket #19027 - 0002522: wpdev54 Customization - Custom venue slug
     */
    $venueTypeLabel = of_get_option(APL_Theme_Option_Site_Config_SubTab::VENUE_WIDGET_SEARCH_VENUE_TYPE_LABEL, __('Venue Type','apollo'));

?>


    <div class="r-blk r-search widget-search">
        <h3 class="r-ttl"> <i class="fa fa-search fa-flip-horizontal fa-lg">     </i><span><?php echo $title ?> </span></h3>
        <form method="" id="search-venue" action="<?php echo Apollo_App::getCustomUrlByModuleName('venue') ?>" class="form-event">
            <div class="organization-search">
                <div class="el-blk full">
                    <input type="text" name="keyword" placeholder="<?php _e('Search by Keyword','apollo') ?>" class="inp inp-txt" value="<?php echo Apollo_App::clean_data_request(get_query_var('keyword'), true) ?>">
                    <div class="show-tip"><?php _e('Search by Keyword','apollo') ?></div>
                </div>
                <div class="el-blk">
                    <div class="select-bkl">
                        <select name="term" class="chosen">
                            <option value=""><?php echo $venueTypeLabel ?></option>
                            <?php
                            Apollo_Org::build_option_tree($venueCategories,$searchData['term']);
                            ?>
                        </select>
                        <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                    </div>
                </div>

                <!--state -->
                <?php if($stateOption) : ?>
                <div class="el-blk">
                    <div class="select-bkl">
                        <select class="chosen" name="state">
                            <?php foreach($listState as $k => $val){ ?>
                                <option <?php selected($k, $searchData['state']) ?> value="<?php echo $k ?>"><?php echo $val ?></option>
                            <?php } ?>
                        </select>
                        <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                    </div>
                </div>
                <?php endif; ?>
                <!--end state -->
                
                <div class="el-blk">
                    <div class="select-bkl">
                        <select name="city" class="chosen">
                            <?php echo Apollo_App::renderCities($listCityState, $searchData['city']); ?>
                        </select>
                        <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                    </div>
                </div>

                <?php if (!Apollo_App::enableMappingRegionZipSelection()): ?>
                <!--start zip -->
                <?php if($zipOption) : ?>
                <div class="el-blk">
                    <div class="select-bkl">
                        <select class="chosen" name="zip">

                            <?php foreach($zipList as $k => $zipItem){
                                $selected = '';

                                if($k == $searchData['zip'])
                                    $selected = 'selected';
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $k; ?>"><?php echo $zipItem; ?></option>
                            <?php } ?>


                        </select>
                        <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                    </div>
                </div>
                <?php endif; ?>
                <!--End zip -->
                <?php endif; ?>

                <!--Region -->
                <?php if($regionOption && $regionList ): ?>
                <div class="el-blk">
                    <div class="select-bkl">
                        <select class="chosen" name="region" data-refer-to="select[data-role=zip]" data-role="region">
                            <?php foreach($regionList as $k => $val){
                                $selected = '';
                                if($k == $searchData['region'])
                                    $selected = 'selected';
                                if( !empty($val)) {
                                ?>
                                <option <?php echo $selected; ?> value="<?php echo $k ?>"><?php echo $val ?></option>
                            <?php } } ?>
                        </select>
                        <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                    </div>
                </div>
                <?php endif; ?>

                <div class="el-blk">
<!--                    <p>--><?php //_e('Accessibility Information','apollo')?><!--</p>-->
                    <?php
                    global $apollo_event_access;
                    ?>
                    <div class="accessibilyty-icons-search">
                        <dl class="accessibilyty-dropdown">
                            <dt>
                                <a href="javascript:void(0)">
                                    <span class="access-drop" default-text="<?php _e('Select Accessibility ', 'apollo');?>" selected-text="<?php _e( 'selected', 'apollo' ); ?>"><?php _e('Select Accessibility ', 'apollo');?></span>
                                    <p class="multiSel"> </a>
                            </dt>
                            <dd>
                                <div class="mutliSelect right" id="venue-accessibility-checkbox">
                                    <ul>
                                        <?php

                                        foreach($apollo_event_access as $img=>$label){
                                            $checked = '';
                                            if(is_array($searchData['custom']) && count($searchData['custom'])){
                                                if(in_array($img,$searchData['custom'])){
                                                    $checked ='checked';
                                                }
                                            }
                                            ?>
                                            <li>
                                <input <?php echo $checked; ?> type="checkbox" value="<?php echo $img ?>" name="custom[]" access-name="<?php echo $label; ?>" class="apl-accessibility-item">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/images/event-accessibility/2x/<?php echo $img ?>.png">
                                 <label><?php echo $label; ?></label></span>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </dd>
                        </dl>
                    </div>
                    <div class="b-btn bt">
                        <input type="submit" class="btn btn-b edu-btn" value="<?php _e('SEARCH','apollo') ?>">
                        <?php
                        Apollo_App::includeTemplate(get_stylesheet_directory(). '/inc/widgets/search/includes/reset-button.php', ['idForm' => 'search-venue', 'typeResetButton' => 'input']);
                        ?>
                    </div>
                </div>

            </div>
        </form>
    </div>
