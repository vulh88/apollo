<?php
global $post;

$isTopSearchMobileVersion = isset($_GET['top-search-mobile']) && !empty($_GET['top-search-mobile']) ? 'ts-mobile' : '';
$enableFeatureSec = of_get_option(APL_Business_Module_Theme_Option::_BUSINESS_ENABLE_FEATURE_SEC, 1);

$searchData = array(
    'city' => isset($_GET['city'])? urldecode($_GET['city']) : of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY,''),
    'region' => isset($_GET['region'])?$_GET['region']:'',
    'keyword' => isset($_GET['keyword'])?Apollo_App::clean_data_request($_GET['keyword']):'',
    'selected-bs-type' => isset($_GET['term'])? urldecode(Apollo_App::clean_data_request($_GET['term'])) : ''
);

/*get current term */
if (empty($_GET['term']) && get_query_var( 'taxonomy' )){
    $currentTerm = Apollo_App::getCurrentTerm();
    $searchData['selected-bs-type'] = $currentTerm ? $currentTerm->term_id : '';
}

$listRegions = Apollo_App::get_regions(false,false);
$firstLabelOpt = __('Select Location', 'apollo');
$cityList = Apollo_App::getCityByTerritory(false, false, true, $firstLabelOpt);

/** @Ticket #13228 */
$featuresLabel = of_get_option(Apollo_DB_Schema::_APL_BUSINESS_SEARCH_WIDGET_LABEL_FEATURES, __( 'Features', 'apollo'));
$openFeaturesList = of_get_option(Apollo_DB_Schema::_APL_BUSINESS_SEARCH_WIDGET_OPEN_LIST_FEATURES, false);

/**
 * @ticket #15928 - huyenln
 * Listing: Only show dining service when the current category is dining
 * Detail page: only show when the business selected dining cat or is a restaurant
 */
$isRestaurant = 'yes'; // Init value set yes due to this widget is available for both listing and detail page
if (is_single()){

    /**
     * If a business is restaurant OR has selected dining category, display the dining feature
     */
    $orgID = get_apollo_meta($post->ID,Apollo_DB_Schema::_APL_BUSINESS_ORG, true);
    $isRestaurant = get_apollo_meta($orgID,Apollo_DB_Schema::_ORG_RESTAURANT,true);
}

$GLOBALS['isRestaurant'] = $isRestaurant;
$selectedVal = of_get_option(APL_Business_Module_Theme_Option::DINING_SELECT);
$keywordPlaceHolder = of_get_option(Apollo_DB_Schema::_APL_BUSINESS_SEARCH_KEYWORD_PLACEHOLDER, __('Search by Keyword', 'apollo'));
?>
<div class="top-search <?php echo $isTopSearchMobileVersion; ?>">
    <div class="inner">
        <div class="top-search-row business-s-t-m">
            <form method="get" id="search-business-m-t" action="<?php echo home_url('/' . Apollo_DB_Schema::_BUSINESS_PT); ?>" class="form-event">
                <button type="button" class="btn btn-l s"><i class="fa fa-search fa-flip-horizontal fa-lg"></i></button>
                <input type="text" name="keyword" value="<?php echo Apollo_App::clean_data_request(get_query_var('keyword'), true) ?>" placeholder="<?php echo $keywordPlaceHolder; ?>" class="inp inp-txt event-search event-search-custom">
                <input type="hidden" name="do_search" value="<?php echo isset($_GET['do_search']) && !empty($_GET['do_search']) ? 'yes' : 'no'; ?>" />

                <!-- Thienld : start rendering search input -->
                <?php
                // business type dropdownlist
                $bsType = APL_Business_Function::getTreeBusinessType();
                if(!empty($bsType)):
                    ?>
                    <div class="el-blk">
                        <div class="select-bkl">
                            <select name="term" class="chosen" id="business-type-select" >
                                <option value=""><?php _e( 'Select Category', 'apollo' ) ?></option>
                                <?php APL_Business_Function::build_option_tree($bsType, $searchData['selected-bs-type'], Apollo_Display_Config::TREE_MAX_LEVEL, array(), $selectedVal); ?>
                            </select>
                            <input type="hidden" class="is_dining_cate" name="is_dining_cate" value="0"/>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php
                if(of_get_option(Apollo_DB_Schema::_ENABLE_BUSINESS_SEARCH_CITY , 1) && !empty($cityList) && count($cityList) > 1):
                    ?>
                    <div class="el-blk">
                        <div class="select-bkl">
                            <select name="city" class="chosen apl-territory-city ">
                                <?php
                                foreach($cityList as $k => $cityItem): ?>
                                    <option <?php echo !empty($searchData['city']) && $searchData['city'] == $cityItem ? 'selected' : '' ?> value="<?php echo $k ?>"><?php echo $cityItem; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>
                <?php endif; ?>

                <!-- show dropdownlist regions -->
                <?php
                // Disable if this site is enable Mapping Region and Zip Code
                if( of_get_option(Apollo_DB_Schema::_ENABLE_BUSINESS_SEARCH_REGION , 1) && $listRegions ): ?>
                    <div class="el-blk">
                        <div class=" select-bkl">
                            <select name="region" id="event-region-select">
                                <option value=""><?php echo __('Select Region','apollo'); ?></option>
                                <?php
                                if($listRegions){
                                    foreach($listRegions as $region){
                                        $selected = '';
                                        if($region == $searchData['region'])
                                            $selected = 'selected';
                                        if(!empty($region)) {
                                            ?>
                                            <option <?php echo $selected; ?> value="<?php echo $region ?>">
                                                <?php echo $region; ?>
                                            </option>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>
                <?php endif; ?>
                <!-- end show dropdownlist regions -->

                <?php

                /** @Ticket #13237 - Filter by price */
                echo apollo_search_business_widget::renderFields(Apollo_DB_Schema::_BUSINESS_PT, Apollo_Tables::_APOLLO_BUSINESS_META);

                // features list
                $selectedItemsCounter = 0;
                $services = Apollo_Business::getFieldBSServicesMetaData(Apollo_DB_Schema::_BUSINESS_PT);
                $filteredServices = isset($_GET['business_service']) ? $_GET['business_service'] : array();
                $checkboxListItems = '';
                if ( $services ) :
                    foreach ( $services as $sKey => $sVal ) :
                        $selected = in_array($sKey,$filteredServices);
                        if ( $selected ) {
                            $selectedItemsCounter += 1;
                            $checked = 'checked="checked"';
                        } else {
                            $checked = '';
                        }
                        $checkboxListItems .= '
                    <li>
                      <input type="checkbox" name="business_service[]" id="'.$sKey.'" '.$checked.' value="'.$sKey.'" />
                      <label for="'.$sKey.'" class="bs-sv-cb">'.$sVal.'</label>
                    </li>';
                    endforeach;
                endif;
                ?>
                <?php if ( $checkboxListItems && $enableFeatureSec ) :
                    /** @Ticket #13228 */
                    $activeFeaturesList = '';
                    if ($openFeaturesList) {
                        $activeFeaturesList = 'active';
                    } else if($selectedItemsCounter > 0) {
                        $activeFeaturesList = 'active';
                    }
                    ?>
                    <div class="el-blk mrg custom-el-blk fl-o-mb apl-business-dining-services hidden <?php echo (is_single() && $isRestaurant == 'yes') ? 'is-restaurant' : '';?> <?php echo is_single() ? 'is-single' : ''; ?>">
                        <a href="#" class="qualification <?php echo $activeFeaturesList; ?>"><?php echo $featuresLabel; ?></a>
                        <ul class="quali-list" id="business-service-checkboxes">
                            <?php echo $checkboxListItems; ?>
                        </ul>
                    </div>
                <?php endif; ?>

                <!-- Thienld : end rendering search input -->

                <div class="sub-blk">
                    <button type="submit" class="btn btn-l lgr fr f-w-btn-search"><?php _e('SEARCH', 'apollo') ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
