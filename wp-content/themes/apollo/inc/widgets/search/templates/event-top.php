<?php
    $title = isset( $instance['title'] ) && $instance['title'] ? $instance['title'] : __('Find An Event', 'apollo');
    //get category for event

    $isAvaiableVenueModule =  Apollo_App::is_avaiable_module( Apollo_DB_Schema::_VENUE_PT );
    $isAvaiableOrgModule =  Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ORGANIZATION_PT );

    $venueSelect2 = apl_instance('APL_Lib_Helpers_Select2', array(
        'post_type' => Apollo_DB_Schema::_VENUE_PT,
        'selected_item' => isset($_GET['event_location'])?$_GET['event_location']:'',
    ));
    $eventVenues = $venueSelect2->getItemsInPostTypeToDropDownList();

    $orgSelect2 = apl_instance('APL_Lib_Helpers_Select2', array(
        'post_type' => Apollo_DB_Schema::_ORGANIZATION_PT,
        'selected_item' => isset($_GET['event_org'])?$_GET['event_org']:'',
    ));
    $eventOrgs = $orgSelect2->getItemsInPostTypeToDropDownList();


    $enableDate = of_get_option(Apollo_DB_Schema::_ENABLE_EVENT_SEARCH_DATE, 1);
    $listCities = Apollo_App::getCityByTerritory(false,false,false);
    $listRegions = Apollo_App::get_regions(false,false);
    $searchData = array(
        'keyword' => isset($_GET['keyword'])?$_GET['keyword']:'',
        'start_date' => isset($_GET['start_date'])?$_GET['start_date']:'',
        'end_date' => isset($_GET['end_date'])?$_GET['end_date']:'',
        'term' => isset($_GET['term'])?$_GET['term']:'',
        'event_location' => isset($_GET['event_location'])?$_GET['event_location']:'',
        'event_org' => isset($_GET['event_org'])?$_GET['event_org']:'',
        'view' => isset($_GET['view'])?$_GET['view']:'',
        'save_lst_list' => isset($_GET['save_lst_list'])?$_GET['save_lst_list']:'',
        'is_discount' => isset($_GET['is_discount']) ? $_GET['is_discount'] :  0,
        'by_my_location' => isset($_GET['by_my_location']) ? $_GET['by_my_location'] :  0,
        'city' => isset($_GET['event_city'])? urldecode($_GET['event_city']) :  urldecode(of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY,'')),
        'region' => isset($_GET['event_region'])?$_GET['event_region']:'',
        'accessibility' => isset($_GET['accessibility']) ? $_GET['accessibility'] : array(),
        'neighborhood' => isset($_GET['event_neighborhood']) ? $_GET['event_neighborhood'] :  '',
        'apl-lat' => isset($_GET['apl-lat']) ? $_GET['apl-lat'] : '',
        'apl-lng' => isset($_GET['apl-lng']) ? $_GET['apl-lng'] : '',
    );
    
    $listCityState = Apollo_App::getCityStateByTerritory();
    $isTopSearchMobileVersion = isset($_GET['top-search-mobile']) && !empty($_GET['top-search-mobile']) ? 'ts-mobile' : '';
    $isTopSearchMobileHorVersion = isset($_GET['top-search-mobile-hor']) && !empty($_GET['top-search-mobile-hor']) ? 'ts-mobile-hor' : '';
    /*@ticket: #17283 */
    $discountLabel = of_get_option(Apollo_DB_Schema::_TEXT_OF_DISCOUNT_OFFER,Apollo_Display_Config::_TEXT_OF_DISCOUNT_OFFER);
    $textSearchKeyword = of_get_option(APL_Theme_Option_Site_Config_SubTab::_APL_EVENT_TEXT_OF_KEYWORD_SEARCH, __('Search by Keyword', 'apollo'));

?>

<div class="top-search apl-event-top-search <?php echo $isTopSearchMobileVersion; ?> <?php echo $isTopSearchMobileHorVersion; ?>">
    <div class="inner">
        <div class="top-search-row">
            <form method="get" id="search-event-m-t" action="<?php echo home_url() ?>/event/" class="form-event">
                <button type="button" class="btn btn-l s" style="overflow: hidden" ><i class="fa fa-search fa-flip-horizontal fa-lg" style="width: 35px; height: 35px; line-height: 30px;"></i><?php _e( 'search' , 'apollo' ); ?></button>

                <div class="" id="search-auto-complate-append-here-header">
                    <label for="search-by-keyword"  class="hidden" ><?php echo $textSearchKeyword; ?></label>
                    <input id="search-by-keyword"  data-parent-append="#search-auto-complate-append-here-header" type="text" name="keyword" value="<?php echo Apollo_App::clean_data_request(get_query_var('keyword'), true) ?>" placeholder="<?php echo $textSearchKeyword; ?>" class="solr-search-event-widget inp inp-txt event-search event-search-custom event-search-custom-horizontal ">
                </div>

                <input type="hidden" name="do_search" value="<?php echo isset($_GET['do_search']) && !empty($_GET['do_search']) ? 'yes' : 'no'; ?>" />

                <div class="el-blk">
                    <div class="select-bkl">
                        <label for="event-category-select" class="hidden" ><?php _e('Select Category', 'apollo'); ?></label>
                        <select class="" name="term" id="event-category-select">
                            <option value=""><?php echo _e('Select Category','apollo'); ?></option>
                            <?php
                                echo apollo_search_event_widget::handleEventCategoryDropDown();
                            ?>
                        </select>
                        <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                    </div>
                </div>

                <?php if (of_get_option(Apollo_DB_Schema::_ENABLE_EVENT_SEARCH_ORG, 0) && $isAvaiableOrgModule): ?>
                    <div class="el-blk">

                        <div class=" select-bkl">
                            <label for="event-org-sel-hor" class="hidden" ><?php _e('Select Organization:', 'apollo'); ?></label>
                            <select name="event_org" id="event-org-sel-hor"
                                    class="apl_select2"
                                    data-selected-id="<?php echo isset($searchData['event_org']) ? $searchData['event_org'] : ''; ?>"
                                    data-enable-remote="<?php echo $orgSelect2->getEnableRemote() ? 1 : 0; ?>"
                                    data-post-type="<?php echo Apollo_DB_Schema::_ORGANIZATION_PT ?>"
                                    data-default-option="<?php _e('Select Organization','apollo') ?>"
                                    data-source-url="apollo_get_remote_data_to_select2_box">
                                <option value=""><?php _e('Select Organization','apollo'); ?></option>
                                <?php
                                if($eventOrgs){
                                    foreach($eventOrgs as $org){
                                        $selected = '';
                                        if($org->ID == $searchData['event_org'])
                                            $selected = 'selected';
                                        ?>
                                        <option <?php echo $selected; ?> value="<?php echo $org->ID ?>">
                                            <?php echo $org->post_title; ?>
                                        </option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>
                <?php endif; ?>


                <?php if (of_get_option(Apollo_DB_Schema::_ENABLE_EVENT_SEARCH_VENUE, 1) && $isAvaiableVenueModule): ?>
                    <div class="el-blk">

                        <div class=" select-bkl">
                            <label for="event-location-sel-hor" class="hidden" ><?php _e('Select Venue:', 'apollo'); ?></label>
                            <select name="event_location" id="event-location-sel-hor"
                                    class="apl_select2"
                                    data-selected-id="<?php echo isset($searchData['event_location']) ? $searchData['event_location'] : ''; ?>"
                                    data-enable-remote="<?php echo $venueSelect2->getEnableRemote() ? 1 : 0; ?>"
                                    data-post-type="<?php echo Apollo_DB_Schema::_VENUE_PT ?>"
                                    data-default-option="<?php _e('Select Venue','apollo') ?>"
                                    data-source-url="apollo_get_remote_data_to_select2_box">
                                <option value=""><?php echo _e('Select Venue','apollo') ?></option>
                                <?php
                                if($eventVenues){
                                    foreach($eventVenues as $eventVenue){
                                        $selected = '';
                                        if($eventVenue->ID == $searchData['event_location'])
                                            $selected = 'selected';
                                        ?>
                                        <option <?php echo $selected; ?> value="<?php echo $eventVenue->ID ?>">
                                            <?php echo $eventVenue->post_title; ?>
                                        </option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php /** @Ticket #12940 */
                if( of_get_option(Apollo_DB_Schema::_ENABLE_EVENT_SEARCH_ACCESSIBILITY, false)): ?>
                    <div class="el-blk accessibility-icon">
                        <?php
                        global $apollo_event_access;
                        ?>
                        <div class="accessibilyty-icons-search">
                            <dl class="accessibilyty-dropdown">
                                <dt>
                                    <a href="javascript:void(0)">
                                        <span class="access-drop" default-text="<?php _e('Select Accessibility ', 'apollo');?>" selected-text="<?php _e( 'selected', 'apollo' ); ?>"><?php _e('Select Accessibility ', 'apollo');?></span>
                                        <p class="multiSel hidden"><?php _e('Select Accessibility ', 'apollo'); ?></a>
                                </dt>
                                <dd>
                                    <div class="mutliSelect top" id="event-accessibility-icons-search">
                                        <ul>
                                            <?php

                                            foreach($apollo_event_access as $img=>$label){
                                                $checked = '';
                                                if(is_array($searchData['accessibility']) && count($searchData['accessibility'])){
                                                    if(in_array($img,$searchData['accessibility'])){
                                                        $checked ='checked';
                                                    }
                                                }
                                                ?>
                                                <li>
                                                    <input <?php echo $checked; ?> type="checkbox" value="<?php echo $img ?>" name="accessibility[]" access-name="<?php echo $label; ?>" class="apl-accessibility-item" >
                                                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/event-accessibility/2x/<?php echo $img ?>.png">
                                                    <label><?php echo $label; ?></label></span>
                                                </li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </dd>
                            </dl>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if( $isAvaiableVenueModule && of_get_option(Apollo_DB_Schema::_ENABLE_EVENT_SEARCH_CITY, 0) && $listCityState): ?>
                    <div class="el-blk">
                        <div class=" select-bkl">
                            <label for="event-city-select" class="hidden" ><?php _e('Select City:', 'apollo'); ?></label>
                            <select name="event_city" id="event-city-select">
                                <?php echo Apollo_App::renderCities($listCityState, $searchData['city']); ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php
                /** @Ticket 16482 - Show dropdown neighborhood */
                if( of_get_option(Apollo_DB_Schema::_ENABLE_EVENT_SEARCH_NEIGHBORHOOD, 0) && $listCityState):
                    if (!empty($searchData['city'])) {
                        $neighborhoodOptions = APL_Lib_Territory_Neighborhood::renderNeighborhoodOnEventSearch($listCityState, $searchData['city'], $searchData['neighborhood']);
                    }
                ?>
                    <div class="el-blk <?php echo empty($searchData['city']) ? 'hidden' : ''; ?>">
                        <?php
                        if($isAvaiableVenueModule){
                            ?>
                            <div class=" select-bkl">
                                <select name="event_neighborhood"
                                        id="event-neighborhood-select"
                                        data-selected="<?php echo $searchData['neighborhood']; ?>"
                                >
                                    <?php echo $neighborhoodOptions; ?>
                                </select>
                                <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                            </div>
                        <?php } ?>
                    </div>
                <?php endif; ?>

                <!-- show dropdownlist regions -->
                <?php
               
                if($isAvaiableVenueModule && of_get_option(Apollo_DB_Schema::_ENABLE_EVENT_SEARCH_REGION , 0) && $listRegions): ?>
                    <div class="el-blk">
                        <div class=" select-bkl">
                            <label for="event-region-select" class="hidden" ><?php _e('Select Region:', 'apollo'); ?></label>
                            <select name="event_region" id="event-region-select" data-refer-to="#event-zipcode-select" data-role="region">
                                <option value=""><?php echo _e('Select Region','apollo'); ?></option>
                                <?php
                                if($listRegions){
                                    foreach($listRegions as $region){
                                        $selected = '';
                                        if($region == $searchData['region'])
                                            $selected = 'selected';
                                        if(!empty($region)) {
                                            ?>
                                            <option <?php echo $selected; ?> value="<?php echo $region ?>">
                                                <?php echo $region; ?>
                                            </option>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>
                <?php endif; ?>
                <!-- end show dropdownlist regions -->
                <?php
                /** @Ticket #19484 */
                $enableDiscountOfferCheckbox = of_get_option(Apollo_DB_Schema::_ENABLE_DISCOUNT_OFFER_CHBOX, 1);

                /** @Ticket #13392 */
                if(of_get_option(Apollo_DB_Schema::_ENABLE_EVENT_SEARCH_FREE_EVENT, false)) : ?>
                <div class="el-blk">
                    <input name="free_event" type="checkbox" class="free-event" value="yes" <?php echo (isset($_GET['free_event']) && $_GET['free_event'] == 'yes') ? 'checked' : '' ; ?>/>
                    <label for="free-event" class="free-evt"><?php _e( 'Free Event', 'apollo' ); ?></label>
                </div>
                <?php endif; ?>

                <!-- @ticket: #17283 -->
                <div class="el-blk-quicksearch">
                    <label>Quick Search</label>
                    <div class="list-quicksearch">
                        <?php if ($enableDiscountOfferCheckbox) : ?>
                        <a class="btn-artpass <?php echo $searchData["is_discount"] ? "active" : ""?>" id="discount-quick-search"><?php echo __($discountLabel, 'apollo') ?></a>
                        <?php endif; ?>
                        <a class="btn-today quick-search-date" id="today-quick-search"><?php _e('Today', 'apollo'); ?></a>
                        <a class="btn-weekend quick-search-date" id="weekend-quick-search"><?php _e('Weekend', 'apollo'); ?></a>
                        <a class="btn-tomorrow quick-search-date" id="tomorrow-quick-search"><?php _e('Tomorrow', 'apollo'); ?></a>
                    </div>
                </div>

                <div class="s-rw cln <?php echo $enableDate == 1 ?  '' : 'hidden' ?>">
                    <div class="calendar-ipt clearfix">
                        <span>
                            <label for="top-c-s" class="hidden" ><?php _e( 'Start', 'apollo' ); ?></label>
                            <input data-date-format="mm-dd-yy" name="start_date" value="<?php echo $searchData['start_date']; ?>" type="text"  size="12" id="top-c-s" placeholder="<?php _e('Start', 'apollo');?>" class="inp inp-txt caldr" autocomplete="off">
                        </span>
                        <span>
                            <label for="top-c-e" class="hidden" ><?php _e( 'End', 'apollo' ); ?></label>
                            <input data-date-format="mm-dd-yy" name="end_date" value="<?php echo $searchData['end_date']; ?>" type="text" size="12" id="top-c-e" placeholder="<?php _e('End', 'apollo');?>" class="inp inp-txt caldr" autocomplete="off">
                        </span>

                        <!-- Ticket #11487: determine current date format -->
                        <input name="date_format" type="hidden" value="m-d-Y">

                        <i class="fa fa-calendar fa-lg ico-s apl-start-icon"></i>
                        <i class="fa fa-calendar fa-lg ico-e apl-start-icon">    </i>
                    </div>
                </div>

                <!-- @ticket #16782: 0002308: Calendar Month search widget - Month view layout -->
                <?php
                /**
                 * @ticket #19484
                 * Fix error quick search event when turn off search date rane
                 */
                $hiddenDateRange = of_get_option('apl_search_by_date_range', true) ? '' : 'oc-hidden-date-range'; ?>
                <div class="s-rw block padding-10 <?php echo $hiddenDateRange?>" id="apl-event-widget-date-block">
                    <span class="lb-range" style="display: block; margin: 0 0 7px 0;"><?php _e('Search by date range', 'apollo') ?>:</span>
                    <div class="lst-lik" id="date-range-top">
                        <a  data-attr="today" class="today select-date <?php if($searchData['save_lst_list'] == 'today') echo 'active' ?> "><?php _e('Today', 'apollo'); ?></a>
                        <a  data-attr="tomorrow" class="tomorrow select-date   <?php if($searchData['save_lst_list'] == 'tomorrow') echo 'active' ?>"><?php _e('Tomorrow', 'apollo'); ?></a>
                        <a  data-attr="weekend" class="weekend select-date  <?php if($searchData['save_lst_list'] == 'weekend') echo 'active' ?>" ><?php _e('Weekend', 'apollo'); ?></a>
                        <span data-attr="next" class="no-line "><?php _e('next', 'apollo'); ?></span>
                        <a  data-attr="seven-day" class="seven select-date  <?php if($searchData['save_lst_list'] == 'seven-day') echo 'active' ?>"><?php _e('7', 'apollo'); ?></a>
                        <a  data-attr="fourteen-day" class="fourteen select-date  <?php if($searchData['save_lst_list'] == 'fourteen-day') echo 'active' ?>"><?php _e('14', 'apollo'); ?></a>
                        <a  data-attr="thirsty-day" class="thirsty select-date <?php if($searchData['save_lst_list'] == 'thirsty-day') echo 'active' ?>"><?php _e('30', 'apollo'); ?></a>
                    </div>
                </div>

                <?php
                if ($enableDiscountOfferCheckbox):
                    ?>
                    <div class="s-rw quick-search-hidden">
                        <label for="is_discount" style="display: none;"><?php _e($discountLabel, 'apollo') ?>:</label>
                        <input id="is_discount" <?php echo $searchData['is_discount'] ? 'checked' : '' ?> name="is_discount"value="1" type="checkbox"><span class="offers"><?php _e( $discountLabel, 'apollo' );  ?></span>
                    </div>
                <?php endif; ?>

                <?php
                /** @Ticket #16603 */
                $filterByMyLocation = '';
                echo apply_filters('octave_filter_by_my_location', $filterByMyLocation, $searchData, true);

                    $resetButton = of_get_option(APL_Theme_Option_Site_Config_SubTab::_ENABLE_RESET_BUTTON_ON_HORIZONTAL_TAB, false);
                ?>
                <div class="wrap-form-action">
                    <div class="sub-blk">
                        <button type="submit" class="btn btn-l lgr fr f-w-btn-search apl-event-submit"><?php _e('SEARCH', 'apollo') ?></button>
                    </div>
                    <?php
                    if ($resetButton) : ?>
                        <div class="sub-blk apl-search-btn-reset">
                            <?php
                            Apollo_App::includeTemplate(get_stylesheet_directory(). '/inc/widgets/search/includes/reset-button.php',
                                ['idForm' => 'search-event-m-t', 'typeResetButton' => 'button', 'class' => 'btn-l lgr fr']);
                            ?>
                        </div>
                    <?php
                    endif;
                    ?>
                </div>
                <input type="hidden" name="view" data-current-view="<?php echo $searchData['view']; ?>" value="<?php echo $searchData['view']; ?>">
                <?php include APOLLO_WIDGETS_DIR . '/search/templates/event-map-element.php' ; ?>
            </form>
        </div>
    </div>
</div>
