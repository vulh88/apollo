<?php

    if (of_get_option(Apollo_DB_Schema::_EVENT_SEARCH_TYPE, 'rgt') == 'rgt'):

    $title = isset( $instance['title'] ) && $instance['title'] ? $instance['title'] : __('Find An Event', 'apollo');
    //get category for event

    $isAvaiableVenueModule =  Apollo_App::is_avaiable_module( Apollo_DB_Schema::_VENUE_PT );
    $isAvaiableOrgModule =  Apollo_App::is_avaiable_module( Apollo_DB_Schema::_ORGANIZATION_PT );

    $venueSelect2 = apl_instance('APL_Lib_Helpers_Select2', array(
        'post_type' => Apollo_DB_Schema::_VENUE_PT,
        'selected_item' => isset($_GET['event_location'])?$_GET['event_location']:'',
    ));
    $eventVenues = $venueSelect2->getItemsInPostTypeToDropDownList();


    $orgSelect2 = apl_instance('APL_Lib_Helpers_Select2', array(
        'post_type' => Apollo_DB_Schema::_ORGANIZATION_PT,
        'selected_item' => isset($_GET['event_org'])?$_GET['event_org']:'',
    ));
    $eventOrgs = $orgSelect2->getItemsInPostTypeToDropDownList();


    $enableDate = of_get_option(Apollo_DB_Schema::_ENABLE_EVENT_SEARCH_DATE, 1);
    $listCities = Apollo_App::getCityByTerritory(false,false,false);
    $listRegions = Apollo_App::get_regions(false,false);
    $searchData = array(
        'keyword' => isset($_GET['keyword'])?$_GET['keyword']:'',
        'start_date' => isset($_GET['start_date'])?$_GET['start_date']:'',
        'end_date' => isset($_GET['end_date'])?$_GET['end_date']:'',
        'term' => isset($_GET['term'])?$_GET['term']:'',
        'event_location' => isset($_GET['event_location'])?$_GET['event_location']:'',
        'event_org' => isset($_GET['event_org'])?$_GET['event_org']:'',
        'view' => isset($_GET['view'])?$_GET['view']:'',
        'save_lst_list' => isset($_GET['save_lst_list'])?$_GET['save_lst_list']:'',
        'city' => isset($_GET['event_city'])? urldecode($_GET['event_city']) :  urldecode(of_get_option(Apollo_DB_Schema::_APL_DEFAULT_CITY,'')),
        'region' => isset($_GET['event_region'])?$_GET['event_region']:'',
        'accessibility' => isset($_GET['accessibility']) ? $_GET['accessibility'] : array(),
        'is_discount' => isset($_GET['is_discount']) ? $_GET['is_discount'] :  0,
        'neighborhood' => isset($_GET['event_neighborhood']) ? $_GET['event_neighborhood'] :  '',
        'by_my_location' => isset($_GET['by_my_location']) ? $_GET['by_my_location'] : '',
        'apl-lat' => isset($_GET['apl-lat']) ? $_GET['apl-lat'] : '',
        'apl-lng' => isset($_GET['apl-lng']) ? $_GET['apl-lng'] : '',
    );
    $listCityState = Apollo_App::getCityStateByTerritory();
    $textSearchKeyword = of_get_option(APL_Theme_Option_Site_Config_SubTab::_APL_EVENT_TEXT_OF_KEYWORD_SEARCH, __('Search by Keyword', 'apollo'));
?>
<div class="r-blk r-search widget-search">
    <h3 class="r-ttl"><i class="fa fa-search fa-flip-horizontal fa-lg"> </i><span><?php echo $title ?>        </span></h3>

    <div class="r-blk-ct">
        <form method="get" id="search-event" action="<?php echo home_url() ?>/event/" class="form-event sf-rsb-widget">

            <div id="search-auto-complate-append-here" class="s-rw">
                <input type="text" data-parent-append="#search-auto-complate-append-here" name="keyword" value="<?php echo Apollo_App::clean_data_request(get_query_var('keyword'), true) ?>" placeholder="<?php echo $textSearchKeyword; ?>" class="inp inp-txt event-search event-search-custom solr-search-event-widget">

            </div>


            <div class="el-blk e-s-wg-c-l <?php echo $enableDate == 1 ?  '' : 'hidden' ?>">
                <label><?php _e('Search by date:', 'apollo') ?></label>

                <div class="calendar-ipt clearfix date-range-blk">
                    <span style="position: relative;" class="custom date-picker">
                        <input data-date-format="mm-dd-yy" name="start_date" value="<?php echo $searchData['start_date']; ?>" type="text"  size="12" id="c-s" placeholder="<?php _e('Start', 'apollo');?>" class="inp inp-txt caldr" autocomplete="off">
                    </span>
                    <span style="position: relative;" class="custom date-picker">
                        <input data-date-format="mm-dd-yy" name="end_date" value="<?php echo $searchData['end_date']; ?>" type="text" size="12" id="c-e" placeholder="<?php _e('End', 'apollo');?>" class="inp inp-txt caldr" autocomplete="off">
                    </span>

                    <!-- Ticket #11487: determine current date format -->
                    <input name="date_format" type="hidden" value="m-d-Y">
                </div>
            </div>
            <div class="el-blk">
                <div class=" select-bkl">
                        <select class="" name="term" id="event-category-select">
                            <option value=""><?php echo __('Select Category','apollo') ?></option>
                            <?php
                                echo apollo_search_event_widget::handleEventCategoryDropDown();
                            ?>
                        </select>
                        <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                </div>
            </div>

            <?php if (of_get_option(Apollo_DB_Schema::_ENABLE_EVENT_SEARCH_ORG, 0) && $isAvaiableOrgModule): ?>
                <div class="el-blk">

                    <div class=" select-bkl">
                        <select name="event_org" id="event-org-sel-rgt"
                                class="apl_select2"
                                data-selected-id="<?php echo isset($searchData['event_org']) ? $searchData['event_org'] : ''; ?>"
                                data-enable-remote="<?php echo $orgSelect2->getEnableRemote() ? 1 : 0; ?>"
                                data-post-type="<?php echo Apollo_DB_Schema::_ORGANIZATION_PT ?>"
                                data-default-option="<?php _e('Select Organization','apollo') ?>"
                                data-source-url="apollo_get_remote_data_to_select2_box">
                            <option value=""><?php _e('Select Organization','apollo'); ?></option>
                            <?php
                            if($eventOrgs){
                                foreach($eventOrgs as $org){
                                    $selected = '';
                                    if($org->ID == $searchData['event_org'])
                                        $selected = 'selected';
                                    ?>
                                    <option <?php echo $selected; ?> value="<?php echo $org->ID ?>">
                                        <?php echo $org->post_title; ?>
                                    </option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if( of_get_option(Apollo_DB_Schema::_ENABLE_EVENT_SEARCH_VENUE, 1)): ?>
            <div class="el-blk">
                <?php
                if($isAvaiableVenueModule){

                    ?>
                    <div class=" select-bkl">
                        <select name="event_location" id="event-location-sel-rgt"
                                class="apl_select2"
                                data-enable-remote="<?php echo $venueSelect2->getEnableRemote() ? 1 : 0; ?>"
                                data-post-type="<?php echo Apollo_DB_Schema::_VENUE_PT ?>"
                                data-default-option="<?php _e('Select Venue','apollo') ?>"
                                data-source-url="apollo_get_remote_data_to_select2_box" >
                            <option value=""><?php echo __('Select Venue','apollo') ?></option>
                            <?php
                            if($eventVenues){
                                foreach($eventVenues as $eventVenue){
                                    $selected = '';
                                    if($eventVenue->ID == $searchData['event_location'])
                                        $selected = 'selected';
                                    ?>
                                    <option <?php echo $selected; ?> value="<?php echo $eventVenue->ID ?>">
                                        <?php echo $eventVenue->post_title; ?>
                                    </option>
                                <?php
                                }
                            }
                            ?>
                        </select>
                        <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                    </div>
                <?php } ?>
            </div>
            <?php endif; ?>

            <?php /** @Ticket #12940 */
            if( of_get_option(Apollo_DB_Schema::_ENABLE_EVENT_SEARCH_ACCESSIBILITY, false)): ?>
            <div class="el-blk">
                <?php
                global $apollo_event_access;
                ?>
                <div class="accessibilyty-icons-search" >
                    <dl class="accessibilyty-dropdown">
                        <dt>
                            <a href="javascript:void(0)">
                                <span class="access-drop" default-text="<?php _e('Select Accessibility ', 'apollo');?>" selected-text="<?php _e( 'selected', 'apollo' ); ?>"><?php _e('Select Accessibility ', 'apollo');?></span>
                                <p class="multiSel hidden"><?php _e('Select Accessibility ', 'apollo'); ?> </a>
                        </dt>
                        <dd>
                            <div class="mutliSelect right" id="event-accessibility-icons-search">
                                <ul>
                                    <?php

                                    foreach($apollo_event_access as $img=>$label){
                                        $checked = '';
                                        if(is_array($searchData['accessibility']) && count($searchData['accessibility'])){
                                            if(in_array($img,$searchData['accessibility'])){
                                                $checked ='checked';
                                            }
                                        }
                                        ?>
                                        <li>
                                            <input <?php echo $checked; ?> type="checkbox" value="<?php echo $img ?>" name="accessibility[]" access-name="<?php echo $label; ?>" class="apl-accessibility-item">
                                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/event-accessibility/2x/<?php echo $img ?>.png">
                                            <label><?php echo $label; ?></label></span>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </dd>
                    </dl>
                </div>
            </div>
            <?php endif; ?>

            <?php if( of_get_option(Apollo_DB_Schema::_ENABLE_EVENT_SEARCH_CITY, 0) && $listCityState): ?>
                <div class="el-blk">
                    <?php
                    if($isAvaiableVenueModule){
                        ?>
                        <div class=" select-bkl">
                            <select name="event_city" id="event-city-select">
                                <?php echo Apollo_App::renderCities($listCityState, $searchData['city']); ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    <?php } ?>
                </div>
            <?php endif; ?>

            <?php
            /** @Ticket #16482 - Show dropdown neighborhood */
            if( of_get_option(Apollo_DB_Schema::_ENABLE_EVENT_SEARCH_NEIGHBORHOOD, 0) && $listCityState):
                if (!empty($searchData['city'])) {
                    $neighborhoodOptions = APL_Lib_Territory_Neighborhood::renderNeighborhoodOnEventSearch($listCityState, $searchData['city'], $searchData['neighborhood']);
                }

                ?>
                <div class="el-blk <?php echo (empty($searchData['city']) || empty($neighborhoodOptions)) ? 'hidden' : ''; ?>">
                    <?php
                    if($isAvaiableVenueModule){
                        ?>
                        <div class=" select-bkl">
                            <select name="event_neighborhood"
                                    id="event-neighborhood-select"
                                    data-selected="<?php echo $searchData['neighborhood']; ?>"
                            >
                                <?php echo $neighborhoodOptions; ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    <?php } ?>
                </div>
            <?php endif; ?>

            <!-- show dropdownlist regions -->
            <?php
                // Disable if this site is enable Mapping Region and Zip Code
                if( of_get_option(Apollo_DB_Schema::_ENABLE_EVENT_SEARCH_REGION , 0) && $listRegions ): ?>
                <div class="el-blk">
                    <?php
                    if($isAvaiableVenueModule){
                        ?>
                        <div class=" select-bkl">
                            <select name="event_region" id="event-region-select">
                                <option value=""><?php echo __('Select Region','apollo'); ?></option>
                                <?php
                                if($listRegions){
                                    foreach($listRegions as $region){
                                        $selected = '';
                                        if($region == $searchData['region'])
                                            $selected = 'selected';
                                        if(!empty($region)) {
                                            ?>
                                            <option <?php echo $selected; ?> value="<?php echo $region ?>">
                                                <?php echo $region; ?>
                                            </option>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    <?php } ?>
                </div>
            <?php endif; ?>
            <!-- end show dropdownlist regions -->
            <?php
            /** @Ticket #13392 */
            if(of_get_option(Apollo_DB_Schema::_ENABLE_EVENT_SEARCH_FREE_EVENT, false)) : ?>
            <div class="el-blk">
                <input name="free_event" type="checkbox" class="free-event" value="yes" <?php echo (isset($_GET['free_event']) && $_GET['free_event'] == 'yes') ? 'checked' : '' ; ?>/>
                <label for="free-event" class="free-evt"><?php _e( 'Free Event', 'apollo' ); ?></label>
            </div>
            <?php endif; ?>

            <!-- @ticket #16782: 0002308: Calendar Month search widget - Month view layout -->
            <?php echo apply_filters('apl_add_search_widget_month_view', '') ?>

            <!-- @ticket #16782: 0002308: Calendar Month search widget - Month view layout -->
            <?php if (of_get_option('apl_search_by_date_range', true)) { ?>
            <div class="s-rw el-blk">
                <label><?php _e('Search by date range', 'apollo') ?>:</label>

                <div class="lst-lik">
                    <a  data-attr="today" class="today select-date <?php if($searchData['save_lst_list'] == 'today') echo 'active' ?> "><?php _e('Today', 'apollo'); ?></a>
                    <a  data-attr="tomorrow" class="tomorrow select-date   <?php if($searchData['save_lst_list'] == 'tomorrow') echo 'active' ?>"><?php _e('Tomorrow', 'apollo'); ?></a>
                    <a  data-attr="weekend" class="weekend select-date  <?php if($searchData['save_lst_list'] == 'weekend') echo 'active' ?>" ><?php _e('Weekend', 'apollo'); ?></a>
                    <span data-attr="next" class="no-line "><?php _e('next', 'apollo'); ?></span>
                    <a  data-attr="seven-day" class="seven select-date  <?php if($searchData['save_lst_list'] == 'seven-day') echo 'active' ?>"><?php _e('7', 'apollo'); ?></a>
                    <a  data-attr="fourteen-day" class="fourteen select-date  <?php if($searchData['save_lst_list'] == 'fourteen-day') echo 'active' ?>"><?php _e('14', 'apollo'); ?></a>
                    <a  data-attr="thirsty-day" class="thirsty select-date <?php if($searchData['save_lst_list'] == 'thirsty-day') echo 'active' ?>"><?php _e('30', 'apollo'); ?></a></div>
            </div>
            <?php } ?>

            <input type="hidden" id="save-lst-list" name="save_lst_list" value="<?php echo $searchData['save_lst_list'] ?>">

            <?php
            /** @Ticket #13506 */
            if (of_get_option(Apollo_DB_Schema::_ENABLE_DISCOUNT_OFFER_CHBOX, 0)):
                $text = of_get_option(Apollo_DB_Schema::_TEXT_OF_DISCOUNT_OFFER,Apollo_Display_Config::_TEXT_OF_DISCOUNT_OFFER);
                ?>
                <div class="s-rw">
                    <label for="is_discount" style="display: none;"><?php _e($text, 'apollo') ?>:</label>
                    <input id="is_discount" <?php echo $searchData['is_discount'] ? 'checked' : '' ?> name="is_discount"value="1" type="checkbox"><span class="offers"><?php _e( $text, 'apollo' );  ?></span>
                </div>
            <?php endif; ?>

            <?php
            //@Ticket #16603
            $filterByMyLocation = '';
            echo apply_filters('octave_filter_by_my_location', $filterByMyLocation, $searchData, false);
            ?>

            <div class="b-btn">
                <button  type="submit" class="btn btn-b edu-btn apl-event-submit">
                    <?php _e( 'SEARCH', 'apollo' ) ?>
                </button>
                <?php
                Apollo_App::includeTemplate(get_stylesheet_directory(). '/inc/widgets/search/includes/reset-button.php', ['idForm' => 'search-event', 'typeResetButton' => 'button']);
                ?>
            </div>
            <?php
                //Check enable when user login and event is enable
                if(Apollo_App::is_avaiable_module( Apollo_DB_Schema::_EVENT_PT )){
                    if(get_current_user_id()!= 0){
            ?>

            <?php
                    }
                }
                ///end check
            ?>
            <input type="hidden" name="view" data-current-view="<?php echo $searchData['view']; ?>" value="<?php echo $searchData['view']; ?>">

            <?php include APOLLO_WIDGETS_DIR . '/search/templates/event-map-element.php' ; ?>

        </form>
    </div>
</div>

<?php endif; ?>
