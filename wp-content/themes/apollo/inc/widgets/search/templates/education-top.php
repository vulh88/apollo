<!-- Override your template here -->
<?php 
    global $apollo_modules;
    $module_name = isset( $apollo_modules[get_post_type()] ) ? $apollo_modules[get_post_type()]['sing'] : '';
    $educators = get_posts( array( 
        'post_type' => Apollo_DB_Schema::_EDUCATOR_PT,
        'posts_per_page' => -1,
        'orderby'   => 'post_title',
        'order'          => 'ASC', 
    ));
    $isTopSearchMobileVersion = isset($_GET['top-search-mobile']) && !empty($_GET['top-search-mobile']) ? 'ts-mobile' : '';

?>
<div class="top-search <?php echo $isTopSearchMobileVersion; ?>">
    <div class="inner">
        <div class="top-search-row education-s-t-m">
            <form method="get" id="search-education-m-t" action="<?php echo home_url('program')?>" class="form-event search-education">
                <input name="view" type="hidden" value="<?php echo isset($_GET['view']) ? $_GET['view'] : '' ?>" />
                <button type="button" class="btn btn-l s"><i class="fa fa-search fa-flip-horizontal fa-lg"></i></button>
                <input type="text" name="keyword" value="<?php echo Apollo_App::clean_data_request(get_query_var('keyword'), true) ?>" placeholder="<?php _e('Search by Keyword', 'apollo') ?>" class="inp inp-txt event-search event-search-custom">
                <input type="hidden" name="do_search" value="<?php echo isset($_GET['do_search']) && !empty($_GET['do_search']) ? 'yes' : 'no'; ?>" />

                <div class="el-blk">
                    <div class="select-bkl">
                        <select id="educator-provider" name="edu" class="chosen" id="education-pp-select">
                            <option value="0"><?php _e( 'Program Provider', 'apollo' ) ?></option>
                            <?php if ( $educators ):
                                foreach( $educators as $educator ):
                                    ?>
                                    <option <?php echo "value=".$educator->ID; if ( isset( $_GET['edu'] ) && $_GET['edu'] == $educator->ID ) echo ' selected' ?>><?php echo $educator->post_title ?></option>
                                <?php endforeach; endif; ?>
                        </select>
                        <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>

                    </div>
                </div>

                <?php
                $args = array('hide_empty'   => false);
                $artistic_discipline_label = of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_ARTISTIC_DISCIPLINE_LABEL) ? of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_ARTISTIC_DISCIPLINE_LABEL) : 'Artistic Discipline';
                $taxes = array(
                    'program-type'  => __( 'Program Type', 'apollo' ),
                );

                /** @Task #12782 */
                $bilingual_option = of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_BILINGUAL, true);

                /** @Task #12788 */
                $free_option = of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_PROGRAM_FREE, true);

                if (of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_POPULATION_SERVED_DROP) )
                    $taxes[ Apollo_DB_Schema::_APL_PROG_TAX_POP_SER] = __('Population Served', 'apollo');
                if (of_get_option( Apollo_DB_Schema::_EDUCATOR_ENABLE_SUBJECT_DROP) )
                    $taxes[Apollo_DB_Schema::_APL_PROG_TAX_SUBJECT] =  __('Subject', 'apollo');
                if (of_get_option( Apollo_DB_Schema::_EDUCATOR_ENABLE_ARTISTIC_DISCIPLINE_DROP) )
                    $taxes[Apollo_DB_Schema::_APL_PROG_TAX_ART_DESC] = __($artistic_discipline_label, 'apollo');
                if (of_get_option( Apollo_DB_Schema::_EDUCATOR_ENABLE_CULTURAL_ORIGIN_DROP) )
                    $taxes[Apollo_DB_Schema::_APL_PROG_TAX_CUL_ORIGIN] = __('Cultural Origin', 'apollo');
                if ( get_terms(Apollo_DB_Schema::_APL_EDU_TYPE,$args) )
                    $taxes[Apollo_DB_Schema::_APL_EDU_TYPE] = __('Educator Type','apollo');

                foreach ( $taxes as $tax_key => $tax_label ):
                    $prefix = $tax_key == Apollo_DB_Schema::_APL_PROG_TAX_SUBJECT ? '_' : '';
                    $post_term_key = $prefix.str_replace('-', '_', $tax_key);
                    $default_value = isset( $_GET[$post_term_key] ) ? $_GET[$post_term_key] : '';
                    $tree_term = Apollo_Program::get_tree_terms( $tax_key );
                    ?>

                    <div class="el-blk">
                        <div class="select-bkl">
                            <select name="<?php echo $post_term_key ?>" class="chosen education-pt-select">
                                <option value=""><?php echo $tax_label ?></option>
                                <?php Apollo_Program::build_option_tree($tree_term, $default_value) ?>
                            </select>
                            <div class="arrow-down"><i class="fa fa-sort-desc fa-lg"></i></div>
                        </div>
                    </div>
                <?php endforeach; ?>


                <div class="s-rw cln">
                    <div class="calendar-ipt clearfix">
                        <span>
                            <input name="sd" value="<?php echo isset( $_GET['sd'] ) ? Apollo_App::clean_data_request($_GET['sd']) : '' ?>" type="text"  size="12" id="top-c-s" placeholder="<?php _e('Start', 'apollo');?>" class="education-s-d inp inp-txt caldr">
                        </span>
                        <span>
                            <input name="ed" value="<?php echo isset( $_GET['ed'] ) ? Apollo_App::clean_data_request($_GET['ed']) : '' ?>" type="text" size="12" id="top-c-e" placeholder="<?php _e('End', 'apollo');?>" class="education-e-d inp inp-txt caldr">
                        </span>

                        <i class="fa fa-calendar fa-lg ico-s"></i>
                        <i class="fa fa-calendar fa-lg ico-e">    </i>
                    </div>
                </div>

                <?php if($bilingual_option) : ?>
                <div class="el-blk education-checkbox">
                    <input type="checkbox" name="bil" value="yes" id="education-bilingual-checkbox" <?php if( isset( $_GET['bil'] ) && $_GET['bil'] == 'yes' ) echo 'checked' ?> />
                    <label onclick="checkCBBefore(this)" class="biling"><?php _e( 'Bilingual', 'apollo' ) ?></label>
                </div>
                <?php endif; ?>


                <?php if($free_option) : ?>
                <div class="el-blk education-checkbox">
                    <input type="checkbox" name="free" value="yes" id="education-free-checkbox" <?php if( isset( $_GET['free'] ) && $_GET['free'] == 'yes' ) echo 'checked' ?> />
                    <label onclick="checkCBBefore(this)" class="biling"><?php _e( 'Free', 'apollo' ) ?></label>
                </div>
                <?php endif; ?>

                <?php
                $_have_check = false;
                $_other_fields_str = '';
                $custom_fields = Apollo_Custom_Field::get_field_by_location( Apollo_DB_Schema::_EDUCATOR_PT);
                if ( $custom_fields ):
                    foreach ( $custom_fields as $field ):

                        $k = $field->name;

                        $selected = isset( $_GET[$k] ) && $_GET[$k] == $k ? 'checked' : '';

                        if ( $selected && !$_have_check ) $_have_check = true;

                        $_other_fields_str .= '<li>
                      <input '.$selected.' value="'.$k.'" type="checkbox" name="'.$k.'">
                      <label onclick="checkCBBefore(this)" class="quali">'.$field->label.'</label>
                    </li>';
                    endforeach;
                endif;
                if ( $_other_fields_str ):
                    ?>
                    <div class="el-blk mrg custom-el-blk"><a href="#" class="qualification <?php if ($_have_check) echo 'active' ?>"><?php _e( 'Qualifications', 'apollo' ) ?></a>
                        <ul class="quali-list" id="education-other-fields-checkbox">
                            <?php echo $_other_fields_str ?>
                        </ul>
                    </div>
                <?php endif; ?>

                <div class="sub-blk">
                    <button type="submit" class="btn btn-l edu-btn lgr fr f-w-btn-search"><?php _e( 'SEARCH', 'apollo' ) ?></button>
                </div>
                <div class="sub-blk">
                    <button type="button" id="" data-rise="clear-form" data-form="#search-education" class="clear-edu-search-form btn btn-l edu-btn lgr fr f-w-btn-search"><?php _e( 'RESET', 'apollo' ) ?></button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php 
    if ( $_have_check ):
?>
<script>
    jQuery(function() {
        jQuery('.quali-list').show();
    });
</script>
<?php endif; ?>

