<?php

    $currentWidgetType = !empty($this->type) ? $this->type : '';
    $selectedAplProgTypes = isset($instance['selected_apl_prog_types']) ?   $instance['selected_apl_prog_types'] : array();
?>
<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
           name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"
           value="<?php echo esc_attr( isset( $instance['title'] ) ? $instance['title'] : '' ); ?>" />
</p>
<?php
$programTerms = get_terms('program-type');
$requestSidebar = isset($_POST['sidebar'])?$_POST['sidebar']:'';
$sidebarName =  Apollo_DB_Schema::_SIDEBAR_PREFIX . '-'. str_replace('-', '_', Apollo_DB_Schema::_EDUCATION);
$educationCategory = '';
if( $currentWidgetType == Apollo_DB_Schema::_EDUCATION
    && $programTerms){
    ob_start();
    include ('search-education-category.php');
    $educationCategory = ob_get_contents();
    ob_end_clean();
}

//use for save button clicked
if(
    $_POST //has post request
    && $requestSidebar != $sidebarName
  ){
    $educationCategory = '';
}
echo  $educationCategory;
?>

