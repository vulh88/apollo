<?php if(isset($typeResetButton) && $typeResetButton == 'button'): ?>
<button type="button" id="clear-event-form" class="clear-event-form custom-button btn btn-b edu-btn <?php echo isset($class) ? $class : '';?>" data-form="#<?php echo isset($idForm) ? $idForm : '' ?>">
    <?php _e( 'RESET', 'apollo' ) ?>
</button>
<?php else: ?>
<input type="button" id="clear-event-form" class="clear-event-form custom-button btn btn-b edu-btn <?php echo isset($class) ? $class : '';?>" data-form="#<?php echo isset($idForm) ? $idForm : '' ?>" value="<?php _e('RESET','apollo') ?>">
<?php endif; ?>
