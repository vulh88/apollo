<div data-only-in-module="1"  data-show-in="apl-sidebar-education" class="category-listing">
    <label><?php _e('Do not display this widget in selected Program types:', 'apollo') ?></label>
    <ul class="apl-widget-program-type">
        <?php foreach($programTerms as $term):

            ?>
            <li>
                <input type="checkbox" class="apl-mod-wg-child"
                       value="<?php echo $term->term_id; ?>"
                       name="<?php echo $this->get_field_name( 'selected_apl_prog_types' ) . '[]'; ?>"
                    <?php echo $selectedAplProgTypes && in_array($term->term_id, $selectedAplProgTypes) ? 'checked="checked"' : ''; ?>
                />
                <label><?php echo $term->name; ?></label>
            </li>
        <?php endforeach; ?>
    </ul>
</div>