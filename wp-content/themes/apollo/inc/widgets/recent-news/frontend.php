<?php
$args = array(
    'post_type' => array('news'),
    'post_status' => 'publish',
    'showposts' => $instance['number_show'],
    'order' => 'DESC',
    'orderby' => 'post_date',

);
query_posts($args);
$link = Apollo_App::getCustomUrlByModuleName(Apollo_DB_Schema::_NEWS_PT);
$title = $instance['title'];
$height = isset($instance['height']) ? $instance['height'] : 400;
$height = $height . "px";
$height = "style='height: $height; overflow-y: scroll;'";

$showImage = !isset($instance['show_image']) || !$instance['show_image'];
$showAuthor = !isset($instance['show_author']) || !$instance['show_author'];
$showDate = isset($instance['show_date']) && $instance['show_date'] == 'off';

if (have_posts()): ?>
    <div class="r-blk big std recentpost widget_recent_post apl-recent-news ">
        <?php if(!empty($title)): ?>
            <h3 class="r-ttl"><i class="fa fa-newspaper-o fa-lg"></i><span> <?php echo $title ?></span></h3>
        <?php endif; ?>
        <div <?php echo $height ?>>
            <?php while (have_posts()) : the_post();
                global $post;
                $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'apl_recent_posts'); ?>
                <div class="apl-recent-post">
                    <?php if (!empty($image) && $showImage) : ?>
                        <div class="alignleft apl-recent-post-img-wrapper">
                            <a href="<?php the_permalink(); ?>">
                                <img class="alignleft apl-recent-post-img " src=" <?php echo $image[0]; ?>" alt=""/>
                            </a>
                        </div>
                    <?php endif; ?>

                    <div class="alignleft apl-recent-post-content">
                        <?php
                        /**
                         * @ticket #19232: News Module - Sponsored Content of the news
                         */
                        $postId = isset($post->ID) ? $post->ID : '';
                        $sponsoredContent = get_post_meta($postId,'post-sponsored-content',true);
                        if($sponsoredContent && $sponsoredContent === 'on') :
                            $urlField =  get_post_meta($postId,'post-new-url',true);
                            $newPage = get_post_meta($postId,'post-new-page',true);
                            $target = ($newPage && $newPage == 'on' && $urlField) ? "target='blank'" : '';
                            $urlPost = $urlField ? $urlField : get_permalink($postId);
                            $linkSponsored = "href='$urlPost' $target";
                            ?>
                            <span class="news-sponsored-content">
                            <a <?php echo $linkSponsored ?>><?php echo __('Sponsored Contact', 'apollo'); ?></a>
                        </span>
                        <?php endif; ?>

                        <span>
                            <a href="<?php the_permalink(); ?>">  <h3><?php the_title(); ?></h3></a>
                        </span>
                        <?php if ($showDate): ?>
                            <p class="post-date"><?php echo Apollo_App::apl_date('M d, Y', get_post_time()); ?></p>
                        <?php endif; ?>
                        <?php if ($post && $post->post_author && $showAuthor): ?>
                            <p class="post-author-link">
                                <span><?php _e('By: ', 'apollo'); ?></span>
                                <?php echo the_author_posts_link(); ?>
                            </p>
                        <?php endif; ?>
                    </div>
                </div>

            <?php endwhile;
            wp_reset_query()
            ?>
        </div>
        <div class="recent-post-footer-link">
            <a href="<?php echo $link; ?>"><?php echo sprintf(__('View all %s ', 'apollo'), strtolower($title)); ?>
                &#187;</a>
        </div>
    </div>

<?php endif; ?>