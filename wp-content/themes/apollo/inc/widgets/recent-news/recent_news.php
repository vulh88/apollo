<?php

require_once __DIR__ . '/../Apollo_Widget.php';

class apollo_recent_news_widget extends Apollo_Widget
{

    public function __construct()
    {
        $this->adminTemplatePath = dirname(__FILE__) . '/admin.php';
        parent::__construct(

        // ID of widget
            'apollo_news_posts_widget',

            // Widget name 
            __('Apollo Recent News Widget', 'apollo'),

            // Widget description
            array('description' => __('Apollo Recent News Widget', 'apollo'),)
        );

        add_shortcode('apollo_widgets', array($this, 'setLocation'));
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = !empty($new_instance['title']) ? strip_tags($new_instance['title']) : '';
        $instance['number_show'] = !empty($new_instance['number_show']) ? $new_instance['number_show'] : 5;
        $instance['show_image'] = !empty($new_instance['show_image']) ? $new_instance['show_image'] : 0;
        /*@ticket #18385 0002503: News Module - Modifications for the News widget.*/
        $instance['show_author'] = !empty($new_instance['show_author']) ? $new_instance['show_author'] : 0;
        $instance['show_date'] = !empty($new_instance['show_date']) ? $new_instance['show_date'] : 'off';
        $instance['height'] = !empty($new_instance['height']) ? $new_instance['height'] : 400;
        return $instance;
    }

    /**
     * Creating widget front-end
     * This is where the action happens
     */
    public function childWiget($args, $instance)
    {
        ob_start();
        include dirname(__FILE__) . '/frontend.php';
        $html = ob_get_contents();
        ob_end_clean();
        echo $html;
    }

}  