<p>
    <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
           name="<?php echo $this->get_field_name('title'); ?>" type="text"
           value="<?php echo esc_attr(isset($instance['title']) ? $instance['title'] : ''); ?>"/>
</p>

<p>
    <label for="<?php echo $this->get_field_id('number_show'); ?>"><?php _e('Number of news to show:', 'apollo'); ?></label>
    <input name="<?php echo $this->get_field_name('number_show'); ?>" type="text"
           id="<?php echo $this->get_field_id('number_show'); ?>"
           value="<?php echo isset($instance['number_show']) ? $instance['number_show'] : '5'; ?>" size="3">
</p>
<!-- @ticket #18385 0002503: News Module - Modifications for the News widget. -->
<p>
    <label for="<?php echo $this->get_field_id('height'); ?>"><?php _e('Height', 'apollo'); ?></label>
    <input name="<?php echo $this->get_field_name('height'); ?>" type="text"
           id="<?php echo $this->get_field_id('height'); ?>"
           value="<?php echo isset($instance['height']) ? $instance['height'] : '400'; ?>" size="3">
</p>

<p>
    <input class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('show_image'); ?>"
        <?php echo (isset($instance['show_image']) && $instance['show_image']) ? 'checked' : ''; ?>
           id="<?php echo $this->get_field_id('show_image'); ?>">
    <label for="<?php echo $this->get_field_id('show_image'); ?>"><?php _e('Do not display feature image?', 'apollo'); ?></label>
</p>

<!--@ticket #18385 0002503: News Module - Modifications for the News widget. -->
<p>
    <input class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('show_author'); ?>"
        <?php echo (isset($instance['show_author']) && $instance['show_author']) ? 'checked' : ''; ?>
           id="<?php echo $this->get_field_id('show_author'); ?>">
    <label for="<?php echo $this->get_field_id('show_author'); ?>"><?php _e("Do not display author's name?", 'apollo'); ?></label>
</p>

<p>
    <input class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('show_date'); ?>"
        <?php echo (empty($instance['show_date']) || ($instance['show_date'] == 'on')) ? 'checked' : ''; ?>
           id="<?php echo $this->get_field_id('show_date'); ?>">
    <label for="<?php echo $this->get_field_id('show_date'); ?>"><?php _e('Do not display show date?', 'apollo'); ?></label>
</p>
