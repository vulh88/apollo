<?php
require_once  APOLLO_INCLUDES_DIR. '/widgets/recommend-post/class-recommend-post-model.php';
global $wpdb, $post;

if ($post->post_type === 'post' && is_single()) {

    $title = $instance['recommend-title'] ? $instance['recommend-title'] : __('Recommended stories');
    $maxDisplays = $instance['max-displays'] ? $instance['max-displays'] : 50;

    $model = new Apollo_Recommend_Post_Model();
    $recommendPostID = $model->getRecommendPost($maxDisplays);

    if (!empty($recommendPostID)) {?>

        <div class="r-blk big std recentpost widget_recent_post apl-recommend-post-widget">
            <h3 class="r-ttl"><span> <?php echo $title ?></span></h3>

            <?php foreach ($recommendPostID as $recommend) {
                $postID = $recommend['ID'];
                $image = wp_get_attachment_image_src(get_post_thumbnail_id($postID), 'apl_recent_posts');
                $author_id = get_post_field('post_author', $postID);
                $authorName = get_the_author_meta('display_name', $author_id);
                ?>
                <div class="apl-recent-post">
                    <?php if (!empty($image)) : ?>
                        <div class="alignleft apl-recent-post-img-wrapper">
                            <a href="<?php echo get_permalink($postID); ?>">
                                <img class="alignleft apl-recent-post-img " src=" <?php echo $image[0]; ?>" alt=""/>
                            </a>
                        </div>
                    <?php endif; ?>

                    <div class="apl-recent-post-content">
                        <span>
                            <a href="<?php echo get_permalink($postID); ?>">  <h3><?php echo get_the_title($postID); ?></h3></a>
                        </span>
                        <br/>
                        <p class="post-author-link">
                            <span><?php _e('By: ', 'apollo'); ?></span>
                            <a href="<?php echo get_author_posts_url($author_id) ?>"><?php echo $authorName; ?></a>
                        </p>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php  wp_reset_query(); };
}