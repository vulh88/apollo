<?php

class Apollo_Recommend_Post_Model
{
    public function getRecommendPost($maxDisplays)
    {
        global $post, $wpdb;

        $currentTags = get_apollo_meta($post->ID, Apollo_DB_Schema::_APL_POST_OFFICIAL_TAGS, true);
        $currentTags = $currentTags ? $currentTags : '';

        if(!$currentTags){
            return array();
        }

        $currentTags = explode(',',$currentTags);
        $conditions = implode("%' or meta_value like '%", $currentTags);
        $conditions = "meta_value like '%". $conditions. "%'";

        $strQuery = "SELECT ID " .
            "FROM {$wpdb->posts} JOIN {$wpdb->apollo_postmeta} 
            ON (ID = apollo_post_id AND meta_key='" .Apollo_DB_Schema::_APL_POST_OFFICIAL_TAGS . "' AND ($conditions))  
            WHERE post_status in ('publish') AND meta_value is not null and ID !=  $post->ID  and post_type = 'post' 
            ORDER BY post_date desc  
            LIMIT {$maxDisplays}";

        $recommentPostID = $wpdb->get_results($strQuery, ARRAY_A);

        return $recommentPostID;
    }
}