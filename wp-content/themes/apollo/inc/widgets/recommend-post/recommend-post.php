<?php

require_once APOLLO_INCLUDES_DIR. '/widgets/Apollo_Widget.php';
/*@ticket #17238 0002398 - Recommendation Widgets - Recommended stories widget*/
class apollo_recommend_post_widget extends WP_Widget {
    private $adminTemplatePath = '';
    public function __construct() {
        $this->adminTemplatePath = dirname(__FILE__). '/admin.php';
        parent::__construct(

            // ID of widget
            'apollo_recommend_post_widget',

            // Widget name
            __('Recommended Stories Widget', 'apollo'),

            // Widget description
            array( 'description' => __( 'Recommended Stories Widget', 'apollo' ), )
        );
    }

    public function form($instance)
    {
        if (!file_exists($this->adminTemplatePath)) {
            return '';
        }

        ob_start();
        include $this->adminTemplatePath;
        $html = ob_get_contents();
        ob_end_clean();
        echo $html;
    }

    public function widget($args, $instance)
    {
        global $recommentPostContent;

        /*@ticket #18044: Octave Theme - [CF] 20181025 - Modify the "Recommended Stories" list orientation to 'horizontal' instead of vertical*/
        if(isset($instance['recommend-style']) && $instance['recommend-style'] == 'horizontal'
            && of_get_option(Apollo_DB_Schema::_BLOG_DISPLAY_STYLE,'default') == 'one-column'){
            $fileName = dirname(__FILE__) . '/frontend-templates/recommends-hor.php';
        }
        else{
            $fileName = dirname(__FILE__) . '/frontend-templates/recommends-ver.php';
        }
        if (!file_exists($fileName)) {
            return '';
        }

        if(!$recommentPostContent){
            ob_start();
            include $fileName;
            $html = ob_get_contents();
            ob_end_clean();
            $GLOBALS['recommentPostContent'] = $html;
        }

        echo $recommentPostContent;
    }

    public function update($new_instance, $old_instance)
    {
        $new_instance['max-displays'] = (int)$new_instance['max-displays'];
        if (!is_int($new_instance['max-displays']) || $new_instance['max-displays'] <= 0) {
            $new_instance['max-displays'] = '';
        }
        return parent::update($new_instance, $old_instance);
    }
}