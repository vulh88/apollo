<p>
    <label for="<?php echo $this->get_field_id( 'recommend-title' ); ?>"><?php _e( 'Title:' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'recommend-title' ); ?>"
           name="<?php echo $this->get_field_name( 'recommend-title' ); ?>" type="text"
           value="<?php echo esc_attr( isset( $instance['recommend-title'] ) ? $instance['recommend-title'] : '' ); ?>" />
</p>

<p>
    <label  for="<?php echo $this->get_field_id( 'max-displays' ); ?>"><?php _e( 'Display up to:','apollo' ); ?></label>
    <input  class="widefat"  name="<?php echo $this->get_field_name( 'max-displays' ); ?>" type="text"
            id="<?php echo $this->get_field_id( 'max-displays' ); ?>"
            value="<?php echo  isset( $instance['max-displays'] )  ? $instance['max-displays'] : 50 ; ?>">
    <span><?php echo __('Default value is 50', 'apollo') ?></span>
</p>
<?php
/*@ticket #18044 Octave Theme - [CF] 20181025 - Modify the "Recommended Stories" list orientation to 'horizontal' instead of vertical*/
$style = isset( $instance['recommend-style'] )  ? $instance['recommend-style'] : 'vertical';
?>
<p>
    <label for="<?php echo $this->get_field_id( 'recommend-style' ); ?>"><?php _e( 'Style:','apollo' ); ?></label>
    <select style="width: 100%;" name="<?php echo $this->get_field_name( 'recommend-style' ); ?>" id="<?php echo $this->get_field_id( 'recommend-style' ); ?>" >
        <option <?php echo $style == 'vertical' ? 'selected' : '' ?> value="vertical"><?php echo __('Vertical - Default', 'apollo') ?></option>
        <option <?php echo $style == 'horizontal' ? 'selected' : '' ?> value="horizontal"><?php echo __('Horizontal - Only apply to the one column template', 'apollo') ?></option>
    </select>
</p>
