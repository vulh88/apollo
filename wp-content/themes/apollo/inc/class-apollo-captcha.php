<?php
if(!defined('ABSPATH')) {
	exit; // Silent is golden
}

/*
 * Apollo captcha for wordpress
*/
class Apollo_Captcha {

    /* nr class attributes */
	private $_config;


    /* st class attributes */
    private static $_hasCallHook = false;
    private static $_check_string = 'ELINEXT-APOLLO-HRCXT';

	public function __construct( $config = null ) {

        if(!session_id())
            throw new Exception('Please use session_start() before construct this class is missing');

		if( !function_exists('gd_info') ) {
			throw new Exception('Required GD library is missing');
		}

		$bg_path = __DIR__ . '/captcha/backgrounds/';

		if(!file_exists($bg_path)) {
			throw new Exception('Background directory' . $bg_path .' not found');
		}

		$font_path = __DIR__ . '/captcha/fonts/';

		if(!file_exists($font_path)) {
			throw new Exception('Font directory' . $font_path .' not found');
		}

        self::_destroySession();

		// Default values
		$default_config = array(
			'code' => '',
			'min_length' => 5,
			'max_length' => 5,
			'backgrounds' => array(
				$bg_path . '45-degree-fabric.png',
				$bg_path . 'cloth-alike.png',
				$bg_path . 'grey-sandbag.png',
				$bg_path . 'kinda-jean.png',
				$bg_path . 'polyester-lite.png',
				$bg_path . 'stitched-wool.png',
				$bg_path . 'white-carbon.png',
				$bg_path . 'white-wave.png'
			),
			'fonts' => array(
				$font_path . 'times_new_yorker.ttf'
			),
			'characters' => 'ABCDEFGHJKLMNPRSTUVWXYZabcdefghjkmnprstuvwxyz23456789',
			'min_font_size' => 28,
			'max_font_size' => 28,
			'color' => '#666',
			'angle_min' => 0,
			'angle_max' => 10,
			'shadow' => true,
			'shadow_color' => '#fff',
			'shadow_offset_x' => -1,
			'shadow_offset_y' => 1
		);

		// Overwrite defaults with custom config values
		$captcha_config = wp_parse_args($config, $default_config);

		// Restrict certain values
		if( $captcha_config['min_length'] < 1 ){
			throw new InvalidArgumentException("min_length is invalid ");
		}
		if( $captcha_config['angle_min'] < 0 ) {
			throw new InvalidArgumentException("angle_min is invalid ");
		}
		if( $captcha_config['angle_max'] > 10 ) {
			throw new InvalidArgumentException("angle_max cannot greater than 10 ");
		}
		if( $captcha_config['angle_max'] < $captcha_config['angle_min'] ) $captcha_config['angle_max'] = $captcha_config['angle_min'];
		if( $captcha_config['min_font_size'] < 10 ) {
			throw new InvalidArgumentException("min_font_size must be greater than 10 ");
		}
		if( $captcha_config['max_font_size'] < $captcha_config['min_font_size'] ) $captcha_config['max_font_size'] = $captcha_config['min_font_size'];

		// Use milliseconds instead of seconds - This is seed of random: http://php.net/manual/en/function.srand.php
		srand(microtime(1) * 100);

		$this->_config = $captcha_config;

		// Generate CAPTCHA code if not set by user
        $this->_config['code'] = self::_generateCode($this->_config);
        self::_commitSession($this->_config);

	}

    public function getCode() {
        return $this->_config['code'];
    }

	public static function registerHook() {
        // Call one time
        if(self::$_hasCallHook) return;
        self::$_hasCallHook = true;

        /* Hook Ajax path */
        if(defined('DOING_AJAX') && DOING_AJAX) {
            // Use the ajax to serve the image
            $ajax_events = array(
                'show_captcha_comment_image' => true,
                'check_captcha_comment_code' => true,
            );
            self::_addHookAjax($ajax_events);

            return;
        }

        /* Add normal hook here! */

	}

    private  static  function  _addHookAjax($ajax_events) {
        foreach ( $ajax_events as $ajax_event => $nopriv ) {
            if(!method_exists(__CLASS__, $ajax_event)) {
                throw new InvalidArgumentException(__('You are not implement this method', 'apollo'));
            }

            add_action( 'wp_ajax_apollo_' . $ajax_event, array( __CLASS__, $ajax_event ) );

            if ( $nopriv )
                add_action( 'wp_ajax_nopriv_apollo_' . $ajax_event, array( __CLASS__, $ajax_event ) );
        }
    }

    /**
     * @expect string
     */
    public static function getImagePath() {
		// Generate HTML for image src
        //$check_string = wp_create_nonce(self::$_check_string);
//		return admin_url('admin-ajax.php?action=apollo_show_captcha_comment_image' . '&t=' . urlencode(microtime()).'&c='.$check_string);
        return admin_url('admin-ajax.php?action=apollo_show_captcha_comment_image' . '&t=' . urlencode(microtime()));
	}

	/* @expect code string */
	private static function _generateCode($config) {
		$code = '';
		$length = rand($config['min_length'], $config['max_length']);
		$sourceLength = strlen($config['characters']);
		while( strlen($code) < $length ) {
			$code .= substr($config['characters'], rand() % $sourceLength, 1);
		}

        return $code;
    }

    public static function getCurrentCode() {
        $captcha_config = unserialize($_SESSION['_APOLLO_CAPTCHA_COMMENT']['config']);
        return $captcha_config['code'];
    }

    private static function _commitSession($config) {
        $_SESSION['_APOLLO_CAPTCHA_COMMENT']['config'] = serialize($config);
    }

    public static function _destroySession() {
        if(isset($_SESSION['_APOLLO_CAPTCHA_COMMENT']))
            $_SESSION['_APOLLO_CAPTCHA_COMMENT'];
    }

	/* @expect image */
	public static function show_captcha_comment_image() {
        
		if(!session_id()) {
            throw new Exception('Please use session_start before called this function');
        }


		if(!isset($_SESSION['_APOLLO_CAPTCHA_COMMENT']) || !isset($_SESSION['_APOLLO_CAPTCHA_COMMENT']['config'])) {
			exit();
		}

        /* check string */
//        check_ajax_referer( self::$_check_string, 'c');

		$captcha_config = unserialize($_SESSION['_APOLLO_CAPTCHA_COMMENT']['config']);
		if( !$captcha_config ) exit();

		// test only
//		$captcha_config['code'] = '123';
		//end test

		// Use milliseconds instead of seconds - set seed
		srand(microtime() * 100);

		// Pick random background, get info, and start captcha
		$background = $captcha_config['backgrounds'][rand(0, count($captcha_config['backgrounds']) -1)];        
		list($bg_width, $bg_height, $bg_type, $bg_attr) = getimagesize($background);

		$captcha = imagecreatefrompng($background);

		$color = self::_hex2rgb($captcha_config['color']);
		$color = imagecolorallocate($captcha, $color['r'], $color['g'], $color['b']);

		// Determine text angle
		$angle = rand( $captcha_config['angle_min'], $captcha_config['angle_max'] ) * (rand(0, 1) == 1 ? -1 : 1);

		// Select font randomly
		$font = $captcha_config['fonts'][rand(0, count($captcha_config['fonts']) - 1)];

		// Verify font file exists
		if( !file_exists($font) ) throw new Exception('Font file not found: ' . $font);

		//Set the font size.
		$font_size = rand($captcha_config['min_font_size'], $captcha_config['max_font_size']);
		$text_box_size = imagettfbbox($font_size, $angle, $font, $captcha_config['code']);

		// Determine text position
		$box_width = abs($text_box_size[6] - $text_box_size[2]);
		$box_height = abs($text_box_size[5] - $text_box_size[1]);
		$text_pos_x_min = 0;
		$text_pos_x_max = ($bg_width) - ($box_width);
		$text_pos_x = rand($text_pos_x_min, $text_pos_x_max);
		$text_pos_y_min = $box_height;
		$text_pos_y_max = ($bg_height) - ($box_height / 2);
		$text_pos_y = rand($text_pos_y_min, $text_pos_y_max);

		// Draw shadow
		if( $captcha_config['shadow'] ){
			$shadow_color = self::_hex2rgb($captcha_config['shadow_color']);
		 	$shadow_color = imagecolorallocate($captcha, $shadow_color['r'], $shadow_color['g'], $shadow_color['b']);
			imagettftext($captcha, $font_size, $angle, $text_pos_x + $captcha_config['shadow_offset_x'], $text_pos_y + $captcha_config['shadow_offset_y'], $shadow_color, $font, $captcha_config['code']);
		}

		// Draw text
		imagettftext($captcha, $font_size, $angle, $text_pos_x, $text_pos_y, $color, $font, $captcha_config['code']);

		// Output image
		header("Content-type: image/png");
		imagepng($captcha);
	}

    public function getUrlCheckCaptcha() {
        // Generate HTML for image src
        $check_string = wp_create_nonce(self::$_check_string);
        return admin_url('admin-ajax.php?action=apollo_check_captcha_comment_code' . '&t=' . urlencode(microtime()).'&c='.$check_string);
    }

    /* @expect image */
    public static function check_captcha_comment_code() {
        //check maintenance mode
        if(Apollo_App::isMaintenanceMode()) {
            wp_send_json(
                array(
                    'error' => true,
                    'msg' => __( 'Sorry for the inconvenience.Our website is currently undergoing scheduled maintenance'),
                    'error_code' => '02'
                )
            );
            return;
        }

        //check_ajax_referer( self::$_check_string, 'c');

        $aErrors = array(
            '01' =>array(
                'error' => true,
                'msg' => 'Unexpected situation: Main session do not exists',
                'error_code' => '01'
            ),
            '02' =>array(
                'error' => true,
                'msg' => '',
                'error_code' => '02'
            ),

        );

        $aSuccess = array(
            'error' => false,
        );


        if(!isset($_SESSION['_APOLLO_CAPTCHA_COMMENT']) || !isset($_SESSION['_APOLLO_CAPTCHA_COMMENT']['config'])) {
            wp_send_json(
                $aErrors['01']
            );
        }

        $captcha_config = unserialize($_SESSION['_APOLLO_CAPTCHA_COMMENT']['config']);
        if( !$captcha_config ) {
            wp_send_json(
                $aErrors['01']
            );
        }

        /* Don't match code - Generate new image and code */
        if(!isset($_GET['code']) || $_GET['code'] !== $captcha_config['code'] ) {
            $captcha_config['code'] = self::_generateCode($captcha_config);
            self::_commitSession($captcha_config);

            $aErrors['02']['msg'] = __("Captcha not matched, Please try again", "apollo");
            $aErrors['02']['new_image_link'] = self::getImagePath();
            wp_send_json($aErrors['02']);
        }


        /* Ok don't have any error */
        wp_send_json($aSuccess);
    }

    private static function _hex2rgb($hex_str, $return_string = false, $separator = ',')
    {
        $hex_str = preg_replace("/[^0-9A-Fa-f]/", '', $hex_str); // Gets a proper hex string
        $rgb_array = array();
        if( strlen($hex_str) == 6 ) {
            $color_val = hexdec($hex_str);
            $rgb_array['r'] = 0xFF & ($color_val >> 0x10);
            $rgb_array['g'] = 0xFF & ($color_val >> 0x8);
            $rgb_array['b'] = 0xFF & $color_val;
        } elseif( strlen($hex_str) == 3 ) {
            $rgb_array['r'] = hexdec(str_repeat(substr($hex_str, 0, 1), 2));
            $rgb_array['g'] = hexdec(str_repeat(substr($hex_str, 1, 1), 2));
            $rgb_array['b'] = hexdec(str_repeat(substr($hex_str, 2, 1), 2));
        } else {
            return false;
        }
        return $return_string ? implode($separator, $rgb_array) : $rgb_array;
    }
}


Apollo_Captcha::registerHook();