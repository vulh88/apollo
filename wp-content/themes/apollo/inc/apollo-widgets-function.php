<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

// Required Widget

require_once APOLLO_WIDGETS_DIR. '/most-reviews/most_reviews.php';
require_once APOLLO_WIDGETS_DIR. '/recent-posts/recent_posts.php';
require_once APOLLO_WIDGETS_DIR. '/subscribe/subscribe.php';
require_once APOLLO_WIDGETS_DIR. '/custom/custom.php';
require_once APOLLO_WIDGETS_DIR. '/offer-dates-times/offer-dates-times.php';
require_once APOLLO_WIDGETS_DIR. '/recent-news/recent_news.php';

/*@ticket #17236 */
$currentTheme = wp_get_theme();
$currentTheme =  !empty($currentTheme) ? $currentTheme->stylesheet : '';
if ( $currentTheme == 'octave-child') {
    require_once APOLLO_WIDGETS_DIR . '/recommend-post/recommend-post.php';
    require_once APOLLO_WIDGETS_DIR . '/recommend-event/recommend-event.php';
    register_widget('apollo_recommend_post_widget');
    register_widget('apollo_recommend_event_widget');
}

register_widget( 'apollo_most_reviews_widget' );
register_widget( 'apollo_subscribe_widget' );
register_widget( 'apollo_custom_widget' );
register_widget( 'apollo_offer_dates_times_widget' );
if( Apollo_App::is_avaiable_module(Apollo_DB_Schema::_BLOG_POST_PT) ){
    register_widget( 'apollo_recent_posts_widget' );

    if (Apollo_App::is_homepage()) {
        require_once APOLLO_INCLUDES_DIR. '/widgets/home-feature-blog/Apollo_Home_Blog_Feature.php';
        register_widget( 'Apollo_Home_Blog_Feature' );
    }

}

/*@ticket #18127: News Module*/
if( Apollo_App::is_avaiable_module(Apollo_DB_Schema::_NEWS_PT) ){
    register_widget( 'apollo_recent_news_widget' );
}

require_once APOLLO_WIDGETS_DIR. '/search/search.php';
$avaiable_modules = Apollo_App::get_avaiable_modules();
if(!empty($avaiable_modules)) {

    foreach($avaiable_modules as $module) {
        $path = APOLLO_WIDGETS_DIR. '/search/search-'.$module.'.php';
        if(file_exists($path)) {
            require_once APOLLO_WIDGETS_DIR. '/search/search-'.$module.'.php';
            register_widget( 'apollo_search_'.  str_replace('-', '_', $module).'_widget');
        }

        else {
            Apollo_Event_System::trigger('search-widget', array(
                    'event_name' => 'search-widget-admin',
                    'msg' => 'File: ' . $path . ' is not exists!',
                ));
        }

    }
}
