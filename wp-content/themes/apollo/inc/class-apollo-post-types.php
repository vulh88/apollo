<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Post types
 *
 * Registers post types and taxonomies
 *
 * @class 		Apollo_Post_Types
 * @category	Class
 * @author vulh
 */

class Apollo_Post_Types {
    
    var $permalinks = '';
    
	/**
	 * Constructor
	 */
	public function __construct() {
        
        if (! $this->permalinks) {
            $this->permalinks = get_option( 'apollo_permalinks' );
        }

        add_action( 'init', array( $this, 'register_taxonomies' ), 5 );
        add_action( 'init', array( $this, 'register_post_types' ), 5 );
        add_action( 'init', array( $this, 'apl_register_status' ), 5 );
	}
    
    public function __destruct() {
        unset($this->permalinks);
    }

    /**
     * Register "unconfirmed" status
     * @Ticket #18677
     */
    public function apl_register_status () {
        $postStatusName = __( 'Unconfirmed', 'apollo' );
	    register_post_status('unconfirmed', array(
            'label'                     => $postStatusName,
            'public'                    => true,
            'exclude_from_search'       => true,
            'show_in_admin_all_list'    => true,
            'show_in_admin_status_list' => true,
            'label_count'               => _n_noop( "$postStatusName <span class='count'>(%s)</span>", "$postStatusName <span class='count'>(%s)</span>"),
        ));
    }
    
	/**
	 * Register Apollo taxonomies.
	 */
	public function register_taxonomies() {
		global $blog_id, $apollo_modules;

        // Get avaiable modules for this site
        $avaiable_modules = Apollo_App::get_avaiable_modules( $blog_id );
        
        if ( ! $avaiable_modules ) {
            return false;
        }

        // Register taxanomy for each module
        foreach ( $avaiable_modules as $key ):
            // Add organization taxanomy
            if ( isset( $apollo_modules[$key] ) ) :
                
                if ( isset( $apollo_modules[$key]['childs'] ) ) {
                    
                    foreach ( $apollo_modules[$key]['childs'] as $k1 => $m ) {
                        self::_create_taxanomy( $k1, $apollo_modules[$key]['childs'] );

                        if ( $k1 == Apollo_DB_Schema::_PROGRAM_PT ) {

                            /** #12782 get option*/
                            $add_types = array(
                                'artistic-discipline'   => 'Artistic Discipline',
                                'population-served'     => 'Population Served',
                                'subject'               => 'Subject',
                            );
                            $cultural_option = of_get_option(Apollo_DB_Schema::_EDUCATOR_ENABLE_CULTURAL_ORIGIN_DROP, true);
                            if($cultural_option){
                                $add_types['cultural-origin'] = 'Cultural Origin';
                            }
                            foreach( $add_types as $type => $lable ):
                                self::_create_taxanomy( $k1, $apollo_modules[$key]['childs'], array( 
                                    'type' => $type, 
                                    'single_name' => __( 'Program '.$lable.'', 'apollo' ),
                                    'menu_name' => _x( ''.$lable.'', 'Admin menu name', 'apollo' ),
                                    'search_items'  => __( 'Search '.$lable.'', 'apollo' ),
                                    'all_items'  => __( 'All '.$lable.'', 'apollo' ),
                                    'parent_items'  => __( 'Parent '.$lable.'', 'apollo' ),
                                    'parent_item_colon'  => __( 'Parent '.$lable.'', 'apollo' ),
                                    'edit_item'  => __( 'Edit '.$lable.'', 'apollo' ),
                                    'update_item'  => __( 'Update '.$lable.'', 'apollo' ),
                                    'add_new_item'  => __( 'Add New '.$lable.'', 'apollo' ),
                                    'new_item_name'  => __( 'New '.$lable.'', 'apollo' ),
                                ) );
                            endforeach;
                        }

                        
                        if ( $k1 == Apollo_DB_Schema::_EDU_EVALUATION ) {
                            self::_create_taxanomy( $k1, $apollo_modules[$key]['childs'], array( 
                                'type' => 'district-school', 
                                'single_name' => __( 'Edu Evaluation Districts & Schools', 'apollo' ),
                                'menu_name' => _x( 'District & School', 'Admin menu name', 'apollo' ),
                                'search_items'  => __( 'Search District & School', 'apollo' ),
                                'all_items'  => __( 'All District & School', 'apollo' ),
                                'parent_items'  => __( 'Parent District & School', 'apollo' ),
                                'parent_item_colon'  => __( 'Parent District & School', 'apollo' ),
                                'edit_item'  => __( 'Edit District & School', 'apollo' ),
                                'update_item'  => __( 'Update District & School', 'apollo' ),
                                'add_new_item'  => __( 'Add New', 'apollo' ),
                                'new_item_name'  => __( 'New', 'apollo' ),
                            ) );
                        }
                        
                    }
                } else {
                    // Thienld : skip creating taxonomy for post type Syndication
                    if($key == Apollo_DB_Schema::_SYNDICATION_PT){
                        continue;
                    }
                    if($key == Apollo_DB_Schema::_IFRAME_SEARCH_WIDGET_PT){
                        continue;
                    }
                    if($key == Apollo_DB_Schema::_PHOTO_SLIDER_PT){
                        continue;
                    }

                    /**
                     * @author vulh
                     * @ticket #19027 - 0002522: wpdev54 Customization - Custom venue slug
                     */
                    $customSlug = false;
                    if($key == Apollo_DB_Schema::_BUSINESS_PT || $key == Apollo_DB_Schema::_VENUE_PT){
                        $customSlug = Apollo_App::getTaxSlugByModuleName($key);
                    }

                    self::_create_taxanomy( $key, $apollo_modules, false, false, $customSlug );
                    
                    if ($key == Apollo_DB_Schema::_EVENT_PT) {
                        self::_create_taxanomy( $key, $apollo_modules, array(
                            'type' => Apollo_DB_Schema::_EVENT_TAG,
                            'single_name' => __('Event Tag', 'apollo'),
                            'menu_name' => _x( 'Tags', 'Admin menu name', 'apollo' ),
                            'search_items'  => __( 'Search Tag', 'apollo' ),
                            'all_items'  => __( 'All Tags', 'apollo' ),
                            'parent_items'  => __( 'Parent Tag', 'apollo' ),
                            'parent_item_colon'  => __( 'Parent Tag', 'apollo' ),
                            'edit_item'  => __( 'Edit Tag', 'apollo' ),
                            'update_item'  => __( 'Update Tag', 'apollo' ),
                            'add_new_item'  => __( 'Add New Tag', 'apollo' ),
                            'new_item_name'  => __( 'New Tag', 'apollo' ),
                            'hierarchical'  => false,
                            'taxonomy_name' => __('Event Tag', 'apollo'),
                        ), true );
                    }
                    
                }

                if ( $key == Apollo_DB_Schema::_ARTIST_PT ) {
                    self::_create_taxanomy( $key, $apollo_modules, array( 
                        'type' => 'artist-medium', 
                        'single_name' => __( 'Artist Mediums', 'apollo' ),
                        'menu_name' => _x( 'Artist Mediums', 'Admin menu name', 'apollo' ),
                        'search_items'  => __( 'Search Artist Medium', 'apollo' ),
                        'all_items'  => __( 'All Artist Mediums', 'apollo' ),
                        'parent_items'  => __( 'Parent Artist Medium', 'apollo' ),
                        'parent_item_colon'  => __( 'Parent Artist Medium', 'apollo' ),
                        'edit_item'  => __( 'Edit Aritst Medium', 'apollo' ),
                        'update_item'  => __( 'Update Artist Medium', 'apollo' ),
                        'add_new_item'  => __( 'Add New Artist Medium', 'apollo' ),
                        'new_item_name'  => __( 'New Artist Medium', 'apollo' ),
                    ) );
                    
                    self::_create_taxanomy( $key, $apollo_modules, array( 
                        'type' => 'artist-style', 
                        'single_name' => __( 'Artist Styles', 'apollo' ),
                        'menu_name' => _x( 'Artist Styles', 'Admin menu name', 'apollo' ),
                        'search_items'  => __( 'Search Artist Style', 'apollo' ),
                        'all_items'  => __( 'All Artist Styles', 'apollo' ),
                        'parent_items'  => __( 'Parent Artist Style', 'apollo' ),
                        'parent_item_colon'  => __( 'Parent Artist Style', 'apollo' ),
                        'edit_item'  => __( 'Edit Aritst Style', 'apollo' ),
                        'update_item'  => __( 'Update Artist Style', 'apollo' ),
                        'add_new_item'  => __( 'Add New Artist Style', 'apollo' ),
                        'new_item_name'  => __( 'New Artist Style', 'apollo' ),
                    ) );
                }
                if ( $key == Apollo_DB_Schema::_PUBLIC_ART_PT ) {
                    self::_create_taxanomy( $key, $apollo_modules, array(
                        'type' => Apollo_DB_Schema::_PUBLIC_ART_LOC,
                        'single_name' => __( 'Public Art Locations', 'apollo' ),
                        'menu_name' => _x( 'Locations', 'Admin menu name', 'apollo' ),
                        'search_items'  => __( 'Search Location', 'apollo' ),
                        'all_items'  => __( 'All Locations', 'apollo' ),
                        'parent_items'  => __( 'Parent Location', 'apollo' ),
                        'parent_item_colon'  => __( 'Parent Location', 'apollo' ),
                        'edit_item'  => __( 'Edit Location', 'apollo' ),
                        'update_item'  => __( 'Update Location', 'apollo' ),
                        'add_new_item'  => __( 'Add New Location', 'apollo' ),
                        'new_item_name'  => __( 'New Location', 'apollo' ),
                    ) );

                    self::_create_taxanomy( $key, $apollo_modules, array(
                        'type' => Apollo_DB_Schema::_PUBLIC_ART_COL,
                        'single_name' => __( 'Public Art Collections', 'apollo' ),
                        'menu_name' => _x( 'Collections', 'Admin menu name', 'apollo' ),
                        'search_items'  => __( 'Search Collection', 'apollo' ),
                        'all_items'  => __( 'All Collections', 'apollo' ),
                        'parent_items'  => __( 'Parent Collection', 'apollo' ),
                        'parent_item_colon'  => __( 'Parent Collection', 'apollo' ),
                        'edit_item'  => __( 'Edit Collection', 'apollo' ),
                        'update_item'  => __( 'Update Collections', 'apollo' ),
                        'add_new_item'  => __( 'Add New Collections', 'apollo' ),
                        'new_item_name'  => __( 'New Collections', 'apollo' ),
                    ) );

                    self::_create_taxanomy( $key, $apollo_modules, array(
                        'type' => Apollo_DB_Schema::_PUBLIC_ART_MED,
                        'single_name' => __( 'Public Art Mediums', 'apollo' ),
                        'menu_name' => _x( 'Mediums', 'Admin menu name', 'apollo' ),
                        'search_items'  => __( 'Search Medium', 'apollo' ),
                        'all_items'  => __( 'All Mediums', 'apollo' ),
                        'parent_items'  => __( 'Parent Medium', 'apollo' ),
                        'parent_item_colon'  => __( 'Parent Medium', 'apollo' ),
                        'edit_item'  => __( 'Edit Medium', 'apollo' ),
                        'update_item'  => __( 'Update Medium', 'apollo' ),
                        'add_new_item'  => __( 'Add New Medium', 'apollo' ),
                        'new_item_name'  => __( 'New Medium', 'apollo' ),
                    ) );
                }
               
            endif;
        endforeach;
        
		do_action( 'apollo_event_register_taxonomy' );
	}

    /**
	 * Register core post types
	 */
	public function register_post_types() {
        
		do_action( 'apollo_register_post_type' );

        global $blog_id, $apollo_modules;
        
        $active_modules = Apollo_App::get_avaiable_modules( $blog_id );
        
        $this->_create_post_type( Apollo_DB_Schema::_SPOT_PT, __( 'Home Spotlight', 'apollo' ), __( 'Home Spotlights', 'apollo' ) );


        if ( ! $active_modules ) {
            return false;
        }
        
        foreach( $active_modules as $am ) {
            if ( $am == 'post' || $am == Apollo_DB_Schema::_AGENCY_PT)                continue;

            /*@ticket #18192: Add Education spotlight*/
            if ( $am == Apollo_DB_Schema::_EDUCATION ) {
                $this->_create_post_type( Apollo_DB_Schema::_EDUCATION_SPOTLIGHT_PT,
                    __( 'Education Spotlight', 'apollo' ),
                    __( 'Education Spotlights', 'apollo' )
                );
            }

            if ( isset( $apollo_modules[$am] ) && ! isset( $apollo_modules[$am]['childs'] ) ) {
                $customArgs = isset($apollo_modules[$am]['custom_args']) && is_array($apollo_modules[$am]['custom_args']) ? $apollo_modules[$am]['custom_args'] : array();
                $this->_create_post_type( $am, $apollo_modules[$am]['sing'], $apollo_modules[$am]['plural'], $customArgs );

                /**
                 * Create new custom post type "Business-Spotlight" when Business module is active
                 * @ticket #11186
                 */
                if ( $am == Apollo_DB_Schema::_BUSINESS_PT ) {
                    $this->_create_post_type(
                        Apollo_DB_Schema::_BUSINESS_SPOTLIGHT_PT,
                        __('Business Spotlight', 'apollo'),
                        __('Business Spotlights', 'apollo'),
                        array()
                    );
                }

                /**
                 * Create new custom post type "Event-Spotlight" when Business module is active
                 * @ticket #13565
                 */
                if ( $am == Apollo_DB_Schema::_EVENT_PT ) {
                    $this->_create_post_type(
                        Apollo_DB_Schema::_EVENT_SPOTLIGHT_PT,
                        __('Event Spotlight', 'apollo'),
                        __('Event Spotlights', 'apollo'),
                        array()
                    );
                }

                /** @Ticket #19008 */
                if (in_array(Apollo_DB_Schema::_SYNDICATION_PT, $active_modules)) {
                    if (in_array(Apollo_DB_Schema::_ARTIST_PT, $active_modules)) {
                        $this->_create_post_type(
                            Apollo_DB_Schema::_SYNDICATION_ARTIST_PT,
                            __('Syndication Artist', 'apollo'),
                            __('Artists', 'apollo'),
                            array('supports' => array( 'title' )),
                            false
                        );
                    }

                    /**
                     * @ticket #19069:  [Organization] Syndication form
                     */
                    if (in_array(Apollo_DB_Schema::_ORGANIZATION_PT, $active_modules)) {
                        $this->_create_post_type( Apollo_DB_Schema::_SYNDICATION_ORG_PT,
                            __( 'Syndication Organization', 'apollo' ),
                            __( 'Organizations', 'apollo' ),
                            array('supports' => array('title')),
                            false
                        );
                    }

                }
            } else {
                if ($apollo_modules[$am]['childs']):
                    foreach( $apollo_modules[$am]['childs'] as $k => $m ):
                        $childCustomArgs = isset($m['custom_args']) && is_array($m['custom_args']) ? $m['custom_args'] : array();
                        $this->_create_post_type( $k, $m['sing'], $m['plural'], $childCustomArgs );
                    endforeach;
                endif;
            }
        }
	}

    private function _create_post_type( $type, $sing_name, $plural_name, $register_post_type_custom_args = array(), $show_in_menu = true ) {

        if ( post_type_exists( $type ) ) {
            return;
        } else {
            
        }

        $permalinks        = $this->permalinks;
		$post_type_permalink = empty( $permalinks[''. $type .'_base'] ) ? _x( $type, 'slug', 'apollo' ) : $permalinks[''. $type .'_base'];
        $post_type_permalink = Apollo_App::getCustomSlugByModuleName($post_type_permalink);
        $registerPostTypeData = array(
            'labels' => array(
                'name' 					=> __( $plural_name, 'apollo' ),
                'singular_name' 		=> __( $sing_name, 'apollo' ),
                'menu_name'				=> _x( $plural_name, 'Admin menu name', 'apollo' ),
                'add_new' 				=> __( 'Add', 'apollo' ),
                'add_new_item' 			=> __( 'Add New '. $sing_name .'', 'apollo' ),
                'edit' 					=> __( 'Edit', 'apollo' ),
                'edit_item' 			=> __( 'Edit ' . $sing_name . '', 'apollo' ),
                'new_item' 				=> __( 'New '. $sing_name .' ', 'apollo' ),
                'view' 					=> __( 'View '. $sing_name .'', 'apollo' ),
                'view_item' 			=> __( 'View ' . $sing_name . '', 'apollo' ),
                'search_items' 			=> __( 'Search '. $plural_name .' ', 'apollo' ),
                'not_found' 			=> __( 'No ' . $plural_name . ' found', 'apollo' ),
                'not_found_in_trash' 	=> __( 'No ' . $plural_name . ' found in trash', 'apollo' ),
                'parent' 				=> __( 'Parent '. $sing_name .'', 'apollo' )
            ),
            'description' 			=> '',
            'public' 				=> true,
            'has_archive'           => true,
            'show_ui' 				=> true,
            //'capability_type' 		=> $type,
            'map_meta_cap'			=> true,
            'publicly_queryable' 	=> true,
            'exclude_from_search' 	=> false,
            'hierarchical' 			=> false, // Hierarchical causes memory issues - WP loads all records!
            'rewrite' 				=> $post_type_permalink ? array( 'slug' => untrailingslashit( $post_type_permalink ), 'with_front' => false, 'feeds' => true ) : false,
            'query_var' 			=> true,
            'supports' 				=> array( 'title', 'editor', 'excerpt', 'thumbnail', 'comments', 'custom-fields', 'page-attributes','revisions', 'publicize' ),

            'show_in_nav_menus' 	=> true,
            'menu_icon'           => ''. home_url().'/wp-content/themes/apollo/inc/admin/assets/images/aflag-icon.png',
//            'capabilities' => array(
//                'edit_post' => 'edit_'.$type,
//                'edit_posts' => 'edit_'.$type. 's',
//                'edit_others_posts' => 'edit_other_'.$type. 's',
//                'publish_posts' => 'publish_'.$type. 's',
//                'read_post' => 'read_'.$type,
//                'read_private_posts' => 'read_private_'.$type. 's',
//                'delete_post' => 'delete_'.$type,
//            ),
        );

        /** @Ticket #19008 */
        if (!$show_in_menu) {
            $registerPostTypeData['show_in_menu'] = false;
        }

        if(!empty($register_post_type_custom_args)){
            $registerPostTypeData = array_replace($registerPostTypeData, $register_post_type_custom_args);
        }

		register_post_type( $type,
			apply_filters( 'apollo_register_post_type_'. $type .'', $registerPostTypeData)
		);
    }
    
    private static function _create_taxanomy( $key, $apollo_modules, $force_type = false, $force_slug = false, $customSlug = false ) {

        $type = $force_type ?  $force_type['type'] : $key. '-type';
        
        if ( taxonomy_exists( $type ) ) {
            return;
        }

        $sing_name = $apollo_modules[$key]['sing'];

        if ( $force_slug ) {
            $slug_type = $type;
        } else {
            $slug_type = isset( $apollo_modules[$key]['taxonomy_slug'] ) ? $apollo_modules[$key]['taxonomy_slug'] : $type;
        }

        /**
         * @ticket #18849: [Business] Modify the URL path
         */
        if($customSlug){
            $slug_type = $customSlug;
        }

        if ( $force_type ) {
            $single_name = $force_type['single_name'];
            $menu_name = $force_type['menu_name'];
            $search_items = $force_type['search_items'];
            $all_items = $force_type['all_items'];
            $parent_items = $force_type['parent_items'];
            $parent_item_colon = $force_type['parent_item_colon'];
            $edit_item = $force_type['edit_item'];
            $update_item = $force_type['update_item'];
            $add_new_item = $force_type['add_new_item'];
            $new_item_name = $force_type['new_item_name'];
            $tax_name  = isset( $force_type['taxonomy_name'] ) ? $force_type['taxonomy_name'] : $single_name;
        } else {
            $single_name = sprintf( __( '%s Categories' ), $sing_name );
            $menu_name = sprintf( __( '%s Categories' ), '' );
            $search_items = __( "Search {$sing_name}", 'apollo' );
            $all_items = sprintf( __( 'All %s Categories', 'apollo' ), '' );
            $parent_items = sprintf( __( 'Parent %s Categories', 'apollo' ), '' );
            $parent_item_colon = sprintf( __( 'Parent %s Categories', 'apollo' ), '' );
            $edit_item = sprintf( __( 'Edit %s Categories', 'apollo' ), '' );
            $update_item = sprintf( __( 'Update %s Categories', 'apollo' ), '' );
            $add_new_item = sprintf( __( 'Add new %s Categories', 'apollo' ), '' );
            $new_item_name = sprintf( __( 'New %s Categories', 'apollo' ), '' );
            $tax_name  = isset( $apollo_modules[$key]['taxonomy_name'] ) ? $apollo_modules[$key]['taxonomy_name'] : $single_name;
        }
        

        
        if ( $key == Apollo_DB_Schema::_EVENT_PT ) {
            global $pagenow;
            if ( $pagenow == 'post-new.php' || $pagenow == 'post.php'  ) {
                $tax_name = __( 'Additional Category', 'apollo' );
            }
        }
        if($key == Apollo_DB_Schema::_EDUCATOR_PT){
            $tax_name =  __( 'Educator Types','apollo' );
            $search_items = __( "Search {$sing_name}", 'apollo' );
            $single_name = __( 'Educator Types','apollo' );
            $edit_item =  __( 'Edit Educator Types','apollo' );
            $all_items =  __( 'All Educator Types','apollo' );
            $menu_name =  __( 'Educator Types','apollo' );
            $add_new_item = __( 'Add new Educator type','apollo' );
            $parent_items = __( 'Parent Educator Types','apollo' );
            $parent_item_colon = __( 'Parent Educator Types','apollo' );
            $update_item = __( 'Update Educator Types','apollo' );
            $new_item_name = __( 'New Educator Types','apollo' );
        }

        register_taxonomy( $type,
            apply_filters( "apollo_objects_{$type}", array( $key ) ),
            apply_filters( "apollo_args_{$type}", array(
                'hierarchical' 			=> isset($force_type['hierarchical']) ? $force_type['hierarchical'] : true,                     
                'label' 				=> __( $sing_name, 'Apollo' ),
                'labels' => array(
                        'name' 				=> $tax_name,
                        'singular_name' 	=> $single_name,
                        'menu_name'			=> $menu_name,
                        'search_items' 		=> $search_items,
                        'all_items' 		=> $all_items,
                        'parent_item' 		=> $parent_items,
                        'parent_item_colon' => $parent_item_colon,
                        'edit_item' 		=> $edit_item,
                        'update_item' 		=> $update_item,
                        'add_new_item' 		=> $add_new_item,
                        'new_item_name' 	=> $new_item_name
                    ),
                'show_ui'       => true,
                'query_var' 			=> true,
                'rewrite' 				=> array( 'slug' => $slug_type ),
                'public'  => true,
//                'capabilities' => array(
//                    'manage_terms' => 'manage_'. $type,
//                    'edit_terms' => 'manage_'. $type,
//                    'delete_terms' => 'manage_'. $type,
//                    'assign_terms' => 'administrator',
//                )
            ) )
        );
    }
    
    
}

new Apollo_Post_Types();


class Apollo_WP_Disable_Posts {
    
	public function __construct() {
		global $pagenow;
		/* checks the request and redirects to the dashboard */
		add_action( 'init', array( __CLASS__, 'disallow_post_type_post') );
		/* removes Post Type `Post` related menus from the sidebar menu */
		add_action( 'admin_menu', array( __CLASS__, 'remove_post_type_post' ) );
		if ( !is_admin() && ($pagenow != 'wp-login.php') ) {
			/* need to return a 404 when post_type `post` objects are found */
			add_action( 'posts_results', array( __CLASS__, 'check_post_type' ) );
			/* do not return any instances of post_type `post` */
			add_filter( 'pre_get_posts', array( __CLASS__, 'remove_from_search_filter' ) );
		}
	}
	/**
	 * checks the request and redirects to the dashboard
	 * if the user attempts to access any `post` related links
	 *
	 * @access public
	 * @param none
	 * @return void
	 */
	public static function disallow_post_type_post() {
		global $pagenow;
		switch( $pagenow ) {
			case 'edit.php':
			case 'edit-tags.php':
			case 'post-new.php':
				if ( !array_key_exists('post_type', $_GET) && !array_key_exists('taxonomy', $_GET) && !$_POST ) {
					wp_safe_redirect( get_admin_url(), 301 );
					exit;
				}
            break;
		}
	}
	/**
	 * loops through $menu and $submenu global arrays to remove any `post` related menus and submenu items
	 *
	 * @access public
	 * @param none
	 * @return void
	 *
	 */
	public static function remove_post_type_post() {
		global $menu, $submenu;
		/*
			edit.php
			post-new.php
			edit-tags.php?taxonomy=category
			edit-tags.php?taxonomy=post_tag
		 */
		$done = false;
		foreach( $menu as $k => $v ) {
			foreach($v as $key => $val) {
				switch($val) {
					case 'Posts':
						unset($menu[$k]);
						$done = true;
						break;
				}
			}
			/* bail out as soon as we are done */
			if ( $done ) {
				break;
			}
		}
		$done = false;
		foreach( $submenu as $k => $v ) {
			switch($k) {
				case 'edit.php':
					unset($submenu[$k]);
					$done = true;
					break;
			}
			/* bail out as soon as we are done */
			if ( $done ) {
				break;
			}
		}
	}
    
	/**
	 * checks the SQL statement to see if we are trying to fetch post_type `post`
	 *
	 * @access public
	 * @param array $posts,  found posts based on supplied SQL Query ($wp_query->request)
	 * @return array $posts, found posts
	 */
	public static function check_post_type( $posts = array() ) {
		global $wp_query;
		$look_for = "wp_posts.post_type = 'post'";
		$instance = strpos( $wp_query->request, $look_for );
	
		if ( $instance !== false ) {
			$posts = array(); // we are querying for post type `post`
		}
		return $posts;
	}
    
	/**
	 * excludes post type `post` to be returned from search
	 *
	 * @access public
	 * @param null
	 * @return object $query, wp_query object
	 */
	public static function remove_from_search_filter( $query )
	{
		if ( !is_search() ) {
			return $query;
		}
		$post_types = get_post_types();
		if ( array_key_exists('post', $post_types) ) {
			/* exclude post_type `post` from the query results */
			unset( $post_types['post'] );
		}
		$query->set( 'post_type', array_values($post_types) );
		return $query;
	}
}