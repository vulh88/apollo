<?php
if(is_home()):
    get_template_part('templates/content-home', 'page');
else:
    get_template_part('templates/content', 'page');
endif;
