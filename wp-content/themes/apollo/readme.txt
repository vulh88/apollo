== v2.1.5 ==
* change the order of the PAST EVENTS so that the most recently expired event is first
* add a PAST EVENTS tab to the venue detail page (same position and order as the Org past events tab)
* add location to custom widget

== v2.1.6 ==
* #6826 - SEARCH MODULE
* Place all current settings of event search (Theme Options -> Site Config -> Search) into section called "Event"
* add another section in Theme Options -> Site Config -> Search called "General" (place above "Event" section) with these options
* Additional configuration to allow site admin configures the default number of items in first shown in search result page (default = 24 items)
* Additional configuration to allow site admin configures the "View More" number in search result page
* Search configuration will be applied for both GLOBAL and MODULE search

* #6831
* [Optimize caching] - Event Category Page - Featured Events is duplicated with SpotLight Event
* Once event category page is cached, the first of featured events is duplicated with SpotLight Event.
* Expectation: Follow apollo event category rule:
*  + Featured events must contain all items differ to Spotlight Event.
*  + The other events must contain all item differ to Spotlight Event and Featured Events.