<?php

class Apollo_Upgrade_Tool {
    
    protected $siteId = '';


    public function __construct() {
        echo '<h1>Apollo Upgrade</h1><br/>';
        
        echo "<ul>"; ?>
            <li>Use case=1 parameter to update the venue zip and venue city</li>
            <li>Use case=2 parameter to update the discount url</li>
        <?php
        echo "</ul>";
        
        $case = isset($_GET['case']) ? $_GET['case'] : '';
        
        if ( $case ) {
            switch($case):
                case 1:
                    $this->updateVenueZipCity();
                    break;
                case 2: 
                    $this->updateEventDiscountUrl();
                    break;
                case 3:
                    $this->updateTerritoryOpt();
                    break;
                default:
                    echo 'No action';
            endswitch;
        }
        exit;
    }
    
    private function getTableName($tbl) {
        global $wpdb;
        $siteId = is_main_site($this->siteId) ? '' : $this->siteId. '_';
        return $wpdb->prefix.''.$siteId. $tbl;
    }
    
    function updateVenueZipCity() {
        
        $siteId = isset($_GET['site_id']) ? $_GET['site_id'] : '';
      
        if ( !$siteId ) {
            echo 'Site ID is empty';
            exit;
        }
        
        $this->siteId = $siteId;
      
        $aplQuery = new Apl_Query($this->getTableName(Apollo_Tables::_APL_VENUE_META), true);
        $results = $aplQuery->get_where("meta_key = '".Apollo_DB_Schema::_APL_VENUE_ADDRESS."' ");
        
        if ( ! $results ) {
            echo 'No data';
            exit;
        }
        $countZip = $countCity = 0;
        foreach ( $results as $result ) {
            $isExistVenueZip = $aplQuery->get_row('meta_key = "'.Apollo_DB_Schema::_VENUE_ZIP.'" AND apollo_venue_id = '.$result->apollo_venue_id.' ');
            if ( !$isExistVenueZip ) {
                $addressData = maybe_unserialize($result->meta_value);
                if ($addressData && isset($addressData[Apollo_DB_Schema::_VENUE_ZIP])) {
                    $countZip++;
                    $aplQuery->insert(array(
                        'apollo_venue_id' => $result->apollo_venue_id,
                        'meta_key'        => Apollo_DB_Schema::_VENUE_ZIP,
                        'meta_value'      =>   $addressData[Apollo_DB_Schema::_VENUE_ZIP]
                    ));
                    
                    echo '<p>Insert zip of: venue_id = '.$result->apollo_venue_id.' - Zip = '.$addressData[Apollo_DB_Schema::_VENUE_ZIP].'</p>';
                }
            }
            
            $isExistVenueCity = $aplQuery->get_row('meta_key = "'.Apollo_DB_Schema::_VENUE_CITY.'" AND apollo_venue_id = '.$result->apollo_venue_id.' ');
            if ( !$isExistVenueCity ) {
                $addressData = maybe_unserialize($result->meta_value);
                if ($addressData && isset($addressData[Apollo_DB_Schema::_VENUE_CITY])) {
                    $countCity++;
                    $aplQuery->insert(array(
                        'apollo_venue_id' => $result->apollo_venue_id,
                        'meta_key'        => Apollo_DB_Schema::_VENUE_CITY,
                        'meta_value'      =>   $addressData[Apollo_DB_Schema::_VENUE_CITY]
                    ));
                    echo '<p>Insert City of: venue_id = '.$result->apollo_venue_id.' - City = '.$addressData[Apollo_DB_Schema::_VENUE_CITY].'</p>';
                }
            }
            
        }
        
        echo sprintf('<p>There are %s venues</p>', $results ? count($results) : 0);
        echo sprintf('<p>There are %s updated zip</p>', $results ? $countZip : 0);
        echo sprintf('<p>There are %s updated City</p>', $results ? $countCity : 0);
        
        exit;
    }
    
    function updateEventDiscountUrl() {
        
        $siteId = isset($_GET['site_id']) ? $_GET['site_id'] : '';
      
        if ( !$siteId ) {
            echo 'Site ID is empty';
            exit;
        }
        
        $this->siteId = $siteId;
      
        $aplQuery = new Apl_Query($this->getTableName(Apollo_Tables::_APOLLO_EVENT_META), true);
        $results = $aplQuery->get_where("meta_key = '".Apollo_DB_Schema::_APOLLO_EVENT_DATA."' ");
        
        if ( ! $results ) {
            echo 'No data';
            exit;
        }
        $count = 0;
        foreach ( $results as $result ) {
            $isExist = $aplQuery->get_row('meta_key = "'.Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL.'" AND apollo_event_id = '.$result->apollo_event_id.' ');
            if ( !$isExist ) {
                $metaData = maybe_unserialize($result->meta_value);
                if ($metaData && isset($metaData[Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL])) {
                    $count++;
                    $aplQuery->insert(array(
                        'apollo_event_id' => $result->apollo_event_id,
                        'meta_key'        => Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL,
                        'meta_value'      =>   $metaData[Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL]
                    ));
                    echo '<p>Insert discount url of: event_id = '.$result->apollo_event_id.' - Discount URL = '.$metaData[Apollo_DB_Schema::_ADMISSION_DISCOUNT_URL].'</p>';
                }
            }
            
        }
        
        echo sprintf('<p>There are %s events</p>', $results ? count($results) : 0);
        echo sprintf('<p>There are %s updated Discount URL</p>', $results ? count($count) : 0);
        
        exit;
    }

    function updateTerritoryOpt() {

        $states = of_get_option(Apollo_DB_Schema::_TERR_STATES);
        $cities = of_get_option(Apollo_DB_Schema::_TERR_CITIES);
        $zips = of_get_option(Apollo_DB_Schema::_TERR_ZIPCODE);

	aplDebug($states);
        aplDebug($cities);
        aplDebug($zips);
        

        $terrData = array();
        if ($states) {

            foreach($states as $state => $v) {
                $terrData[$state] = array();
            }

            $cities = explode("<>", $cities);

            if ($cities) {
                $cities = array_unique($cities);

                foreach($cities as $city) {
                    if (!$city) continue;
                    $cityData = explode(" (", $city);
                    $_state = trim(str_replace(")", "", $cityData[1]));

                    if (isset($terrData[$_state])) {
                        $terrData[$_state][$cityData[0]] = array();
                    }
                }
            }

            if ($zips) {
                $zips = explode("<>", $zips);

                global $wpdb;
                $aplQuery = new Apl_Query('wp_apollo_state_zip');
                foreach($zips as $zip) {

                    $zipData = $aplQuery->get_row("zipcode='$zip'");
                    if ($zipData && isset($terrData[$zipData->state_prefix][$zipData->city])) {
                        $terrData[$zipData->state_prefix][$zipData->city][$zip]='on';
                    }
                }
            }

        }
        echo '<p>New Territory data</p>';
        aplDebug($terrData);
        echo "<p>---------------------------------------------------------------</p>";

        $oldThemeOpt = get_option('_apollo_theme_options');
        echo "<p>Old theme option</p>";
        aplDebug($oldThemeOpt);

        echo "<p>---------------------------------------------------------------</p>";
        echo '<p>set update=1 to update new data</p>';
        if (isset($_GET['update']) && $_GET['update'] == 1) {
            $oldThemeOpt[Apollo_DB_Schema::_TERR_DATA] = $terrData;
            aplDebug($oldThemeOpt);
            update_option('_apollo_theme_options', $oldThemeOpt);
        }

    }
    
}

$ob = new Apollo_Upgrade_Tool();