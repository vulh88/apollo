<?php

if ( defined( 'COOKIE_DOMAIN' ) || defined('PLUGINS_COOKIE_PATH') || defined('ADMIN_COOKIE_PATH') || defined('COOKIEPATH')) {
    die( 'The constant "COOKIE_DOMAIN" or "PLUGINS_COOKIE_PATH" or "ADMIN_COOKIE_PATH" or "COOKIEPATH" is defined (probably in wp-config.php). Please remove or comment out that define() line.' );
}

define( 'COOKIE_DOMAIN', $_SERVER[ 'HTTP_HOST' ] );
define('PLUGINS_COOKIE_PATH', '/');
define('ADMIN_COOKIE_PATH', '/');
define('COOKIEPATH', '/');

/*
 * Configuratin sync between two place. class-apollo-const and here
 * */
define('_APL_BLOGUSER', 'apollo_blog_user');
/*
    Foot note:

    Don't user new WP_User('', $name) # We don't $name can be duplicated Use ID instead
*/
function get_list_super_admin() {
    global $wpdb;
    $super_admins = get_site_option( 'site_admins', array( 'admin' ) );

    $qsa = '("' . implode("\",\"", $super_admins) . '")';

    $sql = "SELECT * FROM $wpdb->users WHERE user_login in $qsa; ";
    $arruser = $wpdb->get_results($sql);

    return $arruser;
}

function get_list_email_super_admin() {
    $auser = get_list_super_admin();

    $aE = array();

    foreach($auser as $_ => $v) {
        $aE[] = $v->user_email;
    }

    return $aE;
}

function wp_set_current_user($id) {
	global $current_user;

	if ( isset( $current_user ) && ( $current_user instanceof WP_User ) && ( $id == $current_user->ID ) )
		return $current_user;

	$current_user = new WP_User( $id);

	setup_userdata( $current_user->ID );

	/**
	 * Fires after the current user is set.
	 *
	 * @since 2.0.1
	 */
	do_action( 'set_current_user' );

	return $current_user;
}


/**
 * pluggable
 */
function get_user_by($field, $value) {

    $blog_id = $GLOBALS['blog_id'];
    if(isset($_REQUEST['blog_id'])) {
        $blog_id = $_REQUEST['blog_id'];
    }

    // New flow
    $userdata = apl_get_data_by($field, $value, $blog_id);

    if ( !$userdata )
        return false;

    $user = new WP_User;
    $user->init( $userdata );

    return $user;
}


/* Utility for Apollo Helper - Just get for normal user - not for administrator */
function apl_get_data_by( $field, $value , $blog_id) {
    global $wpdb;

    /* Check blog_id is exist  */
    $blog_id = intval($blog_id);
    if(intval($blog_id) === 0)
        throw new InvalidArgumentException(__('Wrong blog_id use $GLOBALS["blog_id"] instead', 'apollo'));


    if ( 'id' == $field ) {
        // Make sure the value is numeric to avoid casting objects, for example,
        // to int 1.
        if ( ! is_numeric( $value ) )
            return false;
        $value = intval( $value );
        if ( $value < 1 )
            return false;

        return WP_User::get_data_by('id', $value);
    } else {
        $value = trim( $value );
    }

    if ( !$value )
        return false;

    if($field === 'email' && $GLOBALS['pagenow'] ==='site-new.php' && defined('WP_NETWORK_ADMIN') && WP_NETWORK_ADMIN === true) {
        $aE = get_list_email_super_admin();

        if(in_array($value, $aE)) {
            wp_die('This email has exist. Please choice another one');
        }

        return false;
    }

    switch ( $field ) {
        case 'id':
            $user_id = $value;
            $db_field = 'ID';
            break;
        case 'slug':
            /* It still work good in query_post. because we have blog_id already */
            $user_id = wp_cache_get($value, 'userslugs');
            $db_field = 'user_nicename';
            break;
        case 'email':
            $user_id = wp_cache_get($value, 'useremail');
            $db_field = 'user_email';
            break;
        case 'login':
            $value = sanitize_user( $value );
            $user_id = wp_cache_get($value, 'userlogins');
            $db_field = 'user_login';
            break;
        default:
            return false;
    }

    if ( false !== $user_id ) {
        if ( $user = wp_cache_get( $user_id, 'users' ) ){
            return $user;
        }
    }

    // Process user and cached it
    $bloguser_name = $wpdb->base_prefix._APL_BLOGUSER;
    if($field !== 'id') {
        $user = $wpdb->get_row( $wpdb->prepare(
                "SELECT tbluser.*, tblbun.blog_id as blog_id FROM $wpdb->users as tbluser
                                INNER JOIN $bloguser_name as tblbun ON (tblbun.user_id = tbluser.ID AND tblbun.blog_id = %s)
        WHERE tbluser.$db_field = %s ", intval($blog_id) , $value
            ) );
    }


    if(empty($user)) return false;

    update_user_caches( $user );
    return $user;
}

function apl_check_password_reset_key($key, $login) {
    global $wpdb, $wp_hasher;

    $blog_id = $GLOBALS['blog_id'];

    $key = preg_replace('/[^a-z0-9]/i', '', $key);

    if ( empty( $key ) || !is_string( $key ) )
        return new WP_Error('invalid_key', __('Invalid key'));

    if ( empty($login) || !is_string($login) )
        return new WP_Error('invalid_key', __('Invalid key'));

    /* Get id, and user_activation_key */
    $bloguser_name = $wpdb->base_prefix._APL_BLOGUSER;

    $row = $wpdb->get_row( $wpdb->prepare( "
    SELECT tbluser.ID, tbluser.user_login,  tbluser.user_activation_key, tblbun.blog_id as blog_id
    FROM $wpdb->users as tbluser
         INNER JOIN $bloguser_name as tblbun
                   ON (tblbun.blog_id = %s AND tblbun.user_id = tbluser.ID)
    WHERE tbluser.user_login = %s", $blog_id, $login ) );

    if ( ! $row )
        return new WP_Error('invalid_key', __('Invalid key'));

    if ( empty( $wp_hasher ) ) {
        require_once ABSPATH . WPINC . '/class-phpass.php';
        $wp_hasher = new PasswordHash( 8, true );
    }

    if ( $wp_hasher->CheckPassword( $key, $row->user_activation_key ) )
        return get_userdata( $row->ID );

    if ( $key === $row->user_activation_key ) {
        $return = new WP_Error( 'expired_key', __( 'Invalid key' ) );
        $user_id = $row->ID;

        /**
         * Filter the return value of check_password_reset_key() when an
         * old-style key is used (plain-text key was stored in the database).
         *
         * @since 3.7.0
         *
         * @param WP_Error $return  A WP_Error object denoting an expired key.
         *                          Return a WP_User object to validate the key.
         * @param int      $user_id The matched user ID.
         */
        return apply_filters( 'password_reset_key_expired', $return, $user_id );
    }

    return new WP_Error( 'invalid_key', __( 'Invalid key' ) );
}


function auth_redirect() {
    // Checks if a user is logged in, if not redirects them to the login page

    $secure = ( is_ssl() || force_ssl_admin() );

    /**
     * Filter whether to use a secure authentication redirect.
     *
     * @since 3.1.0
     *
     * @param bool $secure Whether to use a secure authentication redirect. Default false.
     */
    $secure = apply_filters( 'secure_auth_redirect', $secure );

    remove_action( 'personal_options_update', 'send_confirmation_on_profile_email' );
    add_action( 'personal_options_update', 'apollo_send_confirmation_on_profile_email' );

    // If https is required and request is http, redirect
    if ( $secure && !is_ssl() && false !== strpos($_SERVER['REQUEST_URI'], 'wp-admin') ) {
        if ( 0 === strpos( $_SERVER['REQUEST_URI'], 'http' ) ) {
            wp_redirect( set_url_scheme( $_SERVER['REQUEST_URI'], 'https' ) );
            exit();
        } else {
            wp_redirect( 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );
            exit();
        }
    }

    if ( is_user_admin() ) {
        $scheme = 'logged_in';
    } else {
        /**
         * Filter the authentication redirect scheme.
         *
         * @since 2.9.0
         *
         * @param string $scheme Authentication redirect scheme. Default empty.
         */
        $scheme = apply_filters( 'auth_redirect_scheme', '' );
    }

    if ( $user_id = wp_validate_auth_cookie( '',  $scheme) ) {
        /**
         * Fires before the authentication redirect.
         *
         * @since 2.8.0
         *
         * @param int $user_id User ID.
         */
        do_action( 'auth_redirect', $user_id );

        // If the user wants ssl but the session is not ssl, redirect.
        if ( !$secure && get_user_option('use_ssl', $user_id) && false !== strpos($_SERVER['REQUEST_URI'], 'wp-admin') ) {
            if ( 0 === strpos( $_SERVER['REQUEST_URI'], 'http' ) ) {
                wp_redirect( set_url_scheme( $_SERVER['REQUEST_URI'], 'https' ) );
                exit();
            } else {
                wp_redirect( 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );
                exit();
            }
        }

        return;  // The cookie is good so we're done
    }

    // The cookie is no good so force login
    nocache_headers();

    $redirect = ( strpos( $_SERVER['REQUEST_URI'], '/options.php' ) && wp_get_referer() ) ? wp_get_referer() : set_url_scheme( 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );

    $login_url = wp_login_url($redirect, true);

    wp_redirect($login_url);
    exit();
}


function apollo_send_confirmation_on_profile_email() {
    global $errors, $wpdb, $blog_id;
    $current_user = wp_get_current_user();
    if ( ! is_object($errors) )
        $errors = new WP_Error();

    if ( $current_user->ID != $_POST['user_id'] )
        return false;

    if ( $current_user->user_email != $_POST['email'] ) {
        if ( !is_email( $_POST['email'] ) ) {
            $errors->add( 'user_email', __( "<strong>ERROR</strong>: The email address isn&#8217;t correct." ), array( 'form-field' => 'email' ) );
            return;
        }

        /**
         * @todo Need overide here. bug same email on network not on local site
         */
        if ( $wpdb->get_var( $wpdb->prepare( "SELECT user_email FROM {$wpdb->users} WHERE user_email=%s", $_POST['email'] ) ) ) {
            $errors->add( 'user_email', __( "<strong>ERROR</strong>: The email address is already used." ), array( 'form-field' => 'email' ) );
            delete_option( $current_user->ID . '_new_email' );
            return;
        }

        $hash = md5( $_POST['email'] . time() . mt_rand() );
        $new_user_email = array(
            'hash' => $hash,
            'newemail' => $_POST['email']
        );
        update_option( $current_user->ID . '_new_email', $new_user_email );

        $email_text = __( 'Dear user,

You recently requested to have the email address on your account changed.
If this is correct, please click on the following link to change it:
###ADMIN_URL###

You can safely ignore and delete this email if you do not want to
take this action.

This email has been sent to ###EMAIL###

Regards,
All at ###SITENAME###
###SITEURL###' );

        /**
         * Filter the email text sent when a user changes emails.
         *
         * The following strings have a special meaning and will get replaced dynamically:
         * ###ADMIN_URL### The link to click on to confirm the email change. Required otherwise this functunalty is will break.
         * ###EMAIL### The new email.
         * ###SITENAME### The name of the site.
         * ###SITEURL### The URL to the site.
         *
         * @since MU
         *
         * @param string $email_text     Text in the email.
         * @param string $new_user_email New user email that the current user has changed to.
         */
        $content = apply_filters( 'new_user_email_content', $email_text, $new_user_email );

        $content = str_replace( '###ADMIN_URL###', esc_url( admin_url( 'profile.php?newuseremail='.$hash ) ), $content );
        $content = str_replace( '###EMAIL###', $_POST['email'], $content);
        $content = str_replace( '###SITENAME###', get_site_option( 'site_name' ), $content );
        $content = str_replace( '###SITEURL###', network_home_url(), $content );

        wp_mail( $_POST['email'], sprintf( __( '[%s] New Email Address' ), wp_specialchars_decode( get_option( 'blogname' ) ) ), $content );
        $_POST['email'] = $current_user->user_email;
    }
}

add_action('wpmu_new_user', 'apollo_wpmu_new_user', 0, 1);

function apollo_wpmu_new_user($user_id) {
    if(defined('WP_NETWORK_ADMIN') && WP_NETWORK_ADMIN === true && $GLOBALS['pagenow'] === 'site-new.php') return;

    global $wpdb;

    $blog_id = $GLOBALS['blog_id'];
    $tbl_blog_user = $wpdb->base_prefix._APL_BLOGUSER;

    $result = $wpdb->query($wpdb->prepare("
    INSERT INTO $tbl_blog_user (user_id, blog_id) VALUES (%s, %s)
    ON DUPLICATE KEY UPDATE user_id = user_id
    ", intval($user_id), intval($blog_id)));

    if($result >= 1) {
        $sql = "select user_login, user_email from $wpdb->users where id = %s";
        $_user = $wpdb->get_row($wpdb->prepare($sql, $user_id));

        if(is_object($_user)) {
            $sql = "DELETE FROM $wpdb->signups WHERE user_login = %s or user_email = %s";
            $wpdb->query($wpdb->prepare($sql, $_user->user_login, $_user->user_email));
        }

        // check come from some source
        if($GLOBALS['pagenow'] === 'user-new.php' && WP_NETWORK_ADMIN === true) {
            add_user_to_blog( $blog_id, $user_id, 'subscriber' );
            update_user_meta( $user_id, 'primary_blog', $blog_id );
            $details = get_blog_details($blog_id);
            update_user_meta($user_id, 'source_domain', $details->domain);
        }
    }
}

function apollo_add_user_to_blog($user_id, $role, $blog_id) {
    global $wpdb;

    $tbl_blog_user = $wpdb->base_prefix._APL_BLOGUSER;

    $result = $wpdb->query($wpdb->prepare("
    INSERT INTO $tbl_blog_user (user_id, blog_id) VALUES (%s, %s)
    ON DUPLICATE KEY UPDATE user_id = user_id
    ", intval($user_id), intval($blog_id)));
}

add_action('add_user_to_blog', 'apollo_add_user_to_blog', 10, 3);

add_action('remove_user_from_blog', 'apollo_remove_user_from_blog', 10, 2);

function apollo_helper_remove_user($arr) {

    # @vulh Fix delete user when add a new user because wordpress add hook remove_user_from_blog in the add_new_user_to_blog function
    if ($_POST && isset($_POST['action']) && $_POST['action'] == 'createuser') {
        return;
    }

    $blog_id = $arr[1]; $user_id = $arr[0];

    $arr_admin_id = get_list_super_admin_id();
    if(in_array($user_id, $arr_admin_id))
        return;

    global $wpdb;
    $tbl_blog_user = $wpdb->base_prefix._APL_BLOGUSER;
    $sql = "DELETE FROM $tbl_blog_user WHERE blog_id = %s and user_id = %s";
    if(empty($blog_id) && !empty($user_id)) {
        $sql = "DELETE FROM $tbl_blog_user WHERE user_id = %s"; /* Don't delete admin of course - Dangerous */
        $naffected_rows = $wpdb->query($wpdb->prepare($sql, $user_id));
    }
    else {
        $naffected_rows = $wpdb->query($wpdb->prepare($sql, $blog_id, $user_id));
    }

    if($naffected_rows !== false) {
        // delete from wpdb->users
        $sql = "DELETE FROM $wpdb->users WHERE ID = '%s'";
        $naffected_rows = $wpdb->query($wpdb->prepare($sql, $user_id));

        if($naffected_rows !== false) {
            // delete all from wpdb->usermeta
            $sql = "DELETE FROM $wpdb->usermeta WHERE user_id = '%s'";
            $naffected_rows = $wpdb->query($wpdb->prepare($sql, $user_id));
        }
    }
}

function apollo_remove_user_from_blog($user_id, $blog_id ){
    $arrcb = debug_backtrace(5);
    if(isset($arrcb['4']['function']) && $arrcb[4]['function'] === 'add_new_user_to_blog') {
        return false;
    }

    register_shutdown_function('apollo_helper_remove_user', array($user_id, $blog_id));
}

function apollo_hidden_create_existing_form() {
    // Hiden create existing user form another blog
    ?>
    <script>
        var $h3_eu = jQuery('#add-existing-user');
        jQuery($h3_eu.next().next()).hide();
        jQuery($h3_eu.next()).hide();
        jQuery($h3_eu).hide();
    </script>
<?php
}

add_action('user_new_form', 'apollo_hidden_create_existing_form', 10, 2);

function get_list_super_admin_id() {
    $auser = get_list_super_admin();

    $aI = array();

    foreach($auser as $_ => $v) {
        $aI[] = $v->ID;
    }

    return $aI;
}



function apollo_delete_blog_remove_super_admin($blog_id, $drop) {
    global $wpdb;
    $bloguser_name = $wpdb->base_prefix._APL_BLOGUSER;

    $query = "delete from $bloguser_name where blog_id = '%s' ;";

    $r = $wpdb->query($wpdb->prepare($query, $blog_id));

}
add_action('delete_blog', 'apollo_delete_blog_remove_super_admin', 10, 2);

add_action('personal_options', 'apollo_check_email_dont_exist_same_blog', 10, 1);

add_action('wp_ajax_apollo_check_email_dont_exist_same_blog', 'apollo_check_email_dont_exist_same_blog_ajax');

function apollo_check_email_dont_exist_same_blog_ajax() {
    global $wpdb;
    $bloguser_name = $wpdb->base_prefix._APL_BLOGUSER;

    $user_id = sanitize_text_field($_GET['user_id']);
    $user_email = sanitize_text_field($_GET['email']);

    if(!wp_verify_nonce(sanitize_text_field($_GET['nonce']), 'check-exist-email')) {
        wp_send_json(array(
            'status'=> 'fail',
            'msg' => __("Miss token. Please refresh page !", 'apollo')
        ));
    }

    $query = "select bu.blog_id from $bloguser_name as bu where bu.user_id = '%s' ;";

    $r = $wpdb->get_var($wpdb->prepare($query, $user_id));

    if($r !== false) {
        $blog_id = $r;
        $_query = "select 1 from $wpdb->users as u inner join $bloguser_name as bu on bu.user_id = u.ID and bu.blog_id = '%s' and u.user_email = '%s' ;";
        $_r = $wpdb->query($wpdb->prepare($_query, $blog_id, $user_email));
        if($_r >= 1 || $_r === false) {
            wp_send_json(array(
                'status'=> 'fail',
                'msg' => __("There are a user with the same email is exists in same blog.\n\nPlease choice another email.", 'apollo')
            ));
        }
    }

    wp_send_json(array(
        'status'=> 'ok',
    ));
}

function apollo_check_email_dont_exist_same_blog($user) {

    if(defined('WP_NETWORK_ADMIN') && WP_NETWORK_ADMIN === true && $GLOBALS['pagenow'] === 'user-edit.php') {
        ?>
        <script>
            (function ($) {

                function showLoading() {
                    if(!$('#_loader').length){
                        $('body').append("<div id='_loader' style='position: fixed; width: 100%; height: 100%; display: block; background: rgba(0, 0, 0, 0.8); z-index: 99999; top: 0;'><i class='fa fa-spinner fa-spin fa-3x' style='left: 50%; top: 50%; position:absolute; margin-left: -17px; margin-top: -19px; color: #0074a2; '></i></div>");
                    }
                    $("#_loader").css('display', 'block');
                };

                function hideLoading() {
                    $("#_loader").css('display', 'none');
                };

                $(function () {
                    var old_email = '<?php echo $user->user_email ?>';

                    $("#submit").on('click', function(e) {
                        var that = $(this), $form = that.parents('form'), email = $("#email").val();
                        if($.trim(old_email) === $.trim(email)) return true;

                        if(that.attr('allow-submit') === 'true') return true;

                        e.preventDefault();
                        $.ajax({
                            url: '<?php echo admin_url('admin-ajax.php') ?>',
                            data: {
                                email: $("#email").val(),
                                user_id: $("#user_id").val(),
                                action: 'apollo_check_email_dont_exist_same_blog',
                                nonce: '<?php echo wp_create_nonce('check-exist-email') ?>'
                            },
                            beforeSend: function() {
                                showLoading();
                            },
                            success: function(resp) {
                                if(resp.status === 'ok') {
                                    that.attr('allow-submit', 'true');
                                    that.trigger('click');
                                }
                                else {
                                    alert(resp.msg);
                                    that.focus();
                                    hideLoading();
                                }
                            },
                            complete: function() {

                            }
                        });

                        e.preventDefault();
                    })
                });

            })(jQuery);
        </script>
    <?php
    }
}

add_filter('network_site_url', 'apollo_network_site_url', 9, 3);
function apollo_network_site_url($url, $path, $scheme ){

    if(defined('WP_NETWORK_ADMIN') && WP_NETWORK_ADMIN === true && $GLOBALS['pagenow'] === 'index.php') {
        return $url;
    }

    $current_blog = get_blog_details();

    if ( 'relative' == $scheme )
        $url = $current_blog->path;
    else
        $url = set_url_scheme( 'http://' . $current_blog->domain . $current_blog->path, $scheme );

    if ( $path && is_string( $path ) )
        $url .= ltrim( $path, '/' );

    return $url;
}

add_action( 'wpmu_new_blog', 'apollo_wpmu_new_blog_auto_create_super_admin' );
function apollo_wpmu_new_blog_auto_create_super_admin($blog_id) {
    global $wpdb;

    $asuperadmin = get_super_admins();
    $ssuperadmin = "('".implode("','", $asuperadmin)."')";
    $tbl = $wpdb->base_prefix._APL_BLOGUSER;

    $sql = "INSERT INTO $tbl (blog_id, user_id)
                SELECT {$blog_id}, ID
                FROM {$wpdb->users} WHERE user_login IN $ssuperadmin
                ON DUPLICATE KEY UPDATE user_id = user_id
                ";

    $wpdb->query($sql);

    /* Update site */
    $level_key = $wpdb->get_blog_prefix($blog_id) . 'capabilities';
    $tbl_user_meta = $wpdb->usermeta;
    $sql = "INSERT INTO $tbl_user_meta (user_id, meta_key, meta_value)
                SELECT ID, '$level_key', 'a:1:{s:13:\"administrator\";b:1;}'
                FROM {$wpdb->users} WHERE user_login IN $ssuperadmin
                ON DUPLICATE KEY UPDATE user_id = user_id
                ";

    $result = $wpdb->query($sql);
}

add_action( 'show_network_site_users_add_existing_form', 'apollo_show_network_site_users_add_existing_form');
function apollo_show_network_site_users_add_existing_form($is_show) {
    return false;
}

add_action( 'grant_super_admin', 'apollo_grant_super_admin' );
function apollo_grant_super_admin( $user_id ) {

    global $wpdb;
    $tbl = $wpdb->users;

    $user = get_userdata( $user_id );

    $n = $wpdb->get_var($wpdb->prepare("select count(*) from $tbl where user_login = '%s' or user_email = '%s'", $user->user_login, $user->user_email));

    if($n > 1)
        wp_die("There are a user with the same name or email is exists in the network system. For security you cannot grant super admin permission for this user.<br/> Please create a new one. <a href='javascript:void(0);' onclick='window.history.go(-1); return false;'>Go Back</a>");

    $tbl_bu = $wpdb->base_prefix._APL_BLOGUSER;

    $sql = "SELECT group_concat(blog_id) as blogs FROM $tbl_bu WHERE user_id = '%s' GROUP BY user_id ;";

    $result = $wpdb->get_results($wpdb->prepare($sql, $user_id), ARRAY_A);

    $sblog = array();
    if($result !== false && is_array($result)) {
        $sblog = $result[0]['blogs'];
    }

    update_user_meta( $user_id, 'apollo_my_blog', $sblog);

    $qblog = '(' . $sblog .')';

    $sql = "INSERT INTO $tbl_bu (blog_id, user_id)
                SELECT b.blog_id, $user_id
                FROM {$wpdb->blogs} as b WHERE b.public = '1' AND b.blog_id NOT IN $qblog
                ON DUPLICATE KEY UPDATE blog_id = b.blog_id
                ";

    $wpdb->query($sql);

    /* Update site */
    $tbl_user_meta = $wpdb->usermeta;
    $wprefix = $wpdb->base_prefix;

    $sql = "INSERT INTO $tbl_user_meta (user_id, meta_key, meta_value)
                SELECT $user_id, concat('$wprefix', b.blog_id, '_', 'capabilities'), 'a:1:{s:13:\"administrator\";b:1;}'
                FROM {$wpdb->blogs} as b WHERE b.public = '1' AND b.blog_id NOT IN $qblog
                ON DUPLICATE KEY UPDATE user_id = $user_id
                ";

    $result = $wpdb->query($sql);
}



add_action( 'personal_options', 'apollo_personal_options_check_grant_super_admin' );

function apollo_personal_options_check_grant_super_admin( $user_profile ) {
    if(!defined('WP_NETWORK_ADMIN')) return;

    ?>
    <script type="text/javascript">
        (function($) {

            function showLoading() {
                if(!$('#_loader').length){
                    $('body').append("<div id='_loader' style='position: fixed; width: 100%; height: 100%; display: block; background: rgba(0, 0, 0, 0.8); z-index: 99999; top: 0;'><i class='fa fa-spinner fa-spin fa-3x' style='left: 50%; top: 50%; position:absolute; margin-left: -17px; margin-top: -19px; color: #0074a2; '></i></div>");
                }
                $("#_loader").css('display', 'block');
            };

            function hideLoading() {
                $("#_loader").css('display', 'none');
            };


            $(function() {
                var $super_admin = $('#super_admin');
                $super_admin.length && $super_admin.on('click', function(e) {
                    var that = $(this);

                    if(that.attr('checked') === undefined) return true;

                    $.ajax({
                        url: '<?php echo admin_url("admin-ajax.php"); ?>',
                        data: {
                            user_login: '<?php echo $user_profile->user_login; ?>',
                            user_email: '<?php echo $user_profile->user_email; ?>',
                            action: 'apollo_is_allow_grant_super_admin',
                            nonce: '<?php echo wp_create_nonce("grant-superadmin") ?>'
                        },
                        beforeSend: function() {
                            showLoading();
                        },
                        success: function(resp) {
                            if(resp.status === 'ok') {
                                that.attr('checked', 'checked');
                            }
                            else {
                                that.attr('checked', false);
                                alert(resp.msg);
                            }
                        },
                        complete: function() {
                            hideLoading();
                        }

                    });

                    return false;
                }) ;
            });

        })(jQuery);
    </script>
<?php
}

add_action( 'wp_ajax_apollo_' . 'is_allow_grant_super_admin', 'apollo_is_allow_grant_super_admin' );

function apollo_is_allow_grant_super_admin(){

    $user_login = sanitize_text_field($_GET['user_login']);
    $user_email = sanitize_text_field($_GET['user_email']);

    if(!wp_verify_nonce(sanitize_text_field($_GET['nonce']), 'grant-superadmin')) {
        wp_send_json(array(
                'status'=> 'fail',
                'msg' => __("Are you want to grant super admin for [$user_login] user ?\n\nIf YES please refresh this page (miss token) !\n\nIf NOT you are in the attack. Please becarefull !", 'apollo')
            ));
    }

    global $wpdb;
    $tbl = $wpdb->users;

    $n = $wpdb->get_var($wpdb->prepare("select count(*) from $tbl where user_login = '%s' or user_email = '%s'", $user_login, $user_email));

    if($n > 1) {
        wp_send_json(array(
                'status'=> 'fail',
                'msg' => __("There are a user with the same name or email is exists in the network system.\n\nFor security you cannot grant super admin permission for this user.", 'apollo')
            ));
    }

    wp_send_json(array(
            'status'=> 'ok',
        ));

}


add_action( 'revoked_super_admin', 'apollo_revoked_super_admin', 9 ,1 );
function apollo_revoked_super_admin($user_id) {
    global $wpdb;
    $tbl = $wpdb->base_prefix._APL_BLOGUSER;

    $sblog = get_user_meta( $user_id, 'apollo_my_blog', true);
    $qblog = '(' . $sblog . ')';

    $sql = "DELETE FROM $tbl WHERE user_id = '{$user_id}' AND blog_id NOT IN $qblog ;";
    $wpdb->query($sql);

    $tbl_user_meta = $wpdb->usermeta;
    $wpprefix = $wpdb->base_prefix;

    $arr_blog = explode(",", $sblog);

    $arr_capa = array();
    foreach($arr_blog as $b) {
        $arr_capa[] = $wpdb->get_blog_prefix($b).'capabilities';
    }
    $scapa = "(\"" . implode('","', $arr_capa) . "\")";

    $sql = "DELETE FROM $tbl_user_meta WHERE user_id = '{$user_id}' AND meta_key LIKE '$wpprefix%capabilities' and meta_key NOT IN $scapa ;";
    $r = $wpdb->query($sql);
}

add_action( 'in_admin_footer', 'apollo_check_create_new_site_policy' );
function apollo_check_create_new_site_policy( ) {

    wp_dequeue_script('user-suggest');

    if(!defined('WP_NETWORK_ADMIN') && !WP_NETWORK_ADMIN && $GLOBALS['pagenow'] !== 'site-new.php') return;

    ?>
    <script type="text/javascript">
        (function($) {
            function showLoading() {
                if(!$('#_loader').length){
                    $('body').append("<div id='_loader' style='position: fixed; width: 100%; height: 100%; display: block; background: rgba(0, 0, 0, 0.8); z-index: 99999; top: 0;'><i class='fa fa-spinner fa-spin fa-3x' style='left: 50%; top: 50%; position:absolute; margin-left: -17px; margin-top: -19px; color: #0074a2; '></i></div>");
                }
                $("#_loader").css('display', 'block');
            };

            function hideLoading() {
                $("#_loader").css('display', 'none');
            };


            $(function() {
                var $add_site = $('#add-site');

                $add_site.length && $add_site.on('click', function(e) {
                    var that = $(this), $form = that.parents('form');

                    $.ajax({
                        url: '<?php echo admin_url("admin-ajax.php"); ?>',
                        data: {
                            email: $('.wp-suggest-user[type="email"]').val(),
                            action: 'apollo_check_email_super_admin_and_site_when_create',
                            site_name: $('[name="blog[domain]"]').val(),
                            nonce: '<?php echo wp_create_nonce("check-email-super-admin") ?>'
                        },
                        beforeSend: function() {
                            showLoading();
                        },
                        success: function(resp) {
                            if(resp.status === 'ok') {
                                $form.submit();
                            }
                            else {
                                alert(resp.msg);
                                that.focus();
                                hideLoading();
                            }
                        },
                        complete: function() {

                        }

                    });

                    e.preventDefault();

                }) ;
            });

        })(jQuery);
    </script>
<?php
}

function apollo_check_email_super_admin_and_site_when_create(){

    global $wpdb;

    $email = sanitize_text_field($_GET['email']);
    $site_name = sanitize_text_field($_GET['site_name']);

    if(!wp_verify_nonce(sanitize_text_field($_GET['nonce']), 'check-email-super-admin')) {
        wp_send_json(array(
                'status'=> 'fail',
                'msg' => __("Token error. Please refresh page!", 'apollo')
            ));
    }

    if(in_array($email, get_list_email_super_admin())) {
        wp_send_json(array(
                'status'=> 'fail',
                'msg' => __("This is superadmin user email. Please choice another email address!", 'apollo')
            ));
    }

    if($wpdb->query($wpdb->prepare("select 1 from $wpdb->users where user_login = '%s' limit 1", $site_name)) === 1) {
        wp_send_json(array(
                'status'=> 'fail',
                'msg' => __("Please choice another domain name!", 'apollo')
            ));
    }

    wp_send_json(array(
            'status'=> 'ok',
        ));

}

add_action( 'wp_ajax_apollo_' . 'check_email_super_admin_and_site_when_create', 'apollo_check_email_super_admin_and_site_when_create' );

add_action('user_profile_update_errors', 'apollo_allow_create_user_duplicate_email', 9, 3);
function apollo_allow_create_user_duplicate_email(&$errors, $update, $user) {

    if(defined('WP_NETWORK_ADMIN') && WP_NETWORK_ADMIN === true && $GLOBALS['pagenow'] === 'user-edit.php') {
        if($errors instanceof WP_Error && isset($errors->errors['email_exists'])) {
            if(count($errors->get_error_codes()) === 1) {
                $errors = new WP_Error();
            }
            else {
                $bkError = new WP_Error();

                foreach($errors->errors as $code => $message) {
                    if($code === 'email_exists') continue;
                    foreach($message as $msg) {
                        $bkError->add($code, $msg);
                    }
                }
                $errors = $bkError;
            }
        }
    }

}

// Call username changer class
include_once __DIR__. '/multisite-user-management/class-apollo-username-changer.php';
if (class_exists('Apl_Username_Changer') ) {
    new Apl_Username_Changer();
}

/**
 * Nginx, PHP-FPM or any other FastCGI method of running PHP you’ve probably noticed that the function getallheaders() does not exist
 * @vulh
 */
if (!function_exists('getallheaders')) {
    function getallheaders() {
        $headers = [];
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}