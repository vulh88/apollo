(function($){
    window.APOLLO =  window.APOLLO || {};
    window.APOLLO.userManagement = (function(){
        var module = {};
        module.init = function(){
            $(document).ready(function(){
                $('#current_username').autocomplete({delay: 1000, source: []});
                $('#current_username').on('keyup', function(e){
                    // $('#current_username').val("");
                    loadUserAutoComplete(e)
                });
                $('#aplUsernameChangerFrm').off('submit').on('submit', function(e){usernameChanger(e)});
                $('#apl-change-username-site').off('change').on('change', function(e){changeSite(e)});
            });

            var changeSite = function(e){
                var currentUser = $('#current_username');
                var newUser = $('#new_username');
                var currentElement = $(e.currentTarget);
                if(currentElement.val() != 0){
                    if(currentElement.hasClass('changer-error')){
                        currentElement.removeClass('changer-error');
                    }
                }
                if(currentUser.length > 0){
                    currentUser.val('');
                }
                if(newUser.length > 0){
                    newUser.val('');
                }
                if(currentUser.hasClass('changer-error')){
                    currentUser.removeClass('changer-error');
                }
                if(newUser.hasClass('changer-error')){
                    newUser.removeClass('changer-error');
                }

            };

            var usernameChanger = function (e) {
                // e.preventDefault();
                var site = $('#apl-change-username-site');
                var currentUser = $('#current_username');
                var newUser = $('#new_username');
                var isSubmit = true;
                if(site.val() == 0){
                    if(!site.hasClass('changer-error')){
                        site.addClass('changer-error');
                    }
                    isSubmit = false;
                }else{
                    if(site.val() != 0 && site.hasClass('changer-error')){
                        site.removeClass('changer-error');
                    }
                }
                if(currentUser.val() == ''){
                    if(!currentUser.hasClass('changer-error')){
                        currentUser.addClass('changer-error');
                    }
                    isSubmit = false;
                }else{
                    if(currentUser.val() != '' && currentUser.hasClass('changer-error')){
                        currentUser.removeClass('changer-error');
                    }
                }
                if(newUser.val() == ''){
                    if(!newUser.hasClass('changer-error')){
                        newUser.addClass('changer-error');
                    }
                    isSubmit = false;
                }else{
                    if(newUser.val() != '' && newUser.hasClass('changer-error')){
                        newUser.removeClass('changer-error');
                    }
                }
                if(!isSubmit){
                    e.preventDefault();
                    return false;
                }
            };

            var loadUserAutoComplete = function(e){
                var textSearch = ($(e.currentTarget).val());
                if(textSearch == ''){
                    return false;
                }
                //get site id
                var siteID = 0;
                var sites = $('#apl-change-username-site');
                if(sites.length > 0){
                    siteID = $('#apl-change-username-site option:selected').val();
                    if(siteID == 0) {
                        return false;
                    }
                    // siteID = parseInt(sites.val());
                    // if(isNaN(siteID) || siteID == 0){
                    //     return false;
                    // }
                }
                $('#current_username').autocomplete({
                    delay: 1000,
                    source: function(request, response){
                        $.ajax({
                            url: APL.ajax_url,
                            type: 'GET',
                            dataType: 'JSON',
                            data:{
                                action : 'username_changer_get_autocomplete',
                                username: textSearch,
                                site_id: siteID
                            },
                            beforeSend: function () {
                                jQuery(window).block(jQuery.apl.blockUI);
                            },
                            success: function (data) {
                                response(data.users);
                                jQuery(window).unblock();
                            }
                        });
                    },
                    select: function( event, ui ) {
                        // if(ui.item && ui.item.value){
                        //     var currentUser = ui.item.value.split(" (");
                        //     if(currentUser.length >= 2){
                        //         $('#current_username').val(currentUser[0]);
                        //     }
                        // }
                    }
                });

            };
            return module;
        };
        return module.init();
    })();
})(jQuery);

