<?php

/**
 * Username changer
 * 
 */
if ( !class_exists( 'Apl_Username_Changer' ) ) {
    
    class Apl_Username_Changer {
        
        private $site;
        private $current_username;
        
        public function __construct() {
            if (is_network_admin()) {
                $this->site = isset($_POST['site']) && $_POST['site'] ? $_POST['site'] : '';
            } else {
                $this->site = get_current_blog_id();
            }   
            
            if (isset($_POST['current_username'])) {
                $this->current_username = $_POST['current_username'];
            }
            
            add_action('wp_ajax_username_changer_get_users', array($this, 'username_changer_get_users'));
            add_action('wp_ajax_username_changer_get_autocomplete', array($this, 'username_changer_get_autocomplete'));
            $this->user_management_enqueue_script();
            $this->hooks();
        }
       
        /**
         * Get users of site
         * 
         * @access public
         * @return string
         */
        function username_changer_get_users() {
            $blog_id = isset($_REQUEST['blog_id']) ? $_REQUEST['blog_id'] : "";
            $this->site = $blog_id;
            wp_send_json(array(
                'html' => $this->render_user_options(),
            ));
        }
        
        /**
         * Run action and filter hooks
         *
         * @access      public
         * @return      void
         */
        public function hooks() {
  
            // Add menu item
            add_action( 'admin_menu', array( &$this, 'add_admin_menu' ) );

            // Add link to users.php    
            add_filter( 'user_row_actions', array( &$this, 'username_changer_link' ), 10, 2 );

            if( is_multisite() ) {
                // Add link to network/users.php
                add_filter( 'ms_user_row_actions', array( &$this, 'username_changer_link' ), 10, 2 );

                // Add network menu item
                add_action( 'network_admin_menu', array( &$this, 'add_admin_menu' ) );
            }
        }
        
         /**
         * Add menu item for Username Changer
         *
         * @access      public
         * @return      void
         */
        public function add_admin_menu() {
            // Only admin-level users with the edit_users capability can change usernames
            add_submenu_page(
                'users.php', 
                __( 'Username Changer', 'apollo' ), 
                __( 'Username Changer', 'apollo' ), 
                'edit_users', 
                'username_changer', 
                array( &$this, 'add_admin_page' ) 
            );
        }
        
        /**
         * Add link to user page
         * 
         * @access      public
         * @param       array $actions The current user actions
         * @param       object $user The user we are editing
         * @return      array $actions The modified user actions
         */
        public function username_changer_link( $actions, $user ) {
            if( current_user_can( 'edit_users' ) ) {
                if( !is_multisite() || ( is_multisite() && !is_network_admin() && !user_can( $user->ID, 'manage_network' ) ) || ( is_multisite() && is_network_admin() ) ) {
                    $actions[] = '<a href="' . add_query_arg( array( 'page' => 'username_changer', 'id' => $user->ID ) ) . '">' . __( 'Change Username', 'apollo' ) . '</a>';
                }
            }

            return $actions;
        }
        
        /**
         * Render html select option
         * 
         * @return string
         */
        function render_user_options() {
            global $wpdb;
            $str = '<option value="">'.__("Select a User", 'apollo').'</option>';
            
            $siteJoin = $this->site ? ' AND ub.blog_id = '.$this->site.'' : '';
            
            $usersql    = "SELECT * from $wpdb->users u"
                . " inner join wp_apollo_blog_user as ub ON ub.user_id = u.ID $siteJoin "
                . " group by u.ID"
                . " order by user_login asc";
   
            $userinfo   = $wpdb->get_results( $usersql );
            
            if ( isset( $_GET['id'] ) ) {
                $userId = $_GET['id'];
                $_userGetInfo = get_user_by('id', $userId);
                if ( $_userGetInfo ) $this->current_username = $_userGetInfo->user_login;
            }
            
            if( $userinfo ) {
                foreach( $userinfo as $userinfoObj ) {
                    $str .= '<option '.  selected($userinfoObj->user_login, $this->current_username, false).' value="' . esc_attr( $userinfoObj->user_login ) . '">' . esc_html( $userinfoObj->user_login ) . ' (' . esc_html( $userinfoObj->user_email ) . ')</option>';
                }
            }
            return $str;
        }
        
        public function usernameExist( $username, $blog_id ) {
            if ( ! $blog_id ) $blog_id = get_current_blog_id();
            return apl_get_data_by('login', $username, $blog_id);
        }
        
        /**
         * Add Username Changer page
         *
         * @access      public
         * @global      object $wpdb The WordPress database object
         * @global      array $userdata The data for the current user
         * @global      array $current_user The data for the current user
         * @return      void
         */
        public function add_admin_page() {
            global $wpdb, $current_user;

            // Get current user info
            get_currentuserinfo();

            // Make SURE this user can edit users
            if( current_user_can( 'edit_users' ) == false ) {
                echo '<div id="message" class="error"><p><strong>' . __( 'You do not have permission to change a username!', 'apollo' ) . '</strong></p></div>';
                return;
            }
            //Vandd custom for autocomplete search user.
            if(!empty($_POST['current_username'])){
                $str_username = explode(" (", $_POST['current_username']);
                if(sizeof($str_username) >= 1){
                    $username = $str_username[0];
                }
            }else{
                $username = '';
            }

            if( isset( $_POST['action'] ) && ( $_POST['action'] == 'update' ) && !empty( $_POST['new_username'] ) && !empty( $username ) ) {

                // Sanitize the new username
                $new_username       = sanitize_user( $_POST['new_username'] );
                $current_username = $username;
                
                $wpdb->escape_by_ref($new_username);
                $wpdb->escape_by_ref($current_username);
                
                $blog_id = isset( $_POST['site'] ) && $_POST['site'] ? $_POST['site'] : get_current_blog_id();
                if( $this->usernameExist( $current_username, $blog_id ) ) {
                    $current_user_data  = get_user_by( 'login', $current_username );
                    
                    if( $new_username == $current_username ) {
                        // Make sure username exists and username != new username
                        echo '<div id="message" class="error"><p><strong>' . sprintf( __( 'Current Username and New Username cannot both be "%1$s"!', 'apollo' ), $new_username ) . '</strong></p></div>';
                    } elseif( $this->usernameExist( $new_username, $blog_id ) ) {
                        // Make sure new username doesn't exist
                        echo '<div id="message" class="error"><p><strong>' . sprintf( __( '"%1$s" cannot be changed to "%2$s", "%3$s" already exists!', 'apollo' ), $current_username, $new_username, $new_username ) . '</strong></p></div>';
                    } elseif( is_multisite() && user_can( $current_user_data->ID, 'manage_network' ) && !is_network_admin() ) {
                        // Super Admins must be changed from Network Dashboard
                        echo '<div id="message" class="error"><p><strong>' . __( '"Super Admin usernames must be changed from the Network Dashboard!', 'apollo' ) . '</strong></p></div>';
                    } elseif( $new_username != $current_username ) {

                        $blogUser = 'wp_apollo_blog_user';

                        // Update username!
                        $q          = $wpdb->prepare( "
                          UPDATE $wpdb->users u
                          INNER JOIN $blogUser b ON b.user_id = u.ID
                          SET u.user_login = %s
                          WHERE u.user_login = %s AND b.blog_id = %s
                          ", $new_username, $current_username, $blog_id );

                        $qnn        = $wpdb->prepare( "
                        UPDATE $wpdb->users u
                        INNER JOIN $blogUser b ON b.user_id = u.ID
                        SET u.user_nicename = %s
                        WHERE u.user_login = %s AND b.blog_id = %s


                        ", $new_username, strtolower( str_replace( ' ', '-', $new_username ) ), $blog_id );

                        // Check if display name is the same as username
                        $usersql    = $wpdb->prepare( "SELECT * from $wpdb->users WHERE user_login = %s", $current_username );
                        $userinfo   = $wpdb->get_row( $usersql );

                        // If display name is the same as username, update both
                        if( $current_username == $userinfo->display_name ) {
                            $qdn    = $wpdb->prepare( "UPDATE $wpdb->users SET display_name = %s WHERE user_login = %s", $new_username, $new_username );
                        }

                        // If the user is a Super Admin, update their permissions
                        if( is_multisite() && is_super_admin( $current_user_data->ID ) ) {
                            grant_super_admin( $current_user_data->ID );
                        }

                        if( false !== $wpdb->query( $q ) ) {
                            $wpdb->query( $qnn );

                            if( isset( $qdn ) ) {
                                $wpdb->query( $qdn );
                            }

                            // If changing own username, display link to re-login
                            if( $current_user->user_login == $current_username ) {
                                echo '<div id="message" class="updated fade"><p><strong>' . sprintf( __( 'Username %1$s was changed to %2$s.&nbsp;&nbsp;Click <a href="%3$s">here</a> to log back in.', 'apollo' ), $current_username, $new_username, wp_login_url() ) . '</strong></p></div>';
                            } else {
                                echo '<div id="message" class="updated fade"><p><strong>' . sprintf( __( 'Username %1$s was changed to %2$s.', 'apollo' ), $current_username, $new_username ) . '</strong></p></div>';
                            }
                        } else {
                            // If database error occurred, display it
                            echo '<div id="message" class="error"><p><strong>' . sprintf( __( 'A database error occurred : %1$s', 'apollo' ), $wpdb->last_error ) . '</strong></p></div>';
                        }
                    }
                } else {
                    // Warn if user doesn't exist (this should never happen!)
                    echo '<div id="message" class="error"><p><strong>' . sprintf( __( 'Username "%1$s" doesn\'t exist!', 'apollo' ), $current_username ) . '</strong></p></div>';
                }
            } elseif( ( isset( $_POST['action'] ) && $_POST['action'] == 'update' ) && ( empty( $_POST['new_username'] ) || empty( $_POST['current_username'] ) ) ) {
                // All fields are required
                echo '<div id="message" class="error"><p><strong>' . __( 'Both "Current Username" and "New Username" fields are required!', 'apollo' ) . '</strong></p></div>';
            } ?>
            <style>
                #aplUsernameChangerFrm table tr td label {
                    width: 150px; 
                    float: left;
                }
                #aplUsernameChangerFrm table {
                    width: 80%; padding: 10px;
                }
                @media (max-width: 768px) {
                    #aplUsernameChangerFrm table {
                        width: 100%; padding: 10px;
                    }
                }
                @media (max-width: 519px) {
                    #aplUsernameChangerFrm table tr td label,
                    #aplUsernameChangerFrm table tr td input,
                    #aplUsernameChangerFrm table tr td select {
                        width: 100%;
                    }
                    
                }
            
            </style>
            <div class="wrap">
                <h2><?php echo __( 'Username Changer', 'apollo' ); ?></h2>

                <br />

                <form name="aplUsernameChangerFrm" id="aplUsernameChangerFrm" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>?page=username_changer">
                    <input type='hidden' name='action' value='update' />
                    <table cellpadding="5" class="widefat post fixed">
                        <thead>
                            <tr>
                                <?php
                                    if( isset( $_REQUEST['id'] ) && $_REQUEST['id'] != '' ) {
                                        $usersql    = $wpdb->prepare( "SELECT * from $wpdb->users where ID = %d", $_REQUEST['id'] );
                                        $userinfo   = $wpdb->get_row( $usersql );
                                        echo '<th><strong>' . __( 'Rename user to what?', 'apollo' ) . '</strong></th>';
                                    } else {
                                        echo '<th><strong>' . __( 'Edit which user?', 'apollo' ) . '</strong></th>';
                                    }
                                ?>
                            </tr>
                            <script>
                                
//                                (function($) {
//
//                                    $(function() {
//
//                                        var btn = $('#aplUsernameChangerFrm input[type=submit]');
//
//                                        if ($('#apl-change-username-site').length) {
//                                            btn.attr('disabled', 'disabled');
//                                        }
//                                        $('#apl-change-username-site').change(function() {
//                                            var blogId = parseInt($(this).val());
//
//                                            if (isNaN(blogId)) {
//                                                btn.attr('disabled', 'disabled');
//                                                return;
//                                            }
//                                            //remove select user
//                                            $('#current_username_auto').val('');
//                                            $('#current_username').val('');
//                                            $('#new_username').val('');
//
//                                            btn.removeAttr('disabled');
//
//                                            $.ajax({
//                                                url: apollo_admin_obj.ajax_url,
//                                                data: {
//                                                    method: 'get',
//                                                    action: 'username_changer_get_users',
//                                                    blog_id : blogId
//                                                },
//                                                success: function(res) {
//                                                    $('form #current_username').html(res.html);
//                                                }
//                                            });
//                                        });
//                                    });
//
//                                }) (jQuery);
                                
                                
                            </script>
                            
                            <?php 
                                if (is_network_admin() ) {
                            ?>
                            <tr>
                                <td>
                                    <label><strong style="padding-right: 18px;"><?php echo __( 'Sites', 'apollo' ); ?></strong></label>
                                    <select name="site" id="apl-change-username-site">
                                        <option value="0"><?php _e( "Select a site", 'apollo' ) ?></option>
                                        <?php 
                                            $sites = wp_get_sites();
                                            foreach( $sites as $site ) {
                                                echo '<option '.  selected($site['blog_id'], $this->site, false).' value="'.$site["blog_id"].'">'.$site["domain"].'</option>';
                                            }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <?php } ?>
                            
                            <tr>
                                <td>
                                    <label><strong><?php echo __( 'Current Username', 'apollo' ); ?></strong></label>
                                    <input id="current_username" name="current_username" />
<!--                                    <input type="text" class="hidden" id="current_username" name="current_username" />-->
<!--                                    --><?php
//                                        echo '<select name="current_username" id="current_username">';
//                                            echo $this->render_user_options();
//                                        echo '</select>';
//                                    ?>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <label><strong style="padding-right: 18px;"><?php echo __( 'New Username', 'apollo' ); ?></strong></label>
                                    
                                    <input name="new_username" id="new_username" type="text" value="" size="30" />
                                    
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <label>&nbsp;</label>
                                    <div style="float: left;">
                                        <input type="submit" id="username-changer" name="submit" class="button-secondary action" value="<?php echo __( 'Save Changes', 'apollo' ); ?>" />
                                    </div>
                                </td>
                            </tr>
                        </thead>
                    </table>
                </form>
            </div>
            <?php
        }

        /**
         * get user for autocomplete
         * @return array $users
         */
        public function username_changer_get_autocomplete(){
            global $wpdb;
            $user_name = isset($_REQUEST['username']) ? $_REQUEST['username'] : "";
            $site_id = (isset($_REQUEST['site_id'])) ? intval($_REQUEST['site_id']) : $this->site;
            if($site_id == 0){
                $site_id = $this->site;
            }
            $siteJoin = $site_id ? ' AND ub.blog_id = '.$site_id.'' : '';
            $user_sql    = "SELECT u.user_login, u.user_email from $wpdb->users u"
                . " inner join wp_apollo_blog_user as ub ON ub.user_id = u.ID $siteJoin  AND (u.user_login like '%" . $user_name . "%' OR u.user_email like '%".$user_name."%')"
                . " group by u.ID"
                . " order by user_login asc"
                . " limit " . Apollo_Display_Config::APL_USER_MANAGEMENT_LIMIT;

            $user_info   = $wpdb->get_results( $user_sql );
            $user_arr = array();
            if($user_info){
                foreach ($user_info as $user_item){
                    array_push($user_arr, ($user_item->user_login . " (" . $user_item->user_email . ")"));
                }
            }
            wp_send_json(array(
                'status' => "TRUE",
                'users' => $user_arr,
            ));
        }

        /**
         * enqueue script
         * @return void
         */
        public function user_management_enqueue_script(){
            if ((is_admin() || is_network_admin()) && (!isset($_GET['post']) && !isset($_GET['post_type']))) {
                wp_enqueue_scripts();
                wp_enqueue_script('jquery-ui-autocomplete');
                wp_enqueue_script('jquery');
                wp_register_script('user-management', '/wp-content/mu-plugins/multisite-user-management/user-management.js');
                wp_enqueue_script('user-management');
                wp_register_style('user-management-style', '/wp-content/mu-plugins/multisite-user-management/user-management-style.css');
                wp_enqueue_style('user-management-style');
            }
        }
    }
}