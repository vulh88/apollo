(function($) {
    $(document).ready(function () {
        var apl_check_require_multi = function (_obj) {
            var name = _obj.attr('name'),
                groups = $('[name="'+ name +'"]'),
                error = true;

            $.each(groups, function(i, v) {
                if ( $(v).attr('checked') == 'checked' ){
                    error = false;
                }
            });
            var hidden_elm = _obj.closest('.options_group').children('input[type="hidden"]');
            hidden_elm.val( error ? '' : 1 );
        };

        $('body').off('click','.options_group .form-field input[type="checkbox"]').on('click', '.options_group .form-field input[type="checkbox"]', function(){
            apl_check_require_multi($(this));
        });
    });

}) (jQuery);

