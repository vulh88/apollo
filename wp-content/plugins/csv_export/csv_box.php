<?php
/**
 * Adds a meta box to the post editing screen
 */
function csv_custom_meta() {
    add_meta_box( 'csv_meta', __( 'Export Details', 'csv-textdomain' ), 'csv_meta_callback', 'csvexport' );
}
add_action( 'add_meta_boxes', 'csv_custom_meta' );


// Hide preview buttons etc

function hide_csv_publishing_actions(){
        $my_post_type = 'csvexport';
        global $post;
        if($post->post_type == $my_post_type){
            echo '
                <style type="text/css">
                    #misc-publishing-actions,
                    #minor-publishing-actions{
                        display:none;
                    }
                </style>
            ';
        }
}
add_action('admin_head-post.php', 'hide_csv_publishing_actions');
add_action('admin_head-post-new.php', 'hide_csv_publishing_actions');

function csv_export_init()
{
    wp_register_style('csv_export_css', plugins_url('/css/csv_export.css', __FILE__));
    wp_enqueue_style('csv_export_css');
    wp_enqueue_script( 'csv_export_js', plugins_url( '/js/csv_export.js', __FILE__ ));
}

add_action('admin_init', 'csv_export_init');


/**
 * Outputs the content of the meta box
 */
function csv_meta_callback( $post ) {
global $wpdb;

    	wp_nonce_field( basename( __FILE__ ), 'csv_nonce' );
    	$csv_stored_meta = get_post_meta( $post->ID );

	$url = get_site_url();
	$domain = substr($url, 7);
	$blog_id = get_current_blog_id();

    //echo "<PRE>";
	//print_r($csv_stored_meta);
	//echo "</PRE>";


?>

<p><b>Include the following post types</b> ( If none are selected it defaults to ALL ) &nbsp;&nbsp;&nbsp;&nbsp;
<input type=checkbox name=csv_publish value="publish" <?php if ( $csv_stored_meta['csv_publish'][0] == 'publish' )  echo "checked"; ?>>Published &nbsp;&nbsp;
<input type=checkbox name=csv_pending value="pending" <?php if ( $csv_stored_meta['csv_pending'][0] == 'pending' )  echo "checked"; ?>>Pending &nbsp;&nbsp;
<input type=checkbox name=csv_draft value="draft" <?php if ( $csv_stored_meta['csv_draft'][0] == 'draft' )  echo "checked"; ?>>Draft
</p><hr>


<p><b>Select the type of data to export and click publish/update.</b></p>

<script type="text/javascript">
   function showdiv(div1, div2, div3, div4){
            var targetA = document.getElementById(div1);
            targetA.style.display = 'inline';
   }
   function hidediv(div1, div2, div3, div4){
            var targetA = document.getElementById(div1);
            targetA.style.display = 'none';
   }
</script>

<script>

jQuery(document).ready(function(){
jQuery('#dateStart').datepicker({
dateFormat : 'yy-mm-dd'
});
});

jQuery(document).ready(function(){
jQuery('#dateEnd').datepicker({
dateFormat : 'yy-mm-dd'
});
});

</script>

<?php
//////////////////////////////
// Set defualts for display
//////////////////////////////
$evt = 'none';
$hiddenCats = Apollo_App::hiddenCategoryIDs();
if (isset($csv_stored_meta['csv_type']) && $csv_stored_meta['csv_type'][0] == 'event' ) $evt = 'inline';
?>

<p><input type=radio onclick="hidediv('org');showdiv('evt');hidediv('ven');hidediv('artist');hidediv('pub');hidediv('bus');hidediv('class');" name=csv_type value="event" <?php if (isset($csv_stored_meta['csv_type']) && $csv_stored_meta['csv_type'][0] == 'event' )  echo "checked"; ?>> Event</p>
<div id=evt style="display:<?php echo $evt; ?>">
 <b>Filter by:</b>
<input type=radio name=csv_filter value=0 <?php if ( $csv_stored_meta['csv_filter'][0] == 0 )  echo "checked"; ?>>Active Date  | <input type=radio name=csv_filter value=1 <?php if ( $csv_stored_meta['csv_filter'][0] == 1 )  echo "checked"; ?>>Post Date

<p>
Include events using a date range &nbsp;&nbsp;&nbsp;<b>Start Date:</b>
<input type="text" name="csv_dateStart" id="dateStart" value="<?php if ( isset ( $csv_stored_meta['csv_dateStart'] ) ) echo $csv_stored_meta['csv_dateStart'][0]; ?>" />&nbsp;&nbsp;&nbsp;
<b>End Date:</b>
<input type="text" name="csv_dateEnd" id="dateEnd" value="<?php if ( isset ( $csv_stored_meta['csv_dateEnd'] ) ) echo $csv_stored_meta['csv_dateEnd'][0]; ?>" /><br>
-OR-<br>
 <b>Set number of days:</b>
<input type=radio name=csv_drange value=7 <?php if ( $csv_stored_meta['csv_drange'][0] == 7 )  echo "checked"; ?>>7 &nbsp;&nbsp;
<input type=radio name=csv_drange value=14 <?php if ( $csv_stored_meta['csv_drange'][0] == 14 )  echo "checked"; ?>>14 &nbsp;&nbsp;
<input type=radio name=csv_drange value=30 <?php if ( $csv_stored_meta['csv_drange'][0] == 30 )  echo "checked"; ?>>30 &nbsp;&nbsp;
<input type=radio name=csv_drange value=90 <?php if ( $csv_stored_meta['csv_drange'][0] == 90 )  echo "checked"; ?>>90 &nbsp;&nbsp;
<input type=radio name=csv_drange value=180 <?php if ( $csv_stored_meta['csv_drange'][0] == 180 )  echo "checked"; ?>>180 &nbsp;&nbsp;
<input type=radio name=csv_drange value=360 <?php if ( $csv_stored_meta['csv_drange'][0] == 360 )  echo "checked"; ?>>360 &nbsp;&nbsp;
 days. If no date options are selected then all events from today onwards will be included.
</p>
<p>Include individual dates and times for the event <input type=checkbox name=csv_incind value="Y" <?php if ( $csv_stored_meta['csv_incind'][0] == 'Y' )  echo "checked"; ?>></p>
<hr>

          <?php
              // Get all theme tools
              $themeToolClass = new Apollo_Theme_Tool();
              $themeToolData = $themeToolClass->getAllThemeTools();

              $key = Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID;
              $type = Apollo_DB_Schema::_EVENT_PT. '-type';
              $primarys = get_terms( $type, array( 'parent' => false, 'hide_empty' => false ) );


          ?>
          <select name="term_primary"
                  id="_apl_event_term_primary"
                  class="custom-validate-field"
              >
              <option value=""><?php _e( 'Select Primary Category', 'apollo' ) ?>:</option>
              <?php

                  if ( $primarys ):
                      foreach( $primarys as $p ):

                          if ($themeToolData && in_array($p->term_id, $themeToolData)) continue;

                          if (in_array($p->term_id, $hiddenCats)) continue;
              ?>
              <option <?php echo $csv_stored_meta['term_primary'][0] == $p->term_id ? 'selected' : '' ?> value="<?php echo $p->term_id ?>"><?php echo $p->name ?></option>
              <?php endforeach; endif; ?>
          </select>
<hr>

    <p><?php _e( 'Secondary categories:', 'apollo' ) ?></p>
    <div><span class="cat-txt"><?php _e( 'Add more category types', 'apollo' ) ?></span>
        <div data-mod="cat" data-action="<?php echo isset($data_action) ? $data_action : ''; ?>" class="expend add-cat-expand">
          </div>

        <div class="cat-list" <?php echo isset($none_style) ? $none_style : ''; ?> >
            <ul class="cat-listing tbl0">

                <?php
                    $total = count( $primarys );
                    if ( $total > 2 ) {
                        $columns = array(
                            array_slice( $primarys , 0, ceil( $total / 2 ) ),
                            array_slice( $primarys , ceil( $total / 2 ), $total - 1 )
                        );
                    } else {
                        $columns = array(
                            $primarys,
                        );
                    }

                    foreach( $columns as $k => $column ):
                ?>

                <li>
                  <ul class="Llist c<?php echo $k ?>">
                      <?php
                      foreach ( $column as $p ):

                          if ($themeToolData && in_array($p->term_id, $themeToolData)) continue;

                          if (in_array($p->term_id, $hiddenCats)) continue;

                          $disable_style = (isset($selected_pri_id) && $selected_pri_id == $p->term_id) ? 'style="opacity: 0.5"' : '';
                          ?>
                      <li>
                          <input <?php echo $disable_style; ?> name="add_cat[]" type="checkbox"
                                 <?php echo in_array( $p->term_id ,  explode(',',$csv_stored_meta['add_cat'][0]) ) ? 'checked' : '' ?>
                                 value="<?php echo $p->term_id ?>" >
                          <label <?php echo $disable_style; ?>><b><?php echo $p->name ?></b></label>
                          <?php
                              $childs = get_terms( $type, array( 'parent' => $p->term_id, 'hide_empty' => false ) );
                              if ( $childs ):
                          ?>
                         <ul>
                              <?php foreach( $childs as $c ):

                                  if ($themeToolData && in_array($p->term_id, $themeToolData)) continue;
                                  if (in_array($c->term_id, $hiddenCats)) continue;
                                ?>
                              <li>
                                  &nbsp; &nbsp;<input <?php echo in_array( $c->term_id , explode(',',$csv_stored_meta['add_cat'][0]) ) ? 'checked' : '' ?> name="add_cat[]" type="checkbox" value="<?php echo $c->term_id ?>" >
                                  <label><?php echo $c->name ?></label>
                              </li>
                              <?php endforeach; ?>
                          </ul>

                          <?php endif; ?>
                      </li>
                      <?php endforeach; ?>

                  </ul>
                </li>
                <?php endforeach; ?>

            </ul>
        </div>
<p><b>Include visit report data</b>&nbsp;&nbsp;
<input type=checkbox name=csv_report value="true" <?php if ( $csv_stored_meta['csv_report'][0] == 'true' )  echo "checked"; ?>>
</p><hr>

</div>
<hr>
    <!--@ticket #16640  Allow select multiple individual organizations, venues, cities, and zip codes with type is the event.-->
    <div class="options_group apollo-checkbox clear">
        <div class="csv-export-wrap-ddl">
            <label class="apl-multi-choice ct-lb-mtc"><?php echo __('Organization', 'apollo') ?></label>
            <?php include_once __DIR__ . '/partials/organization-multiselect.php'; ?>
        </div>
        <div class="csv-export-wrap-ddl">
            <label class="apl-multi-choice ct-lb-mtc"><?php echo __('Venues', 'apollo') ?></label>
            <?php include_once __DIR__ . '/partials/venue-multiselect.php'; ?>
        </div>
    </div>
    <hr>
    <div class="options_group apollo-checkbox clear">
        <div class="csv-export-wrap-ddl">
            <label class="apl-multi-choice ct-lb-mtc"><?php echo __('City', 'apollo') ?></label>
            <?php include_once __DIR__ . '/partials/city-multiselect.php'; ?>
        </div>
        <div class="csv-export-wrap-ddl">
            <label class="apl-multi-choice ct-lb-mtc"><?php echo __('Zip', 'apollo') ?></label>
            <?php include_once __DIR__ . '/partials/zip-multiselect.php'; ?>
        </div>
    </div>
</div>

<?php
//////////////////////////////
// Set defualts for display
//////////////////////////////
$org = 'none';
if ( isset($csv_stored_meta['csv_type']) && $csv_stored_meta['csv_type'][0] == 'organization' ) $org = 'inline';
?>
<p><input type=radio onclick="showdiv('org');hidediv('evt');hidediv('ven');hidediv('artist');hidediv('pub');hidediv('bus');hidediv('class');"  name=csv_type value="organization" <?php if (isset($csv_stored_meta['csv_type']) && $csv_stored_meta['csv_type'][0] == 'organization' )  echo "checked"; ?>> Organization</p>
<div id=org style="display:<?php echo $org; ?>">
        <?php
        // Get all theme tools
        $themeToolClass = new Apollo_Theme_Tool();
        $themeToolData = $themeToolClass->getAllThemeTools();

        //$key = Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID;
        $type = Apollo_DB_Schema::_ORGANIZATION_PT . '-type';
        $primarys = get_terms($type, array('parent' => false, 'hide_empty' => false));


        ?>
    <p><?php _e( 'Categories:', 'apollo' ) ?></p>
    <div>
        <div data-mod="cat" data-action="<?php echo isset($data_action) ? $data_action : ''; ?>" class="expend add-cat-expand">
          </div>

        <div class="cat-list" <?php echo isset($none_style) ? $none_style : ''; ?> >
            <ul class="cat-listing tbl0">

                <?php
                    $total = count( $primarys );
                    if ( $total > 2 ) {
                        $columns = array(
                            array_slice( $primarys , 0, ceil( $total / 2 ) ),
                            array_slice( $primarys , ceil( $total / 2 ), $total - 1 )
                        );
                    } else {
                        $columns = array(
                            $primarys,
                        );
                    }

                    foreach( $columns as $k => $column ):
                ?>

                <li>
                  <ul class="Llist c<?php echo $k ?>">
                      <?php
                      foreach ( $column as $p ):

                          if ($themeToolData && in_array($p->term_id, $themeToolData)) continue;

                          if (in_array($p->term_id, $hiddenCats)) continue;

                          $disable_style = (isset($selected_pri_id) && $selected_pri_id == $p->term_id) ? 'style="opacity: 0.5"' : '';
                          ?>
                      <li>
                          <input <?php echo $disable_style; ?> name="add_cat_organization[]" type="checkbox"
                                 <?php echo in_array( $p->term_id ,  explode(',',$csv_stored_meta['add_cat_organization'][0]) ) ? 'checked' : '' ?>
                                 value="<?php echo $p->term_id ?>" >
                          <label <?php echo $disable_style; ?>><b><?php echo $p->name ?></b></label>
                          <?php
                              $childs = get_terms( $type, array( 'parent' => $p->term_id, 'hide_empty' => false ) );
                              if ( $childs ):
                          ?>
                         <ul>
                              <?php foreach( $childs as $c ):

                                  if ($themeToolData && in_array($p->term_id, $themeToolData)) continue;
                                  if (in_array($c->term_id, $hiddenCats)) continue;
                                ?>
                              <li>
                                  &nbsp; &nbsp;<input <?php echo in_array( $c->term_id , explode(',',$csv_stored_meta['add_cat_organization'][0]) ) ? 'checked' : '' ?> name="add_cat_organization[]" type="checkbox" value="<?php echo $c->term_id ?>" >
                                  <label><?php echo $c->name ?></label>
                              </li>
                              <?php endforeach; ?>
                          </ul>

                          <?php endif; ?>
                      </li>
                      <?php endforeach; ?>

                  </ul>
                </li>
                <?php endforeach; ?>

            </ul>
        </div>
</div>
<hr>
</div>

<?php
//////////////////////////////
// Set defualts for display
//////////////////////////////
$ven = 'none';
if (isset($csv_stored_meta['csv_type']) && $csv_stored_meta['csv_type'][0] == 'venue' ) $ven = 'inline';
?>
<p><input type=radio onclick="hidediv('org');hidediv('evt');showdiv('ven');hidediv('artist');hidediv('pub');hidediv('bus');hidediv('class');"  name=csv_type value="venue" <?php if (isset($csv_stored_meta['csv_type']) && $csv_stored_meta['csv_type'][0] == 'venue' )  echo "checked"; ?>> Venue</p>
<div id=ven style="display:<?php echo $ven; ?>">
        <?php
        // Get all theme tools
        $themeToolClass = new Apollo_Theme_Tool();
        $themeToolData = $themeToolClass->getAllThemeTools();

        $type = Apollo_DB_Schema::_VENUE_PT . '-type';
        $primarys = get_terms($type, array('parent' => false, 'hide_empty' => false));


        ?>

    <p><?php _e( 'Categories:', 'apollo' ) ?></p>
    <div>
        <div data-mod="cat" data-action="<?php echo isset($data_action) ? $data_action : ''; ?>" class="expend add-cat-expand">
          </div>

        <div class="cat-list" <?php echo isset($none_style) ? $none_style : ''; ?> >
            <ul class="cat-listing tbl0">

                <?php
                    $total = count( $primarys );
                    if ( $total > 2 ) {
                        $columns = array(
                            array_slice( $primarys , 0, ceil( $total / 2 ) ),
                            array_slice( $primarys , ceil( $total / 2 ), $total - 1 )
                        );
                    } else {
                        $columns = array(
                            $primarys,
                        );
                    }

                    foreach( $columns as $k => $column ):
                ?>

                <li>
                  <ul class="Llist c<?php echo $k ?>">
                      <?php
                      foreach ( $column as $p ):

                          if ($themeToolData && in_array($p->term_id, $themeToolData)) continue;

                          if (in_array($p->term_id, $hiddenCats)) continue;

                          $disable_style = (isset($selected_pri_id) && $selected_pri_id == $p->term_id) ? 'style="opacity: 0.5"' : '';
                          ?>
                      <li>
                          <input <?php echo $disable_style; ?> name="add_cat_venue[]" type="checkbox"
                                 <?php echo in_array( $p->term_id ,  explode(',',$csv_stored_meta['add_cat_venue'][0]) ) ? 'checked' : '' ?>
                                 value="<?php echo $p->term_id ?>" >
                          <label <?php echo $disable_style; ?>><b><?php echo $p->name ?></b></label>
                          <?php
                              $childs = get_terms( $type, array( 'parent' => $p->term_id, 'hide_empty' => false ) );
                              if ( $childs ):
                          ?>
                         <ul>
                              <?php foreach( $childs as $c ):

                                  if ($themeToolData && in_array($p->term_id, $themeToolData)) continue;
                                  if (in_array($c->term_id, $hiddenCats)) continue;
                                ?>
                              <li>
                                  &nbsp; &nbsp;<input <?php echo in_array( $c->term_id , explode(',',$csv_stored_meta['add_cat_venue'][0]) ) ? 'checked' : '' ?> name="add_cat_venue[]" type="checkbox" value="<?php echo $c->term_id ?>" >
                                  <label><?php echo $c->name ?></label>
                              </li>
                              <?php endforeach; ?>
                          </ul>

                          <?php endif; ?>
                      </li>
                      <?php endforeach; ?>

                  </ul>
                </li>
                <?php endforeach; ?>

            </ul>
        </div>
</div>
<hr>
</div>

<?php
//////////////////////////////
// Set defualts for display
//////////////////////////////
$artist = 'none';
if (isset($csv_stored_meta['csv_type']) && $csv_stored_meta['csv_type'][0] == 'artist' ) $artist = 'inline';
?>
<p><input type=radio onclick="hidediv('org');hidediv('evt');hidediv('ven');showdiv('artist');hidediv('pub');hidediv('bus');hidediv('class');"  name=csv_type value="artist" <?php if (isset($csv_stored_meta['csv_type']) && $csv_stored_meta['csv_type'][0] == 'artist' )  echo "checked"; ?>> Artist</p>
<div id=artist style="display:<?php echo $artist; ?>">
        <?php
        // Get all theme tools
        $themeToolClass = new Apollo_Theme_Tool();
        $themeToolData = $themeToolClass->getAllThemeTools();

        //$key = Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID;
        $type = Apollo_DB_Schema::_ARTIST_PT . '-type';
        $primarys = get_terms($type, array('parent' => false, 'hide_empty' => false));


        ?>

    <p><?php _e( 'Categories:', 'apollo' ) ?></p>
    <div>
        <div data-mod="cat" data-action="<?php echo isset($data_action) ? $data_action : ''; ?>" class="expend add-cat-expand">
          </div>

        <div class="cat-list" <?php echo isset($none_style) ? $none_style : ''; ?> >
            <ul class="cat-listing tbl0">

                <?php
                    $total = count( $primarys );
                    if ( $total > 2 ) {
                        $columns = array(
                            array_slice( $primarys , 0, ceil( $total / 2 ) ),
                            array_slice( $primarys , ceil( $total / 2 ), $total - 1 )
                        );
                    } else {
                        $columns = array(
                            $primarys,
                        );
                    }

                    foreach( $columns as $k => $column ):
                ?>

                <li>
                  <ul class="Llist c<?php echo $k ?>">
                      <?php
                      foreach ( $column as $p ):

                          if ($themeToolData && in_array($p->term_id, $themeToolData)) continue;

                          if (in_array($p->term_id, $hiddenCats)) continue;

                          $disable_style = (isset($selected_pri_id) && $selected_pri_id == $p->term_id) ? 'style="opacity: 0.5"' : '';
                          ?>
                      <li>
                          <input <?php echo $disable_style; ?> name="add_cat_artist[]" type="checkbox"
                                 <?php echo in_array( $p->term_id ,  explode(',',$csv_stored_meta['add_cat_artist'][0]) ) ? 'checked' : '' ?>
                                 value="<?php echo $p->term_id ?>" >
                          <label <?php echo $disable_style; ?>><b><?php echo $p->name ?></b></label>
                          <?php
                              $childs = get_terms( $type, array( 'parent' => $p->term_id, 'hide_empty' => false ) );
                              if ( $childs ):
                          ?>
                         <ul>
                              <?php foreach( $childs as $c ):

                                  if ($themeToolData && in_array($p->term_id, $themeToolData)) continue;
                                  if (in_array($c->term_id, $hiddenCats)) continue;
                                ?>
                              <li>
                                  &nbsp; &nbsp;<input <?php echo in_array( $c->term_id , explode(',',$csv_stored_meta['add_cat_artist'][0]) ) ? 'checked' : '' ?> name="add_cat_artist[]" type="checkbox" value="<?php echo $c->term_id ?>" >
                                  <label><?php echo $c->name ?></label>
                              </li>
                              <?php endforeach; ?>
                          </ul>

                          <?php endif; ?>
                      </li>
                      <?php endforeach; ?>

                  </ul>
                </li>
                <?php endforeach; ?>

            </ul>
        </div>
</div>
<hr>
</div>

<?php
//////////////////////////////
// Set defualts for display
//////////////////////////////
$pub = 'none';
if (isset($csv_stored_meta['csv_type']) && $csv_stored_meta['csv_type'][0] == 'public-art' ) $pub = 'inline';

?>
<p><input type=radio onclick="hidediv('org');hidediv('evt');hidediv('ven');hidediv('artist');showdiv('pub');hidediv('bus');hidediv('class');"  name=csv_type value="public-art" <?php if (isset($csv_stored_meta['csv_type']) && $csv_stored_meta['csv_type'][0] == 'public-art' )  echo "checked"; ?>> Public Art</p>
<div id=pub style="display:<?php echo $pub; ?>">
        <?php
        // Get all theme tools
        $themeToolClass = new Apollo_Theme_Tool();
        $themeToolData = $themeToolClass->getAllThemeTools();

        //$key = Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID;
        $type = Apollo_DB_Schema::_PUBLIC_ART_PT . '-type';
        $primarys = get_terms($type, array('parent' => false, 'hide_empty' => false));

		$locations = get_terms('public-art-location', array('parent' => false, 'hide_empty' => false));

		$mediums = get_terms('public-art-medium', array('parent' => false, 'hide_empty' => false));

		$collections = get_terms('public-art-collection', array('parent' => false, 'hide_empty' => false));

		$artist_qry = "SELECT DISTINCT p.ID, p.post_title FROM $wpdb->apollo_artist_public_art a JOIN $wpdb->posts p on p.ID = a.artist_id";

		$artist_rslt = $wpdb->get_results($artist_qry);
		//print_r($artist_rslt);

        ?>


    <div>
        <div data-mod="cat" data-action="<?php echo isset($data_action) ? $data_action : ''; ?>" class="expend add-cat-expand">
          </div>
    <p><?php _e( 'Categories:', 'apollo' ) ?></p>
        <div class="cat-list" <?php echo isset($none_style) ? $none_style : ''; ?> >
            <ul class="cat-listing tbl0">

                <?php
                    $total = count( $primarys );
                    if ( $total > 2 ) {
                        $columns = array(
                            array_slice( $primarys , 0, ceil( $total / 2 ) ),
                            array_slice( $primarys , ceil( $total / 2 ), $total - 1 )
                        );
                    } else {
                        $columns = array(
                            $primarys,
                        );
                    }

                    foreach( $columns as $k => $column ):
                ?>

                <li>
                  <ul class="Llist c<?php echo $k ?>">
                      <?php
                      foreach ( $column as $p ):

                          if ($themeToolData && in_array($p->term_id, $themeToolData)) continue;

                          if (in_array($p->term_id, $hiddenCats)) continue;

                          $disable_style = (isset($selected_pri_id) && $selected_pri_id == $p->term_id) ? 'style="opacity: 0.5"' : '';
                          ?>
                      <li>
                          <input <?php echo $disable_style; ?> name="add_cat_public_art[]" type="checkbox"
                                 <?php echo in_array( $p->term_id ,  explode(',',$csv_stored_meta['add_cat_public_art'][0]) ) ? 'checked' : '' ?>
                                 value="<?php echo $p->term_id ?>" >
                          <label <?php echo $disable_style; ?>><b><?php echo $p->name ?></b></label>
                          <?php
                              $childs = get_terms( $type, array( 'parent' => $p->term_id, 'hide_empty' => false ) );
                              if ( $childs ):
                          ?>
                         <ul>
                              <?php foreach( $childs as $c ):

                                  if ($themeToolData && in_array($p->term_id, $themeToolData)) continue;
                                  if (in_array($c->term_id, $hiddenCats)) continue;
                                ?>
                              <li>
                                  &nbsp; &nbsp;<input <?php echo in_array( $c->term_id , explode(',',$csv_stored_meta['add_cat_public_art'][0]) ) ? 'checked' : '' ?> name="add_cat_public_art[]" type="checkbox" value="<?php echo $c->term_id ?>" >
                                  <label><?php echo $c->name ?></label>
                              </li>
                              <?php endforeach; ?>
                          </ul>

                          <?php endif; ?>
                      </li>
                      <?php endforeach; ?>

                  </ul>
                </li>
                <?php endforeach; ?>

            </ul>
        </div>
    <p><?php _e( 'Locations:', 'apollo' ) ?></p>
        <div class="location-list" <?php echo isset($none_style) ? $none_style : ''; ?> >
            <ul class="cat-listing tbl0">

                <?php
                    $total = count( $primarys );
                    if ( $total > 2 ) {
                        $columns = array(
                            array_slice( $locations , 0, ceil( $total / 2 ) ),
                            array_slice( $locations , ceil( $total / 2 ), $total - 1 )
                        );
                    } else {
                        $columns = array(
                            $locations,
                        );
                    }

                    foreach( $columns as $k => $column ):
                ?>

                <li>
                  <ul class="Llist c<?php echo $k ?>">
                      <?php
                      foreach ( $column as $p ):

                          if ($themeToolData && in_array($p->term_id, $themeToolData)) continue;

                          if (in_array($p->term_id, $hiddenCats)) continue;

                          $disable_style = (isset($selected_pri_id) && $selected_pri_id == $p->term_id) ? 'style="opacity: 0.5"' : '';
                          ?>
                      <li>
                          <input <?php echo $disable_style; ?> name="add_location_public_art[]" type="checkbox"
                                 <?php echo in_array( $p->term_id ,  explode(',',$csv_stored_meta['add_location_public_art'][0]) ) ? 'checked' : '' ?>
                                 value="<?php echo $p->term_id ?>" >
                          <label <?php echo $disable_style; ?>><b><?php echo $p->name ?></b></label>
                          <?php
                              $childs = get_terms( $type, array( 'parent' => $p->term_id, 'hide_empty' => false ) );
                              if ( $childs ):
                          ?>
                         <ul>
                              <?php foreach( $childs as $c ):

                                  if ($themeToolData && in_array($p->term_id, $themeToolData)) continue;
                                  if (in_array($c->term_id, $hiddenCats)) continue;
                                ?>
                              <li>
                                  &nbsp; &nbsp;<input <?php echo in_array( $c->term_id , explode(',',$csv_stored_meta['public-art-location'][0]) ) ? 'checked' : '' ?> name="public-art-location[]" type="checkbox" value="<?php echo $c->term_id ?>" >
                                  <label><?php echo $c->name ?></label>
                              </li>
                              <?php endforeach; ?>
                          </ul>

                          <?php endif; ?>
                      </li>
                      <?php endforeach; ?>

                  </ul>
                </li>
                <?php endforeach; ?>

            </ul>
        </div>
    <p><?php _e( 'Mediums:', 'apollo' ) ?></p>
        <div class="medium-list" <?php echo isset($none_style) ? $none_style : ''; ?> >
            <ul class="cat-listing tbl0">

                <?php
                    $total = count( $primarys );
                    if ( $total > 2 ) {
                        $columns = array(
                            array_slice( $mediums , 0, ceil( $total / 2 ) ),
                            array_slice( $mediums , ceil( $total / 2 ), $total - 1 )
                        );
                    } else {
                        $columns = array(
                            $mediums,
                        );
                    }

                    foreach( $columns as $k => $column ):
                ?>

                <li>
                  <ul class="Llist c<?php echo $k ?>">
                      <?php
                      foreach ( $column as $p ):

                          if ($themeToolData && in_array($p->term_id, $themeToolData)) continue;

                          if (in_array($p->term_id, $hiddenCats)) continue;

                          $disable_style = (isset($selected_pri_id) && $selected_pri_id == $p->term_id) ? 'style="opacity: 0.5"' : '';
                          ?>
                      <li>
                          <input <?php echo $disable_style; ?> name="add_medium_public_art[]" type="checkbox"
                                 <?php echo in_array( $p->term_id ,  explode(',',$csv_stored_meta['add_medium_public_art'][0]) ) ? 'checked' : '' ?>
                                 value="<?php echo $p->term_id ?>" >
                          <label <?php echo $disable_style; ?>><b><?php echo $p->name ?></b></label>
                          <?php
                              $childs = get_terms( $type, array( 'parent' => $p->term_id, 'hide_empty' => false ) );
                              if ( $childs ):
                          ?>
                         <ul>
                              <?php foreach( $childs as $c ):

                                  if ($themeToolData && in_array($p->term_id, $themeToolData)) continue;
                                  if (in_array($c->term_id, $hiddenCats)) continue;
                                ?>
                              <li>
                                  &nbsp; &nbsp;<input <?php echo in_array( $c->term_id , explode(',',$csv_stored_meta['add_medium_public_art'][0]) ) ? 'checked' : '' ?> name="add_medium_public_art[]" type="checkbox" value="<?php echo $c->term_id ?>" >
                                  <label><?php echo $c->name ?></label>
                              </li>
                              <?php endforeach; ?>
                          </ul>

                          <?php endif; ?>
                      </li>
                      <?php endforeach; ?>

                  </ul>
                </li>
                <?php endforeach; ?>

            </ul>
        </div>
    <p><?php _e( 'Collections:', 'apollo' ) ?></p>
        <div class="collection-list" <?php echo isset($none_style) ? $none_style : ''; ?> >
            <ul class="cat-listing tbl0">

                <?php
                    $total = count( $collections );
                    if ( $total > 2 ) {
                        $columns = array(
                            array_slice( $collections , 0, ceil( $total / 2 ) ),
                            array_slice( $collections , ceil( $total / 2 ), $total - 1 )
                        );
                    } else {
                        $columns = array(
                            $collections,
                        );
                    }

                    foreach( $columns as $k => $column ):
                ?>

                <li>
                  <ul class="Llist c<?php echo $k ?>">
                      <?php
                      foreach ( $column as $p ):

                          if ($themeToolData && in_array($p->term_id, $themeToolData)) continue;

                          if (in_array($p->term_id, $hiddenCats)) continue;

                          $disable_style = (isset($selected_pri_id) && $selected_pri_id == $p->term_id)? 'style="opacity: 0.5"' : '';
                          ?>
                      <li>
                          <input <?php echo $disable_style; ?> name="add_collection_public_art[]" type="checkbox"
                                 <?php echo in_array( $p->term_id ,  explode(',',$csv_stored_meta['add_collection_public_art'][0]) ) ? 'checked' : '' ?>
                                 value="<?php echo $p->term_id ?>" >
                          <label <?php echo $disable_style; ?>><b><?php echo $p->name ?></b></label>
                          <?php
                              $childs = get_terms( $type, array( 'parent' => $p->term_id, 'hide_empty' => false ) );
                              if ( $childs ):
                          ?>
                         <ul>
                              <?php foreach( $childs as $c ):

                                  if ($themeToolData && in_array($p->term_id, $themeToolData)) continue;
                                  if (in_array($c->term_id, $hiddenCats)) continue;
                                ?>
                              <li>
                                  &nbsp; &nbsp;<input <?php echo in_array( $c->term_id , explode(',',$csv_stored_meta['add_collection_public_art'][0]) ) ? 'checked' : '' ?> name="add_collection_public_art[]" type="checkbox" value="<?php echo $c->term_id ?>" >
                                  <label><?php echo $c->name ?></label>
                              </li>
                              <?php endforeach; ?>
                          </ul>

                          <?php endif; ?>
                      </li>
                      <?php endforeach; ?>

                  </ul>
                </li>
                <?php endforeach; ?>

            </ul>
        </div>

          <select name="public_artist"
                  id="_apl_public_artist"
                  class="custom-validate-field"
              >
              <option value=""><?php _e( 'Select Public Artist', 'apollo' ) ?>:</option>
              <?php

                  if ( $primarys ):
                      foreach( $artist_rslt as $p ):

              ?>
              <option <?php echo $csv_stored_meta['public_artist'][0] == $p->ID ? 'selected' : '' ?> value="<?php echo $p->ID ?>"><?php echo $p->post_title ?></option>
              <?php endforeach; endif; ?>
          </select>
<hr>

</div>
<hr>
</div>

<?php
/////////////////////

/////////
// Set defualts for display
//////////////////////////////
$bus = 'none';
if (isset($csv_stored_meta['csv_type']) && $csv_stored_meta['csv_type'][0] == 'business' ) $bus = 'inline';
?>
<p><input type=radio onclick="hidediv('org');hidediv('evt');hidediv('ven');hidediv('artist');hidediv('pub');showdiv('bus');hidediv('class');"  name=csv_type value="business" <?php if (isset($csv_stored_meta['csv_type']) && $csv_stored_meta['csv_type'][0] == 'business' )  echo "checked"; ?>> Business</p>
<div id=bus style="display:<?php echo $bus; ?>">
        <?php
        // Get all theme tools
        $themeToolClass = new Apollo_Theme_Tool();
        $themeToolData = $themeToolClass->getAllThemeTools();

        //$key = Apollo_DB_Schema::_APL_BUSINESS_TERM_PRIMARY_ID;
        $type = Apollo_DB_Schema::_BUSINESS_PT . '-type';
        $primarys = get_terms($type, array('parent' => false, 'hide_empty' => false));


        ?>

    <p><?php _e( 'Categories:', 'apollo' ) ?></p>
    <div>
        <div data-mod="cat" data-action="<?php echo isset($data_action) ? $data_action : ''; ?>" class="expend add-cat-expand">
          </div>

        <div class="cat-list" <?php echo isset($none_style) ? $none_style : ''; ?> >
            <ul class="cat-listing tbl0">

                <?php
                    $total = count( $primarys );
                    if ( $total > 2 ) {
                        $columns = array(
                            array_slice( $primarys , 0, ceil( $total / 2 ) ),
                            array_slice( $primarys , ceil( $total / 2 ), $total - 1 )
                        );
                    } else {
                        $columns = array(
                            $primarys,
                        );
                    }

                    foreach( $columns as $k => $column ):
                ?>

                <li>
                  <ul class="Llist c<?php echo $k ?>">
                      <?php
                      foreach ( $column as $p ):

                          if ($themeToolData && in_array($p->term_id, $themeToolData)) continue;

                          if (in_array($p->term_id, $hiddenCats)) continue;

                          $disable_style = (isset($selected_pri_id) && $selected_pri_id == $p->term_id) ? 'style="opacity: 0.5"' : '';
                          ?>
                      <li>
                          <input <?php echo $disable_style; ?> name="add_cat_business[]" type="checkbox"
                                 <?php echo in_array( $p->term_id ,  explode(',',$csv_stored_meta['add_cat_business'][0]) ) ? 'checked' : '' ?>
                                 value="<?php echo $p->term_id ?>" >
                          <label <?php echo $disable_style; ?>><b><?php echo $p->name ?></b></label>
                          <?php
                              $childs = get_terms( $type, array( 'parent' => $p->term_id, 'hide_empty' => false ) );
                              if ( $childs ):
                          ?>
                         <ul>
                              <?php foreach( $childs as $c ):

                                  if ($themeToolData && in_array($p->term_id, $themeToolData)) continue;
                                  if (in_array($c->term_id, $hiddenCats)) continue;
                                ?>
                              <li>
                                  &nbsp; &nbsp;<input <?php echo in_array( $c->term_id , explode(',',$csv_stored_meta['add_cat_business'][0]) ) ? 'checked' : '' ?> name="add_cat_business[]" type="checkbox" value="<?php echo $c->term_id ?>" >
                                  <label><?php echo $c->name ?></label>
                              </li>
                              <?php endforeach; ?>
                          </ul>

                          <?php endif; ?>
                      </li>
                      <?php endforeach; ?>

                  </ul>
                </li>
                <?php endforeach; ?>

            </ul>
        </div>
</div>
<hr>
</div>

<p><input type=radio onclick="hidediv('evt');"  name=csv_type value="user" <?php if (isset($csv_stored_meta['csv_type']) && $csv_stored_meta['csv_type'][0] == 'user' )  echo "checked"; ?>> User</p>

<?php
//////////////////////////////
// Set defualts for display
//////////////////////////////
$class = 'none';
if (isset($csv_stored_meta['csv_type']) && $csv_stored_meta['csv_type'][0] == 'classified' ) $class = 'inline';
?>
<p><input type=radio onclick="hidediv('org');hidediv('evt');hidediv('ven');hidediv('artist');hidediv('pub');hidediv('bus');showdiv('class');"  name=csv_type value="classified" <?php if (isset($csv_stored_meta['csv_type']) && $csv_stored_meta['csv_type'][0] == 'classified' )  echo "checked"; ?>> Classified</p>
<div id=class style="display:<?php echo $class; ?>">
        <?php
        // Get all theme tools
        $themeToolClass = new Apollo_Theme_Tool();
        $themeToolData = $themeToolClass->getAllThemeTools();

        //$key = Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID;
        $type = Apollo_DB_Schema::_CLASSIFIED . '-type';
        $primarys = get_terms($type, array('parent' => false, 'hide_empty' => false));


        ?>

    <p><?php _e( 'Categories:', 'apollo' ) ?></p>
    <div>
        <div data-mod="cat" data-action="<?php echo isset($data_action) ? $data_action : ''; ?>" class="expend add-cat-expand">
          </div>

        <div class="cat-list" <?php echo isset($none_style) ? $none_style : ''; ?> >
            <ul class="cat-listing tbl0">

                <?php
                    $total = count( $primarys );
                    if ( $total > 2 ) {
                        $columns = array(
                            array_slice( $primarys , 0, ceil( $total / 2 ) ),
                            array_slice( $primarys , ceil( $total / 2 ), $total - 1 )
                        );
                    } else {
                        $columns = array(
                            $primarys,
                        );
                    }

                    foreach( $columns as $k => $column ):
                ?>

                <li>
                  <ul class="Llist c<?php echo $k ?>">
                      <?php
                      foreach ( $column as $p ):

                          if ($themeToolData && in_array($p->term_id, $themeToolData)) continue;

                          if (in_array($p->term_id, $hiddenCats)) continue;

                          $disable_style = (isset($selected_pri_id) && $selected_pri_id == $p->term_id) ? 'style="opacity: 0.5"' : '';
                          ?>
                      <li>
                          <input <?php echo $disable_style; ?> name="add_cat_classified[]" type="checkbox"
                                 <?php echo in_array( $p->term_id ,  explode(',',$csv_stored_meta['add_cat_classified'][0]) ) ? 'checked' : '' ?>
                                 value="<?php echo $p->term_id ?>" >
                          <label <?php echo $disable_style; ?>><b><?php echo $p->name ?></b></label>
                          <?php
                              $childs = get_terms( $type, array( 'parent' => $p->term_id, 'hide_empty' => false ) );
                              if ( $childs ):
                          ?>
                         <ul>
                              <?php foreach( $childs as $c ):

                                  if ($themeToolData && in_array($p->term_id, $themeToolData)) continue;
                                  if (in_array($c->term_id, $hiddenCats)) continue;
                                ?>
                              <li>
                                  &nbsp; &nbsp;<input <?php echo in_array( $c->term_id , explode(',',$csv_stored_meta['add_cat_classified'][0]) ) ? 'checked' : '' ?> name="add_cat_classified[]" type="checkbox" value="<?php echo $c->term_id ?>" >
                                  <label><?php echo $c->name ?></label>
                              </li>
                              <?php endforeach; ?>
                          </ul>

                          <?php endif; ?>
                      </li>
                      <?php endforeach; ?>

                  </ul>
                </li>
                <?php endforeach; ?>

            </ul>
        </div>
</div>
<hr>
</div>

<p><input type=radio onclick="hidediv('evt');"  name=csv_type value="program" <?php if (isset($csv_stored_meta['csv_type']) && $csv_stored_meta['csv_type'][0] == 'program' )  echo "checked"; ?>> Education Program</p>

<p><input type=radio onclick="hidediv('evt');"  name=csv_type value="educator" <?php if (isset($csv_stored_meta['csv_type']) && $csv_stored_meta['csv_type'][0] == 'educator' )  echo "checked"; ?>> Educators</p>




<hr>
<?php if (isset($csv_stored_meta['csv_type']) && $csv_stored_meta['csv_type'][0] != '') : ?>
<p><b>If you make changes to fields above, please click the update button to save those changes before using the download button.</b></p>
<p><a class="button button-primary"  href="<?php echo $url; ?>/csv_download.php?unique=<? echo time(); ?>&apid=<?php echo $post->ID; ?>">Download CSV file</a></p>
<?php endif; ?>

<?php

}

/**
 * Saves the custom meta input
 */
function csv_meta_save( $post_id ) {

    global $post;

    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'csv_nonce' ] ) && wp_verify_nonce( $_POST[ 'csv_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';

    // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }

    // update checkboxes


    if( isset( $_POST[ 'csv_type' ] ) ) {
         update_post_meta( $post_id, 'csv_type', $_POST[ 'csv_type' ] );
    }

    checkPostMeta($post_id, 'csv_incind');
    checkPostMeta($post_id, 'csv_publish');
    checkPostMeta($post_id, 'csv_pending');
    checkPostMeta($post_id, 'csv_draft');
    checkPostMeta($post_id, 'csv_dateStart');
    checkPostMeta($post_id, 'csv_dateEnd');
    checkPostMeta($post_id, 'csv_filter');
    checkPostMeta($post_id, 'csv_report');

    if( isset($_POST[ 'csv-export-event-meta-org' ] ) ) {
        $selected = explode(',', $_POST['hidden-csv-export-event-meta-org']);
        $orgsArr = array_filter(array_unique(array_merge($_POST[ 'csv-export-event-meta-org' ], $selected)));
        update_post_meta( $post_id, 'csv-export-event-meta-org', $orgsArr);
    } else {
        update_post_meta( $post_id, 'csv-export-event-meta-org', '' );
    }

    if( isset( $_POST[ 'csv-export-event-meta-venue' ] ) ) {
        $selected = explode(',', $_POST['hidden-csv-export-event-meta-venue']);
        $venuesArr = array_filter(array_unique(array_merge($_POST[ 'csv-export-event-meta-venue' ], $selected)));
        update_post_meta( $post_id, 'csv-export-event-meta-venue', $venuesArr );
    }
    else{
        update_post_meta( $post_id, 'csv-export-event-meta-venue', '' );
    }

    checkPostMeta($post_id, 'csv-export-event-meta-cities');
    checkPostMeta($post_id, 'csv-export-event-meta-zip');

    if(isset($_POST['csv_dateStart']) && $_POST[ 'csv_dateStart' ] != '' ) {
         update_post_meta( $post_id, 'csv_drange', '' );
    } else {
        checkPostMeta( $post_id, 'csv_drange' );
    }

    checkPostMeta($post_id, 'term_primary');
    checkPostMeta($post_id, 'term_primary_organization');
    checkPostMeta($post_id, 'term_primary_venue');
    checkPostMeta($post_id, 'term_primary_artist');
    checkPostMeta($post_id, 'term_primary_public_art');
    checkPostMeta($post_id, 'term_primary_classified');
    checkPostMeta($post_id, 'term_primary_business');
    checkPostMeta($post_id, 'add_cat', true);
    checkPostMeta($post_id, 'add_cat_organization', true);
    checkPostMeta($post_id, 'add_cat_venue', true);
    checkPostMeta($post_id, 'add_cat_artist', true);
    checkPostMeta($post_id, 'add_cat_business', true);
    checkPostMeta($post_id, 'add_cat_public_art', true);
    checkPostMeta($post_id, 'add_location_public_art', true);
    checkPostMeta($post_id, 'add_medium_public_art', true);
    checkPostMeta($post_id, 'add_collection_public_art', true);
    checkPostMeta($post_id, 'public_artist');
    checkPostMeta($post_id, 'add_cat_classified', true);
    checkPostMeta($post_id, 'venue_zip');
    checkPostMeta($post_id, 'organization');
    checkPostMeta($post_id, 'venue');
    checkPostMeta($post_id, 'event_city');
    checkPostMeta($post_id, 'venue_zip');

}

function checkPostMeta($postId, $key, $isImplode = false)
{
    if (!empty($_POST[$key])) {
        $value = $isImplode ? implode(',', $_POST[$key]) : $_POST[$key];
        update_post_meta($postId, $key, $value);
    } else {
        update_post_meta($postId, $key, '');
    }

}

add_action( 'save_post', 'csv_meta_save' );

//echo "<PRE>";
//print_r($_POST);exit;
?>