<?php
$listCityState = Apollo_App::getCityStateByTerritory(false);

$citiesSelected = [];

if (isset($csv_stored_meta['csv-export-event-meta-cities']) && isset($csv_stored_meta['csv-export-event-meta-cities'][0])){
    if(is_serialized($csv_stored_meta['csv-export-event-meta-cities'][0])){
        $citiesSelected = maybe_unserialize($csv_stored_meta['csv-export-event-meta-cities'][0]);
    }
    else{
        $citiesSelected = $csv_stored_meta['csv-export-event-meta-cities'][0];
    }
}

$selectedHtml = '';
if (!empty($citiesSelected)) {
    foreach($citiesSelected as $city) {
        if (!$city) continue;
        $selectedHtml .= sprintf('<li><a>%s</a></li>', $city);
    }
} else {
    $selectedHtml = __('No data', 'apollo');
}

?>
<div class="wrap-ddl-blk">
<?php

foreach($listCityState as $state => $cityState){
    if ($state === 0) continue;
    $cities = Apollo_App::getCityByTerritory(false,$state,false);
    echo '<strong>' . $state .'</strong>';
    foreach ($cities as $cityKey => $cityName):
        // Description
        apollo_wp_multi_checkbox(array(
                'id' => 'csv-export-event-meta-city-' . $cityKey,
                'name' => 'csv-export-event-meta-cities[]',
                'label' => $cityName,
                'desc_tip' => 'true',
                'description' => '',
                'type' => 'checkbox',
                'default' => false,
                'value' => $citiesSelected,
                'cbvalue' => $cityKey)
        );
    endforeach;
}

echo '</div>';
echo sprintf('<div class="wrapper-selected"><i><strong>%s</strong></i><div class="wrap-ddl-blk h-100"><ul>%s</ul></div></div>', __('Selected Items', 'apollo'), $selectedHtml);

?>



