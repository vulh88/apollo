<?php
$zipOptions = Apollo_App::getZipByTerritory(false,'',false,false);

$zipSelected = [];

if (isset($csv_stored_meta['csv-export-event-meta-zip']) && isset($csv_stored_meta['csv-export-event-meta-zip'][0])){
    if(is_serialized($csv_stored_meta['csv-export-event-meta-zip'][0])){
        $zipSelected = maybe_unserialize($csv_stored_meta['csv-export-event-meta-zip'][0]);
    }
    else{
        $zipSelected = $csv_stored_meta['csv-export-event-meta-zip'][0];
    }
}

$selectedHtml = '';
if (!empty($zipSelected)) {
    foreach($zipSelected as $zip) {
        if (!$zip) continue;
        $selectedHtml .= sprintf('<li><a>%s</a></li>', $zip);
    }
} else {
    $selectedHtml = __('No data', 'apollo');
}

?>
<div class="wrap-ddl-blk">
<?php foreach ($zipOptions as $zip):
    // Description
    apollo_wp_multi_checkbox(array(
            'id' => 'csv-export-event-meta-zip-' . $zip,
            'name' => 'csv-export-event-meta-zip[]',
            'label' => $zip,
            'desc_tip' => 'true',
            'description' => '',
            'type' => 'checkbox',
            'default' => false,
            'value' => $zipSelected,
            'cbvalue' => $zip)
    );
endforeach;
echo '</div>';
echo sprintf('<div class="wrapper-selected"><i><strong>%s</strong></i><div class="wrap-ddl-blk h-100"><ul>%s</ul></div></div>', __('Selected Items', 'apollo'), $selectedHtml);

?>



