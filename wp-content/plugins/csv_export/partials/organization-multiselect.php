<?php
$orgListing = apl_instance('APL_Lib_Helpers_PostTypeListing', array(
    'postType' => Apollo_DB_Schema::_ORGANIZATION_PT,
));

$orgListing->setLimit(100);

$orgOptions = $orgListing->get();

$orgSelected = array();

if (isset($csv_stored_meta['csv-export-event-meta-org']) && isset($csv_stored_meta['csv-export-event-meta-org'][0])) {
    if (is_serialized($csv_stored_meta['csv-export-event-meta-org'][0])) {
        $orgSelected = maybe_unserialize($csv_stored_meta['csv-export-event-meta-org'][0]);
    } else {
        $orgSelected = array();
    }
}

$selectedHtml = '';
if (!empty($orgSelected)) {
    foreach ($orgSelected as $orgId) {
        if (!$orgId) continue;
        $selectedInput =  '<input type="checkbox" class="checkbox-hidden checkbox " name="csv-export-event-meta-org[]" id="hiddencsv-export-event-meta-org-'.$orgId.'" value="'.$orgId.'" checked>';
        $selectedHtml .= sprintf('<li>%s<a target="_blank" href="%s">%s</a></li>',$selectedInput, get_edit_post_link($orgId), get_the_title($orgId));
    }
} else {
    $selectedHtml = __('No data', 'apollo');
}

$orgURL = admin_url(sprintf('admin-ajax.php?action=apollo_show_more_posts_type&post_type=%s&page=2&selected=%s&meta_name=%s', Apollo_DB_Schema::_ORGANIZATION_PT, implode(",", $orgSelected), 'csv-export-event-meta-org'));

echo sprintf('<div class="wrap-ddl-blk"
                       data-loading="<p class=\'form-field loading\'>%s</p>"
                       data-ride="scroll-more" data-action="%s"
                       data-has-more="%s" >', __('Loading ...', 'apollo'), $orgURL, $orgListing->hasMore());
echo sprintf('<input type="hidden" name="hidden-csv-export-event-meta-org" class="hidden-meta" value="%s" />', implode(',', $orgSelected));

foreach ($orgOptions as $org):
    // Description
    apollo_wp_multi_checkbox(array(
            'id' => 'csv-export-event-meta-org-' . $org->ID,
            'name' => 'csv-export-event-meta-org[]',
            'label' => $org->post_title,
            'desc_tip' => 'true',
            'description' => '',
            'type' => 'checkbox',
            'default' => false,
            'value' => $orgSelected,
            'cbvalue' => $org->ID)
    );
endforeach;

echo '</div>';
echo sprintf('<div class="wrapper-selected"><i><strong>%s</strong></i><div class="wrap-ddl-blk h-100"><ul>%s</ul></div></div>', __('Selected Items', 'apollo'), $selectedHtml);

?>



