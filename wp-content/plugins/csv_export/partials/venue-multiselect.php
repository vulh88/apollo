<?php
$venueListing = apl_instance('APL_Lib_Helpers_PostTypeListing', array(
    'postType' => Apollo_DB_Schema::_VENUE_PT,
));

$venueListing->setLimit(100);

$venueOptions = $venueListing->get();

$venueSelected = array();

if (isset($csv_stored_meta['csv-export-event-meta-venue']) && isset($csv_stored_meta['csv-export-event-meta-venue'][0])) {
    if (is_serialized($csv_stored_meta['csv-export-event-meta-venue'][0])) {
        $venueSelected = maybe_unserialize($csv_stored_meta['csv-export-event-meta-venue'][0]);
    } else {
        $venueSelected = array();
    }
}
$selectedHtml = '';
if (!empty($venueSelected)) {
    foreach ($venueSelected as $venueId) {
        if (!$venueId) continue;
        $selectedInput =  '<input type="checkbox" class="checkbox-hidden checkbox " name="csv-export-event-meta-venue[]" id="hiddencsv-export-event-meta-venue-'.$venueId.'" value="'.$venueId.'" checked>';
        $selectedHtml .= sprintf('<li>%s<a target="_blank" href="%s">%s</a></li>',$selectedInput, get_edit_post_link($venueId), get_the_title($venueId));
    }
} else {
    $selectedHtml = __('No data', 'apollo');
}

$venueURL = admin_url(sprintf('admin-ajax.php?action=apollo_show_more_posts_type&post_type=%s&page=2&selected=%s&meta_name=%s', Apollo_DB_Schema::_VENUE_PT, implode(",", $venueSelected), 'csv-export-event-meta-venue'));

echo sprintf('<div class="wrap-ddl-blk"
			data-loading="<p class=\'form-field loading\'>%s</p>"
			data-ride="scroll-more" data-action="%s"
			data-has-more="%s" >', __('Loading ...', 'apollo'), $venueURL, $venueListing->hasMore());
echo sprintf('<input type="hidden" name="hidden-csv-export-event-meta-venue" class="hidden-meta" value="%s" />', implode(',', $venueSelected));

foreach ($venueOptions as $venue):
    // Description
    apollo_wp_multi_checkbox(array(
        'id' => 'csv-export-event-meta-venue-' . $venue->ID,
        'name' => 'csv-export-event-meta-venue[]',
        'label' => $venue->post_title,
        'desc_tip' => 'true',
        'description' => '',
        'type' => 'checkbox',
        'default' => false,
        'value' => $venueSelected,
        'cbvalue' => $venue->ID
    ));
endforeach;
echo '</div>';
echo sprintf('<div class="wrapper-selected"><i><strong>%s</strong></i><div class="wrap-ddl-blk h-100"><ul>%s</ul></div></div>', __('Selected Items', 'apollo'), $selectedHtml);

?>