<?php
/*
Plugin Name: CSV Export
Plugin URI: NONE
Description: Declares a plugin that will allow CSV file downloads for custom post types
Version: 1.0
Author: BEST4WEB
Author URI: NONE
License: GPL
*/


function create_csvexport()
{
    register_post_type('csvexport',
        array(
            'labels' => array(
                'name' => 'CSV Export',
                'singular_name' => 'CSV Export',
                'add_new' => 'Add New',
                'add_new_item' => 'CSV Export Information',
                'edit' => 'Edit',
                'edit_item' => 'Edit CSV Export',
                'new_item' => 'New CSV Export',
                'view' => 'View',
                'view_item' => 'View CSV Export',
                'search_items' => 'Search CSV Export',
                'not_found' => 'No CSV export found',
                'not_found_in_trash' => 'No CSV export found in Trash',
                'parent' => 'Parent CSV Export'
            ),

            'public' => true,
            'menu_position' => 15,
            'supports' => ('title'),
            'taxonomies' => array(''),
            'menu_icon' => plugins_url('images/image.png', __FILE__),
            'has_archive' => false
        )
    );
}

function change_default_csvexport_title($title)
{
    $screen = get_current_screen();

    if ('csvexport' == $screen->post_type) {
        $title = 'CSV Export Name';
    }

    return $title;
}

add_filter('enter_title_here', 'change_default_csvexport_title');


function posttype_admin_css()
{
    global $post_type;
    if ($post_type == 'csvexport') {
        echo '<style type="text/css">#edit-slug-box,#view-post-btn,#post-    preview,.updated p a{display: none;}</style>';
    }
}

add_action('admin_head', 'posttype_admin_css');


function my_edit_csvexport_columns($columns)
{

    $columns = array(
        'cb' => '<input type="checkbox" />',
        'id' => __('ID'),
        'title' => __('CSV Export Name')
    );

    return $columns;
}


function my_manage_csvexport_columns($column, $post_id)
{
    global $post;

    switch ($column) {

        case 'id' :
            echo $post_id;
            break;

    }
}


include 'csv_box.php';

add_action('init', 'create_csvexport');

add_filter('manage_edit-csvexport_columns', 'my_edit_csvexport_columns');

add_action('manage_csvexport_posts_custom_column', 'my_manage_csvexport_columns', 10, 7);


?>