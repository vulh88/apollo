<?php
/*
Plugin Name: CSV bulk action download
Description: Export post list data to CSV file
Author: BEST4WEB
Version: 1.0
*/

if (!class_exists('B4W_Custom_Bulk_Action')) {

    class B4W_Custom_Bulk_Action
    {

        public function __construct()
        {

            if (is_admin()) {
                // admin actions/filters
                add_action('admin_footer-edit.php', array(&$this, 'custom_bulk_admin_footer'));
                add_action('load-edit.php', array(&$this, 'custom_bulk_action'));
                add_action('admin_notices', array(&$this, 'custom_bulk_admin_notices'));
            }
        }


        /**
         * Step 1: add the custom Bulk Action to the select menus
         */
        function custom_bulk_admin_footer()
        {
            global $post_type;

            $allowed_post_types = array('grant_education', 'organization');
            if (in_array($post_type, $allowed_post_types)) {
                ?>
                <script type="text/javascript">
                    jQuery(document).ready(function () {
                        jQuery('<option>').val('export').text('<?php _e('Export to CSV')?>').appendTo("select[name='action']");
                        jQuery('<option>').val('export').text('<?php _e('Export to CSV')?>').appendTo("select[name='action2']");
                    });
                </script>
                <?php
            }
        }


        /**
         * Step 2: handle the custom Bulk Action
         */
        function custom_bulk_action()
        {
            global $typenow;
            $post_type = $typenow;

            $allowed_post_types = array('grant_education', 'organization');
            if (in_array($post_type, $allowed_post_types)) {

                // get the action
                $wp_list_table = _get_list_table('WP_Posts_List_Table');  // depending on your resource type this could be WP_Users_List_Table, WP_Comments_List_Table, etc
                $action = $wp_list_table->current_action();

                $allowed_actions = array("export");
                if (!in_array($action, $allowed_actions)) return;

                // security check
                check_admin_referer('bulk-posts');

                // make sure ids are submitted.  depending on the resource type, this may be 'media' or 'ids'
                if (isset($_REQUEST['post'])) {
                    $post_ids = array_map('intval', $_REQUEST['post']);
                }

                if (empty($post_ids)) return;

                // this is based on wp-admin/edit.php
                $sendback = remove_query_arg(array('exported', 'untrashed', 'deleted', 'ids'), wp_get_referer());
                if (!$sendback)
                    $sendback = admin_url("edit.php?post_type=$post_type");

                $pagenum = $wp_list_table->get_pagenum();
                $sendback = add_query_arg('paged', $pagenum, $sendback);

                switch ($action) {

                    case 'export':

                        $exported = 0;
                        foreach ($post_ids as $post_id) {
                            $exported++;
                        }

                        $sendback = add_query_arg(array('exported' => $exported, 'ids' => join(',', $post_ids)), $sendback);
                        $this->perform_export($post_ids);
                        break;

                    default:
                        return;
                }

                $sendback = remove_query_arg(array('action', 'action2', 'tags_input', 'post_author', 'comment_status', 'ping_status', '_status', 'post', 'bulk_edit', 'post_view'), $sendback);

                wp_redirect($sendback);
                exit();
            }
        }


        /**
         * Step 3: display an admin notice on the Posts page after exporting
         */
        function custom_bulk_admin_notices()
        {
            global $post_type, $pagenow;

            $allowed_post_types = array('grant_education', 'organization');

            if ($pagenow == 'edit.php' && in_array($post_type, $allowed_post_types) && isset($_REQUEST['exported']) && (int)$_REQUEST['exported']) {
                $message = sprintf(_n('Post exported.', '%s posts exported.', $_REQUEST['exported']), number_format_i18n($_REQUEST['exported']));
                $link = "Download " . $_SESSION['csv_filename'];
                $siteurl = get_site_url();
                echo "<div class=\"updated\"><p>{$message} <a href='" . $siteurl . "/csv_file_download.php'><b>$link</b></a></p></div>";
            }
        }

        function perform_export($post_ids)
        {
            global $typenow;
            $post_type = $typenow;

            if ($post_type == 'organization') $csv_file = $this->get_org_csv($post_ids);
            if ($post_type == 'grant_education') $csv_file = $this->get_grant_csv($post_ids);

            $date = date('m-d-Y');
            session_start();
            $_SESSION['csv_file'] = $csv_file; // store session data
            $_SESSION['csv_filename'] = $post_type . "_" . $date . "_" . "download.csv"; // store session data
            return true;
        }


        function get_org_csv($post_ids)
        {
            global $wpdb;

            $output = "orgid, name, address1, address2, city, state, zip, phone, fax, email, website_url, blog_url, instagram_url, twitter_url, pinterest_url, facebook_url, linkedin url, category_types, image_name, contactName, contactPhone, contactEmail \r\n";

            foreach ($post_ids as $post_id) {
                $org_name = wp_get_single_post($post_id);
                $orgid = (string)$post_id;

                $org_addr = $wpdb->get_results("select meta_value from $wpdb->apollo_organizationmeta where meta_key ='_apl_org_address' and apollo_organization_id = " . $post_id);
                $addr = unserialize(unserialize($org_addr[0]->meta_value));

                $org_data = $wpdb->get_results("select meta_key, meta_value from $wpdb->apollo_organizationmeta where meta_key ='_apl_org_data' and apollo_organization_id = " . $post_id);
                $data = unserialize(unserialize($org_data[0]->meta_value));

                $querystr = "SELECT * from " . $wpdb->prefix . "term_relationships join " . $wpdb->prefix . "terms on term_id = term_taxonomy_id where object_id = $post_id";
                $terms = $wpdb->get_results($querystr, OBJECT);

                $org_cats = '';
                foreach ($terms as $term) {
                    $org_cats .= $term->name . ",";

                }
                $org_cats = substr($org_cats, 0, -1);
                $org_category = '"' . $org_cats . '"';
                $org_img = wp_get_attachment_url(get_post_thumbnail_id($post_id));

                $output .= $orgid . "," . $org_name->post_title . "," . $addr['_org_address1'] . "," . $addr['_org_address2'] . "," . $addr['_org_city'] . "," . $addr['_org_state'] . "," . $addr['_org_zip'] . "," . $data['_org_phone'] . "," . $data['_org_fax'] .
                    "," . $data['_org_email'] . "," . $data['_org_website_url'] . "," . $data['_org_blog_url'] . "," . $data['_org_instagram_url'] . "," . $data['_org_twitter_url'] . "," . $data['_org_pinterest_url'] . "," . $data['_org_facebook_url'] .
                    "," . $data['_org_linkedin_url'] . "," . $org_category . "," . $org_image . "," . $data['_org_contact_name'] . "," . $data['_org_contact_phone'] . "," . $data['_org_contact_email'] . "\r\n";
            }
            return $output;

        }


        function get_grant_csv($post_ids)
        {
            global $wpdb;

            $output = "grant_id, teacher_name, email, school_name, contactphone, district, grade, num_students, organization, program_title, grantfor, other, event_date,funds_needed, permission, principal, total, transport, admission, artist_fee, materials, other_cost, purpose, curriculum  \r\n";

            foreach ($post_ids as $post_id) {
                $grant_name = wp_get_single_post($post_id);
                $name = '"' . $grant_name->post_title . '"';
                $desc = '"' . $grant_name->post_content . '"';

                $querystr = "SELECT * from " . $wpdb->prefix . "apollo_grant_educationmeta where meta_key = '_apl_post_type_cf_data' and apollo_grant_education_id = $post_id";
                $grant = $wpdb->get_results($querystr, OBJECT);

                $str = $grant[0]->meta_value;
                $data = @unserialize($str);
                if ($data !== false) {
                    $grant = unserialize($grant[0]->meta_value);
                }

                $str = $grant;
                $data = @unserialize($str);
                if ($data !== false) {
                    $grant = unserialize($grant);
                }

                $teacher = $grant['cf_field_1432955237TwpmI'];
                $school = $grant['cf_field_1441079165ppNoc'];
                $email = $grant['cf_field_1433305995kqbJ2'];
                $contact = $grant['cf_field_1433305995KpuG3'];
                $district = $grant['cf_field_1441079165pjLYl'];
                $grade = $grant['cf_field_1441079600QjNPh'];
                $students = $grant['cf_field_1441079600iA5o1'];
                $org = $grant['cf_field_1441080139ozRB0'];
                $program = $grant['cf_field_1441080139tTjEP'];
                $grantfor = $grant['cf_field_14410806186uuKJ'];
                $other = $grant['cf_field_14410806186uuKJ_other_choice'];
                $event_date = $grant['cf_field_1441080839TQozu'];
                $funds_needed = $grant['cf_field_1441080929g8u8B'];
                $permission = $grant['cf_field_1441081070z7bWA'];
                $principal = $grant['cf_field_14410810703RREx'];
                $total = $grant['cf_field_1441081469S3oc7'];
                $transport = $grant['cf_field_14410814694k7nl'];
                $admission = $grant['cf_field_1441081469WFAP0'];
                $artist_fee = $grant['cf_field_14410814691ekhm'];
                $materials = $grant['cf_field_144108146910jnl'];
                $other_cost = $grant['cf_field_1441081469Bxv0k'];
                $purpose = $grant['cf_field_14410817774FLa5'];
                $relate = $grant['cf_field_1441081860SbEje'];
                $intid = (string)$post_id;

                $purpose = str_replace('"', "&quot;", $purpose);
                $relate = str_replace('"', "&quot;", $relate);

                $output .= $intid . "," . $teacher . "," . $email . "," . $school . "," . $contact . "," . $district . "," . $grade . "," . $students . "," . $org . "," . $program . ",\"" . $grantfor . "\",\"" . $other . "\",\"" . $event_date . "\",\"" . $funds_needed . "\"," . $permission . "," . $principal . ",\"" . $total . "\"," . $transport . "," . $admission . ",\"" . $artist_fee . "\",\"" . $materials . "\",\"" . $other_cost . "\",\"" . $purpose . "\",\"" . $relate . "\"\r\n";
            }
            return $output;

        }

    }  // end class
}  // end if class exists

new B4W_Custom_Bulk_Action();

/////////////////////////////////////
// Add print action to admin menu bar
/////////////////////////////////////

function wp_admin_bar_new_item()
{
    global $post, $post_type, $pagenow;

    if (($post_type == 'grant_education') && ($pagenow == 'post.php')) {

        global $wp_admin_bar;
        $wp_admin_bar->add_menu(array(
            'id' => 'wp-admin-bar-new-item',
            'title' => __('Print Grant Page'),
            "meta" => array("target" => 'print'),
            'href' => $siteurl . '/print_grant.php?post=' . $post->ID
        ));
    }
}

add_action('wp_before_admin_bar_render', 'wp_admin_bar_new_item');
