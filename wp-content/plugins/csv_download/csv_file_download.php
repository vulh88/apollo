<?

session_start();
$file = $_SESSION['csv_file'];
$filename = $_SESSION['csv_filename'];
$fileSize = strlen($file);

// Output headers.
header("Cache-Control: private");
header("Content-Type: application/stream");
header("Content-Length: " . $fileSize);
header("Content-Disposition: attachment; filename=" . $filename);

// Output file.
flush();
echo $file;
flush();
exit;

?>