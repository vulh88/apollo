<?php
//////////////////////////////////////////////////////
// Display current offers and allow edit of event
//////////////////////////////////////////////////////
global $wpdb;

	// reset current offers
	update_post_meta($post->ID, 'wecamefromcurrentoffers', 'N');

		$querystr = "select p.ID, p.post_title, post_author, post_date from ".$wpdb->prefix."posts AS p join ".$wpdb->prefix."apollo_event_org AS eo on eo.post_id = p.ID join ".$wpdb->prefix."apollo_eventmeta AS em on em.apollo_event_id = p.ID and em.meta_key = '_apollo_event_end_date' and em.meta_value > curdate() where p.post_type = 'event' and p.post_status = 'publish' group by p.ID order by post_title";
		//echo $querystr; die;
		$events = $wpdb->get_results($querystr, OBJECT);

		$offer_count = 0;

		// Add escape back to ticketing button
		echo "<a class=button style='vertical-align:middle;' href='post.php?post=".$post->ID."&action=edit'>Return To Ticket Association</a>&nbsp;&nbsp;&nbsp;&nbsp;<span style='font-size:14px;font-weight:800;' id=\"offer_count\"></span><br>";

		echo "<table class='wp-list-table widefat fixed posts'>
				<tr><td style='font-size:15px; border-bottom:1px solid #dcdcdc;'>Img</td><td style='font-size:15px; border-bottom:1px solid #dcdcdc;'>Name</td>
				<td style='font-size:15px; border-bottom:1px solid #dcdcdc;'>Organization</td><td style='font-size:15px; border-bottom:1px solid #dcdcdc;'>Start Date</td>
				<td style='font-size:15px; border-bottom:1px solid #dcdcdc;'>End Date</td><td style='font-size:15px; border-bottom:1px solid #dcdcdc;'>Category(s)</td>
				<td style='font-size:15px; border-bottom:1px solid #dcdcdc;'>Date</td><td style='font-size:15px; border-bottom:1px solid #dcdcdc;'>Associated User</td></tr>";

		//echo "<table class='wp-list-table widefat fixed posts'>";

		foreach ($events as $event){

			list ($eventdate, $dummy) = explode(' ', $event->post_date);
			$eventdate = date("m-d-Y", strtotime($eventdate));

		/////////////////////////////////////
		// Get orgs for this event
		/////////////////////////////////////

			$eventID = $event->ID;
			$querystr = "select meta_key from ".$wpdb->prefix."postmeta where post_id = $post->ID and meta_value like '%:\"$eventID%'";
			$orgpair = $wpdb->get_results($querystr, OBJECT);

			list($orgP, $orgT) = explode('_', $orgpair[0]->meta_key);

		/////////////////////////////////////
		// Get times for this event
		/////////////////////////////////////

			$querystr = "select * from $wpdb->apollo_event_calendar where event_id = ".$eventID;
			$hastime = $wpdb->get_results($querystr, OBJECT);

		/////////////////////////////////////
		// Get org name for this event
		/////////////////////////////////////

			$org_name = $wpdb->get_results("select post_title, ID from $wpdb->posts where ID = ".$orgP);
			$orgname = $org_name[0]->post_title;

		/////////////////////////////////////
		// Get start and end date plus primary category for this event
		/////////////////////////////////////

     			$start_date = $wpdb->get_results("SELECT meta_value FROM $wpdb->apollo_eventmeta WHERE meta_key = '_apollo_event_start_date' AND apollo_event_id = " . $eventID);
			$start_date = date("m-d-Y", strtotime($start_date[0]->meta_value));
       		$end_date = $wpdb->get_results("SELECT meta_value FROM $wpdb->apollo_eventmeta WHERE meta_key = '_apollo_event_end_date' AND apollo_event_id = " .  $eventID);
			$end_date = date("m-d-Y", strtotime($end_date[0]->meta_value));
       		$pricat = $wpdb->get_results("SELECT name, meta_value FROM $wpdb->apollo_eventmeta join wp_20_terms on term_id = meta_value WHERE meta_key = '_apl_event_term_primary_id' AND apollo_event_id = " .  $eventID);
			$pricat = $pricat[0]->name;


		/////////////////////////////////////
		// Get author name from users
		/////////////////////////////////////

			$author = $wpdb->get_results("select user_login from $wpdb->users where ID = ".$event->post_author);
			$author = $author[0]->user_login;


		/////////////////////////////////////
		// Get categories for this event
		/////////////////////////////////////

			$cats = $wpdb->get_results("select name from $wpdb->term_relationships join $wpdb->terms on term_id = term_taxonomy_id where object_id =".$eventID);

			$catlist = array();
			$catlist[] = $pricat;
			foreach ($cats as $cat){
				$catlist[] = $cat->name;
			}

			$cats = array_unique($catlist);

			$catlist = '';
			foreach ($cats as $cat){
				$catlist .= $cat.", ";
			}
			$catlist = substr($catlist, 0, -1);

		//////////////////////////////////////////
		// Check event has orgP and orgT
		// has at least one date time set
		//////////////////////////////////////////

			if ( ($orgP == '') or ($orgT == '') ) continue;
			if (count($hastime > 0)) {} else {continue;}

			$offer_count++;

			$img = wp_get_attachment_url( get_post_thumbnail_id( $event->ID ) );
			echo "<tr><td><a href='post.php?post=$post->ID&action=edit&orgP=".$orgP."&orgT=".$orgT."&pID=".$eventID."'><img border=0 src=$img style='width:40px;height:30px;vertical-align:middle;'></a></td><td><a href='post.php?post=$post->ID&action=edit&orgP=".$orgP."&orgT=".$orgT."&pID=".$eventID."'><strong>$event->post_title</strong></a></td>
				<td><a target=_blank href='post.php?post=".$orgP."&action=edit'>$orgname</a></td><td>".$start_date."</td><td>".$end_date."</td><td>".$catlist."</td><td>".$eventdate."</td><td>".$author."</td></tr>";


			// Get event times from calendar
			$querystr = "select event_id, date_event, time_from from $wpdb->apollo_event_calendar where event_id =".$event->ID;
			$evdts = $wpdb->get_results($querystr, OBJECT);

			foreach ($evdts as $evdt){
				if ($evdt->date_event < date('Y-m-d')) continue; 
				$human_date = date('m/d/Y', strtotime($evdt->date_event));
				$human_time = date('h:i:s A', strtotime($evdt->time_from));
				$matchdate = $evdt->event_id."_".$human_date." ".$human_time;
				//echo "<br><br>Event date time<b> ".$human_date." ".$human_time."</b> ";
				//echo "<br><span style='line-height:120%'>Event date time<b> ".$human_date." ".$human_time."</b> ";

			} // end foreach evdts

		} // end foreach events

		echo "<script>";
		echo "document.getElementById(\"offer_count\").innerHTML = \"$offer_count current offers\"";
		echo "</script>";

		echo "</table>";





?>