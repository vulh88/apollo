<?php
/*
Plugin Name: Ticketing
Plugin URI: NONE
Description: Declares a plugin that will create a custom post type ticketing
Version: 1.0
Author: BEST4WEB
Author URI: NONE
License: GPL
*/


function create_ticketing() {
    register_post_type( 'ticketing',
        array(
            'labels' => array(
                'name' => 'Ticketing',
                'singular_name' => 'Ticketing',
                'add_new' => 'Add New',
                'add_new_item' => 'Ticketing Information',
                'edit' => 'Edit',
                'edit_item' => 'Edit ticketing',
                'new_item' => 'New ticketing',
                'view' => 'View',
                'view_item' => 'View ticketing',
                'search_items' => 'Search ticketing',
                'not_found' => 'No ticketing found',
                'not_found_in_trash' => 'No ticketing found in Trash',
                'parent' => 'Parent ticketing'
            ),
 
            'public' => true,
            'menu_position' => 15,
            'supports' => ('title'),
            'taxonomies' => array( '' ),
            'menu_icon' => plugins_url( 'images/image.png', __FILE__ ),
            'has_archive' => false
        )
    );
}

function change_ticketing_title( $title ){
     $screen = get_current_screen();
 
     if  ( 'ticketing' == $screen->post_type ) {
          $title = 'Ticketing Name';
     }
 
     return $title;
}
 
add_filter( 'enter_title_here', 'change_ticketing_title' );


function my_edit_ticketing_columns( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'id' => __( 'ID' ),
		'title' => __( 'Ticketing Name' ),
	);

	return $columns;
}


function my_manage_ticketing_columns( $column, $post_id ) {
	global $post;

//print_r($post); die;

	switch( $column ) {

		case 'id' :
			echo $post_id;
			break;

		case 'status' :
			echo get_post_meta( $post_id, 'meta-status', true );
			break;

	}
}


//function b4w_add_pages() {
//	add_submenu_page('edit.php?post_type=ticketing', __('Current Offers','currentoffers'), __('Current Offers','curentoffers'), 'manage_options', 'edit.php?post_type=currentoffers', NULL);
//    }


include 'tick_box.php';

add_action( 'init', 'create_ticketing' );

add_filter( 'manage_edit-ticketing_columns', 'my_edit_ticketing_columns' ) ;

add_action( 'manage_ticketing_posts_custom_column', 'my_manage_ticketing_columns', 10, 2 );

//add_action('admin_menu', 'b4w_add_pages');




  
