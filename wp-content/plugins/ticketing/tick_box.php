<?php
/**
 * Adds a meta box to the post editing screen
 */
function tick_custom_meta() {
    add_meta_box( 'tick_meta', __( 'Ticketing Associaton', 'tick-textdomain' ), 'tick_meta_callback', 'ticketing' );
}
add_action( 'add_meta_boxes', 'tick_custom_meta' );


// Hide preview buttons etc

function tick_hide_publishing_actions(){
        $my_post_type = 'ticketing';
        global $post;
        if($post->post_type == $my_post_type){
            echo '
                <style type="text/css">
                    #misc-publishing-actions,
                    #minor-publishing-actions{
                        display:none;
                    }
                </style>
            ';
        }
}
add_action('admin_head-post.php', 'tick_hide_publishing_actions');
add_action('admin_head-post-new.php', 'tick_hide_publishing_actions');


/**
 * Outputs the content of the meta box
 */
function tick_meta_callback( $post ) {
global $wpdb, $pagenow;

 if ( (current_user_can( 'manage_network' )) OR (current_user_can( 'administrator' )) OR (current_user_can( 'manage_sites' )) ) {$allowuser=true;} else {echo "You do not have access to this screen."; die;}

////////////////////////////////
// List current offers then exit
////////////////////////////////

$wecamefromcurrentoffers = get_post_meta($post->ID, 'wecamefromcurrentoffers', 'Y');

if ( (isset($_GET['current']) && $_GET['current'] == 'Y') OR ($wecamefromcurrentoffers == 'Y') ){
	include('current_offers.php');
	exit;
}

///////////////////////////////////////////////////////////////////
// If link from current offers reset orgP and orgT and get event ID
///////////////////////////////////////////////////////////////////

if (!empty($_GET['orgP'])){
	update_post_meta( $post->ID, 'meta-orgP', $_GET['orgP'] );
}
if (!empty($_GET['orgT'])){
	update_post_meta( $post->ID, 'meta-orgT', $_GET['orgT'] );
}

if (!empty($_GET['pID'])){
	$edit_eventID = $_GET['pID'];

	// remember we just came from current offers
	update_post_meta($post->ID, 'wecamefromcurrentoffers', 'Y');

}


    wp_nonce_field( basename( __FILE__ ), 'tick_nonce' );

	// get file name from postmeta
       $meta_url = get_post_meta( $post->ID, 'meta-url');

	$filename = isset($meta_url[0]) ? $meta_url[0] : null;
	$xml = @file_get_contents($filename);
	$xml = simplexml_load_string($xml);

       $orgP = get_post_meta( $post->ID, 'meta-orgP');
       $orgT = get_post_meta( $post->ID, 'meta-orgT');
       $automatch = get_post_meta( $post->ID, 'automatch');

//echo "orgP = "; print_r($orgP);

	if ( !empty($orgP[0]) && !empty($orgT[0]) ) {
	    $block = 'disabled';
        $urlsaved = $orgP[0]."_".$orgT[0];
    } else {
	    $block = '';
        $urlsaved = "_";
    }

    $urlsaved = get_post_meta( $post->ID, $urlsaved);
	$urlsaved = unserialize(isset($urlsaved[0]) ? $urlsaved[0] : null);

//print_r($urlsaved);

//////////////////////////////////
// Create dropdown for ticket orgs 
//////////////////////////////////

	$ticketorgs = array();
	if (is_object($xml)) {
		foreach($xml->children() as $child){
			// Get rid of the useless stuff in the feed first
			if ($child->EventName == 'Gift Certificates') {continue;}
			$orgname = $child->Performances->Performance->PerformancePromoter;
			$orgname = str_replace("'","", $orgname);
			//array_push($ticketorgs, $child->Performances->Performance->PerformancePromoter);
			array_push($ticketorgs, $orgname);
		}
	} else {
		if ($pagenow != 'post-new.php'){
			echo "<span style='color:red; font-weight:800';>WARNING: Problem with XML feed - Feed URL missing or XML file has errors.</span><br><br>";}
		}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Save ticketorgs to keep historical list in case one or more ticketing orgs don't appear in the current list.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

	$orgTsaved = get_post_meta( $post->ID, 'orgTsaved');

	if (isset($orgTsaved[0]) && is_array($orgTsaved[0])):
		foreach ($orgTsaved[0] as $org){
			if ($org[0] != ''){
				$org[0] = str_replace("'","", $org[0]);
				array_push($ticketorgs, $org[0]);
			}
		}
	endif;

	$ticketorgs_tmp = array_unique($ticketorgs);

	$ticketorgs = array();
	if ($ticketorgs_tmp):
		foreach ($ticketorgs_tmp as $key => $value){
			$mykey=$value.$key;

if($_SERVER['REMOTE_ADDR'] == '73.252.6.1' ) {
			//echo "<br>key=$key value=$value mykey=$mykey";
}
			$ticketorgs[$mykey]=(array) $value;

		}
	endif;

	update_post_meta( $post->ID, 'orgTsaved', $ticketorgs);

	ksort($ticketorgs);

	$orglistT = "<option value=''>Select Org</option>";
	//$orglistT .= "<option value='test' selected>test remove</option>";
	if ($ticketorgs):
		foreach ($ticketorgs as $org){
			$selected = '';
			if ($orgT[0] == $org[0]) $selected = 'selected';
			$short_name = substr($org[0],0,43);
			$short_name = str_replace("'","", $short_name);
			$fixed_name = str_replace("'","", $org[0]);

			//$orglistT .= "<option value='".$org[0]."' $selected>".$short_name."</option>";
			$orglistT .= "<option value='".$fixed_name."' $selected>".$short_name."</option>";

		}
	endif;



//////////////////////////////////////
// Create dropdown for presenting orgs 
//////////////////////////////////////

	$querystr = "select post_title, ID from $wpdb->posts where post_type='organization' and post_status = 'publish';";
	$orgs1 = $wpdb->get_results($querystr, OBJECT);

	$presentorgs = array();
	if ($orgs1):
		foreach ($orgs1 as $org){
			$orgid_name = $org->post_title."_".$org->ID;
			array_push($presentorgs, $orgid_name);

		}
	endif;


	$presentorgs = array_unique($presentorgs);
	asort($presentorgs);

	$orglistP = "<option value=''>Select Org</option>";
	if ($presentorgs):
		foreach ($presentorgs as $org){
			$selected = '';
			list ($orgname, $orgid) = explode('_',$org);
			if (isset($orgP[0]) && $orgP[0] == $orgid) $selected = 'selected';
			$short_name = substr($orgname,0,43);
			$orglistP .= "<option value=$orgid $selected>$short_name</option>";
		}
	endif;


//////////////////////////////////////////////////////
// Display the presenting and ticketing dropdown boxes
//////////////////////////////////////////////////////

echo "<b>XML Feed URL:</b> <input size=80 type=text name=meta_url value='" . (isset($meta_url[0]) ? $meta_url[0] : '') . "'><br><br>";


$html = <<<EOHTML
<!--a class="button" href="post.php?post=$post->ID&action=edit&current=Y&pID=$post->ID">View Currrent Offers</a><br><br-->
<a class="button" href="post.php?post=$post->ID&action=edit&current=Y">View Currrent Offers</a><br><br>
Select the presenting organization and ticketing organization then click publish or update to display associated events. You can select "auto match" if you want the system to automatically attempt to match tickets to event date and times. When you complete the ticketing association for an organization you can select new organizations by clicking the "Select Org" checkbox then click publish or update. 
<br><b>NOTE:</b> The "Auto Match" feature is only available before ticketing associations have been made. Once ticketing associations are made this feature should not be used, to prevent auto match altering the existing associations.<hr>
<table width=900>
<tr><td><b>PRESENTING ORGANIZATION</b></td><td><b>TICKETING ORGANIZATION</b></td><td><b>Auto Match</b></td></tr>
<tr><td>
<select name="meta_orgP" $block>$orglistP</select>
</td><td>
<select name="meta_orgT" $block>$orglistT</select>
</td><td>
<select name="automatch"><option value='N'>NO</option><option value='Y'>YES</option></select>
</td><td>
<input type=checkbox name="resetorgs" value="Y"> Select Org
</td>
</tr>
</table>
EOHTML;

if ($block == 'disabled') { echo "<input type=hidden name=meta_orgP value=".$orgP[0]."><input type=hidden name=meta_orgT value='".$orgT[0]."'>";}

echo $html;

///////////////////////////
// do the individual events
///////////////////////////

	if (!empty($orgP[0])){

		$querystr = "select p.ID, p.post_title from ".$wpdb->prefix."posts AS p join ".$wpdb->prefix."apollo_event_org AS eo on eo.post_id = p.ID join ".$wpdb->prefix."apollo_eventmeta AS em on em.apollo_event_id = p.ID and em.meta_key = '_apollo_event_end_date' and em.meta_value >= curdate() where p.post_type = 'event' and p.post_status = 'publish' and eo.org_id =".$orgP[0]." group by p.ID";
		$events = $wpdb->get_results($querystr, OBJECT);

//if($_SERVER['REMOTE_ADDR'] == '73.252.6.1' ) {print_r($querystr); die; }

		if ($events):
            $kc = 0;
            $iEvent = 0;
			foreach ($events as $event){

			// If linked to by current offers we only edit one event matched by $edit_eventID
			if (isset($edit_eventID)) {
				if ($event->ID == $edit_eventID) {$display = 'inline';} else {$display='none';}
			}

			// Create dropdown for the event URLS

			$urls = array();
			foreach($xml->children() as $child){

				// Get rid of the useless stuff in the feed first
				if ($child->EventName == 'Gift Certificates') {continue;}

				$orgname = $child->Performances->Performance->PerformancePromoter;
				$orgname = str_replace("'","", $orgname);
				//if ($child->Performances->Performance->PerformancePromoter != $orgT[0]) continue;
				if ($orgname != $orgT[0]) continue;

				$key = $child->EventName;
				$value = $child->EventURL;
				$nameurl = $key."_".$value;

				array_push($urls, $nameurl);
			}

			$urls = array_unique($urls);
			asort($urls);

//print_r($urls); die;

			$urldrop = "<select name=evurl[]><option value=''>Select Event URL</option>";
			foreach ($urls as $url){
				list ($ename, $eurl) = explode('_',$url);
				$eventID = $event->ID;
				$evurl = $eventID."_".$eurl;
				if (@in_array($evurl, $urlsaved))  {$sel = 'selected';} else { $sel='';}
				$urldrop .= "<option value='$evurl' $sel>$ename</option>";
			}
			$delurl = $eventID."_"."";
			$urldrop .= "<option value='$delurl'>Delete Event URL</option></select>";

			// Create dropdown for date time

			$dt = array();
			foreach($xml->children() as $child){

				// Get rid of the useless stuff in the feed first
				if ($child->EventName == 'Gift Certificates') {continue;}

				$orgname = $child->Performances->Performance->PerformancePromoter;
				$orgname = str_replace("'","", $orgname);
				//if ($child->Performances->Performance->PerformancePromoter != $orgT[0]) continue;
				if ($orgname != $orgT[0]) continue;

				$ename = $child->EventName;
				$ename = str_replace('-', '&hyphen;', $ename);

				foreach ($child->Performances->Performance as $perform){
					$datetime = $perform->PerformanceDateTime;

					$purl = $perform->PerformanceURL;
					$nameurl = $ename."-".$datetime."-".$purl;
					array_push($dt, $nameurl);
				}
			}
            $labelBooth = !$kc ? "<span style='float: right;width: 94px;text-align: center;'>Booth Override Check Box</span>" : '';
            $kc++;
            
			$img = wp_get_attachment_url( get_post_thumbnail_id( $event->ID ) );
			echo "<div style='display:$display';>";
			echo "<hr><img src=$img style='width:40px;height:30px;vertical-align:middle;'><strong style='font-size:15px'>&nbsp;&nbsp;&nbsp;EVENT NAME: $event->post_title</strong>$labelBooth<br>";
			echo " <br>Event URL: $urldrop<br>";

			// Get event times from calendar
			$querystr = "select * from $wpdb->apollo_event_calendar where event_id =".$event->ID;
			$evdts = $wpdb->get_results($querystr, OBJECT);
            
			foreach ($evdts as $evdt){
				if ($evdt->date_event < date('Y-m-d')) continue; 
				$human_date = date('m/d/Y', strtotime($evdt->date_event));
				$human_time = date('h:i:s A', strtotime($evdt->time_from));
				$matchdate = $evdt->event_id."_".$human_date." ".$human_time;
                
                $priKey = $evdt->event_id."_".$evdt->date_event." ".$evdt->time_from." ".$evdt->time_to;
                
				//echo "<br><br>Event date time<b> ".$human_date." ".$human_time."</b> ";
				echo "<br><span style='line-height:120%'>Event date time<b> ".$human_date." ".$human_time."</b> ";
                
                $checkedBoothOverride = isset($evdt->booth_override) && $evdt->booth_override == 1 ? 'checked' : '';
                
/////////////////////////////////////////////
// Create individual drop down for each event
/////////////////////////////////////////////

				// Create dropdown for the event URLS
				$dtdrop = "<select style='width: 400px' name=indurl[]><option value=''>Select Date & Time</option>";
                 
				foreach ($dt as $url){
					$sel = '';
					list ($ename, $datetime, $purl) = explode('-',$url);
					$indurl = $eventID."_".$datetime."_".$purl;

					list($mydate, $mytime, $ampm)=explode(' ',$datetime);

					$mydate = date('m/d/Y',strtotime($mydate));
					$mytime = date('h:i:s',strtotime($mytime));

					$seldate = $eventID."_".$mydate." ".$mytime." ".$ampm;

					//echo "<br>match on event name"; similar_text($ename, $event->post_title, $percent); echo $percent."<br>";
					similar_text($ename, $event->post_title, $percent);
					
					if ($automatch[0] == 'Y'){
						if ( ($seldate == $matchdate) AND ($percent > 80) ) {$sel = 'selected'; }
					} else {
						if ( ($seldate == $matchdate) AND (in_array($indurl, $urlsaved)) ) {$sel = 'selected'; }
					}

 					$dtdrop .= "<option value='$indurl' $sel>$datetime - $ename</option>";
					
					// fix here for multiple times fopr same event
					if ($sel == 'selected') $timetodel = $datetime;

				}

				//$detldt = $eventID."_".$datetime."_"."";
				$detldt = $eventID."_".$timetodel."_"."";
				$dtdrop .= "<option value='$detldt'>Delete Date & Time</option></select>
                    <input type='hidden' name='booth_hidden[]' value='$priKey' />
                    <input style='float: right; margin-top: 7px;' ".$checkedBoothOverride." name='booth_override[$evdt->event_id.$iEvent]' value=1 type='checkbox' /> </span>";
				echo $dtdrop;
                $iEvent++;

			} // end foreach evdts
		 echo '</div>';
		} // end foreach events
		endif;
	
	} // end if orgP

	if (!isset($edit_eventID)) {
		//wp_redirect('post.php?post=24564&action=edit&current=Y&pID=24564');
	}

}

/**
 * Saves the custom meta input
 */
function tick_meta_save( $post_id ) {
global $wpdb;

////////////////////////////////////////////////////////////
// See if we are switching orgs - if so, don't save any data
////////////////////////////////////////////////////////////

//echo "resetorgs = ".$_POST['resetorgs']; die;

	if (isset($_POST['resetorgs']) && $_POST['resetorgs'] == 'Y'){
	       update_post_meta( $post_id, 'meta-orgP', '' );
       	update_post_meta( $post_id, 'meta-orgT', '' );
		update_post_meta($post_id, 'wecamefromcurrentoffers', 'N');
		return;
	}


    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'tick_nonce' ] ) && wp_verify_nonce( $_POST[ 'tick_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
 
    // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }


//////////////////////////////
// Save the discount URL field
//////////////////////////////
    
	$datalist = array();

	if ( isset($_POST['evurl'])  && $_POST['evurl']):
		foreach ($_POST['evurl'] as $evurl){

		array_push($datalist, $evurl);

		list($eventID, $discount_url) = explode('_', $evurl);

		if (!empty($eventID)) {
			$meta =  $wpdb->get_results("select meta_key, meta_value from $wpdb->apollo_eventmeta where meta_key = '_apollo_event_data' and apollo_event_id = $eventID");

			$vars = array();
			foreach ($meta as $met) {
				$vars[$met->meta_key] = $met->meta_value;
			}

			$evrow = unserialize(unserialize($vars['_apollo_event_data']));
			$evrow['_admission_discount_url'] = $discount_url;
			$evrow = serialize($evrow);
			$evrow = serialize($evrow);	

			$querystr = "update $wpdb->apollo_eventmeta set meta_value = '$evrow' where meta_key = '_apollo_event_data' and apollo_event_id = $eventID";
			$result = $wpdb->get_results($querystr, OBJECT);
		}
	}
	endif;

/////////////////////////////////////
// Save the individual date time URLs
/////////////////////////////////////
    $boothOverride = isset($_POST['booth_override']) ? $_POST['booth_override'] : '';
    
	if (isset($_POST['indurl']) && $_POST['indurl']):
		foreach ($_POST['indurl'] as $kIndurl => $indurl){

		array_push($datalist, $indurl);

		list($eventID, $datetime, $discount_url) = explode('_', $indurl);

		//if ($eventID == 82538) {echo "ev=$eventID, dt=$datetime, du=$discount_url"; die;}

		if (!empty($eventID)) {

			list($date, $time, $ampm) = explode(' ',$datetime);
			$date = date('Y-m-d', strtotime($date));
			$time = $time.$ampm;
			$time = date('H:i', strtotime($time));
           
			$querystr = "update $wpdb->apollo_event_calendar set ticket_url = '$discount_url' where date_event = '$date' and time_from = '$time' and event_id = $eventID";
			$result = $wpdb->get_results($querystr, OBJECT);
		//if  ($eventID == 82538)  {echo $querystr; die;}
		}
	}
	endif;
  
    if ( isset($_POST['booth_hidden']) && $_POST['booth_hidden'] ) {
       
        foreach ($_POST['booth_hidden'] as $kb => $booth) {
            list($eventID, $datetime, $discount_url) = explode('_', $booth);
            
            if (!empty($eventID)) {
                list($date, $time, $ampm) = explode(' ',$datetime);
                $date = date('Y-m-d', strtotime($date));
                
                $outputBoothOverride = isset($boothOverride[$eventID.'.'.$kb]) ? 1 : NULL;
                $querystr = "update $wpdb->apollo_event_calendar set booth_override = '$outputBoothOverride' where date_event = '$date' and time_from = '$time' and event_id = $eventID";
                $result = $wpdb->get_results($querystr, OBJECT);
            }
        }
    }
    
       $orgP = get_post_meta( $post_id, 'meta-orgP');
       $orgT = get_post_meta( $post_id, 'meta-orgT');
    if ( !empty($orgP[0]) and !empty($orgT[0]) ) {
		$urlsaved = $_POST['meta_orgP']."_". $_POST['meta_orgT'];
		$datalist = serialize($datalist);
       	update_post_meta( $post_id, $urlsaved, $datalist );
	}   
   	if(isset($_POST['meta_orgP']))
       update_post_meta( $post_id, 'meta-orgP', $_POST['meta_orgP'] );
	if(isset($_POST['meta_orgT']))
       update_post_meta( $post_id, 'meta-orgT', $_POST['meta_orgT'] );
	if(isset($_POST['meta_url']))
       update_post_meta( $post_id, 'meta-url', $_POST['meta_url'] );
	if(isset($_POST['automatch']))
       update_post_meta( $post_id, 'automatch', $_POST['automatch'] );


}

add_action( 'save_post', 'tick_meta_save' );

