<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class Apollo_Mini_Site_App
{

    private static $copied_term_from_parent_id = 'copied_term_from_parent_id';

    /**
     * @param $post_id
     * @param array $meta_values
     */
    public static function copyMetaData($post_id, $meta_values = array()){

        if ($post_id && $meta_values) {
            foreach ($meta_values as $value) {
                $v = $value->meta_value;
                if (is_serialized_string($v)) {
                    $v = maybe_serialize(maybe_unserialize(maybe_unserialize($v)));
                }

                update_apollo_meta($post_id, $value->meta_key, $v);
            }
        }
    }

    /**
     * @param $parent_post_id
     * @param $mini_site_id
     * @param $mini_post_id
     * @param $tag_taxonomy
     */
    public static function copyTagsToMiniSite($parent_post_id, $mini_site_id, $mini_post_id, $tag_taxonomy){

        //taxonomy = 'event-tag'
        $parent_tags = wp_get_post_terms($parent_post_id, $tag_taxonomy);
        $parent_tags_desc = array();
        if ($parent_tags){
            foreach ($parent_tags as $parent_tag){
                $parent_tags_desc[$parent_tag->term_id] = tag_description($parent_tag->term_id);
            }
        }

        if (switch_to_blog($mini_site_id)) {
            $tag_ids = array();

            //unset post terms
            $current_tags = wp_get_post_terms($mini_post_id, $tag_taxonomy);
            if ($current_tags){
                $remove_ids = array();
                foreach ($current_tags as $current_tag){
                    $remove_ids[] = $current_tag->term_id;
                }
                if ($remove_ids){
                    wp_remove_object_terms($mini_post_id,$remove_ids, $tag_taxonomy);
                }
            }

            foreach ($parent_tags as $parent_tag) {

                $tag = get_term_by('name', $parent_tag->name, $tag_taxonomy);
                $tag_id = $tag ? $tag->term_id : 0;

                if (!$tag_id){
                    $args = array(
                        'description' => $parent_tag->description
                    );

                    $result = wp_insert_term($parent_tag->name, $tag_taxonomy, $args);
                    if (isset($result['term_id'])){
                        $tag_id = $result['term_id'];
                    }
                }

                //update description
                if ($tag_id) {
                    $tag_ids[] = $tag_id;
                    if (isset($parent_tags_desc[$parent_tag->term_id])){
                        wp_update_term($tag_id, $tag_taxonomy, array('description' => $parent_tags_desc[$tag_id]));
                    }
                }

            }

            wp_set_post_terms($mini_post_id, $tag_ids, $tag_taxonomy, true);

            restore_current_blog();
        }

    }

    /**
     * get post_id by post_name
     * @param $postType
     * @param $postName
     * @return int
     */
    public static function checkPostByName($postType, $postName) {

        global $wpdb;

        $sql = "SELECT ID FROM $wpdb->posts WHERE post_type = '%s' AND post_title = '%s' AND post_status IN ('publish', 'pending') LIMIT 1";
        $result = $wpdb->get_row($wpdb->prepare($sql, $postType, $postName));

        if ($result) {
            return $result->ID;
        }

        return 0;
    }

    /**
     * @param $mini_site_id
     * @param $parent_org_id
     * @return int
     */
    public static function getOrgByParentOrgID($mini_site_id, $parent_org_id) {
        global $wpdb;

        //return xref data
        $tbl = miniSiteOrgTable();
        $data =  $wpdb->get_results($wpdb->prepare("SELECT mini_org_id FROM $tbl where org_id = '%s' AND mini_site_id = '%s' AND mini_org_id != 0", $parent_org_id, $mini_site_id));

        $orgID = 0;
        if(!empty($data)){
            $orgID = $data[0]->mini_org_id;
        }

        if (!$orgID) {
            $default_table = miniSiteXrefEventTable();
            $data = $wpdb->get_results($wpdb->prepare("SELECT mini_orgID FROM $default_table WHERE orgID = %s AND mini_site_id = %s", $parent_org_id, $mini_site_id));
            if(!empty($data)){
                $orgID = $data[0]->mini_orgID;
            }
        }

        if(!$orgID){
            $table = miniSiteXrefTable();
            $data = $wpdb->get_results("SELECT mini_post_id FROM ". $table ." WHERE parent_post_id =" . $parent_org_id . " AND mini_site_id = " . $mini_site_id, ARRAY_A);
            if(!empty($data)){
                $orgID =  $data[0]['mini_post_id'];
            }
        }

        // Ignore a draft or trash org
        switch_to_blog($mini_site_id);
        $org = get_post($orgID);
        restore_current_blog();

        if ($org){
            if (($org->post_status != 'publish' && $org->post_status != 'pending') || $org->post_type != 'organization') {
                return 0;
            }
        }

        return $orgID;
    }


    /**
     * @param $insertID
     * @param $attachment_ids
     * @param $mini_site_id
     * @param $meta_key
     */

    public static function updateGalleryImagesToMiniSite($insertID, $attachment_ids, $mini_site_id, $meta_key){

        $gallery_items = array();
        $image_urls = array();
        if(!empty($attachment_ids)){
            foreach ($attachment_ids as $imgd){
                $guidx = wp_get_attachment_url($imgd);
                array_push($image_urls,$guidx);
            }
        }
        foreach ($image_urls as $url){
            if ($url != ''){
                array_push($gallery_items, self::updatePostAttachment($mini_site_id, $url));
            } // end if image name not blank
        }
        // Add the image post IDs to the eventmeta table
        if(!empty($gallery_items)){
            $gall = implode(",",$gallery_items);
        }else{
            $gall = '';
        }
        if(switch_to_blog($mini_site_id)){
            update_apollo_meta($insertID, $meta_key, $gall);
            restore_current_blog();
            return;
        }

    }

    /**
     * Get attachment id by attachment name
     * @param $mini_site_id
     * @param $attachment_name
     * @return int
     */
    public static function getAttachmentIDByName($mini_site_id, $attachment_name){
        if(switch_to_blog($mini_site_id)){
            $args = array(
                'post_type' => 'attachment',
                'name' => sanitize_title($attachment_name),
                'posts_per_page' => 1,
                'post_status' => 'inherit'
            );
            $queryPosts = get_posts( $args );
            $att = $queryPosts ? array_pop($queryPosts) : null;
            restore_current_blog();
            return $att ? $att->ID : 0;
        }
        return 0;
    }


    /**
     * @param $orgID
     * @return array|null|object
     */
    public static function getOrgMeta($orgID){
        global $wpdb;

        //return meta data
        $data =  $wpdb->get_results("select * from " . $wpdb->apollo_organizationmeta . " where apollo_organization_id = " . $orgID);
        return $data;
    }

    /**
     * Update term relationship
     * @param $parent_post_id
     * @param $taxonomy
     * @param $mini_site_id
     * @param $mini_post_id
     * @param $meta_mini
     */
    public static function updatePostTerm($parent_post_id, $taxonomy, $mini_site_id, $mini_post_id, $meta_mini){
        global  $wpdb;
        $post_terms =  $wpdb->get_results("SELECT * FROM $wpdb->term_relationships WHERE object_id = ". $parent_post_id);
        $cate_option = get_post_meta($meta_mini, 'meta-classified-catsel');
        $check_term = false;
        $cat_ids = array();
        if($cate_option && sizeof($cate_option) > 0 && $cate_option[0]  == 'on'){
            $cate_save = get_post_meta($meta_mini, 'classified-meta-type');
            if($cate_save && sizeof($cate_save) > 0){
                foreach ($cate_save as $item_save){
                    $cat_ids[] = $item_save[0];
                }
            }
            $check_term = true;
        }

        if($post_terms){
            $term_ids = array();
            foreach ($post_terms as $item){
                if($check_term && !in_array($item->term_taxonomy_id, $cat_ids)){
                    continue;
                }
//                $term = get_term($item->term_taxonomy_id, $taxonomy);
                $term = get_term_by('term_taxonomy_id',$item->term_taxonomy_id, $taxonomy);
                if($term){
                    $mini_term_id = self::insertCategory($mini_site_id, $term);
                    if($mini_term_id){
                        $term_ids[] = intval($mini_term_id);
                    }
                }
            }
            //update term relationship
            if(switch_to_blog($mini_site_id)){
                wp_set_object_terms($mini_post_id, $term_ids, $taxonomy);
                restore_current_blog();
            }
        }
    }

    /**
     * Insert category if not exists.
     * @param $mini_site_id
     * @param $term
     * @return int|object
     */
    private static function insertCategory($mini_site_id, $term){
        //check term exists.
        global $wpdb;
        if(switch_to_blog($mini_site_id)){
            $mini_terms = $wpdb->get_results("SELECT term_id FROM ". $wpdb->prefix . "termmeta WHERE meta_key = '" . self::$copied_term_from_parent_id . "' AND meta_value = " . $term->term_id);
            if(!empty($mini_terms) && sizeof($mini_terms)){
                $term_id = $mini_terms[0]->term_id;
            }else{
                //Insert term
                $newCat = array(
                    'ID' => 0,
                    'taxonomy' => $term->taxonomy,
                    'cat_name' => $term->name,
                    'category_description' => $term->description,
                    'category_nicename' => $term->nicename
                );
                $term_id = wp_insert_category($newCat);
                //update term meta
                add_term_meta($term_id, self::$copied_term_from_parent_id, $term->term_id);
            }
            restore_current_blog();
            return $term_id;
        }
        return 0;
    }


    /**
     * @param $parent_post_id
     * @param $mini_site_id
     * @param $mini_post_id
     * @param $mini_site_config_post_id
     * @param string $taxonomy
     * @param string $meta_cat_option
     * @param string $meta_type_option
     * @param int $cat_primary
     */
    public static function copyCategoryToMiniSite($parent_post_id, $mini_site_id, $mini_post_id, $mini_site_config_post_id, $taxonomy='event-type', $meta_cat_option = 'meta-catsel', $meta_type_option = 'meta-type', $cat_primary = 0) {

        global $wpdb;
        $parent_terms = wp_get_post_terms($parent_post_id, $taxonomy);

        // get setting
        if($mini_site_config_post_id && !empty($meta_cat_option) && !empty($meta_type_option)){
            $is_all_cat = get_post_meta($mini_site_config_post_id, $meta_cat_option);

            if ($is_all_cat && $is_all_cat[0] == 'on') {
                $selected_cats = get_post_meta($mini_site_config_post_id, $meta_type_option);
                $selected_cats = $selected_cats && $selected_cats[0] ? $selected_cats[0] : array();
            }
        }

        // Parent Term metas
        $parent_term_metas = array();
        if ($parent_terms) {
            foreach ($parent_terms as $parent_term) {
                // Get all term meta
                $table = $wpdb->prefix. Apollo_Tables::_APOLLO_TERM_META;
                $result = $wpdb->get_results($wpdb->prepare("select * from $table where apollo_term_id = %s", $parent_term->term_id));
                $parent_term_metas[$parent_term->term_id] = $result;
            }
        }

        if(switch_to_blog($mini_site_id)){
            $term_ids = array();

            //remove relationship
            $current_tags = wp_get_post_terms($mini_post_id, $taxonomy);
            if ($current_tags){
                $remove_ids = array();
                foreach ($current_tags as $current_tag){
                    $remove_ids[] = $current_tag->term_id;
                }
                if ($remove_ids){
                    wp_remove_object_terms($mini_post_id,$remove_ids, $taxonomy);
                }
            }

            if($parent_terms){
                foreach ($parent_terms as $parent_term) {

                    if (isset($selected_cats) && $selected_cats && !in_array($parent_term->term_id, $selected_cats)) {
                        continue;
                    }

                    // Get child term by name
                    $term = get_term_by('name', $parent_term->name, $taxonomy);

                    $term_id = $term ? $term->term_id : 0;

                    if (!$term_id) {
                        // Add new category
                        $newTerm = array(
                            'ID'    => '',
                            'cat_name'  => $parent_term->name,
                            'category_description'   => $parent_term->description,
                            'taxonomy'  => $taxonomy,
                        );

                        $term_id = wp_insert_category($newTerm);
                    }

                    // Update term meta
                    if (isset($parent_term_metas[$parent_term->term_id]) && $parent_term_metas[$parent_term->term_id]) {
                        foreach($parent_term_metas[$parent_term->term_id] as $mt) {
                            update_apollo_term_meta($term_id, $mt->meta_key, $mt->meta_value);
                        }
                    }

                    $term_ids[] = $term_id;
                    if($cat_primary && $cat_primary == $parent_term->term_id){
                        update_apollo_meta( $mini_post_id, Apollo_DB_Schema::_APL_EVENT_TERM_PRIMARY_ID, $term_id );
                    }
                }
                if($term_ids){
                    wp_set_post_terms($mini_post_id, $term_ids, $taxonomy);
                }

            }
            restore_current_blog();
        }

    }

    /**
     * @param $mini_site_id
     * @param $address
     */
    public static function updateStateZipToMiniSite($mini_site_id, $address) {

        if (Apollo_App::get_network_manage_states_cities($mini_site_id)){

            global $wpdb;
            $parent_table_name = $wpdb->prefix . Apollo_Tables::_APL_STATE_ZIP;

            $parent_state = self::getStateCityData($parent_table_name, $address[Apollo_DB_Schema::_CLASSIFIED_STATE], 'state');

            if(switch_to_blog($mini_site_id) && $parent_state){
                $table_name = $wpdb->prefix . Apollo_Tables::_APL_STATE_ZIP;

                //check state
                $state_insert_id = 0;
                $mini_state = self::getStateCityData($table_name, $address[Apollo_DB_Schema::_CLASSIFIED_STATE], 'state');

                if($mini_state === null){
                    //insert new state to mini site.
                    $new_state = $wpdb->insert($table_name, array(
                        'code' => $address[Apollo_DB_Schema::_CLASSIFIED_STATE],
                        'name' => $parent_state->name,
                        'type' => 'state',
                        'parent' => 0
                    ));
                    if($new_state){
                        $state_insert_id = $wpdb->insert_id;
                    }
                }
                else {
                    $state_insert_id = $mini_state->id;
                }

                //Check city
                $city_insert_id = 0;
                $mini_city = self::getStateCityData($table_name, $address[Apollo_DB_Schema::_CLASSIFIED_CITY], 'city', $state_insert_id);
                if($mini_city === null){
                    if($state_insert_id != 0){
                        $new_city = $wpdb->insert($table_name, array(
                            'code' => $address[Apollo_DB_Schema::_CLASSIFIED_CITY],
                            'name' => $address[Apollo_DB_Schema::_CLASSIFIED_CITY],
                            'type' => 'city',
                            'parent' => $state_insert_id
                        ));
                        if($new_city){
                            $city_insert_id = $wpdb->insert_id;
                        }
                    }
                }
                else {
                    $city_insert_id = $mini_city->id;
                }

                //insert zip
                $parent_zip = self::getParentZipData($parent_table_name, $parent_state->id,$address[Apollo_DB_Schema::_CLASSIFIED_CITY]);
                if($parent_zip && sizeof($parent_zip) > 0 && $city_insert_id){
                    foreach ($parent_zip as $zip_item){
                        $mini_zip = self::getStateCityData($table_name, $zip_item->code, 'zip', $city_insert_id);
                        if(!$mini_zip){
                            $wpdb->insert($table_name, array(
                                'code' => $zip_item->code,
                                'name' => $zip_item->name,
                                'type' => 'zip',
                                'parent' => $city_insert_id
                            ));
                        }
                    }
                }
                restore_current_blog();
            }
        }
    }

    /**
     * @param $table_name
     * @param $code
     * @param $type
     * @param int $parent_id
     * @return null
     */
    private static function getStateCityData($table_name, $code, $type, $parent_id = 0){
        global  $wpdb;
        if ($parent_id) {
            $sql = "SELECT * FROM " . $table_name . " WHERE code='$code' AND type='$type' AND parent= $parent_id";
        }
        else {
            $sql = "SELECT * FROM " . $table_name . " WHERE code='$code' AND type='$type'";
        }

        $result = $wpdb->get_results($sql);

        if ($result && sizeof($result) > 0) {
            return $result[0];
        }
        return null;
    }

    /**
     * @param $table_name
     * @param $parentStateID
     * @param $city_code
     * @return array|null|object
     */
    private static function getParentZipData($table_name, $parentStateID, $city_code){
        global $wpdb;
        $parent_city = $wpdb->get_results("SELECT id FROM ". $table_name . " WHERE code = '" . $city_code . "' AND type = 'city' AND parent = $parentStateID");
        if($parent_city && sizeof($parent_city) > 0){
            $result = $wpdb->get_results("SELECT * FROM ". $table_name . " WHERE parent = " . intval($parent_city[0]->id) . " AND type = 'zip'");
            return $result;
        }
        return null;
    }

    /**
     * @param $mini_site_id
     * @param $parent_post_id
     * @param $mini_post_id
     */
    public static function updateAttachment($mini_site_id, $parent_post_id, $mini_post_id){
        $parent_url = wp_get_attachment_url(get_post_thumbnail_id($parent_post_id));
        self::updatePostAttachment($mini_site_id, $parent_url, $mini_post_id);
    }

    /**
     * @param $mini_site_id
     * @param $image_url
     * @param int $mini_post_id
     * @return int
     */
    private static function updatePostAttachment($mini_site_id, $image_url, $mini_post_id = 0){
        if($image_url){
            $filename_parts = explode('/', $image_url);
            $filename = end($filename_parts);
            $attachment_name = self::getAttachmentNiceName($image_url);
            if(empty($attachment_name)) return 0;
            $attach_id = self::getAttachmentIDByName($mini_site_id, $attachment_name);
            if(switch_to_blog($mini_site_id)){
                if($attach_id === 0){
                    require_once(ABSPATH . 'wp-admin/includes/media.php');
                    require_once(ABSPATH . 'wp-admin/includes/file.php');
                    require_once(ABSPATH . 'wp-admin/includes/image.php');
                    $result = media_sideload_image($image_url, $mini_post_id, null, 'src');
                    while (preg_match('/--/',$filename)){
                        $filename = preg_replace('/--/','-',$filename);
                    }

                    if($result && is_string($result)){
                        $attach_id = self::get_attachment_id_by_src($result);
                    }
                }
                if($mini_post_id != 0){
                    update_post_meta($mini_post_id, '_thumbnail_id', $attach_id);
                }
                restore_current_blog();
                return $attach_id;
            }

        }
        return 0;
    }

    /**
     * Get attachment ID by image URL
     * @param $image_src
     * @return null|string
     */
    function get_attachment_id_by_src ($image_src) {

        global $wpdb;
        $table = $wpdb->prefix. 'posts';
        $query = "SELECT ID FROM $table WHERE guid='$image_src'";
        $id = $wpdb->get_var($query);
        return $id;

    }

    /**
     * @param $image_url
     * @return mixed|string
     */
    private static function getAttachmentNiceName($image_url){
        if(!empty($image_url)){
            $filename_parts = explode('/', $image_url);
            $filename = end($filename_parts);
            $attachment_name = explode('.', $filename);
            $nice_name = $filename;
            if(sizeof($attachment_name) > 0){
                $nice_name = '';
                for ($i = 0; $i < sizeof($attachment_name) - 1; $i++){
                    $nice_name.= $attachment_name[$i];
                }
            }
            return $nice_name;
        }
        return '';
    }

    /**
     * @ticket #17506: [CF] 20180910 - [Mini Sites] Not seeing the copied events in the Pending list
     * @param $postID
     * @param $postType
     */
    public static function insertIclTranslations($postID, $postType){

        if ( is_plugin_active('sitepress-multilingual-cms/sitepress.php') ) {
            include_once WP_PLUGIN_DIR. '/sitepress-multilingual-cms/sitepress.php';
            if(function_exists('icl_object_id')){
                global $sitepress, $wpdb;
                $table = new Apl_Query( "{$wpdb->prefix}icl_translations");
                if( intval($postID) && !$table->get_row("element_id =$postID")) {
                    $def_trid = $sitepress->get_element_trid($postID);
                    $sitepress->set_element_language_details($postID, "post_$postType", $def_trid, $sitepress->get_default_language());
                }
            }
        }
    }
}