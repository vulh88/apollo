<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Apollo_Event_Copy_To_Mini_Site
{

    /**
     * @param $parent_post_id
     * @param $mini_site_id
     * @param $mini_post_id
     */
    public static function copyEventCalendars($parent_post_id, $mini_site_id, $mini_post_id) {
        global $wpdb;

        $calendars = self::getCalendarData($parent_post_id);

        if (switch_to_blog($mini_site_id)) {

            $table = $wpdb->prefix . "apollo_event_calendar";
            $where = array('event_id' => $mini_post_id);
            $wpdb->delete($table, $where);

            // copy calendar data
            $table = $wpdb->prefix . "apollo_event_calendar";
            foreach ($calendars as $cal) {
                $data = array('event_id' => $mini_post_id, 'date_event' => $cal->date_event, 'time_from' => $cal->time_from, 'time_to' => $cal->time_to);
                self::copyCalendar($data, $table);
            }

            restore_current_blog();

        }
    }

    /**
     * @param $event_id
     * @return array|null|object
     */
    private static function getCalendarData($event_id) {
        global $wpdb;

        //return copy calendar data
        $sql = $wpdb->prepare("SELECT * FROM $wpdb->apollo_event_calendar WHERE event_id = '%s'", $event_id);
        $data = $wpdb->get_results($sql);
        return $data;
    }

    /**
     * @param $data
     * @param $table
     */
    private static function copyCalendar($data, $table)
    {
        global $wpdb;

        //copy metadata
        $okay = $wpdb->insert($table, $data);
        return;
    }

}