<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Apollo_Org_Copy_To_Mini_Site
{
    protected $mini_site_id;

    protected $parent_post_id;

    protected $mini_sites;

    public function __construct( $parent_post_id){
        $this->parent_post_id = $parent_post_id;
    }

    /**
     * @return mixed
     */
    public function getSiteId()
    {
        return $this->mini_site_id;
    }

    /**
     * @param mixed $mini_site_id
     */
    public function setSiteId($mini_site_id)
    {
        $this->mini_site_id = $mini_site_id;
    }

    /**
     * @return mixed
     */
    public function getMiniSites()
    {
        return $this->mini_sites;
    }

    /**
     * @param mixed $mini_sites
     */
    public function setMiniSites($mini_sites)
    {
        $this->mini_sites = $mini_sites;
    }

    public function copyToMiniSite()
    {
        if(get_post_status( $this->parent_post_id) === 'publish' ){
            global $wpdb;

            if (isset($_POST['mini_sites']) && $_POST['mini_sites']) {

                $site_ids = implode(',', $_POST['mini_sites']);
                $sql = $wpdb->prepare("SELECT * FROM $wpdb->postmeta WHERE meta_key = 'meta-mini' AND meta_value IN ($site_ids) AND post_id IN (SELECT ID FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'minisites')", array());
                $meta =  $wpdb->get_results($sql);
                if (count($meta) <= 0) return;

                $this->setMiniSites($_POST['mini_sites']);
                $globalSites = array();
                foreach ($meta as $mini_site) {
                    if (in_array($mini_site->meta_value, $_POST['mini_sites'])){
                        $submit_sites[] = $mini_site->meta_value;
                        $this->setSiteId($mini_site->meta_value);
                        $this->copyData($mini_site->post_id);
                    }
                    $globalSites[$mini_site->meta_value] = $mini_site->post_id;
                }

                $GLOBALS['apollo_mini_submit_sites'] = $globalSites;
            }
        }
    }

    /**
     * Copy post to mini site.
     * @param $mini_post_id_meta post ID of the mini post site
     */
    public function copyData($mini_post_id_meta){
        global $wpdb;

        $post = get_post($this->parent_post_id, ARRAY_A);
        $mini_post_id = Apollo_Mini_Site_App::getOrgByParentOrgID($this->mini_site_id, $this->parent_post_id);

        $copy_post = $post;

        // Set post status
        $bypass = get_post_meta(intval($mini_post_id_meta), 'org-meta-bypass');
        $copy_post['post_status'] = (sizeof($bypass) > 0 && $bypass[0] == 'on') ? 'publish' : 'pending';


        if(!$mini_post_id) {
            $copy_post['ID'] = '';
            //insert new post
            if(switch_to_blog(intval($this->mini_site_id))) {
                $insertID = wp_insert_post($copy_post);
                restore_current_blog();

                // update mini sites
                $wpdb->insert(miniSiteXrefTable(), array(
                    'parent_post_id' => $this->parent_post_id,
                    'mini_site_id' => $this->getSiteId(),
                    'mini_post_id' => $insertID,
                    'mini_options_id' => $mini_post_id_meta
                ));
            }
        }
        else{
            //update post
            if(switch_to_blog(intval($this->mini_site_id))) {
                $copy_post['ID'] = $mini_post_id;
                //option bypass

                wp_update_post($copy_post);
                restore_current_blog();
            }
        }
    }


    /**
     * @param $parent_post_id
     * @param $sites
     * @return array|null|object
     */
    public function getMiniPostByParentPost($parent_post_id, $siteId){
        global $wpdb;

        $tbl = miniSiteOrgTable();
        $data =  $wpdb->get_results("SELECT mini_org_id as mini_post_id, mini_site_id FROM $tbl where org_id =" . $parent_post_id . " AND mini_site_id =". $siteId);

        if (empty($data)) {
            $default_table = miniSiteXrefEventTable();
            $data = $wpdb->get_results("SELECT mini_orgID as mini_post_id, mini_site_id FROM $default_table WHERE orgID =" .$parent_post_id . " AND mini_site_id =". $siteId);
        }

        if(empty($data)){
            $table = miniSiteXrefTable();
            $data = $wpdb->get_results("SELECT mini_post_id, mini_site_id FROM ". $table ." WHERE parent_post_id =" . $parent_post_id . " AND mini_site_id =" . $siteId);
        }

        return $data;
    }

    /**
     * @param $postID
     * @return array|null|object
     */
    public function getMetaData($postID){
        global $wpdb;

        //return meta data
        $data =  $wpdb->get_results("select * from " . $wpdb->apollo_organizationmeta . " where apollo_organization_id =". $postID);
        return $data;
    }

    /**
     * @param $mini_site_id
     * @param $parent_business_id
     * @param $orgId
     */
    public static function copyBusinessToMiniSite($mini_site_id, $parent_business_id, $orgId){
        require_once (ABSPATH . 'wp-content/plugins/copy-to-mini-site/class-apollo-business-copy-to-mini-site.php');

        $businessMiniSite = new Apollo_Business_Copy_To_Mini_Site($parent_business_id);
        $businessMiniSite->copyToMiniSite();

        $site_item = $businessMiniSite->getMiniPostByParentPost($parent_business_id, $mini_site_id);
        $site_item = count($site_item) ? $site_item[0] : [];

        if($site_item->mini_site_id && $site_item->mini_post_id && $site_item->mini_options_id){

            $metaValuesBusiness = $businessMiniSite->getMetaData($parent_business_id);

            if(switch_to_blog($site_item->mini_site_id)){
                Apollo_Mini_Site_App::insertIclTranslations($site_item->mini_post_id, 'business');
                Apollo_Mini_Site_App::copyMetaData($site_item->mini_post_id, $metaValuesBusiness);

                update_apollo_meta($orgId, Apollo_DB_Schema::_APL_ORG_BUSINESS, $site_item->mini_post_id);
                update_apollo_meta($site_item->mini_post_id, Apollo_DB_Schema::_APL_BUSINESS_ORG, $orgId);
                restore_current_blog();
            }
            Apollo_Mini_Site_App::copyCategoryToMiniSite($parent_business_id, $site_item->mini_site_id, $site_item->mini_post_id, $site_item->mini_options_id , 'business-type', 'business-meta-catsel', 'business-meta-type');
        }
    }
}