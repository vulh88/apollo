<?php

/**
 * The default table for event module
 */
if (!function_exists('miniSiteXrefEventTable')) {
    function miniSiteXrefEventTable()
    {
        global $wpdb;
        return $wpdb->prefix. MINI_SITE_XREF;
    }
}

/**
 * Using on migrate event to mini sites
 */
if (!function_exists('miniSiteOrgTable')) {
    function miniSiteOrgTable()
    {
        global $wpdb;
        return $wpdb->prefix. APOLLO_MINI_SITE_ORG;
    }
}

/**
 * This table is using for all modules
 * the event module was built by client dev team, so can not use this one to avoid conflicting data
*/
if (!function_exists('miniSiteXrefTable')) {
    function miniSiteXrefTable()
    {
        global $wpdb;
        return $wpdb->prefix. APOLLO_MINI_SITE_XREF;
    }
}
