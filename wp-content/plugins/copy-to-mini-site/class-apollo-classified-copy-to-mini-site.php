<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Apollo_Classified_Copy_To_Mini_Site
{
    protected $mini_site_id;

    protected $parent_post_id;

    protected $mini_sites;

    const XREF_TABLE = 'classified_mini_site_xref';

    public function __construct( $parent_post_id){
        $this->parent_post_id = $parent_post_id;
    }

    /**
     * @return mixed
     */
    public function getSiteId()
    {
        return $this->mini_site_id;
    }

    /**
     * @param mixed $mini_site_id
     */
    public function setSiteId($mini_site_id)
    {
        $this->mini_site_id = $mini_site_id;
    }

    /**
     * @return mixed
     */
    public function getMiniSites()
    {
        return $this->mini_sites;
    }

    /**
     * @param mixed $mini_sites
     */
    public function setMiniSites($mini_sites)
    {
        $this->mini_sites = $mini_sites;
    }

    /**
     * Copy post to mini site.
     * @param $mini_post_id_meta post ID of the mini post site
     */
    public function copyToMiniSite($mini_post_id_meta){
        global $wpdb;

        $post = get_post($this->parent_post_id, ARRAY_A);
        $mini_post_id = $this->checkParentPostID();

        $copy_post = $post;
        // Set post status
        $bypass = get_post_meta(intval($mini_post_id_meta), 'classified-meta-bypass');
        $copy_post['post_status'] = (sizeof($bypass) > 0 && $bypass[0] == 'on') ? 'publish' : 'pending';


        if(!$mini_post_id) {
            $copy_post['ID'] = '';
            //insert new post
            if(switch_to_blog(intval($this->mini_site_id))) {
                $insertID = wp_insert_post($copy_post);
                restore_current_blog();

                // update mini sites
                $wpdb->insert(miniSiteXrefTable(), array(
                    'parent_post_id' => $this->parent_post_id,
                    'mini_site_id' => $this->getSiteId(),
                    'mini_post_id' => $insertID,
                    'mini_options_id' => $mini_post_id_meta
                ));
            }
        }
        else{
            //update post
            if(switch_to_blog(intval($this->mini_site_id))) {
                $copy_post['ID'] = $mini_post_id;
                //option bypass

                wp_update_post($copy_post);
                restore_current_blog();
            }
        }
    }

    /**
     * Get post mini id
     * @return int
     */
    public function checkParentPostID(){
        global $wpdb;
        $table = miniSiteXrefTable();
        $result = $wpdb->get_results("SELECT mini_post_id FROM ". $table ." WHERE parent_post_id =" . $this->parent_post_id . " AND mini_site_id = " . $this->mini_site_id, ARRAY_A);
        if(empty($result) || $result === null){
            return 0;
        }else{
            return $result[0]['mini_post_id'];
        }
    }

    /**
     * Get meta data parent post.
     * @return array|null|object
     */
    public function getMetaData(){
        global $wpdb;

        //return meta data
        $data =  $wpdb->get_results("select * from " . $wpdb->prefix . "apollo_classifiedmeta where apollo_classified_id =". $this->parent_post_id);
        return $data;
    }

    /**
     * @param $mini_site_id
     * @param $parent_org_id
     * @return int|WP_Error
     */
    public static function copyOrgToMiniSite($mini_site_id, $parent_org_id)
    {
        global $wpdb;
        $org_id = Apollo_Mini_Site_App::getOrgByParentOrgID($mini_site_id, $parent_org_id);
        $parent_gallery_ids = get_apollo_meta($parent_org_id , Apollo_DB_Schema::_APL_ORG_IMAGE_GALLERY);
        $parent_gallery_ids = $parent_gallery_ids ? explode(',', $parent_gallery_ids[0]) : '';
        if (!$org_id) {
            $meta_values_org = Apollo_Mini_Site_App::getOrgMeta($parent_org_id);
            $org_post = get_post($parent_org_id, ARRAY_A);

            if (switch_to_blog($mini_site_id)) {

                $org_post['ID'] = '';
                $org_post['post_status'] = 'publish';
                $org_id = wp_insert_post($org_post);

                // copy org meta
                if ($meta_values_org) {
                    Apollo_Mini_Site_App::copyMetaData($org_id, $meta_values_org);
                }

                restore_current_blog();
            }

            //update featured image
            Apollo_Mini_Site_App::updateAttachment($mini_site_id, $parent_org_id, $org_id);

            //update gallery
            if($parent_gallery_ids){
                Apollo_Mini_Site_App::updateGalleryImagesToMiniSite($org_id, $parent_gallery_ids, $mini_site_id, Apollo_DB_Schema::_APL_ORG_IMAGE_GALLERY);
            }

            //copy category
            $cat_type = Apollo_DB_Schema::_ORGANIZATION_PT . '-type';
            Apollo_Mini_Site_App::copyCategoryToMiniSite($parent_org_id, $mini_site_id, $org_id, 0, $cat_type, '', '');

            /** Insert mini-site org info */
            $mini_site_org_table = miniSiteOrgTable();
            $wpdb->insert($mini_site_org_table, array(
                'org_id' => $parent_org_id,
                'mini_site_id' => $mini_site_id,
                'mini_org_id' => $org_id
            ));

        }

        return $org_id;
    }


    /**
     * @param $parent_post_id
     * @param $mini_site_id
     * @return array|null|object
     */
    public static function checkCopiedMiniSiteByParentPost($parent_post_id, $mini_site_id){
        global $wpdb;
        $table_name = miniSiteXrefTable();
        $result = $wpdb->get_results($wpdb->prepare("SELECT mini_site_id FROM $table_name WHERE parent_post_id = %s AND mini_site_id = %s", $parent_post_id, $mini_site_id));
        return $result;
    }

    /**
     * @param $parent_post_id
     * @param $sites
     * @return array|null|object
     */
    public static function getAllMiniPostByParentPost($parent_post_id, $sites){
        global $wpdb;
        $sitesStr = implode(',', $sites);
        $table = miniSiteXrefTable();
        $sql = "SELECT * FROM $table WHERE parent_post_id = " . $parent_post_id . " AND mini_site_id  IN ($sitesStr)";
        $mini_posts = $wpdb->get_results($sql);
        return $mini_posts;
    }
}