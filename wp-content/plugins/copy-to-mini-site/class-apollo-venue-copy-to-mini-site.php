<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Apollo_Venue_Copy_To_Mini_Site
{
    protected $mini_site_id;

    protected $parent_post_id;

    protected $mini_sites;

    public function __construct( $parent_post_id){
        $this->parent_post_id = $parent_post_id;
    }

    /**
     * @return mixed
     */
    public function getSiteId()
    {
        return $this->mini_site_id;
    }

    /**
     * @param mixed $mini_site_id
     */
    public function setSiteId($mini_site_id)
    {
        $this->mini_site_id = $mini_site_id;
    }

    /**
     * @return mixed
     */
    public function getMiniSites()
    {
        return $this->mini_sites;
    }

    /**
     * @param mixed $mini_sites
     */
    public function setMiniSites($mini_sites)
    {
        $this->mini_sites = $mini_sites;
    }


    public function copyToMiniSite()
    {
        if(get_post_status( $this->parent_post_id) === 'publish' ){
            global $wpdb;

            if (isset($_POST['mini_sites']) && $_POST['mini_sites']) {

                $site_ids = implode(',', $_POST['mini_sites']);
                $sql = $wpdb->prepare("SELECT * FROM $wpdb->postmeta WHERE meta_key = 'meta-mini' AND meta_value IN ($site_ids) AND post_id IN (SELECT ID FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'minisites')", array());
                $meta =  $wpdb->get_results($sql);
                if (count($meta) <= 0) return;

                $this->setMiniSites($_POST['mini_sites']);

                $globalSites = array();

                foreach ($meta as $mini_site) {
                    if (in_array($mini_site->meta_value, $_POST['mini_sites'])){
                        $submit_sites[] = $mini_site->meta_value;
                        $this->setSiteId($mini_site->meta_value);
                        $this->copyData($mini_site->post_id);
                    }
                    $globalSites[$mini_site->meta_value] = $mini_site->post_id;
                }

                $GLOBALS['apollo_mini_submit_sites'] = $globalSites;
            }
        }
    }

    /**
     * Copy post to mini site.
     * @param $mini_post_id_meta post ID of the mini post site
     */
    public function copyData($mini_post_id_meta){
        global $wpdb;

        $post = get_post($this->parent_post_id, ARRAY_A);
        $mini_post_id = self::checkParentPostID($this->parent_post_id, $this->mini_site_id);

        $copy_post = $post;
        // Set post status
        $bypass = get_post_meta(intval($mini_post_id_meta), 'venue-meta-bypass');
        $copy_post['post_status'] = (sizeof($bypass) > 0 && $bypass[0] == 'on') ? 'publish' : 'pending';


        if(!$mini_post_id) {
            $copy_post['ID'] = '';
            //insert new post
            if(switch_to_blog(intval($this->mini_site_id))) {
                $insertID = wp_insert_post($copy_post);
                restore_current_blog();

                // update mini sites
                $wpdb->insert(miniSiteXrefTable(), array(
                    'parent_post_id' => $this->parent_post_id,
                    'mini_site_id' => $this->getSiteId(),
                    'mini_post_id' => $insertID,
                    'mini_options_id' => $mini_post_id_meta
                ));
            }
        }
        else{
            //update post
            if(switch_to_blog(intval($this->mini_site_id))) {
                $copy_post['ID'] = $mini_post_id;
                //option bypass

                wp_update_post($copy_post);
                restore_current_blog();
            }
        }
    }

    /**
     * Get post mini id
     * @return int
     */
    public static function checkParentPostID($parent_post_id, $mini_site_id){
        global $wpdb;

        $default_table = miniSiteXrefEventTable();
        $data = $wpdb->get_results("SELECT mini_venueID FROM $default_table WHERE venueID =" . $parent_post_id . " AND mini_site_id =" . $mini_site_id);
        if(!empty($data)){
            $venueId = $data[0]->mini_venueID;
            return $venueId;
        }

        $table = miniSiteXrefTable();
        $data = $wpdb->get_results("SELECT mini_post_id FROM ". $table ." WHERE parent_post_id =" . $parent_post_id . " AND mini_site_id = " . $mini_site_id, ARRAY_A);
        if(empty($data)){
            return 0;
        }else{
            $venueId =  $data[0]['mini_post_id'];
        }

        return $venueId;
    }

    /**
     * @param $parent_post_id
     * @param $sites
     * @return array|null|object
     */
    public static function getMiniPostByParentPost($parent_post_id, $siteID){
        global $wpdb;
        $table = miniSiteXrefTable();
        $sql = "SELECT * FROM $table WHERE parent_post_id = " . $parent_post_id . " AND mini_site_id=" .$siteID;
        $mini_posts = $wpdb->get_results($sql);

        if (!empty($mini_posts)){
            return $mini_posts;
        }
        else{
            $table = miniSiteXrefEventTable();
            $sql = "SELECT mini_site_id, mini_venueID as mini_post_id  FROM $table WHERE venueID = " . $parent_post_id . " AND mini_site_id=" . $siteID;
            $mini_posts = $wpdb->get_results($sql);
            return $mini_posts;
        }
    }
}