<?php
/*
Plugin Name: copy_to_mini_site
Plugin URI: NONE
Description: Declares a plugin that will copy event data from one site to another (also orgs and venues if needed).
Version: 1.0
Author: BEST4WEB
Author URI: NONE
License: GPL
*/


if (!defined('MINI_SITE_XREF')) {
    define('MINI_SITE_XREF', 'mini_site_xref');
}

if (!defined('APOLLO_MINI_SITE_ORG')) {
    define('APOLLO_MINI_SITE_ORG', 'apollo_mini_site_org');
}

if (!defined('APOLLO_MINI_SITE_XREF')) {
    define('APOLLO_MINI_SITE_XREF', 'apollo_mini_site_xref');
}


require_once (ABSPATH . 'wp-content/plugins/copy-to-mini-site/inc/helper.php');
require_once (ABSPATH . 'wp-content/plugins/copy-to-mini-site/class-apollo-mini-site-app-function.php');
require_once (ABSPATH . 'wp-content/plugins/copy-to-mini-site/class-apollo-classified-copy-to-mini-site.php');
require_once (ABSPATH . 'wp-content/plugins/copy-to-mini-site/class-apollo-event-copy-to-mini-site.php');
/*@ticket #16299*/
require_once (ABSPATH . 'wp-content/plugins/copy-to-mini-site/class-apollo-venue-copy-to-mini-site.php');
require_once (ABSPATH . 'wp-content/plugins/copy-to-mini-site/class-apollo-business-copy-to-mini-site.php');
require_once (ABSPATH . 'wp-content/plugins/copy-to-mini-site/class-apollo-org-copy-to-mini-site.php');


function minisite_activate()
{
    global $wpdb;

    $old_blog = $wpdb->blogid;

    // Get all blog ids
    $blogids = $wpdb->get_col("SELECT blog_id FROM $wpdb->blogs");

    foreach ($blogids as $blog_id) {
        switch_to_blog($blog_id);

        $table_name = $wpdb->prefix . 'mini_site_xref';

        $sqlMini = "CREATE TABLE $table_name (
 		`parent_post_id` int(11) NOT NULL,
 		`mini_site_id` int(11) NOT NULL,
  		`mini_post_id` int(11) NOT NULL,
  		`venueID` int(11) NULL,
  		`orgID` int(11) NULL,
  		`mini_venueID` int(11) NULL,
  		`mini_orgID` int(11) NULL,
  		PRIMARY KEY (`parent_post_id`, `mini_site_id`)
		);";

        /** @Ticket #12623 */
        $tbl = $wpdb->prefix . APOLLO_MINI_SITE_XREF;
        $classified_sql = "CREATE TABLE $tbl (
 		`parent_post_id` int(11) NOT NULL,
  		`mini_post_id` int(11) NOT NULL,
  		`mini_site_id` int(11) NOT NULL,
  		`mini_options_id` int(11) DEFAULT 0,
  		PRIMARY KEY (`parent_post_id`, `mini_post_id`, `mini_site_id`)
		);";


        $tbl = $wpdb->prefix. APOLLO_MINI_SITE_ORG;
        $miniSiteOrgSQL = "CREATE TABLE $tbl (
 		`org_id` int(11) NOT NULL,
  	    `mini_site_id` int(11) NOT NULL,
  	    `mini_org_id` int(11) NOT NULL,
  		PRIMARY KEY (`org_id`, `mini_site_id`)
		);";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        dbDelta($sqlMini);
        dbDelta($classified_sql);
        dbDelta($miniSiteOrgSQL);

    }
    switch_to_blog($old_blog);
}

register_activation_hook(__FILE__, 'minisite_activate');

add_action('publish_event', 'copy_event_to_mini_site', 10, 1);
add_action('publish_classified', 'checkClassifiedStatus', 10, 1);
add_action('publish_venue', 'copy_venue_to_mini_site', 10, 1);
add_action('publish_organization', 'copy_org_to_mini_site', 10, 1);
add_action('publish_business', 'copy_business_to_mini_site', 10, 1);

/**
 * logic copy to mini site
 * @param $post_id
 */
function checkClassifiedStatus($post_id){

    if(get_post_status($post_id) === 'publish' ){
        global $wpdb;

        if (isset($_POST['mini_sites']) && $_POST['mini_sites']) {

            $site_ids = implode(',', $_POST['mini_sites']);
            $sql = $wpdb->prepare("SELECT * FROM $wpdb->postmeta WHERE meta_key = 'meta-mini' AND meta_value IN ($site_ids) AND post_id IN (SELECT ID FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'minisites')", array());
            $meta =  $wpdb->get_results($sql);
            if (count($meta) <= 0) return;

            $classified_mini = new Apollo_Classified_Copy_To_Mini_Site($post_id);
            $classified_mini->setMiniSites($_POST['mini_sites']);
            foreach ($meta as $mini_site) {
                if (in_array($mini_site->meta_value, $_POST['mini_sites'])){
                    $submit_sites[] = $mini_site->meta_value;
                    $classified_mini->setSiteId($mini_site->meta_value);
                    $classified_mini->copyToMiniSite($mini_site->post_id);
                }
            }

            // Store submit mini-sites for updating meta data
            $GLOBALS['mini_submit_sites'] = $_POST['mini_sites'];
        }

    }
}

function copy_venue_to_mini_site($postId){

    $venue = new Apollo_Venue_Copy_To_Mini_Site($postId);
    $venue->copyToMiniSite();
}

function copy_org_to_mini_site($postId){

    $org = new Apollo_Org_Copy_To_Mini_Site($postId);
    $org->copyToMiniSite();
}

function copy_business_to_mini_site($postId){

    $business = new Apollo_Business_Copy_To_Mini_Site($postId);
    $business->copyToMiniSite();
}

function minibox_custom_meta()
{
    global $wpdb;

    // Only add custom meta box if there have been mini sites defined for this site

    $meta = $wpdb->get_results("select * from $wpdb->postmeta where meta_key ='meta-mini' and post_id in (select ID from $wpdb->posts where post_status = 'publish' and post_type = 'minisites')");
    if (count($meta) <= 0) return;
    /** @Ticket #15874 */
    foreach ($meta as $met) {
        $miniMetaData = get_post_meta($met->post_id);
        if (!empty($miniMetaData['meta-event-status'][0]) && $miniMetaData['meta-event-status'][0] == 'on') {
            add_meta_box('minibox_meta', __('Copy to mini site', 'prfx-textdomain'), 'minibox_meta_callback', 'event', 'side', 'high');
        }
        if (!empty($miniMetaData['meta-classified-status'][0]) && $miniMetaData['meta-classified-status'][0] == 'on') {
            add_meta_box( 'minibox_meta', __( 'Copy to mini site', 'prfx-textdomain' ), 'minibox_meta_callback', Apollo_DB_Schema::_CLASSIFIED, 'side', 'high' );
        }

        /** @ticket #16299**/
        if (!empty($miniMetaData['org-meta-status'][0]) && $miniMetaData['org-meta-status'][0] == 'on') {
            add_meta_box( 'minibox_meta', __( 'Copy to mini site', 'prfx-textdomain' ), 'minibox_meta_callback', Apollo_DB_Schema::_ORGANIZATION_PT, 'side', 'high' );
        }
        if (!empty($miniMetaData['business-meta-status'][0]) && $miniMetaData['business-meta-status'][0] == 'on') {
            add_meta_box( 'minibox_meta', __( 'Copy to mini site', 'prfx-textdomain' ), 'minibox_meta_callback', Apollo_DB_Schema::_BUSINESS_PT, 'side', 'high' );
        }
        if (!empty($miniMetaData['venue-meta-status'][0]) && $miniMetaData['venue-meta-status'][0] == 'on') {
            add_meta_box( 'minibox_meta', __( 'Copy to mini site', 'prfx-textdomain' ), 'minibox_meta_callback', Apollo_DB_Schema::_VENUE_PT, 'side', 'high' );
        }
    }

}

add_action('add_meta_boxes', 'minibox_custom_meta');


function minibox_meta_callback($post)
{
    global $wpdb;


    $meta = $wpdb->get_results("select * from $wpdb->postmeta where meta_key ='meta-mini' and post_id in (select ID from $wpdb->posts where post_status = 'publish' and post_type = 'minisites')");
    if (count($meta) <= 0) return;

    // loop through the mini site relationships
    $retval = '';
    foreach ($meta as $met) {

        $post_cats = $wpdb->get_results("SELECT * FROM $wpdb->term_relationships WHERE object_id = " . $post->ID);
        $checked = '';

        $xref_data = get_xref_data($post->ID, $met->meta_value);
        $xref_data = $xref_data ? $xref_data[0] : '';

        // Have we seen this event before?
        if ($xref_data && $post->ID == $xref_data->parent_post_id) {
            $checked = 'checked';
        }

        /**@Ticket #12623 show checkbox copy to mini site */
        if($post->post_type == Apollo_DB_Schema::_CLASSIFIED){
            $classified_xref_data = Apollo_Classified_Copy_To_Mini_Site::checkCopiedMiniSiteByParentPost( $post->ID, $met->meta_value );
            if($classified_xref_data && !empty($classified_xref_data)){
                $checked = 'checked';
            }
        }

        /**@ticket #16299
         * begin
         */
        $isExist = false;
        if($post->post_type == Apollo_DB_Schema::_VENUE_PT){
            $isExist = Apollo_Venue_Copy_To_Mini_Site::checkParentPostID( $post->ID, $met->meta_value );
        }

        if($post->post_type == Apollo_DB_Schema::_ORGANIZATION_PT){
            $isExist =  Apollo_Mini_Site_App::getOrgByParentOrgID( $met->meta_value, $post->ID );
        }

        if($post->post_type == Apollo_DB_Schema::_BUSINESS_PT){
            $isExist = Apollo_Business_Copy_To_Mini_Site::checkParentPostID( $post->ID, $met->meta_value );
        }

        if($isExist){
            $checked = 'checked';
        }

        /*end*/

        $selected = get_post_meta($met->post_id, 'meta-catsel');
        $selected = isset($selected[0]) ? $selected[0] : '';
        $copyall = get_post_meta($met->post_id, 'meta-copyall');
        $types = get_post_meta($met->post_id, 'meta-type');
        $types = $types ? $types[0] : '';

        if ((!empty($types)) && ($selected == 'on')) {
            foreach ($post_cats as $cats) {
                if (in_array($cats->term_taxonomy_id, $types)) {
                    if ($copyall[0] == 'on') {
                        $checked = 'checked';
                    }
                }
            }
        }
        $enableModule = array();
        $metaCopyAll = array();
        $metaCatSel = array();
        $metaTypes = array();
        if ($post->post_type == Apollo_DB_Schema::_EVENT_PT) {
            $enableModule = get_post_meta($met->post_id, 'meta-event-status');
        }

        $target_mini_site = $met->meta_value;
        if (isset($copyall[0]) &&  $copyall[0] == 'on') {
            $checked = 'checked';
        }

        if($post->post_type == Apollo_DB_Schema::_CLASSIFIED){
            $metaCopyAll = get_post_meta($met->post_id, 'copy-all-classified');
            $metaCatSel = get_post_meta($met->post_id, 'classified-meta-catsel');
            $enableModule = get_post_meta($met->post_id, 'meta-classified-status');
            $metaTypes = get_post_meta($met->post_id, 'classified-meta-type');
        }

        if($post->post_type == Apollo_DB_Schema::_VENUE_PT){
            $metaCopyAll = get_post_meta($met->post_id, 'venue-copy-all');
            $metaCatSel = get_post_meta($met->post_id, 'venue-meta-catsel');
            $enableModule = get_post_meta($met->post_id, 'venue-meta-status');
            $metaTypes = get_post_meta($met->post_id, 'venue-meta-type');
        }

        if($post->post_type == Apollo_DB_Schema::_ORGANIZATION_PT){
            $metaCopyAll = get_post_meta($met->post_id, 'org-copy-all');
            $metaCatSel = get_post_meta($met->post_id, 'org-meta-catsel');
            $enableModule = get_post_meta($met->post_id, 'org-meta-status');
            $metaTypes = get_post_meta($met->post_id, 'org-meta-type');
        }

        if($post->post_type == Apollo_DB_Schema::_BUSINESS_PT){
            $metaCopyAll = get_post_meta($met->post_id, 'business-copy-all');
            $metaCatSel = get_post_meta($met->post_id, 'business-meta-catsel');
            $enableModule = get_post_meta($met->post_id, 'business-meta-status');
            $metaTypes = get_post_meta($met->post_id, 'business-meta-type');
        }

        /** check with selected category */
        if ($post->post_type != Apollo_DB_Schema::_EVENT_PT && isset($metaCopyAll[0]) && $metaCopyAll[0] == 'on') {
            $checked = '';
            if (isset($metaCatSel[0]) && $metaCatSel[0]  == 'on') {
                $metaCats = $wpdb->get_results("SELECT * FROM $wpdb->term_relationships WHERE object_id = " . $post->ID);
                if ((!empty($metaCats))) {
                    $metaTypes = $metaTypes ? $metaTypes[0] : '';
                    foreach ($post_cats as $cats) {
                        if (in_array($cats->term_taxonomy_id, $metaTypes)) {
                            $checked = 'checked';
                        }
                    }
                }
            } else {
                $checked = 'checked';
            }
        }
        if (!empty($enableModule[0]) && $enableModule[0] == 'on') {
            $domain = $wpdb->get_results("select domain from $wpdb->blogs where blog_id = $target_mini_site");
            $retval .= '<input type=checkbox name=mini_sites[] value="' . $target_mini_site . '" ' . $checked . ' >' . $domain[0]->domain . '<br>';
        }
    }
    echo $retval;

}


function copy_event_to_mini_site($ID)
{

    global $wpdb;
    $globalSites = array();

    // In here we have to check that a copy to mini-site relationship exists for a target if not just return
    $meta = $wpdb->get_results("select * from $wpdb->postmeta where meta_key ='meta-mini' and post_id in (select ID from $wpdb->posts where post_status = 'publish' and post_type = 'minisites')");
    if (count($meta) <= 0) return;

    $selected_for_copy = $_POST['mini_sites'];

    // loop through the mini site relationships
    foreach ($meta as $met) {

        if (!in_array($met->meta_value, $selected_for_copy)) continue;

        $xref_data = get_xref_data($ID, $met->meta_value);
        $xref_data = $xref_data[0];

        // Check mini post has existed or not
        $miniPostStatus = false;
        if ($xref_data) {
            switch_to_blog($xref_data->mini_site_id);
            $miniPostStatus = get_post_status($xref_data->mini_post_id);
            restore_current_blog();
        }

        $target_blog_id = $met->meta_value;

        $copy_event = false;
        $update_event = false;

        // Have we seen this event before and it has already existed?
        if ($ID == $xref_data->parent_post_id && ($miniPostStatus == 'publish' || $miniPostStatus == 'pending')) {
            $update_event = true;
        } else {
            $copy_event = true;
        }


        // Get the event post and the post meta for copying
        $post = get_post($ID, ARRAY_A);

        if ($copy_event) {
            $post['ID'] = '';
            $post['post_status'] = 'pending';
        }

        if ($update_event) {
            $post['ID'] = $xref_data->mini_post_id;
        }

        $notify = get_post_meta($met->post_id, 'meta-notify');
        $bypass = get_post_meta($met->post_id, 'meta-bypass');

        // get term relationship before switch to new blog id
        $post_terms = $wpdb->get_results("SELECT * FROM $wpdb->term_relationships WHERE object_id = " . $ID);

        // new logic for copy on selected categories
        $types = get_post_meta($met->post_id, 'meta-type');
        $types = $types[0];
        if (!empty($types)) {
            foreach ($post_terms as $cats) {
                if (in_array($cats->term_taxonomy_id, $types)) {
                    if (($copy_event) OR ($update_event)) {
                        $already_set = true;
                    } else {
                        $copy_event = true;
                    }
                }
            }
        }



//////////////////
//
// Get name of cat and get capradio equivalent
//
//////////////////


        // OKAY lets switch to the new blog and start copying
        switch_to_blog($target_blog_id);


        if ($update_event) {
            $mini_site_event_post = get_post($xref_data->mini_post_id, ARRAY_A);
            $post['post_status'] = $mini_site_event_post['post_status'];
        }


        ///////////////////////////////////////////////
        // Lets start making copies or updates
        ///////////////////////////////////////////////

        if ($copy_event) {

            if ($bypass[0] == 'on') {
                $post['post_status'] = 'publish';
            }
            $insertID = wp_insert_post($post);

            $globalSites[$target_blog_id] = [
                'mini_site_config_post_id' => $met->post_id,
                'mini_post_id' =>  $insertID
            ];

            if ($notify != '') {
                $domain = get_site_url();
                $permevent = get_permalink($insertID);
                $subject = "New event copied to $domain";
                $body = "A new event \"" . $post['post_title'] . "\" has been copied to $domain. View it here " . $permevent;
                wp_mail($notify, $subject, $body);
            }

            // Include image.php
            require_once(ABSPATH . 'wp-admin/includes/image.php');

        }


//////////////////////////
//
// Update event
//
//////////////////////////


        if ($update_event) {

            wp_update_post($post);

            /** eventmeta save by Apollo Theme */
            $globalSites[$target_blog_id] = [
                'mini_site_config_post_id' => $met->post_id,
                'mini_post_id' =>  $xref_data->mini_post_id
            ];

        }

        $eventId = $copy_event ? $insertID : $xref_data->mini_post_id;

        // Lets go back to original blog ID
        restore_current_blog();

        $data = array('parent_post_id' => $ID, 'mini_post_id' => $eventId, 'mini_site_id'   => $target_blog_id);
        update_xref_data($data);

    } // end foreach minisite loop

    $GLOBALS['apollo_mini_submit_sites'] = $globalSites;

}

if (!function_exists('insert_mini_site_org')) {
    function insert_mini_site_org($data) {
        global $wpdb;
        $xrefCopresenterOrgData = miniSiteOrgTable();

        $checkExist = $wpdb->prepare("SELECT count(*) FROM $xrefCopresenterOrgData WHERE mini_site_id = %d AND org_id = %d ", intval($data['mini_site_id']), intval($data['org_id']));

        if ($wpdb->get_var($checkExist)) {
            $query = $wpdb->prepare("
                UPDATE $xrefCopresenterOrgData SET mini_org_id = %d WHERE mini_site_id = %d AND org_id = %d
            ", intval($data['mini_org_id']), intval($data['mini_site_id']), intval($data['org_id']));
        }
        else {
            $query = $wpdb->prepare("INSERT INTO $xrefCopresenterOrgData(mini_site_id, org_id, mini_org_id) VALUES(%s, %s, %s)", $data['mini_site_id'], $data['org_id'], $data['mini_org_id']);
        }

        return $wpdb->get_results($query);
    }
}

function get_meta_data_org($ID)
{
    global $wpdb;

    //return meta data
    $data = $wpdb->get_results("select * from $wpdb->apollo_organizationmeta where apollo_organization_id = $ID");
    return $data;
}

function get_meta_data_venue($ID)
{
    global $wpdb;

    //return meta data
    $data = $wpdb->get_results("select * from $wpdb->apollo_venuemeta where apollo_venue_id = $ID");
    return $data;
}


function copy_meta_data($table, $data)
{
    global $wpdb;

    //copy metadata
    $okay = $wpdb->insert($table, $data);
    //if (!$okay) {echo $table; print_r($data); die;}
    return;
}


function copy_meta_data_with_where($table, $data, $where)
{
    global $wpdb;

    //copy metadata
    $okay = $wpdb->update($table, $data, $where);
    //if (!$okay) {echo $table; print_r($data); die;}
    return;
}

function update_xref_data($data)
{
    global $wpdb;

    $table = $wpdb->prefix . "mini_site_xref";

    $existedData = get_xref_data($data['parent_post_id'], $data['mini_site_id']);

    if ($existedData && count($existedData)) {
        $parentPostID = $data['parent_post_id'];
        unset($data['parent_post_id']);
        return $wpdb->update($table, $data, [
            'parent_post_id'    => $parentPostID,
            'mini_site_id' => $data['mini_site_id']
        ]);
    }

    //return xref data
    $okay = $wpdb->insert($table, $data);
    return $okay;
}


function get_xref_data($ID, $miniSiteId = null)
{
    global $wpdb;

    //return xref data
    $sql = "select * from " . $wpdb->prefix . "mini_site_xref where parent_post_id = $ID";

    if($miniSiteId){
        $sql .= " AND mini_site_id = $miniSiteId";
    }

    $data = $wpdb->get_results($sql);

    return $data;
}

function get_venue_xref_data($ID)
{
    global $wpdb;

    //return xref data
    $data = $wpdb->get_results("select * from " . $wpdb->prefix . "mini_site_xref where venueID = $ID and mini_venueID != 0");
    return $data;
}

?>