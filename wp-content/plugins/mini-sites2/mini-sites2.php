<?php
/*
Plugin Name: Minisites
Plugin URI: NONE
Description: Declares a plugin that will create a parent to mini-site relationship
Version: 1.0
Author: BEST4WEB
Author URI: NONE
License: GPL
*/


function create_minisites() {
    register_post_type( 'minisites',
        array(
            'labels' => array(
                'name' => 'Mini Sites',
                'singular_name' => 'Mini Site',
                'add_new' => 'Add New',
                'add_new_item' => 'Mini Site Information',
                'edit' => 'Edit',
                'edit_item' => 'Edit Mini Sites',
                'new_item' => 'New Mini Site',
                'view' => 'View',
                'view_item' => 'View Mini Site',
                'search_items' => 'Search Mini Sites',
                'not_found' => 'No Mini Site found',
                'not_found_in_trash' => 'No Mini Sites found in Trash',
                'parent' => 'Parent Mini Site'
            ),
 
            'public' => true,
            'menu_position' => 15,
            'supports' => ('title'),
            'taxonomies' => array( '' ),
            'menu_icon' => plugins_url( 'images/image.png', __FILE__ ),
            'has_archive' => false
        )
    );
}

function change_default_minisites_title( $title ){
     $screen = get_current_screen();
 
     if  ( 'minisites' == $screen->post_type ) {
          $title = 'Relationship Name';
     }
 
     return $title;
}
 
add_filter( 'enter_title_here', 'change_default_minisites_title' );


function my_edit_minisites_columns( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'id' => __( 'ID' ),
		'title' => __( 'Site Relationship Name' )
	);

	return $columns;
}


function my_manage_minisites_columns( $column, $post_id ) {
	global $post;

	switch( $column ) {

		case 'id' :
			echo $post_id;
			break;

	}
}



include 'meta_box.php';

add_action( 'init', 'create_minisites' );

add_filter( 'manage_edit-minisites_columns', 'my_edit_minisites_columns' ) ;

add_action( 'manage_minisites_posts_custom_column', 'my_manage_minisites_columns', 10, 2 );


?>