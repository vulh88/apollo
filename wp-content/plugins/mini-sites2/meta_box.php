<?php
/**
 * Adds a meta box to the post editing screen
 */
function mini_custom_meta() {
    add_meta_box( 'mini_meta', __( 'Relationship Details', 'mini-textdomain' ), 'mini_meta_callback', 'minisites' );
}
add_action( 'add_meta_boxes', 'mini_custom_meta' );

function miniSitesEnqueueScript(){
    wp_register_style('mini-sites-style', plugins_url('/assets/css/style.css', __FILE__));
    wp_enqueue_style('mini-sites-style');
}
add_action( 'admin_print_styles', 'miniSitesEnqueueScript');

// Hide preview buttons etc

function hide_mini_publishing_actions(){
        $my_post_type = 'minisites';
        global $post;
        if($post->post_type == $my_post_type){
            echo '
                <style type="text/css">
                    #misc-publishing-actions,
                    #minor-publishing-actions{
                        display:none;
                    }
                </style>
            ';
        }
}
add_action('admin_head-post.php', 'hide_mini_publishing_actions');
add_action('admin_head-post-new.php', 'hide_mini_publishing_actions');


/**
 * Outputs the content of the meta box
 */
function mini_meta_callback( $post )
{
    global $wpdb;

    wp_nonce_field(basename(__FILE__), 'mini_nonce');
    $mini_stored_meta = get_post_meta($post->ID);

    $url = get_site_url();
    $domain = substr($url, 7);
    $blog_id = get_current_blog_id();

    ?>


    Parent Site Domain: <strong><?php echo $domain; ?></strong><input type=hidden name="meta-parent"
                                                                      value="<?php echo $blog_id; ?>"><br>

    <?php
    $dropdown = '<select name="meta-mini"><option value="">Select Mini-site</option>';
    $bloglist = wp_get_sites();
    foreach ($bloglist as $blog) {
        if ($blog['blog_id'] != $blog_id) {

            if (isset($mini_stored_meta['meta-mini'][0]) && ($mini_stored_meta['meta-mini'][0] == $blog['blog_id'])) {
                $dropdown .= '<option value="' . $blog['blog_id'] . '" selected>' . $blog['domain'] . '</option>';
            } else {
                $dropdown .= '<option value="' . $blog['blog_id'] . '" >' . $blog['domain'] . '</option>';
            }
        }
    }
    $dropdown .= "</select>";
    $eventDisabled = (empty($mini_stored_meta['meta-event-status'][0]) || $mini_stored_meta['meta-event-status'][0] == 'off');
    $classifiedDisabled = (empty($mini_stored_meta['meta-classified-status'][0]) || $mini_stored_meta['meta-classified-status'][0] == 'off');
    ?>


    Mini-site Domain: <?php echo $dropdown; ?><br>
    Notification Email: <input size=30 type=text name="meta-notify"
                               value="<?php echo isset($mini_stored_meta['meta-notify'][0]) ? $mini_stored_meta['meta-notify'][0] : ''?>"> If you want to be notified of every event copied to the mini-site.
    <br><br>


    <!--h3>Mini-site copy options</h3-->
    <div class="mini-site-options">
        <div class="mini-tab-blk">
            <a href="#" module-option="event-tab"
               class="mini-site-tab active"><?php _e('Event options', 'apollo'); ?></a>
            <a href="#" module-option="classified-tab"
               class="mini-site-tab"><?php _e('Classified options', 'apollo'); ?></a>
            <a href="#" module-option="org-tab"
               class="mini-site-tab"><?php _e('Organization options', 'apollo'); ?></a>
            <a href="#" module-option="business-tab"
               class="mini-site-tab"><?php _e('Business options', 'apollo'); ?></a>
            <a href="#" module-option="venue-tab"
               class="mini-site-tab"><?php _e('Venue options', 'apollo'); ?></a>
        </div>
       <?php

            include_once __DIR__ . '/partials/event-tab.php';
            include_once __DIR__ . '/partials/classified-tab.php';
            include_once __DIR__ . '/partials/org-tab.php';
            include_once __DIR__ . '/partials/business-tab.php';
            include_once __DIR__ . '/partials/venue-tab.php';
       ?>
    </div>

    <script>

        jQuery("#selcaton").click(function () {
            bFlag = jQuery('#selcaton').is(':checked');
            if (bFlag) {
                jQuery('#catsel').show();
            }
        });
        jQuery("#selcatoff").click(function () {
            bFlag = jQuery('#selcatoff').is(':checked');
            if (bFlag) {
                jQuery('#catsel').hide();
            }
        });

        jQuery("#classified-selcaton").click(function () {
            if (jQuery('#classified-selcaton').is(':checked')) {
                jQuery('#classified-catsel').show();
            }
        });
        jQuery("#classified-selcatoff").click(function () {
            if (jQuery('#classified-selcatoff').is(':checked')) {
                jQuery('#classified-catsel').hide();
            }
        });

        //org tab
        jQuery("#org-selcaton").click(function () {
            if (jQuery('#org-selcaton').is(':checked')) {
                jQuery('#org-catsel').show();
            }
        });
        jQuery("#org-selcatoff").click(function () {
            if (jQuery('#org-selcatoff').is(':checked')) {
                jQuery('#org-catsel').hide();
            }
        });

        //business tab
        jQuery("#business-selcaton").click(function () {
            if (jQuery('#business-selcaton').is(':checked')) {
                jQuery('#business-catsel').show();
            }
        });
        jQuery("#business-selcatoff").click(function () {
            if (jQuery('#business-selcatoff').is(':checked')) {
                jQuery('#business-catsel').hide();
            }
        });

        //venue tab
        jQuery("#venue-selcaton").click(function () {
            if (jQuery('#venue-selcaton').is(':checked')) {
                jQuery('#venue-catsel').show();
            }
        });
        jQuery("#venue-selcatoff").click(function () {
            if (jQuery('#venue-selcatoff').is(':checked')) {
                jQuery('#venue-catsel').hide();
            }
        });

        jQuery("#event-tab").off("click").on("click", function (e) {
            e.preventDefault();
        });

        jQuery(".mini-site-tab").off("click").on("click", function (e) {
            e.preventDefault();
            var current = jQuery(e.currentTarget);
            jQuery(".mini-site-tab").removeClass('active');
            current.addClass('active');

            var tabs = {
                'event-tab': jQuery('#event-options'),
                'classified-tab': jQuery('#classified-options'),
                'org-tab': jQuery('#org-options'),
                'business-tab': jQuery('#business-options'),
                'venue-tab': jQuery('#venue-options')
            };

            for(var i in  tabs){
                if (current.attr('module-option') === i ) {
                    console.log(tabs[i]);
                    if (tabs[i].length > 0 && tabs[i].hasClass('hidden')) {
                        tabs[i].removeClass('hidden');
                    }
                }
                else{
                    if (tabs[i].length > 0 && !tabs[i].hasClass('hidden')) {
                        tabs[i].addClass('hidden');
                    }
                }
            }
        });
        jQuery('.meta-status-option').off('click').on('click', function(e){
            var current = jQuery(e.currentTarget);
            var parent = current.closest('.tab-content');
            var listOption = parent.find('.option');
            if (listOption.length > 0) {
                jQuery.each(listOption, function (index, item) {
                   if (current.val() == 'on') {
                           jQuery(item).find('input').prop('disabled', false);
                   } else {
                       if (!jQuery(item).hasClass('status')) {
                           jQuery(item).find('input').prop('disabled', true);
                       }
                   }
                });
            }
        });

    </script>

    <?php
}

/**
 * Saves the custom meta input
 */
function mini_meta_save( $post_id ) {

    global $post;
 
    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'mini_nonce' ] ) && wp_verify_nonce( $_POST[ 'mini_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
 
    // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }

    // update checkboxes
    if(isset($_POST[ 'meta-catsel' ])){
        update_post_meta( $post_id, 'meta-catsel', $_POST[ 'meta-catsel' ] );
    }
    if(isset($_POST[ 'meta-bypass' ])){
        update_post_meta( $post_id, 'meta-bypass', $_POST[ 'meta-bypass' ] );
    }
    if(isset($_POST[ 'classified-meta-bypass' ])){
        update_post_meta( $post_id, 'classified-meta-bypass', $_POST[ 'classified-meta-bypass' ] );
    }
    if(isset($_POST[ 'meta-copyall' ])){
        update_post_meta( $post_id, 'meta-copyall', $_POST[ 'meta-copyall' ] );
    }
    if(isset($_POST[ 'meta-classified-catsel' ])){
        update_post_meta( $post_id, 'meta-classified-catsel', $_POST['meta-classified-catsel']);
    }
    if(isset($_POST[ 'copy-all-classified' ])){
        update_post_meta( $post_id, 'copy-all-classified', $_POST['copy-all-classified']);
    }

    if( isset( $_POST[ 'meta-type' ] ) ) {
         update_post_meta( $post_id, 'meta-type', $_POST[ 'meta-type' ] );
    }
    if( isset( $_POST[ 'classified-meta-type' ] ) ) {
         update_post_meta( $post_id, 'classified-meta-type', $_POST[ 'classified-meta-type' ] );
    }
    if( isset( $_POST[ 'meta-notify' ] ) ) {
         update_post_meta( $post_id, 'meta-notify', $_POST[ 'meta-notify' ] );
    }

    if( isset( $_POST[ 'meta-copyonce' ] ) ) {
         update_post_meta( $post_id, 'meta-copyonce', $_POST[ 'meta-copyonce' ] );
    }

    if( isset( $_POST[ 'meta-mini' ] ) ) {
         update_post_meta( $post_id, 'meta-mini', $_POST[ 'meta-mini' ] );
    }

    if (isset($_POST['meta-event-status'])) {
        update_post_meta($post_id, 'meta-event-status', $_POST['meta-event-status']);
    }

    if (isset($_POST['meta-classified-status'])) {
        update_post_meta($post_id, 'meta-classified-status', $_POST['meta-classified-status']);
    }

    //venues
    updatePostMeta($post_id,'venue-meta-status');
    updatePostMeta($post_id,'venue-copy-all');
    updatePostMeta($post_id,'venue-meta-bypass');
    updatePostMeta($post_id,'venue-meta-catsel');
    updatePostMeta($post_id,'venue-meta-type');

    //org
    updatePostMeta($post_id,'org-meta-status');
    updatePostMeta($post_id,'org-copy-all');
    updatePostMeta($post_id,'org-meta-bypass');
    updatePostMeta($post_id,'org-meta-catsel');
    updatePostMeta($post_id,'org-meta-type');

    //business
    updatePostMeta($post_id,'business-meta-status');
    updatePostMeta($post_id,'business-copy-all');
    updatePostMeta($post_id,'business-meta-bypass');
    updatePostMeta($post_id,'business-meta-catsel');
    updatePostMeta($post_id,'business-meta-type');
}

function updatePostMeta($postId, $key){
    if (isset($_POST[$key])) {
        update_post_meta($postId, $key, $_POST[$key]);
    }

}

add_action( 'save_post', 'mini_meta_save' );

?>