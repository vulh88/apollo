<?php
$businessDisabled = (empty($mini_stored_meta['business-meta-status'][0]) || $mini_stored_meta['business-meta-status'][0] == 'off');
?>

    <div id="business-options" class="tab-content hidden">
        <div class="option status">
            <span class="option-title"><?php _e('STATUS: ', 'apollo'); ?></span>
            <input type="radio" class="meta-status-option" name="business-meta-status" value="on"
                <?php echo !$businessDisabled ? 'checked' : ''; ?>
            /><?php _e('On', 'apollo'); ?>
            <br>
            <input type="radio" class="meta-status-option" name="business-meta-status" value="off"
                <?php echo $businessDisabled ? 'checked' : ''; ?>
            /><?php _e('Off', 'apollo'); ?>
        </div>
        <div class="option">
            <span class="option-title"><?php _e('COPY OPTIONS:', 'apollo'); ?></span>
            <input type="radio" name="business-copy-all" value="on" <?php echo $businessDisabled ? 'disabled' : ''; ?>
                   class="copy-all-business" <?php if (isset($mini_stored_meta['business-copy-all'][0]) && $mini_stored_meta['business-copy-all'][0] == 'on') echo "checked"; ?> > <?php _e('Copy all busineses', 'apollo'); ?>
            <br>
            <input type="radio" name="business-copy-all" value="off" <?php echo $businessDisabled ? 'disabled' : ''; ?>
                   class="copy-all-business" <?php if (isset($mini_stored_meta['business-copy-all'][0]) && (($mini_stored_meta['business-copy-all'][0] == 'off') OR ($mini_stored_meta['business-copy-all'][0] == ''))) echo "checked"; ?> > <?php _e('Copy selected businesss', 'apollo'); ?>
        </div>
        <div class="option">
            <span class="option-title"><?php _e('PUBLISHING OPTIONS:', 'apollo'); ?></span>
            <input type="radio" name="business-meta-bypass" <?php echo $businessDisabled ? 'disabled' : ''; ?>
                   value="on" <?php if (isset($mini_stored_meta['business-meta-bypass'][0]) && (($mini_stored_meta['business-meta-bypass'][0] == 'on') OR ($mini_stored_meta['business-meta-bypass'][0] == ''))) echo "checked"; ?>> <?php _e('Publish Immediately', 'apollo'); ?>
            <br>
            <input type="radio" name="business-meta-bypass" <?php echo $businessDisabled ? 'disabled' : ''; ?>
                   value="off" <?php if (isset($mini_stored_meta['business-meta-bypass'][0]) && $mini_stored_meta['business-meta-bypass'][0] == 'off') echo "checked"; ?>> <?php _e('Require approval before publishing', 'apollo'); ?>
        </div>
        <div class="option">
            <span class="option-title"><?php _e('CATEGORY OPTIONS:', 'apollo'); ?></span>
            <?php if (isset($mini_stored_meta['business-meta-catsel'][0]) && (($mini_stored_meta['business-meta-catsel'][0] == 'off') OR ($mini_stored_meta['business-meta-catsel'][0] == ''))) {
                $check_business_catsel = ' checked ';
                $business_distype = 'none';
            } else {
                $check_business_catsel = '';
                $business_distype = 'inline';
            } ?>
            <input type=radio id="business-selcatoff"
                   name="business-meta-catsel" <?php echo $businessDisabled ? 'disabled' : ''; ?>
                   value="off" <?php echo $check_business_catsel; ?>> <?php _e('Copy all categories', 'apollo'); ?>
            <br>
            <?php if (isset($mini_stored_meta['business-meta-catsel'][0]) && $mini_stored_meta['business-meta-catsel'][0] == 'on') {
                $check_business_catsel = ' checked ';
                $business_distype = 'inline';
            } else {
                $check_business_catsel = '';
                $business_distype = 'none';
            } ?>
            <input type=radio id="business-selcaton"
                   name="business-meta-catsel" <?php echo $businessDisabled ? 'disabled' : ''; ?>
                   value="on" <?php echo $check_business_catsel; ?>> <?php _e('Copy selected categories', 'apollo'); ?>
            <br>
            <?php echo renderbusinessCategory($post, $business_distype); ?>
        </div>
    </div>

<?php

function renderbusinessCategory($post, $hide)
{
    $result = '<div id="business-catsel" style="display: ' . $hide . '">';
    $result .= '<p>' . __('Please select the Categories and or Subcategories for the businesses to be automatically copied to the mini-site.', 'apollo');
    $event_types = get_post_meta($post->ID, 'business-meta-type', TRUE);

    $args = array('orderby' => 'name', 'order' => 'ASC', 'hide_empty' => 0);
    $types = get_terms('business-type', $args);
    $limit = count($types);
    $limit = (int)($limit / 4);
    $split = 1;

    $result .= "<table width=100% border=0><tr><td valign=top>";

    foreach ($types as $type) {

        if ($type->parent != 0) continue;

        if ($event_types && in_array($type->term_id, $event_types)) {
            $checked = 'checked';
        } else {
            $checked = '';
        }
        $result .= " <input type='checkbox' name='business-meta-type[]' value='" . $type->term_id . "' " . $checked . " >" . $type->name . "<br>";

        $split++;
        if ($split >= $limit) {
            $result .= "</td><td valign=top>";
            $split = 1;
        }

        $children = get_term_children($type->term_id, 'event-type');

        foreach ($children as $child) {

            if (in_array($child, $event_types)) {
                $checked = 'checked';
            } else {
                $checked = '';
            }
            $name = get_term_by('id', absint($child), 'business-type');
            $result .= " &mdash; <input type='checkbox' name='business-meta-type[]' value='" . $child . "' " . $checked . " >" . $name->name . " &nbsp;<br>";
            $split++;
            if ($split >= $limit) {
                $result .= "</td><td valign=top>";
                $split = 1;
            }
        }
    }
    $result .= "</td></tr></table>";
    $result .= '</p></div>';
    return $result;
}
