<?php

$venueDisabled = (empty($mini_stored_meta['venue-meta-status'][0]) || $mini_stored_meta['venue-meta-status'][0] == 'off');

?>

    <div id="venue-options" class="tab-content hidden">
        <div class="option status">
            <span class="option-title"><?php _e('STATUS: ', 'apollo'); ?></span>
            <input type="radio" class="meta-status-option" name="venue-meta-status" value="on"
                <?php echo !$venueDisabled ? 'checked' : ''; ?>
            /><?php _e('On', 'apollo'); ?>
            <br>
            <input type="radio" class="meta-status-option" name="venue-meta-status" value="off"
                <?php echo $venueDisabled ? 'checked' : ''; ?>
            /><?php _e('Off', 'apollo'); ?>
        </div>
        <div class="option">
            <span class="option-title"><?php _e('COPY OPTIONS:', 'apollo'); ?></span>
            <input type="radio" name="venue-copy-all" value="on" <?php echo $venueDisabled ? 'disabled' : ''; ?>
                   class="copy-all-venue" <?php if (isset($mini_stored_meta['venue-copy-all'][0]) && $mini_stored_meta['venue-copy-all'][0] == 'on') echo "checked"; ?> > <?php _e('Copy all venues', 'apollo'); ?>
            <br>
            <input type="radio" name="venue-copy-all" value="off" <?php echo $venueDisabled ? 'disabled' : ''; ?>
                   class="copy-all-venue" <?php if (isset($mini_stored_meta['venue-copy-all'][0]) && (($mini_stored_meta['venue-copy-all'][0] == 'off') OR ($mini_stored_meta['venue-copy-all'][0] == ''))) echo "checked"; ?> > <?php _e('Copy selected venues', 'apollo'); ?>
        </div>
        <div class="option">
            <span class="option-title"><?php _e('PUBLISHING OPTIONS:', 'apollo'); ?></span>
            <input type="radio" name="venue-meta-bypass" <?php echo $venueDisabled ? 'disabled' : ''; ?>
                   value="on" <?php if (isset($mini_stored_meta['venue-meta-bypass'][0]) && (($mini_stored_meta['venue-meta-bypass'][0] == 'on') OR ($mini_stored_meta['venue-meta-bypass'][0] == ''))) echo "checked"; ?>> <?php _e('Publish Immediately', 'apollo'); ?>
            <br>
            <input type="radio" name="venue-meta-bypass" <?php echo $venueDisabled ? 'disabled' : ''; ?>
                   value="off" <?php if (isset($mini_stored_meta['venue-meta-bypass'][0]) && $mini_stored_meta['venue-meta-bypass'][0] == 'off') echo "checked"; ?>> <?php _e('Require approval before publishing', 'apollo'); ?>
        </div>
        <div class="option">
            <span class="option-title"><?php _e('CATEGORY OPTIONS:', 'apollo'); ?></span>
            <?php if (isset($mini_stored_meta['venue-meta-catsel'][0]) && (($mini_stored_meta['venue-meta-catsel'][0] == 'off') OR ($mini_stored_meta['venue-meta-catsel'][0] == ''))) {
                $check_venue_catsel = ' checked ';
                $venue_distype = 'none';
            } else {
                $check_venue_catsel = '';
                $venue_distype = 'inline';
            } ?>
            <input type=radio id="venue-selcatoff"
                   name="venue-meta-catsel" <?php echo $venueDisabled ? 'disabled' : ''; ?>
                   value="off" <?php echo $check_venue_catsel; ?>> <?php _e('Copy all categories', 'apollo'); ?>
            <br>
            <?php if (isset($mini_stored_meta['venue-meta-catsel'][0]) && $mini_stored_meta['venue-meta-catsel'][0] == 'on') {
                $check_venue_catsel = ' checked ';
                $venue_distype = 'inline';
            } else {
                $check_venue_catsel = '';
                $venue_distype = 'none';
            } ?>
            <input type=radio id="venue-selcaton"
                   name="venue-meta-catsel" <?php echo $venueDisabled ? 'disabled' : ''; ?>
                   value="on" <?php echo $check_venue_catsel; ?>> <?php _e('Copy selected categories', 'apollo'); ?>
            <br>
            <?php echo renderVenueCategory($post, $venue_distype); ?>
        </div>
    </div>

<?php

function renderVenueCategory($post, $hide)
{
    $result = '<div id="venue-catsel" style="display: ' . $hide . '">';
    $result .= '<p>' . __('Please select the Categories and or Subcategories for the venues to be automatically copied to the mini-site.', 'apollo');
    $event_types = get_post_meta($post->ID, 'venue-meta-type', TRUE);

    $args = array('orderby' => 'name', 'order' => 'ASC', 'hide_empty' => 0);
    $types = get_terms('venue-type', $args);
    $limit = count($types);
    $limit = (int)($limit / 4);
    $split = 1;

    $result .= "<table width=100% border=0><tr><td valign=top>";

    foreach ($types as $type) {

        if ($type->parent != 0) continue;

        if ($event_types && in_array($type->term_id, $event_types)) {
            $checked = 'checked';
        } else {
            $checked = '';
        }
        $result .= " <input type='checkbox' name='venue-meta-type[]' value='" . $type->term_id . "' " . $checked . " >" . $type->name . "<br>";

        $split++;
        if ($split >= $limit) {
            $result .= "</td><td valign=top>";
            $split = 1;
        }

        $children = get_term_children($type->term_id, 'event-type');

        foreach ($children as $child) {

            if (in_array($child, $event_types)) {
                $checked = 'checked';
            } else {
                $checked = '';
            }
            $name = get_term_by('id', absint($child), 'venue-type');
            $result .= " &mdash; <input type='checkbox' name='venue-meta-type[]' value='" . $child . "' " . $checked . " >" . $name->name . " &nbsp;<br>";
            $split++;
            if ($split >= $limit) {
                $result .= "</td><td valign=top>";
                $split = 1;
            }
        }
    }
    $result .= "</td></tr></table>";
    $result .= '</p></div>';
    return $result;
}