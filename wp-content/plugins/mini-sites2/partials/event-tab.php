<div id="event-options" class="tab-content">
    <div class="option status">
        <span class="option-title"><?php _e('STATUS: ', 'apollo'); ?></span>
        <input type="radio" class="meta-status-option" name="meta-event-status" value="on"
            <?php echo !$eventDisabled ? 'checked' : '';?>
        /><?php _e('On', 'apollo');?>
        <br>
        <input type="radio" class="meta-status-option" name="meta-event-status" value="off"
            <?php echo $eventDisabled ? 'checked' : ''; ?>
        /><?php _e('Off', 'apollo');?>
    </div>
    <div class="option">
        <span class="option-title"><?php _e('COPY OPTIONS:', 'apollo'); ?></span>
        <input type=radio name=meta-copyall <?php echo $eventDisabled ? 'disabled' : ''; ?>
               value="on" <?php if (isset($mini_stored_meta['meta-copyall'][0]) && $mini_stored_meta['meta-copyall'][0] == 'on') echo "checked"; ?>> Copy all
        events<br>
        <input type=radio name=meta-copyall <?php echo $eventDisabled ? 'disabled' : ''; ?>
               value="off" <?php if (isset($mini_stored_meta['meta-copyall'][0]) && (($mini_stored_meta['meta-copyall'][0] == 'off') OR ($mini_stored_meta['meta-copyall'][0] == ''))) echo "checked"; ?>>
        Copy only selected events
    </div>
    <div class="option">
        <span class="option-title"><?php _e('PUBLISHING OPTIONS:', 'apollo'); ?></span>
        <input type=radio name=meta-bypass <?php echo $eventDisabled ? 'disabled' : ''; ?>
               value="on" <?php if (isset($mini_stored_meta['meta-bypass'][0]) && (($mini_stored_meta['meta-bypass'][0] == 'on') OR ($mini_stored_meta['meta-bypass'][0] == ''))) echo "checked"; ?>>
        Publish Immediately<br>
        <input type=radio name=meta-bypass <?php echo $eventDisabled ? 'disabled' : ''; ?>
               value="off" <?php if (isset($mini_stored_meta['meta-bypass'][0]) && $mini_stored_meta['meta-bypass'][0] == 'off') echo "checked"; ?>> Require
        approval before publishing
    </div>
    <div class="option">
        <span class="option-title"><?php _e('CATEGORY OPTIONS:', 'apollo'); ?></span>
        <?php if (isset($mini_stored_meta['meta-catsel'][0]) && (($mini_stored_meta['meta-catsel'][0] == 'off') OR ($mini_stored_meta['meta-catsel'][0] == ''))) {
            $checkcatsel = ' checked ';
            $distype = 'none';
        } else {
            $checkcatsel = '';
            $distype = 'inline';
        } ?>
        <input type=radio id="selcatoff" name="meta-catsel" value="off" <?php echo $eventDisabled ? 'disabled' : ''; ?> <?php echo $checkcatsel; ?>> Copy all
        categories<br>
        <?php if (isset($mini_stored_meta['meta-catsel'][0]) && $mini_stored_meta['meta-catsel'][0] == 'on') {
            $checkcatsel = ' checked ';
            $distype = 'inline';
        } else {
            $checkcatsel = '';
            $distype = 'none';
        } ?>
        <input type=radio id="selcaton" name="meta-catsel" value="on" <?php echo $eventDisabled ? 'disabled' : ''; ?> <?php echo $checkcatsel; ?>> Copy selected
        categories<br>
        <?php echo renderEventCategory($post, $distype); ?>
    </div>
</div>
<?php
function renderEventCategory($post, $hide){
    $result = '<div id=catsel style="display:'.$hide.'">';
    $result .= '<p>' . __( 'Please select the Categories and or Subcategories for the events to be automatically copied to the mini-site.', 'apollo');
    $event_types = get_post_meta($post->ID, 'meta-type', TRUE);

    $args = array('orderby' => 'name', 'order' => 'ASC', 'hide_empty' => 0);
    $types = get_terms('event-type', $args);
    $limit = count($types);
    $limit = (int) ($limit / 4);
    $split = 1;

    $result .= '<table width=100% border=0><tr><td valign=top>';

    foreach ($types as $type){

        if ($type->parent != 0) continue;

        if ($event_types && in_array($type->term_id, $event_types)) {$checked = 'checked'; } else {$checked = '';}
        $result.= " <input type='checkbox' name='meta-type[]' value='".$type->term_id."' ".$checked." >".$type->name."<br>";

        $split++;
        if ($split >= $limit) {
            $result.= "</td><td valign=top>";
            $split=1;
        }

        $children = get_term_children($type->term_id, 'event-type');

        foreach ($children as $child) {

            if ($event_types && in_array($child, $event_types)) {$checked = 'checked'; } else {$checked = '';}
            $name = get_term_by( 'id', absint($child), 'event-type' );
            $result .= " &mdash; <input type='checkbox' name='meta-type[]' value='".$child."' ".$checked." >".$name->name." &nbsp;<br>";
            $split++;
            if ($split >= $limit) {
                $result .= "</td><td valign=top>";
                $split=1;
            }
        }
    }
    $result .= "</td></tr></table>";
    $result .= '</p></div>';
    return $result;
}
