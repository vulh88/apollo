<div id="classified-options" class="tab-content hidden">
    <div class="option status">
        <span class="option-title"><?php _e('STATUS: ', 'apollo'); ?></span>
        <input type="radio" class="meta-status-option" name="meta-classified-status" value="on"
            <?php echo !$classifiedDisabled ? 'checked' : '';?>
        /><?php _e('On', 'apollo');?>
        <br>
        <input type="radio" class="meta-status-option" name="meta-classified-status" value="off"
            <?php echo $classifiedDisabled ? 'checked' : ''; ?>
        /><?php _e('Off', 'apollo');?>
    </div>
    <div class="option">
        <span class="option-title"><?php _e('COPY OPTIONS:', 'apollo'); ?></span>
        <input type="radio" name="copy-all-classified" value="on" <?php echo $classifiedDisabled ? 'disabled' : ''; ?>
               class="copy-all-classified" <?php if (isset($mini_stored_meta['copy-all-classified'][0]) && $mini_stored_meta['copy-all-classified'][0] == 'on') echo "checked"; ?> > <?php _e('Copy all classifieds', 'apollo'); ?>
        <br>
        <input type="radio" name="copy-all-classified" value="off" <?php echo $classifiedDisabled ? 'disabled' : ''; ?>
               class="copy-all-classified" <?php if (isset($mini_stored_meta['copy-all-classified'][0]) &&(($mini_stored_meta['copy-all-classified'][0] == 'off') OR ($mini_stored_meta['copy-all-classified'][0] == ''))) echo "checked"; ?> > <?php _e('Copy selected Classifieds', 'apollo'); ?>
    </div>
    <div class="option">
        <span class="option-title"><?php _e('PUBLISHING OPTIONS:', 'apollo'); ?></span>
        <input type="radio" name="classified-meta-bypass" <?php echo $classifiedDisabled ? 'disabled' : ''; ?>
               value="on" <?php if (isset($mini_stored_meta['classified-meta-bypass'][0]) && (($mini_stored_meta['classified-meta-bypass'][0] == 'on') OR ($mini_stored_meta['classified-meta-bypass'][0] == ''))) echo "checked"; ?>> <?php _e('Publish Immediately', 'apollo'); ?>
        <br>
        <input type="radio" name="classified-meta-bypass" <?php echo $classifiedDisabled ? 'disabled' : ''; ?>
               value="off" <?php if (isset($mini_stored_meta['classified-meta-bypass'][0]) && $mini_stored_meta['classified-meta-bypass'][0] == 'off') echo "checked"; ?>> <?php _e('Require approval before publishing', 'apollo'); ?>
    </div>
    <div class="option">
        <span class="option-title"><?php _e('CATEGORY OPTIONS:', 'apollo'); ?></span>
        <?php if (isset($mini_stored_meta['meta-classified-catsel'][0]) && (($mini_stored_meta['meta-classified-catsel'][0] == 'off') OR ($mini_stored_meta['meta-classified-catsel'][0] == ''))) {
            $check_classified_catsel = ' checked ';
            $classified_distype = 'none';
        } else {
            $check_classified_catsel = '';
            $classified_distype = 'inline';
        } ?>
        <input type=radio id="classified-selcatoff" name="meta-classified-catsel" <?php echo $classifiedDisabled ? 'disabled' : ''; ?>
               value="off" <?php echo $check_classified_catsel; ?>> <?php _e('Copy all categories', 'apollo'); ?>
        <br>
        <?php if (isset($mini_stored_meta['meta-classified-catsel'][0]) && $mini_stored_meta['meta-classified-catsel'][0] == 'on') {
            $check_classified_catsel = ' checked ';
            $classified_distype = 'inline';
        } else {
            $check_classified_catsel = '';
            $classified_distype = 'none';
        } ?>
        <input type=radio id="classified-selcaton" name="meta-classified-catsel" <?php echo $classifiedDisabled ? 'disabled' : ''; ?>
               value="on" <?php echo $check_classified_catsel; ?>> <?php _e('Copy selected categories', 'apollo'); ?>
        <br>
        <?php echo renderClassifiedCategory($post, $classified_distype); ?>
    </div>
</div>
<?php
function renderClassifiedCategory($post, $hide){
    $result = '<div id="classified-catsel" style="display: '.$hide.'">';
    $result .= '<p>' . __('Please select the Categories and or Subcategories for the classifieds to be automatically copied to the mini-site.', 'apollo');
    $event_types = get_post_meta($post->ID, 'classified-meta-type', TRUE);

    $args = array('orderby' => 'name', 'order' => 'ASC', 'hide_empty' => 0);
    $types = get_terms('classified-type', $args);
    $limit = count($types);
    $limit = (int) ($limit / 4);
    $split = 1;

    $result.= "<table width=100% border=0><tr><td valign=top>";

    foreach ($types as $type){

        if ($type->parent != 0) continue;

        if ($event_types && in_array($type->term_id, $event_types)) {
            $checked = 'checked';
        } else {
            $checked = '';
        }
        $result.= " <input type='checkbox' name='classified-meta-type[]' value='".$type->term_id."' ".$checked." >".$type->name."<br>";

        $split++;
        if ($split >= $limit) {
            $result.= "</td><td valign=top>";
            $split=1;
        }

        $children = get_term_children($type->term_id, 'event-type');

        foreach ($children as $child) {

            if (in_array($child, $event_types)) {$checked = 'checked'; } else {$checked = '';}
            $name = get_term_by( 'id', absint($child), 'classified-type' );
            $result.= " &mdash; <input type='checkbox' name='classified-meta-type[]' value='".$child."' ".$checked." >".$name->name." &nbsp;<br>";
            $split++;
            if ($split >= $limit) {$result.= "</td><td valign=top>"; $split=1;}
        }
    }
    $result.= "</td></tr></table>";
    $result.= '</p></div>';
    return $result;

}