<?php
$orgDisabled = (empty($mini_stored_meta['org-meta-status'][0]) || $mini_stored_meta['org-meta-status'][0] == 'off');
?>
    <div id="org-options" class="tab-content hidden">
        <div class="option status">
            <span class="option-title"><?php _e('STATUS: ', 'apollo'); ?></span>
            <input type="radio" class="meta-status-option" name="org-meta-status" value="on"
                <?php echo !$orgDisabled ? 'checked' : ''; ?>
            /><?php _e('On', 'apollo'); ?>
            <br>
            <input type="radio" class="meta-status-option" name="org-meta-status" value="off"
                <?php echo $orgDisabled ? 'checked' : ''; ?>
            /><?php _e('Off', 'apollo'); ?>
        </div>
        <div class="option">
            <span class="option-title"><?php _e('COPY OPTIONS:', 'apollo'); ?></span>
            <input type="radio" name="org-copy-all" value="on" <?php echo $orgDisabled ? 'disabled' : ''; ?>
                   class="copy-all-org" <?php if (isset($mini_stored_meta['org-copy-all'][0]) && $mini_stored_meta['org-copy-all'][0] == 'on') echo "checked"; ?> > <?php _e('Copy all organizations', 'apollo'); ?>
            <br>
            <input type="radio" name="org-copy-all" value="off" <?php echo $orgDisabled ? 'disabled' : ''; ?>
                   class="copy-all-org" <?php if (isset($mini_stored_meta['org-copy-all'][0]) && (($mini_stored_meta['org-copy-all'][0] == 'off') OR ($mini_stored_meta['org-copy-all'][0] == ''))) echo "checked"; ?> > <?php _e('Copy selected orgs', 'apollo'); ?>
        </div>
        <div class="option">
            <span class="option-title"><?php _e('PUBLISHING OPTIONS:', 'apollo'); ?></span>
            <input type="radio" name="org-meta-bypass" <?php echo $orgDisabled ? 'disabled' : ''; ?>
                   value="on" <?php if (isset($mini_stored_meta['org-meta-bypass'][0]) && (($mini_stored_meta['org-meta-bypass'][0] == 'on') OR ($mini_stored_meta['org-meta-bypass'][0] == ''))) echo "checked"; ?>> <?php _e('Publish Immediately', 'apollo'); ?>
            <br>
            <input type="radio" name="org-meta-bypass" <?php echo $orgDisabled ? 'disabled' : ''; ?>
                   value="off" <?php if (isset($mini_stored_meta['org-meta-bypass'][0]) && $mini_stored_meta['org-meta-bypass'][0] == 'off') echo "checked"; ?>> <?php _e('Require approval before publishing', 'apollo'); ?>
        </div>
        <div class="option">
            <span class="option-title"><?php _e('CATEGORY OPTIONS:', 'apollo'); ?></span>
            <?php if (isset($mini_stored_meta['org-meta-catsel'][0]) && (($mini_stored_meta['org-meta-catsel'][0] == 'off') OR ($mini_stored_meta['org-meta-catsel'][0] == ''))) {
                $check_org_catsel = ' checked ';
                $org_distype = 'none';
            } else {
                $check_org_catsel = '';
                $org_distype = 'inline';
            } ?>
            <input type=radio id="org-selcatoff" name="org-meta-catsel" <?php echo $orgDisabled ? 'disabled' : ''; ?>
                   value="off" <?php echo $check_org_catsel; ?>> <?php _e('Copy all categories', 'apollo'); ?>
            <br>
            <?php if (isset($mini_stored_meta['org-meta-catsel'][0]) && $mini_stored_meta['org-meta-catsel'][0] == 'on') {
                $check_org_catsel = ' checked ';
                $org_distype = 'inline';
            } else {
                $check_org_catsel = '';
                $org_distype = 'none';
            } ?>
            <input type=radio id="org-selcaton" name="org-meta-catsel" <?php echo $orgDisabled ? 'disabled' : ''; ?>
                   value="on" <?php echo $check_org_catsel; ?>> <?php _e('Copy selected categories', 'apollo'); ?>
            <br>
            <?php echo renderOrgCategory($post, $org_distype); ?>
        </div>
    </div>

<?php

function renderOrgCategory($post, $hide)
{
    $result = '<div id="org-catsel" style="display: ' . $hide . '">';
    $result .= '<p>' . __('Please select the Categories and or Subcategories for the organizations to be automatically copied to the mini-site.', 'apollo');
    $event_types = get_post_meta($post->ID, 'org-meta-type', TRUE);

    $args = array('orderby' => 'name', 'order' => 'ASC', 'hide_empty' => 0);
    $types = get_terms('organization-type', $args);
    $limit = count($types);
    $limit = (int)($limit / 4);
    $split = 1;

    $result .= "<table width=100% border=0><tr><td valign=top>";

    foreach ($types as $type) {

        if ($type->parent != 0) continue;

        if ($event_types && in_array($type->term_id, $event_types)) {
            $checked = 'checked';
        } else {
            $checked = '';
        }
        $result .= " <input type='checkbox' name='org-meta-type[]' value='" . $type->term_id . "' " . $checked . " >" . $type->name . "<br>";

        $split++;
        if ($split >= $limit) {
            $result .= "</td><td valign=top>";
            $split = 1;
        }

        $children = get_term_children($type->term_id, 'event-type');

        foreach ($children as $child) {

            if (in_array($child, $event_types)) {
                $checked = 'checked';
            } else {
                $checked = '';
            }
            $name = get_term_by('id', absint($child), 'org-type');
            $result .= " &mdash; <input type='checkbox' name='org-meta-type[]' value='" . $child . "' " . $checked . " >" . $name->name . " &nbsp;<br>";
            $split++;
            if ($split >= $limit) {
                $result .= "</td><td valign=top>";
                $split = 1;
            }
        }
    }
    $result .= "</td></tr></table>";
    $result .= '</p></div>';
    return $result;
}