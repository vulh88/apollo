/**

 <div id="apl-iframe" data-url="{site_url}" data-width="400" data-height="1000" data-flag="{defined_constant}" data-ifswid={iframe-search-widget-id}></div>
 <script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "{site_url}/custom-iframe-handler.js";
  fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'apl-elinext-jssdk'));</script>

 */

function apl_hookLoad(handler) {
    if (window.addEventListener) {
        window.addEventListener("load", handler, false);
    }
    else if (window.attachEvent) {
        window.attachEvent("onload", handler);
    }
}

function apl_removeElementsByClass(className){
    var elements = document.getElementsByClassName(className);
    while(elements.length > 0){
        elements[0].parentNode.removeChild(elements[0]);
    }
}

function apl_loadJSCSSFile(filename, filetype){
    if (filetype=="js"){ //if filename is a external JavaScript file
        var fileref = document.createElement('script')
        fileref.setAttribute("type","text/javascript")
        fileref.setAttribute("src", filename)
    }
    else if (filetype=="css"){ //if filename is an external CSS file
        var fileref = document.createElement("link")
        fileref.setAttribute("rel", "stylesheet")
        fileref.setAttribute("type", "text/css")
        fileref.setAttribute("href", filename)
    }
    if (typeof fileref!="undefined")
        document.getElementsByTagName("head")[0].appendChild(fileref)
}

function apl_handler_iframe() {

    apl_loadJSCSSFile("http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,800italic,400,300,700,800",'css');
    var iframe_parent = document.getElementById('apl-iframe');
    //var srcJS = document.getElementById('apl-elinext-jssdk').src;
	var currentUrl = iframe_parent.getAttribute('data-url');
    var iframeWidthOpt = parseInt(iframe_parent.getAttribute('data-width'));
    var iframeHeightOpt = parseInt(iframe_parent.getAttribute('data-height'));
    var iframePrimaryColor = iframe_parent.getAttribute('data-primary-color');
    console.log(iframePrimaryColor);
    iframePrimaryColor = typeof iframePrimaryColor === 'undefined' || iframePrimaryColor === '' ? '#fcb234' : iframePrimaryColor;

    var iframe_root_domain = currentUrl;

    var iframe_root_id = 'apl-iframe';

    // get iframe parent url
    var iframe_dest_url =  window.location.host;
    iframe_dest_url = encodeURIComponent(iframe_dest_url);

    var iframe_link = iframe_root_domain + '?dest_url=' + iframe_dest_url;

    // get iframe search widget flag detection
    var iframe_flag = iframe_parent.getAttribute('data-flag');
	if(iframe_flag != null && iframe_flag.trim() != '') {
		iframe_link += '&' + iframe_flag + '=1';
	}

    var iframeswid = parseInt(iframe_parent.getAttribute('data-ifswid'));
    iframe_link += '&ifswid=' + iframeswid;

    var iframesw_ele_id = '__apl_iframe_search_widget';
    var iframe_elem = document.createElement('iframe');
    iframe_elem.src = iframe_link;

    var device_with = (window.innerWidth > 0) ? window.innerWidth : screen.width;
    var parent_width = iframe_parent.offsetWidth;
    
    iframe_elem.width = iframeWidthOpt > 0 ? iframeWidthOpt : Math.min(parent_width, device_with);
    iframe_elem.height = iframeHeightOpt > 0 ? iframeHeightOpt : 1000;
    iframe_elem.frameBorder = 0;
    iframe_elem.id = iframesw_ele_id;
    iframe_elem.style.visibility = 'hidden';
    iframe_elem.onload = apl_iframeLoadedHandler;


    String.prototype.toDOM=function(){
        var d=document
            ,i
            ,a=d.createElement("div")
            ,b=d.createDocumentFragment();
        a.innerHTML=this;
        while(i=a.firstChild)b.appendChild(i);
        return b;
    };
    var loadingOffsetTop = (iframe_elem.height / 2) - 20 - 50;
    var loadingOffsetLeft = (iframe_elem.width / 2) - 25;

    var htmlIFrameLoading = '' +
        '<div class="blockUI" style="display:none"></div>' +
        '<div class="blockUI blockOverlay" style="z-index: 1000; border: none; margin: 0px; padding: 0px; width:'+iframe_elem.width+'; height: '+iframe_elem.height+'; top: 0px; left: 0px; opacity: 0.6; cursor: wait; position: absolute; background: none;"></div>' +
        '<div class="blockUI blockMsg blockElement" style="z-index: 1011; position: absolute; padding: 0px; margin: 0px; width: 50px; top: '+loadingOffsetTop+'px; left: '+loadingOffsetLeft+'px; text-align: center; color: rgb(0, 0, 0); border: none; cursor: wait; background: none;">' +
        '   <a style="box-shadow: none;" ><i class="fa fa-spinner fa-spin fa-3x" style="color: '+iframePrimaryColor+';"></i></a>' +
        '</div>'
    var divWarpLoading = htmlIFrameLoading.toDOM();

    document.getElementById(iframe_root_id).appendChild(divWarpLoading);
    // append iframe to div "iframe_root_id"
    document.getElementById(iframe_root_id).appendChild(iframe_elem);

    if (window.addEventListener) {
        window.addEventListener("message", receiveMessage, false);
    }
    else if (window.attachEvent) {
        window.attachEvent('onmessage', receiveMessage);
    }

    function receiveMessage(event) {
        
        if(!isNaN(parseInt(event.data))) {
            document.getElementById(iframesw_ele_id).height = parseInt(event.data);
        }
    }

    // add event window resize landscape
    window.addEventListener('resize', apl_setWindowSize);
    function apl_setWindowSize() {
        var device_with = (window.innerWidth > 0) ? window.innerWidth : screen.width;
        var parent_width = iframe_parent.offsetWidth;
        document.getElementById(iframesw_ele_id).width = Math.min(parent_width, device_with);
    }

    function apl_iframeLoadedHandler(){
        apl_removeElementsByClass('blockUI');
        this.style.visibility = 'visible';
    }
}



apl_hookLoad(apl_handler_iframe);