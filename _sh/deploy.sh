#!/usr/bin/env bash
git remote set-url origin 'http://git.elidev.info/nhanlt/apollo-theme.git';
git checkout dev;
git pull origin dev;
git push origin dev;
git checkout master;
git pull origin master;
git merge dev;
git checkout production;
git pull origin production;
git merge dev;
git push origin production;
git checkout dev;