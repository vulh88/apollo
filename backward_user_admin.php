<?php
define('DOING_AJAX', true);
define('WP_USE_THEMES', false);
//define('SHORTINIT', true);
$_SERVER = array(
    "HTTP_HOST" => "WPCLI",
    "SERVER_NAME" => "WPCLI",
    "REQUEST_URI" => "/",
    "REQUEST_METHOD" => "GET"
);

/* Fix error internal in wordpress */
global $current_blog, $current_site;

$current_blog = new stdClass();
$current_blog->blog_id = 'WPCLI';
$current_blog->site_id = 'WPCLI';

$current_site = new stdClass();
$current_site->domain = 'WPCLI';
$current_site->blog_id = 'WPCLI';
$current_site->path = 'WPCLI';

require __DIR__.'/wp-load.php';


//SELECT * FROM `wp_usermeta` where user_id = 1 and meta_key like '%capabilities'

global $wpdb;
$sql = "select blog_id from $wpdb->blogs where public = '1'";

$data = $wpdb->get_results($sql);
/* Update site */

$asuperadmin = get_super_admins();
$ssuperadmin = "('".implode("','", $asuperadmin)."')";
$tbl = $wpdb->base_prefix._APL_BLOGUSER;


foreach($data as $obj) {
    $blog_id = $obj->blog_id;

    $level_key = $wpdb->get_blog_prefix($blog_id) . 'capabilities';

    $tbl_user_meta = $wpdb->usermeta;

    // check if this user has exits this meta key
    foreach($asuperadmin as $user_admin) {
        $userObj =  $wpdb->get_row("select ID from {$wpdb->users} where user_login = '$user_admin' limit 1");

        $user_id =$userObj->ID;
        $sql = "select * from $tbl_user_meta where meta_key = '$level_key' and user_id = $user_id";
        $result = $wpdb->query($sql);

        if($result >= 1) continue;

        $sql = "INSERT INTO $tbl_user_meta (user_id, meta_key, meta_value)
                VALUE ( $user_id, '$level_key', 'a:1:{s:13:\"administrator\";b:1;}')
                ";

        $result = $wpdb->query($sql);
    }
}